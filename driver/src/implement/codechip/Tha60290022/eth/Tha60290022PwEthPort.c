/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290022PwEthPort.c
 *
 * Created Date: Jun 7, 2017
 *
 * Description : ETH Port for PW binding
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022PwEthPortInternal.h"
#include "Tha60290022ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tThaEthPortMethods   m_ThaEthPortOverride;

/* Save super implementation */
static const tAtChannelMethods     *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxBandwidthInKbps(ThaEthPort self)
    {
    AtUnused(self);
    return 40000000; /* 40G */
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (counterType == cAtEthPortCounterRxDiscardedPackets)
        return cAtTrue;

    return m_AtChannelMethods->CounterIsSupported(self, counterType);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel port = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(port), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        }

    mMethodsSet(port, &m_AtChannelOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort port = (ThaEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, mMethodsGet(port), sizeof(m_ThaEthPortOverride));

        mMethodOverride(m_ThaEthPortOverride, MaxBandwidthInKbps);
        }

    mMethodsSet(port, &m_ThaEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PwEthPort);
    }

AtEthPort Tha60290022PwEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021PwEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290022PwEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022PwEthPortObjectInit(newPort, portId, module);
    }

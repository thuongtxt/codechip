/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290022FaceplateEthPortCounters.c
 *
 * Created Date: Mar 22, 2019
 *
 * Description : 60290022 Faceplate Ethernet port counters
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/Tha60290022Device.h"
#include "Tha60290022ModuleEth.h"
#include "Tha60290022FaceplateEthPortCountersInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021EthPortCountersMethods m_Tha60290021EthPortCountersOverride;

/* Save super implementation */
static const tTha60290021EthPortCountersMethods     *m_Tha60290021EthPortCountersMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eBool ShouldAccumulate(Tha60290021EthPortCounters self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasAllCounters32(Tha60290021EthPortCounters self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self->port);
    return Tha60290022DeviceFaceplateEthCounter32bitIsSupported(device);
    }

static uint32 UpperCounterGet(Tha60290021EthPortCounters self, uint32 numBits, uint32 *longRegVal)
    {
    if (HasAllCounters32(self))
        return longRegVal[1];

    return m_Tha60290021EthPortCountersMethods->UpperCounterGet(self, numBits, longRegVal);
    }

static uint32 LowerCounterGet(Tha60290021EthPortCounters self, uint32 numBits, uint32 *longRegVal)
    {
    if (HasAllCounters32(self))
        return longRegVal[0];

    return m_Tha60290021EthPortCountersMethods->LowerCounterGet(self, numBits, longRegVal);
    }

static void OverrideTha60290021EthPortCounters(Tha60290021EthPortCounters self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021EthPortCountersMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021EthPortCountersOverride,
                                  mMethodsGet(self), sizeof(m_Tha60290021EthPortCountersOverride));

        mMethodOverride(m_Tha60290021EthPortCountersOverride, ShouldAccumulate);
        mMethodOverride(m_Tha60290021EthPortCountersOverride, UpperCounterGet);
        mMethodOverride(m_Tha60290021EthPortCountersOverride, LowerCounterGet);
        }

    mMethodsSet(self, &m_Tha60290021EthPortCountersOverride);
    }

static void Override(Tha60290021EthPortCounters self)
    {
    OverrideTha60290021EthPortCounters(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022FaceplateEthPortCounters);
    }

Tha60290021EthPortCounters Tha60290022FaceplateEthPortCountersObjectInit(Tha60290021EthPortCounters self, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021EthPortCountersObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha60290021EthPortCounters Tha60290022FaceplateEthPortCountersNew(AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60290021EthPortCounters newCounters = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newCounters == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022FaceplateEthPortCountersObjectInit(newCounters, port);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290022SgmiiSohEthPortInternal.h
 * 
 * Created Date: Mar 2, 2018
 *
 * Description : Internal data of the SOH eth port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022SGMIISOHETHPORTINTERNAL_H_
#define _THA60290022SGMIISOHETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/eth/Tha60290021SgmiiSohEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022SgmiiSohEthPort
    {
    tTha60290021SgmiiSohEthPort super;
    }tTha60290022SgmiiSohEthPort;

typedef struct tTha60290022SgmiiSohEthPortV2
    {
    tTha60290022SgmiiSohEthPort super;
    }tTha60290022SgmiiSohEthPortV2;

typedef struct tTha60290022SgmiiSohEthPortV3
    {
    tTha60290022SgmiiSohEthPortV2 super;
    }tTha60290022SgmiiSohEthPortV3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290022SgmiiSohEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);
AtEthPort Tha60290022SgmiiSohEthPortV2ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022SGMIISOHETHPORTINTERNAL_H_ */


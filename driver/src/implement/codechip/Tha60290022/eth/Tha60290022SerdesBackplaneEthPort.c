/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290022SerdesBackplaneEthPort.c
 *
 * Created Date: Apr 25, 2017
 *
 * Description : ETH Port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022SerdesBackplaneEthPortInternal.h"
#include "Tha60290022ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEthPortMethods                         m_AtEthPortOverride;
static tTha60290021SerdesBackplaneEthPortMethods m_Tha60290021SerdesBackplaneEthPortOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CounterTxOversizeUndersizeSupported(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FecIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasInterruptMaskRegister(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, FecIsSupported);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideTha60290021SerdesBackplaneEthPort(AtEthPort self)
    {
    Tha60290021SerdesBackplaneEthPort port = (Tha60290021SerdesBackplaneEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021SerdesBackplaneEthPortOverride, mMethodsGet(port), sizeof(m_Tha60290021SerdesBackplaneEthPortOverride));

        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, CounterTxOversizeUndersizeSupported);
        mMethodOverride(m_Tha60290021SerdesBackplaneEthPortOverride, HasInterruptMaskRegister);
        }

    mMethodsSet(port, &m_Tha60290021SerdesBackplaneEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtEthPort(self);
    OverrideTha60290021SerdesBackplaneEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SerdesBackplaneEthPort);
    }

AtEthPort Tha60290022SerdesBackplaneEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021SerdesBackplaneEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290022SerdesBackplaneEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022SerdesBackplaneEthPortObjectInit(newPort, portId, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290022FaceplateSerdesEthPortV3.c
 *
 * Created Date: Mar 28, 2018
 *
 * Description : Implement the LF/RF for 100M fx
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022FaceplateSerdesEthPortInternal.h"
#include "Tha60290022SgmiiMultirateReg.h"
#include "Tha60290022ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#define c100MFxInterruptMaskLfFromApp (cAtEthPortAlarmLinkDown | cAtEthPortAlarmLossofDataSync)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tTha60290021FaceplateSerdesEthPortMethods m_Tha60290021FaceplateSerdesEthPortOverride;
static tAtEthPortMethods m_AtEthPortOverride;

/* Cache super implementation */
static const tAtChannelMethods *m_AtChannelMethod  = NULL;
static const tTha60290021FaceplateSerdesEthPortMethods *m_Tha60290021FaceplateSerdesEthPortMethod = NULL;
static const tAtEthPortMethods *m_AtEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 LocalId(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    return Tha60290021ModuleEthFaceplateLocalId(ethModule, (uint8)AtChannelIdGet((AtChannel)self));
    }

static uint8 LocalIdForMultiRate(AtEthPort self)
    {
    return (uint8)(LocalId(self) % 16);
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    AtChannel port = (AtChannel)self;
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(AtChannelDeviceGet(port));
    if (serdesManager)
        return AtSerdesManagerSerdesControllerGet(serdesManager, LocalId(self));
    return NULL;
    }

static eBool SerdesIsIn100MFxMode(AtEthPort self)
    {
    if (AtSerdesControllerModeGet(SerdesController(self)) == cAtSerdesModeEth100M)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 Eth100MFxLfMask(AtEthPort self)
    {
    return cBit0 << (LocalIdForMultiRate(self) * 2UL);
    }

static uint32 Eth100MFxLfShift(AtEthPort self)
    {
    return (LocalIdForMultiRate(self) * 2UL);
    }

static uint32 Eth100MFxRfMask(AtEthPort self)
    {
    return cBit0 << ((LocalIdForMultiRate(self) * 2UL) + 1UL);
    }

static uint32 Eth100MFxRfShift(AtEthPort self)
    {
    return ((LocalIdForMultiRate(self) * 2UL) + 1UL);
    }

static uint32 Eth100MFxInterruptStickyAddress(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint8 sixteenPortGroupId = Tha60290021FaceplateSerdesEthPortSixteenPortGroupId(self);
    uint32 address = cAf6Reg_fx_stk_fefd_pen_Base + Tha602900xxModuleEthMultirateBaseAddress(ethModule, sixteenPortGroupId);
    return address;
    }

static uint32 Eth100MFxAlarmAddress(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint8 sixteenPortGroupId = Tha60290021FaceplateSerdesEthPortSixteenPortGroupId(self);
    uint32 address = cAf6Reg_fx_alm_fefd_pen_Base + Tha602900xxModuleEthMultirateBaseAddress(ethModule, sixteenPortGroupId);
    return address;
    }

static uint32 Eth100MFxInterruptEnableAddress(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint8 sixteenPortGroupId = Tha60290021FaceplateSerdesEthPortSixteenPortGroupId(self);
    uint32 address = cAf6Reg_fx_fefd_inten_Base + Tha602900xxModuleEthMultirateBaseAddress(ethModule, sixteenPortGroupId);
    return address;
    }

static uint32 Eth100MFxDefectGet(AtEthPort self)
    {
    uint32 regAddr = Eth100MFxAlarmAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 defects = 0;

    if (regVal & Eth100MFxLfMask(self))
        defects |= c100MFxInterruptMaskLfFromApp;

    if (regVal & Eth100MFxRfMask(self))
        defects |= cAtEthPortAlarmRemoteFault;

    return defects;
    }

static uint32 DefectGet(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 defects = 0;

    if (SerdesIsIn100MFxMode(port))
        defects |= Eth100MFxDefectGet(port);
    else
        defects |= m_AtChannelMethod->DefectGet(self);

    return defects;
    }

static eAtEthPortLinkStatus LinkStatus(AtEthPort self)
    {
    if (SerdesIsIn100MFxMode(self))
        {
        if (Eth100MFxDefectGet(self) & cAtEthPortAlarmLinkDown)
            return cAtEthPortLinkStatusDown;
        return cAtEthPortLinkStatusUp;
        }

    return m_AtEthPortMethods->LinkStatus(self);
    }

static uint32 Eth100MFxDefectRead2ClearGet(AtEthPort self, eBool read2Clear)
    {
    uint32 regAddr = Eth100MFxInterruptStickyAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 defects = 0;
    uint32 lfMask = Eth100MFxLfMask(self);
    uint32 rfMask = Eth100MFxRfMask(self);

    if (read2Clear)
        mChannelHwWrite(self, regAddr, (lfMask | rfMask), cAtModuleEth);

    if (regVal & lfMask)
        defects |= c100MFxInterruptMaskLfFromApp;

    if (regVal & rfMask)
        defects |= cAtEthPortAlarmRemoteFault;

    return defects;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 defects = 0;

    if (SerdesIsIn100MFxMode(port))
        defects = Eth100MFxDefectRead2ClearGet(port, cAtFalse);
    else
        defects = m_AtChannelMethod->DefectHistoryGet(self);

    return defects;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 defects = 0;

    if (SerdesIsIn100MFxMode(port))
        defects = Eth100MFxDefectRead2ClearGet(port, cAtTrue);
    else
        defects = m_AtChannelMethod->DefectHistoryClear(self);

    return defects;
    }

static uint32 Eth100MFxInterruptProcess(AtEthPort self, uint32 ethIntr)
    {
    uint32 events = 0, defects = 0;
    uint32 regStkAddr, regAlarmAddr, regStkVal, regAlarmVal;
    uint32 lfMask = Eth100MFxLfMask(self);
    uint32 rfMask = Eth100MFxRfMask(self);

    if (ethIntr & cAf6_GE_Interrupt_OR_fx100base_int_or_Mask)
        {
        regStkAddr = Eth100MFxInterruptStickyAddress(self);
        regStkVal  = mChannelHwRead(self, regStkAddr, cAtModuleEth);
        regAlarmAddr = Eth100MFxAlarmAddress(self);
        regAlarmVal  = mChannelHwRead(self, regAlarmAddr, cAtModuleEth);
        if (regStkVal & lfMask)
            {
            events  |= c100MFxInterruptMaskLfFromApp;
            if (regAlarmVal & lfMask)
                defects |= c100MFxInterruptMaskLfFromApp;
            }
        if (regStkVal & rfMask)
            {
            events  |= cAtEthPortAlarmRemoteFault;
            if (regAlarmVal & rfMask)
                defects |= cAtEthPortAlarmRemoteFault;
            }


        mChannelHwWrite(self, regStkAddr, (lfMask|rfMask), cAtModuleEth);
        }

    if (events)
        AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);

    return defects;
    }

static uint32 OneGeInterruptProcess(AtEthPort self, uint32 ethIntr)
    {
    uint32 defects = m_Tha60290021FaceplateSerdesEthPortMethod->OneGeInterruptProcess(self, ethIntr);
    defects |= Eth100MFxInterruptProcess(self, ethIntr);
    return defects;
    }

static uint32 Eth100MFxSupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return c100MFxInterruptMaskLfFromApp | cAtEthPortAlarmRemoteFault;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;

    if (SerdesIsIn100MFxMode(port))
        return Eth100MFxSupportedInterruptMasks(self);

    return m_AtChannelMethod->SupportedInterruptMasks(self);
    }

static eAtRet Eth100MFxInterruptMaskSet(AtEthPort self, uint32 defectMask, uint32 enableMask)
    {
    uint32 enabled, lf_Mask, lf_Shift, rf_Mask, rf_Shift;
    uint32 address = Eth100MFxInterruptEnableAddress(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModuleEth);

    if (defectMask & c100MFxInterruptMaskLfFromApp)
        {
        enabled = (enableMask & c100MFxInterruptMaskLfFromApp) ? 1 : 0;
        lf_Mask = Eth100MFxLfMask(self);
        lf_Shift = Eth100MFxLfShift(self);
        mRegFieldSet(regVal, lf_, enabled);
        }

    if (defectMask & cAtEthPortAlarmRemoteFault)
        {
        enabled = (enableMask & cAtEthPortAlarmRemoteFault) ? 1 : 0;
        rf_Mask = Eth100MFxRfMask(self);
        rf_Shift = Eth100MFxRfShift(self);
        mRegFieldSet(regVal, rf_, enabled);
        }

    mChannelHwWrite(self, address, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 Eth100MFxInterruptMaskGet(AtEthPort self)
    {
    uint32 masks = 0;

    uint32 enabled, lf_Mask, lf_Shift, rf_Mask, rf_Shift;
    uint32 address = Eth100MFxInterruptEnableAddress(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModuleEth);

    lf_Mask = Eth100MFxLfMask(self);
    lf_Shift = Eth100MFxLfShift(self);
    enabled = mRegField(regVal, lf_);
    if (enabled)
        masks |= c100MFxInterruptMaskLfFromApp;

    rf_Mask = Eth100MFxRfMask(self);
    rf_Shift = Eth100MFxRfShift(self);
    enabled = mRegField(regVal, rf_);
    if (enabled)
        masks |= cAtEthPortAlarmRemoteFault;


    return masks;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    AtEthPort port = (AtEthPort)self;

    if (SerdesIsIn100MFxMode(port))
        return Eth100MFxInterruptMaskSet(port, defectMask, enableMask);

    return m_AtChannelMethod->InterruptMaskSet(self, defectMask, enableMask);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;

    if (SerdesIsIn100MFxMode(port))
        return Eth100MFxInterruptMaskGet(port);

    return m_AtChannelMethod->InterruptMaskGet(self);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethod = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethod, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(tAtEthPortMethods));

        mMethodOverride(m_AtEthPortOverride, LinkStatus);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideTha60290021FaceplateSerdesEthPort(AtEthPort self)
    {
    Tha60290021FaceplateSerdesEthPort port = (Tha60290021FaceplateSerdesEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021FaceplateSerdesEthPortMethod = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021FaceplateSerdesEthPortOverride,
                                  m_Tha60290021FaceplateSerdesEthPortMethod, sizeof(m_Tha60290021FaceplateSerdesEthPortOverride));

        mMethodOverride(m_Tha60290021FaceplateSerdesEthPortOverride, OneGeInterruptProcess);
        }

    mMethodsSet(port, &m_Tha60290021FaceplateSerdesEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideTha60290021FaceplateSerdesEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022FaceplateSerdesEthPortV3);
    }

AtEthPort Tha60290022FaceplateSerdesEthPortV3ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022FaceplateSerdesEthPortV2ObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290022FaceplateSerdesEthPortV3New(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022FaceplateSerdesEthPortV3ObjectInit(newPort, portId, module);
    }

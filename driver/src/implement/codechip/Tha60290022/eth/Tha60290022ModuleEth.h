/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290022ModuleEth.h
 * 
 * Created Date: Apr 25, 2017
 *
 * Description : ETH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEETH_H_
#define _THA60290022MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/eth/Tha60290021EthPortCounters.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290022FaceplateSerdesEthPortV3New(uint8 portId, AtModuleEth module);
AtEthPort Tha60290022FaceplateSerdesEthPortV2New(uint8 portId, AtModuleEth module);
AtEthPort Tha60290022FaceplateSerdesEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290022SerdesBackplaneEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290022PwEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290022SgmiiSohEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290022SgmiiSohEthPortV2New(uint8 portId, AtModuleEth module);
AtEthPort Tha60290022SgmiiSohEthPortV3New(uint8 portId, AtModuleEth module);

Tha60290021EthPortCounters Tha60290022FaceplateEthPortCountersNew(AtEthPort port);

eAtRet Tha60290022ModuleEthFaceplateMacLoopbackSet(AtModuleEth self, eAtLoopbackMode mode);
eAtRet Tha60290022ModuleEthBackplaneMacLoopbackSet(AtModuleEth self, eAtLoopbackMode mode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEETH_H_ */


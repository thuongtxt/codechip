/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290022FaceplateSerdesEthPort.c
 *
 * Created Date: Apr 25, 2017
 *
 * Description : ETH Port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "Tha60290022ModuleEth.h"
#include "Tha60290022FaceplateSerdesEthPortInternal.h"
#include "../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021FaceplateSerdesEthPortMethods m_Tha60290021FaceplateSerdesEthPortOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Interface100BaseFxIsSupported(Tha60290021FaceplateSerdesEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasDisparityFeature(Tha60290021FaceplateSerdesEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtDevice Device(Tha60290021FaceplateSerdesEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return device;
    }

static eBool HasK30_7Feature(Tha60290021FaceplateSerdesEthPort self)
    {
    return Tha60290022DeviceFaceplatSerdesEthHasK30_7Feature(Device(self));
    }

static uint32 StartVersionSupportTxAlarmForcing(Tha60290021FaceplateSerdesEthPort self)
    {
    return Tha60290022DeviceFaceplatSerdesEthStartVersionSupportTxAlarmForcing(Device(self));
    }

static eBool ExcessiveErrorRatioIsSupported(Tha60290021FaceplateSerdesEthPort self)
    {
    return Tha60290022DeviceFaceplatSerdesEthExcessiveErrorRatioIsSupported(Device(self));
    }

static void OverrideTha60290021FaceplateSerdesEthPort(AtEthPort self)
    {
    Tha60290021FaceplateSerdesEthPort port = (Tha60290021FaceplateSerdesEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021FaceplateSerdesEthPortOverride, mMethodsGet(port), sizeof(m_Tha60290021FaceplateSerdesEthPortOverride));

        mMethodOverride(m_Tha60290021FaceplateSerdesEthPortOverride, Interface100BaseFxIsSupported);
        mMethodOverride(m_Tha60290021FaceplateSerdesEthPortOverride, HasDisparityFeature);
        mMethodOverride(m_Tha60290021FaceplateSerdesEthPortOverride, HasK30_7Feature);
        mMethodOverride(m_Tha60290021FaceplateSerdesEthPortOverride, StartVersionSupportTxAlarmForcing);
        mMethodOverride(m_Tha60290021FaceplateSerdesEthPortOverride, ExcessiveErrorRatioIsSupported);
        }

    mMethodsSet(port, &m_Tha60290021FaceplateSerdesEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideTha60290021FaceplateSerdesEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022FaceplateSerdesEthPort);
    }

AtEthPort Tha60290022FaceplateSerdesEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021FaceplateSerdesEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290022FaceplateSerdesEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022FaceplateSerdesEthPortObjectInit(newPort, portId, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290022FaceplateEthPortCountersInternal.h
 * 
 * Created Date: Mar 22, 2019
 *
 * Description : 60290022 Faceplate Ethernet port counters
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022FACEPLATEETHPORTCOUNTERSINTERNAL_H_
#define _THA60290022FACEPLATEETHPORTCOUNTERSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/eth/Tha60290021EthPortCountersInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022FaceplateEthPortCounters
    {
    tTha60290021EthPortCounters super;
    }tTha60290022FaceplateEthPortCounters;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60290021EthPortCounters Tha60290022FaceplateEthPortCountersObjectInit(Tha60290021EthPortCounters self, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022FACEPLATEETHPORTCOUNTERSINTERNAL_H_ */


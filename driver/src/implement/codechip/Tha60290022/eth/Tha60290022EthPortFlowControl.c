/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290022EthPortFlowControl.c
 *
 * Created Date: Apr 9, 2018
 *
 * Description : Implementation for 60290022 flow control
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/eth/Tha60290021EthPortFlowControlInternal.h"
#include "../../Tha60290021/eth/Tha60290021ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_upen_stabufwater_Base 0x840
#define cAf6_upen_stabufwater_MaxBufferLength_Mask cBit15_8
#define cAf6_upen_stabufwater_MaxBufferLength_Shift 8
#define cAf6_upen_stabufwater_MinBufferLength_Mask cBit7_0
#define cAf6_upen_stabufwater_MinBufferLength_Shift 0

#define cAf6Reg_upen_stathreshold_Base               0x830
#define cAf6_upen_stathreshold_BufferLength_Mask   cBit7_0
#define cAf6_upen_stathreshold_BufferLength_Shift        0

#define cThaEthFlowControlWaterMarkResetMax 0x0
#define cThaEthFlowControlWaterMarkResetMin 0xFF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290022EthPortFlowControl *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022EthPortFlowControl
    {
    tTha60290021EthPortFlowControl super;
    }tTha60290022EthPortFlowControl;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEthFlowControlMethods m_AtEthFlowControlOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 OffSet(AtEthFlowControl self)
    {
    AtChannel port = AtEthFlowControlChannelGet(self);
    AtModuleEth ethModule = (AtModuleEth) AtChannelModuleGet(port);
    return (uint32) Tha60290021ModuleEthFaceplateLocalId(ethModule, (uint8) AtChannelIdGet(port));
    }

static uint32 WaterMarkStatusReg(AtEthFlowControl self)
    {
    return OffSet(self) + cAf6Reg_upen_stabufwater_Base + Tha60290021ModuleEthBypassBaseAddress();
    }

static uint32 HwTxFifoWaterMarkMinClear(AtEthFlowControl self, eBool r2c)
    {
    uint32 regAddr, regVal, ret;
    AtChannel port = AtEthFlowControlChannelGet(self);

    regAddr = WaterMarkStatusReg(self);
    regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    ret = mRegField(regVal, cAf6_upen_stabufwater_MinBufferLength_);

    if (r2c)
        {
        mRegFieldSet(regVal, cAf6_upen_stabufwater_MinBufferLength_, cThaEthFlowControlWaterMarkResetMin);
        mChannelHwWrite(port, regAddr, regVal, cAtModuleEth);
        }

    return ret;
    }

static uint32 TxFifoWaterMarkMinGet(AtEthFlowControl self)
    {
    return HwTxFifoWaterMarkMinClear(self, cAtFalse);
    }

static uint32 TxFifoWaterMarkMinClear(AtEthFlowControl self)
    {
    return HwTxFifoWaterMarkMinClear(self, cAtTrue);
    }

static uint32 HwTxFifoWaterMarkMaxGet(AtEthFlowControl self, eBool r2c)
    {
    uint32 regAddr, regVal, ret;
    AtChannel port = AtEthFlowControlChannelGet(self);

    regAddr = WaterMarkStatusReg(self);
    regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    ret = mRegField(regVal, cAf6_upen_stabufwater_MaxBufferLength_);

    if (r2c)
        {
        mRegFieldSet(regVal, cAf6_upen_stabufwater_MaxBufferLength_, cThaEthFlowControlWaterMarkResetMax);
        mChannelHwWrite(port, regAddr, regVal, cAtModuleEth);
        }

    return ret;
    }

static uint32 TxFifoWaterMarkMaxGet(AtEthFlowControl self)
    {
    return HwTxFifoWaterMarkMaxGet(self, cAtFalse);
    }

static uint32 TxFifoWaterMarkMaxClear(AtEthFlowControl self)
    {
    return HwTxFifoWaterMarkMaxGet(self, cAtTrue);
    }

static uint32 BufferStatusReg(AtEthFlowControl self)
    {
    return OffSet(self) + cAf6Reg_upen_stathreshold_Base + Tha60290021ModuleEthBypassBaseAddress();
    }

static uint32 TxFifoLevelGet(AtEthFlowControl self)
    {
    uint32 regAddr = BufferStatusReg(self);
    uint32 regVal = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_upen_stathreshold_BufferLength_);
    }

static void OverrideAtEthFlowControl(AtEthFlowControl self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthFlowControlOverride, mMethodsGet(self), sizeof(m_AtEthFlowControlOverride));

        mMethodOverride(m_AtEthFlowControlOverride, TxFifoWaterMarkMaxGet);
        mMethodOverride(m_AtEthFlowControlOverride, TxFifoWaterMarkMaxClear);
        mMethodOverride(m_AtEthFlowControlOverride, TxFifoWaterMarkMinGet);
        mMethodOverride(m_AtEthFlowControlOverride, TxFifoWaterMarkMinClear);
        mMethodOverride(m_AtEthFlowControlOverride, TxFifoLevelGet);
        }

    mMethodsSet(self, &m_AtEthFlowControlOverride);
    }

static void Override(AtEthFlowControl self)
    {
    OverrideAtEthFlowControl(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022EthPortFlowControl);
    }

static AtEthFlowControl ObjectInit(AtEthFlowControl self, AtModule module, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021EthPortFlowControlObjectInit(self, module, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthFlowControl Tha60290022EthPortFlowControlNew(AtModule module, AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthFlowControl newFlowCtrl = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlowCtrl == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlowCtrl, module, port);
    }

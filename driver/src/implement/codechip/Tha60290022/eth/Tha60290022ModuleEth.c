/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290022ModuleEth.c
 *
 * Created Date: Apr 25, 2017
 *
 * Description : ETH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022ModuleEthInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/common/Tha6029DccKbyte.h"
#include "../man/Tha60290022DeviceReg.h"
#include "../man/Tha60290022Device.h"
#include "Tha60290022ModuleEth.h"
#include "Tha60290022SgmiiMultirateReg.h"
#include "Tha60290022ModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleEthMethods         m_ThaModuleEthOverride;
static tTha60290021ModuleEthMethods m_Tha60290021ModuleEthOverride;
static tAtModuleEthMethods          m_AtModuleEthOverride;

/* Save super implementation */
static const tAtModuleMethods      *m_AtModuleMethods     = NULL;
static const tThaModuleEthMethods  *m_ThaModuleEthMethods = NULL;
static const tAtModuleEthMethods   *m_AtModuleEthMethods  = NULL;
static const tTha60290021ModuleEthMethods  *m_Tha60290021ModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool UsePdhSgmiiPrbs(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtEthPort FaceplatePortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (Tha60290022DeviceEth100MFxLfRfSupported(device))
        return Tha60290022FaceplateSerdesEthPortV3New(portId, (AtModuleEth)self);

    if (Tha60290022DeviceTxIpgIsSupported(device))
        return Tha60290022FaceplateSerdesEthPortV2New(portId, (AtModuleEth)self);

    return Tha60290022FaceplateSerdesEthPortNew(portId, (AtModuleEth)self);
    }

static AtEthPort SerdesBackplaneEthPortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    return Tha60290022SerdesBackplaneEthPortNew(portId, (AtModuleEth)self);
    }

static eBool InterruptIsSupported(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtEthPort PwPortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    return Tha60290022PwEthPortNew(portId, (AtModuleEth)self);
    }

static uint32 MacLoopbackAddress(AtModuleEth self)
    {
    AtUnused(self);
    return cAf6Reg_DebugMacLoopControl_Base;
    }

static eBool FaceplateMacLoopbackIsSupported(AtModuleEth self, eAtLoopbackMode mode)
    {
    AtUnused(self);
    if ((mode == cAtLoopbackModeRelease) ||
        (mode == cAtLoopbackModeRemote))
        return cAtTrue;
    return cAtFalse;
    }

static eBool BackplaneMacLoopbackIsSupported(AtModuleEth self, eAtLoopbackMode mode)
    {
    AtUnused(self);
    if ((mode == cAtLoopbackModeRelease) ||
        (mode == cAtLoopbackModeLocal))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet FaceplateMacLoopbackSet(AtModuleEth self, eAtLoopbackMode mode)
    {
    uint32 regAddr = MacLoopbackAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 enabled = (mode == cAtLoopbackModeRemote) ? 1 : 0;
    mRegFieldSet(regVal, cAf6_DebugMacLoopControl_MacMroLoopOut_, enabled);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtLoopbackMode FaceplateMacLoopbackGet(AtModuleEth self)
    {
    uint32 regAddr = MacLoopbackAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if ((regVal & cAf6_DebugMacLoopControl_MacMroLoopOut_Mask))
        return cAtLoopbackModeRemote;
    return cAtLoopbackModeRelease;
    }

static eAtRet BackplaneMacLoopbackSet(AtModuleEth self, eAtLoopbackMode mode)
    {
    uint32 regAddr = MacLoopbackAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 enabled = (mode == cAtLoopbackModeLocal) ? 1 : 0;
    mRegFieldSet(regVal, cAf6_DebugMacLoopControl_Mac40gLoopIn_, enabled);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtLoopbackMode BackplaneMacLoopbackGet(AtModuleEth self)
    {
    uint32 regAddr = MacLoopbackAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    if ((regVal & cAf6_DebugMacLoopControl_Mac40gLoopIn_Mask))
        return cAtLoopbackModeLocal;
    return cAtLoopbackModeRelease;
    }

static eAtRet LoopbackShow(AtModule self)
    {
    AtModuleEth module = (AtModuleEth)self;

    AtPrintc(cSevNormal, "* MAC loopbacks \r\n");
    AtPrintc(cSevNormal, "    * Backplane : %s\r\n", AtLoopbackMode2Str(BackplaneMacLoopbackGet(module)));
    AtPrintc(cSevNormal, "    * Faceplate : %s\r\n", AtLoopbackMode2Str(FaceplateMacLoopbackGet(module)));

    return cAtOk;
    }

static eAtRet Debug(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Debug(self);
    if (ret != cAtOk)
        return ret;

    return LoopbackShow(self);
    }

static eBool DccEthMaxLengthConfigurationIsSupported(ThaModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return Tha60290022DeviceDccEthMaxLengthConfigurationIsSupported(device);
    }

static eAtRet DefaultSet(ThaModuleEth self)
    {
    static const uint32 cDccPacketMaxLength = 1536;
    eAtRet ret = cAtOk;

    ret |= Tha60290022ModuleEthFaceplateMacLoopbackSet((AtModuleEth)self, cAtLoopbackModeRelease);
    ret |= Tha60290022ModuleEthBackplaneMacLoopbackSet((AtModuleEth)self, cAtLoopbackModeRelease);

    if (DccEthMaxLengthConfigurationIsSupported(self))
        {
        ret |= ThaModuleEthRxDccPacketMaxLengthSet(self, cDccPacketMaxLength);
        ret |= ThaModuleEthTxDccPacketMaxLengthSet(self, cDccPacketMaxLength);
        }

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet((ThaModuleEth)self);
    }

static uint8 NumberOfFaceplateGroupsGet(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return 2;
    }

static AtEthPort KByteDccPortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (Tha60290022DeviceDccSgmiiTxIpg31ByteIsSupported(device))
        return Tha60290022SgmiiSohEthPortV3New(portId, (AtModuleEth)self);

    if (Tha60290022DeviceDccTxFcsMsbIsSupported(device))
        return Tha60290022SgmiiSohEthPortV2New(portId, (AtModuleEth)self);

    return Tha60290022SgmiiSohEthPortNew(portId, (AtModuleEth)self);
    }

static eBool HasSgmiiDiagnosticLogic(Tha60290021ModuleEth self)
    {
    return Tha60290022DeviceEthHasSgmiiDiagnosticLogic(AtModuleDeviceGet((AtModule)self));
    }

static eBool PortIsUsed(ThaModuleEth self, uint8 portId)
    {
    if (Tha60290021ModuleEthHasSgmiiDiagnosticLogic((Tha60290021ModuleEth)self))
        return m_ThaModuleEthMethods->PortIsUsed(self, portId);

    /* Only one port is used now */
    if (Tha60290021ModuleEthIsSgmiiPort((AtModuleEth)self, portId))
        return Tha60290021ModuleEthIsSgmiiDccKBytePort((AtModuleEth)self, portId) ? cAtTrue : cAtFalse;

    return m_ThaModuleEthMethods->PortIsUsed(self, portId);
    }

static AtEthFlowControl FaceplatePortFlowControlCreate(Tha60290021ModuleEth self, AtEthPort port)
    {
    AtDevice device = (AtDevice)AtModuleDeviceGet((AtModule)self);
    if (Tha60290022DeviceEthPortFlowControlWaterMarkIsSupported(device))
        return Tha60290022EthPortFlowControlNew((AtModule)self, port);

    return m_Tha60290021ModuleEthMethods->FaceplatePortFlowControlCreate(self, port);
    }

static Tha60290021EthPortCounters FaceplatePortCountersCreate(Tha60290021ModuleEth self, AtEthPort port)
    {
    AtUnused(self);
    return Tha60290022FaceplateEthPortCountersNew(port);
    }

static eAtRet TxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength)
    {
    if (DccEthMaxLengthConfigurationIsSupported(self))
        return Tha6029ModuleEthTxDccPacketMaxLengthSet(self, maxLength);

    return cAtErrorModeNotSupport;
    }

static uint32 TxDccPacketMaxLengthGet(ThaModuleEth self)
    {
    if (DccEthMaxLengthConfigurationIsSupported(self))
        return Tha6029ModuleEthTxDccPacketMaxLengthGet(self);

    return 0;
    }

static eAtRet RxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength)
    {
    if (DccEthMaxLengthConfigurationIsSupported(self))
        return Tha6029ModuleEthRxDccPacketMaxLengthSet(self, maxLength);

    return cAtErrorModeNotSupport;
    }

static uint32 RxDccPacketMaxLengthGet(ThaModuleEth self)
    {
    if (DccEthMaxLengthConfigurationIsSupported(self))
        return Tha6029ModuleEthRxDccPacketMaxLengthGet(self);

    return 0;
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, Init);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth module = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleEthMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(module), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, InterruptIsSupported);
        mMethodOverride(m_ThaModuleEthOverride, PortIsUsed);
        mMethodOverride(m_ThaModuleEthOverride, TxDccPacketMaxLengthSet);
        mMethodOverride(m_ThaModuleEthOverride, TxDccPacketMaxLengthGet);
        mMethodOverride(m_ThaModuleEthOverride, RxDccPacketMaxLengthSet);
        mMethodOverride(m_ThaModuleEthOverride, RxDccPacketMaxLengthGet);
        }

    mMethodsSet(module, &m_ThaModuleEthOverride);
    }

static void OverrideTha60290021ModuleEth(AtModuleEth self)
    {
    Tha60290021ModuleEth module = (Tha60290021ModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021ModuleEthMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModuleEthOverride, mMethodsGet(module), sizeof(m_Tha60290021ModuleEthOverride));

        mMethodOverride(m_Tha60290021ModuleEthOverride, UsePdhSgmiiPrbs);
        mMethodOverride(m_Tha60290021ModuleEthOverride, FaceplatePortCreate);
        mMethodOverride(m_Tha60290021ModuleEthOverride, SerdesBackplaneEthPortCreate);
        mMethodOverride(m_Tha60290021ModuleEthOverride, PwPortCreate);
        mMethodOverride(m_Tha60290021ModuleEthOverride, KByteDccPortCreate);
        mMethodOverride(m_Tha60290021ModuleEthOverride, NumberOfFaceplateGroupsGet);
        mMethodOverride(m_Tha60290021ModuleEthOverride, HasSgmiiDiagnosticLogic);
        mMethodOverride(m_Tha60290021ModuleEthOverride, FaceplatePortCountersCreate);
        mMethodOverride(m_Tha60290021ModuleEthOverride, FaceplatePortFlowControlCreate);
        }

    mMethodsSet(module, &m_Tha60290021ModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModule(self);
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    OverrideTha60290021ModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleEth);
    }

AtModuleEth Tha60290022ModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60290022ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModuleEthObjectInit(newModule, device);
    }

eAtRet Tha60290022ModuleEthFaceplateMacLoopbackSet(AtModuleEth self, eAtLoopbackMode mode)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (FaceplateMacLoopbackIsSupported(self, mode))
        return FaceplateMacLoopbackSet(self, mode);

    return cAtErrorModeNotSupport;
    }

eAtRet Tha60290022ModuleEthBackplaneMacLoopbackSet(AtModuleEth self, eAtLoopbackMode mode)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (BackplaneMacLoopbackIsSupported(self, mode))
        return BackplaneMacLoopbackSet(self, mode);

    return cAtErrorModeNotSupport;
    }

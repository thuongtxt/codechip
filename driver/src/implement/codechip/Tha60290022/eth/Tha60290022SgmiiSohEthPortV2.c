/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290022SgmiiSohEthPortV2.c
 *
 * Created Date: Mar 2, 2018
 *
 * Description : Implement counters for ETH port according to Kbyte and Dcc sgmii counter changes
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/common/Tha6029DccKbyte.h"
#include "Tha60290022SgmiiSohEthPortInternal.h"
#include "Tha60290022ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthPortMethods           m_ThaEthPortOverride;
static tAtChannelMethods            m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods      *m_AtChannelMethods  = NULL;
static const tThaEthPortMethods     *m_ThaEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterLocalAddress(ThaEthPort self, uint16 counterType)
    {
    return Tha6029MroDccKbyteEthPortCounterV3BaseAddress(self, counterType);
    }

static void InternalSgmiiPortCounterClear(AtChannel self)
    {
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeDccRxGoodFrames, cAtTrue);
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeDccRxDiscardFrames, cAtTrue);
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeKbyteRxGoodFrames, cAtTrue);
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeKbyteRxDiscardFrames, cAtTrue);
    }

static void StatusClear(AtChannel self)
    {
    m_AtChannelMethods->StatusClear(self);

    InternalSgmiiPortCounterClear(self);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort object = (ThaEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPortMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, m_ThaEthPortMethods, sizeof(m_ThaEthPortOverride));

        mMethodOverride(m_ThaEthPortOverride, CounterLocalAddress);
        }

    mMethodsSet(object, &m_ThaEthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, StatusClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SgmiiSohEthPortV2);
    }

AtEthPort Tha60290022SgmiiSohEthPortV2ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022SgmiiSohEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290022SgmiiSohEthPortV2New(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022SgmiiSohEthPortV2ObjectInit(newPort, portId, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290022ModuleEthInternal.h
 * 
 * Created Date: Jul 16, 2018
 *
 * Description : 60290022 module Eth internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEETHINTERNAL_H_
#define _THA60290022MODULEETHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/eth/Tha60290021ModuleEthInternal.h"
#include "Tha60290022ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModuleEth
    {
    tTha60290021ModuleEth super;
    }tTha60290022ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha60290022ModuleEthObjectInit(AtModuleEth self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEETHINTERNAL_H_ */


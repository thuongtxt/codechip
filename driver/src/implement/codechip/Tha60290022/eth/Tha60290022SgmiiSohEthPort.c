/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290022SgmiiSohEthPort.c
 *
 * Created Date: Sep 19, 2017
 *
 * Description : SGMII overhead port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/common/Tha6029DccKbyte.h"
#include "Tha60290022ModuleEth.h"
#include "../man/Tha60290022Device.h"
#include "Tha60290022SgmiiSohEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021SgmiiSerdesEthPortMethods m_Tha60290021SgmiiSerdesEthPortOverride;

/* Save super implementation */
static const tTha60290021SgmiiSerdesEthPortMethods *m_Tha60290021SgmiiSerdesEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(Tha60290021SgmiiSerdesEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return device;
    }

static eBool UseNewMac(Tha60290021SgmiiSerdesEthPort self)
    {
    return Tha60290022DeviceSgmiiSohEthUseNewMac(Device(self));
    }

static void OverrideTha60290021SgmiiSerdesEthPort(AtEthPort self)
    {
    Tha60290021SgmiiSerdesEthPort port = (Tha60290021SgmiiSerdesEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021SgmiiSerdesEthPortMethods = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021SgmiiSerdesEthPortOverride, m_Tha60290021SgmiiSerdesEthPortMethods, sizeof(m_Tha60290021SgmiiSerdesEthPortOverride));

        mMethodOverride(m_Tha60290021SgmiiSerdesEthPortOverride, UseNewMac);
        }

    mMethodsSet(port, &m_Tha60290021SgmiiSerdesEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideTha60290021SgmiiSerdesEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SgmiiSohEthPort);
    }

AtEthPort Tha60290022SgmiiSohEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021SgmiiSohEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290022SgmiiSohEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort port = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (port == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022SgmiiSohEthPortObjectInit(port, portId, module);
    }

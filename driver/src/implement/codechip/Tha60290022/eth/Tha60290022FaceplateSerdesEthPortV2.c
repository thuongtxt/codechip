/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290022FaceplatSerdesEthPortV2.c
 *
 * Created Date: Mar 6, 2018
 *
 * Description : Implement faceplate serdes eth port for IPG and  link down
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022FaceplateSerdesEthPortInternal.h"
#include "Tha60290022SgmiiMultirateReg.h"
#include "Tha60290022ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEthPortMethods m_AtEthPortOverride;
static tAtChannelMethods m_AtChannelOverride;
static tTha60290021FaceplateSerdesEthPortMethods m_Tha60290021FaceplateSerdesEthPortOverride;

/* Cache super implementation */
static const tAtChannelMethods *m_AtChannelMethod  = NULL;
static const tTha60290021FaceplateSerdesEthPortMethods *m_Tha60290021FaceplateSerdesEthPortMethod = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 LocalIdForMultiRate(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint8 localId = Tha60290021ModuleEthFaceplateLocalId(ethModule, (uint8)AtChannelIdGet((AtChannel)self));
    return (uint8)(localId % 16);
    }

static uint32 FaceplateGroup(AtEthPort self)
    {
    return LocalIdForMultiRate(self) / 8;
    }

static uint32 FaceplateLocalIdInGroup(AtEthPort self)
    {
    return LocalIdForMultiRate(self) % 8;
    }

static uint32 TxIpgAddressBase(AtEthPort self)
    {
    uint32 groupId = FaceplateGroup(self);

    if (groupId == 0)
        return cAf6Reg_TX_IPG_Config_Part0_Base;
    if (groupId == 1)
        return cAf6Reg_TX_IPG_Config_Part1_Base;

    return cInvalidUint32;
    }

static uint32 TxIpgAddress(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint8 sixteenPortGroupId = Tha60290021FaceplateSerdesEthPortSixteenPortGroupId(self);

    return TxIpgAddressBase(self) + Tha602900xxModuleEthMultirateBaseAddress(ethModule, sixteenPortGroupId);
    }

static void TxIpgRegFieldMaskShift(AtEthPort self, uint32 *mask, uint32 *shift)
    {
    *shift = 4 * FaceplateLocalIdInGroup(self);
    *mask = cBit3_0 << (*shift);
    }

static eBool IsTxIpgRangeSupported(uint8 txIpg)
    {
    if ((txIpg <= 30) && (txIpg %2 == 0))
        return cAtTrue;

    return cAtFalse;
    }

static eBool TxIpgIsInRange(AtEthPort self, uint8 ipg)
    {
    AtUnused(self);
    return IsTxIpgRangeSupported(ipg);
    }

static uint8 SwTxIpgToHwIpg(uint8 txIpg)
    {
    return txIpg / 2;
    }

static uint8 HwTxIpgToSwIpg(uint8 txIpg)
    {
    return (uint8)(txIpg * 2);
    }

static eAtModuleEthRet TxIpgSet(AtEthPort self, uint8 txIpg)
    {
    uint32 regAddr, regVal, txIpg_Mask, txIpg_Shift;

    if (!AtEthPortTxIpgIsInRange(self, txIpg))
        return cAtErrorOutOfRangParm;

    regAddr = TxIpgAddress(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    TxIpgRegFieldMaskShift(self, &txIpg_Mask, &txIpg_Shift);

    mRegFieldSet(regVal, txIpg_, SwTxIpgToHwIpg(txIpg));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint8 TxIpgGet(AtEthPort self)
    {
    uint32 regAddr, regVal, txIpg_Mask, txIpg_Shift;
    uint8 hwTxIpg;

    regAddr = TxIpgAddress(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    TxIpgRegFieldMaskShift(self, &txIpg_Mask, &txIpg_Shift);

    hwTxIpg = (uint8)mRegField(regVal, txIpg_);
    return HwTxIpgToSwIpg(hwTxIpg);
    }

static eBool IsLinkStatusApplicable(AtEthPort self)
    {
    if (AtEthPortSpeedGet(self) == cAtEthPortSpeed10G)
        return cAtFalse;
    return cAtTrue;
    }

static uint32 LinkDownDefectGet(AtEthPort self)
    {
    uint32 defect = 0;

    if (!IsLinkStatusApplicable(self))
        return defect;

    if (AtEthPortLinkStatus(self) == cAtEthPortLinkStatusDown)
        defect |= cAtEthPortAlarmLinkDown;

    return defect;
    }

static uint32 DefectGet(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 defects = 0;

    defects |= m_AtChannelMethod->DefectGet(self);
    defects |= LinkDownDefectGet(port);

    return defects;
    }

static uint32 GeLinkStatusChangeStickyAddress(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint8 sixteenPortGroupId = Tha60290021FaceplateSerdesEthPortSixteenPortGroupId(self);
    return cAf6Reg_Ge_Link_State_Sticky_Base + Tha602900xxModuleEthMultirateBaseAddress(ethModule, sixteenPortGroupId);
    }

static uint32 MultirateCommonAlarmMask(AtEthPort self)
    {
    return cBit0 << LocalIdForMultiRate(self);
    }

static uint32 MultirateCommonAlarmShift(AtEthPort self)
    {
    return LocalIdForMultiRate(self);
    }

static uint32 OneGeDefectLinkDownHistoryRead2Clear(AtChannel self, eBool read2Clear)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 regAddr = GeLinkStatusChangeStickyAddress((AtEthPort)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 fieldMask = MultirateCommonAlarmMask(port);

    if (read2Clear)
        mChannelHwWrite(self, regAddr, fieldMask, cAtModuleEth);

    return (regVal & fieldMask) ? cAtEthPortAlarmLinkDown : 0;
    }

static uint32 MultirateDefectHistoryRead2Clear(AtEthPort self, eBool read2Clear)
    {
    if (IsLinkStatusApplicable(self))
        return OneGeDefectLinkDownHistoryRead2Clear((AtChannel)self, read2Clear);

    return 0;
    }

static uint32 DefectHistoryRead2Clear(AtChannel self, eBool read2Clear)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 defects = 0;

    defects |= MultirateDefectHistoryRead2Clear(port, read2Clear);

    return defects;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    uint32 defect = m_AtChannelMethod->DefectHistoryGet(self);
    defect |= DefectHistoryRead2Clear(self, cAtFalse);
    return defect;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    uint32 defect = m_AtChannelMethod->DefectHistoryClear(self);
    defect |= DefectHistoryRead2Clear(self, cAtTrue);
    return defect;
    }

static uint32 OneGeLinkDownInterruptProcess(AtEthPort self, uint32 ethIntr)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint8 sixteenPortGroupId = Tha60290021FaceplateSerdesEthPortSixteenPortGroupId(self);
    uint32 baseAddress = Tha602900xxModuleEthMultirateBaseAddress(ethModule, sixteenPortGroupId);
    uint32 portMask = MultirateCommonAlarmMask(self);
    uint32 events = 0, defects = 0;
    uint32 regAddr, regVal;

    if (ethIntr & cAf6_GE_Interrupt_OR_link_stat_int_or_Mask)
        {
        regAddr = baseAddress + cAf6Reg_Ge_Link_State_AND_MASK;
        regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
        if (regVal & portMask)
            {
            events  |= cAtEthPortAlarmLinkDown;

            regAddr = baseAddress + cAf6Reg_lnk_sync_pen0_Base;
            regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
            if ((regVal & portMask) == 0)
                defects |= cAtEthPortAlarmLinkDown;

            regAddr = baseAddress + cAf6Reg_Ge_Link_State_Sticky_Base;
            mChannelHwWrite(self, regAddr, portMask, cAtModuleEth);
            }
        }

    if (events)
        AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);

    return defects;
    }

static uint32 OneGeInterruptProcess(AtEthPort self, uint32 ethIntr)
    {
    uint32 defects = m_Tha60290021FaceplateSerdesEthPortMethod->OneGeInterruptProcess(self, ethIntr);
    defects |= OneGeLinkDownInterruptProcess(self, ethIntr);
    return defects;
    }

static uint32 OneGeLinkDownSupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return cAtEthPortAlarmLinkDown;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    uint32 supportedMasks = m_AtChannelMethod->SupportedInterruptMasks(self);
    if (IsLinkStatusApplicable((AtEthPort)self))
        supportedMasks |= OneGeLinkDownSupportedInterruptMasks(self);
    return supportedMasks;
    }

static uint32 GeLinkStatusInterruptMaskAddress(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint8 sixteenPortGroupId = Tha60290021FaceplateSerdesEthPortSixteenPortGroupId(self);
    return cAf6Reg_Ge_Link_State_Interrupt_enb_Base + Tha602900xxModuleEthMultirateBaseAddress(ethModule, sixteenPortGroupId);
    }

static eAtRet OneGeInterruptLinkDownInterruptEnable(AtEthPort self, eBool enabled)
    {
    uint32 regAddr = GeLinkStatusInterruptMaskAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 fieldMask = MultirateCommonAlarmMask(self);
    uint32 fieldShift = MultirateCommonAlarmShift(self);
    mRegFieldSet(regVal, field, enabled ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool OneGeInterruptLinkDownIsEnabled(AtEthPort self)
    {
    uint32 regAddr = GeLinkStatusInterruptMaskAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 fieldMask = MultirateCommonAlarmMask(self);
    return (regVal & fieldMask) ? cAtTrue : cAtFalse;
    }

static eAtRet OneGeLinkDownInterruptMaskSet(AtEthPort self, uint32 defectMask, uint32 enableMask)
    {
    eAtRet ret = cAtOk;
    eBool enabled;

    if (defectMask & cAtEthPortAlarmLinkDown)
        {
        enabled = (enableMask & cAtEthPortAlarmLinkDown) ? cAtTrue : cAtFalse;
        ret |= OneGeInterruptLinkDownInterruptEnable(self, enabled);
        }

    return ret;
    }

static uint32 OneGeLinkDownInterruptMaskGet(AtEthPort self)
    {
    uint32 masks = 0;

    if (OneGeInterruptLinkDownIsEnabled(self))
        masks |= cAtEthPortAlarmLinkDown;

    return masks;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    AtEthPort port = (AtEthPort)self;
    eAtRet ret = cAtOk;

    ret = m_AtChannelMethod->InterruptMaskSet(self, defectMask, enableMask);
    if (IsLinkStatusApplicable(port))
        ret |= OneGeLinkDownInterruptMaskSet(port, defectMask, enableMask);

    return ret;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 mask = m_AtChannelMethod->InterruptMaskGet(self);

    if (IsLinkStatusApplicable(port))
        mask |= OneGeLinkDownInterruptMaskGet(port);

    return mask;
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethod = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethod, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(tAtEthPortMethods));

        mMethodOverride(m_AtEthPortOverride, TxIpgSet);
        mMethodOverride(m_AtEthPortOverride, TxIpgGet);
        mMethodOverride(m_AtEthPortOverride, TxIpgIsInRange);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideTha60290021FaceplateSerdesEthPort(AtEthPort self)
    {
    Tha60290021FaceplateSerdesEthPort port = (Tha60290021FaceplateSerdesEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021FaceplateSerdesEthPortMethod = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021FaceplateSerdesEthPortOverride,
                                  m_Tha60290021FaceplateSerdesEthPortMethod, sizeof(m_Tha60290021FaceplateSerdesEthPortOverride));

        mMethodOverride(m_Tha60290021FaceplateSerdesEthPortOverride, OneGeInterruptProcess);
        }

    mMethodsSet(port, &m_Tha60290021FaceplateSerdesEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideTha60290021FaceplateSerdesEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022FaceplateSerdesEthPortV2);
    }

AtEthPort Tha60290022FaceplateSerdesEthPortV2ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022FaceplateSerdesEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290022FaceplateSerdesEthPortV2New(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022FaceplateSerdesEthPortV2ObjectInit(newPort, portId, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290022SgmiiSohEthPortV3.c
 *
 * Created Date: Jul 13, 2018
 *
 * Description : Implement 60290022 DCCKbyte SGMII ETH port for 16byte Tx IPG
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022SgmiiSohEthPortInternal.h"
#include "Tha60290022ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_dcckgmiicfg_DcckGmiiLoopOut_V3_Mask                                                            cBit10
#define cAf6_dcckgmiicfg_DcckGmiiLoopOut_V3_Shift                                                               10
#define cAf6_dcckgmiicfg_DcckGmiiLoopIn_V3_Mask                                                             cBit9
#define cAf6_dcckgmiicfg_DcckGmiiLoopIn_V3_Shift                                                                9
#define cAf6_dcckgmiicfg_DcckGmiiPreNum_V3_Mask                                                           cBit8_5
#define cAf6_dcckgmiicfg_DcckGmiiPreNum_V3_Shift                                                                5
#define cAf6_dcckgmiicfg_DcckGmiiIpgNum_V3_Mask                                                           cBit4_0
#define cAf6_dcckgmiicfg_DcckGmiiIpgNum_V3_Shift                                                                0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEthPortMethods            m_AtEthPortOverride;
static tTha60290021SgmiiSerdesEthPortMethods m_Tha60290021SgmiiSerdesEthPortOverride;

/* Save super implementation */
static const tAtEthPortMethods      *m_AtEthPortMethods = NULL;
static const tTha60290021SgmiiSerdesEthPortMethods *m_Tha60290021SgmiiSerdesEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IpgIsInRange(uint8 ipg)
    {
    return mInRange(ipg, 4, 31);
    }

static eBool TxIpgIsInRange(AtEthPort self, uint8 ipg)
    {
    AtUnused(self);
    return IpgIsInRange(ipg);
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiLoopIn_Mask_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiLoopIn_V3_Mask;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiLoopIn_Shift_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiLoopIn_V3_Shift;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiLoopOut_Mask_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiLoopOut_V3_Mask;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiLoopOut_Shift_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiLoopOut_V3_Shift;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiPreNum_Mask_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiPreNum_V3_Mask;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiPreNum_Shift_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiPreNum_V3_Shift;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiIpgNum_Mask_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiIpgNum_V3_Mask;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiIpgNum_Shift_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiIpgNum_V3_Shift;
    }

static void OverrideTha60290021SgmiiSerdesEthPort(AtEthPort self)
    {
    Tha60290021SgmiiSerdesEthPort port = (Tha60290021SgmiiSerdesEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021SgmiiSerdesEthPortMethods = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021SgmiiSerdesEthPortOverride, m_Tha60290021SgmiiSerdesEthPortMethods, sizeof(m_Tha60290021SgmiiSerdesEthPortOverride));

        mMethodOverride(m_Tha60290021SgmiiSerdesEthPortOverride, Af6_dcckgmiicfg_DcckGmiiLoopIn_Mask_Get);
        mMethodOverride(m_Tha60290021SgmiiSerdesEthPortOverride, Af6_dcckgmiicfg_DcckGmiiLoopIn_Shift_Get);
        mMethodOverride(m_Tha60290021SgmiiSerdesEthPortOverride, Af6_dcckgmiicfg_DcckGmiiLoopOut_Mask_Get);
        mMethodOverride(m_Tha60290021SgmiiSerdesEthPortOverride, Af6_dcckgmiicfg_DcckGmiiLoopOut_Shift_Get);
        mMethodOverride(m_Tha60290021SgmiiSerdesEthPortOverride, Af6_dcckgmiicfg_DcckGmiiPreNum_Mask_Get);
        mMethodOverride(m_Tha60290021SgmiiSerdesEthPortOverride, Af6_dcckgmiicfg_DcckGmiiPreNum_Shift_Get);
        mMethodOverride(m_Tha60290021SgmiiSerdesEthPortOverride, Af6_dcckgmiicfg_DcckGmiiIpgNum_Mask_Get);
        mMethodOverride(m_Tha60290021SgmiiSerdesEthPortOverride, Af6_dcckgmiicfg_DcckGmiiIpgNum_Shift_Get);
        }

    mMethodsSet(port, &m_Tha60290021SgmiiSerdesEthPortOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(tAtEthPortMethods));

        mMethodOverride(m_AtEthPortOverride, TxIpgIsInRange);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtEthPort(self);
    OverrideTha60290021SgmiiSerdesEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SgmiiSohEthPortV3);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022SgmiiSohEthPortV2ObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290022SgmiiSohEthPortV3New(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

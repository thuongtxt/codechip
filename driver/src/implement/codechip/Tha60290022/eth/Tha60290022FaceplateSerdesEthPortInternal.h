/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290022FaceplateSerdesEthPortInternal.h
 * 
 * Created Date: Mar 6, 2018
 *
 * Description : 60290022 faceplate eth port internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022FACEPLATESERDESETHPORTINTERNAL_H_
#define _THA60290022FACEPLATESERDESETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/eth/Tha60290021FaceplateSerdesEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022FaceplateSerdesEthPort
    {
    tTha60290021FaceplateSerdesEthPort super;
    }tTha60290022FaceplateSerdesEthPort;

typedef struct tTha60290022FaceplateSerdesEthPortV2
    {
    tTha60290022FaceplateSerdesEthPort super;
    }tTha60290022FaceplateSerdesEthPortV2;

typedef struct tTha60290022FaceplateSerdesEthPortV3
    {
    tTha60290022FaceplateSerdesEthPortV2 super;
    }tTha60290022FaceplateSerdesEthPortV3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290022FaceplateSerdesEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);
AtEthPort Tha60290022FaceplateSerdesEthPortV2ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);
AtEthPort Tha60290022FaceplateSerdesEthPortV3ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022FACEPLATESERDESETHPORTINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive
 * Technologies. The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      :
 *
 * File        :
 *
 * Created Date:
 *
 * Description : This file contain all constance definitions of  block.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_SGMII_Multirate_H_
#define _AF6_REG_AF6CNC0022_RD_SGMII_Multirate_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Pedit Block Version
Reg Addr   : 0x0800
Reg Formula:
    Where  :
Reg Desc   :
Pedit Block Version

------------------------------------------------------------------------------*/
#define cAf6Reg_version_pen_Base                                                                        0x0800

/*--------------------------------------
BitField Name: day
BitField Type: R_O
BitField Desc: day
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_version_pen_day_Mask                                                                    cBit31_24
#define cAf6_version_pen_day_Shift                                                                          24

/*--------------------------------------
BitField Name: month
BitField Type: R_O
BitField Desc: month
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_version_pen_month_Mask                                                                  cBit23_16
#define cAf6_version_pen_month_Shift                                                                        16

/*--------------------------------------
BitField Name: year
BitField Type: R_O
BitField Desc: year
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_version_pen_year_Mask                                                                    cBit15_8
#define cAf6_version_pen_year_Shift                                                                          8

/*--------------------------------------
BitField Name: number
BitField Type: R_O
BitField Desc: number
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_version_pen_number_Mask                                                                   cBit7_0
#define cAf6_version_pen_number_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg-group0 mode
Reg Addr   : 0x0801
Reg Formula:
    Where  :
Reg Desc   :
Select Auto-neg mode for 1000Basex or SGMII

------------------------------------------------------------------------------*/
#define cAf6Reg_an_mod_pen0_Base                                                                        0x0801

/*--------------------------------------
BitField Name: tx_enb0
BitField Type: R/W
BitField Desc: Enable TX side bit per port, bit[16] port 0, bit[31] port 15
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_an_mod_pen0_tx_enb0_Mask                                                                cBit31_16
#define cAf6_an_mod_pen0_tx_enb0_Shift                                                                      16

/*--------------------------------------
BitField Name: an_mod0
BitField Type: R/W
BitField Desc: Auto-neg mode, bit per port, bit[0] port 0, bit[15] port 15
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_an_mod_pen0_an_mod0_Mask                                                                 cBit15_0
#define cAf6_an_mod_pen0_an_mod0_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg-group0 reset
Reg Addr   : 0x0802
Reg Formula:
    Where  :
Reg Desc   :
Restart Auto-neg process of group0 from port 0 to port 15

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rst_pen0_Base                                                                        0x0802

/*--------------------------------------
BitField Name: an_rst0
BitField Type: R/W
BitField Desc: Auto-neg reset, sw write 1 then write 0 to restart auto-neg bit
per port, bit[0] port 0, bit[15] port 15
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_an_rst_pen0_an_rst0_Mask                                                                 cBit15_0
#define cAf6_an_rst_pen0_an_rst0_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg-group0 enable
Reg Addr   : 0x0803
Reg Formula:
    Where  :
Reg Desc   :
enable/disbale Auto-neg of group 0 from port 0 to port 15

------------------------------------------------------------------------------*/
#define cAf6Reg_an_enb_pen0_Base                                                                        0x0803

/*--------------------------------------
BitField Name: an_enb0
BitField Type: R/W
BitField Desc: Auto-neg enb , bit per port, bit[0] port 0, bit[15] port 15
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_an_enb_pen0_an_enb0_Mask                                                                 cBit15_0
#define cAf6_an_enb_pen0_an_enb0_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Config_speed-group0
Reg Addr   : 0x0804
Reg Formula:
    Where  :
Reg Desc   :
configure speed when disable Auto-neg of group 0 from port 0 to 15

------------------------------------------------------------------------------*/
#define cAf6Reg_an_spd_pen0_Base                                                                        0x0804

/*--------------------------------------
BitField Name: cfg_spd15
BitField Type: R/W
BitField Desc: Speed port 15
BitField Bits: [31:30]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd15_Mask                                                              cBit31_30
#define cAf6_an_spd_pen0_cfg_spd15_Shift                                                                    30

/*--------------------------------------
BitField Name: cfg_spd14
BitField Type: R/W
BitField Desc: Speed port 14
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd14_Mask                                                              cBit29_28
#define cAf6_an_spd_pen0_cfg_spd14_Shift                                                                    28

/*--------------------------------------
BitField Name: cfg_spd13
BitField Type: R/W
BitField Desc: Speed port 13
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd13_Mask                                                              cBit27_26
#define cAf6_an_spd_pen0_cfg_spd13_Shift                                                                    26

/*--------------------------------------
BitField Name: cfg_spd12
BitField Type: R/W
BitField Desc: Speed port 12
BitField Bits: [25:24]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd12_Mask                                                              cBit25_24
#define cAf6_an_spd_pen0_cfg_spd12_Shift                                                                    24

/*--------------------------------------
BitField Name: cfg_spd11
BitField Type: R/W
BitField Desc: Speed port 11
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd11_Mask                                                              cBit23_22
#define cAf6_an_spd_pen0_cfg_spd11_Shift                                                                    22

/*--------------------------------------
BitField Name: cfg_spd10
BitField Type: R/W
BitField Desc: Speed port 10
BitField Bits: [21:20]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd10_Mask                                                              cBit21_20
#define cAf6_an_spd_pen0_cfg_spd10_Shift                                                                    20

/*--------------------------------------
BitField Name: cfg_spd09
BitField Type: R/W
BitField Desc: Speed port 9
BitField Bits: [19:18]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd09_Mask                                                              cBit19_18
#define cAf6_an_spd_pen0_cfg_spd09_Shift                                                                    18

/*--------------------------------------
BitField Name: cfg_spd08
BitField Type: R/W
BitField Desc: Speed port 8
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd08_Mask                                                              cBit17_16
#define cAf6_an_spd_pen0_cfg_spd08_Shift                                                                    16

/*--------------------------------------
BitField Name: cfg_spd07
BitField Type: R/W
BitField Desc: Speed port 7
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd07_Mask                                                              cBit15_14
#define cAf6_an_spd_pen0_cfg_spd07_Shift                                                                    14

/*--------------------------------------
BitField Name: cfg_spd06
BitField Type: R/W
BitField Desc: Speed port 6
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd06_Mask                                                              cBit13_12
#define cAf6_an_spd_pen0_cfg_spd06_Shift                                                                    12

/*--------------------------------------
BitField Name: cfg_spd05
BitField Type: R/W
BitField Desc: Speed port 5
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd05_Mask                                                              cBit11_10
#define cAf6_an_spd_pen0_cfg_spd05_Shift                                                                    10

/*--------------------------------------
BitField Name: cfg_spd04
BitField Type: R/W
BitField Desc: Speed port 4
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd04_Mask                                                                cBit9_8
#define cAf6_an_spd_pen0_cfg_spd04_Shift                                                                     8

/*--------------------------------------
BitField Name: cfg_spd03
BitField Type: R/W
BitField Desc: Speed port 3
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd03_Mask                                                                cBit7_6
#define cAf6_an_spd_pen0_cfg_spd03_Shift                                                                     6

/*--------------------------------------
BitField Name: cfg_spd02
BitField Type: R/W
BitField Desc: Speed port 2
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd02_Mask                                                                cBit5_4
#define cAf6_an_spd_pen0_cfg_spd02_Shift                                                                     4

/*--------------------------------------
BitField Name: cfg_spd01
BitField Type: R/W
BitField Desc: Speed port 1
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd01_Mask                                                                cBit3_2
#define cAf6_an_spd_pen0_cfg_spd01_Shift                                                                     2

/*--------------------------------------
BitField Name: cfg_spd00
BitField Type: R/W
BitField Desc: Speed port 0
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_an_spd_pen0_cfg_spd00_Mask                                                                cBit1_0
#define cAf6_an_spd_pen0_cfg_spd00_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg-group00 status
Reg Addr   : 0x0805
Reg Formula:
    Where  :
Reg Desc   :
status of Auto-neg of group0 from port 0 to port 7

------------------------------------------------------------------------------*/
#define cAf6Reg_an_sta_pen00_Base                                                                       0x0805

/*--------------------------------------
BitField Name: an_sta07
BitField Type: R_O
BitField Desc: Speed port 7
BitField Bits: [31:28]
--------------------------------------*/
#define cAf6_an_sta_pen00_an_sta07_Mask                                                              cBit31_28
#define cAf6_an_sta_pen00_an_sta07_Shift                                                                    28

/*--------------------------------------
BitField Name: an_sta06
BitField Type: R_O
BitField Desc: Speed port 6
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_an_sta_pen00_an_sta06_Mask                                                              cBit27_24
#define cAf6_an_sta_pen00_an_sta06_Shift                                                                    24

/*--------------------------------------
BitField Name: an_sta05
BitField Type: R_O
BitField Desc: Speed port 5
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_an_sta_pen00_an_sta05_Mask                                                              cBit23_20
#define cAf6_an_sta_pen00_an_sta05_Shift                                                                    20

/*--------------------------------------
BitField Name: an_sta04
BitField Type: R_O
BitField Desc: Speed port 4
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_an_sta_pen00_an_sta04_Mask                                                              cBit19_16
#define cAf6_an_sta_pen00_an_sta04_Shift                                                                    16

/*--------------------------------------
BitField Name: an_sta03
BitField Type: R_O
BitField Desc: Speed port 3
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_an_sta_pen00_an_sta03_Mask                                                              cBit15_12
#define cAf6_an_sta_pen00_an_sta03_Shift                                                                    12

/*--------------------------------------
BitField Name: an_sta02
BitField Type: R_O
BitField Desc: Speed port 2
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_an_sta_pen00_an_sta02_Mask                                                               cBit11_8
#define cAf6_an_sta_pen00_an_sta02_Shift                                                                     8

/*--------------------------------------
BitField Name: an_sta01
BitField Type: R_O
BitField Desc: Speed port 1
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_an_sta_pen00_an_sta01_Mask                                                                cBit7_4
#define cAf6_an_sta_pen00_an_sta01_Shift                                                                     4

/*--------------------------------------
BitField Name: an_sta00
BitField Type: R_O
BitField Desc: Speed port 0
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_an_sta_pen00_an_sta00_Mask                                                                cBit3_0
#define cAf6_an_sta_pen00_an_sta00_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg-group01 status
Reg Addr   : 0x0806
Reg Formula:
    Where  :
Reg Desc   :
status of Auto-neg of group0 from port 8 to port 15

------------------------------------------------------------------------------*/
#define cAf6Reg_an_sta_pen01_Base                                                                       0x0806

/*--------------------------------------
BitField Name: an_sta15
BitField Type: R_O
BitField Desc: Speed port 15
BitField Bits: [31:28]
--------------------------------------*/
#define cAf6_an_sta_pen01_an_sta15_Mask                                                              cBit31_28
#define cAf6_an_sta_pen01_an_sta15_Shift                                                                    28

/*--------------------------------------
BitField Name: an_sta14
BitField Type: R_O
BitField Desc: Speed port 14
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_an_sta_pen01_an_sta14_Mask                                                              cBit27_24
#define cAf6_an_sta_pen01_an_sta14_Shift                                                                    24

/*--------------------------------------
BitField Name: an_sta13
BitField Type: R_O
BitField Desc: Speed port 13
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_an_sta_pen01_an_sta13_Mask                                                              cBit23_20
#define cAf6_an_sta_pen01_an_sta13_Shift                                                                    20

/*--------------------------------------
BitField Name: an_sta12
BitField Type: R_O
BitField Desc: Speed port 12
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_an_sta_pen01_an_sta12_Mask                                                              cBit19_16
#define cAf6_an_sta_pen01_an_sta12_Shift                                                                    16

/*--------------------------------------
BitField Name: an_sta11
BitField Type: R_O
BitField Desc: Speed port 11
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_an_sta_pen01_an_sta11_Mask                                                              cBit15_12
#define cAf6_an_sta_pen01_an_sta11_Shift                                                                    12

/*--------------------------------------
BitField Name: an_sta10
BitField Type: R_O
BitField Desc: Speed port 10
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_an_sta_pen01_an_sta10_Mask                                                               cBit11_8
#define cAf6_an_sta_pen01_an_sta10_Shift                                                                     8

/*--------------------------------------
BitField Name: an_sta09
BitField Type: R_O
BitField Desc: Speed port 09
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_an_sta_pen01_an_sta09_Mask                                                                cBit7_4
#define cAf6_an_sta_pen01_an_sta09_Shift                                                                     4

/*--------------------------------------
BitField Name: an_sta08
BitField Type: R_O
BitField Desc: Speed port 08
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_an_sta_pen01_an_sta08_Mask                                                                cBit3_0
#define cAf6_an_sta_pen01_an_sta08_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Link-status-group0
Reg Addr   : 0x0808
Reg Formula:
    Where  :
Reg Desc   :
Link status of group 0 from port 0  to port 15

------------------------------------------------------------------------------*/
#define cAf6Reg_lnk_sync_pen0_Base                                                                      0x0808

/*--------------------------------------
BitField Name: lnk_sta0
BitField Type: R_O
BitField Desc: Link status, bit per port, bit[0] port  0, bit[7] port 7 ... ,
bit[15] port 15
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_lnk_sync_pen0_lnk_sta0_Mask                                                              cBit15_0
#define cAf6_lnk_sync_pen0_lnk_sta0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX00-RX01 SGMII ability
Reg Addr   : 0x0A00
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port00-01 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen00_Base                                                                      0x0A00

/*--------------------------------------
BitField Name: link01
BitField Type: R_O
BitField Desc: Link status of port1
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen00_link01_Mask                                                                  cBit31
#define cAf6_an_rxab_pen00_link01_Shift                                                                     31

/*--------------------------------------
BitField Name: duplex01
BitField Type: R_O
BitField Desc: duplex mode of port1
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_an_rxab_pen00_duplex01_Mask                                                                cBit28
#define cAf6_an_rxab_pen00_duplex01_Shift                                                                   28

/*--------------------------------------
BitField Name: speed01
BitField Type: R_O
BitField Desc: speed of SGMII of port1
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_an_rxab_pen00_speed01_Mask                                                              cBit27_26
#define cAf6_an_rxab_pen00_speed01_Shift                                                                    26

/*--------------------------------------
BitField Name: link00
BitField Type: R_O
BitField Desc: Link status of port0
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen00_link00_Mask                                                                  cBit15
#define cAf6_an_rxab_pen00_link00_Shift                                                                     15

/*--------------------------------------
BitField Name: duplex00
BitField Type: R_O
BitField Desc: duplex mode of port0
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_an_rxab_pen00_duplex00_Mask                                                                cBit12
#define cAf6_an_rxab_pen00_duplex00_Shift                                                                   12

/*--------------------------------------
BitField Name: speed00
BitField Type: R_O
BitField Desc: speed of SGMII of port0
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_an_rxab_pen00_speed00_Mask                                                              cBit11_10
#define cAf6_an_rxab_pen00_speed00_Shift                                                                    10


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX02-RX03 SGMII ability
Reg Addr   : 0x0A01
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port02-03 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen01_Base                                                                      0x0A01

/*--------------------------------------
BitField Name: link03
BitField Type: R_O
BitField Desc: Link status of port3
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen01_link03_Mask                                                                  cBit31
#define cAf6_an_rxab_pen01_link03_Shift                                                                     31

/*--------------------------------------
BitField Name: duplex03
BitField Type: R_O
BitField Desc: duplex mode of port3
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_an_rxab_pen01_duplex03_Mask                                                                cBit28
#define cAf6_an_rxab_pen01_duplex03_Shift                                                                   28

/*--------------------------------------
BitField Name: speed03
BitField Type: R_O
BitField Desc: speed of SGMII of port3
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_an_rxab_pen01_speed03_Mask                                                              cBit27_26
#define cAf6_an_rxab_pen01_speed03_Shift                                                                    26

/*--------------------------------------
BitField Name: link02
BitField Type: R_O
BitField Desc: Link status of port2
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen01_link02_Mask                                                                  cBit15
#define cAf6_an_rxab_pen01_link02_Shift                                                                     15

/*--------------------------------------
BitField Name: duplex02
BitField Type: R_O
BitField Desc: duplex mode of port2
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_an_rxab_pen01_duplex02_Mask                                                                cBit12
#define cAf6_an_rxab_pen01_duplex02_Shift                                                                   12

/*--------------------------------------
BitField Name: speed02
BitField Type: R_O
BitField Desc: speed of SGMII of port2
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_an_rxab_pen01_speed02_Mask                                                              cBit11_10
#define cAf6_an_rxab_pen01_speed02_Shift                                                                    10


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX04-RX05 SGMII ability
Reg Addr   : 0x0A02
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port04-05 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen02_Base                                                                      0x0A02

/*--------------------------------------
BitField Name: link05
BitField Type: R_O
BitField Desc: Link status of port5
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen02_link05_Mask                                                                  cBit31
#define cAf6_an_rxab_pen02_link05_Shift                                                                     31

/*--------------------------------------
BitField Name: duplex05
BitField Type: R_O
BitField Desc: duplex mode of port5
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_an_rxab_pen02_duplex05_Mask                                                                cBit28
#define cAf6_an_rxab_pen02_duplex05_Shift                                                                   28

/*--------------------------------------
BitField Name: speed05
BitField Type: R_O
BitField Desc: speed of SGMII of port5
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_an_rxab_pen02_speed05_Mask                                                              cBit27_26
#define cAf6_an_rxab_pen02_speed05_Shift                                                                    26

/*--------------------------------------
BitField Name: link04
BitField Type: R_O
BitField Desc: Link status of port4
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen02_link04_Mask                                                                  cBit15
#define cAf6_an_rxab_pen02_link04_Shift                                                                     15

/*--------------------------------------
BitField Name: duplex04
BitField Type: R_O
BitField Desc: duplex mode of port4
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_an_rxab_pen02_duplex04_Mask                                                                cBit12
#define cAf6_an_rxab_pen02_duplex04_Shift                                                                   12

/*--------------------------------------
BitField Name: speed04
BitField Type: R_O
BitField Desc: speed of SGMII of port4
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_an_rxab_pen02_speed04_Mask                                                              cBit11_10
#define cAf6_an_rxab_pen02_speed04_Shift                                                                    10


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX06-RX07 SGMII ability
Reg Addr   : 0x0A03
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port06-07 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen03_Base                                                                      0x0A03

/*--------------------------------------
BitField Name: link07
BitField Type: R_O
BitField Desc: Link status of port7
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen03_link07_Mask                                                                  cBit31
#define cAf6_an_rxab_pen03_link07_Shift                                                                     31

/*--------------------------------------
BitField Name: duplex07
BitField Type: R_O
BitField Desc: duplex mode of port7
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_an_rxab_pen03_duplex07_Mask                                                                cBit28
#define cAf6_an_rxab_pen03_duplex07_Shift                                                                   28

/*--------------------------------------
BitField Name: speed07
BitField Type: R_O
BitField Desc: speed of SGMII of port7
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_an_rxab_pen03_speed07_Mask                                                              cBit27_26
#define cAf6_an_rxab_pen03_speed07_Shift                                                                    26

/*--------------------------------------
BitField Name: link06
BitField Type: R_O
BitField Desc: Link status of port6
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen03_link06_Mask                                                                  cBit15
#define cAf6_an_rxab_pen03_link06_Shift                                                                     15

/*--------------------------------------
BitField Name: duplex06
BitField Type: R_O
BitField Desc: duplex mode of port6
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_an_rxab_pen03_duplex06_Mask                                                                cBit12
#define cAf6_an_rxab_pen03_duplex06_Shift                                                                   12

/*--------------------------------------
BitField Name: speed06
BitField Type: R_O
BitField Desc: speed of SGMII of port6
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_an_rxab_pen03_speed06_Mask                                                              cBit11_10
#define cAf6_an_rxab_pen03_speed06_Shift                                                                    10


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX08-RX09 SGMII ability
Reg Addr   : 0x0A04
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port08-09 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen04_Base                                                                      0x0A04

/*--------------------------------------
BitField Name: link09
BitField Type: R_O
BitField Desc: Link status of port9
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen04_link09_Mask                                                                  cBit31
#define cAf6_an_rxab_pen04_link09_Shift                                                                     31

/*--------------------------------------
BitField Name: duplex09
BitField Type: R_O
BitField Desc: duplex mode of port9
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_an_rxab_pen04_duplex09_Mask                                                                cBit28
#define cAf6_an_rxab_pen04_duplex09_Shift                                                                   28

/*--------------------------------------
BitField Name: speed09
BitField Type: R_O
BitField Desc: speed of SGMII of port9
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_an_rxab_pen04_speed09_Mask                                                              cBit27_26
#define cAf6_an_rxab_pen04_speed09_Shift                                                                    26

/*--------------------------------------
BitField Name: link08
BitField Type: R_O
BitField Desc: Link status of port8
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen04_link08_Mask                                                                  cBit15
#define cAf6_an_rxab_pen04_link08_Shift                                                                     15

/*--------------------------------------
BitField Name: duplex08
BitField Type: R_O
BitField Desc: duplex mode of port8
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_an_rxab_pen04_duplex08_Mask                                                                cBit12
#define cAf6_an_rxab_pen04_duplex08_Shift                                                                   12

/*--------------------------------------
BitField Name: speed08
BitField Type: R_O
BitField Desc: speed of SGMII of port8
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_an_rxab_pen04_speed08_Mask                                                              cBit11_10
#define cAf6_an_rxab_pen04_speed08_Shift                                                                    10


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX10-RX11 SGMII ability
Reg Addr   : 0x0A05
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port10-11 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen05_Base                                                                      0x0A05

/*--------------------------------------
BitField Name: link11
BitField Type: R_O
BitField Desc: Link status of port11
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen05_link11_Mask                                                                  cBit31
#define cAf6_an_rxab_pen05_link11_Shift                                                                     31

/*--------------------------------------
BitField Name: duplex11
BitField Type: R_O
BitField Desc: duplex mode of port11
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_an_rxab_pen05_duplex11_Mask                                                                cBit28
#define cAf6_an_rxab_pen05_duplex11_Shift                                                                   28

/*--------------------------------------
BitField Name: speed11
BitField Type: R_O
BitField Desc: speed of SGMII of port11
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_an_rxab_pen05_speed11_Mask                                                              cBit27_26
#define cAf6_an_rxab_pen05_speed11_Shift                                                                    26

/*--------------------------------------
BitField Name: link10
BitField Type: R_O
BitField Desc: Link status of port10
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen05_link10_Mask                                                                  cBit15
#define cAf6_an_rxab_pen05_link10_Shift                                                                     15

/*--------------------------------------
BitField Name: duplex10
BitField Type: R_O
BitField Desc: duplex mode of port10
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_an_rxab_pen05_duplex10_Mask                                                                cBit12
#define cAf6_an_rxab_pen05_duplex10_Shift                                                                   12

/*--------------------------------------
BitField Name: speed10
BitField Type: R_O
BitField Desc: speed of SGMII of port10
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_an_rxab_pen05_speed10_Mask                                                              cBit11_10
#define cAf6_an_rxab_pen05_speed10_Shift                                                                    10


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX12-RX13 SGMII ability
Reg Addr   : 0x0A06
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port12-13 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen06_Base                                                                      0x0A06

/*--------------------------------------
BitField Name: link13
BitField Type: R_O
BitField Desc: Link status of port13
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen06_link13_Mask                                                                  cBit31
#define cAf6_an_rxab_pen06_link13_Shift                                                                     31

/*--------------------------------------
BitField Name: duplex13
BitField Type: R_O
BitField Desc: duplex mode of port13
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_an_rxab_pen06_duplex13_Mask                                                                cBit28
#define cAf6_an_rxab_pen06_duplex13_Shift                                                                   28

/*--------------------------------------
BitField Name: speed13
BitField Type: R_O
BitField Desc: speed of SGMII of port13
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_an_rxab_pen06_speed13_Mask                                                              cBit27_26
#define cAf6_an_rxab_pen06_speed13_Shift                                                                    26

/*--------------------------------------
BitField Name: link12
BitField Type: R_O
BitField Desc: Link status of port12
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen06_link12_Mask                                                                  cBit15
#define cAf6_an_rxab_pen06_link12_Shift                                                                     15

/*--------------------------------------
BitField Name: duplex12
BitField Type: R_O
BitField Desc: duplex mode of port12
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_an_rxab_pen06_duplex12_Mask                                                                cBit12
#define cAf6_an_rxab_pen06_duplex12_Shift                                                                   12

/*--------------------------------------
BitField Name: speed12
BitField Type: R_O
BitField Desc: speed of SGMII of port12
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_an_rxab_pen06_speed12_Mask                                                              cBit11_10
#define cAf6_an_rxab_pen06_speed12_Shift                                                                    10


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX14-RX15 SGMII ability
Reg Addr   : 0x0A07
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port14-15 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen07_Base                                                                      0x0A07

/*--------------------------------------
BitField Name: link15
BitField Type: R_O
BitField Desc: Link status of port15
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen07_link15_Mask                                                                  cBit31
#define cAf6_an_rxab_pen07_link15_Shift                                                                     31

/*--------------------------------------
BitField Name: duplex15
BitField Type: R_O
BitField Desc: duplex mode of port15
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_an_rxab_pen07_duplex15_Mask                                                                cBit28
#define cAf6_an_rxab_pen07_duplex15_Shift                                                                   28

/*--------------------------------------
BitField Name: speed15
BitField Type: R_O
BitField Desc: speed of SGMII of port15
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_an_rxab_pen07_speed15_Mask                                                              cBit27_26
#define cAf6_an_rxab_pen07_speed15_Shift                                                                    26

/*--------------------------------------
BitField Name: link14
BitField Type: R_O
BitField Desc: Link status of port14
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen07_link14_Mask                                                                  cBit15
#define cAf6_an_rxab_pen07_link14_Shift                                                                     15

/*--------------------------------------
BitField Name: duplex14
BitField Type: R_O
BitField Desc: duplex mode of port14
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_an_rxab_pen07_duplex14_Mask                                                                cBit12
#define cAf6_an_rxab_pen07_duplex14_Shift                                                                   12

/*--------------------------------------
BitField Name: speed14
BitField Type: R_O
BitField Desc: speed of SGMII of port14
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_an_rxab_pen07_speed14_Mask                                                              cBit11_10
#define cAf6_an_rxab_pen07_speed14_Shift                                                                    10


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX00-RX01 1000Basex ability
Reg Addr   : 0x0A00
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port00-01 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen00_Base                                                                      0x0A00

/*--------------------------------------
BitField Name: NP01
BitField Type: R_O
BitField Desc: Next page of port1
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen00_NP01_Mask                                                                    cBit31
#define cAf6_an_rxab_pen00_NP01_Shift                                                                       31

/*--------------------------------------
BitField Name: Ack01
BitField Type: R_O
BitField Desc: Acknowledge of port1
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_an_rxab_pen00_Ack01_Mask                                                                   cBit30
#define cAf6_an_rxab_pen00_Ack01_Shift                                                                      30

/*--------------------------------------
BitField Name: ReFault01
BitField Type: R_O
BitField Desc: Remote Fault of port1
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_an_rxab_pen00_ReFault01_Mask                                                            cBit29_28
#define cAf6_an_rxab_pen00_ReFault01_Shift                                                                  28

/*--------------------------------------
BitField Name: Hduplex01
BitField Type: R_O
BitField Desc: Half duplex of port1
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_an_rxab_pen00_Hduplex01_Mask                                                               cBit22
#define cAf6_an_rxab_pen00_Hduplex01_Shift                                                                  22

/*--------------------------------------
BitField Name: Fduplex01
BitField Type: R_O
BitField Desc: Full duplex of port1
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_an_rxab_pen00_Fduplex01_Mask                                                               cBit21
#define cAf6_an_rxab_pen00_Fduplex01_Shift                                                                  21

/*--------------------------------------
BitField Name: NP00
BitField Type: R_O
BitField Desc: Next page of port0
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen00_NP00_Mask                                                                    cBit15
#define cAf6_an_rxab_pen00_NP00_Shift                                                                       15

/*--------------------------------------
BitField Name: Ack00
BitField Type: R_O
BitField Desc: Acknowledge of port0
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_an_rxab_pen00_Ack00_Mask                                                                   cBit14
#define cAf6_an_rxab_pen00_Ack00_Shift                                                                      14

/*--------------------------------------
BitField Name: ReFault00
BitField Type: R_O
BitField Desc: Remote Fault of port0
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_an_rxab_pen00_ReFault00_Mask                                                            cBit13_12
#define cAf6_an_rxab_pen00_ReFault00_Shift                                                                  12

/*--------------------------------------
BitField Name: Hduplex00
BitField Type: R_O
BitField Desc: Half duplex of port0
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_an_rxab_pen00_Hduplex00_Mask                                                                cBit6
#define cAf6_an_rxab_pen00_Hduplex00_Shift                                                                   6

/*--------------------------------------
BitField Name: Fduplex00
BitField Type: R_O
BitField Desc: Full duplex of port0
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_an_rxab_pen00_Fduplex00_Mask                                                                cBit5
#define cAf6_an_rxab_pen00_Fduplex00_Shift                                                                   5


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX02-RX03 1000Basex ability
Reg Addr   : 0x0A01
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port02-03 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen01_Base                                                                      0x0A01

/*--------------------------------------
BitField Name: NP03
BitField Type: R_O
BitField Desc: Next page  of port3
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen01_NP03_Mask                                                                    cBit31
#define cAf6_an_rxab_pen01_NP03_Shift                                                                       31

/*--------------------------------------
BitField Name: Ack03
BitField Type: R_O
BitField Desc: Acknowledge of port3
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_an_rxab_pen01_Ack03_Mask                                                                   cBit30
#define cAf6_an_rxab_pen01_Ack03_Shift                                                                      30

/*--------------------------------------
BitField Name: ReFault03
BitField Type: R_O
BitField Desc: Remote Fault of port3
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_an_rxab_pen01_ReFault03_Mask                                                            cBit29_28
#define cAf6_an_rxab_pen01_ReFault03_Shift                                                                  28

/*--------------------------------------
BitField Name: Hduplex03
BitField Type: R_O
BitField Desc: Half duplex of port3
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_an_rxab_pen01_Hduplex03_Mask                                                               cBit22
#define cAf6_an_rxab_pen01_Hduplex03_Shift                                                                  22

/*--------------------------------------
BitField Name: Fduplex03
BitField Type: R_O
BitField Desc: Full duplex of port3
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_an_rxab_pen01_Fduplex03_Mask                                                               cBit21
#define cAf6_an_rxab_pen01_Fduplex03_Shift                                                                  21

/*--------------------------------------
BitField Name: NP02
BitField Type: R_O
BitField Desc: Next page of port2
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen01_NP02_Mask                                                                    cBit15
#define cAf6_an_rxab_pen01_NP02_Shift                                                                       15

/*--------------------------------------
BitField Name: Ack02
BitField Type: R_O
BitField Desc: Acknowledge of port2
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_an_rxab_pen01_Ack02_Mask                                                                   cBit14
#define cAf6_an_rxab_pen01_Ack02_Shift                                                                      14

/*--------------------------------------
BitField Name: ReFault02
BitField Type: R_O
BitField Desc: Remote Fault of port2
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_an_rxab_pen01_ReFault02_Mask                                                            cBit13_12
#define cAf6_an_rxab_pen01_ReFault02_Shift                                                                  12

/*--------------------------------------
BitField Name: Hduplex02
BitField Type: R_O
BitField Desc: Half duplex of port2
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_an_rxab_pen01_Hduplex02_Mask                                                                cBit6
#define cAf6_an_rxab_pen01_Hduplex02_Shift                                                                   6

/*--------------------------------------
BitField Name: Fduplex02
BitField Type: R_O
BitField Desc: Full duplex of port2
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_an_rxab_pen01_Fduplex02_Mask                                                                cBit5
#define cAf6_an_rxab_pen01_Fduplex02_Shift                                                                   5


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX04-RX05 1000Basex ability
Reg Addr   : 0x0A02
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port04-05 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen02_Base                                                                      0x0A02

/*--------------------------------------
BitField Name: NP05
BitField Type: R_O
BitField Desc: Next page  of port5
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen02_NP05_Mask                                                                    cBit31
#define cAf6_an_rxab_pen02_NP05_Shift                                                                       31

/*--------------------------------------
BitField Name: Ack05
BitField Type: R_O
BitField Desc: Acknowledge of port5
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_an_rxab_pen02_Ack05_Mask                                                                   cBit30
#define cAf6_an_rxab_pen02_Ack05_Shift                                                                      30

/*--------------------------------------
BitField Name: ReFault05
BitField Type: R_O
BitField Desc: Remote Fault of port5
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_an_rxab_pen02_ReFault05_Mask                                                            cBit29_28
#define cAf6_an_rxab_pen02_ReFault05_Shift                                                                  28

/*--------------------------------------
BitField Name: Hduplex05
BitField Type: R_O
BitField Desc: Half duplex of port5
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_an_rxab_pen02_Hduplex05_Mask                                                               cBit22
#define cAf6_an_rxab_pen02_Hduplex05_Shift                                                                  22

/*--------------------------------------
BitField Name: Fduplex05
BitField Type: R_O
BitField Desc: Full duplex of port5
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_an_rxab_pen02_Fduplex05_Mask                                                               cBit21
#define cAf6_an_rxab_pen02_Fduplex05_Shift                                                                  21

/*--------------------------------------
BitField Name: NP04
BitField Type: R_O
BitField Desc: Next page of port4
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen02_NP04_Mask                                                                    cBit15
#define cAf6_an_rxab_pen02_NP04_Shift                                                                       15

/*--------------------------------------
BitField Name: Ack04
BitField Type: R_O
BitField Desc: Acknowledge of port4
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_an_rxab_pen02_Ack04_Mask                                                                   cBit14
#define cAf6_an_rxab_pen02_Ack04_Shift                                                                      14

/*--------------------------------------
BitField Name: ReFault04
BitField Type: R_O
BitField Desc: Remote Fault of port4
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_an_rxab_pen02_ReFault04_Mask                                                            cBit13_12
#define cAf6_an_rxab_pen02_ReFault04_Shift                                                                  12

/*--------------------------------------
BitField Name: Hduplex04
BitField Type: R_O
BitField Desc: Half duplex of port4
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_an_rxab_pen02_Hduplex04_Mask                                                                cBit6
#define cAf6_an_rxab_pen02_Hduplex04_Shift                                                                   6

/*--------------------------------------
BitField Name: Fduplex04
BitField Type: R_O
BitField Desc: Full duplex of port4
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_an_rxab_pen02_Fduplex04_Mask                                                                cBit5
#define cAf6_an_rxab_pen02_Fduplex04_Shift                                                                   5


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX06-RX07 1000Basex ability
Reg Addr   : 0x0A03
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port06-07 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen03_Base                                                                      0x0A03

/*--------------------------------------
BitField Name: NP07
BitField Type: R_O
BitField Desc: Next page  of port7
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen03_NP07_Mask                                                                    cBit31
#define cAf6_an_rxab_pen03_NP07_Shift                                                                       31

/*--------------------------------------
BitField Name: Ack07
BitField Type: R_O
BitField Desc: Acknowledge of port7
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_an_rxab_pen03_Ack07_Mask                                                                   cBit30
#define cAf6_an_rxab_pen03_Ack07_Shift                                                                      30

/*--------------------------------------
BitField Name: ReFault07
BitField Type: R_O
BitField Desc: Remote Fault of port7
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_an_rxab_pen03_ReFault07_Mask                                                            cBit29_28
#define cAf6_an_rxab_pen03_ReFault07_Shift                                                                  28

/*--------------------------------------
BitField Name: Hduplex07
BitField Type: R_O
BitField Desc: Half duplex of port7
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_an_rxab_pen03_Hduplex07_Mask                                                               cBit22
#define cAf6_an_rxab_pen03_Hduplex07_Shift                                                                  22

/*--------------------------------------
BitField Name: Fduplex07
BitField Type: R_O
BitField Desc: Full duplex of port7
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_an_rxab_pen03_Fduplex07_Mask                                                               cBit21
#define cAf6_an_rxab_pen03_Fduplex07_Shift                                                                  21

/*--------------------------------------
BitField Name: NP06
BitField Type: R_O
BitField Desc: Next page of port6
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen03_NP06_Mask                                                                    cBit15
#define cAf6_an_rxab_pen03_NP06_Shift                                                                       15

/*--------------------------------------
BitField Name: Ack06
BitField Type: R_O
BitField Desc: Acknowledge of port6
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_an_rxab_pen03_Ack06_Mask                                                                   cBit14
#define cAf6_an_rxab_pen03_Ack06_Shift                                                                      14

/*--------------------------------------
BitField Name: ReFault06
BitField Type: R_O
BitField Desc: Remote Fault of port6
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_an_rxab_pen03_ReFault06_Mask                                                            cBit13_12
#define cAf6_an_rxab_pen03_ReFault06_Shift                                                                  12

/*--------------------------------------
BitField Name: Hduplex06
BitField Type: R_O
BitField Desc: Half duplex of port6
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_an_rxab_pen03_Hduplex06_Mask                                                                cBit6
#define cAf6_an_rxab_pen03_Hduplex06_Shift                                                                   6

/*--------------------------------------
BitField Name: Fduplex06
BitField Type: R_O
BitField Desc: Full duplex of port6
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_an_rxab_pen03_Fduplex06_Mask                                                                cBit5
#define cAf6_an_rxab_pen03_Fduplex06_Shift                                                                   5


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX08-RX09 1000Basex ability
Reg Addr   : 0x0A04
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port08-09 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen04_Base                                                                      0x0A04

/*--------------------------------------
BitField Name: NP09
BitField Type: R_O
BitField Desc: Next page  of port9
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen04_NP09_Mask                                                                    cBit31
#define cAf6_an_rxab_pen04_NP09_Shift                                                                       31

/*--------------------------------------
BitField Name: Ack09
BitField Type: R_O
BitField Desc: Acknowledge of port9
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_an_rxab_pen04_Ack09_Mask                                                                   cBit30
#define cAf6_an_rxab_pen04_Ack09_Shift                                                                      30

/*--------------------------------------
BitField Name: ReFault09
BitField Type: R_O
BitField Desc: Remote Fault of port9
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_an_rxab_pen04_ReFault09_Mask                                                            cBit29_28
#define cAf6_an_rxab_pen04_ReFault09_Shift                                                                  28

/*--------------------------------------
BitField Name: Hduplex09
BitField Type: R_O
BitField Desc: Half duplex of port9
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_an_rxab_pen04_Hduplex09_Mask                                                               cBit22
#define cAf6_an_rxab_pen04_Hduplex09_Shift                                                                  22

/*--------------------------------------
BitField Name: Fduplex09
BitField Type: R_O
BitField Desc: Full duplex of port9
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_an_rxab_pen04_Fduplex09_Mask                                                               cBit21
#define cAf6_an_rxab_pen04_Fduplex09_Shift                                                                  21

/*--------------------------------------
BitField Name: NP08
BitField Type: R_O
BitField Desc: Next page of port8
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen04_NP08_Mask                                                                    cBit15
#define cAf6_an_rxab_pen04_NP08_Shift                                                                       15

/*--------------------------------------
BitField Name: Ack08
BitField Type: R_O
BitField Desc: Acknowledge of port8
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_an_rxab_pen04_Ack08_Mask                                                                   cBit14
#define cAf6_an_rxab_pen04_Ack08_Shift                                                                      14

/*--------------------------------------
BitField Name: ReFault08
BitField Type: R_O
BitField Desc: Remote Fault of port8
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_an_rxab_pen04_ReFault08_Mask                                                            cBit13_12
#define cAf6_an_rxab_pen04_ReFault08_Shift                                                                  12

/*--------------------------------------
BitField Name: Hduplex08
BitField Type: R_O
BitField Desc: Half duplex of port8
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_an_rxab_pen04_Hduplex08_Mask                                                                cBit6
#define cAf6_an_rxab_pen04_Hduplex08_Shift                                                                   6

/*--------------------------------------
BitField Name: Fduplex08
BitField Type: R_O
BitField Desc: Full duplex of port8
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_an_rxab_pen04_Fduplex08_Mask                                                                cBit5
#define cAf6_an_rxab_pen04_Fduplex08_Shift                                                                   5


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX10-RX11 1000Basex ability
Reg Addr   : 0x0A05
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port10-11 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen05_Base                                                                      0x0A05

/*--------------------------------------
BitField Name: NP11
BitField Type: R_O
BitField Desc: Next page  of port11
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen05_NP11_Mask                                                                    cBit31
#define cAf6_an_rxab_pen05_NP11_Shift                                                                       31

/*--------------------------------------
BitField Name: Ack11
BitField Type: R_O
BitField Desc: Acknowledge of port11
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_an_rxab_pen05_Ack11_Mask                                                                   cBit30
#define cAf6_an_rxab_pen05_Ack11_Shift                                                                      30

/*--------------------------------------
BitField Name: ReFault11
BitField Type: R_O
BitField Desc: Remote Fault of port11
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_an_rxab_pen05_ReFault11_Mask                                                            cBit29_28
#define cAf6_an_rxab_pen05_ReFault11_Shift                                                                  28

/*--------------------------------------
BitField Name: Hduplex11
BitField Type: R_O
BitField Desc: Half duplex of port11
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_an_rxab_pen05_Hduplex11_Mask                                                               cBit22
#define cAf6_an_rxab_pen05_Hduplex11_Shift                                                                  22

/*--------------------------------------
BitField Name: Fduplex11
BitField Type: R_O
BitField Desc: Full duplex of port11
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_an_rxab_pen05_Fduplex11_Mask                                                               cBit21
#define cAf6_an_rxab_pen05_Fduplex11_Shift                                                                  21

/*--------------------------------------
BitField Name: NP10
BitField Type: R_O
BitField Desc: Next page of port10
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen05_NP10_Mask                                                                    cBit15
#define cAf6_an_rxab_pen05_NP10_Shift                                                                       15

/*--------------------------------------
BitField Name: Ack10
BitField Type: R_O
BitField Desc: Acknowledge of port10
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_an_rxab_pen05_Ack10_Mask                                                                   cBit14
#define cAf6_an_rxab_pen05_Ack10_Shift                                                                      14

/*--------------------------------------
BitField Name: ReFault10
BitField Type: R_O
BitField Desc: Remote Fault of port10
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_an_rxab_pen05_ReFault10_Mask                                                            cBit13_12
#define cAf6_an_rxab_pen05_ReFault10_Shift                                                                  12

/*--------------------------------------
BitField Name: Hduplex10
BitField Type: R_O
BitField Desc: Half duplex of port10
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_an_rxab_pen05_Hduplex10_Mask                                                                cBit6
#define cAf6_an_rxab_pen05_Hduplex10_Shift                                                                   6

/*--------------------------------------
BitField Name: Fduplex10
BitField Type: R_O
BitField Desc: Full duplex of port10
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_an_rxab_pen05_Fduplex10_Mask                                                                cBit5
#define cAf6_an_rxab_pen05_Fduplex10_Shift                                                                   5


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX12-RX13 1000Basex ability
Reg Addr   : 0x0A06
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port12-13 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen06_Base                                                                      0x0A06

/*--------------------------------------
BitField Name: NP13
BitField Type: R_O
BitField Desc: Next page  of port13
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen06_NP13_Mask                                                                    cBit31
#define cAf6_an_rxab_pen06_NP13_Shift                                                                       31

/*--------------------------------------
BitField Name: Ack13
BitField Type: R_O
BitField Desc: Acknowledge of port13
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_an_rxab_pen06_Ack13_Mask                                                                   cBit30
#define cAf6_an_rxab_pen06_Ack13_Shift                                                                      30

/*--------------------------------------
BitField Name: ReFault13
BitField Type: R_O
BitField Desc: Remote Fault of port13
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_an_rxab_pen06_ReFault13_Mask                                                            cBit29_28
#define cAf6_an_rxab_pen06_ReFault13_Shift                                                                  28

/*--------------------------------------
BitField Name: Hduplex13
BitField Type: R_O
BitField Desc: Half duplex of port13
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_an_rxab_pen06_Hduplex13_Mask                                                               cBit22
#define cAf6_an_rxab_pen06_Hduplex13_Shift                                                                  22

/*--------------------------------------
BitField Name: Fduplex13
BitField Type: R_O
BitField Desc: Full duplex of port13
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_an_rxab_pen06_Fduplex13_Mask                                                               cBit21
#define cAf6_an_rxab_pen06_Fduplex13_Shift                                                                  21

/*--------------------------------------
BitField Name: NP12
BitField Type: R_O
BitField Desc: Next page of port12
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen06_NP12_Mask                                                                    cBit15
#define cAf6_an_rxab_pen06_NP12_Shift                                                                       15

/*--------------------------------------
BitField Name: Ack12
BitField Type: R_O
BitField Desc: Acknowledge of port12
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_an_rxab_pen06_Ack12_Mask                                                                   cBit14
#define cAf6_an_rxab_pen06_Ack12_Shift                                                                      14

/*--------------------------------------
BitField Name: ReFault12
BitField Type: R_O
BitField Desc: Remote Fault of port12
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_an_rxab_pen06_ReFault12_Mask                                                            cBit13_12
#define cAf6_an_rxab_pen06_ReFault12_Shift                                                                  12

/*--------------------------------------
BitField Name: Hduplex12
BitField Type: R_O
BitField Desc: Half duplex of port12
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_an_rxab_pen06_Hduplex12_Mask                                                                cBit6
#define cAf6_an_rxab_pen06_Hduplex12_Shift                                                                   6

/*--------------------------------------
BitField Name: Fduplex12
BitField Type: R_O
BitField Desc: Full duplex of port12
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_an_rxab_pen06_Fduplex12_Mask                                                                cBit5
#define cAf6_an_rxab_pen06_Fduplex12_Shift                                                                   5


/*------------------------------------------------------------------------------
Reg Name   : Auto-neg RX14-RX15 1000Basex ability
Reg Addr   : 0x0A07
Reg Formula:
    Where  :
Reg Desc   :
RX ability of RX-Port12-13 after Auto-neg succeed

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rxab_pen07_Base                                                                      0x0A07

/*--------------------------------------
BitField Name: NP15
BitField Type: R_O
BitField Desc: Next page  of port15
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_an_rxab_pen07_NP15_Mask                                                                    cBit31
#define cAf6_an_rxab_pen07_NP15_Shift                                                                       31

/*--------------------------------------
BitField Name: Ack15
BitField Type: R_O
BitField Desc: Acknowledge of port15
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_an_rxab_pen07_Ack15_Mask                                                                   cBit30
#define cAf6_an_rxab_pen07_Ack15_Shift                                                                      30

/*--------------------------------------
BitField Name: ReFault15
BitField Type: R_O
BitField Desc: Remote Fault of port15
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_an_rxab_pen07_ReFault15_Mask                                                            cBit29_28
#define cAf6_an_rxab_pen07_ReFault15_Shift                                                                  28

/*--------------------------------------
BitField Name: Hduplex15
BitField Type: R_O
BitField Desc: Half duplex of port15
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_an_rxab_pen07_Hduplex15_Mask                                                               cBit22
#define cAf6_an_rxab_pen07_Hduplex15_Shift                                                                  22

/*--------------------------------------
BitField Name: Fduplex15
BitField Type: R_O
BitField Desc: Full duplex of port15
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_an_rxab_pen07_Fduplex15_Mask                                                               cBit21
#define cAf6_an_rxab_pen07_Fduplex15_Shift                                                                  21

/*--------------------------------------
BitField Name: NP14
BitField Type: R_O
BitField Desc: Next page of port14
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_an_rxab_pen07_NP14_Mask                                                                    cBit15
#define cAf6_an_rxab_pen07_NP14_Shift                                                                       15

/*--------------------------------------
BitField Name: Ack14
BitField Type: R_O
BitField Desc: Acknowledge of port14
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_an_rxab_pen07_Ack14_Mask                                                                   cBit14
#define cAf6_an_rxab_pen07_Ack14_Shift                                                                      14

/*--------------------------------------
BitField Name: ReFault14
BitField Type: R_O
BitField Desc: Remote Fault of port14
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_an_rxab_pen07_ReFault14_Mask                                                            cBit13_12
#define cAf6_an_rxab_pen07_ReFault14_Shift                                                                  12

/*--------------------------------------
BitField Name: Hduplex14
BitField Type: R_O
BitField Desc: Half duplex of port14
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_an_rxab_pen07_Hduplex14_Mask                                                                cBit6
#define cAf6_an_rxab_pen07_Hduplex14_Shift                                                                   6

/*--------------------------------------
BitField Name: Fduplex14
BitField Type: R_O
BitField Desc: Full duplex of port14
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_an_rxab_pen07_Fduplex14_Mask                                                                cBit5
#define cAf6_an_rxab_pen07_Fduplex14_Shift                                                                   5


/*------------------------------------------------------------------------------
Reg Name   : GE Loss Of Synchronization Sticky
Reg Addr   : 0x080B
Reg Formula:
    Where  :
Reg Desc   :
Sticky state change Loss of synchronization

------------------------------------------------------------------------------*/
#define cAf6Reg_los_sync_stk_pen_Base                                                                   0x080B

/*--------------------------------------
BitField Name: GeLossOfSync_Stk
BitField Type: W1C
BitField Desc: Sticky state change of Loss Of Synchronization, bit per port,
bit0 is port0, bit7 is port7 ,..., bit15 is port15
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_los_sync_stk_pen_GeLossOfSync_Stk_Mask                                                   cBit15_0
#define cAf6_los_sync_stk_pen_GeLossOfSync_Stk_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : GE Loss Of Synchronization Status
Reg Addr   : 0x080A
Reg Formula:
    Where  :
Reg Desc   :
Current status of Loss of synchronization

------------------------------------------------------------------------------*/
#define cAf6Reg_los_sync_pen_Base                                                                       0x080A

/*--------------------------------------
BitField Name: GeLossOfSync_Cur
BitField Type: R_O
BitField Desc: Current status of Loss Of Synchronization, bit per port, bit0 is
port0, bit7 is port7 ,..., bit15 is port15
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_los_sync_pen_GeLossOfSync_Cur_Mask                                                       cBit15_0
#define cAf6_los_sync_pen_GeLossOfSync_Cur_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : GE Loss Of Synchronization Interrupt Enb
Reg Addr   : 0x0818
Reg Formula:
    Where  :
Reg Desc   :
Interrupt enable of Loss of synchronization

------------------------------------------------------------------------------*/
#define cAf6Reg_los_sync_int_enb_pen_Base                                                               0x0818

/*--------------------------------------
BitField Name: GeLossOfSync_Enb
BitField Type: R/W
BitField Desc: Interrupt enable of Loss of synchronization, bit per port, bit0
is port0, bit7 is port7 ,..., bit15 is port15
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_los_sync_int_enb_pen_GeLossOfSync_Enb_Mask                                               cBit15_0
#define cAf6_los_sync_int_enb_pen_GeLossOfSync_Enb_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : GE Auto-neg Sticky
Reg Addr   : 0x0807
Reg Formula:
    Where  :
Reg Desc   :
Sticky state change Auto-neg

------------------------------------------------------------------------------*/
#define cAf6Reg_an_sta_stk_pen_Base                                                                     0x0807

/*--------------------------------------
BitField Name: GeAuto_neg_Stk
BitField Type: W1C
BitField Desc: Sticky state change of Auto-neg , bit per port, bit0 is port0,
bit7 is port7 ,..., bit15 is port15
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_an_sta_stk_pen_GeAuto_neg_Stk_Mask                                                       cBit15_0
#define cAf6_an_sta_stk_pen_GeAuto_neg_Stk_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : GE Auto-neg Interrupt Enb
Reg Addr   : 0x0817
Reg Formula:
    Where  :
Reg Desc   :
Interrupt enable of Loss of synchronization

------------------------------------------------------------------------------*/
#define cAf6Reg_an_int_enb_pen_Base                                                                     0x0817

/*--------------------------------------
BitField Name: GeAuto_neg_Enb
BitField Type: R/W
BitField Desc: Interrupt enable of Auto-neg, bit per port, bit0 is port0, bit7
is port7 ,... , bit15 is port15
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_an_int_enb_pen_GeAuto_neg_Enb_Mask                                                       cBit15_0
#define cAf6_an_int_enb_pen_GeAuto_neg_Enb_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : GE Remote Fault Sticky
Reg Addr   : 0x0819
Reg Formula:
    Where  :
Reg Desc   :
Sticky state change Remote Fault

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rfi_stk_pen_Base                                                                     0x0819

/*--------------------------------------
BitField Name: GeRemote_Fault_Stk
BitField Type: W1C
BitField Desc: Sticky state change of Remote Fault , bit per port, bit0 is
port0, bit7 is port7 ,..., bit15 is port15
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_an_rfi_stk_pen_GeRemote_Fault_Stk_Mask                                                   cBit15_0
#define cAf6_an_rfi_stk_pen_GeRemote_Fault_Stk_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : GE Remote Fault Interrupt Enb
Reg Addr   : 0x081A
Reg Formula:
    Where  :
Reg Desc   :
Interrupt enable of Remote Fault

------------------------------------------------------------------------------*/
#define cAf6Reg_an_rfi_int_enb_pen_Base                                                                 0x081A

/*--------------------------------------
BitField Name: GeRemote_Fault_enb
BitField Type: R/W
BitField Desc: Interrupt enable of Remote Fault, bit per port, bit0 is port0,
bit7 is port7 , bit15 is port15
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_an_rfi_int_enb_pen_GeRemote_Fault_enb_Mask                                               cBit15_0
#define cAf6_an_rfi_int_enb_pen_GeRemote_Fault_enb_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : GE Excessive Error Ratio Status
Reg Addr   : 0x081C
Reg Formula:
    Where  :
Reg Desc   :
Interrupt Excessive Error Ratio Status

------------------------------------------------------------------------------*/
#define cAf6Reg_rxexcer_sta_pen_Base                                                                    0x081C

/*--------------------------------------
BitField Name: rxexcer_sta
BitField Type: RO
BitField Desc: Interrupt status of Excessive Error Ratio, bit per port, bit0 is
port0, bit7 is port7
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_rxexcer_sta_pen_rxexcer_sta_Mask                                                         cBit15_0
#define cAf6_rxexcer_sta_pen_rxexcer_sta_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : GE Excessive Error Ratio Sticky
Reg Addr   : 0x081D
Reg Formula:
    Where  :
Reg Desc   :
Interrupt Excessive Error Ratio Sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_rxexcer_stk_pen_Base                                                                    0x081D

/*--------------------------------------
BitField Name: rxexcer_stk
BitField Type: W1C
BitField Desc: Interrupt sticky of Excessive Error Ratio, bit per port, bit0 is
port0, bit7 is port7
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_rxexcer_stk_pen_rxexcer_stk_Mask                                                         cBit15_0
#define cAf6_rxexcer_stk_pen_rxexcer_stk_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : GE Excessive Error Ratio Enble
Reg Addr   : 0x081E
Reg Formula:
    Where  :
Reg Desc   :
Interrupt Excessive Error Ratio Enable

------------------------------------------------------------------------------*/
#define cAf6Reg_rxexcer_enb_pen_Base                                                                    0x081E

/*--------------------------------------
BitField Name: rxexcer_enb
BitField Type: R/W
BitField Desc: Interrupt Enable of Excessive Error Ratio, bit per port, bit0 is
port0, bit7 is port7
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_rxexcer_enb_pen_rxexcer_enb_Mask                                                         cBit15_0
#define cAf6_rxexcer_enb_pen_rxexcer_enb_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : GE Excessive Error Ratio Threshold
Reg Addr   : 0x0835
Reg Formula:
    Where  :
Reg Desc   :
Interrupt Excessive Error Ratio Threshold

------------------------------------------------------------------------------*/
#define cAf6Reg_rxexcer_thres_pen_Base                                                                  0x0835

/*--------------------------------------
BitField Name: rxexcer_thres_up
BitField Type: R/W
BitField Desc: Upper threshold of Excessive Error Ratio, max value is 32767
Excessive error alarm will raise when value error counter is greater than upper
threshold
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_rxexcer_thres_pen_rxexcer_thres_up_Mask                                                 cBit31_16
#define cAf6_rxexcer_thres_pen_rxexcer_thres_up_Shift                                                       16

/*--------------------------------------
BitField Name: rxexcer_thres_low
BitField Type: R/W
BitField Desc: Lower threshold of Excessive Error Ratio, max value is 32767
Excessive error alarm will clear when value error counter is lower than upper
threshold
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_rxexcer_thres_pen_rxexcer_thres_low_Mask                                                 cBit15_0
#define cAf6_rxexcer_thres_pen_rxexcer_thres_low_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : GE Interrupt OR
Reg Addr   : 0x0830
Reg Formula:
    Where  :
Reg Desc   :
Interrupt OR of all port per interrupt type

------------------------------------------------------------------------------*/
#define cAf6Reg_GE_Interrupt_OR_Base                                                                    0x0830

#define cAf6_GE_Interrupt_OR_link_stat_int_or_Mask                                                         cBit5
#define cAf6_GE_Interrupt_OR_link_stat_int_or_Shift                                                            5

#define cAf6_GE_Interrupt_OR_fx100base_int_or_Mask                                                         cBit4
#define cAf6_GE_Interrupt_OR_fx100base_int_or_Shift                                                            4

/*--------------------------------------
BitField Name: rxexcer_int_or
BitField Type: R_O
BitField Desc: Interrupt OR of Excessive Error Ratio
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_GE_Interrupt_OR_rxexcer_int_or_Mask                                                         cBit3
#define cAf6_GE_Interrupt_OR_rxexcer_int_or_Shift                                                            3

/*--------------------------------------
BitField Name: an_rfi_int_or
BitField Type: R_O
BitField Desc: Interrupt OR of Remote fault
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_GE_Interrupt_OR_an_rfi_int_or_Mask                                                          cBit2
#define cAf6_GE_Interrupt_OR_an_rfi_int_or_Shift                                                             2

/*--------------------------------------
BitField Name: ant_int_or
BitField Type: R_O
BitField Desc: Interrupt OR of Auto-neg state change
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_GE_Interrupt_OR_ant_int_or_Mask                                                             cBit1
#define cAf6_GE_Interrupt_OR_ant_int_or_Shift                                                                1

/*--------------------------------------
BitField Name: los_sync_int_or
BitField Type: R_O
BitField Desc: Interrupt OR of Loss of sync
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_GE_Interrupt_OR_los_sync_int_or_Mask                                                        cBit0
#define cAf6_GE_Interrupt_OR_los_sync_int_or_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : GE Loss Of Sync AND MASK
Reg Addr   : 0x0831
Reg Formula:
    Where  :
Reg Desc   :
Interrupt Loss of sync current status AND MASK

------------------------------------------------------------------------------*/
#define cAf6Reg_GE_Loss_Of_Sync_AND_MASK_Base                                                           0x0831

/*--------------------------------------
BitField Name: GeLossOfSync_and_mask
BitField Type: R_O
BitField Desc: Interrupt Loss of sync, bit per port, bit0 is port0, bit7 is
port7
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_GE_Loss_Of_Sync_AND_MASK_GeLossOfSync_and_mask_Mask                                      cBit15_0
#define cAf6_GE_Loss_Of_Sync_AND_MASK_GeLossOfSync_and_mask_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : GE Auto_neg State Change AND MASK
Reg Addr   : 0x0832
Reg Formula:
    Where  :
Reg Desc   :
Interrupt Auto-neg state change current status AND MASK

------------------------------------------------------------------------------*/
#define cAf6Reg_GE_Auto_neg_State_Change_AND_MASK_Base                                                  0x0832

/*--------------------------------------
BitField Name: GeAuto_neg_and_mask
BitField Type: R_O
BitField Desc: Interrupt Auto-neg state change, bit per port, bit0 is port0,
bit7 is port7
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_GE_Auto_neg_State_Change_AND_MASK_GeAuto_neg_and_mask_Mask                                cBit15_0
#define cAf6_GE_Auto_neg_State_Change_AND_MASK_GeAuto_neg_and_mask_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : GE Remote Fault AND MASK
Reg Addr   : 0x0833
Reg Formula:
    Where  :
Reg Desc   :
Interrupt Remote Fault current status AND MASK

------------------------------------------------------------------------------*/
#define cAf6Reg_GE_Remote_Fault_AND_MASK_Base                                                           0x0833

/*--------------------------------------
BitField Name: GeRemote_Fault_and_mask
BitField Type: R_O
BitField Desc: Interrupt Enable of Remote Fault, bit per port, bit0 is port0,
bit7 is port7
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_GE_Remote_Fault_AND_MASK_GeRemote_Fault_and_mask_Mask                                    cBit15_0
#define cAf6_GE_Remote_Fault_AND_MASK_GeRemote_Fault_and_mask_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : GE Excessive Error Ratio AND MASK
Reg Addr   : 0x0834
Reg Formula:
    Where  :
Reg Desc   :
Interrupt Excessive Error Ratio current status AND MASK

------------------------------------------------------------------------------*/
#define cAf6Reg_GE_Excessive_Error_Ratio_AND_MASK_Base                                                  0x0834

/*--------------------------------------
BitField Name: rxexcer_and_mask
BitField Type: R_O
BitField Desc: Interrupt Enable of Excessive Error Ratio, bit per port, bit0 is
port0, bit7 is port7
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_GE_Excessive_Error_Ratio_AND_MASK_rxexcer_and_mask_Mask                                  cBit15_0
#define cAf6_GE_Excessive_Error_Ratio_AND_MASK_rxexcer_and_mask_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : GE 1000basex Force K30_7
Reg Addr   : 0x0822
Reg Formula:
    Where  :
Reg Desc   :
Forcing countinuous K30.7 for 1000basex mode

------------------------------------------------------------------------------*/
#define cAf6Reg_GE_1000basex_Force_K30_7_Base                                                           0x0822

/*--------------------------------------
BitField Name: tx_fk30_7
BitField Type: R/W
BitField Desc: Force K30.7 code, bit per port, bit0 is port0, bit7 is port7
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_GE_1000basex_Force_K30_7_tx_fk30_7_Mask                                                  cBit15_0
#define cAf6_GE_1000basex_Force_K30_7_tx_fk30_7_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : ENABLE TX 100base FX
Reg Addr   : 0x0A08
Reg Formula:
    Where  :
Reg Desc   :
Interrupt enable of Remote Fault

------------------------------------------------------------------------------*/
#define cAf6Reg_fx_txenb_Base                                                                           0x0A08
#define cAf6Reg_fx_txenb_WidthVal                                                                           32

/*--------------------------------------
BitField Name: fx_entx
BitField Type: R/W
BitField Desc: (1) is enable, (0) is disable,  bit0 is port0, bit15 is port15
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_fx_txenb_fx_entx_Mask                                                                    cBit15_0
#define cAf6_fx_txenb_fx_entx_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Sticky Rx PATTERN DETECT
Reg Addr   : 0x0A09
Reg Formula:
    Where  :
Reg Desc   :
Interrupt enable of Remote Fault

------------------------------------------------------------------------------*/
#define cAf6Reg_fx_patstken_Base                                                                        0x0A09
#define cAf6Reg_fx_patstken_WidthVal                                                                        32

/*--------------------------------------
BitField Name: fx_patstk
BitField Type: R/W
BitField Desc: sticky pattern detect
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_fx_patstken_fx_patstk_Mask                                                               cBit15_0
#define cAf6_fx_patstken_fx_patstk_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Sticky Rx CODE ERROR DETECT
Reg Addr   : 0x0A0A
Reg Formula:
    Where  :
Reg Desc   :
Interrupt enable of Remote Fault

------------------------------------------------------------------------------*/
#define cAf6Reg_fx_codestken_Base                                                                       0x0A0A
#define cAf6Reg_fx_codestken_WidthVal                                                                       32

/*--------------------------------------
BitField Name: fx_codeerrstk
BitField Type: R/W
BitField Desc: sticky code error detect
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_fx_codestken_fx_codeerrstk_Mask                                                          cBit15_0
#define cAf6_fx_codestken_fx_codeerrstk_Shift                                                                0

/*--------------------------------------------------------------------------------
TX IPG Config Port07-00
Register Full Name: TX IPG Config Part0
RTL Instant Name  : TX_IPG_Config_Part0
Address      : 0x0810
Formula      : {N/A}
Description  : TX IPG Config Part0, port#00 to port#07
Width        : 32
Field : [Bit:Bit] %% Name            %% Description          %% Type %% Reset %% Default
Fieldx: [31:28]   %% txipg_port07    %% TX IPG port07, IPG = txipg_port07 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [27:24]   %% txipg_port06    %% TX IPG port06, IPG = txipg_port06 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [23:20]   %% txipg_port05    %% TX IPG port05, IPG = txipg_port05 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [19:16]   %% txipg_port04    %% TX IPG port04, IPG = txipg_port04 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [15:12]   %% txipg_port03    %% TX IPG port03, IPG = txipg_port03 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [11:08]   %% txipg_port02    %% TX IPG port02, IPG = txipg_port02 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [07:04]   %% txipg_port01    %% TX IPG port01, IPG = txipg_port01 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [03:00]   %% txipg_port00    %% TX IPG port00, IPG = txipg_port00 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
----------------------------------------------------------------------------------*/
#define cAf6Reg_TX_IPG_Config_Part0_Base 0x0810

/*---------------------------------------------------------------------------------
TX IPG Config Port15-08
Register Full Name: TX IPG Config Part1
RTL Instant Name  : TX_IPG_Config_Part1
Address      : 0x0811
Formula      : {N/A}
Description  : TX IPG Config Part1, port#08 to port#15
Width        : 32
Field : [Bit:Bit] %% Name            %% Description          %% Type %% Reset %% Default
Fieldx: [31:28]   %% txipg_port15    %% TX IPG port15, IPG = txipg_port15 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [27:24]   %% txipg_port14    %% TX IPG port14, IPG = txipg_port14 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [23:20]   %% txipg_port13    %% TX IPG port13, IPG = txipg_port13 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [19:16]   %% txipg_port12    %% TX IPG port12, IPG = txipg_port12 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [15:12]   %% txipg_port11    %% TX IPG port11, IPG = txipg_port11 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [11:08]   %% txipg_port10    %% TX IPG port10, IPG = txipg_port10 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [07:04]   %% txipg_port09    %% TX IPG port09, IPG = txipg_port09 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
Fieldx: [03:00]   %% txipg_port08    %% TX IPG port08, IPG = txipg_port08 * 2, default value is 6, it means IPG is 12bytes %% R/W  %% 0x6   %% 0x6
-------------------------------------------------------------------------------------*/
#define cAf6Reg_TX_IPG_Config_Part1_Base 0x0811

/*-----------------------------------------------------------------------------------
Register Full Name: Ge Link State Sticky
RTL Instant Name  : Ge_Link_State_Sticky
Address      : 0x0809
Formula      : {N/A}
Description  : Link status change of group 0 from port 0  to port 15
Width        : 32
Field : [Bit:Bit] %% Name       %% Description          %% Type %% Reset %% Default
Fieldx: [31:16]   %% Unused     %%                      %%      %%       %%
Fieldx: [15:00]   %% lnk_sta_stk%% Link status, bit per port, bit[0] port  0, bit[7] port 7 ... , bit[15] port 15
                                     {1} : link status change
                                     {0} : link status change     %% W1C  %% 0x0   %% 0x0
-------------------------------------------------------------------------------------*/
#define cAf6Reg_Ge_Link_State_Sticky_Base 0x0809

/*-----------------------------------------------------------------------------------
Register Full Name: Ge Link State Interrupt Enb
RTL Instant Name  : Ge Link_State_Interrupt_enb
Address      : 0x0815
Formula      : {N/A}
Description  : Link status of group 0 from port 0  to port 15
Width        : 32
Field : [Bit:Bit] %% Name       %% Description          %% Type %% Reset %% Default
Fieldx: [31:16]   %% Unused     %%                      %%      %%       %%
Fieldx: [15:00]   %% lnk_sta_enb%% Link status, bit per port, bit[0] port  0, bit[7] port 7 ... , bit[15] port 15
                                     {1} : enb
                                     {0} : dis           %% R/W  %% 0x0   %% 0x0
--------------------------------------------------------------------------------------*/
#define cAf6Reg_Ge_Link_State_Interrupt_enb_Base 0x0815

/*-------------------------------------------------------------------------------------
Link Status Ratio AND MASK
Register Full Name: Ge Link State AND MASK
RTL Instant Name  : Ge_Link_State_AND_MASK
Address      : 0x083A
Formula      : {N/A}
Description  : Interrupt Excessive Error Ratio current status AND MASK
Width        : 32
Field : [Bit:Bit] %% Name               %% Description          %% Type %% Reset %% Default
Fieldx: [31:16]   %% Unused             %%                      %% R_O  %% 0x0   %% 0x0
Fieldx: [15:00]   %% linksta_and_mask   %% Ge Link State AND MASK, bit per port, bit0 is port0, bit7 is port7 %% R_O  %% 0x0   %% 0x0
--------------------------------------------------------------------------------------*/
#define cAf6Reg_Ge_Link_State_AND_MASK 0x083A


/*---------------------------------------------------------------
Sticky FAR END FAULT DETECT                                  #
Register Full Name: Sticky Rx CODE ERROR DETECT
RTL Instant Name  : fx_stk_fefd_pen
Address      : 0x0A16
Formula      : {N/A}
Description  : Interrupt Alarm of Remote Fault and Local fault
----------------------------------------------------------------*/
#define cAf6Reg_fx_stk_fefd_pen_Base 0x0A16

/*----------------------------------------------------------------
CURRENT STATUS FAR END FAULT DETECT                          #
Register Full Name: CURRENT STATUS Rx CODE ERROR DETECT
RTL Instant Name  : fx_alm_fefd_pen
Address      : 0x0A17
Formula      : {N/A}
Description  : Interrupt current status of Remote Fault
------------------------------------------------------------------------*/
#define cAf6Reg_fx_alm_fefd_pen_Base 0x0A17

/*-----------------------------------------------------------------------
ENABLE FEFD INTTERUPT
Register Full Name: ENABLE FEFD INTTERUPT
RTL Instant Name  : fx_fefd_inten
Address      : 0x0A18
Description  : Interrupt enable of Remote Fault
Width        : 32
Field : [Bit:Bit] %% Name              %% Description          %% Type %% Reset %% Default

Fieldx: [31:31]   %% fx_int_msk_rf15       %% enable interrupt far end detect port 15 ( remote fault)          %% R/W  %% 0x0   %% 0x0
Fieldx: [30:30]   %% fx_int_msk_lf15       %% enable interrupt local fault port 15                             %% R/W  %% 0x0   %% 0x0

Fieldx: [29:29]   %% fx_int_msk_rf14       %% enable interrupt far end detect port 14 ( remote fault)          %% R/W  %% 0x0   %% 0x0
Fieldx: [28:28]   %% fx_int_msk_lf14       %% enable interrupt local fault port 14                             %% R/W  %% 0x0   %% 0x0

Fieldx: [27:27]   %% fx_int_msk_rf13       %% enable interrupt far end detect port 13 ( remote fault)          %% R/W  %% 0x0   %% 0x0
Fieldx: [26:26]   %% fx_int_msk_lf13       %% enable interrupt local fault port 13                             %% R/W  %% 0x0   %% 0x0

Fieldx: [25:25]   %% fx_int_msk_rf12       %% enable interrupt far end detect port 12 ( remote fault)          %% R/W  %% 0x0   %% 0x0
Fieldx: [24:24]   %% fx_int_msk_lf12       %% enable interrupt local fault port 12                             %% R/W  %% 0x0   %% 0x0

Fieldx: [23:23]   %% fx_int_msk_rf11       %% enable interrupt far end detect port 11 ( remote fault)          %% R/W  %% 0x0   %% 0x0
Fieldx: [22:22]   %% fx_int_msk_lf11       %% enable interrupt local fault port 11                             %% R/W  %% 0x0   %% 0x0

Fieldx: [21:21]   %% fx_int_msk_rf10       %% enable interrupt far end detect port 10 ( remote fault)          %% R/W  %% 0x0   %% 0x0
Fieldx: [20:20]   %% fx_int_msk_lf10       %% enable interrupt local fault port 10                             %% R/W  %% 0x0   %% 0x0

Fieldx: [19:19]   %% fx_int_msk_rf9        %% enable interrupt far end detect port 9 ( remote fault)           %% R/W  %% 0x0   %% 0x0
Fieldx: [18:18]   %% fx_int_msk_lf9        %% enable interrupt local fault port 9                              %% R/W  %% 0x0   %% 0x0

Fieldx: [17:17]   %% fx_int_msk_rf8        %% enable interrupt far end detect port 8 ( remote fault)           %% R/W  %% 0x0   %% 0x0
Fieldx: [16:16]   %% fx_int_msk_lf8        %% enable interrupt local fault port 8                              %% R/W  %% 0x0   %% 0x0

Fieldx: [15:15]   %% fx_int_msk_rf7        %% enable interrupt far end detect port 7 ( remote fault)           %% R/W  %% 0x0   %% 0x0
Fieldx: [14:14]   %% fx_int_msk_lf7        %% enable interrupt local fault port 7                              %% R/W  %% 0x0   %% 0x0

Fieldx: [13:13]   %% fx_int_msk_rf7        %% enable interrupt far end detect port 6 ( remote fault)           %% R/W  %% 0x0   %% 0x0
Fieldx: [12:12]   %% fx_int_msk_lf6        %% enable interrupt local fault port 6                              %% R/W  %% 0x0   %% 0x0

Fieldx: [11:11]   %% fx_int_msk_rf5        %% enable interrupt far end detect port 5 ( remote fault)           %% R/W  %% 0x0   %% 0x0
Fieldx: [10:10]   %% fx_int_msk_lf5        %% enable interrupt local fault port 5                              %% R/W  %% 0x0   %% 0x0

Fieldx: [09:09]   %% fx_int_msk_rf4        %% enable interrupt far end detect port 4 ( remote fault)           %% R/W  %% 0x0   %% 0x0
Fieldx: [08:08]   %% fx_int_msk_lf4        %% enable interrupt local fault port 4                              %% R/W  %% 0x0   %% 0x0

Fieldx: [07:07]   %% fx_int_msk_rf3        %% enable interrupt far end detect port 3 ( remote fault)           %% R/W  %% 0x0   %% 0x0
Fieldx: [06:06]   %% fx_int_msk_lf3        %% enable interrupt local fault port 3                              %% R/W  %% 0x0   %% 0x0

Fieldx: [05:05]   %% fx_int_msk_rf2        %% enable interrupt far end detect port 2 ( remote fault)           %% R/W  %% 0x0   %% 0x0
Fieldx: [04:04]   %% fx_int_msk_lf2        %% enable interrupt local fault port 2                              %% R/W  %% 0x0   %% 0x0

Fieldx: [03:03]   %% fx_int_msk_rf1        %% enable interrupt far end detect port 1 ( remote fault)           %% R/W  %% 0x0   %% 0x0
Fieldx: [02:02]   %% fx_int_msk_lf1        %% enable interrupt local fault port 1                              %% R/W  %% 0x0   %% 0x0

Fieldx: [01:01]   %% fx_int_msk_rf0        %% enable interrupt far end detect port 0 ( remote fault)           %% R/W  %% 0x0   %% 0x0
Fieldx: [00:00]   %% fx_int_msk_lf0        %% enable interrupt local fault port 0
---------------------------------------------------------------------*/
#define cAf6Reg_fx_fefd_inten_Base 0x0A18
#define cAf6_fx_int_msk_lf_Mask(portId) (cBit0 << (portId * 2))
#define cAf6_fx_int_msk_lf_Shift(portId) (portId * 2)
#define cAf6_fx_int_msk_rf_Mask(portId) (cBit0 << (portId * 2 + 1))
#define cAf6_fx_int_msk_rf_Shift(portId) (portId * 2 + 1)

#endif /* _AF6_REG_AF6CNC0022_RD_SGMII_Multirate_H_ */

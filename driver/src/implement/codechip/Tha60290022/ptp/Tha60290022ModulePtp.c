/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : Tha60290022ModulePtp.c
 *
 * Created Date: Jul 2, 2018
 *
 * Description : Implement the 60290022 module PTP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60290021/eth/Tha60290021ModuleEth.h"
#include "../../Tha60290021/eth/Tha60290021SerdesBackplaneEthPort.h"
#include "../eth/Tha60290022ETH40GReg.h"
#include "Tha60290022ModulePtp.h"
#include "Tha60290022ModulePtpInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290022ModulePtp)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePtpMethods m_ThaModulePtpOverride;

/* Save super implementations */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule(ThaModulePtp self)
    {
    return (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEth);
    }

static AtEthPort PhysicalEthPortGet(ThaModulePtp self, AtPtpPort port)
    {
    uint32 portId = AtChannelIdGet((AtChannel)port);

    if (portId == 0)
        return Tha60290021ModuleEth40GMacPortGet(EthModule(self), 0);

    return Tha60290021ModuleEthFaceplatePortGet(EthModule(self), (uint8)(portId - 1));
    }

static eAtModulePtpRet PhysicalBackplanePortDeviceTypeSet(ThaModulePtp self, eAtPtpDeviceType deviceType, uint32 localPortId)
    {
    Tha60290021SerdesBackplaneEthPort backplanePort = (Tha60290021SerdesBackplaneEthPort)Tha60290021ModuleEth40GMacPortGet(EthModule(self), localPortId);
    uint32 address = Tha60290021SerdesbackplaneEthPortAddressWithLocalAddress(backplanePort, cAf6Reg_ETH_40G_CFG_PTP_Base);
    uint32 regVal = mChannelHwRead(backplanePort, address, cAtModuleEth);
    uint32 hwDeviceType = (deviceType == cAtPtpDeviceTypeBoundary) ? 0 : 1;

    mRegFieldSet(regVal, cAf6_ETH_40G_CFG_PTP_tc_mode_, hwDeviceType);
    mChannelHwWrite(backplanePort, address, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtModulePtpRet PhysicalBackplaneDeviceTypeSet(ThaModulePtp self, eAtPtpDeviceType deviceType)
    {
    eAtRet ret = cAtOk;
    ret  = PhysicalBackplanePortDeviceTypeSet(self, deviceType, 0);
    ret |= PhysicalBackplanePortDeviceTypeSet(self, deviceType, 1);
    return ret;
    }

static AtPtpPort EgressPortIdToPtpPort(ThaModulePtp self, uint32 egressPortId)
    {
    return AtModulePtpPortGet((AtModulePtp)self, (egressPortId + 1));
    }

static void OverrideThaModulePtp(AtModulePtp self)
    {
    ThaModulePtp module = (ThaModulePtp)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePtpOverride, mMethodsGet(module), sizeof(m_ThaModulePtpOverride));

        /* Setup methods */
        mMethodOverride(m_ThaModulePtpOverride, PhysicalEthPortGet);
        mMethodOverride(m_ThaModulePtpOverride, PhysicalBackplaneDeviceTypeSet);
        mMethodOverride(m_ThaModulePtpOverride, EgressPortIdToPtpPort);
        }

    mMethodsSet(module, &m_ThaModulePtpOverride);
    }

static void Override(AtModulePtp self)
    {
    OverrideThaModulePtp(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModulePtp);
    }

AtModulePtp Tha60290022ModulePtpObjectInit(AtModulePtp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePtpObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePtp Tha60290022ModulePtpNew(AtDevice self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePtp newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    return Tha60290022ModulePtpObjectInit(newModule, self);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : Tha60290022ModulePtpInternal.h
 * 
 * Created Date: Nov 19, 2018
 *
 * Description : Internal data of the 60290022 module ptp
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPTPINTERNAL_H_
#define _THA60290022MODULEPTPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ptp/ThaModulePtpInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModulePtp
    {
    tThaModulePtp super;
    }tTha60290022ModulePtp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePtp Tha60290022ModulePtpObjectInit(AtModulePtp self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPTPINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290022PdhPrmAnalyzer.c
 *
 * Created Date: Nov 15, 2017
 *
 * Description : PRM analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/common/AtChannelInternal.h"
#include "../../../../default/pdh/ThaPdhPrmAnalyzerInternal.h"
#include "../../../../default/pdh/ThaPdhDe1.h"
#include "../Tha60290022PdhPrmReg.h"
#include "Tha60290022PdhPrmAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PdhPrmAnalyzer
    {
    tThaPdhPrmAnalyzer super;
    }tTha60290022PdhPrmAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPdhPrmAnalyzerMethods m_ThaPdhPrmAnalyzerOverride;

/* Save super implementation */
static const tThaPdhPrmAnalyzerMethods *m_ThaPdhPrmAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 UpenRxPrmCtrlBase(ThaPdhPrmAnalyzer self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_ctrl_Base;
    }

static uint32 UpenRxprmMonBase(ThaPdhPrmAnalyzer self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_mon_Base;
    }

static eAtRet ChannelHwSet(ThaPdhPrmAnalyzer self, AtChannel channel)
    {
    uint32 hwSlice, hwSts, vtgId, vtId;
    AtPdhChannel de1 = (AtPdhChannel)channel;
    eAtRet ret;
    uint32 regAddr, regVal;
    uint32 prmId;

    ret = ThaPdhDe1HwIdFactorGet((ThaPdhDe1)de1, cAtModulePdh, &hwSlice, &hwSts, &vtgId, &vtId);
    if (ret != cAtOk)
        return ret;

    regAddr = ThaPdhPrmAnalyzerUpenRxPrmCtrlAddress(self);
    regVal = mChannelHwRead(channel, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_rxprm_ctrl_prm_slid_, hwSlice);

    prmId = (hwSts * 32) + (vtgId * 4) + vtId;
    mRegFieldSet(regVal, cAf6_upen_rxprm_ctrl_prm_cfgid_, prmId);
    mChannelHwWrite(channel, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static void OverrideThaPdhPrmAnalyzer(AtPktAnalyzer self)
    {
    ThaPdhPrmAnalyzer analyzer = (ThaPdhPrmAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhPrmAnalyzerMethods = mMethodsGet(analyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhPrmAnalyzerOverride, m_ThaPdhPrmAnalyzerMethods, sizeof(m_ThaPdhPrmAnalyzerOverride));

        mMethodOverride(m_ThaPdhPrmAnalyzerOverride, ChannelHwSet);
        mMethodOverride(m_ThaPdhPrmAnalyzerOverride, UpenRxPrmCtrlBase);
        mMethodOverride(m_ThaPdhPrmAnalyzerOverride, UpenRxprmMonBase);
        }

    mMethodsSet(analyzer, &m_ThaPdhPrmAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideThaPdhPrmAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PdhPrmAnalyzer);
    }

static AtPktAnalyzer ObjectInit(AtPktAnalyzer self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPrmAnalyzerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60290022PdhPrmAnalyzerNew(AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newAnalyzer, channel);
    }

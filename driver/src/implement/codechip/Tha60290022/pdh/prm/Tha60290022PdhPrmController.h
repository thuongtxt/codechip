/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290022PdhPrmController.h
 * 
 * Created Date: Oct 17, 2017
 *
 * Description : PRM controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PDHPRMCONTROLLER_H_
#define _THA60290022PDHPRMCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhPrmController Tha60290022PdhPrmControllerNew(AtPdhDe1 de1);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PDHPRMCONTROLLER_H_ */


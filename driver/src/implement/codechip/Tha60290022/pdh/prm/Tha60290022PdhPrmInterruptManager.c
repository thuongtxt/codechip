/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290022PdhPrmInterruptManager.c
 *
 * Created Date: Oct 18, 2017
 *
 * Description : PDH PRM interrupt manager.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/pdh/AtPdhPrmControllerInternal.h"
#include "../../../../default/pdh/ThaModulePdh.h"
#include "../../../Tha60210011/pdh/prm/Tha60210011PdhPrmInterruptManagerInternal.h"
#include "../Tha60290022PdhPrmReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PdhPrmInterruptManager
    {
    tTha60210011PdhPrmInterruptManager super;
    }tTha60290022PdhPrmInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods  m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods m_ThaInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePdh PdhModule(AtInterruptManager self)
    {
    return (ThaModulePdh)AtInterruptManagerModuleGet(self);
    }

static uint32 GroupOffset(uint8 groupId)
    {
    if (groupId == 0)
        return 0x0;
    return 0x1000;
    }

static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    ThaInterruptManager manager = (ThaInterruptManager)self;
    ThaModulePdh pdhModule = PdhModule(self);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(manager);
    uint32 numSlices = ThaInterruptManagerNumSlices(manager);
    uint32 numStsInSlice = ThaInterruptManagerNumStsInSlices(manager);
    uint32 groupIntr = AtHalRead(hal, (baseAddress + cAf6Reg_upen_int_prmsta_Base));
    uint8 groupId, de3;
    AtUnused(glbIntr);

    for (groupId = 0, de3 = 0; groupId < 2; groupId++)
        {
        if (groupIntr & cIteratorMask(groupId))
            {
            uint32 groupBaseAddress = baseAddress + GroupOffset(groupId);
            uint32 intrStatus = AtHalRead(hal, (groupBaseAddress + cAf6Reg_prm_lb_sta_int1_Base));
            uint32 intrMask = AtHalRead(hal, (groupBaseAddress + cAf6Reg_prm_lb_en_int_Base));
            uint32 idx;
            intrStatus &= intrMask;

            for (idx = 0; idx < 32 && de3 < numStsInSlice; idx++, de3++)
                {
                if (intrStatus & cIteratorMask(idx))
                    {
                    uint32 intrDe1Status = AtHalRead(hal, (groupBaseAddress + cAf6Reg_prm_lb_int1sta_Base + idx));
                    uint8 de28 = 0;
                    uint8 de2, de1;

                    for (de2 = 0, de28 = 0; de2 < 7; de2++)
                        {
                        for (de1 = 0; de1 < 4; de1++, de28++)
                            {
                            if (intrDe1Status & cIteratorMask(de28))
                                {
                                uint8 slice;
                                for (slice = 0; slice < numSlices; slice++)
                                    {
                                    AtPdhDe1 de1Channel = ThaModulePdhDe1ChannelFromHwIdGet(pdhModule, cAtModulePdh, slice, de3, de2, de1);
                                    AtPdhPrmController controller = AtPdhDe1PrmControllerGet(de1Channel);
                                    AtPdhPrmControllerInterruptProcess(controller, slice, de3, de2, de1, hal);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

static void InterruptHwEnable(ThaInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    if (hal)
        {
        uint32 baseAddress = ThaInterruptManagerBaseAddress(self);
        uint32 regVal = (enable) ? cAf6_prm_lb_en_int_prm_lben_int_Mask : 0x0;
        AtHalWrite(hal, baseAddress + cAf6Reg_prm_lb_en_int_Base, regVal);
        AtHalWrite(hal, baseAddress + cAf6Reg_prm_lb_en_int2_Base, regVal);
        }
    }

static uint32 NumStsInSlice(ThaInterruptManager self)
    {
    AtUnused(self);
    return 48;
    }

static uint32 InterruptStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_prm_lb_int1_sta_Base;
    }

static uint32 InterruptEnableRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_prm_cfg_lben_int1_Base;
    }

static uint32 CurrentStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_prm_lb_int1_crrsta_Base;
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, InterruptHwEnable);
        mMethodOverride(m_ThaInterruptManagerOverride, NumStsInSlice);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptEnableRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, CurrentStatusRegister);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PdhPrmInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PdhPrmInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60290022PdhPrmInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newManager, module);
    }

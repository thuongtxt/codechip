/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290022PdhPrmController.c
 *
 * Created Date: Oct 17, 2017
 *
 * Description : PRM controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60210011/pdh/Tha60210011PdhPrmControllerInternal.h"
#include "../Tha60290022PdhPrmReg.h"
#include "Tha60290022PdhPrmController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PdhPrmController
    {
    tTha60210011PdhPrmController super;
    }tTha60290022PdhPrmController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPdhPrmControllerMethods m_ThaPdhPrmControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TxByteRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_txprm_cnt_byte_Base;
    }

static uint32 Read2ClearOffset(ThaPdhPrmController self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? 0 : 16384;
    }

static uint32 TxMessageRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_txprm_cnt_pkt_Base;
    }

static uint32 RxGoodMessageRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_gmess_Base;
    }

static uint32 RxDiscardRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_drmess_Base;
    }

static uint32 RxMissRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_mmess_Base;
    }

static uint32 RxByteRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_cnt_byte_Base;
    }

static uint32 InterruptOffset(ThaPdhPrmController self, uint8 *sliceId)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)AtPdhPrmControllerDe1Get((AtPdhPrmController)self);
    uint8 hwStsId, hwSliceId, de2Id, de1Id;
    uint32 offset;

    if (ThaPdhDe1HwIdGet(de1, cAtModulePdh, &hwSliceId, &hwStsId, &de2Id, &de1Id) != cAtOk)
        return cBit31_0;

    if (sliceId)
        *sliceId = hwSliceId;

    offset = (uint32)(((hwStsId & 0x1F) << 5) + (de2Id << 2) + de1Id);
    if (hwStsId >= 32)
        return offset + 0x400;
    else
        return offset;
    }

static void OverrideThaPdhPrmController(AtPdhPrmController self)
    {
    ThaPdhPrmController controller = (ThaPdhPrmController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhPrmControllerOverride, mMethodsGet(controller), sizeof(m_ThaPdhPrmControllerOverride));

        mMethodOverride(m_ThaPdhPrmControllerOverride, TxByteRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, Read2ClearOffset);
        mMethodOverride(m_ThaPdhPrmControllerOverride, TxMessageRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxGoodMessageRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxDiscardRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxMissRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxByteRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, InterruptOffset);
        }

    mMethodsSet(controller, &m_ThaPdhPrmControllerOverride);
    }

static void Override(AtPdhPrmController self)
    {
    OverrideThaPdhPrmController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PdhPrmController);
    }

static AtPdhPrmController ObjectInit(AtPdhPrmController self, AtPdhDe1 de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PdhPrmControllerObjectInit(self, de1) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhPrmController Tha60290022PdhPrmControllerNew(AtPdhDe1 de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhPrmController newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newObject, de1);
    }

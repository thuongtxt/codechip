/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290022ModulePdhV3.c
 *
 * Created Date: Feb 6, 2018
 *
 * Description : 60290022 Fpga optimization implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../sdh/Tha60290022ModuleSdh.h"
#include "Tha60290022ModulePdhInternal.h"
#include "Tha60290022ModulePdhReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdhMethods         m_ThaModulePdhOverride;
static tTha60290022ModulePdhMethods m_Tha60290022ModulePdhOverride;
static tAtModulePdhMethods          m_AtModulePdhOverride;

/* Save super implementation */
static const tThaModulePdhMethods         *m_ThaModulePdhMethods = NULL;
static const tTha60290022ModulePdhMethods *m_Tha60290022ModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Af6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_Address(Tha60290022ModulePdh self, uint8 groupId)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_V3(groupId);
    }

static uint32 Af6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_Address(Tha60290022ModulePdh self, uint8 groupId)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_V3(groupId);
    }

static uint32 Af6Reg_dej1_rx_framer_per_chn_intr_or_stat_Address(Tha60290022ModulePdh self, uint8 groupId, uint8 de3Id)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat_V3(groupId, de3Id);
    }

static eBool ShouldReportBerViaBerController(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtPdhDe3 VcDe3Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    return Tha60290022PdhVcDe3V3New(AtSdhChannelSts1Get(sdhVc), self);
    }

static ThaModuleSdh ModuleSdh(ThaModulePdh self)
    {
    return (ThaModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleSdh);
    }

static eAtRet De3InterruptEnable(ThaModulePdh self, eBool enable)
    {
    eAtRet ret;

    ret  = m_ThaModulePdhMethods->De3InterruptEnable(self, enable);
    ret |= Tha60290022ModuleSdhV2De3InterruptEnable(ModuleSdh(self), enable);

    return ret;
    }

static eAtRet De1InterruptEnable(ThaModulePdh self, eBool enable)
    {
    eAtRet ret;

    ret  = m_ThaModulePdhMethods->De1InterruptEnable(self, enable);
    ret |= Tha60290022ModuleSdhV2De1InterruptEnable(ModuleSdh(self), enable);

    return ret;
    }

static void OverrideTha60290022ModulePdh(AtModulePdh self)
    {
    Tha60290022ModulePdh object = (Tha60290022ModulePdh)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290022ModulePdhMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290022ModulePdhOverride, m_Tha60290022ModulePdhMethods, sizeof(m_Tha60290022ModulePdhOverride));

        mMethodOverride(m_Tha60290022ModulePdhOverride, Af6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_Address);
        mMethodOverride(m_Tha60290022ModulePdhOverride, Af6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_Address);
        mMethodOverride(m_Tha60290022ModulePdhOverride, Af6Reg_dej1_rx_framer_per_chn_intr_or_stat_Address);
        }

    mMethodsSet(object, &m_Tha60290022ModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(pdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, ShouldReportBerViaBerController);
        mMethodOverride(m_ThaModulePdhOverride, De3InterruptEnable);
        mMethodOverride(m_ThaModulePdhOverride, De1InterruptEnable);
        }

    mMethodsSet(pdhModule, &m_ThaModulePdhOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(self), sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, VcDe3Create);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideTha60290022ModulePdh(self);
    OverrideThaModulePdh(self);
    OverrideAtModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModulePdhV3);
    }

AtModulePdh Tha60290022ModulePdhV3ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60290022ModulePdhV3New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModulePdhV3ObjectInit(newModule, device);
    }

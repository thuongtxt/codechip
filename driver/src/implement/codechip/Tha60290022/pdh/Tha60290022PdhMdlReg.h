/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_PDH_MDL_H_
#define _AF6_REG_AF6CNC0022_RD_PDH_MDL_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL BUFFER1
Reg Addr   : 0x0000 - 0x025F
Reg Formula: 0x0000+ $SLICEID*0x1000 + $DWORDID*32 + $DE3ID1
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID1(0-31)  : DE3 ID1
Reg Desc   : 
config message MDL BUFFER Channel ID 0-31

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_buffer1_Base                                                                   0x0000

/*--------------------------------------
BitField Name: idle_byte13
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_idle_byte13_Mask                                                       cBit31_24
#define cAf6_upen_mdl_buffer1_idle_byte13_Shift                                                             24

/*--------------------------------------
BitField Name: ilde_byte12
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_ilde_byte12_Mask                                                       cBit23_16
#define cAf6_upen_mdl_buffer1_ilde_byte12_Shift                                                             16

/*--------------------------------------
BitField Name: idle_byte11
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_idle_byte11_Mask                                                        cBit15_8
#define cAf6_upen_mdl_buffer1_idle_byte11_Shift                                                              8

/*--------------------------------------
BitField Name: idle_byte10
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_idle_byte10_Mask                                                         cBit7_0
#define cAf6_upen_mdl_buffer1_idle_byte10_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL BUFFER1_2
Reg Addr   : 0x0260 - 0x038F
Reg Formula: 0x0260+ $SLICEID*0x1000 + $DWORDID*16 + $DE3ID2
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID2 (0-15) : DE3 ID2
Reg Desc   : 
config message MDL BUFFER Channel ID 32-47

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_buffer1_2_Base                                                                 0x0260

/*--------------------------------------
BitField Name: idle_byte23
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_2_idle_byte23_Mask                                                     cBit31_24
#define cAf6_upen_mdl_buffer1_2_idle_byte23_Shift                                                           24

/*--------------------------------------
BitField Name: idle_byte22
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_2_idle_byte22_Mask                                                     cBit23_16
#define cAf6_upen_mdl_buffer1_2_idle_byte22_Shift                                                           16

/*--------------------------------------
BitField Name: idle_byte21
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_2_idle_byte21_Mask                                                      cBit15_8
#define cAf6_upen_mdl_buffer1_2_idle_byte21_Shift                                                            8

/*--------------------------------------
BitField Name: idle_byte20
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_2_idle_byte20_Mask                                                       cBit7_0
#define cAf6_upen_mdl_buffer1_2_idle_byte20_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message MDL BUFFER2
Reg Addr   : 0x0400 - 0x065F
Reg Formula: 0x0400+$SLICEID*0x1000 + $DE3ID1 + $DWORDID*16
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID1(0-31)  : DS3 E3 ID
Reg Desc   : 
config message MDL BUFFER2 Channel ID 0-15

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_tp1_Base                                                                       0x0400

/*--------------------------------------
BitField Name: tp_byte13
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_tp_byte13_Mask                                                             cBit31_24
#define cAf6_upen_mdl_tp1_tp_byte13_Shift                                                                   24

/*--------------------------------------
BitField Name: tp_byte12
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_tp_byte12_Mask                                                             cBit23_16
#define cAf6_upen_mdl_tp1_tp_byte12_Shift                                                                   16

/*--------------------------------------
BitField Name: tp_byte11
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_tp_byte11_Mask                                                              cBit15_8
#define cAf6_upen_mdl_tp1_tp_byte11_Shift                                                                    8

/*--------------------------------------
BitField Name: tp_byte10
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_tp_byte10_Mask                                                               cBit7_0
#define cAf6_upen_mdl_tp1_tp_byte10_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message MDL BUFFER2_2
Reg Addr   : 0x0660 - 0x078F
Reg Formula: 0x0660+$SLICEID*0x1000 + $DWORDID*8+ $DE3ID2
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID2 (0-15) : DE3 ID2
Reg Desc   : 
config message MDL BUFFER2 Channel ID 16-23

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_tp2_Base                                                                       0x0660

/*--------------------------------------
BitField Name: tp_byte23
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_tp_byte23_Mask                                                             cBit31_24
#define cAf6_upen_mdl_tp2_tp_byte23_Shift                                                                   24

/*--------------------------------------
BitField Name: tp_byte22
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_tp_byte22_Mask                                                             cBit23_16
#define cAf6_upen_mdl_tp2_tp_byte22_Shift                                                                   16

/*--------------------------------------
BitField Name: tp_byte21
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_tp_byte21_Mask                                                              cBit15_8
#define cAf6_upen_mdl_tp2_tp_byte21_Shift                                                                    8

/*--------------------------------------
BitField Name: tp_byte20
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_tp_byte20_Mask                                                               cBit7_0
#define cAf6_upen_mdl_tp2_tp_byte20_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : SET TO HAVE PACKET MDL BUFFER 1
Reg Addr   : 0x0840 - 0x86F
Reg Formula: 0x0840+$SLICEID*0x1000 +$DE3ID
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DE3ID (0-47) : DE3 ID
Reg Desc   : 
SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sta_idle_alren_Base                                                                0x0840
#define cAf6Reg_upen_sta_idle_alren_WidthVal                                                                32

/*--------------------------------------
BitField Name: idle_cfgen
BitField Type: R/W
BitField Desc: (0) : engine clear for indication to have sent, (1) CPU set for
indication to have new message which must send for buffer 0
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_sta_idle_alren_idle_cfgen_Mask                                                         cBit0
#define cAf6_upen_sta_idle_alren_idle_cfgen_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : SET TO HAVE PACKET MDL BUFFER 2
Reg Addr   : 0x0880 - 0x8AF
Reg Formula: 0x0880+$SLICEID*0x1000 +$DE3ID
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DE3ID (0-47) : DE3 ID
Reg Desc   : 
SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sta_tp_alren_Base                                                                  0x0880
#define cAf6Reg_upen_sta_tp_alren_WidthVal                                                                  32

/*--------------------------------------
BitField Name: tp_cfgen
BitField Type: R/W
BitField Desc: (0) : engine clear for indication to have sent, (1) CPU set for
indication to have new message which must send for buffer 1
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_sta_tp_alren_tp_cfgen_Mask                                                             cBit0
#define cAf6_upen_sta_tp_alren_tp_cfgen_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG MDL Tx
Reg Addr   : 0x08C0 - 0x8EF
Reg Formula: 0x08C0+$SLICEID*0x1000 +$DE3ID
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DE3ID (0-47) : DE3 ID
Reg Desc   : 
Config Tx MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_mdl_Base                                                                       0x08C0
#define cAf6Reg_upen_cfg_mdl_WidthVal                                                                       32

/*--------------------------------------
BitField Name: cfg_seq_tx
BitField Type: R/W
BitField Desc: config enable Tx continous, (0) is disable, (1) is enable
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_seq_tx_Mask                                                                cBit4
#define cAf6_upen_cfg_mdl_cfg_seq_tx_Shift                                                                   4

/*--------------------------------------
BitField Name: cfg_entx
BitField Type: R/W
BitField Desc: config enable Tx, (0) is disable, (1) is enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_entx_Mask                                                                  cBit3
#define cAf6_upen_cfg_mdl_cfg_entx_Shift                                                                     3

/*--------------------------------------
BitField Name: cfg_fcs_tx
BitField Type: R/W
BitField Desc: config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_fcs_tx_Mask                                                                cBit2
#define cAf6_upen_cfg_mdl_cfg_fcs_tx_Shift                                                                   2

/*--------------------------------------
BitField Name: cfg_mdlstd_tx
BitField Type: R/W
BitField Desc: config standard Tx, (0) is ANSI, (1) is AT&T
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_mdlstd_tx_Mask                                                             cBit1
#define cAf6_upen_cfg_mdl_cfg_mdlstd_tx_Shift                                                                1

/*--------------------------------------
BitField Name: cfg_mdl_cr
BitField Type: R/W
BitField Desc: config bit command/respond MDL message, bit 0 is channel 0 of DS3
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_mdl_cr_Mask                                                                cBit0
#define cAf6_upen_cfg_mdl_cfg_mdl_cr_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL IDLE
Reg Addr   : 0x0C00 - 0xD2F
Reg Formula: 0x0C00+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256
    Where  : 
           + $DE3ID (0-47) : DE3 ID
           + $UPRO (0-1) : UP READ ONLY
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
counter byte read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_byteidle1_Base                                                        0x0E00

/*--------------------------------------
BitField Name: cntr2c_byte_idle_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_byteidle1_cntr2c_byte_idle_mdl_Mask                                    cBit31_0
#define cAf6_upen_txmdl_cntr2c_byteidle1_cntr2c_byte_idle_mdl_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL PATH
Reg Addr   : 0x0C40 - 0x0D6F
Reg Formula: 0x0C40+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256
    Where  : 
           + $DE3ID (0-47) : DE3 ID
           + $UPRO (0-1) : UP READ ONLY
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
counter byte read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_bytepath_Base                                                         0x0E40

/*--------------------------------------
BitField Name: cntr2c_byte_path_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Mask                                     cBit31_0
#define cAf6_upen_txmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL TEST
Reg Addr   : 0x0C80 - 0xDAF
Reg Formula: 0x0C80+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256
    Where  : 
           + $DE3ID (0-47) : DE3 ID
           + $UPRO (0-1) : UP READ ONLY
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
counter byte read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_bytetest_Base                                                         0x0E80

/*--------------------------------------
BitField Name: cntr2c_byte_test_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Mask                                     cBit31_0
#define cAf6_upen_txmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL IDLE
Reg Addr   : 0x0E00 - 0x0F2F
Reg Formula: 0x0E00+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256
    Where  : 
           + $DE3ID (0-47) : DE3 ID
           + $UPRO (0-1) : UP READ ONLY
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
counter valid read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_valididle_Base                                                        0x0C00
#define cAf6Reg_upen_txmdl_cntr2c_valididle_WidthVal                                                        32

/*--------------------------------------
BitField Name: cntr2c_valid_idle_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_valididle_cntr2c_valid_idle_mdl_Mask                                   cBit17_0
#define cAf6_upen_txmdl_cntr2c_valididle_cntr2c_valid_idle_mdl_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL PATH
Reg Addr   : 0x0E40 - 0x0F6F
Reg Formula: 0x0E40+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256
    Where  : 
           + $DE3ID (0-47) : DE3 ID
           + $UPRO (0-1) : UP READ ONLY
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
counter valid read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_validpath_Base                                                        0x0C40

/*--------------------------------------
BitField Name: cntr2c_valid_path_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_validpath_cntr2c_valid_path_mdl_Mask                                   cBit17_0
#define cAf6_upen_txmdl_cntr2c_validpath_cntr2c_valid_path_mdl_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL TEST R2C
Reg Addr   : 0x0E80 - 0x0FAF
Reg Formula: 0x0E80+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256
    Where  : 
           + $DE3ID (0-47) : DE3 ID
           + $UPRO (0-1) : UP READ ONLY
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
counter valid read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_validtest_Base                                                        0x0C80

/*--------------------------------------
BitField Name: cntr2c_valid_test_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_validtest_cntr2c_valid_test_mdl_Mask                                   cBit17_0
#define cAf6_upen_txmdl_cntr2c_validtest_cntr2c_valid_test_mdl_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL with configuration type
Reg Addr   : 0x44000 - 0x47FFF
Reg Formula: 0x44000 + $STSID* 256 + $SLICEID*32 + $RXDWORDID
    Where  : 
           + $STSID (0-47) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $RXDWORDID (0-19) : RX Double WORD ID
Reg Desc   : 
buffer message MDL which is configured type

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_typebuff_Base                                                               0x44000

/*--------------------------------------
BitField Name: mdl_tbyte3
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte3_Mask                                                     cBit31_24
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte3_Shift                                                           24

/*--------------------------------------
BitField Name: mdl_tbyte2
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte2_Mask                                                     cBit23_16
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte2_Shift                                                           16

/*--------------------------------------
BitField Name: mdl_tbyte1
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte1_Mask                                                      cBit15_8
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte1_Shift                                                            8

/*--------------------------------------
BitField Name: mdl_tbyte0
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte0_Mask                                                       cBit7_0
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte0_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message MDL TYPE
Reg Addr   : 0x4A800 - 0x4A97F
Reg Formula: 0x4A800+ $STSID*4 + $SLICEID
    Where  : 
           + $STSID (0-47) : STS ID
           + $SLICEID (0-7) : SLICE ID
Reg Desc   : 
config type message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cfgtype_Base                                                                0x4A800
#define cAf6Reg_upen_rxmdl_cfgtype_WidthVal                                                                 32

/*--------------------------------------
BitField Name: cfg_fcs_rx
BitField Type: R/W
BitField Desc: config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_fcs_rx_Mask                                                          cBit5
#define cAf6_upen_rxmdl_cfgtype_cfg_fcs_rx_Shift                                                             5

/*--------------------------------------
BitField Name: cfg_mdl_cr
BitField Type: R/W
BitField Desc: config C/R bit expected
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_cr_Mask                                                          cBit4
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_cr_Shift                                                             4

/*--------------------------------------
BitField Name: cfg_mdl_std
BitField Type: R/W
BitField Desc: (0) is ANSI, (1) is AT&T
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_Mask                                                         cBit3
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_Shift                                                            3

/*--------------------------------------
BitField Name: cfg_mdl_mask_test
BitField Type: R/W
BitField Desc: config enable mask to moitor test massage (1): enable, (0):
disable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_test_Mask                                                   cBit2
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_test_Shift                                                      2

/*--------------------------------------
BitField Name: cfg_mdl_mask_path
BitField Type: R/W
BitField Desc: config enable mask to moitor path massage (1): enable, (0):
disable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_path_Mask                                                   cBit1
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_path_Shift                                                      1

/*--------------------------------------
BitField Name: cfg_mdl_mask_idle
BitField Type: R/W
BitField Desc: config enable mask to moitor idle massage (1): enable, (0):
disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_idle_Mask                                                   cBit0
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_idle_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL DESTUFF 0
Reg Addr   : 0x4AA00
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_destuff_ctrl0_Base                                                                0x4AA00
#define cAf6Reg_upen_destuff_ctrl0_WidthVal                                                                 32

/*--------------------------------------
BitField Name: cfg_crmon
BitField Type: R/W
BitField Desc: (0) : disable, (1) : enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Mask                                                           cBit5
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Shift                                                              5

/*--------------------------------------
BitField Name: reserve1
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_reserve1_Mask                                                            cBit4
#define cAf6_upen_destuff_ctrl0_reserve1_Shift                                                               4

/*--------------------------------------
BitField Name: fcsmon
BitField Type: R/W
BitField Desc: (0) : disable monitor, (1) : enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_fcsmon_Mask                                                              cBit3
#define cAf6_upen_destuff_ctrl0_fcsmon_Shift                                                                 3

/*--------------------------------------
BitField Name: headermon_en
BitField Type: R/W
BitField Desc: (1) : enable monitor, (0) disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_headermon_en_Mask                                                        cBit0
#define cAf6_upen_destuff_ctrl0_headermon_en_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF CONFIG1
Reg Addr   : 0x4AA10
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg1_Base                                                                 0x4AA10

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 0-31
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg1_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg1_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 2
Reg Addr   : 0x4AA11
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg2_Base                                                                 0x4AA11

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 32 -63
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg2_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg2_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 3
Reg Addr   : 0x4AA12
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg3_Base                                                                 0x4AA12

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 64 -95
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg3_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg3_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 4
Reg Addr   : 0x4AA13
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg4_Base                                                                 0x4AA13

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 96 -127
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg4_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg4_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 5
Reg Addr   : 0x4AA14
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg5_Base                                                                 0x4AA14

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 128 -159
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg5_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg5_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF CONFIG 6
Reg Addr   : 0x4AA15
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg6_Base                                                                 0x4AA15

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 160 -191
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg6_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg6_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF CONFIG7
Reg Addr   : 0x4AA16
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg7_Base                                                                 0x4AA16

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 192-223
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg7_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg7_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 8
Reg Addr   : 0x4AA17
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg8_Base                                                                 0x4AA17

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 224 -255
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg8_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg8_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 9
Reg Addr   : 0x4AA18
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg9_Base                                                                 0x4AA18

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 256 -287
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg9_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg9_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 10
Reg Addr   : 0x4AA19
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg10_Base                                                                0x4AA19

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 288 -319
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg10_mdl_cfg_alr_Mask                                                      cBit31_0
#define cAf6_upen_mdl_stk_cfg10_mdl_cfg_alr_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 11
Reg Addr   : 0x4AA1A
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg11_Base                                                                0x4AA1A

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 320 -351
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg11_mdl_cfg_alr_Mask                                                      cBit31_0
#define cAf6_upen_mdl_stk_cfg11_mdl_cfg_alr_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF CONFIG 12
Reg Addr   : 0x4AA1B
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg12_Base                                                                0x4AA1B

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 352 -383
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg12_mdl_cfg_alr_Mask                                                      cBit31_0
#define cAf6_upen_mdl_stk_cfg12_mdl_cfg_alr_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX MDL IDLE
Reg Addr   : 0x4D000 - 0x4D97F
Reg Formula: 0x4D000+ $STSID*8 + $SLICEID + $UPRO * 2048
    Where  : 
           + $STSID (0-47) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter byte read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_byteidle_Base                                                        0x4D000

/*--------------------------------------
BitField Name: cntr2c_byte_idle_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_byteidle_cntr2c_byte_idle_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_byteidle_cntr2c_byte_idle_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX MDL PATH
Reg Addr   : 0x4D200 - 0x4DB7F
Reg Formula: 0x4D200+ $STSID*8 + $SLICEID + $UPRO * 2048
    Where  : 
           + $STSID (0-47) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter byte read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_bytepath_Base                                                        0x4D200

/*--------------------------------------
BitField Name: cntr2c_byte_path_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX MDL TEST
Reg Addr   : 0x4D400 - 0x4DF7F
Reg Formula: 0x4D400+ $STSID*8 + $SLICEID + $UPRO * 2048
    Where  : 
           + $STSID (0-47) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter byte read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_bytetest_Base                                                        0x4D400

/*--------------------------------------
BitField Name: cntr2c_byte_test_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX MDL IDLE
Reg Addr   : 0x4E000 - 0x4F17F
Reg Formula: 0x4E000+ $STSID*8 + $SLICEID + $UPRO * 4096
    Where  : 
           + $STSID (0-47) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter good read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_goodidle_Base                                                        0x4E000

/*--------------------------------------
BitField Name: cntr2c_good_idle_mdl
BitField Type: R/W
BitField Desc: value counter good
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_goodidle_cntr2c_good_idle_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_goodidle_cntr2c_good_idle_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX MDL PATH
Reg Addr   : 0x4E200 - 0x4F37F
Reg Formula: 0x4E200+ $STSID*8 + $SLICEID + $UPRO * 4096
    Where  : 
           + $STSID (0-47) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter good read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_goodpath_Base                                                        0x4E200

/*--------------------------------------
BitField Name: cntr2c_good_path_mdl
BitField Type: R/W
BitField Desc: value counter good
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_goodpath_cntr2c_good_path_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_goodpath_cntr2c_good_path_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX MDL TEST
Reg Addr   : 0x4E400 - 0x4F57F
Reg Formula: 0x4E400+ $STSID*8 + $SLICEID + $UPRO * 4096
    Where  : 
           + $STSID (0-47) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter good read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_goodtest_Base                                                        0x4E400

/*--------------------------------------
BitField Name: cntr2c_good_test_mdl
BitField Type: R/W
BitField Desc: value counter good
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_goodtest_cntr2c_good_test_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_goodtest_cntr2c_good_test_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX MDL IDLE
Reg Addr   : 0x4E600 - 0x4F77F
Reg Formula: 0x4E600+ $STSID*8 + $SLICEID + $UPRO * 4096
    Where  : 
           + $STSID (0-47) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter drop read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_dropidle_Base                                                        0x4E600

/*--------------------------------------
BitField Name: cntr2c_drop_idle_mdl
BitField Type: R/W
BitField Desc: value counter drop
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_dropidle_cntr2c_drop_idle_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_dropidle_cntr2c_drop_idle_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX MDL PATH
Reg Addr   : 0x4E800 - 0x4F97F
Reg Formula: 0x4E800+ $STSID*8 + $SLICEID + $UPRO * 4096
    Where  : 
           + $STSID (0-47) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter drop read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_droppath_Base                                                        0x4E800

/*--------------------------------------
BitField Name: cntr2c_drop_path_mdl
BitField Type: R/W
BitField Desc: value counter drop
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_droppath_cntr2c_drop_path_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_droppath_cntr2c_drop_path_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX MDL TEST R2C
Reg Addr   : 0x4EA00 - 0x4FBFF
Reg Formula: 0x4EA00+ $STSID*8 + $SLICEID + $UPRO * 4096
    Where  : 
           + $STSID (0-47) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter drop read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_droptest_Base                                                        0x4EA00

/*--------------------------------------
BitField Name: cntr2c_drop_test_mdl
BitField Type: R/W
BitField Desc: value counter drop
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_droptest_cntr2c_drop_test_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_droptest_cntr2c_drop_test_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : RX MDL INT STA
Reg Addr   : 0x4AA06
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_ndlsta_Base                                                                        0x4AA06
#define cAf6Reg_upen_int_ndlsta_WidthVal                                                                    32

/*--------------------------------------
BitField Name: mdllb_intsta2
BitField Type: R/W
BitField Desc: interrupt sta MDL 2
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_int_ndlsta_mdllb_intsta2_Mask                                                          cBit1
#define cAf6_upen_int_ndlsta_mdllb_intsta2_Shift                                                             1

/*--------------------------------------
BitField Name: mdllb_intsta1
BitField Type: R/W
BitField Desc: interrupt sta MDL 1
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_int_ndlsta_mdllb_intsta1_Mask                                                          cBit0
#define cAf6_upen_int_ndlsta_mdllb_intsta1_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : MDL per Channel Interrupt1 Enable Control
Reg Addr   : 0x4A000-0x4A0FF
Reg Formula: 0x4A000 +  $STSID1 + $SLICEID * 32
    Where  : 
           + $STSID1 (0-31) : STS1 ID
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
This is the per Channel interrupt enable

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_cfgen_int1_Base                                                                    0x4A000

/*--------------------------------------
BitField Name: mdl_cfgen_int1
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_mdl_cfgen_int1_mdl_cfgen_int1_Mask                                                          cBit0
#define cAf6_mdl_cfgen_int1_mdl_cfgen_int1_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt1 per Channel Interrupt Status
Reg Addr   : 0x4A200-0x4A2FF
Reg Formula: 0x4A200 +  $STSID1 + $SLICEID * 32
    Where  : 
           + $STSID1 (0-31) : STS1 ID
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
This is the per Channel interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_int1_sta_Base                                                                      0x4A200

/*--------------------------------------
BitField Name: mdlint1_sta
BitField Type: RW
BitField Desc: Set 1 if there is a change event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_mdl_int1_sta_mdlint1_sta_Mask                                                               cBit0
#define cAf6_mdl_int1_sta_mdlint1_sta_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt per Channel Current Status
Reg Addr   : 0x4A400-0x4A4FF
Reg Formula: 0x4A400 +  $STSID1 + $SLICEID * 32
    Where  : 
           + $STSID1 (0-31) : STS1 ID
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
This is the per Channel Current status.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_int1_crrsta_Base                                                                   0x4A400

/*--------------------------------------
BitField Name: mdlint1_crrsta
BitField Type: RW
BitField Desc: Current status of event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_mdl_int1_crrsta_mdlint1_crrsta_Mask                                                         cBit0
#define cAf6_mdl_int1_crrsta_mdlint1_crrsta_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt1 per Channel Interrupt OR Status
Reg Addr   : 0x4A600-0x4A607
Reg Formula: 0x4A600 +  $GID
    Where  : 
           + $GID (0-7) : group ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_int1sta_Base                                                                       0x4A600

/*--------------------------------------
BitField Name: mdlint1sta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding channel is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mdl_int1sta_mdlint1sta_Mask                                                              cBit31_0
#define cAf6_mdl_int1sta_mdlint1sta_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt1 OR Status
Reg Addr   : 0x4A6FF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits. Each bit is used to store Interrupt OR status.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_sta_int1_Base                                                                      0x4A6FF
#define cAf6Reg_mdl_sta_int1_WidthVal                                                                       32

/*--------------------------------------
BitField Name: mdlsta_int1
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit is set and its interrupt is
enabled
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_mdl_sta_int1_mdlsta_int1_Mask                                                             cBit7_0
#define cAf6_mdl_sta_int1_mdlsta_int1_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt1 Enable Control
Reg Addr   : 0x4A6FE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 interrupt enable bits .

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_en_int1_Base                                                                       0x4A6FE
#define cAf6Reg_mdl_en_int1_WidthVal                                                                        32

/*--------------------------------------
BitField Name: mdlen_int1
BitField Type: RW
BitField Desc: Set to 1 to enable to generate interrupt.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_mdl_en_int1_mdlen_int1_Mask                                                               cBit7_0
#define cAf6_mdl_en_int1_mdlen_int1_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : MDL per Channel Interrupt2 Enable Control
Reg Addr   : 0x4A100-0x4A1FF
Reg Formula: 0x4A100 +  $STSID2 + $SLICEID * 32
    Where  : 
           + $STSID2 (0-15) : STS2 ID
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
This is the per Channel interrupt enable

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_cfgen_int2_Base                                                                    0x4A100

/*--------------------------------------
BitField Name: mdl_cfgen_int2
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_mdl_cfgen_int2_mdl_cfgen_int2_Mask                                                          cBit0
#define cAf6_mdl_cfgen_int2_mdl_cfgen_int2_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt2 per Channel Interrupt Status
Reg Addr   : 0x4A300-0x4A3FF
Reg Formula: 0x4A300 +  $STSID2 + $SLICEID * 32
    Where  : 
           + $STSID2 (0-15) : STS2 ID
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
This is the per Channel interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_int2_sta_Base                                                                      0x4A300

/*--------------------------------------
BitField Name: mdlint2_sta
BitField Type: RW
BitField Desc: Set 1 if there is a change event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_mdl_int2_sta_mdlint2_sta_Mask                                                               cBit0
#define cAf6_mdl_int2_sta_mdlint2_sta_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt per Channel Current Status
Reg Addr   : 0x4A500-0x4A5FF
Reg Formula: 0x4A500 +  $STSID2 + $SLICEID * 32
    Where  : 
           + $STSID2 (0-15) : STS2 ID
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
This is the per Channel Current status.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_int2_crrsta_Base                                                                   0x4A500

/*--------------------------------------
BitField Name: mdlint2_crrsta
BitField Type: RW
BitField Desc: Current status of event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_mdl_int2_crrsta_mdlint2_crrsta_Mask                                                         cBit0
#define cAf6_mdl_int2_crrsta_mdlint2_crrsta_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt2 per Channel Interrupt OR Status
Reg Addr   : 0x4A700-0x4A707
Reg Formula: 0x4A700 +  $GID
    Where  : 
           + $GID (0-7) : group ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_int2sta_Base                                                                       0x4A700

/*--------------------------------------
BitField Name: mdlint2sta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding channel is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mdl_int2sta_mdlint2sta_Mask                                                              cBit31_0
#define cAf6_mdl_int2sta_mdlint2sta_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt2 OR Status
Reg Addr   : 0x4A7FF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits. Each bit is used to store Interrupt OR status.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_sta_int2_Base                                                                      0x4A7FF
#define cAf6Reg_mdl_sta_int2_WidthVal                                                                       32

/*--------------------------------------
BitField Name: mdlsta_int2
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit is set and its interrupt is
enabled
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_mdl_sta_int2_mdlsta_int2_Mask                                                             cBit7_0
#define cAf6_mdl_sta_int2_mdlsta_int2_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt2 Enable Control
Reg Addr   : 0x4A7FE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 interrupt enable bits .

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_en_int2_Base                                                                       0x4A7FE
#define cAf6Reg_mdl_en_int2_WidthVal                                                                        32

/*--------------------------------------
BitField Name: mdlen_int2
BitField Type: RW
BitField Desc: Set to 1 to enable to generate interrupt.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_mdl_en_int2_mdlen_int2_Mask                                                               cBit7_0
#define cAf6_mdl_en_int2_mdlen_int2_Shift                                                                    0

#endif /* _AF6_REG_AF6CNC0022_RD_PDH_MDL_H_ */

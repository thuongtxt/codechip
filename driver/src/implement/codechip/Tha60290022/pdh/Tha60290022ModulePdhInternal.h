/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290022ModulePdhInternal.h
 * 
 * Created Date: Feb 6, 2018
 *
 * Description : 60290022 module PDH internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPDHINTERNAL_H_
#define _THA60290022MODULEPDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/pdh/Tha60290021ModulePdhInternal.h"
#include "Tha60290022ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModulePdhMethods
    {
    uint32 (*Af6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_Address)(Tha60290022ModulePdh self, uint8 groupId);
    uint32 (*Af6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_Address)(Tha60290022ModulePdh self, uint8 groupId);
    uint32 (*Af6Reg_dej1_rx_framer_per_chn_intr_or_stat_Address)(Tha60290022ModulePdh self, uint8 groupId, uint8 de3Id);
    }tTha60290022ModulePdhMethods;

typedef struct tTha60290022ModulePdh
    {
    tTha60290021ModulePdh super;
    const tTha60290022ModulePdhMethods *methods;
    }tTha60290022ModulePdh;

typedef struct tTha60290022ModulePdhV3
    {
    tTha60290022ModulePdh super;
    }tTha60290022ModulePdhV3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh Tha60290022ModulePdhObjectInit(AtModulePdh self, AtDevice device);
AtModulePdh Tha60290022ModulePdhV3ObjectInit(AtModulePdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPDHINTERNAL_H_ */

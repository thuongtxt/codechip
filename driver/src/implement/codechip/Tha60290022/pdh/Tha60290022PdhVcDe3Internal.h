/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290022PdhVcDe3Internal.h
 * 
 * Created Date: Mar 15, 2018
 *
 * Description : Internal data of the DE3
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PDHVCDE3INTERNAL_H_
#define _THA60290022PDHVCDE3INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/pdh/Tha60290021PdhVcDe3Internal.h"
#include "Tha60290022ModulePdh.h"
#include "AtPdhDe3.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022PdhVcDe3
    {
    tTha60290021PdhVcDe3 super;
    }tTha60290022PdhVcDe3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe3 Tha60290022PdhVcDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PDHVCDE3INTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290022PdhUtil.h
 * 
 * Created Date: Apr 23, 2017
 *
 * Description : PDH util
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PDHUTIL_H_
#define _THA60290022PDHUTIL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaPdhDe1.h"
#include "../../../default/pdh/ThaModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60290022PdhDe1ErrCounterOffset(ThaPdhDe1 de1, eBool r2cEn);
eBool Tha602900xxModulePdhRegAddressExist(ThaModulePdh self, uint32 addressBase);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PDHUTIL_H_ */


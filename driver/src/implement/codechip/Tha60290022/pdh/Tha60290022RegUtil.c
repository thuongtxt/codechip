/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290022RegUtil.c
 *
 * Created Date: May 11, 2017
 *
 * Description : To manage PDH register
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022PdhUtil.h"
#include "../../Tha60210031/pdh/Tha60210031ModulePdhReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool Tha602900xxModulePdhRegAddressExist(ThaModulePdh self, uint32 addressBase)
    {
    AtUnused(self);

    if (addressBase == cAf6Reg_stsvt_demap_conc_ctrl_Base)
        return cAtFalse;

    return cAtTrue;
    }

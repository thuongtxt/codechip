/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290021PdhVcDe1Internal.h
 * 
 * Created Date: Apr 23, 2017
 *
 * Description : DE1
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PDHVCDE1INTERNAL_H_
#define _THA60290022PDHVCDE1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/pdh/Tha60290021PdhVcDe1Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022PdhVcDe1
    {
	tTha60290021PdhVcDe1 super;
    }tTha60290022PdhVcDe1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 Tha60290022PdhVcDe1ObjectInit(AtPdhDe1 self, AtSdhVc vc1x, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PDHVCDE1INTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290022PdhDe2De1.c
 *
 * Created Date: Apr 23, 2017
 *
 * Description : DE1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../Tha60290021/pdh/Tha60290021PdhDe2De1Internal.h"
#include "Tha60290022PdhUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PdhDe2De1
    {
    tTha60290021PdhDe2De1 super;
    }tTha60290022PdhDe2De1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPdhDe1Methods m_ThaPdhDe1Override;

/* Save super implementation */
static const tThaPdhDe1Methods * m_ThaPdhDe1Methods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 De1ErrCounterOffset(ThaPdhDe1 de1, eBool r2cEn)
    {
    return Tha60290022PdhDe1ErrCounterOffset(de1, r2cEn);
    }

static void OverrideThaPdhDe1(AtPdhDe1 self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhDe1Methods = mMethodsGet(de1);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe1Override, mMethodsGet(de1), sizeof(m_ThaPdhDe1Override));

        mMethodOverride(m_ThaPdhDe1Override, De1ErrCounterOffset);
        }

    mMethodsSet(de1, &m_ThaPdhDe1Override);
    }

static void Override(AtPdhDe1 self)
    {
    OverrideThaPdhDe1(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PdhDe2De1);
    }

static AtPdhDe1 ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60290021PdhDe2De1ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe1 Tha60290022PdhDe2De1New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newDe1 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe1 == NULL)
        return NULL;

    /* construct it */
    return ObjectInit(newDe1, channelId, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290022ModulePdh.c
 *
 * Created Date: Apr 16, 2017
 *
 * Description : PDH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/pdh/prm/ThaPdhPrmInterruptManager.h"
#include "../../../default/pdh/mdl/ThaPdhMdlInterruptManager.h"
#include "../../../default/pdh/ThaPdhMdlController.h"
#include "../../Tha60210011/man/Tha60210011Device.h"
#include "../../Tha60210031/pdh/Tha60210031PdhDe1.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../man/Tha60290022Device.h"
#include "prm/Tha60290022PdhPrmController.h"
#include "prm/Tha60290022PdhPrmAnalyzer.h"
#include "Tha60290022ModulePdhReg.h"
#include "Tha60290022PdhMdlReg.h"
#include "Tha60290022PdhPrmReg.h"
#include "Tha60290022ModulePdhInternal.h"
#include "Tha60290022PdhUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cIteratorMask(_bit) (1U << (_bit))
#define cAf6Reg_global_pdh_interrupt_status    0x000004
#define cSliceOffset 0x1000

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290022ModulePdh)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290022ModulePdhMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tAtModulePdhMethods          m_AtModulePdhOverride;
static tThaModulePdhMethods         m_ThaModulePdhOverride;
static tThaStmModulePdhMethods      m_ThaStmModulePdhOverride;
static tTha60210011ModulePdhMethods m_Tha60210011ModulePdhOverride;
static tTha60210031ModulePdhMethods m_Tha60210031ModulePdhOverride;

/* Save super implementation */
static const tAtModuleMethods             *m_AtModuleMethods             = NULL;
static const tThaModulePdhMethods         *m_ThaModulePdhMethods         = NULL;
static const tTha60210031ModulePdhMethods *m_Tha60210031ModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumStsInSlice(ThaModulePdh self)
    {
    AtUnused(self);
    return 48;
    }

static uint32 RxDe3OhThrshCfgBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_rxde3_oh_thrsh_cfg_Base;
    }

static void HwFlushPdhVt(AtModulePdh self, uint32 sliceOffset, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint32 vtOffset = (uint32)((32UL * stsId) + (4UL * vtgId) + vtId + sliceOffset);

    mModuleHwWrite(self, cAf6Reg_stsvt_demap_ctrl_Base + vtOffset, 0);
    mModuleHwWrite(self, cAf6Reg_stsvt_map_ctrl_Base + vtOffset, 0);
    mModuleHwWrite(self, cAf6Reg_dej1_rx_framer_ctrl_Base + vtOffset, 0);
    mModuleHwWrite(self, cAf6Reg_dej1_tx_framer_ctrl_Base + vtOffset, 0);
    mModuleHwWrite(self, cAf6Reg_stsvt_map_hw_stat_Base + vtOffset, 0);
    mModuleHwWrite(self, cAf6Reg_dej1_rx_framer_per_chn_intr_en_ctrl_Base + vtOffset, 0);
    }

static void HwFlushPdhVtg(AtModulePdh self, uint32 sliceOffset, uint8 sts_i, uint8 vtgId)
    {
    uint8 vt_i;
    uint32 vtgOffset = (8UL * sts_i) + vtgId + sliceOffset;

    mModuleHwWrite(self, cAf6Reg_rxm12e12_ctrl_Base + vtgOffset, 0);
    mModuleHwWrite(self, cAf6Reg_txm12e12_ctrl_Base + vtgOffset, 0);

    for (vt_i = 0; vt_i < 4; vt_i++)
        HwFlushPdhVt(self, sliceOffset, sts_i, vtgId, vt_i);
    }

static void HwFlushPdhSts(AtModulePdh self, uint32 sliceOffset, uint8 stsId)
    {
    uint8 vtg_i;
    uint32 stsOffset = sliceOffset + stsId;

    mModuleHwWrite(self, cAf6Reg_rx_stsvc_payload_ctrl_Base + stsOffset, 0);
    mModuleHwWrite(self, cAf6Reg_rxm23e23_ctrl_Base + stsOffset, 0);
    mModuleHwWrite(self, cAf6Reg_txm23e23_ctrl_Base + stsOffset, 0);
    mModuleHwWrite(self, cAf6Reg_rxm23e23_per_chn_intr_cfg_Base + stsOffset, 0);
    mModuleHwWrite(self, cAf6Reg_RxM12E12_per_chn_intr_cfg_Base + stsOffset, 0);
    for (vtg_i = 0; vtg_i < 7; vtg_i++)
        HwFlushPdhVtg(self, sliceOffset, stsId, vtg_i);
    }

static void HwFlushPdhSlice(AtModulePdh self, uint8 slice)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    uint32 baseAddress = ThaModulePdhBaseAddress(pdhModule);
    uint32 offset = 0x100000UL * slice + baseAddress;
    uint8 sts_i;
    uint8 numSts = ThaModulePdhNumStsInSlice(pdhModule);

    mModuleHwWrite(self, cAf6Reg_dej1_errins_en_cfg_Base  + offset, 0);
    mModuleHwWrite(self, cAf6Reg_dej1_errins_thr_cfg_Base + offset, 0);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        HwFlushPdhSts(self, offset, sts_i);
    }

static uint32 RxPerStsVCPldCtrlBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_rx_stsvc_payload_ctrl_Base;
    }

static uint32 DS3E3RxFrmrperChnIntrEnCtrlBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_rxm23e23_per_chn_intr_cfg_Base;
    }

static uint32 RxM12E12MuxBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_rxm12e12_stsvc_payload_ctrl_Base;
    }

static uint32 RxM12E12FrmChnIntrEnCtrlBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_RxM12E12_per_chn_intr_cfg_Base;
    }

static uint32 RxM12E12FrmChnIntrChngStatusBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_RxM12E12_per_chn_intr_stat_Base;
    }

static uint32 RxM12E12FrmChnAlarmCurStatusBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_RxM12E12_per_chn_curr_stat_Base;
    }

static uint32 DS1E1J1RxFrmrCrcErrCntBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_crcerr_cnt_Base;
    }

static uint32 DS1E1J1RxFrmrReiCntBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_rei_cnt_Base;
    }

static uint32 DS1E1J1RxFrmrFBECntBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_fbe_cnt_Base;
    }

static uint32 DS1E1J1RxFrmrperChnIntrEnCtrlBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_per_chn_intr_en_ctrl_Base;
    }

static uint32 De1RxFrmrPerChnCurrentAlmBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_per_chn_curr_stat_Base;
    }

static uint32 De1RxFrmrPerChnInterruptAlmBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_per_chn_intr_stat_Base;
    }

static uint32 RxM23E23FrmrReiCntBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_rxm23e23_rei_cnt_Base;
    }

static uint32 RxM23E23FrmrFBECntBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_rxm23e23_fbe_cnt_Base;
    }

static uint32 RxM23E23FrmrParCntBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_rxm23e23_parity_cnt_Base;
    }

static uint32 RxM23E23FrmrCbitParCntBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_rxm23e23_cbit_parity_cnt_Base;
    }

static uint32 RxDs3E3OHProCtrlBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_rx_de3_oh_ctrl_Base;
    }

static uint32 RxDe3FeacBufferBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_rx_de3_feac_buffer_Base;
    }

static uint32 DS3E3RxFrmrperChnIntrStatCtrlBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_rxm23e23_per_chn_intr_stat_Base;
    }

static uint32 DS3E3RxFrmrperChnCurStatCtrlBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_rxm23e23_per_chn_curr_stat_Base;
    }

static uint32 De1RxFramerHwStatusBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_hw_stat_Base;
    }

static AtPdhDe1 VcDe1Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    return Tha60290022PdhVcDe1New((AtSdhVc)sdhVc, self);
    }

static AtPdhDe1 De2De1Create(AtModulePdh self, AtPdhDe2 de2, uint32 de1Id)
    {
    AtUnused(de2);
    return Tha60290022PdhDe2De1New(de1Id, self);
    }

static eBool RegExist(ThaModulePdh self, uint32 addressBase)
    {
    return Tha602900xxModulePdhRegAddressExist(self, addressBase);
    }

static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
    return Tha60290022DevicePdhStartVersionSupportPrm(AtModuleDeviceGet((AtModule)self));
    }

static eAtRet StsVtDemapVc4FractionalVc3Set(ThaStmModulePdh self, AtSdhChannel sdhVc, eBool isVc4FractionalVc3)
    {
    /* This product does not have this configuration as registers are removed */
    AtUnused(self);
    AtUnused(sdhVc);
    AtUnused(isVc4FractionalVc3);
    return cAtOk;
    }

static uint32 Af6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_Address(Tha60290022ModulePdh self, uint8 groupId)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat(groupId);
    }

static uint32 Af6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_Address(Tha60290022ModulePdh self, uint8 groupId)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl(groupId);
    }

static uint32 Af6Reg_dej1_rx_framer_per_chn_intr_or_stat_Address(Tha60290022ModulePdh self, uint8 groupId, uint8 de3Id)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat(groupId, de3Id);
    }

static void De3InterruptProcess(ThaModulePdh self, uint32 de3SliceIntr, AtHal hal)
    {
    uint8 numStsPerSlice = ThaModulePdhNumStsInSlice(self);
    uint8 numSlices = ThaModulePdhNumSlices(self);
    uint8 slice;

    for (slice = 0; slice < numSlices; slice++)
        {
        if (de3SliceIntr & cIteratorMask(slice))
            {
            /* Each slice is divided into 2 groups in which the first comprises
             * of 32 DS3/E3s, the second remains 16 DS3/E3s */
            uint32 baseAddress = ThaModulePdhSliceBase(self, slice);
            uint32 sliceOffset = (uint32)ThaModulePdhSliceOffset(self, slice);
            uint32 groupIntr   = AtHalRead(hal, (baseAddress + cAf6Reg_RxM23E23_Framer_per_grp_intr_or_stat_Base)); /* Group interrupt status */
            uint32 groupIntrEn = AtHalRead(hal, (baseAddress + cAf6Reg_RxM23E23_Framer_per_Group_intr_en_ctrl_Base)); /* Group interrupt mask */
            uint8 groupId;
            uint8 de3 = 0;

            groupIntr &= groupIntrEn;
            for (groupId = 0; groupId < 2; groupId++)
                {
                if (groupIntr & cIteratorMask(groupId))
                    {
                    uint8 de3_i;
                    uint32 de3Intr = AtHalRead(hal, (baseAddress + cAf6Reg_RxM23E23_Framer_per_chn_intr_or_stat(groupId)));

                    for (de3_i = 0, de3 = (uint8)(groupId * 32); (de3_i < 32) && (de3 < numStsPerSlice); de3_i++, de3++)
                        {
                        if (de3Intr & cIteratorMask(de3_i))
                            {
                            AtPdhDe3 de3Channel = ThaModulePdhDe3ChannelFromHwIdGet(self, cAtModulePdh, slice, de3);
                            uint32 offset = sliceOffset + de3;

                            AtChannelInterruptProcess((AtChannel)de3Channel, offset);
                            }
                        }
                    }
                }
            }
        }
    }

static void De1InterruptProcess(ThaModulePdh self, uint32 de1SliceIntr, AtHal hal)
    {
    uint8 numStsPerSlice = ThaModulePdhNumStsInSlice(self);
    uint8 numSlices = ThaModulePdhNumSlices(self);
    uint8 slice;

    for (slice = 0; slice < numSlices; slice++)
        {
        if (de1SliceIntr & cIteratorMask(slice))
            {
            /* Each slice is divided into 2 groups in which the first comprises
             * of 32 DS3/E3s, the second remains 16 DS3/E3s */
            uint32 baseAddress = ThaModulePdhSliceBase(self, slice);
            uint32 sliceOffset = (uint32)ThaModulePdhSliceOffset(self, slice);
            uint32 groupIntr = AtHalRead(hal, (baseAddress + cAf6Reg_dej1_rx_framer_per_grp_intr_or_stat_Base));
            uint8 groupId;
            uint8 de3 = 0;

            for (groupId = 0; groupId < 2; groupId++)
                {
                uint32 de3Intr;
                uint32 de3IntrEn;
                uint8 de3_i;
                uint32 addressOffset;

                if (~groupIntr & cIteratorMask(groupId))
                    continue;

                addressOffset = mMethodsGet(mThis(self))->Af6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_Address(mThis(self), groupId);
                de3Intr   = AtHalRead(hal, (baseAddress + addressOffset));
                addressOffset = mMethodsGet(mThis(self))->Af6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_Address(mThis(self), groupId);
                de3IntrEn = AtHalRead(hal, (baseAddress + addressOffset));
                de3Intr &= de3IntrEn;

                for (de3_i = 0, de3 = (uint8)(groupId * 32); (de3_i < 32) && (de3 < numStsPerSlice); de3_i++, de3++)
                    {
                    uint32 de1Intr, intrOrStatusAddressOffset;
                    uint8 de1_i;
                    uint8 de2, de1;

                    if (~de3Intr & cIteratorMask(de3_i))
                        continue;

                    intrOrStatusAddressOffset = mMethodsGet(mThis(self))->Af6Reg_dej1_rx_framer_per_chn_intr_or_stat_Address(mThis(self), groupId, de3_i);
                    de1Intr = AtHalRead(hal, (baseAddress + intrOrStatusAddressOffset));

                    for (de2 = 0, de1_i = 0; de2 < 7; de2++)
                        {
                        for (de1 = 0; de1 < 4; de1++, de1_i++)
                            {
                            if (de1Intr & cIteratorMask(de1_i))
                                {
                                AtPdhDe1 de1Channel = ThaModulePdhDe1ChannelFromHwIdGet(self, cAtModulePdh, slice, de3, de2, de1);
                                uint32 de1Offset = sliceOffset + (uint32)(de3 << 5) + de1_i;

                                AtChannelInterruptProcess((AtChannel)de1Channel, de1Offset);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

static eAtRet De3InterruptEnable(ThaModulePdh self, eBool enable)
    {
    uint8 sliceId, numSlices = ThaModulePdhNumSlices(self);
    uint32 regVal = (enable) ? cBit1_0 : 0;

    for (sliceId = 0; sliceId < numSlices; sliceId++)
        {
        uint32 sliceBase = (uint32)ThaModulePdhSliceBase(self, sliceId);
        mModuleHwWrite(self, sliceBase + cAf6Reg_RxM23E23_Framer_per_Group_intr_en_ctrl_Base, regVal);
        }

    return cAtOk;
    }

static eAtRet De1InterruptEnable(ThaModulePdh self, eBool enable)
    {
    static const uint32 cDe1IntrEnVal[2] = {cBit31_0, cBit15_0};
    uint8 sliceId, numSlices = ThaModulePdhNumSlices(self);

    for (sliceId = 0; sliceId < numSlices; sliceId++)
        {
        uint32 sliceBase = (uint32)ThaModulePdhSliceBase(self, sliceId);
        uint8 groupId;

        for (groupId = 0; groupId < mCount(cDe1IntrEnVal); groupId++)
            {
            uint32 regVal = (enable) ? cDe1IntrEnVal[groupId] : 0x0;
            uint32 addressOffset = mMethodsGet(mThis(self))->Af6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_Address(mThis(self), groupId);
            mModuleHwWrite(self, sliceBase + addressOffset, regVal);
            }
        }

    return cAtOk;
    }

static AtPdhDe3 VcDe3Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    return Tha60290022PdhVcDe3New(AtSdhChannelSts1Get(sdhVc), self);
    }

static ThaModuleSdh ModuleSdh(ThaModulePdh self)
    {
    return (ThaModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleSdh);
    }

static AtPdhDe3 De3ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)ModuleSdh(self);
    AtSdhLine terminatedLine;

    if (phyModule == cAtModuleSur)
        ThaModuleSdhHwSts24ToHwSts48Get(sdhModule, sliceId, de3Id, &sliceId, &de3Id);

    terminatedLine = Tha60290021ModuleSdhTerminatedLineGet((AtModuleSdh)sdhModule, sliceId);
    return Tha60210011PdhDe3FromSdhLineGet(terminatedLine, de3Id);
    }

static uint32 InterruptBaseAddress(ThaModulePdh self)
    {
    return Tha60210011DeviceInterruptBaseAddress((Tha60210011Device)AtModuleDeviceGet((AtModule)self));
    }

static uint32 De3InterruptStatusGet(ThaModulePdh self, uint32 glbIntr, AtHal hal)
    {
    uint32 intrStatus = AtHalRead(hal, InterruptBaseAddress(self) + cAf6Reg_global_pdh_interrupt_status);
    AtUnused(self);
    AtUnused(glbIntr);
    return (intrStatus >> 8) & cBit7_0;
    }

static uint32 De1InterruptStatusGet(ThaModulePdh self, uint32 glbIntr, AtHal hal)
    {
    uint32 intrStatus = AtHalRead(hal, InterruptBaseAddress(self) + cAf6Reg_global_pdh_interrupt_status);
    AtUnused(self);
    AtUnused(glbIntr);
    return intrStatus & cBit7_0;
    }

static eBool HasDataLinkRams(Tha60210011ModulePdh self)
    {
    /* TODO: Will open this when hardware does that */
    AtUnused(self);
    return cAtFalse;
    }

static ThaVersionReader VersionReader(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaDeviceVersionReader(device);
    }

static eBool PrmIsSupported(ThaModulePdh self)
    {
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    uint32 startVersion = mMethodsGet(self)->StartVersionSupportPrm(self);
    return (currentVersion >= startVersion) ? cAtTrue : cAtFalse;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);
    if ((mInRange(localAddress, 0x1000000, 0x10FFFFF)) ||
        (mInRange(localAddress, 0x1100000, 0x11FFFFF)) ||
        (mInRange(localAddress, 0x1200000, 0x12FFFFF)) ||
        (mInRange(localAddress, 0x1300000, 0x13FFFFF)) ||
        (mInRange(localAddress, 0x1400000, 0x14FFFFF)) ||
        (mInRange(localAddress, 0x1500000, 0x15FFFFF)) ||
        (mInRange(localAddress, 0x1600000, 0x16FFFFF)) ||
        (mInRange(localAddress, 0x1700000, 0x17FFFFF)))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 StartVersionSupportDe1LomfConsequentialAction(ThaModulePdh self)
    {
    return Tha60290022DevicePdhStartVersionSupportDe1LomfConsequentialAction(AtModuleDeviceGet((AtModule)self));
    }

static uint32 TxBufferDwordOffset(Tha60210031ModulePdh self, uint8 sliceId, uint8 hwIdInSlice)
    {
    AtUnused(self);
    AtUnused(sliceId);
    return (hwIdInSlice < 32) ? 32 : 16;
    }

static uint32 UpenMdlBuffer1Base(Tha60210031ModulePdh self, uint8 hwStsId)
    {
    AtUnused(self);
    if (hwStsId < 32)
        return cAf6Reg_upen_mdl_buffer1_Base;
    return cAf6Reg_upen_mdl_buffer1_2_Base;
    }

static uint32 UpenMdlBuffer1Offset(Tha60210031ModulePdh self, uint8 sliceId, uint8 hwIdInSlice)
    {
    AtUnused(self);
    return (hwIdInSlice % 32) + ((uint32)sliceId * cSliceOffset);
    }

static uint32 MdlSliceOffset(Tha60210031ModulePdh self, uint8 sliceId)
    {
    AtUnused(self);
    return (uint32)sliceId * cSliceOffset;
    }

static uint32 MdlTxInsCtrlBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_sta_idle_alren_Base;
    }

static uint32 UpenCfgMdlBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cfg_mdl_Base;
    }

static uint32 UpenRxmdlCfgtypeBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxmdl_cfgtype_Base;
    }

static uint32 UpenDestuffCtrl0Base(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_destuff_ctrl0_Base;
    }

static uint32 UpenCfgCtrl0Base(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cfg_ctrl0_Base;
    }

static uint32 MdlControllerSliceOffset(Tha60210031ModulePdh self, uint8 sliceId)
    {
    AtUnused(self);
    return sliceId * 64UL;
    }

static uint32 MdlRxBufBaseAddress(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxmdl_typebuff_Base;
    }

static uint32 MdlRxStickyBaseAddress(Tha60210031ModulePdh self, uint32 bufferId)
    {
    AtUnused(self);
    AtUnused(bufferId);
    return cAf6Reg_upen_mdl_stk_cfg1_Base;
    }

static uint32 MdlRxPktOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    uint32 offset = r2c ? 0 : 4096;

    AtUnused(self);

    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return cAf6Reg_upen_rxmdl_cntr2c_goodpath_Base + offset;
        case cAtPdhMdlControllerTypeIdleSignal:  return cAf6Reg_upen_rxmdl_cntr2c_goodidle_Base + offset;
        case cAtPdhMdlControllerTypeTestSignal:  return cAf6Reg_upen_rxmdl_cntr2c_goodtest_Base + offset;
        default:                                 return 0;
        }
    }

static uint32 MdlRxByteOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    uint32 offset = r2c ? 0 : 2048;

    AtUnused(self);

    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return cAf6Reg_upen_rxmdl_cntr2c_bytepath_Base + offset;
        case cAtPdhMdlControllerTypeIdleSignal:  return cAf6Reg_upen_rxmdl_cntr2c_byteidle_Base + offset;
        case cAtPdhMdlControllerTypeTestSignal:  return cAf6Reg_upen_rxmdl_cntr2c_bytetest_Base + offset;
        default:                                 return 0;
        }
    }

static uint32 MdlRxDropOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    uint32 offset = r2c ? 0 : 4096;

    AtUnused(self);

    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return cAf6Reg_upen_rxmdl_cntr2c_droppath_Base + offset;
        case cAtPdhMdlControllerTypeIdleSignal:  return cAf6Reg_upen_rxmdl_cntr2c_dropidle_Base + offset;
        case cAtPdhMdlControllerTypeTestSignal:  return cAf6Reg_upen_rxmdl_cntr2c_droptest_Base + offset;
        default:                                 return 0;
        }
    }

static uint32 StartVersionSupportMdlFilterMessageByType(ThaModulePdh self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MdlTxPktOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    uint32 offset = r2c ? 0 : 256;

    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return cAf6Reg_upen_txmdl_cntr2c_validpath_Base + offset;
        case cAtPdhMdlControllerTypeIdleSignal:  return cAf6Reg_upen_txmdl_cntr2c_valididle_Base + offset;
        case cAtPdhMdlControllerTypeTestSignal:  return cAf6Reg_upen_txmdl_cntr2c_validtest_Base + offset;
        default:                                 return 0;
        }
    }

static uint32 MdlTxByteOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    uint32 offset = r2c ? 0 : 256;

    AtUnused(self);

    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return cAf6Reg_upen_txmdl_cntr2c_bytepath_Base  + offset;
        case cAtPdhMdlControllerTypeIdleSignal:  return cAf6Reg_upen_txmdl_cntr2c_byteidle1_Base + offset;
        case cAtPdhMdlControllerTypeTestSignal:  return cAf6Reg_upen_txmdl_cntr2c_bytetest_Base  + offset;
        default:                                 return 0;
        }
    }

static uint32 MdlRxCounterOffset(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    uint8 sliceId, hwIdInSlice;
    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);
    return (hwIdInSlice * 8UL) + sliceId;
    }

static uint32 UpenRxmdlCfgtypeOffset(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    uint8 sliceId, hwIdInSlice;
    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);
    return (hwIdInSlice * 8UL) + sliceId;
    }

static uint32 StartVersionSupportDe1IdleCodeInsertion(ThaModulePdh self)
    {
    return Tha60290022DevicePdhStartVersionSupportDe1IdleCodeInsertion(AtModuleDeviceGet((AtModule)self));
    }

static uint32 UpenPrmTxcfgcrBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_prm_txcfgcr_Base;
    }

static uint32 De1PrmOffset(ThaModulePdh self, ThaPdhDe1 de1)
    {
    uint8 hwSts, hwSlice, tugOrDe2Id, tu1xOrDe1Id;
    uint32 offset;

    AtUnused(self);

    if (ThaPdhChannelHwIdGet((AtPdhChannel)de1, cAtModulePdh, &hwSlice, &hwSts) != cAtOk)
        return cBit31_0;

    tu1xOrDe1Id = Tha60210031Tu1xOrDe1IdGet(de1, &tugOrDe2Id);
    offset = hwSts * 256UL + tugOrDe2Id * 32UL + tu1xOrDe1Id * 8UL + hwSlice;
    return (offset + ThaModulePdhPrmBaseAddress((ThaModulePdh)AtChannelModuleGet((AtChannel)de1)));
    }

static uint32 UpenPrmTxcfglbBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_prm_txcfglb_Base;
    }

static uint32 LBOffset(Tha60210031ModulePdh self, ThaPdhDe1 de1)
    {
    return ThaModulePdhDe1PrmOffset((ThaModulePdh)self, de1);
    }

static AtPdhPrmController PrmControllerObjectCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return Tha60290022PdhPrmControllerNew(de1);
    }

static uint32 UpenRxprmCfgstdBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_cfgstd_Base;
    }

static AtPdhMdlController MdlControllerCreate(ThaModulePdh self, AtPdhDe3 de3, uint32 mdlType)
    {
    AtUnused(self);
    return Tha60290022PdhMdlControllerNew(de3, mdlType);
    }

static AtInterruptManager PrmInterruptManagerObjectCreate(ThaModulePdh self)
    {
    return Tha60290022PdhPrmInterruptManagerNew((AtModule)self);
    }

static AtInterruptManager MdlInterruptManagerObjectCreate(ThaModulePdh self)
    {
    return Tha60290022PdhMdlInterruptManagerNew((AtModule)self);
    }

static AtPktAnalyzer PrmAnalyzerObjectCreate(ThaModulePdh self)
    {
    AtUnused(self);
    return Tha60290022PdhPrmAnalyzerNew(NULL);
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    const char *name = AtInternalRamName(ram);

    if (name == NULL)
        return m_AtModuleMethods->InternalRamIsReserved(self, ram);

    if (AtStrstr(name, "PDH RxDS3E3 OH Pro Control") ||
        AtStrstr(name, "PDH TxM23E23 E3g832 Trace Byte"))
        return cAtTrue;

    return m_AtModuleMethods->InternalRamIsReserved(self, ram);
    }

static eBool RxHw3BitFeacSignalIsSupported(ThaModulePdh self)
    {
    return Tha60290022DevicePdhRxHw3BitFeacSignalIsSupported(AtModuleDeviceGet((AtModule)self));
    }

static uint32 DefaultInbandLoopcodeThreshold(ThaModulePdh self)
    {
    static uint32 cPdhSlice48CoreClockInHz = 311040000; /* Hz or clocks per second */
    uint32 numClocksIn5s = (3 * cPdhSlice48CoreClockInHz);
    AtUnused(self);
    return numClocksIn5s;
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideTha60210011ModulePdh(AtModulePdh self)
    {
    Tha60210011ModulePdh pdhModule = (Tha60210011ModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_Tha60210011ModulePdhOverride));

        mMethodOverride(m_Tha60210011ModulePdhOverride, HasDataLinkRams);
        }

    mMethodsSet(pdhModule, &m_Tha60210011ModulePdhOverride);
    }

static void OverrideThaStmModulePdh(AtModulePdh self)
    {
    ThaStmModulePdh module = (ThaStmModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmModulePdhOverride, mMethodsGet(module), sizeof(m_ThaStmModulePdhOverride));

        mMethodOverride(m_ThaStmModulePdhOverride, StsVtDemapVc4FractionalVc3Set);
        }

    mMethodsSet(module, &m_ThaStmModulePdhOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(self), sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, VcDe1Create);
        mMethodOverride(m_AtModulePdhOverride, De2De1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe3Create);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(pdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, RxPerStsVCPldCtrlBase);
        mMethodOverride(m_ThaModulePdhOverride, DS3E3RxFrmrperChnIntrEnCtrlBase);
        mMethodOverride(m_ThaModulePdhOverride, RxM12E12MuxBase);
        mMethodOverride(m_ThaModulePdhOverride, RxM12E12FrmChnIntrEnCtrlBase);
        mMethodOverride(m_ThaModulePdhOverride, RxM12E12FrmChnIntrChngStatusBase);
        mMethodOverride(m_ThaModulePdhOverride, RxM12E12FrmChnAlarmCurStatusBase);
        mMethodOverride(m_ThaModulePdhOverride, DS1E1J1RxFrmrCrcErrCntBase);
        mMethodOverride(m_ThaModulePdhOverride, DS1E1J1RxFrmrReiCntBase);
        mMethodOverride(m_ThaModulePdhOverride, DS1E1J1RxFrmrFBECntBase);
        mMethodOverride(m_ThaModulePdhOverride, DS1E1J1RxFrmrperChnIntrEnCtrlBase);
        mMethodOverride(m_ThaModulePdhOverride, De1RxFrmrPerChnCurrentAlmBase);
        mMethodOverride(m_ThaModulePdhOverride, De1RxFrmrPerChnInterruptAlmBase);
        mMethodOverride(m_ThaModulePdhOverride, RxM23E23FrmrReiCntBase);
        mMethodOverride(m_ThaModulePdhOverride, RxM23E23FrmrFBECntBase);
        mMethodOverride(m_ThaModulePdhOverride, RxM23E23FrmrParCntBase);
        mMethodOverride(m_ThaModulePdhOverride, RxM23E23FrmrCbitParCntBase);
        mMethodOverride(m_ThaModulePdhOverride, RxDs3E3OHProCtrlBase);
        mMethodOverride(m_ThaModulePdhOverride, RxDe3FeacBufferBase);
        mMethodOverride(m_ThaModulePdhOverride, DS3E3RxFrmrperChnIntrStatCtrlBase);
        mMethodOverride(m_ThaModulePdhOverride, DS3E3RxFrmrperChnCurStatCtrlBase);
        mMethodOverride(m_ThaModulePdhOverride, De1RxFramerHwStatusBase);
        mMethodOverride(m_ThaModulePdhOverride, RegExist);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportPrm);
        mMethodOverride(m_ThaModulePdhOverride, De3InterruptProcess);
        mMethodOverride(m_ThaModulePdhOverride, De1InterruptProcess);
        mMethodOverride(m_ThaModulePdhOverride, De3InterruptEnable);
        mMethodOverride(m_ThaModulePdhOverride, De1InterruptEnable);
        mMethodOverride(m_ThaModulePdhOverride, NumStsInSlice);
        mMethodOverride(m_ThaModulePdhOverride, De3ChannelFromHwIdGet);
        mMethodOverride(m_ThaModulePdhOverride, De3InterruptStatusGet);
        mMethodOverride(m_ThaModulePdhOverride, De1InterruptStatusGet);
        mMethodOverride(m_ThaModulePdhOverride, PrmIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1LomfConsequentialAction);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1IdleCodeInsertion);
        mMethodOverride(m_ThaModulePdhOverride, De1PrmOffset);
        mMethodOverride(m_ThaModulePdhOverride, PrmControllerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, MdlControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, PrmInterruptManagerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, MdlInterruptManagerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, PrmAnalyzerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, RxHw3BitFeacSignalIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, DefaultInbandLoopcodeThreshold);
        }

    mMethodsSet(pdhModule, &m_ThaModulePdhOverride);
    }

static void OverrideTha60210031ModulePdh(AtModulePdh self)
    {
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210031ModulePdhMethods = mMethodsGet(pdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031ModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_Tha60210031ModulePdhOverride));

        mMethodOverride(m_Tha60210031ModulePdhOverride, RxDe3OhThrshCfgBase);
        mMethodOverride(m_Tha60210031ModulePdhOverride, TxBufferDwordOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenMdlBuffer1Base);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenMdlBuffer1Offset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlSliceOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlTxInsCtrlBase);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenCfgMdlBase);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenRxmdlCfgtypeBase);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenDestuffCtrl0Base);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenCfgCtrl0Base);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlControllerSliceOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxBufBaseAddress);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxStickyBaseAddress);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxPktOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxByteOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxDropOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, StartVersionSupportMdlFilterMessageByType);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlTxPktOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlTxByteOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxCounterOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenRxmdlCfgtypeOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenPrmTxcfgcrBase);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenPrmTxcfglbBase);
        mMethodOverride(m_Tha60210031ModulePdhOverride, LBOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenRxprmCfgstdBase);
        }

    mMethodsSet(pdhModule, &m_Tha60210031ModulePdhOverride);
    }

static void MethodsInit(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Af6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_Address);
        mMethodOverride(m_methods, Af6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_Address);
        mMethodOverride(m_methods, Af6Reg_dej1_rx_framer_per_chn_intr_or_stat_Address);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModule(self);
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    OverrideThaStmModulePdh(self);
    OverrideTha60210031ModulePdh(self);
    OverrideTha60210011ModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModulePdh);
    }

AtModulePdh Tha60290022ModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60290022ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModulePdhObjectInit(newModule, device);
    }

void Tha60290022ModulePdhHwFlushPdhSlice(AtModulePdh self, uint8 slice)
    {
    if (self)
        HwFlushPdhSlice(self, slice);
    }

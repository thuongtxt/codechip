/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290022ModulePdh.h
 * 
 * Created Date: Apr 22, 2017
 *
 * Description : PDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPDH_H_
#define _THA60290022MODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModulePdh *Tha60290022ModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60290022ModulePdhHwFlushPdhSlice(AtModulePdh self, uint8 slice);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPDH_H_ */


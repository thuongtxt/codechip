/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290022PdhVc3De3V3.c
 *
 * Created Date: Mar 15, 2018
 *
 * Description : Implementation for DE3 of fpga (since 5.x)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "./Tha60290022PdhVcDe3Internal.h"
#include "./Tha60290022ModulePdhReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cDefaultPdhModuleOffset 0x700000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PdhVcDe3V3
    {
    tTha60290022PdhVcDe3 super;
    }tTha60290022PdhVcDe3V3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPdhChannelMethods m_AtPdhChannelOverride;

/* Save super implementation */
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TxM23E23_Ctrl2_Address(ThaPdhDe3 self)
    {
    return cAf6Reg_txm23e23_ctrl2_Base + cDefaultPdhModuleOffset + ThaPdhDe3DefaultOffset(self);
    }

static eAtRet AutoFEBEEnable(ThaPdhDe3 self, eBool enable)
    {
    uint32 address, regVal;

    address = TxM23E23_Ctrl2_Address(self);
    regVal  = mChannelHwRead(self, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_txm23e23_ctrl2_TxDS3E3AutoFEBE_, (enable ? 1:0));
    mChannelHwWrite(self, address, regVal, cAtModulePdh);
    return cAtOk;
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    eAtRet ret = m_AtPdhChannelMethods->FrameTypeSet(self, frameType);

    if (ret != cAtOk)
        return ret;

    if (AtPdhDe3FrameTypeIsUnframed(frameType))
        return AutoFEBEEnable((ThaPdhDe3)self, cAtFalse);

    return AutoFEBEEnable((ThaPdhDe3)self, cAtTrue);
    }

static void OverrideAtPdhChannel(AtPdhDe3 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(m_AtPdhChannelOverride));

        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideAtPdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PdhVcDe3V3);
    }

static AtPdhDe3 ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60290022PdhVcDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe3 Tha60290022PdhVcDe3V3New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe3 == NULL)
        return NULL;

    /* construct it */
    return ObjectInit(newDe3, channelId, module);
    }

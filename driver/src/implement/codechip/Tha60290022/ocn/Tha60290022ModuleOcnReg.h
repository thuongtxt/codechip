/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_OCN_H_
#define _AF6_REG_AF6CNC0022_RD_OCN_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : OCN Global FSM pin value
Reg Addr   : 0xf0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the register to read FSM pin value

------------------------------------------------------------------------------*/
#define cAf6Reg_glbfsm_reg_Base                                                                        0xf0000

/*--------------------------------------
BitField Name: FsmValue
BitField Type: RO
BitField Desc: FSM pin value.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_glbfsm_reg_FsmValue_Mask                                                                    cBit0
#define cAf6_glbfsm_reg_FsmValue_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global STS Pointer Interpreter Control
Reg Addr   : 0xf0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the STS Pointer Interpreter

------------------------------------------------------------------------------*/
#define cAf6Reg_glbspi_reg_Base                                                                        0xf0002

/*--------------------------------------
BitField Name: StsPiAdjCntSel
BitField Type: RW
BitField Desc: Select pointer adjustment counter at "OCN Rx PP STS/VC Pointer
interpreter pointer adjustment per channel counter" 1: Counter for N/P event
which is transfered to PLA after Virtual FIFO module 0: Counter for Pointer
interpreter
BitField Bits: [31]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiAdjCntSel_Mask                                                             cBit31
#define cAf6_glbspi_reg_StsPiAdjCntSel_Shift                                                                31

/*--------------------------------------
BitField Name: RxPgNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames within pointer adjustments for Virtual fifo PG.
BitField Bits: [30:29]
--------------------------------------*/
#define cAf6_glbspi_reg_RxPgNorPtrThresh_Mask                                                        cBit30_29
#define cAf6_glbspi_reg_RxPgNorPtrThresh_Shift                                                              29

/*--------------------------------------
BitField Name: Hwdebug
BitField Type: RW
BitField Desc: Hwdebug
BitField Bits: [28]
--------------------------------------*/
#define cAf6_glbspi_reg_Hwdebug_Mask                                                                    cBit28
#define cAf6_glbspi_reg_Hwdebug_Shift                                                                       28

/*--------------------------------------
BitField Name: RxPgFlowThresh
BitField Type: RW
BitField Desc: Overflow/underflow threshold to resynchronize read/write pointer.
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_glbspi_reg_RxPgFlowThresh_Mask                                                          cBit27_24
#define cAf6_glbspi_reg_RxPgFlowThresh_Shift                                                                24

/*--------------------------------------
BitField Name: RxPgAdjThresh
BitField Type: RW
BitField Desc: Adjustment threshold to make a pointer increment/decrement.
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_glbspi_reg_RxPgAdjThresh_Mask                                                           cBit23_20
#define cAf6_glbspi_reg_RxPgAdjThresh_Shift                                                                 20

/*--------------------------------------
BitField Name: StsPiAisAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding P_AIS when AIS state is detected at STS
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiAisAisPEn_Mask                                                             cBit18
#define cAf6_glbspi_reg_StsPiAisAisPEn_Shift                                                                18

/*--------------------------------------
BitField Name: StsPiLopAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding P_AIS when LOP state is detected at STS
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiLopAisPEn_Mask                                                             cBit17
#define cAf6_glbspi_reg_StsPiLopAisPEn_Shift                                                                17

/*--------------------------------------
BitField Name: StsPiMajorMode
BitField Type: RW
BitField Desc: Majority mode for detecting increment/decrement at STS pointer
Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiMajorMode_Mask                                                             cBit16
#define cAf6_glbspi_reg_StsPiMajorMode_Shift                                                                16

/*--------------------------------------
BitField Name: StsPiNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames within pointer adjustments.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiNorPtrThresh_Mask                                                       cBit13_12
#define cAf6_glbspi_reg_StsPiNorPtrThresh_Shift                                                             12

/*--------------------------------------
BitField Name: StsPiNdfPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous NDF pointers for entering LOP
state at FSM.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_Mask                                                        cBit11_8
#define cAf6_glbspi_reg_StsPiNdfPtrThresh_Shift                                                              8

/*--------------------------------------
BitField Name: StsPiBadPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous invalid pointers for entering
LOP state at FSM.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiBadPtrThresh_Mask                                                         cBit7_4
#define cAf6_glbspi_reg_StsPiBadPtrThresh_Shift                                                              4

/*--------------------------------------
BitField Name: StsPiPohAisType
BitField Type: RW
BitField Desc: Enable/disable STS POH defect types to downstream AIS in case of
terminating the related STS such as the STS carries VT/TU. [0]: Enable for TIM
defect [1]: Enable for Unequiped defect [2]: Enable for VC-AIS [3]: Enable for
PLM defect
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_glbspi_reg_StsPiPohAisType_Mask                                                           cBit3_0
#define cAf6_glbspi_reg_StsPiPohAisType_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global VTTU Pointer Interpreter Control
Reg Addr   : 0xf0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the VTTU Pointer Interpreter

------------------------------------------------------------------------------*/
#define cAf6Reg_glbvpi_reg_Base                                                                        0xf0003

/*--------------------------------------
BitField Name: VtPiLomAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding AIS when LOM is detected. 1: Enable 0:
Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomAisPEn_Mask                                                              cBit29
#define cAf6_glbvpi_reg_VtPiLomAisPEn_Shift                                                                 29

/*--------------------------------------
BitField Name: VtPiLomInvlCntMod
BitField Type: RW
BitField Desc: H4 monitoring mode. 1: Expected H4 is current frame in the
validated sequence plus one. 0: Expected H4 is the last received value plus one.
BitField Bits: [28]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_Mask                                                          cBit28
#define cAf6_glbvpi_reg_VtPiLomInvlCntMod_Shift                                                             28

/*--------------------------------------
BitField Name: VtPiLomGoodThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous frames with validated sequence
of multi framers in LOM state for condition to entering IM state.
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_Mask                                                       cBit27_24
#define cAf6_glbvpi_reg_VtPiLomGoodThresh_Shift                                                             24

/*--------------------------------------
BitField Name: VtPiLomInvlThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous frames with invalidated
sequence of multi framers in IM state  for condition to entering LOM state.
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_Mask                                                       cBit23_20
#define cAf6_glbvpi_reg_VtPiLomInvlThresh_Shift                                                             20

/*--------------------------------------
BitField Name: VtPiAisAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding AIS when AIS state is detected at VTTU
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiAisAisPEn_Mask                                                              cBit18
#define cAf6_glbvpi_reg_VtPiAisAisPEn_Shift                                                                 18

/*--------------------------------------
BitField Name: VtPiLopAisPEn
BitField Type: RW
BitField Desc: Enable/Disable forwarding AIS when LOP state is detected at VTTU
Pointer interpreter. 1: Enable 0: Disable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiLopAisPEn_Mask                                                              cBit17
#define cAf6_glbvpi_reg_VtPiLopAisPEn_Shift                                                                 17

/*--------------------------------------
BitField Name: VtPiMajorMode
BitField Type: RW
BitField Desc: Majority mode detecting increment/decrement in VTTU pointer
Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiMajorMode_Mask                                                              cBit16
#define cAf6_glbvpi_reg_VtPiMajorMode_Shift                                                                 16

/*--------------------------------------
BitField Name: VtPiNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames within pointer adjustments.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_Mask                                                        cBit13_12
#define cAf6_glbvpi_reg_VtPiNorPtrThresh_Shift                                                              12

/*--------------------------------------
BitField Name: VtPiNdfPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous NDF pointers for entering LOP
state at FSM.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_Mask                                                         cBit11_8
#define cAf6_glbvpi_reg_VtPiNdfPtrThresh_Shift                                                               8

/*--------------------------------------
BitField Name: VtPiBadPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of contiguous invalid pointers for entering
LOP state at FSM.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_Mask                                                          cBit7_4
#define cAf6_glbvpi_reg_VtPiBadPtrThresh_Shift                                                               4

/*--------------------------------------
BitField Name: VtPiPohAisType
BitField Type: RW
BitField Desc: Enable/disable VTTU POH defect types to downstream AIS in case of
terminating the related VTTU. [0]: Enable for TIM defect [1]: Enable for Un-
equipment defect [2]: Enable for VC-AIS [3]: Enable for PLM defect
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_glbvpi_reg_VtPiPohAisType_Mask                                                            cBit3_0
#define cAf6_glbvpi_reg_VtPiPohAisType_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Pointer Generator Control
Reg Addr   : 0xf0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Tx Pointer Generator

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtpg_reg_Base                                                                        0xf0004

/*--------------------------------------
BitField Name: TxPgNorPtrThresh
BitField Type: RW
BitField Desc: Threshold of number of normal pointers between two contiguous
frames to make a condition of pointer adjustments.
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_Mask                                                          cBit9_8
#define cAf6_glbtpg_reg_TxPgNorPtrThresh_Shift                                                               8

/*--------------------------------------
BitField Name: TxPgFlowThresh
BitField Type: RW
BitField Desc: Overflow/underflow threshold to resynchronize read/write pointer
of TxFiFo.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbtpg_reg_TxPgFlowThresh_Mask                                                            cBit7_4
#define cAf6_glbtpg_reg_TxPgFlowThresh_Shift                                                                 4

/*--------------------------------------
BitField Name: TxPgAdjThresh
BitField Type: RW
BitField Desc: Adjustment threshold to make a condition of pointer
increment/decrement.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_glbtpg_reg_TxPgAdjThresh_Mask                                                             cBit3_0
#define cAf6_glbtpg_reg_TxPgAdjThresh_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Loopback
Reg Addr   : 0xf0005
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for internal loopback

------------------------------------------------------------------------------*/
#define cAf6Reg_glbloop_reg_Base                                                                       0xf0005

/*--------------------------------------
BitField Name: HoMapSts192cEn1
BitField Type: RW
BitField Desc: Enable/disable STS192C Mode for HoMap group 10G #1 (Line 4-7),
Set 1 to enable.
BitField Bits: [25]
--------------------------------------*/
#define cAf6_glbloop_reg_HoMapSts192cEn1_Mask                                                           cBit25
#define cAf6_glbloop_reg_HoMapSts192cEn1_Shift                                                              25

/*--------------------------------------
BitField Name: HoMapSts192cEn0
BitField Type: RW
BitField Desc: Enable/disable STS192C Mode for HoMap group 10G #0 (Line 0-3),
Set 1 to enable.
BitField Bits: [24]
--------------------------------------*/
#define cAf6_glbloop_reg_HoMapSts192cEn0_Mask                                                           cBit24
#define cAf6_glbloop_reg_HoMapSts192cEn0_Shift                                                              24

/*--------------------------------------
BitField Name: StmLineLocalLb
BitField Type: RW
BitField Desc: STM Line Local Loopback, 16 bits for 16 STM lines. 1: Loopback in
0: Normal (No loopback)
BitField Bits: [23:8]
--------------------------------------*/
#define cAf6_glbloop_reg_StmLineLocalLb_Mask                                                          cBit23_8
#define cAf6_glbloop_reg_StmLineLocalLb_Shift                                                                8

/*--------------------------------------
BitField Name: SxcApsSwFsm
BitField Type: RW
BitField Desc: SW-FSM for APS. 1: Page ID 1 is selected 0: Page ID 0 is selected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_glbloop_reg_SxcApsSwFsm_Mask                                                                cBit4
#define cAf6_glbloop_reg_SxcApsSwFsm_Shift                                                                   4

/*--------------------------------------
BitField Name: StsPohSel
BitField Type: RW
BitField Desc: STS POH Selection. 1: TFI5 is selected to Mon/Insert POH 0:
FacePlate is selected to Mon/Insert POH
BitField Bits: [3]
--------------------------------------*/
#define cAf6_glbloop_reg_StsPohSel_Mask                                                                  cBit3
#define cAf6_glbloop_reg_StsPohSel_Shift                                                                     3

/*--------------------------------------
BitField Name: InternalDebug
BitField Type: RW
BitField Desc: Internal Debug
BitField Bits: [2:1]
--------------------------------------*/
#define cAf6_glbloop_reg_InternalDebug_Mask                                                            cBit2_1
#define cAf6_glbloop_reg_InternalDebug_Shift                                                                 1

/*--------------------------------------
BitField Name: LoLoopBack
BitField Type: RW
BitField Desc: LoBus Loopback. 1: Loopback in 0: Normal (No loopback)
BitField Bits: [0]
--------------------------------------*/
#define cAf6_glbloop_reg_LoLoopBack_Mask                                                                 cBit0
#define cAf6_glbloop_reg_LoLoopBack_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global APS Grouping SXC_Page Selection
Reg Addr   : 0xf0006
Reg Formula: 
    Where  : 
Reg Desc   : 
This register use 16 bits to configure SXC_Page ID for 16 APS Group. Bit 0 for APS Group 0

------------------------------------------------------------------------------*/
#define cAf6Reg_glbapsgrppage_reg_Base                                                                 0xf0006

/*--------------------------------------
BitField Name: ApsGrpPageId
BitField Type: RW
BitField Desc: Each bit is used to configure SXC_Page ID for 16 APS Group. Bit 0
for APS Group 0 1: SXC_Page ID 1 is selected 0: SXC_Page ID 0 is selected
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_glbapsgrppage_reg_ApsGrpPageId_Mask                                                      cBit15_0
#define cAf6_glbapsgrppage_reg_ApsGrpPageId_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Passthrough Grouping Enable
Reg Addr   : 0xf0007
Reg Formula: 
    Where  : 
Reg Desc   : 
This register use 32 bits to configure for 16 Passthrough Groups at Rx direction and 16 Passthrough Groups at Tx direction.

------------------------------------------------------------------------------*/
#define cAf6Reg_glbpasgrpenb_reg_Base                                                                  0xf0007

/*--------------------------------------
BitField Name: TxSpgPassThrGrpEnb
BitField Type: RW
BitField Desc: Each bit is used to configure enable/disable Passthrough mode at
Tx SPG for 1 Passthrough Group. Bit 16 for Passthrough Group Tx0 1: Enable 0:
Disable.
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_glbpasgrpenb_reg_TxSpgPassThrGrpEnb_Mask                                                cBit31_16
#define cAf6_glbpasgrpenb_reg_TxSpgPassThrGrpEnb_Shift                                                      16

/*--------------------------------------
BitField Name: RxSpiPassThrGrpEnb
BitField Type: RW
BitField Desc: Each bit is used to configure enable/disable Passthrough mode at
Rx SPI for 1 Passthrough Group. Bit 0 for Passthrough Group Rx0 1: Enable 0:
Disable.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_glbpasgrpenb_reg_RxSpiPassThrGrpEnb_Mask                                                 cBit15_0
#define cAf6_glbpasgrpenb_reg_RxSpiPassThrGrpEnb_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx HO/LO Selection Control1
Reg Addr   : 0xf0010 - 0xf0017
Reg Formula: 0xf0010 + SliceOc48Id
    Where  : 
           + $SliceOc48Id(0-7)
Reg Desc   : 
Configure Tx HO/LO Selection for 8 slices HO/LO OC48 for STS from 0-31.

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtxholosel1_reg_Base                                                                 0xf0010

/*--------------------------------------
BitField Name: TxHoLoSelCtl1
BitField Type: RW
BitField Desc: Each bit will be config per STS in any slices HO/LO OC48, bit #0
for STS#0. 1: Select from Ho_Bus 0: Select from Lo_Bus
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_glbtxholosel1_reg_TxHoLoSelCtl1_Mask                                                     cBit31_0
#define cAf6_glbtxholosel1_reg_TxHoLoSelCtl1_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx HO/LO Selection Control2
Reg Addr   : 0xf0018 - 0xf001f
Reg Formula: 0xf0018 + SliceOc48Id
    Where  : 
           + $SliceOc48Id(0-7)
Reg Desc   : 
Configure Tx HO/LO Selection for 8 slices HO/LO OC48 for STS from 32-47.

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtxholosel2_reg_Base                                                                 0xf0018

/*--------------------------------------
BitField Name: TxHoLoSelCtl2
BitField Type: RW
BitField Desc: Each bit will be config per STS in any slices HO/LO OC48, bit #0
for STS#32. 1: Select from Ho_Bus 0: Select from Lo_Bus
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_glbtxholosel2_reg_TxHoLoSelCtl2_Mask                                                     cBit15_0
#define cAf6_glbtxholosel2_reg_TxHoLoSelCtl2_Shift                                                           0

/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx HO G1 overwrite Control1
RTL Instant Name  : glbtxhog1ovw1_reg
Address: 0xf0040 - 0xf0047
Formula      : Address + SliceOc48Id
Where        : {$SliceOc48Id(0-7)}
Description  : Configure to overwrite G1 at HO path for 8 slices HO/LO OC48 for STS from 0-31.
------------------------------------------------------------------------------*/
#define cAf6Reg_glbtxhog1ovw1_reg_Base                                                                 0xf0040

/*------------------------------------------------------------------------------
Reg Name: OCN Global Tx HO G1 overwrite Control2
RTL Instant Name  : glbtxhog1ovw2_reg
Address: 0xf0048 - 0xf004f
Formula      : Address + SliceOc48Id
Where        : {$SliceOc48Id(0-7)}
Description  : Configure to overwrite G1 at HO path for 8 slices HO/LO OC48 for STS from 32-47.
------------------------------------------------------------------------------*/
#define cAf6Reg_glbtxhog1ovw2_reg_Base                                                                 0xf0048

/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer Control - TFI5 Side
Reg Addr   : 0x0f000
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Rx Framer

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5glbrfm_reg_Base                                                                    0x0f000

/*--------------------------------------
BitField Name: Tfi5RxLineSts192cEn1
BitField Type: RW
BitField Desc: Enable/disable STS192C Mode for Rx TFI5 group 10G #1 (Line 4-7),
Set 1 to enable.
BitField Bits: [17]
--------------------------------------*/
#define cAf6_tfi5glbrfm_reg_Tfi5RxLineSts192cEn1_Mask                                                   cBit17
#define cAf6_tfi5glbrfm_reg_Tfi5RxLineSts192cEn1_Shift                                                      17

/*--------------------------------------
BitField Name: Tfi5RxLineSts192cEn0
BitField Type: RW
BitField Desc: Enable/disable STS192C Mode for Rx TFI5 group 10G #0 (Line 0-3),
Set 1 to enable.
BitField Bits: [16]
--------------------------------------*/
#define cAf6_tfi5glbrfm_reg_Tfi5RxLineSts192cEn0_Mask                                                   cBit16
#define cAf6_tfi5glbrfm_reg_Tfi5RxLineSts192cEn0_Shift                                                      16

/*--------------------------------------
BitField Name: Tfi5RxFrmLosAisEn
BitField Type: RW
BitField Desc: Enable/disable forwarding P_AIS when LOS detected at Rx Framer.
1: Enable 0: Disable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmLosAisEn_Mask                                                      cBit14
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmLosAisEn_Shift                                                         14

/*--------------------------------------
BitField Name: Tfi5RxFrmOofAisEn
BitField Type: RW
BitField Desc: Enable/disable forwarding P_AIS when OOF detected at Rx Framer.
1: Enable 0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmOofAisEn_Mask                                                      cBit13
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmOofAisEn_Shift                                                         13

/*--------------------------------------
BitField Name: Tfi5RxFrmB1BlockCntMod
BitField Type: RW
BitField Desc: B1 Counter Mode. 1: Block mode 0: Bit-wise mode
BitField Bits: [12]
--------------------------------------*/
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmB1BlockCntMod_Mask                                                 cBit12
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmB1BlockCntMod_Shift                                                    12

/*--------------------------------------
BitField Name: Tfi5RxFrmBadFrmThresh
BitField Type: RW
BitField Desc: Threshold for A1A2 missing counter, that is used to change state
from FRAMED to HUNT.
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmBadFrmThresh_Mask                                                cBit10_8
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmBadFrmThresh_Shift                                                      8

/*--------------------------------------
BitField Name: Tfi5RxFrmB1GoodThresh
BitField Type: RW
BitField Desc: Threshold for B1 good counter, that is used to change state from
CHECK to FRAMED.
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmB1GoodThresh_Mask                                                 cBit6_4
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmB1GoodThresh_Shift                                                      4

/*--------------------------------------
BitField Name: Tfi5RxFrmDescrEn
BitField Type: RW
BitField Desc: Enable/disable de-scrambling of the Rx coming data stream. 1:
Enable 0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmDescrEn_Mask                                                        cBit2
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmDescrEn_Shift                                                           2

/*--------------------------------------
BitField Name: Tfi5RxFrmB1ChkFrmEn
BitField Type: RW
BitField Desc: Enable/disable B1 check option is added to the required framing
algorithm. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmB1ChkFrmEn_Mask                                                     cBit1
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmB1ChkFrmEn_Shift                                                        1

/*--------------------------------------
BitField Name: Tfi5RxFrmModeEn
BitField Type: RW
BitField Desc: TFI-5 mode. 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmModeEn_Mask                                                         cBit0
#define cAf6_tfi5glbrfm_reg_Tfi5RxFrmModeEn_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer LOS Detecting Control 1 - TFI5 Side
Reg Addr   : 0x0f006
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Rx Framer LOS detecting 1

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5glbclkmon_reg_Base                                                                 0x0f006

/*--------------------------------------
BitField Name: Tfi5RxFrmLosDetMod
BitField Type: RW
BitField Desc: Detected LOS mode. 1: the LOS declare when all zero or all one
detected 0: the LOS declare when all zero detected
BitField Bits: [18]
--------------------------------------*/
#define cAf6_tfi5glbclkmon_reg_Tfi5RxFrmLosDetMod_Mask                                                  cBit18
#define cAf6_tfi5glbclkmon_reg_Tfi5RxFrmLosDetMod_Shift                                                     18

/*--------------------------------------
BitField Name: Tfi5RxFrmLosDetDis
BitField Type: RW
BitField Desc: Disable detect LOS. 1: Disable 0: Enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_tfi5glbclkmon_reg_Tfi5RxFrmLosDetDis_Mask                                                  cBit17
#define cAf6_tfi5glbclkmon_reg_Tfi5RxFrmLosDetDis_Shift                                                     17

/*--------------------------------------
BitField Name: Tfi5RxFrmClkMonDis
BitField Type: RW
BitField Desc: Disable to generate LOS to Rx Framer if detecting error on Rx
line clock. 1: Disable 0: Enable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_tfi5glbclkmon_reg_Tfi5RxFrmClkMonDis_Mask                                                  cBit16
#define cAf6_tfi5glbclkmon_reg_Tfi5RxFrmClkMonDis_Shift                                                     16

/*--------------------------------------
BitField Name: Tfi5RxFrmClkMonThr
BitField Type: RW
BitField Desc: Threshold to generate LOS to Rx Framer if detecting error on Rx
line clock.(Threshold = PPM * 155.52/8). Default 0x3cc ~ 50ppm
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tfi5glbclkmon_reg_Tfi5RxFrmClkMonThr_Mask                                                cBit15_0
#define cAf6_tfi5glbclkmon_reg_Tfi5RxFrmClkMonThr_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer LOS Detecting Control 2 - TFI5 Side
Reg Addr   : 0x0f007
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Rx Framer LOS detecting 2

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5glbdetlos_pen_Base                                                                 0x0f007

/*--------------------------------------
BitField Name: Tfi5RxFrmLosClr2Thr
BitField Type: RW
BitField Desc: Configure the period of time during which there is no period of
consecutive data without transition, used for reset no transition counter. The
recommended value is 0x30 (~2.5us).
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_tfi5glbdetlos_pen_Tfi5RxFrmLosClr2Thr_Mask                                              cBit31_24
#define cAf6_tfi5glbdetlos_pen_Tfi5RxFrmLosClr2Thr_Shift                                                    24

/*--------------------------------------
BitField Name: Tfi5RxFrmLosSetThr
BitField Type: RW
BitField Desc: Configure the value that define the period of time in which there
is no transition detected from incoming data from SERDES. The recommended value
is 0x3cc (~50us).
BitField Bits: [23:12]
--------------------------------------*/
#define cAf6_tfi5glbdetlos_pen_Tfi5RxFrmLosSetThr_Mask                                               cBit23_12
#define cAf6_tfi5glbdetlos_pen_Tfi5RxFrmLosSetThr_Shift                                                     12

/*--------------------------------------
BitField Name: Tfi5RxFrmLosClr1Thr
BitField Type: RW
BitField Desc: Configure the period of time during which there is no period of
consecutive data without transition, used for counting at INFRM to change state
to LOS The recommended value is 0x3cc (~50us).
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_tfi5glbdetlos_pen_Tfi5RxFrmLosClr1Thr_Mask                                               cBit11_0
#define cAf6_tfi5glbdetlos_pen_Tfi5RxFrmLosClr1Thr_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Framer Control - TFI5 Side
Reg Addr   : 0x0f001
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Tx Framer

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5glbtfm_reg_Base                                                                    0x0f001

/*--------------------------------------
BitField Name: Tfi5TxLineOofFrc
BitField Type: RW
BitField Desc: Enable/disable force OOF for 8 tx lines. Bit[0] for line 0.
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_tfi5glbtfm_reg_Tfi5TxLineOofFrc_Mask                                                    cBit31_24
#define cAf6_tfi5glbtfm_reg_Tfi5TxLineOofFrc_Shift                                                          24

/*--------------------------------------
BitField Name: Tfi5TxLineB1ErrFrc
BitField Type: RW
BitField Desc: Enable/disable force B1 error for 8 tx lines. Bit[0] for line 0.
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_tfi5glbtfm_reg_Tfi5TxLineB1ErrFrc_Mask                                                  cBit23_16
#define cAf6_tfi5glbtfm_reg_Tfi5TxLineB1ErrFrc_Shift                                                        16

/*--------------------------------------
BitField Name: Tfi5TxLineSyncSel
BitField Type: RW
BitField Desc: Select line id for synchronization 8 tx lines. Bit[0] for line 0.
Note that must only 1 bit is set
BitField Bits: [11:4]
--------------------------------------*/
#define cAf6_tfi5glbtfm_reg_Tfi5TxLineSyncSel_Mask                                                    cBit11_4
#define cAf6_tfi5glbtfm_reg_Tfi5TxLineSyncSel_Shift                                                          4

/*--------------------------------------
BitField Name: Tfi5TxLineSts192cEn1
BitField Type: RW
BitField Desc: Enable/disable STS192C Mode for Tx TFI5 group 10G #1 (Line 4-7),
Set 1 to enable.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tfi5glbtfm_reg_Tfi5TxLineSts192cEn1_Mask                                                    cBit3
#define cAf6_tfi5glbtfm_reg_Tfi5TxLineSts192cEn1_Shift                                                       3

/*--------------------------------------
BitField Name: Tfi5TxLineSts192cEn0
BitField Type: RW
BitField Desc: Enable/disable STS192C Mode for Tx TFI5 group 10G #0 (Line 0-3),
Set 1 to enable.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tfi5glbtfm_reg_Tfi5TxLineSts192cEn0_Mask                                                    cBit2
#define cAf6_tfi5glbtfm_reg_Tfi5TxLineSts192cEn0_Shift                                                       2

/*--------------------------------------
BitField Name: Tfi5TxFrmScrEn
BitField Type: RW
BitField Desc: Enable/disable scrambling of the Tx data stream. 1: Enable 0:
Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tfi5glbtfm_reg_Tfi5TxFrmScrEn_Mask                                                          cBit0
#define cAf6_tfi5glbtfm_reg_Tfi5TxFrmScrEn_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : OCN STS Pointer Interpreter Per Channel Control - TFI5 Side
Reg Addr   : 0x02000 - 0x02e2f
Reg Formula: 0x02000 + 512*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure for STS pointer interpreter engines of the STS-1/VC-3.

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5spiramctl_Base                                                                     0x02000

/*--------------------------------------
BitField Name: Tfi5PassThrEnb
BitField Type: RW
BitField Desc: Enable/disable Passthrough mode for BLSR at Rx SPI of TFI5 Side
1: Enable. 0: Disable.
BitField Bits: [20]
--------------------------------------*/
#define cAf6_tfi5spiramctl_Tfi5PassThrEnb_Mask                                                          cBit20
#define cAf6_tfi5spiramctl_Tfi5PassThrEnb_Shift                                                             20

/*--------------------------------------
BitField Name: Tfi5PassThrGrp
BitField Type: RW
BitField Desc: Passthrough Group ID for BLSR at Rx SPI of TFI5 Side.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_tfi5spiramctl_Tfi5PassThrGrp_Mask                                                       cBit19_16
#define cAf6_tfi5spiramctl_Tfi5PassThrGrp_Shift                                                             16

/*--------------------------------------
BitField Name: Tfi5StsPiAisCcDetMod
BitField Type: RW
BitField Desc: Configure mode for detecting AIS concatenation. 1: AIS detected
if all of STS-1 (master and slave) is AIS This mode is applied for SDH mode 0:
AIS detected if only STS-1 mater is AIS. This mode is applied for SONET mode
BitField Bits: [15]
--------------------------------------*/
#define cAf6_tfi5spiramctl_Tfi5StsPiAisCcDetMod_Mask                                                    cBit15
#define cAf6_tfi5spiramctl_Tfi5StsPiAisCcDetMod_Shift                                                       15

/*--------------------------------------
BitField Name: Tfi5StsPiChkLom
BitField Type: RW
BitField Desc: Enable/disable LOM checking. This field will be set to 1 when
payload type of VC3/VC4 includes any VC11/VC12 1: Enable. 0: Disable.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_tfi5spiramctl_Tfi5StsPiChkLom_Mask                                                         cBit14
#define cAf6_tfi5spiramctl_Tfi5StsPiChkLom_Shift                                                            14

/*--------------------------------------
BitField Name: Tfi5StsPiSSDetPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to compare with the
extracted SS bits from receive direction.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_tfi5spiramctl_Tfi5StsPiSSDetPatt_Mask                                                   cBit13_12
#define cAf6_tfi5spiramctl_Tfi5StsPiSSDetPatt_Shift                                                         12

/*--------------------------------------
BitField Name: Tfi5StsPiAisFrc
BitField Type: RW
BitField Desc: Forcing FSM to AIS state. 1: Force 0: Not force
BitField Bits: [11]
--------------------------------------*/
#define cAf6_tfi5spiramctl_Tfi5StsPiAisFrc_Mask                                                         cBit11
#define cAf6_tfi5spiramctl_Tfi5StsPiAisFrc_Shift                                                            11

/*--------------------------------------
BitField Name: Tfi5StsPiSSDetEn
BitField Type: RW
BitField Desc: Enable/disable checking SS bits in STSPI state machine. 1: Enable
0: Disable
BitField Bits: [10]
--------------------------------------*/
#define cAf6_tfi5spiramctl_Tfi5StsPiSSDetEn_Mask                                                        cBit10
#define cAf6_tfi5spiramctl_Tfi5StsPiSSDetEn_Shift                                                           10

/*--------------------------------------
BitField Name: Tfi5StsPiAdjRule
BitField Type: RW
BitField Desc: Configure the rule for detecting adjustment condition. 1: The n
of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is
selected. This mode is applied for SONET mode
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tfi5spiramctl_Tfi5StsPiAdjRule_Mask                                                         cBit9
#define cAf6_tfi5spiramctl_Tfi5StsPiAdjRule_Shift                                                            9

/*--------------------------------------
BitField Name: Tfi5StsPiStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master. 1: Slaver.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tfi5spiramctl_Tfi5StsPiStsSlvInd_Mask                                                       cBit8
#define cAf6_tfi5spiramctl_Tfi5StsPiStsSlvInd_Shift                                                          8

/*--------------------------------------
BitField Name: Tfi5StsPiStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_tfi5spiramctl_Tfi5StsPiStsMstId_Mask                                                      cBit5_0
#define cAf6_tfi5spiramctl_Tfi5StsPiStsMstId_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN STS Pointer Generator Per Channel Control - TFI5 Side
Reg Addr   : 0x03000 - 0x03e2f
Reg Formula: 0x03000 + 512*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure for STS pointer Generator engines.
# HDL_PATH		: itfi5ho.itxpp_stspp_inst.itxpp_stspp[0].txpg_stspgctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5spgramctl_Base                                                                     0x03000

/*--------------------------------------
BitField Name: Tfi5StsPgStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master. 1: Slaver.
BitField Bits: [16]
--------------------------------------*/
#define cAf6_tfi5spgramctl_Tfi5StsPgStsSlvInd_Mask                                                      cBit16
#define cAf6_tfi5spgramctl_Tfi5StsPgStsSlvInd_Shift                                                         16

/*--------------------------------------
BitField Name: Tfi5StsPgStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1.
BitField Bits: [13:8]
--------------------------------------*/
#define cAf6_tfi5spgramctl_Tfi5StsPgStsMstId_Mask                                                     cBit13_8
#define cAf6_tfi5spgramctl_Tfi5StsPgStsMstId_Shift                                                           8

/*--------------------------------------
BitField Name: Tfi5StsPgB3BipErrFrc
BitField Type: RW
BitField Desc: Forcing B3 Bip error. 1: Force 0: Not force
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tfi5spgramctl_Tfi5StsPgB3BipErrFrc_Mask                                                     cBit7
#define cAf6_tfi5spgramctl_Tfi5StsPgB3BipErrFrc_Shift                                                        7

/*--------------------------------------
BitField Name: Tfi5StsPgLopFrc
BitField Type: RW
BitField Desc: Forcing LOP. 1: Force 0: Not force
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tfi5spgramctl_Tfi5StsPgLopFrc_Mask                                                          cBit6
#define cAf6_tfi5spgramctl_Tfi5StsPgLopFrc_Shift                                                             6

/*--------------------------------------
BitField Name: Tfi5StsPgUeqFrc
BitField Type: RW
BitField Desc: Forcing FSM to UEQ state. 1: Force 0: Not force
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tfi5spgramctl_Tfi5StsPgUeqFrc_Mask                                                          cBit5
#define cAf6_tfi5spgramctl_Tfi5StsPgUeqFrc_Shift                                                             5

/*--------------------------------------
BitField Name: Tfi5StsPgAisFrc
BitField Type: RW
BitField Desc: Forcing FSM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tfi5spgramctl_Tfi5StsPgAisFrc_Mask                                                          cBit4
#define cAf6_tfi5spgramctl_Tfi5StsPgAisFrc_Shift                                                             4

/*--------------------------------------
BitField Name: Tfi5StsPgSSInsPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to insert to pointer
value.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_tfi5spgramctl_Tfi5StsPgSSInsPatt_Mask                                                     cBit3_2
#define cAf6_tfi5spgramctl_Tfi5StsPgSSInsPatt_Shift                                                          2

/*--------------------------------------
BitField Name: Tfi5StsPgSSInsEn
BitField Type: RW
BitField Desc: Enable/disable SS bits insertion. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tfi5spgramctl_Tfi5StsPgSSInsEn_Mask                                                         cBit1
#define cAf6_tfi5spgramctl_Tfi5StsPgSSInsEn_Shift                                                            1

/*--------------------------------------
BitField Name: Tfi5StsPgPohIns
BitField Type: RW
BitField Desc: Enable/disable POH Insertion. High to enable insertion of POH.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tfi5spgramctl_Tfi5StsPgPohIns_Mask                                                          cBit0
#define cAf6_tfi5spgramctl_Tfi5StsPgPohIns_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer Status - TFI5 Side
Reg Addr   : 0x05000 - 0x05e00
Reg Formula: 0x05000 + 512*LineId
    Where  : 
           + $LineId(0-7)
Reg Desc   : 
Rx Framer status

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5rxfrmsta_Base                                                                      0x05000

/*--------------------------------------
BitField Name: Tfi5StaFFCnvFul
BitField Type: RO
BitField Desc: Status fifo convert clock domain is full
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tfi5rxfrmsta_Tfi5StaFFCnvFul_Mask                                                           cBit2
#define cAf6_tfi5rxfrmsta_Tfi5StaFFCnvFul_Shift                                                              2

/*--------------------------------------
BitField Name: Tfi5StaLos
BitField Type: RO
BitField Desc: Loss of Signal status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tfi5rxfrmsta_Tfi5StaLos_Mask                                                                cBit1
#define cAf6_tfi5rxfrmsta_Tfi5StaLos_Shift                                                                   1

/*--------------------------------------
BitField Name: Tfi5StaOof
BitField Type: RO
BitField Desc: Out of Frame that is detected at RxFramer
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tfi5rxfrmsta_Tfi5StaOof_Mask                                                                cBit0
#define cAf6_tfi5rxfrmsta_Tfi5StaOof_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer Sticky - TFI5 Side
Reg Addr   : 0x05001 - 0x05e01
Reg Formula: 0x05001 + 512*LineId
    Where  : 
           + $LineId(0-7)
Reg Desc   : 
Rx Framer sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5rxfrmstk_Base                                                                      0x05001

/*--------------------------------------
BitField Name: Tfi5StkFFCnvFul
BitField Type: W1C
BitField Desc: Sticky fifo convert clock domain is full
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tfi5rxfrmstk_Tfi5StkFFCnvFul_Mask                                                           cBit2
#define cAf6_tfi5rxfrmstk_Tfi5StkFFCnvFul_Shift                                                              2

/*--------------------------------------
BitField Name: Tfi5StkLos
BitField Type: W1C
BitField Desc: Loss of Signal  sticky change
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tfi5rxfrmstk_Tfi5StkLos_Mask                                                                cBit1
#define cAf6_tfi5rxfrmstk_Tfi5StkLos_Shift                                                                   1

/*--------------------------------------
BitField Name: Tfi5StkOof
BitField Type: W1C
BitField Desc: Out of Frame sticky  change
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tfi5rxfrmstk_Tfi5StkOof_Mask                                                                cBit0
#define cAf6_tfi5rxfrmstk_Tfi5StkOof_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer B1 error counter read only - TFI5 Side
Reg Addr   : 0x05002 - 0x05e02
Reg Formula: 0x05002 + 512*LineId
    Where  : 
           + $LineId(0-7)
Reg Desc   : 
Rx Framer B1 error counter read only

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5rxfrmb1cntro_Base                                                                  0x05002

/*--------------------------------------
BitField Name: Tfi5RxFrmB1ErrRo
BitField Type: RO
BitField Desc: Number of B1 error - read only
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_tfi5rxfrmb1cntro_Tfi5RxFrmB1ErrRo_Mask                                                   cBit31_0
#define cAf6_tfi5rxfrmb1cntro_Tfi5RxFrmB1ErrRo_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer B1 error counter read to clear - TFI5 Side
Reg Addr   : 0x05003 - 0x05e03
Reg Formula: 0x05003 + 512*LineId
    Where  : 
           + $LineId(0-7)
Reg Desc   : 
Rx Framer B1 error counter read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5rxfrmb1cntr2c_Base                                                                 0x05003

/*--------------------------------------
BitField Name: Tfi5RxFrmB1ErrR2C
BitField Type: RC
BitField Desc: Number of B1 error - read to clear
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_tfi5rxfrmb1cntr2c_Tfi5RxFrmB1ErrR2C_Mask                                                 cBit31_0
#define cAf6_tfi5rxfrmb1cntr2c_Tfi5RxFrmB1ErrR2C_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC per Alarm Interrupt Status - TFI5 Side
Reg Addr   : 0x02140 - 0x02f6f
Reg Formula: 0x02140 + 512*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
This is the per Alarm interrupt status of STS/VC pointer interpreter.  %%
Each register is used to store 6 sticky bits for 6 alarms in the STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5upstschstkram_Base                                                                 0x02140

/*--------------------------------------
BitField Name: Tfi5StsPiStsConcDetIntr
BitField Type: W1C
BitField Desc: Set to 1 while an Concatenation Detection event is detected at
STS/VC pointer interpreter. This event doesn't raise interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tfi5upstschstkram_Tfi5StsPiStsConcDetIntr_Mask                                              cBit5
#define cAf6_tfi5upstschstkram_Tfi5StsPiStsConcDetIntr_Shift                                                 5

/*--------------------------------------
BitField Name: Tfi5StsPiStsNewDetIntr
BitField Type: W1C
BitField Desc: Set to 1 while an New Pointer Detection event is detected at
STS/VC pointer interpreter. This event doesn't raise interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tfi5upstschstkram_Tfi5StsPiStsNewDetIntr_Mask                                               cBit4
#define cAf6_tfi5upstschstkram_Tfi5StsPiStsNewDetIntr_Shift                                                  4

/*--------------------------------------
BitField Name: Tfi5StsPiStsNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF event is detected at STS/VC pointer
interpreter. This event doesn't raise interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tfi5upstschstkram_Tfi5StsPiStsNdfIntr_Mask                                                  cBit3
#define cAf6_tfi5upstschstkram_Tfi5StsPiStsNdfIntr_Shift                                                     3

/*--------------------------------------
BitField Name: Tfi5StsPiCepUneqStatePChgIntr
BitField Type: W1C
BitField Desc: Set to 1  while there is change in CEP Un-equip Path in the
related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm
Current Status register of the related STS/VC to know the STS/VC whether in CEP
Un-equip Path state or not.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tfi5upstschstkram_Tfi5StsPiCepUneqStatePChgIntr_Mask                                        cBit2
#define cAf6_tfi5upstschstkram_Tfi5StsPiCepUneqStatePChgIntr_Shift                                           2

/*--------------------------------------
BitField Name: Tfi5StsPiStsAISStateChgIntr
BitField Type: W1C
BitField Desc: Set to 1 while there is change in AIS state in the related STS/VC
to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status
register of the related STS/VC to know the STS/VC whether in AIS state or not.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tfi5upstschstkram_Tfi5StsPiStsAISStateChgIntr_Mask                                          cBit1
#define cAf6_tfi5upstschstkram_Tfi5StsPiStsAISStateChgIntr_Shift                                             1

/*--------------------------------------
BitField Name: Tfi5StsPiStsLopStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 to while there is change in LOP state in the related STS/VC
to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status
register of the related STS/VC to know the STS/VC whether in LOP state or not.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tfi5upstschstkram_Tfi5StsPiStsLopStateChgIntr_Mask                                          cBit0
#define cAf6_tfi5upstschstkram_Tfi5StsPiStsLopStateChgIntr_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC per Alarm Current Status - TFI5 Side
Reg Addr   : 0x02180 - 0x02faf
Reg Formula: 0x02180 + 512*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
This is the per Alarm current status of STS/VC pointer interpreter.  %%
Each register is used to store 3 bits to store current status of 3 alarms in the STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5upstschstaram_Base                                                                 0x02180

/*--------------------------------------
BitField Name: Tfi5StsPiStsCepUneqPCurStatus
BitField Type: RO
BitField Desc: CEP Un-eqip Path current status in the related STS/VC. When it
changes for 0 to 1 or vice versa, the Tfi5StsPiStsCepUeqPStateChgIntr bit in the
OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tfi5upstschstaram_Tfi5StsPiStsCepUneqPCurStatus_Mask                                        cBit2
#define cAf6_tfi5upstschstaram_Tfi5StsPiStsCepUneqPCurStatus_Shift                                           2

/*--------------------------------------
BitField Name: Tfi5StsPiStsAisCurStatus
BitField Type: RO
BitField Desc: AIS current status in the related STS/VC. When it changes for 0
to 1 or vice versa, the  Tfi5StsPiStsAISStateChgIntr bit in the OCN Rx STS/VC
per Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tfi5upstschstaram_Tfi5StsPiStsAisCurStatus_Mask                                             cBit1
#define cAf6_tfi5upstschstaram_Tfi5StsPiStsAisCurStatus_Shift                                                1

/*--------------------------------------
BitField Name: Tfi5StsPiStsLopCurStatus
BitField Type: RO
BitField Desc: LOP current status in the related STS/VC. When it changes for 0
to 1 or vice versa, the  Tfi5StsPiStsLopStateChgIntr bit in the OCN Rx STS/VC
per Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tfi5upstschstaram_Tfi5StsPiStsLopCurStatus_Mask                                             cBit0
#define cAf6_tfi5upstschstaram_Tfi5StsPiStsLopCurStatus_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter - TFI5 Side
Reg Addr   : 0x02080 - 0x02eaf
Reg Formula: 0x02080 + 512*LineId + 64*AdjMode + StsId
    Where  : 
           + $LineId(0-7)
           + $AdjMode(0-1)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS/VC pointer interpreter. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5adjcntperstsram_Base                                                               0x02080

/*--------------------------------------
BitField Name: Tfi5RxPpStsPiPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [6] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_tfi5adjcntperstsram_Tfi5RxPpStsPiPtAdjCnt_Mask                                           cBit17_0
#define cAf6_tfi5adjcntperstsram_Tfi5RxPpStsPiPtAdjCnt_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg STS per Alarm Interrupt Status - TFI5 Side
Reg Addr   : 0x03140 - 0x03f6f
Reg Formula: 0x03140 + 512*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
This is the per Alarm interrupt status of STS pointer generator . Each register is used to store 3 sticky bits for 3 alarms

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5stspgstkram_Base                                                                   0x03140

/*--------------------------------------
BitField Name: Tfi5StsPgAisIntr
BitField Type: W1C
BitField Desc: Set to 1 while an AIS status event (at h2pos) is detected at Tx
STS Pointer Generator.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tfi5stspgstkram_Tfi5StsPgAisIntr_Mask                                                       cBit3
#define cAf6_tfi5stspgstkram_Tfi5StsPgAisIntr_Shift                                                          3

/*--------------------------------------
BitField Name: Tfi5StsPgNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF status event (at h2pos) is detected at Tx
STS Pointer Generator.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tfi5stspgstkram_Tfi5StsPgNdfIntr_Mask                                                       cBit1
#define cAf6_tfi5stspgstkram_Tfi5StsPgNdfIntr_Shift                                                          1


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg STS pointer adjustment per channel counter - TFI5 Side
Reg Addr   : 0x03080 - 0x03eaf
Reg Formula: 0x03080 + 512*LineId + 64*AdjMode + StsId
    Where  : 
           + $LineId(0-7)
           + $AdjMode(0-1)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS pointer generator. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5adjcntpgperstsram_Base                                                             0x03080

/*--------------------------------------
BitField Name: Tfi5StsPgPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [11] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_tfi5adjcntpgperstsram_Tfi5StsPgPtAdjCnt_Mask                                             cBit17_0
#define cAf6_tfi5adjcntpgperstsram_Tfi5StsPgPtAdjCnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC Automatically Concatenation Detection - TFI5 Side
Reg Addr   : 0x02040 - 0x02e6f
Reg Formula: 0x02040 + 512*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
This is the Automatically Concatenation Detection Status at Rx Pointer Interpreter

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5rxstsconcdetreg_Base                                                               0x02040

/*--------------------------------------
BitField Name: Tfi5SpiConDetVc416C
BitField Type: RO
BitField Desc: This is the indicator that this STS is member of 1 VC4_16C, which
is automatically detected.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_tfi5rxstsconcdetreg_Tfi5SpiConDetVc416C_Mask                                               cBit11
#define cAf6_tfi5rxstsconcdetreg_Tfi5SpiConDetVc416C_Shift                                                  11

/*--------------------------------------
BitField Name: Tfi5SpiConDetVc44C
BitField Type: RO
BitField Desc: This is the indicator that this STS is member of 1 VC4_4C, which
is automatically detected.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_tfi5rxstsconcdetreg_Tfi5SpiConDetVc44C_Mask                                                cBit10
#define cAf6_tfi5rxstsconcdetreg_Tfi5SpiConDetVc44C_Shift                                                   10

/*--------------------------------------
BitField Name: Tfi5SpiConDetVc4
BitField Type: RO
BitField Desc: This is the indicator that this STS is member of 1 VC4, which is
automatically detected.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tfi5rxstsconcdetreg_Tfi5SpiConDetVc4_Mask                                                   cBit9
#define cAf6_tfi5rxstsconcdetreg_Tfi5SpiConDetVc4_Shift                                                      9

/*--------------------------------------
BitField Name: Tfi5SpiConDetSlvInd
BitField Type: RO
BitField Desc: This is the indicator that this STS is slaver or master which is
automatically detected. 1: Slaver.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tfi5rxstsconcdetreg_Tfi5SpiConDetSlvInd_Mask                                                cBit8
#define cAf6_tfi5rxstsconcdetreg_Tfi5SpiConDetSlvInd_Shift                                                   8

/*--------------------------------------
BitField Name: Tfi5SpiConDetMstId
BitField Type: RO
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1 which is automatically detected.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_tfi5rxstsconcdetreg_Tfi5SpiConDetMstId_Mask                                               cBit5_0
#define cAf6_tfi5rxstsconcdetreg_Tfi5SpiConDetMstId_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx STS/VC Automatically Concatenation Detection - TFI5 Side
Reg Addr   : 0x03040 - 0x03e6f
Reg Formula: 0x03040 + 512*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
This is the Automatically Concatenation Detection Status at Tx Pointer Generator based on Concatenation indicator from SXC.

------------------------------------------------------------------------------*/
#define cAf6Reg_tfi5txstsconcdetreg_Base                                                               0x03040

/*--------------------------------------
BitField Name: Tfi5SpgConDetVc416C
BitField Type: RO
BitField Desc: This is the indicator that this STS is member of 1 VC4_16C, which
is automatically detected.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_tfi5txstsconcdetreg_Tfi5SpgConDetVc416C_Mask                                               cBit11
#define cAf6_tfi5txstsconcdetreg_Tfi5SpgConDetVc416C_Shift                                                  11

/*--------------------------------------
BitField Name: Tfi5SpgConDetVc44C
BitField Type: RO
BitField Desc: This is the indicator that this STS is member of 1 VC4_4C, which
is automatically detected.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_tfi5txstsconcdetreg_Tfi5SpgConDetVc44C_Mask                                                cBit10
#define cAf6_tfi5txstsconcdetreg_Tfi5SpgConDetVc44C_Shift                                                   10

/*--------------------------------------
BitField Name: Tfi5SpgConDetVc4
BitField Type: RO
BitField Desc: This is the indicator that this STS is member of 1 VC4, which is
automatically detected.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tfi5txstsconcdetreg_Tfi5SpgConDetVc4_Mask                                                   cBit9
#define cAf6_tfi5txstsconcdetreg_Tfi5SpgConDetVc4_Shift                                                      9

/*--------------------------------------
BitField Name: Tfi5SpgConDetSlvInd
BitField Type: RW
BitField Desc: This is the indicator that this STS is slaver or master which is
automatically detected. 1: Slaver.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tfi5txstsconcdetreg_Tfi5SpgConDetSlvInd_Mask                                                cBit8
#define cAf6_tfi5txstsconcdetreg_Tfi5SpgConDetSlvInd_Shift                                                   8

/*--------------------------------------
BitField Name: Tfi5SpgConDetMstId
BitField Type: RO
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1 which is automatically detected.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_tfi5txstsconcdetreg_Tfi5SpgConDetMstId_Mask                                               cBit5_0
#define cAf6_tfi5txstsconcdetreg_Tfi5SpgConDetMstId_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer Control
Reg Addr   : 0x20000
Reg Formula: 0x20000 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
This is the global configuration register for the Rx Framer

------------------------------------------------------------------------------*/
#define cAf6Reg_glbrfm_reg_Base                                                                        0x20000

/*--------------------------------------
BitField Name: RxFrmSts192CEnb
BitField Type: RW
BitField Desc: Enable mode STS192C 1: Enable 0: Disable
BitField Bits: [25]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmSts192CEnb_Mask                                                            cBit25
#define cAf6_glbrfm_reg_RxFrmSts192CEnb_Shift                                                               25

/*--------------------------------------
BitField Name: RxFrmOc192Enb
BitField Type: RW
BitField Desc: Enable mode OC192 1: Enable 0: Disable
BitField Bits: [24]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmOc192Enb_Mask                                                              cBit24
#define cAf6_glbrfm_reg_RxFrmOc192Enb_Shift                                                                 24

/*--------------------------------------
BitField Name: RxFrmDescrEn
BitField Type: RW
BitField Desc: Enable/disable de-scrambling of the Rx coming data stream. 1:
Enable 0: Disable
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmDescrEn_Mask                                                            cBit23_16
#define cAf6_glbrfm_reg_RxFrmDescrEn_Shift                                                                  16

/*--------------------------------------
BitField Name: RxFrmStmOcnMode
BitField Type: RW
BitField Desc: STM rate mode for Rx of  8 lines, each line use 2 bits.Bits[1:0]
for line 0 0: OC48 1: OC12 2: OC3 3: OC1
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_glbrfm_reg_RxFrmStmOcnMode_Mask                                                          cBit15_0
#define cAf6_glbrfm_reg_RxFrmStmOcnMode_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer LOS Detecting Control 1
Reg Addr   : 0x20006
Reg Formula: 0x20006 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
This is the global configuration register for the Rx Framer LOS detecting 1

------------------------------------------------------------------------------*/
#define cAf6Reg_glbclkmon_reg_Base                                                                     0x20006

/*--------------------------------------
BitField Name: RxFrmLosDetMod
BitField Type: RW
BitField Desc: Detected LOS mode. 1: the LOS declare when all zero or all one
detected 0: the LOS declare when all zero detected
BitField Bits: [18]
--------------------------------------*/
#define cAf6_glbclkmon_reg_RxFrmLosDetMod_Mask                                                          cBit18
#define cAf6_glbclkmon_reg_RxFrmLosDetMod_Shift                                                             18

/*--------------------------------------
BitField Name: RxFrmLosDetDis
BitField Type: RW
BitField Desc: Disable detect LOS. 1: Disable 0: Enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_glbclkmon_reg_RxFrmLosDetDis_Mask                                                          cBit17
#define cAf6_glbclkmon_reg_RxFrmLosDetDis_Shift                                                             17

/*--------------------------------------
BitField Name: RxFrmClkMonDis
BitField Type: RW
BitField Desc: Disable to generate LOS to Rx Framer if detecting error on Rx
line clock. 1: Disable 0: Enable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glbclkmon_reg_RxFrmClkMonDis_Mask                                                          cBit16
#define cAf6_glbclkmon_reg_RxFrmClkMonDis_Shift                                                             16

/*--------------------------------------
BitField Name: RxFrmClkMonThr
BitField Type: RW
BitField Desc: Threshold to generate LOS to Rx Framer if detecting error on Rx
line clock.(Threshold = PPM * 155.52/8). Default 0x3cc ~ 50ppm
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_glbclkmon_reg_RxFrmClkMonThr_Mask                                                        cBit15_0
#define cAf6_glbclkmon_reg_RxFrmClkMonThr_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer LOS Detecting Control 2
Reg Addr   : 0x20007
Reg Formula: 0x20007 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
This is the global configuration register for the Rx Framer LOS detecting 2

------------------------------------------------------------------------------*/
#define cAf6Reg_glbdetlos_reg_Base                                                                     0x20007

/*--------------------------------------
BitField Name: RxFrmLosClr2Thr
BitField Type: RW
BitField Desc: Configure the period of time during which there is no period of
consecutive data without transition, used for reset no transition counter. The
recommended value is 0x18 (~2.5us).
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_glbdetlos_reg_RxFrmLosClr2Thr_Mask                                                      cBit31_24
#define cAf6_glbdetlos_reg_RxFrmLosClr2Thr_Shift                                                            24

/*--------------------------------------
BitField Name: RxFrmLosSetThr
BitField Type: RW
BitField Desc: Configure the value that define the period of time in which there
is no transition detected from incoming data from SERDES. The recommended value
is 0x1e6 (~50us).
BitField Bits: [23:12]
--------------------------------------*/
#define cAf6_glbdetlos_reg_RxFrmLosSetThr_Mask                                                       cBit23_12
#define cAf6_glbdetlos_reg_RxFrmLosSetThr_Shift                                                             12

/*--------------------------------------
BitField Name: RxFrmLosClr1Thr
BitField Type: RW
BitField Desc: Configure the period of time during which there is no period of
consecutive data without transition, used for counting at INFRM to change state
to LOS The recommended value is 0x3cc (~50us).
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_glbdetlos_reg_RxFrmLosClr1Thr_Mask                                                       cBit11_0
#define cAf6_glbdetlos_reg_RxFrmLosClr1Thr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer LOF Threshold
Reg Addr   : 0x20008
Reg Formula: 0x20008 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure thresholds for LOF detection at receive SONET/SDH framer,There are two thresholds

------------------------------------------------------------------------------*/
#define cAf6Reg_glblofthr_reg_Base                                                                     0x20008

/*--------------------------------------
BitField Name: RxFrmLofDetMod
BitField Type: RW
BitField Desc: Detected LOF mode.Set 1 to clear OOF counter when state into
INFRAMED
BitField Bits: [16]
--------------------------------------*/
#define cAf6_glblofthr_reg_RxFrmLofDetMod_Mask                                                          cBit16
#define cAf6_glblofthr_reg_RxFrmLofDetMod_Shift                                                             16

/*--------------------------------------
BitField Name: RxFrmLofSetThr
BitField Type: RW
BitField Desc: Configure the OOF time counter threshold for entering LOF state.
Resolution of this threshold is one frame.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_glblofthr_reg_RxFrmLofSetThr_Mask                                                        cBit15_8
#define cAf6_glblofthr_reg_RxFrmLofSetThr_Shift                                                              8

/*--------------------------------------
BitField Name: RxFrmLofClrThr
BitField Type: RW
BitField Desc: Configure the In-frame time counter threshold for exiting LOF
state. Resolution of this threshold is one frame.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_glblofthr_reg_RxFrmLofClrThr_Mask                                                         cBit7_0
#define cAf6_glblofthr_reg_RxFrmLofClrThr_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer - AIS dowstream enable
Reg Addr   : 0x2000d
Reg Formula: 0x2000d + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Enable/disable for AIS dowstream at receive SONET/SDH framer of 8 lines, each line used 4bits,Bits[3:0] for line 0

------------------------------------------------------------------------------*/
#define cAf6Reg_glbrfmaisfwd_reg_Base                                                                  0x2000d

/*--------------------------------------
BitField Name: RxFrmAisFwdEn2_8
BitField Type: RW
BitField Desc: Enable/disable for AIS dowstream at receive SONET/SDH framer of
line2-8, each line used 4bits,Bits[3:0] for line 0
BitField Bits: [31:4]
--------------------------------------*/
#define cAf6_glbrfmaisfwd_reg_RxFrmAisFwdEn2_8_Mask                                                   cBit31_4
#define cAf6_glbrfmaisfwd_reg_RxFrmAisFwdEn2_8_Shift                                                         4

/*--------------------------------------
BitField Name: RxFrmAislAisPEn1
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when AIS_L condition detected. 1: Enable 0: Disable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_glbrfmaisfwd_reg_RxFrmAislAisPEn1_Mask                                                      cBit3
#define cAf6_glbrfmaisfwd_reg_RxFrmAislAisPEn1_Shift                                                         3

/*--------------------------------------
BitField Name: RxFrmTimAisPEn1
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when TIM condition detected. 1: Enable 0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_glbrfmaisfwd_reg_RxFrmTimAisPEn1_Mask                                                       cBit2
#define cAf6_glbrfmaisfwd_reg_RxFrmTimAisPEn1_Shift                                                          2

/*--------------------------------------
BitField Name: RxFrmLofAisPEn1
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when LOF condition detected. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_glbrfmaisfwd_reg_RxFrmLofAisPEn1_Mask                                                       cBit1
#define cAf6_glbrfmaisfwd_reg_RxFrmLofAisPEn1_Shift                                                          1

/*--------------------------------------
BitField Name: RxFrmLosAisPEn1
BitField Type: RW
BitField Desc: Enable/disable the insertion of P-AIS at STS pointer interpreter
when LOS condition detected. 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_glbrfmaisfwd_reg_RxFrmLosAisPEn1_Mask                                                       cBit0
#define cAf6_glbrfmaisfwd_reg_RxFrmLosAisPEn1_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer - RDI Back toward enable
Reg Addr   : 0x2000e
Reg Formula: 0x2000e + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Enable/disable for RDI Back toward at transceive SONET/SDH framer of 8 lines, each line used 4bits,Bits[3:0] for line 0

------------------------------------------------------------------------------*/
#define cAf6Reg_glbrfmrdibwd_reg_Base                                                                  0x2000e

/*--------------------------------------
BitField Name: RxFrmAisFwdEn2_8
BitField Type: RW
BitField Desc: Enable/disable for RDI Back toward at transceive SONET/SDH framer
of line2-8, each line used 4bits,Bits[3:0] for line 0
BitField Bits: [31:4]
--------------------------------------*/
#define cAf6_glbrfmrdibwd_reg_RxFrmAisFwdEn2_8_Mask                                                   cBit31_4
#define cAf6_glbrfmrdibwd_reg_RxFrmAisFwdEn2_8_Shift                                                         4

/*--------------------------------------
BitField Name: RxFrmAislRdiLEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when AIS_L
condition detected. 1: Enable 0: Disable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_glbrfmrdibwd_reg_RxFrmAislRdiLEn_Mask                                                       cBit3
#define cAf6_glbrfmrdibwd_reg_RxFrmAislRdiLEn_Shift                                                          3

/*--------------------------------------
BitField Name: RxFrmTimRdiLEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when TIM
condition detected. 1: Enable 0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_glbrfmrdibwd_reg_RxFrmTimRdiLEn_Mask                                                        cBit2
#define cAf6_glbrfmrdibwd_reg_RxFrmTimRdiLEn_Shift                                                           2

/*--------------------------------------
BitField Name: RxFrmLofRdiLEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when LOF
condition detected. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_glbrfmrdibwd_reg_RxFrmLofRdiLEn_Mask                                                        cBit1
#define cAf6_glbrfmrdibwd_reg_RxFrmLofRdiLEn_Shift                                                           1

/*--------------------------------------
BitField Name: RxFrmLosRdiLEn
BitField Type: RW
BitField Desc: Enable/disable the insertion of RDI-L at TOH insertion when LOS
condition detected. 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_glbrfmrdibwd_reg_RxFrmLosRdiLEn_Mask                                                        cBit0
#define cAf6_glbrfmrdibwd_reg_RxFrmLosRdiLEn_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Framer Control
Reg Addr   : 0x20001
Reg Formula: 0x20001 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
This is the global configuration register for the Tx Framer

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtfm_reg_Base                                                                        0x20001

/*--------------------------------------
BitField Name: TxFrmSts192CEnb
BitField Type: RW
BitField Desc: Enable mode STS192C 1: Enable 0: Disable
BitField Bits: [25]
--------------------------------------*/
#define cAf6_glbtfm_reg_TxFrmSts192CEnb_Mask                                                            cBit25
#define cAf6_glbtfm_reg_TxFrmSts192CEnb_Shift                                                               25

/*--------------------------------------
BitField Name: TxFrmOc192Enb
BitField Type: RW
BitField Desc: Enable mode OC192 1: Enable 0: Disable
BitField Bits: [24]
--------------------------------------*/
#define cAf6_glbtfm_reg_TxFrmOc192Enb_Mask                                                              cBit24
#define cAf6_glbtfm_reg_TxFrmOc192Enb_Shift                                                                 24

/*--------------------------------------
BitField Name: TxFrmScrEn
BitField Type: RW
BitField Desc: Enable/disable scrambling of the Tx data stream. 1: Enable 0:
Disable
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_glbtfm_reg_TxFrmScrEn_Mask                                                              cBit23_16
#define cAf6_glbtfm_reg_TxFrmScrEn_Shift                                                                    16

/*--------------------------------------
BitField Name: TxFrmStmOcnMode
BitField Type: RW
BitField Desc: STM rate mode for Tx of  8 lines, each line use 2 bits.Bits[1:0]
for line 0 0: OC48 1: OC12 2: OC3 3: OC1
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_glbtfm_reg_TxFrmStmOcnMode_Mask                                                          cBit15_0
#define cAf6_glbtfm_reg_TxFrmStmOcnMode_Shift                                                                0


/*----------------------------------------
#1.3.1.1.2 OCN Global Rx Framer 8K RefOut AIS_L Squelching Disable
Register Full Name: OCN Global Rx Framer 8K RefOut AIS_L Squelching Disable
RTL Instant Name    : glb8kaislsquel_reg
Address    : 0x20002
Formula  : Address + 65536*GroupID
Where    : {$GroupID(0-1)}
Description: This is the configuration register for 8K RefOut AIS_L Squelching
Width: 32
Field: [31:08]     %% unused             %% unused %% RW  %% 0x0 %% 0x0
Field: [07:0]     %% RxFrm8KAisLSquel    %% Disable Squelching 8K RefOut when AIS_L is detected of  8 lines, Bits[0] for line 0
------------------------------------------*/
#define cAf6_glb8kaislsquel_reg_Base 0x20002

/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Framer Control
Reg Addr   : 0x2000a
Reg Formula: 0x2000a + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
This is the global configuration register for error forcing to Tx Framer

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtfmfrc_reg_Base                                                                     0x2000a

/*--------------------------------------
BitField Name: TxLineOofFrc
BitField Type: RW
BitField Desc: Enable/disable force OOF      for 8 lines. Bit[0] for line 0.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_glbtfmfrc_reg_TxLineOofFrc_Mask                                                          cBit15_8
#define cAf6_glbtfmfrc_reg_TxLineOofFrc_Shift                                                                8

/*--------------------------------------
BitField Name: TxLineB1ErrFrc
BitField Type: RW
BitField Desc: Enable/disable force B1 error for 8 lines. Bit[0] for line 0.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_glbtfmfrc_reg_TxLineB1ErrFrc_Mask                                                         cBit7_0
#define cAf6_glbtfmfrc_reg_TxLineB1ErrFrc_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Framer RDI-L Insertion Threshold Control
Reg Addr   : 0x20009
Reg Formula: 0x20009 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure the number of frames in which L-RDI will be inserted when an L-RDI event is triggered. And Pattern to insert into Z0 bytes. The register contains two numbers

------------------------------------------------------------------------------*/
#define cAf6Reg_glbrdiinsthr_reg_Base                                                                  0x20009

/*--------------------------------------
BitField Name: TxFrmRdiInsThr2
BitField Type: RW
BitField Desc: Threshold 2 for SDH mode
BitField Bits: [28:24]
--------------------------------------*/
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr2_Mask                                                   cBit28_24
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr2_Shift                                                         24

/*--------------------------------------
BitField Name: TxFrmRdiInsThr1
BitField Type: RW
BitField Desc: Threshold 1 for Sonet mode
BitField Bits: [20:16]
--------------------------------------*/
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr1_Mask                                                   cBit20_16
#define cAf6_glbrdiinsthr_reg_TxFrmRdiInsThr1_Shift                                                         16

/*--------------------------------------
BitField Name: TxFrmOhBusDccEnb
BitField Type: RW
BitField Desc: Set 1 to enable insert DCC bytes from OHBUS per 8 lines. Bit[0]
for line 0.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_glbrdiinsthr_reg_TxFrmOhBusDccEnb_Mask                                                   cBit15_8
#define cAf6_glbrdiinsthr_reg_TxFrmOhBusDccEnb_Shift                                                         8

/*--------------------------------------
BitField Name: OCNTxZ0Pat
BitField Type: RW
BitField Desc: Pattern 1 to insert into Z0 bytes.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_glbrdiinsthr_reg_OCNTxZ0Pat_Mask                                                          cBit7_0
#define cAf6_glbrdiinsthr_reg_OCNTxZ0Pat_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer Located Slice Pointer Selection 1
Reg Addr   : 0x20010
Reg Formula: 0x20010 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure location in Slice Pointer for Rx lines. This regester config for line 0-3
Slc12Id#0: STSID(0-47):0,1,2,3,16,17,18,19,32,33,34,35. Slc12Id#1: STSID:4,5,6,7,20,21,22,23,36,37,38,39 ...
Slc3Id#0 : STSID(0-11):0,4,8. Slc3Id#1: STSID:1,5,9 ...

------------------------------------------------------------------------------*/
#define cAf6Reg_glbrxslcppsel1_reg_Base                                                                0x20010

/*--------------------------------------
BitField Name: RxFrmSlcPpEnb3
BitField Type: RW
BitField Desc: Write 1 to enable for Line 3.
BitField Bits: [30]
--------------------------------------*/
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpEnb3_Mask                                                     cBit30
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpEnb3_Shift                                                        30

/*--------------------------------------
BitField Name: RxFrmSlcPpSel3
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Rx line 3, It includes
BitField Bits: [29:24]
--------------------------------------*/
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpSel3_Mask                                                  cBit29_24
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpSel3_Shift                                                        24

/*--------------------------------------
BitField Name: RxFrmSlcPpEnb2
BitField Type: RW
BitField Desc: Write 1 to enable for Line 2.
BitField Bits: [22]
--------------------------------------*/
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpEnb2_Mask                                                     cBit22
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpEnb2_Shift                                                        22

/*--------------------------------------
BitField Name: RxFrmSlcPpSel2
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Rx line 2, It includes
BitField Bits: [21:16]
--------------------------------------*/
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpSel2_Mask                                                  cBit21_16
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpSel2_Shift                                                        16

/*--------------------------------------
BitField Name: RxFrmSlcPpEnb1
BitField Type: RW
BitField Desc: Write 1 to enable for Line 1.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpEnb1_Mask                                                     cBit14
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpEnb1_Shift                                                        14

/*--------------------------------------
BitField Name: RxFrmSlcPpSel1
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Rx line 1, It includes
BitField Bits: [13:8]
--------------------------------------*/
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpSel1_Mask                                                   cBit13_8
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpSel1_Shift                                                         8

/*--------------------------------------
BitField Name: RxFrmSlcPpEnb0
BitField Type: RW
BitField Desc: Write 1 to enable for Line 0.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpEnb0_Mask                                                      cBit6
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpEnb0_Shift                                                         6

/*--------------------------------------
BitField Name: RxFrmSlcPpSel0
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Rx line 0, It includes
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpSel0_Mask                                                    cBit5_0
#define cAf6_glbrxslcppsel1_reg_RxFrmSlcPpSel0_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer Located Slice Pointer Selection 2
Reg Addr   : 0x20011
Reg Formula: 0x20011 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure location in Slice Pointer for Rx lines. This regester config for line 4-7
Slc12Id#0: STSID(0-47):0,1,2,3,16,17,18,19,32,33,34,35. Slc12Id#1: STSID:4,5,6,7,20,21,22,23,36,37,38,39 ...
Slc3Id#0 : STSID(0-11):0,4,8. Slc3Id#1: STSID:1,5,9 ...

------------------------------------------------------------------------------*/
#define cAf6Reg_glbrxslcppsel2_reg_Base                                                                0x20011

/*--------------------------------------
BitField Name: RxFrmSlcPpEnb7
BitField Type: RW
BitField Desc: Write 1 to enable for Line 7.
BitField Bits: [30]
--------------------------------------*/
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpEnb7_Mask                                                     cBit30
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpEnb7_Shift                                                        30

/*--------------------------------------
BitField Name: RxFrmSlcPpSel7
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Rx line 7, It includes
BitField Bits: [29:24]
--------------------------------------*/
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpSel7_Mask                                                  cBit29_24
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpSel7_Shift                                                        24

/*--------------------------------------
BitField Name: RxFrmSlcPpEnb6
BitField Type: RW
BitField Desc: Write 1 to enable for Line 6.
BitField Bits: [22]
--------------------------------------*/
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpEnb6_Mask                                                     cBit22
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpEnb6_Shift                                                        22

/*--------------------------------------
BitField Name: RxFrmSlcPpSel6
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Rx line 6, It includes
BitField Bits: [21:16]
--------------------------------------*/
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpSel6_Mask                                                  cBit21_16
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpSel6_Shift                                                        16

/*--------------------------------------
BitField Name: RxFrmSlcPpEnb5
BitField Type: RW
BitField Desc: Write 1 to enable for Line 5.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpEnb5_Mask                                                     cBit14
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpEnb5_Shift                                                        14

/*--------------------------------------
BitField Name: RxFrmSlcPpSel5
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Rx line 5, It includes
BitField Bits: [13:8]
--------------------------------------*/
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpSel5_Mask                                                   cBit13_8
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpSel5_Shift                                                         8

/*--------------------------------------
BitField Name: RxFrmSlcPpEnb4
BitField Type: RW
BitField Desc: Write 1 to enable for Line 4.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpEnb4_Mask                                                      cBit6
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpEnb4_Shift                                                         6

/*--------------------------------------
BitField Name: RxFrmSlcPpSel4
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Rx line 4, It includes
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpSel4_Mask                                                    cBit5_0
#define cAf6_glbrxslcppsel2_reg_RxFrmSlcPpSel4_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Framer Located Slice Pointer Selection 1
Reg Addr   : 0x20012
Reg Formula: 0x20012 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure location in Slice Pointer for Tx lines. This regester config for line 0-3
Slc12Id#0: STSID(0-47):0,1,2,3,16,17,18,19,32,33,34,35. Slc12Id#1: STSID:4,5,6,7,20,21,22,23,36,37,38,39 ...
Slc3Id#0 : STSID(0-11):0,4,8. Slc3Id#1: STSID:1,5,9 ...

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtxslcppsel1_reg_Base                                                                0x20012

/*--------------------------------------
BitField Name: TxFrmSlcPpEnb3
BitField Type: RW
BitField Desc: Write 1 to enable for Line 3.
BitField Bits: [30]
--------------------------------------*/
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpEnb3_Mask                                                     cBit30
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpEnb3_Shift                                                        30

/*--------------------------------------
BitField Name: TxFrmSlcPpSel3
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Tx line 3, It includes
BitField Bits: [29:24]
--------------------------------------*/
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpSel3_Mask                                                  cBit29_24
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpSel3_Shift                                                        24

/*--------------------------------------
BitField Name: TxFrmSlcPpEnb2
BitField Type: RW
BitField Desc: Write 1 to enable for Line 2.
BitField Bits: [22]
--------------------------------------*/
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpEnb2_Mask                                                     cBit22
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpEnb2_Shift                                                        22

/*--------------------------------------
BitField Name: TxFrmSlcPpSel2
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Tx line 2, It includes
BitField Bits: [21:16]
--------------------------------------*/
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpSel2_Mask                                                  cBit21_16
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpSel2_Shift                                                        16

/*--------------------------------------
BitField Name: TxFrmSlcPpEnb1
BitField Type: RW
BitField Desc: Write 1 to enable for Line 1.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpEnb1_Mask                                                     cBit14
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpEnb1_Shift                                                        14

/*--------------------------------------
BitField Name: TxFrmSlcPpSel1
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Tx line 1, It includes
BitField Bits: [13:8]
--------------------------------------*/
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpSel1_Mask                                                   cBit13_8
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpSel1_Shift                                                         8

/*--------------------------------------
BitField Name: TxFrmSlcPpEnb0
BitField Type: RW
BitField Desc: Write 1 to enable for Line 0.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpEnb0_Mask                                                      cBit6
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpEnb0_Shift                                                         6

/*--------------------------------------
BitField Name: TxFrmSlcPpSel0
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Tx line 0, It includes
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpSel0_Mask                                                    cBit5_0
#define cAf6_glbtxslcppsel1_reg_TxFrmSlcPpSel0_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Framer Located Slice Pointer Selection 2
Reg Addr   : 0x20013
Reg Formula: 0x20013 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure location in Slice Pointer for Tx lines. This regester config for line 4-7
Slc12Id#0: STSID(0-47):0,1,2,3,16,17,18,19,32,33,34,35. Slc12Id#1: STSID:4,5,6,7,20,21,22,23,36,37,38,39 ...
Slc3Id#0 : STSID(0-11):0,4,8. Slc3Id#1: STSID:1,5,9 ...

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtxslcppsel2_reg_Base                                                                0x20013

/*--------------------------------------
BitField Name: TxFrmSlcPpEnb7
BitField Type: RW
BitField Desc: Write 1 to enable for Line 7.
BitField Bits: [30]
--------------------------------------*/
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpEnb7_Mask                                                     cBit30
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpEnb7_Shift                                                        30

/*--------------------------------------
BitField Name: TxFrmSlcPpSel7
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Tx line 7, It includes
BitField Bits: [29:24]
--------------------------------------*/
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpSel7_Mask                                                  cBit29_24
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpSel7_Shift                                                        24

/*--------------------------------------
BitField Name: TxFrmSlcPpEnb6
BitField Type: RW
BitField Desc: Write 1 to enable for Line 6.
BitField Bits: [22]
--------------------------------------*/
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpEnb6_Mask                                                     cBit22
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpEnb6_Shift                                                        22

/*--------------------------------------
BitField Name: TxFrmSlcPpSel6
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Tx line 6, It includes
BitField Bits: [21:16]
--------------------------------------*/
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpSel6_Mask                                                  cBit21_16
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpSel6_Shift                                                        16

/*--------------------------------------
BitField Name: TxFrmSlcPpEnb5
BitField Type: RW
BitField Desc: Write 1 to enable for Line 5.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpEnb5_Mask                                                     cBit14
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpEnb5_Shift                                                        14

/*--------------------------------------
BitField Name: TxFrmSlcPpSel5
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Tx line 5, It includes
BitField Bits: [13:8]
--------------------------------------*/
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpSel5_Mask                                                   cBit13_8
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpSel5_Shift                                                         8

/*--------------------------------------
BitField Name: TxFrmSlcPpEnb4
BitField Type: RW
BitField Desc: Write 1 to enable for Line 4.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpEnb4_Mask                                                      cBit6
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpEnb4_Shift                                                         6

/*--------------------------------------
BitField Name: TxFrmSlcPpSel4
BitField Type: RW
BitField Desc: Configure location in Slice Pointer for Tx line 4, It includes
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpSel4_Mask                                                    cBit5_0
#define cAf6_glbtxslcppsel2_reg_TxFrmSlcPpSel4_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx_TOHBUS K Byte Control
Reg Addr   : 0x20014
Reg Formula: 0x20014 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure TOHBUS for K Bytes at RX OCN.

------------------------------------------------------------------------------*/
#define cAf6Reg_glbrxtohbusctl_reg_Base                                                                0x20014

/*--------------------------------------
BitField Name: RxTohBusKextCtl
BitField Type: RW
BitField Desc: K Byte Configuration for 8 Lines at Rx OCN. Each line use 4
bits[3:0]. Bit[3] :  Unused Bit[2] :  Disable Masking FF to Rx_OHBUS when
Section alarm happen at Rx OCN. Set 1 to disable. Bit[1] :  Enable K_extention
Byte  at Rx OCN. Set 1 to enable. Bit[0] :  Select K_extention Byte is D1_4 byte
or D1_10 byte at Rx OCN. Set 1 to choose D1_4 byte.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_glbrxtohbusctl_reg_RxTohBusKextCtl_Mask                                                  cBit31_0
#define cAf6_glbrxtohbusctl_reg_RxTohBusKextCtl_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global TOHBUS K_Byte Threshold
Reg Addr   : 0x20015
Reg Formula: 0x20015 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure TOHBUS for K_extention Bytes.

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtohbusthr_reg_Base                                                                  0x20015

/*--------------------------------------
BitField Name: RxTohBusKeStbThr
BitField Type: RW
BitField Desc: Stable threshold Configuration for K_extention Byte.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_glbtohbusthr_reg_RxTohBusKeStbThr_Mask                                                   cBit11_8
#define cAf6_glbtohbusthr_reg_RxTohBusKeStbThr_Shift                                                         8

/*--------------------------------------
BitField Name: RxTohBusK2StbThr
BitField Type: RW
BitField Desc: Stable threshold Configuration for K2 Byte.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_glbtohbusthr_reg_RxTohBusK2StbThr_Mask                                                    cBit7_4
#define cAf6_glbtohbusthr_reg_RxTohBusK2StbThr_Shift                                                         4

/*--------------------------------------
BitField Name: RxTohBusK1StbThr
BitField Type: RW
BitField Desc: Stable threshold Configuration for K1 Byte.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_glbtohbusthr_reg_RxTohBusK1StbThr_Mask                                                    cBit3_0
#define cAf6_glbtohbusthr_reg_RxTohBusK1StbThr_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx_TOHBUS K Byte Control
Reg Addr   : 0x20016
Reg Formula: 0x20016 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure TOHBUS for K Bytes at TX OCN.

------------------------------------------------------------------------------*/
#define cAf6Reg_glbtxtohbusctl_reg_Base                                                                0x20016

/*--------------------------------------
BitField Name: TxTohBusKextCtl
BitField Type: RW
BitField Desc: K Byte Configuration for 8 Lines at Tx OCN. Each line use 4
bits[3:0]. Bit[3] :  Unused Bit[2] :  Disable propagation suppression RDI,AIS in
K2[2:0] at Tx OCN. Default is enable this function - when detect RDI/AIS at
K2[2:0] from OHBUS, we must overwrite 3'b000 into K2[2:0]. Set 1 to disable.
Bit[1] :  Enable K_extention Byte  at Tx OCN. Set 1 to enable. Bit[0] :  Select
K_extention Byte is D1_4 byte or D1_10 byte at Tx OCN. Set 1 to choose D1_4
byte.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_glbtxtohbusctl_reg_TxTohBusKextCtl_Mask                                                  cBit31_0
#define cAf6_glbtxtohbusctl_reg_TxTohBusKextCtl_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : OCN STS Pointer Interpreter Per Channel Control
Reg Addr   : 0x22000 - 0x22e2f
Reg Formula: 0x22000 + 65536*GroupID + 512*LineId + StsId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-3)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure for STS pointer interpreter engines of the STS-1/VC-3.
# HDL_PATH		: itfi5ho.irxpp_stspp_inst.irxpp_stspp[0].rxpp_stspiramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_spiramctl_Base                                                                         0x22000

/*--------------------------------------
BitField Name: LineStsPiTerm
BitField Type: RW
BitField Desc: Config that this STS is terminated or not, if this path is
terminated, Number of B3 error (4 bits) & STS-POH defect (3bits-Byte G1[3:1])
will be overwrite into B3 location then transfer through SXC 1: Terminated. 0:
Not terminated.
BitField Bits: [21]
--------------------------------------*/
#define cAf6_spiramctl_LineStsPiTerm_Mask                                                               cBit21
#define cAf6_spiramctl_LineStsPiTerm_Shift                                                                  21

/*--------------------------------------
BitField Name: LinePassThrEnb
BitField Type: RW
BitField Desc: Enable/disable Passthrough mode for BLSR at Rx SPI of FacePlate
Side 1: Enable. 0: Disable.
BitField Bits: [20]
--------------------------------------*/
#define cAf6_spiramctl_LinePassThrEnb_Mask                                                              cBit20
#define cAf6_spiramctl_LinePassThrEnb_Shift                                                                 20

/*--------------------------------------
BitField Name: LinePassThrGrp
BitField Type: RW
BitField Desc: Passthrough Group ID for BLSR at Rx SPI of FacePlate Side.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_spiramctl_LinePassThrGrp_Mask                                                           cBit19_16
#define cAf6_spiramctl_LinePassThrGrp_Shift                                                                 16

/*--------------------------------------
BitField Name: LineStsPiAisCcDetMod
BitField Type: RW
BitField Desc: Configure mode for detecting AIS concatenation. 1: AIS detected
if all of STS-1 (master and slave) is AIS This mode is applied for SDH mode 0:
AIS detected if only STS-1 mater is AIS. This mode is applied for SONET mode
BitField Bits: [15]
--------------------------------------*/
#define cAf6_spiramctl_LineStsPiAisCcDetMod_Mask                                                        cBit15
#define cAf6_spiramctl_LineStsPiAisCcDetMod_Shift                                                           15

/*--------------------------------------
BitField Name: LineStsPiChkLom
BitField Type: RW
BitField Desc: Enable/disable LOM checking. This field will be set to 1 when
payload type of VC3/VC4 includes any VC11/VC12 1: Enable. 0: Disable.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_spiramctl_LineStsPiChkLom_Mask                                                             cBit14
#define cAf6_spiramctl_LineStsPiChkLom_Shift                                                                14

/*--------------------------------------
BitField Name: LineStsPiSSDetPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to compare with the
extracted SS bits from receive direction.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_spiramctl_LineStsPiSSDetPatt_Mask                                                       cBit13_12
#define cAf6_spiramctl_LineStsPiSSDetPatt_Shift                                                             12

/*--------------------------------------
BitField Name: LineStsPiAisFrc
BitField Type: RW
BitField Desc: Forcing FSM to AIS state. 1: Force 0: Not force
BitField Bits: [11]
--------------------------------------*/
#define cAf6_spiramctl_LineStsPiAisFrc_Mask                                                             cBit11
#define cAf6_spiramctl_LineStsPiAisFrc_Shift                                                                11

/*--------------------------------------
BitField Name: LineStsPiSSDetEn
BitField Type: RW
BitField Desc: Enable/disable checking SS bits in STSPI state machine. 1: Enable
0: Disable
BitField Bits: [10]
--------------------------------------*/
#define cAf6_spiramctl_LineStsPiSSDetEn_Mask                                                            cBit10
#define cAf6_spiramctl_LineStsPiSSDetEn_Shift                                                               10

/*--------------------------------------
BitField Name: LineStsPiAdjRule
BitField Type: RW
BitField Desc: Configure the rule for detecting adjustment condition. 1: The n
of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is
selected. This mode is applied for SONET mode
BitField Bits: [9]
--------------------------------------*/
#define cAf6_spiramctl_LineStsPiAdjRule_Mask                                                             cBit9
#define cAf6_spiramctl_LineStsPiAdjRule_Shift                                                                9

/*--------------------------------------
BitField Name: LineStsPiStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master. 1: Slaver.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_spiramctl_LineStsPiStsSlvInd_Mask                                                           cBit8
#define cAf6_spiramctl_LineStsPiStsSlvInd_Shift                                                              8

/*--------------------------------------
BitField Name: LineStsPiStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_spiramctl_LineStsPiStsMstId_Mask                                                          cBit5_0
#define cAf6_spiramctl_LineStsPiStsMstId_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : OCN STS Pointer Generator Per Channel Control
Reg Addr   : 0x23000 - 0x23e2f
Reg Formula: 0x23000 + 65536*GroupID + 512*LineId + StsId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-3)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure for STS pointer Generator engines.
# HDL_PATH		: itfi5ho.itxpp_stspp_inst.itxpp_stspp[0].txpg_stspgctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_spgramctl_Base                                                                         0x23000

/*--------------------------------------
BitField Name: LineStsPgStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master. 1: Slaver.
BitField Bits: [16]
--------------------------------------*/
#define cAf6_spgramctl_LineStsPgStsSlvInd_Mask                                                          cBit16
#define cAf6_spgramctl_LineStsPgStsSlvInd_Shift                                                             16

/*--------------------------------------
BitField Name: LineStsPgStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1.
BitField Bits: [13:8]
--------------------------------------*/
#define cAf6_spgramctl_LineStsPgStsMstId_Mask                                                         cBit13_8
#define cAf6_spgramctl_LineStsPgStsMstId_Shift                                                               8

/*--------------------------------------
BitField Name: LineStsPgB3BipErrFrc
BitField Type: RW
BitField Desc: Forcing B3 Bip error. 1: Force 0: Not force
BitField Bits: [7]
--------------------------------------*/
#define cAf6_spgramctl_LineStsPgB3BipErrFrc_Mask                                                         cBit7
#define cAf6_spgramctl_LineStsPgB3BipErrFrc_Shift                                                            7

/*--------------------------------------
BitField Name: LineStsPgLopFrc
BitField Type: RW
BitField Desc: Forcing LOP. 1: Force 0: Not force
BitField Bits: [6]
--------------------------------------*/
#define cAf6_spgramctl_LineStsPgLopFrc_Mask                                                              cBit6
#define cAf6_spgramctl_LineStsPgLopFrc_Shift                                                                 6

/*--------------------------------------
BitField Name: LineStsPgUeqFrc
BitField Type: RW
BitField Desc: Forcing FSM to UEQ state. 1: Force 0: Not force
BitField Bits: [5]
--------------------------------------*/
#define cAf6_spgramctl_LineStsPgUeqFrc_Mask                                                              cBit5
#define cAf6_spgramctl_LineStsPgUeqFrc_Shift                                                                 5

/*--------------------------------------
BitField Name: LineStsPgAisFrc
BitField Type: RW
BitField Desc: Forcing FSM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_spgramctl_LineStsPgAisFrc_Mask                                                              cBit4
#define cAf6_spgramctl_LineStsPgAisFrc_Shift                                                                 4

/*--------------------------------------
BitField Name: LineStsPgSSInsPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to insert to pointer
value.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_spgramctl_LineStsPgSSInsPatt_Mask                                                         cBit3_2
#define cAf6_spgramctl_LineStsPgSSInsPatt_Shift                                                              2

/*--------------------------------------
BitField Name: LineStsPgSSInsEn
BitField Type: RW
BitField Desc: Enable/disable SS bits insertion. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_spgramctl_LineStsPgSSInsEn_Mask                                                             cBit1
#define cAf6_spgramctl_LineStsPgSSInsEn_Shift                                                                1

/*--------------------------------------
BitField Name: LineStsPgPohIns
BitField Type: RW
BitField Desc: Enable/disable POH Insertion. High to enable insertion of POH.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_spgramctl_LineStsPgPohIns_Mask                                                              cBit0
#define cAf6_spgramctl_LineStsPgPohIns_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC per Alarm Interrupt Status
Reg Addr   : 0x22140 - 0x22f6f
Reg Formula: 0x22140 + 65536*GroupID + 512*LineId + StsId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-3)
           + $StsId(0-47)
Reg Desc   : 
This is the per Alarm interrupt status of STS/VC pointer interpreter.  %%
Each register is used to store 6 sticky bits for 6 alarms in the STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_upstschstkram_Base                                                                     0x22140

/*--------------------------------------
BitField Name: LineStsPiStsConcDetIntr
BitField Type: W1C
BitField Desc: Set to 1 while an Concatenation Detection event is detected at
STS/VC pointer interpreter. This event doesn't raise interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_upstschstkram_LineStsPiStsConcDetIntr_Mask                                                  cBit5
#define cAf6_upstschstkram_LineStsPiStsConcDetIntr_Shift                                                     5

/*--------------------------------------
BitField Name: LineStsPiStsNewDetIntr
BitField Type: W1C
BitField Desc: Set to 1 while an New Pointer Detection event is detected at
STS/VC pointer interpreter. This event doesn't raise interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upstschstkram_LineStsPiStsNewDetIntr_Mask                                                   cBit4
#define cAf6_upstschstkram_LineStsPiStsNewDetIntr_Shift                                                      4

/*--------------------------------------
BitField Name: LineStsPiStsNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF event is detected at STS/VC pointer
interpreter. This event doesn't raise interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upstschstkram_LineStsPiStsNdfIntr_Mask                                                      cBit3
#define cAf6_upstschstkram_LineStsPiStsNdfIntr_Shift                                                         3

/*--------------------------------------
BitField Name: LineStsPiCepUneqStatePChgIntr
BitField Type: W1C
BitField Desc: Set to 1  while there is change in CEP Un-equip Path in the
related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm
Current Status register of the related STS/VC to know the STS/VC whether in CEP
Un-equip Path state or not.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upstschstkram_LineStsPiCepUneqStatePChgIntr_Mask                                            cBit2
#define cAf6_upstschstkram_LineStsPiCepUneqStatePChgIntr_Shift                                               2

/*--------------------------------------
BitField Name: LineStsPiStsAISStateChgIntr
BitField Type: W1C
BitField Desc: Set to 1 while there is change in AIS state in the related STS/VC
to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status
register of the related STS/VC to know the STS/VC whether in AIS state or not.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upstschstkram_LineStsPiStsAISStateChgIntr_Mask                                              cBit1
#define cAf6_upstschstkram_LineStsPiStsAISStateChgIntr_Shift                                                 1

/*--------------------------------------
BitField Name: LineStsPiStsLopStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 to while there is change in LOP state in the related STS/VC
to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status
register of the related STS/VC to know the STS/VC whether in LOP state or not.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upstschstkram_LineStsPiStsLopStateChgIntr_Mask                                              cBit0
#define cAf6_upstschstkram_LineStsPiStsLopStateChgIntr_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC per Alarm Current Status
Reg Addr   : 0x22180 - 0x22faf
Reg Formula: 0x22180 + 65536*GroupID + 512*LineId + StsId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-3)
           + $StsId(0-47)
Reg Desc   : 
This is the per Alarm current status of STS/VC pointer interpreter.  %%
Each register is used to store 3 bits to store current status of 3 alarms in the STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_upstschstaram_Base                                                                     0x22180

/*--------------------------------------
BitField Name: LineStsPiStsCepUneqPCurStatus
BitField Type: RO
BitField Desc: CEP Un-eqip Path current status in the related STS/VC. When it
changes for 0 to 1 or vice versa, the LineStsPiStsCepUeqPStateChgIntr bit in the
OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upstschstaram_LineStsPiStsCepUneqPCurStatus_Mask                                            cBit2
#define cAf6_upstschstaram_LineStsPiStsCepUneqPCurStatus_Shift                                               2

/*--------------------------------------
BitField Name: LineStsPiStsAisCurStatus
BitField Type: RO
BitField Desc: AIS current status in the related STS/VC. When it changes for 0
to 1 or vice versa, the  LineStsPiStsAISStateChgIntr bit in the OCN Rx STS/VC
per Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upstschstaram_LineStsPiStsAisCurStatus_Mask                                                 cBit1
#define cAf6_upstschstaram_LineStsPiStsAisCurStatus_Shift                                                    1

/*--------------------------------------
BitField Name: LineStsPiStsLopCurStatus
BitField Type: RO
BitField Desc: LOP current status in the related STS/VC. When it changes for 0
to 1 or vice versa, the  LineStsPiStsLopStateChgIntr bit in the OCN Rx STS/VC
per Alarm Interrupt Status register of the related STS/VC is set.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upstschstaram_LineStsPiStsLopCurStatus_Mask                                                 cBit0
#define cAf6_upstschstaram_LineStsPiStsLopCurStatus_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter
Reg Addr   : 0x22080 - 0x22eaf
Reg Formula: 0x22080 + 65536*GroupID + 512*LineId + 64*AdjMode + StsId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-3)
           + $AdjMode(0-1)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS/VC pointer interpreter. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntperstsram_Base                                                                   0x22080

/*--------------------------------------
BitField Name: LineRxPpStsPiPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [6] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntperstsram_LineRxPpStsPiPtAdjCnt_Mask                                               cBit17_0
#define cAf6_adjcntperstsram_LineRxPpStsPiPtAdjCnt_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg STS per Alarm Interrupt Status
Reg Addr   : 0x23140 - 0x23f6f
Reg Formula: 0x23140 + 65536*GroupID + 512*LineId + StsId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-3)
           + $StsId(0-47)
Reg Desc   : 
This is the per Alarm interrupt status of STS pointer generator . Each register is used to store 3 sticky bits for 3 alarms

------------------------------------------------------------------------------*/
#define cAf6Reg_stspgstkram_Base                                                                       0x23140

/*--------------------------------------
BitField Name: LineStsPgAisIntr
BitField Type: W1C
BitField Desc: Set to 1 while an AIS status event (at h2pos) is detected at Tx
STS Pointer Generator.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_stspgstkram_LineStsPgAisIntr_Mask                                                           cBit3
#define cAf6_stspgstkram_LineStsPgAisIntr_Shift                                                              3

/*--------------------------------------
BitField Name: LineStsPgNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF status event (at h2pos) is detected at Tx
STS Pointer Generator.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_stspgstkram_LineStsPgNdfIntr_Mask                                                           cBit1
#define cAf6_stspgstkram_LineStsPgNdfIntr_Shift                                                              1


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg STS pointer adjustment per channel counter
Reg Addr   : 0x23080 - 0x23eaf
Reg Formula: 0x23080 + 65536*GroupID + 512*LineId + 64*AdjMode + StsId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-3)
           + $AdjMode(0-1)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS pointer generator. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntpgperstsram_Base                                                                 0x23080

/*--------------------------------------
BitField Name: LineStsPgPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [11] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntpgperstsram_LineStsPgPtAdjCnt_Mask                                                 cBit17_0
#define cAf6_adjcntpgperstsram_LineStsPgPtAdjCnt_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx STS/VC Automatically Concatenation Detection
Reg Addr   : 0x22040 - 0x22e6f
Reg Formula: 0x22040 + 65536*GroupID + 512*LineId + StsId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-3)
           + $StsId(0-47)
Reg Desc   : 
This is the Automatically Concatenation Detection Status at Rx Pointer Interpreter

------------------------------------------------------------------------------*/
#define cAf6Reg_linerxstsconcdetreg_Base                                                               0x22040

/*--------------------------------------
BitField Name: LineSpiConDetVc416C
BitField Type: RO
BitField Desc: This is the indicator that this STS is member of 1 VC4_16C, which
is automatically detected.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_linerxstsconcdetreg_LineSpiConDetVc416C_Mask                                               cBit11
#define cAf6_linerxstsconcdetreg_LineSpiConDetVc416C_Shift                                                  11

/*--------------------------------------
BitField Name: LineSpiConDetVc44C
BitField Type: RO
BitField Desc: This is the indicator that this STS is member of 1 VC4_4C, which
is automatically detected.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_linerxstsconcdetreg_LineSpiConDetVc44C_Mask                                                cBit10
#define cAf6_linerxstsconcdetreg_LineSpiConDetVc44C_Shift                                                   10

/*--------------------------------------
BitField Name: LineSpiConDetVc4
BitField Type: RO
BitField Desc: This is the indicator that this STS is member of 1 VC4, which is
automatically detected.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_linerxstsconcdetreg_LineSpiConDetVc4_Mask                                                   cBit9
#define cAf6_linerxstsconcdetreg_LineSpiConDetVc4_Shift                                                      9

/*--------------------------------------
BitField Name: LineSpiConDetSlvInd
BitField Type: RW
BitField Desc: This is the indicator that this STS is slaver or master which is
automatically detected. 1: Slaver.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_linerxstsconcdetreg_LineSpiConDetSlvInd_Mask                                                cBit8
#define cAf6_linerxstsconcdetreg_LineSpiConDetSlvInd_Shift                                                   8

/*--------------------------------------
BitField Name: LineSpiConDetMstId
BitField Type: RO
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1 which is automatically detected.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_linerxstsconcdetreg_LineSpiConDetMstId_Mask                                               cBit5_0
#define cAf6_linerxstsconcdetreg_LineSpiConDetMstId_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx STS/VC Automatically Concatenation Detection
Reg Addr   : 0x23040 - 0x23e6f
Reg Formula: 0x23040 + 65536*GroupID + 512*LineId + StsId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-3)
           + $StsId(0-47)
Reg Desc   : 
This is the Automatically Concatenation Detection Status at Tx Pointer Generator based on Concatenation indicator from SXC.

------------------------------------------------------------------------------*/
#define cAf6Reg_linetxstsconcdetreg_Base                                                               0x23040

/*--------------------------------------
BitField Name: LineSpgConDetVc416C
BitField Type: RO
BitField Desc: This is the indicator that this STS is member of 1 VC4_16C, which
is automatically detected.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_linetxstsconcdetreg_LineSpgConDetVc416C_Mask                                               cBit11
#define cAf6_linetxstsconcdetreg_LineSpgConDetVc416C_Shift                                                  11

/*--------------------------------------
BitField Name: LineSpgConDetVc44C
BitField Type: RO
BitField Desc: This is the indicator that this STS is member of 1 VC4_4C, which
is automatically detected.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_linetxstsconcdetreg_LineSpgConDetVc44C_Mask                                                cBit10
#define cAf6_linetxstsconcdetreg_LineSpgConDetVc44C_Shift                                                   10

/*--------------------------------------
BitField Name: LineSpgConDetVc4
BitField Type: RO
BitField Desc: This is the indicator that this STS is member of 1 VC4, which is
automatically detected.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_linetxstsconcdetreg_LineSpgConDetVc4_Mask                                                   cBit9
#define cAf6_linetxstsconcdetreg_LineSpgConDetVc4_Shift                                                      9

/*--------------------------------------
BitField Name: LineSpgConDetSlvInd
BitField Type: RW
BitField Desc: This is the indicator that this STS is slaver or master which is
automatically detected. 1: Slaver.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_linetxstsconcdetreg_LineSpgConDetSlvInd_Mask                                                cBit8
#define cAf6_linetxstsconcdetreg_LineSpgConDetSlvInd_Shift                                                   8

/*--------------------------------------
BitField Name: LineSpgConDetMstId
BitField Type: RO
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1 which is automatically detected.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_linetxstsconcdetreg_LineSpgConDetMstId_Mask                                               cBit5_0
#define cAf6_linetxstsconcdetreg_LineSpgConDetMstId_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx Framer Per Line Control
Reg Addr   : 0x21000 - 0x21700
Reg Formula: 0x21000 + 65536*GroupID + 256*LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to configure operation modes for Tx framer engine and APS pattern inserted into APS bytes (K1 and K2 bytes) of the SONET/SDH line being relative with the address of the address of the register.

------------------------------------------------------------------------------*/
#define cAf6Reg_tfmregctl_Base                                                                         0x21000

/*--------------------------------------
BitField Name: OCNTxS1Pat
BitField Type: RW
BitField Desc: Pattern to insert into S1 byte.
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxS1Pat_Mask                                                               cBit31_24
#define cAf6_tfmregctl_OCNTxS1Pat_Shift                                                                     24

/*--------------------------------------
BitField Name: OCNTxApsPat
BitField Type: RW
BitField Desc: Pattern to insert into APS bytes (K1, K2).
BitField Bits: [23:8]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxApsPat_Mask                                                               cBit23_8
#define cAf6_tfmregctl_OCNTxApsPat_Shift                                                                     8

/*--------------------------------------
BitField Name: OCNTxS1LDis
BitField Type: RW
BitField Desc: S1 insertion disable 1: Disable 0: Enable.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxS1LDis_Mask                                                                  cBit7
#define cAf6_tfmregctl_OCNTxS1LDis_Shift                                                                     7

/*--------------------------------------
BitField Name: OCNTxApsDis
BitField Type: RW
BitField Desc: Disable to process APS 1: Disable 0: Enable.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxApsDis_Mask                                                                  cBit6
#define cAf6_tfmregctl_OCNTxApsDis_Shift                                                                     6

/*--------------------------------------
BitField Name: OCNTxReiLDis
BitField Type: RW
BitField Desc: Auto REI_L insertion disable 1: Disable inserting REI_L
automatically. 0: Enable automatically inserting REI_L.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxReiLDis_Mask                                                                 cBit5
#define cAf6_tfmregctl_OCNTxReiLDis_Shift                                                                    5

/*--------------------------------------
BitField Name: OCNTxRdiLDis
BitField Type: RW
BitField Desc: Auto RDI_L insertion disable 1: Disable inserting RDI_L
automatically. 0: Enable automatically inserting RDI_L.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxRdiLDis_Mask                                                                 cBit4
#define cAf6_tfmregctl_OCNTxRdiLDis_Shift                                                                    4

/*--------------------------------------
BitField Name: OCNTxAutoB2Dis
BitField Type: RW
BitField Desc: Auto B2 disable 1: Disable inserting calculated B2 values into B2
positions automatically. 0: Enable automatically inserting calculated B2 values
into B2 positions.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxAutoB2Dis_Mask                                                               cBit3
#define cAf6_tfmregctl_OCNTxAutoB2Dis_Shift                                                                  3

/*--------------------------------------
BitField Name: OCNTxRdiLThresSel
BitField Type: RW
BitField Desc: Select the number of frames being inserted RDI-L defects when
receive direction requests generating RDI-L at transmit direction. 1: Threshold2
is selected. 0: Threshold1 is selected.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxRdiLThresSel_Mask                                                            cBit2
#define cAf6_tfmregctl_OCNTxRdiLThresSel_Shift                                                               2

/*--------------------------------------
BitField Name: OCNTxRdiLFrc
BitField Type: RW
BitField Desc: RDI-L force. 1: Force RDI-L defect into transmit data. 0: Not
force RDI-L
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxRdiLFrc_Mask                                                                 cBit1
#define cAf6_tfmregctl_OCNTxRdiLFrc_Shift                                                                    1

/*--------------------------------------
BitField Name: OCNTxAisLFrc
BitField Type: RW
BitField Desc: AIS-L force. 1: Force AIS-L defect into transmit data. 0: Not
force AIS-L
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxAisLFrc_Mask                                                                 cBit0
#define cAf6_tfmregctl_OCNTxAisLFrc_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx Framer Per Line Control 1
Reg Addr   : 0x21001 - 0x21701
Reg Formula: 0x21001 + 65536*GroupID + 256*LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to configure some RS-OH bytes.

------------------------------------------------------------------------------*/
#define cAf6Reg_tfmregctl1_Base                                                                        0x21001

/*--------------------------------------
BitField Name: OCNTxRsOhUndefPat
BitField Type: RW
BitField Desc: Pattern to insert into undefined RS-OH bytes.
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_tfmregctl1_OCNTxRsOhUndefPat_Mask                                                       cBit31_24
#define cAf6_tfmregctl1_OCNTxRsOhUndefPat_Shift                                                             24

/*--------------------------------------
BitField Name: OCNTxRsOhF1Pat
BitField Type: RW
BitField Desc: Pattern to insert into F1 byte.
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_tfmregctl1_OCNTxRsOhF1Pat_Mask                                                          cBit23_16
#define cAf6_tfmregctl1_OCNTxRsOhF1Pat_Shift                                                                16

/*--------------------------------------
BitField Name: OCNTxRsOhE1Pat
BitField Type: RW
BitField Desc: Pattern to insert into E1 byte.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_tfmregctl1_OCNTxRsOhE1Pat_Mask                                                           cBit15_8
#define cAf6_tfmregctl1_OCNTxRsOhE1Pat_Shift                                                                 8

/*--------------------------------------
BitField Name: OCNTxRsOhZ0Pat
BitField Type: RW
BitField Desc: Pattern to insert into Z0 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tfmregctl1_OCNTxRsOhZ0Pat_Mask                                                            cBit7_0
#define cAf6_tfmregctl1_OCNTxRsOhZ0Pat_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx Framer Per Line Control 2
Reg Addr   : 0x21002 - 0x21702
Reg Formula: 0x21002 + 65536*GroupID + 256*LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to configure some MS-OH bytes.

------------------------------------------------------------------------------*/
#define cAf6Reg_tfmregctl2_Base                                                                        0x21002

/*--------------------------------------
BitField Name: OCNTxMsOhUndefPat
BitField Type: RW
BitField Desc: Pattern to insert into undefined  MS-OH bytes.
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_tfmregctl2_OCNTxMsOhUndefPat_Mask                                                       cBit31_24
#define cAf6_tfmregctl2_OCNTxMsOhUndefPat_Shift                                                             24

/*--------------------------------------
BitField Name: OCNTxMsOhE2Pat
BitField Type: RW
BitField Desc: Pattern to insert into E2 byte.
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_tfmregctl2_OCNTxMsOhE2Pat_Mask                                                          cBit23_16
#define cAf6_tfmregctl2_OCNTxMsOhE2Pat_Shift                                                                16

/*--------------------------------------
BitField Name: OCNTxMsOhZ2Pat
BitField Type: RW
BitField Desc: Pattern to insert into Z2 byte.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_tfmregctl2_OCNTxMsOhZ2Pat_Mask                                                           cBit15_8
#define cAf6_tfmregctl2_OCNTxMsOhZ2Pat_Shift                                                                 8

/*--------------------------------------
BitField Name: OCNTxMsOhZ1Pat
BitField Type: RW
BitField Desc: Pattern to insert into Z1 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tfmregctl2_OCNTxMsOhZ1Pat_Mask                                                            cBit7_0
#define cAf6_tfmregctl2_OCNTxMsOhZ1Pat_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx J0 Insertion Buffer
Reg Addr   : 0x21010 - 0x2171f
Reg Formula: 0x21010 + 65536*GroupID + 256*LineId + DwId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
           + DwId(0-15): Double word (4bytes) ID
Reg Desc   : 
Each register is used to store one J0 double word of in 64-byte message of the SONET/SDH line being relative with the address of the address of the register inserted into J0 positions.

------------------------------------------------------------------------------*/
#define cAf6Reg_tfmj0insctl_Base                                                                       0x21010

/*--------------------------------------
BitField Name: OCNTxJ0
BitField Type: RW
BitField Desc: J0 pattern for insertion.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_tfmj0insctl_OCNTxJ0_Mask                                                                 cBit31_0
#define cAf6_tfmj0insctl_OCNTxJ0_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global K1 Stable Monitoring Threshold Control
Reg Addr   : 0x27000
Reg Formula: 0x27000 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure thresholds for monitoring change of K1 state at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbk1stbthr_reg_Base                                                                0x27000

/*--------------------------------------
BitField Name: TohK1StbThr2
BitField Type: RW
BitField Desc: The second threshold for detecting stable K1 status.
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr2_Mask                                                     cBit10_8
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr2_Shift                                                           8

/*--------------------------------------
BitField Name: TohK1StbThr1
BitField Type: RW
BitField Desc: The first threshold for detecting stable K1 status.
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr1_Mask                                                      cBit2_0
#define cAf6_tohglbk1stbthr_reg_TohK1StbThr1_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global K2 Stable Monitoring Threshold Control
Reg Addr   : 0x27001
Reg Formula: 0x27001 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure thresholds for monitoring change of K2 state at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbk2stbthr_reg_Base                                                                0x27001

/*--------------------------------------
BitField Name: TohK2StbThr2
BitField Type: RW
BitField Desc: The second threshold for detecting stable K2 status.
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr2_Mask                                                     cBit10_8
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr2_Shift                                                           8

/*--------------------------------------
BitField Name: TohK2StbThr1
BitField Type: RW
BitField Desc: The first threshold for detecting stable K2 status.
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr1_Mask                                                      cBit2_0
#define cAf6_tohglbk2stbthr_reg_TohK2StbThr1_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global S1 Stable Monitoring Threshold Control
Reg Addr   : 0x27002
Reg Formula: 0x27002 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure thresholds for monitoring change of S1 state at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbs1stbthr_reg_Base                                                                0x27002

/*--------------------------------------
BitField Name: TohS1StbThr2
BitField Type: RW
BitField Desc: The second threshold for detecting stable S1 status.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr2_Mask                                                     cBit11_8
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr2_Shift                                                           8

/*--------------------------------------
BitField Name: TohS1StbThr1
BitField Type: RW
BitField Desc: The first threshold for detecting stable S1 status.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr1_Mask                                                      cBit3_0
#define cAf6_tohglbs1stbthr_reg_TohS1StbThr1_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global RDI_L Detecting Threshold Control
Reg Addr   : 0x27003
Reg Formula: 0x27003 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure thresholds for detecting RDI_L at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbrdidetthr_reg_Base                                                               0x27003

/*--------------------------------------
BitField Name: TohRdiDetThr2
BitField Type: RW
BitField Desc: The second threshold for detecting RDI_L.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_Mask                                                   cBit11_8
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_Shift                                                         8

/*--------------------------------------
BitField Name: TohRdiDetThr1
BitField Type: RW
BitField Desc: The first threshold for detecting RDI_L.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_Mask                                                    cBit3_0
#define cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global AIS_L Detecting Threshold Control
Reg Addr   : 0x27004
Reg Formula: 0x27004 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure thresholds for detecting AIS_L at TOH monitoring. There are two thresholds for each kind of threshold.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbaisdetthr_reg_Base                                                               0x27004

/*--------------------------------------
BitField Name: TohAisDetThr2
BitField Type: RW
BitField Desc: The second threshold for detecting AIS_L.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr2_Mask                                                   cBit11_8
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr2_Shift                                                         8

/*--------------------------------------
BitField Name: TohAisDetThr1
BitField Type: RW
BitField Desc: The first threshold for detecting AIS_L.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr1_Mask                                                    cBit3_0
#define cAf6_tohglbaisdetthr_reg_TohAisDetThr1_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global K1 Sampling Threshold Control
Reg Addr   : 0x27005
Reg Formula: 0x27005 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure thresholds for sampling K1 bytes to detect APS defect at TOH monitoring. There are two thresholds.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbk1smpthr_reg_Base                                                                0x27005

/*--------------------------------------
BitField Name: TohK1SmpThr2
BitField Type: RW
BitField Desc: The second threshold for sampling K1 to detect APS defect.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_Mask                                                     cBit11_8
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_Shift                                                           8

/*--------------------------------------
BitField Name: TohK1SmpThr1
BitField Type: RW
BitField Desc: The first threshold for sampling K1 to detect APS defect.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_Mask                                                      cBit3_0
#define cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Global Error Counter Control
Reg Addr   : 0x27006
Reg Formula: 0x27006 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure mode for counters in TOH monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglberrcntmod_reg_Base                                                               0x27006

/*--------------------------------------
BitField Name: TohReiErrCntMod
BitField Type: RW
BitField Desc: REI counter Mode. 1: Saturation mode 0: Roll over mode
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohglberrcntmod_reg_TohReiErrCntMod_Mask                                                    cBit2
#define cAf6_tohglberrcntmod_reg_TohReiErrCntMod_Shift                                                       2

/*--------------------------------------
BitField Name: TohB2ErrCntMod
BitField Type: RW
BitField Desc: B2 counter Mode. 1: Saturation mode 0: Roll over mode
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_Mask                                                     cBit1
#define cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_Shift                                                        1

/*--------------------------------------
BitField Name: TohB1ErrCntMod
BitField Type: RW
BitField Desc: B1 counter Mode. 1: Saturation mode 0: Roll over mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohglberrcntmod_reg_TohB1ErrCntMod_Mask                                                     cBit0
#define cAf6_tohglberrcntmod_reg_TohB1ErrCntMod_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Montoring Affect Control
Reg Addr   : 0x27007
Reg Formula: 0x27007 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
Configure affective mode for TOH monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohglbaffen_reg_Base                                                                   0x27007

/*--------------------------------------
BitField Name: TohAisAffStbMon
BitField Type: RW
BitField Desc: AIS affects to Stable monitoring status of the line at which LOF
or LOS is detected 1: Disable 0: Enable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohglbaffen_reg_TohAisAffStbMon_Mask                                                        cBit3
#define cAf6_tohglbaffen_reg_TohAisAffStbMon_Shift                                                           3

/*--------------------------------------
BitField Name: TohAisAffRdilMon
BitField Type: RW
BitField Desc: AIS affects to RDI-L monitoring status of the line at which LOF
or LOS is detected. 1: Disable 0: Enable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohglbaffen_reg_TohAisAffRdilMon_Mask                                                       cBit2
#define cAf6_tohglbaffen_reg_TohAisAffRdilMon_Shift                                                          2

/*--------------------------------------
BitField Name: TohAisAffAislMon
BitField Type: RW
BitField Desc: AIS affects to AIS-L monitoring  status of the line at which LOF
or LOS is detected. 1: Disable 0: Enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohglbaffen_reg_TohAisAffAislMon_Mask                                                       cBit1
#define cAf6_tohglbaffen_reg_TohAisAffAislMon_Shift                                                          1

/*--------------------------------------
BitField Name: TohAisAffErrCnt
BitField Type: RW
BitField Desc: AIS affects to error counters (B1,B2,REI) of the line at which
LOF or LOS is detected. 1: Disable 0: Enable.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohglbaffen_reg_TohAisAffErrCnt_Mask                                                        cBit0
#define cAf6_tohglbaffen_reg_TohAisAffErrCnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring Per Line Control
Reg Addr   : 0x27010 - 0x27017
Reg Formula: 0x27010 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to configure for TOH monitoring engine of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohramctl_Base                                                                         0x27010

/*--------------------------------------
BitField Name: RxAisLFrc
BitField Type: RW
BitField Desc: Rx AIS-L force. 1: Force. 0: Not force
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tohramctl_RxAisLFrc_Mask                                                                    cBit9
#define cAf6_tohramctl_RxAisLFrc_Shift                                                                       9

/*--------------------------------------
BitField Name: B1ErrCntBlkMod
BitField Type: RW
BitField Desc: B1 counter Block Mode for fedding PM. 1: Block count mode 0: Bit
count mode
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tohramctl_B1ErrCntBlkMod_Mask                                                               cBit8
#define cAf6_tohramctl_B1ErrCntBlkMod_Shift                                                                  8

/*--------------------------------------
BitField Name: B2ErrCntBlkMod
BitField Type: RW
BitField Desc: B2 counter Block Mode for fedding PM. 1: Block count mode 0: Bit
count mode
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tohramctl_B2ErrCntBlkMod_Mask                                                               cBit7
#define cAf6_tohramctl_B2ErrCntBlkMod_Shift                                                                  7

/*--------------------------------------
BitField Name: ReiErrCntBlkMod
BitField Type: RW
BitField Desc: REI counter Block Mode for fedding PM. 1: Block count mode 0: Bit
count mode
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tohramctl_ReiErrCntBlkMod_Mask                                                              cBit6
#define cAf6_tohramctl_ReiErrCntBlkMod_Shift                                                                 6

/*--------------------------------------
BitField Name: StbRdiisLThresSel
BitField Type: RW
BitField Desc: Select the thresholds for detecting RDI_L 1: Threshold 2 is
selected 0: Threshold 1 is selected
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tohramctl_StbRdiisLThresSel_Mask                                                            cBit5
#define cAf6_tohramctl_StbRdiisLThresSel_Shift                                                               5

/*--------------------------------------
BitField Name: StbAisLThresSel
BitField Type: RW
BitField Desc: Select the thresholds for detecting AIS_L 1: Threshold 2 is
selected 0: Threshold 1 is selected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tohramctl_StbAisLThresSel_Mask                                                              cBit4
#define cAf6_tohramctl_StbAisLThresSel_Shift                                                                 4

/*--------------------------------------
BitField Name: K2StbMd
BitField Type: RW
BitField Desc: Select K2[7:3] or K2[7:0] to detect validated K2 value 1: K2[7:0]
value is selected 0: K2[7:3] value is selected
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohramctl_K2StbMd_Mask                                                                      cBit3
#define cAf6_tohramctl_K2StbMd_Shift                                                                         3

/*--------------------------------------
BitField Name: StbK1K2ThresSel
BitField Type: RW
BitField Desc: Select the thresholds for detecting stable or non-stable K1/K2
status 1: Threshold 2 is selected 0: Threshold 1 is selected
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohramctl_StbK1K2ThresSel_Mask                                                              cBit2
#define cAf6_tohramctl_StbK1K2ThresSel_Shift                                                                 2

/*--------------------------------------
BitField Name: StbS1ThresSel
BitField Type: RW
BitField Desc: Select the thresholds for detecting stable or non-stable S1
status 1: Threshold 2 is selected 0: Threshold 1 is selected
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohramctl_StbS1ThresSel_Mask                                                                cBit1
#define cAf6_tohramctl_StbS1ThresSel_Shift                                                                   1

/*--------------------------------------
BitField Name: SmpK1ThresSel
BitField Type: RW
BitField Desc: Select the sample threshold for detecting APS defect. 1:
Threshold 2 is selected 0: Threshold 1 is selected
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohramctl_SmpK1ThresSel_Mask                                                                cBit0
#define cAf6_tohramctl_SmpK1ThresSel_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B1 Error Read Only Counter
Reg Addr   : 0x27100 - 0x27107
Reg Formula: 0x27100 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store B1 error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb1errrocnt_Base                                                                     0x27100

/*--------------------------------------
BitField Name: B1ErrRoCnt
BitField Type: RO
BitField Desc: B1 Error Read Only Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohb1errrocnt_B1ErrRoCnt_Mask                                                            cBit22_0
#define cAf6_tohb1errrocnt_B1ErrRoCnt_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B1 Error Read to Clear Counter
Reg Addr   : 0x27140 - 0x27147
Reg Formula: 0x27140 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store B1 error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb1errr2ccnt_Base                                                                    0x27140

/*--------------------------------------
BitField Name: B1ErrR2cCnt
BitField Type: RC
BitField Desc: B1 Error Read To Clear Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohb1errr2ccnt_B1ErrR2cCnt_Mask                                                          cBit22_0
#define cAf6_tohb1errr2ccnt_B1ErrR2cCnt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B1 Block Error Read Only Counter
Reg Addr   : 0x27180 - 0x27187
Reg Formula: 0x27180 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store B1 Block error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb1blkerrrocnt_Base                                                                  0x27180

/*--------------------------------------
BitField Name: B1BlkErrRoCnt
BitField Type: RO
BitField Desc: B1 Block Error Read Only Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohb1blkerrrocnt_B1BlkErrRoCnt_Mask                                                      cBit15_0
#define cAf6_tohb1blkerrrocnt_B1BlkErrRoCnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B1 Block Error Read to Clear Counter
Reg Addr   : 0x271c0 - 0x271c7
Reg Formula: 0x271c0 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store B1 Block error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb1blkerrr2ccnt_Base                                                                 0x271c0

/*--------------------------------------
BitField Name: B1BlkErrR2cCnt
BitField Type: RC
BitField Desc: B1 Block Error Read To Clear Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohb1blkerrr2ccnt_B1BlkErrR2cCnt_Mask                                                    cBit15_0
#define cAf6_tohb1blkerrr2ccnt_B1BlkErrR2cCnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B2 Error Read Only Counter
Reg Addr   : 0x27108 - 0x2710f
Reg Formula: 0x27108 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store B2 error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb2errrocnt_Base                                                                     0x27108

/*--------------------------------------
BitField Name: B2ErrRoCnt
BitField Type: RO
BitField Desc: B2 Error Read Only Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohb2errrocnt_B2ErrRoCnt_Mask                                                            cBit22_0
#define cAf6_tohb2errrocnt_B2ErrRoCnt_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B2 Error Read to Clear Counter
Reg Addr   : 0x27148 - 0x2714f
Reg Formula: 0x27148 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store B2 error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb2errr2ccnt_Base                                                                    0x27148

/*--------------------------------------
BitField Name: B2ErrR2cCnt
BitField Type: RC
BitField Desc: B2 Error Read To Clear Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohb2errr2ccnt_B2ErrR2cCnt_Mask                                                          cBit22_0
#define cAf6_tohb2errr2ccnt_B2ErrR2cCnt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B2 Block Error Read Only Counter
Reg Addr   : 0x27188 - 0x2718f
Reg Formula: 0x27188 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store B2 Block error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb2blkerrrocnt_Base                                                                  0x27188

/*--------------------------------------
BitField Name: B2BlkErrRoCnt
BitField Type: RO
BitField Desc: B2 Block Error Read Only Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohb2blkerrrocnt_B2BlkErrRoCnt_Mask                                                      cBit15_0
#define cAf6_tohb2blkerrrocnt_B2BlkErrRoCnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring B2 Block Error Read to Clear Counter
Reg Addr   : 0x271c8 - 0x271cf
Reg Formula: 0x271c8 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store B2 Block error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohb2blkerrr2ccnt_Base                                                                 0x271c8

/*--------------------------------------
BitField Name: B2BlkErrR2cCnt
BitField Type: RC
BitField Desc: B2 Block Error Read To Clear Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohb2blkerrr2ccnt_B2BlkErrR2cCnt_Mask                                                    cBit15_0
#define cAf6_tohb2blkerrr2ccnt_B2BlkErrR2cCnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring REI Error Read Only Counter
Reg Addr   : 0x27128 - 0x2712f
Reg Formula: 0x27128 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store REI error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohreierrrocnt_Base                                                                    0x27128

/*--------------------------------------
BitField Name: ReiErrRoCnt
BitField Type: RO
BitField Desc: REI Error Read Only Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohreierrrocnt_ReiErrRoCnt_Mask                                                          cBit22_0
#define cAf6_tohreierrrocnt_ReiErrRoCnt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring REI Error Read to Clear Counter
Reg Addr   : 0x27168 - 0x2716f
Reg Formula: 0x27168 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store REI error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohreierrr2ccnt_Base                                                                   0x27168

/*--------------------------------------
BitField Name: ReiErrR2cCnt
BitField Type: RC
BitField Desc: REI Error Read To Clear Counter.
BitField Bits: [22:0]
--------------------------------------*/
#define cAf6_tohreierrr2ccnt_ReiErrR2cCnt_Mask                                                        cBit22_0
#define cAf6_tohreierrr2ccnt_ReiErrR2cCnt_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring REI Block Error Read Only Counter
Reg Addr   : 0x271a8 - 0x271af
Reg Formula: 0x271a8 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store REI Block error read only counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohreiblkerrrocnt_Base                                                                 0x271a8

/*--------------------------------------
BitField Name: ReiBlkErrRoCnt
BitField Type: RO
BitField Desc: REI Block Error Read Only Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohreiblkerrrocnt_ReiBlkErrRoCnt_Mask                                                    cBit15_0
#define cAf6_tohreiblkerrrocnt_ReiBlkErrRoCnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH Monitoring REI Block Error Read to Clear Counter
Reg Addr   : 0x271e8 - 0x270ef
Reg Formula: 0x271e8 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store REI Block error read to clear counter of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohreiblkerrr2ccnt_Base                                                                0x271e8

/*--------------------------------------
BitField Name: ReiBlkErrR2cCnt
BitField Type: RC
BitField Desc: REI Block Error Read To Clear Counter.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_tohreiblkerrr2ccnt_ReiBlkErrR2cCnt_Mask                                                  cBit15_0
#define cAf6_tohreiblkerrr2ccnt_ReiBlkErrR2cCnt_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH K1 Monitoring Status
Reg Addr   : 0x27110 - 0x27117
Reg Formula: 0x27110 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store K1 Monitoring Status of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohk1monsta_Base                                                                       0x27110

/*--------------------------------------
BitField Name: CurApsDef
BitField Type: RW
BitField Desc: current APS Defect. 1: APS defect is detected from APS bytes. 0:
APS defect is not detected from APS bytes.
BitField Bits: [23]
--------------------------------------*/
#define cAf6_tohk1monsta_CurApsDef_Mask                                                                 cBit23
#define cAf6_tohk1monsta_CurApsDef_Shift                                                                    23

/*--------------------------------------
BitField Name: K1SmpCnt
BitField Type: RW
BitField Desc: Sampling counter.
BitField Bits: [22:19]
--------------------------------------*/
#define cAf6_tohk1monsta_K1SmpCnt_Mask                                                               cBit22_19
#define cAf6_tohk1monsta_K1SmpCnt_Shift                                                                     19

/*--------------------------------------
BitField Name: SameK1Cnt
BitField Type: RW
BitField Desc: The number of same contiguous K1 bytes. It is held at StbK1Thr
value when the number of same contiguous K1 bytes is equal to or more than the
StbK1Thr value.In this case, K1 bytes are stable.
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_tohk1monsta_SameK1Cnt_Mask                                                              cBit18_16
#define cAf6_tohk1monsta_SameK1Cnt_Shift                                                                    16

/*--------------------------------------
BitField Name: K1StbVal
BitField Type: RW
BitField Desc: Stable K1 value. It is updated when detecting a new stable value.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_tohk1monsta_K1StbVal_Mask                                                                cBit15_8
#define cAf6_tohk1monsta_K1StbVal_Shift                                                                      8

/*--------------------------------------
BitField Name: K1CurVal
BitField Type: RW
BitField Desc: Current K1 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohk1monsta_K1CurVal_Mask                                                                 cBit7_0
#define cAf6_tohk1monsta_K1CurVal_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH K2 Monitoring Status
Reg Addr   : 0x27118 - 0x2711f
Reg Formula: 0x27118 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store K2 Monitoring Status of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohk2monsta_Base                                                                       0x27118

/*--------------------------------------
BitField Name: Internal
BitField Type: RW
BitField Desc: Internal.
BitField Bits: [28:21]
--------------------------------------*/
#define cAf6_tohk2monsta_Internal_Mask                                                               cBit28_21
#define cAf6_tohk2monsta_Internal_Shift                                                                     21

/*--------------------------------------
BitField Name: CurAisL
BitField Type: RW
BitField Desc: current AIS-L Defect. 1: AIS-L defect is detected from K2 bytes.
0: AIS-L defect is not detected from K2 bytes.
BitField Bits: [20]
--------------------------------------*/
#define cAf6_tohk2monsta_CurAisL_Mask                                                                   cBit20
#define cAf6_tohk2monsta_CurAisL_Shift                                                                      20

/*--------------------------------------
BitField Name: CurRdiL
BitField Type: RW
BitField Desc: current RDI-L Defect. 1: RDI-L defect is detected from K2 bytes.
0: RDI-L defect is not detected from K2 bytes.
BitField Bits: [19]
--------------------------------------*/
#define cAf6_tohk2monsta_CurRdiL_Mask                                                                   cBit19
#define cAf6_tohk2monsta_CurRdiL_Shift                                                                      19

/*--------------------------------------
BitField Name: SameK2Cnt
BitField Type: RW
BitField Desc: The number of same contiguous K2 bytes. It is held at StbK2Thr
value when the number of same contiguous K2 bytes is equal to or more than the
StbK2Thr value.In this case, K2 bytes are stable.
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_tohk2monsta_SameK2Cnt_Mask                                                              cBit18_16
#define cAf6_tohk2monsta_SameK2Cnt_Shift                                                                    16

/*--------------------------------------
BitField Name: K2StbVal
BitField Type: RW
BitField Desc: Stable K2 value. It is updated when detecting a new stable value.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_tohk2monsta_K2StbVal_Mask                                                                cBit15_8
#define cAf6_tohk2monsta_K2StbVal_Shift                                                                      8

/*--------------------------------------
BitField Name: K2CurVal
BitField Type: RW
BitField Desc: Current K2 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohk2monsta_K2CurVal_Mask                                                                 cBit7_0
#define cAf6_tohk2monsta_K2CurVal_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN TOH S1 Monitoring Status
Reg Addr   : 0x27120 - 0x27127
Reg Formula: 0x27120 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to store S1 Monitoring Status of the related line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohs1monsta_Base                                                                       0x27120

/*--------------------------------------
BitField Name: SameS1Cnt
BitField Type: RW
BitField Desc: The number of same contiguous S1 bytes. It is held at StbS1Thr
value when the number of same contiguous S1 bytes is equal to or more than the
StbS1Thr value.In this case, S1 bytes are stable.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_tohs1monsta_SameS1Cnt_Mask                                                              cBit19_16
#define cAf6_tohs1monsta_SameS1Cnt_Shift                                                                    16

/*--------------------------------------
BitField Name: S1StbVal
BitField Type: RW
BitField Desc: Stable S1 value. It is updated when detecting a new stable value.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_tohs1monsta_S1StbVal_Mask                                                                cBit15_8
#define cAf6_tohs1monsta_S1StbVal_Shift                                                                      8

/*--------------------------------------
BitField Name: S1CurVal
BitField Type: RW
BitField Desc: Current S1 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohs1monsta_S1CurVal_Mask                                                                 cBit7_0
#define cAf6_tohs1monsta_S1CurVal_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Alarm Interrupt Enable Control
Reg Addr   : 0x27080 - 0x27087
Reg Formula: 0x27080 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
This is the per Alarm interrupt enable of Rx framer and TOH monitoring. Each register is used to store 9 bits to enable interrupts when the related alarms in related line happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintperalrenbctl_Base                                                                0x27080

/*--------------------------------------
BitField Name: TcaLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable TCA-L state change event in the related line to
generate an interrupt.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_TcaLStateChgIntrEn_Mask                                                 cBit12
#define cAf6_tohintperalrenbctl_TcaLStateChgIntrEn_Shift                                                    12

/*--------------------------------------
BitField Name: SdLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable SD-L state change event in the related line to
generate an interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_SdLStateChgIntrEn_Mask                                                  cBit11
#define cAf6_tohintperalrenbctl_SdLStateChgIntrEn_Shift                                                     11

/*--------------------------------------
BitField Name: SfLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable SF-L state change event in the related line to
generate an interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_SfLStateChgIntrEn_Mask                                                  cBit10
#define cAf6_tohintperalrenbctl_SfLStateChgIntrEn_Shift                                                     10

/*--------------------------------------
BitField Name: TimLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable TIM-L state change event in the related line to
generate an interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_TimLStateChgIntrEn_Mask                                                  cBit9
#define cAf6_tohintperalrenbctl_TimLStateChgIntrEn_Shift                                                     9

/*--------------------------------------
BitField Name: S1StbStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable S1 Stable state change event in the related line
to generate an interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_S1StbStateChgIntrEn_Mask                                                 cBit8
#define cAf6_tohintperalrenbctl_S1StbStateChgIntrEn_Shift                                                    8

/*--------------------------------------
BitField Name: K1StbStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable K1 Stable state change event in the related line
to generate an interrupt.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_K1StbStateChgIntrEn_Mask                                                 cBit7
#define cAf6_tohintperalrenbctl_K1StbStateChgIntrEn_Shift                                                    7

/*--------------------------------------
BitField Name: ApsLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable APS-L Defect Stable state change event in the
related line to generate an interrupt..
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_ApsLStateChgIntrEn_Mask                                                  cBit6
#define cAf6_tohintperalrenbctl_ApsLStateChgIntrEn_Shift                                                     6

/*--------------------------------------
BitField Name: K2StbStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable K2 Stable state change event in the related line
to generate an interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_K2StbStateChgIntrEn_Mask                                                 cBit5
#define cAf6_tohintperalrenbctl_K2StbStateChgIntrEn_Shift                                                    5

/*--------------------------------------
BitField Name: RdiLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable RDI-L Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_RdiLStateChgIntrEn_Mask                                                  cBit4
#define cAf6_tohintperalrenbctl_RdiLStateChgIntrEn_Shift                                                     4

/*--------------------------------------
BitField Name: AisLStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable AIS-L Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_AisLStateChgIntrEn_Mask                                                  cBit3
#define cAf6_tohintperalrenbctl_AisLStateChgIntrEn_Shift                                                     3

/*--------------------------------------
BitField Name: OofStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable OOF Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_OofStateChgIntrEn_Mask                                                   cBit2
#define cAf6_tohintperalrenbctl_OofStateChgIntrEn_Shift                                                      2

/*--------------------------------------
BitField Name: LofStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable LOF Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_LofStateChgIntrEn_Mask                                                   cBit1
#define cAf6_tohintperalrenbctl_LofStateChgIntrEn_Shift                                                      1

/*--------------------------------------
BitField Name: LosStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable LOS Defect Stable state change event in the
related line to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohintperalrenbctl_LosStateChgIntrEn_Mask                                                   cBit0
#define cAf6_tohintperalrenbctl_LosStateChgIntrEn_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Alarm Interrupt Status
Reg Addr   : 0x27088 - 0x2708f
Reg Formula: 0x27088 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintsta_Base                                                                         0x27088

/*--------------------------------------
BitField Name: TcaLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while TCA-L state change detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_tohintsta_TcaLStateChgIntr_Mask                                                            cBit12
#define cAf6_tohintsta_TcaLStateChgIntr_Shift                                                               12

/*--------------------------------------
BitField Name: SdLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while SD-L state change detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_tohintsta_SdLStateChgIntr_Mask                                                             cBit11
#define cAf6_tohintsta_SdLStateChgIntr_Shift                                                                11

/*--------------------------------------
BitField Name: SfLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while SF-L state change detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_tohintsta_SfLStateChgIntr_Mask                                                             cBit10
#define cAf6_tohintsta_SfLStateChgIntr_Shift                                                                10

/*--------------------------------------
BitField Name: TimLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while Tim-L state change detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tohintsta_TimLStateChgIntr_Mask                                                             cBit9
#define cAf6_tohintsta_TimLStateChgIntr_Shift                                                                9

/*--------------------------------------
BitField Name: S1StbStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 one new stable S1 value detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tohintsta_S1StbStateChgIntr_Mask                                                            cBit8
#define cAf6_tohintsta_S1StbStateChgIntr_Shift                                                               8

/*--------------------------------------
BitField Name: K1StbStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 one new stable K1 value detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tohintsta_K1StbStateChgIntr_Mask                                                            cBit7
#define cAf6_tohintsta_K1StbStateChgIntr_Shift                                                               7

/*--------------------------------------
BitField Name: ApsLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while APS-L Defect state change event happens in the
related line, and it is generated an interrupt if it is enabled.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tohintsta_ApsLStateChgIntr_Mask                                                             cBit6
#define cAf6_tohintsta_ApsLStateChgIntr_Shift                                                                6

/*--------------------------------------
BitField Name: K2StbStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 one new stable K2 value detected in the related line, and
it is generated an interrupt if it is enabled.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tohintsta_K2StbStateChgIntr_Mask                                                            cBit5
#define cAf6_tohintsta_K2StbStateChgIntr_Shift                                                               5

/*--------------------------------------
BitField Name: RdiLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while RDI-L Defect state change event happens in the
related line, and it is generated an interrupt if it is enabled.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tohintsta_RdiLStateChgIntr_Mask                                                             cBit4
#define cAf6_tohintsta_RdiLStateChgIntr_Shift                                                                4

/*--------------------------------------
BitField Name: AisLStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while AIS-L Defect state change event happens in the
related line, and it is generated an interrupt if it is enabled.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohintsta_AisLStateChgIntr_Mask                                                             cBit3
#define cAf6_tohintsta_AisLStateChgIntr_Shift                                                                3

/*--------------------------------------
BitField Name: OofStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while OOF state change event happens in the related line,
and it is generated an interrupt if it is enabled.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohintsta_OofStateChgIntr_Mask                                                              cBit2
#define cAf6_tohintsta_OofStateChgIntr_Shift                                                                 2

/*--------------------------------------
BitField Name: LofStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while LOF state change event happens in the related line,
and it is generated an interrupt if it is enabled.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohintsta_LofStateChgIntr_Mask                                                              cBit1
#define cAf6_tohintsta_LofStateChgIntr_Shift                                                                 1

/*--------------------------------------
BitField Name: LosStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 while LOS state change event happens in the related line,
and it is generated an interrupt if it is enabled.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohintsta_LosStateChgIntr_Mask                                                              cBit0
#define cAf6_tohintsta_LosStateChgIntr_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Alarm Current Status
Reg Addr   : 0x27090 - 0x27097
Reg Formula: 0x27090 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohcursta_Base                                                                         0x27090

/*--------------------------------------
BitField Name: TcaLDefCurStatus
BitField Type: RW
BitField Desc: TCA-L Defect  in the related line. 1: TCA-L defect is set 0:
TCA-L defect is cleared.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_tohcursta_TcaLDefCurStatus_Mask                                                            cBit12
#define cAf6_tohcursta_TcaLDefCurStatus_Shift                                                               12

/*--------------------------------------
BitField Name: SdLDefCurStatus
BitField Type: RW
BitField Desc: SD-L Defect  in the related line. 1: SD-L defect is set 0: SD-L
defect is cleared.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_tohcursta_SdLDefCurStatus_Mask                                                             cBit11
#define cAf6_tohcursta_SdLDefCurStatus_Shift                                                                11

/*--------------------------------------
BitField Name: SfLDefCurStatus
BitField Type: RW
BitField Desc: SF-L Defect  in the related line. 1: SF-L defect is set 0: SF-L
defect is cleared.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_tohcursta_SfLDefCurStatus_Mask                                                             cBit10
#define cAf6_tohcursta_SfLDefCurStatus_Shift                                                                10

/*--------------------------------------
BitField Name: TimLDefCurStatus
BitField Type: RW
BitField Desc: TIM-L Defect  in the related line. 1: TIM-L defect is set 0:
TIM-L defect is cleared.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_tohcursta_TimLDefCurStatus_Mask                                                             cBit9
#define cAf6_tohcursta_TimLDefCurStatus_Shift                                                                9

/*--------------------------------------
BitField Name: S1StbStateChgIntr
BitField Type: RW
BitField Desc: S1 stable status  in the related line.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_tohcursta_S1StbStateChgIntr_Mask                                                            cBit8
#define cAf6_tohcursta_S1StbStateChgIntr_Shift                                                               8

/*--------------------------------------
BitField Name: K1StbCurStatus
BitField Type: RW
BitField Desc: K1 stable status  in the related line.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tohcursta_K1StbCurStatus_Mask                                                               cBit7
#define cAf6_tohcursta_K1StbCurStatus_Shift                                                                  7

/*--------------------------------------
BitField Name: ApsLCurStatus
BitField Type: RW
BitField Desc: APS-L Defect  in the related line. 1: APS-L defect is set 0:
APS-L defect is cleared.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tohcursta_ApsLCurStatus_Mask                                                                cBit6
#define cAf6_tohcursta_ApsLCurStatus_Shift                                                                   6

/*--------------------------------------
BitField Name: K2StbCurStatus
BitField Type: RW
BitField Desc: K2 stable status  in the related line.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tohcursta_K2StbCurStatus_Mask                                                               cBit5
#define cAf6_tohcursta_K2StbCurStatus_Shift                                                                  5

/*--------------------------------------
BitField Name: RdiLDefCurStatus
BitField Type: RW
BitField Desc: RDI-L Defect  in the related line. 1: RDI-L defect is set 0:
RDI-L defect is cleared.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tohcursta_RdiLDefCurStatus_Mask                                                             cBit4
#define cAf6_tohcursta_RdiLDefCurStatus_Shift                                                                4

/*--------------------------------------
BitField Name: AisLDefCurStatus
BitField Type: RW
BitField Desc: AIS-L Defect  in the related line. 1: AIS-L defect is set 0:
AIS-L defect is cleared.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tohcursta_AisLDefCurStatus_Mask                                                             cBit3
#define cAf6_tohcursta_AisLDefCurStatus_Shift                                                                3

/*--------------------------------------
BitField Name: OofCurStatus
BitField Type: RW
BitField Desc: OOF current status in the related line. 1: OOF state 0: Not OOF
state.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tohcursta_OofCurStatus_Mask                                                                 cBit2
#define cAf6_tohcursta_OofCurStatus_Shift                                                                    2

/*--------------------------------------
BitField Name: LofCurStatus
BitField Type: RW
BitField Desc: LOF current status in the related line. 1: LOF state 0: Not LOF
state.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tohcursta_LofCurStatus_Mask                                                                 cBit1
#define cAf6_tohcursta_LofCurStatus_Shift                                                                    1

/*--------------------------------------
BitField Name: LosCurStatus
BitField Type: RW
BitField Desc: LOS current status in the related line. 1: LOS state 0: Not LOS
state.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tohcursta_LosCurStatus_Mask                                                                 cBit0
#define cAf6_tohcursta_LosCurStatus_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line Per Line Interrupt Enable Control
Reg Addr   : 0x2709e
Reg Formula: 0x2709e + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
The register consists of 4 or 8 bits per lines (STM1: 8bits,STM4: 4bits) at Rx side to enable interrupts when alarms of related lines happen. It is noted that this register is higher priority than the OCN Rx Line per Alarm Interrupt Enable Control register.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintperlineenctl_Base                                                                0x2709e

/*--------------------------------------
BitField Name: OCNRxLLineIntrEn1
BitField Type: RW
BitField Desc: Bit #0 to enable for line #0.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohintperlineenctl_OCNRxLLineIntrEn1_Mask                                                 cBit7_0
#define cAf6_tohintperlineenctl_OCNRxLLineIntrEn1_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line per Line Interrupt OR Status
Reg Addr   : 0x2709f
Reg Formula: 0x2709f + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
The register consists of 4 or 8 bits per lines (STM1: 8bits,STM4: 4bits) at Rx side. Each bit is used to store Interrupt OR status of the related line. If there are any bits in this register and they are enabled to raise interrupt, the Rx line level interrupt bit is set.

------------------------------------------------------------------------------*/
#define cAf6Reg_tohintperline_Base                                                                     0x2709f

/*--------------------------------------
BitField Name: OCNRxLLineIntr1
BitField Type: RW
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the OCN Rx Line per Alarm Interrupt Status register of the related STS/VC to be
set and they are enabled to raise interrupt. Bit 0 for line #0, respectively.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohintperline_OCNRxLLineIntr1_Mask                                                        cBit7_0
#define cAf6_tohintperline_OCNRxLLineIntr1_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer Z1 Z2 STS Select Control
Reg Addr   : 0x21003 - 0x21703
Reg Formula: 0x21003 + 65536*GroupID + 256*LineId
    Where  : 
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   : 
Each register is used to configure some RS-OH bytes.

------------------------------------------------------------------------------*/
#define cAf6Reg_rfmz1z2selctl_Base                                                                     0x21003

/*--------------------------------------
BitField Name: OCNRxZ1MonStsSel
BitField Type: RW
BitField Desc: Select only Z1 position to monitor. Six config bits are
BitField Bits: [13:8]
--------------------------------------*/
#define cAf6_rfmz1z2selctl_OCNRxZ1MonStsSel_Mask                                                      cBit13_8
#define cAf6_rfmz1z2selctl_OCNRxZ1MonStsSel_Shift                                                            8

/*--------------------------------------
BitField Name: OCNRxZ2MonStsSel
BitField Type: RW
BitField Desc: Select only Z2 position to monitor. Six config bits are
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_rfmz1z2selctl_OCNRxZ2MonStsSel_Mask                                                       cBit5_0
#define cAf6_rfmz1z2selctl_OCNRxZ2MonStsSel_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line Per Line E1 captured byte
Reg Addr   : 0x27200 - 0x27207
Reg Formula: 0x27200 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + LineId(0-7)
Reg Desc   : 
The register consists value of E1 byte

------------------------------------------------------------------------------*/
#define cAf6Reg_tohe1byte_Base                                                                         0x27200

/*--------------------------------------
BitField Name: OCNRxE1CapByte
BitField Type: RW
BitField Desc: E1 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohe1byte_OCNRxE1CapByte_Mask                                                             cBit7_0
#define cAf6_tohe1byte_OCNRxE1CapByte_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line Per Line F1 captured byte
Reg Addr   : 0x27208 - 0x2720f
Reg Formula: 0x27208 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + LineId(0-7)
Reg Desc   : 
The register consists value of F1 byte

------------------------------------------------------------------------------*/
#define cAf6Reg_tohf1byte_Base                                                                         0x27208

/*--------------------------------------
BitField Name: OCNRxF1CapByte
BitField Type: RW
BitField Desc: F1 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohf1byte_OCNRxF1CapByte_Mask                                                             cBit7_0
#define cAf6_tohf1byte_OCNRxF1CapByte_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line Per Line Z1 captured byte
Reg Addr   : 0x27210 - 0x27217
Reg Formula: 0x27210 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + LineId(0-7)
Reg Desc   : 
The register consists value of Z1 byte

------------------------------------------------------------------------------*/
#define cAf6Reg_tohz1byte_Base                                                                         0x27210

/*--------------------------------------
BitField Name: OCNRxZ1CapByte
BitField Type: RW
BitField Desc: Z1 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohz1byte_OCNRxZ1CapByte_Mask                                                             cBit7_0
#define cAf6_tohz1byte_OCNRxZ1CapByte_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line Per Line Z2 captured byte
Reg Addr   : 0x27218 - 0x2721f
Reg Formula: 0x27218 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + LineId(0-7)
Reg Desc   : 
The register consists value of Z2 byte

------------------------------------------------------------------------------*/
#define cAf6Reg_tohz2byte_Base                                                                         0x27218

/*--------------------------------------
BitField Name: OCNRxZ2CapByte
BitField Type: RW
BitField Desc: Z2 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohz2byte_OCNRxZ2CapByte_Mask                                                             cBit7_0
#define cAf6_tohz2byte_OCNRxZ2CapByte_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Line Per Line E2 captured byte
Reg Addr   : 0x27220 - 0x27227
Reg Formula: 0x27220 + 65536*GroupID + LineId
    Where  : 
           + $GroupID(0-1)
           + LineId(0-7)
Reg Desc   : 
The register consists value of E2 byte

------------------------------------------------------------------------------*/
#define cAf6Reg_tohe2byte_Base                                                                         0x27220

/*--------------------------------------
BitField Name: OCNRxE2CapByte
BitField Type: RW
BitField Desc: E2 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_tohe2byte_OCNRxE2CapByte_Mask                                                             cBit7_0
#define cAf6_tohe2byte_OCNRxE2CapByte_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN SXC Control 1 - Config Page 0 and Page 1 of SXC
Reg Addr   : 0x44000 - 0x4572f
Reg Formula: 0x44000 + 256*LineId + StsId
    Where  : 
           + $LineId(0-23)
           + $StsId(0-47)
Reg Desc   : 
Each register is used for each outgoing STS of any line (24 lines: 0-7:TFI5 side, 8-15: Line Side, 16-23: Loho) can be randomly configured to //connect  to any ingoing STS of any ingoing line (24 lines: 0-7:TFI5 side, 8-15: Line Side, 16-23: Loho). These registers are used for config Pape0 and page1 of SXC.
# HDL_PATH		: isdhsxc.isxc_rxrd[0].rxsxcramctl0.array

------------------------------------------------------------------------------*/
#define cAf6Reg_sxcramctl0_Base                                                                        0x44000

/*--------------------------------------
BitField Name: SxcLineIdPage1
BitField Type: RW
BitField Desc: Contains the ingoing LineID Page1 (0-23: 0-7:TFI5 side, 8-15:
Line Side, 16-23: Loho). Disconnect (output is all one) if value LineID is other
value (recommend is 24).
BitField Bits: [28:24]
--------------------------------------*/
#define cAf6_sxcramctl0_SxcLineIdPage1_Mask                                                          cBit28_24
#define cAf6_sxcramctl0_SxcLineIdPage1_Shift                                                                24

/*--------------------------------------
BitField Name: SxcStsIdPage1
BitField Type: RW
BitField Desc: Contains the ingoing STSID Page1 (0-47).
BitField Bits: [21:16]
--------------------------------------*/
#define cAf6_sxcramctl0_SxcStsIdPage1_Mask                                                           cBit21_16
#define cAf6_sxcramctl0_SxcStsIdPage1_Shift                                                                 16

/*--------------------------------------
BitField Name: SxcLineIdPage0
BitField Type: RW
BitField Desc: Contains the ingoing LineID Page0 (0-23: 0-7:TFI5 side, 8-15:
Line Side, 16-23: Loho). Disconnect (output is all one) if value LineID is other
value (recommend is 24).
BitField Bits: [12:8]
--------------------------------------*/
#define cAf6_sxcramctl0_SxcLineIdPage0_Mask                                                           cBit12_8
#define cAf6_sxcramctl0_SxcLineIdPage0_Shift                                                                 8

/*--------------------------------------
BitField Name: SxcStsIdPage0
BitField Type: RW
BitField Desc: Contains the ingoing STSID Page0 (0-47).
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_sxcramctl0_SxcStsIdPage0_Mask                                                             cBit5_0
#define cAf6_sxcramctl0_SxcStsIdPage0_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN SXC Control 2 - Config Page 2 of SXC
Reg Addr   : 0x44040 - 0x4576f
Reg Formula: 0x44040 + 256*LineId + StsId
    Where  : 
           + $LineId(0-23)
           + $StsId(0-47)
Reg Desc   : 
Each register is used for each outgoing STS of any line (24 lines: 0-7:TFI5 side, 8-15: Line Side, 16-23: Loho) can be randomly configured to //connect  to any ingoing STS of any ingoing line (24 lines: 0-7:TFI5 side, 8-15: Line Side, 16-23: Loho). These registers are used for config Pape2 of SXC.
# HDL_PATH		: isdhsxc.isxc_rxrd[0].rxsxcramctl1.array

------------------------------------------------------------------------------*/
#define cAf6Reg_sxcramctl1_Base                                                                        0x44040

/*--------------------------------------
BitField Name: SxcLineIdPage2
BitField Type: RW
BitField Desc: Contains the ingoing LineID Page2 (0-23: 0-7:TFI5 side, 8-15:
Line Side, 16-23: Loho). Disconnect (output is all one) if value LineID is other
value (recommend is 24).
BitField Bits: [12:8]
--------------------------------------*/
#define cAf6_sxcramctl1_SxcLineIdPage2_Mask                                                           cBit12_8
#define cAf6_sxcramctl1_SxcLineIdPage2_Shift                                                                 8

/*--------------------------------------
BitField Name: SxcStsIdPage2
BitField Type: RW
BitField Desc: Contains the ingoing STSID Page2 (0-47).
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_sxcramctl1_SxcStsIdPage2_Mask                                                             cBit5_0
#define cAf6_sxcramctl1_SxcStsIdPage2_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN SXC Control 3 - Config APS
Reg Addr   : 0x44080 - 0x457af
Reg Formula: 0x44080 + 256*LineId + StsId
    Where  : 
           + $LineId(0-23)
           + $StsId(0-47)
Reg Desc   : 
Each register is used for APS configuration
# HDL_PATH		: isdhsxc.isxc_rxrd[0].apsctl_ram.array

------------------------------------------------------------------------------*/
#define cAf6Reg_apsramctl_Base                                                                         0x44080

/*--------------------------------------
BitField Name: SelectorType
BitField Type: RW
BitField Desc: Selector Type. 3: FSM. Choose based FSM input 2: APS Grouping. 1:
UPSR/SNCP Grouping. 0: Disable APS/UPSR.
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_apsramctl_SelectorType_Mask                                                             cBit13_12
#define cAf6_apsramctl_SelectorType_Shift                                                                   12

/*--------------------------------------
BitField Name: SelectorID
BitField Type: RW
BitField Desc: Contains Selector ID. When Selector Type is: - FSM: Unused this
field. - APS: [8]  : Passthrough Enable for BLSR [7:4]: Passthrough Group ID for
BLSR [3:0]: APS Group ID - UPSR/SNCP: Select UPSR ID.
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_apsramctl_SelectorID_Mask                                                                 cBit8_0
#define cAf6_apsramctl_SelectorID_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx High Order Map concatenate configuration
Reg Addr   : 0x48000 - 0x48e2f
Reg Formula: 0x48000 + 512*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure concatenation for each STS to High Order Map path.
# HDL_PATH		: irxpp_outho_inst.irxpp_outputho [0].ohoramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_rxhomapramctl_Base                                                                     0x48000

/*--------------------------------------
BitField Name: HoMapIwkRxTu3toVc3
BitField Type: RW
BitField Desc: Enable inter working from SDH (TU3) to HO (VC3).
BitField Bits: [9]
--------------------------------------*/
#define cAf6_rxhomapramctl_HoMapIwkRxTu3toVc3_Mask                                                       cBit9
#define cAf6_rxhomapramctl_HoMapIwkRxTu3toVc3_Shift                                                          9

/*--------------------------------------
BitField Name: HoMapStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master. 1: Slaver.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rxhomapramctl_HoMapStsSlvInd_Mask                                                           cBit8
#define cAf6_rxhomapramctl_HoMapStsSlvInd_Shift                                                              8

/*--------------------------------------
BitField Name: HoMapStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_rxhomapramctl_HoMapStsMstId_Mask                                                          cBit5_0
#define cAf6_rxhomapramctl_HoMapStsMstId_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : OCN RXPP Per STS payload Control
Reg Addr   : 0x60000 - 0x7c02f
Reg Formula: 0x60000 + 16384*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure VT payload mode per STS.
# HDL_PATH		: itfi5ho.irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_demramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_demramctl_Base                                                                         0x60000

/*--------------------------------------
BitField Name: PiIwkRxVc3toTu3
BitField Type: RW
BitField Desc: Enable inter working from SDH (VC3) to PDH (TU3) .
BitField Bits: [17]
--------------------------------------*/
#define cAf6_demramctl_PiIwkRxVc3toTu3_Mask                                                             cBit17
#define cAf6_demramctl_PiIwkRxVc3toTu3_Shift                                                                17

/*--------------------------------------
BitField Name: PiDemStsTerm
BitField Type: RW
BitField Desc: Enable to terminate the related STS/VC. It means that STS POH
defects related to the STS/VC to generate AIS to downstream. Must be set to 1.
1: Enable 0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_demramctl_PiDemStsTerm_Mask                                                                cBit16
#define cAf6_demramctl_PiDemStsTerm_Shift                                                                   16

/*--------------------------------------
BitField Name: PiDemSpeType
BitField Type: RW
BitField Desc: Configure types of SPE. 0: Disable processing pointers of all
VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2:
TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_demramctl_PiDemSpeType_Mask                                                             cBit15_14
#define cAf6_demramctl_PiDemSpeType_Shift                                                                   14

/*--------------------------------------
BitField Name: PiDemTug26Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug26Type_Mask                                                           cBit13_12
#define cAf6_demramctl_PiDemTug26Type_Shift                                                                 12

/*--------------------------------------
BitField Name: PiDemTug25Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #5.
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug25Type_Mask                                                           cBit11_10
#define cAf6_demramctl_PiDemTug25Type_Shift                                                                 10

/*--------------------------------------
BitField Name: PiDemTug24Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #4.
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug24Type_Mask                                                             cBit9_8
#define cAf6_demramctl_PiDemTug24Type_Shift                                                                  8

/*--------------------------------------
BitField Name: PiDemTug23Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #3.
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug23Type_Mask                                                             cBit7_6
#define cAf6_demramctl_PiDemTug23Type_Shift                                                                  6

/*--------------------------------------
BitField Name: PiDemTug22Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #2.
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug22Type_Mask                                                             cBit5_4
#define cAf6_demramctl_PiDemTug22Type_Shift                                                                  4

/*--------------------------------------
BitField Name: PiDemTug21Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #1.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug21Type_Mask                                                             cBit3_2
#define cAf6_demramctl_PiDemTug21Type_Shift                                                                  2

/*--------------------------------------
BitField Name: PiDemTug20Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #0.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_demramctl_PiDemTug20Type_Mask                                                             cBit1_0
#define cAf6_demramctl_PiDemTug20Type_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN VTTU Pointer Interpreter Per Channel Control
Reg Addr   : 0x60800 - 0x7cfff
Reg Formula: 0x60800 + 16384*LineId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to configure for VTTU pointer interpreter engines.
# HDL_PATH		: itfi5ho.irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_vpiramctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_vpiramctl_Base                                                                         0x60800

/*--------------------------------------
BitField Name: LineVtPiAisFrcDown
BitField Type: RW
BitField Desc: Forcing AIS to PDH in case VT Remote Loopback. 1: Force 0: Not
force
BitField Bits: [7]
--------------------------------------*/
#define cAf6_vpiramctl_LineVtPiAisFrcDown_Mask                                                           cBit7
#define cAf6_vpiramctl_LineVtPiAisFrcDown_Shift                                                              7

/*--------------------------------------
BitField Name: LineVtPiLocLb
BitField Type: RW
BitField Desc: Enable VT Local Looback - loopback to PDH.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_vpiramctl_LineVtPiLocLb_Mask                                                                cBit6
#define cAf6_vpiramctl_LineVtPiLocLb_Shift                                                                   6

/*--------------------------------------
BitField Name: LineVtPiLoTerm
BitField Type: RW
BitField Desc: Enable to terminate the related VTTU. It means that VTTU POH
defects related to the VTTU to generate AIS to downstream.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_vpiramctl_LineVtPiLoTerm_Mask                                                               cBit5
#define cAf6_vpiramctl_LineVtPiLoTerm_Shift                                                                  5

/*--------------------------------------
BitField Name: LineVtPiAisFrc
BitField Type: RW
BitField Desc: Forcing FSM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_vpiramctl_LineVtPiAisFrc_Mask                                                               cBit4
#define cAf6_vpiramctl_LineVtPiAisFrc_Shift                                                                  4

/*--------------------------------------
BitField Name: LineVtPiSSDetPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to compare with the
extracted SS bits from receive direction.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_vpiramctl_LineVtPiSSDetPatt_Mask                                                          cBit3_2
#define cAf6_vpiramctl_LineVtPiSSDetPatt_Shift                                                               2

/*--------------------------------------
BitField Name: LineVtPiSSDetEn
BitField Type: RW
BitField Desc: Enable/disable checking SS bits in PI State Machine. 1: Enable 0:
Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_vpiramctl_LineVtPiSSDetEn_Mask                                                              cBit1
#define cAf6_vpiramctl_LineVtPiSSDetEn_Shift                                                                 1

/*--------------------------------------
BitField Name: LineVtPiAdjRule
BitField Type: RW
BitField Desc: Configure the rule for detecting adjustment condition. 1: The n
of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is
selected. This mode is applied for SONET mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_vpiramctl_LineVtPiAdjRule_Mask                                                              cBit0
#define cAf6_vpiramctl_LineVtPiAdjRule_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN TXPP Per STS Multiplexing Control
Reg Addr   : 0x80000 - 0x9c02f
Reg Formula: 0x80000 + 16384*LineId + StsId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   : 
Each register is used to configure VT payload mode per STS at Tx pointer generator.
# HDL_PATH		: itfi5ho.itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgdmctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_pgdemramctl_Base                                                                       0x80000

/*--------------------------------------
BitField Name: PgIwkTxTu3toVc3
BitField Type: RW
BitField Desc: Enable inter working from PDH (TU3) to SDH (VC3) .
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pgdemramctl_PgIwkTxTu3toVc3_Mask                                                           cBit17
#define cAf6_pgdemramctl_PgIwkTxTu3toVc3_Shift                                                              17

/*--------------------------------------
BitField Name: PgIwkTxVc3toTu3
BitField Type: RW
BitField Desc: Enable inter working from HO  (VC3) to SDH (TU3) .
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pgdemramctl_PgIwkTxVc3toTu3_Mask                                                           cBit16
#define cAf6_pgdemramctl_PgIwkTxVc3toTu3_Shift                                                              16

/*--------------------------------------
BitField Name: PgDemSpeType
BitField Type: RW
BitField Desc: Configure types of SPE. 0: Disable processing pointers of all
VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2:
TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemSpeType_Mask                                                           cBit15_14
#define cAf6_pgdemramctl_PgDemSpeType_Shift                                                                 14

/*--------------------------------------
BitField Name: PgDemTug26Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug26Type_Mask                                                         cBit13_12
#define cAf6_pgdemramctl_PgDemTug26Type_Shift                                                               12

/*--------------------------------------
BitField Name: PgDemTug25Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #5.
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug25Type_Mask                                                         cBit11_10
#define cAf6_pgdemramctl_PgDemTug25Type_Shift                                                               10

/*--------------------------------------
BitField Name: PgDemTug24Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #4.
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug24Type_Mask                                                           cBit9_8
#define cAf6_pgdemramctl_PgDemTug24Type_Shift                                                                8

/*--------------------------------------
BitField Name: PgDemTug23Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #3.
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug23Type_Mask                                                           cBit7_6
#define cAf6_pgdemramctl_PgDemTug23Type_Shift                                                                6

/*--------------------------------------
BitField Name: PgDemTug22Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #2.
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug22Type_Mask                                                           cBit5_4
#define cAf6_pgdemramctl_PgDemTug22Type_Shift                                                                4

/*--------------------------------------
BitField Name: PgDemTug21Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #1.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug21Type_Mask                                                           cBit3_2
#define cAf6_pgdemramctl_PgDemTug21Type_Shift                                                                2

/*--------------------------------------
BitField Name: PgDemTug20Type
BitField Type: RW
BitField Desc: Configure types of VT/TUs in TUG-2 #0.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pgdemramctl_PgDemTug20Type_Mask                                                           cBit1_0
#define cAf6_pgdemramctl_PgDemTug20Type_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN VTTU Pointer Generator Per Channel Control
Reg Addr   : 0x80800 - 0x9cfff
Reg Formula: 0x80800 + 16384*LineId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to configure for VTTU pointer Generator engines.
# HDL_PATH		: itfi5ho.itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgctl.array

------------------------------------------------------------------------------*/
#define cAf6Reg_vpgramctl_Base                                                                         0x80800

/*--------------------------------------
BitField Name: LineVtPiAisFrcUp
BitField Type: RW
BitField Desc: Forcing AIS to OCN in case VT Local Loopback. 1: Force 0: Not
force
BitField Bits: [9]
--------------------------------------*/
#define cAf6_vpgramctl_LineVtPiAisFrcUp_Mask                                                             cBit9
#define cAf6_vpgramctl_LineVtPiAisFrcUp_Shift                                                                9

/*--------------------------------------
BitField Name: LineVtPiRemLb
BitField Type: RW
BitField Desc: Enable VT Remote Looback - loopback to OCN.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_vpgramctl_LineVtPiRemLb_Mask                                                                cBit8
#define cAf6_vpgramctl_LineVtPiRemLb_Shift                                                                   8

/*--------------------------------------
BitField Name: VtPgBipErrFrc
BitField Type: RW
BitField Desc: Forcing Bip error. 1: Force 0: Not force
BitField Bits: [7]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgBipErrFrc_Mask                                                                cBit7
#define cAf6_vpgramctl_VtPgBipErrFrc_Shift                                                                   7

/*--------------------------------------
BitField Name: VtPgLopFrc
BitField Type: RW
BitField Desc: Forcing LOP. 1: Force 0: Not force
BitField Bits: [6]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgLopFrc_Mask                                                                   cBit6
#define cAf6_vpgramctl_VtPgLopFrc_Shift                                                                      6

/*--------------------------------------
BitField Name: VtPgUeqFrc
BitField Type: RW
BitField Desc: Forcing FSM to UEQ state. 1: Force 0: Not force
BitField Bits: [5]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgUeqFrc_Mask                                                                   cBit5
#define cAf6_vpgramctl_VtPgUeqFrc_Shift                                                                      5

/*--------------------------------------
BitField Name: VtPgAisFrc
BitField Type: RW
BitField Desc: Forcing FSM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgAisFrc_Mask                                                                   cBit4
#define cAf6_vpgramctl_VtPgAisFrc_Shift                                                                      4

/*--------------------------------------
BitField Name: VtPgSSInsPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to insert to Pointer
value.
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgSSInsPatt_Mask                                                              cBit3_2
#define cAf6_vpgramctl_VtPgSSInsPatt_Shift                                                                   2

/*--------------------------------------
BitField Name: VtPgSSInsEn
BitField Type: RW
BitField Desc: Enable/disable SS bits insertion. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgSSInsEn_Mask                                                                  cBit1
#define cAf6_vpgramctl_VtPgSSInsEn_Shift                                                                     1

/*--------------------------------------
BitField Name: VtPgPohIns
BitField Type: RW
BitField Desc: Enable/ disable POH Insertion. High to enable insertion of POH.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_vpgramctl_VtPgPohIns_Mask                                                                   cBit0
#define cAf6_vpgramctl_VtPgPohIns_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx VT/TU per Alarm Interrupt Status
Reg Addr   : 0x62800 - 0x7efff
Reg Formula: 0x62800 + 16384*LineId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This is the per Alarm interrupt status of VT/TU pointer interpreter . Each register is used to store 5 sticky bits for 5 alarms in the VT/TU.

------------------------------------------------------------------------------*/
#define cAf6Reg_upvtchstkram_Base                                                                      0x62800

/*--------------------------------------
BitField Name: VtPiStsNewDetIntr
BitField Type: W1C
BitField Desc: Set to 1 while an New Pointer Detection event is detected at
VT/TU pointer interpreter. This event doesn't raise interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_Mask                                                         cBit4
#define cAf6_upvtchstkram_VtPiStsNewDetIntr_Shift                                                            4

/*--------------------------------------
BitField Name: VtPiStsNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF event is detected at VT/TU pointer
interpreter. This event doesn't raise interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsNdfIntr_Mask                                                            cBit3
#define cAf6_upvtchstkram_VtPiStsNdfIntr_Shift                                                               3

/*--------------------------------------
BitField Name: VtPiStsCepUneqVStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 to while there is change in Unequip state in the related
VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status
register of the related VT/TU to know the VT/TU whether in Uneqip state or not.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_Mask                                               cBit2
#define cAf6_upvtchstkram_VtPiStsCepUneqVStateChgIntr_Shift                                                  2

/*--------------------------------------
BitField Name: VtPiStsAISStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 to while there is change in AIS state in the related VT/TU
to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status
register of the related VT/TU to know the VT/TU whether in LOP state or not.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_Mask                                                    cBit1
#define cAf6_upvtchstkram_VtPiStsAISStateChgIntr_Shift                                                       1

/*--------------------------------------
BitField Name: VtPiStsLopStateChgIntr
BitField Type: W1C
BitField Desc: Set 1 to while there is change in LOP state in the related VT/TU
to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status
register of the related VT/TU to know the VT/TU whether in AIS state or not.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_Mask                                                    cBit0
#define cAf6_upvtchstkram_VtPiStsLopStateChgIntr_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx VT/TU per Alarm Current Status
Reg Addr   : 0x63000 - 0x7ffff
Reg Formula: 0x63000 + 16384*LineId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This is the per Alarm current status of VT/TU pointer interpreter. Each register is used to store 3 bits to store current status of 3 alarms in the VT/TU.

------------------------------------------------------------------------------*/
#define cAf6Reg_upvtchstaram_Base                                                                      0x63000

/*--------------------------------------
BitField Name: VtPiStsCepUneqCurStatus
BitField Type: RO
BitField Desc: Unequip current status in the related VT/TU. When it changes for
0 to 1 or vice versa, the VtPiStsCepUneqVStateChgIntr bit in the OCN Rx VT/TU
per Alarm Interrupt Status register of the related VT/TU is set.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Mask                                                   cBit2
#define cAf6_upvtchstaram_VtPiStsCepUneqCurStatus_Shift                                                      2

/*--------------------------------------
BitField Name: VtPiStsAisCurStatus
BitField Type: RO
BitField Desc: AIS current status in the related VT/TU. When it changes for 0 to
1 or vice versa, the VtPiStsAISStateChgIntr bit in the OCN Rx VT/TU per Alarm
Interrupt Status register of the related VT/TU is set.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_Mask                                                       cBit1
#define cAf6_upvtchstaram_VtPiStsAisCurStatus_Shift                                                          1

/*--------------------------------------
BitField Name: VtPiStsLopCurStatus
BitField Type: RO
BitField Desc: LOP current status in the related VT/TU. When it changes for 0 to
1 or vice versa, the VtPiStsLopStateChgIntr bit in the OCN Rx VT/TU per Alarm
Interrupt Status register of the related VT/TU is set.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_Mask                                                       cBit0
#define cAf6_upvtchstaram_VtPiStsLopCurStatus_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx PP VT/TU Pointer interpreter pointer adjustment per channel counter
Reg Addr   : 0x61000 - 0x7dfff
Reg Formula: 0x61000 + 16384*LineId + 2048*AdjMode + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-7)
           + $AdjMode(0-1)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VT/TU pointer interpreter. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntperstkram_Base                                                                   0x61000

/*--------------------------------------
BitField Name: RxPpViPiPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [11] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntperstkram_RxPpViPiPtAdjCnt_Mask                                                    cBit17_0
#define cAf6_adjcntperstkram_RxPpViPiPtAdjCnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg VTTU per Alarm Interrupt Status
Reg Addr   : 0x82800 - 0x9efff
Reg Formula: 0x82800 + 16384*LineId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-7)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This is the per Alarm interrupt status of STS/VT/TU pointer generator . Each register is used to store 3 sticky bits for 3 alarms

------------------------------------------------------------------------------*/
#define cAf6Reg_vtpgstkram_Base                                                                        0x82800

/*--------------------------------------
BitField Name: VtPgAisIntr
BitField Type: W1C
BitField Desc: Set to 1 while an AIS status event (at h2pos) is detected at Tx
VTTU Pointer Generator.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_vtpgstkram_VtPgAisIntr_Mask                                                                 cBit3
#define cAf6_vtpgstkram_VtPgAisIntr_Shift                                                                    3

/*--------------------------------------
BitField Name: VtPgFiFoOvfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an FIFO Overflowed event is detected at Tx VTTU
Pointer Generator.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_Mask                                                             cBit2
#define cAf6_vtpgstkram_VtPgFiFoOvfIntr_Shift                                                                2

/*--------------------------------------
BitField Name: VtPgNdfIntr
BitField Type: W1C
BitField Desc: Set to 1 while an NDF status event (at h2pos) is detected at Tx
VTTU Pointer Generator.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_vtpgstkram_VtPgNdfIntr_Mask                                                                 cBit1
#define cAf6_vtpgstkram_VtPgNdfIntr_Shift                                                                    1


/*------------------------------------------------------------------------------
Reg Name   : OCN TxPg VTTU pointer adjustment per channel counter
Reg Addr   : 0x81000 - 0x9dfff
Reg Formula: 0x81000 + 16384*LineId + 2048*AdjMode + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $LineId(0-7)
           + $AdjMode(0-1)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VTTU pointer generator. These counters are in saturation mode

------------------------------------------------------------------------------*/
#define cAf6Reg_adjcntpgpervtram_Base                                                                  0x81000

/*--------------------------------------
BitField Name: VtpgPtAdjCnt
BitField Type: RC
BitField Desc: The pointer Increment counter or decrease counter. Bit [11] of
address is used to indicate that the counter is pointer increment or decrement
counter. The counter will stop at maximum value (0x3FFFF).
BitField Bits: [17:0]
--------------------------------------*/
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_Mask                                                       cBit17_0
#define cAf6_adjcntpgpervtram_VtpgPtAdjCnt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Force Config 1 - TFI5 Side
Reg Addr   : 0x0f020
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Force Config 1

------------------------------------------------------------------------------*/
#define cAf6Reg_parfrccfg1_Base                                                                        0x0f020

/*--------------------------------------
BitField Name: EsSpgParErrFrc
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN STS Pointer Generator Per
Channel Control - TFI5 Side". Each bit correspond with each LineId. Set 1 to
force.
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_parfrccfg1_EsSpgParErrFrc_Mask                                                          cBit31_24
#define cAf6_parfrccfg1_EsSpgParErrFrc_Shift                                                                24

/*--------------------------------------
BitField Name: EsSpiParErrFrc
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN STS Pointer Interpreter Per
Channel Control - TFI5 Side". Each bit correspond with each LineId. Set 1 to
force.
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_parfrccfg1_EsSpiParErrFrc_Mask                                                          cBit23_16
#define cAf6_parfrccfg1_EsSpiParErrFrc_Shift                                                                16


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Disable Config 1 - TFI5 Side
Reg Addr   : 0x0f021
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Disable Config 1

------------------------------------------------------------------------------*/
#define cAf6Reg_pardiscfg1_Base                                                                        0x0f021

/*--------------------------------------
BitField Name: EsSpgParErrDis
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN STS Pointer Generator Per
Channel Control - TFI5 Side". Each bit correspond with each LineId. Set 1 to
disable.
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_pardiscfg1_EsSpgParErrDis_Mask                                                          cBit31_24
#define cAf6_pardiscfg1_EsSpgParErrDis_Shift                                                                24

/*--------------------------------------
BitField Name: EsSpiParErrDis
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN STS Pointer Interpreter Per
Channel Control - TFI5 Side". Each bit correspond with each LineId. Set 1 to
disable.
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pardiscfg1_EsSpiParErrDis_Mask                                                          cBit23_16
#define cAf6_pardiscfg1_EsSpiParErrDis_Shift                                                                16


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Error Sticky 1 - TFI5 Side
Reg Addr   : 0x0f022
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Error Sticky 1

------------------------------------------------------------------------------*/
#define cAf6Reg_parerrstk1_Base                                                                        0x0f022

/*--------------------------------------
BitField Name: EsSpgParErrStk
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN STS Pointer Generator Per
Channel Control - TFI5 Side". Each bit correspond with each LineId.
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_parerrstk1_EsSpgParErrStk_Mask                                                          cBit31_24
#define cAf6_parerrstk1_EsSpgParErrStk_Shift                                                                24

/*--------------------------------------
BitField Name: EsSpiParErrStk
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN STS Pointer Interpreter Per
Channel Control - TFI5 Side". Each bit correspond with each LineId.
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_parerrstk1_EsSpiParErrStk_Mask                                                          cBit23_16
#define cAf6_parerrstk1_EsSpiParErrStk_Shift                                                                16


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Force Config 2 - Line Side
Reg Addr   : 0x20020
Reg Formula: 0x20020 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
OCN Parity Force Config 2

------------------------------------------------------------------------------*/
#define cAf6Reg_parfrccfg2_Base                                                                        0x20020

/*--------------------------------------
BitField Name: TohParErrFrc
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN TOH Monitoring Per Line
Control".Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [16]
--------------------------------------*/
#define cAf6_parfrccfg2_TohParErrFrc_Mask                                                               cBit16
#define cAf6_parfrccfg2_TohParErrFrc_Shift                                                                  16

/*--------------------------------------
BitField Name: SpgParErrFrc
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN STS Pointer Generator Per
Channel Control". Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_parfrccfg2_SpgParErrFrc_Mask                                                            cBit15_12
#define cAf6_parfrccfg2_SpgParErrFrc_Shift                                                                  12

/*--------------------------------------
BitField Name: SpiParErrFrc
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN STS Pointer Interpreter Per
Channel Control". Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_parfrccfg2_SpiParErrFrc_Mask                                                             cBit11_8
#define cAf6_parfrccfg2_SpiParErrFrc_Shift                                                                   8

/*--------------------------------------
BitField Name: TxFrmParErrFrc
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN Tx J0 Insertion Buffer". Each
bit correspond with each LineId. Set 1 to force.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_parfrccfg2_TxFrmParErrFrc_Mask                                                            cBit7_0
#define cAf6_parfrccfg2_TxFrmParErrFrc_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Disable Config 2 - Line Side
Reg Addr   : 0x20021
Reg Formula: 0x20021 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
OCN Parity Disable Config 2

------------------------------------------------------------------------------*/
#define cAf6Reg_pardiscfg2_Base                                                                        0x20021

/*--------------------------------------
BitField Name: TohParErrDis
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN TOH Monitoring Per Line
Control".Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pardiscfg2_TohParErrDis_Mask                                                               cBit16
#define cAf6_pardiscfg2_TohParErrDis_Shift                                                                  16

/*--------------------------------------
BitField Name: SpgParErrDis
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN STS Pointer Generator Per
Channel Control". Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_pardiscfg2_SpgParErrDis_Mask                                                            cBit15_12
#define cAf6_pardiscfg2_SpgParErrDis_Shift                                                                  12

/*--------------------------------------
BitField Name: SpiParErrDis
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN STS Pointer Interpreter Per
Channel Control". Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_pardiscfg2_SpiParErrDis_Mask                                                             cBit11_8
#define cAf6_pardiscfg2_SpiParErrDis_Shift                                                                   8

/*--------------------------------------
BitField Name: TxFrmParErrDis
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN Tx J0 Insertion Buffer". Each
bit correspond with each LineId. Set 1 to disable.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pardiscfg2_TxFrmParErrDis_Mask                                                            cBit7_0
#define cAf6_pardiscfg2_TxFrmParErrDis_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Error Sticky 2 - Line Side
Reg Addr   : 0x20022
Reg Formula: 0x20022 + 65536*GroupID
    Where  : 
           + $GroupID(0-1)
Reg Desc   : 
OCN Parity Error Sticky 2

------------------------------------------------------------------------------*/
#define cAf6Reg_parerrstk2_Base                                                                        0x20022

/*--------------------------------------
BitField Name: TohParErrStk
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN TOH Monitoring Per Line
Control".Each bit correspond with each LineId.
BitField Bits: [16]
--------------------------------------*/
#define cAf6_parerrstk2_TohParErrStk_Mask                                                               cBit16
#define cAf6_parerrstk2_TohParErrStk_Shift                                                                  16

/*--------------------------------------
BitField Name: SpgParErrStk
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN STS Pointer Generator Per
Channel Control". Each bit correspond with each LineId.
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_parerrstk2_SpgParErrStk_Mask                                                            cBit15_12
#define cAf6_parerrstk2_SpgParErrStk_Shift                                                                  12

/*--------------------------------------
BitField Name: SpiParErrStk
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN STS Pointer Interpreter Per
Channel Control". Each bit correspond with each LineId.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_parerrstk2_SpiParErrStk_Mask                                                             cBit11_8
#define cAf6_parerrstk2_SpiParErrStk_Shift                                                                   8

/*--------------------------------------
BitField Name: TxFrmParErrStk
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN Tx J0 Insertion Buffer". Each
bit correspond with each LineId.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_parerrstk2_TxFrmParErrStk_Mask                                                            cBit7_0
#define cAf6_parerrstk2_TxFrmParErrStk_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Force Config 3 - VPI+VPG+OHO
Reg Addr   : 0xf0020
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Force Config 3 for 4 lines 0-3

------------------------------------------------------------------------------*/
#define cAf6Reg_parfrccfg3_Base                                                                        0xf0020

/*--------------------------------------
BitField Name: RxHoParErrFrc0
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN Rx High Order Map concatenate
configuration".Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_parfrccfg3_RxHoParErrFrc0_Mask                                                          cBit19_16
#define cAf6_parfrccfg3_RxHoParErrFrc0_Shift                                                                16

/*--------------------------------------
BitField Name: VpgCtlParErrFrc0
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN VTTU Pointer Generator Per
Channel Control". Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_parfrccfg3_VpgCtlParErrFrc0_Mask                                                        cBit15_12
#define cAf6_parfrccfg3_VpgCtlParErrFrc0_Shift                                                              12

/*--------------------------------------
BitField Name: VpgDemParErrFrc0
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN TXPP Per STS Multiplexing
Control". Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_parfrccfg3_VpgDemParErrFrc0_Mask                                                         cBit11_8
#define cAf6_parfrccfg3_VpgDemParErrFrc0_Shift                                                               8

/*--------------------------------------
BitField Name: VpiCtlParErrFrc0
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN VTTU Pointer Interpreter Per
Channel Control". Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_parfrccfg3_VpiCtlParErrFrc0_Mask                                                          cBit7_4
#define cAf6_parfrccfg3_VpiCtlParErrFrc0_Shift                                                               4

/*--------------------------------------
BitField Name: VpiDemParErrFrc0
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN RXPP Per STS payload Control".
Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_parfrccfg3_VpiDemParErrFrc0_Mask                                                          cBit3_0
#define cAf6_parfrccfg3_VpiDemParErrFrc0_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Disable Config 3 - VPI+VPG+OHO
Reg Addr   : 0xf0021
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Disable Config 3 for 4 lines 0-3

------------------------------------------------------------------------------*/
#define cAf6Reg_pardiscfg3_Base                                                                        0xf0021

/*--------------------------------------
BitField Name: RxHoParErrDis0
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN Rx High Order Map concatenate
configuration".Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_pardiscfg3_RxHoParErrDis0_Mask                                                          cBit19_16
#define cAf6_pardiscfg3_RxHoParErrDis0_Shift                                                                16

/*--------------------------------------
BitField Name: VpgCtlParErrDis0
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN VTTU Pointer Generator Per
Channel Control". Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_pardiscfg3_VpgCtlParErrDis0_Mask                                                        cBit15_12
#define cAf6_pardiscfg3_VpgCtlParErrDis0_Shift                                                              12

/*--------------------------------------
BitField Name: VpgDemParErrDis0
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN TXPP Per STS Multiplexing
Control". Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_pardiscfg3_VpgDemParErrDis0_Mask                                                         cBit11_8
#define cAf6_pardiscfg3_VpgDemParErrDis0_Shift                                                               8

/*--------------------------------------
BitField Name: VpiCtlParErrDis0
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN VTTU Pointer Interpreter Per
Channel Control". Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_pardiscfg3_VpiCtlParErrDis0_Mask                                                          cBit7_4
#define cAf6_pardiscfg3_VpiCtlParErrDis0_Shift                                                               4

/*--------------------------------------
BitField Name: VpiDemParErrDis0
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN RXPP Per STS payload Control".
Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_pardiscfg3_VpiDemParErrDis0_Mask                                                          cBit3_0
#define cAf6_pardiscfg3_VpiDemParErrDis0_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Error Sticky 3 - VPI+VPG+OHO
Reg Addr   : 0xf0022
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Error Sticky 3 for 4 lines 0-3

------------------------------------------------------------------------------*/
#define cAf6Reg_parerrstk3_Base                                                                        0xf0022

/*--------------------------------------
BitField Name: RxHoParErrStk0
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN Rx High Order Map concatenate
configuration".Each bit correspond with each LineId.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_parerrstk3_RxHoParErrStk0_Mask                                                          cBit19_16
#define cAf6_parerrstk3_RxHoParErrStk0_Shift                                                                16

/*--------------------------------------
BitField Name: VpgCtlParErrStk0
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN VTTU Pointer Generator Per
Channel Control". Each bit correspond with each LineId.
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_parerrstk3_VpgCtlParErrStk0_Mask                                                        cBit15_12
#define cAf6_parerrstk3_VpgCtlParErrStk0_Shift                                                              12

/*--------------------------------------
BitField Name: VpgDemParErrStk0
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN TXPP Per STS Multiplexing
Control". Each bit correspond with each LineId.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_parerrstk3_VpgDemParErrStk0_Mask                                                         cBit11_8
#define cAf6_parerrstk3_VpgDemParErrStk0_Shift                                                               8

/*--------------------------------------
BitField Name: VpiCtlParErrStk0
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN VTTU Pointer Interpreter Per
Channel Control". Each bit correspond with each LineId.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_parerrstk3_VpiCtlParErrStk0_Mask                                                          cBit7_4
#define cAf6_parerrstk3_VpiCtlParErrStk0_Shift                                                               4

/*--------------------------------------
BitField Name: VpiDemParErrStk0
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN RXPP Per STS payload Control".
Each bit correspond with each LineId.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_parerrstk3_VpiDemParErrStk0_Mask                                                          cBit3_0
#define cAf6_parerrstk3_VpiDemParErrStk0_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Force Config 4 - VPI+VPG+OHO
Reg Addr   : 0xf0030
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Force Config 4 for 4 lines 4-7

------------------------------------------------------------------------------*/
#define cAf6Reg_parfrccfg4_Base                                                                        0xf0030

/*--------------------------------------
BitField Name: RxHoParErrFrc1
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN Rx High Order Map concatenate
configuration".Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_parfrccfg4_RxHoParErrFrc1_Mask                                                          cBit19_16
#define cAf6_parfrccfg4_RxHoParErrFrc1_Shift                                                                16

/*--------------------------------------
BitField Name: VpgCtlParErrFrc1
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN VTTU Pointer Generator Per
Channel Control". Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_parfrccfg4_VpgCtlParErrFrc1_Mask                                                        cBit15_12
#define cAf6_parfrccfg4_VpgCtlParErrFrc1_Shift                                                              12

/*--------------------------------------
BitField Name: VpgDemParErrFrc1
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN TXPP Per STS Multiplexing
Control". Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_parfrccfg4_VpgDemParErrFrc1_Mask                                                         cBit11_8
#define cAf6_parfrccfg4_VpgDemParErrFrc1_Shift                                                               8

/*--------------------------------------
BitField Name: VpiCtlParErrFrc1
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN VTTU Pointer Interpreter Per
Channel Control". Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_parfrccfg4_VpiCtlParErrFrc1_Mask                                                          cBit7_4
#define cAf6_parfrccfg4_VpiCtlParErrFrc1_Shift                                                               4

/*--------------------------------------
BitField Name: VpiDemParErrFrc1
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN RXPP Per STS payload Control".
Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_parfrccfg4_VpiDemParErrFrc1_Mask                                                          cBit3_0
#define cAf6_parfrccfg4_VpiDemParErrFrc1_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Disable Config 4 - VPI+VPG+OHO
Reg Addr   : 0xf0031
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Disable Config 4 for 4 lines 4-7

------------------------------------------------------------------------------*/
#define cAf6Reg_pardiscfg4_Base                                                                        0xf0031

/*--------------------------------------
BitField Name: RxHoParErrDis1
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN Rx High Order Map concatenate
configuration".Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_pardiscfg4_RxHoParErrDis1_Mask                                                          cBit19_16
#define cAf6_pardiscfg4_RxHoParErrDis1_Shift                                                                16

/*--------------------------------------
BitField Name: VpgCtlParErrDis1
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN VTTU Pointer Generator Per
Channel Control". Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_pardiscfg4_VpgCtlParErrDis1_Mask                                                        cBit15_12
#define cAf6_pardiscfg4_VpgCtlParErrDis1_Shift                                                              12

/*--------------------------------------
BitField Name: VpgDemParErrDis1
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN TXPP Per STS Multiplexing
Control". Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_pardiscfg4_VpgDemParErrDis1_Mask                                                         cBit11_8
#define cAf6_pardiscfg4_VpgDemParErrDis1_Shift                                                               8

/*--------------------------------------
BitField Name: VpiCtlParErrDis1
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN VTTU Pointer Interpreter Per
Channel Control". Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_pardiscfg4_VpiCtlParErrDis1_Mask                                                          cBit7_4
#define cAf6_pardiscfg4_VpiCtlParErrDis1_Shift                                                               4

/*--------------------------------------
BitField Name: VpiDemParErrDis1
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN RXPP Per STS payload Control".
Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_pardiscfg4_VpiDemParErrDis1_Mask                                                          cBit3_0
#define cAf6_pardiscfg4_VpiDemParErrDis1_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Error Sticky 4 - VPI+VPG+OHO
Reg Addr   : 0xf0032
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Error Sticky 4 for 4 lines 4-7

------------------------------------------------------------------------------*/
#define cAf6Reg_parerrstk4_Base                                                                        0xf0032

/*--------------------------------------
BitField Name: RxHoParErrStk1
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN Rx High Order Map concatenate
configuration".Each bit correspond with each LineId.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_parerrstk4_RxHoParErrStk1_Mask                                                          cBit19_16
#define cAf6_parerrstk4_RxHoParErrStk1_Shift                                                                16

/*--------------------------------------
BitField Name: VpgCtlParErrStk1
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN VTTU Pointer Generator Per
Channel Control". Each bit correspond with each LineId.
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_parerrstk4_VpgCtlParErrStk1_Mask                                                        cBit15_12
#define cAf6_parerrstk4_VpgCtlParErrStk1_Shift                                                              12

/*--------------------------------------
BitField Name: VpgDemParErrStk1
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN TXPP Per STS Multiplexing
Control". Each bit correspond with each LineId.
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_parerrstk4_VpgDemParErrStk1_Mask                                                         cBit11_8
#define cAf6_parerrstk4_VpgDemParErrStk1_Shift                                                               8

/*--------------------------------------
BitField Name: VpiCtlParErrStk1
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN VTTU Pointer Interpreter Per
Channel Control". Each bit correspond with each LineId.
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_parerrstk4_VpiCtlParErrStk1_Mask                                                          cBit7_4
#define cAf6_parerrstk4_VpiCtlParErrStk1_Shift                                                               4

/*--------------------------------------
BitField Name: VpiDemParErrStk1
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN RXPP Per STS payload Control".
Each bit correspond with each LineId.
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_parerrstk4_VpiDemParErrStk1_Mask                                                          cBit3_0
#define cAf6_parerrstk4_VpiDemParErrStk1_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Force Config 5 - SXC selector page0+1
Reg Addr   : 0xf0024
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Force Config 5

------------------------------------------------------------------------------*/
#define cAf6Reg_parfrccfg5_Base                                                                        0xf0024

/*--------------------------------------
BitField Name: SxcParErrFrc0
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN SXC Control 1 - Config Page 0
and Page 1 of SXC". Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_parfrccfg5_SxcParErrFrc0_Mask                                                            cBit23_0
#define cAf6_parfrccfg5_SxcParErrFrc0_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Disable Config 5 - SXC selector page0+1
Reg Addr   : 0xf0025
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Disable Config 5

------------------------------------------------------------------------------*/
#define cAf6Reg_pardiscfg5_Base                                                                        0xf0025

/*--------------------------------------
BitField Name: SxcParErrDis0
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN SXC Control 1 - Config Page 0
and Page 1 of SXC". Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_pardiscfg5_SxcParErrDis0_Mask                                                            cBit23_0
#define cAf6_pardiscfg5_SxcParErrDis0_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Error Sticky 5 - SXC selector page0+1
Reg Addr   : 0xf0026
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Error Sticky 5

------------------------------------------------------------------------------*/
#define cAf6Reg_parerrstk5_Base                                                                        0xf0026

/*--------------------------------------
BitField Name: SxcParErrStk0
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN SXC Control 1 - Config Page 0
and Page 1 of SXC". Each bit correspond with each LineId.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_parerrstk5_SxcParErrStk0_Mask                                                            cBit23_0
#define cAf6_parerrstk5_SxcParErrStk0_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Force Config 6 - SXC selector page2
Reg Addr   : 0xf0028
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Force Config 6

------------------------------------------------------------------------------*/
#define cAf6Reg_parfrccfg6_Base                                                                        0xf0028

/*--------------------------------------
BitField Name: SxcParErrFrc1
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN SXC Control 2 - Config Page 2 of
SXC". Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_parfrccfg6_SxcParErrFrc1_Mask                                                            cBit23_0
#define cAf6_parfrccfg6_SxcParErrFrc1_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Disable Config 6 - SXC selector page2
Reg Addr   : 0xf0029
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Disable Config 6

------------------------------------------------------------------------------*/
#define cAf6Reg_pardiscfg6_Base                                                                        0xf0029

/*--------------------------------------
BitField Name: SxcParErrDis1
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN SXC Control 2 - Config Page 2
of SXC". Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_pardiscfg6_SxcParErrDis1_Mask                                                            cBit23_0
#define cAf6_pardiscfg6_SxcParErrDis1_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Error Sticky 6 - SXC selector page2
Reg Addr   : 0xf002a
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Error Sticky 6

------------------------------------------------------------------------------*/
#define cAf6Reg_parerrstk6_Base                                                                        0xf002a

/*--------------------------------------
BitField Name: SxcParErrStk1
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN SXC Control 2 - Config Page 2 of
SXC". Each bit correspond with each LineId.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_parerrstk6_SxcParErrStk1_Mask                                                            cBit23_0
#define cAf6_parerrstk6_SxcParErrStk1_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Force Config 7 - SXC Config APS
Reg Addr   : 0xf002c
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Force Config 7

------------------------------------------------------------------------------*/
#define cAf6Reg_parfrccfg7_Base                                                                        0xf002c

/*--------------------------------------
BitField Name: SxcParErrFrc2
BitField Type: RW
BitField Desc: Force Parity for ram config "OCN SXC Control 3 - Config APS".
Each bit correspond with each LineId. Set 1 to force.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_parfrccfg7_SxcParErrFrc2_Mask                                                            cBit23_0
#define cAf6_parfrccfg7_SxcParErrFrc2_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Disable Config 7 - SXC Config APS
Reg Addr   : 0xf002d
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Disable Config 7

------------------------------------------------------------------------------*/
#define cAf6Reg_pardiscfg7_Base                                                                        0xf002d

/*--------------------------------------
BitField Name: SxcParErrDis2
BitField Type: RW
BitField Desc: Disable Parity for ram config "OCN SXC Control 3 - Config APS".
Each bit correspond with each LineId. Set 1 to disable.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_pardiscfg7_SxcParErrDis2_Mask                                                            cBit23_0
#define cAf6_pardiscfg7_SxcParErrDis2_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OCN Parity Error Sticky 7 - SXC Config APS
Reg Addr   : 0xf002e
Reg Formula: 
    Where  : 
Reg Desc   : 
OCN Parity Error Sticky 7

------------------------------------------------------------------------------*/
#define cAf6Reg_parerrstk7_Base                                                                        0xf002e

/*--------------------------------------
BitField Name: SxcParErrStk2
BitField Type: W1C
BitField Desc: Error Sticky for ram config "OCN SXC Control 3 - Config APS".
Each bit correspond with each LineId.
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_parerrstk7_SxcParErrStk2_Mask                                                            cBit23_0
#define cAf6_parerrstk7_SxcParErrStk2_Shift                                                                  0

/*-------------------------------------------------------------------------------
#1.1.1.7    OCN Global Sonet/Sdh Mode
Register Full Name: OCN Global Sonet/Sdh Mode
RTL Instant Name  : glbsonetsdhmode_reg
Address: 0xf0008
Description  : This register use 16 bits to configure Sonet/Sdh mode.
Field: [15:0] %% TohSonetSdhMode     %% Config mode is either SDH or Sonet per line. Bit[0] for line 0
                                         1: SDH mode
                                         0: Sonet mode  %% RW  %% 0x0 %% 0x0
---------------------------------------------------------------------------------*/
#define cAf6Reg_glbsonetsdhmode_reg_Base                     0xf0008
#define cAf6_glbsonetsdhmode_TohSonetSdhMode_Line0_Mask  cBit0
#define cAf6_glbsonetsdhmode_TohSonetSdhMode_Line0_Shift 0

#endif /* _AF6_REG_AF6CNC0022_RD_OCN_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60290022ModuleOcnInternal.h
 * 
 * Created Date: Jul 23, 2018
 *
 * Description : 60290022 OCN internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEOCNINTERNAL_H_
#define _THA60290022MODULEOCNINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/ocn/Tha60290021ModuleOcnInternal.h"
#include "Tha60290022ModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModuleOcnMethods
    {
    eAtRet (*ClockExtractorSquelchingAisLEnable)(Tha60290022ModuleOcn self, uint8 faceplateLineId, eBool enable);
    eBool (*ClockExtractorSquelchingAisLIsEnabled)(Tha60290022ModuleOcn self, uint8 faceplateLineId);
    }tTha60290022ModuleOcnMethods;

typedef struct tTha60290022ModuleOcn
    {
    tTha60290021ModuleOcn super;
    const tTha60290022ModuleOcnMethods *methods;
    }tTha60290022ModuleOcn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290022ModuleOcnObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEOCNINTERNAL_H_ */

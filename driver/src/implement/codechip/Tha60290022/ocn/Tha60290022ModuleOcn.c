/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha60290022ModuleOcn.c
 *
 * Created Date: Apr 16, 2017
 *
 * Description : OCN module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../Tha60290021/pw/Tha60290021DccKbyteReg.h"
#include "../ram/Tha60290022InternalRam.h"
#include "../man/Tha60290022Device.h"
#include "Tha60290022ModuleOcnReg.h"
#include "Tha60290022ModuleOcn.h"
#include "Tha60290022ModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)     ((Tha60290022ModuleOcn)self)

/*--------------------------- Macros -----------------------------------------*/
#define cAf6_rfmz1z2selctl_OCNRxZnAugId_Mask        cBit5_2
#define cAf6_rfmz1z2selctl_OCNRxZnAugId_Shift       2
#define cAf6_rfmz1z2selctl_OCNRxZnAu3Id_Mask        cBit1_0
#define cAf6_rfmz1z2selctl_OCNRxZnAu3Id_Shift       0

#define cAf6Reg_upen_cfg_dcc_en 0x10
#define cAf6Reg_upen_cfg_dcc_en_V4dot1 0x1F

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290022ModuleOcnMethods m_methods;

/* Override */
static tThaModuleOcnMethods         m_ThaModuleOcnOverride;
static tTha60210011ModuleOcnMethods m_Tha60210011ModuleOcnOverride;
static tTha60290021ModuleOcnMethods m_Tha60290021ModuleOcnOverride;
static tAtModuleMethods             m_AtModuleOverride;

/* Save super implementation */
static const tTha60210011ModuleOcnMethods *m_Tha60210011ModuleOcnMethods = NULL;
static const tAtModuleMethods             *m_AtModuleMethods             = NULL;
static const tThaModuleOcnMethods         *m_ThaModuleOcnMethods         = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumFaceplateSlices(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 FaceplateXcStartLineId(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 TerminatedXcStartLineId(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return 16;
    }

static eBool LoPathPhyModuleIsDividedToBlockOc24(Tha60210011ModuleOcn self, AtSdhChannel channel, eAtModule phyModule)
    {
    AtUnused(self);
    AtUnused(channel);
    if (phyModule == cAtModuleSur)
        return cAtTrue;
    return cAtFalse;
    }

static eBool HoPathPhyModuleIsDividedToOc24(Tha60210011ModuleOcn self, eAtModule phyModule)
    {
    if (phyModule == cAtModulePdh)
        return cAtFalse;
    return m_Tha60210011ModuleOcnMethods->HoPathPhyModuleIsDividedToOc24(self, phyModule);
    }

static eBool HasOverheadCpuControl(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasRoleStatus(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 NumXcLines(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return 24;
    }

static eBool NewTuVcLoopbackIsReady(Tha60210011ModuleOcn self)
    {
    return Tha60290022DeviceNewTuVcLoopbackIsReady(AtModuleDeviceGet((AtModule)self));
    }

static eBool TuVcIsLoopbackAtPointerProcessor(Tha60210011ModuleOcn self, AtChannel tuVc)
    {
    AtUnused(tuVc);
    return NewTuVcLoopbackIsReady(self);
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    return Tha60290022InternalRamOcnNew(self, ramId, localRamId);
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "TFI-5 OCN Rx Bridge and Roll SXC Control 0",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 1",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 2",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 3",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 4",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 5",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 6",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 7",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 0",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 1",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 2",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 3",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 4",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 5",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 6",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 7",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 0",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 1",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 2",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 3",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 4",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 5",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 6",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 7",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 0",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 1",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 2",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 3",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 4",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 5",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 6",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 7", /*31*/

         "Line Side OCN Tx J0 Insertion Buffer 0",/*32*/
         "Line Side OCN Tx J0 Insertion Buffer 1",
         "Line Side OCN Tx J0 Insertion Buffer 2",
         "Line Side OCN Tx J0 Insertion Buffer 3",
         "Line Side OCN Tx J0 Insertion Buffer 4",
         "Line Side OCN Tx J0 Insertion Buffer 5",
         "Line Side OCN Tx J0 Insertion Buffer 6",
         "Line Side OCN Tx J0 Insertion Buffer 7",
         "Line Side OCN STS Pointer Interpreter Per Channel Control 0", /*40*/
         "Line Side OCN STS Pointer Interpreter Per Channel Control 1",
         "Line Side OCN STS Pointer Interpreter Per Channel Control 2",
         "Line Side OCN STS Pointer Interpreter Per Channel Control 3",
         "Line Side OCN STS Pointer Generator Per Channel Control 0",/*44*/
         "Line Side OCN STS Pointer Generator Per Channel Control 1",
         "Line Side OCN STS Pointer Generator Per Channel Control 2",
         "Line Side OCN STS Pointer Generator Per Channel Control 3", /*47*/
         "Line Side OCN TOH Monitoring Per Line Control", /*48*/

         "OCN RXPP Per STS payload Control 0",/*49*/
         "OCN RXPP Per STS payload Control 1",
         "OCN RXPP Per STS payload Control 2",
         "OCN RXPP Per STS payload Control 3",
         "OCN VTTU Pointer Interpreter Per Channel Control 0", /* 53 */
         "OCN VTTU Pointer Interpreter Per Channel Control 1",
         "OCN VTTU Pointer Interpreter Per Channel Control 2",
         "OCN VTTU Pointer Interpreter Per Channel Control 3",
         "OCN TXPP Per STS Multiplexing Control 0", /* 57 */
         "OCN TXPP Per STS Multiplexing Control 1",
         "OCN TXPP Per STS Multiplexing Control 2",
         "OCN TXPP Per STS Multiplexing Control 3",
         "OCN VTTU Pointer Generator Per Channel Control 0", /* 61 */
         "OCN VTTU Pointer Generator Per Channel Control 1",
         "OCN VTTU Pointer Generator Per Channel Control 2",
         "OCN VTTU Pointer Generator Per Channel Control 3",
         "OCN Rx High Order Map concatenate configuration 0", /* 65 */
         "OCN Rx High Order Map concatenate configuration 1",
         "OCN Rx High Order Map concatenate configuration 2",
         "OCN Rx High Order Map concatenate configuration 3", /*68*/

         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 0",/*69*/
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 1",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 2",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 3",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 4",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 5",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 6",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 7",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 8",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 9",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 10",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 11", /* 80 */

         "OCN SXC Control 2 - Config Page 2 of SXC 0",/**81*/
         "OCN SXC Control 2 - Config Page 2 of SXC 1",
         "OCN SXC Control 2 - Config Page 2 of SXC 2",
         "OCN SXC Control 2 - Config Page 2 of SXC 3",
         "OCN SXC Control 2 - Config Page 2 of SXC 4",
         "OCN SXC Control 2 - Config Page 2 of SXC 5",
         "OCN SXC Control 2 - Config Page 2 of SXC 6",
         "OCN SXC Control 2 - Config Page 2 of SXC 7",
         "OCN SXC Control 2 - Config Page 2 of SXC 8",
         "OCN SXC Control 2 - Config Page 2 of SXC 9",
         "OCN SXC Control 2 - Config Page 2 of SXC 10",
         "OCN SXC Control 2 - Config Page 2 of SXC 11",/*92*/

         "OCN SXC Control 3 - Config APS 0",/*93*/
         "OCN SXC Control 3 - Config APS 1",
         "OCN SXC Control 3 - Config APS 2",
         "OCN SXC Control 3 - Config APS 3",
         "OCN SXC Control 3 - Config APS 4",
         "OCN SXC Control 3 - Config APS 5",
         "OCN SXC Control 3 - Config APS 6",
         "OCN SXC Control 3 - Config APS 7",
         "OCN SXC Control 3 - Config APS 8",
         "OCN SXC Control 3 - Config APS 9",
         "OCN SXC Control 3 - Config APS 10",
         "OCN SXC Control 3 - Config APS 11", /*104*/

         "OCN Rx High Order Map concatenate configuration 4", /* 105 */
         "OCN Rx High Order Map concatenate configuration 5",
         "OCN Rx High Order Map concatenate configuration 6",
         "OCN Rx High Order Map concatenate configuration 7", /* 108 */
         "OCN VTTU Pointer Generator Per Channel Control 4",
         "OCN VTTU Pointer Generator Per Channel Control 5",
         "OCN VTTU Pointer Generator Per Channel Control 6",
         "OCN VTTU Pointer Generator Per Channel Control 7", /* 112 */
         "OCN TXPP Per STS Multiplexing Control 4",
         "OCN TXPP Per STS Multiplexing Control 5",
         "OCN TXPP Per STS Multiplexing Control 6",
         "OCN TXPP Per STS Multiplexing Control 7", /* 116 */
         "OCN VTTU Pointer Interpreter Per Channel Control 4",
         "OCN VTTU Pointer Interpreter Per Channel Control 5",
         "OCN VTTU Pointer Interpreter Per Channel Control 6",
         "OCN VTTU Pointer Interpreter Per Channel Control 7", /* 120 */
         "OCN RXPP Per STS payload Control 4",
         "OCN RXPP Per STS payload Control 5",
         "OCN RXPP Per STS payload Control 6",
         "OCN RXPP Per STS payload Control 7"/* 124 */
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static eBool NeedIssolateDcc(ThaModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return Tha60290022DeviceHasKByteFeatureOnly(device);
    }

static eAtRet Vc3ToTu3VcEnable(ThaModuleOcn self, AtSdhChannel vc3, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 offset = Tha60210011ModuleOcnLoStsDefaultOffset(vc3, AtSdhChannelSts1Get(vc3));

    /* Line-side VC3 to TU3 CEP. */
    regAddr = ThaModuleOcnRxStsPayloadControl(self, vc3) + offset;
    regVal  = mChannelHwRead(vc3, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_demramctl_PiIwkRxVc3toTu3_, enable ? 1 : 0);
    mChannelHwWrite(vc3, regAddr, regVal, cThaModuleOcn);

    /* TU3 CEP to Line-side VC3. */
    regAddr = ThaModuleOcnTxStsMultiplexingControl(self, vc3) + offset;
    regVal  = mChannelHwRead(vc3, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_pgdemramctl_PgIwkTxTu3toVc3_, enable ? 1 : 0);
    mChannelHwWrite(vc3, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool Vc3ToTu3VcIsEnabled(ThaModuleOcn self, AtSdhChannel vc3)
    {
    uint32 regAddr = ThaModuleOcnTxStsMultiplexingControl(self, vc3) +
                     Tha60210011ModuleOcnLoStsDefaultOffset(vc3, AtSdhChannelSts1Get(vc3));
    uint32 regVal  = mChannelHwRead(vc3, regAddr, cThaModuleOcn);
    return mRegField(regVal, cAf6_pgdemramctl_PgIwkTxTu3toVc3_) ? cAtTrue : cAtFalse;
    }

static eAtRet Tu3VcToVc3Enable(ThaModuleOcn self, AtSdhChannel tu3Vc, eBool enable)
    {
    uint32 regAddr, regVal;

    /* Line-side TU3 to VC3 CEP. */
    regAddr = Tha60210011ModuleOcnRxHighOrderMapConcatenationControl((Tha60210011ModuleOcn)self) +
              ThaModuleOcnStsDefaultOffset(self, tu3Vc, AtSdhChannelSts1Get(tu3Vc));
    regVal  = mChannelHwRead(tu3Vc, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapIwkRxTu3toVc3_, enable ? 1 : 0);
    mChannelHwWrite(tu3Vc, regAddr, regVal, cThaModuleOcn);

    /* VC3 CEP to Line-side TU3. */
    regAddr = ThaModuleOcnTxStsMultiplexingControl(self, tu3Vc) +
              Tha60210011ModuleOcnLoStsDefaultOffset(tu3Vc, AtSdhChannelSts1Get(tu3Vc));
    regVal  = mChannelHwRead(tu3Vc, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_pgdemramctl_PgIwkTxVc3toTu3_, enable ? 1 : 0);
    mChannelHwWrite(tu3Vc, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool Tu3VcToVc3IsEnabled(ThaModuleOcn self, AtSdhChannel tu3Vc)
    {
    uint32 regAddr = ThaModuleOcnTxStsMultiplexingControl(self, tu3Vc) +
                     Tha60210011ModuleOcnLoStsDefaultOffset(tu3Vc, AtSdhChannelSts1Get(tu3Vc));
    uint32 regVal  = mChannelHwRead(tu3Vc, regAddr, cThaModuleOcn);
    return mRegField(regVal, cAf6_pgdemramctl_PgIwkTxVc3toTu3_) ? cAtTrue : cAtFalse;
    }

static eAtRet HoLinePiStsConcatMasterSet(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 masterStsId)
    {
    uint32 regAddr, regVal;
    uint8 hwSlice, mastHwSts;

    if (!mMethodsGet(self)->ChannelCanBeTerminated(self, channel))
        return cAtOk;

    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &hwSlice, &mastHwSts);

    /* HO-Line STS */
    regAddr = Tha60210011ModuleOcnRxHighOrderMapConcatenationControl(self) +
              ThaModuleOcnStsDefaultOffset((ThaModuleOcn)self, channel, masterStsId);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapIwkRxTu3toVc3_, 0);
    mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapStsSlvInd_, 0);
    mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapStsMstId_, mastHwSts);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    /* Disable VC3 interworking as well. */
    regAddr = ThaModuleOcnTxStsMultiplexingControl((ThaModuleOcn)self, channel) +
              Tha60210011ModuleOcnLoStsDefaultOffset(channel, masterStsId);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_pgdemramctl_PgIwkTxVc3toTu3_, 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet HoLinePiStsConcatSlaveSet(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId)
    {
    uint32 regAddr, regVal;
    uint8 masterHwStsId, hwSlice;

    if (!mMethodsGet(self)->ChannelCanBeTerminated(self, channel))
        return cAtOk;

    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &hwSlice, &masterHwStsId);

    /* HO-Line STS */
    regAddr = Tha60210011ModuleOcnRxHighOrderMapConcatenationControl(self) +
              ThaModuleOcnStsDefaultOffset((ThaModuleOcn)self, channel, slaveStsId);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapIwkRxTu3toVc3_, 0);
    mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapStsSlvInd_, 1);
    mRegFieldSet(regVal, cAf6_rxhomapramctl_HoMapStsMstId_, masterHwStsId);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    /* Disable VC3 interworking as well. */
    regAddr = ThaModuleOcnTxStsMultiplexingControl((ThaModuleOcn)self, channel) +
              Tha60210011ModuleOcnLoStsDefaultOffset(channel, slaveStsId);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_pgdemramctl_PgIwkTxVc3toTu3_, 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool IsFateplateChannel(AtSdhChannel channel)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)channel);
    return Tha60290021ModuleSdhLineIsFaceplate(sdhModule, AtSdhChannelLineGet(channel));
    }

static eAtRet HoLinePiStsTerminationEnable(Tha60290022ModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool enable)
   {
   uint32 regAddr, regVal;

   regAddr = cAf6Reg_spiramctl_Base + ThaModuleOcnStsDefaultOffset((ThaModuleOcn)self, channel, stsId);
   regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
   mRegFieldSet(regVal, cAf6_spiramctl_LineStsPiTerm_, (enable ? 1:0));
   mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
   return cAtOk;
   }

static eAtRet HoLinePiStsChannelTerminationEnable(Tha60290022ModuleOcn self, AtSdhChannel channel, eBool enable)
   {
   return HoLinePiStsTerminationEnable(self, channel, AtSdhChannelSts1Get(channel), enable);
   }

static eAtRet StsPohInsertEnable(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool enable)
    {
    eAtRet ret = m_ThaModuleOcnMethods->StsPohInsertEnable(self, channel, stsId, enable);

    if ((ret == cAtOk) && IsFateplateChannel(channel) && AtSdhChannelIsHoVc(channel))
        ret = HoLinePiStsTerminationEnable((Tha60290022ModuleOcn)self, channel, stsId, enable);

    return ret;
    }

static eBool HasDiagnosticIp(Tha60290021ModuleOcn self)
    {
    return Tha60290022DeviceEthHasSgmiiDiagnosticLogic(AtModuleDeviceGet((AtModule)self));
    }

static uint8 FaceplateLineRxZnRegValue2Position(Tha60290021ModuleOcn self, uint32 regVal)
    {
    uint8 aug1 = (uint8)mRegField(regVal, cAf6_rfmz1z2selctl_OCNRxZnAugId_);
    uint8 au3  = (uint8)mRegField(regVal, cAf6_rfmz1z2selctl_OCNRxZnAu3Id_);
    AtUnused(self);
    return (uint8)(aug1 * 3 + au3);
    }

static uint32 FaceplateLineRxZnPosition2RegValue(Tha60290021ModuleOcn self, uint8 sts1)
    {
    uint32 regVal = 0;
    AtUnused(self);
    mRegFieldSet(regVal, cAf6_rfmz1z2selctl_OCNRxZnAugId_, (sts1 / 3));
    mRegFieldSet(regVal, cAf6_rfmz1z2selctl_OCNRxZnAu3Id_, (sts1 % 3));
    return regVal;
    }

static eBool IsBridgeAndRollRam(AtInternalRam ram)
    {
    return AtStrstr(AtInternalRamName(ram), "Bridge and Roll SXC Control") ? cAtTrue : cAtFalse;
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    if (IsBridgeAndRollRam(ram))
        return cAtTrue;
    return m_AtModuleMethods->InternalRamIsReserved(self, ram);
    }

static eAtRet FaceplateSts192cEnable(Tha60290022ModuleOcn self, uint32 lineId, eBool enabled)
    {
    uint32 regAddr, regVal;

    regAddr = Tha60290021ModuleOcnFaceplateGlobalRxFramerControl((ThaModuleOcn)self, lineId);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_glbrfm_reg_RxFrmSts192CEnb_, enabled ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = Tha60290021ModuleOcnFaceplateGlobalTxFramerControl((ThaModuleOcn)self, lineId);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_glbtfm_reg_TxFrmSts192CEnb_, enabled ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 LoopbackAddress(Tha60290022ModuleOcn self)
    {
    return cAf6Reg_glbloop_reg_Base + ThaModuleOcnBaseAddress((ThaModuleOcn)self);
    }

static uint32 LocalGroupId(Tha60290022ModuleOcn self, uint32 localId)
    {
    AtUnused(self);
    return (localId) / 4U;
    }

static uint32 Sts192cEnbMask(Tha60290022ModuleOcn self, uint32 localId)
    {
    return cAf6_glbloop_reg_HoMapSts192cEn0_Mask << LocalGroupId(self, localId);
    }

static uint32 Sts192cEnbShift(Tha60290022ModuleOcn self, uint32 localId)
    {
    return cAf6_glbloop_reg_HoMapSts192cEn0_Shift + LocalGroupId(self, localId);
    }

static eAtRet TerminatedLineRateSet(Tha60290022ModuleOcn self, uint32 localId, eAtSdhLineRate rate)
    {
    uint32 regAddr, regVal;
    uint32 sts192cFieldMask = Sts192cEnbMask(self, localId);
    uint32 sts192cFieldShift = Sts192cEnbShift(self, localId);

    regAddr = LoopbackAddress(self);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, sts192cField, (rate == cAtSdhLineRateStm64) ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtSdhLineRate TerminatedLineRateGet(Tha60290022ModuleOcn self, uint32 localId)
    {
    uint32 regAddr, regVal;
    uint32 sts192cFieldMask = Sts192cEnbMask(self, localId);
    uint32 sts192cFieldShift = Sts192cEnbShift(self, localId);

    regAddr = LoopbackAddress(self);
    regVal  = mModuleHwRead(self, regAddr);
    return mRegField(regVal, sts192cField) ? cAtSdhLineRateStm64 : cAtSdhLineRateStm16;
    }

static eBool Sts192cIsSupported(Tha60290021ModuleOcn self)
    {
    return Tha60290022DeviceSts192cIsSupported(AtModuleDeviceGet((AtModule)self));
    }

static AtModuleSdh ModuleSdh(AtSdhChannel channel)
    {
    return (AtModuleSdh)AtChannelModuleGet((AtChannel)channel);
    }

static eBool IsFaceplateChannel(AtSdhChannel channel)
    {
    return Tha60290021ModuleSdhLineIsFaceplate(ModuleSdh(channel), AtSdhChannelLineGet(channel));
    }

static eAtRet UpperSts48_StsIdSw2Hw(ThaModuleOcn self, AtSdhChannel sdhChannel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8* hwStsInSlice)
    {
    eAtRet ret;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 numStsPerSlice = ThaModuleOcnNumStsInOneSlice(self);

    ret = m_ThaModuleOcnMethods->StsIdSw2Hw(self, sdhChannel, phyModule, swSts, sliceId, hwStsInSlice);
    if (ret != cAtOk)
        return ret;

    if (numSts > numStsPerSlice)
        *sliceId = (uint8)(*sliceId + (swSts / numStsPerSlice));

    return cAtOk;
    }

static eAtRet StsIdSw2Hw(ThaModuleOcn self, AtSdhChannel sdhChannel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8* hwStsInSlice)
    {
    if (IsFaceplateChannel(sdhChannel))
        return m_ThaModuleOcnMethods->StsIdSw2Hw(self, sdhChannel, phyModule, swSts, sliceId, hwStsInSlice);

    return UpperSts48_StsIdSw2Hw(self, sdhChannel, phyModule, swSts, sliceId, hwStsInSlice);
    }

static uint32 GlobalDccEnableAddress(ThaModuleOcn self)
    {
    AtDevice device = (AtDevice)AtModuleDeviceGet((AtModule)self);
    if (Tha60290022DeviceDccV4dot1EnableRegAddressIsSupported(device))
        return cAf6Reg_upen_cfg_dcc_en_V4dot1;

    return cAf6Reg_upen_cfg_dcc_en;
    }

static void DccHdlcTestPktGeneratorDisable(ThaModuleOcn self)
    {
    uint32 address = ThaModuleOcnSohOverEthBaseAddress(self) + cAf6Reg_upen_dcc_cfg_testgen_enacid_Base;
    mModuleHwWrite(self, address, 0);
    }

static eAtRet DccEnable(ThaModuleOcn self, eBool enable)
    {
    uint32 address = ThaModuleOcnSohOverEthBaseAddress(self) + GlobalDccEnableAddress(self);
    mModuleHwWrite(self, address, (enable ? 1:0));
    return cAtOk;
    }

static eAtRet SohOverEthInit(ThaModuleOcn self)
    {
    eAtRet ret = m_ThaModuleOcnMethods->SohOverEthInit(self);

    if (Tha60290022DeviceDccV2IsSupported(AtModuleDeviceGet((AtModule)self)))
        ret |= DccEnable(self, cAtTrue);

    DccHdlcTestPktGeneratorDisable(self);

    return ret;
    }

static eBool NeedMasterTfi5(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 TxTfi5Sts192cEnbMask(Tha60290022ModuleOcn self, uint32 localId)
    {
    return cAf6_tfi5glbtfm_reg_Tfi5TxLineSts192cEn0_Mask << LocalGroupId(self, localId);
    }

static uint32 TxTfi5Sts192cEnbShift(Tha60290022ModuleOcn self, uint32 localId)
    {
    return cAf6_tfi5glbtfm_reg_Tfi5TxLineSts192cEn0_Shift + LocalGroupId(self, localId);
    }

static uint32 RxTfi5Sts192cEnbMask(Tha60290022ModuleOcn self, uint32 localId)
    {
    return cAf6_tfi5glbrfm_reg_Tfi5RxLineSts192cEn0_Mask << LocalGroupId(self, localId);
    }

static uint32 RxTfi5Sts192cEnbShift(Tha60290022ModuleOcn self, uint32 localId)
    {
    return cAf6_tfi5glbrfm_reg_Tfi5RxLineSts192cEn0_Shift + LocalGroupId(self, localId);
    }

static uint8 LocalMateLineId(AtSdhChannel channel)
    {
    return (uint8)(AtSdhChannelLineGet(channel) - Tha60290021ModuleSdhMateLineStartId(ModuleSdh(channel)));
    }

static eAtRet HelperMateSts192cEnable(Tha60290022ModuleOcn self, uint8 localId, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 sts192cFieldMask, sts192cFieldShift;
    uint32 baseAddress = ThaModuleOcnBaseAddress((ThaModuleOcn)self);

    /* TX */
    sts192cFieldMask = TxTfi5Sts192cEnbMask(self, localId);
    sts192cFieldShift = TxTfi5Sts192cEnbShift(self, localId);

    regAddr = baseAddress + Tha60210011ModuleOcnTfi5GlbtfmBase((ThaModuleOcn)self);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, sts192cField, (enable) ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    /* RX */
    sts192cFieldMask = RxTfi5Sts192cEnbMask(self, localId);
    sts192cFieldShift = RxTfi5Sts192cEnbShift(self, localId);

    regAddr = baseAddress + Tha60210011ModuleOcnTfi5GlbrfmBase((ThaModuleOcn)self);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, sts192cField, (enable) ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet MateSts192cEnable(Tha60290022ModuleOcn self, AtSdhChannel channel, eBool enable)
    {
    return HelperMateSts192cEnable(self, LocalMateLineId(channel), enable);
    }

static eAtRet MateDefaultSet(ThaModuleOcn self)
    {
    eAtRet ret;

    ret  = HelperMateSts192cEnable((Tha60290022ModuleOcn)self, 0, cAtFalse);  /* Mate #0-3 */
    ret |= HelperMateSts192cEnable((Tha60290022ModuleOcn)self, 4, cAtFalse);  /* Mate #4-7 */

    return ret;
    }

static eAtRet FaceplateLosAndClockMonitorEnable(ThaModuleOcn self, eBool enable)
    {
    uint32 baseAddress = ThaModuleOcnBaseAddress(self);
    uint32 numGroup = Tha60290021ModuleOcnNumFaceplateLineGroups(self);
    uint32 group;

    for (group = 0; group < numGroup; group++)
        {
        uint32 groupOffset = Tha60290021ModuleOcnFaceplateGroupOffset(self, group);
        uint32 address = baseAddress + cAf6Reg_glbclkmon_reg_Base + groupOffset;
        uint32 regVal = mModuleHwRead(self, address);
        mRegFieldSet(regVal, cAf6_glbclkmon_reg_RxFrmLosDetDis_, enable ? 0 : 1);
        mRegFieldSet(regVal, cAf6_glbclkmon_reg_RxFrmClkMonDis_, enable ? 0 : 1);
        mModuleHwWrite(self, address, regVal);
        }

    return cAtOk;
    }

static eAtRet FacePlateDefaultSet(ThaModuleOcn self)
    {
    eAtRet ret;

    ret  = FaceplateSts192cEnable((Tha60290022ModuleOcn)self, 0, cAtFalse);
    ret |= FaceplateSts192cEnable((Tha60290022ModuleOcn)self, 8, cAtFalse);
    ret |= FaceplateLosAndClockMonitorEnable(self, cAtFalse);

    return ret;
    }

static eAtRet DefaultSet(ThaModuleOcn self)
    {
    eAtRet ret;

    ret = m_ThaModuleOcnMethods->DefaultSet(self);
    if (ret != cAtOk)
        return ret;

    ret |= MateDefaultSet(self);
    ret |= FacePlateDefaultSet(self);

    return ret;
    }

static eAtRet ClockExtractorSquelchingAisLEnable(Tha60290022ModuleOcn self, uint8 lineId, eBool enable)
    {
    uint32 group = Tha60290021ModuleOcnFaceplateLineGroup((ThaModuleOcn)self, lineId);
    uint32 groupOffset = Tha60290021ModuleOcnFaceplateGroupOffset((ThaModuleOcn)self, group);
    uint32 address = cAf6_glb8kaislsquel_reg_Base + ThaModuleOcnBaseAddress((ThaModuleOcn)self) + groupOffset;
    uint32 shift = mFaceplateLineLocalInGroup(lineId);
    uint32 mask = cBit0 << shift;
    uint32 regVal = mModuleHwRead(self, address);
    AtModuleSdh moduleSdh = (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleSdh);

    if (lineId >= Tha60290021ModuleSdhNumFaceplateLines(moduleSdh))
        return cAtOk;

    mFieldIns(&regVal, mask, shift, (enable ? 0 : 1));
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

static eBool ClockExtractorSquelchingAisLIsEnabled(Tha60290022ModuleOcn self, uint8 lineId)
    {
    uint32 group = Tha60290021ModuleOcnFaceplateLineGroup((ThaModuleOcn)self, lineId);
    uint32 groupOffset = Tha60290021ModuleOcnFaceplateGroupOffset((ThaModuleOcn)self, group);
    uint32 address = cAf6_glb8kaislsquel_reg_Base + ThaModuleOcnBaseAddress((ThaModuleOcn)self) + groupOffset;
    uint32 field_Shift = mFaceplateLineLocalInGroup(lineId);
    uint32 field_Mask = cBit0 << field_Shift;
    uint32 regVal = mModuleHwRead(self, address);
    AtModuleSdh moduleSdh = (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleSdh);

    if (lineId >= Tha60290021ModuleSdhNumFaceplateLines(moduleSdh))
        return cAtFalse;

    if (mRegField(regVal, field_) == 0)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 TohSonetSdhModeMask(Tha60290022ModuleOcn self, uint32 localId)
    {
    AtUnused(self);
    return cAf6_glbsonetsdhmode_TohSonetSdhMode_Line0_Mask << localId;
    }

static uint32 TohSonetSdhModeShift(Tha60290022ModuleOcn self, uint32 localId)
    {
    AtUnused(self);
    return cAf6_glbsonetsdhmode_TohSonetSdhMode_Line0_Shift + localId;
    }

static uint32 TohSonetSdhModeAddress(Tha60290022ModuleOcn self)
    {
    return cAf6Reg_glbsonetsdhmode_reg_Base + ThaModuleOcnBaseAddress((ThaModuleOcn)self);
    }

static eAtRet FaceplateLineModeSet(Tha60290022ModuleOcn self, uint8 lineId, eAtSdhChannelMode mode)
    {
    uint32 regAddr, regVal;
    uint32 tohSonetSdhModeMask = TohSonetSdhModeMask(self, lineId);
    uint32 tohSonetSdhModeShift = TohSonetSdhModeShift(self, lineId);

    regAddr = TohSonetSdhModeAddress(self);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, tohSonetSdhMode, ThaSdhHwChannelMode(mode));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtSdhChannelMode FaceplateLineModeGet(Tha60290022ModuleOcn self, uint8 lineId)
    {
    uint32 regAddr, regVal;
    uint32 tohSonetSdhModeMask = TohSonetSdhModeMask(self, lineId);
    uint32 tohSonetSdhModeShift = TohSonetSdhModeShift(self, lineId);

    regAddr = TohSonetSdhModeAddress(self);
    regVal  = mModuleHwRead(self, regAddr);
    return ThaSdhSwChannelMode((uint8)mRegField(regVal, tohSonetSdhMode));
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn module = (ThaModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleOcnMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, m_ThaModuleOcnMethods, sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, NeedIssolateDcc);
        mMethodOverride(m_ThaModuleOcnOverride, Vc3ToTu3VcEnable);
        mMethodOverride(m_ThaModuleOcnOverride, Vc3ToTu3VcIsEnabled);
        mMethodOverride(m_ThaModuleOcnOverride, Tu3VcToVc3Enable);
        mMethodOverride(m_ThaModuleOcnOverride, Tu3VcToVc3IsEnabled);
        mMethodOverride(m_ThaModuleOcnOverride, StsIdSw2Hw);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthInit);
        mMethodOverride(m_ThaModuleOcnOverride, StsPohInsertEnable);
        mMethodOverride(m_ThaModuleOcnOverride, DefaultSet);
        }

    mMethodsSet(module, &m_ThaModuleOcnOverride);
    }

static void OverrideTha60210011ModuleOcn(AtModule self)
    {
    Tha60210011ModuleOcn module = (Tha60210011ModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleOcnMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleOcnOverride, m_Tha60210011ModuleOcnMethods, sizeof(m_Tha60210011ModuleOcnOverride));

        mMethodOverride(m_Tha60210011ModuleOcnOverride, LoPathPhyModuleIsDividedToBlockOc24);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, HoPathPhyModuleIsDividedToOc24);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, TuVcIsLoopbackAtPointerProcessor);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, HoLinePiStsConcatMasterSet);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, HoLinePiStsConcatSlaveSet);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, NeedMasterTfi5);
        }

    mMethodsSet(module, &m_Tha60210011ModuleOcnOverride);
    }

static void OverrideTha60290021ModuleOcn(AtModule self)
    {
    Tha60290021ModuleOcn module = (Tha60290021ModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModuleOcnOverride, mMethodsGet(module), sizeof(m_Tha60290021ModuleOcnOverride));

        mMethodOverride(m_Tha60290021ModuleOcnOverride, NumFaceplateSlices);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, FaceplateXcStartLineId);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, TerminatedXcStartLineId);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, HasOverheadCpuControl);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, HasRoleStatus);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, NumXcLines);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, HasDiagnosticIp);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, FaceplateLineRxZnRegValue2Position);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, FaceplateLineRxZnPosition2RegValue);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, Sts192cIsSupported);
        }

    mMethodsSet(module, &m_Tha60290021ModuleOcnOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleOcn(self);
    OverrideTha60290021ModuleOcn(self);
    OverrideTha60210011ModuleOcn(self);
    }

static void MethodsInit(Tha60290022ModuleOcn self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ClockExtractorSquelchingAisLEnable);
        mMethodOverride(m_methods, ClockExtractorSquelchingAisLIsEnabled);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleOcn);
    }

AtModule Tha60290022ModuleOcnObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290022ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60290022ModuleOcnObjectInit(newModule, device);
    }

eAtRet Tha60290022ModuleOcnFaceplateSts192cEnable(Tha60290022ModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return FaceplateSts192cEnable(self, lineId, enable);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290022ModuleOcnTerminatedLineRateSet(Tha60290022ModuleOcn self, uint32 localId, eAtSdhLineRate rate)
    {
    if (self)
        return TerminatedLineRateSet(self, localId, rate);
    return cAtErrorNullPointer;
    }

eAtSdhLineRate Tha60290022ModuleOcnTerminatedLineRateGet(Tha60290022ModuleOcn self, uint32 localId)
    {
    if (self)
        return TerminatedLineRateGet(self, localId);
    return cAtSdhLineRateInvalid;
    }

eAtRet Tha60290022ModuleOcnHoLinePiStsTerminationEnable(Tha60290022ModuleOcn self, AtSdhChannel channel, eBool enable)
    {
    return HoLinePiStsChannelTerminationEnable(self, channel, enable);
    }

eAtRet Tha60290022ModuleOcnClockExtractorSquelchingAisLEnable(ThaModuleOcn self, uint8 lineId, eBool enable)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClockExtractorSquelchingAisLEnable(mThis(self), lineId, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60290022ModuleOcnClockExtractorSquelchingAisLIsEnabled(ThaModuleOcn self, uint8 lineId)
    {
    if (self)
        return mMethodsGet(mThis(self))->ClockExtractorSquelchingAisLIsEnabled(mThis(self), lineId);
    return cAtFalse;
    }

eAtRet Tha60290022ModuleOcnMateSts192cEnable(Tha60290022ModuleOcn self, AtSdhChannel channel, eBool enable)
    {
    if (self)
        return MateSts192cEnable(self, channel, enable);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290022ModuleOcnFaceplateLineModeSet(Tha60290022ModuleOcn self, uint8 lineId, eAtSdhChannelMode mode)
    {
    if (self)
        return FaceplateLineModeSet(self, lineId, mode);
    return cAtErrorNullPointer;
    }

eAtSdhChannelMode Tha60290022ModuleOcnFaceplateLineModeGet(Tha60290022ModuleOcn self, uint8 lineId)
    {
    if (self)
        return FaceplateLineModeGet(self, lineId);
    return cAtSdhChannelModeUnknown;
    }

eAtRet Tha60290022ModuleOcnHelperMateSts192cEnable(Tha60290022ModuleOcn self, uint8 localId, eBool enable)
    {
    return HelperMateSts192cEnable(self, localId, enable);
    }

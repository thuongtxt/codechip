/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60290022ModuleOcn.h
 * 
 * Created Date: Dec 5, 2017
 *
 * Description : Module OCN
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEOCN_H_
#define _THA60290022MODULEOCN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhChannel.h"
#include "../../../default/ocn/ThaModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModuleOcn *Tha60290022ModuleOcn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290022ModuleOcnFaceplateSts192cEnable(Tha60290022ModuleOcn self, uint32 lineId, eBool enable);
eAtRet Tha60290022ModuleOcnTerminatedLineRateSet(Tha60290022ModuleOcn self, uint32 localId, eAtSdhLineRate rate);
eAtSdhLineRate Tha60290022ModuleOcnTerminatedLineRateGet(Tha60290022ModuleOcn self, uint32 localId);
eAtRet Tha60290022ModuleOcnHoLinePiStsTerminationEnable(Tha60290022ModuleOcn self, AtSdhChannel channel, eBool enable);
eAtRet Tha60290022ModuleOcnClockExtractorSquelchingAisLEnable(ThaModuleOcn self, uint8 lineId, eBool enable);
eBool Tha60290022ModuleOcnClockExtractorSquelchingAisLIsEnabled(ThaModuleOcn self, uint8 lineId);
eAtRet Tha60290022ModuleOcnMateSts192cEnable(Tha60290022ModuleOcn self, AtSdhChannel channel, eBool enable);

/* Faceplate */
eAtRet Tha60290022ModuleOcnFaceplateLineModeSet(Tha60290022ModuleOcn self, uint8 lineId, eAtSdhChannelMode mode);
eAtSdhChannelMode Tha60290022ModuleOcnFaceplateLineModeGet(Tha60290022ModuleOcn self, uint8 lineId);
eAtRet Tha60290022ModuleOcnHelperMateSts192cEnable(Tha60290022ModuleOcn self, uint8 localId, eBool enable);
eAtRet Tha60290022ModuleOcnFaceplateSts192cEnable(Tha60290022ModuleOcn self, uint32 lineId, eBool enabled);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEOCN_H_ */

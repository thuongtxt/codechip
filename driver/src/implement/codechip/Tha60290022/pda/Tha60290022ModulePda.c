/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60290022ModulePda.c
 *
 * Created Date: Apr 23, 2017
 *
 * Description : PDA  module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/man/Tha60210011Device.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "Tha60290022ModulePdaInternal.h"
#include "Tha60290022ModulePdaReg.h"
#include "../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/
#define cRemovedField   0

#define cVcSignalTypeVc3     0
#define cVcSignalTypeVc4     1
#define cVcSignalTypeVc4_4c  2
#define cVcSignalTypeVc4_16c 3
#define cVcSignalTypeVc11    4
#define cVcSignalTypeVc12    4

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)        ((Tha60290022ModulePda)(self))
#define mBaseAddress(self) ThaModulePdaBaseAddress((ThaModulePda)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290022ModulePdaMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModulePdaMethods         m_ThaModulePdaOverride;
static tThaModulePdaV2Methods       m_ThaModulePdaV2Override;
static tTha60210011ModulePdaMethods m_Tha60210011ModulePdaOverride;
static tTha60210051ModulePdaMethods m_Tha60210051ModulePdaOverride;

/* Save super implementation */
static const tAtModuleMethods             *m_AtModuleMethods             = NULL;
static const tThaModulePdaMethods         *m_ThaModulePdaMethods         = NULL;
static const tTha60210011ModulePdaMethods *m_Tha60210011ModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 Oc48IdFromSlice(Tha60210011ModulePda self, AtPw pwAdapter, uint8 sliceId)
    {
    AtUnused(self);
    AtUnused(pwAdapter);
    return sliceId;
    }

static uint32 HoLoOc48IdMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwHoLoOc48Id_Mask;
    }

static uint32 HoLoOc48IdShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwHoLoOc48Id_Shift;
    }

static uint32 TdmLineIdMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwTdmLineId_Mask;
    }

static uint32 TdmLineIdShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwTdmLineId_Shift;
    }

static uint32 PrbsStatusBase(ThaModulePda self)
    {
    AtUnused(self);
    return cAf6Reg_ramlotdmprbsmon_Base;
    }

static uint32 PwLoSlice24SelMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cRemovedField;
    }

static uint32 PwLoSlice24SelShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cRemovedField;
    }

static uint32 PwCEPModeMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwCEPMode_Mask;
    }

static uint32 PwCEPModeShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwCEPMode_Shift;
    }

static uint32 PwLowDs0ModeMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwLowDs0Mode_Mask;
    }

static uint32 PwLowDs0ModeShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcfg_PwLowDs0Mode_Shift;
    }

static uint8 NumOc48s(Tha60210011ModulePda self)
    {
    Tha60210011Device device = (Tha60210011Device)AtModuleDeviceGet((AtModule)self);
    return Tha60210011DeviceNumUsedOc48Slices(device);
    }

static eAtRet LookupReset(Tha60210011ModulePda self)
    {
    uint32 oc48_i;
    uint32 baseAddress = ThaModulePdaBaseAddress((ThaModulePda)self);
    uint32 cPwLookupResetValue = mMethodsGet(self)->PwLookupPwIDMask(self);
    uint32 cNumPwsPerSlice = mMethodsGet(mThis(self))->NumHwPwInOc48Slice(mThis(self));

    for (oc48_i = 0; oc48_i < NumOc48s(self); oc48_i++)
        {
        uint32 tdmPwId;

        for (tdmPwId = 0; tdmPwId < cNumPwsPerSlice; tdmPwId++)
            {
            uint32 offset = mMethodsGet(mThis(self))->TdmLookupOffset(mThis(self), (uint8)oc48_i, tdmPwId);
            uint32 regAddr = cAf6Reg_ramtdmlkupcfg_Base + offset + baseAddress;

            mModuleHwWrite(self, regAddr, cPwLookupResetValue);
            }
        }

    return cAtOk;
    }

static uint32 PwTdmLookupOffset(Tha60210011ModulePda self, AtPw pw)
    {
    return ThaModulePdaPwTdmOffset((ThaModulePda)self, pw);
    }

static uint32 PwLoTdmLookupOffset(Tha60210011ModulePda self, AtPw pw)
    {
    return PwTdmLookupOffset(self, pw);
    }

static uint32 PwHoTdmLookupOffset(Tha60210011ModulePda self, AtPw pw)
    {
    return PwTdmLookupOffset(self, pw);
    }

static eBool IsTdmOc192(AtPw pw)
    {
    AtSdhChannel vc;

    if (AtPwTypeGet(pw) != cAtPwTypeCEP)
        return cAtFalse;

    vc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    if (AtSdhChannelTypeGet(vc) == cAtSdhChannelTypeVc4_64c)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsNewOc192cOffset(ThaModulePda self)
    {
    return Tha60290022DevicePdaIsNewOc192cOffset(AtModuleDeviceGet((AtModule)self));
    }

static uint32 PwTdmOc192Offset(ThaModulePda self, AtPw pw)
    {
    uint8 slice = 0;

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    if (IsNewOc192cOffset(self))
        return (slice / 4U) * 4U;

    return (slice / 4U) * 8U;
    }

static uint32 TdmOc192LookupControl(Tha60210011ModulePda self, AtPw pw)
    {
    uint32 ramtdm192lkupcfg_Base = mMethodsGet(mThis(self))->Oc192cTdmLookupCtrl(mThis(self));
    uint32 address = ramtdm192lkupcfg_Base + mBaseAddress(self) + PwTdmLookupOffset(self, pw);
    return address;
    }

static uint32 TdmLookupControl(Tha60210011ModulePda self, AtPw pw)
    {
    if (IsTdmOc192(pw))
        return TdmOc192LookupControl(self, pw);

    return cAf6Reg_ramtdmlkupcfg_Base + mBaseAddress(self) + PwTdmLookupOffset(self, pw);
    }

static eAtRet PwTdmLookupReset(Tha60210011ModulePda self, AtPw pw)
    {
    uint32 cPwLookupResetValue = mMethodsGet(self)->PwLookupPwIDMask(self);
    mChannelHwWrite(pw, TdmLookupControl(self, pw), cPwLookupResetValue, cThaModulePda);
    return cAtOk;
    }

static eAtRet PwLoTdmLookupReset(Tha60210011ModulePda self, AtPw pw)
    {
    return PwTdmLookupReset(self, pw);
    }

static eAtRet PwHoTdmLookupReset(Tha60210011ModulePda self, AtPw pw)
    {
    return PwTdmLookupReset(self, pw);
    }

static uint32 PWPdaTdmModeCtrlAbsoluteAddress(ThaModulePda self, AtPw adapter)
    {
    return mMethodsGet(self)->PWPdaTdmModeCtrl(self, adapter) + mTdmOffset(self, adapter);
    }

static eAtRet PwPdaModeSet(ThaModulePda self, AtPw adapter, uint8 mode)
    {
    uint32 address = PWPdaTdmModeCtrlAbsoluteAddress(self, adapter);
    uint32 regVal  = mChannelHwRead(adapter, address, cThaModulePda);
    mRegFieldSet(regVal, cAf6_ramtdmmodecfg_PDAMode_, mode);
    mChannelHwWrite(adapter, address, regVal, cThaModulePda);

    return Tha60210011ModulePdaPWCESoPLowRateEnable(self, adapter);
    }

static eAtRet PwTdmLookupSet(Tha60210011ModulePda self, AtPw pw)
    {
    uint32 regAddr = TdmLookupControl(self, pw);
    uint32 ramtdmlkupcfg_PwIDMask = mMethodsGet(self)->PwLookupPwIDMask(self);
    uint32 ramtdmlkupcfg_PwIDShift = mMethodsGet(self)->PwLookupPwIDShift(self);
    uint32 regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    mRegFieldSet(regVal, ramtdmlkupcfg_PwID, AtChannelIdGet((AtChannel)pw));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);
    return cAtOk;
    }

static uint32 PwLookupPwIDMask(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramtdmlkupcfg_PwID_Mask;
    }

static uint8 PwLookupPwIDShift(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6_ramtdmlkupcfg_PwID_Shift;
    }

static eAtRet PwHoTdmLookupSet(Tha60210011ModulePda self, AtPw pw)
    {
    return PwTdmLookupSet(self, pw);
    }

static eAtRet PwLoTdmLookupSet(Tha60210011ModulePda self, AtPw pw)
    {
    return PwTdmLookupSet(self, pw);
    }

static eAtRet PwPdaTdmLookupEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    uint32 regAddr = TdmLookupControl((Tha60210011ModulePda)self, pw);
    uint32 regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    mRegFieldSet(regVal, cAf6_ramtdmlkupcfg_PwLkEnable_, mBoolToBin(enable));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);
    return cAtOk;
    }

static uint32 TdmOc48Pwid(AtPw pw)
    {
    return AtChannelHwIdGet((AtChannel)pw);
    }

static uint32 PwTdmOffset(ThaModulePda self, AtPw pw)
    {
    uint8 slice = 0;
    uint32 tdmPwId;

    if (IsTdmOc192(pw))
        return PwTdmOc192Offset(self, pw);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    tdmPwId = TdmOc48Pwid(pw);
    if (AtModuleAccessible((AtModule)self))
        AtAssert(tdmPwId != cInvalidUint32);
    else
        tdmPwId = 0; /* To avoid crashing on standby driver */

    return mMethodsGet(mThis(self))->TdmLookupOffset(mThis(self), slice, tdmPwId);
    }

static eAtRet PwTohModeSet(ThaModulePda self, ThaPwAdapter adapter)
    {
    return ThaModulePdaPwPdaModeSet(self, (AtPw)adapter, 0);
    }

static eAtRet SAToPModeSet(ThaModulePda self, ThaPwAdapter adapter, AtChannel circuit)
    {
    AtUnused(circuit);
    return ThaModulePdaPwPdaModeSet(self, (AtPw)adapter, 0);
    }

static eAtRet CESoPModeSet(ThaModulePda self, ThaPwAdapter adapter)
    {
    return ThaModulePdaPwPdaModeSet(self, (AtPw)adapter, 1);
    }

static eAtRet CepModeSet(ThaModulePda self, ThaPwAdapter adapter, AtSdhChannel sdhVc)
    {
    AtUnused(sdhVc);
    return ThaModulePdaPwPdaModeSet(self, (AtPw)adapter, 2);
    }

static uint32 LoTdmSmallDS0Control(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cAf6Reg_ramlotdmsmallds0control_Base;
    }

static uint32 SdhVc_NcSignalType(AtSdhChannel vc)
    {
    AtSdhChannel au = AtSdhChannelParentChannelGet(vc);
    uint8 numSlaves = AtSdhChannelNumSlaveChannels(au);

    switch (numSlaves)
        {
        case 3  /* VC4-4c  */: return cVcSignalTypeVc4_4c;
        case 7  /* VC4-8c  */: return cVcSignalTypeVc4_16c; /* This uses the same HW value as 16c */
        case 15 /* VC4-16c */: return cVcSignalTypeVc4_16c;
        default:
            return cInvalidUint32;
        }
    }

static uint32 SdhVcSignalType(AtSdhChannel vc)
    {
    uint32 vcType = AtSdhChannelTypeGet(vc);

    switch (vcType)
        {
        case cAtSdhChannelTypeVc3    : return cVcSignalTypeVc3;
        case cAtSdhChannelTypeVc4    : return cVcSignalTypeVc4;
        case cAtSdhChannelTypeVc4_4c : return cVcSignalTypeVc4_4c;
        case cAtSdhChannelTypeVc4_16c: return cVcSignalTypeVc4_16c;
        case cAtSdhChannelTypeVc4_64c: return cVcSignalTypeVc4_16c;
        case cAtSdhChannelTypeVc11   : return cVcSignalTypeVc11;
        case cAtSdhChannelTypeVc12   : return cVcSignalTypeVc12;
        case cAtSdhChannelTypeVc4_nc : return SdhVc_NcSignalType(vc);

        default:
            return cInvalidUint32;
        }
    }

static uint32 PdhChannelSignalType(AtPdhChannel pdhChannel)
    {
    uint32 deType = AtPdhChannelTypeGet(pdhChannel);

    switch (deType)
        {
        case cAtPdhChannelTypeE3   : return 0;
        case cAtPdhChannelTypeDs3  : return 0;
        case cAtPdhChannelTypeE1   : return 4;
        case cAtPdhChannelTypeDs1  : return 4;
        case cAtPdhChannelTypeNxDs0: return 4;
        default:
            return cInvalidUint32;
        }
    }

static uint32 SignalType(AtPw pw, AtChannel circuit)
    {
    eAtPwType pwType = AtPwTypeGet(pw);

    if (pwType == cAtPwTypeCEP)
        return SdhVcSignalType((AtSdhChannel)circuit);

    if ((pwType == cAtPwTypeSAToP) || (pwType == cAtPwTypeCESoP))
        return PdhChannelSignalType((AtPdhChannel)circuit);

    /* Invalid mode */
    return cInvalidUint32;
    }

static uint8 CircuitHwSts(AtPw pw, AtChannel circuit)
    {
    eAtPwType pwType = AtPwTypeGet(pw);
    uint8 hwSlice, hwSts;
    eAtRet ret = cAtError;

    if (pwType == cAtPwTypeCEP)
        {
        AtSdhChannel sdhChannel = (AtSdhChannel)circuit;
        ret = ThaSdhChannelHwStsGet(sdhChannel, cThaModulePda, AtSdhChannelSts1Get(sdhChannel), &hwSlice, &hwSts);
        }

    if ((pwType == cAtPwTypeSAToP) || (pwType == cAtPwTypeCESoP))
        ret = ThaPdhChannelHwIdGet((AtPdhChannel)circuit, cThaModulePda, &hwSlice, &hwSts);

    if (ret == cAtOk)
        return hwSts;

    return cInvalidUint8;
    }

static eAtRet PwCircuitTypeSet(ThaModulePda self, AtPw pw, AtChannel circuit)
    {
    uint32 regAddr, regVal;
    uint32 signalType = SignalType(pw, circuit);

    if (signalType == cInvalidUint32)
        {
        AtChannelLog((AtChannel)pw, cAtLogLevelWarning, AtSourceLocation,
                     "Unknown signal type for circuit %s.%s\r\n",
                     AtChannelTypeString(circuit), AtChannelIdString(circuit));
        return m_ThaModulePdaMethods->PwCircuitTypeSet(self, pw, circuit);
        }

    regAddr = PWPdaTdmModeCtrlAbsoluteAddress(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    mRegFieldSet(regVal, cAf6_ramtdmmodecfg_PDASigType_, signalType);
    mRegFieldSet(regVal, cAf6_ramtdmmodecfg_PDAStsId_, CircuitHwSts(pw, circuit));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static uint32 PwTdmLowRateOffset(Tha60210011ModulePda self, AtPw pw)
    {
    uint8 slice;

    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, NULL) != cAtOk)
        return cInvalidUint32;

    return (slice * 8192UL) + TdmOc48Pwid(pw);
    }

static uint32 PWPdaTdmModeCtrl(ThaModulePda self, AtPw pw)
    {
    if (IsTdmOc192(pw))
        return mMethodsGet(mThis(self))->Oc192cTdmModeCtrl(mThis(self))  + mBaseAddress(self);

    return cAf6Reg_ramtdmmodecfg_Base + mBaseAddress(self);
    }

static uint32 PktReplaceModeMask(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAf6_ramtdmmodecfg_PDARepMode_Mask;
    }

static uint8 PktReplaceModeShift(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAf6_ramtdmmodecfg_PDARepMode_Shift;
    }

static uint32 PdaIdleCodeMask(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAf6_ramtdmmodecfg_PDAIdleCode_Mask;
    }

static uint8 PdaIdleCodeShift(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAf6_ramtdmmodecfg_PDAIdleCode_Shift;
    }

static uint32 AisRdiUneqOffMask(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAf6_ramtdmmodecfg_PDAAisRdiOff_BundEn_Mask;
    }

static uint8 AisRdiUneqOffShift(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAf6_ramtdmmodecfg_PDAAisRdiOff_BundEn_Shift;
    }

static eBool NumJitterBufferBlocksIncreased(ThaModulePda self)
    {
    return Tha60290022DevicePdaNumJitterBufferBlocksIncreased(AtModuleDeviceGet((AtModule)self));
    }

static uint32 MaxNumJitterBufferBlocks(ThaModulePda self)
    {
    uint32 numBlocks = m_ThaModulePdaMethods->MaxNumJitterBufferBlocks(self);

    if (NumJitterBufferBlocksIncreased(self))
        return numBlocks * 2;

    return numBlocks;
    }

static eBool ShouldCheckHwResource(void)
    {
    /* Not now, hardware need to give correct registers */
    return cAtFalse;
    }

static eAtRet HwResourceCheck(AtModule self)
    {
    if (ShouldCheckHwResource())
        return m_AtModuleMethods->HwResourceCheck(self);
    return cAtOk;
    }

static uint32 HotConfigureEngineRequestInfoPwIdMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAf6_ramjitbufcentercfg_CenterPwid_Mask;
    }

static eBool HwPwIdCacheIsSupported(Tha60210051ModulePda self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 LoTdmLookupCtrl(Tha60210011ModulePda self, AtPw pw)
    {
    if (IsTdmOc192(pw))
        {
        return mMethodsGet(mThis(self))->Oc192cTdmLookupCtrl(mThis(self));
        }

    return m_Tha60210011ModulePdaMethods->LoTdmLookupCtrl(self, pw);
    }

static eBool PwCwAutoRxMBitIsConfigurable(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    return Tha60290022DeviceCesopAutoRxMBitConfigurableIsSupported(device);
    }

static eBool CanControlLopsPktReplaceMode(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    return Tha60290022DeviceSeparateLbitAndLopsPktReplacementSupported(device);
    }

static uint32 PktReplaceModeSwToHw(eAtPwPktReplaceMode pktReplaceMode)
    {
    if (pktReplaceMode == cAtPwPktReplaceModeIdleCode)
        return 1;
    return 0;
    }

static eAtPwPktReplaceMode PktReplaceModeHwToSw(uint32 hwVal)
    {
    if (hwVal == 1)
        return cAtPwPktReplaceModeIdleCode;
    return cAtPwPktReplaceModeAis;
    }

static eAtModulePwRet LbitPktReplaceModeSet(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    uint32 address, regVal;

    address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);

    mRegFieldSet(regVal, cAf6_ramtdmmodecfg_PDALbitRepMode_, PktReplaceModeSwToHw(pktReplaceMode));
    mChannelHwWrite(pw, address, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtPwPktReplaceMode LbitPktReplaceModeGet(ThaModulePda self, AtPw pw)
    {
    uint32 address, regVal;

    address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);

    return PktReplaceModeHwToSw(mRegField(regVal, cAf6_ramtdmmodecfg_PDALbitRepMode_));
    }

static eAtRet LopPktReplaceModeSet(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    return ThaModulePdaDefaultPwCwPktReplaceModeV1Set(self, pw, pktReplaceMode);
    }

static eAtPwPktReplaceMode LopPktReplaceModeGet(ThaModulePda self, AtPw pw)
    {
    return ThaModulePdaDefaultPwCwPktReplaceModeV1Get(self, pw);
    }

static eBool CEPVc4_64cPayloadSizeIsValid(ThaModulePda self, AtPw pw, uint16 payloadSize)
    {
    AtUnused(self);
    AtUnused(pw);

    if ((1000U <= payloadSize) && (payloadSize < 1500U))
        return cAtFalse;

    return cAtTrue;
    }

static eBool CEPPayloadSizeIsValid(ThaModulePda self, AtPw pw, uint16 payloadSize)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPwBoundCircuitGet(pw);

    if (AtSdhChannelTypeGet(vc) == cAtSdhChannelTypeVc4_64c)
        return CEPVc4_64cPayloadSizeIsValid(self, pw, payloadSize);

    return cAtTrue;
    }

static uint32 Oc192cTdmLookupCtrl(Tha60290022ModulePda self)
    {
    if (IsNewOc192cOffset((ThaModulePda)self))
        return cAf6Reg_ramtdm192lkupcfg_new_Base;

    return cAf6Reg_ramtdm192lkupcfg_Base;
    }

static uint32 Oc192cTdmModeCtrl(Tha60290022ModulePda self)
    {
    ThaModulePda modulePda = (ThaModulePda)self;
    if (IsNewOc192cOffset(modulePda))
        return cAf6Reg_ramtdmmode192cfg_new_Base;

    return cAf6Reg_ramtdmmode192cfg_Base;
    }

static uint32 TdmLookupOffset(Tha60290022ModulePda self, uint8 slice, uint32 tdmPwId)
    {
    AtUnused(self);
    return slice + (tdmPwId * 8);
    }

static uint32 NumHwPwInOc48Slice(Tha60290022ModulePda self)
    {
    AtUnused(self);
    return 1344U;
    }

static void MethodsInit(AtModule self)
    {
    Tha60290022ModulePda module = (Tha60290022ModulePda)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Oc192cTdmLookupCtrl);
        mMethodOverride(m_methods, Oc192cTdmModeCtrl);
        mMethodOverride(m_methods, TdmLookupOffset);
        mMethodOverride(m_methods, NumHwPwInOc48Slice);
        }

    mMethodsSet(module, &m_methods);
    }

static void OverrideAtModule(AtModule self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, HwResourceCheck);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideThaModulePdaV2(AtModule self)
    {
    ThaModulePdaV2 pdaModule = (ThaModulePdaV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaV2Override, mMethodsGet(pdaModule), sizeof(m_ThaModulePdaV2Override));

        mMethodOverride(m_ThaModulePdaV2Override, PdaIdleCodeMask);
        mMethodOverride(m_ThaModulePdaV2Override, PdaIdleCodeShift);
        mMethodOverride(m_ThaModulePdaV2Override, AisRdiUneqOffMask);
        mMethodOverride(m_ThaModulePdaV2Override, AisRdiUneqOffShift);
        mMethodOverride(m_ThaModulePdaV2Override, HotConfigureEngineRequestInfoPwIdMask);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaV2Override);
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, mMethodsGet(pdaModule), sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, PrbsStatusBase);
        mMethodOverride(m_ThaModulePdaOverride, PwTdmOffset);
        mMethodOverride(m_ThaModulePdaOverride, PwPdaModeSet);
        mMethodOverride(m_ThaModulePdaOverride, PwTohModeSet);
        mMethodOverride(m_ThaModulePdaOverride, SAToPModeSet);
        mMethodOverride(m_ThaModulePdaOverride, CESoPModeSet);
        mMethodOverride(m_ThaModulePdaOverride, CepModeSet);
        mMethodOverride(m_ThaModulePdaOverride, PwCircuitTypeSet);
        mMethodOverride(m_ThaModulePdaOverride, PWPdaTdmModeCtrl);
        mMethodOverride(m_ThaModulePdaOverride, PktReplaceModeMask);
        mMethodOverride(m_ThaModulePdaOverride, PktReplaceModeShift);
        mMethodOverride(m_ThaModulePdaOverride, PktReplaceModeShift);
        mMethodOverride(m_ThaModulePdaOverride, MaxNumJitterBufferBlocks);
        mMethodOverride(m_ThaModulePdaOverride, PwPdaTdmLookupEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitIsConfigurable);
        mMethodOverride(m_ThaModulePdaOverride, CanControlLopsPktReplaceMode);
        mMethodOverride(m_ThaModulePdaOverride, LbitPktReplaceModeSet);
        mMethodOverride(m_ThaModulePdaOverride, LbitPktReplaceModeGet);
        mMethodOverride(m_ThaModulePdaOverride, LopPktReplaceModeSet);
        mMethodOverride(m_ThaModulePdaOverride, LopPktReplaceModeGet);
        mMethodOverride(m_ThaModulePdaOverride, CEPPayloadSizeIsValid);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void OverrideTha60210011ModulePda(AtModule self)
    {
    Tha60210011ModulePda pdaModule = (Tha60210011ModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePdaOverride, m_Tha60210011ModulePdaMethods, sizeof(m_Tha60210011ModulePdaOverride));

        mMethodOverride(m_Tha60210011ModulePdaOverride, Oc48IdFromSlice);
        mMethodOverride(m_Tha60210011ModulePdaOverride, HoLoOc48IdMask);
        mMethodOverride(m_Tha60210011ModulePdaOverride, HoLoOc48IdShift);
        mMethodOverride(m_Tha60210011ModulePdaOverride, TdmLineIdMask);
        mMethodOverride(m_Tha60210011ModulePdaOverride, TdmLineIdShift);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLoSlice24SelMask);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLoSlice24SelShift);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwCEPModeMask);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwCEPModeShift);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLowDs0ModeMask);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLowDs0ModeShift);
        mMethodOverride(m_Tha60210011ModulePdaOverride, LookupReset);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLoTdmLookupOffset);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwHoTdmLookupOffset);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLoTdmLookupReset);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwHoTdmLookupReset);
        mMethodOverride(m_Tha60210011ModulePdaOverride, LoTdmSmallDS0Control);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwHoTdmLookupSet);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLoTdmLookupSet);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwTdmLowRateOffset);
        mMethodOverride(m_Tha60210011ModulePdaOverride, LoTdmLookupCtrl);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLookupPwIDMask);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLookupPwIDShift);
        }

    mMethodsSet(pdaModule, &m_Tha60210011ModulePdaOverride);
    }

static void OverrideTha60210051ModulePda(AtModule self)
    {
    Tha60210051ModulePda pdaModule = (Tha60210051ModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051ModulePdaOverride, mMethodsGet(pdaModule), sizeof(m_Tha60210051ModulePdaOverride));

        mMethodOverride(m_Tha60210051ModulePdaOverride, HwPwIdCacheIsSupported);
        }

    mMethodsSet(pdaModule, &m_Tha60210051ModulePdaOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePda(self);
    OverrideThaModulePdaV2(self);
    OverrideTha60210011ModulePda(self);
    OverrideTha60210051ModulePda(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModulePda);
    }

AtModule Tha60290022ModulePdaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290022ModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModulePdaObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60290022ModulePdaInternal.h
 * 
 * Created Date: Jul 24, 2018
 *
 * Description : 60290022 PDA internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPDAINTERNAL_H_
#define _THA60290022MODULEPDAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/pda/Tha60290021ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModulePda *Tha60290022ModulePda;

typedef struct tTha60290022ModulePdaMethods
    {
    uint32 (*Oc192cTdmLookupCtrl)(Tha60290022ModulePda self);
    uint32 (*Oc192cTdmModeCtrl)(Tha60290022ModulePda self);
    uint32 (*TdmLookupOffset)(Tha60290022ModulePda self, uint8 oc48Slice, uint32 tdmPwId);
    uint32 (*NumHwPwInOc48Slice)(Tha60290022ModulePda self);
    }tTha60290022ModulePdaMethods;

typedef struct tTha60290022ModulePda
    {
    tTha60290021ModulePda super;
    const tTha60290022ModulePdaMethods *methods;
    }tTha60290022ModulePda;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290022ModulePdaObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPDAINTERNAL_H_ */


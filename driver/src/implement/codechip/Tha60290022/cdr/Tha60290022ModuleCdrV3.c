/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60290022ModuleCdrV3.c
 *
 * Created Date: Feb 5, 2018
 *
 * Description : New CDR module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../ram/Tha60290022InternalRam.h"
#include "Tha60290022ModuleCdrInternal.h"
#include "Tha60290022ModuleCdrV3.h"
#include "Tha60290022CdrV3AcrReg.h"
#include "Tha60290022CdrV3TxShapperReg.h"
#include "Tha60290022ModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290022ModuleCdrV3 *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleCdrMethods         m_ThaModuleCdrOverride;
static tThaModuleCdrStmMethods      m_ThaModuleCdrStmOverride;
static tTha60210011ModuleCdrMethods m_Tha60210011ModuleCdrOverride;

/* Save super implementation */
static const tAtObjectMethods      *m_AtObjectMethods = NULL;
static const tAtModuleMethods      *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 EngineTimingCtrlRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAf6Reg_cdr_acr_eng_timing_ctrl_Base;
    }

static uint8 CdrControllerSliceId(ThaCdrController controller)
    {
    AtSdhChannel channel = Tha60210011CdrControllerSdhChannelGet(controller);
    uint8 slice, sts;
    ThaSdhChannel2HwMasterStsId(channel, cThaModuleCdr, &slice, &sts);
    return slice;
    }

static uint32 DcrTxEngineTimingControlRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAf6Reg_dcr_tx_eng_timing_ctrl_Base;
    }

static uint32 TxEngineTimingControlOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    uint8 slice = CdrControllerSliceId(controller);
    AtUnused(self);
    return cTxShapperBaseAddress + (slice * 4096UL) + ThaCdrControllerIdGet(controller);
    }

static uint32 AdjustStateStatusRegister(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    AtUnused(controller);
    return cAf6Reg_cdr_adj_state_stat_Base;
    }

static uint32 LoDcrRtpFreqCfgAddress(Tha60210011ModuleCdr self, uint8 sliceId)
    {
    AtUnused(self);
    AtUnused(sliceId);
    return cAf6Reg_dcr_rtp_freq_cfg_Base + cCdrV3Base;
    }

static uint32 LoDcrPrcSrcSelCfgAddress(Tha60210011ModuleCdr self, uint8 sliceId)
    {
    AtUnused(self);
    AtUnused(sliceId);
    return cAf6Reg_dcr_prc_src_sel_cfg_Base + cCdrV3Base;
    }

static uint32 LoDcrPrcFreqCfgAddress(Tha60210011ModuleCdr self, uint8 sliceId)
    {
    AtUnused(self);
    AtUnused(sliceId);
    return cAf6Reg_dcr_prc_freq_cfg_Base + cCdrV3Base;
    }

static uint32 CDREngineUnlockedIntrMask(ThaModuleCdr self, ThaCdrController controller)
    {
    uint8 slice = CdrControllerSliceId(controller);
    AtUnused(self);
    return cBit0 << slice;
    }

static uint32 EngineTimingOffsetByHwSts(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, uint8 vtgId, uint8 vtId)
    {
    uint32 cdrId = ThaModuleCdrStmSdhVcCdrIdCalculate(self, channel, slice, hwStsId, vtgId, vtId);
    uint32 sliceOffset = slice * 32768UL;
    AtUnused(self);
    return sliceOffset + cdrId + cCdrV3Base;
    }

static uint32 TimingCtrlDefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    AtUnused(controller);
    return cInvalidUint32;
    }

static uint32 TimingCtrlDe1Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)ThaCdrControllerChannelGet(controller);
    uint32 slice, sts, vtg, vt;
    AtUnused(self);
    ThaPdhDe1HwIdFactorGet(de1, cThaModuleCdr, &slice, &sts, &vtg, &vt);
    return EngineTimingOffsetByHwSts(self, NULL, (uint8)slice, (uint8)sts, (uint8)vtg, (uint8)vt);
    }

static uint32 TimingCtrlDe2De1Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    return TimingCtrlDe1Offset(self, controller);
    }

static uint32 TimingCtrlVcDe1Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    return TimingCtrlDe1Offset(self, controller);
    }

static uint32 TimingCtrlDe3Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)ThaCdrControllerChannelGet(controller);
    uint32 slice, sts;
    AtUnused(self);
    ThaPdhDe3HwIdFactorGet(de3, cThaModuleCdr, &slice, &sts);
    return EngineTimingOffsetByHwSts(self, NULL, (uint8)slice, (uint8)sts, 0, 0);
    }

static uint32 TimingCtrlVcDe3Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    return TimingCtrlDe3Offset(self, controller);
    }

static uint32 TimingCtrlVcOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet(controller);
    uint8 hwSlice, hwSts, vtg, vt;
    ThaSdhChannelHwStsGet(sdhChannel, cThaModuleCdr,
                          AtSdhChannelSts1Get(sdhChannel),
                          &hwSlice, &hwSts);
    vtg = AtSdhChannelTug2Get(sdhChannel);
    vt = AtSdhChannelTu1xGet(sdhChannel);
    return EngineTimingOffsetByHwSts(self, sdhChannel, hwSlice, hwSts, vtg, vt);
    }

static eAtRet SetupLongRegisterAccesses(AtModule self)
    {
    static uint32 holdRegs[] = {cCdrV3Base + cAf6Reg_cdr_acr_cpu_hold_ctrl_Base + 0,
                                cCdrV3Base + cAf6Reg_cdr_acr_cpu_hold_ctrl_Base + 1,
                                cCdrV3Base + cAf6Reg_cdr_acr_cpu_hold_ctrl_Base + 2};

    if (mThis(self)->v3LongAccess == NULL)
        mThis(self)->v3LongAccess = AtDefaultLongRegisterAccessNew(holdRegs, mCount(holdRegs), holdRegs, mCount(holdRegs));

    return cAtOk;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret;

    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    return SetupLongRegisterAccesses(self);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->v3LongAccess);
    mThis(self)->v3LongAccess = NULL;

    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290022ModuleCdrV3 *object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(v3LongAccess);
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    if (mInRange(localAddress, cCdrV3Base, cCdrV3Base + 0x7FFFF))
        return mThis(self)->v3LongAccess;
    return m_AtModuleMethods->LongRegisterAccess(self, localAddress);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    if (mInRange(localAddress, cCdrV3Base, cCdrV3Base + 0x7FFFF) ||
        mInRange(localAddress, cTxShapperBaseAddress, cTxShapperBaseAddress + 0x08FFFF))
        return cAtTrue;

    return m_AtModuleMethods->HasRegister(self, localAddress);
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    if (managerIndex == cTha60210011ModuleCdrLoInterruptManagerIndex)
        return Tha60290022CdrInterruptManagerV3New(self);

    return m_AtModuleMethods->InterruptManagerCreate(self, managerIndex);
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    return Tha60290022InternalRamCdrV3New(self, ramId, localRamId);
    }

static uint32 AdjustHoldoverValueStatusRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAf6Reg_cdr_adj_holdover_value_stat_Base;
    }

static uint32 EngineIdOfHoVc(ThaModuleCdr self, AtSdhVc vc)
    {
    return ThaModuleCdrStmEngineIdOfHoVc(self, vc);
    }

static ThaCdrController HoVcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60290022HoVcCdrControllerNew(EngineIdOfHoVc(self, vc), (AtChannel)vc);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideThaModuleCdrStm(AtModule self)
    {
    ThaModuleCdrStm module = (ThaModuleCdrStm)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrStmOverride, mMethodsGet(module), sizeof(m_ThaModuleCdrStmOverride));

        mMethodOverride(m_ThaModuleCdrStmOverride, EngineTimingOffsetByHwSts);
        }

    mMethodsSet(module, &m_ThaModuleCdrStmOverride);
    }

static void OverrideTha60210011ModuleCdr(AtModule self)
    {
    Tha60210011ModuleCdr module = (Tha60210011ModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleCdrOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleCdrOverride));

        mMethodOverride(m_Tha60210011ModuleCdrOverride, LoDcrPrcSrcSelCfgAddress);
        mMethodOverride(m_Tha60210011ModuleCdrOverride, LoDcrPrcFreqCfgAddress);
        mMethodOverride(m_Tha60210011ModuleCdrOverride, LoDcrRtpFreqCfgAddress);
        }

    mMethodsSet(module, &m_Tha60210011ModuleCdrOverride);
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr module = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, mMethodsGet(module), sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, EngineTimingCtrlRegister);
        mMethodOverride(m_ThaModuleCdrOverride, TxEngineTimingControlOffset);
        mMethodOverride(m_ThaModuleCdrOverride, AdjustStateStatusRegister);
        mMethodOverride(m_ThaModuleCdrOverride, CDREngineUnlockedIntrMask);
        mMethodOverride(m_ThaModuleCdrOverride, TimingCtrlDefaultOffset);
        mMethodOverride(m_ThaModuleCdrOverride, TimingCtrlDe2De1Offset);
        mMethodOverride(m_ThaModuleCdrOverride, TimingCtrlVcDe1Offset);
        mMethodOverride(m_ThaModuleCdrOverride, TimingCtrlVcDe3Offset);
        mMethodOverride(m_ThaModuleCdrOverride, TimingCtrlDe3Offset);
        mMethodOverride(m_ThaModuleCdrOverride, TimingCtrlVcOffset);
        mMethodOverride(m_ThaModuleCdrOverride, DcrTxEngineTimingControlRegister);
        mMethodOverride(m_ThaModuleCdrOverride, AdjustHoldoverValueStatusRegister);
        mMethodOverride(m_ThaModuleCdrOverride, HoVcCdrControllerCreate);
        }

    mMethodsSet(module, &m_ThaModuleCdrOverride);
    }

static void Override(AtModule self)
	{
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideThaModuleCdr(self);
    OverrideThaModuleCdrStm(self);
    OverrideTha60210011ModuleCdr(self);
	}

static uint32 ObjectSize(void)
	{
	return sizeof(tTha60290022ModuleCdrV3);
	}

AtModule Tha60290022ModuleCdrV3ObjectInit(AtModule self, AtDevice device)
	{
	/* Clear memory */
	AtOsal osal = AtSharedDriverOsalGet();
	mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

	/* Super constructor */
	if (Tha60290022ModuleCdrObjectInit(self, device) == NULL)
		return NULL;

	/* Setup class */
	Override(self);
	m_methodsInit = 1;

	return self;
	}

AtModule Tha60290022ModuleCdrV3New(AtDevice device)
	{
	/* Allocate memory */
	AtOsal osal = AtSharedDriverOsalGet();
	AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
	if (newModule == NULL)
		return NULL;

	/* Construct it */
	return Tha60290022ModuleCdrV3ObjectInit(newModule, device);
	}

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_CDR_v3_TimGen_H_
#define _AF6_REG_AF6CNC0022_RD_CDR_v3_TimGen_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : CDR CPU  Reg Hold Control
Reg Addr   : 0x030000-0x030002
Reg Formula: 0x030000 + HoldId
    Where  : 
           + $HoldId(0-2): Hold register
Reg Desc   : 
The register provides hold register for three word 32-bits MSB when CPU access to engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_cpu_hold_ctrl_Base                                                                0x030000

/*--------------------------------------
BitField Name: HoldReg0
BitField Type: RW
BitField Desc: Hold 32 bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_cpu_hold_ctrl_HoldReg0_Mask                                                          cBit31_0
#define cAf6_cdr_cpu_hold_ctrl_HoldReg0_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : CDR line mode control
Reg Addr   : 0x00000200 - 0x0000022F
Reg Formula: 0x00000200 +  STS
    Where  : 
           + $STS(0-47): HDL_PATH     : engidcnt.vtcnt.stsctrlbuf.array.ram[$STS]
Reg Desc   : 
The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_line_mode_ctrl_Base                                                             0x00000200
#define cAf6Reg_cdr_line_mode_ctrl_WidthVal                                                                 32

/*--------------------------------------
BitField Name: VC3Mode
BitField Type: RW
BitField Desc: VC3 or TU3 mode, each bit is used configured for each STS (only
valid in TU3 CEP) 1: TU3 CEP 0: Other mode
BitField Bits: [9]
--------------------------------------*/
#define cAf6_cdr_line_mode_ctrl_VC3Mode_Mask                                                             cBit9
#define cAf6_cdr_line_mode_ctrl_VC3Mode_Shift                                                                9

/*--------------------------------------
BitField Name: DE3Mode
BitField Type: RW
BitField Desc: DS3 or E3 mode, each bit is used configured for each STS. 1: DS3
mode 0: E3 mod
BitField Bits: [8]
--------------------------------------*/
#define cAf6_cdr_line_mode_ctrl_DE3Mode_Mask                                                             cBit8
#define cAf6_cdr_line_mode_ctrl_DE3Mode_Shift                                                                8

/*--------------------------------------
BitField Name: STS1STSMode
BitField Type: RW
BitField Desc: STS mode of STS1 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [7]
--------------------------------------*/
#define cAf6_cdr_line_mode_ctrl_STS1STSMode_Mask                                                         cBit7
#define cAf6_cdr_line_mode_ctrl_STS1STSMode_Shift                                                            7

/*--------------------------------------
BitField Name: STS1VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS1, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_cdr_line_mode_ctrl_STS1VTType_Mask                                                        cBit6_0
#define cAf6_cdr_line_mode_ctrl_STS1VTType_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : CDR Timing External reference control
Reg Addr   : 0x0000003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure for the 2Khz coefficient of two external reference signal in order to generate timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_timing_extref_ctrl_Base                                                          0x0000003
#define cAf6Reg_cdr_timing_extref_ctrl_WidthVal                                                             64

/*--------------------------------------
BitField Name: Ext3N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the third external reference signal
BitField Bits: [47:32]
--------------------------------------*/
#define cAf6_cdr_timing_extref_ctrl_Ext3N2k_Mask                                                      cBit15_0
#define cAf6_cdr_timing_extref_ctrl_Ext3N2k_Shift                                                            0

/*--------------------------------------
BitField Name: Ext2N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the second external reference signal
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_cdr_timing_extref_ctrl_Ext2N2k_Mask                                                     cBit31_16
#define cAf6_cdr_timing_extref_ctrl_Ext2N2k_Shift                                                           16

/*--------------------------------------
BitField Name: Ext1N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the first external reference signal
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_cdr_timing_extref_ctrl_Ext1N2k_Mask                                                      cBit15_0
#define cAf6_cdr_timing_extref_ctrl_Ext1N2k_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : CDR STS Timing control
Reg Addr   : 0x0001000-0x000102F
Reg Formula: 0x0001000+STS
    Where  : 
           + $STS(0-47): HDL_PATH     : engidcnt.timgen.ram1.array.ram[$STS]
Reg Desc   : 
This register is used to configure timing mode for per STS

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_sts_timing_ctrl_Base                                                             0x0001000

/*--------------------------------------
BitField Name: CDRChid
BitField Type: RW
BitField Desc: CDR channel ID in CDR mode.
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_CDRChid_Mask                                                        cBit29_20
#define cAf6_cdr_sts_timing_ctrl_CDRChid_Shift                                                              20

/*--------------------------------------
BitField Name: VC3EParEn
BitField Type: RW
BitField Desc: VC3 EPAR mode 0 0: Disable 1: Enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_VC3EParEn_Mask                                                         cBit17
#define cAf6_cdr_sts_timing_ctrl_VC3EParEn_Shift                                                            17

/*--------------------------------------
BitField Name: MapSTSMode
BitField Type: RW
BitField Desc: Map STS mode 0: Payload STS mode 1: Payload DE3 mode
BitField Bits: [16]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_MapSTSMode_Mask                                                        cBit16
#define cAf6_cdr_sts_timing_ctrl_MapSTSMode_Shift                                                           16

/*--------------------------------------
BitField Name: DE3TimeMode
BitField Type: RW
BitField Desc: DE3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR
timing mode 9: OCN System timing mode 10: DCR timing mode
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_DE3TimeMode_Mask                                                      cBit7_4
#define cAf6_cdr_sts_timing_ctrl_DE3TimeMode_Shift                                                           4

/*--------------------------------------
BitField Name: VC3TimeMode
BitField Type: RW
BitField Desc: VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR
timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP
mode
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_VC3TimeMode_Mask                                                      cBit3_0
#define cAf6_cdr_sts_timing_ctrl_VC3TimeMode_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : CDR VT Timing control
Reg Addr   : 0x0001800-0x0001FFF
Reg Formula: 0x0001800 + STS*32 + TUG*4 + VT
    Where  : 
           + $STS(0-47):
           + $TUG(0 -6):
           + $VT(0-3): (0-2)in E1 and (0-3) in DS1 HDL_PATH     : engidcnt.timgen.ram2.array.ram[$STS*32 + $TUG*4 + $VT]
Reg Desc   : 
This register is used to configure timing mode for per VT

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_vt_timing_ctrl_Base                                                              0x0001800
#define cAf6Reg_cdr_vt_timing_ctrl_WidthVal                                                                 32

/*--------------------------------------
BitField Name: CDRChid
BitField Type: RW
BitField Desc: CDR channel ID in CDR mode
BitField Bits: [21:12]
--------------------------------------*/
#define cAf6_cdr_vt_timing_ctrl_CDRChid_Mask                                                         cBit21_12
#define cAf6_cdr_vt_timing_ctrl_CDRChid_Shift                                                               12

/*--------------------------------------
BitField Name: VTEParEn
BitField Type: RW
BitField Desc: VT EPAR mode 0: Disable 1: Enable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_cdr_vt_timing_ctrl_VTEParEn_Mask                                                            cBit9
#define cAf6_cdr_vt_timing_ctrl_VTEParEn_Shift                                                               9

/*--------------------------------------
BitField Name: MapVTMode
BitField Type: RW
BitField Desc: Map VT mode 0: Payload VT15/VT2 mode 1: Payload DS1/E1 mode
BitField Bits: [8]
--------------------------------------*/
#define cAf6_cdr_vt_timing_ctrl_MapVTMode_Mask                                                           cBit8
#define cAf6_cdr_vt_timing_ctrl_MapVTMode_Shift                                                              8

/*--------------------------------------
BitField Name: DE1TimeMode
BitField Type: RW
BitField Desc: DE1 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR
timing mode 9: OCN System timing mode 10: DCR timing mode
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_cdr_vt_timing_ctrl_DE1TimeMode_Mask                                                       cBit7_4
#define cAf6_cdr_vt_timing_ctrl_DE1TimeMode_Shift                                                            4

/*--------------------------------------
BitField Name: VTTimeMode
BitField Type: RW
BitField Desc: VT time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR
timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP
mode
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_vt_timing_ctrl_VTTimeMode_Mask                                                        cBit3_0
#define cAf6_cdr_vt_timing_ctrl_VTTimeMode_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : CDR ToP Timing Frame sync 8K control
Reg Addr   : 0x0001040
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure timing source to generate the 125us signal frame sync for ToP

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_top_timing_frmsync_ctrl_Base                                                     0x0001040

/*--------------------------------------
BitField Name: ToPPDHLineType
BitField Type: RW
BitField Desc: Line type of DS1/E1 loop time or PDH CDR 0: E1 1: DS1 2: VT2 3:
VT15 4: E3 5: DS3 6: STS1 7: Reserve
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_cdr_top_timing_frmsync_ctrl_ToPPDHLineType_Mask                                         cBit18_16
#define cAf6_cdr_top_timing_frmsync_ctrl_ToPPDHLineType_Shift                                               16

/*--------------------------------------
BitField Name: ToPPDHLineID
BitField Type: RW
BitField Desc: Line ID of DS1/E1 loop time or PDH/CEP CDR
BitField Bits: [13:4]
--------------------------------------*/
#define cAf6_cdr_top_timing_frmsync_ctrl_ToPPDHLineID_Mask                                            cBit13_4
#define cAf6_cdr_top_timing_frmsync_ctrl_ToPPDHLineID_Shift                                                  4

/*--------------------------------------
BitField Name: ToPTimeMode
BitField Type: RW
BitField Desc: Time mode  for ToP 0: System timing mode 1: DS1/E1 Loop timing
mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing
mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing
mode 8: PDH CDR timing mode 9: OCN System timing mode
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_top_timing_frmsync_ctrl_ToPTimeMode_Mask                                              cBit3_0
#define cAf6_cdr_top_timing_frmsync_ctrl_ToPTimeMode_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CDR Reference Sync 8K Master Output control
Reg Addr   : 0x0001041
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure timing source to generate the reference sync master output signal

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_refsync_master_octrl_Base                                                        0x0001041

/*--------------------------------------
BitField Name: RefOut1PDHLineType
BitField Type: RW
BitField Desc: Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2
3: VT15 4: E3 5: DS3 6: STS1 7: Reserve
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_cdr_refsync_master_octrl_RefOut1PDHLineType_Mask                                        cBit18_16
#define cAf6_cdr_refsync_master_octrl_RefOut1PDHLineType_Shift                                              16

/*--------------------------------------
BitField Name: RefOut1PDHLineID
BitField Type: RW
BitField Desc: Line ID of DS1/E1 loop time or PDH CDR
BitField Bits: [13:4]
--------------------------------------*/
#define cAf6_cdr_refsync_master_octrl_RefOut1PDHLineID_Mask                                           cBit13_4
#define cAf6_cdr_refsync_master_octrl_RefOut1PDHLineID_Shift                                                 4

/*--------------------------------------
BitField Name: RefOut1TimeMode
BitField Type: RW
BitField Desc: Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing
mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing
mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing
mode 8: PDH CDR timing mode 9: External Clock sync
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_refsync_master_octrl_RefOut1TimeMode_Mask                                             cBit3_0
#define cAf6_cdr_refsync_master_octrl_RefOut1TimeMode_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : CDR Reference Sync 8k Slaver Output control
Reg Addr   : 0x0001042
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure timing source to generate the reference sync slaver output signal

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_refsync_slaver_octrl_Base                                                        0x0001042

/*--------------------------------------
BitField Name: RefOut2PDHLineType
BitField Type: RW
BitField Desc: Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2
3: VT15 4: E3 5: DS3 6: STS1 7: Reserve
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_cdr_refsync_slaver_octrl_RefOut2PDHLineType_Mask                                        cBit18_16
#define cAf6_cdr_refsync_slaver_octrl_RefOut2PDHLineType_Shift                                              16

/*--------------------------------------
BitField Name: RefOut2PDHLineID
BitField Type: RW
BitField Desc: Line ID of DS1/E1 loop time or PDH CDR
BitField Bits: [13:4]
--------------------------------------*/
#define cAf6_cdr_refsync_slaver_octrl_RefOut2PDHLineID_Mask                                           cBit13_4
#define cAf6_cdr_refsync_slaver_octrl_RefOut2PDHLineID_Shift                                                 4

/*--------------------------------------
BitField Name: RefOut2TimeMode
BitField Type: RW
BitField Desc: Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing
mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing
mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing
mode 8: PDH CDR timing mode 9: External Clock sync
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_refsync_slaver_octrl_RefOut2TimeMode_Mask                                             cBit3_0
#define cAf6_cdr_refsync_slaver_octrl_RefOut2TimeMode_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : RAM TimingGen Parity Force Control
Reg Addr   : 0x104c
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_TimingGen_Parity_Force_Control_Base                                                 0x104c

/*--------------------------------------
BitField Name: CDRVTTimingCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR VT Timing control"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_TimingGen_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Mask                                   cBit1
#define cAf6_RAM_TimingGen_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Shift                                       1

/*--------------------------------------
BitField Name: CDRSTSTimingCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR STS Timing control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_TimingGen_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Mask                                   cBit0
#define cAf6_RAM_TimingGen_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RAM TimingGen Parity Disable Control
Reg Addr   : 0x104d
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_TimingGen_Parity_Disbale_Control_Base                                               0x104d

/*--------------------------------------
BitField Name: CDRVTTimingCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR VT Timing control"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_TimingGen_Parity_Disbale_Control_CDRVTTimingCtrl_ParErrDis_Mask                                   cBit1
#define cAf6_RAM_TimingGen_Parity_Disbale_Control_CDRVTTimingCtrl_ParErrDis_Shift                                       1

/*--------------------------------------
BitField Name: CDRSTSTimingCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR STS Timing control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_TimingGen_Parity_Disbale_Control_CDRSTSTimingCtrl_ParErrDis_Mask                                   cBit0
#define cAf6_RAM_TimingGen_Parity_Disbale_Control_CDRSTSTimingCtrl_ParErrDis_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RAM TimingGen parity Error Sticky
Reg Addr   : 0x104e
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_TimingGen_Parity_Error_Sticky_Base                                                  0x104e

/*--------------------------------------
BitField Name: CDRVTTimingCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "CDR VT Timing control"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_TimingGen_Parity_Error_Sticky_CDRVTTimingCtrl_ParErrStk_Mask                                   cBit1
#define cAf6_RAM_TimingGen_Parity_Error_Sticky_CDRVTTimingCtrl_ParErrStk_Shift                                       1

/*--------------------------------------
BitField Name: CDRSTSTimingCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "CDR STS Timing control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_TimingGen_Parity_Error_Sticky_CDRSTSTimingCtrl_ParErrStk_Mask                                   cBit0
#define cAf6_RAM_TimingGen_Parity_Error_Sticky_CDRSTSTimingCtrl_ParErrStk_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit3_0 Control
Reg Addr   : 0x30200
Reg Formula: 0x30200 + HaAddr3_0
    Where  : 
           + $HaAddr3_0(0-15): HA Address Bit3_0
Reg Desc   : 
This register is used to send HA read address bit3_0 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha3_0_control_Base                                                                   0x30200
#define cAf6Reg_rdha3_0_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr3_0
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr3_0
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha3_0_control_ReadAddr3_0_Mask                                                         cBit19_0
#define cAf6_rdha3_0_control_ReadAddr3_0_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit7_4 Control
Reg Addr   : 0x30210
Reg Formula: 0x30210 + HaAddr7_4
    Where  : 
           + $HaAddr7_4(0-15): HA Address Bit7_4
Reg Desc   : 
This register is used to send HA read address bit7_4 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha7_4_control_Base                                                                   0x30210
#define cAf6Reg_rdha7_4_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr7_4
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr7_4
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha7_4_control_ReadAddr7_4_Mask                                                         cBit19_0
#define cAf6_rdha7_4_control_ReadAddr7_4_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit11_8 Control
Reg Addr   : 0x30220
Reg Formula: 0x30220 + HaAddr11_8
    Where  : 
           + $HaAddr11_8(0-15): HA Address Bit11_8
Reg Desc   : 
This register is used to send HA read address bit11_8 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha11_8_control_Base                                                                  0x30220
#define cAf6Reg_rdha11_8_control_WidthVal                                                                   32

/*--------------------------------------
BitField Name: ReadAddr11_8
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr11_8
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha11_8_control_ReadAddr11_8_Mask                                                       cBit19_0
#define cAf6_rdha11_8_control_ReadAddr11_8_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit15_12 Control
Reg Addr   : 0x30230
Reg Formula: 0x30230 + HaAddr15_12
    Where  : 
           + $HaAddr15_12(0-15): HA Address Bit15_12
Reg Desc   : 
This register is used to send HA read address bit15_12 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha15_12_control_Base                                                                 0x30230
#define cAf6Reg_rdha15_12_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr15_12
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr15_12
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha15_12_control_ReadAddr15_12_Mask                                                     cBit19_0
#define cAf6_rdha15_12_control_ReadAddr15_12_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit19_16 Control
Reg Addr   : 0x30240
Reg Formula: 0x30240 + HaAddr19_16
    Where  : 
           + $HaAddr19_16(0-15): HA Address Bit19_16
Reg Desc   : 
This register is used to send HA read address bit19_16 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha19_16_control_Base                                                                 0x30240
#define cAf6Reg_rdha19_16_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr19_16
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr19_16
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha19_16_control_ReadAddr19_16_Mask                                                     cBit19_0
#define cAf6_rdha19_16_control_ReadAddr19_16_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit23_20 Control
Reg Addr   : 0x30250
Reg Formula: 0x30250 + HaAddr23_20
    Where  : 
           + $HaAddr23_20(0-15): HA Address Bit23_20
Reg Desc   : 
This register is used to send HA read address bit23_20 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha23_20_control_Base                                                                 0x30250
#define cAf6Reg_rdha23_20_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr23_20
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr23_20
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha23_20_control_ReadAddr23_20_Mask                                                     cBit19_0
#define cAf6_rdha23_20_control_ReadAddr23_20_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit24 and Data Control
Reg Addr   : 0x30260
Reg Formula: 0x30260 + HaAddr24
    Where  : 
           + $HaAddr24(0-1): HA Address Bit24
Reg Desc   : 
This register is used to send HA read address bit24 to HA engine to read data

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha24data_control_Base                                                                0x30260

/*--------------------------------------
BitField Name: ReadHaData31_0
BitField Type: RW
BitField Desc: HA read data bit31_0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha24data_control_ReadHaData31_0_Mask                                                   cBit31_0
#define cAf6_rdha24data_control_ReadHaData31_0_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data63_32
Reg Addr   : 0x30270
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha_hold63_32_Base                                                                    0x30270

/*--------------------------------------
BitField Name: ReadHaData63_32
BitField Type: RW
BitField Desc: HA read data bit63_32
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha_hold63_32_ReadHaData63_32_Mask                                                      cBit31_0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data95_64
Reg Addr   : 0x30271
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold95_64_Base                                                                  0x30271

/*--------------------------------------
BitField Name: ReadHaData95_64
BitField Type: RW
BitField Desc: HA read data bit95_64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Mask                                                    cBit31_0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data127_96
Reg Addr   : 0x30272
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold127_96_Base                                                                 0x30272

/*--------------------------------------
BitField Name: ReadHaData127_96
BitField Type: RW
BitField Desc: HA read data bit127_96
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Mask                                                  cBit31_0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Shift                                                        0

#endif /* _AF6_REG_AF6CNC0022_RD_CDR_v3_TimGen_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60290022HoVcCdrController.c
 *
 * Created Date: Apr 24, 2018
 *
 * Description : 60290022 CDR controller for AU-VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/cdr/controllers/ThaHoVcCdrControllerInternal.h"
#include "../../../Tha60290021/xc/Tha60290021ModuleXc.h"
#include "../../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022HoVcCdrController
    {
    tThaHoVcCdrController super;
    }tTha60290022HoVcCdrController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrControllerMethods           m_ThaCdrControllerOverride;
static tThaHoVcCdrControllerMethods       m_ThaHoVcCdrControllerOverride;

/* Save super implementation */
static const tThaCdrControllerMethods           *m_ThaCdrControllerMethods = NULL;
static const tThaHoVcCdrControllerMethods       *m_ThaHoVcCdrControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleXc XcModule(AtChannel channel)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    return (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static eTha60290021XcHideMode XcHideMode(AtChannel channel)
    {
    return Tha60290021ModuleXcHideModeGet(XcModule(channel));
    }

static AtSdhChannel LineSideAuVc(AtSdhChannel channel)
    {
    AtSdhChannel pairChannel;

    if (Tha60290021ModuleSdhIsLineSideVc(channel))
        return channel;

    /* Try to get source channel, or return itself. */
    pairChannel = (AtSdhChannel)AtChannelSourceGet((AtChannel)channel);
    if (pairChannel)
        return pairChannel;

    return channel;
    }

static AtChannel ReportChannelGet(ThaCdrController self)
    {
    AtChannel channel = m_ThaCdrControllerMethods->ChannelGet(self);

    if (XcHideMode(channel) == cTha60290021XcHideModeDirect)
        return (AtChannel)LineSideAuVc((AtSdhChannel)channel);

    return channel;
    }

static eBool NeedSts48cMastersForVc4_64cPseudowire(AtSdhChannel sdhChannel)
    {
    if (AtSdhChannelTypeGet(sdhChannel) != cAtSdhChannelTypeVc4_64c)
        return cAtFalse;

    return Tha60290022DeviceNeedMapConfigureWhenBindingVc4_64cToPseudowire(AtChannelDeviceGet((AtChannel)sdhChannel));
    }

static eBool TimingSourceStsIdIsMaster(ThaHoVcCdrController self, AtSdhChannel sdhChannel, uint8 sts1Id)
    {
    /* We need to enable master for 4 first STS1s of 4 OC48 Slices. */
    if (NeedSts48cMastersForVc4_64cPseudowire(sdhChannel))
        return ((sts1Id % 48) == 0) ? cAtTrue : cAtFalse;

    return m_ThaHoVcCdrControllerMethods->TimingSourceStsIdIsMaster(self, sdhChannel, sts1Id);
    }

static void OverrideThaHoVcCdrController(ThaCdrController self)
    {
    ThaHoVcCdrController object = (ThaHoVcCdrController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHoVcCdrControllerMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHoVcCdrControllerOverride, m_ThaHoVcCdrControllerMethods, sizeof(m_ThaHoVcCdrControllerOverride));

        mMethodOverride(m_ThaHoVcCdrControllerOverride, TimingSourceStsIdIsMaster);
        }

    mMethodsSet(object, &m_ThaHoVcCdrControllerOverride);
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, ReportChannelGet);
        }

    mMethodsSet(self, &m_ThaCdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
    OverrideThaHoVcCdrController(self);
    OverrideThaCdrController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022HoVcCdrController);
    }

static ThaCdrController ObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHoVcCdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController Tha60290022HoVcCdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, engineId, channel);
    }

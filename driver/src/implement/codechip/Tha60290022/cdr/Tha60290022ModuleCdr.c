/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60290022ModuleCdr.c
 *
 * Created Date: Apr 19, 2017
 *
 * Description : CDR module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/intrcontroller/ThaInterruptManager.h"
#include "Tha60290022ModuleCdrInternal.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../man/Tha60290022Device.h"
#include "../ram/Tha60290022InternalRam.h"
#include "Tha60290022ModuleCdrInternal.h"
#include "Tha60290022ModuleCdrReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mCdrLineModeCtrlAddress(self, slice, hwSts) CdrLineModeCtrlNewAddress((ThaModuleCdr)self, slice, hwSts)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleCdrMethods         m_ThaModuleCdrOverride;
static tThaModuleCdrStmMethods      m_ThaModuleCdrStmOverride;
static tTha60210011ModuleCdrMethods m_Tha60210011ModuleCdrOverride;
static tTha60150011ModuleCdrMethods m_Tha60150011ModuleCdrOverride;

/* Save super implementation */
static const tAtModuleMethods             *m_AtModuleMethods             = NULL;
static const tThaModuleCdrMethods         *m_ThaModuleCdrMethods         = NULL;
static const tThaModuleCdrStmMethods      *m_ThaModuleCdrStmMethods      = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaModuleCdr self)
    {
    AtUnused(self);
    return 0x0C00000;
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    if (managerIndex == cTha60210011ModuleCdrHoInterruptManagerIndex)
        return NULL;

    if (managerIndex == cTha60210011ModuleCdrLoInterruptManagerIndex)
        return Tha60290022CdrInterruptManagerNew(self);

    return m_AtModuleMethods->InterruptManagerCreate(self, managerIndex);
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    return 1;
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "LO CDR STS Timing control, slice 0",
         "LO CDR VT Timing control, slice 0",
         "LO CDR ACR Engine Timing control, slice 0",
         "LO CDR STS Timing control, slice 1",
         "LO CDR VT Timing control, slice 1",
         "LO CDR ACR Engine Timing control, slice 1",
         "LO CDR STS Timing control, slice 2",
         "LO CDR VT Timing control, slice 2",
         "LO CDR ACR Engine Timing control, slice 2",
         "LO CDR STS Timing control, slice 3",
         "LO CDR VT Timing control, slice 3",
         "LO CDR ACR Engine Timing control, slice 3",
         "LO CDR STS Timing control, slice 4",
         "LO CDR VT Timing control, slice 4",
         "LO CDR ACR Engine Timing control, slice 4",
         "LO CDR STS Timing control, slice 5",
         "LO CDR VT Timing control, slice 5",
         "LO CDR ACR Engine Timing control, slice 5",
         "LO CDR STS Timing control, slice 6",
         "LO CDR VT Timing control, slice 6",
         "LO CDR ACR Engine Timing control, slice 6",
         "LO CDR STS Timing control, slice 7",
         "LO CDR VT Timing control, slice 7",
         "LO CDR ACR Engine Timing control, slice 7",
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    return Tha60290022InternalRamCdrNew(self, ramId, localRamId);
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    AtUnused(self);
    AtUnused(ram);
    return cAtFalse;
    }

static uint32 StartLoRamLocalId(Tha60210011ModuleCdr self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 StsTimingControlReg(ThaModuleCdrStm self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_cdr_sts_timing_ctrl_Base;
    }

static uint32 StsCdrChannelIdMask(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cAf6_cdr_sts_timing_ctrl_CDRChid_Mask;
    }

static uint8  StsCdrChannelIdShift(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cAf6_cdr_sts_timing_ctrl_CDRChid_Shift;
    }

static uint32 VtTimingControlReg(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cAf6Reg_cdr_vt_timing_ctrl_Base;
    }

static uint32 VtCdrChannelIdMask(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cAf6_cdr_vt_timing_ctrl_CDRChid_Mask;
    }

static uint8  VtCdrChannelIdShift(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cAf6_cdr_vt_timing_ctrl_CDRChid_Shift;
    }

static AtModuleSdh SdhModule(Tha60210011ModuleCdr self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static uint8 MaxNumLoSlices(Tha60210011ModuleCdr self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (Tha60290022DeviceShouldOpenFullCapacity(device))
        return Tha60290022DeviceCapacity15G(device) ? 6 : 8;

    return Tha60290021ModuleSdhNumUseableTerminatedLines(SdhModule(self));
    }

static uint32 CDRLineModeControlRegister(ThaModuleCdr self)
    {
    /* Controlling this register is completely different, bit fields and offsets
     * are all changed. For error catching, this is overridden to invalid
     * address. Note, the cBit31_0 is not used here because of overflow will
     * happen when added with channel offset */
    AtUnused(self);
    return 0xF0000000;
    }

static uint32 CommonSliceOffset(ThaModuleCdr self, uint8 slice)
    {
    AtUnused(self);
    return (slice * 0x40000UL) + ThaModuleCdrBaseAddress(self);
    }

static uint32 CdrLineModeCtrlNewAddress(ThaModuleCdr self, uint8 slice, uint8 hwSts)
    {
    uint32 sliceOffset = CommonSliceOffset(self, slice);
    return cAf6Reg_cdr_line_mode_ctrl_Base + (uint32)hwSts + sliceOffset;
    }

static eAtRet De3UnChannelizedModeSet(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwSts, eBool isUnChannelized)
    {
    uint8 stsMd = isUnChannelized ? 1: 0;
    uint32 regAddr = mCdrLineModeCtrlAddress(self, slice, hwSts);
    uint32 regVal = mChannelHwRead(channel, regAddr, cThaModuleCdr);
    mRegFieldSet(regVal, cAf6_cdr_line_mode_ctrl_STS1STSMode_, stsMd);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleCdr);
    return cAtOk;
    }

static eAtRet LineModeHwSet(Tha60150011ModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, uint8 hwStsMode, uint8 hwPayloadType)
    {
    uint32 regAddr = mCdrLineModeCtrlAddress(self, slice, hwSts);
    uint32 regVal = mChannelHwRead(channel, regAddr, cThaModuleCdr);
    mRegFieldSet(regVal, cAf6_cdr_line_mode_ctrl_STS1STSMode_, hwStsMode);
    mRegFieldSet(regVal, cAf6_cdr_line_mode_ctrl_DE3Mode_, hwPayloadType);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleCdr);
    return cAtOk;
    }

static eAtRet HwVtgPldSet(Tha60150011ModuleCdr self, AtChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType, uint32 lineModeCtrlOffset)
    {
    uint32 regAddr = mCdrLineModeCtrlAddress(self, slice, hwSts);
    uint32 regVal = mChannelHwRead(channel, regAddr, cThaModuleCdr);
    AtUnused(lineModeCtrlOffset);
    mFieldIns(&regVal, cBit0 << vtgId, vtgId, Tha60150011ModuleCdrHwVtType(payloadType));
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleCdr);
    return cAtOk;
    }

static eAtRet Vc3CepHwModeEnable(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, eBool enable)
    {
    uint32 regAddr = mCdrLineModeCtrlAddress(self, slice, hwSts);
    uint32 regVal = mChannelHwRead(channel, regAddr, cThaModuleCdr);
    mRegFieldSet(regVal, cAf6_cdr_line_mode_ctrl_VC3Mode_, enable ? 1 : 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleCdr);
    return cAtOk;
    }

static eAtRet Vc3CepModeEnable(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, eBool enable)
    {
    if (Tha60290021ModuleSdhIsTerminatedVc(channel))
        return Vc3CepHwModeEnable(self, channel, slice, hwSts, enable);
    return m_ThaModuleCdrStmMethods->Vc3CepModeEnable(self, channel, slice, hwSts, enable);
    }

static uint32 SliceOffset(ThaModuleCdr self, AtSdhChannel channel, uint8 slice)
    {
    AtUnused(channel);
    return CommonSliceOffset(self, slice);
    }

static uint32 LoCdrStsTimingControl(Tha60210011ModuleCdr self)
    {
    AtUnused(self);
    return cAf6Reg_cdr_sts_timing_ctrl_Base;
    }

static uint32 EngineIdOfHoVc(AtSdhVc vc)
    {
    uint8 slice, hwSts;
    ThaSdhChannel2HwMasterStsId((AtSdhChannel)vc, cThaModuleCdr, &slice, &hwSts);

    return hwSts;
    }

static ThaCdrController HoVcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60290022HoVcCdrControllerNew(EngineIdOfHoVc(vc), (AtChannel)vc);
    }

static void OverrideTha60150011ModuleCdr(AtModule self)
    {
    Tha60150011ModuleCdr module = (Tha60150011ModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011ModuleCdrOverride, mMethodsGet(module), sizeof(m_Tha60150011ModuleCdrOverride));

        mMethodOverride(m_Tha60150011ModuleCdrOverride, LineModeHwSet);
        mMethodOverride(m_Tha60150011ModuleCdrOverride, HwVtgPldSet);
        }

    mMethodsSet(module, &m_Tha60150011ModuleCdrOverride);
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr module = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, m_ThaModuleCdrMethods, sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, CDRLineModeControlRegister);
        mMethodOverride(m_ThaModuleCdrOverride, BaseAddress);
        mMethodOverride(m_ThaModuleCdrOverride, HoVcCdrControllerCreate);
        }

    mMethodsSet(module, &m_ThaModuleCdrOverride);
    }

static void OverrideTha60210011ModuleCdr(AtModule self)
    {
    Tha60210011ModuleCdr cdrModule = (Tha60210011ModuleCdr) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleCdrOverride, mMethodsGet(cdrModule), sizeof(m_Tha60210011ModuleCdrOverride));

        mMethodOverride(m_Tha60210011ModuleCdrOverride, MaxNumLoSlices);
        mMethodOverride(m_Tha60210011ModuleCdrOverride, LoCdrStsTimingControl);
        mMethodOverride(m_Tha60210011ModuleCdrOverride, StartLoRamLocalId);
        }

    mMethodsSet(cdrModule, &m_Tha60210011ModuleCdrOverride);
    }

static void OverrideThaModuleCdrStm(AtModule self)
    {
    ThaModuleCdrStm module = (ThaModuleCdrStm)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrStmMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrStmOverride, m_ThaModuleCdrStmMethods, sizeof(m_ThaModuleCdrStmOverride));

        mMethodOverride(m_ThaModuleCdrStmOverride, StsTimingControlReg);
        mMethodOverride(m_ThaModuleCdrStmOverride, StsCdrChannelIdMask);
        mMethodOverride(m_ThaModuleCdrStmOverride, StsCdrChannelIdShift);
        mMethodOverride(m_ThaModuleCdrStmOverride, VtTimingControlReg);
        mMethodOverride(m_ThaModuleCdrStmOverride, VtCdrChannelIdMask);
        mMethodOverride(m_ThaModuleCdrStmOverride, VtCdrChannelIdShift);
        mMethodOverride(m_ThaModuleCdrStmOverride, SliceOffset);
        mMethodOverride(m_ThaModuleCdrStmOverride, Vc3CepModeEnable);
        mMethodOverride(m_ThaModuleCdrStmOverride, De3UnChannelizedModeSet);
        }

    mMethodsSet(module, &m_ThaModuleCdrStmOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleCdr(self);
    OverrideThaModuleCdrStm(self);
    OverrideTha60150011ModuleCdr(self);
    OverrideTha60210011ModuleCdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleCdr);
    }

AtModule Tha60290022ModuleCdrObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290022ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModuleCdrObjectInit(newModule, device);
    }

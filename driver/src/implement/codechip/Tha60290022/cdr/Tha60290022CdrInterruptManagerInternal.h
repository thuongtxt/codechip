/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60290022CdrInterruptManagerInternal.h
 * 
 * Created Date: Feb 6, 2018
 *
 * Description : Interrupt manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022CDRINTERRUPTMANAGERINTERNAL_H_
#define _THA60290022CDRINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/cdr/Tha60210011CdrInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022CdrInterruptManager * Tha60290022CdrInterruptManager;

typedef struct tTha60290022CdrInterruptManager
    {
    tTha60210011LoCdrInterruptManager super;
    }tTha60290022CdrInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager Tha60290022CdrInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022CDRINTERRUPTMANAGERINTERNAL_H_ */


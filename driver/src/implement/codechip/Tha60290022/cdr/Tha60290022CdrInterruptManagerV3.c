/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60290022CdrInterruptManagerV3.c
 *
 * Created Date: Feb 6, 2018
 *
 * Description : Interrupt manager for CDR version 3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022ModuleCdrV3.h"
#include "Tha60290022CdrInterruptManagerInternal.h"
#include "Tha60290022CdrV3AcrReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022CdrInterruptManagerV3
    {
    tTha60290022CdrInterruptManager super;
    }tTha60290022CdrInterruptManagerV3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods             m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods            m_ThaInterruptManagerOverride;
static tThaCdrInterruptManagerMethods         m_ThaCdrInterruptManagerOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 InterruptEnableRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_cdr_per_chn_intr_en_ctrl_Base;
    }

static uint32 InterruptStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_cdr_per_chn_intr_stat_Base;
    }

static uint32 CurrentStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_cdr_per_chn_curr_stat_Base;
    }

static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self));
    uint32 numSlices = mMethodsGet(mIntrManager(self))->NumSlices(mIntrManager(self));
    uint32 intrGroupOR = AtHalRead(hal, baseAddress + cAf6Reg_CDR_per_grp_intr_or_stat_Base);
    uint8 group;
    AtUnused(glbIntr);

    for (group = 0; group < 2; group++)
        {
        if (intrGroupOR & cIteratorMask(group))
            {
            uint32 groupOffset = (uint32)(group * 1024U) + baseAddress;
            uint32 groupIntrOr = AtHalRead(hal, cAf6Reg_cdr_per_stsvc_intr_or_stat_Base + groupOffset);
            uint32 groupIntrEn = AtHalRead(hal, cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base + groupOffset);
            uint32 sts_i;
            uint32 sts48;

            groupIntrOr &= groupIntrEn;
            for (sts_i = 0, sts48 = group * 32U; sts_i < 32 && sts48 < 48; sts_i++, sts48++)
                {
                if (groupIntrOr & cIteratorMask(sts_i))
                    {
                    uint32 stsIntrOr = AtHalRead(hal, groupOffset + cAf6Reg_cdr_per_chn_intr_or_stat_Base + sts_i);
                    uint8 vtInSts;

                    for (vtInSts = 0; vtInSts < 28; vtInSts++)
                        {
                        if (stsIntrOr & cIteratorMask(vtInSts))
                            {
                            uint32 vtOffset = (uint32)(sts48 * 32 + vtInSts);
                            uint32 vtIntrSta = AtHalRead(hal, baseAddress + cAf6Reg_cdr_per_chn_intr_stat_Base + vtOffset);
                            uint32 vtIntrEn = AtHalRead(hal, baseAddress + cAf6Reg_cdr_per_chn_intr_en_ctrl_Base + vtOffset);
                            uint8 slice;

                            vtIntrSta &= vtIntrEn;
                            for (slice = 0; slice < numSlices; slice++)
                                {
                                if (vtIntrSta & cIteratorMask(slice))
                                    {
                                    ThaCdrController controller = ThaCdrInterruptManagerAcrDcrControllerFromHwIdGet((ThaCdrInterruptManager)self, slice, (uint8)sts48, vtInSts);
                                    ThaCdrControllerInterruptProcess(controller, vtOffset);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

static void InterruptOnIpCoreEnable(AtInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress((ThaInterruptManager)self);

    AtHalWrite(hal, baseAddress + cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base ,        enable ? cBit31_0 : 0);
    AtHalWrite(hal, baseAddress + cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base + 1024U, enable ? cBit15_0 : 0);
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x0E00000;
    }

static uint32 InterruptOffset(ThaCdrInterruptManager self, ThaCdrController controller)
    {
    AtUnused(self);
    return ThaCdrControllerIdGet(controller);
    }

static void OverrideThaCdrInterruptManager(AtInterruptManager self)
    {
    ThaCdrInterruptManager manager = (ThaCdrInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaCdrInterruptManagerOverride));

        mMethodOverride(m_ThaCdrInterruptManagerOverride, InterruptOffset);
        }

    mMethodsSet(manager, &m_ThaCdrInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, InterruptEnableRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, CurrentStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        mMethodOverride(m_AtInterruptManagerOverride, InterruptOnIpCoreEnable);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    OverrideThaCdrInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022CdrInterruptManagerV3);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022CdrInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60290022CdrInterruptManagerV3New(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newManager, module);
    }

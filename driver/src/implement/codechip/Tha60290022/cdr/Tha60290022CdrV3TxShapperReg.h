/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_CDR_v3_TxShapper_H_
#define _AF6_REG_AF6CNC0022_RD_CDR_v3_TxShapper_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : DCR Tx Engine Timing control
Reg Addr   : 0x0010400
Reg Formula: 0x0010400 + OC48*4096 + STS*32 + TUG*4 + VT
    Where  : 
           + $OC48(0-7):
           + $STS(0-47):
           + $TUG(0 -6):
           + $VT(0-3): (0-2)in E1 and (0-3) in DS1
Reg Desc   : 
This register is used to configure timing mode for per STS

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_tx_eng_timing_ctrl_Base                                                          0x0000000

/*--------------------------------------
BitField Name: PktLen_bit
BitField Type: RW
BitField Desc: The payload packet length BIT, PktLen[2:0]
BitField Bits: [21:19]
--------------------------------------*/
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_bit_Mask                                                  cBit21_19
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_bit_Shift                                                        19

/*--------------------------------------
BitField Name: LineType
BitField Type: RW
BitField Desc: Line type mode 0: DS1 1: E1 2: DS3 3: E3 4: VT15 5: VT2 6:
STS1/TU3 7: Reserve
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_dcr_tx_eng_timing_ctrl_LineType_Mask                                                    cBit18_16
#define cAf6_dcr_tx_eng_timing_ctrl_LineType_Shift                                                          16

/*--------------------------------------
BitField Name: PktLen_byte
BitField Type: RW
BitField Desc: The payload packet length Byte, PktLen[17:3]
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_byte_Mask                                                  cBit15_0
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_byte_Shift                                                        0

#endif /* _AF6_REG_AF6CNC0022_RD_CDR_v3_TxShapper_H_ */

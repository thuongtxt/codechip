/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60290022ModuleCdrInternal.h
 * 
 * Created Date: Feb 5, 2018
 *
 * Description : CDR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULECDRINTERNAL_H_
#define _THA60290022MODULECDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/cdr/Tha60290021ModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModuleCdr
    {
    tTha60290021ModuleCdr super;
    }tTha60290022ModuleCdr;

typedef struct tTha60290022ModuleCdrV3
    {
    tTha60290022ModuleCdr super;

    AtLongRegisterAccess v3LongAccess; /* For new holds in AF6CNC0022_RD_CDR_v3.atreg */
    }tTha60290022ModuleCdrV3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290022ModuleCdrObjectInit(AtModule self, AtDevice device);
AtModule Tha60290022ModuleCdrV3ObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULECDRINTERNAL_H_ */


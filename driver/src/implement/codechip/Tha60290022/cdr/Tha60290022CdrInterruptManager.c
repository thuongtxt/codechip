/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60290022CdrInterruptManager.c
 *
 * Created Date: Apr 19, 2017
 *
 * Description : CDR interrupt manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtIpCoreInternal.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "Tha60290022ModuleCdrReg.h"
#include "Tha60290022CdrInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods       m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods      m_ThaInterruptManagerOverride;
static tThaCdrInterruptManagerMethods   m_ThaCdrInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 8;
    }

static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    static const uint8 cNumGroups = 2;
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self));
    uint32 numSlices = mMethodsGet(mIntrManager(self))->NumSlices(mIntrManager(self));
    uint32 intrSliceOR = mMethodsGet(mIntrManager(self))->SlicesInterruptGet(mIntrManager(self), glbIntr, hal);
    uint8 slice;

    for (slice = 0; slice < numSlices; slice++)
        {
        if (intrSliceOR & cIteratorMask(slice))
            {
            uint32 sliceOffset = mMethodsGet(mIntrManager(self))->SliceOffset(mIntrManager(self), slice);
            uint32 sliceBase = baseAddress + sliceOffset;
            uint32 groupOrStatus = AtHalRead(hal, sliceBase + cAf6Reg_CDR_per_grp_intr_or_stat_Base);
            uint32 group_i;

            for (group_i = 0; group_i < cNumGroups; group_i++)
                {
                if (groupOrStatus & cIteratorMask(group_i))
                    {
                    uint32 groupOffset = group_i * 1024;
                    uint32 intrMask = AtHalRead(hal, sliceBase + cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base + groupOffset);
                    uint32 intrStatus = AtHalRead(hal, sliceBase + cAf6Reg_cdr_per_stsvc_intr_or_stat_Base + groupOffset);
                    uint32 stsInGroup;
                    uint32 numStsInGroup = (group_i == 0) ? 32 :16;

                    intrStatus &= intrMask;

                    for (stsInGroup = 0; stsInGroup < numStsInGroup; stsInGroup++)
                        {
                        if (intrStatus & cIteratorMask(stsInGroup))
                            {
                            uint32 intrStatus32 = AtHalRead(hal, sliceBase + groupOffset + cAf6Reg_cdr_per_chn_intr_or_stat_Base + stsInGroup);
                            uint8 vtInSts;

                            for (vtInSts = 0; vtInSts < 28; vtInSts++)
                                {
                                if (intrStatus32 & cIteratorMask(vtInSts))
                                    {
                                    uint32 hwStsId = (group_i * 32) + stsInGroup;
                                    uint32 offset = sliceOffset + (hwStsId << 5) + vtInSts;
                                    ThaCdrController controller = ThaCdrInterruptManagerAcrDcrControllerFromHwIdGet((ThaCdrInterruptManager)self, slice, (uint8)hwStsId, vtInSts);
                                    ThaCdrControllerInterruptProcess(controller, offset);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

static void InterruptOnIpCoreEnable(AtInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress((ThaInterruptManager)self);
    uint32 numSlices = mMethodsGet(mIntrManager(self))->NumSlices(mIntrManager(self));
    uint32 slice_i;

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        {
        uint32 sliceOffset = mMethodsGet(mIntrManager(self))->SliceOffset(mIntrManager(self), (uint8)slice_i);
        uint32 address = baseAddress + cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base + sliceOffset;
        AtHalWrite(hal, address       , enable ? cBit31_0 : 0);
        AtHalWrite(hal, address + 1024, enable ? cBit16_0 : 0);
        }
    }

static uint32 CurrentStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_cdr_per_chn_curr_stat_Base;
    }

static uint32 InterruptStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_cdr_per_chn_intr_stat_Base;
    }

static uint32 InterruptEnableRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_cdr_per_chn_intr_en_ctrl_Base;
    }

static AtModuleSdh ModuleSdh(AtInterruptManager self)
    {
    AtDevice device = AtModuleDeviceGet(AtInterruptManagerModuleGet(self));
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static ThaCdrController AcrDcrControllerFromHwIdGet(ThaCdrInterruptManager self, uint8 slice, uint8 sts, uint8 vt)
    {
    AtModuleSdh sdhModule = ModuleSdh((AtInterruptManager)self);
    AtSdhLine terminatedLine = Tha60290021ModuleSdhTerminatedLineGet(sdhModule, slice);
    AtSdhChannel au;
    AtSdhChannel vc;

    if (AtSdhLineRateGet(terminatedLine) == cAtSdhLineRateStm64)
        {
        vc = (AtSdhChannel)AtSdhLineVc4_64cGet(terminatedLine, 0);
        return ThaSdhVcCdrControllerGet((AtSdhVc)vc);
        }

    au = Tha60210011ModuleSdhAuFromHwStsGet(terminatedLine, sts);
    if (au == NULL)
        return NULL;

    vc = AtSdhChannelSubChannelGet((AtSdhChannel)au, 0);
    if (Tha60290021ModuleSdhVcIsFromLoBus(sdhModule, vc) == cAtFalse)
        return ThaSdhVcCdrControllerGet((AtSdhVc)vc);

    return Tha6021CdrControllerFromAuPathGet((AtSdhPath)au, sts, vt);
    }

static void OverrideThaCdrInterruptManager(AtInterruptManager self)
    {
    ThaCdrInterruptManager manager = (ThaCdrInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaCdrInterruptManagerOverride));

        mMethodOverride(m_ThaCdrInterruptManagerOverride, AcrDcrControllerFromHwIdGet);
        }

    mMethodsSet(manager, &m_ThaCdrInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        mMethodOverride(m_ThaInterruptManagerOverride, CurrentStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptEnableRegister);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        mMethodOverride(m_AtInterruptManagerOverride, InterruptOnIpCoreEnable);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    OverrideThaCdrInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022CdrInterruptManager);
    }

AtInterruptManager Tha60290022CdrInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011LoCdrInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60290022CdrInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022CdrInterruptManagerObjectInit(newManager, module);
    }

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_CDR_v3_ACR_H_
#define _AF6_REG_AF6CNC0022_RD_CDR_v3_ACR_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : CDR ACR CPU  Reg Hold Control
Reg Addr   : 0x070000-0x070002
Reg Formula: 0x070000 + HoldId
    Where  : 
           + $HoldId(0-2): Hold register
Reg Desc   : 
The register provides hold register for three word 32-bits MSB when CPU access to engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_acr_cpu_hold_ctrl_Base                                                            0x070000

/*--------------------------------------
BitField Name: HoldReg0
BitField Type: RW
BitField Desc: Hold 32 bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_acr_cpu_hold_ctrl_HoldReg0_Mask                                                      cBit31_0
#define cAf6_cdr_acr_cpu_hold_ctrl_HoldReg0_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : CDR ACR Engine Timing control
Reg Addr   : 0x0000800-0x000FFF
Reg Formula: 0x0000800 + OC48*32768 + STS*32 + TUG*4 + VT
    Where  : 
           + $OC48(0-7):
           + $STS(0-47):
           + $TUG(0 -6):
           + $VT(0-3): (0-2)in E1 and (0-3) in DS1 HDL_PATH     : engcdr.slc0_cdrnco_engacr[OC48].ramcfg.array.ram[$STS*32 + $TUG*4 + $VT]
Reg Desc   : 
This register is used to configure timing mode for per STS

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_acr_eng_timing_ctrl_Base                                                         0x0000800

/*--------------------------------------
BitField Name: VC4_4c
BitField Type: RW
BitField Desc: VC4_4c/TU3 mode 0: STS1/VC4/DS3 1: TU3/VC4-4c/OC48 STS192c
(VC4_64c = 4xOC48 => payload / 4, for support full payload size of STS192c
STS192c (VC4_64c), STS48c (VC4_16c), STS24c (VC4_8c), STS12c (VC4_4c), thi phai
config theo VC4_4c rate (bit26 =1) STS192c (VC4_64c = 16xVC4_4c => payload / 16,
STS48c (VC4_16c = 4xVC4_4c => payload / 4), STS24c (VC4_8c = 2xVC4_4c => payload
/ 2), STS12c (VC4_4c, payload real) Sample: VC4_8c (STS24c) with paylaod zie =
783byte =  783x8 = 6264 bit => payload config to CDR = 6264/2 = 3132bit = 0xC3C
BitField Bits: [26]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_VC4_4c_Mask                                                        cBit26
#define cAf6_cdr_acr_eng_timing_ctrl_VC4_4c_Shift                                                           26

/*--------------------------------------
BitField Name: PktLen_ind
BitField Type: RW
BitField Desc: The payload packet  length for jumbo frame
BitField Bits: [25]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_ind_Mask                                                    cBit25
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_ind_Shift                                                       25

/*--------------------------------------
BitField Name: HoldValMode
BitField Type: RW
BitField Desc: Hold value mode of NCO, default value 0 0: Hardware calculated
and auto update 1: Software calculated and update, hardware is disabled
BitField Bits: [24]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_HoldValMode_Mask                                                   cBit24
#define cAf6_cdr_acr_eng_timing_ctrl_HoldValMode_Shift                                                      24

/*--------------------------------------
BitField Name: SeqMode
BitField Type: RW
BitField Desc: Sequence mode mode, default value 0 0: Wrap zero 1: Skip zero
BitField Bits: [23]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_SeqMode_Mask                                                       cBit23
#define cAf6_cdr_acr_eng_timing_ctrl_SeqMode_Shift                                                          23

/*--------------------------------------
BitField Name: LineType
BitField Type: RW
BitField Desc: Line type mode 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3/OC48 6:
STS1/TU3 7: VC4/VC4-4c
BitField Bits: [22:20]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_LineType_Mask                                                   cBit22_20
#define cAf6_cdr_acr_eng_timing_ctrl_LineType_Shift                                                         20

/*--------------------------------------
BitField Name: PktLen
BitField Type: RW
BitField Desc: The payload packet  length parameter to create a packet. SAToP
mode: The number payload of bit. CESoPSN mode: The number of bit which converted
to full DS1/E1 rate mode. In CESoPSN mode, the payload is assembled by NxDS0
with M frame, the value configured to this register is Mx256 bits for E1 mode,
Mx193 bits for DS1 mode.
BitField Bits: [19:4]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_Mask                                                      cBit19_4
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_Shift                                                            4

/*--------------------------------------
BitField Name: CDRTimeMode
BitField Type: RW
BitField Desc: CDR time mode 0: System mode 1: Loop timing mode, transparency
service Rx clock to service Tx clock 2: LIU timing mode, using service Rx clock
for CDR source to generate service Tx clock 3: Prc timing mode, using Prc clock
for CDR source to generate service Tx clock 4: Ext#1 timing mode, using Ext#1
clock for CDR source to generate service Tx clock 5: Ext#2 timing mode, using
Ext#2 clock for CDR source to generate service Tx clock 6: Tx SONET timing mode,
using Tx Line OCN clock for CDR source to generate service Tx clock 7: Free
timing mode, using system clock for CDR source to generate service Tx clock 8:
ACR timing mode 9: Reserve 10: DCR timing mode 11: Reserve 12: ACR Fast lock
mode
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_CDRTimeMode_Mask                                                  cBit3_0
#define cAf6_cdr_acr_eng_timing_ctrl_CDRTimeMode_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : CDR Adjust State status
Reg Addr   : 0x0001000-0x0017FF
Reg Formula: 0x0001000 + OC48*32768 + STS*32 + TUG*4 + VT
    Where  : 
           + $OC48(0-7):
           + $STS(0-47):
           + $TUG(0 -6):
           + $VT(0-3): (0-2)in E1 and (0-3) in DS1
Reg Desc   : 
This register is used to store status or configure  some parameter of per CDR engine

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_adj_state_stat_Base                                                              0x0001000
#define cAf6Reg_cdr_adj_state_stat_WidthVal                                                                 32

/*--------------------------------------
BitField Name: Adjstate
BitField Type: RW
BitField Desc: Adjust state 0: Load state 1: Wait state 2: Init state 3: Learn
State 4: ReLearn State 5: Lock State 6: ReInit State 7: Holdover State
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_cdr_adj_state_stat_Adjstate_Mask                                                          cBit2_0
#define cAf6_cdr_adj_state_stat_Adjstate_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : CDR Adjust Holdover value status
Reg Addr   : 0x0001800-0x001FFF
Reg Formula: 0x0001800 + OC48*32768 + STS*32 + TUG*4 + VT
    Where  : 
           + $OC48(0-7):
           + $STS(0-47):
           + $TUG(0 -6):
           + $VT(0-3): (0-2)in E1 and (0-3) in DS1
Reg Desc   : 
This register is used to store status or configure  some parameter of per CDR engine

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_adj_holdover_value_stat_Base                                                     0x0001800

/*--------------------------------------
BitField Name: HoldVal
BitField Type: RO
BitField Desc: NCO Holdover value parameter E1RATE   = 32'd3817748707;
Resolution =    0.262 parameter DS1RATE  = 32'd3837632815;   Resolution =
0.260 parameter VT2RATE  = 32'd4175662648;   Resolution =    0.239 parameter
VT15RATE = 32'd4135894433;   Resolution =    0.241 parameter E3RATE   =
32'd2912117977;   Resolution =    0.343 parameter DS3RATE  = 32'd3790634015;
Resolution =    0.263 parameter TU3RATE  = 32'd4148547956;   Resolution =
0.239 parameter STS1RATE = 32'd4246160849;   Resolution =    0.239
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_adj_holdover_value_stat_HoldVal_Mask                                                 cBit31_0
#define cAf6_cdr_adj_holdover_value_stat_HoldVal_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCR TX Engine Active Control
Reg Addr   : 0x0040000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to activate the DCR TX Engine. Active high.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_tx_eng_active_ctrl_Base                                                          0x0040000

/*--------------------------------------
BitField Name: data
BitField Type: RW
BitField Desc: DCR TX Engine Active Control
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dcr_tx_eng_active_ctrl_data_Mask                                                            cBit0
#define cAf6_dcr_tx_eng_active_ctrl_data_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : DCR PRC Source Select Configuration
Reg Addr   : 0x0040001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure to select PRC source for PRC timer. The PRC clock selected must be less than 19.44 Mhz.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_prc_src_sel_cfg_Base                                                             0x0040001
#define cAf6Reg_dcr_prc_src_sel_cfg_WidthVal                                                                32

/*--------------------------------------
BitField Name: DcrPrcSourceSel
BitField Type: RW
BitField Desc: PRC source selection. 0: PRC Reference Clock 1: System Clock
19Mhz 2: External Reference Clock 1. 3: External Reference Clock 2. 4: Ocn Line
Clock Port 1 5: Ocn Line Clock Port 2 6: Ocn Line Clock Port 3 7: Ocn Line Clock
Port 4
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_Mask                                                  cBit2_0
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCR PRC Frequency Configuration
Reg Addr   : 0x004000b
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure the frequency of PRC clock.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_prc_freq_cfg_Base                                                                0x004000b
#define cAf6Reg_dcr_prc_freq_cfg_WidthVal                                                                   32

/*--------------------------------------
BitField Name: DCRPrcFrequency
BitField Type: RW
BitField Desc: Frequency of PRC clock. This is used to configure the frequency
of PRC clock in Khz. Unit is Khz. Exp: The PRC clock is 19.44Mhz. This register
will be configured to 19440.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dcr_prc_freq_cfg_DCRPrcFrequency_Mask                                                    cBit15_0
#define cAf6_dcr_prc_freq_cfg_DCRPrcFrequency_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCR RTP Frequency Configuration
Reg Addr   : 0x004000C
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure the frequency of PRC clock.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_rtp_freq_cfg_Base                                                                0x004000C
#define cAf6Reg_dcr_rtp_freq_cfg_WidthVal                                                                   32

/*--------------------------------------
BitField Name: DCRRTPFreq
BitField Type: RW
BitField Desc: Frequency of RTP clock. This is used to configure the frequency
of RTP clock in Khz. Unit is Khz. Exp: The RTP clock is 19.44Mhz. This register
will be configured to 19440.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dcr_rtp_freq_cfg_DCRRTPFreq_Mask                                                         cBit15_0
#define cAf6_dcr_rtp_freq_cfg_DCRRTPFreq_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : RAM ACR Parity Force Control
Reg Addr   : 0x0000c
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_ACR_Parity_Force_Control_Base                                                      0x0000c

/*--------------------------------------
BitField Name: CDRACRDDR_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For DDR of CDR
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRDDR_ParErrFrc_Mask                                       cBit8
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRDDR_ParErrFrc_Shift                                          8

/*--------------------------------------
BitField Name: CDRACRTimingCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR ACR Engine Timing control",
each bit for per OC48
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Mask                                 cBit7_0
#define cAf6_RAM_ACR_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RAM ACR Parity Disable Control
Reg Addr   : 0x000d
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_ACR_Parity_Disable_Control_Base                                                     0x000d

/*--------------------------------------
BitField Name: CDRACRDDR_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For DDR of CDR
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRDDR_ParErrDis_Mask                                     cBit8
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRDDR_ParErrDis_Shift                                        8

/*--------------------------------------
BitField Name: CDRACRTimingCtrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR ACR Engine Timing control" ,
each bit for per OC48
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrl_ParErrDis_Mask                                 cBit7_0
#define cAf6_RAM_ACR_Parity_Disable_Control_CDRACRTimingCtrl_ParErrDis_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RAM ACR parity Error Sticky
Reg Addr   : 0x000e
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_ACR_Parity_Error_Sticky_Base                                                        0x000e

/*--------------------------------------
BitField Name: CDRACRDDR_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For DDR of CDR
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRDDR_ParErrStk_Mask                                        cBit8
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRDDR_ParErrStk_Shift                                           8

/*--------------------------------------
BitField Name: CDRACRTimingCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Error parity For RAM Control "CDR ACR Engine Timing control"  ,
each bit for per OC48
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrl_ParErrStk_Mask                                 cBit7_0
#define cAf6_RAM_ACR_Parity_Error_Sticky_CDRACRTimingCtrl_ParErrStk_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit3_0 Control
Reg Addr   : 0x70200
Reg Formula: 0x70200 + HaAddr3_0
    Where  : 
           + $HaAddr3_0(0-15): HA Address Bit3_0
Reg Desc   : 
This register is used to send HA read address bit3_0 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha3_0_control_Base                                                                   0x70200
#define cAf6Reg_rdha3_0_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr3_0
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr3_0
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha3_0_control_ReadAddr3_0_Mask                                                         cBit19_0
#define cAf6_rdha3_0_control_ReadAddr3_0_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit7_4 Control
Reg Addr   : 0x70210
Reg Formula: 0x70210 + HaAddr7_4
    Where  : 
           + $HaAddr7_4(0-15): HA Address Bit7_4
Reg Desc   : 
This register is used to send HA read address bit7_4 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha7_4_control_Base                                                                   0x70210
#define cAf6Reg_rdha7_4_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr7_4
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr7_4
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha7_4_control_ReadAddr7_4_Mask                                                         cBit19_0
#define cAf6_rdha7_4_control_ReadAddr7_4_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit11_8 Control
Reg Addr   : 0x70220
Reg Formula: 0x70220 + HaAddr11_8
    Where  : 
           + $HaAddr11_8(0-15): HA Address Bit11_8
Reg Desc   : 
This register is used to send HA read address bit11_8 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha11_8_control_Base                                                                  0x70220
#define cAf6Reg_rdha11_8_control_WidthVal                                                                   32

/*--------------------------------------
BitField Name: ReadAddr11_8
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr11_8
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha11_8_control_ReadAddr11_8_Mask                                                       cBit19_0
#define cAf6_rdha11_8_control_ReadAddr11_8_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit15_12 Control
Reg Addr   : 0x70230
Reg Formula: 0x70230 + HaAddr15_12
    Where  : 
           + $HaAddr15_12(0-15): HA Address Bit15_12
Reg Desc   : 
This register is used to send HA read address bit15_12 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha15_12_control_Base                                                                 0x70230
#define cAf6Reg_rdha15_12_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr15_12
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr15_12
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha15_12_control_ReadAddr15_12_Mask                                                     cBit19_0
#define cAf6_rdha15_12_control_ReadAddr15_12_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit19_16 Control
Reg Addr   : 0x70240
Reg Formula: 0x70240 + HaAddr19_16
    Where  : 
           + $HaAddr19_16(0-15): HA Address Bit19_16
Reg Desc   : 
This register is used to send HA read address bit19_16 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha19_16_control_Base                                                                 0x70240
#define cAf6Reg_rdha19_16_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr19_16
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr19_16
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha19_16_control_ReadAddr19_16_Mask                                                     cBit19_0
#define cAf6_rdha19_16_control_ReadAddr19_16_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit23_20 Control
Reg Addr   : 0x70250
Reg Formula: 0x70250 + HaAddr23_20
    Where  : 
           + $HaAddr23_20(0-15): HA Address Bit23_20
Reg Desc   : 
This register is used to send HA read address bit23_20 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha23_20_control_Base                                                                 0x70250
#define cAf6Reg_rdha23_20_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr23_20
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr23_20
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha23_20_control_ReadAddr23_20_Mask                                                     cBit19_0
#define cAf6_rdha23_20_control_ReadAddr23_20_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit24 and Data Control
Reg Addr   : 0x70260
Reg Formula: 0x70260 + HaAddr24
    Where  : 
           + $HaAddr24(0-1): HA Address Bit24
Reg Desc   : 
This register is used to send HA read address bit24 to HA engine to read data

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha24data_control_Base                                                                0x70260

/*--------------------------------------
BitField Name: ReadHaData31_0
BitField Type: RW
BitField Desc: HA read data bit31_0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha24data_control_ReadHaData31_0_Mask                                                   cBit31_0
#define cAf6_rdha24data_control_ReadHaData31_0_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data63_32
Reg Addr   : 0x70270
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha_hold63_32_Base                                                                    0x70270

/*--------------------------------------
BitField Name: ReadHaData63_32
BitField Type: RW
BitField Desc: HA read data bit63_32
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha_hold63_32_ReadHaData63_32_Mask                                                      cBit31_0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data95_64
Reg Addr   : 0x70271
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold95_64_Base                                                                  0x70271

/*--------------------------------------
BitField Name: ReadHaData95_64
BitField Type: RW
BitField Desc: HA read data bit95_64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Mask                                                    cBit31_0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data127_96
Reg Addr   : 0x70272
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold127_96_Base                                                                 0x70272

/*--------------------------------------
BitField Name: ReadHaData127_96
BitField Type: RW
BitField Desc: HA read data bit127_96
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Mask                                                  cBit31_0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Interrupt Enable Control
Reg Addr   : 0x0006000-0x00067FF
Reg Formula: 0x0006000 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-47): STS-1/VC-3 ID
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel interrupt enable of CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_intr_en_ctrl_Base                                                        0x0006000

/*--------------------------------------
BitField Name: CDRUnlokcedIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change UnLocked te event to generate an
interrupt. Each bit for per OC48
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_Mask                                           cBit7_0
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Interrupt Status
Reg Addr   : 0x0006800-0x0006FFF
Reg Formula: 0x0006800 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-47): STS-1/VC-3 ID
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel interrupt tus of CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_intr_stat_Base                                                           0x0006800

/*--------------------------------------
BitField Name: CDRUnLockedIntr
BitField Type: W1C
BitField Desc: Set 1 if there is a change in UnLocked the event. Each bit for
per OC48
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_Mask                                                cBit7_0
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Current Status
Reg Addr   : 0x0007000-0x00077FF
Reg Formula: 0x0007000 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-47): STS-1/VC-3 ID
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel Current tus of CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_curr_stat_Base                                                           0x0007000

/*--------------------------------------
BitField Name: CDRUnLockedCurrSta
BitField Type: RW
BitField Desc: Current tus of UnLocked event. Each bit for per OC48
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_Mask                                             cBit7_0
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Interrupt OR Status
Reg Addr   : 0x0007800-0x000782F
Reg Formula: 0x0007800 +  GrpID*1024 + STSID
    Where  : 
           + $GrpID(0-1): Group 0 for STS 0-31, 1 for STS 32-47
           + $STSID(0-31)
Reg Desc   : 
The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the CDR. Each bit is used to store Interrupt OR tus of the related DS1/E1.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_intr_or_stat_Base                                                        0x0007800

/*--------------------------------------
BitField Name: CDRVtIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding DS1/E1 is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_Mask                                             cBit31_0
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CDR per STS/VC Interrupt OR Status
Reg Addr   : 0x0007BFF
Reg Formula: 0x0007BFF +  GrpID*1024
    Where  : 
           + $GrpID(0-1): Group 0 for STS 0-31, 1 for STS 32-47
Reg Desc   : 
The register consists of 24 bits for 24 STS/VCs of the CDR. Each bit is used to store Interrupt OR tus of the related STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_stsvc_intr_or_stat_Base                                                      0x0007BFF

/*--------------------------------------
BitField Name: CDRStsIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding STS/VC is
set and its interrupt is enabled
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_Mask                                          cBit31_0
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : CDR per STS/VC Interrupt Enable Control
Reg Addr   : 0x0007BFE
Reg Formula: 0x0007BFE +  GrpID*1024
    Where  : 
           + $GrpID(0-1): Group 0 for STS 0-31, 1 for STS 32-47
Reg Desc   : 
The register consists of 24 interrupt enable bits for 24 STS/VCs in the Rx DS1/E1/J1 Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base                                                      0x0007BFE

/*--------------------------------------
BitField Name: CDRStsIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related STS/VC to generate interrupt.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_Mask                                             cBit31_0
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Group Interrupt OR Status
Reg Addr   : 0x0000010
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits for 2 Group of the CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_CDR_per_grp_intr_or_stat_Base                                                        0x0000010

/*--------------------------------------
BitField Name: RxDE1GrpIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt tus bit of corresponding Group is set
and its interrupt is enabled
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_CDR_per_grp_intr_or_stat_RxDE1GrpIntrOrSta_Mask                                           cBit1_0
#define cAf6_CDR_per_grp_intr_or_stat_RxDE1GrpIntrOrSta_Shift                                                0

#endif /* _AF6_REG_AF6CNC0022_RD_CDR_v3_ACR_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60290022Device.c
 *
 * Created Date: Jul 2, 2016
 *
 * Description : 20G CEM product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtIpCoreInternal.h"
#include "../../../default/xc/ThaModuleXc.h"
#include "../../../default/aps/ThaModuleHardAps.h"
#include "Tha60290022DeviceInternal.h"
#include "../../../default/ptp/ThaModulePtp.h"
#include "../pdh/Tha60290022ModulePdh.h"
#include "../pwe/Tha60290022ModulePwe.h"
#include "../physical/Tha60290022Physical.h"
#include "../prbs/Tha60290022ModulePrbs.h"
#include "Tha60290022DeviceInternal.h"
#include "../pktanalyzer/Tha60290022ModulePktAnalyzer.h"
#include "Tha60290022Device.h"
#include "Tha60290022DeviceTopReg.h"
#include "Tha60290022InterruptController.h"
#include "../../../default/physical/ThaPowerSupplySensor.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290022Device)(self))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290022DeviceMethods m_methods;

/* Override */
static tAtObjectMethods          m_AtObjectOverride;
static tAtDeviceMethods          m_AtDeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;
static tTha60210011DeviceMethods m_Tha60210011DeviceOverride;
static tTha60210051DeviceMethods m_Tha60210051DeviceOverride;
static tTha60290021DeviceMethods m_Tha60290021DeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;

/* Super implementation */
static const tAtObjectMethods          *m_AtObjectMethods  = NULL;
static const tAtDeviceMethods          *m_AtDeviceMethods          = NULL;
static const tThaDeviceMethods         *m_ThaDeviceMethods         = NULL;
static const tTha60150011DeviceMethods *m_Tha60150011DeviceMethods = NULL;
static const tTha60210011DeviceMethods *m_Tha60210011DeviceMethods = NULL;
static const tTha60290021DeviceMethods *m_Tha60290021DeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static  AtSerdesManager SerdesManagerObjectCreate(AtDevice self)
    {
    return Tha60290022SerdesManagerNew(self);
    }

static eBool Refout8kCanDisplay(Tha60290021Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasHoBus(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 NumUsedOc48Slice(Tha60210011Device self)
    {
    if (Tha60290022DeviceShouldOpenFullCapacity((AtDevice)self))
        return 8;
    return m_Tha60210011DeviceMethods->NumUsedOc48Slice(self);
    }

static void HwFlushPdhSlice(Tha60210051Device self, uint8 slice)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet((AtDevice)self, cAtModulePdh);
    Tha60290022ModulePdhHwFlushPdhSlice(pdhModule, slice);
    }

static eAtRet PlaHwFlush(Tha60210011Device self)
	{
	Tha60290022ModulePweHwFlushPla((ThaModulePwe)AtDeviceModuleGet((AtDevice)self, cThaModulePwe));
	return cAtOk;
	}

static eAtRet PrbsHwFlush(Tha60210011Device self)
	{
	Tha60290022ModulePrbsHwFlush((ThaModulePrbs)AtDeviceModuleGet((AtDevice)self, cAtModulePrbs));
	return cAtOk;
	}

static AtModule ModuleSdhCreate(AtDevice self)
    {
    if (Tha60290022DeviceV3OptimizationIsSupported(self))
        return (AtModule)Tha60290022ModuleSdhV2New(self);

    return (AtModule)Tha60290022ModuleSdhNew(self);
    }

static AtModule ModulePdhCreate(AtDevice self)
    {
    if (Tha60290022DeviceV3OptimizationIsSupported(self))
        return (AtModule)Tha60290022ModulePdhV3New(self);

    return (AtModule)Tha60290022ModulePdhNew(self);
    }

static eBool ShouldOpenFeatureFromVersion(AtDevice self, uint32 major, uint32 minor, uint32 betaBuild)
    {
    uint32 startVerion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(major, minor, betaBuild);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(self));
    return (currentVersion >= startVerion) ? cAtTrue : cAtFalse;
    }

static AtModule ModuleCdrCreate(AtDevice self)
    {
    if (Tha60290022DeviceV3OptimizationIsSupported(self))
        return Tha60290022ModuleCdrV3New(self);
    return Tha60290022ModuleCdrNew(self);
    }

static AtModule ModulePmcCreate(AtDevice self)
    {
    if (Tha60290022DevicePmc1PageIsSupported(self))
        return Tha60290022ModulePmcV2New(self);

    return Tha60290022ModulePmcNew(self);
    }

static AtModule ModulePohCreate(AtDevice self)
    {
    if (Tha60290022DeviceV3OptimizationIsSupported(self))
        return Tha60290022ModulePohV2New(self);
    return Tha60290022ModulePohNew(self);
    }

static AtModule ModuleClockCreate(AtDevice self)
    {
    if (Tha60290022DeviceClockSquelchingIsSupported(self))
        return (AtModule)Tha60290022ModuleClockNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, cAtModuleClock);
    }

static AtModule ModuleClaCreate(AtDevice self)
    {
    if (Tha60290022DeviceV3OptimizationIsSupported(self))
        return Tha60290022ModuleClaV3New(self);

    return Tha60290022ModuleClaNew(self);
    }

static AtModule ModulePrbsCreate(AtDevice self)
    {
    if (Tha60290022DeviceSts192cPrbsSupported(self))
        return (AtModule)Tha60290022ModulePrbsV2New(self);

    return (AtModule)Tha60290022ModulePrbsNew(self);
    }

static AtModule ModulePtpCreate(AtDevice self)
    {
    if (Tha60290022DevicePtpSupported(self))
        return (AtModule)Tha60290022ModulePtpNew(self);

    return NULL;
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    uint32 phyModule = moduleId;

    /* Public modules */
    if (moduleId == cAtModulePrbs)  return ModulePrbsCreate(self);
    if (moduleId == cAtModuleEth)   return (AtModule)Tha60290022ModuleEthNew(self);
    if (moduleId == cAtModuleSdh)   return ModuleSdhCreate(self);
    if (moduleId == cAtModulePdh)   return ModulePdhCreate(self);
    if (moduleId == cAtModuleXc)    return (AtModule)Tha60290022ModuleXcNew(self);
    if (moduleId == cAtModulePw)    return (AtModule)Tha60290022ModulePwNew(self);
    if (moduleId == cAtModuleRam)   return (AtModule)Tha60290022ModuleRamNew(self);
    if (moduleId == cAtModuleSur)   return (AtModule)Tha60290022ModuleSurNew(self);
    if (moduleId == cAtModuleAps)   return (AtModule)Tha60290022ModuleApsNew(self);
    if (moduleId == cAtModuleBer)   return (AtModule)Tha60290022ModuleBerNew(self);
    if (moduleId == cAtModulePktAnalyzer)   return (AtModule)Tha60290022ModulePktAnalyzerNew(self);
    if (moduleId == cAtModuleClock)   return (AtModule)ModuleClockCreate(self);
    if (moduleId == cAtModulePtp)   return ModulePtpCreate(self);

    /* Private modules */
    if (phyModule == cThaModuleOcn)   return Tha60290022ModuleOcnNew(self);
    if (phyModule == cThaModuleMap)   return Tha60290022ModuleMapNew(self);
    if (phyModule == cThaModuleDemap) return Tha60290022ModuleDemapNew(self);
    if (phyModule == cThaModuleCdr)   return ModuleCdrCreate(self);
    if (phyModule == cThaModulePda)   return Tha60290022ModulePdaNew(self);
    if (phyModule == cThaModulePwe)   return Tha60290022ModulePweNew(self);
    if (phyModule == cThaModuleCos)   return Tha60290022ModuleCosNew(self);
    if (phyModule == cThaModuleCla)   return ModuleClaCreate(self);
    if (phyModule == cThaModulePoh)   return ModulePohCreate(self);
    if (phyModule == cThaModulePmc)   return ModulePmcCreate(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleXc,
                                                 cAtModuleSdh,
                                                 cAtModuleSur,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleBer,
                                                 cAtModulePrbs,
                                                 cAtModuleClock,
                                                 cAtModuleAps,
                                                 cAtModulePtp};
    /*PTP module is placed at then end this array to avoid that its module is NULL will
     * make iterator of device modules miss modules behind the PTP module*/

    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleValue = (uint32)moduleId;

    if (moduleValue == cAtModulePtp)
        return cAtTrue;

    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static eBool HasUpsrEthInterrupt(Tha60290021Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool IsRunningOnVu13P(Tha60290021Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ShouldSwitchToPmc(ThaDevice self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x7, 0x0);
    ThaVersionReader versionReader = ThaDeviceVersionReader((AtDevice)self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 DefaultCounterModule(ThaDevice self, uint32 moduleId)
    {
    if (ShouldSwitchToPmc(self))
        return cThaModulePmc;

    return m_ThaDeviceMethods->DefaultCounterModule(self, moduleId);
    }

static eBool ShouldFlushRange(ThaDevice self, uint32 startAddress, uint32 stopAddress)
    {
    if (Tha60290022DeviceCapacity15G((AtDevice)self))
        {
        /* CDR last two slices are not available */
        if (((startAddress >= 0x0D80000) && (stopAddress <= 0x0DBFFFF)) ||
            ((startAddress >= 0x0DC0000) && (stopAddress <= 0x0DFFFFF)))
            return cAtFalse;
        return cAtTrue;
        }

    return m_ThaDeviceMethods->ShouldFlushRange(self, startAddress, stopAddress);
    }

static eAtRet HwDdrQdrResetValueSet(Tha60150011Device self, uint32 value)
    {
    AtDevice device = (AtDevice)self;
    AtIpCore core = AtDeviceIpCoreGet(device, 0);
    uint32 regAddr = Tha60290021DeviceTopRegisterWithLocalAddress(device, cAf6Reg_o_reset_ctr_Base);
    uint32 regVal = AtIpCoreRead(core, regAddr);

    if (AtDeviceIsSimulated(device))
        return cAtOk;

    if (regVal != 0xFF)
        AtDeviceLog(device, cAtLogLevelWarning, AtSourceLocation, "address 0x%x value = 0x%x\r\n", regAddr, regVal);

    AtIpCoreWrite(core, regAddr, value);

    return cAtOk;
    }

static eAtRet HwSemClockProvide(Tha60150011Device self)
    {
    AtDevice device = (AtDevice)self;
    AtIpCore core = AtDeviceIpCoreGet(device, 0);
    uint32 regAddr = Tha60290021DeviceTopRegisterWithLocalAddress(device, cAf6Reg_o_control12_Base);
    uint32 regVal = AtIpCoreRead(core, regAddr);
    mRegFieldSet(regVal, cAf6_o_control12_conf_enb_clk_sem_, 1);
    AtIpCoreWrite(core, regAddr, regVal);
    return cAtOk;
    }

static eAtRet SemClockProvide(Tha60150011Device self)
    {
    if (Tha60290022DeviceHasSoftResetForSem((AtDevice)self))
        HwDdrQdrResetValueSet(self, 0xFF);

    if (AtDeviceSemControllerIsSupported((AtDevice)self))
        return HwSemClockProvide(self);

    return cAtOk;
    }

static void HwFlushCdrSlice(Tha60210051Device self, uint8 slice)
    {
    ThaDevice device = (ThaDevice)self;
    uint32 offset = 0x40000UL * slice;

    ThaDeviceMemoryFlush(device, 0xc01000 + offset, 0xc0102f + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0xc01800 + offset, 0xc01fff + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0xc20800 + offset, 0xc20fff + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0xc21000 + offset, 0xc217ff + offset, 0x0);
    }

static void HwFlushPmc(Tha60210051Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryFlush(device, 0x1E60000, 0x1E629FF, 0x0);
    }

static void HwFlush(Tha60150011Device self)
    {
    m_Tha60150011DeviceMethods->HwFlush(self);
    ThaModuleHardSurHwFlush((ThaModuleHardSur)AtDeviceModuleGet((AtDevice)self, cAtModuleSur));
    }

static eBool SemControllerIsSupported(AtDevice self)
    {
    return Tha60290022DeviceSemIsSupported(self);
    }

static uint32 ExpectedFrequencyOf100FxSerdes(Tha60290021Device self)
    {
    if (Tha60290022Device100FxSerdesClock31Dot25MhzIsSupported((AtDevice)self))
        return 31250000;

    return     m_Tha60290021DeviceMethods->ExpectedFrequencyOf100FxSerdes(self);
    }

static eBool DeviceCapacity15G(AtDevice self)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader(self);
    uint32 builtNumber = ThaVersionReaderHardwareBuiltNumber(versionReader);
    uint32 numSlices;

    mFieldGet(builtNumber, cBit15_12, 12, uint32, &numSlices);
    return (numSlices == 6) ? cAtTrue : cAtFalse;
    }

static eBool HasKByteFeatureOnly(AtDevice self)
    {
    uint32 versionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x6F45);
    ThaVersionReader versionReader = ThaDeviceVersionReader(self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion == versionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool HwLogicOptimized(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x3, 0x0, 0x0);
    }

static eBool DccV2IsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x3, 0x9, 0x0000);
    }

static eBool  ShouldSemCheck(Tha60150011Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtSemController SemControllerObjectCreate(AtDevice self, uint32 semId)
    {
    return Tha60290022SemControllerNew(self, semId);
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret;

    ret = m_AtDeviceMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (!AtDeviceInAccessible(self))
        ret = Tha60210011DeviceMoveAllSemControllersToState(self, cAtSemControllerAlarmObservation);

    return ret;
    }

static eBool ShouldMoveSemToObservationDuringHwReset(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldApplyNewReset(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool DeviceV3OptimizationIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x5, 0x0, 0x0000);
    }

static eBool Pmc1PageIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x4, 0x0, 0x0000);
    }

static eBool HoTxG1OverWriteIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x3, 0x9, 0x8845);
    }

static eBool DccV4dot1EnableRegAddressIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x4, 0x1, 0x0000);
    }

static eBool ClockSquelchingIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x4, 0x1, 0x8145);
    }

static eBool ClockEthSquelchingIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x4, 0x4, 0x0000);
    }

static eBool EthTxIpgIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x4, 0x4, 0x0000);
    }

static eBool DccTxFcsMsbIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x4, 0x3, 0x0000);
    }

static eBool DccSgmiiTxIpg31ByteIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x6, 0x0, 0x0000);
    }

static eBool Eth100MFxLfRfSupported(AtDevice self)
    {
    if (ShouldOpenFeatureFromVersion(self, 0x5, 0x3, 0x0000))
        return cAtTrue;

    if (ShouldOpenFeatureFromVersion(self, 0x5, 0x0, 0x0000))
        return cAtFalse;

    if (ShouldOpenFeatureFromVersion(self, 0x4, 0x6, 0x0000))
        return cAtTrue;

    return cAtFalse;
    }

static eBool DccHdlcTransmissionBitOrderPerChannelIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x5, 0x3, 0x0000);
    }

static eBool Sts192cPrbsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x5, 0x5, 0x0000);
    }

static eBool SemIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x7, 0x1, 0x0000);
    }

static eBool OcnSeparatedSectionAndLineDccIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x5, 0x5, 0x0000);
    }

static eBool EthPortFlowControlWaterMarkIsSupported(AtDevice self)
    {
    if (ShouldOpenFeatureFromVersion(self, 0x5, 0x4, 0x0000))
        return cAtTrue;

    if (ShouldOpenFeatureFromVersion(self, 0x5, 0x0, 0x0000))
        return cAtFalse;

    if (ShouldOpenFeatureFromVersion(self, 0x4, 0x6, 0x0000))
        return cAtTrue;

    return cAtFalse;
    }

static eBool ClockEthSquelchingSelectOnCoreOnlyIsSupported(AtDevice self)
    {
    if (ShouldOpenFeatureFromVersion(self, 0x5, 0x4, 0x0000))
        return cAtTrue;

    if (ShouldOpenFeatureFromVersion(self, 0x5, 0x0, 0x0000))
        return cAtFalse;

    if (ShouldOpenFeatureFromVersion(self, 0x4, 0x6, 0x0000))
        return cAtTrue;

    return cAtFalse;
    }

static eBool Sts192cTxG1OverrideIsNotUsed(AtDevice self)
    {
    if (ShouldOpenFeatureFromVersion(self, 0x5, 0x7, 0x0000))
        return cAtFalse;

    if (ShouldOpenFeatureFromVersion(self, 0x5, 0x6, 0x0000))
        return cAtTrue;

    return cAtFalse;
    }

static eBool Sts192cPrbsInvertionIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x5, 0x6, 0x0000);
    }

static uint32 DcrAcrTxShapperSupportedVersion(AtDevice self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0x0000);
    }

static eBool ClockExtractorMonitorPpmIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x5, 0x7, 0x0000);
    }

static eBool TohSonetSdhModeIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x5, 0x8, 0x0000);
    }

static eBool CesopAutoRxMBitConfigurableIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x5, 0x8, 0x0000);
    }

static eBool SeparateLbitAndLopsPktReplacementSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x5, 0x8, 0x0000);
    }

static eBool FaceplateEthCounter32bitIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x5, 0xA, 0x0000);
    }

static eBool Eth100FxSerdesClock31Dot25MhzIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x6, 0x0, 0x0000);
    }

static eBool DccEthMaxLengthConfigurationIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x6, 0x0, 0x0000);
    }

static uint8 MapDemapNumLoSlices(AtDevice self)
    {
    if (Tha60290022DeviceCapacity15G(self))
        return 6;

    return 8;
    }

static eBool HasStandardClearTime(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x3, 0x5, 0x8045);
    }

static eBool IsNewBerInterrupt(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x5, 0x0, 0x0000);
    }

static eBool FaceplatSerdesEthHasK30_7Feature(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x2, 0x00);
    }

static uint32 FaceplatSerdesEthStartVersionSupportTxAlarmForcing(AtDevice self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x0, 0x0);
    }

static eBool FaceplatSerdesEthExcessiveErrorRatioIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x4, 0x0);
    }

static eBool EthHasSgmiiDiagnosticLogic(AtDevice self)
    {
    return !ShouldOpenFeatureFromVersion(self, 0x3, 0x3, 0x0);
    }

static eBool SgmiiSohEthUseNewMac(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x5, 0x0);
    }

static uint32 NumberOfMapLoPwPerSlice(AtDevice self)
    {
    AtUnused(self);
    return 1344;
    }

static eBool NewTuVcLoopbackIsReady(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x7, 0x8145);
    }

static eBool Sts192cIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x3, 0x8, 0x0000);
    }

static eBool PdaIsNewOc192cOffset(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x5, 0x8, 0x0);
    }

static eBool PdaNumJitterBufferBlocksIncreased(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x7, 0x0);
    }

static uint32 PdhStartVersionSupportPrm(AtDevice self)
    {
    if (Tha60290022DeviceRunOnVu9P(self))
        return cInvalidUint32;

    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x7, 0x0);
    }

static uint32 PdhStartVersionSupportDe1LomfConsequentialAction(AtDevice self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x0, 0x0);
    }

static uint32 PdhStartVersionSupportDe1IdleCodeInsertion(AtDevice self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x3, 0x3, 0x0);
    }

static eBool PdhRxHw3BitFeacSignalIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x3, 0x9, 0x0000);
    }

static eBool FaceplateSerdesPmaFarEndLoopbackSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x2, 0x0000);
    }

static eBool KByteSerdesIsControllable(AtDevice self)
    {
    return !ShouldOpenFeatureFromVersion(self, 0x3, 0x3, 0x0000);
    }

static eBool SgmiiSerdesShouldUseMdioLocalLoopback(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x3, 0x0000);
    }

static eBool PohHasCpujnreqen(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x3, 0x9, 0x8945);
    }

static uint32 PohStartVersionSupportSeparateRdiErdiS(AtDevice self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x3, 0x0);
    }

static eBool PrbsBertOptimizeIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x4, 0x8245);
    }

static eBool PwDccKbyteInterruptIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x5, 0x9, 0x0000);
    }

static eBool FaceplateFramerLocalLoopbackSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x2, 0x0000);
    }

static eBool SurFailureHoldOnTimerConfigurable(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x0, 0x0000);
    }

static eBool FaceplateSerdesStm1SamplingFromStm4(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x2, 0x3, 0x6345);
    }

static eBool FaceplateSerdesHasNewParams(Tha60290022Device self)
    {
    return ShouldOpenFeatureFromVersion((AtDevice)self, 0x3, 0x3, 0x0000);
    }

static eBool DccHdlcHasPacketCounterDuringFifoFull(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x6, 0x2, 0x0000);
    }

static eBool FacePlateSerdesDataMaskOnResetConfigurable(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x6, 0x2, 0x0000);
    }

static eBool ModuleBerMeasureTimeEngineIsSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x3, 0x9, 0x8945);
    }

static eBool PtpSupported(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x7, 0x0, 0x0000);
    }

static eBool HasApsWtr3SecondResolution(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x7, 0x1, 0x0000);
    }

static eBool HasSoftResetForSem(AtDevice self)
    {
    return ShouldOpenFeatureFromVersion(self, 0x7, 0x2, 0x0000);
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtUnused(self);
    return Tha60290022IntrControllerNew(core);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(isForcedToUseTxShapper);
    }

static AtPowerSupplySensor PowerSupplySensorObjectCreate(AtDevice self)
    {
    return Tha60290022PowerSupplySensorNew(self);
    }

static void MethodsInit(Tha60290022Device self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DeviceCapacity15G);
        mMethodOverride(m_methods, HasKByteFeatureOnly);
        mMethodOverride(m_methods, HwLogicOptimized);
        mMethodOverride(m_methods, DccV2IsSupported);
        mMethodOverride(m_methods, DeviceV3OptimizationIsSupported);
        mMethodOverride(m_methods, Pmc1PageIsSupported);
        mMethodOverride(m_methods, HoTxG1OverWriteIsSupported);
        mMethodOverride(m_methods, DccV4dot1EnableRegAddressIsSupported);
        mMethodOverride(m_methods, ClockSquelchingIsSupported);
        mMethodOverride(m_methods, ClockEthSquelchingIsSupported);
        mMethodOverride(m_methods, EthTxIpgIsSupported);
        mMethodOverride(m_methods, DccTxFcsMsbIsSupported);
        mMethodOverride(m_methods, DccSgmiiTxIpg31ByteIsSupported);
        mMethodOverride(m_methods, Eth100MFxLfRfSupported);
        mMethodOverride(m_methods, DccHdlcTransmissionBitOrderPerChannelIsSupported);
        mMethodOverride(m_methods, Sts192cPrbsSupported);
        mMethodOverride(m_methods, SemIsSupported);
        mMethodOverride(m_methods, OcnSeparatedSectionAndLineDccIsSupported);
        mMethodOverride(m_methods, EthPortFlowControlWaterMarkIsSupported);
        mMethodOverride(m_methods, ClockEthSquelchingSelectOnCoreOnlyIsSupported);
        mMethodOverride(m_methods, Sts192cTxG1OverrideIsNotUsed);
        mMethodOverride(m_methods, Sts192cPrbsInvertionIsSupported);
        mMethodOverride(m_methods, DcrAcrTxShapperSupportedVersion);
        mMethodOverride(m_methods, ClockExtractorMonitorPpmIsSupported);
        mMethodOverride(m_methods, TohSonetSdhModeIsSupported);
        mMethodOverride(m_methods, CesopAutoRxMBitConfigurableIsSupported);
        mMethodOverride(m_methods, SeparateLbitAndLopsPktReplacementSupported);
        mMethodOverride(m_methods, FaceplateEthCounter32bitIsSupported);
        mMethodOverride(m_methods, Eth100FxSerdesClock31Dot25MhzIsSupported);
        mMethodOverride(m_methods, DccEthMaxLengthConfigurationIsSupported);
        mMethodOverride(m_methods, MapDemapNumLoSlices);
        mMethodOverride(m_methods, HasStandardClearTime);
        mMethodOverride(m_methods, IsNewBerInterrupt);
        mMethodOverride(m_methods, FaceplatSerdesEthHasK30_7Feature);
        mMethodOverride(m_methods, FaceplatSerdesEthStartVersionSupportTxAlarmForcing);
        mMethodOverride(m_methods, FaceplatSerdesEthExcessiveErrorRatioIsSupported);
        mMethodOverride(m_methods, EthHasSgmiiDiagnosticLogic);
        mMethodOverride(m_methods, SgmiiSohEthUseNewMac);
        mMethodOverride(m_methods, NumberOfMapLoPwPerSlice);
        mMethodOverride(m_methods, NewTuVcLoopbackIsReady);
        mMethodOverride(m_methods, Sts192cIsSupported);
        mMethodOverride(m_methods, PdaIsNewOc192cOffset);
        mMethodOverride(m_methods, PdaNumJitterBufferBlocksIncreased);
        mMethodOverride(m_methods, PdhStartVersionSupportPrm);
        mMethodOverride(m_methods, PdhStartVersionSupportDe1LomfConsequentialAction);
        mMethodOverride(m_methods, PdhStartVersionSupportDe1IdleCodeInsertion);
        mMethodOverride(m_methods, PdhRxHw3BitFeacSignalIsSupported);
        mMethodOverride(m_methods, FaceplateSerdesPmaFarEndLoopbackSupported);
        mMethodOverride(m_methods, KByteSerdesIsControllable);
        mMethodOverride(m_methods, SgmiiSerdesShouldUseMdioLocalLoopback);
        mMethodOverride(m_methods, PohHasCpujnreqen);
        mMethodOverride(m_methods, PohStartVersionSupportSeparateRdiErdiS);
        mMethodOverride(m_methods, PrbsBertOptimizeIsSupported);
        mMethodOverride(m_methods, PwDccKbyteInterruptIsSupported);
        mMethodOverride(m_methods, FaceplateFramerLocalLoopbackSupported);
        mMethodOverride(m_methods, SurFailureHoldOnTimerConfigurable);
        mMethodOverride(m_methods, FaceplateSerdesStm1SamplingFromStm4);
        mMethodOverride(m_methods, FaceplateSerdesHasNewParams);
        mMethodOverride(m_methods, DccHdlcHasPacketCounterDuringFifoFull);
        mMethodOverride(m_methods, FacePlateSerdesDataMaskOnResetConfigurable);
        mMethodOverride(m_methods, ModuleBerMeasureTimeEngineIsSupported);
        mMethodOverride(m_methods, PtpSupported);
        mMethodOverride(m_methods, HasApsWtr3SecondResolution);
        mMethodOverride(m_methods, HasSoftResetForSem);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtDevice self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideTha60150011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60150011DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, SemClockProvide);
        mMethodOverride(m_Tha60150011DeviceOverride, ShouldSemCheck);
        mMethodOverride(m_Tha60150011DeviceOverride, HwFlush);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, m_ThaDeviceMethods, sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, DefaultCounterModule);
        mMethodOverride(m_ThaDeviceOverride, ShouldFlushRange);
        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideTha60290021Device(AtDevice self)
    {
    Tha60290021Device device = (Tha60290021Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021DeviceOverride, m_Tha60290021DeviceMethods, sizeof(m_Tha60290021DeviceOverride));

        mMethodOverride(m_Tha60290021DeviceOverride, Refout8kCanDisplay);
        mMethodOverride(m_Tha60290021DeviceOverride, HasUpsrEthInterrupt);
        mMethodOverride(m_Tha60290021DeviceOverride, IsRunningOnVu13P);
        mMethodOverride(m_Tha60290021DeviceOverride, ExpectedFrequencyOf100FxSerdes);
        }

    mMethodsSet(device, &m_Tha60290021DeviceOverride);
    }

static void OverrideTha60210011Device(AtDevice self)
    {
    Tha60210011Device this = (Tha60210011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011DeviceMethods = mMethodsGet(this);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011DeviceOverride, m_Tha60210011DeviceMethods, sizeof(m_Tha60210011DeviceOverride));

        mMethodOverride(m_Tha60210011DeviceOverride, HasHoBus);
        mMethodOverride(m_Tha60210011DeviceOverride, NumUsedOc48Slice);
        mMethodOverride(m_Tha60210011DeviceOverride, PlaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, PrbsHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, ShouldMoveSemToObservationDuringHwReset);
        mMethodOverride(m_Tha60210011DeviceOverride, ShouldApplyNewReset);
        }

    mMethodsSet(this, &m_Tha60210011DeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    AtDevice object = (AtDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(m_AtDeviceOverride));

        mMethodOverride(m_AtDeviceOverride, SerdesManagerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, SemControllerIsSupported);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, SemControllerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, PowerSupplySensorObjectCreate);
        }

    mMethodsSet(object, &m_AtDeviceOverride);
    }

static void OverrideTha60210051Device(AtDevice self)
    {
    Tha60210051Device device = (Tha60210051Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210051DeviceOverride));

        mMethodOverride(m_Tha60210051DeviceOverride, HwFlushPdhSlice);
        mMethodOverride(m_Tha60210051DeviceOverride, HwFlushCdrSlice);
        mMethodOverride(m_Tha60210051DeviceOverride, HwFlushPmc);
        }

    mMethodsSet(device, &m_Tha60210051DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtObject(self);
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60150011Device(self);
    OverrideTha60210011Device(self);
    OverrideTha60210051Device(self);
    OverrideTha60290021Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022Device);
    }

AtDevice Tha60290022DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60290022DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022DeviceObjectInit(newDevice, driver, productCode);
    }

eBool Tha60290022DeviceRunOnVu9P(AtDevice self)
    {
    if (AtDevicePlatformGet(self) == 0x60290021)
        return cAtTrue;
    return cAtFalse;
    }

eBool Tha60290022DeviceShouldOpenFullCapacity(AtDevice self)
    {
    return Tha60290022DeviceRunOnVu9P(self) ? cAtFalse : cAtTrue;
    }

eBool Tha60290022DeviceCapacity15G(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->DeviceCapacity15G(self);

    return cAtFalse;
    }

eBool Tha60290022DeviceHasKByteFeatureOnly(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->HasKByteFeatureOnly(self);
    return cAtFalse;
    }

eBool Tha60290022DeviceHwLogicOptimized(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->HwLogicOptimized(self);
    return cAtFalse;
    }

eBool Tha60290022DeviceDccV2IsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->DccV2IsSupported(self);
    }

eBool Tha60290022DevicePmc1PageIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->Pmc1PageIsSupported(self);
    }

eBool Tha60290022DeviceHoTxG1OverWriteIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->HoTxG1OverWriteIsSupported(self);
    }

eBool Tha60290022DeviceDccTxFcsMsbIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->DccTxFcsMsbIsSupported(self);
    }

eBool Tha60290022DeviceDccSgmiiTxIpg31ByteIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->DccSgmiiTxIpg31ByteIsSupported(self);
    }

eBool Tha60290022DeviceV3OptimizationIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->DeviceV3OptimizationIsSupported(self);
    }

eBool Tha60290022DeviceDccHdlcTransmissionBitOrderPerChannelIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->DccHdlcTransmissionBitOrderPerChannelIsSupported(self);
    }

eBool Tha60290022DeviceOcnSeparatedSectionAndLineDccIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->OcnSeparatedSectionAndLineDccIsSupported(self);
    }

eBool Tha60290022DeviceDccV4dot1EnableRegAddressIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->DccV4dot1EnableRegAddressIsSupported(self);
    }

eBool Tha60290022DeviceClockSquelchingIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->ClockSquelchingIsSupported(self);
    }

eBool Tha60290022DeviceClockEthSquelchingIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->ClockEthSquelchingIsSupported(self);
    }

eBool Tha60290022DeviceTxIpgIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->EthTxIpgIsSupported(self);
    }

eBool Tha60290022DeviceEthPortFlowControlWaterMarkIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->EthPortFlowControlWaterMarkIsSupported(self);
    }

eBool Tha60290022DeviceClockEthSquelchingSelectOnCoreOnlyIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->ClockEthSquelchingSelectOnCoreOnlyIsSupported(self);
    }

eBool Tha60290022DeviceEth100MFxLfRfSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->Eth100MFxLfRfSupported(self);
    }

eBool Tha60290022DeviceSts192cPrbsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->Sts192cPrbsSupported(self);
    }

eBool Tha60290022DeviceSemIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->SemIsSupported(self);
    }

eBool Tha60290022DeviceSts192cTxG1OverrideIsNotUsed(AtDevice self)
    {
    return mMethodsGet(mThis(self))->Sts192cTxG1OverrideIsNotUsed(self);
    }

eBool Tha60290022DeviceSts192cPrbsInvertionIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->Sts192cPrbsInvertionIsSupported(self);
    }

uint32 Tha60290022DeviceDcrAcrTxShapperSupportedVersion(AtDevice self)
    {
    if (Tha60290022DeviceTxShapperIsEnabled(self))
        return 0x0;

    return mMethodsGet(mThis(self))->DcrAcrTxShapperSupportedVersion(self);
    }

eBool Tha60290022DeviceClockExtractorMonitorPpmIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->ClockExtractorMonitorPpmIsSupported(self);
    }

eBool Tha60290022DeviceTohSonetSdhModeIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->TohSonetSdhModeIsSupported(self);
    }

eBool Tha60290022DeviceCesopAutoRxMBitConfigurableIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->CesopAutoRxMBitConfigurableIsSupported(self);
    }

eBool Tha60290022DeviceSeparateLbitAndLopsPktReplacementSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->SeparateLbitAndLopsPktReplacementSupported(self);
    }

eAtRet Tha60290022DeviceTxShapperEnable(AtDevice self, eBool enable)
    {
    if (self)
        {
        mThis(self)->isForcedToUseTxShapper = enable;
        return cAtOk;
        }
    return cAtErrorNullPointer;
    }

eBool Tha60290022DeviceTxShapperIsEnabled(AtDevice self)
    {
    if (self)
        return mThis(self)->isForcedToUseTxShapper;
    return cAtFalse;
    }

eBool Tha60290022DeviceFaceplateEthCounter32bitIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->FaceplateEthCounter32bitIsSupported(self);
    }

eBool Tha60290022Device100FxSerdesClock31Dot25MhzIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->Eth100FxSerdesClock31Dot25MhzIsSupported(self);
    }

eBool Tha60290022DeviceDccEthMaxLengthConfigurationIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->DccEthMaxLengthConfigurationIsSupported(self);
    }

uint8 Tha60290022DeviceMapDemapNumLoSlices(AtDevice self)
    {
    return mMethodsGet(mThis(self))->MapDemapNumLoSlices(self);
    }

eBool Tha60290022DeviceHasStandardClearTime(AtDevice self)
    {
    return mMethodsGet(mThis(self))->HasStandardClearTime(self);
    }

eBool Tha60290022DeviceIsNewBerInterrupt(AtDevice self)
    {
    return mMethodsGet(mThis(self))->IsNewBerInterrupt(self);
    }

eBool Tha60290022DeviceFaceplatSerdesEthHasK30_7Feature(AtDevice self)
    {
    return mMethodsGet(mThis(self))->FaceplatSerdesEthHasK30_7Feature(self);
    }

uint32 Tha60290022DeviceFaceplatSerdesEthStartVersionSupportTxAlarmForcing(AtDevice self)
    {
    return mMethodsGet(mThis(self))->FaceplatSerdesEthStartVersionSupportTxAlarmForcing(self);
    }

eBool Tha60290022DeviceFaceplatSerdesEthExcessiveErrorRatioIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->FaceplatSerdesEthExcessiveErrorRatioIsSupported(self);
    }

eBool Tha60290022DeviceEthHasSgmiiDiagnosticLogic(AtDevice self)
    {
    return mMethodsGet(mThis(self))->EthHasSgmiiDiagnosticLogic(self);
    }

eBool Tha60290022DeviceSgmiiSohEthUseNewMac(AtDevice self)
    {
    return mMethodsGet(mThis(self))->SgmiiSohEthUseNewMac(self);
    }

uint32 Tha60290022DeviceNumberOfMapLoPwPerSlice(AtDevice self)
    {
    return mMethodsGet(mThis(self))->NumberOfMapLoPwPerSlice(self);
    }

eBool Tha60290022DeviceNewTuVcLoopbackIsReady(AtDevice self)
    {
    return mMethodsGet(mThis(self))->NewTuVcLoopbackIsReady(self);
    }

eBool Tha60290022DeviceSts192cIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->Sts192cIsSupported(self);
    }

eBool Tha60290022DevicePdaIsNewOc192cOffset(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PdaIsNewOc192cOffset(self);
    }

eBool Tha60290022DevicePdaNumJitterBufferBlocksIncreased(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PdaNumJitterBufferBlocksIncreased(self);
    }

uint32 Tha60290022DevicePdhStartVersionSupportPrm(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PdhStartVersionSupportPrm(self);
    }

uint32 Tha60290022DevicePdhStartVersionSupportDe1LomfConsequentialAction(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PdhStartVersionSupportDe1LomfConsequentialAction(self);
    }

uint32 Tha60290022DevicePdhStartVersionSupportDe1IdleCodeInsertion(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PdhStartVersionSupportDe1IdleCodeInsertion(self);
    }

eBool Tha60290022DevicePdhRxHw3BitFeacSignalIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PdhRxHw3BitFeacSignalIsSupported(self);
    }

eBool Tha60290022DeviceFaceplateSerdesPmaFarEndLoopbackSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->FaceplateSerdesPmaFarEndLoopbackSupported(self);
    }

eBool Tha60290022DeviceKByteSerdesIsControllable(AtDevice self)
    {
    return mMethodsGet(mThis(self))->KByteSerdesIsControllable(self);
    }

eBool Tha60290022DeviceSgmiiSerdesShouldUseMdioLocalLoopback(AtDevice self)
    {
    return mMethodsGet(mThis(self))->SgmiiSerdesShouldUseMdioLocalLoopback(self);
    }

eBool Tha60290022DevicePohHasCpujnreqen(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PohHasCpujnreqen(self);
    }

uint32 Tha60290022DevicePohStartVersionSupportSeparateRdiErdiS(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PohStartVersionSupportSeparateRdiErdiS(self);
    }

eBool Tha60290022DevicePrbsBertOptimizeIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PrbsBertOptimizeIsSupported(self);
    }

eBool Tha60290022DevicePwDccKbyteInterruptIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PwDccKbyteInterruptIsSupported(self);
    }

eBool Tha60290022DeviceFaceplateFramerLocalLoopbackSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->FaceplateFramerLocalLoopbackSupported(self);
    }

eBool Tha60290022DeviceSurFailureHoldOnTimerConfigurable(AtDevice self)
    {
    return mMethodsGet(mThis(self))->SurFailureHoldOnTimerConfigurable(self);
    }

eBool Tha60290022DeviceFaceplateSerdesStm1SamplingFromStm4(AtDevice self)
    {
    return mMethodsGet(mThis(self))->FaceplateSerdesStm1SamplingFromStm4(self);
    }

eBool Tha60290022DeviceFaceplateSerdesHasNewParams(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->FaceplateSerdesHasNewParams(mThis(self));
    return cAtFalse;
    }

eBool Tha60290022DeviceDccHdlcHasPacketCounterDuringFifoFull(AtDevice self)
    {
    return mMethodsGet(mThis(self))->DccHdlcHasPacketCounterDuringFifoFull(self);
    }

eBool Tha60290022DeviceFacePlateSerdesDataMaskOnResetConfigurable(AtDevice self)
    {
    return mMethodsGet(mThis(self))->FacePlateSerdesDataMaskOnResetConfigurable(self);
    }

eBool Tha60290022DeviceModuleBerMeasureTimeEngineIsSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->ModuleBerMeasureTimeEngineIsSupported(self);
    }

eBool Tha60290022DevicePtpSupported(AtDevice self)
    {
    return mMethodsGet(mThis(self))->PtpSupported(self);
    }

eBool Tha60290022DeviceHasApsWtr3SecondResolution(AtDevice self)
    {
    return mMethodsGet(mThis(self))->HasApsWtr3SecondResolution(self);
    }

eBool Tha60290022DeviceHasSoftResetForSem(AtDevice self)
    {
    return mMethodsGet(mThis(self))->HasSoftResetForSem(self);
    }

eBool Tha60290022DeviceNeedMapConfigureWhenBindingVc4_64cToPseudowire(AtDevice self)
    {
    static const uint32 cNumVersions = 3;
    static uint32 versions[][3] = {{6, 6, 0x8145},
                                   {7, 4, 0x8045},
                                   {7, 6, 0x8045}};
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(self));
    uint32 i;

    /* Only few versions above require SW bugfix */
    for (i = 0; i < cNumVersions; i++)
        {
        uint32 supportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(versions[i][0], versions[i][1], versions[i][2]);
        if (supportedVersion == currentVersion)
            return cAtTrue;
        }

    return cAtFalse;
    }

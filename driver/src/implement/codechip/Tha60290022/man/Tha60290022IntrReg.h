/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6NC0022_RD_INTR_H_
#define _AF6_REG_AF6NC0022_RD_INTR_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Status
Reg Addr   : 0x00_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicate global interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_interrupt_status_Base                                                          0x000002

/*--------------------------------------
BitField Name: ExtPinIntStatus
BitField Type: RO
BitField Desc: External Pin Interrupt Status  1: Set  0: Clear
BitField Bits: [31]
--------------------------------------*/
#define cAf6_global_interrupt_status_ExtPinIntStatus_Mask                                               cBit31
#define cAf6_global_interrupt_status_ExtPinIntStatus_Shift                                                  31

/*--------------------------------------
BitField Name: ExtOrIntStatus
BitField Type: RO
BitField Desc: External OR Interrupt Status  1: Set  0: Clear
BitField Bits: [30]
--------------------------------------*/
#define cAf6_global_interrupt_status_ExtOrIntStatus_Mask                                                cBit30
#define cAf6_global_interrupt_status_ExtOrIntStatus_Shift                                                   30

/*--------------------------------------
BitField Name: ReserveIntStatus
BitField Type: RO
BitField Desc: Reserve Interrupt Status  1: Set  0: Clear
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_global_interrupt_status_ReserveIntStatus_Mask                                           cBit29_20
#define cAf6_global_interrupt_status_ReserveIntStatus_Shift                                                 20

/*--------------------------------------
BitField Name: DccKIntStatus
BitField Type: RO
BitField Desc: DCCK Interrupt Status  1: Set  0: Clear
BitField Bits: [19]
--------------------------------------*/
#define cAf6_global_interrupt_status_DccKIntStatus_Mask                                                 cBit19
#define cAf6_global_interrupt_status_DccKIntStatus_Shift                                                    19

/*--------------------------------------
BitField Name: Mac40gIntStatus
BitField Type: RO
BitField Desc: Two Mac40G Interrupt Status  1: Set  0: Clear
BitField Bits: [18:17]
--------------------------------------*/
#define cAf6_global_interrupt_status_Mac40gIntStatus_Mask                                            cBit18_17
#define cAf6_global_interrupt_status_Mac40gIntStatus_Shift                                                  17

/*--------------------------------------
BitField Name: UpsrIntStatus
BitField Type: RO
BitField Desc: UPSR Interrupt Status  1: Set  0: Clear
BitField Bits: [16]
--------------------------------------*/
#define cAf6_global_interrupt_status_UpsrIntStatus_Mask                                                 cBit16
#define cAf6_global_interrupt_status_UpsrIntStatus_Shift                                                    16

/*--------------------------------------
BitField Name: TenGeIntStatus
BitField Type: RO
BitField Desc: Chip SEM Interrupt Status  1: Set  0: Clear
BitField Bits: [15]
--------------------------------------*/
#define cAf6_global_interrupt_status_TenGeIntStatus_Mask                                                cBit15
#define cAf6_global_interrupt_status_TenGeIntStatus_Shift                                                   15

/*--------------------------------------
BitField Name: GeIntStatus
BitField Type: RO
BitField Desc: Chip SEM Interrupt Status  1: Set  0: Clear
BitField Bits: [14]
--------------------------------------*/
#define cAf6_global_interrupt_status_GeIntStatus_Mask                                                   cBit14
#define cAf6_global_interrupt_status_GeIntStatus_Shift                                                      14

/*--------------------------------------
BitField Name: GlobalPmc
BitField Type: RO
BitField Desc: Global PMC 2 Page Interrupt Status  1: Set  0: Clear
BitField Bits: [13]
--------------------------------------*/
#define cAf6_global_interrupt_status_GlobalPmc_Mask                                                     cBit13
#define cAf6_global_interrupt_status_GlobalPmc_Shift                                                        13

/*--------------------------------------
BitField Name: SemIntStatus
BitField Type: RO
BitField Desc: Chip SEM Interrupt Status  1: Set  0: Clear
BitField Bits: [12]
--------------------------------------*/
#define cAf6_global_interrupt_status_SemIntStatus_Mask                                                  cBit12
#define cAf6_global_interrupt_status_SemIntStatus_Shift                                                     12

/*--------------------------------------
BitField Name: PtpIntStatus
BitField Type: RO
BitField Desc: PTP Interrupt Status  1: Set  0: Clear
BitField Bits: [11]
--------------------------------------*/
#define cAf6_global_interrupt_status_PtpIntStatus_Mask                                                  cBit11
#define cAf6_global_interrupt_status_PtpIntStatus_Shift                                                     11

/*--------------------------------------
BitField Name: LoCdrIntStatus
BitField Type: RO
BitField Desc: Low Order CDR Interrupt Status  1: Set  0: Clear
BitField Bits: [10]
--------------------------------------*/
#define cAf6_global_interrupt_status_LoCdrIntStatus_Mask                                                cBit10
#define cAf6_global_interrupt_status_LoCdrIntStatus_Shift                                                   10

/*--------------------------------------
BitField Name: MdlIntStatus
BitField Type: RO
BitField Desc: MDL Interrupt Status  1: Set  0: Clear
BitField Bits: [9]
--------------------------------------*/
#define cAf6_global_interrupt_status_MdlIntStatus_Mask                                                   cBit9
#define cAf6_global_interrupt_status_MdlIntStatus_Shift                                                      9

/*--------------------------------------
BitField Name: PrmIntStatus
BitField Type: RO
BitField Desc: PRM Interrupt Status  1: Set  0: Clear
BitField Bits: [8]
--------------------------------------*/
#define cAf6_global_interrupt_status_PrmIntStatus_Mask                                                   cBit8
#define cAf6_global_interrupt_status_PrmIntStatus_Shift                                                      8

/*--------------------------------------
BitField Name: PmIntStatus
BitField Type: RO
BitField Desc: PM Interrupt Status  1: Set  0: Clear
BitField Bits: [7]
--------------------------------------*/
#define cAf6_global_interrupt_status_PmIntStatus_Mask                                                    cBit7
#define cAf6_global_interrupt_status_PmIntStatus_Shift                                                       7

/*--------------------------------------
BitField Name: FmIntStatus
BitField Type: RO
BitField Desc: FM Interrupt Status  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_interrupt_status_FmIntStatus_Mask                                                    cBit6
#define cAf6_global_interrupt_status_FmIntStatus_Shift                                                       6

/*--------------------------------------
BitField Name: TFI5IntStatus
BitField Type: RO
BitField Desc: TFI5 Interrupt Status  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_interrupt_status_TFI5IntStatus_Mask                                                  cBit5
#define cAf6_global_interrupt_status_TFI5IntStatus_Shift                                                     5

/*--------------------------------------
BitField Name: STSIntStatus
BitField Type: RO
BitField Desc: STS Interrupt Status  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_interrupt_status_STSIntStatus_Mask                                                   cBit4
#define cAf6_global_interrupt_status_STSIntStatus_Shift                                                      4

/*--------------------------------------
BitField Name: VTIntStatus
BitField Type: RO
BitField Desc: VT Interrupt Status  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_interrupt_status_VTIntStatus_Mask                                                    cBit3
#define cAf6_global_interrupt_status_VTIntStatus_Shift                                                       3

/*--------------------------------------
BitField Name: DE3IntStatus
BitField Type: RO
BitField Desc: DS3/E3 Interrupt Status  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_interrupt_status_DE3IntStatus_Mask                                                   cBit2
#define cAf6_global_interrupt_status_DE3IntStatus_Shift                                                      2

/*--------------------------------------
BitField Name: DE1IntStatus
BitField Type: RO
BitField Desc: DS1/E1 Interrupt Status  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_interrupt_status_DE1IntStatus_Mask                                                   cBit1
#define cAf6_global_interrupt_status_DE1IntStatus_Shift                                                      1

/*--------------------------------------
BitField Name: PWEIntStatus
BitField Type: RO
BitField Desc: PWE Interrupt Status  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_interrupt_status_PWEIntStatus_Mask                                                   cBit0
#define cAf6_global_interrupt_status_PWEIntStatus_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Mask Enable
Reg Addr   : 0x00_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures global interrupt mask enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_interrupt_mask_enable_Base                                                     0x000003
#define cAf6Reg_global_interrupt_mask_enable_WidthVal                                                       32

/*--------------------------------------
BitField Name: DccKIntEnable
BitField Type: RO
BitField Desc: DccK Interrupt Enable  1: Set  0: Clear
BitField Bits: [19]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_DccKIntEnable_Mask                                            cBit19
#define cAf6_global_interrupt_mask_enable_DccKIntEnable_Shift                                               19

/*--------------------------------------
BitField Name: Mac40gIntEnable
BitField Type: RO
BitField Desc: Two MAC40G Interrupt Enable  1: Set  0: Clear
BitField Bits: [18:17]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_Mac40gIntEnable_Mask                                       cBit18_17
#define cAf6_global_interrupt_mask_enable_Mac40gIntEnable_Shift                                             17

/*--------------------------------------
BitField Name: UpsrIntEnable
BitField Type: RO
BitField Desc: UPSR Interrupt Enable  1: Set  0: Clear
BitField Bits: [16]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_UpsrIntEnable_Mask                                            cBit16
#define cAf6_global_interrupt_mask_enable_UpsrIntEnable_Shift                                               16

/*--------------------------------------
BitField Name: TenGeIntEnable
BitField Type: RO
BitField Desc: Ten Ge Interrupt Enable  1: Set  0: Clear
BitField Bits: [15]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_TenGeIntEnable_Mask                                           cBit15
#define cAf6_global_interrupt_mask_enable_TenGeIntEnable_Shift                                              15

/*--------------------------------------
BitField Name: GeIntEnable
BitField Type: RO
BitField Desc: Ge Interrupt Enable  1: Set  0: Clear
BitField Bits: [14]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_GeIntEnable_Mask                                              cBit14
#define cAf6_global_interrupt_mask_enable_GeIntEnable_Shift                                                 14

/*--------------------------------------
BitField Name: GlbpmcIntEnable
BitField Type: RO
BitField Desc: GLB PMC Interrupt Enable  1: Set  0: Clear
BitField Bits: [13]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_GlbpmcIntEnable_Mask                                          cBit13
#define cAf6_global_interrupt_mask_enable_GlbpmcIntEnable_Shift                                             13

/*--------------------------------------
BitField Name: SemIntEnable
BitField Type: RO
BitField Desc: Chip SEM Interrupt Enable  1: Set  0: Clear
BitField Bits: [12]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_SemIntEnable_Mask                                             cBit12
#define cAf6_global_interrupt_mask_enable_SemIntEnable_Shift                                                12

/*--------------------------------------
BitField Name: PtpIntEnable
BitField Type: RW
BitField Desc: PTP  Interrupt Enable  1: Set  0: Clear
BitField Bits: [11]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_PtpIntEnable_Mask                                             cBit11
#define cAf6_global_interrupt_mask_enable_PtpIntEnable_Shift                                                11

/*--------------------------------------
BitField Name: CdrIntEnable
BitField Type: RW
BitField Desc: CDR Interrupt Enable  1: Set  0: Clear
BitField Bits: [10]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_CdrIntEnable_Mask                                             cBit10
#define cAf6_global_interrupt_mask_enable_CdrIntEnable_Shift                                                10

/*--------------------------------------
BitField Name: MdlIntEnable
BitField Type: RW
BitField Desc: MDL Interrupt Enable  1: Set  0: Clear
BitField Bits: [9]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_MdlIntEnable_Mask                                              cBit9
#define cAf6_global_interrupt_mask_enable_MdlIntEnable_Shift                                                 9

/*--------------------------------------
BitField Name: PrmIntEnable
BitField Type: RW
BitField Desc: PRM Interrupt Enable  1: Set  0: Clear
BitField Bits: [8]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_PrmIntEnable_Mask                                              cBit8
#define cAf6_global_interrupt_mask_enable_PrmIntEnable_Shift                                                 8

/*--------------------------------------
BitField Name: PmIntEnable
BitField Type: RW
BitField Desc: PM Interrupt Enable  1: Set  0: Clear
BitField Bits: [7]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_PmIntEnable_Mask                                               cBit7
#define cAf6_global_interrupt_mask_enable_PmIntEnable_Shift                                                  7

/*--------------------------------------
BitField Name: FmIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Enable  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_FmIntEnable_Mask                                               cBit6
#define cAf6_global_interrupt_mask_enable_FmIntEnable_Shift                                                  6

/*--------------------------------------
BitField Name: TFI5IntEnable
BitField Type: RW
BitField Desc: TFI5 Interrupt Enable  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_TFI5IntEnable_Mask                                             cBit5
#define cAf6_global_interrupt_mask_enable_TFI5IntEnable_Shift                                                5

/*--------------------------------------
BitField Name: STSIntEnable
BitField Type: RW
BitField Desc: STS Interrupt Enable  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_STSIntEnable_Mask                                              cBit4
#define cAf6_global_interrupt_mask_enable_STSIntEnable_Shift                                                 4

/*--------------------------------------
BitField Name: VTIntEnable
BitField Type: RW
BitField Desc: VT Interrupt Enable  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_VTIntEnable_Mask                                               cBit3
#define cAf6_global_interrupt_mask_enable_VTIntEnable_Shift                                                  3

/*--------------------------------------
BitField Name: DE3IntEnable
BitField Type: RW
BitField Desc: DS3/E3 Interrupt Enable  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_DE3IntEnable_Mask                                              cBit2
#define cAf6_global_interrupt_mask_enable_DE3IntEnable_Shift                                                 2

/*--------------------------------------
BitField Name: DE1IntEnable
BitField Type: RW
BitField Desc: DS1/E1 Interrupt Enable  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_DE1IntEnable_Mask                                              cBit1
#define cAf6_global_interrupt_mask_enable_DE1IntEnable_Shift                                                 1

/*--------------------------------------
BitField Name: PWEIntEnable
BitField Type: RW
BitField Desc: PWE Interrupt Enable  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_PWEIntEnable_Mask                                              cBit0
#define cAf6_global_interrupt_mask_enable_PWEIntEnable_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Global PDH Interrupt Status
Reg Addr   : 0x00_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicate global pdh interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_pdh_interrupt_status_Base                                                      0x000004
#define cAf6Reg_global_pdh_interrupt_status_WidthVal                                                        32

/*--------------------------------------
BitField Name: DE3Slice8IntStatus
BitField Type: RO
BitField Desc: DE3 Slice8 Interrupt Status  1: Set  0: Clear
BitField Bits: [15]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice8IntStatus_Mask                                        cBit15
#define cAf6_global_pdh_interrupt_status_DE3Slice8IntStatus_Shift                                           15

/*--------------------------------------
BitField Name: DE3Slice7IntStatus
BitField Type: RO
BitField Desc: DE3 Slice7 Interrupt Status  1: Set  0: Clear
BitField Bits: [14]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice7IntStatus_Mask                                        cBit14
#define cAf6_global_pdh_interrupt_status_DE3Slice7IntStatus_Shift                                           14

/*--------------------------------------
BitField Name: DE3Slice6IntStatus
BitField Type: RO
BitField Desc: DE3 Slice6 Interrupt Status  1: Set  0: Clear
BitField Bits: [13]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice6IntStatus_Mask                                        cBit13
#define cAf6_global_pdh_interrupt_status_DE3Slice6IntStatus_Shift                                           13

/*--------------------------------------
BitField Name: DE3Slice5IntStatus
BitField Type: RO
BitField Desc: DE3 Slice5 Interrupt Status  1: Set  0: Clear
BitField Bits: [12]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice5IntStatus_Mask                                        cBit12
#define cAf6_global_pdh_interrupt_status_DE3Slice5IntStatus_Shift                                           12

/*--------------------------------------
BitField Name: DE3Slice4IntStatus
BitField Type: RO
BitField Desc: DE3 Slice4 Interrupt Status  1: Set  0: Clear
BitField Bits: [11]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice4IntStatus_Mask                                        cBit11
#define cAf6_global_pdh_interrupt_status_DE3Slice4IntStatus_Shift                                           11

/*--------------------------------------
BitField Name: DE3Slice3IntStatus
BitField Type: RO
BitField Desc: DE3 Slice3 Interrupt Status  1: Set  0: Clear
BitField Bits: [10]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice3IntStatus_Mask                                        cBit10
#define cAf6_global_pdh_interrupt_status_DE3Slice3IntStatus_Shift                                           10

/*--------------------------------------
BitField Name: DE3Slice2IntStatus
BitField Type: RO
BitField Desc: DE3 Slice2 Interrupt Status  1: Set  0: Clear
BitField Bits: [09]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice2IntStatus_Mask                                         cBit9
#define cAf6_global_pdh_interrupt_status_DE3Slice2IntStatus_Shift                                            9

/*--------------------------------------
BitField Name: DE3Slice1IntStatus
BitField Type: RO
BitField Desc: DE3 Slice1 Interrupt Status  1: Set  0: Clear
BitField Bits: [08]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE3Slice1IntStatus_Mask                                         cBit8
#define cAf6_global_pdh_interrupt_status_DE3Slice1IntStatus_Shift                                            8

/*--------------------------------------
BitField Name: DE1Slice8IntStatus
BitField Type: RO
BitField Desc: DE1 Slice8 Interrupt Status  1: Set  0: Clear
BitField Bits: [07]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice8IntStatus_Mask                                         cBit7
#define cAf6_global_pdh_interrupt_status_DE1Slice8IntStatus_Shift                                            7

/*--------------------------------------
BitField Name: DE1Slice7IntStatus
BitField Type: RO
BitField Desc: DE1 Slice7 Interrupt Status  1: Set  0: Clear
BitField Bits: [06]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice7IntStatus_Mask                                         cBit6
#define cAf6_global_pdh_interrupt_status_DE1Slice7IntStatus_Shift                                            6

/*--------------------------------------
BitField Name: DE1Slice6IntStatus
BitField Type: RO
BitField Desc: DE1 Slice6 Interrupt Status  1: Set  0: Clear
BitField Bits: [05]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice6IntStatus_Mask                                         cBit5
#define cAf6_global_pdh_interrupt_status_DE1Slice6IntStatus_Shift                                            5

/*--------------------------------------
BitField Name: DE1Slice5IntStatus
BitField Type: RO
BitField Desc: DE1 Slice5 Interrupt Status  1: Set  0: Clear
BitField Bits: [04]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice5IntStatus_Mask                                         cBit4
#define cAf6_global_pdh_interrupt_status_DE1Slice5IntStatus_Shift                                            4

/*--------------------------------------
BitField Name: DE1Slice4IntStatus
BitField Type: RO
BitField Desc: DE1 Slice4 Interrupt Status  1: Set  0: Clear
BitField Bits: [03]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice4IntStatus_Mask                                         cBit3
#define cAf6_global_pdh_interrupt_status_DE1Slice4IntStatus_Shift                                            3

/*--------------------------------------
BitField Name: DE1Slice3IntStatus
BitField Type: RO
BitField Desc: DE1 Slice3 Interrupt Status  1: Set  0: Clear
BitField Bits: [02]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice3IntStatus_Mask                                         cBit2
#define cAf6_global_pdh_interrupt_status_DE1Slice3IntStatus_Shift                                            2

/*--------------------------------------
BitField Name: DE1Slice2IntStatus
BitField Type: RO
BitField Desc: DE1 Slice2 Interrupt Status  1: Set  0: Clear
BitField Bits: [01]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice2IntStatus_Mask                                         cBit1
#define cAf6_global_pdh_interrupt_status_DE1Slice2IntStatus_Shift                                            1

/*--------------------------------------
BitField Name: DE1Slice1IntStatus
BitField Type: RO
BitField Desc: DE1 Slice1 Interrupt Status  1: Set  0: Clear
BitField Bits: [00]
--------------------------------------*/
#define cAf6_global_pdh_interrupt_status_DE1Slice1IntStatus_Mask                                         cBit0
#define cAf6_global_pdh_interrupt_status_DE1Slice1IntStatus_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Pin Disable
Reg Addr   : 0x00_0005
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures global interrupt pin disable

------------------------------------------------------------------------------*/
#define cAf6Reg_global_interrupt_pin_disable_Base                                                     0x000005
#define cAf6Reg_global_interrupt_pin_disable_WidthVal                                                       32

/*--------------------------------------
BitField Name: GlbPinIntDisable
BitField Type: RW
BitField Desc: Global Interrupt Pin Disable  1: Disable  0: Enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_interrupt_pin_disable_GlbPinIntDisable_Mask                                          cBit0
#define cAf6_global_interrupt_pin_disable_GlbPinIntDisable_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Mask Restore
Reg Addr   : 0x00_0006
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures global interrupt mask enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_interrupt_mask_restore_Base                                                    0x000006
#define cAf6Reg_global_interrupt_mask_restore_WidthVal                                                      32

/*--------------------------------------
BitField Name: UpsrIntRestore
BitField Type: RW
BitField Desc: UPSR Interrupt Restore  1: Set  0: Clear
BitField Bits: [12]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_UpsrIntRestore_Mask                                          cBit12
#define cAf6_global_interrupt_mask_restore_UpsrIntRestore_Shift                                             12

/*--------------------------------------
BitField Name: ReserveIntRestore
BitField Type: RW
BitField Desc: HoCDR Interrupt Restore  1: Set  0: Clear
BitField Bits: [11]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_ReserveIntRestore_Mask                                       cBit11
#define cAf6_global_interrupt_mask_restore_ReserveIntRestore_Shift                                          11

/*--------------------------------------
BitField Name: CdrIntRestore
BitField Type: RW
BitField Desc: CDR Interrupt Restore  1: Set  0: Clear
BitField Bits: [10]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_CdrIntRestore_Mask                                           cBit10
#define cAf6_global_interrupt_mask_restore_CdrIntRestore_Shift                                              10

/*--------------------------------------
BitField Name: MdlIntRestore
BitField Type: RW
BitField Desc: MDL Interrupt Restore  1: Set  0: Clear
BitField Bits: [9]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_MdlIntRestore_Mask                                            cBit9
#define cAf6_global_interrupt_mask_restore_MdlIntRestore_Shift                                               9

/*--------------------------------------
BitField Name: PrmIntRestore
BitField Type: RW
BitField Desc: PRM Interrupt Restore  1: Set  0: Clear
BitField Bits: [8]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_PrmIntRestore_Mask                                            cBit8
#define cAf6_global_interrupt_mask_restore_PrmIntRestore_Shift                                               8

/*--------------------------------------
BitField Name: PmIntRestore
BitField Type: RW
BitField Desc: PM Interrupt Restore  1: Set  0: Clear
BitField Bits: [7]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_PmIntRestore_Mask                                             cBit7
#define cAf6_global_interrupt_mask_restore_PmIntRestore_Shift                                                7

/*--------------------------------------
BitField Name: FmIntRestore
BitField Type: RW
BitField Desc: FM Interrupt Restore  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_FmIntRestore_Mask                                             cBit6
#define cAf6_global_interrupt_mask_restore_FmIntRestore_Shift                                                6

/*--------------------------------------
BitField Name: TFI5IntRestore
BitField Type: RW
BitField Desc: TFI5 Interrupt Restore  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_TFI5IntRestore_Mask                                           cBit5
#define cAf6_global_interrupt_mask_restore_TFI5IntRestore_Shift                                              5

/*--------------------------------------
BitField Name: STSIntRestore
BitField Type: RW
BitField Desc: STS Interrupt Restore  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_STSIntRestore_Mask                                            cBit4
#define cAf6_global_interrupt_mask_restore_STSIntRestore_Shift                                               4

/*--------------------------------------
BitField Name: VTIntRestore
BitField Type: RW
BitField Desc: VT Interrupt Restore  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_VTIntRestore_Mask                                             cBit3
#define cAf6_global_interrupt_mask_restore_VTIntRestore_Shift                                                3

/*--------------------------------------
BitField Name: DE3IntRestore
BitField Type: RW
BitField Desc: DS3/E3 Interrupt Restore  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_DE3IntRestore_Mask                                            cBit2
#define cAf6_global_interrupt_mask_restore_DE3IntRestore_Shift                                               2

/*--------------------------------------
BitField Name: DE1IntRestore
BitField Type: RW
BitField Desc: DS1/E1 Interrupt Restore  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_DE1IntRestore_Mask                                            cBit1
#define cAf6_global_interrupt_mask_restore_DE1IntRestore_Shift                                               1

/*--------------------------------------
BitField Name: PWEIntRestore
BitField Type: RW
BitField Desc: PWE Interrupt Restore  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_interrupt_mask_restore_PWEIntRestore_Mask                                            cBit0
#define cAf6_global_interrupt_mask_restore_PWEIntRestore_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : Global  Configuration RAM Parity Interrupt Mask Enable
Reg Addr   : 0x00_0007
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures parity global interrupt mask enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_parity_interrupt_mask_enable_Base                                              0x000007

/*--------------------------------------
BitField Name: PrmParIntEnable
BitField Type: RW
BitField Desc: PRM Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [31]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_PrmParIntEnable_Mask                                   cBit31
#define cAf6_global_parity_interrupt_mask_enable_PrmParIntEnable_Shift                                      31

/*--------------------------------------
BitField Name: PmcParIntEnable
BitField Type: RW
BitField Desc: PMC Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [30]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_PmcParIntEnable_Mask                                   cBit30
#define cAf6_global_parity_interrupt_mask_enable_PmcParIntEnable_Shift                                      30

/*--------------------------------------
BitField Name: ClaParIntEnable
BitField Type: RW
BitField Desc: CLA Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [29]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_ClaParIntEnable_Mask                                   cBit29
#define cAf6_global_parity_interrupt_mask_enable_ClaParIntEnable_Shift                                      29

/*--------------------------------------
BitField Name: PweParIntEnable
BitField Type: RW
BitField Desc: PWE ETH Port0 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [28]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_PweParIntEnable_Mask                                   cBit28
#define cAf6_global_parity_interrupt_mask_enable_PweParIntEnable_Shift                                      28

/*--------------------------------------
BitField Name: PdaParIntEnable
BitField Type: RW
BitField Desc: PDA Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [27]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_PdaParIntEnable_Mask                                   cBit27
#define cAf6_global_parity_interrupt_mask_enable_PdaParIntEnable_Shift                                      27

/*--------------------------------------
BitField Name: PlaParIntEnable
BitField Type: RW
BitField Desc: PLA Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [26]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_PlaParIntEnable_Mask                                   cBit26
#define cAf6_global_parity_interrupt_mask_enable_PlaParIntEnable_Shift                                      26

/*--------------------------------------
BitField Name: Cdr8ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice6 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [25]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr8ParIntEnable_Mask                                  cBit25
#define cAf6_global_parity_interrupt_mask_enable_Cdr8ParIntEnable_Shift                                      25

/*--------------------------------------
BitField Name: Map8ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice6 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [24]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map8ParIntEnable_Mask                                  cBit24
#define cAf6_global_parity_interrupt_mask_enable_Map8ParIntEnable_Shift                                      24

/*--------------------------------------
BitField Name: Pdh8ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice6 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [23]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh8ParIntEnable_Mask                                  cBit23
#define cAf6_global_parity_interrupt_mask_enable_Pdh8ParIntEnable_Shift                                      23

/*--------------------------------------
BitField Name: Cdr7ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice6 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [22]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr7ParIntEnable_Mask                                  cBit22
#define cAf6_global_parity_interrupt_mask_enable_Cdr7ParIntEnable_Shift                                      22

/*--------------------------------------
BitField Name: Map7ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice6 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [21]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map7ParIntEnable_Mask                                  cBit21
#define cAf6_global_parity_interrupt_mask_enable_Map7ParIntEnable_Shift                                      21

/*--------------------------------------
BitField Name: Pdh7ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice6 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [20]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh7ParIntEnable_Mask                                  cBit20
#define cAf6_global_parity_interrupt_mask_enable_Pdh7ParIntEnable_Shift                                      20

/*--------------------------------------
BitField Name: Cdr6ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice6 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [19]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr6ParIntEnable_Mask                                  cBit19
#define cAf6_global_parity_interrupt_mask_enable_Cdr6ParIntEnable_Shift                                      19

/*--------------------------------------
BitField Name: Map6ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice6 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [18]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map6ParIntEnable_Mask                                  cBit18
#define cAf6_global_parity_interrupt_mask_enable_Map6ParIntEnable_Shift                                      18

/*--------------------------------------
BitField Name: Pdh6ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice6 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [17]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh6ParIntEnable_Mask                                  cBit17
#define cAf6_global_parity_interrupt_mask_enable_Pdh6ParIntEnable_Shift                                      17

/*--------------------------------------
BitField Name: Cdr5ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice5 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [16]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr5ParIntEnable_Mask                                  cBit16
#define cAf6_global_parity_interrupt_mask_enable_Cdr5ParIntEnable_Shift                                      16

/*--------------------------------------
BitField Name: Map5ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice5 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [15]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map5ParIntEnable_Mask                                  cBit15
#define cAf6_global_parity_interrupt_mask_enable_Map5ParIntEnable_Shift                                      15

/*--------------------------------------
BitField Name: Pdh5ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice5 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [14]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh5ParIntEnable_Mask                                  cBit14
#define cAf6_global_parity_interrupt_mask_enable_Pdh5ParIntEnable_Shift                                      14

/*--------------------------------------
BitField Name: Cdr4ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice4 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [13]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr4ParIntEnable_Mask                                  cBit13
#define cAf6_global_parity_interrupt_mask_enable_Cdr4ParIntEnable_Shift                                      13

/*--------------------------------------
BitField Name: Map4ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice4 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [12]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map4ParIntEnable_Mask                                  cBit12
#define cAf6_global_parity_interrupt_mask_enable_Map4ParIntEnable_Shift                                      12

/*--------------------------------------
BitField Name: Pdh4ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice4 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [11]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh4ParIntEnable_Mask                                  cBit11
#define cAf6_global_parity_interrupt_mask_enable_Pdh4ParIntEnable_Shift                                      11

/*--------------------------------------
BitField Name: Cdr3ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice3 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [10]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr3ParIntEnable_Mask                                  cBit10
#define cAf6_global_parity_interrupt_mask_enable_Cdr3ParIntEnable_Shift                                      10

/*--------------------------------------
BitField Name: Map3ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice3 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [09]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map3ParIntEnable_Mask                                   cBit9
#define cAf6_global_parity_interrupt_mask_enable_Map3ParIntEnable_Shift                                       9

/*--------------------------------------
BitField Name: Pdh3ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice3 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [08]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh3ParIntEnable_Mask                                   cBit8
#define cAf6_global_parity_interrupt_mask_enable_Pdh3ParIntEnable_Shift                                       8

/*--------------------------------------
BitField Name: Cdr2ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice2 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [7]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr2ParIntEnable_Mask                                   cBit7
#define cAf6_global_parity_interrupt_mask_enable_Cdr2ParIntEnable_Shift                                       7

/*--------------------------------------
BitField Name: Map2ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice2 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map2ParIntEnable_Mask                                   cBit6
#define cAf6_global_parity_interrupt_mask_enable_Map2ParIntEnable_Shift                                       6

/*--------------------------------------
BitField Name: Pdh2ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice2 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh2ParIntEnable_Mask                                   cBit5
#define cAf6_global_parity_interrupt_mask_enable_Pdh2ParIntEnable_Shift                                       5

/*--------------------------------------
BitField Name: Cdr1ParIntEnable
BitField Type: RW
BitField Desc: CDR Slice1 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Cdr1ParIntEnable_Mask                                   cBit4
#define cAf6_global_parity_interrupt_mask_enable_Cdr1ParIntEnable_Shift                                       4

/*--------------------------------------
BitField Name: Map1ParIntEnable
BitField Type: RW
BitField Desc: MAP Slice1 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Map1ParIntEnable_Mask                                   cBit3
#define cAf6_global_parity_interrupt_mask_enable_Map1ParIntEnable_Shift                                       3

/*--------------------------------------
BitField Name: Pdh1ParIntEnable
BitField Type: RW
BitField Desc: PDH Slice1 Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_Pdh1ParIntEnable_Mask                                   cBit2
#define cAf6_global_parity_interrupt_mask_enable_Pdh1ParIntEnable_Shift                                       2

/*--------------------------------------
BitField Name: PohParIntEnable
BitField Type: RW
BitField Desc: POH Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_PohParIntEnable_Mask                                    cBit1
#define cAf6_global_parity_interrupt_mask_enable_PohParIntEnable_Shift                                       1

/*--------------------------------------
BitField Name: OcnParIntEnable
BitField Type: RW
BitField Desc: OCN Parity Interrupt Enable  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_parity_interrupt_mask_enable_OcnParIntEnable_Mask                                    cBit0
#define cAf6_global_parity_interrupt_mask_enable_OcnParIntEnable_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Global  Configuration RAM Parity Interrupt Status
Reg Addr   : 0x00_0008
Reg Formula: 
    Where  : 
Reg Desc   : 
This register show global parity interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_parity_interrupt_status_Base                                                   0x000008

/*--------------------------------------
BitField Name: PrmParIntStatus
BitField Type: RO
BitField Desc: PRM Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [31]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_PrmParIntStatus_Mask                                        cBit31
#define cAf6_global_parity_interrupt_status_PrmParIntStatus_Shift                                           31

/*--------------------------------------
BitField Name: PmcParIntStatus
BitField Type: RO
BitField Desc: PMC Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [30]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_PmcParIntStatus_Mask                                        cBit30
#define cAf6_global_parity_interrupt_status_PmcParIntStatus_Shift                                           30

/*--------------------------------------
BitField Name: ClaParIntStatus
BitField Type: RO
BitField Desc: CLA Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [29]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_ClaParIntStatus_Mask                                        cBit29
#define cAf6_global_parity_interrupt_status_ClaParIntStatus_Shift                                           29

/*--------------------------------------
BitField Name: PweParIntStatus
BitField Type: RO
BitField Desc: PWE ETH Port0 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [28]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_PweParIntStatus_Mask                                        cBit28
#define cAf6_global_parity_interrupt_status_PweParIntStatus_Shift                                           28

/*--------------------------------------
BitField Name: PdaParIntStatus
BitField Type: RO
BitField Desc: PDA Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [27]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_PdaParIntStatus_Mask                                        cBit27
#define cAf6_global_parity_interrupt_status_PdaParIntStatus_Shift                                           27

/*--------------------------------------
BitField Name: PlaParIntStatus
BitField Type: RO
BitField Desc: PLA Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [26]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_PlaParIntStatus_Mask                                        cBit26
#define cAf6_global_parity_interrupt_status_PlaParIntStatus_Shift                                           26

/*--------------------------------------
BitField Name: Cdr8ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice6 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [25]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr8ParIntStatus_Mask                                       cBit25
#define cAf6_global_parity_interrupt_status_Cdr8ParIntStatus_Shift                                          25

/*--------------------------------------
BitField Name: Map8ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice6 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [24]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map8ParIntStatus_Mask                                       cBit24
#define cAf6_global_parity_interrupt_status_Map8ParIntStatus_Shift                                          24

/*--------------------------------------
BitField Name: Pdh8ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice6 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [23]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh8ParIntStatus_Mask                                       cBit23
#define cAf6_global_parity_interrupt_status_Pdh8ParIntStatus_Shift                                          23

/*--------------------------------------
BitField Name: Cdr7ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice6 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [22]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr7ParIntStatus_Mask                                       cBit22
#define cAf6_global_parity_interrupt_status_Cdr7ParIntStatus_Shift                                          22

/*--------------------------------------
BitField Name: Map7ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice6 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [21]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map7ParIntStatus_Mask                                       cBit21
#define cAf6_global_parity_interrupt_status_Map7ParIntStatus_Shift                                          21

/*--------------------------------------
BitField Name: Pdh7ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice6 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [20]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh7ParIntStatus_Mask                                       cBit20
#define cAf6_global_parity_interrupt_status_Pdh7ParIntStatus_Shift                                          20

/*--------------------------------------
BitField Name: Cdr6ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice6 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [19]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr6ParIntStatus_Mask                                       cBit19
#define cAf6_global_parity_interrupt_status_Cdr6ParIntStatus_Shift                                          19

/*--------------------------------------
BitField Name: Map6ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice6 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [18]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map6ParIntStatus_Mask                                       cBit18
#define cAf6_global_parity_interrupt_status_Map6ParIntStatus_Shift                                          18

/*--------------------------------------
BitField Name: Pdh6ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice6 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [17]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh6ParIntStatus_Mask                                       cBit17
#define cAf6_global_parity_interrupt_status_Pdh6ParIntStatus_Shift                                          17

/*--------------------------------------
BitField Name: Cdr5ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice5 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [16]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr5ParIntStatus_Mask                                       cBit16
#define cAf6_global_parity_interrupt_status_Cdr5ParIntStatus_Shift                                          16

/*--------------------------------------
BitField Name: Map5ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice5 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [15]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map5ParIntStatus_Mask                                       cBit15
#define cAf6_global_parity_interrupt_status_Map5ParIntStatus_Shift                                          15

/*--------------------------------------
BitField Name: Pdh5ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice5 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [14]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh5ParIntStatus_Mask                                       cBit14
#define cAf6_global_parity_interrupt_status_Pdh5ParIntStatus_Shift                                          14

/*--------------------------------------
BitField Name: Cdr4ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice4 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [13]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr4ParIntStatus_Mask                                       cBit13
#define cAf6_global_parity_interrupt_status_Cdr4ParIntStatus_Shift                                          13

/*--------------------------------------
BitField Name: Map4ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice4 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [12]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map4ParIntStatus_Mask                                       cBit12
#define cAf6_global_parity_interrupt_status_Map4ParIntStatus_Shift                                          12

/*--------------------------------------
BitField Name: Pdh4ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice4 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [11]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh4ParIntStatus_Mask                                       cBit11
#define cAf6_global_parity_interrupt_status_Pdh4ParIntStatus_Shift                                          11

/*--------------------------------------
BitField Name: Cdr3ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice3 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [10]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr3ParIntStatus_Mask                                       cBit10
#define cAf6_global_parity_interrupt_status_Cdr3ParIntStatus_Shift                                          10

/*--------------------------------------
BitField Name: Map3ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice3 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [09]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map3ParIntStatus_Mask                                        cBit9
#define cAf6_global_parity_interrupt_status_Map3ParIntStatus_Shift                                           9

/*--------------------------------------
BitField Name: Pdh3ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice3 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [08]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh3ParIntStatus_Mask                                        cBit8
#define cAf6_global_parity_interrupt_status_Pdh3ParIntStatus_Shift                                           8

/*--------------------------------------
BitField Name: Cdr2ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice2 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [7]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr2ParIntStatus_Mask                                        cBit7
#define cAf6_global_parity_interrupt_status_Cdr2ParIntStatus_Shift                                           7

/*--------------------------------------
BitField Name: Map2ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice2 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map2ParIntStatus_Mask                                        cBit6
#define cAf6_global_parity_interrupt_status_Map2ParIntStatus_Shift                                           6

/*--------------------------------------
BitField Name: Pdh2ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice2 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh2ParIntStatus_Mask                                        cBit5
#define cAf6_global_parity_interrupt_status_Pdh2ParIntStatus_Shift                                           5

/*--------------------------------------
BitField Name: Cdr1ParIntStatus
BitField Type: RO
BitField Desc: CDR Slice1 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Cdr1ParIntStatus_Mask                                        cBit4
#define cAf6_global_parity_interrupt_status_Cdr1ParIntStatus_Shift                                           4

/*--------------------------------------
BitField Name: Map1ParIntStatus
BitField Type: RO
BitField Desc: MAP Slice1 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Map1ParIntStatus_Mask                                        cBit3
#define cAf6_global_parity_interrupt_status_Map1ParIntStatus_Shift                                           3

/*--------------------------------------
BitField Name: Pdh1ParIntStatus
BitField Type: RO
BitField Desc: PDH Slice1 Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_Pdh1ParIntStatus_Mask                                        cBit2
#define cAf6_global_parity_interrupt_status_Pdh1ParIntStatus_Shift                                           2

/*--------------------------------------
BitField Name: PohParIntStatus
BitField Type: RO
BitField Desc: POH Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_PohParIntStatus_Mask                                         cBit1
#define cAf6_global_parity_interrupt_status_PohParIntStatus_Shift                                            1

/*--------------------------------------
BitField Name: OcnParIntStatus
BitField Type: RO
BitField Desc: OCN Parity Interrupt Status  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_parity_interrupt_status_OcnParIntStatus_Mask                                         cBit0
#define cAf6_global_parity_interrupt_status_OcnParIntStatus_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Global LoCDR Interrupt Status
Reg Addr   : 0x00_0009
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicate global low order CDR interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_locdr_interrupt_status_Base                                                    0x000009
#define cAf6Reg_global_locdr_interrupt_status_WidthVal                                                      32

/*--------------------------------------
BitField Name: LoCDRSlice8IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice8 Interrupt Status  1: Set  0: Clear
BitField Bits: [7]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice8IntStatus_Mask                                     cBit7
#define cAf6_global_locdr_interrupt_status_LoCDRSlice8IntStatus_Shift                                        7

/*--------------------------------------
BitField Name: LoCDRSlice7IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice7 Interrupt Status  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice7IntStatus_Mask                                     cBit6
#define cAf6_global_locdr_interrupt_status_LoCDRSlice7IntStatus_Shift                                        6

/*--------------------------------------
BitField Name: LoCDRSlice6IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice6 Interrupt Status  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice6IntStatus_Mask                                     cBit5
#define cAf6_global_locdr_interrupt_status_LoCDRSlice6IntStatus_Shift                                        5

/*--------------------------------------
BitField Name: LoCDRSlice5IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice5 Interrupt Status  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice5IntStatus_Mask                                     cBit4
#define cAf6_global_locdr_interrupt_status_LoCDRSlice5IntStatus_Shift                                        4

/*--------------------------------------
BitField Name: LoCDRSlice4IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice4 Interrupt Status  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice4IntStatus_Mask                                     cBit3
#define cAf6_global_locdr_interrupt_status_LoCDRSlice4IntStatus_Shift                                        3

/*--------------------------------------
BitField Name: LoCDRSlice3IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice3 Interrupt Status  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice3IntStatus_Mask                                     cBit2
#define cAf6_global_locdr_interrupt_status_LoCDRSlice3IntStatus_Shift                                        2

/*--------------------------------------
BitField Name: LoCDRSlice2IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice2 Interrupt Status  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice2IntStatus_Mask                                     cBit1
#define cAf6_global_locdr_interrupt_status_LoCDRSlice2IntStatus_Shift                                        1

/*--------------------------------------
BitField Name: LoCDRSlice1IntStatus
BitField Type: RO
BitField Desc: LoCDR Slice1 Interrupt Status  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_locdr_interrupt_status_LoCDRSlice1IntStatus_Mask                                     cBit0
#define cAf6_global_locdr_interrupt_status_LoCDRSlice1IntStatus_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : Global PMC 2page Interrupt Status
Reg Addr   : 0x00_000A
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicate global low order CDR interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_pmc_2page_interrupt_status_Base                                                0x00000A
#define cAf6Reg_global_pmc_2page_interrupt_status_WidthVal                                                  32

/*--------------------------------------
BitField Name: PmrIntStatus
BitField Type: RO
BitField Desc: Pmr     Interrupt Status  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_PmrIntStatus_Mask                                         cBit6
#define cAf6_global_pmc_2page_interrupt_status_PmrIntStatus_Shift                                            6

/*--------------------------------------
BitField Name: PohpmvtIntStatus
BitField Type: RO
BitField Desc: Pohpmvt Interrupt Status  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_PohpmvtIntStatus_Mask                                     cBit5
#define cAf6_global_pmc_2page_interrupt_status_PohpmvtIntStatus_Shift                                        5

/*--------------------------------------
BitField Name: PohpmstsIntStatus
BitField Type: RO
BitField Desc: Pohpmsts Interrupt Status  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_PohpmstsIntStatus_Mask                                    cBit4
#define cAf6_global_pmc_2page_interrupt_status_PohpmstsIntStatus_Shift                                       4

/*--------------------------------------
BitField Name: PohIntStatus
BitField Type: RO
BitField Desc: Poh    Interrupt Status  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_PohIntStatus_Mask                                         cBit3
#define cAf6_global_pmc_2page_interrupt_status_PohIntStatus_Shift                                            3

/*--------------------------------------
BitField Name: Pdhde1IntStatus
BitField Type: RO
BitField Desc: Pdhde1 Interrupt Status  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_Pdhde1IntStatus_Mask                                      cBit2
#define cAf6_global_pmc_2page_interrupt_status_Pdhde1IntStatus_Shift                                         2

/*--------------------------------------
BitField Name: Pdhde3IntStatus
BitField Type: RO
BitField Desc: Pdhde3 Interrupt Status  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_Pdhde3IntStatus_Mask                                      cBit1
#define cAf6_global_pmc_2page_interrupt_status_Pdhde3IntStatus_Shift                                         1

/*--------------------------------------
BitField Name: PwcntIntStatus
BitField Type: RO
BitField Desc: Pwcnt  Interrupt Status  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_PwcntIntStatus_Mask                                       cBit0
#define cAf6_global_pmc_2page_interrupt_status_PwcntIntStatus_Shift                                          0

#endif /* _AF6_REG_AF6NC0022_RD_INTR_H_ */

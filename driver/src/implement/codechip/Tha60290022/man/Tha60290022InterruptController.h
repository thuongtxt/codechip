/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60290022InterruptController.h
 * 
 * Created Date: Oct 18, 2018
 *
 * Description : Interrupt controller interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022INTERRUPTCONTROLLER_H_
#define _THA60290022INTERRUPTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtIpCore.h"
#include "../../../default/man/intrcontroller/ThaIntrController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaIntrController Tha60290022IntrControllerNew(AtIpCore ipCore);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022INTERRUPTCONTROLLER_H_ */


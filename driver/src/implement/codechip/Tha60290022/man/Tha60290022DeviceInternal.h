/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60290022DeviceInternal.h
 * 
 * Created Date: Jul 23, 2018
 *
 * Description : 60290022 Device management internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022DEVICEINTERNAL_H_
#define _THA60290022DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/man/Tha60290021DeviceInternal.h"
#include "Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022Device *Tha60290022Device;

typedef struct tTha60290022DeviceMethods
    {
    eBool (*DeviceCapacity15G)(AtDevice self);
    eBool (*HasKByteFeatureOnly)(AtDevice self);
    eBool (*HwLogicOptimized)(AtDevice self);
    eBool (*DccV2IsSupported)(AtDevice self);
    eBool (*DeviceV3OptimizationIsSupported)(AtDevice self);
    eBool (*Pmc1PageIsSupported)(AtDevice self);
    eBool (*HoTxG1OverWriteIsSupported)(AtDevice self);
    eBool (*DccV4dot1EnableRegAddressIsSupported)(AtDevice self);
    eBool (*ClockSquelchingIsSupported)(AtDevice self);
    eBool (*ClockEthSquelchingIsSupported)(AtDevice self);
    eBool (*EthTxIpgIsSupported)(AtDevice self);
    eBool (*DccTxFcsMsbIsSupported)(AtDevice self);
    eBool (*DccSgmiiTxIpg31ByteIsSupported)(AtDevice self);
    eBool (*Eth100MFxLfRfSupported)(AtDevice self);
    eBool (*DccHdlcTransmissionBitOrderPerChannelIsSupported)(AtDevice self);
    eBool (*Sts192cPrbsSupported)(AtDevice self);
    eBool (*SemIsSupported)(AtDevice self);
    eBool (*OcnSeparatedSectionAndLineDccIsSupported)(AtDevice self);
    eBool (*EthPortFlowControlWaterMarkIsSupported)(AtDevice self);
    eBool (*ClockEthSquelchingSelectOnCoreOnlyIsSupported)(AtDevice self);
    eBool (*Sts192cTxG1OverrideIsNotUsed)(AtDevice self);
    eBool (*Sts192cPrbsInvertionIsSupported)(AtDevice self);
    uint32 (*DcrAcrTxShapperSupportedVersion)(AtDevice self);
    eBool (*ClockExtractorMonitorPpmIsSupported)(AtDevice self);
    eBool (*TohSonetSdhModeIsSupported)(AtDevice self);
    eBool (*CesopAutoRxMBitConfigurableIsSupported)(AtDevice self);
    eBool (*SeparateLbitAndLopsPktReplacementSupported)(AtDevice self);
    eBool (*FaceplateEthCounter32bitIsSupported)(AtDevice self);
    eBool (*Eth100FxSerdesClock31Dot25MhzIsSupported)(AtDevice self);
    eBool (*DccEthMaxLengthConfigurationIsSupported)(AtDevice self);
    uint8 (*MapDemapNumLoSlices)(AtDevice self);
    eBool (*HasStandardClearTime)(AtDevice self);
    eBool (*IsNewBerInterrupt)(AtDevice self);
    eBool (*FaceplatSerdesEthHasK30_7Feature)(AtDevice self);
    uint32 (*FaceplatSerdesEthStartVersionSupportTxAlarmForcing)(AtDevice self);
    eBool (*FaceplatSerdesEthExcessiveErrorRatioIsSupported)(AtDevice self);
    eBool (*EthHasSgmiiDiagnosticLogic)(AtDevice self);
    eBool (*SgmiiSohEthUseNewMac)(AtDevice self);
    uint32 (*NumberOfMapLoPwPerSlice)(AtDevice self);
    eBool (*NewTuVcLoopbackIsReady)(AtDevice self);
    eBool (*Sts192cIsSupported)(AtDevice self);
    eBool (*PdaIsNewOc192cOffset)(AtDevice self);
    eBool (*PdaNumJitterBufferBlocksIncreased)(AtDevice self);
    uint32 (*PdhStartVersionSupportPrm)(AtDevice self);
    uint32 (*PdhStartVersionSupportDe1LomfConsequentialAction)(AtDevice self);
    uint32 (*PdhStartVersionSupportDe1IdleCodeInsertion)(AtDevice self);
    eBool (*PdhRxHw3BitFeacSignalIsSupported)(AtDevice self);
    eBool (*FaceplateSerdesPmaFarEndLoopbackSupported)(AtDevice self);
    eBool (*KByteSerdesIsControllable)(AtDevice self);
    eBool (*SgmiiSerdesShouldUseMdioLocalLoopback)(AtDevice self);
    eBool (*PohHasCpujnreqen)(AtDevice self);
    uint32 (*PohStartVersionSupportSeparateRdiErdiS)(AtDevice self);
    eBool (*PrbsBertOptimizeIsSupported)(AtDevice self);
    eBool (*PwDccKbyteInterruptIsSupported)(AtDevice self);
    eBool (*FaceplateFramerLocalLoopbackSupported)(AtDevice self);
    eBool (*SurFailureHoldOnTimerConfigurable)(AtDevice self);
    eBool (*FaceplateSerdesStm1SamplingFromStm4)(AtDevice self);
    eBool (*FaceplateSerdesHasNewParams)(Tha60290022Device self);
    eBool (*DccHdlcHasPacketCounterDuringFifoFull)(AtDevice self);
    eBool (*FacePlateSerdesDataMaskOnResetConfigurable)(AtDevice self);
    eBool (*ModuleBerMeasureTimeEngineIsSupported)(AtDevice self);
    eBool (*PtpSupported)(AtDevice self);
    eBool (*HasApsWtr3SecondResolution)(AtDevice self);
    eBool (*HasSoftResetForSem)(AtDevice self);
    }tTha60290022DeviceMethods;

typedef struct tTha60290022Device
    {
    tTha60290021Device super;
    eBool isForcedToUseTxShapper;
    const tTha60290022DeviceMethods *methods;
    }tTha60290022Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60290022DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022DEVICEINTERNAL_H_ */


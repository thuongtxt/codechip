/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_GLB_H_
#define _AF6_REG_AF6CNC0022_RD_GLB_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Device Product ID
Reg Addr   : 0x00_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicates Product ID.

------------------------------------------------------------------------------*/
#define cAf6Reg_ProductID_Base                                                                        0x000000

/*--------------------------------------
BitField Name: ProductID
BitField Type: RO
BitField Desc: ProductId
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ProductID_ProductID_Mask                                                                 cBit31_0
#define cAf6_ProductID_ProductID_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Device Year Month Day Version ID
Reg Addr   : 0x00_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicates Year Month Day and main version ID.

------------------------------------------------------------------------------*/
#define cAf6Reg_YYMMDD_VerID_Base                                                                     0x000002

/*--------------------------------------
BitField Name: Year
BitField Type: RO
BitField Desc: Year
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_YYMMDD_VerID_Year_Mask                                                                  cBit31_24
#define cAf6_YYMMDD_VerID_Year_Shift                                                                        24

/*--------------------------------------
BitField Name: Month
BitField Type: RO
BitField Desc: Month
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_YYMMDD_VerID_Month_Mask                                                                 cBit23_16
#define cAf6_YYMMDD_VerID_Month_Shift                                                                       16

/*--------------------------------------
BitField Name: Day
BitField Type: RO
BitField Desc: Day
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_YYMMDD_VerID_Day_Mask                                                                    cBit15_8
#define cAf6_YYMMDD_VerID_Day_Shift                                                                          8

/*--------------------------------------
BitField Name: Version
BitField Type: RO
BitField Desc: Version
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_YYMMDD_VerID_Version_Mask                                                                 cBit7_0
#define cAf6_YYMMDD_VerID_Version_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Device Internal ID
Reg Addr   : 0x00_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicates internal ID.

------------------------------------------------------------------------------*/
#define cAf6Reg_InternalID_Base                                                                       0x000003
#define cAf6Reg_InternalID_WidthVal                                                                         32

/*--------------------------------------
BitField Name: InternalID
BitField Type: RO
BitField Desc: InternalID
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_InternalID_InternalID_Mask                                                               cBit15_0
#define cAf6_InternalID_InternalID_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : GLB Sub-Core Active
Reg Addr   : 0x00_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicates the active ports.

------------------------------------------------------------------------------*/
#define cAf6Reg_Active_Base                                                                           0x000001

/*--------------------------------------
BitField Name: SubCoreEn
BitField Type: RW
BitField Desc: Enable per sub-core
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Active_SubCoreEn_Mask                                                                    cBit31_0
#define cAf6_Active_SubCoreEn_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW Control
Reg Addr   : 0x00_0010
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure debug PW parameters

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwControl_Base                                                                   0x000010
#define cAf6Reg_DebugPwControl_WidthVal                                                                     32

/*--------------------------------------
BitField Name: Oc192CepEna
BitField Type: RW
BitField Desc: Set 1 to enable debug OC192c CEP PW
BitField Bits: [31]
--------------------------------------*/
#define cAf6_DebugPwControl_Oc192CepEna_Mask                                                            cBit31
#define cAf6_DebugPwControl_Oc192CepEna_Shift                                                               31

/*--------------------------------------
BitField Name: LoTdmPwID
BitField Type: RW
BitField Desc: Lo TDM PW ID or HO Master STS ID corresponding to global PW ID
BitField Bits: [30:20]
--------------------------------------*/
#define cAf6_DebugPwControl_LoTdmPwID_Mask                                                           cBit30_20
#define cAf6_DebugPwControl_LoTdmPwID_Shift                                                                 20

/*--------------------------------------
BitField Name: GlobalPwID12_0
BitField Type: RW
BitField Desc: Global PW ID bit12_0 need to debug
BitField Bits: [19:7]
--------------------------------------*/
#define cAf6_DebugPwControl_GlobalPwID12_0_Mask                                                       cBit19_7
#define cAf6_DebugPwControl_GlobalPwID12_0_Shift                                                             7

/*--------------------------------------
BitField Name: OC48ID
BitField Type: RW
BitField Desc: OC48 ID, OC192 ID
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_DebugPwControl_OC48ID_Mask                                                                cBit6_4
#define cAf6_DebugPwControl_OC48ID_Shift                                                                     4

/*--------------------------------------
BitField Name: LoOC24Slice
BitField Type: RW
BitField Desc: Low Order OC24 slice if the PW belong to low order path
BitField Bits: [3]
--------------------------------------*/
#define cAf6_DebugPwControl_LoOC24Slice_Mask                                                             cBit3
#define cAf6_DebugPwControl_LoOC24Slice_Shift                                                                3

/*--------------------------------------
BitField Name: GlobalPwID13
BitField Type: RW
BitField Desc: Global PW ID bit13 need to debug
BitField Bits: [3]
--------------------------------------*/
#define cAf6_DebugPwControl_GlobalPwID13_Mask                                                            cBit3
#define cAf6_DebugPwControl_GlobalPwID13_Shift                                                               3

/*--------------------------------------
BitField Name: HoPwType
BitField Type: RW
BitField Desc: High Order PW type if the PW belong to high order path 0:
STS1/VC3 CEP 1: STS3/VC4 CEP 2: STS12/VC4-4C CEP 3: STS48/VC4-16C CEP 4:
VC11/VC12 CEP 5: TU3 CEP 6: STS24/VC4-8C CEP 7: SAToP/CESoP
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_DebugPwControl_HoPwType_Mask                                                              cBit2_0
#define cAf6_DebugPwControl_HoPwType_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW Control
Reg Addr   : 0x00_0024
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to debug PW modules

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwSticky_Base                                                                    0x000024
#define cAf6Reg_DebugPwSticky_WidthVal                                                                      32

/*--------------------------------------
BitField Name: ClaPrbsErr
BitField Type: WC
BitField Desc: CLA output PRBS error
BitField Bits: [28]
--------------------------------------*/
#define cAf6_DebugPwSticky_ClaPrbsErr_Mask                                                              cBit28
#define cAf6_DebugPwSticky_ClaPrbsErr_Shift                                                                 28

/*--------------------------------------
BitField Name: PwePrbsErr
BitField Type: RC
BitField Desc: PWE input PRBS error
BitField Bits: [19]
--------------------------------------*/
#define cAf6_DebugPwSticky_PwePrbsErr_Mask                                                              cBit19
#define cAf6_DebugPwSticky_PwePrbsErr_Shift                                                                 19

/*--------------------------------------
BitField Name: PdaPrbsErr
BitField Type: RC
BitField Desc: High Order PDA output PRBS error
BitField Bits: [17]
--------------------------------------*/
#define cAf6_DebugPwSticky_PdaPrbsErr_Mask                                                              cBit17
#define cAf6_DebugPwSticky_PdaPrbsErr_Shift                                                                 17

/*--------------------------------------
BitField Name: PlaPrbsErr
BitField Type: RC
BitField Desc: High Order PLA input PRBS error
BitField Bits: [16]
--------------------------------------*/
#define cAf6_DebugPwSticky_PlaPrbsErr_Mask                                                              cBit16
#define cAf6_DebugPwSticky_PlaPrbsErr_Shift                                                                 16

/*--------------------------------------
BitField Name: ClaPrbsSyn
BitField Type: WC
BitField Desc: CLA output PRBS sync
BitField Bits: [12]
--------------------------------------*/
#define cAf6_DebugPwSticky_ClaPrbsSyn_Mask                                                              cBit12
#define cAf6_DebugPwSticky_ClaPrbsSyn_Shift                                                                 12

/*--------------------------------------
BitField Name: PwePrbsSyn
BitField Type: RC
BitField Desc: PWE input PRBS sync
BitField Bits: [3]
--------------------------------------*/
#define cAf6_DebugPwSticky_PwePrbsSyn_Mask                                                               cBit3
#define cAf6_DebugPwSticky_PwePrbsSyn_Shift                                                                  3

/*--------------------------------------
BitField Name: PdaPrbsSyn
BitField Type: RC
BitField Desc: High Order PDA output PRBS sync
BitField Bits: [1]
--------------------------------------*/
#define cAf6_DebugPwSticky_PdaPrbsSyn_Mask                                                               cBit1
#define cAf6_DebugPwSticky_PdaPrbsSyn_Shift                                                                  1

/*--------------------------------------
BitField Name: PlaPrbsSyn
BitField Type: RC
BitField Desc: High Order PLA input PRBS sync
BitField Bits: [0]
--------------------------------------*/
#define cAf6_DebugPwSticky_PlaPrbsSyn_Mask                                                               cBit0
#define cAf6_DebugPwSticky_PlaPrbsSyn_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW PLA HO Speed
Reg Addr   : 0x00_0030
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to show PLA HO PW speed

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwPlaSpeed_Base                                                                  0x000030

/*--------------------------------------
BitField Name: PlaPwSpeed
BitField Type: RO
BitField Desc: PLA input PW speed
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_DebugPwPlaSpeed_PlaPwSpeed_Mask                                                          cBit31_0
#define cAf6_DebugPwPlaSpeed_PlaPwSpeed_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW PDA HO Speed
Reg Addr   : 0x00_0031
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to show PDA HO PW speed

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwPdaHoSpeed_Base                                                                0x000031

/*--------------------------------------
BitField Name: PdaPwSpeed
BitField Type: RO
BitField Desc: PDA output PW speed
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_DebugPwPdaHoSpeed_PdaPwSpeed_Mask                                                        cBit31_0
#define cAf6_DebugPwPdaHoSpeed_PdaPwSpeed_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW PWE Speed
Reg Addr   : 0x00_0034
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to show PWE PW speed

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwPweSpeed_Base                                                                  0x000034

/*--------------------------------------
BitField Name: PwePwSpeed
BitField Type: RO
BitField Desc: PWE input PW speed
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_DebugPwPweSpeed_PwePwSpeed_Mask                                                          cBit31_0
#define cAf6_DebugPwPweSpeed_PwePwSpeed_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug PW CLA Speed
Reg Addr   : 0x00_0035
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to show PWE PW speed

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugPwClaLoSpeed_Base                                                                0x000035

/*--------------------------------------
BitField Name: ClaPwSpeed
BitField Type: RO
BitField Desc: CLA output PW speed
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_DebugPwClaLoSpeed_ClaPwSpeed_Mask                                                        cBit31_0
#define cAf6_DebugPwClaLoSpeed_ClaPwSpeed_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Chip Temperature Sticky
Reg Addr   : 0x00_0020
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to sticky temperature

------------------------------------------------------------------------------*/
#define cAf6Reg_ChipTempSticky_Base                                                                   0x000020
#define cAf6Reg_ChipTempSticky_WidthVal                                                                     32

/*--------------------------------------
BitField Name: ChipSlr2TempStk
BitField Type: WC
BitField Desc: Chip SLR2 temperature
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ChipTempSticky_ChipSlr2TempStk_Mask                                                         cBit2
#define cAf6_ChipTempSticky_ChipSlr2TempStk_Shift                                                            2

/*--------------------------------------
BitField Name: ChipSlr1TempStk
BitField Type: WC
BitField Desc: Chip SLR1 temperature
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ChipTempSticky_ChipSlr1TempStk_Mask                                                         cBit1
#define cAf6_ChipTempSticky_ChipSlr1TempStk_Shift                                                            1

/*--------------------------------------
BitField Name: ChipSlr0TempStk
BitField Type: WC
BitField Desc: Chip SLR0 temperature
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ChipTempSticky_ChipSlr0TempStk_Mask                                                         cBit0
#define cAf6_ChipTempSticky_ChipSlr0TempStk_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : GLB Debug Mac Loopback Control
Reg Addr   : 0x00_0013
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure debug loopback MAC parameters

------------------------------------------------------------------------------*/
#define cAf6Reg_DebugMacLoopControl_Base                                                              0x000013
#define cAf6Reg_DebugMacLoopControl_WidthVal                                                                32

/*--------------------------------------
BitField Name: MacMroLoopOut
BitField Type: RW
BitField Desc: Set 1 for Mac 10G/1G/100Fx Loop Out
BitField Bits: [1]
--------------------------------------*/
#define cAf6_DebugMacLoopControl_MacMroLoopOut_Mask                                                      cBit1
#define cAf6_DebugMacLoopControl_MacMroLoopOut_Shift                                                         1

/*--------------------------------------
BitField Name: Mac40gLoopIn
BitField Type: RW
BitField Desc: Set 1 for Mac40gLoopIn
BitField Bits: [0]
--------------------------------------*/
#define cAf6_DebugMacLoopControl_Mac40gLoopIn_Mask                                                       cBit0
#define cAf6_DebugMacLoopControl_Mac40gLoopIn_Shift                                                          0


/*---------------------------------------------------------
Global 1G/100M/10M Ethernet Clock Squelching Control
Register Full Name: GLB Eth1G Clock Squelching Control
RTL Instant Name  : GlbEth1GClkSquelControl
Address      : 0x00_0011
Formula      : {N/A}
Description  : This register is used to configure debug PW parameters
Width: 32
Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
Field: [31:16] %% EthGeLinkDownSchelEn %% Each Bit represent for each ETH port, value each bit is as below
     1: Disable 8Khz refout when GE link down occur
     0: Enable 8Khz refout when GE link down occur  %% RW %% 0x0 %% 0x0
Field: [15:0] %% EthGeExcessErrRateSchelEn  %% Each Bit represent for each ETH port, value each bit is as below
     1: Disable 8Khz refout when GE Excessive Error Rate occur
     0: Enable 8Khz refout when GE Excessive Error Rate occur  %% RW %% 0x0 %% 0x0
-----------------------------------------------------------*/
#define cAf6_GlbEth1GClkSquelControl_Base 0x11
#define cAf6_EthGeLinkDownSchelEn_Mask(localId) (cBit16 << (localId))
#define cAf6_EthGeLinkDownSchelEn_Shift(localId) (16UL + (localId))
#define cAf6_EthGeExcessErrRateSchelEn_Mask(localId) (cBit0 << (localId))
#define cAf6_EthGeExcessErrRateSchelEn_Shift(localId) (0UL + (localId))


/*---------------------------------------------------------
Global 10G Ethernet Clock Squelching Control
Register Full Name: GLB Eth10G Clock Squelching Control
RTL Instant Name  : GlbEth10GClkSquelControl
Address      : 0x00_0012
Address is relative address, will be sum with Base_Address for each function block
Formula      : {N/A}
Description  : This register is used to configure debug PW parameters
Width: 8
 Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
Field: [7:6] %% TenGeLocalFaultSchelEn  %% Each Bit represent for each TenGe port0 and port8, value each bit is as below
     1: Disable 8Khz refout when TenGe Local Fault occur
     0: Enable 8Khz refout when TenGe Local Fault occur  %% RW %% 0x0 %% 0x0
Field: [5:4] %% TenGeRemoteFaultSchelEn  %% Each Bit represent for each TenGe port0 and port8, value each bit is as below
     1: Disable 8Khz refout when TenGe Remote Fault occur
     0: Enable 8Khz refout when TenGe Remote Fault occur  %% RW %% 0x0 %% 0x0
Field: [3:2] %% TenGeLossDataSyncSchelEn  %% Each Bit represent for each TenGe port0 and port8, value each bit is as below
     1: Disable 8Khz refout when TenGe Loss of Data Sync occur
     0: Enable 8Khz refout when TenGe Loss of Data Sync occur  %% RW %% 0x0 %% 0x0
Field: [1:0] %% TenGeExcessErrRateSchelEn  %% Each Bit represent for each TenGe port0 and port8, value each bit is as below
     1: Disable 8Khz refout when TenGe Excessive Error Rate occur
     0: Enable 8Khz refout when TenGe Excessive Error Rate occur  %% RW %% 0x0 %% 0x0
-----------------------------------------------------------------*/
#define cAf6_GlbEth10GClkSquelControl_Base 0x12
#define cAf6_TenGeLocalFaultSchelEn_Mask(local10g) (cBit6 << (local10g))
#define cAf6_TenGeLocalFaultSchelEn_Shift(local10g) (6UL + (local10g))
#define cAf6_TenGeRemoteFaultSchelEn_Mask(local10g) (cBit4 << (local10g))
#define cAf6_TenGeRemoteFaultSchelEn_Shift(local10g) (4UL + (local10g))
#define cAf6_TenGeLossDataSyncSchelEn_Mask(local10g) (cBit2 << (local10g))
#define cAf6_TenGeLossDataSyncSchelEn_Shift(local10g) (2UL + (local10g))
#define cAf6_TenGeExcessErrRateSchelEn_Mask(local10g) (cBit0 << (local10g))
#define cAf6_TenGeExcessErrRateSchelEn_Shift(local10g) (local10g)


#endif /* _AF6_REG_AF6CNC0022_RD_GLB_H_ */

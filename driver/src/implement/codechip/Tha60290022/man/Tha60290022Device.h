/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60290022Device.h
 * 
 * Created Date: May 9, 2017
 *
 * Description : Product 60290022
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022DEVICE_H_
#define _THA60290022DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60290022DeviceShouldOpenFullCapacity(AtDevice self);
eBool Tha60290022DeviceRunOnVu9P(AtDevice self);
eBool Tha60290022DeviceCapacity15G(AtDevice self);
eBool Tha60290022DeviceHasKByteFeatureOnly(AtDevice self);
eBool Tha60290022DeviceHwLogicOptimized(AtDevice self);
eBool Tha60290022DeviceDccV2IsSupported(AtDevice self);
eBool Tha60290022DeviceV3OptimizationIsSupported(AtDevice self);
eBool Tha60290022DevicePmc1PageIsSupported(AtDevice self);
eBool Tha60290022DeviceHoTxG1OverWriteIsSupported(AtDevice self);
eBool Tha60290022DeviceDccV4dot1EnableRegAddressIsSupported(AtDevice self);
eBool Tha60290022DeviceClockSquelchingIsSupported(AtDevice self);
eBool Tha60290022DeviceClockEthSquelchingIsSupported(AtDevice self);
eBool Tha60290022DeviceTxIpgIsSupported(AtDevice self);
eBool Tha60290022DeviceDccTxFcsMsbIsSupported(AtDevice self);
eBool Tha60290022DeviceDccSgmiiTxIpg31ByteIsSupported(AtDevice self);
eBool Tha60290022DeviceEth100MFxLfRfSupported(AtDevice self);
eBool Tha60290022DeviceDccHdlcTransmissionBitOrderPerChannelIsSupported(AtDevice self);
eBool Tha60290022DeviceSts192cPrbsSupported(AtDevice self);
eBool Tha60290022DeviceSemIsSupported(AtDevice self);
eBool Tha60290022DeviceOcnSeparatedSectionAndLineDccIsSupported(AtDevice self);
eBool Tha60290022DeviceEthPortFlowControlWaterMarkIsSupported(AtDevice self);
eBool Tha60290022DeviceClockEthSquelchingSelectOnCoreOnlyIsSupported(AtDevice self);
eBool Tha60290022DeviceSts192cTxG1OverrideIsNotUsed(AtDevice self);
eBool Tha60290022DeviceSts192cPrbsInvertionIsSupported(AtDevice self);
uint32 Tha60290022DeviceDcrAcrTxShapperSupportedVersion(AtDevice self);
eBool Tha60290022DeviceClockExtractorMonitorPpmIsSupported(AtDevice self);
eBool Tha60290022DeviceTohSonetSdhModeIsSupported(AtDevice self);
eBool Tha60290022DeviceCesopAutoRxMBitConfigurableIsSupported(AtDevice self);
eBool Tha60290022DeviceSeparateLbitAndLopsPktReplacementSupported(AtDevice self);
eBool Tha60290022DeviceFaceplateEthCounter32bitIsSupported(AtDevice self);
eBool Tha60290022Device100FxSerdesClock31Dot25MhzIsSupported(AtDevice self);
eBool Tha60290022DeviceDccEthMaxLengthConfigurationIsSupported(AtDevice self);
eBool Tha60290022DeviceDccHdlcHasPacketCounterDuringFifoFull(AtDevice self);
eBool Tha60290022DeviceFacePlateSerdesDataMaskOnResetConfigurable(AtDevice self);
uint8 Tha60290022DeviceMapDemapNumLoSlices(AtDevice self);
eBool Tha60290022DeviceHasStandardClearTime(AtDevice self);
eBool Tha60290022DeviceIsNewBerInterrupt(AtDevice self);
eBool Tha60290022DeviceFaceplatSerdesEthHasK30_7Feature(AtDevice self);
uint32 Tha60290022DeviceFaceplatSerdesEthStartVersionSupportTxAlarmForcing(AtDevice self);
eBool Tha60290022DeviceFaceplatSerdesEthExcessiveErrorRatioIsSupported(AtDevice self);
eBool Tha60290022DeviceEthHasSgmiiDiagnosticLogic(AtDevice self);
eBool Tha60290022DeviceSgmiiSohEthUseNewMac(AtDevice self);
uint32 Tha60290022DeviceNumberOfMapLoPwPerSlice(AtDevice self);
eBool Tha60290022DeviceNewTuVcLoopbackIsReady(AtDevice self);
eBool Tha60290022DeviceSts192cIsSupported(AtDevice self);
eBool Tha60290022DevicePdaIsNewOc192cOffset(AtDevice self);
eBool Tha60290022DevicePdaNumJitterBufferBlocksIncreased(AtDevice self);
uint32 Tha60290022DevicePdhStartVersionSupportPrm(AtDevice self);
uint32 Tha60290022DevicePdhStartVersionSupportDe1LomfConsequentialAction(AtDevice self);
uint32 Tha60290022DevicePdhStartVersionSupportDe1IdleCodeInsertion(AtDevice self);
eBool Tha60290022DevicePdhRxHw3BitFeacSignalIsSupported(AtDevice self);
eBool Tha60290022DeviceFaceplateSerdesPmaFarEndLoopbackSupported(AtDevice self);
eBool Tha60290022DeviceKByteSerdesIsControllable(AtDevice self);
eBool Tha60290022DeviceSgmiiSerdesShouldUseMdioLocalLoopback(AtDevice self);
eBool Tha60290022DevicePohHasCpujnreqen(AtDevice self);
uint32 Tha60290022DevicePohStartVersionSupportSeparateRdiErdiS(AtDevice self);
eBool Tha60290022DevicePrbsBertOptimizeIsSupported(AtDevice self);
eBool Tha60290022DevicePwDccKbyteInterruptIsSupported(AtDevice self);
eBool Tha60290022DeviceFaceplateFramerLocalLoopbackSupported(AtDevice self);
eBool Tha60290022DeviceSurFailureHoldOnTimerConfigurable(AtDevice self);
eBool Tha60290022DeviceFaceplateSerdesStm1SamplingFromStm4(AtDevice self);
eBool Tha60290022DeviceModuleBerMeasureTimeEngineIsSupported(AtDevice self);
eBool Tha60290022DeviceFaceplateSerdesHasNewParams(AtDevice self);
eBool Tha60290022DevicePtpSupported(AtDevice self);
eBool Tha60290022DeviceHasApsWtr3SecondResolution(AtDevice self);
eBool Tha60290022DeviceHasSoftResetForSem(AtDevice self);
eBool Tha60290022DeviceNeedMapConfigureWhenBindingVc4_64cToPseudowire(AtDevice self);

/* Debug function */
eAtRet Tha60290022DeviceTxShapperEnable(AtDevice self, eBool enable);
eBool Tha60290022DeviceTxShapperIsEnabled(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022DEVICE_H_ */


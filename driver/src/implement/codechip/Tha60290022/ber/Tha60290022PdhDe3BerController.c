/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60290022PdhDe3BerController.c
 *
 * Created Date: Feb 7, 2018
 *
 * Description : BER controller of DS3/E3 Path
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ber/Tha60210011SdhAuVcBerControllerInternal.h"
#include "../poh/Tha60290022ModulePohV2Reg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PdhDe3BerController
    {
    tTha60210011PdhDe3BerController super;
    }tTha60290022PdhDe3BerController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerControllerMethods m_AtBerControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleBer BerModule(AtBerController self)
    {
    return (AtModuleBer)AtBerControllerModuleGet(self);
    }

static AtPdhChannel De3(AtBerController self)
    {
    return (AtPdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    }

static uint32 BaseAddress(AtBerController self)
    {
    return AtModuleBerBaseAddress(BerModule(self));
    }

static uint32 BerInterruptMaskRegister(AtBerController self)
    {
    AtUnused(self);
    return cAf6Reg_dealm_mskhi_Base;
    }

static uint32 BerCurrentStatusRegister(AtBerController self)
    {
    AtUnused(self);
    return cAf6Reg_dealm_stahi_Base;
    }

static uint32 BerInterruptStatusRegister(AtBerController self)
    {
    AtUnused(self);
    return cAf6Reg_dealm_chghi_Base;
    }

static uint32 DefectHw2Sw(uint32 regVal)
    {
    uint32 defects = 0;
    if (regVal & cAf6_dealm_mskhi_bertcamsk_Mask)
        defects |= cAtPdhDe3AlarmBerTca;
    if (regVal & cAf6_dealm_mskhi_bersdmsk_Mask)
        defects |= cAtPdhDe3AlarmSdBer;
    if (regVal & cAf6_dealm_mskhi_bersfmsk_Mask)
        defects |= cAtPdhDe3AlarmSfBer;

    return defects;
    }

static uint32 BerInterruptOffset(AtBerController self)
    {
    AtPdhChannel pdhChannel = De3(self);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(pdhChannel);
    uint8 slice = 0, hwSts = 0;

    if (ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePoh, &slice, &hwSts) == cAtOk)
        return BaseAddress(self) + (slice + (hwSts * 16UL));

    return cInvalidUint32;
    }

static eAtRet InterruptMaskSet(AtBerController self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = BerInterruptMaskRegister(self) + BerInterruptOffset(self);
    uint32 regVal = AtBerControllerRead(self, regAddr);

    if (defectMask & cAtPdhDe3AlarmBerTca)
        mRegFieldSet(regVal, cAf6_dealm_mskhi_bertcamsk_, (enableMask & cAtPdhDe3AlarmBerTca) ? 1 : 0);
    if (defectMask & cAtPdhDe3AlarmSdBer)
        mRegFieldSet(regVal, cAf6_dealm_mskhi_bersdmsk_, (enableMask & cAtPdhDe3AlarmSdBer) ? 1 : 0);
    if (defectMask & cAtPdhDe3AlarmSfBer)
        mRegFieldSet(regVal, cAf6_dealm_mskhi_bersfmsk_, (enableMask & cAtPdhDe3AlarmSfBer) ? 1 : 0);

    AtBerControllerWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtBerController self)
    {
    uint32 regAddr = BerInterruptMaskRegister(self) + BerInterruptOffset(self);
    uint32 regVal = AtBerControllerRead(self, regAddr);
    return DefectHw2Sw(regVal);
    }

static uint32 InterruptProcess(AtBerController self, uint32 isrContext)
    {
    uint32 offset = BerInterruptOffset(self);
    uint32 regIntrAddr = BerInterruptStatusRegister(self) + offset;
    uint32 regMaskVal = AtBerControllerRead(self, BerInterruptMaskRegister(self) + offset);
    uint32 regCurrVal = AtBerControllerRead(self, BerCurrentStatusRegister(self) + offset);
    uint32 regIntrVal = AtBerControllerRead(self, regIntrAddr);
    uint32 changedDefects = 0;
    uint32 currentDefects = 0;
    AtChannel monitoredDe3 = AtBerControllerMonitoredChannel(self);
    AtUnused(isrContext);

    regIntrVal &= regMaskVal;
    AtBerControllerWrite(self, regIntrAddr, regIntrVal);

    if (regIntrVal & cAf6_dealm_mskhi_bertcamsk_Mask)
        {
        changedDefects |= cAtPdhDe3AlarmBerTca;
        if (regCurrVal & cAf6_dealm_mskhi_bertcamsk_Mask)
            currentDefects |= cAtPdhDe3AlarmBerTca;
        }
    if (regIntrVal & cAf6_dealm_mskhi_bersdmsk_Mask)
        {
        changedDefects |= cAtPdhDe3AlarmSdBer;
        if (regCurrVal & cAf6_dealm_mskhi_bersdmsk_Mask)
            currentDefects |= cAtPdhDe3AlarmSdBer;
        }
    if (regIntrVal & cAf6_dealm_mskhi_bersfmsk_Mask)
        {
        changedDefects |= cAtPdhDe3AlarmSfBer;
        if (regCurrVal & cAf6_dealm_mskhi_bersfmsk_Mask)
            currentDefects |= cAtPdhDe3AlarmSfBer;
        }

    AtChannelAllAlarmListenersCall(monitoredDe3, changedDefects, currentDefects);

    return changedDefects;
    }

static uint32 DefectGet(AtBerController self)
    {
    uint32 regAddr = BerCurrentStatusRegister(self) + BerInterruptOffset(self);
    uint32 regVal = AtBerControllerRead(self, regAddr);
    return DefectHw2Sw(regVal);
    }

static uint32 DefectHistoryRead2Clear(AtBerController self, eBool r2c)
    {
    uint32 regAddr = BerInterruptStatusRegister(self) + BerInterruptOffset(self);
    uint32 regVal = AtBerControllerRead(self, regAddr);
    if (r2c)
        AtBerControllerWrite(self, regAddr, regVal);

    return DefectHw2Sw(regVal);
    }

static uint32 DefectHistoryGet(AtBerController self)
    {
    return DefectHistoryRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtBerController self)
    {
    return DefectHistoryRead2Clear(self, cAtTrue);
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride,  mMethodsGet(self), sizeof(m_AtBerControllerOverride));
        
        mMethodOverride(m_AtBerControllerOverride, InterruptMaskSet);
        mMethodOverride(m_AtBerControllerOverride, InterruptMaskGet);
        mMethodOverride(m_AtBerControllerOverride, InterruptProcess);
        mMethodOverride(m_AtBerControllerOverride, DefectGet);
        mMethodOverride(m_AtBerControllerOverride, DefectHistoryGet);
        mMethodOverride(m_AtBerControllerOverride, DefectHistoryClear);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PdhDe3BerController);
    }

static AtBerController ObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PdhDe3BerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60290022PdhDe3BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, controllerId, channel, berModule);
    }

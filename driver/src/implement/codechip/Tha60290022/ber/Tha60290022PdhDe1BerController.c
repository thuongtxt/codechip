/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60290022PdhDe1BerController.c
 *
 * Created Date: Feb 7, 2018
 *
 * Description : BER controller of DS1/E1 Path
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaPdhDe2De1.h"
#include "../../Tha60210011/ber/Tha60210011SdhAuVcBerControllerInternal.h"
#include "../poh/Tha60290022ModulePohV2Reg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PdhDe1BerController
    {
    tTha60210011PdhDe1BerController super;
    }tTha60290022PdhDe1BerController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerControllerMethods m_AtBerControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleBer BerModule(AtBerController self)
    {
    return (AtModuleBer)AtBerControllerModuleGet(self);
    }

static AtPdhChannel De1(AtBerController self)
    {
    return (AtPdhChannel)AtBerControllerMonitoredChannel(self);
    }

static uint32 BaseAddress(AtBerController self)
    {
    return AtModuleBerBaseAddress(BerModule(self));
    }

static uint32 BerInterruptMaskRegister(AtBerController self)
    {
    AtUnused(self);
    return cAf6Reg_dealm_msklo_Base;
    }

static uint32 BerCurrentStatusRegister(AtBerController self)
    {
    AtUnused(self);
    return cAf6Reg_dealm_stalo_Base;
    }

static uint32 BerInterruptStatusRegister(AtBerController self)
    {
    AtUnused(self);
    return cAf6Reg_dealm_chglo_Base;
    }

static uint32 DefectHw2Sw(uint32 regVal)
    {
    uint32 defects = 0;
    if (regVal & cAf6_dealm_msklo_bertcamsk_Mask)
        defects |= cAtPdhDe1AlarmBerTca;
    if (regVal & cAf6_dealm_msklo_bersdmsk_Mask)
        defects |= cAtPdhDe1AlarmSdBer;
    if (regVal & cAf6_dealm_msklo_bersfmsk_Mask)
        defects |= cAtPdhDe1AlarmSfBer;

    return defects;
    }

static uint32 BerInterruptOffset(AtBerController self)
    {
    AtPdhChannel pdhChannel = De1(self);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(pdhChannel);
    uint8 slice = 0, hwSts = 0, hwVtg, hwVt;

    if (ThaPdhChannelHwIdGet(pdhChannel, cThaModulePoh, &slice, &hwSts) != cAtOk)
        return cInvalidUint32;

    if (sdhChannel)
        {
        hwVtg = AtSdhChannelTug2Get(sdhChannel);
        hwVt = AtSdhChannelTu1xGet(sdhChannel);
        }
    else
        {
        hwVtg = ThaPdhDe2De1HwDe2IdGet((ThaPdhDe1)pdhChannel);
        hwVt = ThaPdhDe2De1HwDe1IdGet((ThaPdhDe1)pdhChannel);
        }

    return BaseAddress(self) + (slice * 8192UL) + (hwSts * 32UL) + (hwVtg * 4UL) + hwVt;
    }

static eAtRet InterruptMaskSet(AtBerController self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = BerInterruptMaskRegister(self) + BerInterruptOffset(self);
    uint32 regVal = AtBerControllerRead(self, regAddr);

    if (defectMask & cAtPdhDe1AlarmBerTca)
        mRegFieldSet(regVal, cAf6_dealm_msklo_bertcamsk_, (enableMask & cAtPdhDe1AlarmBerTca) ? 1 : 0);
    if (defectMask & cAtPdhDe1AlarmSdBer)
        mRegFieldSet(regVal, cAf6_dealm_msklo_bersdmsk_, (enableMask & cAtPdhDe1AlarmSdBer) ? 1 : 0);
    if (defectMask & cAtPdhDe1AlarmSfBer)
        mRegFieldSet(regVal, cAf6_dealm_msklo_bersfmsk_, (enableMask & cAtPdhDe1AlarmSfBer) ? 1 : 0);

    AtBerControllerWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtBerController self)
    {
    uint32 regAddr = BerInterruptMaskRegister(self) + BerInterruptOffset(self);
    uint32 regVal = AtBerControllerRead(self, regAddr);
    return DefectHw2Sw(regVal);
    }

static uint32 InterruptProcess(AtBerController self, uint32 isrContext)
    {
    uint32 offset = BerInterruptOffset(self);
    uint32 regIntrAddr = BerInterruptStatusRegister(self) + offset;
    uint32 regMaskVal = AtBerControllerRead(self, BerInterruptMaskRegister(self) + offset);
    uint32 regCurrVal = AtBerControllerRead(self, BerCurrentStatusRegister(self) + offset);
    uint32 regIntrVal = AtBerControllerRead(self, regIntrAddr);
    uint32 changedDefects = 0;
    uint32 currentDefects = 0;
    AtChannel monitoredDe1 = AtBerControllerMonitoredChannel(self);
    AtUnused(isrContext);

    regIntrVal &= regMaskVal;
    AtBerControllerWrite(self, regIntrAddr, regIntrVal);

    if (regIntrVal & cAf6_dealm_msklo_bertcamsk_Mask)
        {
        changedDefects |= cAtPdhDe1AlarmBerTca;
        if (regCurrVal & cAf6_dealm_msklo_bertcamsk_Mask)
            currentDefects |= cAtPdhDe1AlarmBerTca;
        }
    if (regIntrVal & cAf6_dealm_msklo_bersdmsk_Mask)
        {
        changedDefects |= cAtPdhDe1AlarmSdBer;
        if (regCurrVal & cAf6_dealm_msklo_bersdmsk_Mask)
            currentDefects |= cAtPdhDe1AlarmSdBer;
        }
    if (regIntrVal & cAf6_dealm_msklo_bersfmsk_Mask)
        {
        changedDefects |= cAtPdhDe1AlarmSfBer;
        if (regCurrVal & cAf6_dealm_msklo_bersfmsk_Mask)
            currentDefects |= cAtPdhDe1AlarmSfBer;
        }

    AtChannelAllAlarmListenersCall(monitoredDe1, changedDefects, currentDefects);

    return changedDefects;
    }

static uint32 DefectGet(AtBerController self)
    {
    uint32 regAddr = BerCurrentStatusRegister(self) + BerInterruptOffset(self);
    uint32 regVal = AtBerControllerRead(self, regAddr);
    return DefectHw2Sw(regVal);
    }

static uint32 DefectHistoryRead2Clear(AtBerController self, eBool r2c)
    {
    uint32 regAddr = BerInterruptStatusRegister(self) + BerInterruptOffset(self);
    uint32 regVal = AtBerControllerRead(self, regAddr);
    if (r2c)
        AtBerControllerWrite(self, regAddr, regVal);

    return DefectHw2Sw(regVal);
    }

static uint32 DefectHistoryGet(AtBerController self)
    {
    return DefectHistoryRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtBerController self)
    {
    return DefectHistoryRead2Clear(self, cAtTrue);
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride,  mMethodsGet(self), sizeof(m_AtBerControllerOverride));
        
        mMethodOverride(m_AtBerControllerOverride, InterruptMaskSet);
        mMethodOverride(m_AtBerControllerOverride, InterruptMaskGet);
        mMethodOverride(m_AtBerControllerOverride, InterruptProcess);
        mMethodOverride(m_AtBerControllerOverride, DefectGet);
        mMethodOverride(m_AtBerControllerOverride, DefectHistoryGet);
        mMethodOverride(m_AtBerControllerOverride, DefectHistoryClear);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PdhDe1BerController);
    }

static AtBerController ObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PdhDe1BerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60290022PdhDe1BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, controllerId, channel, berModule);
    }

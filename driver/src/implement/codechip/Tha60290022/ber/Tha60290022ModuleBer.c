/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60290022ModuleBer.c
 *
 * Created Date: Jul 12, 2017
 *
 * Description : BER module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhChannel.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/poh/ThaModulePoh.h"
#include "../../Tha60290021/ber/Tha60290021ModuleBerInternal.h"
#include "../poh/Tha60290022ModulePohReg.h"
#include "../poh/Tha60290022ModulePoh.h"
#include "../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022ModuleBer
    {
    tTha60290021ModuleBer super;
    }tTha60290022ModuleBer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleBerMethods          m_AtModuleBerOverride;
static tTha60210011ModuleBerMethods m_Tha60210011ModuleBerOverride;
static tThaModuleHardBerMethods     m_ThaModuleHardBerOverride;

/* Save super implementation */
static const tAtModuleBerMethods          *m_AtModuleBerMethods          = NULL;
static const tTha60210011ModuleBerMethods *m_Tha60210011ModuleBerMethods = NULL;
static const tThaModuleHardBerMethods     *m_ThaModuleHardBerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BerMeasureStsChannelOffset(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return 10752UL;
    }

static uint32 Imemrwptrsh1Base(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return cAf6Reg_imemrwptrsh1_Base;
    }

static uint32 Imemrwpctrl2Base(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return cAf6Reg_imemrwpctrl2_Base;
    }

static uint32 Threshold1Offset(Tha60210011ModuleBer self, uint32 rate)
    {
    AtUnused(self);
    return rate;
    }

static uint32 Vc1xDsnBerControllerOffset(Tha60210011ModuleBer self, uint32 slice, uint32 hwSts, uint32 tug2Id)
    {
    AtUnused(self);
    return (hwSts * 8UL) + (slice * 512UL) + tug2Id;
    }

static uint32 Vc1xBerControlOffset(Tha60210011ModuleBer self, AtSdhChannel vc1x, uint8 slice, uint8 hwSts)
    {
    return Vc1xDsnBerControllerOffset(self, slice, hwSts, AtSdhChannelTug2Get(vc1x));
    }

static uint32 De1BerControlOffset(Tha60210011ModuleBer self, AtPdhChannel de1, uint8 slice, uint8 hwSts, uint8 tug2Id)
    {
    AtUnused(de1);
    return Vc1xDsnBerControllerOffset(self, slice + 8UL, hwSts, tug2Id);
    }

static uint32 Vc1xDe1CurrentBerOffset(Tha60210011ModuleBer self, uint32 slice, uint32 hwSts, uint32 tug2Id, uint32 tu1xId)
    {
    AtUnused(self);
    return hwSts * 28UL + slice * 1344UL + tug2Id * 4UL + tu1xId;
    }

static uint32 Vc1xCurrentBerOffset(Tha60210011ModuleBer self, AtSdhChannel vc1x, uint8 slice, uint8 hwSts)
    {
    return Vc1xDe1CurrentBerOffset(self, slice, hwSts, AtSdhChannelTug2Get(vc1x), AtSdhChannelTu1xGet(vc1x));
    }

static uint32 De1CurrentBerOffset(Tha60210011ModuleBer self, AtPdhChannel de1, uint8 slice, uint8 hwSts, uint8 tug2Id, uint8 tu1xId)
    {
    AtUnused(de1);
    return Vc1xDe1CurrentBerOffset(self, slice + 8UL, hwSts, tug2Id, tu1xId);
    }

static eAtRet LinePohHwId(Tha60210011ModuleBer self, AtSdhLine line, uint8 *slice, uint8 *hwSts)
    {
    eAtRet ret = m_Tha60210011ModuleBerMethods->LinePohHwId(self, line, slice, hwSts);
    *slice = 8;
    return ret;
    }

static uint32 StsDe3BerControllerOffset(Tha60210011ModuleBer self, uint32 slice, uint32 hwSts)
    {
    AtUnused(self);
    return (hwSts * 8UL) + slice * 512UL;
    }

static uint32 LineBerControlOffset(Tha60210011ModuleBer self, AtSdhLine line, uint8 slice, uint8 hwSts)
    {
    AtUnused(line);
    return StsDe3BerControllerOffset(self, slice, hwSts);
    }

static uint32 De3BerControlOffset(Tha60210011ModuleBer self, AtPdhChannel de3, uint8 slice, uint8 hwSts)
    {
    AtUnused(de3);
    return StsDe3BerControllerOffset(self, slice + 8UL, hwSts);
    }

static uint32 StsBerControlOffset(Tha60210011ModuleBer self, AtSdhChannel channel, uint8 slice, uint8 hwSts)
    {
    AtUnused(channel);
    return StsDe3BerControllerOffset(self, slice, hwSts);
    }

static uint32 BerStsTu3ReportBaseAddress(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return cAf6Reg_ramberrateststu3_Base;
    }

static uint32 StsDe3LineCurrentBerOffset(Tha60210011ModuleBer self, uint32 slice, uint32 hwSts, uint32 tu3Type)
    {
    AtUnused(self);
    return hwSts * 2UL + slice * 128UL + tu3Type;
    }

static uint32 De3CurrentBerOffset(Tha60210011ModuleBer self, AtPdhChannel de3, uint8 slice, uint8 hwSts)
    {
    static const uint32 cTu3Ttype = 1;
    AtUnused(de3);
    return StsDe3LineCurrentBerOffset(self, slice + 8UL, hwSts, cTu3Ttype);
    }

static uint32 StsCurrentBerOffset(Tha60210011ModuleBer self, AtSdhChannel channel, uint8 slice, uint8 hwSts)
    {
    static const uint32 cTu3Ttype = 0;
    AtUnused(channel);
    return StsDe3LineCurrentBerOffset(self, slice, hwSts, cTu3Ttype);
    }

static uint32 LineCurrentBerOffset(Tha60210011ModuleBer self, AtSdhLine line, uint8 slice, uint8 hwSts)
    {
    static const uint32 cTu3Ttype = 0;
    AtUnused(line);
    return StsDe3LineCurrentBerOffset(self, slice, hwSts, cTu3Ttype);
    }

static uint32 Imemrwpctrl1Base(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return cAf6Reg_imemrwpctrl1_Base;
    }

static ThaModulePoh ModulePoh(Tha60210011ModuleBer self)
    {
    return (ThaModulePoh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    }

static eAtRet BerEnable(Tha60210011ModuleBer self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret = m_Tha60210011ModuleBerMethods->BerEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    if (enable)
        return cAtOk;

    return Tha60290022ModulePohBerRequestDdrDisableDoneCheck(ModulePoh(self));
    }

static eBool HasStandardClearTime(Tha60210011ModuleBer self)
    {
    return Tha60290022DeviceHasStandardClearTime(AtModuleDeviceGet((AtModule)self));
    }

static eBool MeasureTimeEngineIsSupported(AtModuleBer self)
    {
    return Tha60290022DeviceModuleBerMeasureTimeEngineIsSupported(AtModuleDeviceGet((AtModule)self));
    }

static eBool IsNewBerInterrupt(ThaModuleHardBer self)
    {
    return Tha60290022DeviceIsNewBerInterrupt(AtModuleDeviceGet((AtModule)self));
    }

static AtBerController De1PathBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de1)
    {
    if (IsNewBerInterrupt(self))
        return Tha60290022PdhDe1BerControllerNew(AtChannelIdGet((AtChannel)de1), (AtChannel)de1, (AtModuleBer)self);

    return m_ThaModuleHardBerMethods->De1PathBerControllerCreate(self, de1);
    }

static AtBerController De3PathBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de3)
    {
    if (IsNewBerInterrupt(self))
        return Tha60290022PdhDe3BerControllerNew(AtChannelIdGet((AtChannel)de3), (AtChannel)de3, (AtModuleBer)self);

    return m_ThaModuleHardBerMethods->De3PathBerControllerCreate(self, de3);
    }

static void OverrideAtModuleBer(AtModuleBer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleBerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleBerOverride, m_AtModuleBerMethods, sizeof(m_AtModuleBerOverride));

        mMethodOverride(m_AtModuleBerOverride, MeasureTimeEngineIsSupported);
        }

    mMethodsSet(self, &m_AtModuleBerOverride);
    }

static void OverrideTha60210011ModuleBer(AtModuleBer self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleBerMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleBerOverride, m_Tha60210011ModuleBerMethods, sizeof(m_Tha60210011ModuleBerOverride));

        mMethodOverride(m_Tha60210011ModuleBerOverride, Imemrwptrsh1Base);
        mMethodOverride(m_Tha60210011ModuleBerOverride, Imemrwpctrl2Base);
        mMethodOverride(m_Tha60210011ModuleBerOverride, Threshold1Offset);
        mMethodOverride(m_Tha60210011ModuleBerOverride, Vc1xBerControlOffset);
        mMethodOverride(m_Tha60210011ModuleBerOverride, De3BerControlOffset);
        mMethodOverride(m_Tha60210011ModuleBerOverride, LinePohHwId);
        mMethodOverride(m_Tha60210011ModuleBerOverride, LineBerControlOffset);
        mMethodOverride(m_Tha60210011ModuleBerOverride, StsBerControlOffset);
        mMethodOverride(m_Tha60210011ModuleBerOverride, Vc1xCurrentBerOffset);
        mMethodOverride(m_Tha60210011ModuleBerOverride, BerStsTu3ReportBaseAddress);
        mMethodOverride(m_Tha60210011ModuleBerOverride, De3CurrentBerOffset);
        mMethodOverride(m_Tha60210011ModuleBerOverride, StsCurrentBerOffset);
        mMethodOverride(m_Tha60210011ModuleBerOverride, LineCurrentBerOffset);
        mMethodOverride(m_Tha60210011ModuleBerOverride, Imemrwpctrl1Base);
        mMethodOverride(m_Tha60210011ModuleBerOverride, De1BerControlOffset);
        mMethodOverride(m_Tha60210011ModuleBerOverride, De1CurrentBerOffset);
        mMethodOverride(m_Tha60210011ModuleBerOverride, HasStandardClearTime);
        mMethodOverride(m_Tha60210011ModuleBerOverride, BerEnable);
        mMethodOverride(m_Tha60210011ModuleBerOverride, BerMeasureStsChannelOffset);
        }

    mMethodsSet(module, &m_Tha60210011ModuleBerOverride);
    }

static void OverrideThaModuleHardBer(AtModuleBer self)
    {
    ThaModuleHardBer module = (ThaModuleHardBer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleHardBerMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardBerOverride, mMethodsGet(module), sizeof(m_ThaModuleHardBerOverride));

        mMethodOverride(m_ThaModuleHardBerOverride, De1PathBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, De3PathBerControllerCreate);
        }

    mMethodsSet(module, &m_ThaModuleHardBerOverride);
    }

static void Override(AtModuleBer self)
    {
    OverrideAtModuleBer(self);
    OverrideTha60210011ModuleBer(self);
    OverrideThaModuleHardBer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleBer);
    }

static AtModuleBer ObjectInit(AtModuleBer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleBerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleBer Tha60290022ModuleBerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleBer newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

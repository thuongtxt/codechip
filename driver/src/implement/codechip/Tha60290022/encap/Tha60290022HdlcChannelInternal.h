/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : Tha60290021HdlcChanelInternal.h
 * 
 * Created Date: Sep 3, 2016
 *
 * Description : HDLC channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022HDLCCHANELINTERNAL_H_
#define _THA60290022HDLCCHANELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/encap/Tha60290021HdlcChannelInternal.h"
#include "Tha60290022HdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022HdlcChannel
    {
    tTha60290021HdlcChannelV4 super;
    }tTha60290022HdlcChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcChannel Tha60290022HdlcChannelObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);


#ifdef __cplusplus
}
#endif
#endif /* _THA60290022HDLCCHANELINTERNAL_H_ */


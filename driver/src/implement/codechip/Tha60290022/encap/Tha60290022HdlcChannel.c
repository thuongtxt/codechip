/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HDLC ENCAP
 *
 * File        : Tha60290021HdlcChannelV5.c
 *
 * Created Date: Apr 16, 2018
 *
 * Description : Implement HDLC encapsulation/decapsulation which support enable/disable
 * DCC insertion separately for section and line layer in the same sonet/sdh line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60290021/ocn/Tha60290021ModuleOcn.h"
#include "../man/Tha60290022Device.h"
#include "Tha60290022HdlcChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021HdlcChannelMethods m_Tha60290021HdlcChannelOverride;

/* Save super implementation */
static const tTha60290021HdlcChannelMethods *m_Tha60290021HdlcChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet TxHwOcnDccInsertionEnable(Tha60290021HdlcChannel self, eBool enable)
    {
    return Tha60290021HdlcChannelTxRsMsDccInsHandle(self, enable);
    }

static eBool HasPacketCounterDuringFifoFull(Tha60290021HdlcChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return Tha60290022DeviceDccHdlcHasPacketCounterDuringFifoFull(device);
    }

static void OverrideTha60290021HdlcChannel(AtHdlcChannel self)
    {
    Tha60290021HdlcChannel dcc = (Tha60290021HdlcChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021HdlcChannelMethods = mMethodsGet(dcc);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021HdlcChannelOverride, m_Tha60290021HdlcChannelMethods, sizeof(tTha60290021HdlcChannelMethods));

        mMethodOverride(m_Tha60290021HdlcChannelOverride, TxHwOcnDccInsertionEnable);
        mMethodOverride(m_Tha60290021HdlcChannelOverride, HasPacketCounterDuringFifoFull);
        }

    mMethodsSet(dcc, &m_Tha60290021HdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideTha60290021HdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022HdlcChannel);
    }

static AtHdlcChannel ObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021HdlcChannelV4ObjectInit(self, channelId, line, layers, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel Tha60290022HdlcChannelNew(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChannel, channelId, line, layers, module);
    }

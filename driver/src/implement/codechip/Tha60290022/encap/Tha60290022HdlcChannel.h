/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60290022HdlcChannel.h
 * 
 * Created Date: May 28, 2018
 *
 * Description : HDLC channel.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022HDLCCHANNEL_H_
#define _THA60290022HDLCCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
AtHdlcChannel Tha60290022HdlcChannelNew(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290022HDLCCHANNEL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : Tha60290022ModuleXc.c
 *
 * Created Date: Apr 16, 2017
 *
 * Description : XC module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/xc/ThaVcCrossConnect.h"
#include "../../Tha60290021/xc/Tha60290021ModuleXcInternal.h"
#include "../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022ModuleXc
    {
    tTha60290021ModuleXc super;
    }tTha60290022ModuleXc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleXcMethods          m_AtModuleXcOverride;
static tThaModuleXcMethods         m_ThaModuleXcOverride;
static tTha60210011ModuleXcMethods m_Tha60210011ModuleXcOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtCrossConnect VcCrossConnectObjectCreate(AtModuleXc self)
    {
    return Tha60290022HoVcCrossConnectNew(self);
    }

static eAtRet HwXcInit(Tha60210011ModuleXc self)
    {
    /* OCN module already flush all of related XC registers. By the way, this
     * product is different with 10G, so it is better not to reuse super logic
     * to avoid register overwritten */
    AtUnused(self);
    return cAtOk;
    }

static uint32 DisconnectSliceId(ThaModuleXc self)
    {
    AtUnused(self);
    return 24;
    }

static eBool BridgeAndRollRemoved(Tha60210011ModuleXc self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (Tha60290022DeviceV3OptimizationIsSupported(device))
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideThaModuleXc(AtModuleXc self)
    {
    ThaModuleXc module = (ThaModuleXc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleXcOverride, mMethodsGet(module), sizeof(m_ThaModuleXcOverride));

        mMethodOverride(m_ThaModuleXcOverride, DisconnectSliceId);
        }

    mMethodsSet(module, &m_ThaModuleXcOverride);
    }

static void OverrideTha60210011ModuleXc(AtModuleXc self)
    {
    Tha60210011ModuleXc module = (Tha60210011ModuleXc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleXcOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleXcOverride));

        mMethodOverride(m_Tha60210011ModuleXcOverride, HwXcInit);
        mMethodOverride(m_Tha60210011ModuleXcOverride, BridgeAndRollRemoved);
        }

    mMethodsSet(module, &m_Tha60210011ModuleXcOverride);
    }

static void OverrideAtModuleXc(AtModuleXc self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleXcOverride, mMethodsGet(self), sizeof(m_AtModuleXcOverride));

        mMethodOverride(m_AtModuleXcOverride, VcCrossConnectObjectCreate);
        }

    mMethodsSet(self, &m_AtModuleXcOverride);
    }

static void Override(AtModuleXc self)
    {
    OverrideAtModuleXc(self);
    OverrideThaModuleXc(self);
    OverrideTha60210011ModuleXc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleXc);
    }

static AtModuleXc ObjectInit(AtModuleXc self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleXcObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleXc Tha60290022ModuleXcNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleXc newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

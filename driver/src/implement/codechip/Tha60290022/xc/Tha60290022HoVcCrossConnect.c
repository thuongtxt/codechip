/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : Tha60290022HoVcCrossConnect.c
 *
 * Created Date: Aug 1, 2017
 *
 * Description : HO VC Cross-connect
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
/*#include "../../../default/ocn/ThaModuleOcn.h"*/
#include "../../../default/sdh/ThaSdhVcInternal.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../Tha60290021/xc/Tha60290021HoVcCrossConnectInternal.h"
#include "../ocn/Tha60290022ModuleOcnReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022HoVcCrossConnect
    {
    tTha60290021HoVcCrossConnect super;
    }tTha60290022HoVcCrossConnect;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaVcCrossConnectMethods     m_ThaVcCrossConnectOverride;

/* To save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 SliceIdPageMask(ThaVcCrossConnect self, uint32 pageId)
    {
    AtUnused(self);

    if (pageId == 0) return cAf6_sxcramctl0_SxcLineIdPage0_Mask;
    if (pageId == 1) return cAf6_sxcramctl0_SxcLineIdPage1_Mask;
    if (pageId == 2) return cAf6_sxcramctl1_SxcLineIdPage2_Mask;

    return 0;
    }

static uint8 SliceIdPageShift(ThaVcCrossConnect self, uint32 pageId)
    {
    AtUnused(self);

    if (pageId == 0) return cAf6_sxcramctl0_SxcLineIdPage0_Shift;
    if (pageId == 1) return cAf6_sxcramctl0_SxcLineIdPage1_Shift;
    if (pageId == 2) return cAf6_sxcramctl1_SxcLineIdPage2_Shift;

    return 0;
    }

static void OverrideThaVcCrossConnect(AtCrossConnect self)
    {
    ThaVcCrossConnect vcCrossConnect = (ThaVcCrossConnect)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcCrossConnectOverride, mMethodsGet(vcCrossConnect), sizeof(m_ThaVcCrossConnectOverride));

        mMethodOverride(m_ThaVcCrossConnectOverride, SliceIdPageMask);
        mMethodOverride(m_ThaVcCrossConnectOverride, SliceIdPageShift);
        }

    mMethodsSet(vcCrossConnect, &m_ThaVcCrossConnectOverride);
    }

static void Override(AtCrossConnect self)
    {
    OverrideThaVcCrossConnect(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022HoVcCrossConnect);
    }

static AtCrossConnect ObjectInit(AtCrossConnect self, AtModuleXc module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021HoVcCrossConnectObjectInit(self, module) == NULL)
        return NULL;

    /* Over-ride */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtCrossConnect Tha60290022HoVcCrossConnectNew(AtModuleXc module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtCrossConnect newXc = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newXc == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newXc, module);
    }

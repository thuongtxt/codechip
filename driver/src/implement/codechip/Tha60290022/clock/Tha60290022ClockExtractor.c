/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : Tha60290022ClockExtractor.c
 *
 * Created Date: Feb 27, 2018
 *
 * Description : Implement clock extractor for 60290022
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022ClockExtractorInternal.h"
#include "../../Tha60290021/man/Tha60290021Device.h"
#include "../ocn/Tha60290022ModuleOcn.h"
#include "../man/Tha60290022DeviceReg.h"
#include "../man/Tha60290022DeviceTopReg.h"
#include "../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/
#define cTenGeSerdesId_0 0
#define cTenGeSerdesId_1 8

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290022ClockExtractor)self)
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290022ClockExtractorMethods m_methods;
static tAtClockExtractorMethods  m_AtClockExtractorOverride;
static tThaClockExtractorMethods m_ThaClockExtractorOverride;

/* Supper implementations */
static const tAtClockExtractorMethods  *m_AtClockExtractorMethods  = NULL;
static const tThaClockExtractorMethods *m_ThaClockExtractorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 SonetSquelchingOptionMin(void)
    {
    return cAtClockExtractorSquelchingLof | cAtClockExtractorSquelchingLos;
    }

static uint32 SonetSquelchingOptionMax(void)
    {
    return cAtClockExtractorSquelchingAis | cAtClockExtractorSquelchingLof | cAtClockExtractorSquelchingLos;
    }

static eAtModuleClockRet SonetSquelchingOptionCheck(eAtClockExtractorSquelching options)
    {
    /* HW always squelch on LOS/LOF, only configurable for AIS-L */
    if ((options >= SonetSquelchingOptionMin()) &&
        (options <= SonetSquelchingOptionMax()))
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static uint32 TenGeEthSquelchingOptionMin(void)
    {
    return cAtClockExtractorSquelchingEthLf;
    }

static uint32 TenGeEthSquelchingOptionMax(void)
    {
    return cAtClockExtractorSquelchingEthLf | cAtClockExtractorSquelchingEthRf |
            cAtClockExtractorSquelchingEthLossync | cAtClockExtractorSquelchingEthEer;
    }

static eAtModuleClockRet TenGeEthSquelchingOptionCheck(eAtClockExtractorSquelching options)
    {
    if (options == cAtClockExtractorSquelchingNone)
        return cAtOk;

    if ((options >= TenGeEthSquelchingOptionMin()) &&
        (options <= TenGeEthSquelchingOptionMax()))
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static uint32 GeEthSquelchingOptionMin(void)
    {
    return cAtClockExtractorSquelchingEthEer;
    }

static uint32 GeEthSquelchingOptionMax(void)
    {
    return cAtClockExtractorSquelchingEthEer | cAtClockExtractorSquelchingEthLinkDown;
    }

static eAtModuleClockRet GeEthSquelchingOptionCheck(eAtClockExtractorSquelching options)
    {
    if (options == cAtClockExtractorSquelchingNone)
        return cAtOk;

    if ((options >= GeEthSquelchingOptionMin()) &&
        (options <= GeEthSquelchingOptionMax()))
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static uint32 Eth100MSquelchingOptionMin(void)
    {
    return cAtClockExtractorSquelchingEthLf;
    }

static uint32 Eth100MSquelchingOptionMax(void)
    {
    return cAtClockExtractorSquelchingEthLf;
    }

static eAtModuleClockRet Eth100MSquelchingOptionCheck(eAtClockExtractorSquelching options)
    {
    if (options == cAtClockExtractorSquelchingNone)
        return cAtOk;

    if ((options >= Eth100MSquelchingOptionMin()) &&
        (options <= Eth100MSquelchingOptionMax()))
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static ThaModuleOcn ModuleOcn(ThaClockExtractor self)
    {
    AtModule moduleClock = (AtModule)AtClockExtractorModuleGet((AtClockExtractor)self);
    ThaModuleOcn moduleOcn = (ThaModuleOcn)AtDeviceModuleGet(AtModuleDeviceGet(moduleClock), cThaModuleOcn);
    return moduleOcn;
    }

static ThaModuleClock ModuleClock(Tha60290022ClockExtractor self)
    {
    ThaModuleClock moduleClock = (ThaModuleClock)AtClockExtractorModuleGet((AtClockExtractor)self);
    return moduleClock;
    }

static eAtModuleClockRet Hw1GeSerdesClockSquelOptionSet(Tha60290022ClockExtractor self, AtSerdesController serdes, eAtClockExtractorSquelching options)
    {
    uint32 address = cAf6_GlbEth1GClkSquelControl_Base;
    uint32 regVal = mModuleHwRead(ModuleClock(self), address);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    uint32 errField_Mask, errField_Shift, errBinVal = 0, linkDownField_Mask, linkDownField_Shift, linkDownBinVal = 0;

    errField_Mask = cAf6_EthGeExcessErrRateSchelEn_Mask(serdesId);
    errField_Shift = cAf6_EthGeExcessErrRateSchelEn_Shift(serdesId);
    if (options & cAtClockExtractorSquelchingEthEer)
        errBinVal = 1;

    linkDownField_Mask = cAf6_EthGeLinkDownSchelEn_Mask(serdesId);
    linkDownField_Shift = cAf6_EthGeLinkDownSchelEn_Shift(serdesId);
    if (options & cAtClockExtractorSquelchingEthLinkDown)
        linkDownBinVal = 1;

    mRegFieldSet(regVal, errField_, errBinVal);
    mRegFieldSet(regVal, linkDownField_, linkDownBinVal);
    mModuleHwWrite(ModuleClock(self), address, regVal);
    return cAtOk;
    }

static eAtClockExtractorSquelching Hw1GeSerdesClockSquelOptionGet(Tha60290022ClockExtractor self, AtSerdesController serdes)
    {
    eAtClockExtractorSquelching options = cAtClockExtractorSquelchingNone;
    uint32 address = cAf6_GlbEth1GClkSquelControl_Base;
    uint32 regVal = mModuleHwRead(ModuleClock(self), address);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    uint32 errField_Mask, errField_Shift, errBinVal = 0, linkDownField_Mask, linkDownField_Shift, linkDownBinVal = 0;

    errField_Mask = cAf6_EthGeExcessErrRateSchelEn_Mask(serdesId);
    errField_Shift = cAf6_EthGeExcessErrRateSchelEn_Shift(serdesId);
    errBinVal = mRegField(regVal, errField_);
    if (errBinVal == 1)
        options |= cAtClockExtractorSquelchingEthEer;

    linkDownField_Mask = cAf6_EthGeLinkDownSchelEn_Mask(serdesId);
    linkDownField_Shift = cAf6_EthGeLinkDownSchelEn_Shift(serdesId);
    linkDownBinVal = mRegField(regVal, linkDownField_);
    if (linkDownBinVal == 1)
        options |= cAtClockExtractorSquelchingEthLinkDown;

    return options;
    }

static eAtModuleClockRet Hw100MSerdesClockSquelOptionSet(Tha60290022ClockExtractor self, AtSerdesController serdes, eAtClockExtractorSquelching options)
    {
    uint32 address = cAf6_GlbEth1GClkSquelControl_Base;
    uint32 regVal = mModuleHwRead(ModuleClock(self), address);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    uint32 linkDownField_Mask, linkDownField_Shift, linkDownBinVal = 0;

    linkDownField_Mask = cAf6_EthGeLinkDownSchelEn_Mask(serdesId);
    linkDownField_Shift = cAf6_EthGeLinkDownSchelEn_Shift(serdesId);
    if (options & cAtClockExtractorSquelchingEthLf)
        linkDownBinVal = 1;

    mRegFieldSet(regVal, linkDownField_, linkDownBinVal);
    mModuleHwWrite(ModuleClock(self), address, regVal);
    return cAtOk;
    }

static eAtClockExtractorSquelching Hw100MSerdesClockSquelOptionGet(Tha60290022ClockExtractor self, AtSerdesController serdes)
    {
    eAtClockExtractorSquelching options = cAtClockExtractorSquelchingNone;
    uint32 address = cAf6_GlbEth1GClkSquelControl_Base;
    uint32 regVal = mModuleHwRead(ModuleClock(self), address);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    uint32 linkDownField_Mask, linkDownField_Shift, linkDownBinVal = 0;

    linkDownField_Mask = cAf6_EthGeLinkDownSchelEn_Mask(serdesId);
    linkDownField_Shift = cAf6_EthGeLinkDownSchelEn_Shift(serdesId);
    linkDownBinVal = mRegField(regVal, linkDownField_);
    if (linkDownBinVal == 1)
        options |= cAtClockExtractorSquelchingEthLf;

    return options;
    }

static uint8 TenGeSerdesIdToLocalHwId(uint8 serdesId)
    {
    if (serdesId == cTenGeSerdesId_0)
        return 0;
    if (serdesId == cTenGeSerdesId_1)
        return 1;
    return 0;
    }

static eAtModuleClockRet HwTenGeSerdesClockSquelOptionSet(Tha60290022ClockExtractor self, AtSerdesController serdes, eAtClockExtractorSquelching options)
    {
    uint32 address = cAf6_GlbEth10GClkSquelControl_Base;
    uint32 regVal = mModuleHwRead(ModuleClock(self), address);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    uint32 lfField_Mask, lfField_Shift, lfBinVal = 0;
    uint32 rfField_Mask, rfField_Shift, rfBinVal = 0;
    uint32 lossyncField_Mask, lossyncField_Shift, lossyncBinVal = 0;
    uint32 errField_Mask, errField_Shift, errBinVal = 0;
    uint8 tenGeLocalHwId = TenGeSerdesIdToLocalHwId(serdesId);

    lfField_Mask = cAf6_TenGeLocalFaultSchelEn_Mask(tenGeLocalHwId);
    lfField_Shift = cAf6_TenGeLocalFaultSchelEn_Shift(tenGeLocalHwId);
    if (options & cAtClockExtractorSquelchingEthLf)
        lfBinVal = 1;

    rfField_Mask = cAf6_TenGeRemoteFaultSchelEn_Mask(tenGeLocalHwId);
    rfField_Shift = cAf6_TenGeRemoteFaultSchelEn_Shift(tenGeLocalHwId);
    if (options & cAtClockExtractorSquelchingEthRf)
        rfBinVal = 1;

    lossyncField_Mask = cAf6_TenGeLossDataSyncSchelEn_Mask(tenGeLocalHwId);
    lossyncField_Shift = cAf6_TenGeLossDataSyncSchelEn_Shift(tenGeLocalHwId);
    if (options & cAtClockExtractorSquelchingEthLossync)
        lossyncBinVal = 1;

    errField_Mask = cAf6_TenGeExcessErrRateSchelEn_Mask(tenGeLocalHwId);
    errField_Shift = cAf6_TenGeExcessErrRateSchelEn_Shift(tenGeLocalHwId);
    if (options & cAtClockExtractorSquelchingEthEer)
        errBinVal = 1;

    mRegFieldSet(regVal, lfField_, lfBinVal);
    mRegFieldSet(regVal, rfField_, rfBinVal);
    mRegFieldSet(regVal, lossyncField_, lossyncBinVal);
    mRegFieldSet(regVal, errField_, errBinVal);
    mModuleHwWrite(ModuleClock(self), address, regVal);
    return cAtOk;
    }

static eAtClockExtractorSquelching HwTenGeSerdesClockSquelOptionGet(Tha60290022ClockExtractor self, AtSerdesController serdes)
    {
    eAtClockExtractorSquelching options = cAtClockExtractorSquelchingNone;
    uint32 address = cAf6_GlbEth10GClkSquelControl_Base;
    uint32 regVal = mModuleHwRead(ModuleClock(self), address);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    uint32 lfField_Mask, lfField_Shift, lfBinVal = 0;
    uint32 rfField_Mask, rfField_Shift, rfBinVal = 0;
    uint32 lossyncField_Mask, lossyncField_Shift, lossyncBinVal = 0;
    uint32 errField_Mask, errField_Shift, errBinVal = 0;
    uint8 tenGeLocalHwId = TenGeSerdesIdToLocalHwId(serdesId);

    lfField_Mask = cAf6_TenGeLocalFaultSchelEn_Mask(tenGeLocalHwId);
    lfField_Shift = cAf6_TenGeLocalFaultSchelEn_Shift(tenGeLocalHwId);
    lfBinVal = mRegField(regVal, lfField_);
    if (lfBinVal == 1)
        options |= cAtClockExtractorSquelchingEthLf;

    rfField_Mask = cAf6_TenGeRemoteFaultSchelEn_Mask(tenGeLocalHwId);
    rfField_Shift = cAf6_TenGeRemoteFaultSchelEn_Shift(tenGeLocalHwId);
    rfBinVal = mRegField(regVal, rfField_);
    if (rfBinVal == 1)
        options |= cAtClockExtractorSquelchingEthRf;

    lossyncField_Mask = cAf6_TenGeLossDataSyncSchelEn_Mask(tenGeLocalHwId);
    lossyncField_Shift = cAf6_TenGeLossDataSyncSchelEn_Shift(tenGeLocalHwId);
    lossyncBinVal = mRegField(regVal, lossyncField_);
    if (lossyncBinVal == 1)
        options |= cAtClockExtractorSquelchingEthLossync;

    errField_Mask = cAf6_TenGeExcessErrRateSchelEn_Mask(tenGeLocalHwId);
    errField_Shift = cAf6_TenGeExcessErrRateSchelEn_Shift(tenGeLocalHwId);
    errBinVal = mRegField(regVal, errField_);
    if (errBinVal == 1)
        options |= cAtClockExtractorSquelchingEthEer;

    return options;
    }

static eAtModuleClockRet HwGeAnd100MSerdesClockSquelOptionDefault(ThaClockExtractor self, AtSerdesController serdes)
    {
    if (Tha60290021ClockExtractorIs1GeSerdes(serdes))
        return mMethodsGet(mThis(self))->Hw1GeSerdesClockSquelOptionSet(mThis(self), serdes, GeEthSquelchingOptionMax());
    if (Tha60290021ClockExtractorIs100MSerdes(serdes))
        return mMethodsGet(mThis(self))->Hw100MSerdesClockSquelOptionSet(mThis(self), serdes, Eth100MSquelchingOptionMax());
    return mMethodsGet(mThis(self))->Hw1GeSerdesClockSquelOptionSet(mThis(self), serdes, cAtClockExtractorSquelchingNone);
    }

static eAtModuleClockRet HwTenGeSerdesClockSquelOptionDefault(ThaClockExtractor self, AtSerdesController serdes)
    {
    if (Tha60290021ClockExtractorIsTenGeSerdes(serdes))
        return mMethodsGet(mThis(self))->HwTenGeSerdesClockSquelOptionSet(mThis(self), serdes, TenGeEthSquelchingOptionMax());
    return mMethodsGet(mThis(self))->HwTenGeSerdesClockSquelOptionSet(mThis(self), serdes, cAtClockExtractorSquelchingNone);
    }

static eAtModuleClockRet HwSonetSerdesClockSquelOptionDefault(ThaClockExtractor self, AtSerdesController serdes)
    {
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    if (Tha60290021ClockExtractorIsSonetSerdes(serdes))
        return Tha60290022ModuleOcnClockExtractorSquelchingAisLEnable(ModuleOcn(self), serdesId, cAtTrue);
    return Tha60290022ModuleOcnClockExtractorSquelchingAisLEnable(ModuleOcn(self), serdesId, cAtFalse);
    }

static eAtModuleClockRet SonetSerdesSquelchingOptionSet(AtClockExtractor self, eAtClockExtractorSquelching options)
    {
    eAtRet ret = SonetSquelchingOptionCheck(options);
    AtSerdesController serdes = AtClockExtractorSerdesGet(self);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);

    if (ret != cAtOk)
        return ret;

    if (options & cAtClockExtractorSquelchingAis)
        return Tha60290022ModuleOcnClockExtractorSquelchingAisLEnable(ModuleOcn((ThaClockExtractor)self), serdesId, cAtTrue);

    return Tha60290022ModuleOcnClockExtractorSquelchingAisLEnable(ModuleOcn((ThaClockExtractor)self), serdesId, cAtFalse);
    }

static eAtClockExtractorSquelching SonetSerdesSquelchingOptionGet(AtClockExtractor self)
    {
    AtSerdesController serdes = AtClockExtractorSerdesGet(self);
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    eBool isAisLSquel = Tha60290022ModuleOcnClockExtractorSquelchingAisLIsEnabled(ModuleOcn((ThaClockExtractor)self), serdesId);

    if (isAisLSquel)
        return (cAtClockExtractorSquelchingAis | SonetSquelchingOptionMin());

    return SonetSquelchingOptionMin();
    }

static eAtModuleClockRet SonetLineSquelchingOptionSet(AtClockExtractor self, eAtClockExtractorSquelching options)
    {
    eAtRet ret = SonetSquelchingOptionCheck(options);
    AtSdhLine sdhLine = AtClockExtractorSdhLineGet(self);
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)sdhLine);

    if (ret != cAtOk)
        return ret;

    if (options & cAtClockExtractorSquelchingAis)
        return Tha60290022ModuleOcnClockExtractorSquelchingAisLEnable(ModuleOcn((ThaClockExtractor)self), lineId, cAtTrue);

    return Tha60290022ModuleOcnClockExtractorSquelchingAisLEnable(ModuleOcn((ThaClockExtractor)self), lineId, cAtFalse);
    }

static eAtClockExtractorSquelching SonetLineSquelchingOptionGet(AtClockExtractor self)
    {
    AtSdhLine sdhLine = AtClockExtractorSdhLineGet(self);
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)sdhLine);
    eBool isAisLSquel = Tha60290022ModuleOcnClockExtractorSquelchingAisLIsEnabled(ModuleOcn((ThaClockExtractor)self), lineId);

    if (isAisLSquel)
        return (cAtClockExtractorSquelchingAis | SonetSquelchingOptionMin());

    return SonetSquelchingOptionMin();
    }

static eAtModuleClockRet TenGeEthSerdesSquelchingOptionSet(AtClockExtractor self, eAtClockExtractorSquelching options)
    {
    eAtRet ret = TenGeEthSquelchingOptionCheck(options);
    AtSerdesController serdes = AtClockExtractorSerdesGet(self);

    if (ret != cAtOk)
        return ret;

    return mMethodsGet(mThis(self))->HwTenGeSerdesClockSquelOptionSet(mThis(self), serdes, options);
    }

static eAtClockExtractorSquelching TenGeEthSerdesSquelchingOptionGet(AtClockExtractor self)
    {
    AtSerdesController serdes = AtClockExtractorSerdesGet(self);

    return mMethodsGet(mThis(self))->HwTenGeSerdesClockSquelOptionGet(mThis(self), serdes);
    }

static eAtModuleClockRet GeEthSerdesSquelchingOptionSet(AtClockExtractor self, eAtClockExtractorSquelching options)
    {
    eAtRet ret = GeEthSquelchingOptionCheck(options);
    AtSerdesController serdes = AtClockExtractorSerdesGet(self);

    if (ret != cAtOk)
        return ret;

    return mMethodsGet(mThis(self))->Hw1GeSerdesClockSquelOptionSet(mThis(self), serdes, options);
    }

static eAtClockExtractorSquelching GeEthSerdesSquelchingOptionGet(AtClockExtractor self)
    {
    AtSerdesController serdes = AtClockExtractorSerdesGet(self);

    return mMethodsGet(mThis(self))->Hw1GeSerdesClockSquelOptionGet(mThis(self), serdes);
    }

static eAtModuleClockRet Eth100MSerdesSquelchingOptionSet(AtClockExtractor self, eAtClockExtractorSquelching options)
    {
    eAtRet ret = Eth100MSquelchingOptionCheck(options);
    AtSerdesController serdes = AtClockExtractorSerdesGet(self);

    if (ret != cAtOk)
        return ret;

    return mMethodsGet(mThis(self))->Hw100MSerdesClockSquelOptionSet(mThis(self), serdes, options);
    }

static eAtClockExtractorSquelching Eth100MSerdesSquelchingOptionGet(AtClockExtractor self)
    {
    AtSerdesController serdes = AtClockExtractorSerdesGet(self);

    return mMethodsGet(mThis(self))->Hw100MSerdesClockSquelOptionGet(mThis(self), serdes);
    }

static eBool IsClockFromSonetSerdes(AtClockExtractor self)
    {
    AtSerdesController serdes = AtClockExtractorSerdesGet(self);

    if (!serdes)
        return cAtFalse;

    return Tha60290021ClockExtractorIsSonetSerdes(serdes);
    }

static eBool IsEthClockSquelchingSupported(AtClockExtractor self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtClockExtractorModuleGet(self));
    if (Tha60290022DeviceClockEthSquelchingIsSupported(device))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsClockFromTenGeEthSerdes(AtClockExtractor self)
    {
    AtSerdesController serdes = AtClockExtractorSerdesGet(self);

    if (!serdes)
        return cAtFalse;

    if (!mMethodsGet(mThis(self))->IsValidTenGeSerdesId(mThis(self), serdes))
        return cAtFalse;

    return Tha60290021ClockExtractorIsTenGeSerdes(serdes);
    }

static eBool IsClockFromGeEthSerdes(AtClockExtractor self)
    {
    AtSerdesController serdes = AtClockExtractorSerdesGet(self);

    if (!serdes)
        return cAtFalse;

    return Tha60290021ClockExtractorIs1GeSerdes(serdes);
    }

static eBool IsClockFromEth100MSerdes(AtClockExtractor self)
    {
    AtSerdesController serdes = AtClockExtractorSerdesGet(self);

    if (!serdes)
        return cAtFalse;

    return Tha60290021ClockExtractorIs100MSerdes(serdes);
    }

static eBool IsClockFromSonetLine(AtClockExtractor self)
    {
    AtSdhLine sdhLine = AtClockExtractorSdhLineGet(self);

    if (sdhLine)
        return cAtTrue;

    return cAtFalse;
    }

static eBool SquelchingIsSupported(AtClockExtractor self)
    {
    if (IsClockFromSonetSerdes(self) || IsClockFromSonetLine(self))
        return cAtTrue;

    if (IsClockFromTenGeEthSerdes(self) && IsEthClockSquelchingSupported(self))
        return cAtTrue;

    if (IsClockFromGeEthSerdes(self) && IsEthClockSquelchingSupported(self))
        return cAtTrue;

    if (IsClockFromEth100MSerdes(self) && IsEthClockSquelchingSupported(self))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModuleClockRet SquelchingOptionSet(AtClockExtractor self, eAtClockExtractorSquelching options)
    {
    if (IsClockFromSonetSerdes(self))
        return SonetSerdesSquelchingOptionSet(self, options);

    if (IsClockFromSonetLine(self))
        return SonetLineSquelchingOptionSet(self, options);

    if (IsClockFromTenGeEthSerdes(self) && IsEthClockSquelchingSupported(self))
        return TenGeEthSerdesSquelchingOptionSet(self, options);

    if (IsClockFromGeEthSerdes(self) && IsEthClockSquelchingSupported(self))
        return GeEthSerdesSquelchingOptionSet(self, options);

    if (IsClockFromEth100MSerdes(self) && IsEthClockSquelchingSupported(self))
        return Eth100MSerdesSquelchingOptionSet(self, options);

    return cAtErrorModeNotSupport;
    }

static eAtClockExtractorSquelching SquelchingOptionGet(AtClockExtractor self)
    {
    if (IsClockFromSonetSerdes(self))
        return SonetSerdesSquelchingOptionGet(self);

    if (IsClockFromSonetLine(self))
        return SonetLineSquelchingOptionGet(self);

    if (IsClockFromTenGeEthSerdes(self) && IsEthClockSquelchingSupported(self))
        return TenGeEthSerdesSquelchingOptionGet(self);

    if (IsClockFromGeEthSerdes(self) && IsEthClockSquelchingSupported(self))
        return GeEthSerdesSquelchingOptionGet(self);

    if (IsClockFromEth100MSerdes(self) && IsEthClockSquelchingSupported(self))
        return Eth100MSerdesSquelchingOptionGet(self);

    return cAtClockExtractorSquelchingNone;
    }

static uint32 OutputCounterGet(AtClockExtractor self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtClockExtractorModuleGet(self));
    uint8 clockId = AtClockExtractorIdGet(self);
    return Tha6029DeviceRefout8kStatusCounter(device, clockId);
    }

static uint32 TopRegisterWithLocalAddress(AtClockExtractor self, uint32 localAddress)
    {
    AtUnused(self);
    return localAddress + cTopBaseAddress;
    }

static int64 OutputPmmGet(AtClockExtractor self)
    {
    /*Number of real system clock cycle in 200 cycle 8Khz: SYSPPMCYCLE = (194400*200)/8 = 4860000
      if (RefPpmCycle > SYSPPMCYCLE) then NEGATIVE_PPM = (RefPpmCycle - SYSPPMCYCLE)*1000000/SYSPPMCYCLE
      else  POSITIVE_PPM = (SYSPPMCYCLE - RefPpmCycle)*1000000/SYSPPMCYCLE*/
    const int64 SYSPPMCYCLE = 4860000;
    uint8 clockId = AtClockExtractorIdGet(self);
    uint32 localAddress = (uint32)(cAf6Reg_i_status_Base + clockId);
    uint32 address = TopRegisterWithLocalAddress(self, localAddress);
    uint32 regVal = mModuleHwRead(ModuleClock(mThis(self)), address);
    uint32 RefPpmCycle = mRegField(regVal, cAf6_i_status_RefPpmCycle_);
    int64 ret = 0;

    if (RefPpmCycle > SYSPPMCYCLE)
        ret = ret - (((RefPpmCycle - SYSPPMCYCLE)*1000000)/SYSPPMCYCLE);
    else
        ret = (((SYSPPMCYCLE - RefPpmCycle)*1000000)/SYSPPMCYCLE);

    return ret;
    }

static void Debug(AtClockExtractor self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ModuleClock(mThis(self)));

    if (Tha60290022DeviceClockExtractorMonitorPpmIsSupported(device))
        AtPrintf("Clock extractor #%d, output counter: %lld ppm\r\n", AtClockExtractorIdGet(self), OutputPmmGet(self));
    else
        AtPrintf("Clock extractor #%d, output counter: %d Hz\r\n", AtClockExtractorIdGet(self), AtClockExtractorOutputCounterGet(self));
    }

static eBool IsValidTenGeSerdesId(Tha60290022ClockExtractor self, AtSerdesController serdes)
    {
    uint8 serdesId = (uint8)AtSerdesControllerIdGet(serdes);
    AtUnused(self);
    if ((serdesId == cTenGeSerdesId_0) || (serdesId == cTenGeSerdesId_1))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet EthSerdesDefaultSquelchingOptionSet(ThaClockExtractor self, AtSerdesController serdes)
    {
    eAtRet ret = cAtOk;

    if (IsEthClockSquelchingSupported((AtClockExtractor)self))
        ret |= HwGeAnd100MSerdesClockSquelOptionDefault(self, serdes);

    if (IsEthClockSquelchingSupported((AtClockExtractor)self) && mMethodsGet(mThis(self))->IsValidTenGeSerdesId(mThis(self), serdes))
        ret |= HwTenGeSerdesClockSquelOptionDefault(self, serdes);

    return ret;
    }

static eAtModuleClockRet HwSerdesClockExtract(ThaClockExtractor self, AtSerdesController serdes)
    {
    eAtModuleClockRet ret = m_ThaClockExtractorMethods->HwSerdesClockExtract(self, serdes);

    if (ret != cAtOk)
        return ret;

    ret |= HwSonetSerdesClockSquelOptionDefault(self, serdes);

    ret |= Tha60290022ClockExtractorEthSerdesDefaultSquelchingOptionSet(self, serdes);

    return ret;
    }

static eAtModuleClockRet HwSdhLineClockExtract(ThaClockExtractor self, AtSdhLine line)
    {
    eAtModuleClockRet ret = m_ThaClockExtractorMethods->HwSdhLineClockExtract(self, line);
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)line);

    if (ret != cAtOk)
        return ret;

    return Tha60290022ModuleOcnClockExtractorSquelchingAisLEnable(ModuleOcn(self), lineId, cAtTrue);
    }

static void MethodsInit(AtClockExtractor self)
    {
    Tha60290022ClockExtractor extractor = (Tha60290022ClockExtractor)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, IsValidTenGeSerdesId);
        mMethodOverride(m_methods, Hw1GeSerdesClockSquelOptionSet);
        mMethodOverride(m_methods, Hw1GeSerdesClockSquelOptionGet);
        mMethodOverride(m_methods, Hw100MSerdesClockSquelOptionSet);
        mMethodOverride(m_methods, Hw100MSerdesClockSquelOptionGet);
        mMethodOverride(m_methods, HwTenGeSerdesClockSquelOptionSet);
        mMethodOverride(m_methods, HwTenGeSerdesClockSquelOptionGet);
        }

    mMethodsSet(extractor, &m_methods);
    }

static void OverrideAtClockExtractor(AtClockExtractor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtClockExtractorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtClockExtractorOverride, m_AtClockExtractorMethods, sizeof(m_AtClockExtractorOverride));

        mMethodOverride(m_AtClockExtractorOverride, SquelchingIsSupported);
        mMethodOverride(m_AtClockExtractorOverride, SquelchingOptionSet);
        mMethodOverride(m_AtClockExtractorOverride, SquelchingOptionGet);
        mMethodOverride(m_AtClockExtractorOverride, OutputCounterGet);
        mMethodOverride(m_AtClockExtractorOverride, Debug);
        }

    mMethodsSet(self, &m_AtClockExtractorOverride);
    }

static void OverrideThaClockExtractor(AtClockExtractor self)
    {
    ThaClockExtractor extractor = (ThaClockExtractor) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClockExtractorMethods = mMethodsGet(extractor);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockExtractorOverride, m_ThaClockExtractorMethods, sizeof(m_ThaClockExtractorOverride));

        mMethodOverride(m_ThaClockExtractorOverride, HwSerdesClockExtract);
        mMethodOverride(m_ThaClockExtractorOverride, HwSdhLineClockExtract);
        }

    mMethodsSet(extractor, &m_ThaClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideAtClockExtractor(self);
    OverrideThaClockExtractor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ClockExtractor);
    }

AtClockExtractor Tha60290022ClockExtractorObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtClockExtractor Tha60290022ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ClockExtractorObjectInit(newExtractor, clockModule, extractorId);
    }

eAtRet Tha60290022ClockExtractorEthSerdesDefaultSquelchingOptionSet(ThaClockExtractor self, AtSerdesController serdes)
    {
    return EthSerdesDefaultSquelchingOptionSet(self, serdes);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60290022ClockExtractorInternal.h
 * 
 * Created Date: Apr 9, 2018
 *
 * Description : Internal data of the 60290022 clock extractor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022CLOCKEXTRACTORINTERNAL_H_
#define _THA60290022CLOCKEXTRACTORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/clock/Tha60290021ClockExtractorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ClockExtractor *Tha60290022ClockExtractor;

typedef struct tTha60290022ClockExtractorMethods
    {
    eBool (*IsValidTenGeSerdesId)(Tha60290022ClockExtractor self, AtSerdesController serdes);
    eAtModuleClockRet (*Hw1GeSerdesClockSquelOptionSet)(Tha60290022ClockExtractor self, AtSerdesController serdes, eAtClockExtractorSquelching options);
    eAtClockExtractorSquelching (*Hw1GeSerdesClockSquelOptionGet)(Tha60290022ClockExtractor self, AtSerdesController serdes);
    eAtModuleClockRet (*Hw100MSerdesClockSquelOptionSet)(Tha60290022ClockExtractor self, AtSerdesController serdes, eAtClockExtractorSquelching options);
    eAtClockExtractorSquelching (*Hw100MSerdesClockSquelOptionGet)(Tha60290022ClockExtractor self, AtSerdesController serdes);
    eAtModuleClockRet (*HwTenGeSerdesClockSquelOptionSet)(Tha60290022ClockExtractor self, AtSerdesController serdes, eAtClockExtractorSquelching options);
    eAtClockExtractorSquelching (*HwTenGeSerdesClockSquelOptionGet)(Tha60290022ClockExtractor self, AtSerdesController serdes);
    }tTha60290022ClockExtractorMethods;

typedef struct tTha60290022ClockExtractor
    {
    tTha60290021ClockExtractor super;
    const tTha60290022ClockExtractorMethods *methods;
    }tTha60290022ClockExtractor;

typedef struct tTha60290022ClockExtractorV2
    {
    tTha60290022ClockExtractor super;
    }tTha60290022ClockExtractorV2;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtClockExtractor Tha60290022ClockExtractorObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId);
AtClockExtractor Tha60290022ClockExtractorV2ObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId);
eAtRet Tha60290022ClockExtractorEthSerdesDefaultSquelchingOptionSet(ThaClockExtractor self, AtSerdesController serdes);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022CLOCKEXTRACTORINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : Tha60290022ClockExtractorV2.c
 *
 * Created Date: Apr 9, 2018
 *
 * Description : Clock extractor implementation for eth squelching which use inside
 *               CORE instead top diag
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022ClockExtractorInternal.h"
#include "../../Tha60290021/man/Tha60290021Device.h"
#include "../ocn/Tha60290022ModuleOcn.h"
#include "../man/Tha60290022DeviceReg.h"
#include "../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021ClockExtractorMethods m_Tha60290021ClockExtractorOverride;

/* Supper implementations */
static const tTha60290021ClockExtractorMethods *m_Tha60290021ClockExtractorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HwClockExtractShouldUseDiag(Tha60290021ClockExtractor self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return cAtFalse;
    }

static void OverrideTha60290021ClockExtractor(AtClockExtractor self)
    {
    Tha60290021ClockExtractor extractor = (Tha60290021ClockExtractor) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021ClockExtractorMethods = mMethodsGet(extractor);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ClockExtractorOverride, m_Tha60290021ClockExtractorMethods, sizeof(m_Tha60290021ClockExtractorOverride));

        mMethodOverride(m_Tha60290021ClockExtractorOverride, HwClockExtractShouldUseDiag);
        }

    mMethodsSet(extractor, &m_Tha60290021ClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideTha60290021ClockExtractor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ClockExtractorV2);
    }

AtClockExtractor Tha60290022ClockExtractorV2ObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtClockExtractor Tha60290022ClockExtractorV2New(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ClockExtractorV2ObjectInit(newExtractor, clockModule, extractorId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PMC
 *
 * File        : Tha60290022ModulePmc.c
 *
 * Created Date: Dec 10, 2016
 *
 * Description : PMC module concrete class for 60290022
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022ModulePmcInternal.h"
#include "Tha60290022ModulePmcReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_Pmc_Counter_Field_Mask              cBit17_0

/* PW TX counter offset */
#define cAf6_txpwcnt_rbit_packet_offset         0
#define cAf6_txpwcnt_nbit_packet_offset         1
#define cAf6_txpwcnt_packet_offset              2

#define cAf6_txpwcnt_pbit_packet_offset         0
#define cAf6_txpwcnt_lbit_packet_offset         1

/* PW RX counter offset */
#define cAf6_rxpwcnt_packet_offset              0
#define cAf6_rxpwcnt_rbit_packet_offset         0
#define cAf6_rxpwcnt_nbit_packet_offset         1
#define cAf6_rxpwcnt_stray_packet_offset        2

#define cAf6_rxpwcnt_pbit_packet_offset         0
#define cAf6_rxpwcnt_lbit_packet_offset         1
#define cAf6_rxpwcnt_malformed_packet_offset    2

#define cAf6_rxpwcnt_underrun_packet_offset     0
#define cAf6_rxpwcnt_lopsta_packet_offset       1
#define cAf6_rxpwcnt_early_packet_offset        2

#define cAf6_rxpwcnt_overrun_packet_offset      0
#define cAf6_rxpwcnt_lost_packet_offset         1
#define cAf6_rxpwcnt_late_packet_offset         2

#define cAf6_reg_tick                           0xf0004F
#define cAf6_reg_tick_mode_Mask                 cBit0
#define cAf6_reg_tick_mode_Shift                0
#define cAf6_reg_tick_set_Mask                  cBit1
#define cAf6_reg_tick_set_Shift                 1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePmcMethods m_ThaModulePmcOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 AddressWithLocalAddress(ThaModulePmc self, uint32 localAddr)
    {
    return ThaModulePmcBaseAddress(self) + localAddr;
    }

static uint32 PwCounterOffset(ThaModulePmc self, AtPw pw, uint32 cntIndex, eBool clear)
    {
    AtUnused(self);
    AtUnused(clear);
    return AtChannelIdGet((AtChannel)pw) + (10752UL * cntIndex);
    }

static uint32 CounterRead2Clear(ThaModulePmc self, uint32 address, eBool clear)
    {
    uint32 regVal = mModuleHwRead(self, address);

    if (clear)
        mModuleHwWrite(self, address, 0);

    return regVal;
    }

static uint32 PwByteCounterGet(ThaModulePmc self, AtPw pw, uint32 regAddress, eBool clear)
    {
    uint32 address = AddressWithLocalAddress(self, regAddress) + AtChannelIdGet((AtChannel)pw);
    return CounterRead2Clear(self, address, clear) & cAf6_upen_txpwcnt_info0_pwcntbyte_Mask;
    }

static uint32 PwCommonCounterGet(ThaModulePmc self, AtPw pw, uint32 regAddress, uint32 cntIndex, eBool clear)
    {
    uint32 offset = mMethodsGet(self)->PwCounterOffset(self, pw, cntIndex, clear);
    uint32 address = AddressWithLocalAddress(self, regAddress) + offset;
    return CounterRead2Clear(self, address, clear) & cAf6Reg_Pmc_Counter_Field_Mask;
    }

static uint32 AuVcDefaultOffset(ThaModulePmc self, AtSdhChannel channel)
    {
    uint8 slice, hwStsInSlice;
    AtUnused(self);

    ThaSdhChannel2HwMasterStsId(channel, cThaModuleOcn, &slice, &hwStsInSlice);
    return (uint32)(slice + 8UL * hwStsInSlice);
    }

static uint32 AuVcCounterAddress(ThaModulePmc self, AtSdhChannel channel, uint32 localAddress)
    {
    return AddressWithLocalAddress(self, localAddress) + mMethodsGet(self)->AuVcDefaultOffset(self, channel);
    }

static uint32 AuVcCounterGet(ThaModulePmc self, AtSdhChannel channel, uint32 regAddress, eBool clear)
    {
    return CounterRead2Clear(self, AuVcCounterAddress(self, channel, regAddress), clear) & cAf6Reg_Pmc_Counter_Field_Mask;
    }

static uint32 Vc1xDefaultOffset(ThaModulePmc self, AtSdhChannel sdhChannel)
    {
    uint8 slice, hwStsInSlice;
    uint8 vtgId = AtSdhChannelTug2Get(sdhChannel);
    uint8 vtId = AtSdhChannelTu1xGet(sdhChannel);
    AtUnused(self);

    if (ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleOcn, &slice, &hwStsInSlice) == cAtOk)
        return (uint32)(slice + (256UL * hwStsInSlice) + (32UL * vtgId) + (8UL * vtId));

    return cBit31_0;
    }

static uint32 Vc1xCounterAddress(ThaModulePmc self, AtSdhChannel channel, uint32 localAddress)
    {
    return AddressWithLocalAddress(self, localAddress) + mMethodsGet(self)->Vc1xDefaultOffset(self, channel);
    }

static uint32 Vc1xCounterGet(ThaModulePmc self, AtSdhChannel channel, uint32 regAddress, eBool clear)
    {
    return CounterRead2Clear(self, Vc1xCounterAddress(self, channel, regAddress), clear) & cAf6Reg_Pmc_Counter_Field_Mask;
    }

static uint32 De1DefaultOffset(ThaModulePmc self, AtPdhChannel de1)
    {
    uint8 slice, hwIdInSlice, vtg, vt;
    AtUnused(self);

    ThaPdhDe1HwIdGet((ThaPdhDe1)de1, cAtModulePdh, &slice, &hwIdInSlice, &vtg, &vt);
    return (slice + (256UL * hwIdInSlice) + (32UL * vtg) + (8UL * vt));
    }

static uint32 VcDe1CounterAddress(ThaModulePmc self, AtPdhChannel de1, uint32 regAddress, uint32 cntIndex)
    {
    return AddressWithLocalAddress(self, regAddress) + mMethodsGet(self)->De1DefaultOffset(self, de1) + (0x4000UL * cntIndex);
    }

static uint32 De1CounterGet(ThaModulePmc self, AtPdhChannel de1, uint32 regAddress, uint32 cntIndex, eBool clear)
    {
    return CounterRead2Clear(self, VcDe1CounterAddress(self, de1, regAddress, cntIndex), clear) & cAf6Reg_Pmc_Counter_Field_Mask;
    }

static uint32 De3DefaultOffset(ThaModulePmc self, AtPdhChannel de3)
    {
    uint8 sliceId, hwIdInSlice;
    AtUnused(self);

    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);
    return (8UL * hwIdInSlice) + sliceId;
    }

static uint32 De3CounterAddress(ThaModulePmc self, AtPdhChannel de3, uint32 regAddress, uint32 cntIndex)
    {
    return AddressWithLocalAddress(self, regAddress) + mMethodsGet(self)->De3DefaultOffset(self, de3) + (0x200UL * cntIndex);
    }

static uint32 De3CounterGet(ThaModulePmc self, AtPdhChannel channel, uint32 regAddress, uint32 cntIndex, eBool clear)
    {
    return CounterRead2Clear(self, De3CounterAddress(self, channel, regAddress, cntIndex), clear) & cAf6Reg_Pmc_Counter_Field_Mask;
    }

static uint32 AuVcPohCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtSdhPathCounterTypeBip:
            return AuVcCounterGet(self, channel, cAf6Reg_upen_sts_pohpm1_rw_Base, clear);

        case cAtSdhPathCounterTypeRei:
            return AuVcCounterGet(self, channel, cAf6Reg_upen_sts_pohpm_rw_Base, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 AuVcPointerCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtSdhPathCounterTypeRxPPJC:
            return AuVcCounterGet(self, channel, cAf6Reg_upen_poh_sts_cnt11_rw_Base, clear);

        case cAtSdhPathCounterTypeRxNPJC:
            return AuVcCounterGet(self, channel, cAf6Reg_upen_poh_sts_cnt1_rw_Base, clear);

        case cAtSdhPathCounterTypeTxPPJC:
            return AuVcCounterGet(self, channel, cAf6Reg_upen_poh_sts_cnt01_rw_Base, clear);

        case cAtSdhPathCounterTypeTxNPJC:
            return AuVcCounterGet(self, channel, cAf6Reg_upen_poh_sts_cnt0_rw_Base, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 TuVc1xPointerCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtSdhPathCounterTypeBip:
            return Vc1xCounterGet(self, channel, cAf6Reg_upen_vt_pohpm1_rw_Base, clear);

        case cAtSdhPathCounterTypeRei:
            return Vc1xCounterGet(self, channel, cAf6Reg_upen_vt_pohpm_rw_Base, clear);

        case cAtSdhPathCounterTypeRxPPJC:
            return Vc1xCounterGet(self, channel, cAf6Reg_upen_poh_vt_cnt11_rw_Base, clear);

        case cAtSdhPathCounterTypeRxNPJC:
            return Vc1xCounterGet(self, channel, cAf6Reg_upen_poh_vt_cnt1_rw_Base, clear);

        case cAtSdhPathCounterTypeTxPPJC:
            return Vc1xCounterGet(self, channel, cAf6Reg_upen_poh_vt_cnt01_rw_Base, clear);

        case cAtSdhPathCounterTypeTxNPJC:
            return Vc1xCounterGet(self, channel, cAf6Reg_upen_poh_vt_cnt0_rw_Base, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 VcDe1CounterOffsetCountGet(ThaModulePmc self, uint16 counterType)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtPdhDe1CounterFe:  return 0;
        case cAtPdhDe1CounterCrc: return 1;
        case cAtPdhDe1CounterRei: return 2;
        default:
            return cInvalidUint32;
        }
    }

static uint32 VcDe1CounterGet(ThaModulePmc self, AtPdhChannel channel, uint16 counterType, eBool clear)
    {
    uint32 offsetCount = VcDe1CounterOffsetCountGet(self, counterType);

    /* Not support BPV */
    if (counterType == cAtPdhDe1CounterBpvExz) return 0;
    if (counterType == cAtPdhDe1CounterTxCs)   return 0;
    return De1CounterGet(self, channel, cAf6Reg_upen_pdh_de1cntval30_rw_Base, offsetCount, clear);
    }

static uint32 VcDe3CounterOffsetCountGet(ThaModulePmc self, uint16 counterType)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtPdhDe3CounterFBit:  return 0;
        case cAtPdhDe3CounterRei:   return 1;
        case cAtPdhDe3CounterPBit:  return 2;
        case cAtPdhDe3CounterCPBit: return 3;
        default:
            return cInvalidUint32;
        }
    }

static uint32 VcDe3CounterGet(ThaModulePmc self, AtPdhChannel channel, uint16 counterType, eBool clear)
    {
    uint32 offsetCount = VcDe3CounterOffsetCountGet(self, counterType);

    /* Not support BPV */
    if (counterType == cAtPdhDe3CounterBpvExz) return 0;
    if (counterType == cAtPdhDe3CounterTxCs)   return 0;
    return De3CounterGet(self, channel, cAf6Reg_upen_pdh_de3cnt0_rw_Base, offsetCount, clear);
    }

static uint32 PwTxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_txpwcnt_info1_rw_Base, cAf6_txpwcnt_packet_offset, clear);
    }

static uint32 PwTxBytesGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    if (self == NULL || pw == NULL)
        return 0;
    return PwByteCounterGet(self, pw, cAf6Reg_upen_txpwcnt_info0_Base, clear);
    }

static uint32 PwTxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_txpwcnt_info11_rw_Base, cAf6_txpwcnt_lbit_packet_offset, clear);
    }

static uint32 PwTxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_txpwcnt_info1_rw_Base, cAf6_txpwcnt_rbit_packet_offset, clear);
    }

static uint32 PwTxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_txpwcnt_info1_rw_Base, cAf6_txpwcnt_nbit_packet_offset, clear);
    }

static uint32 PwTxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_txpwcnt_info11_rw_Base, cAf6_txpwcnt_pbit_packet_offset, clear);
    }

static uint32 PwTxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_txpwcnt_info1_rw_Base, cAf6_txpwcnt_nbit_packet_offset, clear);
    }

static uint32 PwRxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info01_rw_Base, cAf6_rxpwcnt_packet_offset, clear);
    }

static uint32 PwRxMalformedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info11_rw_Base, cAf6_rxpwcnt_malformed_packet_offset, clear);
    }

static uint32 PwRxStrayPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info1_rw_Base, cAf6_rxpwcnt_stray_packet_offset, clear);
    }

static uint32 PwRxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info11_rw_Base, cAf6_rxpwcnt_lbit_packet_offset, clear);
    }

static uint32 PwRxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info1_rw_Base, cAf6_rxpwcnt_rbit_packet_offset, clear);
    }

static uint32 PwRxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info1_rw_Base, cAf6_rxpwcnt_nbit_packet_offset, clear);
    }

static uint32 PwRxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info11_rw_Base, cAf6_rxpwcnt_pbit_packet_offset, clear);
    }

static uint32 PwRxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info1_rw_Base, cAf6_rxpwcnt_nbit_packet_offset, clear);
    }

static uint32 PwRxBytesGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwByteCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info0_rw_Base, clear);
    }

static uint32 PwRxLostPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info21_rw_Base, cAf6_rxpwcnt_lost_packet_offset, clear);
    }

static uint32 PwRxReorderedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info2_rw_Base, cAf6_rxpwcnt_early_packet_offset, clear);
    }

static uint32 PwRxOutOfSeqDropedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info21_rw_Base, cAf6_rxpwcnt_late_packet_offset, clear);
    }

static uint32 PwRxJitBufOverrunGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info21_rw_Base, cAf6_rxpwcnt_overrun_packet_offset, clear);
    }

static uint32 PwRxJitBufUnderrunGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info2_rw_Base, cAf6_rxpwcnt_underrun_packet_offset, clear);
    }

static uint32 PwRxLopsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    return PwCommonCounterGet(self, pw, cAf6Reg_upen_rxpwcnt_info2_rw_Base, cAf6_rxpwcnt_lopsta_packet_offset, clear);;
    }

static eBool LineSupportedBlockCounters(ThaModulePmc self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 LineCounter1Reg(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_poh_pmr_cnt0_rw_Base;
    }

static uint32 LineCounter2Reg(ThaModulePmc self)
    {
    AtUnused(self);
    return cAf6Reg_upen_poh_pmr_cnt1_rw_Base;
    }

static uint32 LineCounterOffset(ThaModulePmc self, ThaSdhLine line, uint32 cntIndex, eBool clear)
    {
    AtUnused(self);
    AtUnused(clear);
    return AtChannelIdGet((AtChannel)line) + 16UL * cntIndex;
    }

static eAtRet TickModeSet(ThaModulePmc self, eThaModulePmcTickMode mode)
    {
    uint32 regAddr = cAf6_reg_tick;
    uint32 regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_reg_tick_mode_, (mode == cThaModulePmcTickModeAuto) ? 0 : 1);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eThaModulePmcTickMode TickModeGet(ThaModulePmc self)
    {
    uint32 regVal  = mModuleHwRead(self, cAf6_reg_tick);
    return (regVal & cAf6_reg_tick_mode_Mask) ? cThaModulePmcTickModeManual : cThaModulePmcTickModeAuto;
    }

static eAtRet Tick(ThaModulePmc self)
    {
    uint32 regAddr = cAf6_reg_tick;
    uint32 regVal  = mModuleHwRead(self, regAddr);

    /* Write 0 -> 1: tick */
    mRegFieldSet(regVal, cAf6_reg_tick_set_, 0);
    mModuleHwWrite(self, regAddr, regVal);
    mRegFieldSet(regVal, cAf6_reg_tick_set_, 1);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool HasLongAccess(ThaModulePmc self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModulePmc(AtModule self)
	{
	ThaModulePmc module = (ThaModulePmc)self;

	if (!m_methodsInit)
		{
		AtOsal osal = AtSharedDriverOsalGet();
		mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePmcOverride, mMethodsGet(module), sizeof(m_ThaModulePmcOverride));

		mMethodOverride(m_ThaModulePmcOverride, HasLongAccess);
        mMethodOverride(m_ThaModulePmcOverride, LineSupportedBlockCounters);
        mMethodOverride(m_ThaModulePmcOverride, LineCounter1Reg);
        mMethodOverride(m_ThaModulePmcOverride, LineCounter2Reg);
        mMethodOverride(m_ThaModulePmcOverride, LineCounterOffset);
        mMethodOverride(m_ThaModulePmcOverride, AuVcPointerCounterGet);
        mMethodOverride(m_ThaModulePmcOverride, AuVcPohCounterGet);
        mMethodOverride(m_ThaModulePmcOverride, TuVc1xPointerCounterGet);
        mMethodOverride(m_ThaModulePmcOverride, VcDe1CounterGet);
        mMethodOverride(m_ThaModulePmcOverride, VcDe3CounterGet);

        mMethodOverride(m_ThaModulePmcOverride, PwTxPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxBytesGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxLbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxRbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxMbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxPbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxNbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxMalformedPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxStrayPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxLbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxRbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxMbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxPbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxNbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxBytesGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxLostPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxReorderedPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxOutOfSeqDropedPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxJitBufOverrunGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxJitBufUnderrunGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxLopsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwCounterOffset);

        mMethodOverride(m_ThaModulePmcOverride, TickModeSet);
        mMethodOverride(m_ThaModulePmcOverride, TickModeGet);
        mMethodOverride(m_ThaModulePmcOverride, Tick);

        mMethodOverride(m_ThaModulePmcOverride, AuVcDefaultOffset);
        mMethodOverride(m_ThaModulePmcOverride, Vc1xDefaultOffset);
        mMethodOverride(m_ThaModulePmcOverride, De3DefaultOffset);
        mMethodOverride(m_ThaModulePmcOverride, De1DefaultOffset);
		}

	mMethodsSet(module, &m_ThaModulePmcOverride);
	}

static void Override(AtModule self)
	{
	OverrideThaModulePmc(self);
	}

static uint32 ObjectSize(void)
	{
	return sizeof(tTha60290022ModulePmc);
	}

AtModule Tha60290022ModulePmcObjectInit(AtModule self, AtDevice device)
	{
	/* Clear memory */
	AtOsal osal = AtSharedDriverOsalGet();
	mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

	/* Super constructor */
	if (ThaModulePmcObjectInit(self, device) == NULL)
		return NULL;
    
    /* Setup class */
	Override(self);
	m_methodsInit = 1;

	return self;
	}

AtModule Tha60290022ModulePmcNew(AtDevice self)
	{
	/* Allocate memory */
	AtOsal osal = AtSharedDriverOsalGet();
	AtModule module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
	if (module == NULL)
		return NULL;

	/* Construct it */
	return Tha60290022ModulePmcObjectInit(module, self);
	}

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMC
 * 
 * File        : Tha60290022ModulePmc.h
 * 
 * Created Date: Feb 2, 2018
 *
 * Description : PMC module interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPMC_H_
#define _THA60290022MODULEPMC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"
#include "AtSdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModulePmcV2 *Tha60290022ModulePmcV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290022ModulePmcV2HwHiPwIdLookupSet(ThaModulePmc self, AtPw pw, AtSdhChannel hoSdhVc, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPMC_H_ */


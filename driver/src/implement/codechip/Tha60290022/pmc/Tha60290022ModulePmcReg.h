/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive
 * Technologies. The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      :
 *
 * File        :
 *
 * Created Date:
 *
 * Description : This file contain all constance definitions of  block.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_GLBPMC_H_
#define _AF6_REG_AF6CNC0022_RD_GLBPMC_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Counter
Reg Addr   : 0x0_0000-0x0_7DFF(RW)
Reg Formula: 0x0_0000 + 10752*$offset + $pwid
    Where  :
           + $pwid(0-10751)
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txpwcnt_info1_rw_Base                                                             0x00000
#define cAf6Reg_upen_txpwcnt_info11_rw_Base                                                            0x08000

/*--------------------------------------
BitField Name: txpwcnt
BitField Type: RW
BitField Desc: in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 :
txnbit + offset = 2 : txpkt in case of txpwcnt1 side: + offset = 0 : txpbit +
offset = 1 : txlbit + offset = 2 : unused
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_txpwcnt_info1_rw_txpwcnt_Mask                                                       cBit17_0
#define cAf6_upen_txpwcnt_info1_rw_txpwcnt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire TX Byte Counter
Reg Addr   : 0x8_0000-0x8_29FF (RW)
Reg Formula: 0x8_0000 + $pwid
    Where  :
           + $pwid(0-10751)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txpwcnt_info0_Base                                                                0x80000

/*--------------------------------------
BitField Name: pwcntbyte
BitField Type: RW
BitField Desc: txpwcntbyte
BitField Bits: [29:00]
--------------------------------------*/
#define cAf6_upen_txpwcnt_info0_pwcntbyte_Mask                                                        cBit29_0
#define cAf6_upen_txpwcnt_info0_pwcntbyte_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info0
Reg Addr   : 0x1_0000-0x1_29FF (RW)
Reg Formula: 0x1_0000 + $pwid
    Where  :
           + $pwid(0-10751)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info0_rw_Base                                                             0x10000
#define cAf6Reg_upen_rxpwcnt_info01_rw_Base                                                            0x14000

/*--------------------------------------
BitField Name: rxpwcnt_info0
BitField Type: RW
BitField Desc: in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of
rxpwcnt_info0_cnt1 side: + rxpkt
BitField Bits: [29:00]
--------------------------------------*/
#define cAf6_upen_rxpwcnt_info0_rw_rxpwcnt_info0_Mask                                                 cBit29_0
#define cAf6_upen_rxpwcnt_info0_rw_rxpwcnt_info0_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info1
Reg Addr   : 0x2_0000-0x2_7DFF(RW)
Reg Formula: 0x2_0000 + 10752*$offset + $pwid
    Where  :
           + $pwid(0-10751)
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info1_rw_Base                                                             0x20000
#define cAf6Reg_upen_rxpwcnt_info11_rw_Base                                                            0x28000

/*--------------------------------------
BitField Name: rxpwcnt_info0
BitField Type: RW
BitField Desc: in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxrbit +
offset = 1 : rxnbit + offset = 2 : rxstray in case of rxpwcnt_info0_cnt1 side: +
offset = 0 : rxpbit + offset = 1 : rxlbit + offset = 2 : rxmalform
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_rxpwcnt_info1_rw_rxpwcnt_info0_Mask                                                 cBit17_0
#define cAf6_upen_rxpwcnt_info1_rw_rxpwcnt_info0_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receiver Counter Info2
Reg Addr   : 0x3_0000-0x3_7DFF(RW)
Reg Formula: 0x3_0000 + 10752*$offset + $pwid
    Where  :
           + $pwid(0-10751)
           + $offset(0-2)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt_info2_rw_Base                                                             0x30000
#define cAf6Reg_upen_rxpwcnt_info21_rw_Base                                                            0x38000

/*--------------------------------------
BitField Name: rxpwcnt_info1
BitField Type: RW
BitField Desc: in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxunderrun +
offset = 1 : rxlops + offset = 2 : rxearly in case of rxpwcnt_info1_cnt1 side: +
offset = 0 : rxoverrun + offset = 1 : rxlost + offset = 2 : rxlate
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_rxpwcnt_info2_rw_rxpwcnt_info1_Mask                                                 cBit17_0
#define cAf6_upen_rxpwcnt_info2_rw_rxpwcnt_info1_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : PMR Error Counter
Reg Addr   : 0x9_1800-0x9_182F(RW)
Reg Formula: 0x9_1800 + 16*$offset + $lineid
    Where  :
           + $offset(0-2)
           + $lineid(0-15)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_pmr_cnt0_rw_Base                                                              0x91800
#define cAf6Reg_upen_poh_pmr_cnt1_rw_Base                                                              0x91840

/*--------------------------------------
BitField Name: pmr_err_cnt
BitField Type: RW
BitField Desc: in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 :
b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 :
rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_poh_pmr_cnt0_rw_pmr_err_cnt_Mask                                                    cBit31_0
#define cAf6_upen_poh_pmr_cnt0_rw_pmr_err_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Path STS Counter
Reg Addr   : 0x9_2000-0x9_21FF (RW)
Reg Formula: 0x9_2000 + 8*$stsid + $slcid
    Where  :
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sts_pohpm_rw_Base                                                                 0x92000
#define cAf6Reg_upen_sts_pohpm1_rw_Base                                                                0x92200

/*--------------------------------------
BitField Name: pohpath_vt_cnt
BitField Type: RW
BitField Desc: in case of pohpath_cnt0 side: + sts_rei in case of pohpath_cnt1
side: + sts_bip(B3)
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_sts_pohpm_rw_pohpath_vt_cnt_Mask                                                    cBit17_0
#define cAf6_upen_sts_pohpm_rw_pohpath_vt_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH Path VT Counter
Reg Addr   : 0x7_0000 - 0x7_3FFF(RW)
Reg Formula: 0x7_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  :
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_vt_pohpm_rw_Base                                                                   0x70000
#define cAf6Reg_upen_vt_pohpm1_rw_Base                                                                  0x74000

/*--------------------------------------
BitField Name: pohpath_vt_cnt
BitField Type: RW
BitField Desc: in case of pohpath_cnt0 side: + vt_rei in case of pohpath_cnt1
side: + vt_bip
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_vt_pohpm_rw_pohpath_vt_cnt_Mask                                                     cBit17_0
#define cAf6_upen_vt_pohpm_rw_pohpath_vt_cnt_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : POH vt pointer Counter 0
Reg Addr   : 0x5_0000-0x5_3FFF(RW)
Reg Formula: 0x5_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  :
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_vt_cnt0_rw_Base                                                               0x50000
#define cAf6Reg_upen_poh_vt_cnt01_rw_Base                                                              0x54000

/*--------------------------------------
BitField Name: poh_vt_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + pohtx_vt_dec in case of
poh_vt_cnt1 side: + pohtx_vt_inc
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_vt_cnt0_rw_poh_vt_cnt_Mask                                                      cBit17_0
#define cAf6_upen_poh_vt_cnt0_rw_poh_vt_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH vt pointer Counter 1
Reg Addr   : 0x6_0000-0x6_3FFF(RW)
Reg Formula: 0x6_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  :
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_vt_cnt1_rw_Base                                                               0x60000
#define cAf6Reg_upen_poh_vt_cnt11_rw_Base                                                              0x64000

/*--------------------------------------
BitField Name: poh_vt_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + pohrx_vt_dec in case of
poh_vt_cnt1 side: + pohrx_vt_inc
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_vt_cnt1_rw_poh_vt_cnt_Mask                                                      cBit17_0
#define cAf6_upen_poh_vt_cnt1_rw_poh_vt_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH sts pointer Counter
Reg Addr   : 0x9_0800-0x9_09FF(RW)
Reg Formula: 0x9_0800 + 8*$stsid + $slcid
    Where  :
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_sts_cnt0_rw_Base                                                              0x90800
#define cAf6Reg_upen_poh_sts_cnt01_rw_Base                                                             0x90A00

/*--------------------------------------
BitField Name: poh_sts_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + pohtx_sts_dec in case of
poh_vt_cnt1 side: + pohtx_sts_inc
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_sts_cnt0_rw_poh_sts_cnt_Mask                                                    cBit17_0
#define cAf6_upen_poh_sts_cnt0_rw_poh_sts_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : POH sts pointer Counter
Reg Addr   : 0x9_1000-0x9_11FF(RW)
Reg Formula: 0x9_1000 + 8*$stsid + $slcid
    Where  :
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_sts_cnt1_rw_Base                                                              0x91000
#define cAf6Reg_upen_poh_sts_cnt11_rw_Base                                                              0x91200

/*--------------------------------------
BitField Name: poh_sts_cnt
BitField Type: RW
BitField Desc: in case of poh_vt_cnt0 side: + pohrx_sts_dec in case of
poh_vt_cnt1 side: + pohrx_sts_inc
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_poh_sts_cnt1_rw_poh_sts_cnt_Mask                                                    cBit17_0
#define cAf6_upen_poh_sts_cnt1_rw_poh_sts_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : PDH ds1 cntval Counter
Reg Addr   : 0x4_0000-0x4_3FFF(RW)
Reg Formula: 0x4_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  :
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pdh_de1cntval30_rw_Base                                                           0x40000


/*------------------------------------------------------------------------------
Reg Name   : PDH ds3 cntval Counter
Reg Addr   : 0x9_0000-0x9_01FF(RW)
Reg Formula: 0x9_0000 + 8*$stsid + $slcid
    Where  :
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   :
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pdh_de3cnt0_rw_Base                                                               0x90000

#endif /* _AF6_REG_AF6CNC0022_RD_GLBPMC_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PMC
 *
 * File        : Tha60290022ModulePmcV2.c
 *
 * Created Date: Feb 1, 2018
 *
 * Description : PMC v2 - 1page counter implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022ModulePmcInternal.h"
#include "Tha60290022GlbPmc1PageReg.h"
#include "Tha60290022ModulePmc.h"

/*--------------------------- Define -----------------------------------------*/
#define cHwPwCounterTypeTxByte   0
#define cHwPwCounterTypeTxPacket 1

#define cHwPwCounterTypeTxRbit     0
#define cHwPwCounterTypeTxPBit     1
#define cHwPwCounterTypeTxNbitMbit 2
#define cHwPwCounterTypeTxLbit     3

#define cHwPwCounterTypeRxByte   0
#define cHwPwCounterTypeRxPacket 1

#define cHwPwCounterTypeRxRbit     0
#define cHwPwCounterTypeRxPBit     1
#define cHwPwCounterTypeRxNbitMbit 2
#define cHwPwCounterTypeRxLbit     3

#define cHwPwCounterTypeRxStray    0
#define cHwPwCounterTypeRxMalform  1
#define cHwPwCounterTypeRxUnderrun 2
#define cHwPwCounterTypeRxOverrun  3

#define cHwPwCounterTypeRxLops        0
#define cHwPwCounterTypeRxReorderLost 1
#define cHwPwCounterTypeRxReorderOk   2
#define cHwPwCounterTypeRxReorderDrop 3

#define cHwHiPwCounterTypeTxByte   0
#define cHwHiPwCounterTypeTxPacket 1
#define cHwHiPwCounterTypeTxRbit   2
#define cHwHiPwCounterTypeTxLbit   3

#define cHwHiPwCounterTypeRxByte    0
#define cHwHiPwCounterTypeRxPacket  1
#define cHwHiPwCounterTypeRxRbit    2
#define cHwHiPwCounterTypeRxLbit    3
#define cHwHiPwCounterTypeRxStray   4
#define cHwHiPwCounterTypeRxMalform 5

#define cHwSonetLineCounterTypeRei           0
#define cHwSonetLineCounterTypeReiBlockError 1
#define cHwSonetLineCounterTypeB2Error       2
#define cHwSonetLineCounterTypeB2BlockError  3
#define cHwSonetLineCounterTypeB1Error       4
#define cHwSonetLineCounterTypeB1BlockError  5

#define cHwStsPohCounterTypeRei     0
#define cHwStsPohCounterTypeB3Error 1

#define cHwStsPointerCounterTypeTxDec   0
#define cHwStsPointerCounterTypeTxInc   1

#define cHwStsPointerCounterTypeRxDec   0
#define cHwStsPointerCounterTypeRxInc   1

#define cHwVtPohCounterTypeRei      0
#define cHwVtPohCounterTypeBipError 1

#define cHwVtPointerCounterTypeTxDec 0
#define cHwVtPointerCounterTypeTxInc 1
#define cHwVtPointerCounterTypeRxDec 2
#define cHwVtPointerCounterTypeRxInc 3

#define cHwDe3CounterTypeFe  0
#define cHwDe3CounterTypeRei 1
#define cHwDe3CounterTypePb  2
#define cHwDe3CounterTypeCb  3

#define cHwDe1CounterTypeFe   0
#define cHwDe1CounterTypeCrc  1
#define cHwDe1CounterTypeRei  2

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)     ((Tha60290022ModulePmcV2)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290022ModulePmcV2Methods m_methods;

/* Override */
static tThaModulePmcMethods m_ThaModulePmcOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 AddressWithLocalAddress(ThaModulePmc self, uint32 localAddr)
    {
    return ThaModulePmcBaseAddress(self) + localAddr;
    }

static uint32 CounterRead2Clear(ThaModulePmc self, uint32 address, eBool clear)
    {
    uint32 regVal = mModuleHwRead(self, address);
    AtUnused(clear);

    return regVal;
    }

static eBool IsHiPw(AtPw pw)
    {
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    uint16 pwType = AtPwTypeGet(pw);
    uint16 channelType = 0;

    if (pwType != cAtPwTypeCEP)
        return cAtFalse;

    channelType = AtSdhChannelTypeGet((AtSdhChannel)circuit);
    if ((channelType == cAtSdhChannelTypeVc4_4c) ||
        (channelType == cAtSdhChannelTypeVc4_16c) ||
        (channelType == cAtSdhChannelTypeVc4_64c))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 HoSdhVcToHwId(AtSdhChannel circuit)
    {
    uint8 lineId = AtSdhChannelLineGet(circuit);
    uint8 stsId = AtSdhChannelSts1Get(circuit);
    uint8 localLineId = 0;
    uint32 hiPwId = 0;
    ThaModuleSdh moduleSdh = (ThaModuleSdh)AtChannelModuleGet((AtChannel)circuit);

    localLineId = ThaModuleSdhLineIdLocalId(moduleSdh, lineId);
    hiPwId = (uint32)(stsId / 12 + localLineId * 4);
    return hiPwId;
    }

static eAtRet HwHiPwIdLookupSet(ThaModulePmc self, AtPw pw, AtSdhChannel hoSdhVc, eBool enable)
    {
    uint32 channelOffset = AtChannelIdGet((AtChannel)pw);
    uint32 hwId = HoSdhVcToHwId(hoSdhVc);
    uint32 address = AddressWithLocalAddress(self, cAf6Reg_ramcnthotdmlkupcfg_Base) + channelOffset;
    uint32 regVal = mModuleHwRead(self, address);

    mRegFieldSet(regVal, cAf6_ramcnthotdmlkupcfg_HighRateSwPwID_, hwId);
    mRegFieldSet(regVal, cAf6_ramcnthotdmlkupcfg_PwCntHighRateEn_, enable ? 1:0);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

static uint32 HwHiPwIdLookupGet(ThaModulePmc self, AtPw pw)
    {
    uint32 channelOffset = AtChannelIdGet((AtChannel)pw);
    uint32 address = AddressWithLocalAddress(self, cAf6Reg_ramcnthotdmlkupcfg_Base) + channelOffset;
    uint32 regVal = mModuleHwRead(self, address);
    uint32 hwId = mRegField(regVal, cAf6_ramcnthotdmlkupcfg_HighRateSwPwID_);

    return hwId;
    }

static uint32 HwHiPwId(ThaModulePmc self, AtPw pw)
    {
    return HwHiPwIdLookupGet(self, pw);
    }

static uint32 HwHiPwCounterAddress(ThaModulePmc self, AtPw pw, uint32 cntOffset, uint32 cntType, eBool clear)
    {
    uint32 r2cOffset = clear ? 0 : 32;
    uint32 typeOffset = cntType * 64;
    uint32 channelOffset = HwHiPwId(self, pw);
    uint32 baseOffset = ThaModulePmcBaseAddress(self);

    return baseOffset + cntOffset + typeOffset + channelOffset + r2cOffset;
    }

static uint32 HwLoPwCounterAddress(ThaModulePmc self, AtPw pw, uint32 cntOffset, uint32 cntType, eBool clear)
    {
    uint32 r2cOffset;
    uint32 typeOffset;
    uint32 channelOffset = AtChannelIdGet((AtChannel)pw);
    uint32 baseOffset = ThaModulePmcBaseAddress(self);
    uint32 r2cCoeff, typeCoeff;

    if (mMethodsGet(mThis(self))->LoPwCounterCoeff(mThis(self), &r2cCoeff, &typeCoeff) != cAtOk)
        return cInvalidUint32;

    r2cOffset = clear ? 0 : r2cCoeff;
    typeOffset = cntType * typeCoeff;

    return baseOffset + cntOffset + typeOffset + channelOffset + r2cOffset;
    }

static uint32 PwTxBytesGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address;

    if (IsHiPw(pw))
        address = HwHiPwCounterAddress(self, pw, cAf6Reg_upen_txpwcntbyte_Base, cHwHiPwCounterTypeTxByte, clear);
    else
        address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_txpwcnt0_Base, cHwPwCounterTypeTxByte, clear);

    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwTxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address;

    if (IsHiPw(pw))
        address = HwHiPwCounterAddress(self, pw, cAf6Reg_upen_txpwcntbyte_Base, cHwHiPwCounterTypeTxPacket, clear);
    else
        address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_txpwcnt0_Base, cHwPwCounterTypeTxPacket, clear);

    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwTxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address;

    if (IsHiPw(pw))
        address = HwHiPwCounterAddress(self, pw, cAf6Reg_upen_txpwcntbyte_Base, cHwHiPwCounterTypeTxLbit, clear);
    else
        address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_txpwcnt1_Base, cHwPwCounterTypeTxLbit, clear);

    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwTxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address;

    if (IsHiPw(pw))
        address = HwHiPwCounterAddress(self, pw, cAf6Reg_upen_txpwcntbyte_Base, cHwHiPwCounterTypeTxRbit, clear);
    else
        address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_txpwcnt1_Base, cHwPwCounterTypeTxRbit, clear);

    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwTxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_txpwcnt1_Base, cHwPwCounterTypeTxNbitMbit, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwTxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_txpwcnt1_Base, cHwPwCounterTypeTxPBit, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwTxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_txpwcnt1_Base, cHwPwCounterTypeTxNbitMbit, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxBytesGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address;

    if (IsHiPw(pw))
        address = HwHiPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcntbyte_Base, cHwHiPwCounterTypeRxByte, clear);
    else
        address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt0_Base, cHwPwCounterTypeRxByte, clear);

    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address;

    if (IsHiPw(pw))
        address = HwHiPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcntbyte_Base, cHwHiPwCounterTypeRxPacket, clear);
    else
        address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt0_Base, cHwPwCounterTypeRxPacket, clear);

    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxRbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address;

    if (IsHiPw(pw))
        address = HwHiPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcntbyte_Base, cHwHiPwCounterTypeRxRbit, clear);
    else
        address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt1_Base, cHwPwCounterTypeRxRbit, clear);

    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxLbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address;

    if (IsHiPw(pw))
        address = HwHiPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcntbyte_Base, cHwHiPwCounterTypeRxLbit, clear);
    else
        address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt1_Base, cHwPwCounterTypeRxLbit, clear);

    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxStrayPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address;

    if (IsHiPw(pw))
        address = HwHiPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcntbyte_Base, cHwHiPwCounterTypeRxStray, clear);
    else
        address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt2_Base, cHwPwCounterTypeRxStray, clear);

    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxMalformedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address;

    if (IsHiPw(pw))
        address = HwHiPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcntbyte_Base, cHwHiPwCounterTypeRxMalform, clear);
    else
        address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt2_Base, cHwPwCounterTypeRxMalform, clear);

    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxMbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt1_Base, cHwPwCounterTypeRxNbitMbit, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxPbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt1_Base, cHwPwCounterTypeRxPBit, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxNbitPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt1_Base, cHwPwCounterTypeRxNbitMbit, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxJitBufUnderrunGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt2_Base, cHwPwCounterTypeRxUnderrun, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxJitBufOverrunGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt2_Base, cHwPwCounterTypeRxOverrun, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxLopsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt3_Base, cHwPwCounterTypeRxLops, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxLostPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt3_Base, cHwPwCounterTypeRxReorderLost, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxReorderedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt3_Base, cHwPwCounterTypeRxReorderOk, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 PwRxOutOfSeqDropedPacketsGet(ThaModulePmc self, AtPw pw, eBool clear)
    {
    uint32 address = HwLoPwCounterAddress(self, pw, cAf6Reg_upen_rxpwcnt3_Base, cHwPwCounterTypeRxReorderDrop, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 LineCounterAddress(ThaModulePmc self, ThaSdhLine line, uint32 cntOffset, uint32 cntType, eBool clear)
    {
    uint32 channelOffset = AtChannelIdGet((AtChannel)line);
    uint32 cntTypeOffset = cntType * 32;
    uint32 r2cOffset = clear ? 0 : 16;
    uint32 baseOffset = ThaModulePmcBaseAddress(self);

    return baseOffset + cntOffset + cntTypeOffset + r2cOffset + channelOffset;
    }

static uint32 LineCounterGet(ThaModulePmc self, ThaSdhLine line, uint32 cntOffset, uint32 cntType, eBool clear)
    {
    uint32 address = LineCounterAddress(self, line, cntOffset, cntType, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 SdhLineCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    ThaSdhLine line = (ThaSdhLine)channel;

    switch (counterType)
        {
        case cAtSdhLineCounterTypeB1:
            return LineCounterGet(self, line, cAf6Reg_upen_poh_pmr_cnt_Base, cHwSonetLineCounterTypeB1Error, clear);

        case cAtSdhLineCounterTypeB2:
            return LineCounterGet(self, line, cAf6Reg_upen_poh_pmr_cnt_Base, cHwSonetLineCounterTypeB2Error, clear);

        case cAtSdhLineCounterTypeRei:
            return LineCounterGet(self, line, cAf6Reg_upen_poh_pmr_cnt_Base, cHwSonetLineCounterTypeRei, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 SdhLineBlockErrorCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    ThaSdhLine line = (ThaSdhLine)channel;

    if (mMethodsGet(self)->LineSupportedBlockCounters(self) == cAtFalse)
        return 0;

    switch (counterType)
        {
        case cAtSdhLineCounterTypeB1:
            return LineCounterGet(self, line, cAf6Reg_upen_poh_pmr_cnt_Base, cHwSonetLineCounterTypeB1BlockError, clear);

        case cAtSdhLineCounterTypeB2:
            return LineCounterGet(self, line, cAf6Reg_upen_poh_pmr_cnt_Base, cHwSonetLineCounterTypeB2BlockError, clear);

        case cAtSdhLineCounterTypeRei:
            return LineCounterGet(self, line, cAf6Reg_upen_poh_pmr_cnt_Base, cHwSonetLineCounterTypeReiBlockError, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 AuVcCounterAddress(ThaModulePmc self, AtSdhChannel channel, uint32 cntOffset, uint32 cntType, eBool clear)
    {
    uint32 channelOffset = mMethodsGet(self)->AuVcDefaultOffset(self, channel);
    uint32 baseOffset = ThaModulePmcBaseAddress(self);
    uint32 cntTypeOffset;
    uint32 r2cOffset;
    uint32 r2cCoeff, typeCoeff;

    if (mMethodsGet(mThis(self))->AuVcCounterCoeff(mThis(self), &r2cCoeff, &typeCoeff) != cAtOk)
        return cInvalidUint32;

    cntTypeOffset = cntType * typeCoeff;
    r2cOffset = clear ? 0 : r2cCoeff;

    return baseOffset + cntOffset + cntTypeOffset + channelOffset + r2cOffset;
    }

static uint32 AuVcCounterGet(ThaModulePmc self, AtSdhChannel channel, uint32 cntOffset, uint32 cntType, eBool clear)
    {
    uint32 address = AuVcCounterAddress(self, channel, cntOffset, cntType, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 AuVcPohCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtSdhPathCounterTypeBip:
            return AuVcCounterGet(self, channel, cAf6Reg_upen_sts_pohpm_Base, cHwStsPohCounterTypeB3Error, clear);

        case cAtSdhPathCounterTypeRei:
            return AuVcCounterGet(self, channel, cAf6Reg_upen_sts_pohpm_Base, cHwStsPohCounterTypeRei, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 AuVcPointerCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtSdhPathCounterTypeRxPPJC:
            return AuVcCounterGet(self, channel, cAf6Reg_upen_poh_sts_cnt1_Base, cHwStsPointerCounterTypeRxInc, clear);

        case cAtSdhPathCounterTypeRxNPJC:
            return AuVcCounterGet(self, channel, cAf6Reg_upen_poh_sts_cnt1_Base, cHwStsPointerCounterTypeRxDec, clear);

        case cAtSdhPathCounterTypeTxPPJC:
            return AuVcCounterGet(self, channel, cAf6Reg_upen_poh_sts_cnt0_Base, cHwStsPointerCounterTypeTxInc, clear);

        case cAtSdhPathCounterTypeTxNPJC:
            return AuVcCounterGet(self, channel, cAf6Reg_upen_poh_sts_cnt0_Base, cHwStsPointerCounterTypeTxDec, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 Vc1xCounterAddress(ThaModulePmc self, AtSdhChannel channel, uint32 cntOffset, uint32 cntType, eBool clear)
    {
    uint32 channelOffset = mMethodsGet(self)->Vc1xDefaultOffset(self, channel);
    uint32 baseOffset = ThaModulePmcBaseAddress(self);
    uint32 cntTypeOffset;
    uint32 r2cOffset;
    uint32 r2cCoeff, typeCoeff;

    if (mMethodsGet(mThis(self))->Vc1xCounterCoeff(mThis(self), &r2cCoeff, &typeCoeff) != cAtOk)
        return cInvalidUint32;

    cntTypeOffset = cntType * typeCoeff;
    r2cOffset = clear ? 0 : r2cCoeff;

    return baseOffset + cntOffset + cntTypeOffset + channelOffset + r2cOffset;
    }

static uint32 Vc1xCounterGet(ThaModulePmc self, AtSdhChannel channel, uint32 cntOffset, uint32 cntType, eBool clear)
    {
    uint32 address = Vc1xCounterAddress(self, channel, cntOffset, cntType, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 TuVc1xPointerCounterGet(ThaModulePmc self, AtSdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtSdhPathCounterTypeBip:
            return Vc1xCounterGet(self, channel, cAf6Reg_upen_vt_pohpm_Base, cHwVtPohCounterTypeBipError, clear);

        case cAtSdhPathCounterTypeRei:
            return Vc1xCounterGet(self, channel, cAf6Reg_upen_vt_pohpm_Base, cHwVtPohCounterTypeRei, clear);

        case cAtSdhPathCounterTypeRxPPJC:
            return Vc1xCounterGet(self, channel, cAf6Reg_upen_poh_vt_cnt_Base, cHwVtPointerCounterTypeRxInc, clear);

        case cAtSdhPathCounterTypeRxNPJC:
            return Vc1xCounterGet(self, channel, cAf6Reg_upen_poh_vt_cnt_Base, cHwVtPointerCounterTypeRxDec, clear);

        case cAtSdhPathCounterTypeTxPPJC:
            return Vc1xCounterGet(self, channel, cAf6Reg_upen_poh_vt_cnt_Base, cHwVtPointerCounterTypeTxInc, clear);

        case cAtSdhPathCounterTypeTxNPJC:
            return Vc1xCounterGet(self, channel, cAf6Reg_upen_poh_vt_cnt_Base, cHwVtPointerCounterTypeTxDec, clear);

        default:
            return 0;
        }

    return 0;
    }

static uint32 De3CounterAddress(ThaModulePmc self, AtPdhChannel de3, uint32 cntOffset, uint32 cntType, eBool clear)
    {
    uint32 channelOffset = mMethodsGet(self)->De3DefaultOffset(self, de3);
    uint32 baseOffset = ThaModulePmcBaseAddress(self);
    uint32 cntTypeOffset;
    uint32 r2cOffset;
    uint32 r2cCoeff, typeCoeff;

    if (mMethodsGet(mThis(self))->De3CounterCoeff(mThis(self), &r2cCoeff, &typeCoeff) != cAtOk)
        return cInvalidUint32;

    cntTypeOffset = cntType * typeCoeff;
    r2cOffset = clear ? 0 : r2cCoeff;

    return baseOffset + cntOffset + cntTypeOffset + channelOffset + r2cOffset;
    }

static uint32 De3CounterGet(ThaModulePmc self, AtPdhChannel channel, uint32 cntOffset, uint32 cntType, eBool clear)
    {
    uint32 address = De3CounterAddress(self, channel, cntOffset, cntType, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 VcDe3CounterGet(ThaModulePmc self, AtPdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtPdhDe3CounterFBit:
            return De3CounterGet(self, channel, cAf6Reg_upen_pdh_de3cnt_Base, cHwDe3CounterTypeFe, clear);
        case cAtPdhDe3CounterRei:
            return De3CounterGet(self, channel, cAf6Reg_upen_pdh_de3cnt_Base, cHwDe3CounterTypeRei, clear);
        case cAtPdhDe3CounterPBit:
            return De3CounterGet(self, channel, cAf6Reg_upen_pdh_de3cnt_Base, cHwDe3CounterTypePb, clear);
        case cAtPdhDe3CounterCPBit:
            return De3CounterGet(self, channel, cAf6Reg_upen_pdh_de3cnt_Base, cHwDe3CounterTypeCb, clear);
        case cAtPdhDe3CounterBpvExz: return 0;
        case cAtPdhDe3CounterTxCs:   return 0;
        default:
            return 0;
        }
    }

static uint32 VcDe1CounterAddress(ThaModulePmc self, AtPdhChannel de1, uint32 cntOffset, uint32 cntType, eBool clear)
    {
    uint32 channelOffset = mMethodsGet(self)->De1DefaultOffset(self, de1);
    uint32 baseOffset = ThaModulePmcBaseAddress(self);
    uint32 cntTypeOffset;
    uint32 r2cOffset;
    uint32 r2cCoeff, typeCoeff;

    if (mMethodsGet(mThis(self))->De1CounterCoeff(mThis(self), &r2cCoeff, &typeCoeff) != cAtOk)
        return cInvalidUint32;

    cntTypeOffset = cntType * typeCoeff;
    r2cOffset = clear ? 0 : r2cCoeff;

    return baseOffset + cntOffset + cntTypeOffset + channelOffset + r2cOffset;
    }

static uint32 De1CounterGet(ThaModulePmc self, AtPdhChannel de1, uint32 cntOffset, uint32 cntType, eBool clear)
    {
    uint32 address = VcDe1CounterAddress(self, de1, cntOffset, cntType, clear);
    return CounterRead2Clear(self, address, clear);
    }

static uint32 VcDe1CounterGet(ThaModulePmc self, AtPdhChannel channel, uint16 counterType, eBool clear)
    {
    switch (counterType)
        {
        case cAtPdhDe1CounterFe:
            return De1CounterGet(self, channel, cAf6Reg_upen_pdh_de1cntval_Base, cHwDe1CounterTypeFe, clear);
        case cAtPdhDe1CounterRei:
            return De1CounterGet(self, channel, cAf6Reg_upen_pdh_de1cntval_Base, cHwDe1CounterTypeRei, clear);
        case cAtPdhDe1CounterCrc:
            return De1CounterGet(self, channel, cAf6Reg_upen_pdh_de1cntval_Base, cHwDe1CounterTypeCrc, clear);
        case cAtPdhDe1CounterBpvExz: return 0;
        case cAtPdhDe1CounterTxCs:   return 0;
        default:
            return 0;
        }
    }

static eBool HasLongAccess(ThaModulePmc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet LoPwCounterCoeff(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff)
    {
    AtUnused(self);
    *r2cCoeff  = 16384;
    *typeCoeff = 32768;
    return cAtOk;
    }

static eAtRet AuVcCounterCoeff(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff)
    {
    AtUnused(self);
    *r2cCoeff  = 512;
    *typeCoeff = 1024;
    return cAtOk;
    }

static eAtRet Vc1xCounterCoeff(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff)
    {
    AtUnused(self);
    *r2cCoeff  = 16384;
    *typeCoeff = 32768;
    return cAtOk;
    }

static eAtRet De3CounterCoeff(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff)
    {
    AtUnused(self);
    *r2cCoeff  = 512;
    *typeCoeff = 1024;
    return cAtOk;
    }

static eAtRet De1CounterCoeff(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff)
    {
    AtUnused(self);
    *r2cCoeff  = 16384;
    *typeCoeff = 32768;
    return cAtOk;
    }

static void OverrideThaModulePmc(AtModule self)
    {
    ThaModulePmc module = (ThaModulePmc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePmcOverride, mMethodsGet(module), sizeof(m_ThaModulePmcOverride));

        mMethodOverride(m_ThaModulePmcOverride, HasLongAccess);
        mMethodOverride(m_ThaModulePmcOverride, SdhLineCounterGet);
        mMethodOverride(m_ThaModulePmcOverride, SdhLineBlockErrorCounterGet);

        mMethodOverride(m_ThaModulePmcOverride, AuVcPointerCounterGet);
        mMethodOverride(m_ThaModulePmcOverride, AuVcPohCounterGet);
        mMethodOverride(m_ThaModulePmcOverride, TuVc1xPointerCounterGet);
        mMethodOverride(m_ThaModulePmcOverride, VcDe1CounterGet);
        mMethodOverride(m_ThaModulePmcOverride, VcDe3CounterGet);

        mMethodOverride(m_ThaModulePmcOverride, PwTxPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxBytesGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxLbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxRbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxMbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxPbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwTxNbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxMalformedPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxStrayPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxLbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxRbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxMbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxPbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxNbitPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxBytesGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxLostPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxReorderedPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxOutOfSeqDropedPacketsGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxJitBufOverrunGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxJitBufUnderrunGet);
        mMethodOverride(m_ThaModulePmcOverride, PwRxLopsGet);
        }

    mMethodsSet(module, &m_ThaModulePmcOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePmc(self);
    }

static void MethodsInit(Tha60290022ModulePmcV2 self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, LoPwCounterCoeff);
        mMethodOverride(m_methods, AuVcCounterCoeff);
        mMethodOverride(m_methods, Vc1xCounterCoeff);
        mMethodOverride(m_methods, De3CounterCoeff);
        mMethodOverride(m_methods, De1CounterCoeff);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModulePmcV2);
    }

AtModule Tha60290022ModulePmcV2ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePmcObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290022ModulePmcV2New(AtDevice self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (module == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModulePmcV2ObjectInit(module, self);
    }

eAtRet Tha60290022ModulePmcV2HwHiPwIdLookupSet(ThaModulePmc self, AtPw pw, AtSdhChannel hoSdhVc, eBool enable)
    {
    return HwHiPwIdLookupSet(self, pw, hoSdhVc, enable);
    }

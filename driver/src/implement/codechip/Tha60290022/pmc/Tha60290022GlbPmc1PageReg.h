/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_GLBPMC_1PAGE_H_
#define _AF6_REG_AF6CNC0022_RD_GLBPMC_1PAGE_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Group0 Counter
Reg Addr   : 0x2_0000-0x2_FFFF
Reg Formula: 0x2_0000 + $type*32768 + $rdmode*16384 + $pwid
    Where  : 
           + $pwid(0-10751) : Pseudowire ID
           + $type(0-1): counter type
           + $rdmode(0-1): 0 is R2C and 1 is RO
Reg Desc   : 
The register count Tx PW group0 of rate up to VC4

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txpwcnt0_Base                                                                     0x20000
#define cAf6Reg_upen_txpwcnt0_WidthVal                                                                      32

/*--------------------------------------
BitField Name: txpwcnt0
BitField Type: RW
BitField Desc: transmit PW group0 counter + type = 0: tx PW byte counter + type
= 0: tx PW packet counter
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_upen_txpwcnt0_txpwcnt0_Mask                                                              cBit27_0
#define cAf6_upen_txpwcnt0_txpwcnt0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Group1 Counter
Reg Addr   : 0x0_0000-0x1_FFFF
Reg Formula: 0x0_0000 + $type*32768 + $rdmode*16384 + $pwid
    Where  : 
           + $pwid(0-10751) : Pseudowire ID
           + $type(0-3): counter type
           + $rdmode(0-3): 0 is R2C and 1 is RO
Reg Desc   : 
The register count Tx PW group1 of rate up to VC4

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txpwcnt1_Base                                                                     0x00000
#define cAf6Reg_upen_txpwcnt1_WidthVal                                                                      32

/*--------------------------------------
BitField Name: txpwcnt1
BitField Type: RW
BitField Desc: transmit PW group1 counter + type = 0: tx PW Rbit counter + type
= 1: tx PW Pbit counter + type = 2: tx PW Nbit/Mbit counter + type = 3: tx PW
Lbit counter
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_txpwcnt1_txpwcnt1_Mask                                                              cBit17_0
#define cAf6_upen_txpwcnt1_txpwcnt1_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Group0 Counter
Reg Addr   : 0x3_0000-0x3_FFFF
Reg Formula: 0x3_0000 + $type*32768 + $rdmode*16384 + $pwid
    Where  : 
           + $pwid(0-10751) : Pseudowire ID
           + $type(0-1): counter type
           + $rdmode(0-1): 0 is R2C and 1 is RO
Reg Desc   : 
The register count Rx PW group0 of rate up to VC4

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt0_Base                                                                     0x30000
#define cAf6Reg_upen_rxpwcnt0_WidthVal                                                                      32

/*--------------------------------------
BitField Name: rxpwcnt0
BitField Type: RW
BitField Desc: receive PW group0 counter + type = 0: rx PW byte counter + type =
0: rx PW packet counter
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_upen_rxpwcnt0_rxpwcnt0_Mask                                                              cBit27_0
#define cAf6_upen_rxpwcnt0_rxpwcnt0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Group1 Counter
Reg Addr   : 0x4_0000-0x5_FFFF
Reg Formula: 0x4_0000 + $type*32768 + $rdmode*16384 + $pwid
    Where  : 
           + $pwid(0-10751) : Pseudowire ID
           + $type(0-3): counter type
           + $rdmode(0-3): 0 is R2C and 1 is RO
Reg Desc   : 
The register count Rx PW group1 of rate up to VC4

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt1_Base                                                                     0x40000
#define cAf6Reg_upen_rxpwcnt1_WidthVal                                                                      32

/*--------------------------------------
BitField Name: rxpwcnt1
BitField Type: RW
BitField Desc: receive PW group1 counter + type = 0: rx PW Rbit counter + type =
1: rx PW Pbit counter + type = 2: rx PW Nbit/Mbit counter + type = 3: rx PW Lbit
counter
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_rxpwcnt1_rxpwcnt1_Mask                                                              cBit17_0
#define cAf6_upen_rxpwcnt1_rxpwcnt1_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Group2 Counter
Reg Addr   : 0x6_0000-0x7_FFFF
Reg Formula: 0x6_0000 + $type*32768 + $rdmode*16384 + $pwid
    Where  : 
           + $pwid(0-10751) : Pseudowire ID
           + $type(0-3): counter type
           + $rdmode(0-3): 0 is R2C and 1 is RO
Reg Desc   : 
The register count Rx PW group2 of rate up to VC4

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt2_Base                                                                     0x60000
#define cAf6Reg_upen_rxpwcnt2_WidthVal                                                                      32

/*--------------------------------------
BitField Name: rxpwcnt2
BitField Type: RW
BitField Desc: receive PW group2 counter + type = 0: rx PW Stray counter + type
= 1: rx PW Malform counter + type = 2: rx PW Underrun counter + type = 3: rx PW
Overrun counter
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_rxpwcnt2_rxpwcnt2_Mask                                                              cBit17_0
#define cAf6_upen_rxpwcnt2_rxpwcnt2_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Group3 Counter
Reg Addr   : 0x8_0000-0x9_FFFF
Reg Formula: 0x8_0000 + $type*32768 + $rdmode*16384 + $pwid
    Where  : 
           + $pwid(0-10751) : Pseudowire ID
           + $type(0-3): counter type
           + $rdmode(0-3): 0 is R2C and 1 is RO
Reg Desc   : 
The register count Rx PW group2 of rate up to VC4

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcnt3_Base                                                                     0x80000
#define cAf6Reg_upen_rxpwcnt3_WidthVal                                                                      32

/*--------------------------------------
BitField Name: rxpwcnt3
BitField Type: RW
BitField Desc: receive PW group2 counter + type = 0: rx PW LOPS counter + type =
1: rx PW ReorderLost counter + type = 2: rx PW ReorderOk counter + type = 3: rx
PW ReorderDrop counter
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_rxpwcnt2_rxpwcnt3_Mask                                                              cBit17_0
#define cAf6_upen_rxpwcnt2_rxpwcnt3_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Count High Order Look Up Control
Reg Addr   : 0xF8_000 - 0xFA_9FF
Reg Formula: 0xF8_000 + $pwid
    Where  : 
           + $pwid(0-10751): Pseodowire ID
Reg Desc   : 
This register configure lookup from PWID to a pool high speed ID allocated by SW

------------------------------------------------------------------------------*/
#define cAf6Reg_ramcnthotdmlkupcfg_Base                                                                0xF8000
#define cAf6Reg_ramcnthotdmlkupcfg_WidthVal                                                                 32

/*--------------------------------------
BitField Name: PwCntHighRateEn
BitField Type: RW
BitField Desc: Set 1 to indicate the rate of PW is higher than VC4
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ramcnthotdmlkupcfg_PwCntHighRateEn_Mask                                                     cBit5
#define cAf6_ramcnthotdmlkupcfg_PwCntHighRateEn_Shift                                                        5

/*--------------------------------------
BitField Name: HighRateSwPwID
BitField Type: RW
BitField Desc: Allocated high rate PW ID by solfware
BitField Bits: [4:0]
--------------------------------------*/
#define cAf6_ramcnthotdmlkupcfg_HighRateSwPwID_Mask                                                    cBit4_0
#define cAf6_ramcnthotdmlkupcfg_HighRateSwPwID_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire High Rate Transmit Byte Counter
Reg Addr   : 0xF_5000-0xF_5FFF
Reg Formula: 0xF_5000 + $type*64 + $rdmode*32 + $hipwid
    Where  : 
           + $pwid(0-31) : Sw High Rate Pseudowire ID
           + $rdmode(0-1): 0 is R2C and 1 is RO
           + $type(0-3): counter type
Reg Desc   : 
The register count Tx PW byte of rate over VC4

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txpwcntbyte_Base                                                                  0xF5000

/*--------------------------------------
BitField Name: hitxpwbytecnt
BitField Type: RW
BitField Desc: high rate transmit PW byte counter + type = 0 : high rate tx PW
byte counter + type = 1 : high rate tx PW packet counter + type = 2 : high rate
tx PW Rbit counter + type = 3 : high rate tx PW Lbit counter
BitField Bits: [32:00]
--------------------------------------*/
#define cAf6_upen_txpwcntbyte_hitxpwbytecnt_01_Mask                                                   cBit31_0
#define cAf6_upen_txpwcntbyte_hitxpwbytecnt_01_Shift                                                         0
#define cAf6_upen_txpwcntbyte_hitxpwbytecnt_02_Mask                                                   cBit32_0
#define cAf6_upen_txpwcntbyte_hitxpwbytecnt_02_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire High Rate Receive Byte Counter
Reg Addr   : 0xF_6000-0xF_6FFF
Reg Formula: 0xF_6000 + $type*64 + $rdmode*32 + $hipwid
    Where  : 
           + $pwid(0-31) : Sw High Rate Pseudowire ID
           + $rdmode(0-1): 0 is R2C and 1 is RO
           + $type(0-5): counter type
Reg Desc   : 
The register count Rx PW byte of rate over VC4

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpwcntbyte_Base                                                                  0xF6000

/*--------------------------------------
BitField Name: hirxpwbytecnt
BitField Type: RW
BitField Desc: high rate receive PW byte counter + type = 0 : high rate rx PW
byte counter + type = 1 : high rate rx PW packet counter + type = 2 : high rate
rx PW Rbit counter + type = 3 : high rate rx PW Rbit counter + type = 4 : high
rate rx PW Stray counter + type = 5 : high rate rx PW Malform counter
BitField Bits: [32:00]
--------------------------------------*/
#define cAf6_upen_rxpwcntbyte_hirxpwbytecnt_01_Mask                                                   cBit31_0
#define cAf6_upen_rxpwcntbyte_hirxpwbytecnt_01_Shift                                                         0
#define cAf6_upen_rxpwcntbyte_hirxpwbytecnt_02_Mask                                                   cBit32_0
#define cAf6_upen_rxpwcntbyte_hirxpwbytecnt_02_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : PMR Error Counter
Reg Addr   : 0xF_3000 - 0xF_3FFF
Reg Formula: 0xF_3000 + 32*$type + $rdmode*16 + $lineid
    Where  : 
           + $type(0-5): counter type
           + $rdmode(0-1): 0 is R2C and 1 is RO
           + $lineid(0-15)
Reg Desc   : 
The register count information as below. Depending on type it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_pmr_cnt_Base                                                                  0xF3000
#define cAf6Reg_upen_poh_pmr_cnt_WidthVal                                                                   32

/*--------------------------------------
BitField Name: pmr_err_cnt
BitField Type: RW
BitField Desc: + type = 0 : rei_l_block_err + type = 1 : rei_l + type = 2 :
b2_block_err + type = 3 : b2 + type = 4 : b1_block_err + type = 5 : b1
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_upen_poh_pmr_cnt_pmr_err_cnt_Mask                                                        cBit19_0
#define cAf6_upen_poh_pmr_cnt_pmr_err_cnt_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : POH Path STS Counter
Reg Addr   : 0xF_4000-0xF_4FFF
Reg Formula: 0xF_4000 + 1024*$type + 8*$stsid + $slcid
    Where  : 
           + $type(0-1): counter type
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sts_pohpm_Base                                                                    0xF4000
#define cAf6Reg_upen_sts_pohpm_WidthVal                                                                     32

/*--------------------------------------
BitField Name: pohpath_sts_cnt
BitField Type: RW
BitField Desc: pohpath_sts_cnt type = 0:  sts_rei type = 1:  sts_b3
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_sts_pohpm_pohpath_sts_cnt_Mask                                                      cBit17_0
#define cAf6_upen_sts_pohpm_pohpath_sts_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : POH Path VT Counter
Reg Addr   : 0xE_0000 - 0xE_FFFF(RW)
Reg Formula: 0xE_0000 + 32768*$type + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  : 
           + $type(0-1): counter type
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_vt_pohpm_Base                                                                     0xE0000
#define cAf6Reg_upen_vt_pohpm_WidthVal                                                                      32

/*--------------------------------------
BitField Name: pohpath_vt_cnt
BitField Type: RW
BitField Desc: pohpath_vt_cnt type = 0:  vt_rei type = 1:  vt_bip
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_vt_pohpm_pohpath_vt_cnt_Mask                                                        cBit11_0
#define cAf6_upen_vt_pohpm_pohpath_vt_cnt_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : POH vt pointer Counter
Reg Addr   : 0xC_0000-0xD_FFFF(RW)
Reg Formula: 0xC_0000 + $type*32768 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  : 
           + $type(0-3): counter type
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_vt_cnt_Base                                                                   0xC0000
#define cAf6Reg_upen_poh_vt_cnt_WidthVal                                                                    32

/*--------------------------------------
BitField Name: poh_vt_cnt
BitField Type: RW
BitField Desc: poh vt pointer + type = 0 :  pohtx_vt_dec + type = 1 :
pohtx_vt_inc + type = 2 :  pohrx_vt_dec + type = 3 :  pohrx_vt_inc
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_poh_vt_cnt_poh_vt_cnt_Mask                                                          cBit11_0
#define cAf6_upen_poh_vt_cnt_poh_vt_cnt_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : POH sts pointer Counter
Reg Addr   : 0xF_000-0xF_0FFF
Reg Formula: 0xF_000 + 1024*$type + 8*$stsid + $slcid
    Where  : 
           + $type(0-1): counter type
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_sts_cnt0_Base                                                                  0xF0000
#define cAf6Reg_upen_poh_sts_cnt0_WidthVal                                                                  32

/*--------------------------------------
BitField Name: poh_sts_cnt
BitField Type: RW
BitField Desc: poh sts pointer + type = 0 :  pohtx_sts_dec + type = 1 :
pohtx_sts_inc
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_poh_sts_cnt0_poh_sts_cnt_Mask                                                       cBit15_0
#define cAf6_upen_poh_sts_cnt0_poh_sts_cnt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : POH sts pointer Counter
Reg Addr   : 0xF_1000-0xF_1FFF
Reg Formula: 0xF_1000 + 1024*$type + 8*$stsid + $slcid
    Where  : 
           + $type(0-1): counter type
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   : 
The register count information as below. Depending on offset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_poh_sts_cnt1_Base                                                                 0xF1000
#define cAf6Reg_upen_poh_sts_cnt1_WidthVal                                                                  32

/*--------------------------------------
BitField Name: poh_sts_cnt
BitField Type: RW
BitField Desc: poh sts pointer + type = 0 :  pohrx_sts_dec + type = 1 :
pohrx_sts_inc
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_poh_sts_cnt1_poh_sts_cnt_Mask                                                       cBit15_0
#define cAf6_upen_poh_sts_cnt1_poh_sts_cnt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : PDH ds3 cntval Counter
Reg Addr   : 0xF_2000-0xF_2FFF
Reg Formula: 0xF_2000 + $type*1024 + 8*$stsid + $slcid
    Where  : 
           + $type(0-3): counter type
           + $stsid(0-47)
           + $slcid(0-7)
Reg Desc   : 
The register count DS3

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pdh_de3cnt_Base                                                                   0xF2000
#define cAf6Reg_upen_pdh_de3cnt_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ds3_cnt_val
BitField Type: RW
BitField Desc: ds3_cnt_val + type = 0 : FE + type = 1 : REI + type = 2 : PB +
type = 3 : CB
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_pdh_de3cnt_ds3_cnt_val_Mask                                                         cBit17_0
#define cAf6_upen_pdh_de3cnt_ds3_cnt_val_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : PDH ds1 cntval Counter
Reg Addr   : 0xA_0000-0xA_FFFF
Reg Formula: 0xA_0000 + $type*32768 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
    Where  : 
           + $type(0-2): counter type
           + $stsid(0-48)
           + $vtgid(0-6)
           + $vtid(0-3)
           + $slcid(0-7)
Reg Desc   : 
The register count DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pdh_de1cntval_Base                                                                0xA0000
#define cAf6Reg_upen_pdh_de1cntval_WidthVal                                                                 32

/*--------------------------------------
BitField Name: ds1_cnt_val
BitField Type: RW
BitField Desc: ds1_cnt_val + type = 0 : CRC + type = 1 : FE + type = 2 : REI
BitField Bits: [13:00]
--------------------------------------*/
#define cAf6_upen_pdh_de1cntval_ds1_cnt_val_Mask                                                      cBit13_0
#define cAf6_upen_pdh_de1cntval_ds1_cnt_val_Shift                                                            0

#endif /* _AF6_REG_AF6CNC0022_RD_GLBPMC_1PAGE_H_ */

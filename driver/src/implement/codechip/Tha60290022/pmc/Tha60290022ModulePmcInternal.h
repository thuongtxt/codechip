/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMC module name
 * 
 * File        : Tha60290022ModulePmcInternal.h
 * 
 * Created Date: Oct 13, 2017
 *
 * Description : 60290022 PMC internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPMCINTERNAL_H_
#define _THA60290022MODULEPMCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pmc/ThaModulePmcInternal.h"
#include "Tha60290022ModulePmc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModulePmc
    {
    tThaModulePmc super;
    }tTha60290022ModulePmc;

typedef struct tTha60290022ModulePmcV2Methods
    {
    eAtRet (*LoPwCounterCoeff)(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff);
    eAtRet (*AuVcCounterCoeff)(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff);
    eAtRet (*Vc1xCounterCoeff)(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff);
    eAtRet (*De3CounterCoeff)(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff);
    eAtRet (*De1CounterCoeff)(Tha60290022ModulePmcV2 self, uint32 *r2cCoeff, uint32 *typeCoeff);
    }tTha60290022ModulePmcV2Methods;

typedef struct tTha60290022ModulePmcV2
    {
    tTha60290022ModulePmc super;
    const tTha60290022ModulePmcV2Methods *methods;
    }tTha60290022ModulePmcV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290022ModulePmcObjectInit(AtModule self, AtDevice device);
AtModule Tha60290022ModulePmcV2ObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPMCINTERNAL_H_ */


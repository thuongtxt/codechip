/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60290022SgmiiSerdesController.c
 *
 * Created Date: Apr 25, 2017
 *
 * Description : SGMII SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/physical/Tha6029SgmiiSerdesControllerInternal.h"
#include "Tha60290022Physical.h"
#include "../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022SgmiiSerdesController
    {
    tTha6029SgmiiSerdesController super;
    }tTha60290022SgmiiSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6029SgmiiSerdesControllerMethods m_Tha6029SgmiiSerdesControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool UseEngineOfPdhProduct(Tha6029SgmiiSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtDevice Device(Tha6029SgmiiSerdesController self)
    {
    AtDevice device = AtSerdesControllerDeviceGet((AtSerdesController)self);
    return device;
    }

static eBool ShouldUseMdioLocalLoopback(Tha6029SgmiiSerdesController self)
    {
    return Tha60290022DeviceSgmiiSerdesShouldUseMdioLocalLoopback(Device(self));
    }

static void OverrideTha6029SgmiiSerdesController(AtSerdesController self)
    {
    Tha6029SgmiiSerdesController controller = (Tha6029SgmiiSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SgmiiSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6029SgmiiSerdesControllerOverride));

        mMethodOverride(m_Tha6029SgmiiSerdesControllerOverride, UseEngineOfPdhProduct);
        mMethodOverride(m_Tha6029SgmiiSerdesControllerOverride, ShouldUseMdioLocalLoopback);
        }

    mMethodsSet(controller, &m_Tha6029SgmiiSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideTha6029SgmiiSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SgmiiSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SgmiiSerdesControllerObjectInit(self, manager, physicalPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60290022SgmiiSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, manager, physicalPort, serdesId);
    }

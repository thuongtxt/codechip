/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60290022PowerSupplySensorInternal.h
 * 
 * Created Date: Aug 15, 2019
 *
 * Description : Internal data and method for the power supply sensor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022POWERSUPPLYSENSORINTERNAL_H_
#define _THA60290022POWERSUPPLYSENSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../Tha60290021/physical/Tha60290021PowerSupplySensorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022PowerSupplySensor
    {
    tTha60290021PowerSupplySensor super;
    }tTha60290022PowerSupplySensor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022POWERSUPPLYSENSORINTERNAL_H_ */


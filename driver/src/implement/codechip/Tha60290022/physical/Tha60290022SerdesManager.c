/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60290022SerdesManager.c
 *
 * Created Date: Apr 25, 2017
 *
 * Description : SERDES manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022Physical.h"
#include "Tha60290022SerdesManagerInternal.h"
#include "../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesManagerMethods          m_AtSerdesManagerOverride;
static tTha60290021SerdesManagerMethods m_Tha60290021SerdesManagerOverride;

/* Save super implementation */
static const tAtSerdesManagerMethods          *m_AtSerdesManagerMethods          = NULL;
static const tTha60290021SerdesManagerMethods *m_Tha60290021SerdesManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PortsCanBeReduced(Tha60290021SerdesManager self)
    {
    if (Tha60290022DeviceShouldOpenFullCapacity(AtSerdesManagerDeviceGet((AtSerdesManager)self)))
        return cAtFalse;
    return m_Tha60290021SerdesManagerMethods->PortsCanBeReduced(self);
    }

static AtSerdesController FaceplateSerdesControllerObjectCreate(Tha60290021SerdesManager self, AtChannel physicalPort, uint32 serdesId)
    {
    return Tha60290022FaceplateSerdesControllerNew((AtSerdesManager)self, physicalPort, serdesId);
    }

static AtSerdesController SgmiiSerdesControllerObjectCreate(Tha60290021SerdesManager self, AtChannel physicalPort, uint32 serdesId)
    {
    return Tha60290022SgmiiSerdesControllerNew((AtSerdesManager)self, physicalPort, serdesId);
    }

static AtSerdesController MateSerdesControllerObjectCreate(Tha60290021SerdesManager self, AtChannel physicalPort, uint32 serdesId)
    {
    return Tha60290022MateSerdesControllerNew((AtSerdesManager)self, physicalPort, serdesId);
    }

static eBool SpareSgmiiSerdesAreRemoved(Tha60290021SerdesManager self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtDevice Device(AtSerdesManager self)
    {
    return AtSerdesManagerDeviceGet(self);
    }

static eBool KByteSerdesIsControllable(AtSerdesManager self)
    {
    return Tha60290022DeviceKByteSerdesIsControllable(Device(self));
    }

static eBool SerdesIsControllable(AtSerdesManager self, uint32 serdesId)
    {
    if (Tha60290021SerdesManagerSgmiiSerdesLocalId(self, serdesId) == Tha60290021SerdesManagerSgmiiSerdesLocalKByteId(self))
        return KByteSerdesIsControllable(self);

    return m_AtSerdesManagerMethods->SerdesIsControllable(self, serdesId);
    }

static ThaVersionReader VersionReader(AtSerdesManager self)
    {
    AtDevice device = AtSerdesManagerDeviceGet(self);
    return ThaDeviceVersionReader(device);
    }

static eBool ShouldOpenFeature(Tha60290021SerdesManager self, uint32 fromVersion)
    {
    ThaVersionReader versionReader = VersionReader((AtSerdesManager)self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= fromVersion) ? cAtTrue : cAtFalse;
    }

static eBool SerdesEyeScanResetIsSupported(Tha60290021SerdesManager self)
    {
    uint32 startVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x3, 0x8045);
    return ShouldOpenFeature(self, startVersion);
    }

static void OverrideAtSerdesManager(AtSerdesManager self)
    {
    AtSerdesManager manager = (AtSerdesManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesManagerMethods = mMethodsGet(manager);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesManagerOverride, m_AtSerdesManagerMethods, sizeof(m_AtSerdesManagerOverride));

        mMethodOverride(m_AtSerdesManagerOverride, SerdesIsControllable);
        }

    mMethodsSet(manager, &m_AtSerdesManagerOverride);
    }

static void OverrideTha60290021SerdesManager(AtSerdesManager self)
    {
    Tha60290021SerdesManager manager = (Tha60290021SerdesManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021SerdesManagerMethods = mMethodsGet(manager);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021SerdesManagerOverride, mMethodsGet(manager), sizeof(m_Tha60290021SerdesManagerOverride));

        mMethodOverride(m_Tha60290021SerdesManagerOverride, PortsCanBeReduced);
        mMethodOverride(m_Tha60290021SerdesManagerOverride, FaceplateSerdesControllerObjectCreate);
        mMethodOverride(m_Tha60290021SerdesManagerOverride, SgmiiSerdesControllerObjectCreate);
        mMethodOverride(m_Tha60290021SerdesManagerOverride, MateSerdesControllerObjectCreate);
        mMethodOverride(m_Tha60290021SerdesManagerOverride, SpareSgmiiSerdesAreRemoved);
        mMethodOverride(m_Tha60290021SerdesManagerOverride, SerdesEyeScanResetIsSupported);
        }

    mMethodsSet(manager, &m_Tha60290021SerdesManagerOverride);
    }

static void Override(AtSerdesManager self)
    {
    OverrideAtSerdesManager(self);
    OverrideTha60290021SerdesManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SerdesManager);
    }

AtSerdesManager Tha60290022SerdesManagerObjectInit(AtSerdesManager self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021SerdesManagerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesManager Tha60290022SerdesManagerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesManager newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022SerdesManagerObjectInit(newModule, device);
    }

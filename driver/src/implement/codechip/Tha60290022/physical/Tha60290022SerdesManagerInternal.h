/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60290022SerdesManagerInternal.h
 * 
 * Created Date: Aug 20, 2018
 *
 * Description : 60290022 serdes manager internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022SERDESMANAGERINTERNAL_H_
#define _THA60290022SERDESMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/physical/Tha60290021SerdesManagerInternal.h"
#include "Tha60290022Physical.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022SerdesManager
    {
    tTha60290021SerdesManager super;
    }tTha60290022SerdesManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManager Tha60290022SerdesManagerObjectInit(AtSerdesManager self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022SERDESMANAGERINTERNAL_H_ */

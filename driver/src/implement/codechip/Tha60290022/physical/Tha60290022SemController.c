/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60290022SemController.c
 *
 * Created Date: Aug 31, 2018
 *
 * Description : 60290022 Sem controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022SemControllerInternal.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "Tha60290022Physical.h"

/*--------------------------- Define -----------------------------------------*/


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSemControllerMethods  m_AtSemControllerOverride;

/* Super implementations */
static const tAtSemControllerMethods  *m_AtSemControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumSlr(AtSemController self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 MaxLfa(AtSemController self)
    {
    AtUnused(self);
    return 0x0000F20B;
    }

static const char* DeviceDescription(AtSemController self)
    {
    AtUnused(self);
    return "XCVU13P";
    }

static void OverrideAtSemController(AtSemController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSemControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSemControllerOverride, m_AtSemControllerMethods, sizeof(m_AtSemControllerOverride));

        mMethodOverride(m_AtSemControllerOverride, MaxLfa);
        mMethodOverride(m_AtSemControllerOverride, NumSlr);
        mMethodOverride(m_AtSemControllerOverride, DeviceDescription);
        }

    mMethodsSet(self, &m_AtSemControllerOverride);
    }

static void Override(AtSemController self)
    {
    OverrideAtSemController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SemController);
    }

static AtSemController ObjectInit(AtSemController self, AtDevice device, uint32 semId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021SemControllerObjectInit(self, device, semId) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSemController Tha60290022SemControllerNew(AtDevice device, uint32 semId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSemController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (controller == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(controller, device, semId);
    }

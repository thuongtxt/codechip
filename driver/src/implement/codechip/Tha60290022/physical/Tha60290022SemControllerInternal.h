/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60290022SemControllerInternal.h
 * 
 * Created Date: Aug 31, 2018
 *
 * Description : Tha60290022 Sem controller internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022SEMCONTROLLERINTERNAL_H_
#define _THA60290022SEMCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/physical/Tha60290021SemControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022SemController
    {
    tTha60290021SemController super;
    }tTha60290022SemController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290022SEMCONTROLLERINTERNAL_H_ */


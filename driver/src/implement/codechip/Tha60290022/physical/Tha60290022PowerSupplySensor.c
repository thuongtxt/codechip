/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PHYSICAL
 *
 * File        : Tha60290021PowerSupplySensor.c
 *
 * Created Date: Aug 15, 2019
 *
 * Description : Tha60290022PowerSupplySensor implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022PowerSupplySensorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSensorMethods             m_AtSensorOverride;

/* Save super implementation */
static const tAtSensorMethods     *m_AtSensorMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtSensor self)
    {
    eAtRet ret;

    ret = m_AtSensorMethods->Init(self);
    if (ret != cAtOk)
        return ret;
    /*ds923-virtex-ultrascale-plus.pdf*/
    ret = AtPowerSupplySensorAlarmUpperThresholdSet((AtPowerSupplySensor)self, cAtPowerSupplyVoltageInt, 742);
    ret |= AtPowerSupplySensorAlarmLowerThresholdSet((AtPowerSupplySensor)self, cAtPowerSupplyVoltageInt, 698);
    ret |= AtPowerSupplySensorAlarmUpperThresholdSet((AtPowerSupplySensor)self, cAtPowerSupplyVoltageBram, 876);
    ret |= AtPowerSupplySensorAlarmLowerThresholdSet((AtPowerSupplySensor)self, cAtPowerSupplyVoltageBram, 825);
    ret |= AtPowerSupplySensorAlarmUpperThresholdSet((AtPowerSupplySensor)self, cAtPowerSupplyVoltageAux, 1854);
    ret |= AtPowerSupplySensorAlarmLowerThresholdSet((AtPowerSupplySensor)self, cAtPowerSupplyVoltageAux, 1746);

    return ret;
    }

static void OverrideAtSensor(AtPowerSupplySensor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSensorMethods = mMethodsGet((AtSensor)self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSensorOverride, m_AtSensorMethods, sizeof(m_AtSensorOverride));

        mMethodOverride(m_AtSensorOverride, Init);
        }

    mMethodsSet(((AtSensor)self), &m_AtSensorOverride);
    }

static void Override(AtPowerSupplySensor self)
    {
    OverrideAtSensor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PowerSupplySensor);
    }

static AtPowerSupplySensor ObjectInit(AtPowerSupplySensor self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021PowerSupplySensorObjectInit(self, device) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPowerSupplySensor Tha60290022PowerSupplySensorNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPowerSupplySensor newSensor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSensor == NULL)
        return NULL;

    return ObjectInit(newSensor, device);
    }

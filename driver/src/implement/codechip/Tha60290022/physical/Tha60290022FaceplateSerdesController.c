/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60290022FaceplateSerdesController.c
 *
 * Created Date: Apr 25, 2017
 *
 * Description : Faceplate SERDES
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "Tha60290022FaceplateSerdesControllerInternal.h"
#include "../prbs/Tha60290022ModulePrbs.h"
#include "Tha60290022Physical.h"
#include "../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods               m_AtSerdesControllerOverride;
static tTha6029SerdesControllerMethods          m_Tha6029SerdesControllerOverride;
static tTha6029FaceplateSerdesControllerMethods m_Tha6029FaceplateSerdesControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool UseDifferentRemoteLoopbackFor10G(Tha6029FaceplateSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtPrbsEngine RawPrbsEngineObjectCreate(Tha6029FaceplateSerdesController self)
    {
    return Tha60290022FacePlateSerdesPrbsEngineNew((AtSerdesController)self);
    }

static eBool CanControlTimingMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtDevice Device(Tha6029FaceplateSerdesController self)
    {
    AtDevice device = AtSerdesControllerDeviceGet((AtSerdesController)self);
    return device;
    }

static eBool PmaFarEndLoopbackSupported(Tha6029FaceplateSerdesController self)
    {
    return Tha60290022DeviceFaceplateSerdesPmaFarEndLoopbackSupported(Device(self));
    }

static eBool EthFramedPrbsIsRemoved(Tha6029FaceplateSerdesController self)
    {
    /* Not now */
    AtUnused(self);
    return cAtFalse;
    }

static eBool Stm1SamplingFromStm4(Tha6029FaceplateSerdesController self)
    {
    return Tha60290022DeviceFaceplateSerdesStm1SamplingFromStm4(Device(self));
    }

static uint32 StartVersionShouldNotTouchAfeTrim(Tha6029FaceplateSerdesController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eBool HasNewParams(Tha6029FaceplateSerdesController self)
    {
    return Tha60290022DeviceFaceplateSerdesHasNewParams(Device(self));
    }

static eBool DataMaskOnResetConfigurable(Tha6029FaceplateSerdesController self)
    {
    AtDevice device = AtSerdesControllerDeviceGet((AtSerdesController)self);
    return Tha60290022DeviceFacePlateSerdesDataMaskOnResetConfigurable(device);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, CanControlTimingMode);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideTha6029SerdesController(AtSerdesController self)
    {
    Tha6029SerdesController controller = (Tha6029SerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6029SerdesControllerOverride));

        }

    mMethodsSet(controller, &m_Tha6029SerdesControllerOverride);
    }

static void OverrideTha6029FaceplateSerdesController(AtSerdesController self)
    {
    Tha6029FaceplateSerdesController controller = (Tha6029FaceplateSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029FaceplateSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6029FaceplateSerdesControllerOverride));

        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, UseDifferentRemoteLoopbackFor10G);
        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, RawPrbsEngineObjectCreate);
        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, PmaFarEndLoopbackSupported);
        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, EthFramedPrbsIsRemoved);
        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, Stm1SamplingFromStm4);
        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, StartVersionShouldNotTouchAfeTrim);
        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, HasNewParams);
        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, DataMaskOnResetConfigurable);
        }

    mMethodsSet(controller, &m_Tha6029FaceplateSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideTha6029SerdesController(self);
    OverrideTha6029FaceplateSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022FaceplateSerdesController);
    }

AtSerdesController Tha60290022FaceplateSerdesControllerObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029FaceplateSerdesControllerObjectInit(self, manager, physicalPort, serdesId) == NULL)
       return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60290022FaceplateSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022FaceplateSerdesControllerObjectInit(newModule, manager, physicalPort, serdesId);
    }

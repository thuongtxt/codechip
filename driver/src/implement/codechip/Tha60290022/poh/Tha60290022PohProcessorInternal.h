/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60290011PohProcessorInternal.h
 *
 * Created Date: Sep 8, 2016 
 *
 * Description : POH Procecssor Internal Interface
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022POHPROCESSORINTERNAL_H_
#define _THA60290022POHPROCESSORINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/poh/Tha60210051PohProcessorInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022AuVcPohProcessor
    {
    tTha60210051AuVcPohProcessor super;
    }tTha60290022AuVcPohProcessor;

typedef struct tTha60290022Tu3VcPohProcessor
    {
    tTha60210051Tu3VcPohProcessor super;
    }tTha60290022Tu3VcPohProcessor;

typedef struct tTha60290022Vc1xPohProcessor
    {
    tTha60210051Vc1xPohProcessor super;
    }tTha60290022Vc1xPohProcessor;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* _THA60290022POHPROCESSORINTERNAL_H_ */

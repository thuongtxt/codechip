/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60290022ModulePoh.c
 *
 * Created Date: Apr 29, 2017
 *
 * Description : POH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/ocn/Tha60290021ModuleOcn.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "Tha60290022ModulePohInternal.h"
#include "Tha60290022ModulePohReg.h"
#include "Tha60290022ModulePoh.h"
#include "Tha60290022PohProcessor.h"
#include "../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/
#define cStsMax   47
#define cVtMax    27
#define cVtSliceMax  3
#define cStsSliceMax 4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePohMethods         m_ThaModulePohOverride;
static tTha60210011ModulePohMethods m_Tha60210011ModulePohOverride;
static tTha60290021ModulePohMethods m_Tha60290021ModulePohOverride;
static tAtModuleMethods             m_AtModuleOverride;

/* Save super implementation */
static const tTha60210011ModulePohMethods *m_Tha60210011ModulePohMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 LinepohgrbBase(AtSdhChannel sdhChannel)
    {
    AtUnused(sdhChannel);
    return cAf6Reg_pohstspohgrb1_Base;
    }

static uint32 StspohgrbBase(ThaModulePoh self, AtSdhChannel sdhChannel)
    {
    uint8 slice, sts;

    AtUnused(self);

    if (AtSdhChannelTypeGet(sdhChannel) == cAtSdhChannelTypeLine)
        return LinepohgrbBase(sdhChannel);

    if (ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePoh, &slice, &sts) == cAtOk)
        return (slice < 4) ? cAf6Reg_pohstspohgrb_Base : cAf6Reg_pohstspohgrb1_Base;

    return cInvalidUint32;
    }

static uint32 VtpohgrbBase(ThaModulePoh self, AtSdhChannel sdhChannel)
    {
    uint8 slice, sts;

    AtUnused(self);

    if (ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePoh, &slice, &sts) == cAtOk)
        return (slice < 4) ? cAf6Reg_pohvtpohgrb_Base : cAf6Reg_pohvtpohgrb1_Base;

    return cInvalidUint32;
    }

static uint32 Reg_pohstspohgrb(ThaModulePoh self, AtSdhChannel sdhChannel, uint8 sliceid, uint8 stsid)
    {
    AtUnused(self);

    /* Hardware use local slice for Paths */
    if (AtSdhChannelTypeGet(sdhChannel) != cAtSdhChannelTypeLine)
        sliceid = sliceid % 4;

    return stsid + (sliceid * 48UL);
    }

static uint32 Reg_pohvtpohgrb(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 vtid)
    {
    AtUnused(self);
    sliceid = sliceid % 4;
    return (sliceid * 32UL) + (stsid * 128UL) + vtid;
    }

static uint32 CpestsctrBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohcpestsctr_Base;
    }

static uint32 CpeStsStatus(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohcpestssta_Base;
    }

static uint32 MsgstsexpBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohmsgstsexp_Base;
    }

static uint32 MsgstscurBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohmsgstscur_Base;
    }

static uint32 MsgstsinsBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohmsgstsins_Base;
    }

static uint32 MsgvtinsBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohmsgvtins_Base;
    }

static uint32 Reg_pohmsgstsins(ThaModulePoh self, uint8 sliceid, uint8 stsid, uint8 msgid)
    {
    AtUnused(self);
    return (sliceid * 384UL) + (stsid * 8UL) + msgid;
    }

static uint32 IpmCnthiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_ipm_cnthi_Base;
    }

static uint32 AlmMskhiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_alm_mskhi_Base;
    }

static uint32 AlmStahiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_alm_stahi_Base;
    }

static uint32 AlmChghiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_alm_chghi_Base;
    }

static uint32 IpmCntloBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_ipm_cntlo_Base;
    }

static uint32 AlmStaloBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_alm_stalo_Base;
    }

static uint32 AlmChgloBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_alm_chglo_Base;
    }

static uint32 MsgvtcurBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_pohmsgvtcur_Base;
    }

static ThaPohProcessor AuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60290022AuVcPohProcessorNew(vc);
    }

static uint32 HoPathInterruptOffset(Tha60210011ModulePoh self, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(self);
    return hwSts + (hwSlice * 256UL);
    }

static uint32 LoPathInterruptOffset(Tha60210011ModulePoh self, uint8 hwSlice, uint8 hwSts, uint8 vtgId, uint8 vtId)
    {
    AtUnused(self);
    return (hwSts * 32UL) + (hwSlice * 8192UL) + (vtgId * 4UL) + vtId;
    }

static ThaTtiProcessor FaceplateLineTtiProcessorObjectCreate(Tha60290021ModulePoh self, AtSdhLine line)
    {
    AtUnused(self);
    return Tha60290022FaceplateLineTtiProcessorNew((AtSdhChannel)line);
    }

static uint32 AddressWithLocalAddress(ThaModulePoh self, uint32 localAddress)
    {
    return localAddress + ThaModulePohBaseAddress(self);
    }

static uint32 NumPohSlices(ThaModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    Tha60290021ModuleOcn ocnModule = (Tha60290021ModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    return Tha60290021ModuleOcnNumFaceplateSlices(ocnModule);
    }

static eAtRet MonitoringDefaultSet(ThaModulePoh self)
    {
    uint32 numSlices = NumPohSlices(self);
    uint32 slice_i;
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_pcfg_glbcpectr_Base);
    uint32 regVal = 0;

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        {
        uint32 fieldMask, fieldShift;

        /* Enable STS layer */
        fieldMask = cAf6_pcfg_glbcpectr_cpestsline0en_Mask << slice_i;
        fieldShift = slice_i;
        mRegFieldSet(regVal, field, 1);

        /* Enable VT layer */
        fieldMask = cAf6_pcfg_glbcpectr_cpevtline0en_Mask << slice_i;
        fieldShift = slice_i + cAf6_pcfg_glbcpectr_cpevtline0en_Shift;
        mRegFieldSet(regVal, field, 1);
        }

    /* Line layer will start from 8 */
    mRegFieldSet(regVal, cAf6_pcfg_glbcpectr_cpestsline8en_, 1);

    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static AtDevice Device(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return device;
    }

static eBool HasCpujnreqen(AtModule self)
    {
    return Tha60290022DevicePohHasCpujnreqen(Device(self));
    }

static eAtRet HwJnRequestDdrEnable(Tha60210011ModulePoh self, eBool enable)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;
    uint32 regAddr = AddressWithLocalAddress(pohModule, cAf6Reg_pcfg_glbctr_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 numSlices = NumPohSlices(pohModule);
    uint32 line_i;
    uint32 fieldVal;

    /* Common Jn request */
    fieldVal = 0;
    for (line_i = 0; line_i < numSlices + 1; line_i++) /* For Jn, plus 1 for line 9 which is used for TTI */
        mFieldIns(&fieldVal, cBit0 << line_i, line_i, enable ? 1 : 0);
    mRegFieldSet(regVal, cAf6_pcfg_glbctr_cpejnreqlineen_, fieldVal);

    /* Enable for all STS groups */
    fieldVal = 0;
    if (enable)
        fieldVal = cAf6_pcfg_glbctr_cpejnreqstsen_Mask >> cAf6_pcfg_glbctr_cpejnreqstsen_Shift;
    mRegFieldSet(regVal, cAf6_pcfg_glbctr_cpejnreqstsen_, fieldVal);

    if (HasCpujnreqen((AtModule)self))
        mRegFieldSet(regVal, cAf6_pcfg_glbctr_cpujnreqen_, (enable) ? 1 : 0);

    mModuleHwWrite(self, regAddr, regVal);

    /* J0 request */
    regAddr = AddressWithLocalAddress(pohModule, cAf6Reg_pcfg_glbctr2_Base);
    regVal = mModuleHwRead(self, regAddr);
    fieldVal = 0;
    for (line_i = 0; line_i < numSlices; line_i++)
        mFieldIns(&fieldVal, cBit0 << line_i, line_i, enable ? 1 : 0);
    mRegFieldSet(regVal, cAf6_pcfg_glbctr2_terjnreqslen_, fieldVal);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet JnRequestDdrEnable(Tha60210011ModulePoh self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret = HwJnRequestDdrEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    if (enable)
        return cAtOk;

    return Tha60290022ModulePohJnRequestQdrAndPohCpuDisableDoneCheck((ThaModulePoh)self);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);

    if ((localAddress >= 0x0200000) && (localAddress <= 0x02FFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsPohstspohgrbWithRegBase(Tha60210011ModulePoh self, uint32 localAddress, uint32 regBase)
    {
    uint32 startAddr = regBase;
    uint32 stopAddr = startAddr + cStsMax + cStsSliceMax * (cStsMax + 1);
    AtUnused(self);
    return mInRange(localAddress, startAddr, stopAddr) ? cAtTrue : cAtFalse;
    }

static eBool IsPohstspohgrb(Tha60210011ModulePoh self, uint32 localAddress)
    {
    if (IsPohstspohgrbWithRegBase(self, localAddress, cAf6Reg_pohstspohgrb_Base) ||
        IsPohstspohgrbWithRegBase(self, localAddress, cAf6Reg_pohstspohgrb1_Base))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsPohvtpohgrbWithRegBase(Tha60210011ModulePoh self, uint32 localAddress, uint32 regBase)
    {
    uint32 startAddr = regBase;
    uint32 stopAddr  = startAddr + (cVtSliceMax * 32) + (cStsMax * 128) + cVtMax;
    AtUnused(self);
    return mInRange(localAddress, startAddr, stopAddr) ? cAtTrue : cAtFalse;
    }

static eBool IsPohvtpohgrb(Tha60210011ModulePoh self, uint32 localAddress)
    {
    if (IsPohvtpohgrbWithRegBase(self, localAddress, cAf6Reg_pohvtpohgrb_Base) ||
        IsPohvtpohgrbWithRegBase(self, localAddress, cAf6Reg_pohvtpohgrb1_Base))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsGrabberAddress(Tha60210011ModulePoh self, uint32 localAddress)
    {
    if (IsPohstspohgrb(self, localAddress) || IsPohvtpohgrb(self, localAddress))
        return cAtTrue;
    return cAtFalse;
    }

static eBool LocalAddressBelongTo155Part(Tha60210011ModulePoh self, uint32 localAddress)
    {
    if (IsGrabberAddress(self, localAddress))
        return cAtFalse;

    return cAtTrue;
    }

static uint32 StartVersionSupportSeparateRdiErdiS(Tha60210011ModulePoh self)
    {
    return Tha60290022DevicePohStartVersionSupportSeparateRdiErdiS(Device((AtModule)self));
    }

static ThaPohProcessor Tu3VcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60290022Tu3VcPohProcessorNew(vc);
    }

static ThaPohProcessor Vc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60290022Vc1xPohProcessorNew(vc);
    }

static eAtRet RequestDdrDisableIsDoneCheck(ThaModulePoh self, uint32 mask, uint32 shift)
    {
    uint32 address = cAf6Reg_pcfg_glbalm_Base + ThaModulePohBaseAddress(self);
    tAtOsalCurTime startTime, curTime;
    uint32 elapsedTimeMs = 0;
    static uint32 cTimeoutMs = 1000;

    if (AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)self)))
        return cAtOk;

    AtOsalCurTimeGet(&startTime);
    while (elapsedTimeMs < cTimeoutMs)
        {
        uint32 regVal = mModuleHwRead(self, address);
        eBool isDone = cAtFalse;

        mFieldIns(&regVal, mask, shift, 1);
        mModuleHwWrite(self, address, regVal);
        AtOsalUSleep(1000);
        regVal = mModuleHwRead(self, address);
        isDone = regVal & mask ? cAtFalse : cAtTrue;
        if (isDone)
            return cAtOk;

        AtOsalUSleep(1000);
        AtOsalCurTimeGet(&curTime);
        elapsedTimeMs = AtOsalDifferenceTimeInMs(&curTime, &startTime);
        }

    return cAtErrorOperationTimeout;
    }

static eAtRet JnRequestQdrAndPohCpuDisableDoneCheck(ThaModulePoh self)
    {
    if (HasCpujnreqen((AtModule)self))
        return RequestDdrDisableIsDoneCheck(self, cJnQdrandPohCpuRequestDisableIsDone_Mask, cJnQdrandPohCpuRequestDisableIsDone_Shift);

    return cAtOk;
    }

static eAtRet BerRequestDdrDisableIsDoneHwCheck(ThaModulePoh self)
    {
    if (HasCpujnreqen((AtModule)self))
        return RequestDdrDisableIsDoneCheck(self, cBerDdrRequestDisableIsDone_Mask, cBerDdrRequestDisableIsDone_Shift);

    return cAtOk;
    }

static void OverrideAtModule(AtModule self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, HasRegister);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideTha60290021ModulePoh(AtModule self)
    {
    Tha60290021ModulePoh module = (Tha60290021ModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModulePohOverride, mMethodsGet(module), sizeof(m_Tha60290021ModulePohOverride));

        mMethodOverride(m_Tha60290021ModulePohOverride, FaceplateLineTtiProcessorObjectCreate);
        }

    mMethodsSet(module, &m_Tha60290021ModulePohOverride);
    }

static void OverrideTha60210011ModulePoh(AtModule self)
    {
    Tha60210011ModulePoh pohModule = (Tha60210011ModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePohMethods = mMethodsGet(pohModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePohOverride, m_Tha60210011ModulePohMethods, sizeof(m_Tha60210011ModulePohOverride));

        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohstspohgrb);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohvtpohgrb);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_pohmsgstsins);
        mMethodOverride(m_Tha60210011ModulePohOverride, HoPathInterruptOffset);
        mMethodOverride(m_Tha60210011ModulePohOverride, LoPathInterruptOffset);
        mMethodOverride(m_Tha60210011ModulePohOverride, JnRequestDdrEnable);
        mMethodOverride(m_Tha60210011ModulePohOverride, LocalAddressBelongTo155Part);
        mMethodOverride(m_Tha60210011ModulePohOverride, StartVersionSupportSeparateRdiErdiS);
        }

    mMethodsSet(pohModule, &m_Tha60210011ModulePohOverride);
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, StspohgrbBase);
        mMethodOverride(m_ThaModulePohOverride, CpestsctrBase);
        mMethodOverride(m_ThaModulePohOverride, CpeStsStatus);
        mMethodOverride(m_ThaModulePohOverride, MsgstsexpBase);
        mMethodOverride(m_ThaModulePohOverride, MsgstscurBase);
        mMethodOverride(m_ThaModulePohOverride, MsgstsinsBase);
        mMethodOverride(m_ThaModulePohOverride, MsgvtinsBase);
        mMethodOverride(m_ThaModulePohOverride, IpmCnthiBase);
        mMethodOverride(m_ThaModulePohOverride, AlmMskhiBase);
        mMethodOverride(m_ThaModulePohOverride, AlmStahiBase);
        mMethodOverride(m_ThaModulePohOverride, AlmChghiBase);
        mMethodOverride(m_ThaModulePohOverride, VtpohgrbBase);
        mMethodOverride(m_ThaModulePohOverride, IpmCntloBase);
        mMethodOverride(m_ThaModulePohOverride, AlmStaloBase);
        mMethodOverride(m_ThaModulePohOverride, AlmChgloBase);
        mMethodOverride(m_ThaModulePohOverride, MsgvtcurBase);
        mMethodOverride(m_ThaModulePohOverride, AuVcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Tu3VcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Vc1xPohProcessorCreate);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePoh(self);
    OverrideTha60210011ModulePoh(self);
    OverrideTha60290021ModulePoh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModulePoh);
    }

AtModule Tha60290022ModulePohObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModulePohObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290022ModulePohNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModulePohObjectInit(newModule, device);
    }

eAtRet Tha60290022ModulePohMonitoringDefaultSet(ThaModulePoh self)
    {
    if (self)
        return MonitoringDefaultSet(self);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290022ModulePohJnRequestQdrAndPohCpuDisableDoneCheck(ThaModulePoh self)
    {
    if (self)
        return JnRequestQdrAndPohCpuDisableDoneCheck(self);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha60290022ModulePohBerRequestDdrDisableDoneCheck(ThaModulePoh self)
    {
    if (self)
        return BerRequestDdrDisableIsDoneHwCheck(self);
    return cAtErrorObjectNotExist;
    }

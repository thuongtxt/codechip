/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60290022ModulePoh.h
 * 
 * Created Date: May 17, 2017
 *
 * Description : POH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPOH_H_
#define _THA60290022MODULEPOH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/poh/ThaModulePoh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290022ModulePohMonitoringDefaultSet(ThaModulePoh self);
eAtRet Tha60290022ModulePohJnRequestQdrAndPohCpuDisableDoneCheck(ThaModulePoh self);
eAtRet Tha60290022ModulePohBerRequestDdrDisableDoneCheck(ThaModulePoh self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPOH_H_ */


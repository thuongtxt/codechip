/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60290022ModulePohV2.c
 *
 * Created Date: Feb 6, 2018
 *
 * Description : POH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "Tha60290022ModulePohInternal.h"
#include "Tha60290022ModulePohV2Reg.h"
#include "Tha60290022ModulePoh.h"
#include "Tha60290022PohProcessor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePohMethods         m_ThaModulePohOverride;
static tTha60210011ModulePohMethods m_Tha60210011ModulePohOverride;

/* Save super implementation */
static const tTha60210011ModulePohMethods *m_Tha60210011ModulePohMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 AlmStahiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_alm_stahi_Base;
    }

static uint32 AlmChghiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cAf6Reg_alm_chghi_Base;
    }

static uint32 HoPathInterruptOffset(Tha60210011ModulePoh self, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(self);
    return (hwSts * 16UL) + hwSlice;
    }

static void OverrideTha60210011ModulePoh(AtModule self)
    {
    Tha60210011ModulePoh pohModule = (Tha60210011ModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePohMethods = mMethodsGet(pohModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePohOverride, m_Tha60210011ModulePohMethods, sizeof(m_Tha60210011ModulePohOverride));

        mMethodOverride(m_Tha60210011ModulePohOverride, HoPathInterruptOffset);
        }

    mMethodsSet(pohModule, &m_Tha60210011ModulePohOverride);
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, AlmStahiBase);
        mMethodOverride(m_ThaModulePohOverride, AlmChghiBase);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePoh(self);
    OverrideTha60210011ModulePoh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModulePohV2);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePohObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290022ModulePohV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }


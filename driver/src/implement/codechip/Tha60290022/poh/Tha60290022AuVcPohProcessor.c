/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60290011AuVcPohProcessor.c
 *
 * Created Date: Oct 6, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "Tha60290022ModulePohReg.h"
#include "Tha60290022PohProcessorInternal.h"
#include "Tha60290022PohProcessor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011AuVcPohProcessorMethods  m_Tha60210011AuVcPohProcessorOverride;

/* Save super implementation */
static const tTha60210011AuVcPohProcessorMethods *m_Tha60210011AuVcPohProcessorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePoh ModulePoh(Tha60210011AuVcPohProcessor self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    return (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    }

static uint32 PohCpeStatusRegAddr(Tha60210011AuVcPohProcessor self)
    {
    return ThaModulePohCpeStsStatus(ModulePoh(self));
    }

static void OverrideTha60210011AuVcPohProcessor(ThaPohProcessor self)
    {
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011AuVcPohProcessorMethods = mMethodsGet(processor);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011AuVcPohProcessorOverride, mMethodsGet(processor), sizeof(m_Tha60210011AuVcPohProcessorOverride));

        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCpeStatusRegAddr);
        }

    mMethodsSet(processor, &m_Tha60210011AuVcPohProcessorOverride);
    }

static void Override(ThaPohProcessor self)
    {
    OverrideTha60210011AuVcPohProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022AuVcPohProcessor);
    }

static ThaPohProcessor ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051AuVcPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60290022AuVcPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newProcessor, vc);
    }


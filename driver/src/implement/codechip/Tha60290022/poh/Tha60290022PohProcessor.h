/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60290022PohProcessor.h
 * 
 * Created Date: Aug 18, 2017
 *
 * Description : Processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022POHPROCESSOR_H_
#define _THA60290022POHPROCESSOR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

ThaPohProcessor Tha60290022AuVcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha60290022Tu3VcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha60290022Vc1xPohProcessorNew(AtSdhVc vc);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022POHPROCESSOR_H_ */


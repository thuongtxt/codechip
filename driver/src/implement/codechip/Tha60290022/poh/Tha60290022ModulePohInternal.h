/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60290022ModulePohInternal.h
 * 
 * Created Date: Feb 6, 2018
 *
 * Description : POH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPOHINTERNAL_H_
#define _THA60290022MODULEPOHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/poh/Tha60290021ModulePohInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModulePoh
    {
    tTha60290021ModulePoh super;
    }tTha60290022ModulePoh;

typedef struct tTha60290022ModulePohV2
    {
    tTha60290022ModulePoh super;
    }tTha60290022ModulePohV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290022ModulePohObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPOHINTERNAL_H_ */


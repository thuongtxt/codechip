/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60290022FaceplateLineTtiProcessor.c
 *
 * Created Date: Apr 30, 2017
 *
 * Description : Line TTI processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/poh/Tha60290021FaceplateLineTtiProcessorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022FaceplateLineTtiProcessor
    {
    tTha60290021FaceplateLineTtiProcessor super;
    }tTha60290022FaceplateLineTtiProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021FaceplateLineTtiProcessorMethods m_Tha60290021FaceplateLineTtiProcessorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 GrabberSliceId(Tha60290021FaceplateLineTtiProcessor self)
    {
    AtUnused(self);
    return 4;
    }

static uint8 BufferSliceId(Tha60290021FaceplateLineTtiProcessor self)
    {
    AtUnused(self);
    return 8;
    }

static void OverrideTha60290021FaceplateLineTtiProcessor(ThaTtiProcessor self)
    {
    Tha60290021FaceplateLineTtiProcessor processor = (Tha60290021FaceplateLineTtiProcessor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021FaceplateLineTtiProcessorOverride, mMethodsGet(processor), sizeof(m_Tha60290021FaceplateLineTtiProcessorOverride));

        mMethodOverride(m_Tha60290021FaceplateLineTtiProcessorOverride, GrabberSliceId);
        mMethodOverride(m_Tha60290021FaceplateLineTtiProcessorOverride, BufferSliceId);
        }

    mMethodsSet(processor, &m_Tha60290021FaceplateLineTtiProcessorOverride);
    }

static void Override(ThaTtiProcessor self)
    {
    OverrideTha60290021FaceplateLineTtiProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022FaceplateLineTtiProcessor);
    }

static ThaTtiProcessor ObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021FaceplateLineTtiProcessorObjectInit(self, sdhChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaTtiProcessor Tha60290022FaceplateLineTtiProcessorNew(AtSdhChannel sdhChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaTtiProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, sdhChannel);
    }

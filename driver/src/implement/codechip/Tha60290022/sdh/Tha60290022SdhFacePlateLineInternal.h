/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290022SdhFacePlateLineInternal.h
 * 
 * Created Date: Aug 21, 2018
 *
 * Description : SDH Faceplate line
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022SDHFACEPLATELINEINTERNAL_H_
#define _THA60290022SDHFACEPLATELINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/sdh/Tha6029SdhFacePlateLineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022SdhFacePlateLine
    {
    tTha6029SdhFacePlateLine super;
    }tTha60290022SdhFacePlateLine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhLine Tha60290022SdhFacePlateLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022SDHFACEPLATELINEINTERNAL_H_ */


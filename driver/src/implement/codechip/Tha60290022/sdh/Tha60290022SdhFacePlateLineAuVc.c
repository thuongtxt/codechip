/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290022SdhFacePlateLineAuVc.c
 *
 * Created Date: Sep 25, 2017
 *
 * Description : PWCodechip-60290022 AU VC FacePlate Line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/xc/ThaVcCrossConnect.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../Tha60290021/sdh/Tha6029SdhFacePlateLineAuVcInternal.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../ocn/Tha60290022ModuleOcnReg.h"
#include "Tha60290022ModuleSdh.h"
#include "Tha60290022SdhFacePlateLineAuVc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha60290022SdhFacePlateLineAuVc
    {
    tTha6029SdhFacePlateLineAuVc super;
    }tTha60290022SdhFacePlateLineAuVc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhVcMethods                m_ThaSdhVcOverride;

/* Super implementation */

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn OcnModule(ThaSdhVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static eAtRet AutoRdiReiEnable(ThaSdhVc self, eBool enable)
    {
    ThaModuleOcn ocnModule = OcnModule(self);
    AtSdhChannel channel = (AtSdhChannel)self;
    uint32 address = Tha60210011ModuleOcnStsPointerInterpreterPerChannelControl((Tha60210011ModuleOcn)ocnModule, channel) +
                     ThaModuleOcnStsDefaultOffset(ocnModule, channel, AtSdhChannelSts1Get(channel));
    uint32 regVal  = mChannelHwRead(self, address, cThaModuleOcn);

    /* If enabled, the calculated B3 errors will be inserted to G1[b1:b4] and
     * the generated ERDI code will be inserted to G1[b5:b7], then forwarding
     * downstream. */
    mRegFieldSet(regVal, cAf6_spiramctl_LineStsPiTerm_, (enable) ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc thaVc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(thaVc), sizeof(m_ThaSdhVcOverride));

        mMethodOverride(m_ThaSdhVcOverride, AutoRdiReiEnable);
        }

    mMethodsSet(thaVc, &m_ThaSdhVcOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideThaSdhVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SdhFacePlateLineAuVc);
    }

static AtSdhVc ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhFacePlateLineAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60290022SdhFacePlateLineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, channelId, channelType, module);
    }

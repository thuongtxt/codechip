/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290022SdhFacePlateLineAu.c
 *
 * Created Date: Aug 17, 2017
 *
 * Description : FacePlate Line AU path
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../Tha60290021/sdh/Tha6029SdhFacePlateLineAuInternal.h"
#include "../man/Tha60290022Device.h"
#include "../ocn/Tha60290022ModuleOcnReg.h"
#include "Tha60290022ModuleSdh.h"
#include "Tha60290022SdhFacePlateLineAuInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods       m_AtSdhChannelOverride;

/* Super implementation */
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModuleOcn ModuleOcn(AtSdhChannel self)
    {
    return (Tha60210011ModuleOcn)ThaModuleOcnFromChannel((AtChannel)self);
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    uint32 address, regVal;

    address = Tha60210011ModuleOcnStsPointerInterpreterPerChannelControl(ModuleOcn(self), self) +
              Tha60210011ModuleOcnStsDefaultOffset(self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spiramctl_LineStsPiAisCcDetMod_, ThaSdhHwChannelMode(mode));
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return m_AtSdhChannelMethods->ModeSet(self, mode);
    }

static eAtSdhChannelMode ModeGet(AtSdhChannel self)
    {
    uint32 address, regVal;
    address = Tha60210011ModuleOcnStsPointerInterpreterPerChannelControl(ModuleOcn(self), self) +
              Tha60210011ModuleOcnStsDefaultOffset(self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    return ThaSdhSwChannelMode((uint8)mRegField(regVal, cAf6_spiramctl_LineStsPiAisCcDetMod_));
    }

static void SerializeMode(AtSdhChannel object, AtCoder encoder)
    {
    /* Just does nothing as this is retrieved directly from HW */
    AtUnused(object);
    AtUnused(encoder);
    }

static void OverrideAtSdhChannel(AtSdhAu self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        mMethodOverride(m_AtSdhChannelOverride, ModeGet);
        mMethodOverride(m_AtSdhChannelOverride, SerializeMode);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhAu self)
    {
    OverrideAtSdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SdhFacePlateLineAu);
    }

AtSdhAu Tha60290022SdhFacePlateLineAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhFacePlateLineAuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAu Tha60290022SdhFacePlateLineAuNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAu self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022SdhFacePlateLineAuObjectInit(self, channelId, channelType, module);
    }

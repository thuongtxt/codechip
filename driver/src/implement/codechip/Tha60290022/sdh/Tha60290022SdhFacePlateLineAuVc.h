/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290022SdhFacePlateLineAuVc.h
 * 
 * Created Date: Sep 25, 2017
 *
 * Description : PWCodechip-60290022 AU VC FacePlate Line.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022SDHFACEPLATELINEAUVC_H_
#define _THA60290022SDHFACEPLATELINEAUVC_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290022SdhFacePlateLineAuVcAutoReiRdiOverridingUpdate(AtSdhChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022SDHFACEPLATELINEAUVC_H_ */


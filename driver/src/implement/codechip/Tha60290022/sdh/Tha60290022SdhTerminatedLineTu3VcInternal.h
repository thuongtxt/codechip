/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290022SdhTerminatedLineTu3VcInternal.h
 * 
 * Created Date: Sep 18, 2019
 *
 * Description : 60290022 Terminated TU3 VC representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022SDHTERMINATEDLINETU3VCINTERNAL_H_
#define _THA60290022SDHTERMINATEDLINETU3VCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/sdh/Tha6029SdhTerminatedLineTu3VcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022SdhTerminatedLineTu3Vc
    {
    tTha6029SdhTerminatedLineTu3Vc super;
    }tTha60290022SdhTerminatedLineTu3Vc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha60290022SdhTerminatedLineTu3VcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022SDHTERMINATEDLINETU3VCINTERNAL_H_ */


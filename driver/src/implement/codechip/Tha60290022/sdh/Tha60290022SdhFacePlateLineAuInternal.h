/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290022SdhFacePlateLineAuInternal.h
 * 
 * Created Date: Aug 22, 2018
 *
 * Description : Faceplate line AU
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022SDHFACEPLATELINEAUINTERNAL_H_
#define _THA60290022SDHFACEPLATELINEAUINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/sdh/Tha6029SdhFacePlateLineAuInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022SdhFacePlateLineAu
    {
    tTha6029SdhFacePlateLineAu super;
    }tTha60290022SdhFacePlateLineAu;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAu Tha60290022SdhFacePlateLineAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022SDHFACEPLATELINEAUINTERNAL_H_ */


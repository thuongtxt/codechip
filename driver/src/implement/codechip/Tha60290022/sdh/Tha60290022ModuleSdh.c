/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290022ModuleSdh.c
 *
 * Created Date: Apr 16, 2017
 *
 * Description : SDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhPathInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "../../Tha60290021/ocn/Tha60290021ModuleOcn.h"
#include "../../Tha60290021/encap/Tha60290021HdlcChannel.h"
#include "../encap/Tha60290022HdlcChannelInternal.h"
#include "../poh/Tha60290022ModulePohReg.h"
#include "../man/Tha60290022Device.h"
#include "../poh/Tha60290022ModulePoh.h"
#include "../ocn/Tha60290022ModuleOcn.h"
#include "Tha60290022ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cIteratorMask(idx)      (cBit0 << idx)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods          m_AtModuleSdhOverride;
static tThaModuleSdhMethods         m_ThaModuleSdhOverride;
static tTha60210011ModuleSdhMethods m_Tha60210011ModuleSdhOverride;
static tTha60290021ModuleSdhMethods m_Tha60290021ModuleSdhOverride;

/* Save super implementation */
static const tAtModuleSdhMethods          *m_AtModuleSdhMethods          = NULL;
static const tThaModuleSdhMethods         *m_ThaModuleSdhMethods = NULL;
static const tTha60290021ModuleSdhMethods *m_Tha60290021ModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Capacity20G(Tha60290021ModuleSdh self)
    {
    return Tha60290022DeviceShouldOpenFullCapacity(AtModuleDeviceGet((AtModule)self));
    }

static eBool Capacity15G(Tha60290021ModuleSdh self)
    {
    return Tha60290022DeviceCapacity15G(AtModuleDeviceGet((AtModule)self));
    }

static uint8 NumUseableFaceplateLines(Tha60290021ModuleSdh self)
    {
    if (Capacity20G(self))
        return Tha60290021ModuleSdhNumFaceplateLines((AtModuleSdh)self);
    return m_Tha60290021ModuleSdhMethods->NumUseableFaceplateLines(self);
    }

static uint8 NumUseableMateLines(Tha60290021ModuleSdh self)
    {
    if (Capacity20G(self))
        return Tha60290021ModuleSdhNumMateLines((AtModuleSdh)self);
    return m_Tha60290021ModuleSdhMethods->NumUseableMateLines(self);
    }

static uint8 NumUseableTerminatedLines(Tha60290021ModuleSdh self)
    {
    if (Capacity15G(self))
        return 6;

    if (Capacity20G(self))
        return Tha60290021ModuleSdhNumTerminatedLines((AtModuleSdh)self);

    return m_Tha60290021ModuleSdhMethods->NumUseableTerminatedLines(self);
    }

static eBool UseNewStsIdConvert(Tha60290021ModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet PohMonitorDefaultSet(Tha60210011ModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return Tha60290022ModulePohMonitoringDefaultSet((ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh));
    }

static ThaModulePoh ModulePoh(ThaModuleSdh self)
    {
    return (ThaModulePoh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    }

static uint32 StsInterruptStatusGet(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 regAddr = ThaModulePohBaseAddress(modulePoh) + cAf6Reg_alm_glbchgo_Base;
    uint32 regVal = AtHalRead(hal, regAddr);
    AtUnused(glbIntr);
    return mRegField(regVal, cAf6_alm_glbchgo_glbstachghi_);
    }

static uint32 VtInterruptStatusGet(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 regAddr = ThaModulePohBaseAddress(modulePoh) + cAf6Reg_alm_glbchgo_Base;
    uint32 regVal = AtHalRead(hal, regAddr);
    AtUnused(glbIntr);
    return mRegField(regVal, cAf6_alm_glbchgo_glbstachglo_);
    }

static uint32 StsSliceOffset(uint8 slice)
    {
    return (slice * 256UL);
    }

static uint32 VtSliceOffset(uint8 slice)
    {
    return (slice * 8192UL);
    }

static uint32 HalfSliceOffset(uint8 halfSlice)
    {
    return (halfSlice * 2U);
    }

static void StsInterruptProcess(ThaModuleSdh self, uint32 pohIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 numSlice = Tha60210011ModulePohNumberStsSlices((Tha60210011ModulePoh)modulePoh);
    uint8 slice, sts1, halfSlice;

    for (slice = 0; slice < numSlice; slice++)
        {
        if (pohIntr & cIteratorMask(slice))
            {
            uint32 sliceOffset = baseAddress + StsSliceOffset(slice);
            uint8 sts48 = 0;

            for (halfSlice = 0; halfSlice < 2; halfSlice++)
                {
                /* Get STS24 OR interrupt status */
                uint32 halfSliceOffset = sliceOffset + HalfSliceOffset(halfSlice);
                uint32 intrSts24Val = AtHalRead(hal, cAf6Reg_alm_glbchghi_Base + halfSliceOffset);
                uint32 intrSts24Mask = AtHalRead(hal, cAf6Reg_alm_glbmskhi_Base + halfSliceOffset);

                intrSts24Val &= intrSts24Mask;
                for (sts1 = 0; sts1 < 24; sts1++, sts48++)
                    {
                    if (intrSts24Val & cIteratorMask(sts1))
                        {
                        AtSdhPath auPath;

                        /* Get AU path from hardware ID */
                        auPath = ThaModuleSdhAuPathFromHwIdGet((ThaModuleSdh)self, cAtModuleSdh, slice, sts48);

                        /* Execute path interrupt process */
                        AtSdhPathInterruptProcess(auPath, slice, sts48, 0, hal);
                        }
                    }
                }
            }
        }
    }

static void VtInterruptProcess(ThaModuleSdh self, uint32 pohIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 numSlice = Tha60210011ModulePohNumberVtSlices((Tha60210011ModulePoh)modulePoh);
    uint8 slice, sts1, halfSlice;

    for (slice = 0; slice < numSlice; slice++)
        {
        if (pohIntr & cIteratorMask(slice))
            {
            uint32 sliceOffset = baseAddress + VtSliceOffset(slice);
            uint8 sts48 = 0;

            for (halfSlice = 0; halfSlice < 2; halfSlice++)
                {
                /* Get STS24 OR interrupt status */
                uint32 halfSliceOffset = sliceOffset + HalfSliceOffset(halfSlice);
                uint32 intrSts24Val = AtHalRead(hal, cAf6Reg_alm_glbchglo_Base + halfSliceOffset);
                uint32 intrSts24Mask = AtHalRead(hal, cAf6Reg_alm_glbmsklo_Base + halfSliceOffset);

                intrSts24Val &= intrSts24Mask;
                for (sts1 = 0; sts1 < 24; sts1++, sts48++)
                    {
                    if (intrSts24Val & cIteratorMask(sts1))
                        {
                        uint8 vtg, vt, vt28 = 0;
                        uint32 intrVt28Val = AtHalRead(hal, sliceOffset + cAf6Reg_alm_orstalo_Base + sts48);

                        for (vtg = 0, vt28 = 0; vtg < 7; vtg++)
                            {
                            for (vt = 0; vt < 4; vt++, vt28++)
                                {
                                if (intrVt28Val & cIteratorMask(vt28))
                                    {
                                    AtSdhPath tuPath;

                                    /* Get TU path from hardware ID */
                                    tuPath = ThaModuleSdhTuPathFromHwIdGet((ThaModuleSdh)self, cAtModuleSdh, slice, sts48, vtg, vt);

                                    /* Execute path interrupt process */
                                    AtSdhPathInterruptProcess(tuPath, slice, sts48, vt28, hal);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

static eAtRet StsInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 numSlice = Tha60210011ModulePohNumberStsSlices((Tha60210011ModulePoh)modulePoh);
    uint32 regAddr, regVal;
    uint8 slice, halfSlice;

    regAddr = baseAddress + cAf6Reg_alm_glbmskhislice_Base;
    regVal = mModuleHwRead(self, regAddr);
    regVal = (enable) ? (regVal | cBit7_0) : (regVal & ~cBit7_0);
    mModuleHwWrite(self, regAddr, regVal);

    regVal = (enable) ? cAf6_alm_glbmskhi_glbmsk_Mask : 0x0;
    for (slice = 0; slice < numSlice; slice++)
        {
        uint32 sliceOffset = baseAddress + StsSliceOffset(slice);
        for (halfSlice = 0; halfSlice < 2; halfSlice++)
            {
            regAddr = sliceOffset + cAf6Reg_alm_glbmskhi_Base + HalfSliceOffset(halfSlice);
            mModuleHwWrite(self, regAddr, regVal);
            }
        }

    return cAtOk;
    }

static eAtRet VtInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 numSlice = Tha60210011ModulePohNumberVtSlices((Tha60210011ModulePoh)modulePoh);
    uint32 regAddr, regVal;
    uint8 slice, halfSlice;

    regAddr = baseAddress + cAf6Reg_alm_glbmsk1oslice_Base;
    regVal = mModuleHwRead(self, regAddr);
    regVal = (enable) ? (regVal | cAf6_alm_glbmsk1oslice_glbmsklo_Mask) : (regVal & ~cAf6_alm_glbmsk1oslice_glbmsklo_Mask);
    mModuleHwWrite(self, regAddr, regVal);

    regVal = (enable) ? cAf6_alm_glbmsklo_glbmsk_Mask : 0x0;
    for (slice = 0; slice < numSlice; slice++)
        {
        uint32 sliceOffset = baseAddress + VtSliceOffset(slice);
        for (halfSlice = 0; halfSlice < 2; halfSlice++)
            {
            regAddr = sliceOffset + cAf6Reg_alm_glbmsklo_Base + HalfSliceOffset(halfSlice);
            mModuleHwWrite(self, regAddr, regVal);
            }
        }

    return cAtOk;
    }

static AtSdhChannel Vc1xObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60290022SdhTerminatedLineVc1xNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel Tu3VcObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60290022SdhTerminatedLineTu3VcNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhLine FaceplateLineCreate(Tha60290021ModuleSdh self, uint8 lineId)
    {
    return Tha60290022SdhFacePlateLineNew(lineId, (AtModuleSdh)self);
    }

static AtSdhLine TerminatedLineCreate(Tha60290021ModuleSdh self, uint8 lineId)
    {
    return Tha60290022SdhTerminatedLineNew(lineId, (AtModuleSdh)self);
    }

static AtSdhLine MateLineCreate(Tha60290021ModuleSdh self, uint8 lineId)
    {
    return Tha60290022SdhMateLineNew(lineId, (AtModuleSdh)self);
    }

static AtSdhPath AuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 hwStsId)
    {
    if (phyModule == cAtModuleSur)
        ThaModuleSdhHwSts24ToHwSts48Get(self, sliceId, hwStsId, &sliceId, &hwStsId);

    return Tha60290021ModuleSdhLineSideAuPathFromHwIdGet(self, sliceId, hwStsId);
    }

static AtSdhPath TuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    AtSdhLine terminatedLine;
    AtSdhChannel au;

    if (phyModule == cAtModuleSur)
        ThaModuleSdhHwSts24ToHwSts48Get(self, sliceId, stsId, &sliceId, &stsId);

    terminatedLine = Tha60290021ModuleSdhTerminatedLineGet((AtModuleSdh)self, sliceId);
    au = Tha60210011ModuleSdhAuFromHwStsGet(terminatedLine, stsId);

    return (AtSdhPath)Tha60210011SdhTuPathFromAuPathHwStsGet(au, stsId, vtgId, vtId);
    }

static AtSdhAu MateLineAuCreate(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType)
    {
    return Tha60290022SdhMateLineAuNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhAu FacePlateLineAuCreate(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType)
    {
    return Tha60290022SdhFacePlateLineAuNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhVc FacePlateLineAuVcCreate(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType)
    {
    return Tha60290022SdhFacePlateLineAuVcNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhVc TerminatedLineAuVcCreate(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType)
    {
    return Tha60290022SdhTerminatedLineAuVcNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhAug FacePlateLineAugCreate(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType)
    {
    return Tha60290022SdhFacePlateAugNew(channelId, channelType, (AtModuleSdh)self);
    }

static uint8 TerminatedLineIdFirstStm64(AtModuleSdh self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 TerminatedLineIdSecondStm64(AtModuleSdh self)
    {
    AtUnused(self);
    return 4;
    }

static eBool IsTerminatedLineSupportStm64(AtModuleSdh self, uint8 lineId)
    {
    AtUnused(self);
    return ((lineId == TerminatedLineIdFirstStm64(self)) || (lineId == TerminatedLineIdSecondStm64(self)));
    }

static Tha60290022ModuleOcn ModuleOcn(AtModuleSdh self)
    {
    return (Tha60290022ModuleOcn)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleOcn);
    }

static eBool IsTerminatedIdsFirstPartUnused(AtModuleSdh self, uint8 lineId)
    {
    if (lineId >= TerminatedLineIdSecondStm64(self))
        return cAtFalse;

    /* Configure line rate of line 25 to OC–192/STM–64 will make line 26 to 28 be unused */
    if (Tha60290022ModuleOcnTerminatedLineRateGet(ModuleOcn(self), TerminatedLineIdFirstStm64(self)) == cAtSdhLineRateStm64)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsTerminatedIdsSecondPartUnused(AtModuleSdh self, uint8 lineId)
    {
    if (lineId <= TerminatedLineIdSecondStm64(self))
        return cAtFalse;

    /* Configure line rate of line 29 to OC–192/STM–64 will make line 30 to 31 be unused */
    if (Tha60290022ModuleOcnTerminatedLineRateGet(ModuleOcn(self), TerminatedLineIdSecondStm64(self)) == cAtSdhLineRateStm64)
        return cAtTrue;

    return cAtFalse;
    }

static eBool TerminatedLineIdUse(AtModuleSdh self, uint8 lineId)
    {
    if (IsTerminatedLineSupportStm64(self, lineId))
        return cAtTrue;

    if (IsTerminatedIdsFirstPartUnused(self, lineId))
        return cAtFalse;

    if (IsTerminatedIdsSecondPartUnused(self, lineId))
        return cAtFalse;

    return cAtTrue;
    }

static AtSdhLine TerminatedLineGet(AtModuleSdh self, uint8 lineId)
    {
    uint8 localLineId = ThaModuleSdhLineIdLocalId((ThaModuleSdh)self, lineId);
    if (!TerminatedLineIdUse(self, localLineId))
        return NULL;

    return m_AtModuleSdhMethods->LineGet(self, lineId);
    }

static AtSdhLine LineGet (AtModuleSdh self, uint8 lineId)
    {
    if (Tha60290021ModuleSdhLineIsTerminated(self, lineId))
        return TerminatedLineGet(self, lineId);

    return m_AtModuleSdhMethods->LineGet(self, lineId);
    }

static eBool ChannelConcateIsSupported(AtModuleSdh self, uint8 channelType, uint8 numChannels)
    {
    static const uint8 cNumSts48cInConcate = 4;

    if (channelType == cAtSdhChannelTypeAu4)
        return m_AtModuleSdhMethods->ChannelConcateIsSupported(self, channelType, numChannels);

    /* NxSTS-48c concatenation is supported from the version of STS192c */
    if (!Tha60290021ModuleOcnSts192cIsSupported((Tha60290021ModuleOcn)ModuleOcn(self)))
        return cAtFalse;

    if (channelType != cAtSdhChannelTypeAu4_16c)
        return cAtFalse;

    if (numChannels != cNumSts48cInConcate)
        return cAtFalse;

    return cAtTrue;
    }

static AtHdlcChannel DccChannelObjectCreate(Tha60290021ModuleSdh module, uint32 dccId,  AtSdhLine line, eAtSdhLineDccLayer layers)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)module);
    if (Tha60290022DeviceOcnSeparatedSectionAndLineDccIsSupported(device))
        return Tha60290022HdlcChannelNew(dccId, line, layers, (AtModuleSdh)module);

    if (Tha60290022DeviceDccHdlcTransmissionBitOrderPerChannelIsSupported(device))
        return Tha60290021HdlcChannelV4New(dccId, line, layers, (AtModuleSdh)module);

    if (Tha60290022DeviceDccTxFcsMsbIsSupported(device))
        return Tha60290021HdlcChannelV3New(dccId, line, layers, (AtModuleSdh)module);

    if (Tha60290022DeviceDccV2IsSupported(device))
        return Tha60290021HdlcChannelV2New(dccId, line, layers, (AtModuleSdh)module);

    return m_Tha60290021ModuleSdhMethods->DccChannelObjectCreate(module, dccId, line, layers);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, LineGet);
        mMethodOverride(m_AtModuleSdhOverride, ChannelConcateIsSupported);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void OverrideTha60210011ModuleSdh(AtModuleSdh self)
    {
    Tha60210011ModuleSdh module = (Tha60210011ModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleSdhOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleSdhOverride));

        mMethodOverride(m_Tha60210011ModuleSdhOverride, PohMonitorDefaultSet);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, Tu3VcObjectCreate);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, Vc1xObjectCreate);
        }

    mMethodsSet(module, &m_Tha60210011ModuleSdhOverride);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh moduleSdh = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(moduleSdh);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, mMethodsGet(moduleSdh), sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, StsInterruptStatusGet);
        mMethodOverride(m_ThaModuleSdhOverride, VtInterruptStatusGet);
        mMethodOverride(m_ThaModuleSdhOverride, StsInterruptProcess);
        mMethodOverride(m_ThaModuleSdhOverride, VtInterruptProcess);
        mMethodOverride(m_ThaModuleSdhOverride, StsInterruptEnable);
        mMethodOverride(m_ThaModuleSdhOverride, VtInterruptEnable);
        mMethodOverride(m_ThaModuleSdhOverride, AuPathFromHwIdGet);
        mMethodOverride(m_ThaModuleSdhOverride, TuPathFromHwIdGet);
        }

    mMethodsSet(moduleSdh, &m_ThaModuleSdhOverride);
    }

static void OverrideTha60290021ModuleSdh(AtModuleSdh self)
    {
    Tha60290021ModuleSdh sdh = (Tha60290021ModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021ModuleSdhMethods = mMethodsGet(sdh);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModuleSdhOverride, mMethodsGet(sdh), sizeof(m_Tha60290021ModuleSdhOverride));

        mMethodOverride(m_Tha60290021ModuleSdhOverride, UseNewStsIdConvert);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, NumUseableFaceplateLines);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, NumUseableMateLines);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, NumUseableTerminatedLines);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, FaceplateLineCreate);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, TerminatedLineCreate);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, MateLineCreate);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, MateLineAuCreate);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, FacePlateLineAuCreate);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, FacePlateLineAuVcCreate);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, TerminatedLineAuVcCreate);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, FacePlateLineAugCreate);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, DccChannelObjectCreate);
        }

    mMethodsSet(sdh, &m_Tha60290021ModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    OverrideTha60210011ModuleSdh(self);
    OverrideTha60290021ModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleSdh);
    }

AtModuleSdh Tha60290022ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60290022ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModuleSdhObjectInit(newModule, device);
    }

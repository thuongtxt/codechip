/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290022SdhTerminatedLine.c
 *
 * Created Date: Dec 5, 2017
 *
 * Description : PWCodechip-60290022 SDH Terminated line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/ocn/Tha60290021ModuleOcn.h"
#include "../../Tha60290021/sdh/Tha6029SdhLineInternal.h"
#include "../ocn/Tha60290022ModuleOcn.h"
#include "Tha60290022ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022SdhTerminatedLine
    {
    tTha6029SdhTerminatedLine super;
    }tTha60290022SdhTerminatedLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhLineMethods m_AtSdhLineOverride;

/* Save super implementation */
static const tAtSdhLineMethods *m_AtSdhLineMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290021ModuleOcn ModuleOcn(AtSdhLine self)
    {
    return (Tha60290021ModuleOcn)ThaModuleOcnFromChannel((AtChannel)self);
    }

static ThaModuleSdh ModuleSdh(AtSdhLine self)
    {
    return (ThaModuleSdh)AtChannelModuleGet((AtChannel)self);
    }

static eBool ShouldResetMapping(AtSdhLine self)
    {
    return (AtSdhLineRateGet(self) == cAtSdhLineRateStm64) ? cAtFalse : cAtTrue;
    }

static eAtModuleSdhRet RateSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    eAtModuleSdhRet ret;
    Tha60290021ModuleOcn moduleOcn = ModuleOcn(self);
    uint32 localId = ThaModuleSdhLineIdLocalId(ModuleSdh(self), (uint8)AtChannelIdGet((AtChannel)self));

    if (AtChannelServiceIsRunning((AtChannel)self))
        return cAtErrorChannelBusy;

    /* Reset mapping to bring concatenation to default state */
    if (ShouldResetMapping(self))
        {
        ret = AtSdhChannelMappingReset((AtSdhChannel)self);
        if (ret != cAtOk)
            return ret;
        }

    if (Tha60290021ModuleOcnSts192cIsSupported(moduleOcn))
        {
        ret = Tha60290022ModuleOcnTerminatedLineRateSet((Tha60290022ModuleOcn)moduleOcn, localId, rate);
        if (ret != cAtOk)
            return ret;
        }

    /* Update database */
    ret  = AtSdhLineSoftwareRateSet(self, rate);
    if (rate == cAtSdhLineRateStm64)
        {
        static const uint8 defaultAug64Mapping = cAtSdhAugMapTypeAug64MapVc4_64c;
        AtSdhChannel aug64 = AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
        if (aug64)
            ret |= mMethodsGet(aug64)->MapTypeSet(aug64, defaultAug64Mapping);
        }

    return ret;
    }

static eBool RateIsSupported(AtSdhLine self, eAtSdhLineRate rate)
    {
    if (rate == cAtSdhLineRateStm64)
        return Tha60290021ModuleOcnSts192cIsSupported(ModuleOcn(self)) ? cAtTrue : cAtFalse;

    return m_AtSdhLineMethods->RateIsSupported(self, rate);
    }

static eAtSdhLineRate RateGet(AtSdhLine self)
    {
    Tha60290021ModuleOcn moduleOcn = ModuleOcn(self);
    uint32 localId = ThaModuleSdhLineIdLocalId(ModuleSdh(self), (uint8)AtChannelIdGet((AtChannel)self));

    if (!Tha60290021ModuleOcnSts192cIsSupported(moduleOcn))
        return m_AtSdhLineMethods->RateGet(self);

    return Tha60290022ModuleOcnTerminatedLineRateGet((Tha60290022ModuleOcn)moduleOcn, localId);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, mMethodsGet(self), sizeof(m_AtSdhLineOverride));

        mMethodOverride(m_AtSdhLineOverride, RateSet);
        mMethodOverride(m_AtSdhLineOverride, RateGet);
        mMethodOverride(m_AtSdhLineOverride, RateIsSupported);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtSdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SdhTerminatedLine);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhTerminatedLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60290022SdhTerminatedLineNew(uint32 lineId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newLine, lineId, module);
    }

eAtRet Tha60290022SdhTerminatedLineRateSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    if (self)
        return RateSet(self, rate);
    return cAtErrorNullPointer;
    }

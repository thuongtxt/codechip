/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290022SdhMateLineAu.c
 *
 * Created Date: Aug 17, 2017
 *
 * Description : Mate line AU path
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../Tha60290021/sdh/Tha6029SdhAuInternal.h"
#include "../man/Tha60290022Device.h"
#include "../ocn/Tha60290022ModuleOcn.h"
#include "../ocn/Tha60290022ModuleOcnReg.h"
#include "Tha60290022ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022SdhMateLineAu
    {
    tTha6029SdhMateLineAu super;
    }tTha60290022SdhMateLineAu;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods             m_AtSdhChannelOverride;
static tTha60210011Tfi5LineAuMethods    m_Tha60210011Tfi5LineOverride;

/* Super implementation */
static const tAtSdhChannelMethods           *m_AtSdhChannelMethods = NULL;
static const tTha60210011Tfi5LineAuMethods  *m_Tha60210011Tfi5LineAuMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModuleOcn ModuleOcn(AtSdhChannel self)
    {
    return (Tha60210011ModuleOcn)ThaModuleOcnFromChannel((AtChannel)self);
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    uint32 address, regVal;

    address = Tha60210011ModuleOcnStsPointerInterpreterPerChannelControl(ModuleOcn(self), self) +
              Tha60210011ModuleOcnStsDefaultOffset(self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_spiramctl_LineStsPiAisCcDetMod_, ThaSdhHwChannelMode(mode));
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return m_AtSdhChannelMethods->ModeSet(self, mode);
    }

static AtModuleSdh ModuleSdh(AtSdhChannel self)
    {
    return (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    }

static eBool CanBeMaster(AtSdhChannel self)
    {
    uint8 lineId = AtSdhChannelLineGet(self);
    uint8 startMateLineId = Tha60290021ModuleSdhMateLineStartId(ModuleSdh(self));

    return ((lineId == startMateLineId) || (lineId == (uint8)(startMateLineId + 4))) ? cAtTrue : cAtFalse;
    }

static eBool CanConcate(Tha60210011Tfi5LineAu self, AtSdhChannel *slaveList, uint8 numSlaves)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    uint8 slave_i;

    if (AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeAu4)
        return m_Tha60210011Tfi5LineAuMethods->CanConcate(self, slaveList, numSlaves);

    if (!CanBeMaster(channel))
        return cAtFalse;

    for (slave_i = 0; slave_i < numSlaves; slave_i++)
        {
        uint8 expectedLineId = (uint8)(AtSdhChannelLineGet(channel) + slave_i + 1);

        if (expectedLineId != AtSdhChannelLineGet(slaveList[slave_i]))
            return cAtFalse;
        }

    return cAtTrue;
    }

static eAtRet Concate(AtSdhChannel self, AtSdhChannel *slaveList, uint8 numSlaves)
    {
    eAtRet ret = m_AtSdhChannelMethods->Concate(self, slaveList, numSlaves);
    if (ret != cAtOk)
        return ret;

    if (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeAu4_16nc)
        return Tha60290022ModuleOcnMateSts192cEnable((Tha60290022ModuleOcn)ModuleOcn(self), self, cAtTrue);

    return cAtOk;
    }

static eAtRet Deconcate(AtSdhChannel self)
    {
    eAtRet ret = m_AtSdhChannelMethods->Deconcate(self);
    if (ret != cAtOk)
        return ret;

    if (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeAu4_16c)
        return Tha60290022ModuleOcnMateSts192cEnable((Tha60290022ModuleOcn)ModuleOcn(self), self, cAtFalse);

    return cAtOk;
    }

static eAtSdhChannelMode ModeGet(AtSdhChannel self)
    {
    uint32 address, regVal;
    address = Tha60210011ModuleOcnStsPointerInterpreterPerChannelControl(ModuleOcn(self), self) +
              Tha60210011ModuleOcnStsDefaultOffset(self);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    return ThaSdhSwChannelMode((uint8)mRegField(regVal, cAf6_spiramctl_LineStsPiAisCcDetMod_));
    }

static void SerializeMode(AtSdhChannel object, AtCoder encoder)
    {
    /* Just does nothing as this is retrieved directly from HW */
    AtUnused(object);
    AtUnused(encoder);
    }

static void OverrideAtSdhChannel(AtSdhAu self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        mMethodOverride(m_AtSdhChannelOverride, Concate);
        mMethodOverride(m_AtSdhChannelOverride, Deconcate);
        mMethodOverride(m_AtSdhChannelOverride, ModeGet);
        mMethodOverride(m_AtSdhChannelOverride, SerializeMode);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideTha60210011Tfi5LineAu(AtSdhAu self)
    {
    Tha60210011Tfi5LineAu tfi5LineAu = (Tha60210011Tfi5LineAu)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011Tfi5LineAuMethods = mMethodsGet(tfi5LineAu);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011Tfi5LineOverride, m_Tha60210011Tfi5LineAuMethods, sizeof(m_Tha60210011Tfi5LineOverride));

        mMethodOverride(m_Tha60210011Tfi5LineOverride, CanConcate);
        }

    mMethodsSet(tfi5LineAu, &m_Tha60210011Tfi5LineOverride);
    }

static void Override(AtSdhAu self)
    {
    OverrideAtSdhChannel(self);
    OverrideTha60210011Tfi5LineAu(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SdhMateLineAu);
    }

static AtSdhAu ObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhMateLineAuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAu Tha60290022SdhMateLineAuNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAu self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, channelId, channelType, module);
    }

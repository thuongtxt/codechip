/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290022ModuleSdhInternal.h
 * 
 * Created Date: Feb 6, 2018
 *
 * Description : SDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULESDHINTERNAL_H_
#define _THA60290022MODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/sdh/Tha60290021ModuleSdhInternal.h"
#include "Tha60290022ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModuleSdh
    {
    tTha60290021ModuleSdh super;
    }tTha60290022ModuleSdh;

typedef struct tTha60290022ModuleSdhV2
    {
    tTha60290022ModuleSdh super;
    }tTha60290022ModuleSdhV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh Tha60290022ModuleSdhObjectInit(AtModuleSdh self, AtDevice device);
AtModuleSdh Tha60290022ModuleSdhV2ObjectInit(AtModuleSdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULESDHINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290022SdhTerminatedLineAuVc.c
 *
 * Created Date: Sep 25, 2017
 *
 * Description : PWCodechip-60290022 AU VC Terminated line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/xc/AtCrossConnectInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../ocn/Tha60290022ModuleOcnReg.h"
#include "../ocn/Tha60290022ModuleOcn.h"
#include "../man/Tha60290022Device.h"
#include "Tha60290022ModuleSdh.h"
#include "Tha60290022SdhFacePlateLineAuVc.h"
#include "../man/Tha60290022Device.h"
#include "../pmc/Tha60290022ModulePmc.h"
#include "Tha60290022SdhTerminatedLineAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhVcMethods                          m_AtSdhVcOverride;
static tThaSdhVcMethods                         m_ThaSdhVcOverride;
static tAtChannelMethods                        m_AtChannelOverride;
static tAtSdhPathMethods                        m_AtSdhPathOverride;

/* Save super implementation */
static const tAtChannelMethods                    *m_AtChannelMethods    = NULL;
static const tAtSdhVcMethods                      *m_AtSdhVcMethods      = NULL;
static const tAtSdhPathMethods                    *m_AtSdhPathMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasOcnStuffing(ThaSdhVc self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet CanChangeStuffing(AtSdhVc self, eBool enable)
    {
    uint16 mapType;

    if (AtSdhChannelTypeGet((AtSdhChannel)self) != cAtSdhChannelTypeVc3)
        return (enable) ? cAtOk : cAtErrorModeNotSupport;

    mapType = AtSdhChannelMapTypeGet((AtSdhChannel)self);
    if ((mapType == cAtSdhVcMapTypeVc3Map7xTug2s) ||
        (mapType == cAtSdhVcMapTypeVc3MapDe3))
        return (enable) ? cAtOk : cAtErrorModeNotSupport;

    return m_AtSdhVcMethods->CanChangeStuffing(self, enable);
    }

static eAtRet StuffEnable(AtSdhVc self, eBool enable)
    {
    eAtRet ret;
    AtSdhChannel vc = (AtSdhChannel)self;

    ret = m_AtSdhVcMethods->StuffEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    return Tha6029SdhTerminatedLineAuVcHoSelect(vc, enable);
    }

static eBool IsLookupForPwCounterApplicable(AtChannel  self)
    {
    uint16 channelType = 0;

    if (!Tha60290022DevicePmc1PageIsSupported(AtChannelDeviceGet(self)))
       return cAtFalse;

    channelType = AtSdhChannelTypeGet((AtSdhChannel)self);
    if ((channelType == cAtSdhChannelTypeVc4_4c) ||
        (channelType == cAtSdhChannelTypeVc4_16c) ||
        (channelType == cAtSdhChannelTypeVc4_64c))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet EnableLookupHiPwCounter(AtChannel self, AtPw pseudowire, eBool lookupEn)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModulePmc modulePmc = (ThaModulePmc)AtDeviceModuleGet(device, cThaModulePmc);
    return Tha60290022ModulePmcV2HwHiPwIdLookupSet(modulePmc, pseudowire, (AtSdhChannel)self, lookupEn);
    }

static eAtRet BindToHiPseudowire(AtChannel self, AtPw pseudowire)
    {
    eAtRet ret = cAtOk;

    if (pseudowire)
        {
        ret = m_AtChannelMethods->BindToPseudowire(self, pseudowire);
        if (ret != cAtOk)
            return ret;
        ret = EnableLookupHiPwCounter(self, pseudowire, cAtTrue);
        }
    else
        {
        AtPw boundedPw = AtChannelBoundPwGet(self);
        ret = EnableLookupHiPwCounter(self, boundedPw, cAtFalse);
        if (ret != cAtOk)
            return ret;
        ret = m_AtChannelMethods->BindToPseudowire(self, pseudowire);
        }

    return ret;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    if (IsLookupForPwCounterApplicable(self))
        return BindToHiPseudowire(self, pseudowire);

    return m_AtChannelMethods->BindToPseudowire(self, pseudowire);
    }

static uint32 HoLoSelectionMask(uint8 hwSts)
    {
    return  cBit0 << (hwSts % 32);
    }

static uint32 HoLoSelectionShift(uint8 hwSts)
    {
    return hwSts % 32;
    }

static uint32 GlbTxHoG1_OverWriteRegister(AtSdhChannel self, uint8 sts1Id)
    {
    ThaModuleOcn ocnModule;
    uint32 baseAddress;
    uint32 localAddr;
    uint8 hwSlice, hwSts;
    eAtRet ret;
    AtDevice device = AtChannelDeviceGet((AtChannel)self);

    ret = ThaSdhChannelHwStsGet(self, cThaModuleOcn, sts1Id, &hwSlice, &hwSts);
    if (ret != cAtOk)
        return cInvalidUint32;

    ocnModule = (ThaModuleOcn) AtDeviceModuleGet(device, cThaModuleOcn);
    baseAddress = ThaModuleOcnBaseAddress(ocnModule);
    localAddr = (hwSts < 32) ? cAf6Reg_glbtxhog1ovw1_reg_Base : cAf6Reg_glbtxhog1ovw2_reg_Base;
    return localAddr + baseAddress + hwSlice;
    }

static eBool ShouldTxG1OverrideNotUsedForSts192c(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(self);

    if ((channelType == cAtSdhChannelTypeVc4_64c) && Tha60290022DeviceSts192cTxG1OverrideIsNotUsed(device))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet HwStsTxG1OverrideEnable(AtSdhChannel self, uint8 swSts, eBool enable)
    {
    uint32 regAddr = GlbTxHoG1_OverWriteRegister(self, swSts);
    uint8 hwSlice, hwSts;
    uint32 regVal;
    eAtRet ret = ThaSdhChannelHwStsGet(self, cThaModuleOcn, swSts, &hwSlice, &hwSts);

    if ((ret != cAtOk) || (regAddr == cInvalidUint32))
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "Can not convert to hardware Id \r\n");
        return cAtError;
        }

    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, HoLoSelectionMask(hwSts), HoLoSelectionShift(hwSts), enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet HwTxG1SlaveSts1OverrideReset(AtSdhChannel self)
    {
    uint8 numSts = AtSdhChannelNumSts(self);
    uint8 startSts = AtSdhChannelSts1Get(self);
    uint8 sts_i;
    uint8 sts;
    eAtRet ret = cAtOk;

    for (sts_i = 1; sts_i < numSts; sts_i++)
        {
        sts = (uint8)(startSts + sts_i);
        ret |= HwStsTxG1OverrideEnable(self, sts, cAtFalse);
        }

    return ret;
    }

static eAtRet AutoReiRdiEnable(AtSdhChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    uint8 masterSts1 = AtSdhChannelSts1Get(self);;

    if (ShouldTxG1OverrideNotUsedForSts192c(self))
        enable = cAtFalse;

    ret |= HwTxG1SlaveSts1OverrideReset(self);
    ret |= HwStsTxG1OverrideEnable(self, masterSts1, enable);

    return ret;
    }

static eAtRet PohInsertionEnable(AtSdhPath self, eBool enabled)
    {
    eAtRet ret = cAtOk;

    ret = m_AtSdhPathMethods->PohInsertionEnable(self, enabled);
    if (ret != cAtOk)
        return ret;

    return ThaSdhVcAutoRdiReiEnable((ThaSdhVc)self, enabled);
    }

static eAtRet AutoRdiReiEnable(ThaSdhVc self, eBool enabled)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    uint32 numSourceVcs = ThaSdhVcNumSourceVcs((ThaSdhVc)self);
    uint32 sourceIndex;

    if (Tha60290022DeviceHoTxG1OverWriteIsSupported(device))
        ret |= AutoReiRdiEnable((AtSdhChannel)self, enabled);

    for (sourceIndex = 0; sourceIndex < numSourceVcs; sourceIndex++)
        {
        ThaSdhVc sourceVc = ThaSdhVcSourceVcGet((ThaSdhVc)self, sourceIndex);
        if ((sourceVc) && Tha60290021ModuleSdhIsLineSideVc((AtSdhChannel)sourceVc))
            ret |= ThaSdhVcAutoRdiReiEnable(sourceVc, enabled);
        }

    return ret;
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhPath(AtSdhVc self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhPathMethods = mMethodsGet(path);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, m_AtSdhPathMethods, sizeof(m_AtSdhPathOverride));

        mMethodOverride(m_AtSdhPathOverride, PohInsertionEnable);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc vc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(vc), sizeof(m_ThaSdhVcOverride));
        mMethodOverride(m_ThaSdhVcOverride, HasOcnStuffing);
        mMethodOverride(m_ThaSdhVcOverride, AutoRdiReiEnable);
        }

    mMethodsSet(vc, &m_ThaSdhVcOverride);
    }

static void OverrideAtSdhVc(AtSdhVc self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhVcMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhVcOverride, mMethodsGet(self), sizeof(m_AtSdhVcOverride));
        mMethodOverride(m_AtSdhVcOverride, CanChangeStuffing);
        mMethodOverride(m_AtSdhVcOverride, StuffEnable);
        }

    mMethodsSet(self, &m_AtSdhVcOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideThaSdhVc(self);
    OverrideAtSdhVc(self);
    OverrideAtChannel(self);
    OverrideAtSdhPath(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SdhTerminatedLineAuVc);
    }

AtSdhVc Tha60290022SdhTerminatedLineAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhTerminatedLineAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60290022SdhTerminatedLineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022SdhTerminatedLineAuVcObjectInit(self, channelId, channelType, module);
    }

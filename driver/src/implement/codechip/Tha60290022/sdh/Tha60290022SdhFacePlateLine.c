/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290022SdhFacePlateLine.c
 *
 * Created Date: Aug 6, 2017
 *
 * Description : Faceplate line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../ocn/Tha60290022ModuleOcnReg.h"
#include "../ocn/Tha60290022ModuleOcn.h"
#include "../man/Tha60290022Device.h"
#include "Tha60290022ModuleSdh.h"
#include "Tha60290022SdhFacePlateLineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods        m_AtChannelOverride;
static tAtSdhChannelMethods     m_AtSdhChannelOverride;
static tAtSdhLineMethods        m_AtSdhLineOverride;

/* Save super implementation */
static const tAtChannelMethods      *m_AtChannelMethods = NULL;
static const tAtSdhChannelMethods   *m_AtSdhChannelMethods = NULL;
static const tAtSdhLineMethods      *m_AtSdhLineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn OcnModule(AtChannel self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleOcn);
    }

static AtModuleSdh SdhModule(AtChannel self)
    {
    return (AtModuleSdh)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleSdh);
    }

static uint32 LoopbackAddress(AtChannel self)
    {
    return cAf6Reg_glbloop_reg_Base + ThaModuleOcnBaseAddress(OcnModule(self));
    }

static uint32 LocalId(AtChannel self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)SdhModule(self);
    return ThaModuleSdhLineIdLocalId(sdhModule, (uint8)AtChannelIdGet(self));
    }

static uint32 FramerLocalLoopbackMask(AtChannel self)
    {
    return cBit8 << LocalId(self);
    }

static uint32 FramerLocalLoopbackShift(AtChannel self)
    {
    return 8 + LocalId(self);
    }

static eAtRet FramerLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    uint32 regAddr = LoopbackAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 fieldMask = FramerLocalLoopbackMask(self);
    uint32 fieldShift = FramerLocalLoopbackShift(self);
    uint32 enabled = (loopbackMode == cAtLoopbackModeLocal) ? 1 : 0;
    mRegFieldSet(regVal, field, enabled);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static uint8 FramerLoopbackGet(AtChannel self)
    {
    uint32 regAddr = LoopbackAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    if (regVal & FramerLocalLoopbackMask(self))
        return cAtLoopbackModeLocal;
    return cAtLoopbackModeRelease;
    }

static eBool FramerLocalLoopbackSupported(AtChannel self)
    {
    return Tha60290022DeviceFaceplateFramerLocalLoopbackSupported(AtChannelDeviceGet(self));
    }

static eBool ShouldOpenFramerLoopbackAsDefault(AtChannel self)
    {
    /* Customers was first asking for local loopback and AIS will be forced toward
     * faceplace side, but now they are holding this. Keep this logic
     * deactivated */
    AtUnused(self);
    return cAtTrue; /* Open this on test branch for testing */
    }

static eBool ShouldLoopbackAtFramer(AtChannel self, uint8 loopbackMode)
    {
    if (!ShouldOpenFramerLoopbackAsDefault(self))
        return cAtFalse;

    if (!FramerLocalLoopbackSupported(self))
        return cAtFalse;

    if (loopbackMode == cAtLoopbackModeLocal)
        return cAtTrue;

    /* Local loopback had been made here, so release should be done here as well */
    if (loopbackMode == cAtLoopbackModeRelease)
        return (FramerLoopbackGet(self) == cAtLoopbackModeLocal) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    if (FramerLocalLoopbackSupported(self))
        FramerLoopbackSet(self, cAtLoopbackModeRelease);

    if (ShouldLoopbackAtFramer(self, loopbackMode))
        return FramerLoopbackSet(self, loopbackMode);

    return m_AtChannelMethods->LoopbackSet(self, loopbackMode);
    }

static uint8 LoopbackGet(AtChannel self)
    {
    /* There may be local loopback at framer, just ask it */
    if (FramerLocalLoopbackSupported(self) && (FramerLoopbackGet(self) == cAtLoopbackModeLocal))
        return cAtLoopbackModeLocal;

    return m_AtChannelMethods->LoopbackGet(self);
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return cAtSdhLineAlarmAis;
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);

    if (Tha60290022DeviceTohSonetSdhModeIsSupported(device))
        {
        eAtRet ret = Tha60290022ModuleOcnFaceplateLineModeSet((Tha60290022ModuleOcn)OcnModule((AtChannel)self), (uint8)AtChannelIdGet((AtChannel)self), mode);
        if (ret != cAtOk)
            return ret;
        }

    return m_AtSdhChannelMethods->ModeSet(self, mode);
    }

static eAtSdhChannelMode ModeGet(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);

    if (Tha60290022DeviceTohSonetSdhModeIsSupported(device))
        return Tha60290022ModuleOcnFaceplateLineModeGet((Tha60290022ModuleOcn)OcnModule((AtChannel)self), (uint8)AtChannelIdGet((AtChannel)self));

    return m_AtSdhChannelMethods->ModeGet(self);
    }

static uint8 LocalIdGet(AtSdhLine self)
    {
    return ThaModuleSdhLineIdLocalId((ThaModuleSdh)SdhModule((AtChannel)self), (uint8)AtChannelIdGet((AtChannel)self));
    }

static AtSdhLine HideLineGet(AtSdhLine self, uint8 sts1)
    {
    uint8 localId = LocalIdGet(self);
    uint8 firstTerminatedLocalId = (uint8)((localId / 8U) * 4U); /* First ID in group 4 lines */
    AtSdhLine terminatedLine = Tha60290021ModuleSdhTerminatedLineGet(SdhModule((AtChannel)self), firstTerminatedLocalId);

    /* If faceplate line is not STM64, the terminated line must be STM16. */
    if (AtSdhLineRateGet(self) != cAtSdhLineRateStm64)
        {
        if (AtSdhLineRateGet(terminatedLine) == cAtSdhLineRateStm64)
            Tha60290022SdhTerminatedLineRateSet(terminatedLine, cAtSdhLineRateStm16);

        return m_AtSdhLineMethods->HideLineGet(self, sts1);
        }

    /* One faceplate line STM64 is mapped to one terminated line STM64. */
    if (AtSdhLineRateGet(terminatedLine) == cAtSdhLineRateStm64)
        return terminatedLine;

    return m_AtSdhLineMethods->HideLineGet(self, sts1);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, mMethodsGet(self), sizeof(m_AtSdhLineOverride));

        mMethodOverride(m_AtSdhLineOverride, HideLineGet);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void OverrideAtSdhChannel(AtSdhLine self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        mMethodOverride(m_AtSdhChannelOverride, ModeGet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtSdhLine(self);
    OverrideAtSdhChannel(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SdhFacePlateLine);
    }

AtSdhLine Tha60290022SdhFacePlateLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhFacePlateLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60290022SdhFacePlateLineNew(uint32 lineId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022SdhFacePlateLineObjectInit(newLine, lineId, module);
    }

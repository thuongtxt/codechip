/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290022ModuleSdh.h
 * 
 * Created Date: Jul 25, 2017
 *
 * Description : SDH module header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULESDH_H_
#define _THA60290022MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Line */
AtSdhLine Tha60290022SdhFacePlateLineNew(uint32 lineId, AtModuleSdh module);
AtSdhLine Tha60290022SdhTerminatedLineNew(uint32 lineId, AtModuleSdh module);
AtSdhLine Tha60290022SdhMateLineNew(uint32 lineId, AtModuleSdh module);

/* AUG */
AtSdhAug Tha60290022SdhFacePlateAugNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/* AU VC */
AtSdhVc Tha60290022SdhFacePlateLineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60290022SdhTerminatedLineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/* TU3 VC */
AtSdhVc Tha60290022SdhTerminatedLineTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/* VC1x */
AtSdhVc Tha60290022SdhTerminatedLineVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/* AU */
AtSdhAu Tha60290022SdhMateLineAuNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhAu Tha60290022SdhFacePlateLineAuNew(uint32 channelId, uint8 channelType, AtModuleSdh module);


eAtRet Tha60290022ModuleSdhV2De3InterruptEnable(ThaModuleSdh self, eBool enable);
eAtRet Tha60290022ModuleSdhV2De1InterruptEnable(ThaModuleSdh self, eBool enable);
eAtRet Tha60290022SdhTerminatedLineRateSet(AtSdhLine self, eAtSdhLineRate rate);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULESDH_H_ */


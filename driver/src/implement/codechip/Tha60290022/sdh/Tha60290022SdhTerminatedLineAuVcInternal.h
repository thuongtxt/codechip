/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290022SdhTerminatedLineAuVcInternal.h
 * 
 * Created Date: Jun 15, 2019
 *
 * Description : 60290022 Terminated AUVC representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022SDHTERMINATEDLINEAUVCINTERNAL_H_
#define _THA60290022SDHTERMINATEDLINEAUVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/sdh/Tha6029SdhTerminatedLineAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022SdhTerminatedLineAuVc
    {
    tTha6029SdhTerminatedLineAuVc super;
    }tTha60290022SdhTerminatedLineAuVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha60290022SdhTerminatedLineAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022SDHTERMINATEDLINEAUVCINTERNAL_H_ */


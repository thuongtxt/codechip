/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290022SdhMateLine.c
 *
 * Created Date: May 24, 2018
 *
 * Description : Mate SDH line for product 60290022
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/sdh/Tha6029SdhLineInternal.h"
#include "Tha60290022ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022SdhMateLine
    {
    tTha6029SdhMateLine super;
    }tTha60290022SdhMateLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhLineMethods           m_AtSdhLineOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static AtModuleSdh SdhModule(AtSdhLine self)
    {
    return (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    }

static uint8 LocalIdGet(AtSdhLine self)
    {
    return ThaModuleSdhLineIdLocalId((ThaModuleSdh)SdhModule(self), (uint8)AtChannelIdGet((AtChannel)self));
    }

static AtSdhLine HideLineGet(AtSdhLine self, uint8 sts1)
    {
    eAtRet ret = cAtOk;
    uint8 localId = LocalIdGet(self);
    uint8 firstTerminatedLocalId = localId / 4U;
    AtSdhLine terminatedLine;
    AtUnused(sts1);

    terminatedLine = Tha60290021ModuleSdhTerminatedLineGet(SdhModule(self), firstTerminatedLocalId);
    if (AtSdhLineRateGet(terminatedLine) == cAtSdhLineRateStm64)
        {
        ret = Tha60290022SdhTerminatedLineRateSet(terminatedLine, cAtSdhLineRateStm16);
        if (ret != cAtOk)
            return NULL;
        }

    return Tha60290021ModuleSdhTerminatedLineGet(SdhModule(self), localId);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, mMethodsGet(self), sizeof(m_AtSdhLineOverride));

        mMethodOverride(m_AtSdhLineOverride, HideLineGet);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtSdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SdhMateLine);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhMateLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60290022SdhMateLineNew(uint32 lineId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newLine, lineId, module);
    }

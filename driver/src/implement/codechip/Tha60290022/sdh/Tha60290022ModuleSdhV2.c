/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290022ModuleSdhV2.c
 *
 * Created Date: Feb 6, 2018
 *
 * Description : SDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhPathInternal.h"
#include "../../../../generic/ber/AtBerControllerInternal.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "../poh/Tha60290022ModulePohV2Reg.h"
#include "../man/Tha60290022Device.h"
#include "../poh/Tha60290022ModulePoh.h"
#include "Tha60290022ModuleSdhInternal.h"
#include "Tha60290022ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cIteratorMask(idx)      (cBit0 << idx)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleSdhMethods         m_ThaModuleSdhOverride;

/* Save super implementation */
static const tThaModuleSdhMethods         *m_ThaModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePoh ModulePoh(ThaModuleSdh self)
    {
    return (ThaModulePoh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    }

static ThaModulePdh ModulePdh(ThaModuleSdh self)
    {
    return (ThaModulePdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePdh);
    }

static uint32 StsInterruptStatusGet(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 regAddr = ThaModulePohBaseAddress(modulePoh) + cAf6Reg_alm_glbchgohi;
    uint32 regVal = AtHalRead(hal, regAddr);
    AtUnused(glbIntr);
    return mRegField(regVal, cAf6_alm_glbchgohi_glbstachghi_);
    }

static uint32 VtInterruptStatusGet(ThaModuleSdh self, uint32 glbIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 regAddr = ThaModulePohBaseAddress(modulePoh) + cAf6Reg_alm_glbchgolo;
    uint32 regVal = AtHalRead(hal, regAddr);
    AtUnused(glbIntr);
    return mRegField(regVal, cAf6_alm_glbchgolo_glbstachglo_);
    }

static void StsHwInterruptProcess(ThaModuleSdh self, uint32 pohIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 numSlice = Tha60210011ModulePohNumberStsSlices((Tha60210011ModulePoh)modulePoh);
    uint8 slice, sts1, halfSlice, sts48;

    for (halfSlice = 0; halfSlice < 2; halfSlice++)
        {
        if (pohIntr & cIteratorMask(halfSlice))
            {
            uint32 intrSts32Val = AtHalRead(hal, baseAddress + cAf6Reg_alm_glbchghi_Base + (uint32)halfSlice);
            uint32 intrSts32Mask = AtHalRead(hal, baseAddress + cAf6Reg_alm_glbmskhi_Base + (uint32)halfSlice);

            intrSts32Val &= intrSts32Mask;

            for (sts1 = 0, sts48 = (uint8)(halfSlice * 32U); sts1 < 32 && sts48 < 48; sts1++, sts48++)
                {
                if (intrSts32Val & cIteratorMask(sts1))
                    {
                    uint32 stsIntr = AtHalRead(hal, baseAddress + (uint32)cAf6Reg_alm_orstahi(sts48));
                    for (slice = 0; slice < numSlice; slice++)
                        {
                        if (stsIntr & cIteratorMask(slice))
                            {
                            AtSdhPath auPath;

                            /* Get AU path from hardware ID */
                            auPath = ThaModuleSdhAuPathFromHwIdGet(self, cAtModuleSdh, slice, sts48);

                            /* Execute path interrupt process */
                            AtSdhPathInterruptProcess(auPath, slice, sts48, 0, hal);
                            }
                        }
                    }
                }
            }
        }
    }

static void De3BerInterruptProcess(ThaModuleSdh self, uint32 pohIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 numSlice = Tha60210011ModulePohNumberStsSlices((Tha60210011ModulePoh)modulePoh);
    uint8 slice, sts1, halfSlice, sts48;

    for (halfSlice = 0; halfSlice < 2; halfSlice++)
        {
        if (pohIntr & cIteratorMask(halfSlice))
            {
            uint32 intrSts32Val = AtHalRead(hal, baseAddress + cAf6Reg_dealm_glbchghi_Base + (uint32)halfSlice);
            uint32 intrSts32Mask = AtHalRead(hal, baseAddress + cAf6Reg_dealm_glbmskhi_Base + (uint32)halfSlice);

            intrSts32Val &= intrSts32Mask;

            for (sts1 = 0, sts48 = (uint8)(halfSlice * 32U); sts1 < 32 && sts48 < 48; sts1++, sts48++)
                {
                if (intrSts32Val & cIteratorMask(sts1))
                    {
                    uint32 stsIntr = AtHalRead(hal, baseAddress + (uint32)cAf6Reg_dealm_orstahi(sts48));
                    for (slice = 0; slice < numSlice; slice++)
                        {
                        if (stsIntr & cIteratorMask(slice))
                            {
                            AtPdhDe3 de3;
                            AtBerController berController;

                            de3 = ThaModulePdhDe3ChannelFromHwIdGet(ModulePdh(self), cAtModuleSdh, slice, sts48);
                            berController = AtPdhChannelPathBerControllerGet((AtPdhChannel)de3);

                            AtBerControllerInterruptProcess(berController, 0);
                            }
                        }
                    }
                }
            }
        }
    }

static void StsInterruptProcess(ThaModuleSdh self, uint32 pohIntr, AtHal hal)
    {
    uint32 stsIntr = pohIntr & cBit1_0;

    if (stsIntr)
        StsHwInterruptProcess(self, stsIntr, hal);

    stsIntr = (pohIntr & cBit3_2) >> 2;
    if (stsIntr)
        De3BerInterruptProcess(self, stsIntr, hal);
    }

static void VtHwInterruptProcess(ThaModuleSdh self, uint32 pohIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 numSlice = Tha60210011ModulePohNumberStsSlices((Tha60210011ModulePoh)modulePoh);
    uint32 sts1, sts32;
    uint8 slice;

    for (slice = 0; slice < numSlice; slice++)
        {
        for (sts32 = 0; sts32 < 2; sts32++)
            {
            uint32 grpId = (uint32)(slice * 2U + sts32);

            if (pohIntr & cIteratorMask(grpId))
                {
                uint32 intrSts32Val = AtHalRead(hal, baseAddress + cAf6Reg_alm_glbchglo(grpId));
                uint32 intrSts32Mask = AtHalRead(hal, baseAddress + cAf6Reg_alm_glbmsklo(grpId));
                uint8 sts48;

                intrSts32Val &= intrSts32Mask;

                for (sts1 = 0, sts48 = (uint8)(sts32 * 32); sts1 < 32 && sts48 < 48; sts1++, sts48++)
                    {
                    if (intrSts32Val & cIteratorMask(sts1))
                        {
                        uint8 vtg, vt, vt28;
                        uint32 intrVt28Val = AtHalRead(hal, baseAddress + (uint32)cAf6Reg_alm_orstalo(sts48, slice));

                        for (vtg = 0, vt28 = 0; vtg < 7; vtg++)
                            {
                            for (vt = 0; vt < 4; vt++, vt28++)
                                {
                                if (intrVt28Val & cIteratorMask(vt28))
                                    {
                                    AtSdhPath tuPath;

                                    /* Get TU path from hardware ID */
                                    tuPath = ThaModuleSdhTuPathFromHwIdGet((ThaModuleSdh)self, cAtModuleSdh, slice, sts48, vtg, vt);

                                    /* Execute path interrupt process */
                                    AtSdhPathInterruptProcess(tuPath, slice, sts48, vt28, hal);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

static void De1BerInterruptProcess(ThaModuleSdh self, uint32 pohIntr, AtHal hal)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 numSlice = Tha60210011ModulePohNumberStsSlices((Tha60210011ModulePoh)modulePoh);
    uint32 sts1, sts32;
    uint8 slice;

    for (slice = 0; slice < numSlice; slice++)
        {
        for (sts32 = 0; sts32 < 2; sts32++)
            {
            uint32 grpId = (uint32)(slice * 2U + sts32);

            if (pohIntr & cIteratorMask(grpId))
                {
                uint32 intrSts32Val = AtHalRead(hal, baseAddress + cAf6Reg_dealm_glbchglo(grpId));
                uint32 intrSts32Mask = AtHalRead(hal, baseAddress + cAf6Reg_dealm_glbmsklo(grpId));
                uint8 sts48;

                intrSts32Val &= intrSts32Mask;

                for (sts1 = 0, sts48 = (uint8)(sts32 * 32); sts1 < 32; sts1++, sts48++)
                    {
                    if (intrSts32Val & cIteratorMask(sts1))
                        {
                        uint8 vtg, vt, vt28;
                        uint32 intrVt28Val = AtHalRead(hal, baseAddress + (uint32)cAf6Reg_dealm_orstalo(sts48, slice));

                        for (vtg = 0, vt28 = 0; vtg < 7; vtg++)
                            {
                            for (vt = 0; vt < 4; vt++, vt28++)
                                {
                                if (intrVt28Val & cIteratorMask(vt28))
                                    {
                                    AtPdhDe1 de1;
                                    AtBerController berController;

                                    de1 = ThaModulePdhDe1ChannelFromHwIdGet(ModulePdh(self), cAtModuleSdh, slice, sts48, vtg, vt);
                                    berController = AtPdhChannelPathBerControllerGet((AtPdhChannel)de1);

                                    AtBerControllerInterruptProcess(berController, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

static void VtInterruptProcess(ThaModuleSdh self, uint32 pohIntr, AtHal hal)
    {
    uint32 vtIntr;

    vtIntr = mRegField(pohIntr, cAf6_alm_glbchgo_glbstachgvt_);
    if (vtIntr)
        VtHwInterruptProcess(self, pohIntr, hal);

    vtIntr = mRegField(pohIntr, cAf6_alm_glbchgo_glbstachgde1_);
    if (vtIntr)
        De1BerInterruptProcess(self, pohIntr, hal);
    }

static eAtRet StsInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 regAddr, regVal;
    uint8 group;

    regAddr = baseAddress + cAf6Reg_alm_glbmskhislice;
    regVal = mModuleHwRead(self, regAddr);
    regVal = (enable) ? (regVal | cAf6_alm_glbmskhislice_glbmsksts_Mask) : (regVal & ~cAf6_alm_glbmskhislice_glbmsksts_Mask);
    mModuleHwWrite(self, regAddr, regVal);

    regVal = (enable) ? cAf6_alm_glbmskhi_glbmsk_Mask : 0x0;
    for (group = 0; group < 2; group++)
        {
        regAddr = baseAddress + cAf6Reg_alm_glbmskhi_Base + group;
        mModuleHwWrite(self, regAddr, regVal);
        }

    return cAtOk;
    }

static eAtRet VtInterruptEnable(ThaModuleSdh self, eBool enable)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 regAddr, regVal;
    uint8 sts32;

    regAddr = baseAddress + cAf6Reg_alm_glbmsk1oslice;
    regVal = mModuleHwRead(self, regAddr);
    regVal = (enable) ? (regVal | cAf6_alm_glbmsk1oslice_glbmskvt_Mask) : (regVal & ~cAf6_alm_glbmsk1oslice_glbmskvt_Mask);
    mModuleHwWrite(self, regAddr, regVal);

    regVal = (enable) ? cAf6_alm_glbmsklo_glbmsk_Mask : 0x0;
    for (sts32 = 0; sts32 < 16; sts32++)
        mModuleHwWrite(self, baseAddress + (uint32)cAf6Reg_alm_glbmsklo(sts32), regVal);

    return cAtOk;
    }

static eAtRet De3InterruptEnable(ThaModuleSdh self, eBool enable)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 regAddr, regVal;
    uint8 group;

    regAddr = baseAddress + cAf6Reg_alm_glbmskhislice;
    regVal = mModuleHwRead(self, regAddr);
    regVal = (enable) ? (regVal | cAf6_alm_glbmskhislice_glbmskde3_Mask) : (regVal & ~cAf6_alm_glbmskhislice_glbmskde3_Mask);
    mModuleHwWrite(self, regAddr, regVal);

    regVal = (enable) ? cAf6_alm_glbmskhi_glbmsk_Mask : 0x0;
    for (group = 0; group < 2; group++)
        mModuleHwWrite(self, baseAddress + (uint32)cAf6Reg_dealm_glbmskhi(group), regVal);

    return cAtOk;
    }

static eAtRet De1InterruptEnable(ThaModuleSdh self, eBool enable)
    {
    ThaModulePoh modulePoh = ModulePoh(self);
    uint32 baseAddress = ThaModulePohBaseAddress(modulePoh);
    uint32 regAddr, regVal;
    uint8 sts32;

    regAddr = baseAddress + cAf6Reg_alm_glbmsk1oslice;
    regVal = mModuleHwRead(self, regAddr);
    regVal = (enable) ? (regVal | cAf6_alm_glbmsk1oslice_glbmskde1_Mask) : (regVal & ~cAf6_alm_glbmsk1oslice_glbmskde1_Mask);
    mModuleHwWrite(self, regAddr, regVal);

    regVal = (enable) ? cAf6_dealm_glbmsklo_glbmsk_Mask : 0x0;
    for (sts32 = 0; sts32 < 16; sts32++)
        mModuleHwWrite(self, baseAddress + (uint32)cAf6Reg_dealm_glbmsklo(sts32), regVal);

    return cAtOk;
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh moduleSdh = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(moduleSdh);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, mMethodsGet(moduleSdh), sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, StsInterruptStatusGet);
        mMethodOverride(m_ThaModuleSdhOverride, VtInterruptStatusGet);
        mMethodOverride(m_ThaModuleSdhOverride, StsInterruptProcess);
        mMethodOverride(m_ThaModuleSdhOverride, VtInterruptProcess);
        mMethodOverride(m_ThaModuleSdhOverride, StsInterruptEnable);
        mMethodOverride(m_ThaModuleSdhOverride, VtInterruptEnable);
        }

    mMethodsSet(moduleSdh, &m_ThaModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideThaModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleSdhV2);
    }

AtModuleSdh Tha60290022ModuleSdhV2ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60290022ModuleSdhV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModuleSdhV2ObjectInit(newModule, device);
    }

eAtRet Tha60290022ModuleSdhV2De3InterruptEnable(ThaModuleSdh self, eBool enable)
    {
    if (self)
        return De3InterruptEnable(self, enable);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290022ModuleSdhV2De1InterruptEnable(ThaModuleSdh self, eBool enable)
    {
    if (self)
        return De1InterruptEnable(self, enable);
    return cAtErrorNullPointer;
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290022SdhFacePlateAugInternal.h
 * 
 * Created Date: Aug 22, 2018
 *
 * Description : Faceplate AUG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022SDHFACEPLATEAUGINTERNAL_H_
#define _THA60290022SDHFACEPLATEAUGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/sdh/Tha60290021SdhAugInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022SdhFacePlateAug
    {
    tTha60290021SdhFaceplateAug super;
    }tTha60290022SdhFacePlateAug;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAug Tha60290022SdhFacePlateAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022SDHFACEPLATEAUGINTERNAL_H_ */


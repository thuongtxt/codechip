/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290022SdhFacePlateAug.c
 *
 * Created Date: Dec 5, 2017
 *
 * Description : Faceplate AUG
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhLineInternal.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../ocn/Tha60290022ModuleOcn.h"
#include "Tha60290022SdhFacePlateAugInternal.h"
#include "Tha60290022ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods                 m_AtSdhChannelOverride;
static tTha60290021SdhLineSideAugMethods    m_Tha60290021SdhLineSideAugOverride;

/* Save super implementation */
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290022ModuleOcn ModuleOcn(AtSdhChannel self)
    {
    return (Tha60290022ModuleOcn)ThaModuleOcnFromChannel((AtChannel)self);
    }

static eAtModuleSdhRet Aug64MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    uint32 lineId = AtSdhChannelLineGet(self);

    if (mapType == cAtSdhAugMapTypeAug64MapVc4_64c)
        return Tha60290022ModuleOcnFaceplateSts192cEnable(ModuleOcn(self), lineId, cAtTrue);

    return Tha60290022ModuleOcnFaceplateSts192cEnable(ModuleOcn(self), lineId, cAtFalse);
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtModuleSdhRet ret;

    if (AtSdhChannelMapTypeGet(self) == mapType)
        return cAtOk;

    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    if (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeAug64)
        return Aug64MapTypeSet(self, mapType);

    return cAtOk;
    }

static AtSdhChannel HideAug64Get(Tha60290021SdhLineSideAug self, uint8 mapType)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    AtSdhLine terminatedLine;

    if (!Tha60290021SdhChannelHaveHideChannel(channel))
        return NULL;

    terminatedLine = AtSdhLineHideLineGet(AtSdhChannelLineObjectGet(channel), AtSdhChannelSts1Get(channel));

    if (mapType == cAtSdhAugMapTypeAug64MapVc4_64c)
        {
        if (AtSdhLineRateGet(terminatedLine) != cAtSdhLineRateStm64)
            Tha60290022SdhTerminatedLineRateSet(terminatedLine, cAtSdhLineRateStm64);
        return (AtSdhChannel)AtSdhLineAug64Get(terminatedLine, 0);
        }

    /* If AUG-64 mapping differs from VC4-64c, it must set terminated line rate to STM16 */
    if (AtSdhLineRateGet(terminatedLine) == cAtSdhLineRateStm64)
        Tha60290022SdhTerminatedLineRateSet(terminatedLine, cAtSdhLineRateStm16);

    return NULL;
    }

static void OverrideTha60290021SdhLineSideAug(AtSdhAug self)
    {
    Tha60290021SdhLineSideAug channel = (Tha60290021SdhLineSideAug)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021SdhLineSideAugOverride, mMethodsGet(channel), sizeof(m_Tha60290021SdhLineSideAugOverride));

        mMethodOverride(m_Tha60290021SdhLineSideAugOverride, HideAug64Get);
        }

    mMethodsSet(channel, &m_Tha60290021SdhLineSideAugOverride);
    }

static void OverrideAtSdhChannel(AtSdhAug self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhAug self)
    {
    OverrideTha60290021SdhLineSideAug(self);
    OverrideAtSdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SdhFacePlateAug);
    }

AtSdhAug Tha60290022SdhFacePlateAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021SdhFaceplateAugObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAug Tha60290022SdhFacePlateAugNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAug newAug = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newAug == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022SdhFacePlateAugObjectInit(newAug, channelId, channelType, module);
    }

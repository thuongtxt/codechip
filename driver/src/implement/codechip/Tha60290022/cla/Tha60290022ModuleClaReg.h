/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_CLA_H_
#define _AF6_REG_AF6CNC0022_RD_CLA_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Classify Global PSN Control
Reg Addr   : 0x00000000 - 0x00000003
Reg Formula: 0x00000000 +  eth_port
    Where  : 
           + $eth_port(0-3): Ethernet port ID
Reg Desc   : 
This register configures identification per Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_glb_psn_Base                                                                    0x00000000
#define cAf6Reg_cla_glb_psn_WidthVal                                                                        96

/*--------------------------------------
BitField Name: RxEthTypeDis
BitField Type: RW
BitField Desc: Disable check ETH type
BitField Bits: [78]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxEthTypeDis_Mask                                                              cBit14
#define cAf6_cla_glb_psn_RxEthTypeDis_Shift                                                                 14

/*--------------------------------------
BitField Name: RxPsnDAExp
BitField Type: RW
BitField Desc: Mac Address expected at Rx Ethernet port
BitField Bits: [77:30]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnDAExp_01_Mask                                                          cBit31_30
#define cAf6_cla_glb_psn_RxPsnDAExp_01_Shift                                                                30
#define cAf6_cla_glb_psn_RxPsnDAExp_02_Mask                                                           cBit13_0
#define cAf6_cla_glb_psn_RxPsnDAExp_02_Shift                                                                 0

/*--------------------------------------
BitField Name: RxSendLbit2CdrEn
BitField Type: RW
BitField Desc: Enable to send Lbit to CDR engine 1: Enable to send Lbit 0:
Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxSendLbit2CdrEn_Mask                                                          cBit29
#define cAf6_cla_glb_psn_RxSendLbit2CdrEn_Shift                                                             29

/*--------------------------------------
BitField Name: RxPsnMplsOutLabelCheckEn
BitField Type: RW
BitField Desc: Enable to check MPLS outer 1: Enable checking 0: Disable checking
BitField Bits: [28]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnMplsOutLabelCheckEn_Mask                                                  cBit28
#define cAf6_cla_glb_psn_RxPsnMplsOutLabelCheckEn_Shift                                                     28

/*--------------------------------------
BitField Name: RxPsnCpuBfdCtlEn
BitField Type: RW
BitField Desc: Enable VCCV BFD control packet sending to CPU for processing 1:
Enable sending 0: Discard
BitField Bits: [27]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnCpuBfdCtlEn_Mask                                                          cBit27
#define cAf6_cla_glb_psn_RxPsnCpuBfdCtlEn_Shift                                                             27

/*--------------------------------------
BitField Name: RxMacCheckDis
BitField Type: RW
BitField Desc: Disable to check MAC address at Ethernet port receive direction
1: Disable checking 0: Enable checking
BitField Bits: [26]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxMacCheckDis_Mask                                                             cBit26
#define cAf6_cla_glb_psn_RxMacCheckDis_Shift                                                                26

/*--------------------------------------
BitField Name: RxPsnCpuIcmpEn
BitField Type: RW
BitField Desc: Enable ICMP control packet sending to CPU for processing 1:
Enable sending 0: Discard
BitField Bits: [25]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnCpuIcmpEn_Mask                                                            cBit25
#define cAf6_cla_glb_psn_RxPsnCpuIcmpEn_Shift                                                               25

/*--------------------------------------
BitField Name: RxPsnCpuArpEn
BitField Type: RW
BitField Desc: Enable ARP control packet sending to CPU for processing 1: Enable
sending 0: Discard
BitField Bits: [24]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnCpuArpEn_Mask                                                             cBit24
#define cAf6_cla_glb_psn_RxPsnCpuArpEn_Shift                                                                24

/*--------------------------------------
BitField Name: PweLoopClaEn
BitField Type: RW
BitField Desc: Enable Loop back traffic from PW Encapsulation to Classification
1: Enable Loop back mode 0: Normal, not loop back
BitField Bits: [23]
--------------------------------------*/
#define cAf6_cla_glb_psn_PweLoopClaEn_Mask                                                              cBit23
#define cAf6_cla_glb_psn_PweLoopClaEn_Shift                                                                 23

/*--------------------------------------
BitField Name: RxPsnIpUdpMode
BitField Type: RW
BitField Desc: This bit is applicable for Ipv4/Ipv6 packet from Ethernet side 1:
Classify engine uses RxPsnIpUdpSel to decide which UDP port (Source or
Destination) is used to identify pseudowire packet 0: Classify engine will
automatically search for value 0x85E in source or destination UDP port. The
remaining UDP port is used to identify pseudowire packet
BitField Bits: [22]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_Mask                                                            cBit22
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_Shift                                                               22

/*--------------------------------------
BitField Name: RxPsnIpUdpSel
BitField Type: RW
BitField Desc: This bit is applicable for Ipv4/Ipv6 using to select Source or
Destination to identify pseudowire packet from Ethernet side. It is not use when
RxPsnIpUdpMode is zero 1: Classify engine selects source UDP port to identify
pseudowire packet 0: Classify engine selects destination UDP port to identify
pseudowire packet
BitField Bits: [21]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_Mask                                                             cBit21
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_Shift                                                                21

/*--------------------------------------
BitField Name: RxPsnIpTtlChkEn
BitField Type: RW
BitField Desc: Enable check TTL field in MPLS/Ipv4 or Hop Limit field in Ipv6 1:
Enable checking 0: Disable checking
BitField Bits: [20]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnIpTtlChkEn_Mask                                                           cBit20
#define cAf6_cla_glb_psn_RxPsnIpTtlChkEn_Shift                                                              20

/*--------------------------------------
BitField Name: RxPsnMplsOutLabel
BitField Type: RW
BitField Desc: Received 2-label MPLS packet from PSN side will be discarded when
it's outer label is different than RxPsnMplsOutLabel
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnMplsOutLabel_Mask                                                       cBit19_0
#define cAf6_cla_glb_psn_RxPsnMplsOutLabel_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Pseudowire MEF Over MPLS Control
Reg Addr   : 0x00060000 - 0x00061FFF #The address format for these registers is 0x00060000 + PWID
Reg Formula: 0x00060000 +  PWID
    Where  : 
           + $PWID(0-8191): Pseudowire ID
Reg Desc   : 
This register configures identification MEF over MPLS

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_per_pw_mefompls_Base                                                            0x00060000
#define cAf6Reg_cla_per_pw_mefompls_WidthVal                                                                96

/*--------------------------------------
BitField Name: RxEthPwOnflyVC34
BitField Type: RW
BitField Desc: CEP EBM on-fly fractional, length changed
BitField Bits: [60]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxEthPwOnflyVC34_Mask                                                  cBit28
#define cAf6_cla_per_pw_mefompls_RxEthPwOnflyVC34_Shift                                                     28

/*--------------------------------------
BitField Name: RxEthPwLen
BitField Type: RW
BitField Desc: length of packet to check malform
BitField Bits: [59:46]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxEthPwLen_Mask                                                     cBit27_14
#define cAf6_cla_per_pw_mefompls_RxEthPwLen_Shift                                                           14

/*--------------------------------------
BitField Name: RxEthSupEn
BitField Type: RW
BitField Desc: Suppress Enable
BitField Bits: [45]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxEthSupEn_Mask                                                        cBit13
#define cAf6_cla_per_pw_mefompls_RxEthSupEn_Shift                                                           13

/*--------------------------------------
BitField Name: RxEthCepMode
BitField Type: RW
BitField Desc: CEP mode working 0,1: CEP basic
BitField Bits: [44]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxEthCepMode_Mask                                                      cBit12
#define cAf6_cla_per_pw_mefompls_RxEthCepMode_Shift                                                         12

/*--------------------------------------
BitField Name: RxEthRtpSsrcValue
BitField Type: RW
BitField Desc: This value is used to compare with SSRC value in RTP header of
received TDM PW packets
BitField Bits: [43:12]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxEthRtpSsrcValue_01_Mask                                           cBit31_12
#define cAf6_cla_per_pw_mefompls_RxEthRtpSsrcValue_01_Shift                                                 12
#define cAf6_cla_per_pw_mefompls_RxEthRtpSsrcValue_02_Mask                                            cBit11_0
#define cAf6_cla_per_pw_mefompls_RxEthRtpSsrcValue_02_Shift                                                  0

/*--------------------------------------
BitField Name: RxEthRtpPtValue
BitField Type: RW
BitField Desc: This value is used to compare with PT value in RTP header of
received TDM PW packets
BitField Bits: [11:5]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxEthRtpPtValue_Mask                                                 cBit11_5
#define cAf6_cla_per_pw_mefompls_RxEthRtpPtValue_Shift                                                       5

/*--------------------------------------
BitField Name: RxEthRtpEn
BitField Type: RW
BitField Desc: Enable RTP 1: Enable RTP 0: Disable RTP
BitField Bits: [4]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxEthRtpEn_Mask                                                         cBit4
#define cAf6_cla_per_pw_mefompls_RxEthRtpEn_Shift                                                            4

/*--------------------------------------
BitField Name: RxEthRtpSsrcChkEn
BitField Type: RW
BitField Desc: Enable checking SSRC field of RTP header in received TDM PW
packet 1: Enable checking 0: Disable checking
BitField Bits: [3]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxEthRtpSsrcChkEn_Mask                                                  cBit3
#define cAf6_cla_per_pw_mefompls_RxEthRtpSsrcChkEn_Shift                                                     3

/*--------------------------------------
BitField Name: RxEthRtpPtChkEn
BitField Type: RW
BitField Desc: Enable checking PT field of RTP header in received TDM PW packet
1: Enable checking 0: Disable checking
BitField Bits: [2]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxEthRtpPtChkEn_Mask                                                    cBit2
#define cAf6_cla_per_pw_mefompls_RxEthRtpPtChkEn_Shift                                                       2

/*--------------------------------------
BitField Name: RxPwMEFoMPLSEcidValue
BitField Type: RW
BitField Desc: ECID value used to compare with ECID in Ethernet MEF packet
BitField Bits: [70:51]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidValue_01_Mask                                       cBit31_19
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidValue_01_Shift                                             19
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidValue_02_Mask                                         cBit6_0
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSEcidValue_02_Shift                                              0

/*--------------------------------------
BitField Name: RxPwMEFoMPLSDaValue
BitField Type: RW
BitField Desc: DA value used to compare with DA in Ethernet MEF packet
BitField Bits: [50:3]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaValue_01_Mask                                          cBit31_3
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaValue_01_Shift                                                3
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaValue_02_Mask                                          cBit18_0
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaValue_02_Shift                                                0

/*--------------------------------------
BitField Name: RxPwMEFoMPLSDaCheckEn
BitField Type: 
BitField Desc: Enable checking DA in Ethernet MEF packet with MPLS PW label
Begin:
BitField Bits: [2]
--------------------------------------*/
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaCheckEn_Mask                                              cBit2
#define cAf6_cla_per_pw_mefompls_RxPwMEFoMPLSDaCheckEn_Shift                                                 2


/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Hashing Table Control
Reg Addr   : 0x00020000 - 0x00023FFF
Reg Formula: 0x00020000 + HashID
    Where  : 
           + $HashID(0-32768): HashID
Reg Desc   : 
HBCE module uses 14 bits for tab-index. Tab-index is generated by hashing function applied to the original id. %%
HDL_PATH     :rtlclapro.iaf6ces96rtlcla_hcbe.mem_p0hshtabinf.memram.ram.ram[$HashID]
Hashing function applies an XOR function to all bits of tab-index. %%
The indexes to the tab-index are generated by hashing function and therefore collisions may occur. %%
There are maximum fours (4) entries for every hash to identify flow traffic whether match or not. %%
If the collisions are more than fours (4), they are handled by pointer to link another memory. %%
The formula of hash pattern is {label ID(20bits), PSN mode (2bits), eth_port(2bits)}, call HashPattern (24bits)%%
The HashID formula has two case depend on CLAHbceCodingSelectedMode%%
CLAHbceCodingSelectedMode = 1: HashID = HashPattern[14:0] XOR {6'd0,HashPattern[23:15]}, CLAHbceStoreID[8:0] = HashPattern[23:15]%%
CLAHbceCodingSelectedMode = 0: HashID = HashPattern[14:0], CLAHbceStoreID = HashPattern[23:15]
#CLAHbceCodingSelectedMode = 1: HashID = HashPattern[13:0] XOR {4'd0,HashPattern[23:14]}, CLAHbceStoreID = HashPattern[23:14]%%
#CLAHbceCodingSelectedMode = 0: HashID = HashPattern[13:0], CLAHbceStoreID = HashPattern[23:14]

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_hbce_hash_table_Base                                                            0x00020000
#define cAf6Reg_cla_hbce_hash_table_WidthVal                                                                32

/*--------------------------------------
BitField Name: CLAHbceLink
BitField Type: RW
BitField Desc: this is pointer to link extra location for conflict hash more
than fours 1: Link to another memory 0: not link more
BitField Bits: [12]
--------------------------------------*/
#define cAf6_cla_hbce_hash_table_CLAHbceLink_Mask                                                       cBit12
#define cAf6_cla_hbce_hash_table_CLAHbceLink_Shift                                                          12

/*--------------------------------------
BitField Name: CLAHbceMemoryExtraStartPointer
BitField Type: RW
BitField Desc: this is a MemExtraPtr to read the Classify HBCE Looking Up
Information Extra Control in case the number of collisions are more than 4
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Mask                                  cBit11_0
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Looking Up Information Control
Reg Addr   : 0x00080000 - 0x00083FFF
Reg Formula: 0x00080000 + CollisHashID*0x10000 + HashID
    Where  : 
           + $HashID(0-32768): HashID
           + $CollisHashID(0-3): Collision
Reg Desc   : 
This memory contain 4 entries (collisions) to examine one Pseudowire label whether match or not.%%
HDL_PATH: begin:
IF($CollisHashID==0)
rtlclapro.iaf6ces96rtlcla_hcbe.mem_memlk1.memram.ram.ram[$HashID]
ELSEIF($CollisHashID==1)
rtlclapro.iaf6ces96rtlcla_hcbe.mem_memlk2.memram.ram.ram[$HashID]
ELSEIF($CollisHashID==2)
rtlclapro.iaf6ces96rtlcla_hcbe.mem_memlk3.memram.ram.ram[$HashID]
ELSE
rtlclapro.iaf6ces96rtlcla_hcbe.mem_memlk4.memram.ram.ram[$HashID]
HDL_PATH: end:
In general, One hashing(HashID) contain 4 entries (collisions). If the collisions are over 4, it will jump to the another extra memory.

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_hbce_lkup_info_Base                                                             0x00080000
#define cAf6Reg_cla_hbce_lkup_info_WidthVal                                                                 64

/*--------------------------------------
BitField Name: CLAHbceFlowDirect
BitField Type: RW
BitField Desc: this configure FlowID working in normal mode (not in any group)
BitField Bits: [37]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Mask                                                   cBit5
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Shift                                                      5

/*--------------------------------------
BitField Name: CLAHbceGrpWorking
BitField Type: RW
BitField Desc: this configure group working or protection
BitField Bits: [36]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Mask                                                   cBit4
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Shift                                                      4

/*--------------------------------------
BitField Name: CLAHbceGrpIDFlow
BitField Type: RW
BitField Desc: this configure a group ID that FlowID following
BitField Bits: [35:24]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Mask_01                                             cBit31_24
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Shift_01                                                   24
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Mask_02                                               cBit3_0
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Shift_02                                                    0

/*--------------------------------------
BitField Name: CLAHbceFlowID
BitField Type: RW
BitField Desc: This is PW ID identification
BitField Bits: [23:10]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Mask                                                   cBit23_10
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Shift                                                         10

/*--------------------------------------
BitField Name: CLAHbceFlowEnb
BitField Type: RW
BitField Desc: The flow is identified in this table, it mean the traffic is
identified 1: Flow identified 0: Flow look up fail
BitField Bits: [9]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Mask                                                      cBit9
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Shift                                                         9

/*--------------------------------------
BitField Name: CLAHbceStoreID
BitField Type: RW
BitField Desc: this is saving some additional information to identify a certain
lookup address rule within a particular table entry HBCE and also to be able to
distinguish those that collide
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Mask                                                    cBit8_0
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Shift                                                         0

/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Looking Up Information Control
Reg Addr   : 0x00080000 - 0x00087FFF
Reg Formula: 0x00080000 + HashID
    Where  : 
           + $HashID(0-32768): HashID
Reg Desc   : 
This memory contain 4 entries (collisions) to examine one Pseudowire label whether match or not.%%
In general, One hashing(HashID) contain 4 entries (collisions). If the collisions are over 4, it will jump to the another extra memory.

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_hbce_lkup_info_Base                                                             0x00080000

/*--------------------------------------
BitField Name: CLAHbceFlowID3
BitField Type: RW
BitField Desc: This is PW ID identification
BitField Bits: [95:82]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID3_Mask                                                  cBit31_18
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID3_Shift                                                        18

/*--------------------------------------
BitField Name: CLAHbceFlowEnb3
BitField Type: RW
BitField Desc: The flow is identified in this table, it mean the traffic is
identified 1: Flow identified 0: Flow look up fail
BitField Bits: [81]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb3_Mask                                                    cBit17
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb3_Shift                                                       17

/*--------------------------------------
BitField Name: CLAHbceStoreID3
BitField Type: RW
BitField Desc: this is saving some additional information to identify a certain
lookup address rule within a particular table entry HBCE and also to be able to
distinguish those that collide
BitField Bits: [80:72]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID3_Mask                                                  cBit16_8
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID3_Shift                                                        8

/*--------------------------------------
BitField Name: CLAHbceFlowID2
BitField Type: RW
BitField Desc: This is PW ID identification
BitField Bits: [71:58]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID2_01_Mask                                               cBit31_26
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID2_01_Shift                                                     26
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID2_02_Mask                                                 cBit7_0
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID2_02_Shift                                                      0

/*--------------------------------------
BitField Name: CLAHbceFlowEnb2
BitField Type: RW
BitField Desc: The flow is identified in this table, it mean the traffic is
identified 1: Flow identified 0: Flow look up fail
BitField Bits: [57]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb2_Mask                                                    cBit25
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb2_Shift                                                       25

/*--------------------------------------
BitField Name: CLAHbceStoreID2
BitField Type: RW
BitField Desc: this is saving some additional information to identify a certain
lookup address rule within a particular table entry HBCE and also to be able to
distinguish those that collide
BitField Bits: [56:48]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID2_Mask                                                 cBit24_16
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID2_Shift                                                       16

/*--------------------------------------
BitField Name: CLAHbceFlowID1
BitField Type: RW
BitField Desc: This is PW ID identification
BitField Bits: [47:34]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID1_Mask                                                   cBit15_2
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID1_Shift                                                         2

/*--------------------------------------
BitField Name: CLAHbceFlowEnb1
BitField Type: RW
BitField Desc: The flow is identified in this table, it mean the traffic is
identified 1: Flow identified 0: Flow look up fail
BitField Bits: [33]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb1_Mask                                                     cBit1
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb1_Shift                                                        1

/*--------------------------------------
BitField Name: CLAHbceStoreID1
BitField Type: RW
BitField Desc: this is saving some additional information to identify a certain
lookup address rule within a particular table entry HBCE and also to be able to
distinguish those that collide
BitField Bits: [32:24]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID1_01_Mask                                              cBit31_24
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID1_01_Shift                                                    24
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID1_02_Mask                                                  cBit0
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID1_02_Shift                                                     0

/*--------------------------------------
BitField Name: CLAHbceFlowID0
BitField Type: RW
BitField Desc: This is PW ID identification
BitField Bits: [23:10]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID0_Mask                                                  cBit23_10
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID0_Shift                                                        10

/*--------------------------------------
BitField Name: CLAHbceFlowEnb0
BitField Type: RW
BitField Desc: The flow is identified in this table, it mean the traffic is
identified 1: Flow identified 0: Flow look up fail
BitField Bits: [9]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb0_Mask                                                     cBit9
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb0_Shift                                                        9

/*--------------------------------------
BitField Name: CLAHbceStoreID0
BitField Type: RW
BitField Desc: this is saving some additional information to identify a certain
lookup address rule within a particular table entry HBCE and also to be able to
distinguish those that collide
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID0_Mask                                                   cBit8_0
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID0_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Group Enable Control
Reg Addr   : 0x00070000 - 0x00071FFF
Reg Formula: 0x00070000 + Working_ID*0x1000 + Grp_ID
    Where  : 
           + $Working_ID(0-1): CLAHbceGrpWorking
           + $Grp_ID(0-4095): CLAHbceGrpIDFlow
Reg Desc   : 
This register configures Group that Flowid (or Pseudowire ID) is enable or not
HDL_PATH     :rtlclapro.iaf6ces96rtlcla_hcbe.irampwgrp.ram.ram[$Grp_ID]

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_per_grp_enb_Base                                                                0x00070000
#define cAf6Reg_cla_per_grp_enb_WidthVal                                                                    32

/*--------------------------------------
BitField Name: CLAGrpPWEn
BitField Type: RW
BitField Desc: This indicate the FlowID (or Pseudowire ID) is enable or not 1:
FlowID enable 0: FlowID disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Mask                                                             cBit0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Ethernet port Enable Control
Reg Addr   : 0x000D0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures specific Ethernet port is enable or not

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_per_eth_enb_Base                                                                0x000D0000
#define cAf6Reg_cla_per_eth_enb_WidthVal                                                                    32

/*--------------------------------------
BitField Name: CLAEthPort4En
BitField Type: RW
BitField Desc: This indicate Ethernet port 4 is enable or not 1: enable 0:
disable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_cla_per_eth_enb_CLAEthPort4En_Mask                                                          cBit3
#define cAf6_cla_per_eth_enb_CLAEthPort4En_Shift                                                             3

/*--------------------------------------
BitField Name: CLAEthPort3En
BitField Type: RW
BitField Desc: This indicate Ethernet port 3 is enable or not 1: enable 0:
disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_cla_per_eth_enb_CLAEthPort3En_Mask                                                          cBit2
#define cAf6_cla_per_eth_enb_CLAEthPort3En_Shift                                                             2

/*--------------------------------------
BitField Name: CLAEthPort2En
BitField Type: RW
BitField Desc: This indicate Ethernet port 2 is enable or not 1: enable 0:
disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_cla_per_eth_enb_CLAEthPort2En_Mask                                                          cBit1
#define cAf6_cla_per_eth_enb_CLAEthPort2En_Shift                                                             1

/*--------------------------------------
BitField Name: CLAEthPort1En
BitField Type: RW
BitField Desc: This indicate Ethernet port 1 is enable or not 1: enable 0:
disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cla_per_eth_enb_CLAEthPort1En_Mask                                                          cBit0
#define cAf6_cla_per_eth_enb_CLAEthPort1En_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count0_64 bytes packet
Reg Addr   : 0x000D1000(RO)
Reg Formula: 0x000D1000 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt0_64_ro_Base                                                                 0x000D1000

/*--------------------------------------
BitField Name: CLAEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Mask                                                        cBit31_0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count0_64 bytes packet
Reg Addr   : 0x000D1800(RC)
Reg Formula: 0x000D1800 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt0_64_rc_Base                                                                 0x000D1800

/*--------------------------------------
BitField Name: CLAEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Mask                                                        cBit31_0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count65_127 bytes packet
Reg Addr   : 0x000D1004(RO)
Reg Formula: 0x000D1004 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt65_127_ro_Base                                                               0x000D1004

/*--------------------------------------
BitField Name: CLAEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Mask                                                    cBit31_0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count65_127 bytes packet
Reg Addr   : 0x000D1804(RC)
Reg Formula: 0x000D1804 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt65_127_rc_Base                                                               0x000D1804

/*--------------------------------------
BitField Name: CLAEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Mask                                                    cBit31_0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count128_255 bytes packet
Reg Addr   : 0x000D1008(RO)
Reg Formula: 0x000D1008 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt128_255_ro_Base                                                              0x000D1008

/*--------------------------------------
BitField Name: CLAEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Mask                                                  cBit31_0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count128_255 bytes packet
Reg Addr   : 0x000D1808(RC)
Reg Formula: 0x000D1808 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt128_255_rc_Base                                                              0x000D1808

/*--------------------------------------
BitField Name: CLAEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Mask                                                  cBit31_0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count256_511 bytes packet
Reg Addr   : 0x000D100C(RO)
Reg Formula: 0x000D100C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt256_511_ro_Base                                                              0x000D100C

/*--------------------------------------
BitField Name: CLAEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Mask                                                  cBit31_0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count256_511 bytes packet
Reg Addr   : 0x000D180C(RC)
Reg Formula: 0x000D180C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt256_511_rc_Base                                                              0x000D180C

/*--------------------------------------
BitField Name: CLAEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Mask                                                  cBit31_0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count512_1024 bytes packet
Reg Addr   : 0x000D1010(RO)
Reg Formula: 0x000D1010 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt512_1024_ro_Base                                                             0x000D1010

/*--------------------------------------
BitField Name: CLAEthCnt512_1024
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt512_1024_ro_CLAEthCnt512_1024_Mask                                                cBit31_0
#define cAf6_Eth_cnt512_1024_ro_CLAEthCnt512_1024_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count512_1024 bytes packet
Reg Addr   : 0x000D1810(RC)
Reg Formula: 0x000D1810 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt512_1024_rc_Base                                                             0x000D1810

/*--------------------------------------
BitField Name: CLAEthCnt512_1024
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt512_1024_rc_CLAEthCnt512_1024_Mask                                                cBit31_0
#define cAf6_Eth_cnt512_1024_rc_CLAEthCnt512_1024_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count1025_1528 bytes packet
Reg Addr   : 0x000D1014(RO)
Reg Formula: 0x000D1014 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1025_1528_ro_Base                                                            0x000D1014

/*--------------------------------------
BitField Name: CLAEthCnt1025_1528
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1025_1528_ro_CLAEthCnt1025_1528_Mask                                              cBit31_0
#define cAf6_Eth_cnt1025_1528_ro_CLAEthCnt1025_1528_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count1025_1528 bytes packet
Reg Addr   : 0x000D1814(RC)
Reg Formula: 0x000D1814 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1025_1528_rc_Base                                                            0x000D1814

/*--------------------------------------
BitField Name: CLAEthCnt1025_1528
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1025_1528_rc_CLAEthCnt1025_1528_Mask                                              cBit31_0
#define cAf6_Eth_cnt1025_1528_rc_CLAEthCnt1025_1528_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count1529_2047 bytes packet
Reg Addr   : 0x000D1018(RO)
Reg Formula: 0x000D1018 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1529_2047_ro_Base                                                            0x000D1018

/*--------------------------------------
BitField Name: CLAEthCnt1529_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1529_2047_ro_CLAEthCnt1529_2047_Mask                                              cBit31_0
#define cAf6_Eth_cnt1529_2047_ro_CLAEthCnt1529_2047_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count1529_2047 bytes packet
Reg Addr   : 0x000D1818(RC)
Reg Formula: 0x000D1818 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1529_2047_rc_Base                                                            0x000D1818

/*--------------------------------------
BitField Name: CLAEthCnt1529_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1529_2047_rc_CLAEthCnt1529_2047_Mask                                              cBit31_0
#define cAf6_Eth_cnt1529_2047_rc_CLAEthCnt1529_2047_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Jumbo packet
Reg Addr   : 0x000D101C(RO)
Reg Formula: 0x000D101C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_jumbo_ro_Base                                                               0x000D101C

/*--------------------------------------
BitField Name: CLAEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Mask                                                     cBit31_0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Jumbo packet
Reg Addr   : 0x000D181C(RC)
Reg Formula: 0x000D181C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_jumbo_rc_Base                                                               0x000D181C

/*--------------------------------------
BitField Name: CLAEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Mask                                                     cBit31_0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Unicast packet
Reg Addr   : 0x000D1020(RO)
Reg Formula: 0x000D1020 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the unicast packet

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_Unicast_ro_Base                                                             0x000D1020

/*--------------------------------------
BitField Name: CLAEthCntUnicast
BitField Type: RO
BitField Desc: This is statistic counter for the unicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Mask                                                 cBit31_0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Unicast packet
Reg Addr   : 0x000D1820(RC)
Reg Formula: 0x000D1820 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the unicast packet

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_Unicast_rc_Base                                                             0x000D1820

/*--------------------------------------
BitField Name: CLAEthCntUnicast
BitField Type: RO
BitField Desc: This is statistic counter for the unicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Mask                                                 cBit31_0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Total packet
Reg Addr   : 0x000D1024(RO)
Reg Formula: 0x000D1024 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the total packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_pkt_cnt_ro_Base                                                             0x000D1024

/*--------------------------------------
BitField Name: CLAEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_Mask                                                   cBit31_0
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Total packet
Reg Addr   : 0x000D1824(RC)
Reg Formula: 0x000D1824 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the total packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_pkt_cnt_rc_Base                                                             0x000D1824

/*--------------------------------------
BitField Name: CLAEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_Mask                                                   cBit31_0
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Broadcast packet
Reg Addr   : 0x000D1028(RO)
Reg Formula: 0x000D1028 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the Broadcast packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_bcast_pkt_cnt_ro_Base                                                       0x000D1028

/*--------------------------------------
BitField Name: CLAEthCntBroadcast
BitField Type: RO
BitField Desc: This is statistic counter broadcast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Mask                                         cBit31_0
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Broadcast packet
Reg Addr   : 0x000D1828(RC)
Reg Formula: 0x000D1828 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the Broadcast packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_bcast_pkt_cnt_rc_Base                                                       0x000D1828

/*--------------------------------------
BitField Name: CLAEthCntBroadcast
BitField Type: RO
BitField Desc: This is statistic counter broadcast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Mask                                         cBit31_0
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Multicast packet
Reg Addr   : 0x000D102C(RO)
Reg Formula: 0x000D102C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the Multicast packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_mcast_pkt_cnt_ro_Base                                                       0x000D102C

/*--------------------------------------
BitField Name: CLAEthCntMulticast
BitField Type: RO
BitField Desc: This is statistic counter multicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_Mask                                         cBit31_0
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Multicast packet
Reg Addr   : 0x000D182C(RC)
Reg Formula: 0x000D182C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the Multicast packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_mcast_pkt_cnt_rc_Base                                                       0x000D182C

/*--------------------------------------
BitField Name: CLAEthCntMulticast
BitField Type: RO
BitField Desc: This is statistic counter multicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_Mask                                         cBit31_0
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Under size packet
Reg Addr   : 0x000D1030(RO)
Reg Formula: 0x000D1030 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the under size packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_under_size_pkt_cnt_ro_Base                                                  0x000D1030

/*--------------------------------------
BitField Name: CLAEthCntUnderSize
BitField Type: RO
BitField Desc: This is statistic counter under size packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_Mask                                    cBit31_0
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Under size packet
Reg Addr   : 0x000D1830(RC)
Reg Formula: 0x000D1830 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the under size packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_under_size_pkt_cnt_rc_Base                                                  0x000D1830

/*--------------------------------------
BitField Name: CLAEthCntUnderSize
BitField Type: RO
BitField Desc: This is statistic counter under size packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_Mask                                    cBit31_0
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Over size packet
Reg Addr   : 0x000D1034(RO)
Reg Formula: 0x000D1034 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the over size packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_over_size_pkt_cnt_ro_Base                                                   0x000D1034

/*--------------------------------------
BitField Name: CLAEthCntOverSize
BitField Type: RO
BitField Desc: This is statistic counter over size packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_Mask                                      cBit31_0
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Over size packet
Reg Addr   : 0x000D1834(RC)
Reg Formula: 0x000D1834 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic counter for the over size packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_over_size_pkt_cnt_rc_Base                                                   0x000D1834

/*--------------------------------------
BitField Name: CLAEthCntOverSize
BitField Type: RO
BitField Desc: This is statistic counter over size packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_Mask                                      cBit31_0
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count FCS error packet
Reg Addr   : 0x000D1038(RO)
Reg Formula: 0x000D1038 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic count FCS error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_phy_err_ro_Base                                                             0x000D1038

/*--------------------------------------
BitField Name: CLAEthCntPhyErr
BitField Type: RO
BitField Desc: This is statistic counter FCS error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_Mask                                                  cBit31_0
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count FCS error packet
Reg Addr   : 0x000D1838(RC)
Reg Formula: 0x000D1838 + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic count FCS error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_phy_err_rc_Base                                                             0x000D1838

/*--------------------------------------
BitField Name: CLAEthCntPhyErr
BitField Type: RO
BitField Desc: This is statistic counter FCS error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_Mask                                                  cBit31_0
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count number of bytes of packet
Reg Addr   : 0x000D103C(RO)
Reg Formula: 0x000D103C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic count number of bytes of packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_byte_ro_Base                                                                0x000D103C

/*--------------------------------------
BitField Name: CLAEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Mask                                                       cBit31_0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count number of bytes of packet
Reg Addr   : 0x000D183C(RC)
Reg Formula: 0x000D183C + eth_port
    Where  : 
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   : 
This register is statistic count number of bytes of packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_byte_rc_Base                                                                0x000D183C

/*--------------------------------------
BitField Name: CLAEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Mask                                                       cBit31_0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Looking Up Information Control2
Reg Addr   : 0x0040000 - 0x0047FFF
Reg Formula: 0x0040000 + CollisHashIDExtra*0x1000  + MemExtraPtr
    Where  : 
           + $MemExtraPtr(0-4095): MemExtraPtr
           + $CollisHashIDExtra (0-7): Collision
Reg Desc   : 
This memory contain maximum 8 entries extra to examine one Pseudowire label whether match or not

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_hbce_lkup_info_extra_Base                                                        0x0040000
#define cAf6Reg_cla_hbce_lkup_info_extra_WidthVal                                                           64

/*--------------------------------------
BitField Name: CLAExtraHbceFlowDirect
BitField Type: RW
BitField Desc: this configure FlowID working in normal mode (not in any group)
BitField Bits: [37]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_Mask                                        cBit5
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_Shift                                           5

/*--------------------------------------
BitField Name: CLAExtraHbceGrpWorking
BitField Type: RW
BitField Desc: this configure group working or protection
BitField Bits: [36]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_Mask                                        cBit4
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_Shift                                           4

/*--------------------------------------
BitField Name: CLAExtraHbceGrpIDFlow
BitField Type: RW
BitField Desc: this configure a group ID that FlowID following
BitField Bits: [35:24]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_01_Mask                                  cBit31_24
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_01_Shift                                        24
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_02_Mask                                    cBit3_0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_02_Shift                                         0

/*--------------------------------------
BitField Name: CLAExtraHbceFlowID
BitField Type: RW
BitField Desc: This is PW ID identification
BitField Bits: [23:11]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_Mask                                        cBit23_11
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_Shift                                              11

/*--------------------------------------
BitField Name: CLAExtraHbceFlowEnb
BitField Type: RW
BitField Desc: The flow is identified in this table, it mean the traffic is
identified 1: Flow identified 0: Flow look up fail
BitField Bits: [10]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_Mask                                          cBit10
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_Shift                                             10

/*--------------------------------------
BitField Name: CLAExtraHbceStoreID
BitField Type: RW
BitField Desc: this is saving some additional information to identify a certain
lookup address rule within a particular table entry HBCE and also to be able to
distinguish those that collide
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_Mask                                         cBit9_0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Pseudowire Slice to CDR Control
Reg Addr   : 0x00C0000 - 0x00C1FFF
Reg Formula: 0x00C0000 +  PWID
    Where  : 
           + $PWID(0-8191): Pseudowire ID
Reg Desc   : 
This register configures Slice ID to CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_2_cdr_cfg_Base                                                                   0x00C0000
#define cAf6Reg_cla_2_cdr_cfg_WidthVal                                                                      32

/*--------------------------------------
BitField Name: PwCdrEn
BitField Type: RW
BitField Desc: Indicate Pseudowire enable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_cla_2_cdr_cfg_PwCdrEn_Mask                                                                 cBit15
#define cAf6_cla_2_cdr_cfg_PwCdrEn_Shift                                                                    15

/*--------------------------------------
BitField Name: PwHoLoOc48Id
BitField Type: RW
BitField Desc: Indicate 8x Hi order OC48
BitField Bits: [13:11]
--------------------------------------*/
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_Mask                                                         cBit13_11
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_Shift                                                               11

/*--------------------------------------
BitField Name: PwTdmLineId
BitField Type: RW
BitField Desc: The formular of this TdmLineId is mastersts*32+vtid
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_Mask                                                           cBit10_0
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Classify Hold Register Status
Reg Addr   : 0x00000A - 0x00000C
Reg Formula: 0x00000A +  HID
    Where  : 
           + $HID(0-2): Hold ID
Reg Desc   : 
This register using for hold remain that more than 128bits

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_hold_status_Base                                                                  0x00000A

/*--------------------------------------
BitField Name: PwHoldStatus
BitField Type: RW
BitField Desc: Hold 32bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cla_hold_status_PwHoldStatus_Mask                                                        cBit31_0
#define cAf6_cla_hold_status_PwHoldStatus_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit3_0 Control
Reg Addr   : 0xE0000 - 0xE000F
Reg Formula: 0xE0000 + HaAddr3_0
    Where  : 
           + $HaAddr3_0(0-15): HA Address Bit3_0
Reg Desc   : 
This register is used to send HA read address bit3_0 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha3_0_control_Base                                                                   0xE0000
#define cAf6Reg_rdha3_0_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr3_0
BitField Type: RW
BitField Desc: Read value will be 0xE00000 plus HaAddr3_0
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha3_0_control_ReadAddr3_0_Mask                                                         cBit19_0
#define cAf6_rdha3_0_control_ReadAddr3_0_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit7_4 Control
Reg Addr   : 0xE0010 - 0xE001F
Reg Formula: 0xE0010 + HaAddr7_4
    Where  : 
           + $HaAddr7_4(0-15): HA Address Bit7_4
Reg Desc   : 
This register is used to send HA read address bit7_4 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha7_4_control_Base                                                                   0xE0010
#define cAf6Reg_rdha7_4_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr7_4
BitField Type: RW
BitField Desc: Read value will be 0xE00000 plus HaAddr7_4
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha7_4_control_ReadAddr7_4_Mask                                                         cBit19_0
#define cAf6_rdha7_4_control_ReadAddr7_4_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit11_8 Control
Reg Addr   : 0xE0020 - 0xE002F
Reg Formula: 0xE0020 + HaAddr11_8
    Where  : 
           + $HaAddr11_8(0-15): HA Address Bit11_8
Reg Desc   : 
This register is used to send HA read address bit11_8 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha11_8_control_Base                                                                  0xE0020
#define cAf6Reg_rdha11_8_control_WidthVal                                                                   32

/*--------------------------------------
BitField Name: ReadAddr11_8
BitField Type: RW
BitField Desc: Read value will be 0xE00000 plus HaAddr11_8
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha11_8_control_ReadAddr11_8_Mask                                                       cBit19_0
#define cAf6_rdha11_8_control_ReadAddr11_8_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit15_12 Control
Reg Addr   : 0xE0030 - 0xE003F
Reg Formula: 0xE0030 + HaAddr15_12
    Where  : 
           + $HaAddr15_12(0-15): HA Address Bit15_12
Reg Desc   : 
This register is used to send HA read address bit15_12 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha15_12_control_Base                                                                 0xE0030
#define cAf6Reg_rdha15_12_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr15_12
BitField Type: RW
BitField Desc: Read value will be 0xE00000 plus HaAddr15_12
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha15_12_control_ReadAddr15_12_Mask                                                     cBit19_0
#define cAf6_rdha15_12_control_ReadAddr15_12_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit19_16 Control
Reg Addr   : 0xE0040 - 0xE004F
Reg Formula: 0xE0040 + HaAddr19_16
    Where  : 
           + $HaAddr19_16(0-15): HA Address Bit19_16
Reg Desc   : 
This register is used to send HA read address bit19_16 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha19_16_control_Base                                                                 0xE0040
#define cAf6Reg_rdha19_16_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr19_16
BitField Type: RW
BitField Desc: Read value will be 0xE00000 plus HaAddr19_16
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha19_16_control_ReadAddr19_16_Mask                                                     cBit19_0
#define cAf6_rdha19_16_control_ReadAddr19_16_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit23_20 Control
Reg Addr   : 0xE0050 - 0xE005F
Reg Formula: 0xE0050 + HaAddr23_20
    Where  : 
           + $HaAddr23_20(0-15): HA Address Bit23_20
Reg Desc   : 
This register is used to send HA read address bit23_20 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha23_20_control_Base                                                                 0xE0050
#define cAf6Reg_rdha23_20_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr23_20
BitField Type: RW
BitField Desc: Read value will be 0xE00000 plus HaAddr23_20
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha23_20_control_ReadAddr23_20_Mask                                                     cBit19_0
#define cAf6_rdha23_20_control_ReadAddr23_20_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit24 and Data Control
Reg Addr   : 0xE0060 - 0xE0061
Reg Formula: 0xE0060 + HaAddr24
    Where  : 
           + $HaAddr24(0-1): HA Address Bit24
Reg Desc   : 
This register is used to send HA read address bit24 to HA engine to read data

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha24data_control_Base                                                                0xE0060

/*--------------------------------------
BitField Name: ReadHaData31_0
BitField Type: RW
BitField Desc: HA read data bit31_0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha24data_control_ReadHaData31_0_Mask                                                   cBit31_0
#define cAf6_rdha24data_control_ReadHaData31_0_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data63_32
Reg Addr   : 0xE0070
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha_hold63_32_Base                                                                    0xE0070

/*--------------------------------------
BitField Name: ReadHaData63_32
BitField Type: RW
BitField Desc: HA read data bit63_32
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha_hold63_32_ReadHaData63_32_Mask                                                      cBit31_0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data95_64
Reg Addr   : 0xE0071
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold95_64_Base                                                                  0xE0071

/*--------------------------------------
BitField Name: ReadHaData95_64
BitField Type: RW
BitField Desc: HA read data bit95_64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Mask                                                    cBit31_0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data127_96
Reg Addr   : 0xE0072
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold127_96_Base                                                                 0xE0072

/*--------------------------------------
BitField Name: ReadHaData127_96
BitField Type: RW
BitField Desc: HA read data bit127_96
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Mask                                                  cBit31_0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Classify Parity Register Control
Reg Addr   : 0x000F8000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register using for Force Parity

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_Parity_control_Base                                                             0x000F8000
#define cAf6Reg_cla_Parity_control_WidthVal                                                                 32

/*--------------------------------------
BitField Name: ClaForceErr7
BitField Type: RW
BitField Desc: Enable parity error force for "Classify Per Pseudowire
Identification to CDR Control"
BitField Bits: [16]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr7_Mask                                                       cBit16
#define cAf6_cla_Parity_control_ClaForceErr7_Shift                                                          16

/*--------------------------------------
BitField Name: ClaForceErr6
BitField Type: RW
BitField Desc: Enable parity error force for "Classify HBCE Hashing Table
Control"
BitField Bits: [15]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr6_Mask                                                       cBit15
#define cAf6_cla_Parity_control_ClaForceErr6_Shift                                                          15

/*--------------------------------------
BitField Name: ClaForceErr5
BitField Type: RW
BitField Desc: Enable parity error force for "Classify HBCE Looking Up
Information Control2"
BitField Bits: [14:7]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr5_Mask                                                     cBit14_7
#define cAf6_cla_Parity_control_ClaForceErr5_Shift                                                           7

/*--------------------------------------
BitField Name: ClaForceErr4
BitField Type: RW
BitField Desc: Enable parity error force for "Classify Per Group Enable Control"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr4_Mask                                                        cBit6
#define cAf6_cla_Parity_control_ClaForceErr4_Shift                                                           6

/*--------------------------------------
BitField Name: ClaForceErr3
BitField Type: RW
BitField Desc: Enable parity error force for "Classify Per Pseudowire MEF Over
MPLS Control"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr3_Mask                                                        cBit5
#define cAf6_cla_Parity_control_ClaForceErr3_Shift                                                           5

/*--------------------------------------
BitField Name: ClaForceErr2
BitField Type: RW
BitField Desc: Enable parity error force for "Classify Per Pseudowire Type
Control"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr2_Mask                                                        cBit4
#define cAf6_cla_Parity_control_ClaForceErr2_Shift                                                           4

/*--------------------------------------
BitField Name: ClaForceErr1
BitField Type: RW
BitField Desc: Enable parity error force for "Classify HBCE Looking Up
Information Control"
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr1_Mask                                                      cBit3_0
#define cAf6_cla_Parity_control_ClaForceErr1_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Classify Parity Disable register Control
Reg Addr   : 0x000F8001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register using for Disable Parity

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_Parity_Disable_control_Base                                                     0x000F8001
#define cAf6Reg_cla_Parity_Disable_control_WidthVal                                                         32

/*--------------------------------------
BitField Name: ClaDisChkErr7
BitField Type: RW
BitField Desc: Disable parity error check for "Classify Per Pseudowire
Identification to CDR Control"
BitField Bits: [16]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr7_Mask                                              cBit16
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr7_Shift                                                 16

/*--------------------------------------
BitField Name: ClaDisChkErr6
BitField Type: RW
BitField Desc: Disable parity error check for "Classify HBCE Hashing Table
Control"
BitField Bits: [15]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr6_Mask                                              cBit15
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr6_Shift                                                 15

/*--------------------------------------
BitField Name: ClaDisChkErr5
BitField Type: RW
BitField Desc: Disable parity error check for "Classify HBCE Looking Up
Information Control2"
BitField Bits: [14:7]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr5_Mask                                            cBit14_7
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr5_Shift                                                  7

/*--------------------------------------
BitField Name: ClaDisChkErr4
BitField Type: RW
BitField Desc: Disable parity error check for "Classify Per Group Enable
Control"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr4_Mask                                               cBit6
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr4_Shift                                                  6

/*--------------------------------------
BitField Name: ClaDisChkErr3
BitField Type: RW
BitField Desc: Disable parity error check for "Classify Per Pseudowire MEF Over
MPLS Control"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr3_Mask                                               cBit5
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr3_Shift                                                  5

/*--------------------------------------
BitField Name: ClaDisChkErr2
BitField Type: RW
BitField Desc: Disable parity error check for "Classify Per Pseudowire Type
Control"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr2_Mask                                               cBit4
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr2_Shift                                                  4

/*--------------------------------------
BitField Name: ClaDisChkErr1
BitField Type: RW
BitField Desc: Disable parity error check for "Classify HBCE Looking Up
Information Control"
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr1_Mask                                             cBit3_0
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr1_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Classify Parity sticky error
Reg Addr   : 0x000F8002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register using for checking sticky error

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_Parity_stk_err_Base                                                             0x000F8002
#define cAf6Reg_cla_Parity_stk_err_WidthVal                                                                 32

/*--------------------------------------
BitField Name: ClaParStkErr7
BitField Type: W1C
BitField Desc: Parity sticky error check for "Classify Per Pseudowire
Identification to CDR Control"
BitField Bits: [16]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr7_Mask                                                      cBit16
#define cAf6_cla_Parity_stk_err_ClaParStkErr7_Shift                                                         16

/*--------------------------------------
BitField Name: ClaParStkErr6
BitField Type: W1C
BitField Desc: Parity sticky error check for "Classify HBCE Hashing Table
Control"
BitField Bits: [15]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr6_Mask                                                      cBit15
#define cAf6_cla_Parity_stk_err_ClaParStkErr6_Shift                                                         15

/*--------------------------------------
BitField Name: ClaParStkErr5
BitField Type: W1C
BitField Desc: Parity sticky error check for "Classify HBCE Looking Up
Information Control2"
BitField Bits: [14:7]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr5_Mask                                                    cBit14_7
#define cAf6_cla_Parity_stk_err_ClaParStkErr5_Shift                                                          7

/*--------------------------------------
BitField Name: ClaParStkErr4
BitField Type: W1C
BitField Desc: Parity sticky error check for "Classify Per Group Enable Control"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr4_Mask                                                       cBit6
#define cAf6_cla_Parity_stk_err_ClaParStkErr4_Shift                                                          6

/*--------------------------------------
BitField Name: ClaParStkErr3
BitField Type: W1C
BitField Desc: Parity sticky error check for "Classify Per Pseudowire MEF Over
MPLS Control"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr3_Mask                                                       cBit5
#define cAf6_cla_Parity_stk_err_ClaParStkErr3_Shift                                                          5

/*--------------------------------------
BitField Name: ClaParStkErr2
BitField Type: W1C
BitField Desc: Parity sticky error check for "Classify Per Pseudowire Type
Control"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr2_Mask                                                       cBit4
#define cAf6_cla_Parity_stk_err_ClaParStkErr2_Shift                                                          4

/*--------------------------------------
BitField Name: ClaParStkErr1
BitField Type: W1C
BitField Desc: Parity sticky error check for "Classify HBCE Looking Up
Information Control"
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr1_Mask                                                     cBit3_0
#define cAf6_cla_Parity_stk_err_ClaParStkErr1_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count PSN error packet
Reg Addr   : 0x00000005(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is statistic count PSN error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_psn_err1_ro_Base                                                            0x00000005

/*--------------------------------------
BitField Name: CLAEthCntPSNErr1
BitField Type: RO
BitField Desc: This is statistic counter PSN error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_psn_err1_ro_CLAEthCntPSNErr1_Mask                                                cBit31_0
#define cAf6_Eth_cnt_psn_err1_ro_CLAEthCntPSNErr1_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count PSN error packet
Reg Addr   : 0x00000006(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is statistic count PSN error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_psn_err1_rc_Base                                                            0x00000006

/*--------------------------------------
BitField Name: CLAEthCntPSNErr1
BitField Type: RO
BitField Desc: This is statistic counter PSN error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_psn_err1_rc_CLAEthCntPSNErr1_Mask                                                cBit31_0
#define cAf6_Eth_cnt_psn_err1_rc_CLAEthCntPSNErr1_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count PSN error packet
Reg Addr   : 0x00000007(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is statistic count PSN error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_psn_err2_ro_Base                                                            0x00000007

/*--------------------------------------
BitField Name: CLAEthCntPSNErr2
BitField Type: RO
BitField Desc: This is statistic counter PSN error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_psn_err2_ro_CLAEthCntPSNErr2_Mask                                                cBit31_0
#define cAf6_Eth_cnt_psn_err2_ro_CLAEthCntPSNErr2_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count PSN error packet
Reg Addr   : 0x00000008(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is statistic count PSN error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_psn_err2_rc_Base                                                            0x00000008

/*--------------------------------------
BitField Name: CLAEthCntPSNErr2
BitField Type: RO
BitField Desc: This is statistic counter PSN error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_psn_err2_rc_CLAEthCntPSNErr2_Mask                                                cBit31_0
#define cAf6_Eth_cnt_psn_err2_rc_CLAEthCntPSNErr2_Shift                                                      0

#endif /* _AF6_REG_AF6CNC0022_RD_CLA_H_ */

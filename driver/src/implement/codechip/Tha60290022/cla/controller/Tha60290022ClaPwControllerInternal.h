/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60290022ClaPwControllerInternal.h
 * 
 * Created Date: Mar 13, 2018
 *
 * Description : CLA PW controller internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022CLAPWCONTROLLERINTERNAL_H_
#define _THA60290022CLAPWCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60290021/cla/controller/Tha60290021ClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ClaPwController
    {
    tTha60290021ClaPwController super;
    }tTha60290022ClaPwController;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaPwController Tha60290022ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022CLAPWCONTROLLERINTERNAL_H_ */


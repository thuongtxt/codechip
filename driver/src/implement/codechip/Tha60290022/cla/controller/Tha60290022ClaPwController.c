/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290022ClaPwController.c
 *
 * Created Date: Apr 27, 2017
 *
 * Description : PW controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "../../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "./Tha60290022ClaPwControllerInternal.h"
#include "../Tha60290022ModuleClaReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mClaModule(self) ThaClaControllerModuleGet((ThaClaController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaPwControllerMethods m_ThaClaPwControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaHbce HbceCreate(ThaClaPwController self, uint8 hbceId, AtIpCore core)
    {
    return Tha60290022HbceNew(hbceId, (AtModule)mClaModule(self), core);
    }

static eAtRet CdrCircuitSliceSet(ThaClaPwController self, AtPw pw, uint8 slice)
    {
    uint32 regAddr = cAf6Reg_cla_2_cdr_cfg_Base + Tha60210011ModuleClaBaseAddress(mClaModule(self)) + mClaPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModuleCla);
    uint32 tdmLineId = Tha60210011ModulePwTdmLineIdOfPw(pw, cThaModuleCla);

    mRegFieldSet(regVal, cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_, slice);
    mRegFieldSet(regVal, cAf6_cla_2_cdr_cfg_PwTdmLineId_, tdmLineId);

    mChannelHwWrite(pw, regAddr, regVal, cThaModuleCla);
    return cAtOk;
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, mMethodsGet(self), sizeof(m_ThaClaPwControllerOverride));

        mMethodOverride(m_ThaClaPwControllerOverride, HbceCreate);
        mMethodOverride(m_ThaClaPwControllerOverride, CdrCircuitSliceSet);
        }

    mMethodsSet(self, &m_ThaClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ClaPwController);
    }

ThaClaPwController Tha60290022ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60290022ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ClaPwControllerObjectInit(newController, cla);
    }

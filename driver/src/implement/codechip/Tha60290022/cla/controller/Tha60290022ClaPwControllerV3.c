/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290022ClaPwControllerV3.c
 *
 * Created Date: Mar 13, 2018
 *
 * Description : CLA PW Controller implementation for HW optimization version 3 (fpga 5.x)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "./Tha60290022ClaPwControllerInternal.h"
#include "../Tha60290022ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mClaModule(self) ThaClaControllerModuleGet((ThaClaController)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022ClaPwControllerV3
    {
    tTha60290022ClaPwController super;
    }tTha60290022ClaPwControllerV3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaPwControllerMethods m_ThaClaPwControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 MaxCollision(ThaClaPwController self)
    {
    AtUnused(self);
    return (uint8)(cNumberOfEntryBlockV3 * cNumberOfCellPerEntryV3);
    }

static uint8 HbceMaxFlowsPerEntry(ThaClaPwController self)
    {
    return MaxCollision(self);
    }

static ThaHbce HbceCreate(ThaClaPwController self, uint8 hbceId, AtIpCore core)
    {
    return Tha60290022HbceV3New(hbceId, (AtModule)mClaModule(self), core);
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, mMethodsGet(self), sizeof(m_ThaClaPwControllerOverride));

        mMethodOverride(m_ThaClaPwControllerOverride, HbceCreate);
        mMethodOverride(m_ThaClaPwControllerOverride, HbceMaxFlowsPerEntry);
        }

    mMethodsSet(self, &m_ThaClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ClaPwControllerV3);
    }

static ThaClaPwController ObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60290022ClaPwControllerV3New(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cla);
    }

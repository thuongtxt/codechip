/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290022ModuleClaV3.c
 *
 * Created Date: Mar 13, 2018
 *
 * Description : Module CLA implementation for HW optimization version 3 (fpga 5.x)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "./Tha60290022ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleClaMethods         m_ThaModuleClaOverride;

/* Save super implementation */
static const tAtModuleMethods      *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return Tha60290022ClaPwControllerV3New(self);
    }

static eBool CLAHBCEHashingTabCtrlIsUsed(ThaModuleCla self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    const char *ramName = AtInternalRamName(ram);

    if (AtStrcmp(ramName, sTha60210011ModuleClaRamClassifyPerGroupEnableControl) == 0)
        return cAtTrue;

    if (AtStrncmp(ramName,
                  sTha60210011ModuleClaRamClassifyHBCELookupUpInformationControl2,
                  AtStrlen(sTha60210011ModuleClaRamClassifyHBCELookupUpInformationControl2)) == 0)
        return cAtTrue;

    if (AtStrcmp(ramName, sTha60210011ModuleClaRamClassifyHBCEHashingTableControl) == 0)
        return cAtTrue;

    return m_AtModuleMethods->InternalRamIsReserved(self, ram);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        mMethodOverride(m_ThaModuleClaOverride, CLAHBCEHashingTabCtrlIsUsed);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCla(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleClaV3);
    }

AtModule Tha60290022ModuleClaV3ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290022ModuleClaV3New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModuleClaV3ObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60290022ModuleClaInternal.h
 * 
 * Created Date: Mar 13, 2018
 *
 * Description : CLA module interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULECLAV3_H_
#define _THA60290022MODULECLAV3_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/cla/Tha60290021ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cNumberOfCellPerEntryV3      3
#define cNumberOfHashEntryBlockV3    1
#define cNumberOfEntryBlockV3        3
#define cNumberOfEntryPerBlockV3 32768

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModuleCla
    {
    tTha60290021ModuleCla super;
    }tTha60290022ModuleCla;

typedef struct tTha60290022ModuleClaV3
    {
    tTha60290022ModuleCla super;
    }tTha60290022ModuleClaV3;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290022ModuleClaObjectInit(AtModule self, AtDevice device);
AtModule Tha60290022ModuleClaV3ObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULECLAV3_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290022Hbce.c
 *
 * Created Date: Apr 27, 2017
 *
 * Description : HBCE
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "./Tha60290022HbceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaHbcePatern23_15Mask  cBit23_15
#define cThaHbcePatern23_15Shift 15
#define cThaHbcePatern14_0Mask   cBit14_0
#define cThaHbcePatern14_0Shift  0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaHbceMethods         m_ThaHbceOverride;
static tTha60210011HbceMethods m_Tha60210011HbceOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumEntries(ThaHbce self)
    {
    AtUnused(self);
    return 32768;
    }

static uint32 HashIndexGet(Tha60210011Hbce self, uint32 hwLabel)
    {
    uint32 swHbcePatern23_15 = mRegField(hwLabel, cThaHbcePatern23_15);
    uint32 swHbcePatern14_0  = mRegField(hwLabel, cThaHbcePatern14_0);
    AtUnused(self);

    /* Calculate hash index */
    return swHbcePatern14_0 ^ swHbcePatern23_15;
    }

static eAtRet RemainBitsGet(Tha60210011Hbce self, uint32 hwLabel, uint32 *remainBits)
    {
    AtUnused(self);
    remainBits[0] = mRegField(hwLabel, cThaHbcePatern23_15);
    return cAtOk;
    }

static ThaHbceMemoryPool MemoryPoolCreate(ThaHbce self)
    {
    return Tha60290022HbceMemoryPoolNew(self);
    }

static uint32 MaxNumHashCells(Tha60210011Hbce self)
    {
    return ThaHbceMaxNumEntries((ThaHbce)self) * Tha60210011HbceNumHashCollisionInOneHashIndex(self);
    }

static void OverrideTha60210011Hbce(ThaHbce self)
    {
    Tha60210011Hbce hbce = (Tha60210011Hbce)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011HbceOverride, mMethodsGet(hbce), sizeof(m_Tha60210011HbceOverride));

        mMethodOverride(m_Tha60210011HbceOverride, HashIndexGet);
        mMethodOverride(m_Tha60210011HbceOverride, RemainBitsGet);
        mMethodOverride(m_Tha60210011HbceOverride, MaxNumHashCells);
        }

    mMethodsSet(hbce, &m_Tha60210011HbceOverride);
    }

static void OverrideThaHbce(ThaHbce self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceOverride, mMethodsGet(self), sizeof(m_ThaHbceOverride));

        mMethodOverride(m_ThaHbceOverride, MaxNumEntries);
        mMethodOverride(m_ThaHbceOverride, MemoryPoolCreate);
        }

    mMethodsSet(self, &m_ThaHbceOverride);
    }

static void Override(ThaHbce self)
    {
    OverrideThaHbce(self);
    OverrideTha60210011Hbce(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022Hbce);
    }

ThaHbce Tha60290022HbceObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011HbceObjectInit(self, hbceId, claModule, core) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbce Tha60290022HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbce newHbce = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newHbce == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022HbceObjectInit(newHbce, hbceId, claModule, core);
    }

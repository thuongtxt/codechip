/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290022HbceMemoryPool.c
 *
 * Created Date: Jul 31, 2017
 *
 * Description : HBCE memory pool
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/util/AtUtil.h"
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/util/ThaUtil.h"
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCellContent.h"
#include "./Tha60290022HbceInternal.h"
#include "../../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "../../man/Tha60290022Device.h"
#include "../Tha60290022ModuleClaReg.h"
#include "./Tha60290022HbceMemoryPoolInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override  */
static tThaHbceMemoryPoolMethods         m_ThaHbceMemoryPoolOverride;
static tTha60210011HbceMemoryPoolMethods m_Tha60210011HbceMemoryPoolOverride;

/* Save super implementation */
static const tThaHbceMemoryPoolMethods         *m_ThaHbceMemoryPoolMethods         = NULL;
static const tTha60210011HbceMemoryPoolMethods *m_Tha60210011HbceMemoryPoolMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumCells(ThaHbceMemoryPool self)
    {
    ThaHbce hbce = ThaHbceMemoryPoolHbceGet(self);
    uint32 numHashEntries = ThaHbceMaxNumEntries(hbce);
    uint32 numExtraPointers = Tha60210011HbceNumExtraMemoryPointer(hbce);
    uint32 numCellPerEntries = Tha60210011HbceNumHashCollisionInOneHashIndex((Tha60210011Hbce)hbce);
    uint32 numCellPerExtraPointer = Tha60210011HbceNumHashCollisionInOneExtraPointer((Tha60210011Hbce)hbce);

    return (numHashEntries * numCellPerEntries) + (numExtraPointers * numCellPerExtraPointer);
    }

static AtDevice Device(ThaHbceMemoryPool self)
    {
    AtModule module = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    return AtModuleDeviceGet(module);
    }

static uint32 ClaHbceLookingUpInformationCtrlPerCellOffset(Tha60210011HbceMemoryPool self, uint32 cellIndex, uint32 blockIndex, uint32 indexInBlock)
    {
    if (Tha60290022DeviceHwLogicOptimized(Device((ThaHbceMemoryPool)self)))
        return indexInBlock;

    return m_Tha60210011HbceMemoryPoolMethods->ClaHbceLookingUpInformationCtrlPerCellOffset(self, cellIndex, blockIndex, indexInBlock);
    }

static uint32 LocalIndex(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    uint32 maxNumHashIdsPerColision = ThaHbceMaxNumEntries(self->hbce);
    return cellIndex / maxNumHashIdsPerColision;
    }

static AtIpCore Core(ThaHbceMemoryPool self)
    {
    return ThaHbceCoreGet(self->hbce);
    }

static eAtRet StandardCellInfoSet(ThaHbceMemoryPool self, uint32 cellIndex, eBool enable, uint32 flowId, uint32 storeId)
    {
    uint32 localIndex = LocalIndex(self, cellIndex);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 regAddr = Tha60210011ClaHbceLookingUpInformationCtrlPerCell(self, cellIndex);
    Tha60210011ModuleCla claModule = (Tha60210011ModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 hwEnabled = enable ? 1 : 0;

    mModuleHwLongRead(claModule, regAddr, longReg, cThaLongRegMaxSize, Core(self));

    switch (localIndex)
        {
        case 0:
            mRegFieldSet(longReg[0], cAf6_cla_hbce_lkup_info_CLAHbceStoreID0_, storeId);
            mRegFieldSet(longReg[0], cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb0_, hwEnabled);
            mRegFieldSet(longReg[0], cAf6_cla_hbce_lkup_info_CLAHbceFlowID0_, flowId);
            break;
        case 1:
            mLongFieldSet(longReg, 0, cAf6_cla_hbce_lkup_info_CLAHbceStoreID1_, storeId);
            mRegFieldSet(longReg[1], cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb1_, hwEnabled);
            mRegFieldSet(longReg[1], cAf6_cla_hbce_lkup_info_CLAHbceFlowID1_, flowId);
            break;
        case 2:
            mRegFieldSet(longReg[1], cAf6_cla_hbce_lkup_info_CLAHbceStoreID2_, storeId);
            mRegFieldSet(longReg[1], cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb2_, hwEnabled);
            mLongFieldSet(longReg, 1, cAf6_cla_hbce_lkup_info_CLAHbceFlowID2_, flowId);
            break;
        case 3:
            mRegFieldSet(longReg[2], cAf6_cla_hbce_lkup_info_CLAHbceStoreID3_, storeId);
            mRegFieldSet(longReg[2], cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb3_, hwEnabled);
            mRegFieldSet(longReg[2], cAf6_cla_hbce_lkup_info_CLAHbceFlowID3_, flowId);
            break;
        default:
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "Invalid local index (%u)\r\n", localIndex);
            return cAtErrorInvalidOperation;
        }

    mModuleHwLongWrite(claModule, regAddr, longReg, cThaLongRegMaxSize, Core(self));

    return cAtOk;
    }

static eAtRet StandardCellInfoGet(Tha60210011HbceMemoryPool self, uint32 cellIndex, uint32 *flowId, eBool *enable, uint32 *storeId)
    {
    ThaHbceMemoryPool pool = (ThaHbceMemoryPool)self;
    uint32 localIndex = LocalIndex(pool, cellIndex);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 regAddr = Tha60210011ClaHbceLookingUpInformationCtrlPerCell(pool, cellIndex);
    Tha60210011ModuleCla claModule = (Tha60210011ModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);

    mModuleHwLongRead(claModule, regAddr, longReg, cThaLongRegMaxSize, Core(pool));

    switch (localIndex)
        {
        case 0:
            mAssign(storeId, mRegField(longReg[0], cAf6_cla_hbce_lkup_info_CLAHbceStoreID0_));
            mAssign(enable , (longReg[0] & cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb0_Mask) ? cAtTrue : cAtFalse);
            mAssign(flowId , mRegField(longReg[0], cAf6_cla_hbce_lkup_info_CLAHbceFlowID0_));
            break;
        case 1:
            mAssign(storeId, mLongField(longReg, 0, cAf6_cla_hbce_lkup_info_CLAHbceStoreID1_));
            mAssign(enable , (longReg[1] & cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb1_Mask) ? cAtTrue : cAtFalse);
            mAssign(flowId , mRegField(longReg[1], cAf6_cla_hbce_lkup_info_CLAHbceFlowID1_));
            break;
        case 2:
            mAssign(storeId, mRegField(longReg[1], cAf6_cla_hbce_lkup_info_CLAHbceStoreID2_));
            mAssign(enable , (longReg[1] & cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb2_Mask) ? cAtTrue : cAtFalse);
            mAssign(flowId , mLongField(longReg, 1, cAf6_cla_hbce_lkup_info_CLAHbceFlowID2_));
            break;
        case 3:
            mAssign(storeId, mRegField(longReg[2], cAf6_cla_hbce_lkup_info_CLAHbceStoreID3_));
            mAssign(enable , (longReg[2] & cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb3_Mask) ? cAtTrue : cAtFalse);
            mAssign(flowId , mRegField(longReg[2], cAf6_cla_hbce_lkup_info_CLAHbceFlowID3_));
            break;
        default:
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "Invalid local index (%u)\r\n", localIndex);
            return cAtErrorInvalidOperation;
        }

    return cAtOk;
    }

static eAtRet CellInfoGet(Tha60210011HbceMemoryPool self, uint32 cellIndex, uint32 *flowId, eBool *enable, uint32 *storeId)
    {
    ThaHbceMemoryPool pool = (ThaHbceMemoryPool)self;

    if (Tha60210011HbceMemoryPoolIsStandardCellIndex(pool, cellIndex))
        return StandardCellInfoGet(self, cellIndex, flowId, enable, storeId);

    return m_Tha60210011HbceMemoryPoolMethods->CellInfoGet(self, cellIndex, flowId, enable, storeId);
    }

static eAtRet CellEnable(ThaHbceMemoryPool self, ThaHbceMemoryCell cell, eBool enable)
    {
    uint32 cellIndex = ThaHbceMemoryCellIndexGet(cell);

    if (Tha60290022DeviceHwLogicOptimized(Device(self)) && Tha60210011HbceMemoryPoolIsStandardCellIndex(self, cellIndex))
        {
        uint32 flowId, storeId;
        eAtRet ret = cAtOk;

        Tha60210011HbceMemoryPoolCellInfoGet(self, cellIndex, &flowId, NULL, &storeId);
        ret |= StandardCellInfoSet(self, cellIndex, enable, flowId, storeId);
        ret |= ThaHbceMemoryCellContentEnable(ThaHbceMemoryCellContentGet(cell), enable);

        return ret;
        }

    return m_ThaHbceMemoryPoolMethods->CellEnable(self, cell, enable);
    }

static eAtRet CellInit(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    eBool enabled = cAtFalse;
    uint32 flowId = 0, storeId = cAf6_cla_hbce_lkup_info_CLAHbceStoreID0_Mask >> cAf6_cla_hbce_lkup_info_CLAHbceStoreID0_Shift;

    if (!Tha60290022DeviceHwLogicOptimized(Device(self)))
        return m_ThaHbceMemoryPoolMethods->CellInit(self, cellIndex);

    if (!Tha60210011HbceMemoryPoolIsStandardCellIndex(self, cellIndex))
        return m_ThaHbceMemoryPoolMethods->CellInit(self, cellIndex);

    return StandardCellInfoSet(self, cellIndex, enabled, flowId, storeId);
    }

static void ApplyCell(Tha60210011HbceMemoryPool self, uint32 cellIndex)
    {
    ThaHbceMemoryPool pool = (ThaHbceMemoryPool)self;

    if (Tha60210011HbceMemoryPoolIsStandardCellIndex(pool, cellIndex) && Tha60290022DeviceHwLogicOptimized(Device(pool)))
        {
        ThaHbceMemoryCellContent cellContent = ThaHbceMemoryCellContentGet(ThaHbceMemoryPoolCellAtIndex(pool, cellIndex));
        uint32 flowId = 0;
        ThaPwAdapter pwAdapter;
        ThaPwHeaderController headerController;
        uint32 storeId;
        eBool enabled = cAtFalse;
        Tha60210011ModuleCla claModule = (Tha60210011ModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);

        if (cellContent == NULL)
            return;

        headerController = ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent);
        pwAdapter = ThaPwHeaderControllerAdapterGet(headerController);
        if (pwAdapter)
            {
            enabled = AtChannelIsEnabled((AtChannel)pwAdapter) ? 1 : 0;
            flowId = Tha60210011ModuleClaHbceFlowIdFromPw(claModule, pwAdapter);
            }

        storeId = ThaHbceMemoryCellContentRemainedBitsGet(cellContent, NULL)[0];
        StandardCellInfoSet(pool, cellIndex, enabled, flowId, storeId);
        return;
        }

    m_Tha60210011HbceMemoryPoolMethods->ApplyCell(self, cellIndex);
    }

static eBool CellIsPrimary(ThaHbceMemoryPool self, uint32 absoluteCellIndex, uint32* groupId)
    {
    if (Tha60290022DeviceHwLogicOptimized(Device(self)))
        return cAtTrue;

    return m_Tha60210011HbceMemoryPoolMethods->CellIsPrimary(self, absoluteCellIndex, groupId);
    }

static void OverrideTha60210011HbceMemoryPool(ThaHbceMemoryPool self)
    {
    Tha60210011HbceMemoryPool pool = (Tha60210011HbceMemoryPool)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011HbceMemoryPoolMethods = mMethodsGet(pool);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011HbceMemoryPoolOverride, m_Tha60210011HbceMemoryPoolMethods, sizeof(m_Tha60210011HbceMemoryPoolOverride));

        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, ClaHbceLookingUpInformationCtrlPerCellOffset);
        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, CellIsPrimary);
        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, ApplyCell);
        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, CellInfoGet);
        }

    mMethodsSet(pool, &m_Tha60210011HbceMemoryPoolOverride);
    }

static void OverrideThaHbceMemoryPool(ThaHbceMemoryPool self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceMemoryPoolMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceMemoryPoolOverride, mMethodsGet(self), sizeof(m_ThaHbceMemoryPoolOverride));

        mMethodOverride(m_ThaHbceMemoryPoolOverride, MaxNumCells);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellEnable);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellInit);
        }

    mMethodsSet(self, &m_ThaHbceMemoryPoolOverride);
    }

static void Override(ThaHbceMemoryPool self)
    {
    OverrideThaHbceMemoryPool(self);
    OverrideTha60210011HbceMemoryPool(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022HbceMemoryPool);
    }

ThaHbceMemoryPool Tha60290022HbceMemoryPoolObjectInit(ThaHbceMemoryPool self, ThaHbce hbce)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011HbceMemoryPoolObjectInit(self, hbce) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbceMemoryPool Tha60290022HbceMemoryPoolNew(ThaHbce hbce)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceMemoryPool newPool = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPool == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022HbceMemoryPoolObjectInit(newPool, hbce);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290022HbceV3.c
 *
 * Created Date: Mar 13, 2018
 *
 * Description : HBCE implementation for HW optimization Version 3 (fpga 5.x)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../../../default/cla/hbce/ThaHbceEntry.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "./Tha60290022HbceInternal.h"
#include "../Tha60290022ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaHbceMethods         m_ThaHbceOverride;
static tTha60210011HbceMethods m_Tha60210011HbceOverride;

static const tThaHbceMethods *m_ThaHbceMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaHbceMemoryPool MemoryPoolCreate(ThaHbce self)
    {
    return Tha60290022HbceMemoryPoolV3New(self);
    }

static uint32 NumCellPerEntry(ThaHbce self)
    {
    AtUnused(self);
    return cNumberOfCellPerEntryV3;
    }

static uint32 NumEntryBlocks(ThaHbce self)
    {
    AtUnused(self);
    return cNumberOfEntryBlockV3;
    }

static uint32 MaxNumHashEntries(ThaHbce self)
    {
    AtUnused(self);
    return cNumberOfEntryPerBlockV3;
    }

static uint32 MaxNumEntries(ThaHbce self)
    {
    return MaxNumHashEntries(self);
    }

static uint8 NumHashCollisionInOneHashIndex(Tha60210011Hbce self)
    {
    return (uint8)(cNumberOfEntryBlockV3 * NumCellPerEntry((ThaHbce)self));
    }

static uint8  NumHashCollisionInOneExtraPointer(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 NumExtraMemoryPointer(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet HashEntryTabCtrlApply(Tha60210011Hbce self, uint32 entryIndex)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    return cAtOk;
    }

static void EntryInitOnPage(ThaHbce self, uint32 entryIndex, uint8 pageId)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    AtUnused(pageId);
    }

static uint32 NumCellPerBlock(ThaHbce self)
    {
    return NumCellPerEntry(self) * MaxNumHashEntries(self);
    }

static uint32 CellLocalIndex(ThaHbce self, uint32 cellIndex)
    {
    uint32 indexInOneBlock = cellIndex % NumCellPerBlock(self);
    uint32 localInOneHash = indexInOneBlock % NumCellPerEntry(self);

    return localInOneHash;
    }

static uint32 CellIndexToHwBlock(ThaHbce self, uint32 cellIndex)
    {
    return cellIndex / NumCellPerBlock(self);
    }

static uint32 CellIndexToHwHashIndex(ThaHbce self, uint32 cellIndex)
    {
    uint32 cellInOneBlock = cellIndex % NumCellPerBlock(self);
    return cellInOneBlock / NumCellPerEntry(self);
    }

static uint32 MaxNumCell(ThaHbce self)
    {
    return NumCellPerBlock(self) * cNumberOfEntryBlockV3;
    }

static uint32 CellIndex(ThaHbce self, uint32 hashEntryIndexPerBlock, uint32 localCellIndexPerEntry, uint32 blockIndex)
    {
    return (uint32)(blockIndex * NumCellPerBlock(self)) + (hashEntryIndexPerBlock * NumCellPerEntry(self)) + localCellIndexPerEntry;
    }

static uint32 FreeCellIndexInHashCollisionPool(ThaHbce self, ThaHbceMemoryPool memPool, uint32 hashIndexPerBlock)
    {
    uint32 cellIndexInOneEntry, blockIndex, cellIndex;
    for (cellIndexInOneEntry = 0; cellIndexInOneEntry < NumCellPerEntry(self); cellIndexInOneEntry++)
        {
        for (blockIndex = 0; blockIndex < NumEntryBlocks(self); blockIndex++)
            {
            cellIndex = CellIndex(self, hashIndexPerBlock, cellIndexInOneEntry, blockIndex);
            if (Tha60210011HbceMemoryCellIsUsed(ThaHbceMemoryPoolCellAtIndex(memPool, cellIndex)) == cAtFalse)
                return cellIndex;
            }
        }

    return cInvalidUint32;
    }

static ThaHbceMemoryCell AllocateCellForPw(ThaHbce self, ThaPwHeaderController controller, uint32 hashIndexPerBlock)
    {
    uint32 cellIndex;
    ThaHbceEntry hashEntry = ThaHbceEntryAtIndex(self, hashIndexPerBlock);
    ThaHbceMemoryPool memoryPool = ThaHbceMemoryPoolGet(self);
    AtPwPsn psn = ThaPwHeaderControllerPsnGet(controller);

    /* Check to see if there is existing pw that has same label as adding pw, if yes, return NULL */
    if (ThaHbceNewPwLabelExistInHashEntry(self, controller, psn, hashEntry))
        return NULL;

    cellIndex = FreeCellIndexInHashCollisionPool(self, memoryPool, hashIndexPerBlock);

    if (cellIndex == cInvalidUint32)
        return NULL;

    return ThaHbceMemoryPoolUseCell(memoryPool, cellIndex);
    }

static ThaHbceMemoryCell SearchCellForPw(ThaHbce self, ThaPwAdapter pwAdapter, ThaHbceEntry hashEntry)
    {
    ThaHbceMemoryPool memoryPool = ThaHbceMemoryPoolGet(self);
    ThaHbceMemoryCellContent content = NULL;
    uint32 cellIndex, flow_i;
    uint32 numFlows = ThaHbceEntryNumFlowsGet(hashEntry);

    for (flow_i = 0; flow_i < numFlows; flow_i++)
        {
        content = ThaHbceEntryCellContentAtIndex(hashEntry, flow_i);
        cellIndex = ThaHbceMemoryCellIndexGet(ThaHbceMemoryCellContentCellGet(content));
        if (Tha60210011HbceMemoryPoolCellHwFlowIdGet(memoryPool, cellIndex) == AtChannelIdGet((AtChannel)pwAdapter))
            break;
        }

    return ThaHbceMemoryCellContentCellGet(content);
    }

static uint32 HashCellIndex(Tha60210011Hbce self, uint32 hashEntryIndex, uint32 localCellIndex)/*localCellIndex is range from 0 to numCollison per hash*/
    {
    ThaHbce hbce = (ThaHbce)self;
    uint32 localCellPerEntryPerBlock = localCellIndex % NumCellPerEntry(hbce);
    uint32 blockIndex = localCellIndex / NumCellPerEntry(hbce);
    uint32 index = CellIndex(hbce, hashEntryIndex, localCellPerEntryPerBlock, blockIndex);
    return index;
    }

static void OverrideTha60210011Hbce(ThaHbce self)
    {
    Tha60210011Hbce hbce = (Tha60210011Hbce)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011HbceOverride, mMethodsGet(hbce), sizeof(m_Tha60210011HbceOverride));
        mMethodOverride(m_Tha60210011HbceOverride, NumHashCollisionInOneHashIndex);
        mMethodOverride(m_Tha60210011HbceOverride, NumHashCollisionInOneExtraPointer);
        mMethodOverride(m_Tha60210011HbceOverride, NumExtraMemoryPointer);
        mMethodOverride(m_Tha60210011HbceOverride, HashEntryTabCtrlApply);
        mMethodOverride(m_Tha60210011HbceOverride, HashCellIndex);
        }

    mMethodsSet(hbce, &m_Tha60210011HbceOverride);
    }

static void OverrideThaHbce(ThaHbce self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceOverride, m_ThaHbceMethods, sizeof(m_ThaHbceOverride));

        mMethodOverride(m_ThaHbceOverride, MaxNumEntries);
        mMethodOverride(m_ThaHbceOverride, MemoryPoolCreate);
        mMethodOverride(m_ThaHbceOverride, EntryInitOnPage);
        mMethodOverride(m_ThaHbceOverride, AllocateCellForPw);
        mMethodOverride(m_ThaHbceOverride, SearchCellForPw);
        }

    mMethodsSet(self, &m_ThaHbceOverride);
    }

static void Override(ThaHbce self)
    {
    OverrideThaHbce(self);
    OverrideTha60210011Hbce(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022HbceV3);
    }

static ThaHbce ObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022HbceObjectInit(self, hbceId, claModule, core) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbce Tha60290022HbceV3New(uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbce newHbce = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newHbce == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHbce, hbceId, claModule, core);
    }

uint32 Tha60290022HbceV3CellLocalIndex(ThaHbce self, uint32 cellIndex)
    {
    return CellLocalIndex(self, cellIndex);
    }

uint32 Tha60290022HbceV3CellIndexToHwBlock(ThaHbce self, uint32 cellIndex)
    {
    return CellIndexToHwBlock(self, cellIndex);
    }

uint32 Tha60290022HbceV3CellIndexToHwHashIndex(ThaHbce self, uint32 cellIndex)
    {
    return CellIndexToHwHashIndex(self, cellIndex);
    }

uint32 Tha60290022HbceV3MaxNumCell(ThaHbce self)
    {
    return MaxNumCell(self);
    }

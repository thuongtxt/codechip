/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60290022HbceInternal.h
 * 
 * Created Date: Mar 13, 2018
 *
 * Description : HBCE internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022HBCEINTERNAL_H_
#define _THA60290022HBCEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210011/cla/hbce/Tha60210011HbceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022Hbce
    {
    tTha60210011Hbce super;
    }tTha60290022Hbce;

typedef struct tTha60290022HbceV3
    {
    tTha60290022Hbce super;
    }tTha60290022HbceV3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHbce Tha60290022HbceObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core);
uint32 Tha60290022HbceV3CellLocalIndex(ThaHbce self, uint32 cellIndex);
uint32 Tha60290022HbceV3CellIndexToHwBlock(ThaHbce self, uint32 cellIndex);
uint32 Tha60290022HbceV3CellIndexToHwHashIndex(ThaHbce self, uint32 cellIndex);
uint32 Tha60290022HbceV3MaxNumCell(ThaHbce self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022HBCEINTERNAL_H_ */


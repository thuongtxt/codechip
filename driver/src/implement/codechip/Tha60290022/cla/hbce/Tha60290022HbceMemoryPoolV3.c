/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290022HbceMemoryPoolV3.c
 *
 * Created Date: Mar 13, 2018
 *
 * Description : HBCE memory pool implementation for HW optimization version 3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCellContent.h"
#include "../../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "../Tha60290022ModuleClaInternal.h"
#include "./Tha60290022HbceMemoryPoolInternal.h"
#include "../Tha60290022ModuleClaReg.h"
#include "./Tha60290022HbceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegClaBaseAddress                   0x0600000
#define cThaRegClaHbceLookingUpInformationCtrl  cAf6Reg_cla_hbce_lkup_info_Base + cThaRegClaBaseAddress

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022HbceMemoryPoolV3
    {
    tTha60290022HbceMemoryPool super;
    }tTha60290022HbceMemoryPoolV3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override  */
static tThaHbceMemoryPoolMethods         m_ThaHbceMemoryPoolOverride;
static tTha60210011HbceMemoryPoolMethods m_Tha60210011HbceMemoryPoolOverride;

/* Save super implementation */
static const tThaHbceMemoryPoolMethods         *m_ThaHbceMemoryPoolMethods         = NULL;
static const tTha60210011HbceMemoryPoolMethods *m_Tha60210011HbceMemoryPoolMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumCells(ThaHbceMemoryPool self)
    {
    return Tha60290022HbceV3MaxNumCell(ThaHbceMemoryPoolHbceGet(self));
    }

static uint32 ClaHbceLookingUpInformationCtrlPerCellOffset(Tha60210011HbceMemoryPool self, uint32 cellIndex, uint32 blockIndex, uint32 indexInBlock)
    {
    AtUnused(self);
    AtUnused(cellIndex);
    return (blockIndex * 0x10000) + indexInBlock;
    }

static uint32 ClaHbceLookingUpInformationCtrlPerCell(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    uint32 blockIndex   = Tha60290022HbceV3CellIndexToHwBlock(ThaHbceMemoryPoolHbceGet(self), cellIndex);
    uint32 indexInBlock = Tha60290022HbceV3CellIndexToHwHashIndex(ThaHbceMemoryPoolHbceGet(self), cellIndex);
    uint32 offset = Tha60290011HbceMemoryPoolClaHbceLookingUpInformationCtrlPerCellOffset((Tha60210011HbceMemoryPool)self, cellIndex, blockIndex, indexInBlock);
    return cThaRegClaHbceLookingUpInformationCtrl + offset;
    }

static AtIpCore Core(ThaHbceMemoryPool self)
    {
    return ThaHbceCoreGet(self->hbce);
    }

static eAtRet CellInfoSet(ThaHbceMemoryPool self, uint32 cellIndex, eBool enable, uint32 flowId, uint32 storeId)
    {
    uint32 localIndex = Tha60290022HbceV3CellLocalIndex(ThaHbceMemoryPoolHbceGet(self), cellIndex);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 regAddr = Tha60210011ClaHbceLookingUpInformationCtrlPerCell(self, cellIndex);
    Tha60210011ModuleCla claModule = (Tha60210011ModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 hwEnabled = enable ? 1 : 0;

    mModuleHwLongRead(claModule, regAddr, longReg, cThaLongRegMaxSize, Core(self));

    switch (localIndex)
        {
        case 0:
            mRegFieldSet(longReg[0], cAf6_cla_hbce_lkup_info_CLAHbceStoreID0_, storeId);
            mRegFieldSet(longReg[0], cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb0_, hwEnabled);
            mRegFieldSet(longReg[0], cAf6_cla_hbce_lkup_info_CLAHbceFlowID0_, flowId);
            break;
        case 1:
            mLongFieldSet(longReg, 0, cAf6_cla_hbce_lkup_info_CLAHbceStoreID1_, storeId);
            mRegFieldSet(longReg[1], cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb1_, hwEnabled);
            mRegFieldSet(longReg[1], cAf6_cla_hbce_lkup_info_CLAHbceFlowID1_, flowId);
            break;
        case 2:
            mRegFieldSet(longReg[1], cAf6_cla_hbce_lkup_info_CLAHbceStoreID2_, storeId);
            mRegFieldSet(longReg[1], cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb2_, hwEnabled);
            mLongFieldSet(longReg, 1, cAf6_cla_hbce_lkup_info_CLAHbceFlowID2_, flowId);
            break;
        default:
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "Invalid local index (%u)\r\n", localIndex);
            return cAtErrorInvalidOperation;
        }

    mModuleHwLongWrite(claModule, regAddr, longReg, cThaLongRegMaxSize, Core(self));

    return cAtOk;
    }

static eAtRet CellInfoGet(Tha60210011HbceMemoryPool self, uint32 cellIndex, uint32 *flowId, eBool *enable, uint32 *storeId)
    {
    ThaHbceMemoryPool pool = (ThaHbceMemoryPool)self;
    uint32 localIndex = Tha60290022HbceV3CellLocalIndex(ThaHbceMemoryPoolHbceGet(pool), cellIndex);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 regAddr = Tha60210011ClaHbceLookingUpInformationCtrlPerCell(pool, cellIndex);
    Tha60210011ModuleCla claModule = (Tha60210011ModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);

    mModuleHwLongRead(claModule, regAddr, longReg, cThaLongRegMaxSize, Core(pool));

    switch (localIndex)
        {
        case 0:
            mAssign(storeId, mRegField(longReg[0], cAf6_cla_hbce_lkup_info_CLAHbceStoreID0_));
            mAssign(enable , (longReg[0] & cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb0_Mask) ? cAtTrue : cAtFalse);
            mAssign(flowId , mRegField(longReg[0], cAf6_cla_hbce_lkup_info_CLAHbceFlowID0_));
            break;
        case 1:
            mAssign(storeId, mLongField(longReg, 0, cAf6_cla_hbce_lkup_info_CLAHbceStoreID1_));
            mAssign(enable , (longReg[1] & cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb1_Mask) ? cAtTrue : cAtFalse);
            mAssign(flowId , mRegField(longReg[1], cAf6_cla_hbce_lkup_info_CLAHbceFlowID1_));
            break;
        case 2:
            mAssign(storeId, mRegField(longReg[1], cAf6_cla_hbce_lkup_info_CLAHbceStoreID2_));
            mAssign(enable , (longReg[1] & cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb2_Mask) ? cAtTrue : cAtFalse);
            mAssign(flowId , mLongField(longReg, 1, cAf6_cla_hbce_lkup_info_CLAHbceFlowID2_));
            break;
        default:
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "Invalid local index (%u)\r\n", localIndex);
            return cAtErrorInvalidOperation;
        }

    return cAtOk;
    }

static eAtRet CellEnable(ThaHbceMemoryPool self, ThaHbceMemoryCell cell, eBool enable)
    {
    uint32 cellIndex = ThaHbceMemoryCellIndexGet(cell);
    uint32 flowId, storeId;
    eAtRet ret = cAtOk;

    Tha60210011HbceMemoryPoolCellInfoGet(self, cellIndex, &flowId, NULL, &storeId);
    ret |= CellInfoSet(self, cellIndex, enable, flowId, storeId);
    ret |= ThaHbceMemoryCellContentEnable(ThaHbceMemoryCellContentGet(cell), enable);

    return ret;
    }

static eAtRet CellInit(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    eBool enabled = cAtFalse;
    uint32 flowId = 0, storeId = cAf6_cla_hbce_lkup_info_CLAHbceStoreID0_Mask >> cAf6_cla_hbce_lkup_info_CLAHbceStoreID0_Shift;

    return CellInfoSet(self, cellIndex, enabled, flowId, storeId);
    }

static void ApplyCell(Tha60210011HbceMemoryPool self, uint32 cellIndex)
    {
    ThaHbceMemoryPool pool = (ThaHbceMemoryPool)self;

    ThaHbceMemoryCellContent cellContent = ThaHbceMemoryCellContentGet(ThaHbceMemoryPoolCellAtIndex(pool, cellIndex));
    uint32 flowId = 0;
    ThaPwAdapter pwAdapter;
    ThaPwHeaderController headerController;
    uint32 storeId;
    eBool enabled = cAtFalse;
    Tha60210011ModuleCla claModule = (Tha60210011ModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);

    if (cellContent == NULL)
        return;

    headerController = ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent);
    pwAdapter = ThaPwHeaderControllerAdapterGet(headerController);
    if (pwAdapter)
        {
        enabled = AtChannelIsEnabled((AtChannel)pwAdapter) ? 1 : 0;
        flowId = Tha60210011ModuleClaHbceFlowIdFromPw(claModule, pwAdapter);
        }

    storeId = ThaHbceMemoryCellContentRemainedBitsGet(cellContent, NULL)[0];
    CellInfoSet(pool, cellIndex, enabled, flowId, storeId);
    return;
    }

static eBool CellIsPrimary(ThaHbceMemoryPool self, uint32 absoluteCellIndex, uint32* groupId)
    {
    AtUnused(self);
    AtUnused(absoluteCellIndex);
    AtUnused(groupId);
    return cAtTrue;
    }

static void OverrideTha60210011HbceMemoryPool(ThaHbceMemoryPool self)
    {
    Tha60210011HbceMemoryPool pool = (Tha60210011HbceMemoryPool)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011HbceMemoryPoolMethods = mMethodsGet(pool);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011HbceMemoryPoolOverride, m_Tha60210011HbceMemoryPoolMethods, sizeof(m_Tha60210011HbceMemoryPoolOverride));

        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, CellIsPrimary);
        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, ApplyCell);
        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, CellInfoGet);
        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, ClaHbceLookingUpInformationCtrlPerCellOffset);
        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, ClaHbceLookingUpInformationCtrlPerCell);
        }

    mMethodsSet(pool, &m_Tha60210011HbceMemoryPoolOverride);
    }

static void OverrideThaHbceMemoryPool(ThaHbceMemoryPool self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceMemoryPoolMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceMemoryPoolOverride, mMethodsGet(self), sizeof(m_ThaHbceMemoryPoolOverride));

        mMethodOverride(m_ThaHbceMemoryPoolOverride, MaxNumCells);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellEnable);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellInit);
        }

    mMethodsSet(self, &m_ThaHbceMemoryPoolOverride);
    }

static void Override(ThaHbceMemoryPool self)
    {
    OverrideThaHbceMemoryPool(self);
    OverrideTha60210011HbceMemoryPool(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022HbceMemoryPoolV3);
    }

static ThaHbceMemoryPool ObjectInit(ThaHbceMemoryPool self, ThaHbce hbce)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022HbceMemoryPoolObjectInit(self, hbce) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbceMemoryPool Tha60290022HbceMemoryPoolV3New(ThaHbce hbce)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceMemoryPool newPool = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPool == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPool, hbce);
    }

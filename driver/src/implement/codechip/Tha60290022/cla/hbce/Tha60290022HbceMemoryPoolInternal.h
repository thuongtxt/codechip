/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60290022HbceMemoryPoolInternal.h
 * 
 * Created Date: Mar 13, 2018
 *
 * Description : HBCE memory pool internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022HBCEMEMORYPOOLINTERNAL_H_
#define _THA60290022HBCEMEMORYPOOLINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210011/cla/hbce/Tha60210011HbceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mAssign(pointer, value)                                                \
    do                                                                         \
        {                                                                      \
        if (pointer)                                                           \
            *pointer = value;                                                  \
        }while(0)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022HbceMemoryPool
    {
    tTha60210011HbceMemoryPool super;
    }tTha60290022HbceMemoryPool;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHbceMemoryPool Tha60290022HbceMemoryPoolObjectInit(ThaHbceMemoryPool self, ThaHbce hbce);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022HBCEMEMORYPOOLINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290022ModuleCla.c
 *
 * Created Date: Apr 27, 2017
 *
 * Description : CLA module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "./Tha60290022ModuleClaInternal.h"
#include "../man/Tha60290022Device.h"
#include "Tha60290022ModuleClaReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegClaStickyEthTypeOk(portid)  (cBit16 << (portid))
#define cRegClaStickyTypeOk             cBit7
#define cRegClaStickyMacErr             cBit8
#define cRegClaStickyPhyErr             cBit9
#define cRegClaStickyRtpPtErr           cBit10

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleClaMethods         m_ThaModuleClaOverride;
static tTha60210011ModuleClaMethods m_Tha60210011ModuleClaOverride;

/* Save super implementation */
static const tAtModuleMethods             *m_AtModuleMethods             = NULL;
static const tTha60210011ModuleClaMethods *m_Tha60210011ModuleClaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HbceFlowIdMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Mask;
    }

static uint32 HbceFlowIdShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Shift;
    }

static uint32 HbceStoreIdMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Mask;
    }

static uint32 HbceStoreIdShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Shift;
    }

static uint32 HbceFlowEnableMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Mask;
    }

static uint32 HbceFlowEnableShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Shift;
    }

static uint32 ClaStickyMacErrorMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cRegClaStickyMacErr;
    }

static uint32 ClaStickyPhyErrorMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cRegClaStickyPhyErr;
    }

static uint32 ClaStickyRtpPtErrorMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cRegClaStickyRtpPtErr;
    }

static void DebugErrorPrint(uint32 sticky, const char *debugName)
    {
    eAtSevLevel color = sticky ? cSevInfo: cSevCritical;
    AtPrintc(cSevNormal, "%-30s: ", debugName);
    AtPrintc(color, "%s\r\n", sticky ? "OK" : "ERROR");
    }

static void DebugShowClaEthTypeSticky(Tha60210011ModuleCla self)
    {
    uint32 pLongRegVal[cThaLongRegMaxSize];
    AtIpCore ipCore = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);

    mModuleHwLongRead(self, cEthernetSticky, pLongRegVal, cThaLongRegMaxSize, ipCore);
    AtPrintc(cSevInfo, "ETH_TYPE OK \r\n");
    DebugErrorPrint((pLongRegVal[3] & cRegClaStickyEthTypeOk(0)), "  Internal Port #0");
    DebugErrorPrint((pLongRegVal[3] & cRegClaStickyEthTypeOk(1)), "  Internal Port #1");
    DebugErrorPrint((pLongRegVal[3] & cRegClaStickyEthTypeOk(2)), "  Internal Port #2");
    DebugErrorPrint((pLongRegVal[3] & cRegClaStickyEthTypeOk(3)), "  Internal Port #3");
    DebugErrorPrint((pLongRegVal[2] & cRegClaStickyTypeOk), "TYPE OK");
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return Tha60290022ClaPwControllerNew(self);
    }

static eBool HwOptimized(Tha60210011ModuleCla self)
    {
    return Tha60290022DeviceHwLogicOptimized(AtModuleDeviceGet((AtModule)self));
    }

static uint32 HbceFlowDirectMask(Tha60210011ModuleCla self)
    {
    if (HwOptimized(self))
        return 0; /* 0 to remove this field */

    return m_Tha60210011ModuleClaMethods->HbceFlowDirectMask(self);
    }

static uint32 HbceFlowDirectShift(Tha60210011ModuleCla self)
    {
    if (HwOptimized(self))
        return 0; /* 0 to remove this field */

    return m_Tha60210011ModuleClaMethods->HbceFlowDirectShift(self);
    }

static uint32 HbceGroupWorkingMask(Tha60210011ModuleCla self)
    {
    if (HwOptimized(self))
        return 0; /* 0 to remove this field */

    return m_Tha60210011ModuleClaMethods->HbceGroupWorkingMask(self);
    }

static uint32 HbceGroupWorkingShift(Tha60210011ModuleCla self)
    {
    if (HwOptimized(self))
        return 0; /* 0 to remove this field */

    return m_Tha60210011ModuleClaMethods->HbceGroupWorkingShift(self);
    }

static uint32 HbceGroupIdFlowMask1(Tha60210011ModuleCla self)
    {
    if (HwOptimized(self))
        return 0; /* 0 to remove this field */

    return m_Tha60210011ModuleClaMethods->HbceGroupIdFlowMask1(self);
    }

static uint32 HbceGroupIdFlowShift1(Tha60210011ModuleCla self)
    {
    if (HwOptimized(self))
        return 0; /* 0 to remove this field */

    return m_Tha60210011ModuleClaMethods->HbceGroupIdFlowShift1(self);
    }

static uint32 HbceGroupIdFlowMask2(Tha60210011ModuleCla self)
    {
    if (HwOptimized(self))
        return 0; /* 0 to remove this field */

    return m_Tha60210011ModuleClaMethods->HbceGroupIdFlowMask2(self);
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    const char *name = AtInternalRamName(ram);

    if (name == NULL)
        return m_AtModuleMethods->InternalRamIsReserved(self, ram);

    if (AtStrstr(name, "Classify HBCE Looking Up Information Control 1, Group 1") ||
        AtStrstr(name, "Classify HBCE Looking Up Information Control 1, Group 2") ||
        AtStrstr(name, "Classify HBCE Looking Up Information Control 1, Group 3"))
        return cAtTrue;

    return m_AtModuleMethods->InternalRamIsReserved(self, ram);
    }

static uint32 HbceGroupIdFlowShift2(Tha60210011ModuleCla self)
    {
    if (HwOptimized(self))
        return 0; /* 0 to remove this field */

    return m_Tha60210011ModuleClaMethods->HbceGroupIdFlowShift2(self);
    }

static void OverrideAtModule(AtModule self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideTha60210011ModuleCla(AtModule self)
    {
    Tha60210011ModuleCla claModule = (Tha60210011ModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleClaMethods = mMethodsGet(claModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleClaOverride, mMethodsGet(claModule), sizeof(m_Tha60210011ModuleClaOverride));

        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowIdMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowIdShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowEnableMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowEnableShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceStoreIdMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceStoreIdShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowDirectMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowDirectShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupWorkingMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupWorkingShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupIdFlowMask1);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupIdFlowShift1);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupIdFlowMask2);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupIdFlowShift2);
        mMethodOverride(m_Tha60210011ModuleClaOverride, DebugShowClaEthTypeSticky);
        mMethodOverride(m_Tha60210011ModuleClaOverride, ClaStickyMacErrorMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, ClaStickyPhyErrorMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, ClaStickyRtpPtErrorMask);
        }

    mMethodsSet(claModule, &m_Tha60210011ModuleClaOverride);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleCla(self);
    OverrideTha60210011ModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleCla);
    }

AtModule Tha60290022ModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290022ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModuleClaObjectInit(newModule, device);
    }

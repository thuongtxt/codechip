/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60290022EthPortSgmiiPktAnalyzer.c
 *
 * Created Date: Jan 4, 2017
 *
 * Description : SGMII Port Analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/pktanalyzer/ThaEthPortPktAnalyzerInternal.h"
#include "../../Tha60290021/pktanalyzer/Tha6029SgmiiEthPacket.h"
#include "../../Tha60290021/pw/Tha60290021ModulePw.h"
#include "Tha60290022ModulePktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumDataLocation 1023
#define cAf6Reg_ramdumtypecfg_Base 0xC00
#define cAf6_DirectionDump_Mask    cBit0
#define cAf6_DirectionDump_Shift   0

#define cAf6Reg_ramdumpdata_Base 0x800
#define cAf6_DumpSop_Mask        cBit18
#define cAf6_DumpSop_Shift       18
#define cAf6_DumpEop_Mask        cBit17
#define cAf6_DumpEop_Shift       17
#define cAf6_DumpNop_Mask        cBit16
#define cAf6_DumpNop_Shift       16
#define cAf6_DumpData_Mask       cBit15_0
#define cAf6_DumpData_Shift      0
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022EthPortSgmiiPktAnalyzerV2
    {
    tThaEthPortPktAnalyzerV2 super;
    }tTha60290022EthPortSgmiiPktAnalyzerV2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods  m_AtPktAnalyzerOverride;
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;


/* Save super implementation */
static const tAtPktAnalyzerMethods  *m_AtPktAnalyzerMethods  = NULL;
static const tThaPktAnalyzerMethods *m_ThaPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaEthPort EthPort(AtPktAnalyzer self)
    {
    return (ThaEthPort) AtPktAnalyzerChannelGet(self);
    }

static uint32 AddressWithLocalAddress(AtPktAnalyzer self, uint32 localAddress)
    {
    return ThaEthPortMacBaseAddress(EthPort(self)) + localAddress;
    }

static uint32 FirstAddressOfBuffer(ThaPktAnalyzer self)
    {
    return AddressWithLocalAddress((AtPktAnalyzer)self, cAf6Reg_ramdumpdata_Base);
    }

static uint32 LastAddressOfBuffer(ThaPktAnalyzer self)
    {
    return FirstAddressOfBuffer(self) + cNumDataLocation;
    }

static uint32 DirectionControlAddress(ThaPktAnalyzer self)
    {
    return AddressWithLocalAddress((AtPktAnalyzer)self, cAf6Reg_ramdumtypecfg_Base);
    }

static void DumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    static const uint32 cDumpTx          = 0x1;
    static const uint32 cDumpRx          = 0;
    uint32 address = DirectionControlAddress(self);

    if (pktDumpMode == cThaPktAnalyzerDumpGbeTx)
        ThaPktAnalyzerWrite(self, address, cDumpTx);
    if (pktDumpMode == cThaPktAnalyzerDumpGbeRx)
        ThaPktAnalyzerWrite(self, address, cDumpRx);
    }

static uint32 DumpModeHw2Sw(uint32 pktDumpMode)
    {
    if (pktDumpMode == 0) return cThaPktAnalyzerDumpGbeRx;
    if (pktDumpMode == 1) return cThaPktAnalyzerDumpGbeTx;

    return cThaPktAnalyzerDumpGbeTx;
    }

static uint32 DumpModeGet(ThaPktAnalyzer self)
    {
    uint32 address = DirectionControlAddress(self);
    uint32 regVal = ThaPktAnalyzerRead(self, address);
    uint32  pktDumpMode;

    mFieldGet(regVal, cAf6_DirectionDump_Mask, cAf6_DirectionDump_Shift, uint32, &pktDumpMode);
    return DumpModeHw2Sw(pktDumpMode);
    }

static eBool IsStartOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    uint32 regVal = ThaPktAnalyzerRead(self, address);
    AtUnused(data);
    return mRegField(regVal, cAf6_DumpSop_) ? cAtTrue : cAtFalse;
    }

static eBool IsEndOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    uint32 regVal = ThaPktAnalyzerRead(self, address);
    AtUnused(data);
    return mRegField(regVal, cAf6_DumpEop_) ? cAtTrue : cAtFalse;
    }

static uint8 EntryRead(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    uint8 nop;
    const uint8 numBytePerDword = 4;
    const uint8 numBitPerByte = 8;

    data[0] = ThaPktAnalyzerRead(self, address);

    nop = (uint8)(mRegField(data[0], cAf6_DumpNop_) + 1);
    data[0] = data[0] >> (numBitPerByte * (2 - nop));
    data[0] = data[0] << (numBitPerByte * (numBytePerDword - nop));
    return nop;
    }

static uint8 MetaDwordIndex(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return 1;
    }

static AtModulePw PwModule(AtPktAnalyzer self)
    {
    AtDevice device = AtChannelDeviceGet(AtPktAnalyzerChannelGet(self));
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    AtPacket newPacket = Tha6029SgmiiEthPacketNew(self, data, length, cAtPacketCacheModeCacheData);
    AtPw pw;

    AtUnused(direction);

    pw = ThaModulePwKBytePwGet((ThaModulePw)PwModule(self));
    if (pw)
        Tha6029SgmiiEthPacketKbyteEthTypeSet(newPacket, Tha6029PwKbytePwExpectedEthTypeGet(pw));

    Tha6029SgmiiEthPacketDccEthTypeSet(newPacket, cThaDccPwEthTypeDefault);

    return newPacket;
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPktAnalyzerMethods = mMethodsGet(analyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, m_ThaPktAnalyzerMethods, sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, FirstAddressOfBuffer);
        mMethodOverride(m_ThaPktAnalyzerOverride, LastAddressOfBuffer);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeSet);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeGet);
        mMethodOverride(m_ThaPktAnalyzerOverride, IsStartOfPacket);
        mMethodOverride(m_ThaPktAnalyzerOverride, IsEndOfPacket);
        mMethodOverride(m_ThaPktAnalyzerOverride, EntryRead);
        mMethodOverride(m_ThaPktAnalyzerOverride, MetaDwordIndex);
        }

    mMethodsSet(analyzer, &m_ThaPktAnalyzerOverride);
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022EthPortSgmiiPktAnalyzerV2);
    }

static AtPktAnalyzer ObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortPktAnalyzerV2ObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60290022EthPortSgmiiPktAnalyzerV2New(AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPktAnalyzer, port);
    }

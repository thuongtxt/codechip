/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PktAnalyzer
 * 
 * File        : Tha60290022PktAnalyzer.h
 * 
 * Created Date: Jan 4, 2018
 *
 * Description : PktAnalyzer Interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PKTANALYZER_H_
#define _THA60290022PKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "AtPktAnalyzer.h"
#include "AtModulePktAnalyzer.h"
#include "AtEthPort.h"
#include "AtHdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer Tha60290022EthPortSgmiiPktAnalyzerV2New(AtEthPort port);
AtModulePktAnalyzer Tha60290022ModulePktAnalyzerNew(AtDevice device);
AtPktAnalyzer Tha60290022DccHdlcPktAnalyzerNew(AtEthPort port, AtHdlcChannel hdlcChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PKTANALYZER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60290022ModulePktAnalyzer.c
 *
 * Created Date: Jan 4, 2017
 *
 * Description : Packet Analyzer of 60290022
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/pktanalyzer/Tha60290021ModulePktAnalyzerInternal.h"
#include "../../Tha60290021/eth/Tha60290021ModuleEth.h"
#include "../man/Tha60290022Device.h"
#include "Tha60290022ModulePktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022ModulePktAnalyzer
    {
    tTha60290021ModulePktAnalyzer super;
    }tTha60290022ModulePktAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePktAnalyzerMethods m_AtModulePktAnalyzerOverride;

/* Save super implementation */
static const tAtModulePktAnalyzerMethods *m_AtModulePktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SgmiiPktAnalyzerV2IsSupported(AtModulePktAnalyzer self, AtEthPort port)
    {
    if (Tha60290021EthPortIsSgmiiPort(port) && Tha60290022DeviceDccV2IsSupported(AtModuleDeviceGet((AtModule)self)))
        return cAtTrue;

    return cAtFalse;
    }

static AtPktAnalyzer EthPortPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthPort port)
    {
    if (SgmiiPktAnalyzerV2IsSupported(self, port))
        return Tha60290022EthPortSgmiiPktAnalyzerV2New(port);

    return m_AtModulePktAnalyzerMethods->EthPortPktAnalyzerCreate(self, port);
    }

static AtEthPort DccSgmiiPort(AtModulePktAnalyzer self)
    {
    AtModuleEth moduleEth = (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEth);
    return Tha60290021ModuleEthSgmiiDccKbytePortGet(moduleEth);
    }

static AtPktAnalyzer HdlcChannelPktAnalyzerCreate(AtModulePktAnalyzer self, AtHdlcChannel channel)
    {
    if (Tha60290022DeviceDccTxFcsMsbIsSupported(AtModuleDeviceGet((AtModule)self)))
        {
        AtEthPort ethPort = DccSgmiiPort(self);
        return Tha60290022DccHdlcPktAnalyzerNew(ethPort, channel);
        }

    return NULL;
    }

static void OverrideAtModulePktAnalyzer(AtModulePktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtModulePktAnalyzerOverride));

        mMethodOverride(m_AtModulePktAnalyzerOverride, EthPortPktAnalyzerCreate);
        mMethodOverride(m_AtModulePktAnalyzerOverride, HdlcChannelPktAnalyzerCreate);
        }

    mMethodsSet(self, &m_AtModulePktAnalyzerOverride);
    }

static void Override(AtModulePktAnalyzer self)
    {
    OverrideAtModulePktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModulePktAnalyzer);
    }

static AtModulePktAnalyzer ObjectInit(AtModulePktAnalyzer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModulePktAnalyzerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePktAnalyzer Tha60290022ModulePktAnalyzerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePktAnalyzer newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

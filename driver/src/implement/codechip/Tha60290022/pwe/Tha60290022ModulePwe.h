/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60290022ModulePwe.h
 * 
 * Created Date: Apr 26, 2017
 *
 * Description : PWE module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPWE_H_
#define _THA60290022MODULEPWE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pwe/ThaModulePwe.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60290022ModulePweHwFlushPla(ThaModulePwe self);

void Tha60290022ModulePwePwPlaDebug(ThaModulePwe self, AtPw pw);
void Tha60290022ModulePwePlaDebug(ThaModulePwe self);
void Tha60290022ModulePwePlaCacheUsageGet(ThaModulePwe self, tThaPweCache *cache);
void Tha60290022ModulePwePwOc192PlaDebug(ThaModulePwe self, AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPWE_H_ */


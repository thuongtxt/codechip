/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60290022ModulePwe.c
 *
 * Created Date: Apr 26, 2017
 *
 * Description : PWE module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "../man/Tha60290022Device.h"
#include "../ram/Tha60290022InternalRam.h"
#include "Tha60290022ModulePlaReg.h"
#include "Tha60290022ModulePlaDebugReg.h"
#include "Tha60290022ModulePwe.h"
#include "Tha60290022ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumOc48SlicesInOc96Slice 2
#define cNumPwsPerOc48Slice       2048

/*--------------------------- Macros -----------------------------------------*/
#define mTha60210011ModulePwe(pweModule) ((Tha60210011ModulePwe)pweModule)
#define mPlaPwAbsoluteOffset(self, pwAdapter) AtChannelIdGet((AtChannel)pwAdapter) + ThaModulePwePlaBaseAddress((ThaModulePwe)self)
#define mPlaOutHspwCtrlBase(self) mMethodsGet(mTha60210011ModulePwe(self))->PlaOutHspwCtrlBase(mTha60210011ModulePwe(self))

#define sTha60290022ModulePweRamLookupPWControlRegisterOC96 "Lookup PW  Control Register OC96"
#define mThis(self) ((Tha60290022ModulePwe)(self))
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/
static uint32 PlaOc192PldControl(ThaModulePwe self, AtPw pw);

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290022ModulePweMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModulePweMethods         m_ThaModulePweOverride;
static tThaModulePweV2Methods       m_ThaModulePweV2Override;
static tTha60210011ModulePweMethods m_Tha60210011ModulePweOverride;
static tTha60210051ModulePweMethods m_Tha60210051ModulePweOverride;

/* Super implementations */
static const tAtModuleMethods               *m_AtModuleMethods = NULL;
static const tThaModulePweMethods           *m_ThaModulePweMethods = NULL;
static const tTha60210011ModulePweMethods   *m_Tha60210011ModulePweMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsV3OptimizationSupported(Tha60210011ModulePwe self)
    {
    AtDevice device = (AtDevice)AtModuleDeviceGet((AtModule)self);
    return Tha60290022DeviceV3OptimizationIsSupported(device);
    }

static uint32 NumPws(ThaModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    return AtModulePwMaxPwsGet(pwModule);
    }

static uint32 Oc96SliceOffset(ThaModulePwe self, uint32 oc96Slice)
    {
    uint32 oc96factor = mMethodsGet(mThis(self))->HwOc96OffsetFactor(mThis(self));
    return oc96Slice * oc96factor;
    }

static uint32 LkPwCtrlOc48SliceOffset(ThaModulePwe self, uint32 localOc48Slice)
    {
    AtUnused(self);
    return localOc48Slice * 2048;
    }

static uint32 Oc48SliceV3Offset(ThaModulePwe self, uint32 localOc48Slice)
    {
    uint32 oc48factor = mMethodsGet(mThis(self))->HwOc48InOc96OffsetFactor(mThis(self));
    return localOc48Slice * oc48factor;
    }

static uint32 HwNumOc96Slices(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return 4;
    }

static void PlaOc96SliceFlush(ThaModulePwe self)
    {
    Tha60210011ModulePwe modulePwe = (Tha60210011ModulePwe)self;
    uint32 cNumOc96Slices = mMethodsGet(mThis(self))->HwNumOc96Slices(mThis(self));
    uint32 oc96Slice;
    uint32 baseAddress = ThaModulePwePlaBaseAddress(self);
    uint32 numHwPwInOc48 = mMethodsGet(mThis(self))->HwNumPwInOc48(mThis(self));
    uint32 pld_ctrl_base = mMethodsGet(modulePwe)->LoPayloadCtrl(modulePwe);
    uint32 add_rmv_pw_ctrl_base = mMethodsGet(modulePwe)->LoAddRemoveProtocolReg(modulePwe);

    for (oc96Slice = 0; oc96Slice < cNumOc96Slices; oc96Slice++)
        {
        uint32 oc96Offset = Oc96SliceOffset(self, oc96Slice);
        uint32 oc48Slice;
        uint32 pw_i;

        for (oc48Slice = 0; oc48Slice < cNumOc48SlicesInOc96Slice; oc48Slice++)
            {
            for (pw_i = 0; pw_i < numHwPwInOc48; pw_i++)
                {
                uint32 absoluteOffset;
                uint32 oc48Offset = Oc48SliceV3Offset(self, oc48Slice);

                absoluteOffset = oc96Offset + oc48Offset + pw_i + baseAddress;
                mModuleHwWrite(self, pld_ctrl_base + absoluteOffset, 0);
                mModuleHwWrite(self, add_rmv_pw_ctrl_base + absoluteOffset, 0);

                if (!IsV3OptimizationSupported((Tha60210011ModulePwe)self))
                    {
                    absoluteOffset = oc96Offset + LkPwCtrlOc48SliceOffset(self, oc48Slice) + pw_i + baseAddress;
                    mModuleHwWrite(self, cAf6Reg_pla_lk_pw_ctrl_Base + absoluteOffset, 0);
                    }
                }
            }
        }
    }

static void PlaPwFlush(ThaModulePwe self)
    {
    uint32 baseAddress = ThaModulePwePlaBaseAddress(self);
    uint32 pw_i;
    uint32 pw_hdr_ctrl_base = mMethodsGet(mThis(self))->PlaPwHeaderControl(mThis(self));

    for (pw_i = 0; pw_i < NumPws(self); pw_i++)
        {
        uint32 absoluteOffset = pw_i + baseAddress;

        mModuleHwWrite(self, pw_hdr_ctrl_base + absoluteOffset, 0);
        mModuleHwWrite(self, cAf6Reg_pla_pw_psn_ctrl_Base + absoluteOffset, 0);
        mModuleHwWrite(self, cAf6Reg_pla_pw_pro_ctrl_Base + absoluteOffset, 0);
        }
    }

static uint32 PlaNumGroupPerRegister(ThaModulePwe self)
    {
    return mMethodsGet((Tha60210011ModulePwe)self)->NumGroupsPerRegister((Tha60210011ModulePwe)self);
    }

static void PlaPwGroupFlush(ThaModulePwe self)
    {
    uint32 baseAddress = ThaModulePwePlaBaseAddress(self);
    uint32 cNumGroupPerRegister = PlaNumGroupPerRegister(self);
    uint32 group_i;

    for (group_i = 0; group_i < NumPws(self) / cNumGroupPerRegister; group_i++)
        {
        uint32 absoluteOffset = group_i + baseAddress;
        mModuleHwWrite(self, cAf6Reg_pla_out_hspw_ctrl_Base + absoluteOffset, 0);
        mModuleHwWrite(self, cAf6Reg_pla_out_upsr_ctrl_Base + absoluteOffset, 0);
        }
    }

static void PlaGlobalFlush(ThaModulePwe self)
    {
    uint32 baseAddress = ThaModulePwePlaBaseAddress(self);
    mModuleHwWrite(self, cAf6Reg_pla_out_psnpro_ctrl_Base + baseAddress, 0);
    }

static void HwFlushPla(ThaModulePwe self)
    {
    PlaGlobalFlush(self);
    PlaOc96SliceFlush(self);
    PlaPwFlush(self);
    PlaPwGroupFlush(self);
    }

static uint32 *PlaHoldRegistersGet(Tha60210051ModulePwe self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x470000, 0x470001, 0x470002};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static uint32 PsnBufferCtrlReg(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_out_psnbuf_ctrl_Base;
    }

static uint32 PsnControlReg(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_out_psnpro_ctrl_Base;
    }

static eBool IsTdmOc192(AtPw pw)
    {
    AtSdhChannel vc;

    if (AtPwTypeGet(pw) != cAtPwTypeCEP)
        return cAtFalse;

    vc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    if (AtSdhChannelTypeGet(vc) == cAtSdhChannelTypeVc4_64c)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PwTdmDefaultOffset(Tha60210011ModulePwe self, AtPw pw)
    {
    return Tha60290022ModulePweHwPwTdmDefaultOffset(mThis(self), pw);
    }

static uint32 PwTdmLookupOffset(Tha60210011ModulePwe self, AtPw pw)
    {
    uint8 oc48FlatSlice = cInvalidUint8;
    uint8 oc96Slice, oc48Slice;

    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &oc48FlatSlice, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    oc96Slice = oc48FlatSlice / 2;
    oc48Slice = oc48FlatSlice % 2;

    return (oc96Slice * 32768UL) + (oc48Slice * 2048UL) + AtChannelHwIdGet((AtChannel)pw);
    }

static uint32 PwTdmOc192DefaultOffset(Tha60210011ModulePwe self, AtPw pw)
    {
    uint8 oc48FlatSlice = cInvalidUint8;
    uint32 oc192factor = mMethodsGet(mThis(self))->HwOc192OffsetFactor(mThis(self));

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &oc48FlatSlice, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    return ((oc48FlatSlice / 4U) * oc192factor);
    }

static uint32 PwTdmOc192LookupOffset(Tha60210011ModulePwe self, AtPw pw)
    {
    uint8 oc48FlatSlice = cInvalidUint8;
    uint32 oc192factor = mMethodsGet(mThis(self))->HwOc192OffsetFactor(mThis(self));

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &oc48FlatSlice, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    return ((oc48FlatSlice / 4U) * oc192factor);
    }

static uint32 LoPayloadCtrl(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_pld_ctrl_Base;
    }

static uint32 PwHdrCtrl(ThaModulePwe self, AtPw pw)
    {
    uint32 pw_hdr_ctrl_Base = mMethodsGet(mThis(self))->PlaPwHeaderControl(mThis(self));
    return pw_hdr_ctrl_Base + mPlaPwAbsoluteOffset(self, pw);
    }

static eAtRet PwCwAutoTxLBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cAf6_pla_pw_hdr_ctrl_PlaPwLbitDisCtrl_, enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxLBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_pw_hdr_ctrl_PlaPwLbitDisCtrl_Mask) ? cAtFalse : cAtTrue;
    }

static eAtRet PwCwAutoTxRBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cAf6_pla_pw_hdr_ctrl_PlaPwRbitDisCtrl_, enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxRBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_pw_hdr_ctrl_PlaPwRbitDisCtrl_Mask) ? cAtFalse : cAtTrue;
    }

static eAtRet PwCwAutoTxMBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cAf6_pla_pw_hdr_ctrl_PlaPwMbitDisCtrl_, enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxMBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_pw_hdr_ctrl_PlaPwMbitDisCtrl_Mask) ? cAtFalse : cAtTrue;
    }

static eAtRet PwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cAf6_pla_pw_hdr_ctrl_PlaPwEnCtrl_, enable ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_pw_hdr_ctrl_PlaPwEnCtrl_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwTxLBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cAf6_pla_pw_hdr_ctrl_PlaPwLbitCPUCtrl_, force ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxLBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_pw_hdr_ctrl_PlaPwLbitCPUCtrl_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwTxRBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cAf6_pla_pw_hdr_ctrl_PlaPwRbitCPUCtrl_, force ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxRBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_pw_hdr_ctrl_PlaPwRbitCPUCtrl_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwTxMBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cAf6_pla_pw_hdr_ctrl_PlaPwMbitCPUCtrl_, force ? 2 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxMBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_pw_hdr_ctrl_PlaPwMbitCPUCtrl_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwSupressEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cAf6_pla_pw_hdr_ctrl_PlaPwSuprCtrl_, enable ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwSupressIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwHdrCtrl(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_pw_hdr_ctrl_PlaPwSuprCtrl_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 LoAddRemoveProtocolReg(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_add_rmv_pw_ctrl_Base;
    }

static uint32 PwPlaPldControl(Tha60210011ModulePwe self, AtPw pw)
    {
    if (IsTdmOc192(pw))
        return PlaOc192PldControl((ThaModulePwe)self, pw);

    return mMethodsGet(self)->LoPayloadCtrl(self) + PwTdmDefaultOffset(self, pw) + ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    }

static eBool HasPlaLookupPwControl(Tha60210011ModulePwe self, AtPw pw)
    {
    if (IsTdmOc192(pw))
        return cAtFalse;

    if (IsV3OptimizationSupported(self))
        return cAtFalse;

    return cAtTrue;
    }

static void PwPlaRegsShow(Tha60210011ModulePwe self, AtPw pw)
    {
    AtPw pwAdapter = (AtPw)ThaPwAdapterGet(pw);
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    uint32 baseAddress = ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    uint32 absoluteOffset;

    if (circuit == NULL)
        {
        AtPrintc(cSevInfo, "   - Pw has not bound circuit\r\n");
        return;
        }

    Tha60210011ModulePwCircuitInfoDisplay(pwAdapter);

    /* Circuit related registers */
    ThaModulePweRegDisplay(pw, "Payload Assembler Payload Control", PwPlaPldControl(self, pw));
    ThaModulePweRegDisplay(pw, "Payload Assembler Pseudowire Header Control", PwHdrCtrl((ThaModulePwe)self, pw));
    ThaModulePweRegDisplay(pw, "Payload Assembler Add/Remove Pseudowire Protocol Control", mMethodsGet(self)->PDHPWAddingProtocolToPreventBurst(self, pw));
    if (HasPlaLookupPwControl(self, pw))
        {
        absoluteOffset = PwTdmLookupOffset(self, pw) + baseAddress;
        ThaModulePweRegDisplay(pw, "Payload Assembler Lookup PW Control", cAf6Reg_pla_lk_pw_ctrl_Base + absoluteOffset);
        }

    /* PW registers */
    absoluteOffset = mPlaPwAbsoluteOffset(self, pw);
    ThaModulePweRegDisplay(pw, "Payload Assembler Pseudowire PSN Control", cAf6Reg_pla_pw_psn_ctrl_Base + absoluteOffset);
    ThaModulePweRegDisplay(pw, "Payload Assembler Psedowire Protection Control", cAf6Reg_pla_pw_pro_ctrl_Base + absoluteOffset);

    /* Group registers */
    absoluteOffset = baseAddress + ((AtChannelIdGet((AtChannel)pw)) / PlaNumGroupPerRegister((ThaModulePwe)self));
    ThaModulePweRegDisplay(pw, "Payload Assembler Output UPSR Control", cAf6Reg_pla_out_upsr_ctrl_Base + absoluteOffset);
    ThaModulePweRegDisplay(pw, "Payload Assembler Output HSPW Control", cAf6Reg_pla_out_hspw_ctrl_Base + absoluteOffset);

    /* Global registers */
    ThaModulePweRegDisplay(pw, "Payload Assembler PSN Process Control", cAf6Reg_pla_out_psnpro_ctrl_Base + baseAddress);
    }

static uint32 PDHPWAddingProtocolToPreventBurst(Tha60210011ModulePwe self, AtPw pw)
    {
    uint32 add_rmv_pw_ctrl_Base = mMethodsGet(self)->LoAddRemoveProtocolReg(self);
    uint32 pla_oc192c_add_rmv_pw_ctrl_Base = mMethodsGet(mThis(self))->PlaOc192cAddRemovePwControl(mThis(self));

    if (IsTdmOc192(pw))
        return pla_oc192c_add_rmv_pw_ctrl_Base + PwTdmOc192DefaultOffset(self, pw) + ThaModulePwePlaBaseAddress((ThaModulePwe)self);

    return add_rmv_pw_ctrl_Base + PwTdmDefaultOffset(self, pw) + ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    }

static uint32 PlaPldControlOffset(Tha60210011ModulePwe self, AtPw pw)
    {
    if (IsTdmOc192(pw))
        return PwTdmOc192DefaultOffset(self, pw) + ThaModulePwePlaBaseAddress((ThaModulePwe)self);

    return PwTdmDefaultOffset(self, pw) + ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    }

static void PlaPwOc96Lookup(Tha60210011ModulePwe self, ThaPwAdapter pwAdapter)
    {
    uint32 regAddr = cAf6Reg_pla_lk_pw_ctrl_Base + PwTdmLookupOffset(self, (AtPw)pwAdapter) +
                     ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    uint32 regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_lk_pw_ctrl_PlaLkPWIDCtrl_, AtChannelIdGet((AtChannel)pwAdapter));
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);
    }

static void PlaPwOc192Lookup(Tha60210011ModulePwe self, ThaPwAdapter pwAdapter)
    {
    uint32 pla_192c_pld_ctrl_Base = mMethodsGet(mThis(self))->PlaOc192cPayloadControl(mThis(self));
    uint32 regAddr = pla_192c_pld_ctrl_Base + PwTdmOc192LookupOffset(self, (AtPw)pwAdapter) +
                     ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    uint32 regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_192c_pld_ctrl_Pla192PWIDCtrl_, AtChannelIdGet((AtChannel)pwAdapter));
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);
    }

static void PlaV3PwOc96Lookup(Tha60210011ModulePwe self, ThaPwAdapter pwAdapter)
    {
    uint32 pla_pld_ctrl_Base = mMethodsGet(self)->LoPayloadCtrl(self);
    uint32 plaLkPWIDCtrlMask = mMethodsGet(mThis(self))->PlaLkPWIDCtrlMask(mThis(self));
    uint8 plaLkPWIDCtrlShift = mMethodsGet(mThis(self))->PlaLkPWIDCtrlShift(mThis(self));
    uint32 regAddr = pla_pld_ctrl_Base + PwTdmDefaultOffset(self, (AtPw)pwAdapter) +
                     ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    uint32 regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, plaLkPWIDCtrl, AtChannelIdGet((AtChannel)pwAdapter));
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);
    }

static void PlaPwLookup(Tha60210011ModulePwe self, ThaPwAdapter pwAdapter)
    {
    if (IsTdmOc192(ThaPwAdapterPwGet(pwAdapter)))
        PlaPwOc192Lookup(self, pwAdapter);
    else
        {
        if (IsV3OptimizationSupported(self))
            PlaV3PwOc96Lookup(self, pwAdapter);
        else
            PlaPwOc96Lookup(self, pwAdapter);
        }
    }

static eBool IsHighRateConcate(AtSdhChannel vc)
    {
    return (AtSdhChannelNumSlaveChannels(vc) >= 2) ? cAtTrue : cAtFalse;
    }

static eBool IsHighRatePw(ThaPwAdapter pwAdapter)
    {
    AtSdhChannel vc;
    uint32 vcType;

    if (AtPwTypeGet((AtPw)pwAdapter) != cAtPwTypeCEP)
        return cAtFalse;

    vc = (AtSdhChannel)AtPwBoundCircuitGet((AtPw)pwAdapter);
    if (vc == NULL)
        {
        AtChannelLog((AtChannel)pwAdapter, cAtLogLevelWarning, AtSourceLocation, "Circuit is NULL, cannot determine if PW is high-rate or not.\r\n");
        return cAtFalse;
        }

    vcType = AtSdhChannelTypeGet(vc);

    switch (vcType)
        {
        case cAtSdhChannelTypeVc4_64c: return cAtTrue;
        case cAtSdhChannelTypeVc4_16c: return cAtTrue;
        case cAtSdhChannelTypeVc4_4c : return cAtTrue;
        case cAtSdhChannelTypeVc4    : return cAtFalse;
        case cAtSdhChannelTypeVc3    : return cAtFalse;
        case cAtSdhChannelTypeVc12   : return cAtFalse;
        case cAtSdhChannelTypeVc11   : return cAtFalse;
        case cAtSdhChannelTypeVc4_nc : return IsHighRateConcate(vc);
        default:
            return cAtFalse;
        }
    }

static void PlaHiRateSet(Tha60210011ModulePwe self, ThaPwAdapter pwAdapter)
    {
    uint32 regAddr = PwHdrCtrl((ThaModulePwe)self, (AtPw)pwAdapter);
    uint32 regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);
    uint32 highRate = IsHighRatePw(pwAdapter) ? 1 : 0;
    mRegFieldSet(regVal, cAf6_pla_pw_hdr_ctrl_PlaPwHiRateCtrl_, highRate);
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);
    }

static void PlaPwDefaultHwConfigure(Tha60210011ModulePwe self, ThaPwAdapter pwAdapter)
    {
    PlaPwLookup(self, pwAdapter);
    if (mMethodsGet(mThis(self))->HasPlaHiRateCtrl(mThis(self)))
        PlaHiRateSet(self, pwAdapter);
    }

static uint32 PwProCtrl(ThaModulePweV2 self, AtPw pwAdapter)
    {
    return cAf6Reg_pla_pw_pro_ctrl_Base + mPlaPwAbsoluteOffset(self, pwAdapter);
    }

static eAtRet ApsGroupPwHwAdd(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter)
    {
    uint32 regAddr = PwProCtrl(self, pwAdapter);
    uint32 plaOutUpsrGrpCtrMask = mMethodsGet(mThis(self))->PlaOutUpsrGrpCtrlMask(mThis(self));
    uint8 plaOutUpsrGrpCtrShift = mMethodsGet(mThis(self))->PlaOutUpsrGrpCtrlShift(mThis(self));
    uint32 regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, plaOutUpsrGrpCtr, pwGroupHwId);
    mRegFieldSet(regVal, cAf6_pla_pw_pro_ctrl_PlaFlowUPSRUsedCtrl_, 1);
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool ApsGroupPwIsInHwGroup(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter)
    {
    uint32 regAddr = PwProCtrl(self, pwAdapter);
    uint32 plaOutUpsrGrpCtrMask = mMethodsGet(mThis(self))->PlaOutUpsrGrpCtrlMask(mThis(self));
    uint8 plaOutUpsrGrpCtrShift = mMethodsGet(mThis(self))->PlaOutUpsrGrpCtrlShift(mThis(self));
    uint32 regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);

    if ((mRegField(regVal, plaOutUpsrGrpCtr) == pwGroupHwId) &&
        (regVal & cAf6_pla_pw_pro_ctrl_PlaFlowUPSRUsedCtrl_Mask))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet PwePwTypeSet(Tha60210011ModulePwe self, AtPw pw, eAtPwType pwType)
    {
    /* Register is gone */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(pwType);
    return cAtOk;
    }

static eAtRet PlaPwTypeSet(Tha60210011ModulePwe self, AtPw pw, eAtPwType pwType)
    {
    if (IsTdmOc192(pw))
        return cAtOk;

    return m_Tha60210011ModulePweMethods->PlaPwTypeSet(self, pw, pwType);
    }

static eAtRet HsGroupPwAdd(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 regAddr = PwProCtrl(self, pwAdapter);
    uint32 regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);
    uint32 groupId = AtPwGroupIdGet(pwGroup);
    uint32 plaOutHspwGrpCtrMask = mMethodsGet(mThis(self))->PlaOutHspwGrpCtrlMask(mThis(self));
    uint8 plaOutHspwGrpCtrShift = mMethodsGet(mThis(self))->PlaOutHspwGrpCtrlShift(mThis(self));

    mRegFieldSet(regVal, plaOutHspwGrpCtr, groupId);
    mRegFieldSet(regVal, cAf6_pla_pw_pro_ctrl_PlaFlowHSPWUsedCtrl_, 1);
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet HsGroupPwRemove(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 regAddr = PwProCtrl(self, pwAdapter);
    uint32 regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);
    uint32 plaOutHspwGrpCtrMask = mMethodsGet(mThis(self))->PlaOutHspwGrpCtrlMask(mThis(self));
    uint8 plaOutHspwGrpCtrShift = mMethodsGet(mThis(self))->PlaOutHspwGrpCtrlShift(mThis(self));

    if (AtPwGroupIdGet(pwGroup) != mRegField(regVal, plaOutHspwGrpCtr))
        return cAtErrorInvlParm;

    mRegFieldSet(regVal, cAf6_pla_pw_pro_ctrl_PlaFlowHSPWUsedCtrl_, 0);
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint32 PlaOutUpsrCtrlBase(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_out_upsr_ctrl_Base;
    }

static uint32 PlaOutHspwCtrlBase(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_out_hspw_ctrl_Base;
    }

static eAtRet HsGroupEnable(ThaModulePweV2 self, AtPwGroup pwGroup, eBool enable)
    {
    AtUnused(self);
    AtUnused(pwGroup);
    AtUnused(enable);
    return cAtOk;
    }

static eBool HsGroupIsEnabled(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    AtUnused(self);
    AtUnused(pwGroup);
    return cAtTrue;
    }

static uint32 GroupAbsoluteOffset(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    uint32 groupId = AtPwGroupIdGet(pwGroup);
    uint32 cNumGroupPerRegister = PlaNumGroupPerRegister((ThaModulePwe)self);
    return (groupId / cNumGroupPerRegister) + ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    }

static eAtRet HsGroupLabelSetSelectShift(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    uint32 cNumGroupPerRegister = PlaNumGroupPerRegister((ThaModulePwe)self);
    AtUnused(self);
    return (AtPwGroupIdGet(pwGroup) % cNumGroupPerRegister);
    }

static eAtRet HsGroupLabelSetSelectMask(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    return cBit0 << HsGroupLabelSetSelectShift(self, pwGroup);
    }

static uint32 GroupLabelSetSw2Hw(eAtPwGroupLabelSet labelSet)
    {
    return (labelSet == cAtPwGroupLabelSetPrimary) ? 0 : 1;
    }

static eAtPwGroupLabelSet GroupLabelSetHw2Sw(uint32 labelSet)
    {
    return (labelSet == 0) ? cAtPwGroupLabelSetPrimary : cAtPwGroupLabelSetBackup;
    }

static uint32 PlaOutHspwCtrl(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    return mPlaOutHspwCtrlBase(self) + GroupAbsoluteOffset(self, pwGroup);
    }

static eAtRet HsGroupLabelSetSelect(ThaModulePweV2 self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet)
    {
    uint32 regAddr = PlaOutHspwCtrl(self, pwGroup);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    uint32 fieldMask = HsGroupLabelSetSelectMask(self, pwGroup);
    uint32 fieldShift = HsGroupLabelSetSelectShift(self, pwGroup);
    mRegFieldSet(regVal, field, GroupLabelSetSw2Hw(labelSet));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HsGroupSelectedLabelSetGet(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    uint32 regAddr = PlaOutHspwCtrl(self, pwGroup);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    uint32 fieldMask = HsGroupLabelSetSelectMask(self, pwGroup);
    uint32 fieldShift = HsGroupLabelSetSelectShift(self, pwGroup);

    return GroupLabelSetHw2Sw(mRegField(regVal, field));
    }

static uint32 PsnChannelIdMask(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Mask;
    }

static uint32 PsnChannelIdShift(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Shift;
    }

static uint32 PlaOutPsnProPageCtrMask(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Mask;
    }

static uint32 PlaOutPsnProPageCtrShift(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Shift;
    }

static eBool CanAccessCacheUsage(ThaModulePwe self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool RunOnVu9P(ThaModulePwe self)
    {
    return Tha60290022DeviceRunOnVu9P(AtModuleDeviceGet((AtModule)self));
    }

static eBool TxPwActiveHwIsReady(ThaModulePwe self)
    {
    return RunOnVu9P(self) ? cAtFalse : cAtTrue;
    }

static void PwPlaDebug(Tha60210011ModulePwe self, AtPw pw)
    {
    if (IsTdmOc192(pw))
        Tha60290022ModulePwePwOc192PlaDebug((ThaModulePwe)self, pw);
    else
        Tha60290022ModulePwePwPlaDebug((ThaModulePwe)self, pw);
    }

static void LowOrderBlockProcessDebug(Tha60210011ModulePwe self)
    {
    /* This product does not have this part */
    AtUnused(self);
    }

static void PlaInterfaceDebug(Tha60210011ModulePwe self)
    {
    Tha60290022ModulePwePlaDebug((ThaModulePwe)self);
    }

static void CacheUsageGet(ThaModulePwe self, tThaPweCache *cache)
    {
    Tha60290022ModulePwePlaCacheUsageGet(self, cache);
    }

static eBool ShouldCheckHwResource(void)
    {
    /* Not now, hardware need to give correct registers */
    return cAtFalse;
    }

static eAtRet HwResourceCheck(AtModule self)
    {
    if (ShouldCheckHwResource())
        return m_AtModuleMethods->HwResourceCheck(self);
    return cAtOk;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    Tha60210011ModulePwe pweModule = (Tha60210011ModulePwe)self;

    if (localRamId < mMethodsGet(pweModule)->StartPweRamLocalId(pweModule))
        return Tha60290022InternalRamPlaNew(self, ramId, localRamId);

    return m_AtModuleMethods->InternalRamCreate(self, ramId, localRamId);
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "Payload Control Register OC48#0 of OC96#0", /* 0x1000 */
         "Payload Control Register OC48#1 of OC96#0", /* 0x3000 */
         sTha60290022ModulePweRamLookupPWControlRegisterOC96 "#0",        /* 0x4000 */
         "Payload Control Register OC48#0 of OC96#1",
         "Payload Control Register OC48#1 of OC96#1",
         sTha60290022ModulePweRamLookupPWControlRegisterOC96 "#1",
         "Payload Control Register OC48#0 of OC96#2",
         "Payload Control Register OC48#1 of OC96#2",
         sTha60290022ModulePweRamLookupPWControlRegisterOC96 "#2",
         "Payload Control Register OC48#0 of OC96#3",
         "Payload Control Register OC48#1 of OC96#3",
         sTha60290022ModulePweRamLookupPWControlRegisterOC96 "#3", /* 11 */

         "PW Header Control Register", /* 12; 0x20000 */
         "PW PSN Control Register", /* 13; 0x48000  */

         "PW Protection Control Register", /* 14; 0x50000 */
         "Ouput UPSR Control Register", /* 0x58000 */

         "PLA CRC Error", /* 16 */

         "PWE Pseudowire Transmit Ethernet Header Mode Control slice 0", /* 17 */
         sTha60210011ModulePweRamPWEPseudowireTransmitEthernetHeaderModeControlslice1,
         "PWE Pseudowire Transmit Ethernet Header Mode Control slice 2",
         "PWE Pseudowire Transmit Ethernet Header Mode Control slice 3"
        };

    AtUnused(self);
    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static uint32 StartCrcRamLocalId(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 16;
    }

static uint32 StartHSPWRamLocalId(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 14;
    }

static uint32 StartHoPlaRamLocalId(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 StartPweRamLocalId(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 17;
    }

static uint32 StartVersionControlJitterAttenuator(Tha60210011ModulePwe self)
    {
    AtDevice device = (AtDevice)AtModuleDeviceGet((AtModule)self);
    return Tha60290022DeviceDcrAcrTxShapperSupportedVersion(device);
    }

static eBool CanWaitForHwDone(Tha60210011ModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return Tha60290022DeviceHasKByteFeatureOnly(device) ? cAtFalse : cAtTrue;
    }

static uint32 AbsolutePwIdV3Get(Tha60210011ModulePwe self, uint32 oc48Slice, uint32 localPwId)
    {
    uint32 baseAddress = ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    uint32 oc96Slice = oc48Slice / cNumOc48SlicesInOc96Slice;
    uint32 localOc48Slice = oc48Slice % cNumOc48SlicesInOc96Slice;
    uint32 offset = Oc96SliceOffset((ThaModulePwe)self, oc96Slice) + Oc48SliceV3Offset((ThaModulePwe)self, localOc48Slice) + localPwId + baseAddress;
    uint32 pla_pld_ctrl_Base = mMethodsGet(self)->LoPayloadCtrl(self);
    uint32 plaLkPWIDCtrlMask = mMethodsGet(mThis(self))->PlaLkPWIDCtrlMask(mThis(self));
    uint8 plaLkPWIDCtrlShift = mMethodsGet(mThis(self))->PlaLkPWIDCtrlShift(mThis(self));
    uint32 regAddr = offset + pla_pld_ctrl_Base;
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, plaLkPWIDCtrl);
    }

static uint32 AbsolutePwIdV1Get(Tha60210011ModulePwe self, uint32 oc48Slice, uint32 localPwId)
    {
    uint32 baseAddress = ThaModulePwePlaBaseAddress((ThaModulePwe)self);
    uint32 oc96Slice = oc48Slice / cNumOc48SlicesInOc96Slice;
    uint32 localOc48Slice = oc48Slice % cNumOc48SlicesInOc96Slice;
    uint32 offset = Oc96SliceOffset((ThaModulePwe)self, oc96Slice) + LkPwCtrlOc48SliceOffset((ThaModulePwe)self, localOc48Slice) + localPwId + baseAddress;
    uint32 regAddr = offset + cAf6Reg_pla_lk_pw_ctrl_Base;
    return mModuleHwRead(self, regAddr);
    }

static uint32 PlaOc192PldControl(ThaModulePwe self, AtPw pw)
    {
    uint32 pla_192c_pld_ctrl_Base = mMethodsGet(mThis(self))->PlaOc192cPayloadControl(mThis(self));
    return pla_192c_pld_ctrl_Base + Tha60210011ModulePlaPldControlOffset(self, pw);
    }

static eAtRet PwOc192PayloadSizeSet(ThaModulePwe self, AtPw pw, uint16 payloadSize)
    {
    uint32 address, regVal;

    address = PlaOc192PldControl(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_192c_pld_ctrl_Pla192PldCtrl_, payloadSize);
    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint16 PwOc192PayloadSizeGet(ThaModulePwe self, AtPw pw)
    {
    uint32 address, regVal;

    address = PlaOc192PldControl(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModulePwe);
    return (uint16)mRegField(regVal, cAf6_pla_192c_pld_ctrl_Pla192PldCtrl_);
    }

static eAtRet PayloadSizeSet(ThaModulePwe self, AtPw pw, uint16 payloadSize)
    {
    if (IsTdmOc192(pw))
        return PwOc192PayloadSizeSet(self, pw, payloadSize);

    return m_ThaModulePweMethods->PayloadSizeSet(self, pw, payloadSize);
    }

static uint16 PayloadSizeGet(ThaModulePwe self, AtPw pw)
    {
    if (IsTdmOc192(pw))
        return PwOc192PayloadSizeGet(self, pw);

    return m_ThaModulePweMethods->PayloadSizeGet(self, pw);
    }

static uint32 AbsolutePwIdGet(Tha60210011ModulePwe self, uint32 oc48Slice, uint32 localPwId)
    {
    if (IsV3OptimizationSupported(self))
        return AbsolutePwIdV3Get(self, oc48Slice, localPwId);
    return AbsolutePwIdV1Get(self, oc48Slice, localPwId);
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    if (IsV3OptimizationSupported((Tha60210011ModulePwe)self))
        {
        const char *ramName = AtInternalRamName(ram);
        if (AtStrncmp(ramName,
                      sTha60290022ModulePweRamLookupPWControlRegisterOC96,
                      AtStrlen(sTha60290022ModulePweRamLookupPWControlRegisterOC96)) == 0)
            return cAtTrue;
        }

    return m_AtModuleMethods->InternalRamIsReserved(self, ram);
    }

static uint32 HwPwTdmDefaultOffset(Tha60290022ModulePwe self, AtPw pw)
    {
    uint8 oc48FlatSlice = cInvalidUint8;
    uint8 oc96Slice, oc48Slice;
    uint32 oc96factor, oc48factor;

    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &oc48FlatSlice, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    oc96Slice = oc48FlatSlice / 2;
    oc48Slice = oc48FlatSlice % 2;
    oc96factor = mMethodsGet(self)->HwOc96OffsetFactor(self);
    oc48factor = mMethodsGet(self)->HwOc48InOc96OffsetFactor(self);
    return (oc96Slice * oc96factor) + (oc48Slice * oc48factor) + AtChannelHwIdGet((AtChannel)pw);
    }

static uint32 HwOc192OffsetFactor(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return 16384;
    }

static uint32 HwOc96OffsetFactor(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return 32768;
    }

static uint32 HwOc48InOc96OffsetFactor(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return 8192;
    }

static uint32 HwNumPwInOc48(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return 2048;
    }

static uint32 PlaLkPWIDCtrlMask(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_lk_pw_ctrl_PlaLkPWIDCtrl_V3_Mask;
    }

static uint8 PlaLkPWIDCtrlShift(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_lk_pw_ctrl_PlaLkPWIDCtrl_V3_Shift;
    }

static uint32 PlaOutUpsrGrpCtrlMask(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_pw_pro_ctrl_PlaOutUpsrGrpCtr_Mask;
    }

static uint8 PlaOutUpsrGrpCtrlShift(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_pw_pro_ctrl_PlaOutUpsrGrpCtr_Shift;
    }

static uint32 PlaOutHspwGrpCtrlMask(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_pw_pro_ctrl_PlaOutHspwGrpCtr_Mask;
    }

static uint8 PlaOutHspwGrpCtrlShift(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_pw_pro_ctrl_PlaOutHspwGrpCtr_Shift;
    }

static uint32 PlaPwHeaderControl(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_pw_hdr_ctrl_Base;
    }

static uint32 PlaOc192cPayloadControl(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_192c_pld_ctrl_Base;
    }

static uint32 PlaOc192cAddRemovePwControl(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_oc192c_add_rmv_pw_ctrl_Base;
    }

static uint32 PlaOutClk311Control(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_out_clk311_ctrl_Base;
    }

static uint32 PlaOc48Clk311Sticky(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_oc48_clk311_stk_Base;
    }

static uint32 PlaOc192cClk311Sticky(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_oc192c_clk311_stk_Base;
    }

static eBool HasPlaHiRateCtrl(Tha60290022ModulePwe self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideTha60210011ModulePwe(AtModule self)
    {
    Tha60210011ModulePwe modulePwe = (Tha60210011ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePweMethods = mMethodsGet(modulePwe);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePweOverride, mMethodsGet(modulePwe), sizeof(m_Tha60210011ModulePweOverride));

        mMethodOverride(m_Tha60210011ModulePweOverride, PsnBufferCtrlReg);
        mMethodOverride(m_Tha60210011ModulePweOverride, PsnControlReg);
        mMethodOverride(m_Tha60210011ModulePweOverride, LoPayloadCtrl);
        mMethodOverride(m_Tha60210011ModulePweOverride, PwPlaRegsShow);
        mMethodOverride(m_Tha60210011ModulePweOverride, LoAddRemoveProtocolReg);
        mMethodOverride(m_Tha60210011ModulePweOverride, PDHPWAddingProtocolToPreventBurst);
        mMethodOverride(m_Tha60210011ModulePweOverride, PlaPldControlOffset);
        mMethodOverride(m_Tha60210011ModulePweOverride, PlaPwDefaultHwConfigure);
        mMethodOverride(m_Tha60210011ModulePweOverride, PwePwTypeSet);
        mMethodOverride(m_Tha60210011ModulePweOverride, PlaOutUpsrCtrlBase);
        mMethodOverride(m_Tha60210011ModulePweOverride, PlaOutHspwCtrlBase);
        mMethodOverride(m_Tha60210011ModulePweOverride, PsnChannelIdMask);
        mMethodOverride(m_Tha60210011ModulePweOverride, PsnChannelIdShift);
        mMethodOverride(m_Tha60210011ModulePweOverride, PlaOutPsnProPageCtrMask);
        mMethodOverride(m_Tha60210011ModulePweOverride, PlaOutPsnProPageCtrShift);
        mMethodOverride(m_Tha60210011ModulePweOverride, PwPlaDebug);
        mMethodOverride(m_Tha60210011ModulePweOverride, LowOrderBlockProcessDebug);
        mMethodOverride(m_Tha60210011ModulePweOverride, PlaInterfaceDebug);
        mMethodOverride(m_Tha60210011ModulePweOverride, StartCrcRamLocalId);
        mMethodOverride(m_Tha60210011ModulePweOverride, StartHSPWRamLocalId);
        mMethodOverride(m_Tha60210011ModulePweOverride, StartHoPlaRamLocalId);
        mMethodOverride(m_Tha60210011ModulePweOverride, StartPweRamLocalId);
        mMethodOverride(m_Tha60210011ModulePweOverride, StartVersionControlJitterAttenuator);
        mMethodOverride(m_Tha60210011ModulePweOverride, CanWaitForHwDone);
        mMethodOverride(m_Tha60210011ModulePweOverride, AbsolutePwIdGet);
        mMethodOverride(m_Tha60210011ModulePweOverride, PlaPwTypeSet);
        }

    mMethodsSet(modulePwe, &m_Tha60210011ModulePweOverride);
    }

static void OverrideTha60210051ModulePwe(AtModule self)
    {
    Tha60210051ModulePwe modulePwe = (Tha60210051ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051ModulePweOverride, mMethodsGet(modulePwe), sizeof(m_Tha60210051ModulePweOverride));

        mMethodOverride(m_Tha60210051ModulePweOverride, PlaHoldRegistersGet);
        }

    mMethodsSet(modulePwe, &m_Tha60210051ModulePweOverride);
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, mMethodsGet(pweModule), sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PwEnable);
        mMethodOverride(m_ThaModulePweOverride, PwIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxLBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxLBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxRBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxRBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxMBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxMBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwTxLBitForce);
        mMethodOverride(m_ThaModulePweOverride, PwTxLBitIsForced);
        mMethodOverride(m_ThaModulePweOverride, PwTxRBitForce);
        mMethodOverride(m_ThaModulePweOverride, PwTxRBitIsForced);
        mMethodOverride(m_ThaModulePweOverride, PwTxMBitForce);
        mMethodOverride(m_ThaModulePweOverride, PwTxMBitIsForced);
        mMethodOverride(m_ThaModulePweOverride, PwSupressEnable);
        mMethodOverride(m_ThaModulePweOverride, PwSupressIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, CanAccessCacheUsage);
        mMethodOverride(m_ThaModulePweOverride, TxPwActiveHwIsReady);
        mMethodOverride(m_ThaModulePweOverride, CacheUsageGet);
        mMethodOverride(m_ThaModulePweOverride, PayloadSizeSet);
        mMethodOverride(m_ThaModulePweOverride, PayloadSizeGet);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void OverrideThaModulePweV2(AtModule self)
    {
    ThaModulePweV2 pweModule = (ThaModulePweV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweV2Override, mMethodsGet(pweModule), sizeof(m_ThaModulePweV2Override));

        mMethodOverride(m_ThaModulePweV2Override, ApsGroupPwHwAdd);
        mMethodOverride(m_ThaModulePweV2Override, ApsGroupPwIsInHwGroup);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupPwAdd);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupPwRemove);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupEnable);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupIsEnabled);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupLabelSetSelect);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupSelectedLabelSetGet);
        }

    mMethodsSet(pweModule, &m_ThaModulePweV2Override);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, HwResourceCheck);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void MethodsInit(AtModule self)
    {
    Tha60290022ModulePwe pweModule = (Tha60290022ModulePwe)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HwOc192OffsetFactor);
        mMethodOverride(m_methods, HwOc96OffsetFactor);
        mMethodOverride(m_methods, HwOc48InOc96OffsetFactor);
        mMethodOverride(m_methods, HwNumPwInOc48);
        mMethodOverride(m_methods, PlaLkPWIDCtrlMask);
        mMethodOverride(m_methods, PlaLkPWIDCtrlShift);
        mMethodOverride(m_methods, PlaOutUpsrGrpCtrlMask);
        mMethodOverride(m_methods, PlaOutUpsrGrpCtrlShift);
        mMethodOverride(m_methods, PlaOutHspwGrpCtrlMask);
        mMethodOverride(m_methods, PlaOutHspwGrpCtrlShift);
        mMethodOverride(m_methods, PlaPwHeaderControl);
        mMethodOverride(m_methods, PlaOc192cPayloadControl);
        mMethodOverride(m_methods, PlaOc192cAddRemovePwControl);
        mMethodOverride(m_methods, PlaOutClk311Control);
        mMethodOverride(m_methods, PlaOc48Clk311Sticky);
        mMethodOverride(m_methods, PlaOc192cClk311Sticky);
        mMethodOverride(m_methods, HwNumOc96Slices);
        mMethodOverride(m_methods, HasPlaHiRateCtrl);
        }

    mMethodsSet(pweModule, &m_methods);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePwe(self);
    OverrideThaModulePweV2(self);
    OverrideTha60210011ModulePwe(self);
    OverrideTha60210051ModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModulePwe);
    }

AtModule Tha60290022ModulePweObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290022ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModulePweObjectInit(newModule, device);
    }

void Tha60290022ModulePweHwFlushPla(ThaModulePwe self)
    {
    if (self)
        HwFlushPla(self);
    }

uint32 Tha60290022ModulePweHwPwTdmDefaultOffset(Tha60290022ModulePwe self, AtPw pw)
    {
    return HwPwTdmDefaultOffset(self, pw);
    }

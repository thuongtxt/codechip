/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_PLA_H_
#define _AF6_REG_AF6CNC0022_RD_PLA_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Payload Control
Reg Addr   : 0x0_1000-0x1_B7FF
Reg Formula: 0x0_1000 + $Oc96Slice*32768 + $Oc48Slice*8192 + $Pwid
    Where  : 
           + $Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G
           + $Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice
           + $Pwid(0-2047): pseudowire channel for each OC-48 slice
Reg Desc   : 
This register is used to configure payload in each Pseudowire channels per slice
HDL_PATH  : ipwcore.ipwslcore.ilo96core[$Oc96Slice].ilo48core[$Oc48Slice].irtlpla.cfgpwctrl.membuf.ram.ram[$Pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_pld_ctrl_Base                                                                      0x01000
#define cAf6Reg_pla_pld_ctrl_WidthVal                                                                       32

/*--------------------------------------
BitField Name: PlaLkPWIDCtrl
BitField Type: RW
BitField Desc: Lookup to a output psedowire
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_pla_lk_pw_ctrl_PlaLkPWIDCtrl_V3_Mask                                                        cBit31_18
#define cAf6_pla_lk_pw_ctrl_PlaLkPWIDCtrl_V3_Shift                                                              18

/*--------------------------------------
BitField Name: PlaLoPldTSSrcCtrl
BitField Type: RW
BitField Desc: Timestamp Source 0: PRC global 1: TS from CDR engine
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_pld_ctrl_PlaLoPldTSSrcCtrl_Mask                                                        cBit17
#define cAf6_pla_pld_ctrl_PlaLoPldTSSrcCtrl_Shift                                                           17

/*--------------------------------------
BitField Name: PlaLoPldTypeCtrl
BitField Type: RW
BitField Desc: Payload Type 0: satop 1: ces without cas 2: cep
BitField Bits: [16:15]
--------------------------------------*/
#define cAf6_pla_pld_ctrl_PlaLoPldTypeCtrl_Mask                                                      cBit16_15
#define cAf6_pla_pld_ctrl_PlaLoPldTypeCtrl_Shift                                                            15

/*--------------------------------------
BitField Name: PlaLoPldSizeCtrl
BitField Type: RW
BitField Desc: Payload Size satop/cep mode: bit[13:0] payload size (ex: value as
0x100 is payload size 256 bytes) ces mode: bit[5:0] number of DS0 timeslot
bit[14:6] number of NxDS0 frame
BitField Bits: [14:0]
--------------------------------------*/
#define cAf6_pla_pld_ctrl_PlaLoPldSizeCtrl_Mask                                                       cBit14_0
#define cAf6_pla_pld_ctrl_PlaLoPldSizeCtrl_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Add/Remove Pseudowire Protocol Control
Reg Addr   : 0x0_1800-0x1_BFFF
Reg Formula: 0x0_1800 + $Oc96Slice*32768 + $Oc48Slice*8192 + $Pwid
    Where  : 
           + $Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G
           + $Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice
           + $Pwid(0-2047): pseudowire channel for each OC-48 slice
Reg Desc   : 
This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each OC-48 slice

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_add_rmv_pw_ctrl_Base                                                               0x01800
#define cAf6Reg_pla_add_rmv_pw_ctrl_WidthVal                                                                32

/*--------------------------------------
BitField Name: PlaLoStaAddRmvCtrl
BitField Type: RW
BitField Desc: protocol state to add/remove pw Step1: Add intitial or pw idle,
the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1"
value for the protocol state to start adding pw. The protocol state value is
"1". Step3: CPU enables pw at demap to finish the adding pw process. HW will
automatically change the protocol state value to "2" to run the pw, and keep
this state. Step4: For removing the pw, CPU will write "3" value for the
protocol state to start removing pw. The protocol state value is "3". Step5:
Poll the protocol state until return value "0" value, after that CPU disables pw
at demap to finish the removing pw process
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pla_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Mask                                               cBit1_0
#define cAf6_pla_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Lookup PW Control
Reg Addr   : 0x0_4000-0x1_8FFF
Reg Formula: 0x0_4000 + $Oc96Slice*32768 + $Oc48Slice*2048 + $Pwid
    Where  : 
           + $Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G
           + $Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per LO OC-96 slice
           + $Pwid(0-2047): pseudowire channel for each OC-48 slice
Reg Desc   : 
This register is used to lookup pseudowire per OC-48 slice to a common pseudowire number (total is 10752 pw)
HDL_PATH  : ipwcore.ipwslcore.ilo96core[$Oc96Slice].pwcfg.membuf.ram.ram[$Oc48Slice*2048+$Pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_lk_pw_ctrl_Base                                                                    0x04000
#define cAf6Reg_pla_lk_pw_ctrl_WidthVal                                                                     32

/*--------------------------------------
BitField Name: PlaLkPWIDCtrl
BitField Type: RW
BitField Desc: Lookup to a output psedowire
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_pla_lk_pw_ctrl_PlaLkPWIDCtrl_Mask                                                        cBit13_0
#define cAf6_pla_lk_pw_ctrl_PlaLkPWIDCtrl_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Pseudowire Header Control
Reg Addr   : 0x2_0000-0x2_29FF
Reg Formula: 0x2_0000 + $pwid
    Where  : 
           + $pwid(0-10751): pseudowire channel
Reg Desc   : 
This register is used to config Control Word for the psedowire channels
HDL_PATH  : ipwcore.ipwcfg.membuf.ram.ram[$pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_pw_hdr_ctrl_Base                                                                   0x20000
#define cAf6Reg_pla_pw_hdr_ctrl_WidthVal                                                                    32

/*--------------------------------------
BitField Name: PlaPwHiRateCtrl
BitField Type: RW
BitField Desc: set 1 for high rate path (12c/48c/192c), other is set 0
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwHiRateCtrl_Mask                                                        cBit9
#define cAf6_pla_pw_hdr_ctrl_PlaPwHiRateCtrl_Shift                                                           9

/*--------------------------------------
BitField Name: PlaPwSuprCtrl
BitField Type: RW
BitField Desc: Suppresion Enable 1: enable 0: disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwSuprCtrl_Mask                                                          cBit8
#define cAf6_pla_pw_hdr_ctrl_PlaPwSuprCtrl_Shift                                                             8

/*--------------------------------------
BitField Name: PlaPwRbitDisCtrl
BitField Type: RW
BitField Desc: R bit disable 1: disable R bit in the Control Word (assign zero
in the packet) 0: normal
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwRbitDisCtrl_Mask                                                       cBit7
#define cAf6_pla_pw_hdr_ctrl_PlaPwRbitDisCtrl_Shift                                                          7

/*--------------------------------------
BitField Name: PlaPwMbitDisCtrl
BitField Type: RW
BitField Desc: M or NP bits disable 1: disable M or NP bits in the Control Word
(assign zero in the packet) 0: normal
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwMbitDisCtrl_Mask                                                       cBit6
#define cAf6_pla_pw_hdr_ctrl_PlaPwMbitDisCtrl_Shift                                                          6

/*--------------------------------------
BitField Name: PlaPwLbitDisCtrl
BitField Type: RW
BitField Desc: L bit disable 1: disable L bit in the Control Word (assign zero
in the packet) 0: normal
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwLbitDisCtrl_Mask                                                       cBit5
#define cAf6_pla_pw_hdr_ctrl_PlaPwLbitDisCtrl_Shift                                                          5

/*--------------------------------------
BitField Name: PlaPwRbitCPUCtrl
BitField Type: RW
BitField Desc: R bit value from CPU (low priority than PlaPwRbitDisCtrl)
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwRbitCPUCtrl_Mask                                                       cBit4
#define cAf6_pla_pw_hdr_ctrl_PlaPwRbitCPUCtrl_Shift                                                          4

/*--------------------------------------
BitField Name: PlaPwMbitCPUCtrl
BitField Type: RW
BitField Desc: M or NP bits value from CPU (low priority than
PlaLoPwMbitDisCtrl)
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwMbitCPUCtrl_Mask                                                     cBit3_2
#define cAf6_pla_pw_hdr_ctrl_PlaPwMbitCPUCtrl_Shift                                                          2

/*--------------------------------------
BitField Name: PlaPwLbitCPUCtrl
BitField Type: RW
BitField Desc: L bit value from CPU (low priority than PlaLoPwLbitDisCtrl)
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwLbitCPUCtrl_Mask                                                       cBit1
#define cAf6_pla_pw_hdr_ctrl_PlaPwLbitCPUCtrl_Shift                                                          1

/*--------------------------------------
BitField Name: PlaPwEnCtrl
BitField Type: RW
BitField Desc: Pseudowire enable 1: enable 0: disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_pw_hdr_ctrl_PlaPwEnCtrl_Mask                                                            cBit0
#define cAf6_pla_pw_hdr_ctrl_PlaPwEnCtrl_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Pseudowire PSN Control
Reg Addr   : 0x4_8000-0x4_A9FF
Reg Formula: 0x4_8000 + $pwid
    Where  : 
           + $pwid(0-10751): pseudowire channel
Reg Desc   : 
This register is used to config psedowire PSN and APS at output
HDL_PATH  : irdport.iflowcfg.membuf.ram.ram[$pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_pw_psn_ctrl_Base                                                                   0x48000
#define cAf6Reg_pla_pw_psn_ctrl_WidthVal                                                                    32

/*--------------------------------------
BitField Name: PlaOutPSNLenCtrl
BitField Type: RW
BitField Desc: PSN length
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_pla_pw_psn_ctrl_PlaOutPSNLenCtrl_Mask                                                     cBit6_0
#define cAf6_pla_pw_psn_ctrl_PlaOutPSNLenCtrl_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Psedowire Protection Control
Reg Addr   : 0x5_0000-0x5_29FF
Reg Formula: 0x5_0000 + $pwid
    Where  : 
           + $pwid(0-10751): flow channel
Reg Desc   : 
This register is used to config UPSR/HSPW for protect psedowire
HDL_PATH  : irdport.iapslkcfg.membuf.ram.ram[$pwid]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_pw_pro_ctrl_Base                                                                   0x50000
#define cAf6Reg_pla_pw_pro_ctrl_WidthVal                                                                    32

/*--------------------------------------
BitField Name: PlaOutUpsrGrpCtr
BitField Type: RW
BitField Desc: UPRS group
BitField Bits: [29:16]
--------------------------------------*/
#define cAf6_pla_pw_pro_ctrl_PlaOutUpsrGrpCtr_Mask                                                   cBit29_16
#define cAf6_pla_pw_pro_ctrl_PlaOutUpsrGrpCtr_Shift                                                         16

/*--------------------------------------
BitField Name: PlaOutHspwGrpCtr
BitField Type: RW
BitField Desc: HSPW group
BitField Bits: [15:2]
--------------------------------------*/
#define cAf6_pla_pw_pro_ctrl_PlaOutHspwGrpCtr_Mask                                                    cBit15_2
#define cAf6_pla_pw_pro_ctrl_PlaOutHspwGrpCtr_Shift                                                          2

/*--------------------------------------
BitField Name: PlaFlowUPSRUsedCtrl
BitField Type: RW
BitField Desc: Flow UPSR is used or not 0:not used, all configurations for UPSR
will be not effected 1:used
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_pw_pro_ctrl_PlaFlowUPSRUsedCtrl_Mask                                                    cBit1
#define cAf6_pla_pw_pro_ctrl_PlaFlowUPSRUsedCtrl_Shift                                                       1

/*--------------------------------------
BitField Name: PlaFlowHSPWUsedCtrl
BitField Type: RW
BitField Desc: Flow HSPW is used or not 0:not used, all configurations for HSPW
will be not effected 1:used
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_pw_pro_ctrl_PlaFlowHSPWUsedCtrl_Mask                                                    cBit0
#define cAf6_pla_pw_pro_ctrl_PlaFlowHSPWUsedCtrl_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output UPSR Control
Reg Addr   : 0x5_8000-0x5_A9FF
Reg Formula: 0x5_8000 + $upsrgrp
    Where  : 
           + $upsrgrp(0-671): UPSR group address
Reg Desc   : 
This register is used to config UPSR group
HDL_PATH  : irdport.iupsrcfg.membuf.ram.ram[$upsrgrp]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_upsr_ctrl_Base                                                                 0x58000
#define cAf6Reg_pla_out_upsr_ctrl_WidthVal                                                                  32

/*--------------------------------------
BitField Name: PlaOutUpsrEnCtr
BitField Type: RW
BitField Desc: Total is 10752 upsrID, divide into 672 group address for
configuration, 16-bit is correlative with 16 upsrID each group. Bit#0 is for
upsrID#0, bit#1 is for upsrID#1... 1: enable 0: disable
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Mask                                                   cBit15_0
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output HSPW Control
Reg Addr   : 0x6_0000-0x6_029F
Reg Formula: 0x6_0000 + $hspwgrp
    Where  : 
           + $hspwgrp(0-671): HSPW group address
Reg Desc   : 
This register is used to config HSPW group
HDL_PATH  : irdport.ihspwcfg.membuf.ram.ram[$hspwgrp]

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_hspw_ctrl_Base                                                                 0x60000
#define cAf6Reg_pla_out_hspw_ctrl_WidthVal                                                                  32

/*--------------------------------------
BitField Name: PlaOutHSPWCtr
BitField Type: RW
BitField Desc: Total is 10752 hspwID, divide into 672 group address for
configuration, 16-bit is correlative with 16 hspwID each group. Bit#0 is for
hspwID#0, bit#1 is for hspwID#1...
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_pla_out_hspw_ctrl_PlaOutHSPWCtr_Mask                                                     cBit15_0
#define cAf6_pla_out_hspw_ctrl_PlaOutHSPWCtr_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output PSN Process Control
Reg Addr   : 0x4_0000
Reg Formula: 0x4_0000
    Where  : 
Reg Desc   : 
This register is used to control PSN configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_psnpro_ctrl_Base                                                               0x40000

/*--------------------------------------
BitField Name: PlaOutPsnProReqCtr
BitField Type: RW
BitField Desc: Request to process PSN 1: request to write/read PSN header
buffer. The CPU need to prepare a complete PSN header into PSN buffer before
request write OR read out a complete PSN header in PSN buffer after request read
done 0: HW will automatically set to 0 when a request done
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Mask                                                cBit31
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Shift                                                   31

/*--------------------------------------
BitField Name: PlaOutPsnProRnWCtr
BitField Type: RW
BitField Desc: read or write PSN 1: read PSN 0: write PSN
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Mask                                                cBit30
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_Shift                                                   30

/*--------------------------------------
BitField Name: PlaOutPsnProLenCtr
BitField Type: RW
BitField Desc: Length of a complete PSN need to read/write
BitField Bits: [22:16]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Mask                                             cBit22_16
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_Shift                                                   16

/*--------------------------------------
BitField Name: PlaOutPsnProPageCtr
BitField Type: RW
BitField Desc: there is 2 pages PSN location per PWID, depend on the
configuration of "PlaOutHspwPsnCtr", the engine will choice which the page to
encapsulate into the packet.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Mask                                               cBit15
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_Shift                                                  15

/*--------------------------------------
BitField Name: PlaOutPsnProFlowCtr
BitField Type: RW
BitField Desc: Flow channel
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Mask                                             cBit13_0
#define cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output PSN Buffer Control
Reg Addr   : 0x4_0010-0x4_0015
Reg Formula: 0x4_0010 + $segid
    Where  : 
           + $segid(0-5): segment 16-byte PSN, total is 6 segments for maximum 96 bytes PSN  %
Reg Desc   : 
This register is used to store PSN data which is before CPU request write or after CPU request read %%
The header format from entry#0 to entry#4 is as follow: %%
{DA(6-byte),SA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 96 bytes)} %%
Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats: %%

PSN header is MEF-8: %%
EthType:  0x88D8 %%
4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for  remote  receive side. %%
PSN header is MPLS:  %%
EthType:  0x8847 %%
4/8/12-byte PSN Header with format: {OuterLabel2[31:0](optional), OuterLabel1[31:0](optional),  InnerLabel[31:0]}   %%
Where InnerLabel[31:12] is pseodowire identification for remote receive side. %%
Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit =  for InnerLabel %%
PSN header is UDP/Ipv4:  %%
EthType:  0x0800 %%
28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %%
{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %%
{IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %%
{IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]} %%
{IP_SrcAdr[31:0]} %%
{IP_DesAdr[31:0]} %%
{UDP_SrcPort[15:0], UDP_DesPort[15:0]} %%
{UDP_Sum[31:0] (optional)} %%
Case: %%
IP_Protocol[7:0]: 0x11 to signify UDP  %%
IP_Protocol[7:0]: 0x11 to signify UDP  %%
IP_Header_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields %%
{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} +%%
IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  +%%
{IP_TTL[7:0], IP_Protocol[7:0]} + %%
IP_SrcAdr[31:16] + IP_SrcAdr[15:0] +%%
IP_DesAdr[31:16] + IP_DesAdr[15:0]%%
UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%
UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%
UDP_Sum[31:0] (optional): CPU calculate these fields as a temporarily for HW before plus rest fields%%
IP_SrcAdr[127:112] +    IP_SrcAdr[111:96] +     %%
IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] +  %%
IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] +  %%
IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] +   %%
IP_DesAdr[127:112] +    IP_DesAdr[111:96] +     %%
IP_DesAdr[95:80]  + IP_DesAdr[79:64] +  %%
IP_DesAdr[63:48]  + IP_DesAdr[47:32] +  %%
IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%
{8-bit zeros, IP_Protocol[7:0]} +%%
UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%
PSN header is UDP/Ipv6:  %%
EthType:  0x86DD%%
48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below:%%
{IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]}%%
{16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]} %%
{IP_SrcAdr[127:96]}%%
{IP_SrcAdr[95:64]}%%
{IP_SrcAdr[63:32]}%%
{IP_SrcAdr[31:0]}%%
{IP_DesAdr[127:96]}%%
{IP_DesAdr[95:64]}%%
{IP_DesAdr[63:32]}%%
{IP_DesAdr[31:0]}%%
{UDP_SrcPort[15:0], UDP_DesPort[15:0]}%%
{UDP_Sum[31:0]}%%
Case:%%
IP_Next_Header[7:0]: 0x11 to signify UDP %%
UDP_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields%%
IP_SrcAdr[127:112] +    IP_SrcAdr[111:96] +     %%
IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] +  %%
IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] +  %%
IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] +   %%
IP_DesAdr[127:112] +    IP_DesAdr[111:96] +     %%
IP_DesAdr[95:80]  + IP_DesAdr[79:64] +  %%
IP_DesAdr[63:48]  + IP_DesAdr[47:32] +  %%
IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%
{8-bit zeros, IP_Next_Header[7:0]} +%%
UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%
UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%
UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%
User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field. %%
PSN header is MPLS over Ipv4:%%
IPv4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS%%
After IPv4 header is MPLS labels where inner label is used for PW identification%%
PSN header is MPLS over Ipv6:%%
IPv6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS%%
After IPv6 header is MPLS labels where inner label is used for PW identification%%
PSN header (all modes) with RTP enable: RTP used for TDM PW (define in RFC3550), is in the first 8-byte of this buffer (bit[127:64] of segid == 0), following is PSN header (start from bit[63:0] of segid = 0)%%
Case:%%
RTPSSRC[31:0] : This is the SSRC value of RTP header%%
RtpPtValue[6:0]: This is the PT value of RTP header, define in http://www.iana.org/assignments/rtp-parameters/rtp-parameters.xml

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_psnbuf_ctrl_Base                                                               0x40010

/*--------------------------------------
BitField Name: PlaOutPsnProReqCtr
BitField Type: RW
BitField Desc: PSN buffer
BitField Bits: [127:0]
--------------------------------------*/
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_01                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_01                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_02                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_02                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_03                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_03                                                 0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Mask_04                                           cBit31_0
#define cAf6_pla_out_psnbuf_ctrl_PlaOutPsnProReqCtr_Shift_04                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Hold Register Control
Reg Addr   : 0x7_0000-0x7_0002
Reg Formula: 0x7_0000 + $holdreg
    Where  : 
           + $holdreg(0-2): hold register with (holdreg=0) for bit63_32, (holdreg=1) for bit95_64, (holdreg=2) for bit127_96
Reg Desc   : 
This register is used to control hold register.

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_hold_reg_ctrl_Base                                                                 0x70000

/*--------------------------------------
BitField Name: PlaHoldRegCtr
BitField Type: RW
BitField Desc: hold register value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_pla_hold_reg_ctrl_PlaHoldRegCtr_Mask                                                     cBit31_0
#define cAf6_pla_hold_reg_ctrl_PlaHoldRegCtr_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit3_0 Control
Reg Addr   : 0x70100 - 0x7010F
Reg Formula: 0x70100 + HaAddr3_0
    Where  : 
           + $HaAddr3_0(0-15): HA Address Bit3_0
Reg Desc   : 
This register is used to send HA read address bit3_0 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha3_0_control_Base                                                                   0x70100
#define cAf6Reg_rdha3_0_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr3_0
BitField Type: RO
BitField Desc: Read value will be 0x8C100 plus HaAddr3_0
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha3_0_control_ReadAddr3_0_Mask                                                         cBit19_0
#define cAf6_rdha3_0_control_ReadAddr3_0_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit7_4 Control
Reg Addr   : 0x70110 - 0x7011F
Reg Formula: 0x70110 + HaAddr7_4
    Where  : 
           + $HaAddr7_4(0-15): HA Address Bit7_4
Reg Desc   : 
This register is used to send HA read address bit7_4 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha7_4_control_Base                                                                   0x70110
#define cAf6Reg_rdha7_4_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr7_4
BitField Type: RO
BitField Desc: Read value will be 0x8C100 plus HaAddr7_4
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha7_4_control_ReadAddr7_4_Mask                                                         cBit19_0
#define cAf6_rdha7_4_control_ReadAddr7_4_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit11_8 Control
Reg Addr   : 0x70120 - 0x7012F
Reg Formula: 0x70120 + HaAddr11_8
    Where  : 
           + $HaAddr11_8(0-15): HA Address Bit11_8
Reg Desc   : 
This register is used to send HA read address bit11_8 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha11_8_control_Base                                                                  0x70120
#define cAf6Reg_rdha11_8_control_WidthVal                                                                   32

/*--------------------------------------
BitField Name: ReadAddr11_8
BitField Type: RO
BitField Desc: Read value will be 0x8C100 plus HaAddr11_8
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha11_8_control_ReadAddr11_8_Mask                                                       cBit19_0
#define cAf6_rdha11_8_control_ReadAddr11_8_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit15_12 Control
Reg Addr   : 0x070130 - 0x7013F
Reg Formula: 0x070130 + HaAddr15_12
    Where  : 
           + $HaAddr15_12(0-15): HA Address Bit15_12
Reg Desc   : 
This register is used to send HA read address bit15_12 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha15_12_control_Base                                                                0x070130
#define cAf6Reg_rdha15_12_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr15_12
BitField Type: RO
BitField Desc: Read value will be 0x8C100 plus HaAddr15_12
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha15_12_control_ReadAddr15_12_Mask                                                     cBit19_0
#define cAf6_rdha15_12_control_ReadAddr15_12_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit19_16 Control
Reg Addr   : 0x70140 - 0x7014F
Reg Formula: 0x70140 + HaAddr19_16
    Where  : 
           + $HaAddr19_16(0-15): HA Address Bit19_16
Reg Desc   : 
This register is used to send HA read address bit19_16 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha19_16_control_Base                                                                 0x70140
#define cAf6Reg_rdha19_16_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr19_16
BitField Type: RO
BitField Desc: Read value will be 0x8C100 plus HaAddr19_16
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha19_16_control_ReadAddr19_16_Mask                                                     cBit19_0
#define cAf6_rdha19_16_control_ReadAddr19_16_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit23_20 Control
Reg Addr   : 0x70150 - 0x7015F
Reg Formula: 0x70150 + HaAddr23_20
    Where  : 
           + $HaAddr23_20(0-15): HA Address Bit23_20
Reg Desc   : 
This register is used to send HA read address bit23_20 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha23_20_control_Base                                                                 0x70150
#define cAf6Reg_rdha23_20_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr23_20
BitField Type: RO
BitField Desc: Read value will be 0x8C100 plus HaAddr23_20
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha23_20_control_ReadAddr23_20_Mask                                                     cBit19_0
#define cAf6_rdha23_20_control_ReadAddr23_20_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit24 and Data Control
Reg Addr   : 0x70160 - 0x70161
Reg Formula: 0x70160 + HaAddr24
    Where  : 
           + $HaAddr24(0-1): HA Address Bit24
Reg Desc   : 
This register is used to send HA read address bit24 to HA engine to read data

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha24data_control_Base                                                                0x70160

/*--------------------------------------
BitField Name: ReadHaData31_0
BitField Type: RO
BitField Desc: HA read data bit31_0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha24data_control_ReadHaData31_0_Mask                                                   cBit31_0
#define cAf6_rdha24data_control_ReadHaData31_0_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data63_32
Reg Addr   : 0x70170
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha_hold63_32_Base                                                                    0x70170

/*--------------------------------------
BitField Name: ReadHaData63_32
BitField Type: RO
BitField Desc: HA read data bit63_32
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha_hold63_32_ReadHaData63_32_Mask                                                      cBit31_0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data95_64
Reg Addr   : 0x70171
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold95_64_Base                                                                  0x70171

/*--------------------------------------
BitField Name: ReadHaData95_64
BitField Type: RO
BitField Desc: HA read data bit95_64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Mask                                                    cBit31_0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data127_96
Reg Addr   : 0x70172
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold127_96_Base                                                                 0x70172

/*--------------------------------------
BitField Name: ReadHaData127_96
BitField Type: RO
BitField Desc: HA read data bit127_96
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Mask                                                  cBit31_0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Output PSN Process Control
Reg Addr   : 0x4_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control PSN configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_out_psnpro_ha_ctrl_Base                                                            0x40001

/*--------------------------------------
BitField Name: PlaOutPsnProReqCtr
BitField Type: RW
BitField Desc: Request to process PSN 1: request to write/read PSN header
buffer. The CPU need to prepare a complete PSN header into PSN buffer before
request write OR read out a complete PSN header in PSN buffer after request read
done 0: HW will automatically set to 0 when a request done
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProReqCtr_Mask                                             cBit31
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProReqCtr_Shift                                                31

/*--------------------------------------
BitField Name: PlaOutPsnProLenCtr
BitField Type: RW
BitField Desc: Length of a complete PSN need to read/write
BitField Bits: [22:16]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProLenCtr_Mask                                          cBit22_16
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProLenCtr_Shift                                                16

/*--------------------------------------
BitField Name: PlaOutPsnProPageCtr
BitField Type: RW
BitField Desc: there is 2 pages PSN location per PWID, depend on the
configuration of "PlaOutHspwPsnCtr", the engine will choice which the page to
encapsulate into the packet.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPageCtr_Mask                                            cBit13
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPageCtr_Shift                                               13

/*--------------------------------------
BitField Name: PlaOutPsnProPWIDCtr
BitField Type: RW
BitField Desc: Pseudowire channel
BitField Bits: [12:0]
--------------------------------------*/
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPWIDCtr_Mask                                          cBit12_0
#define cAf6_pla_out_psnpro_ha_ctrl_PlaOutPsnProPWIDCtr_Shift                                                0

/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order Payload Control
Reg Addr   : 0x2_8000-0x2_C000
Reg Formula: 0x2_8000 + $Oc192Slice*16384
    Where  : 
           + $Oc192Slice(0-1): OC-192 slices, there are total 2xOC-192 slices for 20G
Reg Desc   : 
This register is used to configure payload in each Pseudowire channels per slice

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_192c_pld_ctrl_Base                                                                 0x28000

/*--------------------------------------
BitField Name: Pla192PWIDCtrl
BitField Type: RW
BitField Desc: lookup pwid
BitField Bits: [29:16]
--------------------------------------*/
#define cAf6_pla_192c_pld_ctrl_Pla192PWIDCtrl_Mask                                                   cBit29_16
#define cAf6_pla_192c_pld_ctrl_Pla192PWIDCtrl_Shift                                                         16

/*--------------------------------------
BitField Name: Pla192PldCtrl
BitField Type: RW
BitField Desc: Payload Size
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_pla_192c_pld_ctrl_Pla192PldCtrl_Mask                                                     cBit13_0
#define cAf6_pla_192c_pld_ctrl_Pla192PldCtrl_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler OC192c Add/Remove Pseudowire Protocol Control
Reg Addr   : 0x2_8001-0x2_C001
Reg Formula: 0x2_8001 + $Oc192Slice*16384
    Where  : 
           + $Oc192Slice(0-1): OC-192 slices, there are total 2xOC-192 slices for 20G
Reg Desc   : 
This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each OC-192 slice

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_oc192c_add_rmv_pw_ctrl_Base                                                        0x28001

/*--------------------------------------
BitField Name: PlaLoStaAddRmvCtrl
BitField Type: RW
BitField Desc: protocol state to add/remove pw Step1: Add intitial or pw idle,
the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1"
value for the protocol state to start adding pw. The protocol state value is
"1". Step3: CPU enables pw at demap to finish the adding pw process. HW will
automatically change the protocol state value to "2" to run the pw, and keep
this state. Step4: For removing the pw, CPU will write "3" value for the
protocol state to start removing pw. The protocol state value is "3". Step5:
Poll the protocol state until return value "0" value, after that CPU disables pw
at demap to finish the removing pw process
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pla_oc192c_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Mask                                        cBit1_0
#define cAf6_pla_oc192c_add_rmv_pw_ctrl_PlaLoStaAddRmvCtrl_Shift                                             0

#endif /* _AF6_REG_AF6CNC0022_RD_PLA_H_ */

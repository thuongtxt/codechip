/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60290022ModulePweInternal.h
 * 
 * Created Date: Jul 25, 2018
 *
 * Description : 60290022 module PWE internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPWEINTERNAL_H_
#define _THA60290022MODULEPWEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/pwe/Tha60290021ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModulePwe *Tha60290022ModulePwe;

typedef struct tTha60290022ModulePweMethods
    {
    uint32 (*HwOc192OffsetFactor)(Tha60290022ModulePwe self);
    uint32 (*HwOc96OffsetFactor)(Tha60290022ModulePwe self);
    uint32 (*HwOc48InOc96OffsetFactor)(Tha60290022ModulePwe self);
    uint32 (*HwNumPwInOc48)(Tha60290022ModulePwe self);
    uint32 (*HwNumOc96Slices)(Tha60290022ModulePwe self);

    /* Polymorphism of PLA payload control */
    uint32 (*PlaLkPWIDCtrlMask)(Tha60290022ModulePwe self);
    uint8 (*PlaLkPWIDCtrlShift)(Tha60290022ModulePwe self);
    uint32 (*PlaOutUpsrGrpCtrlMask)(Tha60290022ModulePwe self);
    uint8 (*PlaOutUpsrGrpCtrlShift)(Tha60290022ModulePwe self);
    uint32 (*PlaOutHspwGrpCtrlMask)(Tha60290022ModulePwe self);
    uint8 (*PlaOutHspwGrpCtrlShift)(Tha60290022ModulePwe self);

    uint32 (*PlaPwHeaderControl)(Tha60290022ModulePwe self);
    uint32 (*PlaOc192cPayloadControl)(Tha60290022ModulePwe self);
    uint32 (*PlaOc192cAddRemovePwControl)(Tha60290022ModulePwe self);
    uint32 (*PlaOutClk311Control)(Tha60290022ModulePwe self);
    uint32 (*PlaOc48Clk311Sticky)(Tha60290022ModulePwe self);
    uint32 (*PlaOc192cClk311Sticky)(Tha60290022ModulePwe self);

    /* Backward compatible */
    eBool (*HasPlaHiRateCtrl)(Tha60290022ModulePwe self);
    }tTha60290022ModulePweMethods;

typedef struct tTha60290022ModulePwe
    {
    tTha60290021ModulePwe super;
    const tTha60290022ModulePweMethods *methods;
    }tTha60290022ModulePwe;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290022ModulePweObjectInit(AtModule self, AtDevice device);
uint32 Tha60290022ModulePweHwPwTdmDefaultOffset(Tha60290022ModulePwe self, AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPWEINTERNAL_H_ */

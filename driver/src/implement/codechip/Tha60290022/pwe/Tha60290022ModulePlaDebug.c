/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60290022ModulePlaDebug.c
 *
 * Created Date: Jun 8, 2017
 *
 * Description : PLA debug function
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../default/util/ThaUtil.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "Tha60290022ModulePweInternal.h"
#include "Tha60290022ModulePlaDebugReg.h"
#include "Tha60290022ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/
#define cPlaMaxNum512ByteCacheTotal 0x800
#define cPlaMaxNum256ByteCacheTotal 0x2000
#define cPlaMaxNum128ByteCacheTotal 0x2A00
#define cPlaMaxHiBlockTotal         0x6000
#define cPlaMaxLoBlockTotal         0x10000

/*--------------------------- Macros -----------------------------------------*/
#define mAddressWithLocalAddress(seff, localAddress) AddressWithLocalAddress((ThaModulePwe)self, localAddress)
#define mThis(self) ((Tha60290022ModulePwe)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 AddressWithLocalAddress(ThaModulePwe self, uint32 localAddress)
    {
    return ThaModulePwePlaBaseAddress(self) + localAddress;
    }

static uint32 PwOffset(ThaModulePwe self, AtPw pw)
    {
    uint8 slice48, slice96;
    uint32 oc96factor, oc48factor;

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice48, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    slice96 = slice48 / 2;
    slice48 = slice48 % 2;
    oc96factor = mMethodsGet(mThis(self))->HwOc96OffsetFactor(mThis(self));
    oc48factor = mMethodsGet(mThis(self))->HwOc48InOc96OffsetFactor(mThis(self));
    return (slice96 * oc96factor) + (slice48 * oc48factor);
    }

static void PwSet(ThaModulePwe self, AtPw pw)
    {
    uint32 pla_out_clk311_ctrl_Base = mMethodsGet(mThis(self))->PlaOutClk311Control(mThis(self));
    uint32 regAddr = mAddressWithLocalAddress(self, pla_out_clk311_ctrl_Base + PwOffset(self, pw));
    mChannelHwWrite(pw, regAddr, AtChannelHwIdGet((AtChannel)pw), cThaModulePwe);
    }

static void PwShow(ThaModulePwe self, AtPw pw)
    {
    AtDebugger debugger = NULL;
    uint32 offset = PwOffset(self, pw);
    uint32 pla_oc48_clk311_stk_Base = mMethodsGet(mThis(self))->PlaOc48Clk311Sticky(mThis(self));
    uint32 regAddr = mAddressWithLocalAddress(self, pla_oc48_clk311_stk_Base + offset);
    uint32 regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    Tha6021DebugPrintRegName (debugger, "* Payload Assembler OC48 clk311 Sticky", regAddr, regVal);

    Tha6021DebugPrintErrorBit(debugger, " OC48 conver clock error", regVal, cAf6_pla_oc48_clk311_stk_Pla311OC48_ConvErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC48 input AIS based PW", regVal, cAf6_pla_oc48_clk311_stk_Pla311OC48_InAisPW_Mask);
    Tha6021DebugPrintErrorBit(debugger, " OC48 input AIS", regVal, cAf6_pla_oc48_clk311_stk_Pla311OC48_InAIS_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48 input CEP Pos based PW", regVal, cAf6_pla_oc48_clk311_stk_Pla311OC48_InPosPW_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48 input CEP Neg based PW", regVal, cAf6_pla_oc48_clk311_stk_Pla311OC48_InNegPW_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48 input Valid based PW", regVal, cAf6_pla_oc48_clk311_stk_Pla311OC48_InValodPW_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48 input valid", regVal, cAf6_pla_oc48_clk311_stk_Pla311OC48_InValid_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48 input CEP J1 Posiion based PW", regVal, cAf6_pla_oc48_clk311_stk_Pla311OC48_InJ1PosPW_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48 input CEP Pos Pointer", regVal, cAf6_pla_oc48_clk311_stk_Pla311OC48_InPos_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48 input CEP Neg Pointer", regVal, cAf6_pla_oc48_clk311_stk_Pla311OC48_InNeg_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC48 input CEP J1 Position", regVal, cAf6_pla_oc48_clk311_stk_Pla311OC48_InJ1Pos_Mask);

    Tha6021DebugPrintStop(debugger);

    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);
    }

static void BufStk1Debug(ThaModulePwe self)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_pla_buf_stk1_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    AtDebugger debugger = NULL;

    Tha6021DebugPrintRegName (debugger, "* Payload Assembler Buffer Sticky 1", regAddr, regVal);

    Tha6021DebugPrintErrorBit(debugger, " Get All Block Type Error", regVal, cAf6_pla_buf_stk1_PlaGetAllBlkErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Get Hi-Block Error", regVal, cAf6_pla_buf_stk1_PlaGetHiBlkErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Get Lo-Block Error", regVal, cAf6_pla_buf_stk1_PlaGetLoBlkErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " All Cache Empty Error", regVal, cAf6_pla_buf_stk1_PlaAllCaEmpt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Get cache-512 Error", regVal, cAf6_pla_buf_stk1_PlaGetCa512Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Get cache-256 Error", regVal, cAf6_pla_buf_stk1_PlaGetCa256Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Get cache-128 Error", regVal, cAf6_pla_buf_stk1_PlaGetCa128Err_Mask);
    Tha6021DebugPrintWarningBit(debugger, "Monitor high block near full", regVal, cAf6_pla_buf_stk1_PlaWrHiBlkNearFul_Mask);
    Tha6021DebugPrintWarningBit(debugger, "Monitor low block near full", regVal, cAf6_pla_buf_stk1_PlaWrLoBlkNearFul_Mask);
    Tha6021DebugPrintWarningBit(debugger, "Monitor total cache near full", regVal, cAf6_pla_buf_stk1_PlaWrCaNearFul_Mask);
    Tha6021DebugPrintWarningBit(debugger, "block fail due to near empty cache/block", regVal, cAf6_pla_buf_stk1_PlaWrBlkErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " High Block Empty Error", regVal, cAf6_pla_buf_stk1_PlaHiBlkEmpt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Low Block Empty Error", regVal, cAf6_pla_buf_stk1_PlaLoBlkEmpt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " High Block Same Error", regVal, cAf6_pla_buf_stk1_PlaHiBlkSame_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Low Block Same Error", regVal, cAf6_pla_buf_stk1_PlaLoBlkSame_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Cache 512 Empty Error", regVal, cAf6_pla_buf_stk1_PlaCa512Empt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Cache 256 Empty Error", regVal, cAf6_pla_buf_stk1_PlaCa256Empt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Cache 128 Empty Error", regVal, cAf6_pla_buf_stk1_PlaCa128Empt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Cache 512 Same Error", regVal, cAf6_pla_buf_stk1_PlaCa512Same_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Cache 256 Same Error", regVal, cAf6_pla_buf_stk1_PlaCa256Same_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Cache 128 Same Error", regVal, cAf6_pla_buf_stk1_PlaCa128Same_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Max Length Error", regVal, cAf6_pla_buf_stk1_PlaMaxLengthErr_Mask);
    Tha6021DebugPrintWarningBit(debugger, "Request write DDR near full", regVal, cAf6_pla_buf_stk1_PlaReqWrDdrNearFul_Mask);
    Tha6021DebugPrintBitWithLabelAndColor("Free Block Set", regVal, cAf6_pla_buf_stk1_PlaFreeBlockSet_Mask, "SET", "CLEAR", cSevInfo, cSevCritical);
    Tha6021DebugPrintBitWithLabelAndColor("Free Cache Set", regVal, cAf6_pla_buf_stk1_PlaFreeCacheSet_Mask, "SET", "CLEAR", cSevInfo, cSevCritical);

    Tha6021DebugPrintStop(debugger);
    mModuleHwWrite(self, regAddr, regVal);
    }

static void BufStk2Debug(ThaModulePwe self)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_pla_buf_stk2_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    AtDebugger debugger = NULL;

    Tha6021DebugPrintRegName (debugger, "* Payload Assembler Buffer Sticky 2", regAddr, regVal);

    Tha6021DebugPrintErrorBit(debugger, " Packet Buffer Fifo Full", regVal, cAf6_pla_buf_stk2_PlaPkBufFFFul_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Packet Buffer Fifo Ready", regVal, cAf6_pla_buf_stk2_PlaPkBufFFRdy_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Packet Buffer Full", regVal, cAf6_pla_buf_stk2_PlaPkBufFull_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Packet Buffer Not Empty", regVal, cAf6_pla_buf_stk2_PlaPkBufNotEmpt_Mask);
    Tha6021DebugPrintWarningBit(debugger, "Read Port Side Packet Disabled", regVal, cAf6_pla_buf_stk2_PlaRdPortPktDis_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Read Port Side Packet Fifo Error", regVal, cAf6_pla_buf_stk2_PlaRdPortPktFFErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Read Port Side Data Fifo Error", regVal, cAf6_pla_buf_stk2_PlaRdPortDatFFErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Read Port Side Info Fifo Error", regVal, cAf6_pla_buf_stk2_PlaRdPortInfFFErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Read Buf Side Pkt Vld Fifo Error", regVal, cAf6_pla_buf_stk2_PlaRdBufDatPkVldFFErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Read Buf Side Data Request Vld Fifo Error", regVal, cAf6_pla_buf_stk2_PlaRdBufDatReqVldFFErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Read Buf Side PSN Ack Fifo Error", regVal, cAf6_pla_buf_stk2_PlaRdBufPSNAckFFErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Read Buf Side PSN Vld Fifo Error", regVal, cAf6_pla_buf_stk2_PlaRdBufPSNVldFFErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Fifo Mux PW SLice48#1 of Slice96#3 Error", regVal, cAf6_pla_buf_stk2_PlaMuxPWSl96_3_SL48_1_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Mux PW Slice48#0 of Slice96#3 Error", regVal, cAf6_pla_buf_stk2_PlaMuxPWSl96_3_SL48_0_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Fifo Mux PW Slice48#1 of Slice96#2 Error", regVal, cAf6_pla_buf_stk2_PlaMuxPWSl96_2_SL48_1_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Mux PW Slice48#0 of Slice96#2 Error", regVal, cAf6_pla_buf_stk2_PlaMuxPWSl96_2_SL48_0_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Fifo Mux PW SLice48#1 of Slice96#1 Error", regVal, cAf6_pla_buf_stk2_PlaMuxPWSl96_1_SL48_1_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Mux PW Slice48#0 of Slice96#1 Error", regVal, cAf6_pla_buf_stk2_PlaMuxPWSl96_1_SL48_0_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Fifo Mux PW Slice48#1 of Slice96#0 Error", regVal, cAf6_pla_buf_stk2_PlaMuxPWSl96_0_SL48_1_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Mux PW Slice48#0 of Slice96#0 Error", regVal, cAf6_pla_buf_stk2_PlaMuxPWSl96_0_SL48_0_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Fifo Mux PW Slice96#3 Error", regVal, cAf6_pla_buf_stk2_PlaMuxPWSl96_3_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Fifo Mux PW Slice96#2 Error", regVal, cAf6_pla_buf_stk2_PlaMuxPWSl96_2_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Fifo Mux PW Slice96#1 Error", regVal, cAf6_pla_buf_stk2_PlaMuxPWSl96_1_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Fifo Mux PW Slice96#0 Error", regVal, cAf6_pla_buf_stk2_PlaMuxPWSl96_0_Err_Mask);

    Tha6021DebugPrintStop(debugger);
    mModuleHwWrite(self, regAddr, regVal);
    }

static void BufStk3Debug(ThaModulePwe self)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_pla_buf_stk3_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    AtDebugger debugger = NULL;

    Tha6021DebugPrintRegName (debugger, "* Payload Assembler Buffer Sticky 3", regAddr, regVal);

    Tha6021DebugPrintInfoBit(debugger, "QDR PSN Request set", regVal, cAf6_pla_buf_stk3_PlaQDRPSNReqSet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "QDR PSN Ack set", regVal, cAf6_pla_buf_stk3_PlaQDRPSNAckSet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "QDR PSN valid set", regVal, cAf6_pla_buf_stk3_PlaQDRPSNVldSet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "DDR Read Request set", regVal, cAf6_pla_buf_stk3_PlaDDRRdReqSet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "DDR Read Ack set", regVal, cAf6_pla_buf_stk3_PlaDDRRdAckSet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "DDR Read valid set", regVal, cAf6_pla_buf_stk3_PlaDDRRdVldSet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Output block module EOP set", regVal, cAf6_pla_buf_stk3_PlaBlkMdOutEOPSet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "DDR Write Request set", regVal, cAf6_pla_buf_stk3_PlaDDRWrReqSet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "DDR Write Ack set", regVal, cAf6_pla_buf_stk3_PlaDDRWrAckSet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "DDR Write valid set", regVal, cAf6_pla_buf_stk3_PlaDDRWrVldSet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Input block module valid set", regVal, cAf6_pla_buf_stk3_PlaBlkMdInVldSet_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Input block module EOP set", regVal, cAf6_pla_buf_stk3_PlaBlkMdInEOPSet_Mask);
    Tha6021DebugPrintErrorBit(debugger, " PWE to get PSN Error due to empty", regVal, cAf6_pla_buf_stk3_PlaPWEGetPSNErr1_Mask);
    Tha6021DebugPrintErrorBit(debugger, " PWE to get PSN Error due to PSN len", regVal, cAf6_pla_buf_stk3_PlaPWEGetPSNErr0_Mask);
    Tha6021DebugPrintOkBit("PWE to get PSN from PLA", regVal, cAf6_pla_buf_stk3_PlaPWEGetPSN_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Ready data for PWE", regVal, cAf6_pla_buf_stk3_PlaReadyDat4PWE_Mask);
    Tha6021DebugPrintBitWithLabelAndColor("PWE to get data from PLA", regVal, cAf6_pla_buf_stk3_PlaPWEGetDat_Mask, "SET", "CLEAR", cSevInfo, cSevCritical);
    Tha6021DebugPrintInfoBit(debugger, "Read Port ready to read packet Info", regVal, cAf6_pla_buf_stk3_PlaReady2RdPkt_Mask);
    Tha6021DebugPrintBitWithLabelAndColor("Read Packet Info Valid Set", regVal, cAf6_pla_buf_stk3_PlaRdpkInfVldSet_Mask, "SET", "CLEAR", cSevInfo, cSevCritical);
    Tha6021DebugPrintBitWithLabelAndColor("Write Packet Info Valid Set", regVal, cAf6_pla_buf_stk3_PlaWrpkInfVldSet_Mask, "SET", "CLEAR", cSevInfo, cSevCritical);
    Tha6021DebugPrintBitWithLabelAndColor("Output block module valid set", regVal, cAf6_pla_buf_stk3_PlaBlkMdOutVldSet_Mask, "SET", "CLEAR", cSevInfo, cSevCritical);
    Tha6021DebugPrintBitWithLabelAndColor("Output pwcore valid set", regVal, cAf6_pla_buf_stk3_PlaPWCoreOutVldSet_Mask, "SET", "CLEAR", cSevInfo, cSevCritical);

    Tha6021DebugPrintStop(debugger);
    mModuleHwWrite(self, regAddr, regVal);
    }

void Tha60290022ModulePwePwPlaDebug(ThaModulePwe self, AtPw pw)
    {
    PwSet(self, pw);
    PwShow(self, pw);
    }

void Tha60290022ModulePwePlaDebug(ThaModulePwe self)
    {
    BufStk1Debug(self);
    BufStk2Debug(self);
    BufStk3Debug(self);
    }

void Tha60290022ModulePwePlaCacheUsageGet(ThaModulePwe self, tThaPweCache *cache)
    {
    uint32 longReg[cThaNumDwordsInLongReg];
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);
    uint32 regAddr, lsb, msb;

    /* Read block info */
    regAddr = mAddressWithLocalAddress(self, cAf6Reg_pla_buf_blk_num_sta_Base);
    mModuleHwLongRead(self, regAddr, longReg, cThaNumDwordsInLongReg, core);
    cache->freeNumLoBlock = mRegField(longReg[0], cAf6_pla_buf_blk_num_sta_LoBlkNum_);
    mFieldGet(longReg[0], cAf6_pla_buf_blk_num_sta_HiBlkNum_Mask_01, cAf6_pla_buf_blk_num_sta_HiBlkNum_Shift_01, uint32, &lsb);
    mFieldGet(longReg[1], cAf6_pla_buf_blk_num_sta_HiBlkNum_Mask_02, cAf6_pla_buf_blk_num_sta_HiBlkNum_Shift_02, uint32, &msb);
    cache->freeNumHiBlock = (msb << 12) | lsb;

    /* Read cache info */
    regAddr = mAddressWithLocalAddress(self, cAf6Reg_pla_buf_blk_cache_sta_Base);
    mModuleHwLongRead(self, regAddr, longReg, cThaNumDwordsInLongReg, core);
    cache->free256ByteCache = mRegField(longReg[0], cAf6_pla_buf_blk_cache_sta_Ca256Num_);
    cache->free128ByteCache = mRegField(longReg[0], cAf6_pla_buf_blk_cache_sta_Ca128Num_);
    cache->free512ByteCache = mRegField(longReg[1], cAf6_pla_buf_blk_cache_sta_Ca512Num_);

    /* Calculate used info */
    cache->used512ByteCache = cPlaMaxNum512ByteCacheTotal - cache->free512ByteCache;
    cache->used256ByteCache = cPlaMaxNum256ByteCacheTotal - cache->free256ByteCache;
    cache->used128ByteCache = cPlaMaxNum128ByteCacheTotal - cache->free128ByteCache;
    cache->usedNumHiBlock = cPlaMaxHiBlockTotal - cache->freeNumHiBlock;
    cache->usedNumLoBlock = cPlaMaxLoBlockTotal - cache->freeNumLoBlock;

    /* Not supported cache types must be set to invalid value to let the outside know */
    cache->free64ByteCache = cInvalidUint32;
    cache->used64ByteCache = cInvalidUint32;
    }

void Tha60290022ModulePwePwOc192PlaDebug(ThaModulePwe self, AtPw pw)
    {
    AtDebugger debugger = NULL;
    uint32 pla_oc192c_clk311_stk_Base = mMethodsGet(mThis(self))->PlaOc192cClk311Sticky(mThis(self));
    uint32 regAddr = pla_oc192c_clk311_stk_Base + Tha60210011ModulePlaPldControlOffset(self, pw);
    uint32 regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);

    Tha6021DebugPrintRegName(debugger, "* Payload Assembler OC192c clk311 Sticky", regAddr, regVal);

    Tha6021DebugPrintErrorBit(debugger, "OC192 output AIS", regVal, cAf6_pla_oc192c_clk311_stk_Pla311OC192c_OutAIS_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC192 output CEP Pos Pointer", regVal, cAf6_pla_oc192c_clk311_stk_Pla311OC192c_OutPos_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC192 output CEP Neg Pointer", regVal, cAf6_pla_oc192c_clk311_stk_Pla311OC192c_OutNeg_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC192 input valid", regVal, cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InValid_Mask);
    Tha6021DebugPrintErrorBit(debugger, "OC192 input AIS", regVal, cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InAIS_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC192 input CEP Pos Pointer", regVal, cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InPos_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC192 input CEP Neg Pointer", regVal, cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InNeg_Mask);
    Tha6021DebugPrintInfoBit(debugger, "OC192 input CEP J1 Position", regVal, cAf6_pla_oc192c_clk311_stk_Pla311OC192c_InJ1Pos_Mask);

    Tha6021DebugPrintStop(debugger);

    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : Tha60290022ModuleCos.c
 *
 * Created Date: Apr 27, 2017
 *
 * Description : COS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../pwe/Tha60290022ModulePlaReg.h"
#include "Tha60290022ModuleCosInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleCosMethods m_ThaModuleCosOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PwHeaderLengthReset(ThaModuleCos self, uint8 partId, uint32 localPwId)
    {
    /* Already do this at PWE module by the HW flush action */
    AtUnused(self);
    AtUnused(partId);
    AtUnused(localPwId);
    return cAtOk;
    }

static ThaModulePwe PweModule(ThaModuleCos self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static uint32 PlaBaseAddress(ThaModuleCos self)
    {
    return ThaModulePwePlaBaseAddress(PweModule(self));
    }

static uint32 PwPsnCtrl(ThaModuleCos self, AtPw pw)
    {
    return cAf6Reg_pla_pw_psn_ctrl_Base + AtChannelIdGet((AtChannel)pw) + PlaBaseAddress(self);
    }

static eAtRet PwHeaderLengthHwSet(ThaModuleCos self, AtPw pw, uint8 hwValue)
    {
    uint32 regAddr = PwPsnCtrl(self, pw);
    uint32 regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_pw_psn_ctrl_PlaOutPSNLenCtrl_, hwValue);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);
    return cAtOk;
    }

static uint8 PwHeaderLengthHwGet(ThaModuleCos self, AtPw pw)
    {
    uint32 regAddr = PwPsnCtrl(self, pw);
    uint32 regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (uint8)mRegField(regVal, cAf6_pla_pw_psn_ctrl_PlaOutPSNLenCtrl_);
    }

static void OverrideThaModuleCos(AtModule self)
    {
    ThaModuleCos cos = (ThaModuleCos)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCosOverride, mMethodsGet(cos), sizeof(m_ThaModuleCosOverride));

        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthReset);
        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthHwSet);
        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthHwGet);
        }

    mMethodsSet(cos, &m_ThaModuleCosOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCos(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleCos);
    }

AtModule Tha60290022ModuleCosObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleCosObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290022ModuleCosNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModuleCosObjectInit(newModule, device);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : COS
 * 
 * File        : Tha60290022ModuleCosInternal.h
 * 
 * Created Date: Jul 19, 2019
 *
 * Description : 60290022 Module COS representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULECOSINTERNAL_H_
#define _THA60290022MODULECOSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/cos/Tha60210011ModuleCosInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModuleCos
    {
    tTha60210011ModuleCos super;
    }tTha60290022ModuleCos;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290022ModuleCosObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULECOSINTERNAL_H_ */


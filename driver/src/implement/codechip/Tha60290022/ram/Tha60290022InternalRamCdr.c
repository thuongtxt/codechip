/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290022InternalRamCdr.c
 *
 * Created Date: Mar 23, 2018
 *
 * Description : CDR internal RAM for product 60290022.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022InternalRam.h"
#include "Tha60290022InternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tAtInternalRamMethods             m_AtInternalRamOverride;
static tTha60210011InternalRamCdrMethods m_Tha60210011InternalRamCdrOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCdr(AtInternalRam self)
    {
    return AtInternalRamPhyModuleGet(self);
    }

static eBool IsReserved(AtInternalRam self)
    {
    return AtModuleInternalRamIsReserved(ModuleCdr(self), self);
    }

static uint32 TimingGenParityForceRegister(Tha60210011InternalRamCdr self)
    {
    AtUnused(self);
    return 0x104c;
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, IsReserved);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideTha60210011InternalRamCdr(AtInternalRam self)
    {
    Tha60210011InternalRamCdr ram = (Tha60210011InternalRamCdr)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011InternalRamCdrOverride, mMethodsGet(ram), sizeof(m_Tha60210011InternalRamCdrOverride));

        /* Common */
        mMethodOverride(m_Tha60210011InternalRamCdrOverride, TimingGenParityForceRegister);
        }

    mMethodsSet(ram, &m_Tha60210011InternalRamCdrOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideTha60210011InternalRamCdr(self);
    OverrideAtInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022InternalRamCdr);
    }

AtInternalRam Tha60290022InternalRamCdrObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011InternalRamCdrObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60290022InternalRamCdrNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return Tha60290022InternalRamCdrObjectInit(newRam, phyModule, ramId, localId);
    }

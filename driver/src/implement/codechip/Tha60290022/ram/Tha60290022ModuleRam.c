/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290022ModuleRam.c
 *
 * Created Date: May 9, 2017
 *
 * Description : RAM module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022ModuleRamInternal.h"
#include "../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleRamMethods  m_AtModuleRamOverride;

/* Save super implementation */
static const tAtModuleRamMethods *m_AtModuleRamMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumDdrGet(AtModuleRam self)
    {
    /* Just for debugging, we may need to reduce RAMs */
    return m_AtModuleRamMethods->NumDdrGet(self);
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRamMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, m_AtModuleRamMethods, sizeof(m_AtModuleRamOverride));

        mMethodOverride(m_AtModuleRamOverride, NumDdrGet);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModuleRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleRam);
    }

AtModuleRam Tha60290022ModuleRamObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleRamObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam Tha60290022ModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModuleRamObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60290022RamErrorGenerator.h
 * 
 * Created Date: Aug 23, 2017
 *
 * Description : Tha60290022RamErrorGenerator declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022RAMERRORGENERATOR_H_
#define _THA60290022RAMERRORGENERATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtErrorGenerator Tha60290022PlaRamCrcErrorGeneratorNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022RAMERRORGENERATOR_H_ */


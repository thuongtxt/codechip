/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc..c'
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DIAG
 *
 * File        : Tha60290022PlaRamCrcErrorGenerator.c
 *
 * Created Date: Aug 23, 2017
 *
 * Description : Tha60290022PlaRamCrcErrorGenerator implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022RamErrorGenerator.h"
#include "../../../Tha60210011/ram/diag/Tha60210011RamErrorGeneratorInternal.h"
#include "../../../Tha60210011/pwe/Tha60210011ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_pla_force_crc_err_control_Base                   0x00042030

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha60290022PlaRamCrcErrorGenerator 
    {
    tTha60210011PlaRamCrcErrorGenerator super;
    }tTha60290022PlaRamCrcErrorGenerator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtErrorGeneratorMethods                m_AtErrorGeneratorOverride;
static tTha60210031RamCrcErrorGeneratorMethods m_Tha60210031RamCrcErrorGeneratorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PlaBaseAddress(AtErrorGenerator self)
    {
    AtUnused(self);
    return Tha60210011ModulePlaBaseAddress();
    }

static uint32 ForceErrorRateCtrlRegister(AtErrorGenerator self)
    {
    return PlaBaseAddress(self) + cAf6Reg_pla_force_crc_err_control_Base;
    }

static uint32 ErrorThresholdInsertCtrlRegister(AtErrorGenerator self)
    {
    return PlaBaseAddress(self) + cAf6Reg_pla_force_crc_err_control_Base;
    }

static eBool ModeIsSupported(AtErrorGenerator self, eAtErrorGeneratorMode mode)
    {
    AtUnused(self);
    if ((mode == cAtErrorGeneratorModeOneshot) || (mode == cAtErrorGeneratorModeContinuous))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PlaRamCrcErrorGenerator);
    }

static void AtErrorGeneratorOverride(AtErrorGenerator self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtErrorGeneratorOverride, mMethodsGet(self), sizeof(m_AtErrorGeneratorOverride));

        /* Setup methods */
        mMethodOverride(m_AtErrorGeneratorOverride, ModeIsSupported);
        }

    mMethodsSet(self, &m_AtErrorGeneratorOverride);
    }

static void OverrideTha60210031RamCrcErrorGenerator(AtErrorGenerator self)
    {
    Tha60210031RamCrcErrorGenerator overrideObject = (Tha60210031RamCrcErrorGenerator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031RamCrcErrorGeneratorOverride, mMethodsGet(overrideObject), sizeof(m_Tha60210031RamCrcErrorGeneratorOverride));

        mMethodOverride(m_Tha60210031RamCrcErrorGeneratorOverride, ForceErrorRateCtrlRegister);
        mMethodOverride(m_Tha60210031RamCrcErrorGeneratorOverride, ErrorThresholdInsertCtrlRegister);
        }

    mMethodsSet(overrideObject, &m_Tha60210031RamCrcErrorGeneratorOverride);
    }
    
static void Override(AtErrorGenerator self)
    {
    AtErrorGeneratorOverride(self);
    OverrideTha60210031RamCrcErrorGenerator(self);
    }

static AtErrorGenerator ObjectInit(AtErrorGenerator self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PlaRamCrcErrorGeneratorObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtErrorGenerator Tha60290022PlaRamCrcErrorGeneratorNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtErrorGenerator newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newObject, module);
    }
     

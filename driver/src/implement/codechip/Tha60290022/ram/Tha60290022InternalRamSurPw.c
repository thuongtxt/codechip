/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290022InternalRamSurPw.c
 *
 * Created Date: Aug 18, 2017
 *
 * Description : FM PW Internal RAM implementation for product 60290022.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ram/ThaInternalRamInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60210011/ram/Tha60210011InternalRamInternal.h"
#include "Tha60290022InternalRam.h"
#include "../sur/Tha60290022ModuleSurReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022InternalRamSurPw
    {
    tTha60210011InternalRamSur super;
    }tTha60290022InternalRamSurPw;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 FirstCellAddress(ThaInternalRam self)
    {
    ThaModuleHardSur pmModule = (ThaModuleHardSur)AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 pmBaseAddress = ThaModuleHardSurBaseAddress(pmModule);
    uint32 cellAddress = pmBaseAddress + cAf6Reg_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_Base;
    return cellAddress;
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022InternalRamSurPw);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011InternalRamSurObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60290022InternalRamSurPwNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

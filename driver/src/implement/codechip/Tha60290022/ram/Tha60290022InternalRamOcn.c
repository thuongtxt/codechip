/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc..c'
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290022InternalRamOcn.c
 *
 * Created Date: Aug 21, 2017
 *
 * Description : Tha60290022InternalRamOcn implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/ram/Tha60290021InternalRamInternal.h"
#include "../../Tha60290021/ocn/Tha60290021ModuleOcn.h"
#include "Tha60290022InternalRam.h"

/*--------------------------- Define -----------------------------------------*/
#define cOCN_RXPP_Per_STS_payload_Control_1_Id                         104

#define cRxHoParErrFrc0(localId)   (cBit16 << (localId))
#define cVpgCtlParErrFrc0(localId) (cBit12 << (localId))
#define cVpgDemParErrFrc0(localId) (cBit8 << (localId))
#define cVpiCtlParErrFrc0(localId) (cBit4 << (localId))
#define cVpiDemParErrFrc0(localId) (cBit0 << (localId))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha60290022InternalRamOcn 
    {
    tTha60290021InternalRamOcn super;
    }tTha60290022InternalRamOcn;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaInternalRamMethods m_ThaInternalRamOverride;

/* Super implementations */
static const tThaInternalRamMethods *m_ThaInternalRamMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtInternalRam self)
    {
    return ThaModuleOcnBaseAddress((ThaModuleOcn)AtInternalRamPhyModuleGet(self));
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    uint32 localId = AtInternalRamLocalIdGet(internalRam);

    if ((localId > cOCN_RXPP_Per_STS_payload_Control_1_Id) && (localId < 125))
        return BaseAddress(internalRam) + 0xF0030;

    return m_ThaInternalRamMethods->ErrorForceRegister(self);
    }

static uint32 HoSliceIdGet(ThaInternalRam self, uint32 offset)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    return (uint32)((localId - offset + 4) % 8);
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    uint32 localId = AtInternalRamLocalIdGet(internalRam);
    uint32 baseAddress = BaseAddress((AtInternalRam)self);
    uint32 firstCellAddress = m_ThaInternalRamMethods->FirstCellAddress(self);
    if (firstCellAddress != cInvalidUint32)
        return firstCellAddress;

    /* OCN Rx High Order Map concatenate configuration */
    if (localId < 109)
        return (uint32)(HoSliceIdGet(self, 105) * 0x200 + 0x48000 + baseAddress);

    /* OCN VTTU Pointer Generator Per Channel Control */
    if (localId < 113)
        return (uint32)(HoSliceIdGet(self, 109) * 0x4000 + 0x80800 + baseAddress);

    /* OCN TXPP Per STS Multiplexing Control */
    if (localId < 117)
        return (uint32)(HoSliceIdGet(self, 113) * 0x4000 + 0x80000 + baseAddress);

    /* OCN VTTU Pointer Interpreter Per Channel Control */
    if (localId < 121)
        return (uint32)(HoSliceIdGet(self, 117) * 0x4000 + 0x60800 + baseAddress);

    /* OCN RXPP Per STS payload Control */
    if (localId < 125)
        return (uint32)(HoSliceIdGet(self, 121) * 0x4000 + 0x60000 + baseAddress);

    return m_ThaInternalRamMethods->FirstCellAddress(self);
    }

static uint32 NumSuperRams(ThaInternalRam self)
    {
    AtModule ocn = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 numRams = 0;
    Tha60290021ModuleOcnAllInternalRamsDescription(ocn, &numRams);
    return numRams;
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    uint32 localId = AtInternalRamLocalIdGet(internalRam);
    uint32 mask = m_ThaInternalRamMethods->LocalIdMask(self);

    if (mask != 0)
        return mask;

    /* OCN Rx High Order Map concatenate configuration */
    if (localId < 109)
        return cRxHoParErrFrc0(localId - NumSuperRams(self));

    /* OCN VTTU Pointer Generator Per Channel Control */
    if (localId < 113)
        return cVpgCtlParErrFrc0(localId - 109);

    /* OCN TXPP Per STS Multiplexing Control */
    if (localId < 117)
        return cVpgDemParErrFrc0(localId - 113);

    /* OCN VTTU Pointer Interpreter Per Channel Control */
    if (localId < 121)
        return cVpiCtlParErrFrc0(localId - 117);

    /* OCN RXPP Per STS payload Control */
    if (localId < 125)
        return cVpiDemParErrFrc0(localId - 121);

    return localId - 125;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022InternalRamOcn);
    }
    
static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam overrideObject = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaInternalRamMethods = mMethodsGet(overrideObject);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, m_ThaInternalRamMethods, sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        }

    mMethodsSet(overrideObject, &m_ThaInternalRamOverride);
    }
    
static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021InternalRamOcnObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60290022InternalRamOcnNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newObject, phyModule, ramId, localId);
    }
     

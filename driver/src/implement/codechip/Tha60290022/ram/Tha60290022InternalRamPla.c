/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc..c'
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290022InternalRamPla.c
 *
 * Created Date: Aug 22, 2017
 *
 * Description : Tha60290022InternalRamPla implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "Tha60290022InternalRam.h"
#include "diag/Tha60290022RamErrorGenerator.h"
#include "Tha60290022InternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_PLA_crc_counter_Base                             0x00042032

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaInternalRamMethods m_ThaInternalRamOverride;
static tAtInternalRamMethods m_AtInternalRamOverride;

/* Super implementations */
static const tThaInternalRamMethods *m_ThaInternalRamMethods = NULL;
static const tAtInternalRamMethods *m_AtInternalRamMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModulePwe ModulePwe(ThaInternalRam self)
    {
    return (Tha60210011ModulePwe)AtInternalRamPhyModuleGet((AtInternalRam)self);
    }

static eBool RamIsPla(ThaInternalRam self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) < 12)
        return cAtTrue;

    return cAtFalse;
    }

static eBool RamIsPsn(ThaInternalRam self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) < Tha60210011ModulePweStartHSPWRamLocalId(ModulePwe(self)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool RamIsProtection(ThaInternalRam self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) < Tha60210011ModulePweStartCrcRamLocalId(ModulePwe(self)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool RamIsCrc(ThaInternalRam self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) >= Tha60210011ModulePweStartCrcRamLocalId(ModulePwe(self)))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 CounterTypeRegister(ThaInternalRam self,  uint16 counterType, eBool r2c)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtRamCounterTypeCrcErrors:   return Tha60210011ModulePlaBaseAddress() + cAf6Reg_PLA_crc_counter_Base + (r2c ? 1 : 0);
        default:
            return cBit31_0;
        }
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    if (RamIsPla(self) || RamIsPsn(self) || RamIsProtection(self))
        return Tha60210011ModulePlaBaseAddress() + 0x42040;

    /* CRC PLA */
    return Tha60210011ModulePlaBaseAddress() + 0x42030;
    }

static uint32 ErrorStickyRegister(ThaInternalRam self)
    {
    if (RamIsPla(self) || RamIsPsn(self) || RamIsProtection(self))
        return m_ThaInternalRamMethods->ErrorStickyRegister(self);

    /* CRC PLA */
    return Tha60210011ModulePlaBaseAddress() + 0x42031;
    }

static uint32 PlaLocalIdMask(uint32 localId)
    {
    return cBit0 << (((localId / 3) * 4) + (localId % 3)) ;
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);

    if (RamIsPla(self) || RamIsPsn(self) || RamIsProtection(self))
        return PlaLocalIdMask(localId);

    return cBit0;
    }

static uint32 PlaSliceOffsetGet(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    return (uint32)(0x8000 * (localId / 3));
    }

static uint32 FirstCellOffset(ThaInternalRam self)
    {
    uint32 ramId = AtInternalRamLocalIdGet((AtInternalRam)self);

    if (RamIsPla(self))
        {
        static const uint32 cellAddressOfPla[] = {0x1000, 0x3000, 0x4000};
        return cellAddressOfPla[ramId % 3] + PlaSliceOffsetGet(self);
        }

    if (RamIsPsn(self))
        {
        static const uint32 cellAddressOfPla[] = {0x20000, 0x48000};
        return cellAddressOfPla[ramId % 2];
        }

    if (RamIsProtection(self))
        {
        static const uint32 cellAddressOfProtection[] = {0x50000, 0x58000};
        return cellAddressOfProtection[ramId % 2];
        }

    return cInvalidUint32;
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    uint32 offset = FirstCellOffset(self);
    if (offset == cInvalidUint32)
        return cInvalidUint32;

    return (uint32)(Tha60210011ModulePlaBaseAddress() + offset);
    }

static eAtRet ErrorMonitorEnable(ThaInternalRam self, eBool enable)
    {
    if (RamIsCrc(self))
        return (enable) ? cAtOk : cAtErrorModeNotSupport;
    return m_ThaInternalRamMethods->ErrorMonitorEnable(self, enable);
    }

static eBool ErrorMonitorIsEnabled(ThaInternalRam self)
    {
    if (RamIsCrc(self))
        return cAtTrue;
    return m_ThaInternalRamMethods->ErrorMonitorIsEnabled(self);
    }

static AtErrorGenerator ErrorGeneratorObjectCreate(ThaInternalRam self)
    {
    AtModule moduleRam = (AtModule)AtInternalRamModuleRamGet((AtInternalRam)self);
    return Tha60290022PlaRamCrcErrorGeneratorNew(moduleRam);
    }

static uint32 ForcedErrorsGetByGenerator(AtInternalRam self)
    {
    AtErrorGenerator errorGenerator = AtInternalRamErrorGeneratorGet(self);

    if (AtErrorGeneratorModeGet(errorGenerator) != cAtErrorGeneratorModeContinuous)
        return cAtRamAlarmNone;

    if (!AtInternalRamCrcMonitorIsEnabled(self))
        return cAtRamAlarmNone;

    return cAtRamAlarmCrcError;
    }

static uint32 ForcedErrorsGet(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (mMethodsGet(ram)->ErrorGeneratorIsSupported(ram))
        return ForcedErrorsGetByGenerator(self);

    return m_AtInternalRamMethods->ForcedErrorsGet(self);
    }

static eBool CrcMonitorCanEnable(AtInternalRam self, eBool enable)
    {
    if (RamIsCrc((ThaInternalRam)self))
        return enable;

    return m_AtInternalRamMethods->CrcMonitorCanEnable(self, enable);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022InternalRamPla);
    }
    
static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam overrideObject = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaInternalRamMethods = mMethodsGet(overrideObject);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, m_ThaInternalRamMethods, sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, CounterTypeRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorStickyRegister);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
		mMethodOverride(m_ThaInternalRamOverride, ErrorMonitorEnable);
        mMethodOverride(m_ThaInternalRamOverride, ErrorMonitorIsEnabled);
        mMethodOverride(m_ThaInternalRamOverride, ErrorGeneratorObjectCreate);
        }

    mMethodsSet(overrideObject, &m_ThaInternalRamOverride);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtInternalRamMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        /* Common */
        mMethodOverride(m_AtInternalRamOverride, ForcedErrorsGet);
        mMethodOverride(m_AtInternalRamOverride, CrcMonitorCanEnable);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideAtInternalRam(self);
    OverrideThaInternalRam(self);
    }

AtInternalRam Tha60290022InternalRamPlaObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011InternalRamPlaObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60290022InternalRamPlaNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022InternalRamPlaObjectInit(newObject, phyModule, ramId, localId);
    }
     

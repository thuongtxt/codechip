/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290022InternalRamCdrV3.c
 *
 * Created Date: Mar 23, 2018
 *
 * Description : CDR internal RAM v3 for product 60290022.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/cdr/Tha60210011ModuleCdr.h"
#include "../cdr/Tha60290022ModuleCdrV3.h"
#include "Tha60290022InternalRam.h"
#include "Tha60290022InternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumInternalRamInSlice   3

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022InternalRamCdrV3
    {
    tTha60290022InternalRamCdr super;
    }tTha60290022InternalRamCdrV3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tThaInternalRamMethods            m_ThaInternalRamOverride;
static tTha60210011InternalRamCdrMethods m_Th60210011InternalRamCdrOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleCdr ModuleCdr(ThaInternalRam self)
    {
    return (ThaModuleCdr)AtInternalRamPhyModuleGet((AtInternalRam)self);
    }

static uint32 SliceIdGet(ThaInternalRam self)
    {
    return AtInternalRamLocalIdGet((AtInternalRam)self) / cNumInternalRamInSlice;
    }

static uint32 LocalIdInSlice(ThaInternalRam self)
    {
    return AtInternalRamLocalIdGet((AtInternalRam)self) % cNumInternalRamInSlice;
    }

static uint32 SliceOffset(ThaInternalRam self)
    {
    return SliceIdGet(self) * 0x40000;
    }

static uint32 AcrSliceOffset(ThaInternalRam self)
    {
    return SliceIdGet(self) * 0x8000;
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    uint32 baseAddress = ThaModuleCdrBaseAddress(ModuleCdr(self)) + SliceOffset(self);
    Tha60210011InternalRamCdr ram = (Tha60210011InternalRamCdr)self;

    if (LocalIdInSlice(self) < 2)
        return baseAddress + mMethodsGet(ram)->TimingGenParityForceRegister(ram);

    return cCdrV3Base + mMethodsGet(ram)->ACREngineTimingParityForceRegister(ram);
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    uint32 localIdInSlice;

    if ((localIdInSlice = LocalIdInSlice(self)) < 2)
        return cBit0 << localIdInSlice;

    return cBit0 << SliceIdGet(self);
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    uint32 cellAddressOffsetInSlice[] = {cInvalidUint32, cInvalidUint32, cInvalidUint32};
    uint32 localIdInSlice;
    ThaModuleCdr cdrModule = ModuleCdr(self);

    cellAddressOffsetInSlice[0] = Tha60210011ModuleCdrLoCdrStsTimingCtrl((Tha60210011ModuleCdr)cdrModule);
    cellAddressOffsetInSlice[1] = ThaModuleCdrStmVtTimingControlReg((ThaModuleCdrStm)cdrModule);
    cellAddressOffsetInSlice[2] = ThaModuleCdrEngineTimingCtrlRegister(cdrModule);

    localIdInSlice = LocalIdInSlice(self);
    if (localIdInSlice >= mCount(cellAddressOffsetInSlice))
        return cInvalidUint32;

    if (localIdInSlice < 2)
        return ThaModuleCdrBaseAddress(cdrModule) + cellAddressOffsetInSlice[localIdInSlice] + SliceOffset(self);

    return (uint32)(cCdrV3Base + cellAddressOffsetInSlice[localIdInSlice]) + AcrSliceOffset(self);
    }

static uint32 ACREngineTimingParityForceRegister(Tha60210011InternalRamCdr self)
    {
    AtUnused(self);
    return 0x0000c;
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void OverrideTha60210011InternalRamCdr(AtInternalRam self)
    {
    Tha60210011InternalRamCdr ram = (Tha60210011InternalRamCdr)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Th60210011InternalRamCdrOverride, mMethodsGet(ram), sizeof(m_Th60210011InternalRamCdrOverride));

        /* Common */
        mMethodOverride(m_Th60210011InternalRamCdrOverride, ACREngineTimingParityForceRegister);
        }

    mMethodsSet(ram, &m_Th60210011InternalRamCdrOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideTha60210011InternalRamCdr(self);
    OverrideThaInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022InternalRamCdrV3);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022InternalRamCdrObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60290022InternalRamCdrV3New(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60290022InternalRam.h
 * 
 * Created Date: Aug 21, 2017
 *
 * Description : Tha60290022InternalRam declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022INTERNALRAM_H_
#define _THA60290022INTERNALRAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInternalRam Tha60290022InternalRamSurPwNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60290022InternalRamMapNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60290022InternalRamOcnNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60290022InternalRamPlaNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60290022InternalRamCdrNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60290022InternalRamCdrV3New(AtModule phyModule, uint32 ramId, uint32 localId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022INTERNALRAM_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc..c'
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290022InternalRamMap.c
 *
 * Created Date: Aug 21, 2017
 *
 * Description : Tha60290022InternalRamMap implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ram/Tha60210011InternalRamInternal.h"
#include "Tha60290022InternalRam.h"

/*--------------------------- Define -----------------------------------------*/
 
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha60290022InternalRamMap 
    {
    tTha60210011InternalRamMap super;
    }tTha60290022InternalRamMap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInternalRamMethods             m_AtInternalRamOverride;
static tTha60210011InternalRamMapMethods m_Tha60210011InternalRamMapOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleMap(AtInternalRam self)
    {
    return AtInternalRamPhyModuleGet(self);
    }

static eBool IsReserved(AtInternalRam self)
    {
    return AtModuleInternalRamIsReserved(ModuleMap(self), self);
    }

static uint32 MapErrorForceRegister(Tha60210011InternalRamMap self)
    {
    AtUnused(self);
    return 0x10808;
    }

static uint32 DemapErrorForceRegister(Tha60210011InternalRamMap self)
    {
    AtUnused(self);
    return 0x08218;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022InternalRamMap);
    }
    
static void OverrideTha60210011InternalRamMap(AtInternalRam self)
    {
    Tha60210011InternalRamMap overrideObject = (Tha60210011InternalRamMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011InternalRamMapOverride, mMethodsGet(overrideObject), sizeof(m_Tha60210011InternalRamMapOverride));

        mMethodOverride(m_Tha60210011InternalRamMapOverride, MapErrorForceRegister);
        mMethodOverride(m_Tha60210011InternalRamMapOverride, DemapErrorForceRegister);
        }

    mMethodsSet(overrideObject, &m_Tha60210011InternalRamMapOverride);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, IsReserved);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideTha60210011InternalRamMap(self);
    OverrideAtInternalRam(self);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011InternalRamMapObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60290022InternalRamMapNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newObject, phyModule, ramId, localId);
    }
     

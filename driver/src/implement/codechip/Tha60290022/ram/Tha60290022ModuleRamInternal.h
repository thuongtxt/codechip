/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60290022ModuleRamInternal.h
 * 
 * Created Date: Sep 6, 2018
 *
 * Description : Internal data of the 60290022 module RAM
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULERAMINTERNAL_H_
#define _THA60290022MODULERAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/ram/Tha60290021ModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModuleRam
    {
    tTha60290021ModuleRam super;
    }tTha60290022ModuleRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha60290022ModuleRamObjectInit(AtModuleRam self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULERAMINTERNAL_H_ */


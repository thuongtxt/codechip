/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60290022InternalRamInternal.h
 * 
 * Created Date: Mar 23, 2018
 *
 * Description : Internal RAM representation of product 60290022
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022INTERNALRAMINTERNAL_H_
#define _THA60290022INTERNALRAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/ram/Tha60210011InternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022InternalRamCdr
    {
    tTha60210011InternalRamCdr super;
    }tTha60290022InternalRamCdr;

typedef struct tTha60290022InternalRamPla
    {
    tTha60210011InternalRamPla super;
    }tTha60290022InternalRamPla;

/*--------------------------- Forward declarations ---------------------------*/
AtInternalRam Tha60290022InternalRamCdrObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60290022InternalRamPlaObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290022INTERNALRAMINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290022PrbsEngineNxDs0.c
 *
 * Created Date: May 2, 2017
 *
 * Description : NxDS0 prbs engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhNxDs0.h"
#include "../../Tha60210021/prbs/Tha60210021NxDs0PrbsEngineInternal.h"
#include "Tha60290022PrbsHwController.h"
#include "Tha60290022ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290022PrbsEngineNxDs0 *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PrbsEngineNxDs0
    {
    tTha60210021NxDs0PrbsEngine super;
    }tTha60290022PrbsEngineNxDs0;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60290022PrbsEngineOverride.h"

static uint32 TimeslotBitMap(ThaPrbsEngine self)
    {
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet((AtPrbsEngine)self);

    if (side == cAtPrbsSideTdm)
        return m_ThaPrbsEngineMethods->TimeslotBitMap(self);

    /* PSN side */
    return cBit0;
    }

static void OverrideThaPrbsEngine(AtPrbsEngine self)
    {
    ThaPrbsEngine engine = (ThaPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(engine), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, TimeslotDefaultSet);
        mMethodOverride(m_ThaPrbsEngineOverride, RxChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, TimeslotBitMap);
        mMethodOverride(m_ThaPrbsEngineOverride, HwRxPsnEnable);
        }

    mMethodsSet(engine, &m_ThaPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideThaPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PrbsEngineNxDs0);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210021NxDs0PrbsEngineObjectInit(self, nxDs0, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290022PrbsEngineNxDs0New(AtPdhNxDS0 nxDs0, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, nxDs0, engineId);
    }

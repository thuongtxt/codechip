/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290022FacePlateSerdesPrbsEngine.c
 *
 * Created Date: Apr 25, 2017
 *
 * Description : Faceplate SERDES PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022FacePlateSerdesPrbsEngineInternal.h"
#include "Tha60290022ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021FacePlateSerdesPrbsEngineMethods m_Tha60290021FacePlateSerdesPrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool Prbs15DeprecatedByPrbs23(Tha60290021FacePlateSerdesPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideTha60290021FacePlateSerdesPrbsEngine(AtPrbsEngine self)
    {
    Tha60290021FacePlateSerdesPrbsEngine engine = (Tha60290021FacePlateSerdesPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021FacePlateSerdesPrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha60290021FacePlateSerdesPrbsEngineOverride));

        mMethodOverride(m_Tha60290021FacePlateSerdesPrbsEngineOverride, Prbs15DeprecatedByPrbs23);
        }

    mMethodsSet(engine, &m_Tha60290021FacePlateSerdesPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha60290021FacePlateSerdesPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022FacePlateSerdesPrbsEngine);
    }

AtPrbsEngine Tha60290022FacePlateSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021FacePlateSerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290022FacePlateSerdesPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022FacePlateSerdesPrbsEngineObjectInit(newEngine, serdesController);
    }

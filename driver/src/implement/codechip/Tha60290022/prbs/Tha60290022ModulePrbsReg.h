/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive
 * Technologies. The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      :
 *
 * File        :
 *
 * Created Date:
 *
 * Description : This file contain all constance definitions of  block.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_BERT_OPT_H_
#define _AF6_REG_AF6CNC0022_RD_BERT_OPT_H_

/*--------------------------- Define -----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : Sel Timeslot Gen
Reg Addr   : 0x4102 - 0x7702
Reg Formula: 0x4102 + 512*engid
    Where  :
           + $engid(0-27): id of HO_BERT
Reg Desc   :
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ts_gen_Base                                                                         0x4102

/*--------------------------------------
BitField Name: gen_tsen
BitField Type: RW
BitField Desc: bit map indicase 32 timeslot,"1" : enable
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_sel_ts_gen_gen_tsen_Mask                                                                 cBit31_0
#define cAf6_sel_ts_gen_gen_tsen_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Sel Timeslot Mon tdm
Reg Addr   : 0x8102 - 0xB102
Reg Formula: 0x8102 + 512*engid
    Where  :
           + $engid(0-27): id of HO_BERT
Reg Desc   :
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_mon_tstdm_Base                                                                      0x8102

/*--------------------------------------
BitField Name: tdm_tsen
BitField Type: RW
BitField Desc: bit map indicase 32 timeslot,"1" : enable
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_sel_mon_tstdm_tdm_tsen_Mask                                                              cBit31_0
#define cAf6_sel_mon_tstdm_tdm_tsen_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert Gen
Reg Addr   : 0x0_200 - 0x0_21B
Reg Formula: 0x0_200 + engid
    Where  :
           + $engid(0-27): id of HO_BERT
Reg Desc   :
The registers select id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_gen_Base                                                                    0x0200

/*--------------------------------------
BitField Name: gen_en
BitField Type: RW
BitField Desc: set "1" to enable bert gen
BitField Bits: [14]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen_gen_en_Mask                                                                cBit14
#define cAf6_sel_ho_bert_gen_gen_en_Shift                                                                   14

/*--------------------------------------
BitField Name: lineid
BitField Type: RW
BitField Desc: line id OC48
BitField Bits: [13:11]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen_lineid_Mask                                                             cBit13_11
#define cAf6_sel_ho_bert_gen_lineid_Shift                                                                   11

/*--------------------------------------
BitField Name: id
BitField Type: RW
BitField Desc: TDM ID
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen_id_Mask                                                                  cBit10_0
#define cAf6_sel_ho_bert_gen_id_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Sel TDM Bert mon
Reg Addr   : 0x0_000 - 0x0_01b
Reg Formula: 0x0_000 + engid
    Where  :
           + $engid(0-27): id of HO_BERT
Reg Desc   :
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_bert_montdm_Base                                                                    0x0000

/*--------------------------------------
BitField Name: tdmmon_en
BitField Type: RW
BitField Desc: set "1" to enable bert gen
BitField Bits: [15]
--------------------------------------*/
#define cAf6_sel_bert_montdm_tdmmon_en_Mask                                                             cBit15
#define cAf6_sel_bert_montdm_tdmmon_en_Shift                                                                15

/*--------------------------------------
BitField Name: mon_side
BitField Type: RW
BitField Desc: "0" tdm side, "1" psn side moniter
BitField Bits: [14]
--------------------------------------*/
#define cAf6_sel_bert_montdm_mon_side_Mask                                                              cBit14
#define cAf6_sel_bert_montdm_mon_side_Shift                                                                 14

/*--------------------------------------
BitField Name: tdm_lineid
BitField Type: RW
BitField Desc: line id OC48
BitField Bits: [13:11]
--------------------------------------*/
#define cAf6_sel_bert_montdm_tdm_lineid_Mask                                                         cBit13_11
#define cAf6_sel_bert_montdm_tdm_lineid_Shift                                                               11

/*--------------------------------------
BitField Name: id
BitField Type: RW
BitField Desc: tdm ID
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_sel_bert_montdm_id_Mask                                                                  cBit10_0
#define cAf6_sel_bert_montdm_id_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Sel Mode Bert Gen
Reg Addr   : 0x4100 - 0x7700
Reg Formula: 0x4100 + 512*engid
    Where  :
           + $engid(0-27): bert id
Reg Desc   :
The registers select mode bert gen

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_pen_gen_Base                                                                       0x4100

/*--------------------------------------
BitField Name: swapmode
BitField Type: RW
BitField Desc: swap data
BitField Bits: [05]
--------------------------------------*/
#define cAf6_ctrl_pen_gen_swapmode_Mask                                                                  cBit5
#define cAf6_ctrl_pen_gen_swapmode_Shift                                                                     5

/*--------------------------------------
BitField Name: invmode
BitField Type: RW
BitField Desc: invert data
BitField Bits: [04]
--------------------------------------*/
#define cAf6_ctrl_pen_gen_invmode_Mask                                                                   cBit4
#define cAf6_ctrl_pen_gen_invmode_Shift                                                                      4

/*--------------------------------------
BitField Name: patt_mode
BitField Type: RW
BitField Desc: sel pattern gen # 0x01 : all0 # 0x02 : prbs15 # 0x03 : prbs23 #
0x05 : prbs31 # 0x7  : all1 # 0x8  : SEQ(SIM)/prbs20(SYN) # 0x4  : prbs20r #
0x06 : fixpattern 4byte
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ctrl_pen_gen_patt_mode_Mask                                                               cBit3_0
#define cAf6_ctrl_pen_gen_patt_mode_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Sel Value of fix pattern
Reg Addr   : 0x4104 - 0xB704
Reg Formula: 0x4104 + 512*engid
    Where  :
           + $engid(0-27): bert id
Reg Desc   :
The registers select mode bert gen

------------------------------------------------------------------------------*/
#define cAf6Reg_txfix_gen_Base                                                                          0x4104

/*--------------------------------------
BitField Name: txpatten_val
BitField Type: RW
BitField Desc: value of fix paatern
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_txfix_gen_txpatten_val_Mask                                                              cBit31_0
#define cAf6_txfix_gen_txpatten_val_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Sel Mode Bert Mon TDM
Reg Addr   : 0x8100 - 0xB700
Reg Formula: 0x8100 + 512*engid
    Where  :
           + $engid(0-27): bert id
Reg Desc   :
The registers select mode bert gen

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_pen_montdm_Base                                                                    0x8100

/*--------------------------------------
BitField Name: swapmode
BitField Type: RW
BitField Desc: swap data
BitField Bits: [05]
--------------------------------------*/
#define cAf6_ctrl_pen_montdm_swapmode_Mask                                                               cBit5
#define cAf6_ctrl_pen_montdm_swapmode_Shift                                                                  5

/*--------------------------------------
BitField Name: invmode
BitField Type: RW
BitField Desc: invert data
BitField Bits: [04]
--------------------------------------*/
#define cAf6_ctrl_pen_montdm_invmode_Mask                                                                cBit4
#define cAf6_ctrl_pen_montdm_invmode_Shift                                                                   4

/*--------------------------------------
BitField Name: patt_mode
BitField Type: RW
BitField Desc: sel pattern gen # 0x01 : fixpattern # 0x02 : all0 # 0x04 : prbs15
# 0x03 : prbs23 # 0x05 : prbs31 # 0x00 : all1 # 0x10 : SEQ(SIM)/prbs20(SYN) #
0x08  : prbs20r
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ctrl_pen_montdm_patt_mode_Mask                                                            cBit3_0
#define cAf6_ctrl_pen_montdm_patt_mode_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Sel Value of fix pattern
Reg Addr   : 0x8104 - 0x8704
Reg Formula: 0x8104 + 512*engid
    Where  :
           + $engid(0-27): bert id
Reg Desc   :
The registers select mode bert gen

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfix_gen_Base                                                                          0x8104

/*--------------------------------------
BitField Name: rxpatten_val
BitField Type: RW
BitField Desc: value of fix paatern
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_rxfix_gen_rxpatten_val_Mask                                                              cBit31_0
#define cAf6_rxfix_gen_rxpatten_val_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Inser Error
Reg Addr   : 0x4101 - 0xB_701
Reg Formula: 0x4101 + 512*engid
    Where  :
           + $engid(0-27): bert id
Reg Desc   :
The registers select rate inser error

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_ber_pen_Base                                                                       0x4101

/*--------------------------------------
BitField Name: ber_rate
BitField Type: RW
BitField Desc: TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to
Pattern Generator [31:0] == 32d0          :disable  Incase N DS0 mode : n
= num timeslot use, BER_level_val recalculate by CFG_DS1,CFG_E1 CFG_DS1  = ( 8n
x  BER_level_val) / 193 CFG_E1   = ( 8n  x  BER_level_val) / 256 Other :
BER_level_val  BER_level 1000:         BER 10e-3 10_000:       BER 10e-4
100_000:      BER 10e-5 1_000_000:    BER 10e-6 10_000_000:   BER 10e-7
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ctrl_ber_pen_ber_rate_Mask                                                               cBit31_0
#define cAf6_ctrl_ber_pen_ber_rate_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : good counter tdm gen
Reg Addr   : 0x0600 - 0x063F
Reg Formula: 0x0600 + id
    Where  :
           + $id(0-31): bert id
Reg Desc   :
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_tdm_gen_Base                                                                0x0600

/*--------------------------------------
BitField Name: bertgen
BitField Type: RC
BitField Desc: gen goodbit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_tdm_gen_bertgen_Mask                                                         cBit31_0
#define cAf6_goodbit_pen_tdm_gen_bertgen_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : good counter tdm
Reg Addr   : 0x8600 - 0x863F
Reg Formula: 0x8600 + id
    Where  :
           + $id(0-31): bert id
Reg Desc   :
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_tdm_mon_Base                                                                0x8600

/*--------------------------------------
BitField Name: mongood
BitField Type: RC
BitField Desc: moniter goodbit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_tdm_mon_mongood_Mask                                                         cBit31_0
#define cAf6_goodbit_pen_tdm_mon_mongood_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : TDM err sync
Reg Addr   : 0x8640 - 0x867f
Reg Formula: 0x8640 + id
    Where  :
           + $id(0-31): bert id
Reg Desc   :
The registers indicate bert mon err in tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_err_tdm_mon_Base                                                                        0x8640

/*--------------------------------------
BitField Name: cnt_errbit
BitField Type: RC
BitField Desc: counter err bit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_err_tdm_mon_cnt_errbit_Mask                                                              cBit31_0
#define cAf6_err_tdm_mon_cnt_errbit_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Counter loss bit in TDM mon
Reg Addr   : 0x8105-0xB705
Reg Formula: 0x8105 + engid*512
    Where  :
           + $engid(0-27): slice id of HO_BERT
Reg Desc   :
The registers count lossbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_lossyn_pen_tdm_mon_Base                                                                 0x8105

/*--------------------------------------
BitField Name: stk_losssyn
BitField Type: W1C
BitField Desc: sticky loss sync
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lossyn_pen_tdm_mon_stk_losssyn_Mask                                                      cBit31_0
#define cAf6_lossyn_pen_tdm_mon_stk_losssyn_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter loss bit in TDM mon
Reg Addr   : 0x8680 - 0x86BF
Reg Formula: 0x8680 + id
    Where  :
           + $id(0-31): bert id
Reg Desc   :
The registers count lossbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_lossbit_pen_tdm_mon_Base                                                                0x8680

/*--------------------------------------
BitField Name: cnt_lossbit
BitField Type: RC
BitField Desc: counter lossbit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lossbit_pen_tdm_mon_cnt_lossbit_Mask                                                     cBit31_0
#define cAf6_lossbit_pen_tdm_mon_cnt_lossbit_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Single Inser Error
Reg Addr   : 0x4105-0x7105
Reg Formula: 0x4105 + 512*engid
    Where  :
           + $engid(0-27): bert id
Reg Desc   :
The registers select rate inser error

------------------------------------------------------------------------------*/
#define cAf6Reg_singe_ber_pen_Base                                                                      0x4105

/*--------------------------------------
BitField Name: ena
BitField Type: RW
BitField Desc: sw write "1" to force, hw auto clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_singe_ber_pen_ena_Mask                                                                      cBit0
#define cAf6_singe_ber_pen_ena_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : tdm mon stt
Reg Addr   : 0x8103 - 0xB703
Reg Formula: 0x8103 + 512*engid
    Where  :
           + $engid(0-7): id of HO_BERT
Reg Desc   :
The registers indicate bert mon state in pw side

------------------------------------------------------------------------------*/
#define cAf6Reg_stt_tdm_mon_Base                                                                        0x8103

/*--------------------------------------
BitField Name: tdmstate
BitField Type: RO
BitField Desc: Status [1:0]: Prbs status LOPSTA = 2'd0; SRCSTA = 2'd1; VERSTA =
2'd2; INFSTA = 2'd3;
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_stt_tdm_mon_tdmstate_Mask                                                                 cBit1_0
#define cAf6_stt_tdm_mon_tdmstate_Shift                                                                      0

#endif /* _AF6_REG_AF6CNC0022_RD_BERT_OPT_H_ */

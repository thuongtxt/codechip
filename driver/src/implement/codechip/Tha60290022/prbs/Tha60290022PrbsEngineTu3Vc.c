/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290022PrbsEngineAuVc.c
 *
 * Created Date: Jan 29, 2018
 *
 * Description : AU VC PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "Tha60290022ModulePrbs.h"
#include "Tha60290022PrbsEngineAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PrbsEngineTu3Vc
    {
    tTha60290022PrbsEngineAuVc super;
    }tTha60290022PrbsEngineTu3Vc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsEngineAuVcMethods m_ThaPrbsEngineAuVcOverride;

/* Save super implementation */
static const tThaPrbsEngineAuVcMethods *m_ThaPrbsEngineAuVcMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhChannel ChannelForPohInsertion(ThaPrbsEngineAuVc self, AtSdhChannel channel)
    {
    AtUnused(self);
    return channel;
    }

static void OverrideThaPrbsEngineAuVc(AtPrbsEngine self)
    {
    ThaPrbsEngineAuVc engine = (ThaPrbsEngineAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineAuVcMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineAuVcOverride, m_ThaPrbsEngineAuVcMethods, sizeof(m_ThaPrbsEngineAuVcOverride));

        mMethodOverride(m_ThaPrbsEngineAuVcOverride, ChannelForPohInsertion);
        }

    mMethodsSet(engine, &m_ThaPrbsEngineAuVcOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideThaPrbsEngineAuVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PrbsEngineTu3Vc);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022PrbsEngineAuVcObjectInit(self, vc, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290022PrbsEngineTu3VcNew(AtSdhChannel vc, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, vc, engineId);
    }

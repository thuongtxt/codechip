/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290022PrbsRegProvider.c
 *
 * Created Date: May 1, 2017
 *
 * Description : Register provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/prbs/Tha60290021PrbsRegProviderInternal.h"
#include "Tha60290022ModulePrbsReg.h"
#include "Tha60290022PrbsRegProvider.h"
#include "Tha60290022ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PrbsRegProvider
    {
    tTha60290021PrbsRegProvider super;
    }tTha60290022PrbsRegProvider;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsRegProviderMethods m_ThaPrbsRegProviderOverride;

/* Save super implementation */
static const tThaPrbsRegProviderMethods *m_ThaPrbsRegProviderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 EngineAddressWithLocalAddress(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice, uint32 localAddress)
    {
    AtUnused(prbsModule);
    AtUnused(slice);
    return localAddress + ThaPrbsRegProviderBaseAddress(self) + engineId;
    }

static uint32 EngineSliceAddressWithLocalAddress(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice, uint32 localAddress)
    {
    uint32 factor = ThaPrbsRegProviderTdmChannelIdSliceFactor(self, prbsModule);
    return localAddress + ThaPrbsRegProviderBaseAddress(self) + engineId + (slice * factor);
    }

static uint32 EngineAddressWithLocalAddressAndEngineOffset(ThaPrbsRegProvider self, uint32 engineId, uint32 engineOffset, uint32 localAddress)
    {
    return localAddress + ThaPrbsRegProviderBaseAddress(self) + (engineId * engineOffset);
    }

static uint32 EngineOptimizeAddressWithLocalAddress(ThaPrbsRegProvider self,
                                                    ThaModulePrbs prbsModule,
                                                    uint32 engineId,
                                                    uint32 engineOffset,
                                                    uint32 slice,
                                                    uint32 localAddress)
    {
    AtUnused(prbsModule);
    AtUnused(slice);
    return EngineAddressWithLocalAddressAndEngineOffset(self, engineId, engineOffset, localAddress);
    }

static uint32 RegMapBERTGenSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return EngineAddressWithLocalAddress(self, prbsModule, engineId, slice, cAf6Reg_sel_ho_bert_gen_Base);
    }

static uint32 RegMapBERTGenErrorRateInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    uint32 regAddr = cAf6Reg_ctrl_ber_pen_Base;
    return EngineOptimizeAddressWithLocalAddress(self, prbsModule, engineId, cEngineOffset, slice, regAddr);
    }

static uint32 BaseAddress(ThaPrbsRegProvider self)
    {
    AtUnused(self);
    return 0x0350000;
    }

static uint32 RegDemapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    return EngineAddressWithLocalAddress(self, prbsModule, engineId, slice, cAf6Reg_sel_bert_montdm_Base);
    }

static uint32 RegMapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    /* This product does not have */
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(engineId);
    AtUnused(slice);
    return cInvalidUint32;
    }

static uint32 RegMapBERTGenGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    uint32 regAddr = cAf6Reg_goodbit_pen_tdm_gen_Base;
    return EngineOptimizeAddressWithLocalAddress(self, prbsModule, engineId, cEngineCounterOffset, slice, regAddr);
    }

static uint32 RegDemapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    uint32 regAddr = cAf6Reg_goodbit_pen_tdm_mon_Base;
    return EngineOptimizeAddressWithLocalAddress(self, prbsModule, engineId, cEngineCounterOffset, slice, regAddr);
    }

static uint32 RegDemapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    uint32 regAddr = cAf6Reg_err_tdm_mon_Base;
    return EngineOptimizeAddressWithLocalAddress(self, prbsModule, engineId, cEngineCounterOffset, slice, regAddr);
    }

static uint32 RegDemapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    uint32 regAddr = cAf6Reg_lossbit_pen_tdm_mon_Base;
    return EngineOptimizeAddressWithLocalAddress(self, prbsModule, engineId, cEngineCounterOffset, slice, regAddr);
    }

static uint32 RegMapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    /* This product does not have */
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(engineId);
    AtUnused(slice);
    return cInvalidUint32;
    }

static uint32 RegMapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    /* This product does not have */
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(engineId);
    AtUnused(slice);
    return cInvalidUint32;
    }

static uint32 RegMapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    /* This product does not have */
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(engineId);
    AtUnused(slice);
    return cInvalidUint32;
    }

static uint32 RegDemapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(prbsModule);
    AtUnused(slice);
    return EngineAddressWithLocalAddressAndEngineOffset(self,
                                              engineId,
                                              cEngineOffset,
                                              cAf6Reg_stt_tdm_mon_Base);
    }

static uint32 RegDemapBERTMonRxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cAf6_sel_bert_montdm_tdmmon_en_Mask;
    }

static void OverrideThaPrbsRegProvider(ThaPrbsRegProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsRegProviderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsRegProviderOverride, mMethodsGet(self), sizeof(m_ThaPrbsRegProviderOverride));

        mMethodOverride(m_ThaPrbsRegProviderOverride, BaseAddress);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenErrorRateInsert);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonErrorCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonLossBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonErrorCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonLossBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonStatus);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonRxPtgEnMask);
        }

    mMethodsSet(self, &m_ThaPrbsRegProviderOverride);
    }

static void Override(ThaPrbsRegProvider self)
    {
    OverrideThaPrbsRegProvider(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PrbsRegProvider);
    }

static ThaPrbsRegProvider ObjectInit(ThaPrbsRegProvider self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021PrbsRegProviderObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPrbsRegProvider Tha60290022PrbsRegProvider(void)
    {
    static tTha60290022PrbsRegProvider shareProvider;
    static ThaPrbsRegProvider pShareProvider = NULL;
    if (pShareProvider == NULL)
        pShareProvider = ObjectInit((ThaPrbsRegProvider)&shareProvider);
    return pShareProvider;
    }

uint32 Tha60290022PrbsRegProviderEngineSliceAddressWithLocalAddress(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice, uint32 localAddress)
    {
    if (self)
        return EngineSliceAddressWithLocalAddress(self, prbsModule, engineId, slice, localAddress);
    return cInvalidUint32;
    }

uint32 Tha60290022PrbsRegProviderEngineAddressWithLocalAddressAndEngineOffset(ThaPrbsRegProvider self, uint32 engineId, uint32 localAddress)
    {
    if (self)
        return EngineAddressWithLocalAddressAndEngineOffset(self, engineId, cEngineOffset, localAddress);
    return cInvalidUint32;
    }

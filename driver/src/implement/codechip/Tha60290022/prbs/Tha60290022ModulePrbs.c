/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290022ModulePrbs.c
 *
 * Created Date: Apr 25, 2017
 *
 * Description : PRBS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "Tha60290022ModulePrbsInternal.h"
#include "Tha60290022ModulePrbs.h"
#include "Tha60290022PrbsHwController.h"
#include "../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePrbsMethods          m_AtModulePrbsOverride;
static tThaModulePrbsMethods         m_ThaModulePrbsOverride;
static tTha60290021ModulePrbsMethods m_Tha60290021ModulePrbsOverride;

/* Save super implementation */
static const tThaModulePrbsMethods   *m_ThaModulePrbsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPrbsRegProvider RegProvider(ThaModulePrbs self)
    {
    AtUnused(self);
    return Tha60290022PrbsRegProvider();
    }

static Tha60290021PrbsGatingFactory PrbsGatingFactoryObjectCreate(Tha60290021ModulePrbs self)
    {
    return Tha60290022PrbsGatingFactoryNew((AtModulePrbs)self);
    }

static AtPrbsEngine De1PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
    AtUnused(self);
    return Tha60290022PrbsEngineDe1New(de1, engineId);
    }

static AtPrbsEngine De3PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhDe3 de3)
    {
    AtUnused(self);
    return Tha60290022PrbsEngineDe3New(de3, engineId);
    }

static AtPrbsEngine Vc1xPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    return Tha60290022PrbsEngineVc1xNew(sdhVc, engineId);
    }

static AtPrbsEngine AuVcxPrbsEngineObjectCreate(Tha60290021ModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    return Tha60290022PrbsEngineAuVcNew(sdhVc, engineId);
    }

static AtPrbsEngine Tu3VcPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    return Tha60290022PrbsEngineTu3VcNew(sdhVc, engineId);
    }

static AtPrbsEngine NxDs0PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtUnused(self);
    return Tha60290022PrbsEngineNxDs0New(nxDs0, engineId);
    }

static eBool DynamicAllocation(ThaModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 PwTdmOffset(AtPw pw)
    {
    uint8 slice = 0;

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    return (slice * 0x800UL) + AtChannelHwIdGet((AtChannel)pw);
    }

static uint32 PsnChannelId(ThaModulePrbs self, AtPrbsEngine engine)
    {
    AtChannel channel = AtPrbsEngineChannelGet(engine);
    AtPw pw = AtChannelBoundPwGet(channel);

    AtUnused(self);
    return PwTdmOffset(pw);
    }

static eBool BertOptimizeIsSupported(ThaModulePrbs self)
    {
    return Tha60290022DevicePrbsBertOptimizeIsSupported(AtModuleDeviceGet((AtModule)self));
    }

static eBool ShouldResetFixPattern(ThaModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void HwEngineIdInUseSet(ThaModulePrbs self, ThaPrbsEngine engine, uint32 hwEngineId)
    {
    AtUnused(self);
    ThaPrbsEngineHwEngineIdSet(engine, hwEngineId);
    }

static void ResourceRemove(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    AtUnused(self);
    AtUnused(engine);
    }

static eAtRet HoPrbsEngineRegisterInit(ThaModulePrbs self)
    {
    AtUnused(self);

    return cAtOk;
    }

static eBool AllPwsPrbsIsSupported(AtModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void HwFlush(ThaModulePrbs self)
    {
    if (self)
        Tha60290022PrbsAllEngineHwFlush(self);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, mMethodsGet(self), sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, AllPwsPrbsIsSupported);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideThaModulePrbs(AtModulePrbs self)
    {
    ThaModulePrbs module = (ThaModulePrbs)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePrbsMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePrbsOverride, mMethodsGet(module), sizeof(m_ThaModulePrbsOverride));

        mMethodOverride(m_ThaModulePrbsOverride, RegProvider);
        mMethodOverride(m_ThaModulePrbsOverride, De1PrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, De3PrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, Vc1xPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, Tu3VcPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, NxDs0PrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, DynamicAllocation);
        mMethodOverride(m_ThaModulePrbsOverride, PsnChannelId);
        mMethodOverride(m_ThaModulePrbsOverride, ShouldResetFixPattern);
        mMethodOverride(m_ThaModulePrbsOverride, HwEngineIdInUseSet);
        mMethodOverride(m_ThaModulePrbsOverride, ResourceRemove);
        mMethodOverride(m_ThaModulePrbsOverride, HoPrbsEngineRegisterInit);
        mMethodOverride(m_ThaModulePrbsOverride, HwFlush);
        }

    mMethodsSet(module, &m_ThaModulePrbsOverride);
    }

static void OverrideTha60290021ModulePrbs(AtModulePrbs self)
    {
    Tha60290021ModulePrbs module = (Tha60290021ModulePrbs)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModulePrbsOverride, mMethodsGet(module), sizeof(m_Tha60290021ModulePrbsOverride));

        mMethodOverride(m_Tha60290021ModulePrbsOverride, PrbsGatingFactoryObjectCreate);
        mMethodOverride(m_Tha60290021ModulePrbsOverride, AuVcxPrbsEngineObjectCreate);
        }

    mMethodsSet(module, &m_Tha60290021ModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtModulePrbs(self);
    OverrideThaModulePrbs(self);
    OverrideTha60290021ModulePrbs(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModulePrbs);
    }

AtModulePrbs Tha60290022ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Over-ride */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha60290022ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModulePrbsObjectInit(newModule, device);
    }

eBool Tha60290022ModulePrbsBertOptimizeIsSupported(ThaModulePrbs self)
    {
    if (self)
        return BertOptimizeIsSupported(self);
    return cAtFalse;
    }
    
void Tha60290022ModulePrbsHwFlush(ThaModulePrbs self)
    {
    ThaModulePrbsHwFlush(self);
    }

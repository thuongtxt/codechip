/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290022ModulePrbsV2.c
 *
 * Created Date: Apr 5, 2018
 *
 * Description : Implement for PRBS module support STS192c bert
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pda/ThaModulePda.h"
#include "../../../../default/pwe/ThaModulePwe.h"
#include "../../../../default/man/ThaDevice.h"
#include "../Tha60290022ModulePrbsInternal.h"
#include "../Tha60290022ModulePrbs.h"
#include "Tha60290022ModulePrbsSts192cPdaReg.h"
#include "Tha60290022ModulePrbsSts192cPlaReg.h"
#include "Tha60290022ModulePrbsV2.h"
#include "../../man/Tha60290022Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePrbsMethods         m_ThaModulePrbsOverride;
static tTha60290021ModulePrbsMethods m_Tha60290021ModulePrbsOverride;

/* Save super implementation */
static const tThaModulePrbsMethods   *m_ThaModulePrbsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumAuVc4_64cEngines(Tha60290021ModulePrbs self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 NumPrbsEngineSts192c(ThaModulePrbs self)
    {
    return mMethodsGet((Tha60290021ModulePrbs)self)->NumAuVc4_64cEngines((Tha60290021ModulePrbs)self);
    }

static uint32 HwEngineOffsetFromHwEgineId(uint32 hwEngineId)
    {
    return hwEngineId * 0x1000;
    }

static void BertGenHwFlushForDeviceInit(ThaModulePrbs self)
    {
    uint32 address, offset, engine;
    ThaModulePda modulePda = (ThaModulePda)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePda);
    uint32 baseAddress = ThaModulePdaBaseAddress(modulePda);

    for (engine = 0; engine < NumPrbsEngineSts192c(self); engine++)
        {
        offset = HwEngineOffsetFromHwEgineId(engine);
        address = baseAddress + cAf6Reg_bert_gen_config_Base + offset;
        mModuleHwWrite(self, address, 0);
        }
    }

static void BertMonPsnHwFlushForDeviceInit(ThaModulePrbs self)
    {
    uint32 address, offset, engine;
    ThaModulePda modulePda = (ThaModulePda)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePda);
    uint32 baseAddress = ThaModulePdaBaseAddress(modulePda);

    for (engine = 0; engine < NumPrbsEngineSts192c(self); engine++)
        {
        offset = HwEngineOffsetFromHwEgineId(engine);
        address = baseAddress + cAf6Reg_bert_monpsn_config_Base + offset;
        mModuleHwWrite(self, address, 0);
        }
    }

static void BertMonTdmHwFlushForDeviceInit(ThaModulePrbs self)
    {
    uint32 address, offset, engine;
    ThaModulePwe modulePwe = (ThaModulePwe)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePwe);
    uint32 baseAddress = ThaModulePweBaseAddress(modulePwe);

    for (engine = 0; engine < NumPrbsEngineSts192c(self); engine++)
        {
        offset = HwEngineOffsetFromHwEgineId(engine);
        address = baseAddress + cAf6Reg_bert_montdm_config_Base + offset;
        mModuleHwWrite(self, address, 0);
        }
    }

static AtPrbsEngine AuVc4_64cPrbsEngineObjectCreate(Tha60290021ModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)sdhVc);

    AtUnused(self);

    if (Tha60290022DeviceSts192cPrbsInvertionIsSupported(device))
        return Tha60290022PrbsEngineAuVcSts192cV2New(sdhVc, engineId);

    return Tha60290022PrbsEngineAuVcSts192cNew(sdhVc, engineId);
    }

static void HwFlush(ThaModulePrbs self)
    {
    m_ThaModulePrbsMethods->HwFlush(self);
    BertGenHwFlushForDeviceInit(self);
    BertMonTdmHwFlushForDeviceInit(self);
    BertMonPsnHwFlushForDeviceInit(self);
    }

static void OverrideTha60290021ModulePrbs(AtModulePrbs self)
    {
    Tha60290021ModulePrbs module = (Tha60290021ModulePrbs)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModulePrbsOverride, mMethodsGet(module), sizeof(m_Tha60290021ModulePrbsOverride));

        mMethodOverride(m_Tha60290021ModulePrbsOverride, AuVc4_64cPrbsEngineObjectCreate);
        mMethodOverride(m_Tha60290021ModulePrbsOverride, NumAuVc4_64cEngines);
        }

    mMethodsSet(module, &m_Tha60290021ModulePrbsOverride);
    }

static void OverrideThaModulePrbs(AtModulePrbs self)
    {
    ThaModulePrbs module = (ThaModulePrbs)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePrbsMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePrbsOverride, mMethodsGet(module), sizeof(m_ThaModulePrbsOverride));

        mMethodOverride(m_ThaModulePrbsOverride, HwFlush);
        }

    mMethodsSet(module, &m_ThaModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideTha60290021ModulePrbs(self);
    OverrideThaModulePrbs(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModulePrbsV2);
    }

AtModulePrbs Tha60290022ModulePrbsV2ObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Over-ride */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha60290022ModulePrbsV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModulePrbsV2ObjectInit(newModule, device);
    }

uint32 Tha60290022ModulePrbsSts192cHwEngineOffsetFromHwEgineId(uint32 hwEngineId)
    {
    return HwEngineOffsetFromHwEgineId(hwEngineId);
    }

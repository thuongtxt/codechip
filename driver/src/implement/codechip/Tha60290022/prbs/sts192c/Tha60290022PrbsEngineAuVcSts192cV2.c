/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290022PrbsEngineAuVcSts192cV2.c
 *
 * Created Date: Apr 26, 2018
 *
 * Description : STS192c PRBS Engine support invert/non-invert
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pda/ThaModulePda.h"
#include "../../../../default/pwe/ThaModulePwe.h"
#include "../../../../default/man/ThaDevice.h"
#include "../../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../Tha60290022ModulePrbs.h"
#include "../Tha60290022PrbsHwController.h"
#include "Tha60290022ModulePrbsSts192cPdaReg.h"
#include "Tha60290022ModulePrbsSts192cPlaReg.h"
#include "Tha60290022ModulePrbsV2.h"
#include "Tha60290022PrbsEngineAuVcSts192cInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods    m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Read(ThaPrbsEngine self, uint32 regAddr)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    return mChannelHwRead(channel, regAddr, cAtModulePrbs);
    }

static void Write(ThaPrbsEngine self, uint32 regAddr, uint32 regVal)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    mChannelHwWrite(channel, regAddr, regVal, cAtModulePrbs);
    }

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
    {
    uint32 regAddr, regVal;
    ThaPrbsEngine engine = (ThaPrbsEngine)self;

    regAddr = mMethodsGet(engine)->BertGenSelectedChannelRegAddress(engine);
    regVal = Read(engine, regAddr);
    mRegFieldSet(regVal, cAf6_bert_gen_config_BERT_inv_, invert ? 1 : 0);
    Write(engine, regAddr, regVal);
    return cAtOk;
    }

static eBool TxIsInverted(AtPrbsEngine self)
    {
    uint32 regAddr, regVal, hwInverted;
    ThaPrbsEngine engine = (ThaPrbsEngine)self;

    regAddr = mMethodsGet(engine)->BertGenSelectedChannelRegAddress(engine);
    regVal = Read(engine, regAddr);
    hwInverted = mRegField(regVal, cAf6_bert_gen_config_BERT_inv_);
    return (eBool)(hwInverted ? cAtTrue : cAtFalse);
    }

static eAtRet TdmSideRxInvertedSet(ThaPrbsEngine self, eBool enable)
    {
    uint32 regAddr = mMethodsGet(self)->BertMonTdmSelectedChannelRegAddress(self);
    uint32 regVal = Read(self, regAddr);

    mRegFieldSet(regVal, cAf6_bert_montdm_config_BERT_inv_, mBoolToBin(enable));
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static eBool TdmSideRxIsInverted(ThaPrbsEngine self)
    {
    uint32 hwVal;
    uint32 regAddr = mMethodsGet(self)->BertMonTdmSelectedChannelRegAddress(self);
    uint32 regVal = Read(self, regAddr);

    hwVal = mRegField(regVal, cAf6_bert_montdm_config_BERT_inv_);

    return (eBool)(hwVal ? cAtTrue : cAtFalse);
    }

static eAtRet PsnSideRxInvertedSet(ThaPrbsEngine self, eBool enable)
    {
    uint32 regAddr = mMethodsGet(self)->BertMonPsnSelectedChannelRegAddress(self);
    uint32 regVal = Read(self, regAddr);

    mRegFieldSet(regVal, cAf6_bert_monpsn_config_BERT_inv_, mBoolToBin(enable));
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static eBool PsnSideRxIsInverted(ThaPrbsEngine self)
    {
    uint32 hwVal;
    uint32 regAddr = mMethodsGet(self)->BertMonPsnSelectedChannelRegAddress(self);
    uint32 regVal = Read(self, regAddr);

    hwVal = mRegField(regVal, cAf6_bert_monpsn_config_BERT_inv_);

    return (eBool)(hwVal ? cAtTrue : cAtFalse);
    }

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
    {
    ThaPrbsEngine engine = (ThaPrbsEngine)self;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
        return TdmSideRxInvertedSet(engine, invert);

    if (side == cAtPrbsSidePsn)
        return PsnSideRxInvertedSet(engine, invert);

    return cAtErrorRsrcNoAvail;
    }

static eBool RxIsInverted(AtPrbsEngine self)
    {
    ThaPrbsEngine engine = (ThaPrbsEngine)self;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
        return TdmSideRxIsInverted(engine);

    if (side == cAtPrbsSidePsn)
        return PsnSideRxIsInverted(engine);

    return cAtFalse;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, TxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, RxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsInverted);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PrbsEngineAuVcSts192cV2);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022PrbsEngineAuVcSts192cObjectInit(self, vc, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290022PrbsEngineAuVcSts192cV2New(AtSdhChannel vc, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, vc, engineId);
    }

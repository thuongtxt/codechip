/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_PDA_H_
#define _AF6_REG_AF6CNC0022_RD_PDA_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : BERT GEN CONTROL
Reg Addr   : 0x7_0000 - 0x7_1000
Reg Formula: 0x7_0000 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
This register config mode for bert

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_gen_config_Base                                                                   0x70000
#define cAf6Reg_bert_gen_config_WidthVal                                                                    32

/*--------------------------------------
Field: [4]        %%   BertInv       %% Set 1 to select invert mode   %% RW %% 0x0 %% 0x0
--------------------------------------*/
#define cAf6_bert_gen_config_BERT_inv_Mask                                                        cBit4
#define cAf6_bert_gen_config_BERT_inv_Shift                                                       4


/*--------------------------------------
BitField Name: BertEna
BitField Type: RW
BitField Desc: Set 1 to enable BERT
BitField Bits: [3]
--------------------------------------*/
#define cAf6_bert_gen_config_BertEna_Mask                                                                cBit3
#define cAf6_bert_gen_config_BertEna_Shift                                                                   3

/*--------------------------------------
BitField Name: BertMode
BitField Type: RW
BitField Desc: "0" prbs15, "1" prbs23, "2" prbs31,"3" prbs20, "4" prbs20r, "5"
all1, "6" all0
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_bert_gen_config_BertMode_Mask                                                             cBit2_0
#define cAf6_bert_gen_config_BertMode_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : BERT GEN CONTROL
Reg Addr   : 0x7_0001 - 0x7_1001
Reg Formula: 0x7_0001 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
This register config rate for force error

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_gen_force_Base                                                                    0x70001
#define cAf6Reg_bert_gen_force_WidthVal                                                                     32

/*--------------------------------------
BitField Name: ber_rate
BitField Type: RW
BitField Desc: TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to
Pattern Generator [31:0] == 32'd0          :disable BER_level_val  BER_level
1000:         BER 10e-3 10_000:       BER 10e-4 100_000:      BER 10e-5
1_000_000:    BER 10e-6 10_000_000:   BER 10e-7
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_bert_gen_force_ber_rate_Mask                                                             cBit31_0
#define cAf6_bert_gen_force_ber_rate_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : BERT GEN CONTROL
Reg Addr   : 0x7_0004 - 0x7_1004
Reg Formula: 0x7_0004 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
This register config rate for force error

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_gen_force_sing_Base                                                               0x70004
#define cAf6Reg_bert_gen_force_sing_WidthVal                                                                32

/*--------------------------------------
BitField Name: singe_err
BitField Type: RW
BitField Desc: sw write "1" to force, hw auto clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_bert_gen_force_sing_singe_err_Mask                                                          cBit0
#define cAf6_bert_gen_force_sing_singe_err_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : good counter tdm gen ro
Reg Addr   : 0x7_0002 - 0x7_1002
Reg Formula: 0x7_0002 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_tdm_gen_ro_Base                                                            0x70002

/*--------------------------------------
BitField Name: bertgenro
BitField Type: R0
BitField Desc: gen goodbit mode read only
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_tdm_gen_ro_bertgenro_Mask                                                    cBit31_0
#define cAf6_goodbit_pen_tdm_gen_ro_bertgenro_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : good counter tdm gen r2c
Reg Addr   : 0x7_0003 - 0x7_1003
Reg Formula: 0x7_0003 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_tdm_gen_r2c_Base                                                           0x70003

/*--------------------------------------
BitField Name: bertgenr2c
BitField Type: R0
BitField Desc: gen goodbit mode read 2 clear
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_tdm_gen_r2c_bertgenr2c_Mask                                                  cBit31_0
#define cAf6_goodbit_pen_tdm_gen_r2c_bertgenr2c_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : BERT GEN CONTROL
Reg Addr   : 0x7_2000 - 0x7_3000
Reg Formula: 0x7_2000 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
This register config mode for bert

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_monpsn_config_Base                                                                0x72000
#define cAf6Reg_bert_monpsn_config_WidthVal                                                                 32

/*--------------------------------------
Field: [10]       %%   Bert_Inv      %% Set 1 to select invert mode %% RW %% 0x0 %% 0x0
--------------------------------------*/
#define cAf6_bert_monpsn_config_BERT_inv_Mask                                                        cBit10
#define cAf6_bert_monpsn_config_BERT_inv_Shift                                                       10


/*--------------------------------------
BitField Name: Thr_Err
BitField Type: RW
BitField Desc: Thrhold declare lost syn sta
BitField Bits: [9:7]
--------------------------------------*/
#define cAf6_bert_monpsn_config_Thr_Err_Mask                                                           cBit9_7
#define cAf6_bert_monpsn_config_Thr_Err_Shift                                                                7

/*--------------------------------------
BitField Name: Thr_Syn
BitField Type: RW
BitField Desc: Thrhold declare syn sta
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_bert_monpsn_config_Thr_Syn_Mask                                                           cBit6_4
#define cAf6_bert_monpsn_config_Thr_Syn_Shift                                                                4

/*--------------------------------------
BitField Name: BertEna
BitField Type: RW
BitField Desc: Set 1 to enable BERT
BitField Bits: [3]
--------------------------------------*/
#define cAf6_bert_monpsn_config_BertEna_Mask                                                             cBit3
#define cAf6_bert_monpsn_config_BertEna_Shift                                                                3

/*--------------------------------------
BitField Name: BertMode
BitField Type: RW
BitField Desc: "0" prbs15, "1" prbs23, "2" prbs31,"3" prbs20, "4" prbs20r, "5"
all1, "6" all0
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_bert_monpsn_config_BertMode_Mask                                                          cBit2_0
#define cAf6_bert_monpsn_config_BertMode_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : good counter psn  r2c
Reg Addr   : 0x7_2001 - 0x7_3001
Reg Formula: 0x7_2001 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_psn_mon_r2c_Base                                                           0x72001

/*--------------------------------------
BitField Name: goodmonr2c
BitField Type: R0
BitField Desc: mon goodbit mode read 2clear
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_psn_mon_r2c_goodmonr2c_Mask                                                  cBit31_0
#define cAf6_goodbit_pen_psn_mon_r2c_goodmonr2c_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : good counter psn  ro
Reg Addr   : 0x7_2002 - 0x7_3002
Reg Formula: 0x7_2002 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_psn_mon_ro_Base                                                            0x72002

/*--------------------------------------
BitField Name: goodmonro
BitField Type: R0
BitField Desc: mon goodbit mode read only
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_psn_mon_ro_goodmonro_Mask                                                    cBit31_0
#define cAf6_goodbit_pen_psn_mon_ro_goodmonro_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : error counter psn  r2c
Reg Addr   : 0x7_2003 - 0x7_3003
Reg Formula: 0x7_2003 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_errorbit_pen_psn_mon_r2c_Base                                                          0x72003

/*--------------------------------------
BitField Name: errormonr2c
BitField Type: R0
BitField Desc: mon errorbit mode read 2clear
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_errorbit_pen_psn_mon_r2c_errormonr2c_Mask                                                cBit31_0
#define cAf6_errorbit_pen_psn_mon_r2c_errormonr2c_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : error counter psn  ro
Reg Addr   : 0x7_2004 - 0x7_3004
Reg Formula: 0x7_2004 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_errorbit_pen_psn_mon_ro_Base                                                           0x72004

/*--------------------------------------
BitField Name: errormonro
BitField Type: R0
BitField Desc: mon errorbit mode read only
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_errorbit_pen_psn_mon_ro_errormonro_Mask                                                  cBit31_0
#define cAf6_errorbit_pen_psn_mon_ro_errormonro_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : lost counter psn  r2c
Reg Addr   : 0x7_2005 - 0x7_3005
Reg Formula: 0x7_2005 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_lostbit_pen_psn_mon_r2c_Base                                                           0x72005

/*--------------------------------------
BitField Name: lostmonr2c
BitField Type: R0
BitField Desc: mon lostbit mode read 2clear
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lostbit_pen_psn_mon_r2c_lostmonr2c_Mask                                                  cBit31_0
#define cAf6_lostbit_pen_psn_mon_r2c_lostmonr2c_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : lost counter psn  ro
Reg Addr   : 0x7_2006 - 0x7_3006
Reg Formula: 0x7_2006 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_lostbit_pen_psn_mon_ro_Base                                                            0x72006

/*--------------------------------------
BitField Name: lostmonro
BitField Type: R0
BitField Desc: mon lostbit mode read only
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lostbit_pen_psn_mon_ro_lostmonro_Mask                                                    cBit31_0
#define cAf6_lostbit_pen_psn_mon_ro_lostmonro_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : lost counter psn  ro
Reg Addr   : 0x7_2007 - 0x7_3007
Reg Formula: 0x7_2007 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Status

------------------------------------------------------------------------------*/
#define cAf6Reg_pen_psn_mon_status_Base                                                            0x72007

/*--------------------------------------
BitField Name: staprbs
BitField Type: R0
BitField Desc: "3" SYNC , other LOST
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pen_psn_mon_staprbs_Mask                                                       cBit1_0
#define cAf6_pen_psn_mon_staprbs_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : lost counter psn  ro
Reg Addr   : 0x7_2008 - 0x7_3008
Reg Formula: 0x7_2008 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_pen_psn_mon_sticky_Base                                                            0x72008

/*--------------------------------------
BitField Name: stickyprbs
BitField Type: W1C
BitField Desc: "1" LOSTSYN
BitField Bits: [0:0]
--------------------------------------*/
#define cAf6_pen_psn_mon_stickyprbs_Mask                                                      cBit0
#define cAf6_pen_psn_mon_stickyprbs_Shift                                                         0

#endif /* _AF6_REG_AF6CNC0022_RD_PDA_H_ */

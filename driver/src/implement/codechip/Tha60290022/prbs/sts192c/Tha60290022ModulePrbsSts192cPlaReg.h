/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_PLA_H_
#define _AF6_REG_AF6CNC0022_RD_PLA_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : BERT GEN CONTROL
Reg Addr   : 0x4_4000 - 0x4_5000
Reg Formula: 0x4_4000 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
This register config mode for bert

------------------------------------------------------------------------------*/
#define cAf6Reg_bert_montdm_config_Base                                                                0x44000
#define cAf6Reg_bert_montdm_config_WidthVal                                                                 32

/*----------------------------------------
Field: [10]       %%   BERT_inv      %% Set 1 to enbale invert mode  %% RW %% 0x1 %% 0x1
-----------------------------------------*/
#define cAf6_bert_montdm_config_BERT_inv_Mask                                                        cBit10
#define cAf6_bert_montdm_config_BERT_inv_Shift                                                       10

/*--------------------------------------
BitField Name: Thr_Err
BitField Type: RW
BitField Desc: Thrhold declare lost syn sta
BitField Bits: [9:7]
--------------------------------------*/
#define cAf6_bert_montdm_config_Thr_Err_Mask                                                           cBit9_7
#define cAf6_bert_montdm_config_Thr_Err_Shift                                                                7

/*--------------------------------------
BitField Name: Thr_Syn
BitField Type: RW
BitField Desc: Thrhold declare syn sta
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_bert_montdm_config_Thr_Syn_Mask                                                           cBit6_4
#define cAf6_bert_montdm_config_Thr_Syn_Shift                                                                4

/*--------------------------------------
BitField Name: BertEna
BitField Type: RW
BitField Desc: Set 1 to enable BERT
BitField Bits: [3]
--------------------------------------*/
#define cAf6_bert_montdm_config_BertEna_Mask                                                             cBit3
#define cAf6_bert_montdm_config_BertEna_Shift                                                                3

/*--------------------------------------
BitField Name: BertMode
BitField Type: RW
BitField Desc: "0" prbs15, "1" prbs23, "2" prbs31,"3" prbs20, "4" prbs20r, "5"
all1,"6" all0
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_bert_montdm_config_BertMode_Mask                                                          cBit2_0
#define cAf6_bert_montdm_config_BertMode_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : good counter tdm  r2c
Reg Addr   : 0x4_4001 - 0x4_5001
Reg Formula: 0x4_4001 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_tdm_mon_r2c_Base                                                           0x44001

/*--------------------------------------
BitField Name: goodmonr2c
BitField Type: R0
BitField Desc: mon goodbit mode read 2clear
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_tdm_mon_r2c_goodmonr2c_Mask                                                  cBit31_0
#define cAf6_goodbit_pen_tdm_mon_r2c_goodmonr2c_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : good counter tdm  ro
Reg Addr   : 0x4_4002 - 0x4_5002
Reg Formula: 0x4_4002 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_tdm_mon_ro_Base                                                            0x44002

/*--------------------------------------
BitField Name: goodmonro
BitField Type: R0
BitField Desc: mon goodbit mode read only
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_tdm_mon_ro_goodmonro_Mask                                                    cBit31_0
#define cAf6_goodbit_pen_tdm_mon_ro_goodmonro_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : error counter tdm  r2c
Reg Addr   : 0x4_4003 - 0x4_5003
Reg Formula: 0x4_4003 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_errorbit_pen_tdm_mon_r2c_Base                                                          0x44003

/*--------------------------------------
BitField Name: errormonr2c
BitField Type: R0
BitField Desc: mon errorbit mode read 2clear
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_errorbit_pen_tdm_mon_r2c_errormonr2c_Mask                                                cBit31_0
#define cAf6_errorbit_pen_tdm_mon_r2c_errormonr2c_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : error counter tdm  ro
Reg Addr   : 0x4_4004 - 0x4_5004
Reg Formula: 0x4_4004 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_errorbit_pen_tdm_mon_ro_Base                                                           0x44004

/*--------------------------------------
BitField Name: errormonro
BitField Type: R0
BitField Desc: mon errorbit mode read only
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_errorbit_pen_tdm_mon_ro_errormonro_Mask                                                  cBit31_0
#define cAf6_errorbit_pen_tdm_mon_ro_errormonro_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : lost counter tdm  r2c
Reg Addr   : 0x4_4005 - 0x4_5005
Reg Formula: 0x4_4005 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_lostbit_pen_tdm_mon_r2c_Base                                                           0x44005

/*--------------------------------------
BitField Name: lostmonr2c
BitField Type: R0
BitField Desc: mon lostbit mode read 2clear
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lostbit_pen_tdm_mon_r2c_lostmonr2c_Mask                                                  cBit31_0
#define cAf6_lostbit_pen_tdm_mon_r2c_lostmonr2c_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : lost counter tdm  ro
Reg Addr   : 0x4_4006 - 0x4_5006
Reg Formula: 0x4_4006 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_lostbit_pen_tdm_mon_ro_Base                                                            0x44006

/*--------------------------------------
BitField Name: lostmonro
BitField Type: R0
BitField Desc: mon lostbit mode read only
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lostbit_pen_tdm_mon_ro_lostmonro_Mask                                                    cBit31_0
#define cAf6_lostbit_pen_tdm_mon_ro_lostmonro_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : lost counter tdm  ro
Reg Addr   : 0x4_4007 - 0x4_5007
Reg Formula: 0x4_4007 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Status

------------------------------------------------------------------------------*/
#define cAf6Reg_pen_tdm_mon_status_Base                                                            0x44007

/*--------------------------------------
BitField Name: staprbs
BitField Type: R0
BitField Desc: "3" SYNC , other LOST
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pen_tdm_mon_staprbs_Mask                                                       cBit1_0
#define cAf6_pen_tdm_mon_staprbs_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : lost counter tdm  ro
Reg Addr   : 0x4_4008 - 0x4_5008
Reg Formula: 0x4_4008 + $bid * 0x1000
    Where  : 
           + $bid(0-1): BERT ID
Reg Desc   : 
Sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_pen_tdm_mon_sticky_Base                                                            0x44008

/*--------------------------------------
BitField Name: stickyprbs
BitField Type: W1C
BitField Desc: "1" LOSTSYN
BitField Bits: [0:0]
--------------------------------------*/
#define cAf6_pen_tdm_mon_stickyprbs_Mask                                                      cBit0
#define cAf6_pen_tdm_mon_stickyprbs_Shift                                                         0

#endif /* _AF6_REG_AF6CNC0022_RD_PLA_H_ */

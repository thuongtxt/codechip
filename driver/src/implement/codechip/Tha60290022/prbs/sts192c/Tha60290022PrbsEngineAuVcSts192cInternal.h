/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290022PrbsEngineAuVcSts192cInternal.h
 * 
 * Created Date: Apr 26, 2018
 *
 * Description : STS192c PRBS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PRBSENGINEAUVCSTS192CINTERNAL_H_
#define _THA60290022PRBSENGINEAUVCSTS192CINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../Tha60290022PrbsEngineAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022PrbsEngineAuVcSts192c
    {
    tTha60290022PrbsEngineAuVc super;
    }tTha60290022PrbsEngineAuVcSts192c;

typedef struct tTha60290022PrbsEngineAuVcSts192cV2
    {
    tTha60290022PrbsEngineAuVcSts192c super;
    }tTha60290022PrbsEngineAuVcSts192cV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60290022PrbsEngineAuVcSts192cObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PRBSENGINEAUVCSTS192CINTERNAL_H_ */


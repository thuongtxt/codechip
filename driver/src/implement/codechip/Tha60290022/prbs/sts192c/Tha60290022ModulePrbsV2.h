/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290022ModulePrbsV2.h
 * 
 * Created Date: Apr 5, 2018
 *
 * Description : Interface of prbs module support sts192c bert
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPRBSV2_H_
#define _THA60290022MODULEPRBSV2_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60290022ModulePrbsSts192cHwEngineOffsetFromHwEgineId(uint32 hwEngineId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPRBSV2_H_ */


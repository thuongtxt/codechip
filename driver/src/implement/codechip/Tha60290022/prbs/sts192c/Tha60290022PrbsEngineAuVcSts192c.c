/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290022PrbsEngineAuVcSts192c.c
 *
 * Created Date: Apr 4, 2018
 *
 * Description : Implement STS-192c BERT engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pda/ThaModulePda.h"
#include "../../../../default/pwe/ThaModulePwe.h"
#include "../../../../default/man/ThaDevice.h"
#include "../../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../Tha60290022ModulePrbs.h"
#include "../Tha60290022PrbsHwController.h"
#include "Tha60290022ModulePrbsSts192cPdaReg.h"
#include "Tha60290022ModulePrbsSts192cPlaReg.h"
#include "Tha60290022ModulePrbsV2.h"
#include "Tha60290022PrbsEngineAuVcSts192cInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cSts192cTerminatedLine_0 24
#define cSts192cTerminatedLine_1 28

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods    m_AtPrbsEngineOverride;
static tThaPrbsEngineMethods   m_ThaPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;
static const tThaPrbsEngineMethods *m_ThaPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Read(ThaPrbsEngine self, uint32 regAddr)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    return mChannelHwRead(channel, regAddr, cAtModulePrbs);
    }

static void Write(ThaPrbsEngine self, uint32 regAddr, uint32 regVal)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    mChannelHwWrite(channel, regAddr, regVal, cAtModulePrbs);
    }

static void TxChannelIdSet(ThaPrbsEngine self, uint32 channelId)
    {
    AtUnused(self);
    AtUnused(channelId);
    }

static void DefaultThresholdSet(ThaPrbsEngine self)
    {
    uint32 address = mMethodsGet(self)->BertMonTdmSelectedChannelRegAddress(self);
    uint32 regVal = Read(self, address);
    mRegFieldSet(regVal, cAf6_bert_montdm_config_Thr_Err_, 3);
    mRegFieldSet(regVal, cAf6_bert_montdm_config_Thr_Syn_, 3);
    Write(self, address, regVal);

    address = mMethodsGet(self)->BertMonPsnSelectedChannelRegAddress(self);
    regVal = Read(self, address);
    mRegFieldSet(regVal, cAf6_bert_monpsn_config_Thr_Err_, 3);
    mRegFieldSet(regVal, cAf6_bert_monpsn_config_Thr_Syn_, 3);
    Write(self, address, regVal);
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = ThaPrbsEngineDefaultClassInit((ThaPrbsEngine)self);
    DefaultThresholdSet((ThaPrbsEngine)self);

    return ret;
    }

static uint32 ChannelToHwEngineId(AtChannel self)
    {
    uint8 line = AtSdhChannelLineGet((AtSdhChannel)self);
    if (line == cSts192cTerminatedLine_0)
        return 0;

    if (line == cSts192cTerminatedLine_1)
        return 1;

    return 0;
    }

static uint32 HwEngineId(ThaPrbsEngine self)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    AtChannel channel = AtPrbsEngineChannelGet(engine);
    return ChannelToHwEngineId(channel);
    }

static ThaModulePda ModulePda(ThaPrbsEngine self)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    AtChannel channel = AtPrbsEngineChannelGet(engine);
    AtDevice device = AtChannelDeviceGet(channel);
    ThaModulePda modulePda = (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
    return modulePda;
    }

static ThaModulePwe ModulePwe(ThaPrbsEngine self)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    AtChannel channel = AtPrbsEngineChannelGet(engine);
    AtDevice device = AtChannelDeviceGet(channel);
    ThaModulePwe modulePwe = (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    return modulePwe;
    }

static uint32 TdmMonBaseAddress(ThaPrbsEngine self)
    {
    uint32 address = ThaModulePwePlaBaseAddress(ModulePwe(self));

    return address;
    }

static uint32 TdmGenBaseAddress(ThaPrbsEngine self)
    {
    uint32 address = ThaModulePdaBaseAddress(ModulePda(self));

    return address;
    }

static uint32 PsnMonBaseAddress(ThaPrbsEngine self)
    {
    return TdmGenBaseAddress(self);
    }

static uint32 HwEngineOffset(ThaPrbsEngine self)
    {
    uint32 address;
    address = Tha60290022ModulePrbsSts192cHwEngineOffsetFromHwEgineId(mHwEngine(self));
    return address;
    }

static uint32 BertGenSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = TdmGenBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_bert_gen_config_Base + offset;
    return address;
    }

static uint32 BertGenEnableMask(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAf6_bert_gen_config_BertEna_Mask;
    }

static eAtModulePrbsRet HwTxTdmEnable(ThaPrbsEngine self, eBool enable)
    {
    uint32 regAddr = mMethodsGet(self)->BertGenSelectedChannelRegAddress(self);
    uint32 regVal = Read(self, regAddr);
    uint32 mask = mMethodsGet(self)->BertGenEnableMask(self);
    uint32 shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, mBoolToBin(enable));
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 BertMonTdmSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = TdmMonBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_bert_montdm_config_Base + offset;
    return address;
    }

static uint32 BertMonTdmEnableMask(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAf6_bert_montdm_config_BertEna_Mask;
    }

static eAtRet TdmSideRxEnable(ThaPrbsEngine self, eBool enable)
    {
    uint32 mask, shift;
    uint32 regAddr = mMethodsGet(self)->BertMonTdmSelectedChannelRegAddress(self);
    uint32 regVal = Read(self, regAddr);

    mask  = mMethodsGet(self)->BertMonTdmEnableMask(self);
    shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, mBoolToBin(enable));
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 BertMonPsnSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = PsnMonBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_bert_monpsn_config_Base + offset;
    return address;
    }

static uint32 BertMonPsnEnableMask(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAf6_bert_monpsn_config_BertEna_Mask;
    }

static eAtRet HwRxPsnEnable(ThaPrbsEngine self, eBool enable)
    {
    uint32 regAddr, regVal, mask, shift;

    regAddr = mMethodsGet(self)->BertMonPsnSelectedChannelRegAddress(self);
    regVal  = Read(self, regAddr);

    mask  = mMethodsGet(self)->BertMonPsnEnableMask(self);
    shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, mBoolToBin(enable));
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 BertGenErrorRateInsertRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = TdmGenBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_bert_gen_force_Base + offset;
    return address;
    }

static uint32 BertMonTmdLossynStickyRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = TdmMonBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_pen_tdm_mon_sticky_Base + offset;
    return address;
    }

static uint32 BertMonPsnLossynStickyRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = PsnMonBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_pen_psn_mon_sticky_Base + offset;
    return address;
    }

static uint32 AlarmHistoryHelper(AtPrbsEngine self, eBool r2c)
    {
    uint32 regAddr;
    uint32 regVal;
    uint8 alarm = 0;
    ThaPrbsEngine engine = (ThaPrbsEngine)self;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
        {
        regAddr = BertMonTmdLossynStickyRegAddress(engine);
        regVal = Read(engine, regAddr);
        alarm = mRegField(regVal, cAf6_pen_tdm_mon_stickyprbs_);

        if (r2c)
            Write(engine, regAddr, cAf6_pen_tdm_mon_stickyprbs_Mask);
        return (alarm) ? cAtPrbsEngineAlarmTypeLossSync : cAtPrbsEngineAlarmTypeNone;
        }

    if (side == cAtPrbsSidePsn)
        {
        regAddr = BertMonPsnLossynStickyRegAddress(engine);
        regVal = Read(engine, regAddr);
        alarm = mRegField(regVal, cAf6_pen_psn_mon_stickyprbs_);

        if (r2c)
            Write(engine, regAddr, cAf6_pen_psn_mon_stickyprbs_Mask);
        return (alarm) ? cAtPrbsEngineAlarmTypeLossSync : cAtPrbsEngineAlarmTypeNone;
        }

    return 0;
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtTrue);
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtFalse);
    }

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
    {
    AtUnused(self);
    if (!invert)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eBool TxIsInverted(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
    {
    AtUnused(self);
    if (!invert)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eBool RxIsInverted(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 BertGenErrorSingleInsertRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = TdmGenBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_bert_gen_force_sing_Base + offset;
    return address;
    }

static eAtModulePrbsRet HwTxErrorInject(AtPrbsEngine self, uint32 numErrorForced)
    {
    uint32 regAddr, error_i;
    eBool needDelay = (numErrorForced > 1) ? cAtTrue : cAtFalse;
    static const uint32 cDelayUs = 150; /* The requirement is > 125us, give it 150us */
    ThaPrbsEngine engine = (ThaPrbsEngine)self;

    if (AtDeviceIsSimulated(AtPrbsEngineDeviceGet(self)))
        needDelay = cAtFalse;

    regAddr = BertGenErrorSingleInsertRegAddress(engine);
    for (error_i = 0; error_i < numErrorForced; error_i++)
        {
        Write(engine, regAddr, 1);
        if (needDelay)
            AtOsalUSleep(cDelayUs);
        }

    return cAtOk;
    }

static eAtModulePrbsRet TxErrorInject(AtPrbsEngine self, uint32 numErrors)
    {
    return HwTxErrorInject(self, numErrors);
    }

static uint32 PrbsModeSw2Hw(eAtPrbsMode prbsMode, uint32 fixPattern)
    {
    switch ((uint32)prbsMode)
        {
        case cAtPrbsModePrbs15        : return 0x0;
        case cAtPrbsModePrbs23        : return 0x1;
        case cAtPrbsModePrbs31        : return 0x2;
        case cAtPrbsModePrbs20StdO151 : return 0x3;
        case cAtPrbsModePrbs20rStdO153: return 0x4;

        case cSupportedFixPatternMode :
            if (fixPattern == cPatternAllOne)
                return 0x05;
            if (fixPattern == cPatternAllZero)
                return 0x06;
            return 0x7;

        default:
            return cInvalidUint32;
        }
    }

static eAtPrbsMode PrbsModeHw2Sw(uint32 prbsMode)
    {
    switch ((uint32)prbsMode)
        {
        case 0x0: return cAtPrbsModePrbs15;
        case 0x1: return cAtPrbsModePrbs23;
        case 0x2: return cAtPrbsModePrbs31;
        case 0x3 : return cAtPrbsModePrbs20StdO151;
        case 0x4 : return cAtPrbsModePrbs20rStdO153;

        case 0x5:
            return cSupportedFixPatternMode;
        case 0x6:
            return cSupportedFixPatternMode;
        case 0x7:
            return cSupportedFixPatternMode;

        default:
            return cAtPrbsModeInvalid;
        }
    }

static eBool IsFixedPatternAllOneFromHwMode(AtPrbsEngine self, uint32 hwMode)
    {
    AtUnused(self);
    if (hwMode == 0x5)
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsFixedPatternAllZeroFromHwMode(AtPrbsEngine self, uint32 hwMode)
    {
    AtUnused(self);
    if (hwMode == 0x6)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 FixedPatternHwToSw(AtPrbsEngine self, uint32 hwMode)
    {
    if (IsFixedPatternAllOneFromHwMode(self, hwMode))
        return cPatternAllOne;

    if (IsFixedPatternAllZeroFromHwMode(self, hwMode))
        return cPatternAllZero;

    return cDefaultFixPattern;
    }

static eAtModulePrbsRet HwTxTdmModeSet(ThaPrbsEngine self, eAtPrbsMode prbsMode, uint32 fixedPattern)
    {
    uint32 regAddr, regVal, hwMode;

    regAddr = mMethodsGet(self)->BertGenSelectedChannelRegAddress(self);
    regVal = Read(self, regAddr);
    hwMode = PrbsModeSw2Hw(prbsMode, fixedPattern);
    mRegFieldSet(regVal, cAf6_bert_gen_config_BertMode_, hwMode);
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eAtPrbsMode HwTxTdmModeGet(ThaPrbsEngine self)
    {
    uint32 regAddr, regVal, hwMode;
    regAddr = mMethodsGet(self)->BertGenSelectedChannelRegAddress(self);
    regVal = Read(self, regAddr);
    hwMode = mRegField(regVal, cAf6_bert_gen_config_BertMode_);
    return PrbsModeHw2Sw(hwMode);
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    return HwTxTdmModeSet((ThaPrbsEngine)self, prbsMode, cDefaultFixPattern);
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    return HwTxTdmModeGet((ThaPrbsEngine)self);
    }

static eAtModulePrbsRet HwRxTdmModeSet(ThaPrbsEngine self, eAtPrbsMode prbsMode, uint32 fixedPattern)
    {
    uint32 regAddr, regVal, hwMode;

    regAddr = mMethodsGet(self)->BertMonTdmSelectedChannelRegAddress(self);
    regVal = Read(self, regAddr);
    hwMode = PrbsModeSw2Hw(prbsMode, fixedPattern);
    mRegFieldSet(regVal, cAf6_bert_montdm_config_BertMode_, hwMode);
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eAtPrbsMode HwRxTdmModeGet(ThaPrbsEngine self)
    {
    uint32 regAddr, regVal, hwMode;
    regAddr = mMethodsGet(self)->BertMonTdmSelectedChannelRegAddress(self);
    regVal = Read(self, regAddr);
    hwMode = mRegField(regVal, cAf6_bert_montdm_config_BertMode_);
    return PrbsModeHw2Sw(hwMode);
    }

static eAtModulePrbsRet HwRxPsnModeSet(ThaPrbsEngine self, eAtPrbsMode prbsMode, uint32 fixedPattern)
    {
    uint32 regAddr, regVal, hwMode;

    regAddr = mMethodsGet(self)->BertMonPsnSelectedChannelRegAddress(self);
    regVal = Read(self, regAddr);
    hwMode = PrbsModeSw2Hw(prbsMode, fixedPattern);
    mRegFieldSet(regVal, cAf6_bert_monpsn_config_BertMode_, hwMode);
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eAtPrbsMode HwRxPsnModeGet(ThaPrbsEngine self)
    {
    uint32 regAddr, regVal, hwMode;
    regAddr = mMethodsGet(self)->BertMonPsnSelectedChannelRegAddress(self);
    regVal = Read(self, regAddr);
    hwMode = mRegField(regVal, cAf6_bert_monpsn_config_BertMode_);
    return PrbsModeHw2Sw(hwMode);
    }

static eAtModulePrbsRet RxHwModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode, uint32 fixedPattern)
    {
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
        return HwRxTdmModeSet((ThaPrbsEngine)self, prbsMode, fixedPattern);

    if (side == cAtPrbsSidePsn)
        return HwRxPsnModeSet((ThaPrbsEngine)self, prbsMode, fixedPattern);

    return cAtErrorRsrcNoAvail;
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    return RxHwModeSet(self, prbsMode, cDefaultFixPattern);
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
        return HwRxTdmModeGet((ThaPrbsEngine)self);

    if (side == cAtPrbsSidePsn)
        return HwRxPsnModeGet((ThaPrbsEngine)self);

    return cAtPrbsModeInvalid;
    }

static eBool FixedPatternIsSupported(AtPrbsEngine self, uint32 fixedPattern)
    {
    AtUnused(self);
    if ((fixedPattern == cPatternAllOne) || (fixedPattern == cPatternAllZero))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    if (AtPrbsEngineTxModeGet(self) != cSupportedFixPatternMode)
        return cAtErrorInvalidOperation;

    if (!FixedPatternIsSupported(self, fixedPattern))
        return cAtErrorModeNotSupport;

    return HwTxTdmModeSet((ThaPrbsEngine)self, cSupportedFixPatternMode, fixedPattern);
    }

static uint32 HwTxTdmFixedPatternGet(ThaPrbsEngine self)
    {
    uint32 regAddr, regVal, hwMode;
    regAddr = mMethodsGet(self)->BertGenSelectedChannelRegAddress(self);
    regVal = Read(self, regAddr);
    hwMode = mRegField(regVal, cAf6_bert_gen_config_BertMode_);
    return hwMode;
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    ThaPrbsEngine engine = (ThaPrbsEngine)self;
    uint32 hwMode = HwTxTdmFixedPatternGet(engine);

    return FixedPatternHwToSw(self, hwMode);
    }

static uint32 HwTdmMonFixedPatternGet(ThaPrbsEngine self)
    {
    uint32 regAddr, regVal, hwMode;
    regAddr = mMethodsGet(self)->BertMonTdmSelectedChannelRegAddress(self);
    regVal = Read(self, regAddr);
    hwMode = mRegField(regVal, cAf6_bert_montdm_config_BertMode_);
    return hwMode;
    }

static uint32 HwPsnMonFixedPatternGet(ThaPrbsEngine self)
    {
    uint32 regAddr, regVal, hwMode;
    regAddr = mMethodsGet(self)->BertMonPsnSelectedChannelRegAddress(self);
    regVal = Read(self, regAddr);
    hwMode = mRegField(regVal, cAf6_bert_monpsn_config_BertMode_);
    return hwMode;
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    if (AtPrbsEngineRxModeGet(self) != cSupportedFixPatternMode)
        return cAtErrorInvalidOperation;

    if (!FixedPatternIsSupported(self, fixedPattern))
        return cAtErrorModeNotSupport;

    return RxHwModeSet(self, cSupportedFixPatternMode, fixedPattern);
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    ThaPrbsEngine engine = (ThaPrbsEngine)self;
    uint32 hwMode = 0;

    if (side == cAtPrbsSideTdm)
        {
        hwMode = HwTdmMonFixedPatternGet(engine);
        return FixedPatternHwToSw(self, hwMode);
        }

    if (side == cAtPrbsSidePsn)
        {
        hwMode = HwPsnMonFixedPatternGet(engine);
        return FixedPatternHwToSw(self, hwMode);
        }

    return cDefaultFixPattern;
    }

static uint32 BertGenTdmGoodBitCounterR2cRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = TdmGenBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_goodbit_pen_tdm_gen_r2c_Base + offset;
    return address;
    }

static uint32 BertMonTdmGoodBitCounterR2cRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = TdmMonBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_goodbit_pen_tdm_mon_r2c_Base + offset;
    return address;
    }

static uint32 BertMonTdmErrorBitCounterR2cRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = TdmMonBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_errorbit_pen_tdm_mon_r2c_Base + offset;
    return address;
    }

static uint32 BertMonTdmLostBitCounterR2cRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = TdmMonBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_lostbit_pen_tdm_mon_r2c_Base + offset;
    return address;
    }

static uint32 BertMonPsnGoodBitCounterR2cRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = PsnMonBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_goodbit_pen_psn_mon_r2c_Base + offset;
    return address;
    }

static uint32 BertMonPsnErrorBitCounterR2cRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = PsnMonBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_errorbit_pen_psn_mon_r2c_Base + offset;
    return address;
    }

static uint32 BertMonPsnLostBitCounterR2cRegAddress(ThaPrbsEngine self)
    {
    uint32 baseAddress = PsnMonBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_lostbit_pen_psn_mon_r2c_Base + offset;
    return address;
    }

static uint64 LongCounterRead(ThaPrbsEngine self, uint32 regAddr, AtModule module)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint64 counterVal = 0;
    uint32 longRegVal[cThaLongRegMaxSize];
    AtDevice device = AtChannelDeviceGet(channel);
    AtIpCore ipCore = AtDeviceIpCoreGet(device, 0);

    mModuleHwLongRead(module, regAddr, longRegVal, cThaLongRegMaxSize, ipCore);
    counterVal = longRegVal[1] & cBit1_0;
    counterVal = counterVal << 32;
    counterVal = counterVal + longRegVal[0];

    return counterVal;
    }

static eAtModulePrbsRet RxTdmAllCountersLatchAndClear(AtPrbsEngine self, eBool clear)
    {
    uint64 rxErrorBitCount, rxGoodBitCount, rxLostBitCount, rxBitCount;
    ThaPrbsEngine engine = (ThaPrbsEngine)self;
    AtPrbsCounters counters = AtPrbsEnginePrbsCountersGet(self);
    AtUnused(clear);

    rxErrorBitCount = Read(engine, BertMonTdmErrorBitCounterR2cRegAddress(engine));
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitError, rxErrorBitCount);

    rxGoodBitCount = LongCounterRead(engine, BertMonTdmGoodBitCounterR2cRegAddress(engine), (AtModule)ModulePwe(engine));
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxSync, rxGoodBitCount);

    rxLostBitCount = Read(engine, BertMonTdmLostBitCounterR2cRegAddress(engine));
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitLoss, rxLostBitCount);

    rxBitCount = rxGoodBitCount + rxErrorBitCount + rxLostBitCount;
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBit, rxBitCount);

    return cAtOk;
    }

static eAtModulePrbsRet RxPsnAllCountersLatchAndClear(AtPrbsEngine self, eBool clear)
    {
    uint64 rxErrorBitCount, rxGoodBitCount, rxLostBitCount, rxBitCount;
    ThaPrbsEngine engine = (ThaPrbsEngine)self;
    AtPrbsCounters counters = AtPrbsEnginePrbsCountersGet(self);
    AtUnused(clear);

    rxErrorBitCount = Read(engine, BertMonPsnErrorBitCounterR2cRegAddress(engine));
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitError, rxErrorBitCount);

    rxGoodBitCount = LongCounterRead(engine, BertMonPsnGoodBitCounterR2cRegAddress(engine), (AtModule)ModulePda(engine));
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxSync, rxGoodBitCount);

    rxLostBitCount = Read(engine, BertMonPsnLostBitCounterR2cRegAddress(engine));
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitLoss, rxLostBitCount);

    rxBitCount = rxGoodBitCount + rxErrorBitCount + rxLostBitCount;
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBit, rxBitCount);

    return cAtOk;
    }

static eAtModulePrbsRet AllCountersLatchAndClear(AtPrbsEngine self, eBool clear)
    {
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    uint64 counterValue;
    ThaPrbsEngine engine = (ThaPrbsEngine)self;
    AtUnused(clear);

    counterValue = LongCounterRead(engine, BertGenTdmGoodBitCounterR2cRegAddress(engine), (AtModule)ModulePda(engine));
    AtPrbsCountersCounterUpdate(AtPrbsEnginePrbsCountersGet(self), cAtPrbsEngineCounterTxBit, counterValue);

    if (side == cAtPrbsSideTdm)
        return RxTdmAllCountersLatchAndClear(self, clear);

    if (side == cAtPrbsSidePsn)
        return RxPsnAllCountersLatchAndClear(self, clear);

    return cAtOk;
    }

static eAtModulePrbsRet TxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    AtUnused(self);
    if (order == cAtPrbsBitOrderMsb)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eAtPrbsBitOrder TxBitOrderGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsBitOrderMsb;
    }

static eAtModulePrbsRet RxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    return TxBitOrderSet(self, order);
    }

static eAtPrbsBitOrder RxBitOrderGet(AtPrbsEngine self)
    {
    return TxBitOrderGet(self);
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    return ThaPrbsEngineDefaultClassMonitoringSideSet(self, side);
    }

static uint32 TdmMonitoringStatusRegAddress(ThaPrbsEngine self, uint32 engineId, uint32 slice)
    {
    uint32 baseAddress = TdmMonBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_pen_tdm_mon_status_Base + offset;

    AtUnused(engineId);
    AtUnused(slice);
    return address;
    }

static uint32 PsnMonitoringStatusRegAddress(ThaPrbsEngine self, uint32 engineId, uint32 slice)
    {
    uint32 baseAddress = PsnMonBaseAddress(self);
    uint32 offset = HwEngineOffset(self);
    uint32 address = baseAddress + cAf6Reg_pen_psn_mon_status_Base + offset;

    AtUnused(engineId);
    AtUnused(slice);
    return address;
    }

static void TimeslotDefaultSet(ThaPrbsEngine self, uint32 bitMap)
    {
    AtUnused(self);
    AtUnused(bitMap);
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    return ThaPrbsEngineDefaultClassRxEnable(self, enable);
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    return ThaPrbsEngineDefaultClassRxIsEnabled(self);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    return ThaPrbsEngineDefaultClassAlarmGet(self);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, RxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorInject);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideThaPrbsEngine(AtPrbsEngine self)
    {
    ThaPrbsEngine engine = (ThaPrbsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, m_ThaPrbsEngineMethods, sizeof(m_ThaPrbsEngineOverride));

        /*Updated methods */
        mMethodOverride(m_ThaPrbsEngineOverride, TxChannelIdSet);
        mMethodOverride(m_ThaPrbsEngineOverride, HwEngineId);

        mMethodOverride(m_ThaPrbsEngineOverride, BertGenSelectedChannelRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, BertGenEnableMask);
        mMethodOverride(m_ThaPrbsEngineOverride, HwTxTdmEnable);
        mMethodOverride(m_ThaPrbsEngineOverride, TimeslotDefaultSet);

        mMethodOverride(m_ThaPrbsEngineOverride, BertMonPsnSelectedChannelRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, BertMonPsnEnableMask);
        mMethodOverride(m_ThaPrbsEngineOverride, HwRxPsnEnable);

        mMethodOverride(m_ThaPrbsEngineOverride, BertMonTdmSelectedChannelRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, BertMonTdmEnableMask);
        mMethodOverride(m_ThaPrbsEngineOverride, TdmSideRxEnable);
        mMethodOverride(m_ThaPrbsEngineOverride, BertGenErrorRateInsertRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, TdmMonitoringStatusRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, PsnMonitoringStatusRegAddress);
        }

    mMethodsSet(engine, &m_ThaPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideThaPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PrbsEngineAuVcSts192c);
    }

AtPrbsEngine Tha60290022PrbsEngineAuVcSts192cObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022PrbsEngineAuVcObjectInit(self, vc, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290022PrbsEngineAuVcSts192cNew(AtSdhChannel vc, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60290022PrbsEngineAuVcSts192cObjectInit(newEngine, vc, engineId);
    }

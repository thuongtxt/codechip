/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290022ModulePrbsInternal.h
 * 
 * Created Date: Apr 5, 2018
 *
 * Description : Internal data of the 60290022 module prbs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPRBSINTERNAL_H_
#define _THA60290022MODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/prbs/Tha60290021ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModulePrbs
    {
    tTha60290021ModulePrbs super;
    }tTha60290022ModulePrbs;

typedef struct tTha60290022ModulePrbsV2
    {
    tTha60290022ModulePrbs super;
    }tTha60290022ModulePrbsV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha60290022ModulePrbsObjectInit(AtModulePrbs self, AtDevice device);
AtModulePrbs Tha60290022ModulePrbsV2ObjectInit(AtModulePrbs self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPRBSINTERNAL_H_ */


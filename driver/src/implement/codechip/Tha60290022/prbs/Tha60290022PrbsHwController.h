/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290022PrbsHwController.h
 * 
 * Created Date: May 1, 2017
 *
 * Description : PRBS HW controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PRBSHWCONTROLLER_H_
#define _THA60290022PRBSHWCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/prbs/ThaPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cPatternAllZero    0x00000000
#define cPatternAllOne     0xFFFFFFFF
#define cDefaultFixPattern cPatternAllZero
#define cSupportedFixPatternMode cAtPrbsModePrbsFixedPattern4Bytes

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Modes */
eBool Tha60290022PrbsEngineModeIsSupported(AtPrbsEngine engine, eAtPrbsMode prbsMode);
eAtModulePrbsRet Tha60290022PrbsEngineTxModeSet(AtPrbsEngine engine, eAtPrbsMode prbsMode);
eAtPrbsMode Tha60290022PrbsEngineTxModeGet(AtPrbsEngine engine);
eAtModulePrbsRet Tha60290022PrbsEngineRxModeSet(AtPrbsEngine engine, eAtPrbsMode prbsMode);
eAtPrbsMode Tha60290022PrbsEngineRxModeGet(AtPrbsEngine engine);

/* Invert */
eAtModulePrbsRet Tha60290022PrbsEngineTxInvert(AtPrbsEngine engine, eBool invert);
eBool Tha60290022PrbsEngineTxIsInverted(AtPrbsEngine engine);
eAtModulePrbsRet Tha60290022PrbsEngineRxInvert(AtPrbsEngine engine, eBool invert);
eBool Tha60290022PrbsEngineRxIsInverted(AtPrbsEngine engine);

/* Bit ordering */
eAtModulePrbsRet Tha60290022PrbsEngineTxBitOrderSet(AtPrbsEngine engine, eAtPrbsBitOrder order);
eAtPrbsBitOrder Tha60290022PrbsEngineTxBitOrderGet(AtPrbsEngine engine);
eAtModulePrbsRet Tha60290022PrbsEngineRxBitOrderSet(AtPrbsEngine engine, eAtPrbsBitOrder order);
eAtPrbsBitOrder Tha60290022PrbsEngineRxBitOrderGet(AtPrbsEngine engine);

/* Fixed pattern */
eAtModulePrbsRet Tha60290022PrbsEngineTxFixedPatternSet(AtPrbsEngine engine, uint32 fixedPattern);
uint32 Tha60290022PrbsEngineTxFixedPatternGet(AtPrbsEngine engine);
eAtModulePrbsRet Tha60290022PrbsEngineRxFixedPatternSet(AtPrbsEngine engine, uint32 fixedPattern);
uint32 Tha60290022PrbsEngineRxFixedPatternGet(AtPrbsEngine engine);
eBool Tha60290022PrbsEngineCanUseAnyFixPattern(AtPrbsEngine engine);

/* Alarms */
uint32 Tha60290022PrbsEngineAlarmHistoryRead2Clear(AtPrbsEngine engine, eBool r2c);

/* Forcing */
eAtModulePrbsRet Tha60290022PrbsEngineTxErrorInject(AtPrbsEngine self, uint32 numErrorForced);

/* Timeslot assignment */
void Tha60290022PrbsEngineTimeslotDefaultSet(ThaPrbsEngine self, uint32 bitMap);
void Tha60290022PrbsEngineTimeslotMonDefaultSet(ThaPrbsEngine self, uint32 bitMap);

/* Side */
eAtModulePrbsRet Tha60290022PrbsEngineMonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side);

void Tha60290022PrbsAllEngineHwFlush(ThaModulePrbs module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PRBSHWCONTROLLER_H_ */

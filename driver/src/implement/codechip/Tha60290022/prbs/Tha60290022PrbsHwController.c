/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290022PrbsHwController.c
 *
 * Created Date: May 1, 2017
 *
 * Description : PRBS HW controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../../../default/prbs/ThaModulePrbs.h"
#include "../../../default/prbs/ThaPrbsEngineInternal.h"
#include "Tha60290022PrbsHwController.h"
#include "Tha60290022ModulePrbsReg.h"
#include "Tha60290022PrbsRegProvider.h"
#include "Tha60290022ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPrbsRegProvider RegisterProvider(AtPrbsEngine engine)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(engine);
    return ThaModulePrbsRegProvider(module);
    }

static uint32 EngineAddressWithLocalAddress(AtPrbsEngine engine, uint32 localAddress)
    {
    ThaPrbsRegProvider provider = RegisterProvider(engine);
    uint32 engineId = ThaPrbsEngineHwEngineId(mThaPrbsEngine(engine));
    return Tha60290022PrbsRegProviderEngineAddressWithLocalAddressAndEngineOffset(provider, engineId, localAddress);
    }

static uint32 RxPrbsModeSw2Hw(eAtPrbsMode prbsMode, uint32 fixPattern)
    {
    switch ((uint32)prbsMode)
        {
        case cAtPrbsModePrbs15        : return 0x4;
        case cAtPrbsModePrbs23        : return 0x3;
        case cAtPrbsModePrbs31        : return 0x5;
        case cAtPrbsModePrbs20StdO151 : return 0x6;
        case cAtPrbsModePrbs20rStdO153: return 0x8;

        case cSupportedFixPatternMode:
            if (fixPattern == cPatternAllZero)
                return 0x02;
            if (fixPattern == cPatternAllOne)
                return 0x0;
            return 0x1;

        default:
            return cInvalidUint32;
        }
    }

static eAtPrbsMode RxPrbsModeHw2Sw(uint32 prbsMode)
    {
    switch (prbsMode)
        {
        case 0x04: return cAtPrbsModePrbs15;
        case 0x03: return cAtPrbsModePrbs23;
        case 0x05: return cAtPrbsModePrbs31;
        case 0x06: return cAtPrbsModePrbs20StdO151;
        case 0x08: return cAtPrbsModePrbs20rStdO153;

        case 0x02:
            return cSupportedFixPatternMode;

        case 0x0:
            return cSupportedFixPatternMode;

        case 0x1: return cSupportedFixPatternMode;

        default:
            return cAtPrbsModeInvalid;
        }
    }

static uint32 TxPrbsModeSw2Hw(eAtPrbsMode prbsMode, uint32 fixPattern)
    {
    switch ((uint32)prbsMode)
        {
        case cAtPrbsModePrbs15        : return 0x2;
        case cAtPrbsModePrbs23        : return 0x3;
        case cAtPrbsModePrbs31        : return 0x5;
        case cAtPrbsModePrbs20StdO151 : return 0x8;
        case cAtPrbsModePrbs20rStdO153: return 0x4;

        case cSupportedFixPatternMode :
            if (fixPattern == cPatternAllZero)
                return 0x01;
            if (fixPattern == cPatternAllOne)
                return 0x07;
            return 0x6;

        default:
            return cInvalidUint32;
        }
    }

static eAtPrbsMode TxPrbsModeHw2Sw(uint32 prbsMode)
    {
    switch ((uint32)prbsMode)
        {
        case 0x02: return cAtPrbsModePrbs15;
        case 0x03: return cAtPrbsModePrbs23;
        case 0x05: return cAtPrbsModePrbs31;
        case 0x8 : return cAtPrbsModePrbs20StdO151;
        case 0x4 : return cAtPrbsModePrbs20rStdO153;

        case 0x01:
            return cSupportedFixPatternMode;

        case 0x07:
            return cSupportedFixPatternMode;

        case 0x06:
            return cSupportedFixPatternMode;

        default:
            return cAtPrbsModeInvalid;
        }
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine engine, eAtPrbsMode prbsMode, uint32 pattern)
    {
    uint32 regAddr, regVal;
    uint32 hwMode = TxPrbsModeSw2Hw(prbsMode, pattern);
    uint32 modeMask = cAf6_ctrl_pen_gen_patt_mode_Mask;
    uint32 modeShift = AtRegMaskToShift(modeMask);

    regAddr = EngineAddressWithLocalAddress(engine, cAf6Reg_ctrl_pen_gen_Base);
    regVal  = AtPrbsEngineRead(engine, regAddr, cAtModulePrbs);
    mFieldIns(&regVal, modeMask, modeShift, hwMode);
    AtPrbsEngineWrite(engine, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eAtModulePrbsRet MonModeSet(AtPrbsEngine engine, eAtPrbsMode prbsMode, uint32 fixedPattern)
    {
    uint32 regAddrWithOffset, regVal;
    uint32 hwMode = RxPrbsModeSw2Hw(prbsMode, fixedPattern);
    uint32 modeRegMask = cAf6_ctrl_pen_montdm_patt_mode_Mask;
    uint32 modeRegShift= AtRegMaskToShift(modeRegMask);

    regAddrWithOffset = EngineAddressWithLocalAddress(engine, cAf6Reg_ctrl_pen_montdm_Base);
    regVal = AtPrbsEngineRead(engine, regAddrWithOffset, cAtModulePrbs);
    mFieldIns(&regVal, modeRegMask, modeRegShift, hwMode);
    AtPrbsEngineWrite(engine, regAddrWithOffset, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eAtModulePrbsRet BitOrderSet(AtPrbsEngine engine, eAtPrbsBitOrder order,  uint32 localAddress, uint32 fieldMask, uint32 fieldShift)
    {
    uint32 regVal = AtPrbsEngineRead(engine, localAddress, cAtModulePrbs);
    mRegFieldSet(regVal, field, (order == cAtPrbsBitOrderLsb) ? 1 : 0);
    AtPrbsEngineWrite(engine, localAddress, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtPrbsBitOrder BitOrderGet(AtPrbsEngine engine, uint32 localAddress, uint32 fieldMask)
    {
    uint32 regVal = AtPrbsEngineRead(engine, localAddress, cAtModulePrbs);
    return (regVal & fieldMask) ? cAtPrbsBitOrderLsb : cAtPrbsBitOrderMsb;
    }

static uint32 HwOffsetOfErrorSingleForceAddress(AtPrbsEngine self)
    {
    ThaPrbsRegProvider provider = RegisterProvider(self);
    uint32 baseAddress = ThaPrbsRegProviderBaseAddress(provider);
    uint32 engineId = ThaPrbsEngineHwEngineId((ThaPrbsEngine)self);
    return (uint32)(baseAddress + (engineId * cEngineOffset));
    }

eBool Tha60290022PrbsEngineModeIsSupported(AtPrbsEngine engine, eAtPrbsMode prbsMode)
    {
    AtUnused(engine);

    switch ((uint32)prbsMode)
        {
        case cAtPrbsModePrbs15        : return cAtTrue;
        case cAtPrbsModePrbs23        : return cAtTrue;
        case cAtPrbsModePrbs31        : return cAtTrue;
        case cAtPrbsModePrbs20StdO151 : return cAtTrue;
        case cAtPrbsModePrbs20rStdO153: return cAtTrue;
        case cSupportedFixPatternMode : return cAtTrue;
        default: return cAtFalse;
        }
    }

eAtModulePrbsRet Tha60290022PrbsEngineTxModeSet(AtPrbsEngine engine, eAtPrbsMode prbsMode)
    {
    if (!AtPrbsEngineModeIsSupported(engine, prbsMode))
        return cAtErrorModeNotSupport;

    if (AtPrbsEngineTxModeGet(engine) == prbsMode)
        return cAtOk;

    return TxModeSet(engine, prbsMode, AtPrbsEngineTxFixedPatternGet(engine));
    }

eAtPrbsMode Tha60290022PrbsEngineTxModeGet(AtPrbsEngine engine)
    {
    uint32 modeRegMask = cAf6_ctrl_pen_gen_patt_mode_Mask;
    uint32 modeRegShift = AtRegMaskToShift(modeRegMask);
    uint32 regAddr = EngineAddressWithLocalAddress(engine, cAf6Reg_ctrl_pen_gen_Base);
    uint32 regVal = AtPrbsEngineRead(engine, regAddr, cAtModulePrbs);
    return TxPrbsModeHw2Sw(mRegField(regVal, modeReg));
    }

eAtModulePrbsRet Tha60290022PrbsEngineRxModeSet(AtPrbsEngine engine, eAtPrbsMode prbsMode)
    {
    if (!AtPrbsEngineModeIsSupported(engine, prbsMode))
        return cAtErrorModeNotSupport;

    if (AtPrbsEngineRxModeGet(engine) == prbsMode)
        return cAtOk;

    return MonModeSet(engine, prbsMode, AtPrbsEngineRxFixedPatternGet(engine));
    }

eAtPrbsMode Tha60290022PrbsEngineRxModeGet(AtPrbsEngine engine)
    {
    uint32 modeRegMask = cAf6_ctrl_pen_montdm_patt_mode_Mask;
    uint32 modeRegShift= AtRegMaskToShift(modeRegMask);
    uint32 regAddrWithOffset = EngineAddressWithLocalAddress(engine, cAf6Reg_ctrl_pen_montdm_Base);
    uint32 regVal = AtPrbsEngineRead(engine, regAddrWithOffset, cAtModulePrbs);
    uint32 hwValue = mRegField(regVal, modeReg);
    return RxPrbsModeHw2Sw(hwValue);
    }

eAtModulePrbsRet Tha60290022PrbsEngineTxInvert(AtPrbsEngine engine, eBool invert)
    {
    uint32 invertRegMask = cAf6_ctrl_pen_gen_invmode_Mask;
    uint32 invertRegShift = AtRegMaskToShift(invertRegMask);
    uint32 regAddrWithOffset = EngineAddressWithLocalAddress(engine, cAf6Reg_ctrl_pen_gen_Base);
    uint32 regVal = AtPrbsEngineRead(engine, regAddrWithOffset, cAtModulePrbs);
    mFieldIns(&regVal, invertRegMask, invertRegShift, ThaPrbsEngineTxInvertModeSw2Hw(mThaPrbsEngine(engine), invert));
    AtPrbsEngineWrite(engine, regAddrWithOffset, regVal, cAtModulePrbs);
    return cAtOk;
    }

eBool Tha60290022PrbsEngineTxIsInverted(AtPrbsEngine engine)
    {
    uint32 invertRegMask = cAf6_ctrl_pen_gen_invmode_Mask;
    uint32 regAddrWithOffset = EngineAddressWithLocalAddress(engine, cAf6Reg_ctrl_pen_gen_Base);
    uint32 regVal = AtPrbsEngineRead(engine, regAddrWithOffset, cAtModulePrbs);
    return ThaPrbsEngineTxInvertModeHw2Sw(mThaPrbsEngine(engine), regVal & invertRegMask);
    }

eAtModulePrbsRet Tha60290022PrbsEngineRxInvert(AtPrbsEngine engine, eBool invert)
    {
    uint32 regAddr = EngineAddressWithLocalAddress(engine, cAf6Reg_ctrl_pen_montdm_Base);
    uint32 regVal = AtPrbsEngineRead(engine, regAddr, cAtModulePrbs);
    uint32 fieldMask = cAf6_ctrl_pen_montdm_invmode_Mask;
    uint32 fieldShift = AtRegMaskToShift(fieldMask);
    uint32 hwMode = ThaPrbsEngineRxInvertModeSw2Hw(mThaPrbsEngine(engine), invert);
    mFieldIns(&regVal, fieldMask, fieldShift, hwMode);
    AtPrbsEngineWrite(engine, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

eBool Tha60290022PrbsEngineRxIsInverted(AtPrbsEngine engine)
    {
    uint32 regAddr = EngineAddressWithLocalAddress(engine, cAf6Reg_ctrl_pen_montdm_Base);
    uint32 regVal = AtPrbsEngineRead(engine, regAddr, cAtModulePrbs);
    return ThaPrbsEngineRxInvertModeHw2Sw(mThaPrbsEngine(engine), regVal & cAf6_ctrl_pen_montdm_invmode_Mask);
    }

eAtModulePrbsRet Tha60290022PrbsEngineTxBitOrderSet(AtPrbsEngine engine, eAtPrbsBitOrder order)
    {
    uint32 mask = cAf6_ctrl_pen_gen_swapmode_Mask;
    uint32 localAddress = EngineAddressWithLocalAddress(engine, cAf6Reg_ctrl_pen_gen_Base);
    return BitOrderSet(engine, order, localAddress, mask, AtRegMaskToShift(mask));
    }

eAtPrbsBitOrder Tha60290022PrbsEngineTxBitOrderGet(AtPrbsEngine self)
    {
    uint32 localAddress = EngineAddressWithLocalAddress(self, cAf6Reg_ctrl_pen_gen_Base);
    return BitOrderGet(self, localAddress, cAf6_ctrl_pen_gen_swapmode_Mask);
    }

eAtModulePrbsRet Tha60290022PrbsEngineRxBitOrderSet(AtPrbsEngine engine, eAtPrbsBitOrder order)
    {
    uint32 mask = cAf6_ctrl_pen_montdm_swapmode_Mask;
    uint32 regAddrWithOffset = EngineAddressWithLocalAddress(engine, cAf6Reg_ctrl_pen_montdm_Base);
    return BitOrderSet(engine, order, regAddrWithOffset, mask, AtRegMaskToShift(mask));
    }

eAtPrbsBitOrder Tha60290022PrbsEngineRxBitOrderGet(AtPrbsEngine engine)
    {
    uint32 regAddr = EngineAddressWithLocalAddress(engine, cAf6Reg_ctrl_pen_montdm_Base);
    return BitOrderGet(engine, regAddr, cAf6_ctrl_pen_montdm_swapmode_Mask);
    }

eAtModulePrbsRet Tha60290022PrbsEngineTxFixedPatternSet(AtPrbsEngine engine, uint32 fixedPattern)
    {
    if (AtPrbsEngineTxModeGet(engine) != cSupportedFixPatternMode)
        return cAtErrorInvalidOperation;

    AtPrbsEngineWrite(engine, EngineAddressWithLocalAddress(engine, cAf6Reg_txfix_gen_Base), fixedPattern, cAtModulePrbs);
    return TxModeSet(engine, cSupportedFixPatternMode, fixedPattern);
    }

uint32 Tha60290022PrbsEngineTxFixedPatternGet(AtPrbsEngine self)
    {
    uint32 regAddr = EngineAddressWithLocalAddress(self, cAf6Reg_txfix_gen_Base);
    return AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    }

eAtModulePrbsRet Tha60290022PrbsEngineRxFixedPatternSet(AtPrbsEngine engine, uint32 fixedPattern)
    {
    if (AtPrbsEngineRxModeGet(engine) != cSupportedFixPatternMode)
        return cAtErrorInvalidOperation;

    AtPrbsEngineWrite(engine, EngineAddressWithLocalAddress(engine, cAf6Reg_rxfix_gen_Base), fixedPattern, cAtModulePrbs);
    return MonModeSet(engine, cSupportedFixPatternMode, fixedPattern);;
    }

uint32 Tha60290022PrbsEngineRxFixedPatternGet(AtPrbsEngine engine)
    {
    uint32 regAddrWithOffset = EngineAddressWithLocalAddress(engine, cAf6Reg_rxfix_gen_Base);
    return AtPrbsEngineRead(engine, regAddrWithOffset, cAtModulePrbs);
    }

void Tha60290022PrbsEngineTimeslotMonDefaultSet(ThaPrbsEngine self, uint32 bitMap)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    AtPrbsEngineWrite(engine, EngineAddressWithLocalAddress(engine, cAf6Reg_sel_mon_tstdm_Base), bitMap, cAtModulePrbs);
    }

void Tha60290022PrbsEngineTimeslotDefaultSet(ThaPrbsEngine self, uint32 bitMap)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    AtPrbsEngineWrite(engine, EngineAddressWithLocalAddress(engine, cAf6Reg_sel_ts_gen_Base), bitMap, cAtModulePrbs);
    Tha60290022PrbsEngineTimeslotMonDefaultSet(self, bitMap);
    }

uint32 Tha60290022PrbsEngineAlarmHistoryRead2Clear(AtPrbsEngine engine, eBool r2c)
    {
    uint32 regAddr = EngineAddressWithLocalAddress(engine, cAf6Reg_lossyn_pen_tdm_mon_Base);
    uint32 regVal = AtPrbsEngineRead(engine, regAddr, cAtModulePrbs);
    uint32 mask = cBit0;
    uint32 alarms = 0;

    if (regVal & mask)
        alarms |= cAtPrbsEngineAlarmTypeLossSync;

    if (r2c)
        AtPrbsEngineWrite(engine, regAddr, mask, cAtModulePrbs);

    return alarms;
    }

eAtModulePrbsRet Tha60290022PrbsEngineTxErrorInject(AtPrbsEngine self, uint32 numErrorForced)
    {
    uint32 regAddr, error_i;
    eBool needDelay = (numErrorForced > 1) ? cAtTrue : cAtFalse;
    static const uint32 cDelayUs = 150; /* The requirement is > 125us, give it 150us */

    if (AtDeviceIsSimulated(AtPrbsEngineDeviceGet(self)))
        needDelay = cAtFalse;

    regAddr = cAf6Reg_singe_ber_pen_Base + HwOffsetOfErrorSingleForceAddress(self);
    for (error_i = 0; error_i < numErrorForced; error_i++)
        {
        AtPrbsEngineWrite(self, regAddr, 1, cAtModulePrbs);
        if (needDelay)
            AtOsalUSleep(cDelayUs);
        }

    return cAtOk;
    }

void Tha60290022PrbsAllEngineHwFlush(ThaModulePrbs module)
    {
    uint8 engineId;
    ThaPrbsRegProvider provider = ThaModulePrbsRegProvider(module);
    uint32 baseAddress = ThaPrbsRegProviderBaseAddress(provider);
    uint32 numEngines = AtModulePrbsMaxNumEngines((AtModulePrbs)module);

    for (engineId = 0; engineId < numEngines; engineId++)
        {
        uint32 regAddress;
        uint32 offset = baseAddress + (uint32)(engineId * cEngineOffset);

        regAddress = cAf6Reg_sel_ts_gen_Base + offset;
        mModuleHwWrite(module, regAddress, 0);

        regAddress = cAf6Reg_sel_mon_tstdm_Base + offset;
        mModuleHwWrite(module, regAddress, 0);

        regAddress = cAf6Reg_ctrl_pen_gen_Base + offset;
        mModuleHwWrite(module, regAddress, 0);

        regAddress = cAf6Reg_ctrl_pen_montdm_Base + offset;
        mModuleHwWrite(module, regAddress, 0);

        regAddress = cAf6Reg_ctrl_ber_pen_Base + offset;
        mModuleHwWrite(module, regAddress, 0);

        regAddress = cAf6Reg_lossyn_pen_tdm_mon_Base + offset;
        mModuleHwWrite(module, regAddress, 0);

        offset = baseAddress + engineId;
        regAddress = cAf6Reg_sel_ho_bert_gen_Base + offset;
        mModuleHwWrite(module, regAddress, 0);
        }
    }

eAtModulePrbsRet Tha60290022PrbsEngineMonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    ThaPrbsRegProvider provider = ThaModulePrbsRegProvider(module);
    uint32 offset = ThaPrbsEngineHwEngineId((ThaPrbsEngine)self) + ThaPrbsRegProviderBaseAddress(provider);
    uint32 regAddress = cAf6Reg_sel_bert_montdm_Base + offset;
    uint32 regValue;

    regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6_sel_bert_montdm_mon_side_, (side == cAtPrbsSidePsn) ? 1 : 0);
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);

    return cAtOk;
    }

eBool Tha60290022PrbsEngineCanUseAnyFixPattern(AtPrbsEngine engine)
    {
    /* Just can use all-zero or all one */
    AtUnused(engine);
    return cAtFalse;
    }

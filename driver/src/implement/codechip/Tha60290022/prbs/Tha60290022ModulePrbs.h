/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290022ModulePrbs.h
 * 
 * Created Date: Apr 25, 2017
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPRBS_H_
#define _THA60290022MODULEPRBS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/prbs/Tha60290021ModulePrbs.h"
#include "../../../default/prbs/ThaModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60290021PrbsGatingFactory Tha60290022PrbsGatingFactoryNew(AtModulePrbs module);

AtPrbsEngine Tha60290022FacePlateSerdesPrbsEngineNew(AtSerdesController serdesController);

eBool Tha60290022ModulePrbsBertOptimizeIsSupported(ThaModulePrbs self);
void Tha60290022ModulePrbsHwFlush(ThaModulePrbs self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPRBS_H_ */


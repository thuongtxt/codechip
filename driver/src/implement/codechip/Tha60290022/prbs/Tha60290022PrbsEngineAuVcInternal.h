/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290022PrbsEngineAuVcInternal.h
 * 
 * Created Date: Jan 29, 2018
 *
 * Description : 60290022 AUVC Prbs engine internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PRBSENGINEAUVCINTERNAL_H_
#define _THA60290022PRBSENGINEAUVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/prbs/ThaPrbsEngineAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022PrbsEngineAuVc
    {
    tThaPrbsEngineAuVc super;
    }tTha60290022PrbsEngineAuVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60290022PrbsEngineAuVcObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PRBSENGINEAUVCINTERNAL_H_ */


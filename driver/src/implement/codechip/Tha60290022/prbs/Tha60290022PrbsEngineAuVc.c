/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290022PrbsEngineAuVc.c
 *
 * Created Date: May 2, 2017
 *
 * Description : AU VC PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhPathInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/map/ThaModuleDemap.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/pdh/ThaStmModulePdh.h"
#include "../../../default/cdr/ThaModuleCdrStm.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../Tha60290021/sdh/Tha6029SdhTerminatedLineAuVc.h"
#include "../../Tha60210011/map/Tha60210011ModuleMap.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../map/Tha60290022ModuleMapDemapReg.h"
#include "../map/Tha60290022MapDemap.h"
#include "Tha60290022PrbsHwController.h"
#include "Tha60290022ModulePrbs.h"
#include "Tha60290022PrbsEngineAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290022PrbsEngineAuVc *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/* Override */
static tThaPrbsEngineAuVcMethods m_ThaPrbsEngineAuVcOverride;

/* Save super implementation */
static const tThaPrbsEngineAuVcMethods *m_ThaPrbsEngineAuVcMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60290022PrbsEngineOverride.h"

static AtSdhChannel ChannelForPohInsertion(ThaPrbsEngineAuVc self, AtSdhChannel channel)
    {
    if (!Tha60290021ModuleSdhIsTerminatedVc(channel))
        return m_ThaPrbsEngineAuVcMethods->ChannelForPohInsertion(self, channel);

    return (AtSdhChannel)AtChannelSourceGet((AtChannel)channel);
    }

static eBool IsHwMasterSts(uint8 hwSts, uint8 startHwMasterSts, uint8 numHwMasterSts)
    {
    if ((hwSts >= startHwMasterSts) && (hwSts < (startHwMasterSts + numHwMasterSts)))
        return cAtTrue;

    return cAtFalse;
    }

static void HoBertHwDefault(ThaModuleMap self, AtSdhChannel channel)
    {
    uint8 sts_i = 0, sts1Id = 0, hwSlice = 0, hwSts = 0;
    uint8 numSts = AtSdhChannelNumSts(channel);
    uint8 startSts = AtSdhChannelSts1Get(channel);
    ThaModuleAbstractMap abstractMap = (ThaModuleAbstractMap)self;
    uint8 srcType = (AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeVc3) ? 0 : 1;
    ThaModuleDemap demapModule = (ThaModuleDemap)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleDemap);
    uint32 demapRegAddress = ThaModuleDemapVcDmapChnCtrl(demapModule, (AtSdhVc)channel);
    uint8 numMasterSts1 = (uint8)((numSts >= 3)  ? (numSts / 3) : 1);
    uint8 startHwMasterSts;
    uint32 regAddr, regVal;
    if (ThaSdhChannelHwStsGet(channel, cThaModuleDemap, startSts, &hwSlice, &startHwMasterSts) != cAtOk)
        mChannelLog(channel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        sts1Id = (uint8)(startSts + sts_i);

        if (ThaSdhChannelHwStsGet(channel, cThaModuleDemap, sts1Id, &hwSlice, &hwSts) != cAtOk)
            mChannelLog(channel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

        regAddr = demapRegAddress + ThaModuleAbstractMapHoSliceOffset(abstractMap, hwSlice) + hwSts;
        regVal  = mChannelHwRead(channel, regAddr, cThaModuleMap);

        mRegFieldSet(regVal, cAf6_demapho_channel_ctrl_Demapsr_vc3n3c_, IsHwMasterSts(hwSts, startHwMasterSts, numMasterSts1) ? 1 : 0);
        mRegFieldSet(regVal, cAf6_demapho_channel_ctrl_Demapsrctype_, srcType);

        mRegFieldSet(regVal, cAf6_demapho_channel_ctrl_DemapCh_BusSelect_, 1);

        mChannelHwWrite(channel, regAddr, regVal, cThaModuleMap);
        }

    }

static eBool IsPwEnabled(AtSdhChannel channel)
    {
    AtPw pw = AtChannelBoundPwGet((AtChannel)channel);

    if (pw == NULL)
        return cAtFalse;

    return AtChannelIsEnabled((AtChannel)pw);
    }

static void HoHwCleanUp(ThaModuleMap self, AtSdhChannel channel)
    {
    uint8 sts_i;
    uint8 numSts = AtSdhChannelNumSts(channel);
    uint8 startSts = AtSdhChannelSts1Get(channel);
    ThaModuleAbstractMap abstractMap = (ThaModuleAbstractMap)self;
    uint32 demapRegAddress = cAf6Reg_demapho_channel_ctrl_Base;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 sts1Id = (uint8)(startSts + sts_i);
        uint8 hwSlice = 0, hwSts = 0;
        uint32 regAddr, regVal;

        if (ThaSdhChannelHwStsGet(channel, cThaModuleDemap, sts1Id, &hwSlice, &hwSts) != cAtOk)
            mChannelLog(channel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

        regAddr = demapRegAddress + ThaModuleAbstractMapHoSliceOffset(abstractMap, hwSlice) + hwSts;
        regVal  = mChannelHwRead(channel, regAddr, cThaModuleMap);

        mRegFieldSet(regVal, cAf6_demapho_channel_ctrl_DemapCh_BusSelect_, IsPwEnabled(channel));
        mChannelHwWrite(channel, regAddr, regVal, cThaModuleMap);
        }
    }

static eAtRet HwCleanup(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);
    eAtRet ret = cAtOk;

    if (AtSdhChannelIsHoVc((AtSdhChannel)channel))
        HoHwCleanUp((ThaModuleMap)AtDeviceModuleGet(device, cThaModuleMap), (AtSdhChannel)channel);

    ret |= m_AtPrbsEngineMethods->HwCleanup(self);

    return ret;
    }

static void HwDefault(ThaPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);

    m_ThaPrbsEngineMethods->HwDefault(self);
    if (AtSdhChannelIsHoVc((AtSdhChannel)channel))
        HoBertHwDefault((ThaModuleMap)AtDeviceModuleGet(device, cThaModuleMap), (AtSdhChannel)channel);
    }

static eAtRet Unchannelize(ThaPrbsEngine self)
    {
    AtSdhChannel vc;
    AtChannel sourceVc;
    ThaModulePdh pdhModule;
    ThaModuleAbstractMap mapModule;
    ThaModuleCdr cdrModule;
    AtDevice device = AtPrbsEngineDeviceGet((AtPrbsEngine)self);
    eAtRet ret = m_ThaPrbsEngineMethods->Unchannelize(self);
    eBool isHoBus;
    if (ret != cAtOk)
        return ret;

    vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);

    sourceVc = AtChannelSourceGet((AtChannel)vc);
    if (sourceVc)
        ret |= Tha60210011ModuleOcnLomCheckingEnable((AtSdhChannel)sourceVc, cAtFalse);

    pdhModule = (ThaModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    ret |= ThaModulePdhHoVcUnChannelize(pdhModule, vc);

    isHoBus = Tha60290021ModuleSdhVcIsFromLoBus((AtModuleSdh)AtChannelModuleGet((AtChannel)vc), vc) ? cAtFalse : cAtTrue;
    ret |= Tha6029SdhTerminatedLineAuVcHoSelect(vc, isHoBus);

    mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    ret |= ThaModuleAbstractMapAuVcUnchannelize(mapModule, (AtSdhVc)vc, cAtTrue);

    cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    ret |= ThaModuleCdrHoVcUnChannelize(cdrModule, vc);

    return ret;
    }

static eAtRet Channelize(ThaPrbsEngine self)
    {
    AtSdhChannel vc;
    ThaModuleAbstractMap mapModule;
    ThaModulePdh pdhModule;
    ThaModuleCdr cdrModule;
    AtDevice device = AtPrbsEngineDeviceGet((AtPrbsEngine)self);
    eAtRet ret = m_ThaPrbsEngineMethods->Channelize(self);
    if (ret != cAtOk)
        return ret;

    vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    ret |= Tha6029SdhTerminatedLineAuVcMapTypeUpdate(vc, AtSdhChannelMapTypeGet(vc));
    ret |= Tha6029SdhTerminatedLineAuVcHoSelect(vc, Tha6029SdhTerminatedLineAuVcFromLoBus(vc) ? cAtFalse : cAtTrue);

    mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    ret |= ThaModuleAbstractMapAuVcChannelizeRestore(mapModule, (AtSdhVc)vc);

    pdhModule = (ThaModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    ret |= ThaModulePdhHoVcChannelizeRestore(pdhModule, vc);

    cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    ret |= ThaModuleCdrHoVcChannelizeRestore(cdrModule, vc);

    return ret;
    }

static void HoVcPrbsPohInsertHandle(ThaPrbsEngineAuVc self, eBool prbsEnable)
    {
    AtSdhChannel channel = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);

    if (prbsEnable)
        AtSdhPathPohInsertionEnable((AtSdhPath)channel, cAtTrue);
    else
        {
        /* If channel bound to PW, POH insert must be disabled */
        eBool pohShouldBeInserted = (AtChannelBoundPwGet((AtChannel)channel)) ? cAtFalse : cAtTrue;
        AtSdhPathPohInsertionEnable((AtSdhPath)channel, pohShouldBeInserted);
        }
    }

static void PohInsertHandle(ThaPrbsEngineAuVc self, eBool prbsEnable)
    {
    AtSdhChannel channel = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);

    if (AtSdhChannelIsHoVc((AtSdhChannel)channel))
        HoVcPrbsPohInsertHandle(self, prbsEnable);
    else
        m_ThaPrbsEngineAuVcMethods->PohInsertHandle(self, prbsEnable);
    }

static void OverrideAtPrbsEngineSecondTime(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        mMethodOverride(m_AtPrbsEngineOverride, HwCleanup);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideThaPrbsEngine(AtPrbsEngine self)
    {
    ThaPrbsEngine engine = (ThaPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(engine), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, TimeslotDefaultSet);
        mMethodOverride(m_ThaPrbsEngineOverride, HwDefault);
        mMethodOverride(m_ThaPrbsEngineOverride, RxChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, Unchannelize);
        mMethodOverride(m_ThaPrbsEngineOverride, Channelize);
        mMethodOverride(m_ThaPrbsEngineOverride, HwRxPsnEnable);
        }

    mMethodsSet(engine, &m_ThaPrbsEngineOverride);
    }

static void OverrideThaPrbsEngineAuVc(AtPrbsEngine self)
    {
    ThaPrbsEngineAuVc engine = (ThaPrbsEngineAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineAuVcMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineAuVcOverride, m_ThaPrbsEngineAuVcMethods, sizeof(m_ThaPrbsEngineAuVcOverride));

        mMethodOverride(m_ThaPrbsEngineAuVcOverride, ChannelForPohInsertion);
        mMethodOverride(m_ThaPrbsEngineAuVcOverride, PohInsertHandle);
        }

    mMethodsSet(engine, &m_ThaPrbsEngineAuVcOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideAtPrbsEngineSecondTime(self);
    OverrideThaPrbsEngine(self);
    OverrideThaPrbsEngineAuVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PrbsEngineAuVc);
    }

AtPrbsEngine Tha60290022PrbsEngineAuVcObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineAuVcObjectInit(self, vc, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290022PrbsEngineAuVcNew(AtSdhChannel vc, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60290022PrbsEngineAuVcObjectInit(newEngine, vc, engineId);
    }

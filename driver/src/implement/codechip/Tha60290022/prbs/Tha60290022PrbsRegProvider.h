/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290022PrbsRegProvider.h
 * 
 * Created Date: May 2, 2017
 *
 * Description : Register provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PRBSREGPROVIDER_H_
#define _THA60290022PRBSREGPROVIDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/prbs/ThaPrbsRegProvider.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cEngineOffset 512
#define cErrorInsertEngineOffset 2048
#define cEngineCounterOffset 1

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60290022PrbsRegProviderEngineSliceAddressWithLocalAddress(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice, uint32 localAddress);
uint32 Tha60290022PrbsRegProviderEngineAddressWithLocalAddressAndEngineOffset(ThaPrbsRegProvider self, uint32 engineId, uint32 localAddress);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PRBSREGPROVIDER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290022PrbsEngineDe3.c
 *
 * Created Date: May 2, 2017
 *
 * Description : DE3 PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/prbs/ThaPrbsEngineDe3Internal.h"
#include "Tha60290022PrbsHwController.h"
#include "Tha60290022ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290022PrbsEngineDe3 *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PrbsEngineDe3
    {
    tThaPrbsEngineDe3 super;
    }tTha60290022PrbsEngineDe3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60290022PrbsEngineOverride.h"

static eAtRet TdmSideRxEnable(ThaPrbsEngine self, eBool enable)
    {
    eAtRet ret;

    ret = m_ThaPrbsEngineMethods->TdmSideRxEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    ret |= ThaPrbsEnginePdhChannelMonOnlyEnable((ThaPrbsEnginePdhChannel)self, enable);

    return ret;
    }

static void OverrideThaPrbsEngine(AtPrbsEngine self)
    {
    ThaPrbsEngine engine = (ThaPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(engine), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, TimeslotDefaultSet);
        mMethodOverride(m_ThaPrbsEngineOverride, RxChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, TdmSideRxEnable);
        mMethodOverride(m_ThaPrbsEngineOverride, HwRxPsnEnable);
        }

    mMethodsSet(engine, &m_ThaPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideThaPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PrbsEngineDe3);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtPdhDe3 de3, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineDe3ObjectInit(self, de3, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290022PrbsEngineDe3New(AtPdhDe3 de3, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, de3, engineId);
    }

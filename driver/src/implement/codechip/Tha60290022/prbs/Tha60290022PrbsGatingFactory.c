/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290022PrbsGatingFactory.c
 *
 * Created Date: Apr 25, 2017
 *
 * Description : PRBS gating factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/prbs/gating/Tha60290021PrbsGatingFactoryInternal.h"
#include "../../Tha60290021/prbs/gating/Tha60290021PrbsGatingTimer.h"
#include "Tha60290022ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PrbsGatingFactory
    {
    tTha60290021PrbsGatingFactory super;
    }tTha60290022PrbsGatingFactory;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021PrbsGatingFactoryMethods m_Tha60290021PrbsGatingFactoryOverride;

/* Save super implementation */
static const tTha60290021PrbsGatingFactoryMethods *m_Tha60290021PrbsGatingFactoryMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtTimer BackplaneSerdesTimerCreate(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine)
    {
    AtTimer timer = m_Tha60290021PrbsGatingFactoryMethods->BackplaneSerdesTimerCreate(self, engine);
    Tha60290021PrbsGatingTimerBackplaneSerdesElapseTimeLogicEnable(timer, cAtTrue);
    return timer;
    }

static eBool MateGatingSupported(Tha60290021PrbsGatingFactory self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideTha60290021PrbsGatingFactory(Tha60290021PrbsGatingFactory self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021PrbsGatingFactoryMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021PrbsGatingFactoryOverride, mMethodsGet(self), sizeof(m_Tha60290021PrbsGatingFactoryOverride));

        mMethodOverride(m_Tha60290021PrbsGatingFactoryOverride, BackplaneSerdesTimerCreate);
        mMethodOverride(m_Tha60290021PrbsGatingFactoryOverride, MateGatingSupported);
        }

    mMethodsSet(self, &m_Tha60290021PrbsGatingFactoryOverride);
    }

static void Override(Tha60290021PrbsGatingFactory self)
    {
    OverrideTha60290021PrbsGatingFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PrbsGatingFactory);
    }

static Tha60290021PrbsGatingFactory ObjectInit(Tha60290021PrbsGatingFactory self, AtModulePrbs module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021PrbsGatingFactoryObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha60290021PrbsGatingFactory Tha60290022PrbsGatingFactoryNew(AtModulePrbs module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60290021PrbsGatingFactory newFactory = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newFactory, module);
    }

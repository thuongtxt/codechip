/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290022FacePlateSerdesPrbsEngineInternal.h
 * 
 * Created Date: Sep 7, 2018
 *
 * Description : Internal data of the faceplate serdes prbs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022FACEPLATESERDESPRBSENGINEINTERNAL_H_
#define _THA60290022FACEPLATESERDESPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/prbs/Tha60290021FacePlateSerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022FacePlateSerdesPrbsEngine
    {
    tTha60290021FacePlateSerdesPrbsEngine super;
    }tTha60290022FacePlateSerdesPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60290022FacePlateSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022FACEPLATESERDESPRBSENGINEINTERNAL_H_ */


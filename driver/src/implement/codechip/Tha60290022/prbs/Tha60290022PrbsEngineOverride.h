/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290022PrbsEngineOverride.h
 *
 * Created Date: May 2, 2017
 *
 * Description : Common overriding
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods  m_AtPrbsEngineOverride;
static tThaPrbsEngineMethods m_ThaPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods  *m_AtPrbsEngineMethods  = NULL;
static const tThaPrbsEngineMethods *m_ThaPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return Tha60290022PrbsEngineModeIsSupported(self, prbsMode);
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return Tha60290022PrbsEngineTxModeSet(self, prbsMode);
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    return Tha60290022PrbsEngineTxModeGet(self);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return Tha60290022PrbsEngineRxModeSet(self, prbsMode);
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    return Tha60290022PrbsEngineRxModeGet(self);
    }

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
    {
    return Tha60290022PrbsEngineTxInvert(self, invert);
    }

static eBool TxIsInverted(AtPrbsEngine self)
    {
    return Tha60290022PrbsEngineTxIsInverted(self);
    }

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
    {
    return Tha60290022PrbsEngineRxInvert(self, invert);
    }

static eBool RxIsInverted(AtPrbsEngine self)
    {
    return Tha60290022PrbsEngineRxIsInverted(self);
    }

static eAtModulePrbsRet TxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    return Tha60290022PrbsEngineTxBitOrderSet(self, order);
    }

static eAtPrbsBitOrder TxBitOrderGet(AtPrbsEngine self)
    {
    return Tha60290022PrbsEngineTxBitOrderGet(self);
    }

static eAtModulePrbsRet RxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    return Tha60290022PrbsEngineRxBitOrderSet(self, order);
    }

static eAtPrbsBitOrder RxBitOrderGet(AtPrbsEngine self)
    {
    return Tha60290022PrbsEngineRxBitOrderGet(self);
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    return Tha60290022PrbsEngineTxFixedPatternSet(self, fixedPattern);
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    return Tha60290022PrbsEngineTxFixedPatternGet(self);
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    return Tha60290022PrbsEngineRxFixedPatternSet(self, fixedPattern);
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    return Tha60290022PrbsEngineRxFixedPatternGet(self);
    }

static void TimeslotDefaultSet(ThaPrbsEngine self, uint32 bitMap)
    {
    Tha60290022PrbsEngineTimeslotDefaultSet(self, bitMap);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return Tha60290022PrbsEngineAlarmHistoryRead2Clear(self, cAtTrue);
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return Tha60290022PrbsEngineAlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    return ThaPrbsEngineTdmSideAlarmGet(self);
    }

static eAtModulePrbsRet TxErrorInject(AtPrbsEngine self, uint32 numErrorForced)
    {
    return Tha60290022PrbsEngineTxErrorInject(self, numErrorForced);
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    uint32 ret = m_AtPrbsEngineMethods->MonitoringSideSet(self, side);
    ThaPrbsEngine engine = (ThaPrbsEngine)self;

    if (ret != cAtOk)
        return ret;

    Tha60290022PrbsEngineTimeslotMonDefaultSet(engine, ThaPrbsEngineTimeslotBitMap(engine));
    return Tha60290022PrbsEngineMonitoringSideSet(self, side);
    }

static eBool HasPwAssociation(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    AtPw pw = AtChannelBoundPwGet(channel);
    return pw ? cAtTrue : cAtFalse;
    }

static eAtRet HwRxPsnEnable(ThaPrbsEngine self, eBool enable)
    {
    return ThaPrbsEngineRxTdmSideEnable((ThaPrbsEngine)self, enable);
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    ThaPrbsEngine engine = (ThaPrbsEngine)self;

    /* If there is no associated PW, just nothing (cache only) */
    engine->psnRxEnabled = enable;
    if ((AtPrbsEngineMonitoringSideGet(self) == cAtPrbsSidePsn) && (!HasPwAssociation(self)))
        return cAtOk;

    /* Now, there is only one control for RX enabling and registers of TDM side
     * are used for this purpose */
    return ThaPrbsEngineRxTdmSideEnable((ThaPrbsEngine)self, enable);
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    if ((AtPrbsEngineMonitoringSideGet(self) == cAtPrbsSidePsn) && (!HasPwAssociation(self)))
        return ((ThaPrbsEngine)self)->psnRxEnabled;

    /* Now, there is only one control for RX enabling and registers of TDM side
     * are used for this purpose */
    return ThaPrbsEngineRxTdmSideIsEnabled(self);
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);

    if (ret != cAtOk)
        return ret;

    Tha60290022PrbsEngineMonitoringSideSet(self, cAtPrbsSideTdm);
    return cAtOk;
    }

static uint32 PwIdWithSliceOffsetGet(AtPrbsEngine self, AtPw pw)
    {
    uint8 slice = ThaPrbsEngineHwSliceId((ThaPrbsEngine)self);
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    return ((ThaModulePrbsTdmChannelIdSliceFactor(module) * slice) +
             AtChannelHwIdGet((AtChannel)pw));
    }

static uint32 RxChannelId(ThaPrbsEngine self)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    AtPw pw = AtChannelBoundPwGet(AtPrbsEngineChannelGet(engine));
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(engine);

    if (side == cAtPrbsSideTdm)
        return m_ThaPrbsEngineMethods->RxChannelId(self);

    /* psn side */
    if (pw == NULL)
        return cInvalidUint32;

    return PwIdWithSliceOffsetGet(engine, pw);
    }

static eAtModulePrbsRet AllCountersLatchAndClear(AtPrbsEngine self, eBool clear)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    ThaPrbsEngine engine = (ThaPrbsEngine)self;
    uint32 hwEngineId = ThaPrbsEngineHwEngineId(engine);
    uint8 slice = ThaPrbsEngineHwSliceId(engine);
    uint64 counterValue, rxSyncBit, rxLossBit;
    AtPrbsCounters counters = AtPrbsEnginePrbsCountersGet(self);

    AtUnused(clear);

    counterValue = AtPrbsEngineRead(self, ThaRegMapBERTGenGoodBitCounter(module, hwEngineId, slice), cAtModulePrbs);
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterTxBit, counterValue);

    counterValue = AtPrbsEngineRead(self, ThaRegDemapBERTMonErrorCounter(module, hwEngineId, slice), cAtModulePrbs);
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitError, counterValue);

    rxSyncBit = AtPrbsEngineRead(self, ThaRegDemapBERTMonGoodBitCounter(module, hwEngineId, slice), cAtModulePrbs);
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxSync, rxSyncBit);

    rxLossBit = AtPrbsEngineRead(self, ThaRegDemapBERTMonLossBitCounter(module, hwEngineId, slice), cAtModulePrbs);
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitLoss, rxLossBit);

    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBit, rxSyncBit + rxLossBit);

    return cAtOk;
    }

static eBool CanUseAnyFixedPattern(AtPrbsEngine self)
    {
    return Tha60290022PrbsEngineCanUseAnyFixPattern(self);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);

        mMethodOverride(m_AtPrbsEngineOverride, TxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, RxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsInverted);

        mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderGet);

        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, CanUseAnyFixedPattern);

        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);

        mMethodOverride(m_AtPrbsEngineOverride, TxErrorInject);

        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, Init);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

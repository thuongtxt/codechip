/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60290022SurTcaInterruptManager.c
 *
 * Created Date: May 29, 2017
 *
 * Description : PM TCA Interrupt manager of SUR module of product 60290022
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022SurTcaInterruptManagerInternal.h"
#include "Tha60290022ModuleSurReg.h"
#include "Tha60290022SurInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaInterruptManagerMethods      m_ThaInterruptManagerOverride;
static tThaSurInterruptManagerMethods   m_ThaSurInterruptManagerOverride;

/* Save superclass implementation. */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 16;
    }

static void PwInterruptProcess(ThaSurInterruptManager self, AtHal hal)
    {
    Tha60290022SurInterruptManagerPwInterruptProcess(self, hal);
    }

static void OverrideThaInterruptManager(ThaSurInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideThaSurInterruptManager(ThaSurInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSurInterruptManagerOverride, mMethodsGet(self), sizeof(m_ThaSurInterruptManagerOverride));

        mMethodOverride(m_ThaSurInterruptManagerOverride, PwInterruptProcess);
        }

    mMethodsSet(self, &m_ThaSurInterruptManagerOverride);
    }

static void Override(ThaSurInterruptManager self)
    {
    OverrideThaInterruptManager(self);
    OverrideThaSurInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SurTcaInterruptManager);
    }

AtInterruptManager Tha60290022SurTcaInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSurTcaInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaSurInterruptManager)self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60290022SurTcaInterruptManagerNew(AtModule module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newInterruptManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newInterruptManager == NULL)
        return NULL;

    return Tha60290022SurTcaInterruptManagerObjectInit(newInterruptManager, module);
    }

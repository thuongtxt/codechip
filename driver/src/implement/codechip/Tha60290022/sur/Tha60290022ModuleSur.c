/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60290022ModuleSur.c
 *
 * Created Date: May 11, 2017
 *
 * Description : Surveillance module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/sur/hard/ThaSurInterruptManager.h"
#include "../man/Tha60290022Device.h"
#include "../ram/Tha60290022InternalRam.h"
#include "Tha60290022ModuleSurReg.h"
#include "Tha60290022ModuleSurInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods         m_AtModuleOverride;
static tAtModuleSurMethods      m_AtModuleSurOverride;
static tThaModuleHardSurMethods m_ThaModuleHardSurOverride;

/* Save super implementation */
static const tAtModuleMethods         *m_AtModuleMethods         = NULL;
static const tThaModuleHardSurMethods *m_ThaModuleHardSurMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HwIsReady(AtModule self)
    {
    if (Tha60290022DeviceRunOnVu9P(AtModuleDeviceGet(self)))
        return cAtFalse;
    return m_AtModuleMethods->HwIsReady(self);
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    if (managerIndex == 0)
        return Tha60290022SurInterruptManagerNew(self);

    if (managerIndex == 1)
        return Tha60290022SurTcaInterruptManagerNew(self);

    return NULL;
    }

static uint32 PwCurrentFailuresAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_Base;
    }

static uint32 PwTcaInterruptMaskAddr(ThaModuleHardSur self)
    {
    return mMethodsGet(self)->BaseAddress(self) + cAf6Reg_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_Base;
    }

static eBool FailureForwardingStatusIsSupported(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtDevice Device(AtModuleSur self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return device;
    }

static eBool FailureHoldOnTimerConfigurable(AtModuleSur self)
    {
    return Tha60290022DeviceSurFailureHoldOnTimerConfigurable(Device(self));
    }

static AtSurEngine PwEngineObjectCreate(ThaModuleHardSur self, AtChannel pw)
    {
    return Tha60290022SurEnginePwNew((AtModuleSur)self, pw);
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    const char **ramName = AtModuleAllInternalRamsDescription(self, NULL);

    if (AtStrcmp(ramName[localRamId], sTha60210011ModuleSurRamFMPMPWFMInterruptMASKPerTypePerPW) == 0)
        return Tha60290022InternalRamSurPwNew(self, ramId, localRamId);

    return m_AtModuleMethods->InternalRamCreate(self, ramId, localRamId);
    }

static uint32 PWFramerInterruptMASKPerPWBase(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAf6Reg_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_Base;
    }

static uint32 FmPWInterruptMASKPerTypePerPWBase(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAf6Reg_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_Base;
    }

static void HwFlush(ThaModuleHardSur self)
    {
    uint32 baseAddress = ThaModuleHardSurBaseAddress((ThaModuleHardSur)self);
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);

    m_ThaModuleHardSurMethods->HwFlush(self);

    ThaDeviceMemoryFlush(device,
                         baseAddress + cAf6Reg_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_Base,
                         baseAddress + cAf6Reg_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_Base + 0x3FFF,
                         0);

    ThaDeviceMemoryFlush(device,
                         baseAddress + cAf6Reg_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_Base,
                         baseAddress + cAf6Reg_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_Base + 0x3FFF,
                         0);
    }

static void OverrideAtModuleSur(AtModuleSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSurOverride, mMethodsGet(self), sizeof(m_AtModuleSurOverride));

        mMethodOverride(m_AtModuleSurOverride, FailureHoldOnTimerConfigurable);
        }

    mMethodsSet(self, &m_AtModuleSurOverride);
    }

static void OverrideAtModule(AtModuleSur self)
    {
    AtModule surModule = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(surModule);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, HwIsReady);
        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        }

    mMethodsSet(surModule, &m_AtModuleOverride);
    }

static void OverrideThaModuleHardSur(AtModuleSur self)
    {
    ThaModuleHardSur surModule = (ThaModuleHardSur)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleHardSurMethods = mMethodsGet(surModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardSurOverride, m_ThaModuleHardSurMethods, sizeof(m_ThaModuleHardSurOverride));

        mMethodOverride(m_ThaModuleHardSurOverride, PwCurrentFailuresAddr);
        mMethodOverride(m_ThaModuleHardSurOverride, PwTcaInterruptMaskAddr);
        mMethodOverride(m_ThaModuleHardSurOverride, FailureForwardingStatusIsSupported);
        mMethodOverride(m_ThaModuleHardSurOverride, PwEngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, PWFramerInterruptMASKPerPWBase);
        mMethodOverride(m_ThaModuleHardSurOverride, FmPWInterruptMASKPerTypePerPWBase);
        mMethodOverride(m_ThaModuleHardSurOverride, HwFlush);
        }

    mMethodsSet(surModule, &m_ThaModuleHardSurOverride);
    }

static void Override(AtModuleSur self)
    {
    OverrideAtModule(self);
    OverrideAtModuleSur(self);
    OverrideThaModuleHardSur(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleSur);
    }

AtModuleSur Tha60290022ModuleSurObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSur Tha60290022ModuleSurNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (module == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModuleSurObjectInit(module, device);
    }

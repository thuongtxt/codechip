/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : Tha60290022ModuleSurInternal.h
 * 
 * Created Date: Sep 24, 2019
 *
 * Description : 60290022 Module SUR representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULESURINTERNAL_H_
#define _THA60290022MODULESURINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/sur/Tha60290021ModuleSurInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModuleSur
    {
    tTha60290021ModuleSur super;
    }tTha60290022ModuleSur;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSur Tha60290022ModuleSurObjectInit(AtModuleSur self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULESURINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : Tha60290022SurTcaInterruptManagerInternal.h
 * 
 * Created Date: Oct 16, 2017
 *
 * Description : 60290022 Srur TCA internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022SURTCAINTERRUPTMANAGERINTERNAL_H_
#define _THA60290022SURTCAINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/sur/hard/ThaSurInterruptManagerInternal.h"
#include "Tha60290022SurInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022SurTcaInterruptManager
    {
    tThaSurTcaInterruptManager super;
    }tTha60290022SurTcaInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager Tha60290022SurTcaInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022SURTCAINTERRUPTMANAGERINTERNAL_H_ */


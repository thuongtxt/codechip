/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60290022SurInterruptManager.c
 *
 * Created Date: May 29, 2017
 *
 * Description : FM Interrupt manager of SUR module of product 60290022
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/sur/hard/ThaSurInterruptManagerInternal.h"
#include "Tha60290022ModuleSurReg.h"
#include "Tha60290022SurInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022SurInterruptManager
    {
    tThaSurInterruptManager super;
    }tTha60290022SurInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaInterruptManagerMethods    m_ThaInterruptManagerOverride;
static tThaSurInterruptManagerMethods m_ThaSurInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 16;
    }

static AtModulePw PwModule(ThaSurInterruptManager self)
    {
    AtDevice device = AtModuleDeviceGet(AtInterruptManagerModuleGet((AtInterruptManager)self));
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static void PwInterruptProcess(ThaSurInterruptManager self, AtHal hal)
    {
    const uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self)) + mMethodsGet(self)->Offset(self);
    uint32 intrOR = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_PW_Level1_Group_Interrupt_OR);
    AtModulePw pwModule = PwModule(self);
    const uint32 numSlices = 11;
    uint32 slice;

    for (slice = 0; slice < numSlices; slice++)
        {
        if (intrOR & cIteratorMask(slice))
            {
            uint32 intrStat = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_PW_Level2_Group_Interrupt_OR(slice));
            uint32 grpid;

            for (grpid = 0; grpid < 32; grpid++)
                {
                if (intrStat & cIteratorMask(grpid))
                    {
                    uint32 intrStat32 = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW(slice, grpid));
                    uint32 subid;

                    for (subid = 0; subid < 32; subid++)
                        {
                        if (intrStat32 & cIteratorMask(subid))
                            {
                            uint32 pwid = (slice << 10) + (grpid << 5) + subid;

                            /* Get PW channel */
                            AtPw pwChannel = AtModulePwGetPw(pwModule, pwid);

                            /* Process channel interrupt. */
                            ThaSurInterruptManagerEngineNotify(self, AtChannelSurEngineGet((AtChannel)pwChannel), pwid);
                            }
                        }
                    }
                }
            }
        }
    }

static void OverrideThaInterruptManager(ThaSurInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideThaSurInterruptManager(ThaSurInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSurInterruptManagerOverride, mMethodsGet(self), sizeof(m_ThaSurInterruptManagerOverride));

        mMethodOverride(m_ThaSurInterruptManagerOverride, PwInterruptProcess);
        }

    mMethodsSet(self, &m_ThaSurInterruptManagerOverride);
    }

static void Override(ThaSurInterruptManager self)
    {
    OverrideThaInterruptManager(self);
    OverrideThaSurInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022SurInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSurInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaSurInterruptManager)self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60290022SurInterruptManagerNew(AtModule module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newInterruptManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newInterruptManager == NULL)
        return NULL;

    return ObjectInit(newInterruptManager, module);
    }

void Tha60290022SurInterruptManagerPwInterruptProcess(ThaSurInterruptManager self, AtHal hal)
    {
    if (self)
        PwInterruptProcess(self, hal);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : Tha60290022ModuleSurReg.h
 * 
 * Created Date: May 29, 2017
 *
 * Description : This file contain all constant definitions of SUR block.
 * 
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULESURREG_H_
#define _THA60290022MODULESURREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Level1 Group Interrupt OR
Reg Addr   : 0x18004
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Level1_Group_Interrupt_OR                                                   0x18004

/*--------------------------------------
BitField Name: FMPM_FM_PW_LEV1_OR
BitField Type: R_O
BitField Desc: Interrupt PW level1 group OR
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Level1_Group_Interrupt_OR_FMPM_FM_PW_LEV1_OR_Mask                                   cBit0
#define cAf6_FMPM_FM_PW_Level1_Group_Interrupt_OR_FMPM_FM_PW_LEV1_OR_Shift                                       0

/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Level2 Group Interrupt OR
Reg Addr   : 0x180E0-0x180EF
Reg Formula: 0x180E0+$D
    Where  : 
           + $D(0-10) : PW  Level-1 Group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Level2_Group_Interrupt_OR_Base                                              0x180E0
#define cAf6Reg_FMPM_FM_PW_Level2_Group_Interrupt_OR(D)                                          (0x180E0+(D))

/*--------------------------------------
BitField Name: FMPM_FM_PW_LEV2_OR
BitField Type: R_O
BitField Desc: PW Level2 Group Interrupt OR, bit per group, the last Level-1
group has 12 Level-2 group
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Level2_Group_Interrupt_OR_FMPM_FM_PW_LEV2_OR_Mask                                cBit31_0
#define cAf6_FMPM_FM_PW_Level2_Group_Interrupt_OR_FMPM_FM_PW_LEV2_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Framer Interrupt OR AND MASK Per PW
Reg Addr   : 0x1BC00-0x1BDFF
Reg Formula: 0x1BC00+$D*0x20+$E
    Where  : 
           + $D(0-10) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_Base                                    0x1BC00
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW(D, E)                    (0x1BC00+(D)*0x20+(E))

/*--------------------------------------
BitField Name: FMPM_FM_PW_OAMSK
BitField Type: R_O
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_FM_PW_OAMSK_Mask                                cBit31_0
#define cAf6_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_FM_PW_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Framer Interrupt MASK Per PW
Reg Addr   : 0x1BE00-0x1BFFF
Reg Formula: 0x1BE00+$D*0x20+$E
    Where  : 
           + $D(0-10) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_Base                                           0x1BE00
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW(D, E)                         (0x1B(E)00+(D)*0x20+(E))

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK
BitField Type: R/W
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_FM_PW_MSK_Mask                                cBit31_0
#define cAf6_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_FM_PW_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Interrupt MASK Per Type Per PW
Reg Addr   : 0x20000-0x23FFF
Reg Formula: 0x20000+$D*0x400+$E*0x20+$F
    Where  : 
           + $D(0-10) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
           + $F(0-31) : PW-ID
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_Base                                         0x20000
#define cAf6Reg_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW(D, E, F)            (0x20000+(D)*0x400+(E)*0x20+(F))

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_RBIT
BitField Type: R/W
BitField Desc: RBIT
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_RBIT_Mask                                   cBit7
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_RBIT_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_STRAY
BitField Type: R/W
BitField Desc: STRAY
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_STRAY_Mask                                   cBit6
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_STRAY_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_MALF
BitField Type: R/W
BitField Desc: MALF
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_MALF_Mask                                   cBit5
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_MALF_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_BufUder
BitField Type: R/W
BitField Desc: BufUder
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufUder_Mask                                   cBit4
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufUder_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_BufOver
BitField Type: R/W
BitField Desc: BufOver
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufOver_Mask                                   cBit3
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufOver_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_LBIT
BitField Type: R/W
BitField Desc: LBIT
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LBIT_Mask                                   cBit2
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LBIT_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_LOPSYN
BitField Type: R/W
BitField Desc: LOPSYN
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPSYN_Mask                                   cBit1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPSYN_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_LOPSTA
BitField Type: R/W
BitField Desc: LOPSTA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPSTA_Mask                                   cBit0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPSTA_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Interrupt Current Status Per Type Per PW
Reg Addr   : 0x3C000-0x3FFFF
Reg Formula: 0x3C000+$D*0x400+$E*0x20+$F
    Where  : 
           + $D(0-10) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
           + $F(0-31) : PW-ID
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_Base                                 0x3C000
#define cAf6Reg_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW(D, E, F)        (0x3C000+(D)*0x400+(E)*0x20+(F))

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_RBIT
BitField Type: R_O
BitField Desc: RBIT
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Mask                                  cBit23
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Shift                                      23

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_STRAY
BitField Type: R_O
BitField Desc: STRAY
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Mask                                  cBit22
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Shift                                      22

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_MALF
BitField Type: R_O
BitField Desc: MALF
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Mask                                  cBit21
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Shift                                      21

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_BufUder
BitField Type: R_O
BitField Desc: BufUder
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Mask                                  cBit20
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Shift                                      20

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_BufOver
BitField Type: R_O
BitField Desc: BufOver
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Mask                                  cBit19
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Shift                                      19

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LBIT
BitField Type: R_O
BitField Desc: LBIT
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Mask                                  cBit18
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Shift                                      18

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LOPSYN
BitField Type: R_O
BitField Desc: LOPSYN
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSYN_Mask                                  cBit17
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSYN_Shift                                      17

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LOPSTA
BitField Type: R_O
BitField Desc: LOPSTA
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Mask                                  cBit16
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Shift                                      16

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_RBIT
BitField Type: R_O
BitField Desc: RBIT
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_Mask                                   cBit7
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_Shift                                       7

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_STRAY
BitField Type: R_O
BitField Desc: STRAY
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_Mask                                   cBit6
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_MALF
BitField Type: R_O
BitField Desc: MALF
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_Mask                                   cBit5
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_BufUder
BitField Type: R_O
BitField Desc: BufUder
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_Mask                                   cBit4
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_BufOver
BitField Type: R_O
BitField Desc: BufOver
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_Mask                                   cBit3
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_LBIT
BitField Type: R_O
BitField Desc: LBIT
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_Mask                                   cBit2
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_LOPSYN
BitField Type: R_O
BitField Desc: LOPSYN
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSYN_Mask                                   cBit1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSYN_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_LOPSTA
BitField Type: R_O
BitField Desc: LOPSTA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_Mask                                   cBit0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA PW Level1 Group Interrupt OR
Reg Addr   : 0x58004
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_PW_Level1_Group_Interrupt_OR                                                  0x58004

/*--------------------------------------
BitField Name: FMPM_TCA_PW_LEV1_OR
BitField Type: R_O
BitField Desc: Interrupt PW level1 group OR
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Level1_Group_Interrupt_OR_FMPM_TCA_PW_LEV1_OR_Mask                                   cBit0
#define cAf6_FMPM_TCA_PW_Level1_Group_Interrupt_OR_FMPM_TCA_PW_LEV1_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA PW Level2 Group Interrupt OR
Reg Addr   : 0x580E0-0x580EF
Reg Formula: 0x580E0+$D
    Where  : 
           + $D(0-10) : PW  Level-1 Group
Reg Desc   : 
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_PW_Level2_Group_Interrupt_OR_Base                                             0x580E0
#define cAf6Reg_FMPM_TCA_PW_Level2_Group_Interrupt_OR(D)                                         (0x580E0+(D))

/*--------------------------------------
BitField Name: FMPM_TCA_PW_LEV2_OR
BitField Type: R_O
BitField Desc: PW Level2 Group Interrupt OR, bit per group, the last Level-1
group has 12 Level-2 group
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Level2_Group_Interrupt_OR_FMPM_TCA_PW_LEV2_OR_Mask                                cBit31_0
#define cAf6_FMPM_TCA_PW_Level2_Group_Interrupt_OR_FMPM_TCA_PW_LEV2_OR_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA PW Framer Interrupt OR AND MASK Per PW
Reg Addr   : 0x5BC00-0x5BDFF
Reg Formula: 0x5BC00+$D*0x20+$E
    Where  : 
           + $D(0-10) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
Reg Desc   : 
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_Base                                   0x5BC00
#define cAf6Reg_FMPM_TCA_PW_Framer_Interrupt_OR_AND_MASK_Per_PW(D, E)                   (0x5BC00+(D)*0x20+(E))

/*--------------------------------------
BitField Name: FMPM_TCA_PW_OAMSK
BitField Type: R_O
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_TCA_PW_OAMSK_Mask                                cBit31_0
#define cAf6_FMPM_TCA_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_TCA_PW_OAMSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA PW Framer Interrupt MASK Per PW
Reg Addr   : 0x5BE00-0x5BFFF
Reg Formula: 0x5BE00+$D*0x20+$E
    Where  : 
           + $D(0-10) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
Reg Desc   : 
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW_Base                                          0x5BE00
#define cAf6Reg_FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW(D, E)                        (0x5B(E)00+(D)*0x20+(E))

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK
BitField Type: R/W
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW_FMPM_TCA_PW_MSK_Mask                                cBit31_0
#define cAf6_FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW_FMPM_TCA_PW_MSK_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA PW Interrupt MASK Per Type Per PW
Reg Addr   : 0x60000-0x63FFF
Reg Formula: 0x60000+$D*0x400+$E*0x20+$F
    Where  : 
           + $D(0-10) : PW  Level-1 Group
           + $E(0-31) : PW  Level-2 Group
           + $F(0-31) : PW-ID
Reg Desc   : 
FMPM TCA Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_Base                                        0x60000
#define cAf6Reg_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW(D, E, F)           (0x60000+(D)*0x400+(E)*0x20+(F))

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_ES
BitField Type: R/W
BitField Desc: ES
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_ES_Mask                                   cBit6
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_ES_Shift                                       6

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_SES
BitField Type: R/W
BitField Desc: SES
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_SES_Mask                                   cBit5
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_SES_Shift                                       5

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_UAS
BitField Type: R/W
BitField Desc: UAS
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_UAS_Mask                                   cBit4
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_UAS_Shift                                       4

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_ES_FE
BitField Type: R/W
BitField Desc: ES_FE
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_ES_FE_Mask                                   cBit3
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_ES_FE_Shift                                       3

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_SES_FE
BitField Type: R/W
BitField Desc: SES_FE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_SES_FE_Mask                                   cBit2
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_SES_FE_Shift                                       2

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_UAS_FE
BitField Type: R/W
BitField Desc: UAS_FE
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_UAS_FE_Mask                                   cBit1
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_UAS_FE_Shift                                       1

/*--------------------------------------
BitField Name: FMPM_TCA_PW_MSK_FC
BitField Type: R/W
BitField Desc: FC
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_FC_Mask                                   cBit0
#define cAf6_FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_TCA_PW_MSK_FC_Shift                                       0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULESURREG_H_ */


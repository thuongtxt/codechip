/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : Tha60290022SurInterruptManager.h
 * 
 * Created Date: May 29, 2017
 *
 * Description : Interrupt manager of SUR module of product 60290022
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022SURINTERRUPTMANAGER_H_
#define _THA60290022SURINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void Tha60290022SurInterruptManagerPwInterruptProcess(ThaSurInterruptManager self, AtHal hal);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290022SURINTERRUPTMANAGER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290022PwKbyteChannel.c
 *
 * Created Date: Jun 8, 2018
 *
 * Description : APS/K-byte channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/pw/Tha6029PwKbyteChannelInternal.h"
#include "Tha60290022PwKByte.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PwKbyteChannel
    {
    tTha6029PwKbyteChannel super;
    }tTha60290022PwKbyteChannel;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6029PwKbyteChannelMethods m_Tha6029PwKbyteChannelOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ChannelRxOffset(Tha6029PwKbyteChannel self)
    {
    return AtChannelIdGet((AtChannel)self);
    }

static void OverrideTha6029PwKbyteChannel(AtPwKbyteChannel self)
    {
    Tha6029PwKbyteChannel channel = (Tha6029PwKbyteChannel)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029PwKbyteChannelOverride, mMethodsGet(channel), sizeof(m_Tha6029PwKbyteChannelOverride));

        /* Setup methods */
        mMethodOverride(m_Tha6029PwKbyteChannelOverride, ChannelRxOffset);
        }

    mMethodsSet(channel, &m_Tha6029PwKbyteChannelOverride);
    }

static void Override(AtPwKbyteChannel self)
    {
    OverrideTha6029PwKbyteChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029PwKbyteChannel);
    }

static AtPwKbyteChannel ObjectInit(AtPwKbyteChannel self, uint32 channelId, AtPw pw, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (Tha6029PwKbyteChannelObjectInit(self, channelId, pw, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPwKbyteChannel Tha60290022PwKbyteChannelNew(uint32 channelId, AtPw pw, AtModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwKbyteChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChannel, channelId, pw, module);
    }

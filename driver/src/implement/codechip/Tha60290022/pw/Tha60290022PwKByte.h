/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60290022PwKByte.h
 * 
 * Created Date: Jan 11, 2018
 *
 * Description : Kbyte PW interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PWKBYTE_H_
#define _THA60290022PWKBYTE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPw.h"
#include "AtPwKbyteChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPw Tha60290022PwKbyteV2New(uint32 pwId, AtModulePw module);
AtPwKbyteChannel Tha60290022PwKbyteChannelNew(uint32 channelId, AtPw pw, AtModulePw module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PWKBYTE_H_ */


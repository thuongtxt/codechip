/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290022ModulePw.c
 *
 * Created Date: Apr 17, 2017
 *
 * Description : PW module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022ModulePwInternal.h"
#include "../../Tha60290021/pw/Tha60290021ModulePw.h"
#include "../man/Tha60290022Device.h"
#include "defectcontrollers/Tha60290022PwDefectController.h"
#include "Tha60290022PwDebugger.h"
#include "Tha60290022PwKByte.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegModulePwJitterBufferCenter 0x519000
#define cRegModulePwJitterBufferCenterEnableMask  cBit0
#define cRegModulePwJitterBufferCenterEnableShift 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwMethods          m_AtModulePwOverride;
static tTha60210011ModulePwMethods m_Tha60210011ModulePwOverride;
static tTha60290021ModulePwMethods m_Tha60290021ModulePwOverride;
static tThaModulePwMethods         m_ThaModulePwOverride;

/* Save super implementation */
static const tAtModulePwMethods          *m_AtModulePwMethods          = NULL;
static const tThaModulePwMethods         *m_ThaModulePwMethods         = NULL;
static const tTha60210011ModulePwMethods *m_Tha60210011ModulePwMethods = NULL;
static const tTha60290021ModulePwMethods *m_Tha60290021ModulePwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool RunOnVu9P(ThaModulePw self)
    {
    return Tha60290022DeviceRunOnVu9P(AtModuleDeviceGet((AtModule)self));
    }

static eBool SubportVlanHwIsReady(Tha60290021ModulePw self)
    {
    if (RunOnVu9P((ThaModulePw)self))
        return cAtFalse;
    return cAtTrue;
    }

static eBool CanSelectSubPortVlanPerPw(Tha60290021ModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Capacity15G(AtModulePw self)
    {
    return Tha60290022DeviceCapacity15G(AtModuleDeviceGet((AtModule)self));
    }

static eBool Capacity20G(AtModulePw self)
    {
    return Tha60290022DeviceShouldOpenFullCapacity(AtModuleDeviceGet((AtModule)self));
    }

static uint32 MaxPwsGet(AtModulePw self)
    {
    uint32 numPws = m_AtModulePwMethods->MaxPwsGet(self);

    if (Capacity15G(self))
        return 6 * 1344;

    if (Capacity20G(self))
        return numPws * 2;

    return numPws;
    }

static eBool SubportVlansSupported(ThaModulePw self)
    {
    /* This feature is temporary off on VU9P */
    if (RunOnVu9P(self))
        return cAtFalse;
    return cAtTrue;
    }

static ThaPwHeaderController BackupHeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    /* Not here in this product */
    AtUnused(self);
    AtUnused(adapter);
    return NULL;
    }

static uint32 NumPwsSupportCounters(ThaModulePw self)
    {
    uint32 counterModule = ThaModulePwDebugCountersModuleGet(self);

    /* Due to HW resource limiting: 8K */
    if (counterModule == cAtModulePw)
        return 8192;

    if (counterModule == cThaModulePmc)
        return AtModulePwMaxPwsGet((AtModulePw)self);

    return m_ThaModulePwMethods->NumPwsSupportCounters(self);
    }

static ThaPwDebugger PwDebuggerCreate(ThaModulePw self)
    {
    AtUnused(self);
    return Tha60290022PwDebuggerNew();
    }

static ThaPwInterruptProcessor InterruptProcessorCreate(ThaModulePw self)
    {
    return Tha60290022PwInterruptProcessorNew(self);
    }

static ThaPwDefectController DefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    if (mMethodsGet(self)->DefaultDefectModule(self) == cAtModuleSur)
        return m_ThaModulePwMethods->DefectControllerCreate(self, pw);

    return Tha60290022PwDefectControllerNew(pw);
    }

static uint32 HoPwTdmLineId(Tha60210011ModulePw self, AtPw pwAdapter, uint32 hwIdInSlice, uint32 moduleId)
    {
    if (moduleId == cThaModuleCla)
        return hwIdInSlice * 32;

    return m_Tha60210011ModulePwMethods->HoPwTdmLineId(self, pwAdapter, hwIdInSlice, moduleId);
    }

static AtPwHdlc HdlcPwObjectCreate(AtModulePw self, AtHdlcChannel hdlcChannel)
    {
    if (Tha60290022DeviceDccV2IsSupported(AtModuleDeviceGet((AtModule)self)))
        return Tha60290021PwDccV2New(hdlcChannel, self);

    return m_AtModulePwMethods->HdlcPwObjectCreate(self, hdlcChannel);
    }

static AtPw KBytePwObjectCreate(Tha60290021ModulePw self, uint32 pwId)
    {
    if (Tha60290022DeviceDccV2IsSupported(AtModuleDeviceGet((AtModule)self)))
        return Tha60290022PwKbyteV2New(0, (AtModulePw)self);

    return m_Tha60290021ModulePwMethods->KBytePwObjectCreate(self, pwId);
    }

static eBool JitterBufferCenteringIsSupported(AtModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModulePwRet JitterBufferCenteringEnable(AtModulePw self, eBool enable)
    {
    uint32 reg = cRegModulePwJitterBufferCenter;
    uint32 val = mModuleHwRead(self, reg);

    mFieldIns(&val, cRegModulePwJitterBufferCenterEnableMask, cRegModulePwJitterBufferCenterEnableShift, enable ? 1 : 0);
    mModuleHwWrite(self, reg, val);

    return cAtOk;
    }

static eBool JitterBufferCenteringIsEnabled(AtModulePw self)
    {
    eBool ret;
    uint32 reg = cRegModulePwJitterBufferCenter;
    uint32 val = mModuleHwRead(self, reg);

    mFieldGet(val, cRegModulePwJitterBufferCenterEnableMask, cRegModulePwJitterBufferCenterEnableShift, eBool, &ret);
    ret = mBinToBool(ret);

    return ret;
    }

static AtDevice Device(Tha60290021ModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return device;
    }

static eBool DccKbyteInterruptIsSupported(Tha60290021ModulePw self)
    {
    return Tha60290022DevicePwDccKbyteInterruptIsSupported(Device(self));
    }

static AtPwKbyteChannel KByteChannelObjectCreate(Tha60290021ModulePw self, uint32 channelId, AtPw pw)
    {
    if (Tha60290022DeviceDccV2IsSupported(AtModuleDeviceGet((AtModule)self)))
        return Tha60290022PwKbyteChannelNew(channelId, pw, (AtModulePw)self);

    return m_Tha60290021ModulePwMethods->KByteChannelObjectCreate(self, channelId, pw);
    }

static void OverrideTha60210011ModulePw(AtModulePw self)
    {
    Tha60210011ModulePw module = (Tha60210011ModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePwMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePwOverride, mMethodsGet(module), sizeof(m_Tha60210011ModulePwOverride));

        mMethodOverride(m_Tha60210011ModulePwOverride, HoPwTdmLineId);
        }

    mMethodsSet(module, &m_Tha60210011ModulePwOverride);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw module = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePwMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, m_ThaModulePwMethods, sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, SubportVlansSupported);
        mMethodOverride(m_ThaModulePwOverride, BackupHeaderControllerObjectCreate);
        mMethodOverride(m_ThaModulePwOverride, NumPwsSupportCounters);
        mMethodOverride(m_ThaModulePwOverride, PwDebuggerCreate);
        mMethodOverride(m_ThaModulePwOverride, InterruptProcessorCreate);
        mMethodOverride(m_ThaModulePwOverride, DefectControllerCreate);
        }

    mMethodsSet(module, &m_ThaModulePwOverride);
    }

static void OverrideTha60290021ModulePw(AtModulePw self)
    {
    Tha60290021ModulePw module = (Tha60290021ModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021ModulePwMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModulePwOverride, mMethodsGet(module), sizeof(m_Tha60290021ModulePwOverride));

        mMethodOverride(m_Tha60290021ModulePwOverride, SubportVlanHwIsReady);
        mMethodOverride(m_Tha60290021ModulePwOverride, CanSelectSubPortVlanPerPw);
        mMethodOverride(m_Tha60290021ModulePwOverride, KBytePwObjectCreate);
        mMethodOverride(m_Tha60290021ModulePwOverride, DccKbyteInterruptIsSupported);
        mMethodOverride(m_Tha60290021ModulePwOverride, KByteChannelObjectCreate);
        }

    mMethodsSet(module, &m_Tha60290021ModulePwOverride);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, m_AtModulePwMethods, sizeof(m_AtModulePwOverride));

        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        mMethodOverride(m_AtModulePwOverride, HdlcPwObjectCreate);
        mMethodOverride(m_AtModulePwOverride, JitterBufferCenteringIsSupported);
        mMethodOverride(m_AtModulePwOverride, JitterBufferCenteringEnable);
        mMethodOverride(m_AtModulePwOverride, JitterBufferCenteringIsEnabled);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtModulePw(self);
    OverrideThaModulePw(self);
    OverrideTha60210011ModulePw(self);
    OverrideTha60290021ModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModulePw);
    }

AtModulePw Tha60290022ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60290022ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModulePwObjectInit(newModule, device);
    }

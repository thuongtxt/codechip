/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60290022PwDebugger.h
 * 
 * Created Date: May 26, 2017
 *
 * Description : PW debugger
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PWDEBUGGER_H_
#define _THA60290022PWDEBUGGER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwDebugger Tha60290022PwDebuggerNew(void);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PWDEBUGGER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW Module
 *
 * File        : Tha60290022KBytePwV2.c
 *
 * Created Date: Jan 11, 2018
 *
 * Description : Tha60290022 Pw Kbyte Implement code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/pw/Tha60290021PwKbyteInternal.h"
#include "../../Tha60290021/pw/Tha60290021DccKbyteV2Reg.h"
#include "../../Tha60290021/pw/Tha60290021ModulePw.h"
#include "Tha60290022PwKByte.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PwKByteV2
    {
    tTha60290021PwKByte super;
    }tTha60290022PwKByteV2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021PwKByteMethods m_Tha60290021PwKByteOverride;
static tAtChannelMethods          m_AtChannelOverride;

/* Save super implementation */
static const tTha60290021PwKByteMethods *m_Tha60290021PwKByteMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasInterruptV2(Tha60290021PwKByte self)
    {
    Tha60290021ModulePw pwModule = (Tha60290021ModulePw)AtChannelModuleGet((AtChannel)self);
    return Tha60290021ModulePwDccKbyteInterruptIsSupported(pwModule);
    }

static uint32 InterruptEnableRegister(Tha60290021PwKByte self)
    {
    if (HasInterruptV2(self))
        return cAf6Reg_upen_rxk12_intenb;
    return m_Tha60290021PwKByteMethods->InterruptEnableRegister(self);
    }

static uint32 InterruptStatusRegister(Tha60290021PwKByte self)
    {
    if (HasInterruptV2(self))
        return cAf6Reg_upen_int_rxk12_stt;
    return m_Tha60290021PwKByteMethods->InterruptStatusRegister(self);
    }

static uint32 CurrentStatusRegister(Tha60290021PwKByte self)
    {
    if (HasInterruptV2(self))
        return cAf6Reg_upen_k12rx_cur_err;
    return m_Tha60290021PwKByteMethods->CurrentStatusRegister(self);
    }

static eAtRet HwInterruptEnable(AtChannel self, eBool enable)
    {
    uint32 regAddr = cAf6Reg_af6ces10grtldcck_corepi__upen_rxdcc_or_enb + Tha60290021PwKByteBaseAddress((AtPw)self);
    mChannelHwWrite(self, regAddr, enable ? cBit0 : 0, cAtModuleSdh);
    return cAtOk;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    return HwInterruptEnable(self, cAtTrue);
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    return HwInterruptEnable(self, cAtFalse);
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    uint32 regAddr = cAf6Reg_af6ces10grtldcck_corepi__upen_rxdcc_or_enb + Tha60290021PwKByteBaseAddress((AtPw)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleSdh);
    return (regVal & cBit0) ? cAtTrue : cAtFalse;
    }

static void Tha60290021Override(AtPw self)
    {
    Tha60290021PwKByte kBytePw = (Tha60290021PwKByte)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021PwKByteMethods = mMethodsGet(kBytePw);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021PwKByteOverride, m_Tha60290021PwKByteMethods, sizeof(m_Tha60290021PwKByteOverride));

        mMethodOverride(m_Tha60290021PwKByteOverride, InterruptEnableRegister);
        mMethodOverride(m_Tha60290021PwKByteOverride, InterruptStatusRegister);
        mMethodOverride(m_Tha60290021PwKByteOverride, CurrentStatusRegister);
        }

    mMethodsSet(kBytePw, &m_Tha60290021PwKByteOverride);
    }

static void OverrideAtChannel(AtPw self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPw self)
    {
    Tha60290021Override(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PwKByteV2);
    }

static AtPw ObjectInit(AtPw self, uint32 pwId, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (Tha60290021PwKbyteObjectInit(self, pwId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPw Tha60290022PwKbyteV2New(uint32 pwId, AtModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPw newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChannel, pwId, module);
    }

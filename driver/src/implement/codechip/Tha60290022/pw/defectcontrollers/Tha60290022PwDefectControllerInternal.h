/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW Module
 * 
 * File        : Tha60290022PwDefectControllerInternal.h
 * 
 * Created Date: March 05, 2018
 *
 * Description : Tha60290021 Defect controller Internal
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PWDEFECTCONTROLLERINTERNAL_H_
#define _THA60290022PWDEFECTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210051/pw/defectcontrollers/Tha60210051PwDefectControllerInternal.h"
#include "Tha60290022PwDefectController.h"



/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022PwDefectController
    {
    tTha60210051PwDefectController super;
    }tTha60290022PwDefectController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwDefectController Tha60290022PwDefectControllerObjectInit(ThaPwDefectController self, AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PWDEFECTCONTROLLERINTERNAL_H_ */


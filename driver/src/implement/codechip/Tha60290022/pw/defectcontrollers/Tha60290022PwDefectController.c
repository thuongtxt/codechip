/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290022PwDefectController.c
 *
 * Created Date: Jul 4, 2017
 *
 * Description : PW defect controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290022PwDefectControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwDefectControllerMethods m_ThaPwDefectControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PwDefaultOffset(ThaPwDefectController self)
    {
    return AtChannelIdGet((AtChannel)mAdapter(self));
    }

static void OverrideThaPwDefectController(ThaPwDefectController self)
    {
    ThaPwDefectController controller = (ThaPwDefectController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDefectControllerOverride, mMethodsGet(controller), sizeof(m_ThaPwDefectControllerOverride));

        mMethodOverride(m_ThaPwDefectControllerOverride, PwDefaultOffset);
        }

    mMethodsSet(controller, &m_ThaPwDefectControllerOverride);
    }

static void Override(ThaPwDefectController self)
    {
    OverrideThaPwDefectController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PwDefectController);
    }

ThaPwDefectController Tha60290022PwDefectControllerObjectInit(ThaPwDefectController self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051PwDefectControllerObjectInit(self, pw) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDefectController Tha60290022PwDefectControllerNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDefectController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022PwDefectControllerObjectInit(newController, pw);
    }

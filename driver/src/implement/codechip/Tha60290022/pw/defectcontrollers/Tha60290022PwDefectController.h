/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60290022PwDefectController.h
 * 
 * Created Date: Jul 4, 2017
 *
 * Description : Defect controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PWDEFECTCONTROLLER_H_
#define _THA60290022PWDEFECTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwDefectController Tha60290022PwDefectControllerNew(AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PWDEFECTCONTROLLER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290022PwDebugger.c
 *
 * Created Date: May 26, 2017
 *
 * Description : PW debugger
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/common/AtChannelInternal.h" /* Just for read/write macros */
#include "../../Tha60210011/pw/Tha60210011PwDebuggerInternal.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../man/Tha60290022DeviceReg.h"
#include "Tha60290022PwDebugger.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PwDebugger
    {
    tTha60210011PwDebugger super;
    }tTha60290022PwDebugger;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwDebuggerMethods m_ThaPwDebuggerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CepPwType(AtPw pw)
    {
    AtSdhChannel channel = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    uint32 vcType = AtSdhChannelTypeGet(channel);

    switch (vcType)
        {
        case cAtSdhChannelTypeVc4    : return 1;
        case cAtSdhChannelTypeVc4_4c : return 2;
        case cAtSdhChannelTypeVc4_nc : return 6;
        case cAtSdhChannelTypeVc4_16c: return 3;
        case cAtSdhChannelTypeVc4_64c: return 3;
        case cAtSdhChannelTypeVc11   : return 4;
        case cAtSdhChannelTypeVc12   : return 4;
        case cAtSdhChannelTypeVc3:
            {
            if (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(channel)) == cAtSdhChannelTypeTu3)
                return 5;
            return 0;
            }

        default:
            return cInvalidUint32;
        }
    }

static uint32 PwType(AtPw pw)
    {
    eAtPwType pwType = AtPwTypeGet(pw);

    if (pwType == cAtPwTypeCEP)
        return CepPwType(pw);

    if ((pwType == cAtPwTypeSAToP) ||
        (pwType == cAtPwTypeCESoP))
        return 7;

    return cInvalidUint32;
    }

static eBool IsVc4_64cCep(AtPw pw)
    {
    uint32 vcType;

    if (AtPwTypeGet(pw) != cAtPwTypeCEP)
        return cAtFalse;

    vcType = AtSdhChannelTypeGet((AtSdhChannel)AtPwBoundCircuitGet(pw));
    return (vcType == cAtSdhChannelTypeVc4_64c) ? cAtTrue : cAtFalse;
    }

static eAtRet PwSelect(ThaPwDebugger self, AtPw pw)
    {
    uint8 slice;
    uint32 sts;
    uint32 regAddr = cAf6Reg_DebugPwControl_Base;
    uint32 regVal = mChannelHwRead(pw, regAddr, cAtModulePw);
    uint32 globalPwId = AtChannelIdGet((AtChannel)pw);
    uint32 tdmPwId = AtChannelHwIdGet((AtChannel)pw);
    uint32 fieldVal, pwType;
    eAtRet ret;

    AtUnused(self);

    /* TDM PW */
    mRegFieldSet(regVal, cAf6_DebugPwControl_LoTdmPwID_, tdmPwId);

    /* Global PW */
    mFieldGet(globalPwId, cBit12_0, 0, uint32, &fieldVal);
    mRegFieldSet(regVal, cAf6_DebugPwControl_GlobalPwID12_0_, fieldVal);
    mFieldGet(globalPwId, cBit13, 13, uint32, &fieldVal);
    mRegFieldSet(regVal, cAf6_DebugPwControl_GlobalPwID13_, fieldVal);

    /* STS192c CEP */
    mRegFieldSet(regVal, cAf6_DebugPwControl_Oc192CepEna_, IsVc4_64cCep(pw) ? 1 : 0);

    ret = Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, &sts);
    if (ret != cAtOk)
        return ret;

    if (IsVc4_64cCep(pw))
        mRegFieldSet(regVal, cAf6_DebugPwControl_OC48ID_, (slice / 4U));
    else
        mRegFieldSet(regVal, cAf6_DebugPwControl_OC48ID_, slice);

    pwType = PwType(pw);
    if (pwType == cInvalidUint32)
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "PW type cannot be determined\r\n");
        return cAtErrorInvalidOperation;
        }

    mRegFieldSet(regVal, cAf6_DebugPwControl_HoPwType_, pwType);

    mChannelHwWrite(pw, regAddr, regVal, cAtModulePw);

    return cAtOk;
    }

static uint32 PlaPrbsCheck(ThaPwDebugger self, AtPw pw, uint32 regVal)
    {
    uint32 result = 0;

    AtUnused(self);
    AtUnused(pw);

    if (regVal & cAf6_DebugPwSticky_PlaPrbsErr_Mask)
        result |= cThaPwPrbsPlaError;
    if ((regVal & cAf6_DebugPwSticky_PlaPrbsSyn_Mask) == 0)
        result |= cThaPwPrbsPlaNotSync;

    return result;
    }

static uint32 PdaPrbsCheck(ThaPwDebugger self, AtPw pw, uint32 regVal)
    {
    uint32 result = 0;

    AtUnused(self);
    AtUnused(pw);

    if (regVal & cAf6_DebugPwSticky_PdaPrbsErr_Mask)
        result |= cThaPwPrbsPdaError;
    if ((regVal & cAf6_DebugPwSticky_PdaPrbsSyn_Mask) == 0)
        result |= cThaPwPrbsPdaNotSync;

    return result;
    }

static uint32 PlaSpeedBase(ThaPwDebugger self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAf6Reg_DebugPwPlaSpeed_Base;
    }

static uint32 PdaSpeedBase(ThaPwDebugger self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAf6Reg_DebugPwPdaHoSpeed_Base;
    }

static void OverrideThaPwDebugger(ThaPwDebugger self)
    {
    ThaPwDebugger debugger = (ThaPwDebugger)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDebuggerOverride, mMethodsGet(debugger), sizeof(m_ThaPwDebuggerOverride));

        mMethodOverride(m_ThaPwDebuggerOverride, PwSelect);
        mMethodOverride(m_ThaPwDebuggerOverride, PlaPrbsCheck);
        mMethodOverride(m_ThaPwDebuggerOverride, PdaPrbsCheck);
        mMethodOverride(m_ThaPwDebuggerOverride, PlaSpeedBase);
        mMethodOverride(m_ThaPwDebuggerOverride, PdaSpeedBase);
        }

    mMethodsSet(debugger, &m_ThaPwDebuggerOverride);
    }

static void Override(ThaPwDebugger self)
    {
    OverrideThaPwDebugger(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PwDebugger);
    }

static ThaPwDebugger ObjectInit(ThaPwDebugger self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PwDebuggerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDebugger Tha60290022PwDebuggerNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDebugger newChecker = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChecker == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChecker);
    }

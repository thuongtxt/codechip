/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60290022PwInterruptReg.h
 * 
 * Created Date: Jun 22, 2017
 *
 * Description : This file contain all constant definitions of PW block.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022PWINTERRUPTREG_H_
#define _THA60290022PWINTERRUPTREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : Counter Per Alarm Interrupt Enable Control
Reg Addr   : 0x060000
Reg Formula: 0x060000 + GrpID*32 + BitID
    Where  : 
           + $GrpID(0-335):  Pseudowire ID bits[13:5]
           + $BitID(0-31): Pseudowire ID bits [4:0]
Reg Desc   : 
This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_Base                                         0x060000
#define cAf6Reg_Counter Per Alarm Interrupt Enable Control(GrpID, BitID)           (0x060000+(GrpID)*32+(BitID))


/*------------------------------------------------------------------------------
Reg Name   : Counter Per Alarm Interrupt Status
Reg Addr   : 0x064000
Reg Formula: 0x064000 + GrpID*32 + BitID
    Where  : 
           + $GrpID(0-335):  Pseudowire ID bits[13:5]
           + $BitID(0-31): Pseudowire ID bits [4:0]
Reg Desc   : 
This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status_Base                                               0x064000
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status(GrpID, BitID)                 (0x064000+(GrpID)*32+(BitID))


/*------------------------------------------------------------------------------
Reg Name   : Counter Per Alarm Current Status
Reg Addr   : 0x068000
Reg Formula: 0x068000 + GrpID*32 + BitID
    Where  : 
           + $GrpID(0-335):  Pseudowire ID bits[13:5]
           + $BitID(0-31): Pseudowire ID bits [4:0]
Reg Desc   : 
This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Per_Alarm_Current_Status_Base                                                 0x068000
#define cAf6Reg_Counter_Per_Alarm_Current_Status(GrpID, BitID)                   (0x068000+(GrpID)*32+(BitID))


/*------------------------------------------------------------------------------
Reg Name   : Counter Interrupt OR Status
Reg Addr   : 0x06C000
Reg Formula: 0x06C000 + Grp1024ID*1024 + GrpID
    Where  : 
           + $Grp1024ID(0-10):  Pseudowire ID bits[13:10]
           + $GrpID(0-31):  Pseudowire ID bits[9:5]
Reg Desc   : 
This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Interrupt_OR_Status_Base                                                      0x06C000
#define cAf6Reg_Counter_Interrupt_OR_Status(Grp1024ID, GrpID)              (0x06C000+(Grp1024ID)*32+(GrpID))


/*------------------------------------------------------------------------------
Reg Name   : Counter per Group Interrupt OR Status
Reg Addr   : 0x06FFF0
Reg Formula: 0x06FFF0 + Grp1024ID
    Where  : 
           + $Grp1024ID(0-10):  Pseudowire ID bits[13:10]
Reg Desc   : 
The register consists of 8 bits for 8 Group of the PW Counter. Each bit is used to store Interrupt OR status of the related Group.

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_per_group_intr_or_stat_Base                                                   0x06FFF0
#define cAf6Reg_counter_per_group_intr_or_stat(Grp1024ID)                               (0x06FFF0+(Grp1024ID))


/*------------------------------------------------------------------------------
Reg Name   : Counter per Group Interrupt Enable Control
Reg Addr   : 0x06FFE0
Reg Formula: 0x06FFE0 + Grp1024ID
    Where  : 
           + $Grp1024ID(0-10):  Pseudowire ID bits[13:10]
Reg Desc   : 
The register consists of 8 interrupt enable bits for 8 group in the PW counter.

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_per_group_intr_en_ctrl_Base                                                   0x06FFE0
#define cAf6Reg_counter_per_group_intr_en_ctrl(Grp1024ID)                               (0x06FFE0+(Grp1024ID))

/*--------------------------------------
BitField Name: GroupIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related Group to generate interrupt.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Mask                                          cBit31_0
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : Counter Interrupt OR Status
Reg Addr   : 0x070000
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 11 bits for the PW Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_intr_or_stat                                                                  0x070000

/*--------------------------------------
BitField Name: GroupIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt bit is set and its interrupt is enabled
BitField Bits: [0]
--------------------------------------*/
#define cAf6_counter_intr_or_stat_GroupIntrOrSta_Mask                                                 cBit10_0
#define cAf6_counter_intr_or_stat_GroupIntrOrSta_Shift                                                       0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290022PWINTERRUPTREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290022PwInterruptProcessor.c
 *
 * Created Date: Oct 30, 2015
 *
 * Description : Interrupt processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../Tha60210011/pw/interruptprocessor/Tha60210011PwInterruptProcessor.h"
#include "Tha60290022PwInterruptReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cIteratorMask(_id)         ((cBit0) << (_id))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290022PwInterruptProcessor
    {
    tTha60210011PwInterruptProcessorPmc super;
    } tTha60290022PwInterruptProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwInterruptProcessorMethods m_ThaPwInterruptProcessorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Process(ThaPwInterruptProcessor self, uint32 glbIntr, AtHal hal)
    {
    AtModulePw module = (AtModulePw)ThaPwInterruptProcessorModuleGet(self);
    const uint32 baseAddress = ThaPwInterruptProcessorBaseAddress(self);
    uint32 intrSta = AtHalRead(hal, baseAddress + cAf6Reg_counter_intr_or_stat);
    uint32 grp1024;
    AtUnused(glbIntr);

    for (grp1024 = 0; grp1024 < 11; grp1024++)
        {
        if (intrSta & cIteratorMask(grp1024))
            {
            uint32 grpIntrEn = AtHalRead(hal, baseAddress + cAf6Reg_counter_per_group_intr_en_ctrl(grp1024));
            uint32 grpIntr   = AtHalRead(hal, baseAddress + cAf6Reg_counter_per_group_intr_or_stat(grp1024));
            uint32 grp32;

            grpIntr &= grpIntrEn;
            for (grp32 = 0; grp32 < 32; grp32++)
                {
                if (grpIntr & cIteratorMask(grp32))
                    {
                    uint32 grp32Intr = AtHalRead(hal, baseAddress + cAf6Reg_Counter_Interrupt_OR_Status(grp1024, grp32));
                    uint32 subPwId;

                    for (subPwId = 0; subPwId < 32; subPwId++)
                        {
                        if (grp32Intr & cIteratorMask(subPwId))
                            {
                            uint32 pwId = (grp1024 << 10) + (grp32 << 5) + subPwId;

                            /* Get PW channel */
                            AtPw pwChannel = AtModulePwGetPw(module, pwId);

                            /* Process channel interrupt. */
                            AtPwInterruptProcess(pwChannel, pwId, hal);
                            }
                        }
                    }
                }
            }
        }
    }

static eAtRet Enable(ThaPwInterruptProcessor self, eBool enable)
    {
    const uint32 baseAddress = ThaPwInterruptProcessorBaseAddress(self);
    AtDevice device = AtModuleDeviceGet((AtModule)ThaPwInterruptProcessorModuleGet(self));
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    uint32 grp1024;

    for (grp1024 = 0; grp1024 < 11; grp1024++)
        {
        uint32 regAddr = baseAddress + cAf6Reg_counter_per_group_intr_en_ctrl(grp1024);
        uint32 regVal  = (enable) ? cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Mask : 0x0;
        AtHalWrite(hal, regAddr, regVal);
        }

    return cAtOk;
    }

static uint32 CurrentStatusRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_Counter_Per_Alarm_Current_Status_Base;
    }

static uint32 InterruptStatusRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_Counter_Per_Alarm_Interrupt_Status_Base;
    }

static uint32 InterruptMaskRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_Base;
    }

static void OverrideThaPwInterruptProcessor(ThaPwInterruptProcessor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwInterruptProcessorOverride, mMethodsGet(self), sizeof(m_ThaPwInterruptProcessorOverride));

        mMethodOverride(m_ThaPwInterruptProcessorOverride, Process);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, Enable);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, CurrentStatusRegister);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, InterruptMaskRegister);
        }

    mMethodsSet(self, &m_ThaPwInterruptProcessorOverride);
    }

static void Override(ThaPwInterruptProcessor self)
    {
    OverrideThaPwInterruptProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022PwInterruptProcessor);
    }

static ThaPwInterruptProcessor ObjectInit(ThaPwInterruptProcessor self, ThaModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PwInterruptProcessorPmcObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwInterruptProcessor Tha60290022PwInterruptProcessorNew(ThaModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwInterruptProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW Module
 * 
 * File        : Tha60290021ModulePw.h
 * 
 * Created Date: Sep 4, 2016
 *
 * Description : Tha60290021 Module PW header
 * Module      : PW
 * 
 * File        : Tha60290022ModulePwInternal.h
 * 
 * Created Date: Jul 25, 2018
 *
 * Description : 60290022 module PW internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEPWINTERNAL_H_
#define _THA60290022MODULEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/pw/Tha60290021ModulePwInternal.h"
#include "../../Tha60290021/pw/Tha60290021ModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModulePw
    {
    tTha60290021ModulePw super;
    }tTha60290022ModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw Tha60290022ModulePwObjectInit(AtModulePw self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEPWINTERNAL_H_ */


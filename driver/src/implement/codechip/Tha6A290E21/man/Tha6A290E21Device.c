/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha6A290021Device.c
 *
 * Created Date: Oct 8, 2015
 *
 * Description : Tha60210031 Device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290E21DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
#if 0
static tAtDeviceMethods          m_AtDeviceOverride;


/* Super implementations */
static const tAtDeviceMethods *m_AtDeviceMethods = NULL;
#endif

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#if 0

static  AtSerdesManager SerdesManagerObjectCreate(AtDevice self)
    {
    AtUnused(self);
    return NULL;
    }


static eBool SerdesResetIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, SerdesResetIsSupported);
        mMethodOverride(m_AtDeviceOverride, SerdesManagerObjectCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }


static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    }
#endif

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021Device);
    }

AtDevice Tha6A290E21DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A290021DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    /*Override(self);*/
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha6A290E21DeviceNew(AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    return Tha6A290E21DeviceObjectInit(newDevice, driver, productCode);
    }

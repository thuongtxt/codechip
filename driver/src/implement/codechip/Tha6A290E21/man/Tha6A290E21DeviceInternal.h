/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DEVICE
 * 
 * File        : Tha60290021Device.h
 * 
 * Created Date: Jul 12, 2016
 *
 * Description : Device Management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290E21DEVICEINTERNAL_H_
#define _THA6A290E21DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A290021/man/Tha6A290021DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290E21Device
    {
    tTha6A290021Device super;
    }tTha6A290E21Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha6A290E21DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290E21DEVICEINTERNAL_H_ */


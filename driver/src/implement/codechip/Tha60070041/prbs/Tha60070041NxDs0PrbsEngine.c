/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : NxDS0 PRBS
 *
 * File        : Tha60070041NxDs0PrbsEngine.c
 *
 * Created Date: Mar 12, 2015
 *
 * Description : NxDS0 PRBS implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60070041PrbsEngineInternal.h"
#include "AtIterator.h"
#include "AtPdhDe1.h"
#include "AtPdhNxDs0.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cInvalid16BitValue 0xFF
#define cMaxNumTimeSlotInE1 32

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070041NxDs0PrbsEngine
    {
    tTha60070041PrbsEngine super;
    }tTha60070041NxDs0PrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60070041PrbsEngineMethods  m_Tha60070041PrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070041NxDs0PrbsEngine);
    }

static uint32 NxDs0StartIndex(uint32 nxds0Bitmap)
    {
    uint32 i;

    for (i = 0; i < cMaxNumTimeSlotInE1; i++)
        {
        if (nxds0Bitmap & (cBit0 << i))
            return i;
        }

    return cInvalid16BitValue;
    }

static uint32 NxDs0StopIndex(uint32 nxds0Bitmap)
    {
    uint32 i, startIndex;

    startIndex = cMaxNumTimeSlotInE1 - 1;
    for (i = 0; i < cMaxNumTimeSlotInE1; i++)
        {
        if (nxds0Bitmap & (cBit31 >> i))
            return startIndex;
        startIndex = startIndex - 1;
        }

    return cInvalid16BitValue;
    }

static uint32 HwChannelId(Tha60070041PrbsEngine self)
    {
    AtPdhNxDS0 nxds0 = (AtPdhNxDS0)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtPdhDe1 de1 = AtPdhNxDS0De1Get(nxds0);
    uint32 nxds0Bitmap = AtPdhNxDS0BitmapGet(nxds0);
    uint32 startTsId = NxDs0StartIndex(nxds0Bitmap);

    return (AtChannelIdGet((AtChannel)de1) << 10) + (startTsId << 5) + NxDs0StopIndex(nxds0Bitmap);
    }

static uint32 SideSw2Hw(Tha60070041PrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);

    if (side == cAtPrbsSideTdm) return 0x3;
    if (side == cAtPrbsSidePsn) return 0x2;

    return cInvalid16BitValue;
    }

static eAtPrbsSide SideHw2Sw(Tha60070041PrbsEngine self, uint32 value)
    {
    AtUnused(self);

    if (value == 0x2) return cAtPrbsSidePsn;
    if (value == 0x3) return cAtPrbsSideTdm;

    return cAtPrbsSideUnknown;
    }

static void OverrideTha60070041PrbsEngine(AtPrbsEngine self)
    {
    Tha60070041PrbsEngine engine = (Tha60070041PrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60070041PrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha60070041PrbsEngineOverride));

        mMethodOverride(m_Tha60070041PrbsEngineOverride, HwChannelId);
        mMethodOverride(m_Tha60070041PrbsEngineOverride, SideSw2Hw);
        mMethodOverride(m_Tha60070041PrbsEngineOverride, SideHw2Sw);
        }

    mMethodsSet(engine, &m_Tha60070041PrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha60070041PrbsEngine(self);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtChannel channel, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60070041PrbsEngineObjectInit(self, channel, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60070041NxDs0PrbsEngineNew(AtChannel channel, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, channel, engineId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60070041ModulePrbsReg.h
 * 
 * Created Date: Mar 12, 2015
 *
 * Description : PRBS registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60070041MODULEPRBSREG_H_
#define _THA60070041MODULEPRBSREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/* Bert control */
#define cThaRegBertCtrl               0x800000
#define cThaRegBertCtrlRstVal         0x0

#define cThaRegBertCtrlModeMask      cBit21
#define cThaRegBertCtrlModeShift     21

#define cThaRegBertCtrlErrInsMask    cBit20_19
#define cThaRegBertCtrlErrInsShift   19

#define cThaRegBertCtrlChnIdMask     cBit18_4
#define cThaRegBertCtrlChnIdShift    4

#define cThaRegBertCtrlSourceMask    cBit3_1
#define cThaRegBertCtrlSourceShift   1

#define cThaRegBertCtrlEnMask        cBit0
#define cThaRegBertCtrlEnShift       0


/* Bert status */
#define cThaRegBertStat            0x800001

#define cThaRegBertStatCurStatMask    cBit1 /* Not supported */
#define cThaRegBertStatCurStatShift   1

#define cThaRegBertStatErrorMask      cBit0
#define cThaRegBertStatErrorShift     0


/* Bert status */
#define cThaRegBertErrorCounters(r2c)   (r2c) ? 0x800003 : 0x800002

#ifdef __cplusplus
}
#endif
#endif /* _THA60070041MODULEPRBSREG_H_ */


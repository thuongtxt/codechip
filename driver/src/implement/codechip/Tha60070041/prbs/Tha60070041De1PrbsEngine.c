/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DE1 PRBS
 *
 * File        : Tha60070041De1PrbsEngine.c
 *
 * Created Date: Mar 12, 2015
 *
 * Description : DE1 PRBS implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60070041PrbsEngineInternal.h"
#include "Tha60070041ModulePrbsReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cInvalid16BitValue 0xFF

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070041De1PrbsEngine
    {
    tTha60070041PrbsEngine super;
    }tTha60070041De1PrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60070041PrbsEngineMethods  m_Tha60070041PrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070041De1PrbsEngine);
    }

static uint32 SideSw2Hw(Tha60070041PrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);

    if (side == cAtPrbsSideTdm) return 0x1;
    if (side == cAtPrbsSidePsn) return 0x0;

    return cInvalid16BitValue;
    }

static eAtPrbsSide SideHw2Sw(Tha60070041PrbsEngine self, uint32 value)
    {
    AtUnused(self);

    if (value == 0x0) return cAtPrbsSidePsn;
    if (value == 0x1) return cAtPrbsSideTdm;

    return cAtPrbsSideUnknown;
    }

static void OverrideTha60070041PrbsEngine(AtPrbsEngine self)
    {
    Tha60070041PrbsEngine engine = (Tha60070041PrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60070041PrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha60070041PrbsEngineOverride));

        mMethodOverride(m_Tha60070041PrbsEngineOverride, SideHw2Sw);
        mMethodOverride(m_Tha60070041PrbsEngineOverride, SideSw2Hw);
        }

    mMethodsSet(engine, &m_Tha60070041PrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha60070041PrbsEngine(self);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtChannel channel, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60070041PrbsEngineObjectInit(self, channel, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60070041De1PrbsEngineNew(AtChannel channel, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, channel, engineId);
    }

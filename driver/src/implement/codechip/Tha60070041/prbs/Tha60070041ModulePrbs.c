/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60070041ModulePrbs.c
 *
 * Created Date: Mar 12, 2015
 *
 * Description : PRBS implementations of AF60070041
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/prbs/AtModulePrbsInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/prbs/ThaModulePrbsInternal.h"
#include "Tha60070041ModulePrbsReg.h"
#include "Tha60070041ModulePrbs.h"
#include "Tha60070041PrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070041ModulePrbs
    {
    tThaModulePrbs super;
    }tTha60070041ModulePrbs;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tAtModulePrbsMethods m_AtModulePrbsOverride;
static tThaModulePrbsMethods m_ThaModulePrbsOverride;

/* Save super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPrbsEngine SdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    AtUnused(engineId);
    AtUnused(sdhVc);
    AtUnused(engineId);
    return NULL;
    }

static AtPrbsEngine De1PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha60070041De1PrbsEngineNew((AtChannel)de1, engineId);
    }

static AtPrbsEngine NxDs0PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha60070041NxDs0PrbsEngineNew((AtChannel)nxDs0, engineId);
    }

static AtPrbsEngine De3PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(de3);
    AtUnused(engineId);
    return NULL;
    }

static void PartDefaultSet(AtModule self, uint8 partId)
    {
    AtUnused(partId);
    mModuleHwWrite(self, cThaRegBertCtrl, cThaRegBertCtrlRstVal);
    }

static void DefaultSet(AtModule self)
    {
    uint8 part_i;
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);

    for (part_i = 0; part_i < ThaDeviceNumPartsOfModule(device, cAtModulePrbs); part_i++)
        PartDefaultSet(self, part_i);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    DefaultSet(self);
    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static const char *CapacityDescription(AtModule self)
    {
    AtUnused(self);
    return "de1, nxds0";
    }

static uint32 MaxNumEngines(AtModulePrbs self)
    {
    AtUnused(self);
    return 1;
    }

static AtPrbsEngine De1PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
    AtUnused(self);
    return Tha60070041De1PrbsEngineNew((AtChannel)de1, engineId);
    }

static AtPrbsEngine NxDs0PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtUnused(self);
    return Tha60070041NxDs0PrbsEngineNew((AtChannel)nxDs0, engineId);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, mMethodsGet(self), sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, SdhVcPrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, De3PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, De1PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, NxDs0PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, MaxNumEngines);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideThaModulePrbs(AtModulePrbs self)
    {
    ThaModulePrbs module = (ThaModulePrbs)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePrbsOverride, mMethodsGet(module), sizeof(m_ThaModulePrbsOverride));

        mMethodOverride(m_ThaModulePrbsOverride, De1PrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, NxDs0PrbsEngineObjectCreate);
        }

    mMethodsSet(module, &m_ThaModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtModule(self);
    OverrideAtModulePrbs(self);
    OverrideThaModulePrbs(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070041ModulePrbs);
    }

static AtModulePrbs ObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha60070041ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60070041PrbsEngine.h
 * 
 * Created Date: Apr 16, 2015
 *
 * Description : PRBS engine common declaration
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60070041PRBSENGINE_H_
#define _THA60070041PRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60070041De1PrbsEngineNew(AtChannel channel, uint32 engineId);
AtPrbsEngine Tha60070041NxDs0PrbsEngineNew(AtChannel channel, uint32 engineId);
AtPrbsEngine Tha60070041PwPrbsEngineNew(AtChannel channel, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60070041PRBSENGINE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60070041PrbsEngineInternal.h
 * 
 * Created Date: Mar 12, 2015
 *
 * Description : PRBS declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60070041PRBSENGINEINTERNAL_H_
#define _THA60070041PRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "Tha60070041PrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60070041PrbsEngine * Tha60070041PrbsEngine;

typedef struct tTha60070041PrbsEngineMethods
    {
    uint32 (*HwChannelId)(Tha60070041PrbsEngine self);
    uint32 (*SideSw2Hw)(Tha60070041PrbsEngine self, eAtPrbsSide option);
    eAtPrbsSide (*SideHw2Sw)(Tha60070041PrbsEngine self, uint32 value);
    }tTha60070041PrbsEngineMethods;

typedef struct tTha60070041PrbsEngine
    {
    tAtPrbsEngine super;
    const tTha60070041PrbsEngineMethods *methods;
    }tTha60070041PrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60070041PrbsEngineObjectInit(AtPrbsEngine self, AtChannel channel, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60070041PRBSENGINEINTERNAL_H_ */


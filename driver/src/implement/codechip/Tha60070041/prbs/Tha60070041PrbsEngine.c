/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS Engine
 *
 * File        : Tha60070041PrbsEngine.c
 *
 * Created Date: Mar 12, 2015
 *
 * Description : PRBS Engine implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtModuleInternal.h"
#include "Tha60070041PrbsEngineInternal.h"
#include "Tha60070041ModulePrbsReg.h"
#include "AtChannel.h"
#include "commacro.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60070041PrbsEngine)self)

/*--------------------------- Macros -----------------------------------------*/
#define cInvalid16BitValue  0xFF

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60070041PrbsEngineMethods m_methods;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tAtPrbsEngineMethods    m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtObjectMethods      *m_AtObjectMethods = NULL;
static const tAtPrbsEngineMethods  *m_AtPrbsEngineMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070041PrbsEngine);
    }

static AtModule PrbsModule(AtPrbsEngine self)
    {
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet(self));
    return AtDeviceModuleGet(device, cAtModulePrbs);
    }

static uint32 Read(AtPrbsEngine self, uint32 address)
    {
    return mModuleHwRead(PrbsModule(self), address);
    }

static void Write(AtPrbsEngine self, uint32 address, uint32 value)
    {
    mModuleHwWrite(PrbsModule(self), address, value);
    }

static eAtRet PrbsEngineChannelSet(AtPrbsEngine self, uint32 channelId)
    {
    uint32 regVal = Read(self, cThaRegBertCtrl);

    if (channelId == mRegField(regVal, cThaRegBertCtrlChnId))
        return cAtOk;

    mRegFieldSet(regVal, cThaRegBertCtrlChnId, channelId);
    Write(self, cThaRegBertCtrl, regVal);

    return cAtOk;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 regVal = Read(self, cThaRegBertCtrl);

    mRegFieldSet(regVal, cThaRegBertCtrlEn, enable);
    Write(self, cThaRegBertCtrl, regVal);

    return PrbsEngineChannelSet(self, mMethodsGet(mThis(self))->HwChannelId(mThis(self)));
    }

static eBool EngineIsWorking(AtPrbsEngine self)
    {
    uint32 regVal = Read(self, cThaRegBertCtrl);

    if (mMethodsGet(mThis(self))->HwChannelId(mThis(self)) == mRegField(regVal, cThaRegBertCtrlChnId))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regVal;

    if (!EngineIsWorking(self))
        return cAtFalse;

    regVal = Read(self, cThaRegBertCtrl);
    return (regVal & cThaRegBertCtrlEnMask) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    return AtPrbsEngineIsEnabled(self);
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    return AtPrbsEngineIsEnabled(self);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 regVal;
    uint32 alarmMask = 0;

    if (!EngineIsWorking(self))
        return 0x0;

    regVal = Read(self, cThaRegBertStat);

    /* Need to clear sticky for the next reading */
    Write(self, cThaRegBertStat, 0x1);

    if (regVal & cThaRegBertStatErrorMask)
        alarmMask |= cAtPrbsEngineAlarmTypeError;

    return alarmMask;
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    if ((prbsMode == cAtPrbsModePrbs15) || (prbsMode == cAtPrbsModePrbs23))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 PrbsModeSw2Hw(eAtPrbsMode prbsMode)
    {
    if (prbsMode == cAtPrbsModePrbs15) return 0;
    if (prbsMode == cAtPrbsModePrbs23) return 1;

    return 0;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regVal;

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    regVal = Read(self, cThaRegBertCtrl);
    mRegFieldSet(regVal, cThaRegBertCtrlMode, PrbsModeSw2Hw(prbsMode));
    Write(self, cThaRegBertCtrl, regVal);

    return PrbsEngineChannelSet(self, mMethodsGet(mThis(self))->HwChannelId(mThis(self)));
    }

static eAtPrbsMode PrbsModeHw2Sw(uint32 mode)
    {
    if (mode == 0) return cAtPrbsModePrbs15;
    if (mode == 1) return cAtPrbsModePrbs23;
    return cAtPrbsModeInvalid;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    uint32 regVal;

    if (!EngineIsWorking(self))
        return cAtPrbsModeInvalid;

    regVal = Read(self, cThaRegBertCtrl);
    return PrbsModeHw2Sw(mRegField(regVal, cThaRegBertCtrlMode));
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    AtUnused(prbsMode);
    return cAtErrorModeNotSupport;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineModeGet(self);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    AtUnused(prbsMode);
    return cAtErrorModeNotSupport;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineModeGet(self);
    }

static uint32 SideSw2Hw(Tha60070041PrbsEngine self, eAtPrbsSide side)
    {
    /* Concrete class will know */
    AtUnused(self);
    AtUnused(side);
    return cInvalid16BitValue;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide option)
    {
    uint32 regVal = AtPrbsEngineRead(self, cThaRegBertCtrl, cAtModulePdh);
    mRegFieldSet(regVal, cThaRegBertCtrlSource, mMethodsGet(mThis(self))->SideSw2Hw(mThis(self), option));
    AtPrbsEngineWrite(self, cThaRegBertCtrl, regVal, cAtModulePdh);

    return m_AtPrbsEngineMethods->SideSet(self, option);
    }

static eAtPrbsSide SideHw2Sw(Tha60070041PrbsEngine self, uint32 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtPrbsSideUnknown;
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    uint32 regVal = AtPrbsEngineRead(self, cThaRegBertCtrl, cAtModulePdh);
    return mMethodsGet(mThis(self))->SideHw2Sw(mThis(self), mRegField(regVal, cThaRegBertCtrlSource));
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regVal = AtPrbsEngineRead(self, cThaRegBertCtrl, cAtModulePdh);

    mRegFieldSet(regVal, cThaRegBertCtrlErrIns, force ? 0x2 : 0x0);
    AtPrbsEngineWrite(self, cThaRegBertCtrl, regVal, cAtModulePdh);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regVal = AtPrbsEngineRead(self, cThaRegBertCtrl, cAtModulePdh);
    return (regVal & cThaRegBertCtrlErrInsMask) ? cAtTrue : cAtFalse;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool isR2c)
    {
    uint32 regVal = AtPrbsEngineRead(self, cThaRegBertStat, cAtModulePdh);

    if (isR2c)
        AtPrbsEngineWrite(self, cThaRegBertStat, 0x1, cAtModulePdh);

    return mRegField(regVal, cThaRegBertStatError);
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static uint32 CounterRead2Clear(AtPrbsEngine self, uint16 counterType, eBool r2c)
    {
    if (!AtPrbsEngineCounterIsSupported(self, counterType))
        return cAtErrorModeNotSupport;

    return AtPrbsEngineRead(self, cThaRegBertErrorCounters(r2c), cAtModulePdh);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    if (counterType != cAtPrbsEngineCounterRxBitError)
        return cAtFalse;
    return cAtTrue;
    }

static eAtModulePrbsRet TxErrorInject(AtPrbsEngine self, uint32 numErrors)
    {
    uint32 regVal = AtPrbsEngineRead(self, cThaRegBertCtrl, cAtModulePdh);

    if (numErrors > 1)
        return cAtErrorModeNotSupport;

    mRegFieldSet(regVal, cThaRegBertCtrlErrIns, numErrors);
    AtPrbsEngineWrite(self, cThaRegBertCtrl, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    if (AtPrbsEngineGeneratingSideGet(self) == side)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    if (AtPrbsEngineMonitoringSideGet(self) == side)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static void Delete(AtObject self)
    {
    AtPrbsEngineEnable((AtPrbsEngine)self, cAtFalse);
    AtPrbsEngineErrorForce((AtPrbsEngine)self, cAtFalse);
    m_AtObjectMethods->Delete(self);
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    AtPrbsEngineEnable(self, cAtFalse);
    AtPrbsEngineErrorForce(self, cAtFalse);

    return cAtOk;
    }

static eBool TxErrorInjectionIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorInject);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorInjectionIsSupported);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 HwChannelId(Tha60070041PrbsEngine self)
    {
    return AtChannelIdGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    }

static void MethodsInit(AtPrbsEngine self)
    {
    Tha60070041PrbsEngine engine = (Tha60070041PrbsEngine)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, HwChannelId);
        mMethodOverride(m_methods, SideSw2Hw);
        mMethodOverride(m_methods, SideHw2Sw);
        }

    mMethodsSet(engine, &m_methods);
    }

AtPrbsEngine Tha60070041PrbsEngineObjectInit(AtPrbsEngine self, AtChannel channel, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, channel, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60070041ModuleMap.c
 *
 * Created Date: Jan 19, 2015
 *
 * Description : Map module implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/map/ThaModuleMapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaMapChnTypeMask                          cBit16_15
#define cThaMapChnTypeShift                         15

#define cThaMapFirstTsMask                          cBit14
#define cThaMapFirstTsShift                         14

#define cThaMapTsEnMask                             cBit13
#define cThaMapTsEnShift                            13

#define cThaMapPWIdFieldMask                        cBit7_0
#define cThaMapPWIdFieldShift                       0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070041ModuleMap
    {
    tThaModuleMap super;
    }tTha60070041ModuleMap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaModuleMapMethods         m_ThaModuleMapOverride;

/*--------------------------- Forward declarations ---------------------------*/
mDefineMaskShift(ThaModuleMap, MapChnType)
mDefineMaskShift(ThaModuleMap, MapFirstTs)
mDefineMaskShift(ThaModuleMap, MapTsEn)
mDefineMaskShift(ThaModuleMap, MapPWIdField)

static void OverrideThaModuleMap(AtModule self)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;

    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleMapOverride));

        /* Override bit fields */
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapChnType)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapFirstTs)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapTsEn)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapPWIdField)
        }

    mMethodsSet(mapModule, &m_ThaModuleMapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070041ModuleMap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super construction */
    if (ThaModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60070041ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }


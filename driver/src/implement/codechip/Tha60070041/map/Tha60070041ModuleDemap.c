/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DEMAP
 *
 * File        : Tha60070041ModuleDemap.c
 *
 * Created Date: Jan 19, 2015
 *
 * Description : Demap module implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/map/ThaModuleDemapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaDmapChnTypeMask                         cBit11_10
#define cThaDmapChnTypeShift                        10

#define cThaDmapFirstTsMask                         cBit9
#define cThaDmapFirstTsShift                        9

#define cThaDmapTsEnMask                            cBit8
#define cThaDmapTsEnShift                           8

#define cThaDmapPwIdMask                            cBit7_0
#define cThaDmapPwIdShift                           0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070041ModuleDemap
    {
    tThaModuleDemap super;
    }tTha60070041ModuleDemap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaModuleDemapMethods         m_ThaModuleDemapOverride;

/*--------------------------- Forward declarations ---------------------------*/
mDefineMaskShift(ThaModuleDemap, DmapChnType)
mDefineMaskShift(ThaModuleDemap, DmapFirstTs)
mDefineMaskShift(ThaModuleDemap, DmapTsEn)
mDefineMaskShift(ThaModuleDemap, DmapPwId)

static void OverrideThaModuleMap(AtModule self)
    {
    ThaModuleDemap demapModule = (ThaModuleDemap)self;

    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDemapOverride, mMethodsGet(demapModule), sizeof(m_ThaModuleDemapOverride));

        /* Override bit fields */
        mBitFieldOverride(mapModule, m_ThaModuleDemapOverride, DmapChnType)
        mBitFieldOverride(mapModule, m_ThaModuleDemapOverride, DmapFirstTs)
        mBitFieldOverride(mapModule, m_ThaModuleDemapOverride, DmapTsEn)
        mBitFieldOverride(mapModule, m_ThaModuleDemapOverride, DmapPwId)
        }

    mMethodsSet(demapModule, &m_ThaModuleDemapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070041ModuleDemap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super construction */
    if (ThaModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60070041ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

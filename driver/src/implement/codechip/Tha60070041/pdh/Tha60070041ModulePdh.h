/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60070041ModulePdh.h
 * 
 * Created Date: Apr 26, 2015
 *
 * Description : PDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60070041MODULEPDH_H_
#define _THA60070041MODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaPdhPwProduct/pdh/ThaPdhPwProductModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60070041ModulePdh
    {
    tThaPdhPwProductModulePdh super;
    }tTha60070041ModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh Tha60070041ModulePdhObjectInit(AtModulePdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60070041MODULEPDH_H_ */


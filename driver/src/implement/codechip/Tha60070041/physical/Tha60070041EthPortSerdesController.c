/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60070041EthPortSerdesController.c
 *
 * Created Date: Mar 13, 2014
 *
 * Description : ETH Port SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/physical/ThaSerdesControllerInternal.h"
#include "AtHalLoop.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegEthPortSerdesControl         0x0
#define cThaRegEthPortSerdesIsolateMask     cBit10
#define cThaRegEthPortSerdesIsolateShift    10

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070041EthPortSerdesController
    {
    tAtSerdesController super;
    }tTha60070041EthPortSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods  m_AtSerdesControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070041EthPortSerdesController);
    }

static AtHal SerdesHal(AtSerdesController self)
    {
    AtChannel port = (AtChannel)AtSerdesControllerPhysicalPortGet(self);
    AtDevice device = AtChannelDeviceGet(port);
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    return AtHalLoopGeSerdes(hal, (uint8)AtChannelIdGet(port));
    }

static void Isolate(AtSerdesController self, eBool isolate)
    {
    AtHal hal = SerdesHal(self);
    uint32 regVal = AtHalRead(hal, cThaRegEthPortSerdesControl);
    mRegFieldSet(regVal, cThaRegEthPortSerdesIsolate, mBoolToBin(isolate));
    AtHalWrite(hal, cThaRegEthPortSerdesControl, regVal);
    }

static eAtRet Init(AtSerdesController self)
    {
    Isolate(self, cAtFalse);
    return cAtOk;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return ((mode == cAtSerdesModeGe) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeGe;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    }

static AtSerdesController Tha60070041EthPortSerdesControllerObjectInit(AtSerdesController self, AtEthPort phyPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSerdesControllerObjectInit(self, (AtChannel)phyPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60070041EthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60070041EthPortSerdesControllerObjectInit(newController, ethPort, serdesId);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60070041ModuleEth.c
 *
 * Created Date: Mar 13, 2014
 *
 * Description : ETH Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaPdhPwProduct/eth/ThaPdhPwProductModuleEth.h"
#include "../../../default/physical/ThaSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070041ModuleEth
    {
    tThaPdhPwProductModuleEth super;
    }tTha60070041ModuleEth;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController PortSerdesControllerCreate(ThaModuleEth self, AtEthPort ethPort)
    {
	AtUnused(self);
    return Tha60070041EthPortSerdesControllerNew(ethPort, AtChannelIdGet((AtChannel)ethPort));
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
	AtUnused(self);
    return 1;
    }

static eBool ApsIsSupported(ThaModuleEth self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtEthPortInterface PortDefaultInterface(AtModuleEth self, AtEthPort port)
    {
	AtUnused(self);
	AtUnused(port);
    return cAtEthPortInterfaceGmii;
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, PortDefaultInterface);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth thaModule = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(thaModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, PortSerdesControllerCreate);
        mMethodOverride(m_ThaModuleEthOverride, ApsIsSupported);
        }

    mMethodsSet(thaModule, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070041ModuleEth);
    }

static AtModuleEth ObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60070041ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

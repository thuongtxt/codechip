/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60070041Device.h
 * 
 * Created Date: Apr 26, 2015
 *
 * Description : Product 60070041 (32xE1s CES)
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60070041DEVICE_H_
#define _THA60070041DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60070041DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

uint32 Tha60070041DeviceTemperatureAlarmGet(AtDevice self);
uint32 Tha60070041DeviceTemperatureGet(AtDevice self);
eAtRet Tha60070041DeviceTemperatureRedAlarmSetThresholdSet(AtDevice self, uint32 celsicusValue);
uint32 Tha60070041DeviceTemperatureRedAlarmSetThresholdGet(AtDevice self);
eAtRet Tha60070041DeviceTemperatureRebootEnableWhenRedAlarmSet(AtDevice self, eBool enable);
eBool Tha60070041DeviceTemperatureRebootEnableWhenRedAlarmGet(AtDevice self);
eAtRet Tha60070041DeviceTemperatureRedAlarmClearThresholdSet(AtDevice self, uint32 celsicusValue);
uint32 Tha60070041DeviceTemperatureRedAlarmClearThresholdGet(AtDevice self);
eAtRet Tha60070041DeviceTemperatureYellowAlarmSetThresholdSet(AtDevice self, uint32 celsicusValue);
uint32 Tha60070041DeviceTemperatureYellowAlarmSetThresholdGet(AtDevice self);
eAtRet Tha60070041DeviceTemperatureYellowAlarmClearThresholdSet(AtDevice self, uint32 celsicusValue);
uint32 Tha60070041DeviceTemperatureYellowAlarmClearThresholdGet(AtDevice self);

eAtRet Tha60070061SdhLineGreenLedStateSet(AtSdhLine self, eAtLedState ledState);
eAtRet Tha60070061SdhLineRedLedStateSet(AtSdhLine self, eAtLedState ledState);
eAtRet Tha60070061SdhLineRedLedStateSet(AtSdhLine self, eAtLedState ledState);
eAtLedState Tha60070061SdhLineRedLedStateGet(AtSdhLine self);
eBool Tha60070061SdhLineSfpIsExisted(AtSdhLine self);
eBool Tha60070061SdhLineSfpIsExisted(AtSdhLine self);
eAtLedState Tha60070061SdhLineGreenLedStateGet(AtSdhLine self);

eAtLedState Tha60070061DeviceGreenLedActivationGet(AtDevice self);
eAtLedState Tha60070061DeviceRedLedActivationGet(AtDevice self);
eAtRet Tha60070061DeviceGreenLedStateSet(AtDevice self, eAtLedState ledState);
eAtRet Tha60070061DeviceRedLedStateSet(AtDevice self, eAtLedState ledState);
eAtRet Tha60070061DeviceGreenLedActivationSet(AtDevice self, eAtLedState ledState);
eAtRet Tha60070061DeviceRedLedActivationSet(AtDevice self, eAtLedState ledState);
eAtLedState Tha60070061DeviceGreenLedStateGet(AtDevice self);
eAtLedState Tha60070061DeviceRedLedStateGet(AtDevice self);
eAtLedState Tha60070061DeviceGreenLedActivationGet(AtDevice self);
eAtLedState Tha60070061DeviceRedLedActivationGet(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60070041DEVICE_H_ */


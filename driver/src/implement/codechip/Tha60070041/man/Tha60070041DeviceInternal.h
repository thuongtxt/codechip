/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60070041DeviceInternal.h
 * 
 * Created Date: May 14, 2015
 *
 * Description : Product 60070041
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60070041DEVICEINTERNAL_H_
#define _THA60070041DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaPdhPwProduct/man/ThaPdhPwProductDeviceInternal.h"
#include "Tha60070041Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60070041Device * Tha60070041Device;

typedef struct tTha60070041DeviceMethods
    {
    eBool (*NeedToCheckLiuWhenInit)(AtDevice self);
    }tTha60070041DeviceMethods;

typedef struct tTha60070041Device
    {
    tThaPdhPwProductDevice super;
    const tTha60070041DeviceMethods * methods;
    }tTha60070041Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60070041DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60070041DEVICEINTERNAL_H_ */


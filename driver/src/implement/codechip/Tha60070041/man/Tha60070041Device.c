/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60070041Device.c
 *
 * Created Date: Feb 24, 2014
 *
 * Description : Product 60070041 (32 E1 CES)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60070041DeviceInternal.h"
#include "commacro.h"
#include "Tha60070041Device.h"
#include "../prbs/Tha60070041ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60070041Device)self)

/* ------------ ADC code of temperature --------------*/
#define cThaRegCurrentTemperature  0x810000

#define cThaRegTemperatureAdcCodeMask  cBit15_4
#define cThaRegTemperatureAdcCodeShift 4

/* ------------- Alarm of temperature ----------------*/
#define cThaRegTemperatureAlarm  0x81003F

#define cThaRegTemperatureAlarmRedMask  cBit3
#define cThaRegTemperatureAlarmRedShift 3

#define cThaRegTemperatureAlarmYelMask  cBit0
#define cThaRegTemperatureAlarmYelShift 0

/* ----------------- Red Alarm SET threshold --------------------*/
#define cThaRegTemperatureRedAlarmSetThreshold         0x810053

#define cThaRegTemperatureRedAlarmSetThresholdMask     cBit15_4
#define cThaRegTemperatureRedAlarmSetThresholdShift     4

/* = 3 is reboot when reaching OT alarm, = 0 is otherwise */
#define cThaRegTemperatureRedAlarmSetThresholdRebootEnMask     cBit3_0
#define cThaRegTemperatureRedAlarmSetThresholdRebootEnShift     0

/* ----------------- Red Alarm CLEAR threshold --------------------*/
#define cThaRegTemperatureRedAlarmClearThreshold         0x810057

#define cThaRegTemperatureRedAlarmClearThresholdMask     cBit15_4
#define cThaRegTemperatureRedAlarmClearThresholdShift    4

/* ----------------- Yellow Alarm SET threshold --------------------*/
#define cThaRegTemperatureYellowAlarmSetThreshold        0x810050

#define cThaRegTemperatureYellowAlarmSetThresholdMask     cBit15_4
#define cThaRegTemperatureYellowAlarmSetThresholdShift    4

/* ----------------- Yellow Alarm CLEAR threshold --------------------*/
#define cThaRegTemperatureYellowAlarmClearThreshold      0x810054

#define cThaRegTemperatureYellowAlarmClearThresholdMask     cBit15_4
#define cThaRegTemperatureYellowAlarmClearThresholdShift    4

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60070041DeviceMethods m_methods;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule moduleId_ = moduleId;

    if (moduleId  == cAtModulePdh)     return (AtModule)Tha60070041ModulePdhNew(self);
    if (moduleId  == cAtModuleEth)     return (AtModule)Tha60070041ModuleEthNew(self);
    if (moduleId  == cAtModulePw)      return (AtModule)Tha60070041ModulePwNew(self);
    if (moduleId  == cAtModuleClock)   return (AtModule)Tha60070041ModuleClockNew(self);
    if (moduleId  == cAtModulePrbs)    return (AtModule)Tha60070041ModulePrbsNew(self);

    if (moduleId_ == cThaModuleMap)    return Tha60070041ModuleMapNew(self);
    if (moduleId_ == cThaModuleDemap)  return Tha60070041ModuleDemapNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    if (moduleId == cAtModulePrbs)
        return cAtTrue;
    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleRam,
                                                 cAtModuleEth,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleClock,
                                                 cAtModulePrbs};
    AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return Tha60070041SSKeyCheckerNew(self);
    }

static eBool LiuCanWork(AtDevice self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtHal liuHal = AtHalLiuHalGet(hal);
    uint32 cLiuPhaseMask  = cBit31;
    uint32 cLiuPhaseShift = 31;
    uint32 liuPhase, invertLiuPhase;
    uint32 regVal;

    if (AtHalRead(liuHal, 0x0) != 0xC0CAC0CA)
        return cAtTrue;

    regVal = AtHalRead(liuHal, 0xF00040);
    liuPhase = mRegField(regVal, cLiuPhase);
    invertLiuPhase = (liuPhase == 0) ? 1 : 0;
    mRegFieldSet(regVal, cLiuPhase, invertLiuPhase);
    AtHalWrite(liuHal, 0xF00040, regVal);

    return (AtHalRead(liuHal, 0x0) != 0xC0CAC0CA) ? cAtTrue : cAtFalse;
    }

static eBool NeedToCheckLiuWhenInit(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet Init(AtDevice self)
    {
    eBool needCheckLiu = mMethodsGet(mThis(self))->NeedToCheckLiuWhenInit(self);

    if (needCheckLiu && !LiuCanWork(self))
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "Liu can not work\r\n");
        return cAtErrorDevFail;
        }

    /* Super */
    return m_AtDeviceMethods->Init(self);
    }

static AtHal Hal(AtDevice self)
    {
    return AtDeviceIpCoreHalGet(self, 0);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070041Device);
    }

static void MethodsInit(Tha60070041Device self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, NeedToCheckLiuWhenInit);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        mMethodOverride(m_AtDeviceOverride, Init);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    }

AtDevice Tha60070041DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((Tha60070041Device)self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60070041DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60070041DeviceObjectInit(newDevice, driver, productCode);
    }

uint32 Tha60070041DeviceTemperatureAlarmGet(AtDevice self)
    {
    /* Just need to return HW mask, specific application layer will be convert it */
    return AtHalRead(Hal(self), cThaRegTemperatureAlarm);
    }

static uint32 AdcCodeToCelsiusValue(uint32 adcCode)
    {
    /* Temperature(oC) = (ADC_code * 503.975) / 4096 - 273.15 */
    return (((adcCode * 504) - (4096 * 273)) / 4096);
    }

uint32 Tha60070041DeviceTemperatureGet(AtDevice self)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegCurrentTemperature);
    uint32 adcCode = mRegField(regVal, cThaRegTemperatureAdcCode);

    return AdcCodeToCelsiusValue(adcCode);
    }

static uint32 CelsiusValueToAdcCode(uint32 celsiusValue)
    {
    /* ADC_code (hexa) = (4096 * (Temperature(oC) + 273)) / 504 */
    return (4096 * (celsiusValue + 273)) / 504;
    }

eAtRet Tha60070041DeviceTemperatureRedAlarmSetThresholdSet(AtDevice self, uint32 celsicusValue)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureRedAlarmSetThreshold);
    mRegFieldSet(regVal, cThaRegTemperatureRedAlarmSetThreshold, CelsiusValueToAdcCode(celsicusValue));
    AtHalWrite(Hal(self), cThaRegTemperatureRedAlarmSetThreshold, regVal);

    return cAtOk;
    }

uint32 Tha60070041DeviceTemperatureRedAlarmSetThresholdGet(AtDevice self)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureRedAlarmSetThreshold);
    uint32 adcCode = mRegField(regVal, cThaRegTemperatureRedAlarmSetThreshold);
    return AdcCodeToCelsiusValue(adcCode);
    }

eAtRet Tha60070041DeviceTemperatureRebootEnableWhenRedAlarmSet(AtDevice self, eBool enable)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureRedAlarmSetThreshold);
    mRegFieldSet(regVal, cThaRegTemperatureRedAlarmSetThresholdRebootEn, (enable) ? 0x3 : 0x0);
    AtHalWrite(Hal(self), cThaRegTemperatureRedAlarmSetThreshold, regVal);

    return cAtOk;
    }

eBool Tha60070041DeviceTemperatureRebootEnableWhenRedAlarmGet(AtDevice self)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureRedAlarmSetThreshold);
    uint32 adcCode = mRegField(regVal, cThaRegTemperatureRedAlarmSetThresholdRebootEn);

    return (adcCode == 3) ? cAtTrue : cAtFalse;
    }

eAtRet Tha60070041DeviceTemperatureRedAlarmClearThresholdSet(AtDevice self, uint32 celsicusValue)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureRedAlarmClearThreshold);
    mRegFieldSet(regVal, cThaRegTemperatureRedAlarmClearThreshold, CelsiusValueToAdcCode(celsicusValue));
    AtHalWrite(Hal(self), cThaRegTemperatureRedAlarmClearThreshold, regVal);

    return cAtOk;
    }

uint32 Tha60070041DeviceTemperatureRedAlarmClearThresholdGet(AtDevice self)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureRedAlarmClearThreshold);
    uint32 adcCode = mRegField(regVal, cThaRegTemperatureRedAlarmClearThreshold);
    return AdcCodeToCelsiusValue(adcCode);
    }

eAtRet Tha60070041DeviceTemperatureYellowAlarmSetThresholdSet(AtDevice self, uint32 celsicusValue)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureYellowAlarmSetThreshold);
    mRegFieldSet(regVal, cThaRegTemperatureYellowAlarmSetThreshold, CelsiusValueToAdcCode(celsicusValue));
    AtHalWrite(Hal(self), cThaRegTemperatureYellowAlarmSetThreshold, regVal);

    return cAtOk;
    }

uint32 Tha60070041DeviceTemperatureYellowAlarmSetThresholdGet(AtDevice self)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureYellowAlarmSetThreshold);
    uint32 adcCode = mRegField(regVal, cThaRegTemperatureYellowAlarmSetThreshold);
    return AdcCodeToCelsiusValue(adcCode);
    }

eAtRet Tha60070041DeviceTemperatureYellowAlarmClearThresholdSet(AtDevice self, uint32 celsicusValue)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureYellowAlarmClearThreshold);
    mRegFieldSet(regVal, cThaRegTemperatureYellowAlarmClearThreshold, CelsiusValueToAdcCode(celsicusValue));
    AtHalWrite(Hal(self), cThaRegTemperatureYellowAlarmClearThreshold, regVal);

    return cAtOk;
    }

uint32 Tha60070041DeviceTemperatureYellowAlarmClearThresholdGet(AtDevice self)
    {
    uint32 regVal = AtHalRead(Hal(self), cThaRegTemperatureYellowAlarmClearThreshold);
    uint32 adcCode = mRegField(regVal, cThaRegTemperatureYellowAlarmClearThreshold);
    return AdcCodeToCelsiusValue(adcCode);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock Module
 *
 * File        : Tha60070041ModuleClock.c
 *
 * Created Date: Feb 4, 2015
 *
 * Description : Clock module implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaPdhPwProduct/clock/ThaPdhPwProductModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070041ModuleClock
    {
    tThaPdhPwProductModuleClock super;
    }tTha60070041ModuleClock;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleClockMethods        m_AtModuleClockOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumExtractors(AtModuleClock self)
    {
	AtUnused(self);
    return 2;
    }

static void OverrideAtModuleClock(AtModuleClock self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleClockOverride, mMethodsGet(self), sizeof(m_AtModuleClockOverride));

        mMethodOverride(m_AtModuleClockOverride, NumExtractors);
        }

    mMethodsSet(self, &m_AtModuleClockOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideAtModuleClock(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070041ModuleClock);
    }

static AtModuleClock ObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModuleClockObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleClock Tha60070041ModuleClockNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleClock newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : physical
 *
 * File        : Tha60200011SdhLineSerdesController.c
 *
 * Created Date: Jan 20, 2015
 *
 * Description : SDH Line SERDES controller for product 60200011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/physical/ThaStmPwProductSdhLineSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegTimingModeCtrl               0xf00040

#define cRxLockToDataModeEnMask          cBit15_12
#define cRxLockToDataModeEnShift         12

#define cRxLockToRefClkModeEnMask        cBit11_8
#define cRxLockToRefClkModeEnShift       8

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60200011SdhLineSerdesController
    {
    tThaStmPwProductSdhLineSerdesController super;
    }tTha60200011SdhLineSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods         m_AtSerdesControllerOverride;
static tThaSdhLineSerdesControllerMethods m_ThaSdhLineSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods  *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
	AtUnused(enable);
	AtUnused(loopbackMode);
	AtUnused(self);
    return cAtFalse;
    }

static eBool CanControlTimingMode(AtSerdesController self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 LockToDataRegAddress(ThaSdhLineSerdesController self)
    {
	AtUnused(self);
    return cRegTimingModeCtrl;
    }

static uint32 LocalLineId(ThaSerdesController self)
    {
    AtSdhLine line = (AtSdhLine)AtSerdesControllerPhysicalPortGet((AtSerdesController)self);
    return ThaModuleSdhLineLocalId(line);
    }

static uint32 LockToDataRegMask(ThaSdhLineSerdesController self)
    {
    return cBit12 << LocalLineId((ThaSerdesController)self);
    }

static uint32 LockToDataRegShift(ThaSdhLineSerdesController self)
    {
    return 12 + LocalLineId((ThaSerdesController)self);
    }

static uint32 LockToRefRegAddress(ThaSdhLineSerdesController self)
    {
	AtUnused(self);
    return cRegTimingModeCtrl;
    }

static uint32 LockToRefRegMask(ThaSdhLineSerdesController self)
    {
    return cBit8 << LocalLineId((ThaSerdesController)self);
    }

static uint32 LockToRefRegShift(ThaSdhLineSerdesController self)
    {
    return 8 + LocalLineId((ThaSerdesController)self);
    }

static void OverrideThaSdhLineSerdesController(AtSerdesController self)
    {
    ThaSdhLineSerdesController sdhLineSerdes = (ThaSdhLineSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineSerdesControllerOverride, mMethodsGet(sdhLineSerdes), sizeof(m_ThaSdhLineSerdesControllerOverride));

        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegAddress);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegMask);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegShift);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegAddress);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegMask);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegShift);
        }

    mMethodsSet(sdhLineSerdes, &m_ThaSdhLineSerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, CanControlTimingMode);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideThaSdhLineSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60200011SdhLineSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductSdhLineSerdesControllerObjectInit(self, sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60200011SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newController, sdhLine, serdesId);
    }

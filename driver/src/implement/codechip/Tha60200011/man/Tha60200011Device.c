/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device
 *
 * File        : Tha60200011Device.c
 *
 * Created Date: Jan 12, 2015
 *
 * Description : Device for product 60200011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/aps/ThaModuleHardAps.h"
#include "../../../default/pw/ThaModulePw.h"
#include "../../default/ThaStmPwProduct/man/ThaStmPwProductDevice.h"
#include "AtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60200011Device
    {
    tThaStmPwProductDevice super;
    }tTha60200011Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    if (moduleId  == cAtModuleSdh) return (AtModule)Tha60200011ModuleSdhNew(self);
    if (moduleId  == cAtModulePw)  return (AtModule)Tha60200011ModulePwNew(self);
    if (moduleId  == cAtModuleEth) return (AtModule)Tha60200011ModuleEthNew(self);
    if (moduleId  == cAtModuleRam) return (AtModule)Tha60200011ModuleRamNew(self);
    if (moduleId  == cAtModuleAps) return (AtModule)ThaModuleHardApsNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static uint8 MaxNumParts(ThaDevice self)
    {
	AtUnused(self);
    return 1;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleValue = moduleId;
    switch (moduleValue)
        {
        case cAtModuleAps: return cAtTrue;
        case cAtModuleBer: return cAtFalse;
        default:
            return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
        }
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleSdh,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleClock,
                                                 cAtModuleAps};
	AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return Tha60200011SSKeyCheckerNew(self);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, MaxNumParts);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60200011Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60200011DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60200011ModuleSdh.c
 *
 * Created Date: Jan 12, 2015
 *
 * Description : Module SDH of 60200011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/sdh/ThaStmPwProductModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60200011ModuleSdh
    {
    tThaStmPwProductModuleSdh super;
    }tTha60200011ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods  m_AtModuleSdhOverride;
static tThaModuleSdhMethods m_ThaModuleSdhOverride;

/* Save super implementation */
static const tAtModuleSdhMethods  *m_AtModuleSdhMethods  = NULL;
static const tThaModuleSdhMethods *m_ThaModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60200011ModuleSdh);
    }

static uint8 MaxLinesGet(AtModuleSdh self)
    {
	AtUnused(self);
    return 4;
    }

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
	AtUnused(self);
    return Tha60200011SdhLineSerdesControllerNew(line, serdesId);
    }

static AtSdhChannel ChannelCreateWithVersion(ThaModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId, uint8 version)
    {
    if (channelType == cAtSdhChannelTypeLine)
        return (AtSdhChannel)ThaSdhLineNew(channelId, (AtModuleSdh)self, version);

    /* Ask super for other channel types */
    return m_ThaModuleSdhMethods->ChannelCreateWithVersion(self, lineId, parent, channelType, channelId, version);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, MaxLinesGet);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, ChannelCreateWithVersion);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60200011ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

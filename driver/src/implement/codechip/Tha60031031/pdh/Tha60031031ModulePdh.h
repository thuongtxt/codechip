/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60031031ModulePdh.h
 * 
 * Created Date: Jul 25, 2013
 *
 * Description : PDH module of product 60031031
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031031MODULEPDH_H_
#define _THA60031031MODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/pdh/ThaStmPwProductModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031031ModulePdh
    {
    tThaStmPwProductModulePdh super;
    }tTha60031031ModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh Tha60031031ModulePdhObjectInit(AtModulePdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031031MODULEPDH_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60031031ModuleCla.h
 * 
 * Created Date: Jul 8, 2013
 *
 * Description : CLA module of product 60031031
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031031MODULECLA_H_
#define _THA60031031MODULECLA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/cla/ThaStmPwProductModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031031ModuleCla
    {
    tThaStmPwProductModuleCla super;
    }tTha60031031ModuleCla;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60031031ModuleClaObjectInit(AtModule self, AtDevice device);

eThaPwLookupMode Tha60031031CommonDefaultPwLookupModeGet(ThaStmPwProductModuleCla self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031031MODULECLA_H_ */


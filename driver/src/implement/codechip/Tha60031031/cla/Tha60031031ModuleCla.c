/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60031031ModuleCla.c
 *
 * Created Date: Jun 20, 2013
 *
 * Description : CLA module of product 60031031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60031031ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaStmPwProductModuleClaMethods   m_ThaStmPwProductModuleClaOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eThaPwLookupMode DefaultPwLookupModeGet(ThaStmPwProductModuleCla self)
    {
    AtUnused(self);
    return cThaPwLookupMode2Vlans;
    }

static void OverrideThaStmPwProductModuleCla(AtModule self)
    {
    ThaStmPwProductModuleCla claModule = (ThaStmPwProductModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmPwProductModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaStmPwProductModuleClaOverride));

        mMethodOverride(m_ThaStmPwProductModuleClaOverride, DefaultPwLookupModeGet);
        }

    mMethodsSet(claModule, &m_ThaStmPwProductModuleClaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaStmPwProductModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031031ModuleCla);
    }

AtModule Tha60031031ModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60031031ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60031031ModuleClaObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60031031Device.c
 *
 * Created Date: Feb 10, 2015
 *
 * Description : Product 60031031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60031031Device.h"
#include "../../../default/aps/ThaModuleSoftAps.h"
#include "../../../default/ber/ThaModuleBer.h"
#include "../../../default/sdh/ThaModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    if (moduleId == cAtModuleBer)
        return cAtTrue;
    if (moduleId == cAtModulePrbs)
        return ThaDeviceIsEp((ThaDevice)self);

    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    uint32 phyModule = moduleId;

    if (moduleId  == cAtModuleBer)
        return (AtModule)ThaStmModuleBerNew(self, 800);
    if (moduleId  == cAtModuleSdh)
        return (AtModule)Tha60031031ModuleSdhNew(self);
    if (moduleId  == cAtModulePdh)
        return (AtModule)Tha60031031ModulePdhNew(self);

    if (phyModule == cThaModuleCdr)
        return Tha60031031ModuleCdrNew(self);
    if (phyModule == cThaModuleCla)
        return Tha60031031ModuleClaNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleSdh,
                                                 cAtModuleBer,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleClock};
    AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031031Device);
    }

AtDevice Tha60031031DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60031031DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60031031DeviceObjectInit(newDevice, driver, productCode);
    }

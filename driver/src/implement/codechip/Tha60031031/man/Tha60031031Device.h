/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60031031Device.h
 * 
 * Created Date: Jun 24, 2013
 *
 * Description : Product 60030131 (STM SAToP/CESoP)
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031031DEVICE_H_
#define _THA60031031DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/man/ThaStmPwProductDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031031Device
    {
    tThaStmPwProductDevice super;
    }tTha60031031Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60031031DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031031DEVICE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60031031ModuleSdhInternal.h
 * 
 * Created Date: Mar 7, 2015
 *
 * Description : SDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031031MODULESDHINTERNAL_H_
#define _THA60031031MODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/sdh/ThaStmPwProductModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031031ModuleSdh
    {
    tThaStmPwProductModuleSdh super;
    }tTha60031031ModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh Tha60031031ModuleSdhObjectInit(AtModuleSdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031031MODULESDHINTERNAL_H_ */


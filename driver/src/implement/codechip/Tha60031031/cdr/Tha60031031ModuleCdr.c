/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60031031ModuleCdr.c
 *
 * Created Date: Apr 6, 2015
 *
 * Description : CDR module of product 60031031
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/cdr/ThaStmPwProductModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegCdrDcrControl                0x2a0001
#define cThaRegCdrDcrTxDcrEngineEnableMask  cBit0
#define cThaRegCdrDcrTxDcrEngineEnableShift 0
#define cThaRegCdrDcrTxDcrEngineEnableIndex 2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031031ModuleCdr
    {
    tThaStmPwProductModuleCdr super;
    }tTha60031031ModuleCdr;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleCdrMethods    m_ThaModuleCdrOverride;
static tThaModuleCdrStmMethods m_ThaModuleCdrStmOverride;

/* Save super implementation */
static const tThaModuleCdrStmMethods *m_ThaModuleCdrStmMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 HwSystemTimingMode(ThaModuleCdr self)
    {
    AtUnused(self);
    return 6;
    }

static eAtTimingMode OtherTimingSourceAsSystemTiming(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtTimingModeExt1Ref;
    }

static eAtRet PartDefaultSet(ThaModuleCdr self, uint8 partId)
    {
    uint32 partOffset;
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];
    eAtRet ret;
    AtIpCore core;

    ret = m_ThaModuleCdrStmMethods->PartDefaultSet(self, partId);
    if (ret != cAtOk)
        return ret;

    core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), AtModuleDefaultCoreGet((AtModule)self));
    partOffset = ThaModuleCdrPartOffset(self, partId);

    /* Disable Tx DCR engine in default due to this engine is not applicable
     * for slave timing mode */
    regAddr = cThaRegCdrDcrControl + partOffset;
    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[cThaRegCdrDcrTxDcrEngineEnableIndex], cThaRegCdrDcrTxDcrEngineEnable, 1);
    mModuleHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static void OverrideThaModuleCdrStm(AtModule self)
    {
    ThaModuleCdrStm module = (ThaModuleCdrStm)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrStmMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrStmOverride, m_ThaModuleCdrStmMethods, sizeof(m_ThaModuleCdrStmOverride));

        mMethodOverride(m_ThaModuleCdrStmOverride, PartDefaultSet);
        }

    mMethodsSet(module, &m_ThaModuleCdrStmOverride);
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr module = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, mMethodsGet(module), sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, HwSystemTimingMode);
        mMethodOverride(m_ThaModuleCdrOverride, OtherTimingSourceAsSystemTiming);
        }

    mMethodsSet(module, &m_ThaModuleCdrOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCdr(self);
    OverrideThaModuleCdrStm(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031031ModuleCdr);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60031031ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60031031ModuleEthInternal.h
 * 
 * Created Date: Mar 1, 2015
 *
 * Description : ETH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031031MODULEETHINTERNAL_H_
#define _THA60031031MODULEETHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/eth/ThaModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031031ModuleEth
    {
    tThaModuleEthPwV2 super;
    }tTha60031031ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha60031031ModuleEthObjectInit(AtModuleEth self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031031MODULEETHINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : Tha60210012Vcg.h
 * 
 * Created Date: Sep 16, 2015
 *
 * Description : VCG of Tha60210012 product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012VCG_H_
#define _THA60210012VCG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtConcateGroup.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtVcg Tha60210012VcgVcatNew(AtModuleConcate concateModule, uint32 vcatId, eAtConcateMemberType memberType);
AtConcateGroup Tha60210012VcgNonVcatNew(AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType);

eBool Tha60210012ConcateGroupCanBeCreated(AtModuleConcate self, uint32 vcgId, eAtConcateGroupType concateType);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012VCG_H_ */


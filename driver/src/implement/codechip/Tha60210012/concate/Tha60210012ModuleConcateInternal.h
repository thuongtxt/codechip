/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : Tha60210012ModuleConcateInternal.h
 * 
 * Created Date: Sep 14, 2015
 *
 * Description : Concatenation module of Tha60210012
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULECONCATEINTERNAL_H_
#define _THA60210012MODULECONCATEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/concate/AtModuleConcateInternal.h"
#include "Tha60210012ModuleConcate.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleConcateMethods
    {
    uint32 (*BaseAddress)(Tha60210012ModuleConcate self);

    /* Flexible method */
    void (*HwXcReset)(Tha60210012ModuleConcate self);
    void (*HwE1UnframeReset)(Tha60210012ModuleConcate self);
    void (*HwMuxReset)(Tha60210012ModuleConcate self);
    void (*PrbsEngineDisable)(Tha60210012ModuleConcate self);
    void (*Activate)(Tha60210012ModuleConcate self, eBool enable);
    uint8 (*HwStsSlice24ToMapStsId)(Tha60210012ModuleConcate self,  uint8 hwPdhSliceId, uint8 hwPdhStsId);
    void  (*HwMapStsIdSliceSts24Get)(Tha60210012ModuleConcate self,  uint8 mapStsId, uint8 *hwPdhSliceId, uint8 *hwPdhStsId);

    }tTha60210012ModuleConcateMethods;

typedef struct tTha60210012ModuleConcate
    {
    tAtModuleConcate super;
    tTha60210012ModuleConcateMethods *methods;

    /* Async init */
    uint32 asyncInitState;
    }tTha60210012ModuleConcate;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleConcate Tha60210012ModuleConcateObjectInit(AtModuleConcate self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULECONCATEINTERNAL_H_ */


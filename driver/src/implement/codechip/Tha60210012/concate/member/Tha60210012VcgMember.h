/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60210012VcgMember.h
 *
 * Created Date: Sep 10, 2015
 *
 * Description : Vcg member
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
#ifndef _THA60210012VCGMEMBER_H_
#define _THA60210012VCGMEMBER_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/concate/member/ThaVcgMember.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012VcgMember * Tha60210012VcgMember;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete members */
AtConcateMember Tha60210012VcgMemberPdhDe3New(AtConcateGroup vcg, AtChannel channel);
AtConcateMember Tha60210012VcgMemberPdhDe1New(AtConcateGroup vcg, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012VCGMEMBER_H_ */


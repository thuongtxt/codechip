/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Tha60210012Concate
 *
 * File        : Tha60210012VcgMemberPdhDe1.c
 *
 * Created Date: Mar 11, 2016
 *
 * Description : PDH DE1 - VCG member of 60210012 product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../concate/Tha60210012ModuleConcate.h"
#include "Tha60210012VcgMemberInternal.h"
#include "Tha60210012VcgMember.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaVcgMemberMethods m_ThaVcgMemberOverride;
static tTha60210012VcgMemberMethods m_Tha60210012VcgMemberOverride;

/* Save super implementation */
static const tThaVcgMemberMethods *m_ThaVcgMemberMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPdhDe1 De1Get(ThaVcgMember self)
    {
    return (ThaPdhDe1)AtConcateMemberChannelGet((AtConcateMember)self);
    }

static Tha60210012ModuleConcate ModuleConcate(ThaPdhDe1 de1)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel) de1);
    return (Tha60210012ModuleConcate)AtDeviceModuleGet(device, cAtModuleConcate);
    }

static uint32 DefaultOffset(ThaVcgMember self)
    {
    uint8 hwSlice, hwSts, hwVtg, hwVt, mapSts;
    ThaPdhDe1 de1 = De1Get(self);
    ThaPdhDe1HwIdGet(de1, cThaModuleMap, &hwSlice, &hwSts,  &hwVtg, &hwVt);
    mapSts = Tha60210012ModuleConcateHwStsSlice24ToMapStsId(ModuleConcate(de1), hwSlice, hwSts);
    return (mapSts * 32UL + hwVtg * 4UL + hwVt);
    }

static uint8 StsId(ThaVcgMember self)
    {
    uint8 hwSlice, hwSts, hwVtg, hwVt;
    ThaPdhDe1 de1 = De1Get(self);
    ThaPdhDe1HwIdGet(de1, cThaModuleMap, &hwSlice, &hwSts,  &hwVtg, &hwVt);
    return Tha60210012ModuleConcateHwStsSlice24ToMapStsId(ModuleConcate(de1), hwSlice, hwSts);
    }

static uint8 VtgId(ThaVcgMember self)
    {
    uint8 hwSlice, hwSts, hwVtg, hwVt;
    ThaPdhDe1HwIdGet(De1Get(self), cThaModuleMap, &hwSlice, &hwSts,  &hwVtg, &hwVt);
    return hwVtg;
    }

static uint8 LoMode(ThaVcgMember self)
    {
    AtPdhChannel de1 = (AtPdhChannel)AtConcateMemberChannelGet((AtConcateMember)self);
    return AtPdhDe1IsE1((AtPdhDe1)de1) ? 0x3 : 0x2;
    }

static AtPdhChannel PdhChannel(ThaVcgMember self)
    {
    return (AtPdhChannel)AtConcateMemberChannelGet((AtConcateMember)self);
    }

static void HwIdGet(ThaVcgMember self, uint8 *slice24, uint8* sts24)
    {
    ThaPdhChannelHwIdGet(PdhChannel(self), cAtModulePdh, slice24, sts24);
    }

static void OverrideTha60210012VcgMember(AtConcateMember self)
    {
    Tha60210012VcgMember tha60210012VcgMember = (Tha60210012VcgMember)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012VcgMemberOverride, mMethodsGet(tha60210012VcgMember), sizeof(m_Tha60210012VcgMemberOverride));
        mMethodOverride(m_Tha60210012VcgMemberOverride, HwIdGet);
        }

    mMethodsSet(tha60210012VcgMember, &m_Tha60210012VcgMemberOverride);
    }

static void OverrideThaVcgMember(AtConcateMember self)
    {
    ThaVcgMember vcgMember = (ThaVcgMember)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaVcgMemberMethods = mMethodsGet(vcgMember);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgMemberOverride, m_ThaVcgMemberMethods, sizeof(m_ThaVcgMemberOverride));

        mMethodOverride(m_ThaVcgMemberOverride, DefaultOffset);
        mMethodOverride(m_ThaVcgMemberOverride, StsId);
        mMethodOverride(m_ThaVcgMemberOverride, VtgId);
        mMethodOverride(m_ThaVcgMemberOverride, LoMode);
        }

    mMethodsSet(vcgMember, &m_ThaVcgMemberOverride);
    }

static void Override(AtConcateMember self)
    {
    OverrideThaVcgMember(self);
    OverrideTha60210012VcgMember(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012VcgMemberPdhDe1);
    }

AtConcateMember Tha60210012VcgMemberPdhDe1ObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012VcgMemberObjectInit(self, vcg, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtConcateMember Tha60210012VcgMemberPdhDe1New(AtConcateGroup vcg, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtConcateMember newMember = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newMember == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012VcgMemberPdhDe1ObjectInit(newMember, vcg, channel);
    }

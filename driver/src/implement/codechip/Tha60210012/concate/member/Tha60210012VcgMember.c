/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60210012VcgMember.c
 *
 * Created Date: MAR 11, 2016
 *
 * Description : Vcg member
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../concate/Tha60210012ModuleConcateLoVcatReg.h"
#include "../../concate/Tha60210012ModuleConcate.h"
#include "Tha60210012VcgMemberInternal.h"
#include "Tha60210012VcgMember.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60210012VcgMember)self)
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012VcgMemberMethods m_methods;

/* Override */
static tAtChannelMethods                m_AtChannelOverride;
static tAtConcateMemberMethods          m_AtConcateMemberOverride;
static tThaVcgMemberMethods             m_ThaVcgMemberOverride;

/* Save super implementation */
static const tAtChannelMethods      *m_AtChannelMethods    = NULL;
static const tThaVcgMemberMethods   *m_ThaVcgMemberMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaVcgMember self)
    {
    AtDevice device = AtChannelDeviceGet(AtConcateMemberChannelGet((AtConcateMember)self));
    Tha60210012ModuleConcate concateModule = (Tha60210012ModuleConcate)AtDeviceModuleGet(device, cAtModuleConcate);
    return Tha60210012ModuleConcateBaseAddress(concateModule);
    }

static uint32 Offset(ThaVcgMember self)
    {
    return mMethodsGet(self)->DefaultOffset(self) + BaseAddress(self);
    }

static eAtLcasMemberSourceState SourceStateHw2Sw(uint32 hwState)
    {
    switch (hwState)
        {
        case 0: return cAtLcasMemberSourceStateIdle;
        case 1: return cAtLcasMemberSourceStateAdd;
        case 2: return cAtLcasMemberSourceStateRmv;
        case 3: return cAtLcasMemberSourceStateEos;
        case 4: return cAtLcasMemberSourceStateDnu;
        case 7: return cAtLcasMemberSourceStateFixed;
        default:return cAtLcasMemberSourceStateInvl;
        }
    }

static eAtLcasMemberSourceState SourceStateGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_vcat5g_so_memsta_pen_Base + Offset((ThaVcgMember)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);

    return SourceStateHw2Sw(mRegField(regVal, cAf6_vcat5g_so_memsta_pen_So_Mem_Fsm_));
    }

static eAtLcasMemberSinkState SinkStateHw2Sw(uint32 hwState)
    {
    switch (hwState)
        {
        case 0: return cAtLcasMemberSinkStateIdle;
        case 1: return cAtLcasMemberSinkStateFail;
        case 2: return cAtLcasMemberSinkStateOk;
        case 3: return cAtLcasMemberSinkStateFixed;
        default:return cAtLcasMemberSinkStateInvl;
        }
    }

static eAtLcasMemberSinkState SinkStateGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_vcat5g_sk_memsta_pen_Base + Offset((ThaVcgMember)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);

    return SinkStateHw2Sw(mRegField(regVal, cAf6_vcat5g_sk_memsta_pen_Sk_Mem_Fsm_));
    }

static eAtModuleConcateRet SinkExpectedSequenceSet(AtConcateMember self, uint32 sinkSequence)
    {
    uint32 regAddr = cAf6Reg_vcat5g_sk_expseq_pen_Base + Offset((ThaVcgMember)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_vcat5g_sk_expseq_pen_Sk_Mem_ExpSeq_, sinkSequence);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);
    return cAtOk;
    }

static uint32 SinkExpectedSequenceGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_vcat5g_sk_expseq_pen_Base + Offset((ThaVcgMember)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_vcat5g_sk_expseq_pen_Sk_Mem_ExpSeq_);
    }

static eAtModuleConcateRet SourceSequenceSet(AtConcateMember self, uint32 sourceSequence)
    {
    uint32 regAddr = cAf6Reg_vcat5g_so_txseqmem_pen_Base + Offset((ThaVcgMember)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_vcat5g_so_txseqmem_pen_So_TxSeq_, sourceSequence);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);
    
    return cAtOk;
    }

static uint32 SourceSequenceGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_vcat5g_so_txseqmem_pen_Base + Offset((ThaVcgMember)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_vcat5g_so_txseqmem_pen_So_TxSeq_);
    }

static eAtLcasMemberCtrl SourceControlGet(AtConcateMember self)
    {
    /* TODO: Implement me */
    AtUnused(self);
    return cAtLcasMemberCtrlFixed;
    }

static uint32 SinkSequenceGet(AtConcateMember self)
    {
    uint32 regAddr = cAf6Reg_vcat5g_sk_memsta_pen_Base + Offset((ThaVcgMember)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    return mRegField(regVal, cAf6_vcat5g_sk_memsta_pen_Sk_Mem_RecSeq_);
    }

static eAtLcasMemberCtrl SinkControlGet(AtConcateMember self)
    {
    /* TODO: Implement me */
    AtUnused(self);
    return cAtLcasMemberCtrlFixed;
    }

static uint32 SourceMstGet(AtConcateMember self)
    {
    /* TODO: Implement me */
    AtUnused(self);
    return 0;
    }

static uint32 SinkMstGet(AtConcateMember self)
    {
    /* TODO: Implement me */
    AtUnused(self);
    return 0;
    }

static eAtRet SoMemSizeInit(ThaVcgMember self)
    {
    uint32 regAddr = cAf6Reg_vcat5g_so_ctrl_pen((uint32)mMethodsGet(self)->StsId(self)) + BaseAddress(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    uint8 vtgId    = mMethodsGet(self)->VtgId(self);
    mRegFieldSet(regVal, cAf6_vcat5g_so_ctrl_pen_So_HoMd_, mMethodsGet(self)->HoMode(self));
    mFieldIns(&regVal, cAf6_vcat5g_so_ctrl_pen_So_LoMd_Mask(vtgId), cAf6_vcat5g_so_ctrl_pen_So_LoMd_Shift(vtgId), mMethodsGet(self)->LoMode(self));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);
    return cAtOk;
    }

static eAtRet SkMemSizeInit(ThaVcgMember self)
    {
    uint8 slice, sts;
    uint32 regVal, regAddr;
    uint8 vtgId = mMethodsGet(self)->VtgId(self);
    mMethodsGet(mThis(self))->HwIdGet(self, &slice, &sts);
    regAddr = cAf6Reg_vcat5g_sk_ctrl_pen((uint32)slice, (uint32)sts) + BaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_HoMd_, mMethodsGet(self)->HoMode(self));
    mFieldIns(&regVal, cAf6_vcat5g_so_ctrl_pen_Sk_LoMd_Mask(vtgId), cAf6_vcat5g_so_ctrl_pen_Sk_LoMd_Shift(vtgId), mMethodsGet(self)->LoMode(self));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);
    return cAtOk;
    }

static eAtRet SourceAdd(ThaVcgMember self, AtConcateGroup vcg)
    {
    uint32 regAddr = cAf6Reg_vcat5g_so_memctrl_pen_Base + Offset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    AtUnused(vcg);
    mRegFieldSet(regVal, cAf6_vcat5g_so_memctrl_pen_So_Nms_Cmd_, 1);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);
    return cAtOk;
    }

static eAtRet SinkAdd(ThaVcgMember self, AtConcateGroup vcg, eBool lcasEnable)
    {
    uint32 regAddr = cAf6Reg_vcat5g_sk_memctrl_pen_Base + Offset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    AtUnused(vcg);
    mRegFieldSet(regVal, cAf6_vcat5g_sk_memctrl_pen_Sk_Nms_Cmd_, lcasEnable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);
    return cAtOk;
    }

static eAtRet SinkRemove(ThaVcgMember self)
    {
    uint32 regAddr = cAf6Reg_vcat5g_sk_memctrl_pen_Base + Offset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_vcat5g_sk_memctrl_pen_Sk_Nms_Cmd_, 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);
    return cAtOk;
    }

static eAtRet SourceRemove(ThaVcgMember self)
    {
    uint32 regAddr = cAf6Reg_vcat5g_so_memctrl_pen_Base + Offset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_vcat5g_so_memctrl_pen_So_Nms_Cmd_, 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);
    return cAtOk;
    }

static void HwIdGet(ThaVcgMember self, uint8 *slice24, uint8* sts24)
    {
    AtUnused(self);
    AtUnused(slice24);
    AtUnused(sts24);
    }

static uint32 AlarmHw2Sw(uint32 hwAlarm)
    {
    uint32 alarm = 0;

    if (hwAlarm & cAf6_vcat5g_sk_intrsta_pen_SQNC_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeSqnc;

    if (hwAlarm & cAf6_vcat5g_sk_intrsta_pen_MND_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeMnd;

    if (hwAlarm & cAf6_vcat5g_sk_intrsta_pen_LOA_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeLoa;

    if (hwAlarm & cAf6_vcat5g_sk_intrsta_pen_SQM_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeSqm;

    if (hwAlarm & cAf6_vcat5g_sk_intrsta_pen_OOM_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeOom;

    if (hwAlarm & cAf6_vcat5g_sk_intrsta_pen_LOM_Intr_CrrSta_Mask)
        alarm |= cAtConcateMemberAlarmTypeLom;

    return alarm;
    }

static uint32 AlarmGet(AtChannel self)
    {
    uint32 regAddr = cAf6Reg_vcat5g_sk_intrsta_pen_Base + Offset((ThaVcgMember)self);
    uint32 hwAlarm  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    return AlarmHw2Sw(hwAlarm);
    }

static uint32 AlarmHistoryRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 regAddr = cAf6Reg_vcat5g_sk_intrstk_pen_Base +  Offset((ThaVcgMember)self);
    uint32 hwAlarm  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    if (read2Clear)
        mChannelHwWrite(self, regAddr, hwAlarm, cAtModuleConcate);

    return AlarmHw2Sw(hwAlarm);
    }

static uint32 AlarmHistoryGet(AtChannel self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtChannel self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    uint32 supportedDefects = AtChannelSupportedInterruptMasks(self);
    return (alarmType & supportedDefects) ? cAtTrue : cAtFalse;
    }

static uint32 VcatInterruptMasks(AtChannel self)
    {
    static uint32 supportedMasks = (cAtConcateMemberAlarmTypeLoa|cAtConcateMemberAlarmTypeSqm|
                                    cAtConcateMemberAlarmTypeOom|cAtConcateMemberAlarmTypeLom);
    AtUnused(self);
    return supportedMasks;
    }

static uint32 LcasInterruptMasks(AtChannel self)
    {
    static uint32 supportedMasks = (cAtConcateMemberAlarmTypeSqnc|cAtConcateMemberAlarmTypeMnd|
                                    cAtConcateMemberAlarmTypeOom|cAtConcateMemberAlarmTypeLom);
    AtUnused(self);
    return supportedMasks;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtConcateGroup vcg = AtConcateMemberConcateGroupGet((AtConcateMember) self);

    if (AtConcateGroupIsLcasVcg(vcg))
        return LcasInterruptMasks(self);

    if (AtConcateGroupIsVcatVcg(vcg))
        return VcatInterruptMasks(self);

    return 0;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = cAf6Reg_vcat5g_sk_intrctrl_pen_Base + Offset((ThaVcgMember)self);
    uint32 hwMask  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    if (defectMask & cAtConcateMemberAlarmTypeSqnc)
        mRegFieldSet(hwMask, cAf6_vcat5g_sk_intrctrl_pen_SQNC_Intr_En_, (enableMask & cAtConcateMemberAlarmTypeSqnc) ? 1 : 0);

    if (defectMask & cAtConcateMemberAlarmTypeMnd)
        mRegFieldSet(hwMask, cAf6_vcat5g_sk_intrctrl_pen_MND_Intr_En_, (enableMask & cAtConcateMemberAlarmTypeMnd) ? 1 : 0);

    if (defectMask & cAtConcateMemberAlarmTypeLoa)
        mRegFieldSet(hwMask, cAf6_vcat5g_sk_intrctrl_pen_LOA_Intr_En_, (enableMask & cAtConcateMemberAlarmTypeLoa) ? 1 : 0);

    if (defectMask & cAtConcateMemberAlarmTypeSqm)
        mRegFieldSet(hwMask, cAf6_vcat5g_sk_intrctrl_pen_SQM_Intr_En_, (enableMask & cAtConcateMemberAlarmTypeSqm) ? 1 : 0);

    if (defectMask & cAtConcateMemberAlarmTypeOom)
        mRegFieldSet(hwMask, cAf6_vcat5g_sk_intrctrl_pen_OOM_Intr_En_, (enableMask & cAtConcateMemberAlarmTypeOom) ? 1 : 0);

    if (defectMask & cAtConcateMemberAlarmTypeLom)
        mRegFieldSet(hwMask, cAf6_vcat5g_sk_intrctrl_pen_LOM_Intr_En_, (enableMask & cAtConcateMemberAlarmTypeLom) ? 1 : 0);

    mChannelHwWrite(self, regAddr, hwMask, cAtModuleConcate);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regAddr = cAf6Reg_vcat5g_sk_intrctrl_pen_Base + Offset((ThaVcgMember) self);
    uint32 hwMask  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    return AlarmHw2Sw(hwMask);
    }

static void OverrideAtChannel(AtConcateMember self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        mMethodOverride(m_AtChannelOverride, AlarmGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaVcgMember(AtConcateMember self)
    {
    ThaVcgMember vcgMember = (ThaVcgMember) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaVcgMemberMethods = mMethodsGet(vcgMember);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgMemberOverride, m_ThaVcgMemberMethods, sizeof(m_ThaVcgMemberOverride));

        mMethodOverride(m_ThaVcgMemberOverride, SoMemSizeInit);
        mMethodOverride(m_ThaVcgMemberOverride, SkMemSizeInit);
        mMethodOverride(m_ThaVcgMemberOverride, SourceAdd);
        mMethodOverride(m_ThaVcgMemberOverride, SinkAdd);
        mMethodOverride(m_ThaVcgMemberOverride, SinkRemove);
        mMethodOverride(m_ThaVcgMemberOverride, SourceRemove);
        }

    mMethodsSet(vcgMember, &m_ThaVcgMemberOverride);
    }

static void OverrideAtConcateMember(AtConcateMember self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtConcateMemberOverride, mMethodsGet(self), sizeof(m_AtConcateMemberOverride));

        mMethodOverride(m_AtConcateMemberOverride, SourceStateGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkStateGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceSequenceSet);
        mMethodOverride(m_AtConcateMemberOverride, SourceSequenceGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkSequenceGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkExpectedSequenceSet);
        mMethodOverride(m_AtConcateMemberOverride, SinkExpectedSequenceGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceControlGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkControlGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceMstGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkMstGet);
        }

    mMethodsSet(self, &m_AtConcateMemberOverride);
    }

static void MethodsInit(Tha60210012VcgMember self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HwIdGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtConcateMember self)
    {
    OverrideAtChannel(self);
    OverrideAtConcateMember(self);
    OverrideThaVcgMember(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012VcgMember);
    }

AtConcateMember Tha60210012VcgMemberObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcgMemberObjectInit((AtConcateMember)self, vcg, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((Tha60210012VcgMember)self);
    m_methodsInit = 1;

    return self;
    }

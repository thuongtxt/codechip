/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Tha60210012Concate
 *
 * File        : Tha60210012VcgMemberPdhDe3.c
 *
 * Created Date: Mar 11, 2016
 *
 * Description : PDH DE3 - VCG member of 60210012 product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../concate/Tha60210012ModuleConcate.h"
#include "Tha60210012VcgMemberInternal.h"
#include "Tha60210012VcgMember.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaVcgMemberMethods m_ThaVcgMemberOverride;

/* Save super implementation */
static const tThaVcgMemberMethods *m_ThaVcgMemberMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPdhChannel PdhChannel(ThaVcgMember self)
    {
    return (AtPdhChannel)AtConcateMemberChannelGet((AtConcateMember)self);
    }

static Tha60210012ModuleConcate ModuleConcate(AtPdhChannel channel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel) channel);
    return (Tha60210012ModuleConcate)AtDeviceModuleGet(device, cAtModuleConcate);
    }

static uint8 StsId(ThaVcgMember self)
    {
    uint8 slice24, sts24;
    AtPdhChannel channel = PdhChannel(self);
    ThaPdhChannelHwIdGet(channel, cAtModulePdh, &slice24, &sts24);
    return Tha60210012ModuleConcateHwStsSlice24ToMapStsId(ModuleConcate(channel), slice24, sts24);
    }

static uint8 VtgId(ThaVcgMember self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint8 HoMode(ThaVcgMember self)
    {
    AtPdhDe3 de3 = (AtPdhDe3)AtConcateMemberChannelGet((AtConcateMember)self);
    return AtPdhDe3IsE3((AtPdhDe3)de3) ? 0x2 : 0x1;
    }

static uint32 DefaultOffset(ThaVcgMember self)
    {
    return (uint32)(StsId(self) * 32UL);
    }

static void OverrideThaVcgMember(AtConcateMember self)
    {
    ThaVcgMember vcgMember = (ThaVcgMember)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaVcgMemberMethods = mMethodsGet(vcgMember);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgMemberOverride, m_ThaVcgMemberMethods, sizeof(m_ThaVcgMemberOverride));

        mMethodOverride(m_ThaVcgMemberOverride, StsId);
        mMethodOverride(m_ThaVcgMemberOverride, VtgId);
        mMethodOverride(m_ThaVcgMemberOverride, DefaultOffset);
        mMethodOverride(m_ThaVcgMemberOverride, HoMode);
        }

    mMethodsSet(vcgMember, &m_ThaVcgMemberOverride);
    }

static void Override(AtConcateMember self)
    {
    OverrideThaVcgMember(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012VcgMemberPdhDe3);
    }

AtConcateMember Tha60210012VcgMemberPdhDe3ObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012VcgMemberPdhDe1ObjectInit(self, vcg, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtConcateMember Tha60210012VcgMemberPdhDe3New(AtConcateGroup vcg, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtConcateMember newMember = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newMember == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012VcgMemberPdhDe3ObjectInit(newMember, vcg, channel);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CONCATE
 * 
 * File        : Tha60210012VcgMemberInternal.h
 * 
 * Created Date: Sep 10, 2015
 *
 * Description : Vcg member of 60210012 product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012VCGMEMBERINTERNAL_H_
#define _THA60210012VCGMEMBERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtVcg.h"
#include "AtSdhChannel.h"
#include "AtPdhChannel.h"
#include "../../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../../../generic/concate/AtConcateGroupInternal.h"
#include "../../../../default/pdh/ThaPdhDe1.h"
#include "../../../../default/pdh/ThaModulePdh.h"
#include "../../../../default/concate/member/ThaVcgMemberInternal.h"
#include "../../../../default/man/ThaDevice.h"
#include "../../../../default/ocn/ThaModuleOcn.h"
#include "../../../../default/concate/member/ThaVcgMember.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012VcgMemberMethods
    {
    void (*HwIdGet)(ThaVcgMember self, uint8 *slice24, uint8* sts24);
    }tTha60210012VcgMemberMethods;

typedef struct tTha60210012VcgMember
    {
    tThaVcgMember super;
    const tTha60210012VcgMemberMethods *methods;
    }tTha60210012VcgMember;

typedef struct tTha60210012VcgMemberPdhDe1
    {
    tTha60210012VcgMember super;
    }tTha60210012VcgMemberPdhDe1;

typedef struct tTha60210012VcgMemberPdhDe3
    {
    tTha60210012VcgMemberPdhDe1 super;
    }tTha60210012VcgMemberPdhDe3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtConcateMember Tha60210012VcgMemberObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel);
AtConcateMember Tha60210012VcgMemberPdhDe1ObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel);
AtConcateMember Tha60210012VcgMemberPdhDe3ObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012VCGMEMBERINTERNAL_H_ */


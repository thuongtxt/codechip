/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60210012VcgVcat.c
 *
 * Created Date: Sep 16, 2015
 *
 * Description : VCG of Tha60210012 product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/concate/AtVcgInternal.h"
#include "../../../default/concate/ThaVcgVcatInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../mpeg/Tha60210012ModuleMpeg.h"
#include "Tha60210012Vcg.h"
#include "member/Tha60210012VcgMember.h"
#include "Tha60210012ModuleConcateLoVcatReg.h"
#include "Tha60210012ModuleConcateInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cVcgVcatHoldOffTimeDefault  (5)
#define cVcgVcatWtrTimeDefault      (5)

#define mThis(self) ((tTha60210012VcgVcat *)self)

/*--------------------------- Macros -----------------------------------------*/


/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012VcgVcat
    {
    tThaVcgVcat super;
    }tTha60210012VcgVcat;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Override */
static tAtVcgMethods      m_AtVcgVcatOverride;
static tThaVcgVcatMethods m_ThaVcgVcatOverride;

/* Save super implementation */
static const tAtVcgMethods *m_AtVcgVcatMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60210012VcgCommon.h"

static eAtRet DefaultSet(ThaVcgVcat self)
    {
    /* Nothing to do for this project. Will determine later */
    AtUnused(self);
    return cAtOk;
    }

static eBool LcasIsSupported(AtVcg self)
	{
	AtUnused(self);
	return cAtFalse;
	}

static eAtModuleConcateRet LcasEnable(AtVcg self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport: cAtOk;
    }

static eBool LcasIsEnabled(AtVcg self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleConcateRet HoldOffTimerSet(AtVcg self, uint32 hoTimeMs)
    {
    AtUnused(self);
    AtUnused(hoTimeMs);
    return cAtErrorNotImplemented;
    }

static uint32 HoldOffTimerGet(AtVcg self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleConcateRet WtrTimerSet(AtVcg self, uint32 timerMs)
    {
    AtUnused(self);
    AtUnused(timerMs);
    return cAtErrorNotImplemented;
    }

static uint32 WtrTimerGet(AtVcg self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleConcateRet RmvTimerSet(AtVcg self, uint32 timerMs)
    {
    AtUnused(self);
    AtUnused(timerMs);
    return cAtErrorNotImplemented;
    }

static uint32 RmvTimerGet(AtVcg self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 SourceRsAckGet(AtVcg self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 SinkRsAckGet(AtVcg self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 SinkMstGet(AtVcg self, AtConcateMember member)
    {
    AtUnused(self);
    return (uint8) AtConcateMemberSinkMstGet(member);
    }

static uint8 SourceMstGet(AtVcg self, AtConcateMember member)
    {
    AtUnused(self);
    return (uint8) AtConcateMemberSourceMstGet(member);
    }

static void OverrideAtVcgVcat(AtVcg self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal        = AtSharedDriverOsalGet();
        m_AtVcgVcatMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtVcgVcatOverride, m_AtVcgVcatMethods, sizeof(m_AtVcgVcatOverride));

        mMethodOverride(m_AtVcgVcatOverride, LcasIsSupported);
        mMethodOverride(m_AtVcgVcatOverride, LcasEnable);
        mMethodOverride(m_AtVcgVcatOverride, LcasIsEnabled);
        mMethodOverride(m_AtVcgVcatOverride, HoldOffTimerSet);
        mMethodOverride(m_AtVcgVcatOverride, HoldOffTimerGet);
        mMethodOverride(m_AtVcgVcatOverride, RmvTimerSet);
        mMethodOverride(m_AtVcgVcatOverride, RmvTimerGet);
        mMethodOverride(m_AtVcgVcatOverride, WtrTimerSet);
        mMethodOverride(m_AtVcgVcatOverride, WtrTimerGet);
        mMethodOverride(m_AtVcgVcatOverride, SourceRsAckGet);
        mMethodOverride(m_AtVcgVcatOverride, SinkRsAckGet);
        mMethodOverride(m_AtVcgVcatOverride, SinkMstGet);
        mMethodOverride(m_AtVcgVcatOverride, SourceMstGet);
        }

    mMethodsSet(self, &m_AtVcgVcatOverride);
    }

static void OverrideThaVcgVcat(AtVcg self)
    {
    ThaVcgVcat vcg = (ThaVcgVcat)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgVcatOverride, mMethodsGet(vcg), sizeof(m_ThaVcgVcatOverride));

        mMethodOverride(m_ThaVcgVcatOverride, DefaultSet);
        }

    mMethodsSet(vcg, &m_ThaVcgVcatOverride);
    }

static void Override(AtVcg self)
    {
    AtConcateGroup group = (AtConcateGroup)self;

    OverrideAtObject(group);
    OverrideAtChannel(group);
    OverrideAtConcateGroup(group);
    OverrideAtVcgVcat(self);
    OverrideThaVcgVcat(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012VcgVcat);
    }

static AtVcg ObjectInit(AtVcg self, AtModuleConcate concateModule, uint32 vcatId, eAtConcateMemberType memberType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcgVcatObjectInit(self, concateModule, vcatId, memberType) == NULL)
        return NULL;

    /* Private data */

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtVcg Tha60210012VcgVcatNew(AtModuleConcate concateModule, uint32 vcatId, eAtConcateMemberType memberType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtVcg newVcg = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newVcg == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newVcg, concateModule, vcatId, memberType);
    }

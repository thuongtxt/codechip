/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CONCATE
 * 
 * File        : Tha60210012ModuleConcateAsyncInit.h
 * 
 * Created Date: Aug 23, 2016
 *
 * Description : AsyncInit
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULECONCATEASYNCINIT_H_
#define _THA60210012MODULECONCATEASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210012ModuleConcateAsyncInit(AtObject self);
eAtRet Tha60210012ModuleConcateSuperAsyncInit(AtObject self);
eAtRet Tha60210012ModuleConcateDeactivate(AtObject self);
eAtRet Tha60210012ModuleConcateHwXcReset(AtObject self);
eAtRet Tha60210012ModuleConcateHwE1UnframeReset(AtObject self);
eAtRet Tha60210012ModuleConcateHwMuxReset(AtObject self);
eAtRet Tha60210012ModuleConcatePrbsEngineDisable(AtObject self);
eAtRet Tha60210012ModuleConcateActivate(AtObject self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULECONCATEASYNCINIT_H_ */


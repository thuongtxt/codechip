/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate Module
 *
 * File        : Tha60210012VcgCommon.h
 *
 * Created Date: May 18, 2016
 *
 * Description : Common set up
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
#ifndef _THA60210012VCGCOMMON_H_
#define _THA60210012VCGCOMMON_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/encap/AtEncapChannelInternal.h"
#include "../../../../generic/encap/AtHdlcChannelInternal.h"
#include "../encap/Tha60210012ModuleEncapInternal.h"
#include "Tha60210012ModuleConcate.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;

static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tAtConcateGroupMethods m_AtConcateGroupOverride;

/* To save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;
static const tAtChannelMethods      *m_AtChannelMethods      = NULL;
static const tAtConcateGroupMethods *m_AtConcateGroupMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEncap EncapModuleGet(AtConcateGroup self)
	{
	AtDevice device = AtChannelDeviceGet((AtChannel) self);

	return (AtModuleEncap) AtDeviceModuleGet(device, cAtModuleEncap);
	}

static void Delete(AtObject self)
    {
    AtConcateGroup group = (AtConcateGroup) self;
	AtObjectDelete((AtObject)AtConcateGroupEncapChannelGet(group));
	AtConcateGroupEncapChannelSet(group, NULL);
    m_AtObjectMethods->Delete(self);
    }

static eBool EncapTypeIsSupported(AtConcateGroup self, eAtEncapType encapType)
	{
	AtUnused(self);

	switch((uint32)encapType)
		{
		case cAtEncapHdlc: return cAtTrue;
		case cAtEncapGfp : return cAtTrue;
		default: return cAtFalse;
		}
	}

static eBool ServiceIsRunning(AtChannel self)
    {
    AtEncapChannel channel = AtConcateGroupEncapChannelGet((AtConcateGroup)self);
    return AtChannelServiceIsRunning((AtChannel)channel);
    }

static eAtRet ConcateReset(AtConcateGroup self)
    {
    AtEncapChannel channel = AtConcateGroupEncapChannelGet(self);
    if (channel == NULL)
        return cAtOk;

    if (AtChannelServiceIsRunning((AtChannel)self))
        return cAtErrorChannelBusy;

    AtObjectDelete((AtObject)channel);
    return AtConcateGroupEncapChannelSet(self, NULL);
    }

static eAtModuleConcateRet GfpChannelSet(AtConcateGroup self)
    {
    AtEncapChannel newGfp = (AtEncapChannel)AtModuleEncapGfpChannelObjectCreate(EncapModuleGet(self), AtChannelIdGet((AtChannel)self));

    if (newGfp == NULL)
        return cAtErrorRsrcNoAvail;

    /* Default Configuration */
    AtEncapChannelPhysicalChannelSet((AtEncapChannel) newGfp, (AtChannel) self);
    AtChannelInit((AtChannel)newGfp);
    return AtConcateGroupEncapChannelSet(self, newGfp);
    }

static eAtModuleConcateRet HdlcChannelSet(AtConcateGroup self)
    {
    AtLapsLink lapsLink;
    AtEncapChannel newHdlc = (AtEncapChannel)AtModuleEncapHdlcLapsChannelObjectCreate(EncapModuleGet(self), AtChannelIdGet((AtChannel)self));

    if (newHdlc == NULL)
        return cAtErrorRsrcNoAvail;

    lapsLink =  AtModuleEncapLapsLinkObjectCreate(EncapModuleGet(self), (AtHdlcChannel)newHdlc);
    if (lapsLink == NULL)
        {
        AtObjectDelete((AtObject) newHdlc);
        return cAtErrorRsrcNoAvail;
        }

    /* Default Configuration */
    AtHdlcChannelHdlcLinkSet((AtHdlcChannel)newHdlc, (AtHdlcLink)lapsLink);
    AtEncapChannelPhysicalChannelSet((AtEncapChannel) newHdlc, (AtChannel) self);
    AtChannelInit((AtChannel)newHdlc);
    AtChannelInit((AtChannel)lapsLink);
    return AtConcateGroupEncapChannelSet(self, newHdlc);
    }

static eAtModuleConcateRet EncapTypeSet(AtConcateGroup self, eAtEncapType encapType)
	{
    eAtRet ret;

    /* Let super deal with database. */
    ret = m_AtConcateGroupMethods->EncapTypeSet(self, encapType);
    if (ret != cAtOk)
        return ret;

    /* Encap mode is not changed, nothing to do */
    if (AtConcateGroupEncapTypeGet(self) == encapType)
        return cAtOk;

    ret = ConcateReset(self);
    if (ret != cAtOk)
        return ret;

    if (encapType == cAtEncapGfp)
        return GfpChannelSet(self);

    if (encapType == cAtEncapHdlc)
        return HdlcChannelSet(self);

    return cAtErrorModeNotSupport;
    }

static uint32 EncapTypeGet(AtConcateGroup self)
	{
	return Tha60210012ModuleEncapChannelEncapModeGet(EncapModuleGet(self), AtChannelIdGet((AtChannel)self));
	}

static eAtRet BindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    if (encapChannel == NULL)
        return cAtErrorModeNotSupport;

    if (AtConcateGroupEncapChannelGet((AtConcateGroup)self) == encapChannel)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static AtModuleConcate ModuleConcate(AtConcateGroup self)
    {
    return (AtModuleConcate) AtChannelModuleGet((AtChannel)self);
    }

static eAtRet Init(AtChannel self)
    {
    uint32 vcgId = AtChannelIdGet(self);
    AtConcateGroup group = (AtConcateGroup)self;

    Tha60210012ModuleEncapChannelEncapModeSet(EncapModuleGet(group), vcgId, cAtEncapUnknown);
    Tha60210012ModuleConcateGlobalConcateTypeSet(ModuleConcate(group), AtConcateGroupConcatTypeGet(group));
    AtConcateGroupEncapTypeSet(group, cAtEncapGfp);

    return cAtOk;
    }

static void OverrideAtObject(AtConcateGroup self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtConcateGroup self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, BindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtConcateGroup(AtConcateGroup self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtConcateGroupMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtConcateGroupOverride, m_AtConcateGroupMethods, sizeof(m_AtConcateGroupOverride));

        mMethodOverride(m_AtConcateGroupOverride, EncapTypeIsSupported);
        mMethodOverride(m_AtConcateGroupOverride, EncapTypeSet);
        mMethodOverride(m_AtConcateGroupOverride, EncapTypeGet);
        }

    mMethodsSet(self, &m_AtConcateGroupOverride);
    }

#endif /* _THA60210012VCGCOMMON_H_ */

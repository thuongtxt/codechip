/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60210012ModuleConcateAsyncInit.c
 *
 * Created Date: Aug 23, 2016
 *
 * Description : Asyncinit
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/concate/binder/ThaVcgBinder.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../encap/Tha60210012ModuleEncap.h"
#include "Tha60210012Vcg.h"
#include "member/Tha60210012VcgMember.h"
#include "binder/Tha60210012VcgBinder.h"
#include "Tha60210012ModuleConcateInternal.h"
#include "Tha60210012ModuleConcateLoVcatReg.h"
#include "Tha60210012ModuleConcateAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*tAtAsycnOperationFunc)(AtObject self);


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtAsycnOperationFunc functions[] = {Tha60210012ModuleConcateSuperAsyncInit, Tha60210012ModuleConcateDeactivate,
                                            Tha60210012ModuleConcateHwXcReset, Tha60210012ModuleConcateHwE1UnframeReset,
                                            Tha60210012ModuleConcateHwMuxReset, Tha60210012ModuleConcatePrbsEngineDisable,
                                            Tha60210012ModuleConcateActivate};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 AsyncMaxState(AtObject self)
    {
    AtUnused(self);
    return mCount(functions);
    }

static uint32 AsyncStateGet(AtObject self)
    {
    return ((Tha60210012ModuleConcate)self)->asyncInitState;
    }

static void AsyncStateIncrease(AtObject self)
    {
    ((Tha60210012ModuleConcate)self)->asyncInitState += 1;
    }

static void AsyncStateReset(AtObject self)
    {
    ((Tha60210012ModuleConcate)self)->asyncInitState = 0;
    }

static tAtAsycnOperationFunc AsyncOperationGet(AtObject self, uint32 state)
    {
    if (state >= AsyncMaxState(self))
        return NULL;

    return functions[state];
    }

static eAtRet AsyncInitMain(AtObject self)
    {
    uint32 state = AsyncStateGet(self);
    tAtAsycnOperationFunc func = AsyncOperationGet(self, state);
    eAtRet ret = cAtOk;
    tAtOsalCurTime profileTime;

    char stateString[32];
    AtSprintf(stateString, "Async state: %d", state);
    AtOsalCurTimeGet(&profileTime);

    if (func == NULL)
        {
        AsyncStateReset(self);
        return cAtErrorNullPointer;
        }

    ret = func(self);
    AtDeviceAccessTimeInMsSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, &profileTime, __FILE__, __LINE__, stateString);
    AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, __FILE__, __LINE__, stateString);
    if (ret == cAtOk)
        {
        AsyncStateIncrease(self);
        ret = cAtErrorAgain;
        }

    else if (!AtDeviceAsyncRetValIsInState(ret))
        {
        AsyncStateReset(self);
        return ret;
        }

    if (AsyncStateGet(self) == AsyncMaxState(self))
        {
        ret = cAtOk;
        AsyncStateReset(self);
        }

    return ret;
    }

eAtRet Tha60210012ModuleConcateAsyncInit(AtObject self)
    {
    return AsyncInitMain(self);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60210012VcgBinderDe3.c
 *
 * Created Date: MAR 11, 2016
 *
 * Description : DE3 VCG binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDevice.h"
#include "../../../../default/ocn/ThaModuleOcn.h"
#include "../../../../default/pwe/ThaModulePwe.h"
#include "../../../../default/map/ThaModuleAbstractMap.h"
#include "../../concate/Tha60210012ModuleConcate.h"
#include "../../encap/Tha60210012ModuleEncap.h"
#include "../../pwe/Tha60210012ModulePweBlockManager.h"
#include "../member/Tha60210012VcgMember.h"
#include "Tha60210012VcgBinderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtVcgBinderMethods  m_AtVcgBinderOverride;
static tThaVcgBinderMethods  m_ThaVcgBinderOverride;

/* Save super implementation */
static const tAtVcgBinderMethods  *m_AtVcgBinderMethods  = NULL;
static const tThaVcgBinderMethods  *m_ThaVcgBinderMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPdhChannel PdhChannel(ThaVcgBinder self)
    {
    return (AtPdhChannel)AtVcgBinderChannelGet((AtVcgBinder)self);
    }

static Tha60210012ModuleConcate ModuleConcate(AtPdhChannel channel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel) channel);
    return (Tha60210012ModuleConcate)AtDeviceModuleGet(device, cAtModuleConcate);
    }

static uint32 DefaultOffset(ThaVcgBinder self)
    {
    uint8 slice24, hwSts24, mapStsId;
    AtPdhChannel de3 = PdhChannel(self);
    ThaPdhChannelHwIdGet(PdhChannel(self), cAtModulePdh, &slice24, &hwSts24);
    mapStsId = Tha60210012ModuleConcateHwStsSlice24ToMapStsId(ModuleConcate(de3), slice24, hwSts24);
    return (uint32)(mapStsId * 32);
    }

static AtConcateMember MemberObjectCreate(AtVcgBinder self, AtConcateGroup vcg)
    {
    return Tha60210012VcgMemberPdhDe3New(vcg, AtVcgBinderChannelGet(self));
    }

static ThaModuleAbstractMap ModuleMap(AtChannel channel)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    return (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    }

static ThaModuleAbstractMap ModuleDemap(AtChannel channel)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    return (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    }

static AtModuleEncap ModuleEncap(AtChannel channel)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    return (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static eAtRet SinkVcgAssign(ThaVcgBinder self, AtConcateGroup group)
    {
    AtChannel channel;
    eAtRet ret = m_ThaVcgBinderMethods->SinkVcgAssign(self, group);
    if (ret != cAtOk)
        return ret;

    channel = AtVcgBinderChannelGet((AtVcgBinder)self);
    ret |= Tha60210012VcgDe3MemberBlockAllocate(channel);
    ret |= ThaModuleAbstractMapBindDe3ToConcateGroup(ModuleDemap(channel), (AtPdhDe3)channel, group);
    return ret;
    }

static eAtRet SourceIncreaseBandwidth(AtConcateGroup group, AtChannel channel)
    {
    uint32 numMember = AtConcateGroupNumSourceMembersGet(group);
    uint32 channelId = AtChannelIdGet((AtChannel) group);
    uint32 threshold = ((numMember + 1) > 5) ? 5 : 4;
    return Tha60210012ModuleEncapChannelThresholdSet(ModuleEncap(channel), channelId, threshold);
    }

static eAtRet SourceVcgAssign(ThaVcgBinder self, AtConcateGroup group)
    {
    AtChannel channel;
    eAtRet ret = m_ThaVcgBinderMethods->SourceVcgAssign(self, group);
    if (ret != cAtOk)
        return ret;

    channel = AtVcgBinderChannelGet((AtVcgBinder)self);
    ret |= ThaModuleAbstractMapBindDe3ToConcateGroup(ModuleMap(channel), (AtPdhDe3)channel, group);
    ret |= SourceIncreaseBandwidth(group, channel);
    
    return ret;
    }

static eAtRet SourceDecreaseBandwidth(AtConcateGroup group, AtChannel channel)
    {
    uint32 numMember = AtConcateGroupNumSourceMembersGet(group);
    uint32 channelId = AtChannelIdGet((AtChannel) group);
    uint32 threshold = ((numMember - 1) > 5) ? 5 : 4;
    return Tha60210012ModuleEncapChannelThresholdSet(ModuleEncap(channel), channelId, threshold );
    }

static eAtRet SourceVcgDeassign(ThaVcgBinder self, AtConcateGroup group)
    {
    eAtRet ret = m_ThaVcgBinderMethods->SourceVcgDeassign(self, group);
    if (ret != cAtOk)
        return ret;

    return SourceDecreaseBandwidth(group, AtVcgBinderChannelGet((AtVcgBinder)self));
    }

static void IdMappingShow(ThaVcgBinder self)
    {
    uint8 slice24, hwSts24, mapHwSts;
    AtPdhChannel de3 = PdhChannel(self);
    ThaPdhChannelHwIdGet(de3, cAtModulePdh, &slice24, &hwSts24);
    mapHwSts = Tha60210012ModuleConcateHwStsSlice24ToMapStsId(ModuleConcate(de3), slice24, hwSts24);
    AtPrintc(cSevDebug, " ******************************************************************************** \r\n");
    AtPrintc(cSevInfo, " %-64s : slice24 %2d hwSts24 %2d sts96 %2d\r\n", "Common Hardware Ids", slice24, hwSts24, mapHwSts);
    AtPrintc(cSevDebug, " \r\n");
    }

static void OverrideAtVcgBinder(ThaVcgBinder self)
    {
    AtVcgBinder binder = (AtVcgBinder)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtVcgBinderMethods = mMethodsGet(binder);
        mMethodsGet(osal)->MemCpy(osal, &m_AtVcgBinderOverride, m_AtVcgBinderMethods, sizeof(m_AtVcgBinderOverride));

        mMethodOverride(m_AtVcgBinderOverride, MemberObjectCreate);
        }

    mMethodsSet(binder, &m_AtVcgBinderOverride);
    }

static void OverrideThaVcgBinder(ThaVcgBinder self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaVcgBinderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgBinderOverride, m_ThaVcgBinderMethods, sizeof(m_ThaVcgBinderOverride));

        mMethodOverride(m_ThaVcgBinderOverride, DefaultOffset);
        mMethodOverride(m_ThaVcgBinderOverride, SinkVcgAssign);
        mMethodOverride(m_ThaVcgBinderOverride, SourceVcgAssign);
        mMethodOverride(m_ThaVcgBinderOverride, SourceVcgDeassign);
        mMethodOverride(m_ThaVcgBinderOverride, IdMappingShow);
        }

    mMethodsSet(self, &m_ThaVcgBinderOverride);
    }

static void Override(ThaVcgBinder self)
    {
    OverrideAtVcgBinder(self);
    OverrideThaVcgBinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012VcgBinderPdhDe3);
    }

ThaVcgBinder Tha60210012VcgBinderPdhDe3ObjectInit(ThaVcgBinder self, AtPdhDe3 de3)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012VcgBinderObjectInit(self, (AtChannel)de3) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaVcgBinder Tha60210012VcgBinderPdhDe3New(AtPdhDe3 de3)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaVcgBinder newBinder = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBinder == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012VcgBinderPdhDe3ObjectInit(newBinder, de3);
    }

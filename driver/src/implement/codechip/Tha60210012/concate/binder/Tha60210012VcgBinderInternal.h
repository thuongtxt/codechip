/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : Tha60210012VcgBinderInternal.h
 * 
 * Created Date: MAR 11, 2016
 *
 * Description : VCG binder
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012VCGBINDERINTERNAL_H_
#define _THA60210012VCGBINDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhChannel.h"
#include "AtPdhChannel.h"
#include "../../../../default/concate/binder/ThaVcgBinderInternal.h"
#include "../../../../default/pdh/ThaModulePdh.h"
#include "../../../../default/pdh/ThaPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012VcgBinder
    {
	tThaVcgBinder super;
    }tTha60210012VcgBinder;

typedef struct tTha60210012VcgBinderPdhDe1
    {
	tTha60210012VcgBinder super;
    }tTha60210012VcgBinderPdhDe1;

typedef struct tTha60210012VcgBinderPdhDe3
    {
	tTha60210012VcgBinder super;
    }tTha60210012VcgBinderPdhDe3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaVcgBinder Tha60210012VcgBinderObjectInit(ThaVcgBinder self, AtChannel channel);
ThaVcgBinder Tha60210012VcgBinderPdhDe3ObjectInit(ThaVcgBinder self, AtPdhDe3 de3);
ThaVcgBinder Tha60210012VcgBinderPdhDe1ObjectInit(ThaVcgBinder self, AtPdhDe1 de1);

#endif /* _THA60210012VCGBINDERINTERNAL_H_ */

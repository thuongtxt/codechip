/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60210012VcgBinder.c
 *
 * Created Date: Jul 22, 2014
 *
 * Description : VCG binder of Tha60210012 product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/concate/AtConcateGroupInternal.h"
#include "../../../../default/concate/member/ThaVcgMember.h"
#include "../../../../default/concate/ThaVcg.h"
#include "../../concate/Tha60210012ModuleConcateLoVcatReg.h"
#include "../../concate/Tha60210012ModuleConcate.h"
#include "Tha60210012VcgBinderInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha60210012VcgBinder *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaVcgBinderMethods m_ThaVcgBinderOverride;

/* Save super implementation */
static const tThaVcgBinderMethods *m_ThaVcgBinderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210012ModuleConcate ConcateModule(ThaVcgBinder self)
    {
    AtDevice device = AtChannelDeviceGet(AtVcgBinderChannelGet((AtVcgBinder)self));
    return (Tha60210012ModuleConcate)AtDeviceModuleGet(device, cAtModuleConcate);
    }

static uint32 BaseAddress(ThaVcgBinder self)
    {
    return Tha60210012ModuleConcateBaseAddress(ConcateModule(self));
    }

static uint32 Offset(ThaVcgBinder self)
    {
    return mMethodsGet(self)->DefaultOffset(self) + BaseAddress(self);
    }

static eAtRet SinkVcgAssign(ThaVcgBinder self, AtConcateGroup group)
    {
    uint32 regAddr = cAf6Reg_vcat5g_sk_memctrl_pen_Base + Offset(self);
    uint32 regVal = AtVcgBinderRead((AtVcgBinder)self, regAddr, cAtModuleConcate);
    AtUnused(group);

    mRegFieldSet(regVal, cAf6_vcat5g_sk_memctrl_pen_Sk_G8040_Md_, AtConcateGroupIsNoneVcatG8040(group) ? 0x1 : 0x0);
    mRegFieldSet(regVal, cAf6_vcat5g_sk_memctrl_pen_Sk_Vcat_Md_, AtConcateGroupIsVcatVcg(group) ? 1 : 0);
    mRegFieldSet(regVal, cAf6_vcat5g_sk_memctrl_pen_Sk_Lcas_Md_, AtConcateGroupIsLcasVcg(group) ? 1 : 0);
    mRegFieldSet(regVal, cAf6_vcat5g_sk_memctrl_pen_Sk_Mem_Ena_, 0x1);
    mRegFieldSet(regVal, cAf6_vcat5g_sk_memctrl_pen_Sk_VcgId_, AtChannelIdGet((AtChannel)group));
    AtVcgBinderWrite((AtVcgBinder)self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SourceVcgAssign(ThaVcgBinder self, AtConcateGroup group)
    {
    eAtRet ret = cAtOk;
    eBool swControlEnabled = (AtConcateGroupIsLcasVcg(group) ? cAtFalse : (AtConcateGroupIsVcatVcg(group) ? cAtTrue : cAtFalse));
    uint32 regAddr = cAf6Reg_vcat5g_so_memctrl_pen_Base + Offset(self);
    uint32 regVal = AtVcgBinderRead((AtVcgBinder)self, regAddr, cAtModuleConcate);

    mRegFieldSet(regVal, cAf6_vcat5g_so_memctrl_pen_So_G8040_Md_, AtConcateGroupIsNoneVcatG8040(group) ? 0x1 : 0x0);
    mRegFieldSet(regVal, cAf6_vcat5g_so_memctrl_pen_So_Vcat_Md_, AtConcateGroupIsVcatVcg(group) ? 1 : 0);
    mRegFieldSet(regVal, cAf6_vcat5g_so_memctrl_pen_So_Lcas_Md_, AtConcateGroupIsLcasVcg(group) ? 1 : 0);
    mRegFieldSet(regVal, cAf6_vcat5g_so_memctrl_pen_So_Mem_Ena_, 0x1);
    mRegFieldSet(regVal, cAf6_vcat5g_so_memctrl_pen_So_VcgId_, AtChannelIdGet((AtChannel)group));

    /* Enable Add command */
    mRegFieldSet(regVal, cAf6_vcat5g_so_memctrl_pen_So_Nms_Cmd_, mBoolToBin(swControlEnabled));
    AtVcgBinderWrite((AtVcgBinder)self, regAddr, regVal, cAtModuleConcate);

    /* Enable to control sequence in VCAT mode */
    ret |= ThaVcgBinderTxSeqSwControl(self, swControlEnabled);
    return ret;
    }

static eAtRet SinkVcgDeassign(ThaVcgBinder self, AtConcateGroup group)
    {
    AtUnused(group);
    AtVcgBinderWrite((AtVcgBinder)self, cAf6Reg_vcat5g_sk_memctrl_pen_Base + Offset(self), 0x0, cAtModuleConcate);
    return cAtOk;
    }

static eAtRet SourceVcgDeassign(ThaVcgBinder self, AtConcateGroup group)
    {
    AtUnused(group);
    AtVcgBinderWrite((AtVcgBinder)self, cAf6Reg_vcat5g_so_memctrl_pen_Base + Offset(self), 0x0, cAtModuleConcate);
    return cAtOk;
    }

static eAtRet TxSeqSwControl(ThaVcgBinder self, eBool swControlEnabled)
    {
    uint32 regAddr = cAf6Reg_vcat5g_so_txseqmem_pen_Base +  Offset(self);
    uint32 regVal = AtVcgBinderRead((AtVcgBinder)self, regAddr, cAtModuleConcate);
    mRegFieldSet(regVal, cAf6_vcat5g_so_txseqmem_pen_So_TxSeqEna_, mBoolToBin(swControlEnabled));
    AtVcgBinderWrite((AtVcgBinder)self, regAddr, regVal, cAtModuleConcate);
    return cAtOk;
    }

static void RegsShow(ThaVcgBinder self, const char *description, uint32 address)
    {
    uint32 value = AtVcgBinderRead( (AtVcgBinder)self, address, cAtModuleConcate);
    AtPrintc(cSevInfo, " %-64s : address: 0x%08x = 0x%08x \r\n", description, address, value);
    }

static void ChannelRegsShow(ThaVcgBinder self)
    {
    AtConcateMember concateMember = AtVcgBinderSourceMemberGet((AtVcgBinder)self);
    uint32 soMemberOffset = ThaVcgMemberDefaultOffset((ThaVcgMember) concateMember);
    uint32 offset = Offset(self);
    uint32 mapId;
    uint8 slice24, sts24;

    if(!concateMember)
        concateMember = AtVcgBinderSinkMemberGet((AtVcgBinder)self);

    if(!concateMember)
    	{
    	AtPrintc(cSevWarning, " Concate Member Is Not Added Yet!\r\n");
    	return;
    	}

    mapId = (uint32)ThaVcgMemberStsId((ThaVcgMember)concateMember);
    Tha60210012ModuleConcateMapStsIdToStsAndSlice24(ConcateModule(self), (uint8)mapId, &slice24, &sts24);
    AtPrintc(cSevDebug, " sts96 %d\r\n", mapId);
    AtPrintc(cSevDebug, " \r\n");
    AtPrintc(cSevDebug, " ******************************************************************************** \r\n");
    AtPrintc(cSevDebug, " %-28s :\r\n", "Source Registers Info");
    AtPrintc(cSevDebug, " \r\n");
    AtPrintc(cSevDebug, " Binder-Offset 0x%08x Member-Offset  0x%08x \r\n", offset, soMemberOffset + BaseAddress(self));
    RegsShow(self," Source Mode Configuration", cAf6Reg_vcat5g_so_ctrl_pen(mapId) + BaseAddress(self));
    RegsShow(self," Source E1-Unframed Mode Configuration", cAf6Reg_vcat5g_so_unfrm_pen(mapId) + BaseAddress(self));
    RegsShow(self," Source Member Control Configuration", cAf6Reg_vcat5g_so_memctrl_pen_Base + soMemberOffset + BaseAddress(self));
    RegsShow(self," Source Transmit Sequence Configuration", cAf6Reg_vcat5g_so_txseqmem_pen_Base + soMemberOffset + BaseAddress(self));
    RegsShow(self," Source Member Status", cAf6Reg_vcat5g_so_memsta_pen_Base + soMemberOffset + BaseAddress(self));
    RegsShow(self," Output OC-24 STS Connection Memory", cAf6Reg_vcat5g_so_stsxc_pen(mapId) + BaseAddress(self));
    AtPrintc(cSevDebug, " \r\n");
    AtPrintc(cSevDebug, " ******************************************************************************** \r\n");
    AtPrintc(cSevDebug, " %-28s :\r\n", "Sink Registers Info");
    AtPrintc(cSevDebug, " \r\n");
    AtPrintc(cSevDebug, " slice24 %d sts24 %d\r\n", slice24, sts24);
    RegsShow(self," Input OC-24 STS Connection Memory", cAf6Reg_vcat5g_sk_stsxc_pen((uint32)slice24, (uint32)sts24) + BaseAddress(self));
    RegsShow(self," Sink Slice Mode Configuration", cAf6Reg_vcat5g_sk_ctrl_pen((uint32)slice24, (uint32)sts24) + BaseAddress(self));
    RegsShow(self," Sink Slice E1-Unframed Mode Configuration", cAf6Reg_vcat5g_sk_unfrm_pen((uint32)slice24, (uint32)sts24) + BaseAddress(self));
    RegsShow(self," Sink Member Control Configuration", cAf6Reg_vcat5g_sk_memctrl_pen_Base + offset);
    RegsShow(self," Sink Slice Input Mux Configuration", cAf6Reg_vcat5g_sk_mux_pen((uint32)slice24, (uint32)sts24) + BaseAddress(self));
    RegsShow(self," Sink Member Status", cAf6Reg_vcat5g_sk_memsta_pen_Base + offset);

    AtPrintc(cSevDebug, " \r\n");
    }

static void OverrideThaVcgBinder(ThaVcgBinder self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaVcgBinderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgBinderOverride, m_ThaVcgBinderMethods, sizeof(m_ThaVcgBinderOverride));

        mMethodOverride(m_ThaVcgBinderOverride, SinkVcgAssign);
        mMethodOverride(m_ThaVcgBinderOverride, SourceVcgAssign);
        mMethodOverride(m_ThaVcgBinderOverride, SinkVcgDeassign);
        mMethodOverride(m_ThaVcgBinderOverride, SourceVcgDeassign);
        mMethodOverride(m_ThaVcgBinderOverride, TxSeqSwControl);
        mMethodOverride(m_ThaVcgBinderOverride, ChannelRegsShow);
        }

    mMethodsSet(self, &m_ThaVcgBinderOverride);
    }

static void Override(ThaVcgBinder self)
    {
    OverrideThaVcgBinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012VcgBinder);
    }

ThaVcgBinder Tha60210012VcgBinderObjectInit(ThaVcgBinder self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcgBinderObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

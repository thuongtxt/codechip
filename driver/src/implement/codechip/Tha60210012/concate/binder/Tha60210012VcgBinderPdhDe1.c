/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60210012VcgBinderDe1.c
 *
 * Created Date: MAR 11, 2016
 *
 * Description : DE1 VCG binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDevice.h"
#include "../../../../default/ocn/ThaModuleOcn.h"
#include "../../../../default/map/ThaModuleAbstractMap.h"
#include "../../concate/Tha60210012ModuleConcate.h"
#include "../../pwe/Tha60210012ModulePweBlockManager.h"
#include "../member/Tha60210012VcgMember.h"
#include "Tha60210012VcgBinderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtVcgBinderMethods  m_AtVcgBinderOverride;
static tThaVcgBinderMethods  m_ThaVcgBinderOverride;

/* Save super implementation */
static const tAtVcgBinderMethods  *m_AtVcgBinderMethods  = NULL;
static const tThaVcgBinderMethods  *m_ThaVcgBinderMethods  = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPdhDe1 De1Get(ThaVcgBinder self)
    {
    return (ThaPdhDe1)AtVcgBinderChannelGet((AtVcgBinder)self);
    }

static Tha60210012ModuleConcate ModuleConcate(ThaPdhDe1 de1)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel) de1);
    return (Tha60210012ModuleConcate)AtDeviceModuleGet(device, cAtModuleConcate);
    }

static uint32 DefaultOffset(ThaVcgBinder self)
    {
    uint8 hwSlice24, hwSts24, hwVtg, hwVt, hwSts96;
    ThaPdhDe1 de1 = De1Get(self);
    ThaPdhDe1HwIdGet(de1, cThaModuleMap, &hwSlice24, &hwSts24,  &hwVtg, &hwVt);
    hwSts96 = Tha60210012ModuleConcateHwStsSlice24ToMapStsId(ModuleConcate(de1), hwSlice24, hwSts24);
    return (hwSts96 * 32UL + hwVtg * 4UL + hwVt);
    }

static AtConcateMember MemberObjectCreate(AtVcgBinder self, AtConcateGroup vcg)
    {
    return Tha60210012VcgMemberPdhDe1New(vcg, AtVcgBinderChannelGet(self));
    }

static ThaModuleAbstractMap ModuleMap(AtChannel channel)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    return (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    }

static ThaModuleAbstractMap ModuleDemap(AtChannel channel)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    return (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    }

static eAtRet SinkVcgAssign(ThaVcgBinder self, AtConcateGroup group)
    {
    AtChannel channel;
    eAtRet ret = m_ThaVcgBinderMethods->SinkVcgAssign(self, group);
    if (ret != cAtOk)
        return ret;

    channel = AtVcgBinderChannelGet((AtVcgBinder)self);
    ret |= Tha60210012VcgDe1MemberBlockAllocate(channel);
    ret |= ThaModuleAbstractMapBindDe1ToConcateGroup(ModuleDemap(channel), (AtPdhDe1) channel, group);
    return ret;
    }

static eAtRet SourceVcgAssign(ThaVcgBinder self, AtConcateGroup group)
    {
    AtChannel channel;
    eAtRet ret = m_ThaVcgBinderMethods->SourceVcgAssign(self, group);
    if (ret != cAtOk)
        return ret;

    channel = AtVcgBinderChannelGet((AtVcgBinder)self);
    ret|= ThaModuleAbstractMapBindDe1ToConcateGroup(ModuleMap(channel), (AtPdhDe1)channel, group);
    return ret;
    }

static void IdMappingShow(ThaVcgBinder self)
    {
    uint8 hwSlice24, hwSts24, hwVtg, hwVt, hwMapSts;
    ThaPdhDe1 de1 = De1Get(self);
    ThaPdhDe1HwIdGet(de1, cThaModuleMap, &hwSlice24, &hwSts24,  &hwVtg, &hwVt);
    hwMapSts = Tha60210012ModuleConcateHwStsSlice24ToMapStsId(ModuleConcate(de1), hwSlice24, hwSts24);
    AtPrintc(cSevDebug, " ******************************************************************************** \r\n");
    AtPrintc(cSevInfo, " %-64s : slice24 %2d hwSts24 %2d hwVtg %2d hwVt %2d hwMapSts %2d\r\n", "Common Hardware Ids", hwSlice24, hwSts24, hwVtg, hwVt, hwMapSts);
    AtPrintc(cSevDebug, " \r\n");
    }

static void OverrideAtVcgBinder(ThaVcgBinder self)
    {
    AtVcgBinder binder = (AtVcgBinder)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtVcgBinderMethods = mMethodsGet(binder);
        mMethodsGet(osal)->MemCpy(osal, &m_AtVcgBinderOverride, m_AtVcgBinderMethods, sizeof(m_AtVcgBinderOverride));

        mMethodOverride(m_AtVcgBinderOverride, MemberObjectCreate);
        }

    mMethodsSet(binder, &m_AtVcgBinderOverride);
    }

static void OverrideThaVcgBinder(ThaVcgBinder self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaVcgBinderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgBinderOverride, m_ThaVcgBinderMethods, sizeof(m_ThaVcgBinderOverride));

        mMethodOverride(m_ThaVcgBinderOverride, DefaultOffset);
        mMethodOverride(m_ThaVcgBinderOverride, SinkVcgAssign);
        mMethodOverride(m_ThaVcgBinderOverride, SourceVcgAssign);
        mMethodOverride(m_ThaVcgBinderOverride, IdMappingShow);
        }

    mMethodsSet(self, &m_ThaVcgBinderOverride);
    }

static void Override(ThaVcgBinder self)
    {
    OverrideAtVcgBinder(self);
    OverrideThaVcgBinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012VcgBinderPdhDe1);
    }

ThaVcgBinder Tha60210012VcgBinderPdhDe1ObjectInit(ThaVcgBinder self, AtPdhDe1 de1)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012VcgBinderObjectInit(self, (AtChannel)de1) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaVcgBinder Tha60210012VcgBinderPdhDe1New(AtPdhDe1 de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaVcgBinder newBinder = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBinder == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012VcgBinderPdhDe1ObjectInit(newBinder, de1);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60210012VcgBinder.h
 *
 * Created Date: MAR 16, 2015
 *
 * Description : Vcg member
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
#ifndef _THA60210012VCGBINDER_H_
#define _THA60210012VCGBINDER_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/concate/binder/ThaVcgBinder.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaVcgBinder Tha60210012VcgBinderPdhDe1New(AtPdhDe1 de1);
ThaVcgBinder Tha60210012VcgBinderPdhDe3New(AtPdhDe3 de3);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012VCGBINDER_H_ */


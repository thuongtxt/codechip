/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60210012ModuleConcate.c
 *
 * Created Date: Sep 9, 2015
 *
 * Description : Concatenation module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/concate/binder/ThaVcgBinder.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../encap/Tha60210012ModuleEncap.h"
#include "Tha60210012Vcg.h"
#include "member/Tha60210012VcgMember.h"
#include "binder/Tha60210012VcgBinder.h"
#include "Tha60210012ModuleConcateInternal.h"
#include "Tha60210012ModuleConcateLoVcatReg.h"
#include "Tha60210012ModuleConcateAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60210012ModuleConcate)self)

/* Define to convert ID between OC48 BUS and OC96 BUS */
#define cHwSts3cMemberOffsetOnOc48Bus            16
#define cHwSts3cMemberOffsetOnOc96Bus            32
#define cNumbHwStsOnOc48Bus                      48
#define cNumbHwStsOnOc96Bus                      96
#define cNumbHwStsOnOc24Bus                      24
#define cNumbHwSliceOc24BusOfOc96Bus             (cNumbHwStsOnOc96Bus/cNumbHwStsOnOc24Bus)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012ModuleConcateMethods m_methods;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tAtModuleMethods        m_AtModuleOverride;
static tAtModuleConcateMethods m_AtModuleConcateOverride;

/* Save super implementation */
static const tAtObjectMethods        *m_AtObjectMethods        = NULL;
static const tAtModuleMethods        *m_AtModuleMethods        = NULL;
static const tAtModuleConcateMethods *m_AtModuleConcateMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(Tha60210012ModuleConcate self)
    {
    AtUnused(self);
    return 0x1D00000;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldReadRegisters, numHoldWriteRegisters;
    uint32 *holdReadRegisters;
    uint32 *holdWriteRegisters;

    holdReadRegisters  = AtModuleHoldReadRegistersGet(self, &numHoldReadRegisters);
    holdWriteRegisters = AtModuleHoldWriteRegistersGet(self, &numHoldWriteRegisters);
    return AtDefaultLongRegisterAccessNew(holdReadRegisters, numHoldReadRegisters, holdWriteRegisters, numHoldWriteRegisters);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);

    /* TODO: Need to be checked again this range! */
    if (((localAddress >= 0x1D00000) && (localAddress <= 0x01DFFFFF)))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 *HoldReadRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    uint32 baseAddress = 0x0;
    static uint32 holdRegisters[] = {0x0, 0x0};

    baseAddress = Tha60210012ModuleConcateBaseAddress((Tha60210012ModuleConcate)self);
    holdRegisters[0] = baseAddress + cAf6Reg_vcat5g_rdhold1_pen;
    holdRegisters[1] = baseAddress + cAf6Reg_vcat5g_rdhold2_pen;

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static uint32 *HoldWriteRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    uint32 baseAddress = 0x0;
    static uint32 holdRegisters[] = {0x0, 0x0};

    baseAddress = Tha60210012ModuleConcateBaseAddress((Tha60210012ModuleConcate)self);
    holdRegisters[0] = baseAddress + cAf6Reg_vcat5g_wrhold1_pen;
    holdRegisters[1] = baseAddress + cAf6Reg_vcat5g_wrhold2_pen;

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static void HwXcReset(Tha60210012ModuleConcate self)
    {
    uint32 baseAddress = Tha60210012ModuleConcateBaseAddress(self);
    uint8 slice_24;
    uint8 sts_96;
    uint8 sts_24;
    uint32 regVal;
    for (slice_24 = 0; slice_24 < 4; slice_24 ++)
       {
       for (sts_24 = 0; sts_24 < 24; sts_24 ++)
           {
           regVal = 0x0;
           sts_96 = Tha60210012ModuleConcateHwStsSlice24ToMapStsId(self, slice_24, sts_24);
           mRegFieldSet(regVal, cAf6_vcat5g_so_stsxc_pen_OC24_OutSlice_,  slice_24);
           mRegFieldSet(regVal, cAf6_vcat5g_so_stsxc_pen_OC24_OutSTS_,  sts_24);
           mModuleHwWrite(self, baseAddress + (uint32)cAf6Reg_vcat5g_so_stsxc_pen(sts_96), regVal);

           /* Sink Connection Memory */
           mModuleHwWrite(self, baseAddress + (uint32)cAf6Reg_vcat5g_sk_stsxc_pen(slice_24, sts_24), sts_96);
           }
       }
    }

static void HwE1UnframeReset(Tha60210012ModuleConcate self)
    {
    uint32 baseAddress = Tha60210012ModuleConcateBaseAddress((Tha60210012ModuleConcate) self);
    uint8 hwSts96_i;
    uint8 hwSlice24_i;
    uint8 hwSts24_i;

    /* Reset for source */
    for (hwSts96_i = 0; hwSts96_i < cNumbHwStsOnOc96Bus; hwSts96_i++)
        {
        mModuleHwWrite(self,
                       baseAddress + (uint32)cAf6Reg_vcat5g_so_unfrm_pen(hwSts96_i),
                       0x0);
        }

    /* Reset for sink */
    for (hwSlice24_i = 0; hwSlice24_i < cNumbHwSliceOc24BusOfOc96Bus; hwSlice24_i ++)
       {
        for (hwSts24_i = 0; hwSts24_i < cNumbHwStsOnOc24Bus; hwSts24_i++)
            {
            mModuleHwWrite(self,
                           baseAddress + (uint32)cAf6Reg_vcat5g_sk_unfrm_pen((uint32)hwSlice24_i, (uint32)hwSts24_i),
                           0x0);
            }
       }
    }

static void HwMuxReset(Tha60210012ModuleConcate self)
    {
    const uint32 sinkMuxSelectPdhBus = 0xFFFFFFFF;
    uint8 hwSlice24_i;
    uint8 hwSts24_i;
    uint32 baseAddress = Tha60210012ModuleConcateBaseAddress(self);

    /* Reset for sink : Default*/
    for (hwSlice24_i = 0; hwSlice24_i < cNumbHwSliceOc24BusOfOc96Bus; hwSlice24_i ++)
       {
        for (hwSts24_i = 0; hwSts24_i < cNumbHwStsOnOc24Bus; hwSts24_i++)
            {
            mModuleHwWrite(self,
                           baseAddress + (uint32)cAf6Reg_vcat5g_sk_mux_pen((uint32)hwSlice24_i, (uint32)hwSts24_i),
                           sinkMuxSelectPdhBus);
            }
       }
    }

static void PrbsEngineDisable(Tha60210012ModuleConcate self)
    {
    uint32 baseAddress = Tha60210012ModuleConcateBaseAddress(self);
    mModuleHwWrite(self, baseAddress + cAf6Reg_vcat5g_sodat_pen_Base, 0);
    }

static void HwReset(Tha60210012ModuleConcate self)
    {
    mMethodsGet(self)->HwXcReset(self);
    mMethodsGet(self)->HwE1UnframeReset(self);
    mMethodsGet(self)->HwMuxReset(self);
    mMethodsGet(self)->PrbsEngineDisable(self);
    }

static void Activate(Tha60210012ModuleConcate self, eBool en)
    {
    uint32 regVal = 0x0;
    uint32 baseAddress = Tha60210012ModuleConcateBaseAddress(self);

    regVal = mModuleHwRead(self, baseAddress + cAf6Reg_vcat5g_master_pen);
    mRegFieldSet(regVal, cAf6_vcat5g_master_pen_Active_, en ? 0x1 : 0x0);
    mModuleHwWrite(self, baseAddress + cAf6Reg_vcat5g_master_pen, regVal);
    }

static eAtRet Init(AtModule self)
    {
    Tha60210012ModuleConcate concateModule = (Tha60210012ModuleConcate)self;
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    mMethodsGet(concateModule)->Activate(concateModule, cAtFalse);
    HwReset(concateModule);
    mMethodsGet(concateModule)->Activate(concateModule, cAtTrue);

    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Tha60210012ModuleConcateAsyncInit((AtObject)self);
    }

static uint32 MaxGroupGet(AtModuleConcate self)
    {
    AtUnused(self);
    return 256;
    }

static eBool IsNonVcatConcateType(eAtConcateGroupType concateType)
    {
    if ((concateType == cAtConcateGroupTypeNVcat)      ||
        (concateType == cAtConcateGroupTypeNVcat_g804) ||
        (concateType == cAtConcateGroupTypeNVcat_g8040))
        return cAtTrue;
    return cAtFalse;
    }

void Tha60210012ModuleConcateGlobalConcateTypeSet(AtModuleConcate self, eAtConcateGroupType concateType)
    {
	uint32 regVal = 0x0;
	uint32 baseAddress = Tha60210012ModuleConcateBaseAddress((Tha60210012ModuleConcate)self);

	regVal = mModuleHwRead(self, baseAddress + cAf6Reg_vcat5g_master_pen);
	mRegFieldSet(regVal, cAf6_vcat5g_master_pen_NonVcatMd_, IsNonVcatConcateType(concateType) ? 0x1 : 0x0);
	mModuleHwWrite(self, baseAddress + cAf6Reg_vcat5g_master_pen, regVal);
    return;
    }

static eBool GroupMemberTypeIsSupported(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    AtUnused(self);
    switch ((uint32)memberType)
        {
        case cAtConcateMemberTypeDs1:     return cAtTrue;
        case cAtConcateMemberTypeE1:      return cAtTrue;
        case cAtConcateMemberTypeDs3:     return cAtTrue;
        case cAtConcateMemberTypeE3:      return cAtTrue;
        default: return cAtFalse;
        }
    }

static eBool GroupTypeIsSupported(AtModuleConcate self, eAtConcateGroupType concateType)
    {
    AtUnused(self);
    switch ((uint32)concateType)
        {
        case cAtConcateGroupTypeVcat:           return cAtTrue;
        case cAtConcateGroupTypeNVcat:          return cAtTrue;
        case cAtConcateGroupTypeNVcat_g804:     return cAtTrue;
        case cAtConcateGroupTypeNVcat_g8040:    return cAtTrue;
        default: return cAtFalse;
        }
    }

static AtVcgBinder CreateVcgBinderForDe1(AtModuleConcate self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return (AtVcgBinder)Tha60210012VcgBinderPdhDe1New(de1);
    }

static AtVcgBinder CreateVcgBinderForDe3(AtModuleConcate self, AtPdhDe3 de3)
    {
    AtUnused(self);
    return (AtVcgBinder)Tha60210012VcgBinderPdhDe3New(de3);
    }

static AtConcateGroup VcatVcgObjectCreate(AtModuleConcate self, uint32 vcgId, eAtConcateMemberType memberType)
    {
    if (!Tha60210012ConcateGroupCanBeCreated(self, vcgId, cAtConcateGroupTypeVcat))
        return NULL;

    return (AtConcateGroup)Tha60210012VcgVcatNew(self, vcgId, memberType);
    }

static AtConcateGroup NonVcatVcgObjectCreate(AtModuleConcate self, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    if (!Tha60210012ConcateGroupCanBeCreated(self, vcgId, concateType))
        return NULL;

    return Tha60210012VcgNonVcatNew(self, vcgId, memberType, concateType);
    }

static eBool NoneVcatModeIsUsed(AtModuleConcate self)
    {
    uint32 baseAddress = Tha60210012ModuleConcateBaseAddress((Tha60210012ModuleConcate)self);
    uint32 regVal = mModuleHwRead(self, baseAddress + cAf6Reg_vcat5g_master_pen);
    return mBinToBool(mRegField(regVal, cAf6_vcat5g_master_pen_NonVcatMd_));
    }

static eBool IsVcgFixedToGfpChannel(AtModuleConcate self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleConcateRet ConcateGroupHwDelete(AtModuleConcate self, uint32 vcgId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleEncap encapModule = (AtModuleEncap) AtDeviceModuleGet(device, cAtModuleEncap);
    return Tha60210012ModuleEncapChannelEncapModeSet(encapModule, vcgId, cAtEncapUnknown);
    }

static eAtModuleConcateRet VcatVcgHwDelete(AtModuleConcate self, uint32 vcgId)
    {
    eAtRet ret = m_AtModuleConcateMethods->VcatVcgHwDelete(self, vcgId);
    if (ret != cAtOk)
        return ret;

    return ConcateGroupHwDelete(self, vcgId);
    }

static eAtModuleConcateRet NonVcatVcgHwDelete(AtModuleConcate self, uint32 vcgId)
    {
    eAtRet ret = m_AtModuleConcateMethods->NonVcatVcgHwDelete(self, vcgId);
    if (ret != cAtOk)
        return ret;

    return ConcateGroupHwDelete(self, vcgId);
    }

static void SoStickyDebug(AtModule self)
    {
    uint32 regAddress = BaseAddress(mThis(self)) + cAf6Reg_vcat5g_sostk_pen_Base;
    uint32 regValue = mModuleHwRead(self, regAddress);
    AtDebugger debugger = NULL;

    /* To clear Sticky */
    mModuleHwWrite(self, regAddress, regValue);

    Tha6021DebugPrintRegName (debugger, "* Debug VCAT Source Sticky", regAddress, regValue);
    Tha6021DebugPrintErrorBit(debugger, " Slice 3 LO PDH Adapt FiFo Empt", regValue, cAf6_vcat5g_sostk_pen_So_Slc3_LO_Empt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Slice 3 HO PDH Adapt FiFo Empt", regValue, cAf6_vcat5g_sostk_pen_So_Slc3_HO_Empt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Slice 3 LO PDH Adapt FiFo Full", regValue, cAf6_vcat5g_sostk_pen_So_Slc3_LO_Full_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Slice 3 HO PDH Adapt FiFo Full", regValue, cAf6_vcat5g_sostk_pen_So_Slc3_HO_Full_Mask);

    Tha6021DebugPrintErrorBit(debugger, " Slice 2 LO PDH Adapt FiFo Empt", regValue, cAf6_vcat5g_sostk_pen_So_Slc2_LO_Empt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Slice 2 HO PDH Adapt FiFo Empt", regValue, cAf6_vcat5g_sostk_pen_So_Slc2_HO_Empt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Slice 2 LO PDH Adapt FiFo Full", regValue, cAf6_vcat5g_sostk_pen_So_Slc2_LO_Full_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Slice 2 HO PDH Adapt FiFo Full", regValue, cAf6_vcat5g_sostk_pen_So_Slc2_HO_Full_Mask);

    Tha6021DebugPrintErrorBit(debugger, " Slice 1 LO PDH Adapt FiFo Empt", regValue, cAf6_vcat5g_sostk_pen_So_Slc1_LO_Empt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Slice 1 HO PDH Adapt FiFo Empt", regValue, cAf6_vcat5g_sostk_pen_So_Slc1_HO_Empt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Slice 1 LO PDH Adapt FiFo Full", regValue, cAf6_vcat5g_sostk_pen_So_Slc1_LO_Full_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Slice 1 HO PDH Adapt FiFo Full", regValue, cAf6_vcat5g_sostk_pen_So_Slc1_HO_Full_Mask);

    Tha6021DebugPrintErrorBit(debugger, " Slice 0 LO PDH Adapt FiFo Empt", regValue, cAf6_vcat5g_sostk_pen_So_Slc0_LO_Empt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Slice 0 HO PDH Adapt FiFo Empt", regValue, cAf6_vcat5g_sostk_pen_So_Slc0_HO_Empt_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Slice 0 LO PDH Adapt FiFo Full", regValue, cAf6_vcat5g_sostk_pen_So_Slc0_LO_Full_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Slice 0 HO PDH Adapt FiFo Full", regValue, cAf6_vcat5g_sostk_pen_So_Slc0_HO_Full_Mask);

    Tha6021DebugPrintErrorBit(debugger, " Ouput DS3 VLI Position Error", regValue, cAf6_vcat5g_sostk_pen_So_DS3VLI_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Ouput E3 VLI Position Error", regValue, cAf6_vcat5g_sostk_pen_So_E3VLI_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Ouput DS1 VLI Position Error", regValue, cAf6_vcat5g_sostk_pen_So_DS1VLI_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Ouput E1 VLI Position Error", regValue, cAf6_vcat5g_sostk_pen_So_E1VLI_Err_Mask);

    Tha6021DebugPrintInfoBit(debugger, "Output VLI from VCAT to PDH", regValue, cAf6_vcat5g_sostk_pen_So_PDH_Vli_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Output Valid from VCAT to PDH", regValue, cAf6_vcat5g_sostk_pen_So_PDH_Vld_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Request from PDH to VCAT", regValue, cAf6_vcat5g_sostk_pen_IPDH_Req_Mask);

    Tha6021DebugPrintInfoBit(debugger, "Source Slice 3 Read Valid", regValue, cAf6_vcat5g_sostk_pen_So_Slc3_Vld_Shift);
    Tha6021DebugPrintInfoBit(debugger, "Source Slice 2 Read Valid", regValue, cAf6_vcat5g_sostk_pen_So_Slc2_Vld_Shift);
    Tha6021DebugPrintInfoBit(debugger, "Source Slice 1 Read Valid", regValue, cAf6_vcat5g_sostk_pen_So_Slc1_Vld_Shift);
    Tha6021DebugPrintInfoBit(debugger, "Source Slice 0 Read Valid", regValue, cAf6_vcat5g_sostk_pen_So_Slc0_Vld_Shift);

    Tha6021DebugPrintInfoBit(debugger, "Source Member Valid", regValue, cAf6_vcat5g_sostk_pen_So_Mem_Vld_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Request from VCAT to ENC", regValue, cAf6_vcat5g_sostk_pen_So_Enc_Req_Mask);
    }

static void SkStickyDebug(AtModule self)
    {
    uint32 regAddress = BaseAddress(mThis(self)) + cAf6Reg_vcat5g_skstk_pen_Base;
    uint32 regValue = mModuleHwRead(self, regAddress);
    AtDebugger debugger = NULL;

    /* To clear Sticky */
    mModuleHwWrite(self, regAddress, regValue);

    Tha6021DebugPrintRegName (debugger, "* Debug VCAT Sink Sticky", regAddress, regValue);
    Tha6021DebugPrintErrorBit(debugger, " Sink Aligment FIFO Full", regValue, cAf6_vcat5g_skstk_pen_Sk_Alig_FFFull_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Sink Slice 3 FIFO Full", regValue, cAf6_vcat5g_skstk_pen_Sk_Slc3_FFFull_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Sink Slice 2 FIFO Full", regValue, cAf6_vcat5g_skstk_pen_Sk_Slc2_FFFull_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Sink Slice 1 FIFO Full", regValue, cAf6_vcat5g_skstk_pen_Sk_Slc1_FFFull_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Sink Slice 0 FIFO Full", regValue, cAf6_vcat5g_skstk_pen_Sk_Slc0_FFFull_Mask);

    Tha6021DebugPrintInfoBit(debugger, "Sequence Processing Output Valid to DEC", regValue, cAf6_vcat5g_skstk_pen_Sk_Seq_oVld_Mask);
    Tha6021DebugPrintInfoBit(debugger, "Sequence Processing Input Valid", regValue, cAf6_vcat5g_skstk_pen_Sk_Seq_iVld_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Sequence Processing Input LOM", regValue, cAf6_vcat5g_skstk_pen_Sk_Seq_iLom_Mask);
    }

static void DdrCachedStickyDebug(AtModule self)
    {
    uint32 regAddress = BaseAddress(mThis(self)) + cAf6Reg_vcat5g_ddrstk_pen_Base;
    uint32 regValue = mModuleHwRead(self, regAddress);
    AtDebugger debugger = NULL;

    /* To clear Sticky */
    mModuleHwWrite(self, regAddress, regValue);

    Tha6021DebugPrintRegName (debugger, "* Debug DDR Cache Sticky", regAddress, regValue);
    Tha6021DebugPrintInfoBit(debugger, "VCAT Write Cache Start of Segment", regValue, cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Sos_Mask);
    Tha6021DebugPrintInfoBit(debugger, "VCAT Write Cache End of Segment", regValue, cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Eos_Mask);
    Tha6021DebugPrintInfoBit(debugger, "VCAT Free Read Cache Ready", regValue, cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Rdy_Mask);

    Tha6021DebugPrintErrorBit(debugger, "  VCAT Free Cache Read Error", regValue, cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, "  VCAT Free Cache Read Get", regValue, cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Get_Mask);
    Tha6021DebugPrintErrorBit(debugger, "  VCAT Read Request FiFo Full", regValue, cAf6_vcat5g_ddrstk_pen_Vcat_Reqff_Full_Mask);
    Tha6021DebugPrintErrorBit(debugger, "  VCAT Read Cache Link FiFo Full", regValue, cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_LLfull_Mask);
    Tha6021DebugPrintErrorBit(debugger, "  VCAT Free Cache FiFo Empty", regValue, cAf6_vcat5g_ddrstk_pen_Vcat_FreeCa_Empt_Mask);
    Tha6021DebugPrintInfoBit(debugger, " DDR Data Write Done", regValue, cAf6_vcat5g_ddrstk_pen_DDR_Wr_Done_Mask);
    Tha6021DebugPrintInfoBit(debugger, " DDR Read Data Enable from PLA", regValue, cAf6_vcat5g_ddrstk_pen_DDR_Rd_Ena_Mask);
    Tha6021DebugPrintInfoBit(debugger, " DDR Read Data End of Segment", regValue, cAf6_vcat5g_ddrstk_pen_DDR_Rd_Eop_Mask);
    Tha6021DebugPrintInfoBit(debugger, " DDR Read Data Valid", regValue, cAf6_vcat5g_ddrstk_pen_DDR_Rd_Vld_Mask);
    Tha6021DebugPrintInfoBit(debugger, " Request Read from VCAT to PLA", regValue, cAf6_vcat5g_ddrstk_pen_VCAT_Read_Mask);
    Tha6021DebugPrintInfoBit(debugger, " Request Write from VCAT to PLA", regValue, cAf6_vcat5g_ddrstk_pen_VCAT_Write_Mask);
    Tha6021DebugPrintInfoBit(debugger, " Delay Compensate request Read", regValue, cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Read_Mask);
    Tha6021DebugPrintInfoBit(debugger, " Delay Compensate request Write", regValue, cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Write_Mask);
    }

static void DdrCachedInfo(AtModule self)
    {
    AtDebugger debugger = NULL;
    uint32 regAddress = BaseAddress(mThis(self)) + cAf6Reg_vcat5g_ddrblk_pen_Base;
    uint32 regValue = mModuleHwRead(self, regAddress);
    Tha6021DebugPrintRegName (debugger, "* Debug DDR Free Read Cache Number", regAddress, regValue);
    Tha6021DebugPrintBitFieldVal(debugger, " VCAT Free Read Cache Number", regValue, cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Err_Mask, cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Err_Shift);
    }

static void VcgBertInfo(AtModule self)
    {
    AtDebugger debugger = NULL;
    uint32 regAddress = BaseAddress(mThis(self)) + cAf6Reg_vcat5g_sodat_pen_Base;
    uint32 regValue = mModuleHwRead(self, regAddress);
    Tha6021DebugPrintRegName (debugger, "* VCAT Data Control", regAddress, regValue);
    Tha6021DebugPrintBitFieldVal(debugger, " Select Mode for Data Generator/Monitor", regValue, cAf6_vcat5g_sodat_pen_Cfg_Dat_Mod_Mask, cAf6_vcat5g_sodat_pen_Cfg_Dat_Mod_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " Enable Data Generator/Monitor", regValue, cAf6_vcat5g_sodat_pen_Cfg_Dat_Ena_Mask, cAf6_vcat5g_sodat_pen_Cfg_Dat_Ena_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " Select VCG for Data Generator/Monitor", regValue, cAf6_vcat5g_sodat_pen_Cfg_Dat_VCG_Mask, cAf6_vcat5g_sodat_pen_Cfg_Dat_VCG_Shift);

    regAddress = BaseAddress(mThis(self)) + cAf6Reg_vcat5g_sodatmon_pen_Base;
    regValue = mModuleHwRead(self, regAddress);
    Tha6021DebugPrintRegName (debugger, "* VCAT Data Error Counter", regAddress, regValue);
    Tha6021DebugPrintBitFieldVal(debugger, " Data Error Counter", regValue, cAf6_vcat5g_sodatmon_pen_Dat_Err_Cnt_Mask, cAf6_vcat5g_sodatmon_pen_Dat_Err_Cnt_Shift);
    }

static eAtRet Debug(AtModule self)
    {
    SoStickyDebug(self);
    SkStickyDebug(self);
    DdrCachedStickyDebug(self);
    DdrCachedInfo(self);
    VcgBertInfo(self);
    AtPrintc(cSevNormal, "\r\n");
    return cAtOk;
    }

static void DumpDataShow(AtModuleConcate self, uint32 startAddress, uint32 stopAddress)
    {
    static char buf[50];
    uint32 idx = 0;
    uint32 value;
    uint32 numRegisters = stopAddress - startAddress;
    uint32 offsetCount = 0;

    while (offsetCount <= numRegisters)
        {
        AtSprintf(buf, "\r\n%08X: ", startAddress);
        AtPrintc(cSevNormal, "%s", buf);
        AtOsalMemInit(buf, 0x0, sizeof(buf));
        for (idx = 0; (idx < 4) && (offsetCount <= numRegisters); idx++)
            {
            value = mModuleHwRead(self, startAddress);
            startAddress++;
            offsetCount++;
            AtSprintf(buf, "%08lX   ", (long unsigned int)value);
            AtPrintc(cSevNormal, "%s", buf);
            }
        }

    AtPrintf("\r\n");
    }

static void DataDumpTrigger(AtModuleConcate self, uint8 dumpMode, uint8 selBus)
    {
    uint32 registerAddress = cAf6Reg_vcat5g_dumpctrl_pen_Base + BaseAddress(mThis(self));
    uint32 regVal = mModuleHwRead(self, registerAddress);

    mRegFieldSet(regVal, cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Mod_, dumpMode);
    mRegFieldSet(regVal, cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Sel_, selBus);
    mModuleHwWrite(self, registerAddress, regVal);
    }

static void DdrDumpTrigger(AtModuleConcate self, uint32 lineId, uint8 selBus)
    {
    uint32 registerAddress = cAf6Reg_vcat5g_ddrdumpctrl_pen_Base + BaseAddress(mThis(self));
    uint32 regVal = mModuleHwRead(self, registerAddress);

    mRegFieldSet(regVal, cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Lid_, lineId);
    mRegFieldSet(regVal, cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Sel_, selBus);
    mModuleHwWrite(self, registerAddress, regVal);
    }

static void DataDump(AtModuleConcate self, uint8 dumpMode, uint8 selBus)
    {
    uint32 baseAddress = BaseAddress((Tha60210012ModuleConcate) self);
    uint32 startAddress = baseAddress + cAf6Reg_vcat5g_dumpdat_pen_Base;
    uint32 endAddress = startAddress + 8192UL;

    DataDumpTrigger(self, dumpMode, selBus);
    AtOsalSleep(1);
    DumpDataShow(self, startAddress, endAddress);
    }

static void DdrDump(AtModuleConcate self, uint32 lineId, uint8 selBus)
    {
    uint32 baseAddress  = BaseAddress((Tha60210012ModuleConcate) self);
    uint32 startAddress = baseAddress + cAf6Reg_vcat5g_ddrdumpdat_pen_Base;
    uint32 endAddress   = startAddress + 255UL;

    DdrDumpTrigger(self, lineId, selBus);
    AtOsalSleep(1);

    DumpDataShow(self, startAddress, endAddress);
    }

static uint8 HwStsSlice24ToMapStsId(Tha60210012ModuleConcate self, uint8 hwPdhSliceId, uint8 hwPdhStsId)
    {
    uint8 sts3LocalId = (hwPdhStsId % 3);
    uint8 sts3BlockId = (hwPdhStsId / 3);
    AtUnused(self);
    return (uint8)((sts3LocalId * 4) + hwPdhSliceId + (sts3BlockId * 12));
    }

static void HwMapStsIdSliceSts24Get(Tha60210012ModuleConcate self, uint8 mapStsId, uint8 *hwPdhSliceId, uint8 *hwPdhStsId)
    {
    uint8 sts12LocalId = (mapStsId % 12);
    uint8 sts12BlockId = (mapStsId / 12);
    AtUnused(self);

    *hwPdhSliceId = (uint8)(sts12LocalId % 4);
    *hwPdhStsId = (uint8)((sts12LocalId / 4) + (sts12BlockId * 3));
    }

static void MethodsInit(Tha60210012ModuleConcate self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, HwXcReset);
        mMethodOverride(m_methods, HwE1UnframeReset);
        mMethodOverride(m_methods, HwMuxReset);
        mMethodOverride(m_methods, PrbsEngineDisable);
        mMethodOverride(m_methods, Activate);
        mMethodOverride(m_methods, HwStsSlice24ToMapStsId);
        mMethodOverride(m_methods, HwMapStsIdSliceSts24Get);
        }

    mMethodsSet(self, &m_methods);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210012ModuleConcate object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(asyncInitState);
    }

static void OverrideAtObject(AtModuleConcate self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleConcate self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, HoldReadRegistersGet);
        mMethodOverride(m_AtModuleOverride, HoldWriteRegistersGet);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleConcate(AtModuleConcate self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleConcateMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleConcateOverride, mMethodsGet(self), sizeof(m_AtModuleConcateOverride));

        mMethodOverride(m_AtModuleConcateOverride, MaxGroupGet);
        mMethodOverride(m_AtModuleConcateOverride, GroupMemberTypeIsSupported);
        mMethodOverride(m_AtModuleConcateOverride, GroupTypeIsSupported);
        mMethodOverride(m_AtModuleConcateOverride, CreateVcgBinderForDe1);
        mMethodOverride(m_AtModuleConcateOverride, CreateVcgBinderForDe3);
        mMethodOverride(m_AtModuleConcateOverride, NonVcatVcgObjectCreate);
        mMethodOverride(m_AtModuleConcateOverride, VcatVcgObjectCreate);
        mMethodOverride(m_AtModuleConcateOverride, IsVcgFixedToGfpChannel);
        mMethodOverride(m_AtModuleConcateOverride, VcatVcgHwDelete);
        mMethodOverride(m_AtModuleConcateOverride, NonVcatVcgHwDelete);
        }

    mMethodsSet(self, &m_AtModuleConcateOverride);
    }

static void Override(AtModuleConcate self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleConcate(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleConcate);
    }

AtModuleConcate Tha60210012ModuleConcateObjectInit(AtModuleConcate self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleConcateObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModuleConcate Tha60210012ModuleConcateNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleConcate concateModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (concateModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModuleConcateObjectInit(concateModule, device);
    }

uint32 Tha60210012ModuleConcateBaseAddress(Tha60210012ModuleConcate self)
    {
    return (self) ? mMethodsGet(self)->BaseAddress(self) : 0x0;
    }

eBool Tha60210012ConcateGroupCanBeCreated(AtModuleConcate self, uint32 vcgId, eAtConcateGroupType concateType)
    {
    if (AtModuleConcateNumCreatedGroups(self) == 0)
        return cAtTrue;

    if (AtModuleConcateGroupGet(self, vcgId) != NULL)
        return cAtFalse;

    if (IsNonVcatConcateType(concateType) == NoneVcatModeIsUsed(self))
        return cAtTrue;

    return cAtFalse;
    }

uint8 Tha60210012ModuleConcateHwStsSlice24ToMapStsId(Tha60210012ModuleConcate self, uint8 hwPdhSliceId, uint8 hwPdhStsId)
    {
    return (uint8)((self) ? mMethodsGet(self)->HwStsSlice24ToMapStsId(self, hwPdhSliceId, hwPdhStsId) : cInvalidUint8);
    }

uint8 Tha60210012ModuleConcateHwStsSlice48ToMapStsId(Tha60210012ModuleConcate self, uint8 slice48, uint8 hwSts48)
    {
	uint8 slice24 = (uint8) (slice48 * 2 + hwSts48 % 2);
	uint8 hwSts24 = (uint8) (hwSts48 / 2);
	return Tha60210012ModuleConcateHwStsSlice24ToMapStsId(self, slice24, hwSts24);
    }

void Tha60210012ModuleConcateMapStsIdToStsAndSlice24(Tha60210012ModuleConcate self, uint8 sts96, uint8 *slice24, uint8 *sts24)
    {
    if (self)
        mMethodsGet(self)->HwMapStsIdSliceSts24Get(self, sts96, slice24, sts24);
    }

void Tha60210012ModuleConcateDataDump(AtModuleConcate self, uint8 dumpMode, uint8 selBus)
    {
    if (self)
        DataDump(self, dumpMode, selBus);
    }

void Tha60210012ModuleConcateDdrDump(AtModuleConcate self, uint32 lineId, uint8 selBus)
    {
    if (self)
        DdrDump(self, lineId, selBus);
    }

eAtRet Tha60210012ModuleConcateSuperAsyncInit(AtObject self)
    {
    return m_AtModuleMethods->AsyncInit((AtModule)self);
    }

eAtRet Tha60210012ModuleConcateDeactivate(AtObject self)
    {
    if (self)
        mMethodsGet(mThis(self))->Activate(mThis(self), cAtFalse);
    return cAtOk;
    }

eAtRet Tha60210012ModuleConcateHwXcReset(AtObject self)
    {
    if (self)
        mMethodsGet(mThis(self))->HwXcReset(mThis(self));
    return cAtOk;
    }

eAtRet Tha60210012ModuleConcateHwE1UnframeReset(AtObject self)
    {
    if (self)
        mMethodsGet(mThis(self))->HwE1UnframeReset(mThis(self));
    return cAtOk;
    }

eAtRet Tha60210012ModuleConcateHwMuxReset(AtObject self)
    {
    if (self)
        mMethodsGet(mThis(self))->HwMuxReset(mThis(self));
    return cAtOk;
    }

eAtRet Tha60210012ModuleConcatePrbsEngineDisable(AtObject self)
    {
    if (self)
        mMethodsGet(mThis(self))->PrbsEngineDisable(mThis(self));
    return cAtOk;
    }

eAtRet Tha60210012ModuleConcateActivate(AtObject self)
    {
    if (self)
        mMethodsGet(mThis(self))->Activate(mThis(self), cAtTrue);
    return cAtOk;
    }

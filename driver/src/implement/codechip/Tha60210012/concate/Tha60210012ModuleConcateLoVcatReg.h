/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0012_RD_VCAT_H_
#define _AF6_REG_AF6CCI0012_RD_VCAT_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Version ID Register
Reg Addr   : 0x00000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is version of VCAT block.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_version_pen_Base                                                                0x00000
#define cAf6Reg_vcat5g_version_pen                                                                     0x00000
#define cAf6Reg_vcat5g_version_pen_WidthVal                                                                 32
#define cAf6Reg_vcat5g_version_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: year
BitField Type: R_O
BitField Desc: year
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_vcat5g_version_pen_year_Bit_Start                                                              12
#define cAf6_vcat5g_version_pen_year_Bit_End                                                                15
#define cAf6_vcat5g_version_pen_year_Mask                                                            cBit15_12
#define cAf6_vcat5g_version_pen_year_Shift                                                                  12
#define cAf6_vcat5g_version_pen_year_MaxVal                                                                0xf
#define cAf6_vcat5g_version_pen_year_MinVal                                                                0x0
#define cAf6_vcat5g_version_pen_year_RstVal                                                               0x0F

/*--------------------------------------
BitField Name: week
BitField Type: R_O
BitField Desc: week number
BitField Bits: [11:04]
--------------------------------------*/
#define cAf6_vcat5g_version_pen_week_Bit_Start                                                               4
#define cAf6_vcat5g_version_pen_week_Bit_End                                                                11
#define cAf6_vcat5g_version_pen_week_Mask                                                             cBit11_4
#define cAf6_vcat5g_version_pen_week_Shift                                                                   4
#define cAf6_vcat5g_version_pen_week_MaxVal                                                               0xff
#define cAf6_vcat5g_version_pen_week_MinVal                                                                0x0
#define cAf6_vcat5g_version_pen_week_RstVal                                                               0x23

/*--------------------------------------
BitField Name: number
BitField Type: R_O
BitField Desc: number in week
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_vcat5g_version_pen_number_Bit_Start                                                             0
#define cAf6_vcat5g_version_pen_number_Bit_End                                                               3
#define cAf6_vcat5g_version_pen_number_Mask                                                            cBit3_0
#define cAf6_vcat5g_version_pen_number_Shift                                                                 0
#define cAf6_vcat5g_version_pen_number_MaxVal                                                              0xf
#define cAf6_vcat5g_version_pen_number_MinVal                                                              0x0
#define cAf6_vcat5g_version_pen_number_RstVal                                                             0x00


/*------------------------------------------------------------------------------
Reg Name   : Master Configuration
Reg Addr   : 0x00001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register control global configuration for MAP.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_master_pen_Base                                                                 0x00001
#define cAf6Reg_vcat5g_master_pen                                                                      0x00001
#define cAf6Reg_vcat5g_master_pen_WidthVal                                                                  32
#define cAf6Reg_vcat5g_master_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: VcatLoop
BitField Type: R/W
BitField Desc: Non-VCAT Mode
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_master_pen_VcatLoop_Bit_Start                                                            2
#define cAf6_vcat5g_master_pen_VcatLoop_Bit_End                                                              2
#define cAf6_vcat5g_master_pen_VcatLoop_Mask                                                             cBit2
#define cAf6_vcat5g_master_pen_VcatLoop_Shift                                                                2
#define cAf6_vcat5g_master_pen_VcatLoop_MaxVal                                                             0x1
#define cAf6_vcat5g_master_pen_VcatLoop_MinVal                                                             0x0
#define cAf6_vcat5g_master_pen_VcatLoop_RstVal                                                             0x0

/*--------------------------------------
BitField Name: NonVcatMd
BitField Type: R/W
BitField Desc: Non-VCAT Mode
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_master_pen_NonVcatMd_Bit_Start                                                           1
#define cAf6_vcat5g_master_pen_NonVcatMd_Bit_End                                                             1
#define cAf6_vcat5g_master_pen_NonVcatMd_Mask                                                            cBit1
#define cAf6_vcat5g_master_pen_NonVcatMd_Shift                                                               1
#define cAf6_vcat5g_master_pen_NonVcatMd_MaxVal                                                            0x1
#define cAf6_vcat5g_master_pen_NonVcatMd_MinVal                                                            0x0
#define cAf6_vcat5g_master_pen_NonVcatMd_RstVal                                                            0x0

/*--------------------------------------
BitField Name: Active
BitField Type: R/W
BitField Desc: Engine Active
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_master_pen_Active_Bit_Start                                                              0
#define cAf6_vcat5g_master_pen_Active_Bit_End                                                                0
#define cAf6_vcat5g_master_pen_Active_Mask                                                               cBit0
#define cAf6_vcat5g_master_pen_Active_Shift                                                                  0
#define cAf6_vcat5g_master_pen_Active_MaxVal                                                               0x1
#define cAf6_vcat5g_master_pen_Active_MinVal                                                               0x0
#define cAf6_vcat5g_master_pen_Active_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Write Access Data Holding 1
Reg Addr   : 0x00002
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to write configuration or status that is greater than 32-bit wide.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_wrhold1_pen_Base                                                                0x00002
#define cAf6Reg_vcat5g_wrhold1_pen                                                                     0x00002
#define cAf6Reg_vcat5g_wrhold1_pen_WidthVal                                                                 32
#define cAf6Reg_vcat5g_wrhold1_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: WrDat_Hold1
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_vcat5g_wrhold1_pen_WrDat_Hold1_Bit_Start                                                        0
#define cAf6_vcat5g_wrhold1_pen_WrDat_Hold1_Bit_End                                                         31
#define cAf6_vcat5g_wrhold1_pen_WrDat_Hold1_Mask                                                      cBit31_0
#define cAf6_vcat5g_wrhold1_pen_WrDat_Hold1_Shift                                                            0
#define cAf6_vcat5g_wrhold1_pen_WrDat_Hold1_MaxVal                                                  0xffffffff
#define cAf6_vcat5g_wrhold1_pen_WrDat_Hold1_MinVal                                                         0x0
#define cAf6_vcat5g_wrhold1_pen_WrDat_Hold1_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Write Access Data Holding 2
Reg Addr   : 0x00003
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to write configuration or status that is greater than 32-bit wide.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_wrhold2_pen_Base                                                                0x00003
#define cAf6Reg_vcat5g_wrhold2_pen                                                                     0x00003
#define cAf6Reg_vcat5g_wrhold2_pen_WidthVal                                                                 32
#define cAf6Reg_vcat5g_wrhold2_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: WrDat_Hold2
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_vcat5g_wrhold2_pen_WrDat_Hold2_Bit_Start                                                        0
#define cAf6_vcat5g_wrhold2_pen_WrDat_Hold2_Bit_End                                                         31
#define cAf6_vcat5g_wrhold2_pen_WrDat_Hold2_Mask                                                      cBit31_0
#define cAf6_vcat5g_wrhold2_pen_WrDat_Hold2_Shift                                                            0
#define cAf6_vcat5g_wrhold2_pen_WrDat_Hold2_MaxVal                                                  0xffffffff
#define cAf6_vcat5g_wrhold2_pen_WrDat_Hold2_MinVal                                                         0x0
#define cAf6_vcat5g_wrhold2_pen_WrDat_Hold2_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Read Access Data Holding 1
Reg Addr   : 0x00004
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to read configuration or status that is greater than 32-bit wide.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_rdhold1_pen_Base                                                                0x00004
#define cAf6Reg_vcat5g_rdhold1_pen                                                                     0x00004
#define cAf6Reg_vcat5g_rdhold1_pen_WidthVal                                                                 32
#define cAf6Reg_vcat5g_rdhold1_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: RdDat_Hold1
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_vcat5g_rdhold1_pen_RdDat_Hold1_Bit_Start                                                        0
#define cAf6_vcat5g_rdhold1_pen_RdDat_Hold1_Bit_End                                                         31
#define cAf6_vcat5g_rdhold1_pen_RdDat_Hold1_Mask                                                      cBit31_0
#define cAf6_vcat5g_rdhold1_pen_RdDat_Hold1_Shift                                                            0
#define cAf6_vcat5g_rdhold1_pen_RdDat_Hold1_MaxVal                                                  0xffffffff
#define cAf6_vcat5g_rdhold1_pen_RdDat_Hold1_MinVal                                                         0x0
#define cAf6_vcat5g_rdhold1_pen_RdDat_Hold1_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Read Access Data Holding 2
Reg Addr   : 0x00005
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to read configuration or status that is greater than 32-bit wide.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_rdhold2_pen_Base                                                                0x00005
#define cAf6Reg_vcat5g_rdhold2_pen                                                                     0x00005
#define cAf6Reg_vcat5g_rdhold2_pen_WidthVal                                                                 32
#define cAf6Reg_vcat5g_rdhold2_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: RdDat_Hold2
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_vcat5g_rdhold2_pen_RdDat_Hold2_Bit_Start                                                        0
#define cAf6_vcat5g_rdhold2_pen_RdDat_Hold2_Bit_End                                                         31
#define cAf6_vcat5g_rdhold2_pen_RdDat_Hold2_Mask                                                      cBit31_0
#define cAf6_vcat5g_rdhold2_pen_RdDat_Hold2_Shift                                                            0
#define cAf6_vcat5g_rdhold2_pen_RdDat_Hold2_MaxVal                                                  0xffffffff
#define cAf6_vcat5g_rdhold2_pen_RdDat_Hold2_MinVal                                                         0x0
#define cAf6_vcat5g_rdhold2_pen_RdDat_Hold2_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Source VC4 Mode Configuration
Reg Addr   : 0x21040
Reg Formula: 
    Where  : 
Reg Desc   : 
Source VC4 Mode Enable Control. For VC4 mode, if STS master is n,
the two slave-STSs will be n+64 and n+128 (where n = 0 - 63)

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_vc4md_pen_Base                                                               0x21040
#define cAf6Reg_vcat5g_so_vc4md_pen                                                                    0x21040
#define cAf6Reg_vcat5g_so_vc4md_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_so_vc4md_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: So_VC4_Ena
BitField Type: R/W
BitField Desc: Source VC4 Mode Enable
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_vcat5g_so_vc4md_pen_So_VC4_Ena_Bit_Start                                                        0
#define cAf6_vcat5g_so_vc4md_pen_So_VC4_Ena_Bit_End                                                         31
#define cAf6_vcat5g_so_vc4md_pen_So_VC4_Ena_Mask                                                      cBit31_0
#define cAf6_vcat5g_so_vc4md_pen_So_VC4_Ena_Shift                                                            0
#define cAf6_vcat5g_so_vc4md_pen_So_VC4_Ena_MaxVal                                                  0xffffffff
#define cAf6_vcat5g_so_vc4md_pen_So_VC4_Ena_MinVal                                                         0x0
#define cAf6_vcat5g_so_vc4md_pen_So_VC4_Ena_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Source Mode Configuration
Reg Addr   : 0x21200-0x2125f
Reg Formula: 0x21200 + $sts_id
    Where  : 
           + $sts_id(0-95)
Reg Desc   : 
This register is used to config timing mode for VCAT Source side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_ctrl_pen_Base                                                                0x21200
#define cAf6Reg_vcat5g_so_ctrl_pen(stsid)                                                    (0x21200+(stsid))
#define cAf6Reg_vcat5g_so_ctrl_pen_WidthVal                                                                 32
#define cAf6Reg_vcat5g_so_ctrl_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: So_HoMd
BitField Type: R/W
BitField Desc: 00: SPE-VC3 Mode, 01: DS3 Mode, 10: E3 Mode, 11: Lo-order Mode
(DS1 or E1 or VT1.5 or VT2)
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_HoMd_Bit_Start                                                           16
#define cAf6_vcat5g_so_ctrl_pen_So_HoMd_Bit_End                                                             17
#define cAf6_vcat5g_so_ctrl_pen_So_HoMd_Mask                                                         cBit17_16
#define cAf6_vcat5g_so_ctrl_pen_So_HoMd_Shift                                                               16
#define cAf6_vcat5g_so_ctrl_pen_So_HoMd_MaxVal                                                             0x3
#define cAf6_vcat5g_so_ctrl_pen_So_HoMd_MinVal                                                             0x0
#define cAf6_vcat5g_so_ctrl_pen_So_HoMd_RstVal                                                             0x0

/*--------------------------------------
BitField Name: So_LoMd_Grp_6
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_6_Bit_Start                                                     12
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_6_Bit_End                                                       13
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_6_Mask                                                   cBit13_12
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_6_Shift                                                         12
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_6_MaxVal                                                       0x3
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_6_MinVal                                                       0x0
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_6_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_LoMd_Grp_5
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_5_Bit_Start                                                     10
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_5_Bit_End                                                       11
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_5_Mask                                                   cBit11_10
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_5_Shift                                                         10
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_5_MaxVal                                                       0x3
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_5_MinVal                                                       0x0
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_5_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_LoMd_Grp_4
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_4_Bit_Start                                                      8
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_4_Bit_End                                                        9
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_4_Mask                                                     cBit9_8
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_4_Shift                                                          8
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_4_MaxVal                                                       0x3
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_4_MinVal                                                       0x0
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_4_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_LoMd_Grp_3
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_3_Bit_Start                                                      6
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_3_Bit_End                                                        7
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_3_Mask                                                     cBit7_6
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_3_Shift                                                          6
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_3_MaxVal                                                       0x3
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_3_MinVal                                                       0x0
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_3_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_LoMd_Grp_2
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_2_Bit_Start                                                      4
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_2_Bit_End                                                        5
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_2_Mask                                                     cBit5_4
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_2_Shift                                                          4
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_2_MaxVal                                                       0x3
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_2_MinVal                                                       0x0
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_2_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_LoMd_Grp_1
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_1_Bit_Start                                                      2
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_1_Bit_End                                                        3
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_1_Mask                                                     cBit3_2
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_1_Shift                                                          2
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_1_MaxVal                                                       0x3
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_1_MinVal                                                       0x0
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_1_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_LoMd_Grp_0
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_0_Bit_Start                                                      0
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_0_Bit_End                                                        1
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_0_Mask                                                     cBit1_0
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_0_Shift                                                          0
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_0_MaxVal                                                       0x3
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_0_MinVal                                                       0x0
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Grp_0_RstVal                                                       0x0

#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Mask(grpId)                       (cBit1_0 << grpId * 2)
#define cAf6_vcat5g_so_ctrl_pen_So_LoMd_Shift(grpId)                      (grpId * 2)
/*------------------------------------------------------------------------------
Reg Name   : Source E1-Unframed Mode Configuration
Reg Addr   : 0x21400-0x2145f
Reg Formula: 0x21400 + $sts_id
    Where  : 
           + $sts_id(0-95)
Reg Desc   : 
This register is used to config mode for VCAT Source side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_unfrm_pen_Base                                                               0x21400
#define cAf6Reg_vcat5g_so_unfrm_pen(stsid)                                                   (0x21400+(stsid))
#define cAf6Reg_vcat5g_so_unfrm_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_so_unfrm_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: So_E1Unfrm
BitField Type: R/W
BitField Desc: 1: E1-Unframed Mode, 0: E1-Framed Mode (default)
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_so_unfrm_pen_So_E1Unfrm_Bit_Start                                                        0
#define cAf6_vcat5g_so_unfrm_pen_So_E1Unfrm_Bit_End                                                         27
#define cAf6_vcat5g_so_unfrm_pen_So_E1Unfrm_Mask                                                      cBit27_0
#define cAf6_vcat5g_so_unfrm_pen_So_E1Unfrm_Shift                                                            0
#define cAf6_vcat5g_so_unfrm_pen_So_E1Unfrm_MaxVal                                                   0xfffffff
#define cAf6_vcat5g_so_unfrm_pen_So_E1Unfrm_MinVal                                                         0x0
#define cAf6_vcat5g_so_unfrm_pen_So_E1Unfrm_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Source Member Control Configuration
Reg Addr   : 0x22000-0x22bff
Reg Formula: 0x22000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-95), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to control engine VCAT Source side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_memctrl_pen_Base                                                             0x22000
#define cAf6Reg_vcat5g_so_memctrl_pen(stsid)                                 (0x22000+(stsid)*32+vtgid*4+vtid)
#define cAf6Reg_vcat5g_so_memctrl_pen_WidthVal                                                              32
#define cAf6Reg_vcat5g_so_memctrl_pen_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: So_Mem_Ena
BitField Type: R/W
BitField Desc: 1: Enable Member, 0: Disable Member
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_vcat5g_so_memctrl_pen_So_Mem_Ena_Bit_Start                                                     16
#define cAf6_vcat5g_so_memctrl_pen_So_Mem_Ena_Bit_End                                                       16
#define cAf6_vcat5g_so_memctrl_pen_So_Mem_Ena_Mask                                                      cBit16
#define cAf6_vcat5g_so_memctrl_pen_So_Mem_Ena_Shift                                                         16
#define cAf6_vcat5g_so_memctrl_pen_So_Mem_Ena_MaxVal                                                       0x1
#define cAf6_vcat5g_so_memctrl_pen_So_Mem_Ena_MinVal                                                       0x0
#define cAf6_vcat5g_so_memctrl_pen_So_Mem_Ena_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_G8040_Md
BitField Type: R/W
BitField Desc: 1: ITU G.8040 Mode, 0: ITU G.804 Mode
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_vcat5g_so_memctrl_pen_So_G8040_Md_Bit_Start                                                    15
#define cAf6_vcat5g_so_memctrl_pen_So_G8040_Md_Bit_End                                                      15
#define cAf6_vcat5g_so_memctrl_pen_So_G8040_Md_Mask                                                     cBit15
#define cAf6_vcat5g_so_memctrl_pen_So_G8040_Md_Shift                                                        15
#define cAf6_vcat5g_so_memctrl_pen_So_G8040_Md_MaxVal                                                      0x1
#define cAf6_vcat5g_so_memctrl_pen_So_G8040_Md_MinVal                                                      0x0
#define cAf6_vcat5g_so_memctrl_pen_So_G8040_Md_RstVal                                                      0x0

/*--------------------------------------
BitField Name: So_Vcat_Md
BitField Type: R/W
BitField Desc: 1: VCAT Mode, 0: non-VCAT Mode
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_vcat5g_so_memctrl_pen_So_Vcat_Md_Bit_Start                                                     14
#define cAf6_vcat5g_so_memctrl_pen_So_Vcat_Md_Bit_End                                                       14
#define cAf6_vcat5g_so_memctrl_pen_So_Vcat_Md_Mask                                                      cBit14
#define cAf6_vcat5g_so_memctrl_pen_So_Vcat_Md_Shift                                                         14
#define cAf6_vcat5g_so_memctrl_pen_So_Vcat_Md_MaxVal                                                       0x1
#define cAf6_vcat5g_so_memctrl_pen_So_Vcat_Md_MinVal                                                       0x0
#define cAf6_vcat5g_so_memctrl_pen_So_Vcat_Md_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Lcas_Md
BitField Type: R/W
BitField Desc: LCAS Enable
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_vcat5g_so_memctrl_pen_So_Lcas_Md_Bit_Start                                                     13
#define cAf6_vcat5g_so_memctrl_pen_So_Lcas_Md_Bit_End                                                       13
#define cAf6_vcat5g_so_memctrl_pen_So_Lcas_Md_Mask                                                      cBit13
#define cAf6_vcat5g_so_memctrl_pen_So_Lcas_Md_Shift                                                         13
#define cAf6_vcat5g_so_memctrl_pen_So_Lcas_Md_MaxVal                                                       0x1
#define cAf6_vcat5g_so_memctrl_pen_So_Lcas_Md_MinVal                                                       0x0
#define cAf6_vcat5g_so_memctrl_pen_So_Lcas_Md_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Nms_Cmd
BitField Type: R/W
BitField Desc: Network Management Command
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_vcat5g_so_memctrl_pen_So_Nms_Cmd_Bit_Start                                                     12
#define cAf6_vcat5g_so_memctrl_pen_So_Nms_Cmd_Bit_End                                                       12
#define cAf6_vcat5g_so_memctrl_pen_So_Nms_Cmd_Mask                                                      cBit12
#define cAf6_vcat5g_so_memctrl_pen_So_Nms_Cmd_Shift                                                         12
#define cAf6_vcat5g_so_memctrl_pen_So_Nms_Cmd_MaxVal                                                       0x1
#define cAf6_vcat5g_so_memctrl_pen_So_Nms_Cmd_MinVal                                                       0x0
#define cAf6_vcat5g_so_memctrl_pen_So_Nms_Cmd_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_VcgId
BitField Type: R/W
BitField Desc: VCG ID 0-511
BitField Bits: [08:00]
--------------------------------------*/
#define cAf6_vcat5g_so_memctrl_pen_So_VcgId_Bit_Start                                                        0
#define cAf6_vcat5g_so_memctrl_pen_So_VcgId_Bit_End                                                          8
#define cAf6_vcat5g_so_memctrl_pen_So_VcgId_Mask                                                       cBit8_0
#define cAf6_vcat5g_so_memctrl_pen_So_VcgId_Shift                                                            0
#define cAf6_vcat5g_so_memctrl_pen_So_VcgId_MaxVal                                                       0x1ff
#define cAf6_vcat5g_so_memctrl_pen_So_VcgId_MinVal                                                         0x0
#define cAf6_vcat5g_so_memctrl_pen_So_VcgId_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Source Member Status
Reg Addr   : 0x23000-0x23bff
Reg Formula: 0x23000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-95), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to show status of member at Source side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_memsta_pen_Base                                                              0x23000
#define cAf6Reg_vcat5g_so_memsta_pen(stsid)                                  (0x23000+(stsid)*32+vtgid*4+vtid)
#define cAf6Reg_vcat5g_so_memsta_pen_WidthVal                                                               32
#define cAf6Reg_vcat5g_so_memsta_pen_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: So_Mem_Fsm
BitField Type: RO
BitField Desc: Member FSM
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Fsm_Bit_Start                                                      16
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Fsm_Bit_End                                                        18
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Fsm_Mask                                                    cBit18_16
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Fsm_Shift                                                          16
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Fsm_MaxVal                                                        0x7
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Fsm_MinVal                                                        0x0
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Fsm_RstVal                                                        0x0

/*--------------------------------------
BitField Name: So_Mem_Sta
BitField Type: RO
BitField Desc: Member Status
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Sta_Bit_Start                                                      15
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Sta_Bit_End                                                        15
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Sta_Mask                                                       cBit15
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Sta_Shift                                                          15
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Sta_MaxVal                                                        0x1
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Sta_MinVal                                                        0x0
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Sta_RstVal                                                        0x0

/*--------------------------------------
BitField Name: So_Mem_Act
BitField Type: RO
BitField Desc: Member Active
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Act_Bit_Start                                                      14
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Act_Bit_End                                                        14
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Act_Mask                                                       cBit14
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Act_Shift                                                          14
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Act_MaxVal                                                        0x1
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Act_MinVal                                                        0x0
#define cAf6_vcat5g_so_memsta_pen_So_Mem_Act_RstVal                                                        0x0

/*--------------------------------------
BitField Name: So_Mem_TxSeq
BitField Type: RO
BitField Desc: Source Member Sequence Transmitted
BitField Bits: [13:07]
--------------------------------------*/
#define cAf6_vcat5g_so_memsta_pen_So_Mem_TxSeq_Bit_Start                                                     7
#define cAf6_vcat5g_so_memsta_pen_So_Mem_TxSeq_Bit_End                                                      13
#define cAf6_vcat5g_so_memsta_pen_So_Mem_TxSeq_Mask                                                   cBit13_7
#define cAf6_vcat5g_so_memsta_pen_So_Mem_TxSeq_Shift                                                         7
#define cAf6_vcat5g_so_memsta_pen_So_Mem_TxSeq_MaxVal                                                     0x7f
#define cAf6_vcat5g_so_memsta_pen_So_Mem_TxSeq_MinVal                                                      0x0
#define cAf6_vcat5g_so_memsta_pen_So_Mem_TxSeq_RstVal                                                      0x0

/*--------------------------------------
BitField Name: So_Mem_AcpSeq
BitField Type: RO
BitField Desc: Source Member Accept Sequence
BitField Bits: [06:00]
--------------------------------------*/
#define cAf6_vcat5g_so_memsta_pen_So_Mem_AcpSeq_Bit_Start                                                    0
#define cAf6_vcat5g_so_memsta_pen_So_Mem_AcpSeq_Bit_End                                                      6
#define cAf6_vcat5g_so_memsta_pen_So_Mem_AcpSeq_Mask                                                   cBit6_0
#define cAf6_vcat5g_so_memsta_pen_So_Mem_AcpSeq_Shift                                                        0
#define cAf6_vcat5g_so_memsta_pen_So_Mem_AcpSeq_MaxVal                                                    0x7f
#define cAf6_vcat5g_so_memsta_pen_So_Mem_AcpSeq_MinVal                                                     0x0
#define cAf6_vcat5g_so_memsta_pen_So_Mem_AcpSeq_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Source Transmit Sequence Configuration
Reg Addr   : 0x24000-0x24bff
Reg Formula: 0x24000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-95), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to assign sequence transmitted for VCAT mode only.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_txseqmem_pen_Base                                                            0x24000
#define cAf6Reg_vcat5g_so_txseqmem_pen(stsid)                                (0x24000+(stsid)*32+vtgid*4+vtid)
#define cAf6Reg_vcat5g_so_txseqmem_pen_WidthVal                                                             32
#define cAf6Reg_vcat5g_so_txseqmem_pen_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: So_TxSeqEna
BitField Type: R/W
BitField Desc: 0: Disable config, 1: Enable config
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeqEna_Bit_Start                                                    8
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeqEna_Bit_End                                                      8
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeqEna_Mask                                                     cBit8
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeqEna_Shift                                                        8
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeqEna_MaxVal                                                     0x1
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeqEna_MinVal                                                     0x0
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeqEna_RstVal                                                     0x0

/*--------------------------------------
BitField Name: So_TxSeq
BitField Type: R/W
BitField Desc: Source Member Sequence Transmitted
BitField Bits: [06:00]
--------------------------------------*/
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeq_Bit_Start                                                       0
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeq_Bit_End                                                         6
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeq_Mask                                                      cBit6_0
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeq_Shift                                                           0
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeq_MaxVal                                                       0x7f
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeq_MinVal                                                        0x0
#define cAf6_vcat5g_so_txseqmem_pen_So_TxSeq_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Output OC-24 STS Connection Memory
Reg Addr   : 0x25000-0x2505f
Reg Formula: 0x25000 + $sts_id
    Where  : 
           + $sts_id(0-95)
Reg Desc   : 
Connection Memory for Output 4-Slice OC-24

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_so_stsxc_pen_Base                                                               0x25000
#define cAf6Reg_vcat5g_so_stsxc_pen(stsid)                                                   (0x25000+(stsid))
#define cAf6Reg_vcat5g_so_stsxc_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_so_stsxc_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: OC24_OutSlice
BitField Type: R/W
BitField Desc: Output Slice 0-3
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSlice_Bit_Start                                                     8
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSlice_Bit_End                                                       9
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSlice_Mask                                                    cBit9_8
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSlice_Shift                                                         8
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSlice_MaxVal                                                      0x3
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSlice_MinVal                                                      0x0
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSlice_RstVal                                                      0x0

/*--------------------------------------
BitField Name: OC24_OutSTS
BitField Type: R/W
BitField Desc: Output STS 0-23
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSTS_Bit_Start                                                       0
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSTS_Bit_End                                                         4
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSTS_Mask                                                      cBit4_0
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSTS_Shift                                                           0
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSTS_MaxVal                                                       0x1f
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSTS_MinVal                                                        0x0
#define cAf6_vcat5g_so_stsxc_pen_OC24_OutSTS_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Input OC-24 STS Connection Memory
Reg Addr   : 0x42000-0x4207f
Reg Formula: 0x42000 + $slice_id*32 + $sts_id
    Where  : 
           + $slice_id(0-3)
           + $sts_id(0-23)
Reg Desc   : 
Connection Memory for Input 4-Slice OC-24 to OC-96

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_stsxc_pen_Base                                                               0x42000
#define cAf6Reg_vcat5g_sk_stsxc_pen(sliceid, stsid)                             (0x42000+(sliceid)*32+(stsid))
#define cAf6Reg_vcat5g_sk_stsxc_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_sk_stsxc_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: OC96_OutSTS
BitField Type: R/W
BitField Desc: Output STS 0-95
BitField Bits: [06:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_stsxc_pen_OC96_OutSTS_Bit_Start                                                       0
#define cAf6_vcat5g_sk_stsxc_pen_OC96_OutSTS_Bit_End                                                         6
#define cAf6_vcat5g_sk_stsxc_pen_OC96_OutSTS_Mask                                                      cBit6_0
#define cAf6_vcat5g_sk_stsxc_pen_OC96_OutSTS_Shift                                                           0
#define cAf6_vcat5g_sk_stsxc_pen_OC96_OutSTS_MaxVal                                                       0x7f
#define cAf6_vcat5g_sk_stsxc_pen_OC96_OutSTS_MinVal                                                        0x0
#define cAf6_vcat5g_sk_stsxc_pen_OC96_OutSTS_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 0 Mode Configuration
Reg Addr   : 0x40800-0x40817
Reg Formula: 0x40800 + $sts_id
    Where  : 
           + $sts_id(0-23)
Reg Desc   : 
This register is used to config timing mode for VCAT Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl0_ctrl_pen_Base                                                            0x40800
#define cAf6Reg_vcat5g_sk_sl0_ctrl_pen(stsid)                                                (0x40800+(stsid))
#define cAf6Reg_vcat5g_sk_sl0_ctrl_pen_WidthVal                                                             32
#define cAf6Reg_vcat5g_sk_sl0_ctrl_pen_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: Sk_sl0_HoMd
BitField Type: R/W
BitField Desc: 00: SPE-VC3 Mode, 01: DS3 Mode, 10: E3 Mode, 11: Lo-order Mode
(DS1 or E1 or VT1.5 or VT2)
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_HoMd_Bit_Start                                                   16
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_HoMd_Bit_End                                                     17
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_HoMd_Mask                                                 cBit17_16
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_HoMd_Shift                                                       16
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_HoMd_MaxVal                                                     0x3
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_HoMd_MinVal                                                     0x0
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_HoMd_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_6
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_6_Bit_Start                                             12
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_6_Bit_End                                               13
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_6_Mask                                           cBit13_12
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_6_Shift                                                 12
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_6_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_6_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_6_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_5
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_5_Bit_Start                                             10
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_5_Bit_End                                               11
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_5_Mask                                           cBit11_10
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_5_Shift                                                 10
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_5_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_5_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_5_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_4
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_4_Bit_Start                                              8
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_4_Bit_End                                                9
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_4_Mask                                             cBit9_8
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_4_Shift                                                  8
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_4_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_4_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_4_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_3
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_3_Bit_Start                                              6
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_3_Bit_End                                                7
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_3_Mask                                             cBit7_6
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_3_Shift                                                  6
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_3_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_3_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_3_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_2
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_2_Bit_Start                                              4
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_2_Bit_End                                                5
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_2_Mask                                             cBit5_4
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_2_Shift                                                  4
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_2_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_2_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_2_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_1
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_1_Bit_Start                                              2
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_1_Bit_End                                                3
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_1_Mask                                             cBit3_2
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_1_Shift                                                  2
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_1_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_1_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_1_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl0_LoMd_Grp_0
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_0_Bit_Start                                              0
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_0_Bit_End                                                1
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_0_Mask                                             cBit1_0
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_0_Shift                                                  0
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_0_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_0_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl0_ctrl_pen_Sk_sl0_LoMd_Grp_0_RstVal                                               0x0


#define cAf6Reg_vcat5g_sk_ctrl_pen(slice, stsid)                          (0x40800 + (stsid) + ((slice)* 0x100UL))
#define cAf6_vcat5g_so_ctrl_pen_Sk_LoMd_Mask(grpId)                       (cBit1_0 << grpId * 2)
#define cAf6_vcat5g_so_ctrl_pen_Sk_LoMd_Shift(grpId)                      (grpId * 2)

/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 0 E1-Unframed Mode Configuration
Reg Addr   : 0x40840-0x40857
Reg Formula: 0x40840 + $sts_id
    Where  : 
           + $sts_id(0-23)
Reg Desc   : 
This register is used to config mode for VCAT Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl0_unfrm_pen_Base                                                           0x40840
#define cAf6Reg_vcat5g_sk_sl0_unfrm_pen(stsid)                                               (0x40840+(stsid))
#define cAf6Reg_vcat5g_sk_sl0_unfrm_pen_WidthVal                                                            32
#define cAf6Reg_vcat5g_sk_sl0_unfrm_pen_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: Sk_sl0_E1Unfrm
BitField Type: R/W
BitField Desc: 1: E1-Unframed Mode, 0: E1-Framed Mode (default)
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_unfrm_pen_Sk_sl0_E1Unfrm_Bit_Start                                                0
#define cAf6_vcat5g_sk_sl0_unfrm_pen_Sk_sl0_E1Unfrm_Bit_End                                                 27
#define cAf6_vcat5g_sk_sl0_unfrm_pen_Sk_sl0_E1Unfrm_Mask                                              cBit27_0
#define cAf6_vcat5g_sk_sl0_unfrm_pen_Sk_sl0_E1Unfrm_Shift                                                    0
#define cAf6_vcat5g_sk_sl0_unfrm_pen_Sk_sl0_E1Unfrm_MaxVal                                           0xfffffff
#define cAf6_vcat5g_sk_sl0_unfrm_pen_Sk_sl0_E1Unfrm_MinVal                                                 0x0
#define cAf6_vcat5g_sk_sl0_unfrm_pen_Sk_sl0_E1Unfrm_RstVal                                                 0x0

#define cAf6Reg_vcat5g_sk_unfrm_pen(slice, stsid)                          (0x40840+(stsid) + ((slice)* 0x100UL))
/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 0 Input Mux Configuration
Reg Addr   : 0x40880-0x40897
Reg Formula: 0x40880 + $sts_id
    Where  : 
           + $sts_id(0-23)
Reg Desc   : 
This register is used to select bus running at Sink side input.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl0_mux_pen_Base                                                             0x40880
#define cAf6Reg_vcat5g_sk_sl0_mux_pen(stsid)                                                 (0x40880+(stsid))
#define cAf6Reg_vcat5g_sk_sl0_mux_pen_WidthVal                                                              32
#define cAf6Reg_vcat5g_sk_sl0_mux_pen_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: Sk_sl0_Mux
BitField Type: R/W
BitField Desc: 1: Select from PDH bus (DS3/E3/DS1/E1), 0: Select from STS/VT Bus
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl0_mux_pen_Sk_sl0_Mux_Bit_Start                                                      0
#define cAf6_vcat5g_sk_sl0_mux_pen_Sk_sl0_Mux_Bit_End                                                       27
#define cAf6_vcat5g_sk_sl0_mux_pen_Sk_sl0_Mux_Mask                                                    cBit27_0
#define cAf6_vcat5g_sk_sl0_mux_pen_Sk_sl0_Mux_Shift                                                          0
#define cAf6_vcat5g_sk_sl0_mux_pen_Sk_sl0_Mux_MaxVal                                                 0xfffffff
#define cAf6_vcat5g_sk_sl0_mux_pen_Sk_sl0_Mux_MinVal                                                       0x0
#define cAf6_vcat5g_sk_sl0_mux_pen_Sk_sl0_Mux_RstVal                                                       0x0

#define cAf6Reg_vcat5g_sk_mux_pen(slice, stsid)                          (0x40880+(stsid) + ((slice)* 0x100UL))
/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 1 Mode Configuration
Reg Addr   : 0x40900-0x40917
Reg Formula: 0x40900 + $sts_id
    Where  : 
           + $sts_id(0-23)
Reg Desc   : 
This register is used to config timing mode for VCAT Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl1_ctrl_pen_Base                                                            0x40900
#define cAf6Reg_vcat5g_sk_sl1_ctrl_pen(stsid)                                                (0x40900+(stsid))
#define cAf6Reg_vcat5g_sk_sl1_ctrl_pen_WidthVal                                                             32
#define cAf6Reg_vcat5g_sk_sl1_ctrl_pen_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: Sk_sl1_HoMd
BitField Type: R/W
BitField Desc: 00: SPE-VC3 Mode, 01: DS3 Mode, 10: E3 Mode, 11: Lo-order Mode
(DS1 or E1 or VT1.5 or VT2)
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_HoMd_Bit_Start                                                   16
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_HoMd_Bit_End                                                     17
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_HoMd_Mask                                                 cBit17_16
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_HoMd_Shift                                                       16
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_HoMd_MaxVal                                                     0x3
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_HoMd_MinVal                                                     0x0
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_HoMd_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_6
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_6_Bit_Start                                             12
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_6_Bit_End                                               13
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_6_Mask                                           cBit13_12
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_6_Shift                                                 12
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_6_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_6_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_6_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_5
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_5_Bit_Start                                             10
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_5_Bit_End                                               11
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_5_Mask                                           cBit11_10
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_5_Shift                                                 10
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_5_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_5_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_5_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_4
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_4_Bit_Start                                              8
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_4_Bit_End                                                9
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_4_Mask                                             cBit9_8
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_4_Shift                                                  8
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_4_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_4_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_4_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_3
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_3_Bit_Start                                              6
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_3_Bit_End                                                7
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_3_Mask                                             cBit7_6
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_3_Shift                                                  6
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_3_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_3_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_3_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_2
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_2_Bit_Start                                              4
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_2_Bit_End                                                5
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_2_Mask                                             cBit5_4
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_2_Shift                                                  4
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_2_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_2_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_2_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_1
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_1_Bit_Start                                              2
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_1_Bit_End                                                3
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_1_Mask                                             cBit3_2
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_1_Shift                                                  2
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_1_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_1_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_1_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl1_LoMd_Grp_0
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_0_Bit_Start                                              0
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_0_Bit_End                                                1
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_0_Mask                                             cBit1_0
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_0_Shift                                                  0
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_0_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_0_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl1_ctrl_pen_Sk_sl1_LoMd_Grp_0_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 1 E1-Unframed Mode Configuration
Reg Addr   : 0x40940-0x40957
Reg Formula: 0x40940 + $sts_id
    Where  : 
           + $sts_id(0-23)
Reg Desc   : 
This register is used to config mode for VCAT Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl1_unfrm_pen_Base                                                           0x40940
#define cAf6Reg_vcat5g_sk_sl1_unfrm_pen(stsid)                                               (0x40940+(stsid))
#define cAf6Reg_vcat5g_sk_sl1_unfrm_pen_WidthVal                                                            32
#define cAf6Reg_vcat5g_sk_sl1_unfrm_pen_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: Sk_sl1_E1Unfrm
BitField Type: R/W
BitField Desc: 1: E1-Unframed Mode, 0: E1-Framed Mode (default)
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_unfrm_pen_Sk_sl1_E1Unfrm_Bit_Start                                                0
#define cAf6_vcat5g_sk_sl1_unfrm_pen_Sk_sl1_E1Unfrm_Bit_End                                                 27
#define cAf6_vcat5g_sk_sl1_unfrm_pen_Sk_sl1_E1Unfrm_Mask                                              cBit27_0
#define cAf6_vcat5g_sk_sl1_unfrm_pen_Sk_sl1_E1Unfrm_Shift                                                    0
#define cAf6_vcat5g_sk_sl1_unfrm_pen_Sk_sl1_E1Unfrm_MaxVal                                           0xfffffff
#define cAf6_vcat5g_sk_sl1_unfrm_pen_Sk_sl1_E1Unfrm_MinVal                                                 0x0
#define cAf6_vcat5g_sk_sl1_unfrm_pen_Sk_sl1_E1Unfrm_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 1 Input Mux Configuration
Reg Addr   : 0x40980-0x40997
Reg Formula: 0x40980 + $sts_id
    Where  : 
           + $sts_id(0-23)
Reg Desc   : 
This register is used to select bus running at Sink side input.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl1_mux_pen_Base                                                             0x40980
#define cAf6Reg_vcat5g_sk_sl1_mux_pen(stsid)                                                 (0x40980+(stsid))
#define cAf6Reg_vcat5g_sk_sl1_mux_pen_WidthVal                                                              32
#define cAf6Reg_vcat5g_sk_sl1_mux_pen_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: Sk_sl1_Mux
BitField Type: R/W
BitField Desc: 1: Select from PDH bus (DS3/E3/DS1/E1), 0: Select from STS/VT Bus
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl1_mux_pen_Sk_sl1_Mux_Bit_Start                                                      0
#define cAf6_vcat5g_sk_sl1_mux_pen_Sk_sl1_Mux_Bit_End                                                       27
#define cAf6_vcat5g_sk_sl1_mux_pen_Sk_sl1_Mux_Mask                                                    cBit27_0
#define cAf6_vcat5g_sk_sl1_mux_pen_Sk_sl1_Mux_Shift                                                          0
#define cAf6_vcat5g_sk_sl1_mux_pen_Sk_sl1_Mux_MaxVal                                                 0xfffffff
#define cAf6_vcat5g_sk_sl1_mux_pen_Sk_sl1_Mux_MinVal                                                       0x0
#define cAf6_vcat5g_sk_sl1_mux_pen_Sk_sl1_Mux_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 2 Mode Configuration
Reg Addr   : 0x40a00-0x40a17
Reg Formula: 0x40a00 + $sts_id
    Where  : 
           + $sts_id(0-23)
Reg Desc   : 
This register is used to config timing mode for VCAT Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl2_ctrl_pen_Base                                                            0x40a00
#define cAf6Reg_vcat5g_sk_sl2_ctrl_pen(stsid)                                                (0x40a00+(stsid))
#define cAf6Reg_vcat5g_sk_sl2_ctrl_pen_WidthVal                                                             32
#define cAf6Reg_vcat5g_sk_sl2_ctrl_pen_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: Sk_sl2_HoMd
BitField Type: R/W
BitField Desc: 00: SPE-VC3 Mode, 01: DS3 Mode, 10: E3 Mode, 11: Lo-order Mode
(DS1 or E1 or VT1.5 or VT2)
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_HoMd_Bit_Start                                                   16
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_HoMd_Bit_End                                                     17
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_HoMd_Mask                                                 cBit17_16
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_HoMd_Shift                                                       16
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_HoMd_MaxVal                                                     0x3
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_HoMd_MinVal                                                     0x0
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_HoMd_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Sk_sl2_LoMd_Grp_6
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_6_Bit_Start                                             12
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_6_Bit_End                                               13
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_6_Mask                                           cBit13_12
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_6_Shift                                                 12
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_6_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_6_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_6_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl2_LoMd_Grp_5
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_5_Bit_Start                                             10
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_5_Bit_End                                               11
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_5_Mask                                           cBit11_10
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_5_Shift                                                 10
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_5_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_5_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_5_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl2_LoMd_Grp_4
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_4_Bit_Start                                              8
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_4_Bit_End                                                9
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_4_Mask                                             cBit9_8
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_4_Shift                                                  8
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_4_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_4_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_4_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl2_LoMd_Grp_3
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_3_Bit_Start                                              6
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_3_Bit_End                                                7
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_3_Mask                                             cBit7_6
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_3_Shift                                                  6
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_3_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_3_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_3_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl2_LoMd_Grp_2
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_2_Bit_Start                                              4
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_2_Bit_End                                                5
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_2_Mask                                             cBit5_4
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_2_Shift                                                  4
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_2_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_2_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_2_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl2_LoMd_Grp_1
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_1_Bit_Start                                              2
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_1_Bit_End                                                3
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_1_Mask                                             cBit3_2
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_1_Shift                                                  2
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_1_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_1_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_1_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl2_LoMd_Grp_0
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_0_Bit_Start                                              0
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_0_Bit_End                                                1
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_0_Mask                                             cBit1_0
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_0_Shift                                                  0
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_0_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_0_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl2_ctrl_pen_Sk_sl2_LoMd_Grp_0_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 2 E1-Unframed Mode Configuration
Reg Addr   : 0x40a40-0x40a57
Reg Formula: 0x40a40 + $sts_id
    Where  : 
           + $sts_id(0-23)
Reg Desc   : 
This register is used to config mode for VCAT Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl2_unfrm_pen_Base                                                           0x40a40
#define cAf6Reg_vcat5g_sk_sl2_unfrm_pen(stsid)                                               (0x40a40+(stsid))
#define cAf6Reg_vcat5g_sk_sl2_unfrm_pen_WidthVal                                                            32
#define cAf6Reg_vcat5g_sk_sl2_unfrm_pen_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: Sk_sl2_E1Unfrm
BitField Type: R/W
BitField Desc: 1: E1-Unframed Mode, 0: E1-Framed Mode (default)
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl2_unfrm_pen_Sk_sl2_E1Unfrm_Bit_Start                                                0
#define cAf6_vcat5g_sk_sl2_unfrm_pen_Sk_sl2_E1Unfrm_Bit_End                                                 27
#define cAf6_vcat5g_sk_sl2_unfrm_pen_Sk_sl2_E1Unfrm_Mask                                              cBit27_0
#define cAf6_vcat5g_sk_sl2_unfrm_pen_Sk_sl2_E1Unfrm_Shift                                                    0
#define cAf6_vcat5g_sk_sl2_unfrm_pen_Sk_sl2_E1Unfrm_MaxVal                                           0xfffffff
#define cAf6_vcat5g_sk_sl2_unfrm_pen_Sk_sl2_E1Unfrm_MinVal                                                 0x0
#define cAf6_vcat5g_sk_sl2_unfrm_pen_Sk_sl2_E1Unfrm_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 2 Input Mux Configuration
Reg Addr   : 0x40a80-0x40a97
Reg Formula: 0x40a80 + $sts_id
    Where  : 
           + $sts_id(0-23)
Reg Desc   : 
This register is used to select bus running at Sink side input.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl2_mux_pen_Base                                                             0x40a80
#define cAf6Reg_vcat5g_sk_sl2_mux_pen(stsid)                                                 (0x40a80+(stsid))
#define cAf6Reg_vcat5g_sk_sl2_mux_pen_WidthVal                                                              32
#define cAf6Reg_vcat5g_sk_sl2_mux_pen_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: Sk_sl2_Mux
BitField Type: R/W
BitField Desc: 1: Select from PDH bus (DS3/E3/DS1/E1), 0: Select from STS/VT Bus
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl2_mux_pen_Sk_sl2_Mux_Bit_Start                                                      0
#define cAf6_vcat5g_sk_sl2_mux_pen_Sk_sl2_Mux_Bit_End                                                       27
#define cAf6_vcat5g_sk_sl2_mux_pen_Sk_sl2_Mux_Mask                                                    cBit27_0
#define cAf6_vcat5g_sk_sl2_mux_pen_Sk_sl2_Mux_Shift                                                          0
#define cAf6_vcat5g_sk_sl2_mux_pen_Sk_sl2_Mux_MaxVal                                                 0xfffffff
#define cAf6_vcat5g_sk_sl2_mux_pen_Sk_sl2_Mux_MinVal                                                       0x0
#define cAf6_vcat5g_sk_sl2_mux_pen_Sk_sl2_Mux_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 3 Mode Configuration
Reg Addr   : 0x40b00-0x40b17
Reg Formula: 0x40b00 + $sts_id
    Where  : 
           + $sts_id(0-23)
Reg Desc   : 
This register is used to config timing mode for VCAT Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl3_ctrl_pen_Base                                                            0x40b00
#define cAf6Reg_vcat5g_sk_sl3_ctrl_pen(stsid)                                                (0x40b00+(stsid))
#define cAf6Reg_vcat5g_sk_sl3_ctrl_pen_WidthVal                                                             32
#define cAf6Reg_vcat5g_sk_sl3_ctrl_pen_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: Sk_sl3_HoMd
BitField Type: R/W
BitField Desc: 00: SPE-VC3 Mode, 01: DS3 Mode, 10: E3 Mode, 11: Lo-order Mode
(DS1 or E1 or VT1.5 or VT2)
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_HoMd_Bit_Start                                                   16
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_HoMd_Bit_End                                                     17
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_HoMd_Mask                                                 cBit17_16
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_HoMd_Shift                                                       16
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_HoMd_MaxVal                                                     0x3
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_HoMd_MinVal                                                     0x0
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_HoMd_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Sk_sl3_LoMd_Grp_6
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_6_Bit_Start                                             12
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_6_Bit_End                                               13
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_6_Mask                                           cBit13_12
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_6_Shift                                                 12
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_6_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_6_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_6_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl3_LoMd_Grp_5
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_5_Bit_Start                                             10
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_5_Bit_End                                               11
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_5_Mask                                           cBit11_10
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_5_Shift                                                 10
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_5_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_5_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_5_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl3_LoMd_Grp_4
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_4_Bit_Start                                              8
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_4_Bit_End                                                9
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_4_Mask                                             cBit9_8
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_4_Shift                                                  8
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_4_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_4_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_4_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl3_LoMd_Grp_3
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_3_Bit_Start                                              6
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_3_Bit_End                                                7
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_3_Mask                                             cBit7_6
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_3_Shift                                                  6
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_3_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_3_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_3_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl3_LoMd_Grp_2
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_2_Bit_Start                                              4
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_2_Bit_End                                                5
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_2_Mask                                             cBit5_4
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_2_Shift                                                  4
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_2_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_2_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_2_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl3_LoMd_Grp_1
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_1_Bit_Start                                              2
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_1_Bit_End                                                3
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_1_Mask                                             cBit3_2
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_1_Shift                                                  2
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_1_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_1_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_1_RstVal                                               0x0

/*--------------------------------------
BitField Name: Sk_sl3_LoMd_Grp_0
BitField Type: R/W
BitField Desc: 00: VT1.5 Mode, 01: VT2 Mode, 10: DS1 Mode, 11: E1 Mode
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_0_Bit_Start                                              0
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_0_Bit_End                                                1
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_0_Mask                                             cBit1_0
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_0_Shift                                                  0
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_0_MaxVal                                               0x3
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_0_MinVal                                               0x0
#define cAf6_vcat5g_sk_sl3_ctrl_pen_Sk_sl3_LoMd_Grp_0_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 3 E1-Unframed Mode Configuration
Reg Addr   : 0x40b40-0x40b57
Reg Formula: 0x40b40 + $sts_id
    Where  : 
           + $sts_id(0-23)
Reg Desc   : 
This register is used to config mode for VCAT Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl3_unfrm_pen_Base                                                           0x40b40
#define cAf6Reg_vcat5g_sk_sl3_unfrm_pen(stsid)                                               (0x40b40+(stsid))
#define cAf6Reg_vcat5g_sk_sl3_unfrm_pen_WidthVal                                                            32
#define cAf6Reg_vcat5g_sk_sl3_unfrm_pen_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: Sk_sl3__E1Unfrm
BitField Type: R/W
BitField Desc: 1: E1-Unframed Mode, 0: E1-Framed Mode (default)
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl3_unfrm_pen_Sk_sl3__E1Unfrm_Bit_Start                                               0
#define cAf6_vcat5g_sk_sl3_unfrm_pen_Sk_sl3__E1Unfrm_Bit_End                                                27
#define cAf6_vcat5g_sk_sl3_unfrm_pen_Sk_sl3__E1Unfrm_Mask                                             cBit27_0
#define cAf6_vcat5g_sk_sl3_unfrm_pen_Sk_sl3__E1Unfrm_Shift                                                   0
#define cAf6_vcat5g_sk_sl3_unfrm_pen_Sk_sl3__E1Unfrm_MaxVal                                          0xfffffff
#define cAf6_vcat5g_sk_sl3_unfrm_pen_Sk_sl3__E1Unfrm_MinVal                                                0x0
#define cAf6_vcat5g_sk_sl3_unfrm_pen_Sk_sl3__E1Unfrm_RstVal                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Slice 3 Input Mux Configuration
Reg Addr   : 0x40b80-0x40b97
Reg Formula: 0x40b80 + $sts_id
    Where  : 
           + $sts_id(0-23)
Reg Desc   : 
This register is used to select bus running at Sink side input.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_sl3_mux_pen_Base                                                             0x40b80
#define cAf6Reg_vcat5g_sk_sl3_mux_pen(stsid)                                                 (0x40b80+(stsid))
#define cAf6Reg_vcat5g_sk_sl3_mux_pen_WidthVal                                                              32
#define cAf6Reg_vcat5g_sk_sl3_mux_pen_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: Sk_sl3_Mux
BitField Type: R/W
BitField Desc: 1: Select from PDH bus (DS3/E3/DS1/E1), 0: Select from STS/VT Bus
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_sl3_mux_pen_Sk_sl3_Mux_Bit_Start                                                      0
#define cAf6_vcat5g_sk_sl3_mux_pen_Sk_sl3_Mux_Bit_End                                                       27
#define cAf6_vcat5g_sk_sl3_mux_pen_Sk_sl3_Mux_Mask                                                    cBit27_0
#define cAf6_vcat5g_sk_sl3_mux_pen_Sk_sl3_Mux_Shift                                                          0
#define cAf6_vcat5g_sk_sl3_mux_pen_Sk_sl3_Mux_MaxVal                                                 0xfffffff
#define cAf6_vcat5g_sk_sl3_mux_pen_Sk_sl3_Mux_MinVal                                                       0x0
#define cAf6_vcat5g_sk_sl3_mux_pen_Sk_sl3_Mux_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Member Control Configuration
Reg Addr   : 0x49000-0x49bff
Reg Formula: 0x49000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-95), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to control engine VCAT Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_memctrl_pen_Base                                                             0x49000
#define cAf6Reg_vcat5g_sk_memctrl_pen(stsid)                                 (0x49000+(stsid)*32+vtgid*4+vtid)
#define cAf6Reg_vcat5g_sk_memctrl_pen_WidthVal                                                              32
#define cAf6Reg_vcat5g_sk_memctrl_pen_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: Sk_Mem_Ena
BitField Type: R/W
BitField Desc: 1: Enable Member, 0: Disable Member
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Mem_Ena_Bit_Start                                                     16
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Mem_Ena_Bit_End                                                       16
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Mem_Ena_Mask                                                      cBit16
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Mem_Ena_Shift                                                         16
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Mem_Ena_MaxVal                                                       0x1
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Mem_Ena_MinVal                                                       0x0
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Mem_Ena_RstVal                                                       0x0

/*--------------------------------------
BitField Name: Sk_G8040_Md
BitField Type: R/W
BitField Desc: 1: ITU G.8040 Mode, 0: ITU G.804 Mode
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_vcat5g_sk_memctrl_pen_Sk_G8040_Md_Bit_Start                                                    15
#define cAf6_vcat5g_sk_memctrl_pen_Sk_G8040_Md_Bit_End                                                      15
#define cAf6_vcat5g_sk_memctrl_pen_Sk_G8040_Md_Mask                                                     cBit15
#define cAf6_vcat5g_sk_memctrl_pen_Sk_G8040_Md_Shift                                                        15
#define cAf6_vcat5g_sk_memctrl_pen_Sk_G8040_Md_MaxVal                                                      0x1
#define cAf6_vcat5g_sk_memctrl_pen_Sk_G8040_Md_MinVal                                                      0x0
#define cAf6_vcat5g_sk_memctrl_pen_Sk_G8040_Md_RstVal                                                      0x0

/*--------------------------------------
BitField Name: Sk_Vcat_Md
BitField Type: R/W
BitField Desc: 1: VCAT Mode, 0: non-VCAT Mode
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Vcat_Md_Bit_Start                                                     14
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Vcat_Md_Bit_End                                                       14
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Vcat_Md_Mask                                                      cBit14
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Vcat_Md_Shift                                                         14
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Vcat_Md_MaxVal                                                       0x1
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Vcat_Md_MinVal                                                       0x0
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Vcat_Md_RstVal                                                       0x0

/*--------------------------------------
BitField Name: Sk_Lcas_Md
BitField Type: R/W
BitField Desc: LCAS Enable
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Lcas_Md_Bit_Start                                                     13
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Lcas_Md_Bit_End                                                       13
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Lcas_Md_Mask                                                      cBit13
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Lcas_Md_Shift                                                         13
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Lcas_Md_MaxVal                                                       0x1
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Lcas_Md_MinVal                                                       0x0
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Lcas_Md_RstVal                                                       0xX

/*--------------------------------------
BitField Name: Sk_Nms_Cmd
BitField Type: R/W
BitField Desc: Network Management Command
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Nms_Cmd_Bit_Start                                                     12
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Nms_Cmd_Bit_End                                                       12
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Nms_Cmd_Mask                                                      cBit12
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Nms_Cmd_Shift                                                         12
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Nms_Cmd_MaxVal                                                       0x1
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Nms_Cmd_MinVal                                                       0x0
#define cAf6_vcat5g_sk_memctrl_pen_Sk_Nms_Cmd_RstVal                                                       0xX

/*--------------------------------------
BitField Name: Sk_VcgId
BitField Type: R/W
BitField Desc: VCG ID 0-511
BitField Bits: [08:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_memctrl_pen_Sk_VcgId_Bit_Start                                                        0
#define cAf6_vcat5g_sk_memctrl_pen_Sk_VcgId_Bit_End                                                          8
#define cAf6_vcat5g_sk_memctrl_pen_Sk_VcgId_Mask                                                       cBit8_0
#define cAf6_vcat5g_sk_memctrl_pen_Sk_VcgId_Shift                                                            0
#define cAf6_vcat5g_sk_memctrl_pen_Sk_VcgId_MaxVal                                                       0x1ff
#define cAf6_vcat5g_sk_memctrl_pen_Sk_VcgId_MinVal                                                         0x0
#define cAf6_vcat5g_sk_memctrl_pen_Sk_VcgId_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Member Status
Reg Addr   : 0x51000-0x51bff
Reg Formula: 0x51000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-95), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to show status of member at Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_memsta_pen_Base                                                              0x51000
#define cAf6Reg_vcat5g_sk_memsta_pen(stsid)                                  (0x51000+(stsid)*32+vtgid*4+vtid)
#define cAf6Reg_vcat5g_sk_memsta_pen_WidthVal                                                               32
#define cAf6Reg_vcat5g_sk_memsta_pen_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: Sk_Mem_EnaSta
BitField Type: RO
BitField Desc: Member Enable Status
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_EnaSta_Bit_Start                                                   20
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_EnaSta_Bit_End                                                     20
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_EnaSta_Mask                                                    cBit20
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_EnaSta_Shift                                                       20
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_EnaSta_MaxVal                                                     0x1
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_EnaSta_MinVal                                                     0x0
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_EnaSta_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Sk_Mem_AcpSeq
BitField Type: RO
BitField Desc: Sink Member Accepted Sequence
BitField Bits: [19:13]
--------------------------------------*/
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_AcpSeq_Bit_Start                                                   13
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_AcpSeq_Bit_End                                                     19
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_AcpSeq_Mask                                                 cBit19_13
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_AcpSeq_Shift                                                       13
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_AcpSeq_MaxVal                                                    0x7f
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_AcpSeq_MinVal                                                     0x0
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_AcpSeq_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Sk_Mem_RecSeq
BitField Type: RO
BitField Desc: Sink Member Sequence Received
BitField Bits: [12:06]
--------------------------------------*/
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_RecSeq_Bit_Start                                                    6
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_RecSeq_Bit_End                                                     12
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_RecSeq_Mask                                                  cBit12_6
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_RecSeq_Shift                                                        6
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_RecSeq_MaxVal                                                    0x7f
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_RecSeq_MinVal                                                     0x0
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_RecSeq_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Sk_Mem_Fsm
BitField Type: RO
BitField Desc: Member FSM
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_Fsm_Bit_Start                                                       0
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_Fsm_Bit_End                                                         1
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_Fsm_Mask                                                      cBit1_0
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_Fsm_Shift                                                           0
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_Fsm_MaxVal                                                        0x3
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_Fsm_MinVal                                                        0x0
#define cAf6_vcat5g_sk_memsta_pen_Sk_Mem_Fsm_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Member Expected Sequence Control
Reg Addr   : 0x53000-0x53bff
Reg Formula: 0x53000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-95), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to Control Expected Sequence of member at Sink side.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_expseq_pen_Base                                                              0x53000
#define cAf6Reg_vcat5g_sk_expseq_pen(stsid)                                  (0x53000+(stsid)*32+vtgid*4+vtid)
#define cAf6Reg_vcat5g_sk_expseq_pen_WidthVal                                                               32
#define cAf6Reg_vcat5g_sk_expseq_pen_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: Sk_Mem_ExpSeq
BitField Type: R/W
BitField Desc: Member Expected Sequence
BitField Bits: [06:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_expseq_pen_Sk_Mem_ExpSeq_Bit_Start                                                    0
#define cAf6_vcat5g_sk_expseq_pen_Sk_Mem_ExpSeq_Bit_End                                                      6
#define cAf6_vcat5g_sk_expseq_pen_Sk_Mem_ExpSeq_Mask                                                   cBit6_0
#define cAf6_vcat5g_sk_expseq_pen_Sk_Mem_ExpSeq_Shift                                                        0
#define cAf6_vcat5g_sk_expseq_pen_Sk_Mem_ExpSeq_MaxVal                                                    0x7f
#define cAf6_vcat5g_sk_expseq_pen_Sk_Mem_ExpSeq_MinVal                                                     0x0
#define cAf6_vcat5g_sk_expseq_pen_Sk_Mem_ExpSeq_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Channel Interrupt Enable Control
Reg Addr   : 0x58000-0x58BFF
Reg Formula: 0x58000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-95), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to enable Interrupt, write 1 to enable interrupt for event.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_intrctrl_pen_Base                                                            0x58000
#define cAf6Reg_vcat5g_sk_intrctrl_pen(stsid)                                (0x58000+(stsid)*32+vtgid*4+vtid)
#define cAf6Reg_vcat5g_sk_intrctrl_pen_WidthVal                                                             32
#define cAf6Reg_vcat5g_sk_intrctrl_pen_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: SQNC_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for SQNC event
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrctrl_pen_SQNC_Intr_En_Bit_Start                                                   5
#define cAf6_vcat5g_sk_intrctrl_pen_SQNC_Intr_En_Bit_End                                                     5
#define cAf6_vcat5g_sk_intrctrl_pen_SQNC_Intr_En_Mask                                                    cBit5
#define cAf6_vcat5g_sk_intrctrl_pen_SQNC_Intr_En_Shift                                                       5
#define cAf6_vcat5g_sk_intrctrl_pen_SQNC_Intr_En_MaxVal                                                    0x1
#define cAf6_vcat5g_sk_intrctrl_pen_SQNC_Intr_En_MinVal                                                    0x0
#define cAf6_vcat5g_sk_intrctrl_pen_SQNC_Intr_En_RstVal                                                    0x0

/*--------------------------------------
BitField Name: MND_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for MND event
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrctrl_pen_MND_Intr_En_Bit_Start                                                    4
#define cAf6_vcat5g_sk_intrctrl_pen_MND_Intr_En_Bit_End                                                      4
#define cAf6_vcat5g_sk_intrctrl_pen_MND_Intr_En_Mask                                                     cBit4
#define cAf6_vcat5g_sk_intrctrl_pen_MND_Intr_En_Shift                                                        4
#define cAf6_vcat5g_sk_intrctrl_pen_MND_Intr_En_MaxVal                                                     0x1
#define cAf6_vcat5g_sk_intrctrl_pen_MND_Intr_En_MinVal                                                     0x0
#define cAf6_vcat5g_sk_intrctrl_pen_MND_Intr_En_RstVal                                                     0x0

/*--------------------------------------
BitField Name: LOA_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for LOA event
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrctrl_pen_LOA_Intr_En_Bit_Start                                                    3
#define cAf6_vcat5g_sk_intrctrl_pen_LOA_Intr_En_Bit_End                                                      3
#define cAf6_vcat5g_sk_intrctrl_pen_LOA_Intr_En_Mask                                                     cBit3
#define cAf6_vcat5g_sk_intrctrl_pen_LOA_Intr_En_Shift                                                        3
#define cAf6_vcat5g_sk_intrctrl_pen_LOA_Intr_En_MaxVal                                                     0x1
#define cAf6_vcat5g_sk_intrctrl_pen_LOA_Intr_En_MinVal                                                     0x0
#define cAf6_vcat5g_sk_intrctrl_pen_LOA_Intr_En_RstVal                                                     0x0

/*--------------------------------------
BitField Name: SQM_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for SQM event
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrctrl_pen_SQM_Intr_En_Bit_Start                                                    2
#define cAf6_vcat5g_sk_intrctrl_pen_SQM_Intr_En_Bit_End                                                      2
#define cAf6_vcat5g_sk_intrctrl_pen_SQM_Intr_En_Mask                                                     cBit2
#define cAf6_vcat5g_sk_intrctrl_pen_SQM_Intr_En_Shift                                                        2
#define cAf6_vcat5g_sk_intrctrl_pen_SQM_Intr_En_MaxVal                                                     0x1
#define cAf6_vcat5g_sk_intrctrl_pen_SQM_Intr_En_MinVal                                                     0x0
#define cAf6_vcat5g_sk_intrctrl_pen_SQM_Intr_En_RstVal                                                     0x0

/*--------------------------------------
BitField Name: OOM_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for OOM event
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrctrl_pen_OOM_Intr_En_Bit_Start                                                    1
#define cAf6_vcat5g_sk_intrctrl_pen_OOM_Intr_En_Bit_End                                                      1
#define cAf6_vcat5g_sk_intrctrl_pen_OOM_Intr_En_Mask                                                     cBit1
#define cAf6_vcat5g_sk_intrctrl_pen_OOM_Intr_En_Shift                                                        1
#define cAf6_vcat5g_sk_intrctrl_pen_OOM_Intr_En_MaxVal                                                     0x1
#define cAf6_vcat5g_sk_intrctrl_pen_OOM_Intr_En_MinVal                                                     0x0
#define cAf6_vcat5g_sk_intrctrl_pen_OOM_Intr_En_RstVal                                                     0x0

/*--------------------------------------
BitField Name: LOM_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for LOM event
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrctrl_pen_LOM_Intr_En_Bit_Start                                                    0
#define cAf6_vcat5g_sk_intrctrl_pen_LOM_Intr_En_Bit_End                                                      0
#define cAf6_vcat5g_sk_intrctrl_pen_LOM_Intr_En_Mask                                                     cBit0
#define cAf6_vcat5g_sk_intrctrl_pen_LOM_Intr_En_Shift                                                        0
#define cAf6_vcat5g_sk_intrctrl_pen_LOM_Intr_En_MaxVal                                                     0x1
#define cAf6_vcat5g_sk_intrctrl_pen_LOM_Intr_En_MinVal                                                     0x0
#define cAf6_vcat5g_sk_intrctrl_pen_LOM_Intr_En_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Channel Interrupt Status
Reg Addr   : 0x59000-0x59BFF
Reg Formula: 0x59000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-95), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to show the status of event,which generate interrupt.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_intrstk_pen_Base                                                             0x59000
#define cAf6Reg_vcat5g_sk_intrstk_pen(stsid)                                 (0x59000+(stsid)*32+vtgid*4+vtid)
#define cAf6Reg_vcat5g_sk_intrstk_pen_WidthVal                                                              32
#define cAf6Reg_vcat5g_sk_intrstk_pen_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: SQNC_Intr_Sta
BitField Type: R/W
BitField Desc: SQNC event state change
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrstk_pen_SQNC_Intr_Sta_Bit_Start                                                   5
#define cAf6_vcat5g_sk_intrstk_pen_SQNC_Intr_Sta_Bit_End                                                     5
#define cAf6_vcat5g_sk_intrstk_pen_SQNC_Intr_Sta_Mask                                                    cBit5
#define cAf6_vcat5g_sk_intrstk_pen_SQNC_Intr_Sta_Shift                                                       5
#define cAf6_vcat5g_sk_intrstk_pen_SQNC_Intr_Sta_MaxVal                                                    0x1
#define cAf6_vcat5g_sk_intrstk_pen_SQNC_Intr_Sta_MinVal                                                    0x0
#define cAf6_vcat5g_sk_intrstk_pen_SQNC_Intr_Sta_RstVal                                                    0x0

/*--------------------------------------
BitField Name: MND_Intr_Sta
BitField Type: R/W
BitField Desc: MND event state change
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrstk_pen_MND_Intr_Sta_Bit_Start                                                    4
#define cAf6_vcat5g_sk_intrstk_pen_MND_Intr_Sta_Bit_End                                                      4
#define cAf6_vcat5g_sk_intrstk_pen_MND_Intr_Sta_Mask                                                     cBit4
#define cAf6_vcat5g_sk_intrstk_pen_MND_Intr_Sta_Shift                                                        4
#define cAf6_vcat5g_sk_intrstk_pen_MND_Intr_Sta_MaxVal                                                     0x1
#define cAf6_vcat5g_sk_intrstk_pen_MND_Intr_Sta_MinVal                                                     0x0
#define cAf6_vcat5g_sk_intrstk_pen_MND_Intr_Sta_RstVal                                                     0x0

/*--------------------------------------
BitField Name: LOA_Intr_Sta
BitField Type: R/W
BitField Desc: LOA event state change
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrstk_pen_LOA_Intr_Sta_Bit_Start                                                    3
#define cAf6_vcat5g_sk_intrstk_pen_LOA_Intr_Sta_Bit_End                                                      3
#define cAf6_vcat5g_sk_intrstk_pen_LOA_Intr_Sta_Mask                                                     cBit3
#define cAf6_vcat5g_sk_intrstk_pen_LOA_Intr_Sta_Shift                                                        3
#define cAf6_vcat5g_sk_intrstk_pen_LOA_Intr_Sta_MaxVal                                                     0x1
#define cAf6_vcat5g_sk_intrstk_pen_LOA_Intr_Sta_MinVal                                                     0x0
#define cAf6_vcat5g_sk_intrstk_pen_LOA_Intr_Sta_RstVal                                                     0x0

/*--------------------------------------
BitField Name: SQM_Intr_Sta
BitField Type: R/W
BitField Desc: SQM event state change
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrstk_pen_SQM_Intr_Sta_Bit_Start                                                    2
#define cAf6_vcat5g_sk_intrstk_pen_SQM_Intr_Sta_Bit_End                                                      2
#define cAf6_vcat5g_sk_intrstk_pen_SQM_Intr_Sta_Mask                                                     cBit2
#define cAf6_vcat5g_sk_intrstk_pen_SQM_Intr_Sta_Shift                                                        2
#define cAf6_vcat5g_sk_intrstk_pen_SQM_Intr_Sta_MaxVal                                                     0x1
#define cAf6_vcat5g_sk_intrstk_pen_SQM_Intr_Sta_MinVal                                                     0x0
#define cAf6_vcat5g_sk_intrstk_pen_SQM_Intr_Sta_RstVal                                                     0x0

/*--------------------------------------
BitField Name: OOM_Intr_Sta
BitField Type: R/W
BitField Desc: OOM event state change
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrstk_pen_OOM_Intr_Sta_Bit_Start                                                    1
#define cAf6_vcat5g_sk_intrstk_pen_OOM_Intr_Sta_Bit_End                                                      1
#define cAf6_vcat5g_sk_intrstk_pen_OOM_Intr_Sta_Mask                                                     cBit1
#define cAf6_vcat5g_sk_intrstk_pen_OOM_Intr_Sta_Shift                                                        1
#define cAf6_vcat5g_sk_intrstk_pen_OOM_Intr_Sta_MaxVal                                                     0x1
#define cAf6_vcat5g_sk_intrstk_pen_OOM_Intr_Sta_MinVal                                                     0x0
#define cAf6_vcat5g_sk_intrstk_pen_OOM_Intr_Sta_RstVal                                                     0x0

/*--------------------------------------
BitField Name: LOM_Intr_Sta
BitField Type: R/W
BitField Desc: LOM event state change
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrstk_pen_LOM_Intr_Sta_Bit_Start                                                    0
#define cAf6_vcat5g_sk_intrstk_pen_LOM_Intr_Sta_Bit_End                                                      0
#define cAf6_vcat5g_sk_intrstk_pen_LOM_Intr_Sta_Mask                                                     cBit0
#define cAf6_vcat5g_sk_intrstk_pen_LOM_Intr_Sta_Shift                                                        0
#define cAf6_vcat5g_sk_intrstk_pen_LOM_Intr_Sta_MaxVal                                                     0x1
#define cAf6_vcat5g_sk_intrstk_pen_LOM_Intr_Sta_MinVal                                                     0x0
#define cAf6_vcat5g_sk_intrstk_pen_LOM_Intr_Sta_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Channel Current Status
Reg Addr   : 0x5A000-0x5ABFF
Reg Formula: 0x5A000 + $sts_id*32 + $vtg_id*4 + $vt_id
    Where  : 
           + $sts_id(0-95), $vtg_id(0-6), $vt_id(0-3)
Reg Desc   : 
This register is used to show the current status of event.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_intrsta_pen_Base                                                             0x5A000
#define cAf6Reg_vcat5g_sk_intrsta_pen(stsid)                                 (0x5A000+(stsid)*32+vtgid*4+vtid)
#define cAf6Reg_vcat5g_sk_intrsta_pen_WidthVal                                                              32
#define cAf6Reg_vcat5g_sk_intrsta_pen_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: SQNC_Intr_CrrSta
BitField Type: RO
BitField Desc: SQNC event Current state
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrsta_pen_SQNC_Intr_CrrSta_Bit_Start                                                5
#define cAf6_vcat5g_sk_intrsta_pen_SQNC_Intr_CrrSta_Bit_End                                                  5
#define cAf6_vcat5g_sk_intrsta_pen_SQNC_Intr_CrrSta_Mask                                                 cBit5
#define cAf6_vcat5g_sk_intrsta_pen_SQNC_Intr_CrrSta_Shift                                                    5
#define cAf6_vcat5g_sk_intrsta_pen_SQNC_Intr_CrrSta_MaxVal                                                 0x1
#define cAf6_vcat5g_sk_intrsta_pen_SQNC_Intr_CrrSta_MinVal                                                 0x0
#define cAf6_vcat5g_sk_intrsta_pen_SQNC_Intr_CrrSta_RstVal                                                 0x0

/*--------------------------------------
BitField Name: MND_Intr_CrrSta
BitField Type: RO
BitField Desc: MND event Current state
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrsta_pen_MND_Intr_CrrSta_Bit_Start                                                 4
#define cAf6_vcat5g_sk_intrsta_pen_MND_Intr_CrrSta_Bit_End                                                   4
#define cAf6_vcat5g_sk_intrsta_pen_MND_Intr_CrrSta_Mask                                                  cBit4
#define cAf6_vcat5g_sk_intrsta_pen_MND_Intr_CrrSta_Shift                                                     4
#define cAf6_vcat5g_sk_intrsta_pen_MND_Intr_CrrSta_MaxVal                                                  0x1
#define cAf6_vcat5g_sk_intrsta_pen_MND_Intr_CrrSta_MinVal                                                  0x0
#define cAf6_vcat5g_sk_intrsta_pen_MND_Intr_CrrSta_RstVal                                                  0x0

/*--------------------------------------
BitField Name: LOA_Intr_CrrSta
BitField Type: RO
BitField Desc: LOA event Current state
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrsta_pen_LOA_Intr_CrrSta_Bit_Start                                                 3
#define cAf6_vcat5g_sk_intrsta_pen_LOA_Intr_CrrSta_Bit_End                                                   3
#define cAf6_vcat5g_sk_intrsta_pen_LOA_Intr_CrrSta_Mask                                                  cBit3
#define cAf6_vcat5g_sk_intrsta_pen_LOA_Intr_CrrSta_Shift                                                     3
#define cAf6_vcat5g_sk_intrsta_pen_LOA_Intr_CrrSta_MaxVal                                                  0x1
#define cAf6_vcat5g_sk_intrsta_pen_LOA_Intr_CrrSta_MinVal                                                  0x0
#define cAf6_vcat5g_sk_intrsta_pen_LOA_Intr_CrrSta_RstVal                                                  0x0

/*--------------------------------------
BitField Name: SQM_Intr_CrrSta
BitField Type: RO
BitField Desc: SQM event Current state
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrsta_pen_SQM_Intr_CrrSta_Bit_Start                                                 2
#define cAf6_vcat5g_sk_intrsta_pen_SQM_Intr_CrrSta_Bit_End                                                   2
#define cAf6_vcat5g_sk_intrsta_pen_SQM_Intr_CrrSta_Mask                                                  cBit2
#define cAf6_vcat5g_sk_intrsta_pen_SQM_Intr_CrrSta_Shift                                                     2
#define cAf6_vcat5g_sk_intrsta_pen_SQM_Intr_CrrSta_MaxVal                                                  0x1
#define cAf6_vcat5g_sk_intrsta_pen_SQM_Intr_CrrSta_MinVal                                                  0x0
#define cAf6_vcat5g_sk_intrsta_pen_SQM_Intr_CrrSta_RstVal                                                  0x0

/*--------------------------------------
BitField Name: OOM_Intr_CrrSta
BitField Type: RO
BitField Desc: OOM event Current state
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrsta_pen_OOM_Intr_CrrSta_Bit_Start                                                 1
#define cAf6_vcat5g_sk_intrsta_pen_OOM_Intr_CrrSta_Bit_End                                                   1
#define cAf6_vcat5g_sk_intrsta_pen_OOM_Intr_CrrSta_Mask                                                  cBit1
#define cAf6_vcat5g_sk_intrsta_pen_OOM_Intr_CrrSta_Shift                                                     1
#define cAf6_vcat5g_sk_intrsta_pen_OOM_Intr_CrrSta_MaxVal                                                  0x1
#define cAf6_vcat5g_sk_intrsta_pen_OOM_Intr_CrrSta_MinVal                                                  0x0
#define cAf6_vcat5g_sk_intrsta_pen_OOM_Intr_CrrSta_RstVal                                                  0x0

/*--------------------------------------
BitField Name: LOM_Intr_CrrSta
BitField Type: RO
BitField Desc: LOM event Current state
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_intrsta_pen_LOM_Intr_CrrSta_Bit_Start                                                 0
#define cAf6_vcat5g_sk_intrsta_pen_LOM_Intr_CrrSta_Bit_End                                                   0
#define cAf6_vcat5g_sk_intrsta_pen_LOM_Intr_CrrSta_Mask                                                  cBit0
#define cAf6_vcat5g_sk_intrsta_pen_LOM_Intr_CrrSta_Shift                                                     0
#define cAf6_vcat5g_sk_intrsta_pen_LOM_Intr_CrrSta_MaxVal                                                  0x1
#define cAf6_vcat5g_sk_intrsta_pen_LOM_Intr_CrrSta_MinVal                                                  0x0
#define cAf6_vcat5g_sk_intrsta_pen_LOM_Intr_CrrSta_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Channel Interrupt Or Status
Reg Addr   : 0x5B000-0x5B05F
Reg Formula: 0x5B000 + $sts_id
    Where  : 
           + $sts_id(0-95)
Reg Desc   : 
The register consists of 28 bits for 28 VT/TUs or 28 DS1/E1s of the related STS/VC.
Each bit is used to store Interrupt OR status of the related VT/TU/DS1/E1.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_introsta_pen_Base                                                            0x5B000
#define cAf6Reg_vcat5g_sk_introsta_pen(stsid)                                                (0x5B000+(stsid))
#define cAf6Reg_vcat5g_sk_introsta_pen_WidthVal                                                             32
#define cAf6Reg_vcat5g_sk_introsta_pen_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: IntrOrSta
BitField Type: R/W
BitField Desc: Interrupt Or Status
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_introsta_pen_IntrOrSta_Bit_Start                                                      0
#define cAf6_vcat5g_sk_introsta_pen_IntrOrSta_Bit_End                                                       27
#define cAf6_vcat5g_sk_introsta_pen_IntrOrSta_Mask                                                    cBit27_0
#define cAf6_vcat5g_sk_introsta_pen_IntrOrSta_Shift                                                          0
#define cAf6_vcat5g_sk_introsta_pen_IntrOrSta_MaxVal                                                 0xfffffff
#define cAf6_vcat5g_sk_introsta_pen_IntrOrSta_MinVal                                                       0x0
#define cAf6_vcat5g_sk_introsta_pen_IntrOrSta_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink STS/VC Interrupt Enable Control
Reg Addr   : 0x5FFFE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 96 interrupt enable bits for 96 STS/VCs.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_vcintrctrl_pen_Base                                                          0x5FFFE
#define cAf6Reg_vcat5g_sk_vcintrctrl_pen                                                               0x5FFFE
#define cAf6Reg_vcat5g_sk_vcintrctrl_pen_WidthVal                                                          128
#define cAf6Reg_vcat5g_sk_vcintrctrl_pen_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: STS_Intr_En
BitField Type: R/W
BitField Desc: Interrupt Enable for STS(0-96)
BitField Bits: [95:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Bit_Start                                                  0
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Bit_End                                                   95
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Mask_01                                             cBit31_0
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Shift_01                                                   0
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Mask_02                                             cBit31_0
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Shift_02                                                   0
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Mask_03                                             cBit31_0
#define cAf6_vcat5g_sk_vcintrctrl_pen_STS_Intr_En_Shift_03                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Sink STS/VC Interrupt Or Status
Reg Addr   : 0x5FFFF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 96 bits for 16 STS/VCs. Each bit is used to store Interrupt OR status of the related STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sk_vcintrosta_pen_Base                                                          0x5FFFF
#define cAf6Reg_vcat5g_sk_vcintrosta_pen                                                               0x5FFFF
#define cAf6Reg_vcat5g_sk_vcintrosta_pen_WidthVal                                                          128
#define cAf6Reg_vcat5g_sk_vcintrosta_pen_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: STS_IntrOrSta
BitField Type: R/W
BitField Desc: Interrupt Status for STS(0-96)
BitField Bits: [95:00]
--------------------------------------*/
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Bit_Start                                                0
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Bit_End                                                 95
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Mask_01                                           cBit31_0
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Shift_01                                                 0
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Mask_02                                           cBit31_0
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Shift_02                                                 0
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Mask_03                                           cBit31_0
#define cAf6_vcat5g_sk_vcintrosta_pen_STS_IntrOrSta_Shift_03                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Debug Sticky 0
Reg Addr   : 0x2010a
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_stk0_pen_Base                                                                   0x2010a
#define cAf6Reg_vcat5g_stk0_pen                                                                        0x2010a
#define cAf6Reg_vcat5g_stk0_pen_WidthVal                                                                    64
#define cAf6Reg_vcat5g_stk0_pen_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: So_Enc_Req
BitField Type: R/W
BitField Desc: Output Request to ENC
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_stk0_pen_So_Enc_Req_Bit_Start                                                            0
#define cAf6_vcat5g_stk0_pen_So_Enc_Req_Bit_End                                                              0
#define cAf6_vcat5g_stk0_pen_So_Enc_Req_Mask                                                             cBit0
#define cAf6_vcat5g_stk0_pen_So_Enc_Req_Shift                                                                0
#define cAf6_vcat5g_stk0_pen_So_Enc_Req_MaxVal                                                             0x1
#define cAf6_vcat5g_stk0_pen_So_Enc_Req_MinVal                                                             0x0
#define cAf6_vcat5g_stk0_pen_So_Enc_Req_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Debug Vcat Interface Dump Control
Reg Addr   : 0x0000A
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_dumpctrl_pen_Base                                                               0x0000A
#define cAf6Reg_vcat5g_dumpctrl_pen                                                                    0x0000A
#define cAf6Reg_vcat5g_dumpctrl_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_dumpctrl_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: Cfg_Dump_Mod
BitField Type: R/W
BitField Desc: Data dump mode 1'b0     : Dump full 1'b1     : Dump at frame 0
(DS1 mode)
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Mod_Bit_Start                                                      3
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Mod_Bit_End                                                        3
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Mod_Mask                                                       cBit3
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Mod_Shift                                                          3
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Mod_MaxVal                                                       0x1
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Mod_MinVal                                                       0x0
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Mod_RstVal                                                       0x0

/*--------------------------------------
BitField Name: Cfg_Dump_Sel
BitField Type: R/W
BitField Desc: Select bus info to dump 3'd0     : Source to PDH FiFo Input Data
3'd1     : Source PDH FiFo Output Data 3'd2     : Sink PDH Input Data 3'd3     :
ENC-VCAT Data 3'd4-3'd7: Reserved
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Sel_Bit_Start                                                      0
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Sel_Bit_End                                                        2
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Sel_Mask                                                     cBit2_0
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Sel_Shift                                                          0
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Sel_MaxVal                                                       0x7
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Sel_MinVal                                                       0x0
#define cAf6_vcat5g_dumpctrl_pen_Cfg_Dump_Sel_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Debug Vcat Interface Dump Data
Reg Addr   : 0x60000-0x61FFF
Reg Formula: 0x60000 + $dump_id
    Where  : 
           + $dump_id(0-8191)
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_dumpdat_pen_Base                                                                0x60000
#define cAf6Reg_vcat5g_dumpdat_pen(dumpid)                                                  (0x60000+(dumpid))
#define cAf6Reg_vcat5g_dumpdat_pen_WidthVal                                                                 32
#define cAf6Reg_vcat5g_dumpdat_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: Dump_Dat_Vli
BitField Type: RO
BitField Desc: VLI Indication
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Vli_Bit_Start                                                       8
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Vli_Bit_End                                                         8
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Vli_Mask                                                        cBit8
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Vli_Shift                                                           8
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Vli_MaxVal                                                        0x1
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Vli_MinVal                                                        0x0
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Vli_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Dump_Dat
BitField Type: RO
BitField Desc: Interface Dump Datat
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Bit_Start                                                           0
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Bit_End                                                             7
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Mask                                                          cBit7_0
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_Shift                                                               0
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_MaxVal                                                           0xff
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_MinVal                                                            0x0
#define cAf6_vcat5g_dumpdat_pen_Dump_Dat_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : Source Debug Control
Reg Addr   : 0x2010B
Reg Formula: 
    Where  : 
Reg Desc   : 
This register used to control VCAT debug.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sodbctrl_pen_Base                                                               0x2010B
#define cAf6Reg_vcat5g_sodbctrl_pen                                                                    0x2010B
#define cAf6Reg_vcat5g_sodbctrl_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_sodbctrl_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: Cfg_Dbg_Ena
BitField Type: R/W
BitField Desc: Enable Debug
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Ena_Bit_Start                                                      12
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Ena_Bit_End                                                        12
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Ena_Mask                                                       cBit12
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Ena_Shift                                                          12
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Ena_MaxVal                                                        0x1
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Ena_MinVal                                                        0x0
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Ena_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Cfg_Dbg_Slc
BitField Type: R/W
BitField Desc: Select Slice ID to Debug
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Slc_Bit_Start                                                      10
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Slc_Bit_End                                                        11
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Slc_Mask                                                    cBit11_10
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Slc_Shift                                                          10
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Slc_MaxVal                                                        0x3
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Slc_MinVal                                                        0x0
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Slc_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Cfg_Dbg_Lid
BitField Type: R/W
BitField Desc: Select Channel ID to Debug
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Lid_Bit_Start                                                       0
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Lid_Bit_End                                                         9
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Lid_Mask                                                      cBit9_0
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Lid_Shift                                                           0
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Lid_MaxVal                                                      0x3ff
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Lid_MinVal                                                        0x0
#define cAf6_vcat5g_sodbctrl_pen_Cfg_Dbg_Lid_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Debug VCAT Source Sticky
Reg Addr   : 0x2010A
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sostk_pen_Base                                                                  0x2010A
#define cAf6Reg_vcat5g_sostk_pen                                                                       0x2010A
#define cAf6Reg_vcat5g_sostk_pen_WidthVal                                                                   64
#define cAf6Reg_vcat5g_sostk_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: So_Slc3_LO_Empt
BitField Type: R/W
BitField Desc: Slice 3 LO PDH Adapt FiFo Empt
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Empt_Bit_Start                                                     31
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Empt_Bit_End                                                       31
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Empt_Mask                                                      cBit31
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Empt_Shift                                                         31
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Empt_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Empt_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Empt_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc3_HO_Empt
BitField Type: R/W
BitField Desc: Slice 3 HO PDH Adapt FiFo Empt
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Empt_Bit_Start                                                     30
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Empt_Bit_End                                                       30
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Empt_Mask                                                      cBit30
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Empt_Shift                                                         30
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Empt_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Empt_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Empt_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc3_LO_Full
BitField Type: R/W
BitField Desc: Slice 3 LO PDH Adapt FiFo Full
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Full_Bit_Start                                                     29
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Full_Bit_End                                                       29
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Full_Mask                                                      cBit29
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Full_Shift                                                         29
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Full_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Full_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc3_LO_Full_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc3_HO_Full
BitField Type: R/W
BitField Desc: Slice 3 HO PDH Adapt FiFo Full
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Full_Bit_Start                                                     28
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Full_Bit_End                                                       28
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Full_Mask                                                      cBit28
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Full_Shift                                                         28
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Full_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Full_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc3_HO_Full_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc2_LO_Empt
BitField Type: R/W
BitField Desc: Slice 2 LO PDH Adapt FiFo Empt
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Empt_Bit_Start                                                     27
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Empt_Bit_End                                                       27
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Empt_Mask                                                      cBit27
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Empt_Shift                                                         27
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Empt_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Empt_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Empt_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc2_HO_Empt
BitField Type: R/W
BitField Desc: Slice 2 HO PDH Adapt FiFo Empt
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Empt_Bit_Start                                                     26
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Empt_Bit_End                                                       26
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Empt_Mask                                                      cBit26
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Empt_Shift                                                         26
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Empt_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Empt_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Empt_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc2_LO_Full
BitField Type: R/W
BitField Desc: Slice 2 LO PDH Adapt FiFo Full
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Full_Bit_Start                                                     25
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Full_Bit_End                                                       25
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Full_Mask                                                      cBit25
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Full_Shift                                                         25
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Full_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Full_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc2_LO_Full_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc2_HO_Full
BitField Type: R/W
BitField Desc: Slice 2 HO PDH Adapt FiFo Full
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Full_Bit_Start                                                     24
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Full_Bit_End                                                       24
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Full_Mask                                                      cBit24
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Full_Shift                                                         24
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Full_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Full_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc2_HO_Full_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc1_LO_Empt
BitField Type: R/W
BitField Desc: Slice 1 LO PDH Adapt FiFo Empt
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Empt_Bit_Start                                                     23
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Empt_Bit_End                                                       23
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Empt_Mask                                                      cBit23
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Empt_Shift                                                         23
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Empt_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Empt_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Empt_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc1_HO_Empt
BitField Type: R/W
BitField Desc: Slice 1 HO PDH Adapt FiFo Empt
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Empt_Bit_Start                                                     22
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Empt_Bit_End                                                       22
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Empt_Mask                                                      cBit22
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Empt_Shift                                                         22
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Empt_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Empt_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Empt_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc1_LO_Full
BitField Type: R/W
BitField Desc: Slice 1 LO PDH Adapt FiFo Full
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Full_Bit_Start                                                     21
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Full_Bit_End                                                       21
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Full_Mask                                                      cBit21
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Full_Shift                                                         21
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Full_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Full_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc1_LO_Full_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc1_HO_Full
BitField Type: R/W
BitField Desc: Slice 1 HO PDH Adapt FiFo Full
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Full_Bit_Start                                                     20
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Full_Bit_End                                                       20
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Full_Mask                                                      cBit20
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Full_Shift                                                         20
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Full_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Full_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc1_HO_Full_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc0_LO_Empt
BitField Type: R/W
BitField Desc: Slice 0 LO PDH Adapt FiFo Empt
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Empt_Bit_Start                                                     19
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Empt_Bit_End                                                       19
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Empt_Mask                                                      cBit19
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Empt_Shift                                                         19
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Empt_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Empt_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Empt_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc0_HO_Empt
BitField Type: R/W
BitField Desc: Slice 0 HO PDH Adapt FiFo Empt
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Empt_Bit_Start                                                     18
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Empt_Bit_End                                                       18
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Empt_Mask                                                      cBit18
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Empt_Shift                                                         18
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Empt_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Empt_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Empt_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc0_LO_Full
BitField Type: R/W
BitField Desc: Slice 0 LO PDH Adapt FiFo Full
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Full_Bit_Start                                                     17
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Full_Bit_End                                                       17
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Full_Mask                                                      cBit17
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Full_Shift                                                         17
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Full_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Full_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc0_LO_Full_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_Slc0_HO_Full
BitField Type: R/W
BitField Desc: Slice 0 HO PDH Adapt FiFo Full
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Full_Bit_Start                                                     16
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Full_Bit_End                                                       16
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Full_Mask                                                      cBit16
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Full_Shift                                                         16
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Full_MaxVal                                                       0x1
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Full_MinVal                                                       0x0
#define cAf6_vcat5g_sostk_pen_So_Slc0_HO_Full_RstVal                                                       0x0

/*--------------------------------------
BitField Name: So_DS3VLI_Err
BitField Type: R/W
BitField Desc: Ouput DS3 VLI Position Error
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_DS3VLI_Err_Bit_Start                                                       15
#define cAf6_vcat5g_sostk_pen_So_DS3VLI_Err_Bit_End                                                         15
#define cAf6_vcat5g_sostk_pen_So_DS3VLI_Err_Mask                                                        cBit15
#define cAf6_vcat5g_sostk_pen_So_DS3VLI_Err_Shift                                                           15
#define cAf6_vcat5g_sostk_pen_So_DS3VLI_Err_MaxVal                                                         0x1
#define cAf6_vcat5g_sostk_pen_So_DS3VLI_Err_MinVal                                                         0x0
#define cAf6_vcat5g_sostk_pen_So_DS3VLI_Err_RstVal                                                         0x0

/*--------------------------------------
BitField Name: So_E3VLI_Err
BitField Type: R/W
BitField Desc: Ouput E3 VLI Position Error
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_E3VLI_Err_Bit_Start                                                        14
#define cAf6_vcat5g_sostk_pen_So_E3VLI_Err_Bit_End                                                          14
#define cAf6_vcat5g_sostk_pen_So_E3VLI_Err_Mask                                                         cBit14
#define cAf6_vcat5g_sostk_pen_So_E3VLI_Err_Shift                                                            14
#define cAf6_vcat5g_sostk_pen_So_E3VLI_Err_MaxVal                                                          0x1
#define cAf6_vcat5g_sostk_pen_So_E3VLI_Err_MinVal                                                          0x0
#define cAf6_vcat5g_sostk_pen_So_E3VLI_Err_RstVal                                                          0x0

/*--------------------------------------
BitField Name: So_DS1VLI_Err
BitField Type: R/W
BitField Desc: Ouput DS1 VLI Position Error
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_DS1VLI_Err_Bit_Start                                                       13
#define cAf6_vcat5g_sostk_pen_So_DS1VLI_Err_Bit_End                                                         13
#define cAf6_vcat5g_sostk_pen_So_DS1VLI_Err_Mask                                                        cBit13
#define cAf6_vcat5g_sostk_pen_So_DS1VLI_Err_Shift                                                           13
#define cAf6_vcat5g_sostk_pen_So_DS1VLI_Err_MaxVal                                                         0x1
#define cAf6_vcat5g_sostk_pen_So_DS1VLI_Err_MinVal                                                         0x0
#define cAf6_vcat5g_sostk_pen_So_DS1VLI_Err_RstVal                                                         0x0

/*--------------------------------------
BitField Name: So_E1VLI_Err
BitField Type: R/W
BitField Desc: Ouput E1 VLI Position Error
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_E1VLI_Err_Bit_Start                                                        12
#define cAf6_vcat5g_sostk_pen_So_E1VLI_Err_Bit_End                                                          12
#define cAf6_vcat5g_sostk_pen_So_E1VLI_Err_Mask                                                         cBit12
#define cAf6_vcat5g_sostk_pen_So_E1VLI_Err_Shift                                                            12
#define cAf6_vcat5g_sostk_pen_So_E1VLI_Err_MaxVal                                                          0x1
#define cAf6_vcat5g_sostk_pen_So_E1VLI_Err_MinVal                                                          0x0
#define cAf6_vcat5g_sostk_pen_So_E1VLI_Err_RstVal                                                          0x0

/*--------------------------------------
BitField Name: So_PDH_Vli
BitField Type: R/W
BitField Desc: Output VLI from VCAT to PDH
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_PDH_Vli_Bit_Start                                                          10
#define cAf6_vcat5g_sostk_pen_So_PDH_Vli_Bit_End                                                            10
#define cAf6_vcat5g_sostk_pen_So_PDH_Vli_Mask                                                           cBit10
#define cAf6_vcat5g_sostk_pen_So_PDH_Vli_Shift                                                              10
#define cAf6_vcat5g_sostk_pen_So_PDH_Vli_MaxVal                                                            0x1
#define cAf6_vcat5g_sostk_pen_So_PDH_Vli_MinVal                                                            0x0
#define cAf6_vcat5g_sostk_pen_So_PDH_Vli_RstVal                                                            0x0

/*--------------------------------------
BitField Name: So_PDH_Vld
BitField Type: R/W
BitField Desc: Output Valid from VCAT to PDH
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_PDH_Vld_Bit_Start                                                           9
#define cAf6_vcat5g_sostk_pen_So_PDH_Vld_Bit_End                                                             9
#define cAf6_vcat5g_sostk_pen_So_PDH_Vld_Mask                                                            cBit9
#define cAf6_vcat5g_sostk_pen_So_PDH_Vld_Shift                                                               9
#define cAf6_vcat5g_sostk_pen_So_PDH_Vld_MaxVal                                                            0x1
#define cAf6_vcat5g_sostk_pen_So_PDH_Vld_MinVal                                                            0x0
#define cAf6_vcat5g_sostk_pen_So_PDH_Vld_RstVal                                                            0x0

/*--------------------------------------
BitField Name: IPDH_Req
BitField Type: R/W
BitField Desc: Request from PDH to VCAT
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_IPDH_Req_Bit_Start                                                             8
#define cAf6_vcat5g_sostk_pen_IPDH_Req_Bit_End                                                               8
#define cAf6_vcat5g_sostk_pen_IPDH_Req_Mask                                                              cBit8
#define cAf6_vcat5g_sostk_pen_IPDH_Req_Shift                                                                 8
#define cAf6_vcat5g_sostk_pen_IPDH_Req_MaxVal                                                              0x1
#define cAf6_vcat5g_sostk_pen_IPDH_Req_MinVal                                                              0x0
#define cAf6_vcat5g_sostk_pen_IPDH_Req_RstVal                                                              0x0

/*--------------------------------------
BitField Name: So_Slc3_Vld
BitField Type: R/W
BitField Desc: Source Slice 3 Read Valid
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc3_Vld_Bit_Start                                                          7
#define cAf6_vcat5g_sostk_pen_So_Slc3_Vld_Bit_End                                                            7
#define cAf6_vcat5g_sostk_pen_So_Slc3_Vld_Mask                                                           cBit7
#define cAf6_vcat5g_sostk_pen_So_Slc3_Vld_Shift                                                              7
#define cAf6_vcat5g_sostk_pen_So_Slc3_Vld_MaxVal                                                           0x1
#define cAf6_vcat5g_sostk_pen_So_Slc3_Vld_MinVal                                                           0x0
#define cAf6_vcat5g_sostk_pen_So_Slc3_Vld_RstVal                                                           0x0

/*--------------------------------------
BitField Name: So_Slc2_Vld
BitField Type: R/W
BitField Desc: Source Slice 2 Read Valid
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc2_Vld_Bit_Start                                                          6
#define cAf6_vcat5g_sostk_pen_So_Slc2_Vld_Bit_End                                                            6
#define cAf6_vcat5g_sostk_pen_So_Slc2_Vld_Mask                                                           cBit6
#define cAf6_vcat5g_sostk_pen_So_Slc2_Vld_Shift                                                              6
#define cAf6_vcat5g_sostk_pen_So_Slc2_Vld_MaxVal                                                           0x1
#define cAf6_vcat5g_sostk_pen_So_Slc2_Vld_MinVal                                                           0x0
#define cAf6_vcat5g_sostk_pen_So_Slc2_Vld_RstVal                                                           0x0

/*--------------------------------------
BitField Name: So_Slc1_Vld
BitField Type: R/W
BitField Desc: Source Slice 1 Read Valid
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc1_Vld_Bit_Start                                                          5
#define cAf6_vcat5g_sostk_pen_So_Slc1_Vld_Bit_End                                                            5
#define cAf6_vcat5g_sostk_pen_So_Slc1_Vld_Mask                                                           cBit5
#define cAf6_vcat5g_sostk_pen_So_Slc1_Vld_Shift                                                              5
#define cAf6_vcat5g_sostk_pen_So_Slc1_Vld_MaxVal                                                           0x1
#define cAf6_vcat5g_sostk_pen_So_Slc1_Vld_MinVal                                                           0x0
#define cAf6_vcat5g_sostk_pen_So_Slc1_Vld_RstVal                                                           0x0

/*--------------------------------------
BitField Name: So_Slc0_Vld
BitField Type: R/W
BitField Desc: Source Slice 0 Read Valid
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Slc0_Vld_Bit_Start                                                          4
#define cAf6_vcat5g_sostk_pen_So_Slc0_Vld_Bit_End                                                            4
#define cAf6_vcat5g_sostk_pen_So_Slc0_Vld_Mask                                                           cBit4
#define cAf6_vcat5g_sostk_pen_So_Slc0_Vld_Shift                                                              4
#define cAf6_vcat5g_sostk_pen_So_Slc0_Vld_MaxVal                                                           0x1
#define cAf6_vcat5g_sostk_pen_So_Slc0_Vld_MinVal                                                           0x0
#define cAf6_vcat5g_sostk_pen_So_Slc0_Vld_RstVal                                                           0x0

/*--------------------------------------
BitField Name: So_Mem_Vld
BitField Type: R/W
BitField Desc: Source Member Valid
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Mem_Vld_Bit_Start                                                           1
#define cAf6_vcat5g_sostk_pen_So_Mem_Vld_Bit_End                                                             1
#define cAf6_vcat5g_sostk_pen_So_Mem_Vld_Mask                                                            cBit1
#define cAf6_vcat5g_sostk_pen_So_Mem_Vld_Shift                                                               1
#define cAf6_vcat5g_sostk_pen_So_Mem_Vld_MaxVal                                                            0x1
#define cAf6_vcat5g_sostk_pen_So_Mem_Vld_MinVal                                                            0x0
#define cAf6_vcat5g_sostk_pen_So_Mem_Vld_RstVal                                                            0x0

/*--------------------------------------
BitField Name: So_Enc_Req
BitField Type: R/W
BitField Desc: Request from VCAT to ENC
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_sostk_pen_So_Enc_Req_Bit_Start                                                           0
#define cAf6_vcat5g_sostk_pen_So_Enc_Req_Bit_End                                                             0
#define cAf6_vcat5g_sostk_pen_So_Enc_Req_Mask                                                            cBit0
#define cAf6_vcat5g_sostk_pen_So_Enc_Req_Shift                                                               0
#define cAf6_vcat5g_sostk_pen_So_Enc_Req_MaxVal                                                            0x1
#define cAf6_vcat5g_sostk_pen_So_Enc_Req_MinVal                                                            0x0
#define cAf6_vcat5g_sostk_pen_So_Enc_Req_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : VCAT Data Control
Reg Addr   : 0x000FD
Reg Formula: 
    Where  : 
Reg Desc   : 
This register used to control Data Generator/Monitor for VCAT debug.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sodat_pen_Base                                                                  0x000FD
#define cAf6Reg_vcat5g_sodat_pen                                                                       0x000FD
#define cAf6Reg_vcat5g_sodat_pen_WidthVal                                                                   32
#define cAf6Reg_vcat5g_sodat_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Cfg_Ber_Mod
BitField Type: R/W
BitField Desc: BER Mode
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Mod_Bit_Start                                                         16
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Mod_Bit_End                                                           18
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Mod_Mask                                                       cBit18_16
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Mod_Shift                                                             16
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Mod_MaxVal                                                           0x7
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Mod_MinVal                                                           0x0
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Mod_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Cfg_Ber_Ena
BitField Type: R/W
BitField Desc: BER Control
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Ena_Bit_Start                                                         15
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Ena_Bit_End                                                           15
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Ena_Mask                                                          cBit15
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Ena_Shift                                                             15
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Ena_MaxVal                                                           0x1
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Ena_MinVal                                                           0x0
#define cAf6_vcat5g_sodat_pen_Cfg_Ber_Ena_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Cfg_Swp_Mod
BitField Type: R/W
BitField Desc: Data Swap Mode
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Swp_Mod_Bit_Start                                                         14
#define cAf6_vcat5g_sodat_pen_Cfg_Swp_Mod_Bit_End                                                           14
#define cAf6_vcat5g_sodat_pen_Cfg_Swp_Mod_Mask                                                          cBit14
#define cAf6_vcat5g_sodat_pen_Cfg_Swp_Mod_Shift                                                             14
#define cAf6_vcat5g_sodat_pen_Cfg_Swp_Mod_MaxVal                                                           0x1
#define cAf6_vcat5g_sodat_pen_Cfg_Swp_Mod_MinVal                                                           0x0
#define cAf6_vcat5g_sodat_pen_Cfg_Swp_Mod_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Cfg_Inv_Mod
BitField Type: R/W
BitField Desc: Data Invert Mode
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Inv_Mod_Bit_Start                                                         13
#define cAf6_vcat5g_sodat_pen_Cfg_Inv_Mod_Bit_End                                                           13
#define cAf6_vcat5g_sodat_pen_Cfg_Inv_Mod_Mask                                                          cBit13
#define cAf6_vcat5g_sodat_pen_Cfg_Inv_Mod_Shift                                                             13
#define cAf6_vcat5g_sodat_pen_Cfg_Inv_Mod_MaxVal                                                           0x1
#define cAf6_vcat5g_sodat_pen_Cfg_Inv_Mod_MinVal                                                           0x0
#define cAf6_vcat5g_sodat_pen_Cfg_Inv_Mod_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Cfg_Err_Mod
BitField Type: R/W
BitField Desc: Force Data Error
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Err_Mod_Bit_Start                                                         12
#define cAf6_vcat5g_sodat_pen_Cfg_Err_Mod_Bit_End                                                           12
#define cAf6_vcat5g_sodat_pen_Cfg_Err_Mod_Mask                                                          cBit12
#define cAf6_vcat5g_sodat_pen_Cfg_Err_Mod_Shift                                                             12
#define cAf6_vcat5g_sodat_pen_Cfg_Err_Mod_MaxVal                                                           0x1
#define cAf6_vcat5g_sodat_pen_Cfg_Err_Mod_MinVal                                                           0x0
#define cAf6_vcat5g_sodat_pen_Cfg_Err_Mod_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Cfg_Dat_Mod
BitField Type: R/W
BitField Desc: Select Mode for Data Generator/Monitor
BitField Bits: [11:09]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Mod_Bit_Start                                                          9
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Mod_Bit_End                                                           11
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Mod_Mask                                                        cBit11_9
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Mod_Shift                                                              9
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Mod_MaxVal                                                           0x7
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Mod_MinVal                                                           0x0
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Mod_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Cfg_Dat_Ena
BitField Type: R/W
BitField Desc: Enable Data Generator/Monitor
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Ena_Bit_Start                                                          8
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Ena_Bit_End                                                            8
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Ena_Mask                                                           cBit8
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Ena_Shift                                                              8
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Ena_MaxVal                                                           0x1
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Ena_MinVal                                                           0x0
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_Ena_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Cfg_Dat_VCG
BitField Type: R/W
BitField Desc: Select VCG for Data Generator/Monitor
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_VCG_Bit_Start                                                          0
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_VCG_Bit_End                                                            7
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_VCG_Mask                                                         cBit7_0
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_VCG_Shift                                                              0
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_VCG_MaxVal                                                          0xff
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_VCG_MinVal                                                           0x0
#define cAf6_vcat5g_sodat_pen_Cfg_Dat_VCG_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : VCAT Data Error Counter
Reg Addr   : 0x000FA-0x000FB
Reg Formula: 0x000FA + $r2c
    Where  : 
           + $r2c(0-1): 1 for read to clear
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sodatmon_pen_Base                                                               0x000FA
#define cAf6Reg_vcat5g_sodatmon_pen(r2c)                                                       (0x000FA+(r2c))
#define cAf6Reg_vcat5g_sodatmon_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_sodatmon_pen_WriteMask 

/*--------------------------------------
BitField Name: Dat_Err_Cnt
BitField Type: RO
BitField Desc: Data Error Counter
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_vcat5g_sodatmon_pen_Dat_Err_Cnt_Bit_Start                                                       0
#define cAf6_vcat5g_sodatmon_pen_Dat_Err_Cnt_Bit_End                                                         7
#define cAf6_vcat5g_sodatmon_pen_Dat_Err_Cnt_Mask                                                      cBit7_0
#define cAf6_vcat5g_sodatmon_pen_Dat_Err_Cnt_Shift                                                           0
#define cAf6_vcat5g_sodatmon_pen_Dat_Err_Cnt_MaxVal                                                       0xff
#define cAf6_vcat5g_sodatmon_pen_Dat_Err_Cnt_MinVal                                                        0x0
#define cAf6_vcat5g_sodatmon_pen_Dat_Err_Cnt_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Debug VCAT Data Error Sticky
Reg Addr   : 0x000FF
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_dbdatstk_pen_Base                                                               0x000FF
#define cAf6Reg_vcat5g_dbdatstk_pen                                                                    0x000FF
#define cAf6Reg_vcat5g_dbdatstk_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_dbdatstk_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: Dat_Err_Sta
BitField Type: RO
BitField Desc: Data Error Status
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Sta_Bit_Start                                                       1
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Sta_Bit_End                                                         1
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Sta_Mask                                                        cBit1
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Sta_Shift                                                           1
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Sta_MaxVal                                                        0x1
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Sta_MinVal                                                        0x0
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Sta_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Dat_Err_Stk
BitField Type: RO
BitField Desc: Data Error Sticky
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Stk_Bit_Start                                                       0
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Stk_Bit_End                                                         0
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Stk_Mask                                                        cBit0
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Stk_Shift                                                           0
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Stk_MaxVal                                                        0x1
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Stk_MinVal                                                        0x0
#define cAf6_vcat5g_dbdatstk_pen_Dat_Err_Stk_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Debug VCAT Sink Sticky
Reg Addr   : 0x50004
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_skstk_pen_Base                                                                  0x50004
#define cAf6Reg_vcat5g_skstk_pen                                                                       0x50004
#define cAf6Reg_vcat5g_skstk_pen_WidthVal                                                                   32
#define cAf6Reg_vcat5g_skstk_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Sk_Alig_FFFull
BitField Type: R/W
BitField Desc: Sink Aligment FIFO Full
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Alig_FFFull_Bit_Start                                                      20
#define cAf6_vcat5g_skstk_pen_Sk_Alig_FFFull_Bit_End                                                        20
#define cAf6_vcat5g_skstk_pen_Sk_Alig_FFFull_Mask                                                       cBit20
#define cAf6_vcat5g_skstk_pen_Sk_Alig_FFFull_Shift                                                          20
#define cAf6_vcat5g_skstk_pen_Sk_Alig_FFFull_MaxVal                                                        0x1
#define cAf6_vcat5g_skstk_pen_Sk_Alig_FFFull_MinVal                                                        0x0
#define cAf6_vcat5g_skstk_pen_Sk_Alig_FFFull_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Sk_Slc3_FFFull
BitField Type: R/W
BitField Desc: Sink Slice 3 FIFO Full
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Slc3_FFFull_Bit_Start                                                      19
#define cAf6_vcat5g_skstk_pen_Sk_Slc3_FFFull_Bit_End                                                        19
#define cAf6_vcat5g_skstk_pen_Sk_Slc3_FFFull_Mask                                                       cBit19
#define cAf6_vcat5g_skstk_pen_Sk_Slc3_FFFull_Shift                                                          19
#define cAf6_vcat5g_skstk_pen_Sk_Slc3_FFFull_MaxVal                                                        0x1
#define cAf6_vcat5g_skstk_pen_Sk_Slc3_FFFull_MinVal                                                        0x0
#define cAf6_vcat5g_skstk_pen_Sk_Slc3_FFFull_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Sk_Slc2_FFFull
BitField Type: R/W
BitField Desc: Sink Slice 2 FIFO Full
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Slc2_FFFull_Bit_Start                                                      18
#define cAf6_vcat5g_skstk_pen_Sk_Slc2_FFFull_Bit_End                                                        18
#define cAf6_vcat5g_skstk_pen_Sk_Slc2_FFFull_Mask                                                       cBit18
#define cAf6_vcat5g_skstk_pen_Sk_Slc2_FFFull_Shift                                                          18
#define cAf6_vcat5g_skstk_pen_Sk_Slc2_FFFull_MaxVal                                                        0x1
#define cAf6_vcat5g_skstk_pen_Sk_Slc2_FFFull_MinVal                                                        0x0
#define cAf6_vcat5g_skstk_pen_Sk_Slc2_FFFull_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Sk_Slc1_FFFull
BitField Type: R/W
BitField Desc: Sink Slice 1 FIFO Full
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Slc1_FFFull_Bit_Start                                                      17
#define cAf6_vcat5g_skstk_pen_Sk_Slc1_FFFull_Bit_End                                                        17
#define cAf6_vcat5g_skstk_pen_Sk_Slc1_FFFull_Mask                                                       cBit17
#define cAf6_vcat5g_skstk_pen_Sk_Slc1_FFFull_Shift                                                          17
#define cAf6_vcat5g_skstk_pen_Sk_Slc1_FFFull_MaxVal                                                        0x1
#define cAf6_vcat5g_skstk_pen_Sk_Slc1_FFFull_MinVal                                                        0x0
#define cAf6_vcat5g_skstk_pen_Sk_Slc1_FFFull_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Sk_Slc0_FFFull
BitField Type: R/W
BitField Desc: Sink Slice 0 FIFO Full
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Slc0_FFFull_Bit_Start                                                      16
#define cAf6_vcat5g_skstk_pen_Sk_Slc0_FFFull_Bit_End                                                        16
#define cAf6_vcat5g_skstk_pen_Sk_Slc0_FFFull_Mask                                                       cBit16
#define cAf6_vcat5g_skstk_pen_Sk_Slc0_FFFull_Shift                                                          16
#define cAf6_vcat5g_skstk_pen_Sk_Slc0_FFFull_MaxVal                                                        0x1
#define cAf6_vcat5g_skstk_pen_Sk_Slc0_FFFull_MinVal                                                        0x0
#define cAf6_vcat5g_skstk_pen_Sk_Slc0_FFFull_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Sk_Seq_oVld
BitField Type: R/W
BitField Desc: Sequence Processing Output Valid to DEC
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Seq_oVld_Bit_Start                                                          2
#define cAf6_vcat5g_skstk_pen_Sk_Seq_oVld_Bit_End                                                            2
#define cAf6_vcat5g_skstk_pen_Sk_Seq_oVld_Mask                                                           cBit2
#define cAf6_vcat5g_skstk_pen_Sk_Seq_oVld_Shift                                                              2
#define cAf6_vcat5g_skstk_pen_Sk_Seq_oVld_MaxVal                                                           0x1
#define cAf6_vcat5g_skstk_pen_Sk_Seq_oVld_MinVal                                                           0x0
#define cAf6_vcat5g_skstk_pen_Sk_Seq_oVld_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Sk_Seq_iVld
BitField Type: R/W
BitField Desc: Sequence Processing Input Valid
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iVld_Bit_Start                                                          1
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iVld_Bit_End                                                            1
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iVld_Mask                                                           cBit1
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iVld_Shift                                                              1
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iVld_MaxVal                                                           0x1
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iVld_MinVal                                                           0x0
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iVld_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Sk_Seq_iLom
BitField Type: R/W
BitField Desc: Sequence Processing Input LOM
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iLom_Bit_Start                                                          0
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iLom_Bit_End                                                            0
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iLom_Mask                                                           cBit0
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iLom_Shift                                                              0
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iLom_MaxVal                                                           0x1
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iLom_MinVal                                                           0x0
#define cAf6_vcat5g_skstk_pen_Sk_Seq_iLom_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Debug DDR Cache Sticky
Reg Addr   : 0x01001
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_ddrstk_pen_Base                                                                 0x01001
#define cAf6Reg_vcat5g_ddrstk_pen                                                                      0x01001
#define cAf6Reg_vcat5g_ddrstk_pen_WidthVal                                                                  32
#define cAf6Reg_vcat5g_ddrstk_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: Vcat_Wrca_Sos
BitField Type: R/W
BitField Desc: VCAT Write Cache Start of Segment
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Sos_Bit_Start                                                      21
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Sos_Bit_End                                                        21
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Sos_Mask                                                       cBit21
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Sos_Shift                                                          21
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Sos_MaxVal                                                        0x1
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Sos_MinVal                                                        0x0
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Sos_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Vcat_Wrca_Eos
BitField Type: R/W
BitField Desc: VCAT Write Cache End of Segment
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Eos_Bit_Start                                                      20
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Eos_Bit_End                                                        20
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Eos_Mask                                                       cBit20
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Eos_Shift                                                          20
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Eos_MaxVal                                                        0x1
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Eos_MinVal                                                        0x0
#define cAf6_vcat5g_ddrstk_pen_Vcat_Wrca_Eos_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Vcat_Rdca_Rdy
BitField Type: R/W
BitField Desc: VCAT Free Read Cache Ready
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Rdy_Bit_Start                                                      16
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Rdy_Bit_End                                                        16
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Rdy_Mask                                                       cBit16
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Rdy_Shift                                                          16
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Rdy_MaxVal                                                        0x1
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Rdy_MinVal                                                        0x0
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Rdy_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Vcat_Rdca_Err
BitField Type: R/W
BitField Desc: VCAT Free Cache Read Error
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Err_Bit_Start                                                      14
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Err_Bit_End                                                        14
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Err_Mask                                                       cBit14
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Err_Shift                                                          14
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Err_MaxVal                                                        0x1
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Err_MinVal                                                        0x0
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Err_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Vcat_Rdca_Get
BitField Type: R/W
BitField Desc: VCAT Free Cache Read Get
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Get_Bit_Start                                                      13
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Get_Bit_End                                                        13
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Get_Mask                                                       cBit13
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Get_Shift                                                          13
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Get_MaxVal                                                        0x1
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Get_MinVal                                                        0x0
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_Get_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Vcat_Reqff_Full
BitField Type: R/W
BitField Desc: VCAT Read Request FiFo Full
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Reqff_Full_Bit_Start                                                    12
#define cAf6_vcat5g_ddrstk_pen_Vcat_Reqff_Full_Bit_End                                                      12
#define cAf6_vcat5g_ddrstk_pen_Vcat_Reqff_Full_Mask                                                     cBit12
#define cAf6_vcat5g_ddrstk_pen_Vcat_Reqff_Full_Shift                                                        12
#define cAf6_vcat5g_ddrstk_pen_Vcat_Reqff_Full_MaxVal                                                      0x1
#define cAf6_vcat5g_ddrstk_pen_Vcat_Reqff_Full_MinVal                                                      0x0
#define cAf6_vcat5g_ddrstk_pen_Vcat_Reqff_Full_RstVal                                                      0x0

/*--------------------------------------
BitField Name: Vcat_Rdca_LLfull
BitField Type: R/W
BitField Desc: VCAT Read Cache Link FiFo Full
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_LLfull_Bit_Start                                                   10
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_LLfull_Bit_End                                                     10
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_LLfull_Mask                                                    cBit10
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_LLfull_Shift                                                       10
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_LLfull_MaxVal                                                     0x1
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_LLfull_MinVal                                                     0x0
#define cAf6_vcat5g_ddrstk_pen_Vcat_Rdca_LLfull_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Vcat_FreeCa_Empt
BitField Type: R/W
BitField Desc: VCAT Free Cache FiFo Empty
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_FreeCa_Empt_Bit_Start                                                    8
#define cAf6_vcat5g_ddrstk_pen_Vcat_FreeCa_Empt_Bit_End                                                      8
#define cAf6_vcat5g_ddrstk_pen_Vcat_FreeCa_Empt_Mask                                                     cBit8
#define cAf6_vcat5g_ddrstk_pen_Vcat_FreeCa_Empt_Shift                                                        8
#define cAf6_vcat5g_ddrstk_pen_Vcat_FreeCa_Empt_MaxVal                                                     0x1
#define cAf6_vcat5g_ddrstk_pen_Vcat_FreeCa_Empt_MinVal                                                     0x0
#define cAf6_vcat5g_ddrstk_pen_Vcat_FreeCa_Empt_RstVal                                                     0x0

/*--------------------------------------
BitField Name: DDR_Wr_Done
BitField Type: R/W
BitField Desc: DDR Data Write Done
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_DDR_Wr_Done_Bit_Start                                                         7
#define cAf6_vcat5g_ddrstk_pen_DDR_Wr_Done_Bit_End                                                           7
#define cAf6_vcat5g_ddrstk_pen_DDR_Wr_Done_Mask                                                          cBit7
#define cAf6_vcat5g_ddrstk_pen_DDR_Wr_Done_Shift                                                             7
#define cAf6_vcat5g_ddrstk_pen_DDR_Wr_Done_MaxVal                                                          0x1
#define cAf6_vcat5g_ddrstk_pen_DDR_Wr_Done_MinVal                                                          0x0
#define cAf6_vcat5g_ddrstk_pen_DDR_Wr_Done_RstVal                                                          0x0

/*--------------------------------------
BitField Name: DDR_Rd_Ena
BitField Type: R/W
BitField Desc: DDR Read Data Enable from PLA
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Ena_Bit_Start                                                          6
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Ena_Bit_End                                                            6
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Ena_Mask                                                           cBit6
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Ena_Shift                                                              6
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Ena_MaxVal                                                           0x1
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Ena_MinVal                                                           0x0
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Ena_RstVal                                                           0x0

/*--------------------------------------
BitField Name: DDR_Rd_Eop
BitField Type: R/W
BitField Desc: DDR Read Data End of Segment
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Eop_Bit_Start                                                          5
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Eop_Bit_End                                                            5
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Eop_Mask                                                           cBit5
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Eop_Shift                                                              5
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Eop_MaxVal                                                           0x1
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Eop_MinVal                                                           0x0
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Eop_RstVal                                                           0x0

/*--------------------------------------
BitField Name: DDR_Rd_Vld
BitField Type: R/W
BitField Desc: DDR Read Data Valid
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Vld_Bit_Start                                                          4
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Vld_Bit_End                                                            4
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Vld_Mask                                                           cBit4
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Vld_Shift                                                              4
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Vld_MaxVal                                                           0x1
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Vld_MinVal                                                           0x0
#define cAf6_vcat5g_ddrstk_pen_DDR_Rd_Vld_RstVal                                                           0x0

/*--------------------------------------
BitField Name: VCAT_Read
BitField Type: R/W
BitField Desc: Request Read from VCAT to PLA
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_VCAT_Read_Bit_Start                                                           3
#define cAf6_vcat5g_ddrstk_pen_VCAT_Read_Bit_End                                                             3
#define cAf6_vcat5g_ddrstk_pen_VCAT_Read_Mask                                                            cBit3
#define cAf6_vcat5g_ddrstk_pen_VCAT_Read_Shift                                                               3
#define cAf6_vcat5g_ddrstk_pen_VCAT_Read_MaxVal                                                            0x1
#define cAf6_vcat5g_ddrstk_pen_VCAT_Read_MinVal                                                            0x0
#define cAf6_vcat5g_ddrstk_pen_VCAT_Read_RstVal                                                            0x0

/*--------------------------------------
BitField Name: VCAT_Write
BitField Type: R/W
BitField Desc: Request Write from VCAT to PLA
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_VCAT_Write_Bit_Start                                                          2
#define cAf6_vcat5g_ddrstk_pen_VCAT_Write_Bit_End                                                            2
#define cAf6_vcat5g_ddrstk_pen_VCAT_Write_Mask                                                           cBit2
#define cAf6_vcat5g_ddrstk_pen_VCAT_Write_Shift                                                              2
#define cAf6_vcat5g_ddrstk_pen_VCAT_Write_MaxVal                                                           0x1
#define cAf6_vcat5g_ddrstk_pen_VCAT_Write_MinVal                                                           0x0
#define cAf6_vcat5g_ddrstk_pen_VCAT_Write_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Vcat_MFI_Read
BitField Type: R/W
BitField Desc: Delay Compensate request Read
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Read_Bit_Start                                                       1
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Read_Bit_End                                                         1
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Read_Mask                                                        cBit1
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Read_Shift                                                           1
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Read_MaxVal                                                        0x1
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Read_MinVal                                                        0x0
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Read_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Vcat_MFI_Write
BitField Type: R/W
BitField Desc: Delay Compensate request Write
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Write_Bit_Start                                                      0
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Write_Bit_End                                                        0
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Write_Mask                                                       cBit0
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Write_Shift                                                          0
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Write_MaxVal                                                       0x1
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Write_MinVal                                                       0x0
#define cAf6_vcat5g_ddrstk_pen_Vcat_MFI_Write_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Debug DDR Free Read Cache Number
Reg Addr   : 0x01002
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_ddrblk_pen_Base                                                                 0x01002
#define cAf6Reg_vcat5g_ddrblk_pen                                                                      0x01002
#define cAf6Reg_vcat5g_ddrblk_pen_WidthVal                                                                  32
#define cAf6Reg_vcat5g_ddrblk_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: RDCA_NUM
BitField Type: R/W
BitField Desc: VCAT Free Read Cache Number
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_vcat5g_ddrblk_pen_RDCA_NUM_Bit_Start                                                            0
#define cAf6_vcat5g_ddrblk_pen_RDCA_NUM_Bit_End                                                             11
#define cAf6_vcat5g_ddrblk_pen_RDCA_NUM_Mask                                                          cBit11_0
#define cAf6_vcat5g_ddrblk_pen_RDCA_NUM_Shift                                                                0
#define cAf6_vcat5g_ddrblk_pen_RDCA_NUM_MaxVal                                                           0xfff
#define cAf6_vcat5g_ddrblk_pen_RDCA_NUM_MinVal                                                             0x0
#define cAf6_vcat5g_ddrblk_pen_RDCA_NUM_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Debug DDR Interface Dump Control
Reg Addr   : 0x01004
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_ddrdumpctrl_pen_Base                                                            0x01004
#define cAf6Reg_vcat5g_ddrdumpctrl_pen                                                                 0x01004
#define cAf6Reg_vcat5g_ddrdumpctrl_pen_WidthVal                                                             32
#define cAf6Reg_vcat5g_ddrdumpctrl_pen_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: DDR_Dump_Lid
BitField Type: R/W
BitField Desc: DDR Dump Line ID
BitField Bits: [13:02]
--------------------------------------*/
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Lid_Bit_Start                                                   2
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Lid_Bit_End                                                    13
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Lid_Mask                                                 cBit13_2
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Lid_Shift                                                       2
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Lid_MaxVal                                                  0xfff
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Lid_MinVal                                                    0x0
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Lid_RstVal                                                    0x0

/*--------------------------------------
BitField Name: DDR_Dump_Sel
BitField Type: R/W
BitField Desc: Select bus info to dump 2'd0     : Write Data from VCAT to PLA
2'd1     : Read Data from PLA to VCAT 2'd2-2'd3: Reserved
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Sel_Bit_Start                                                   0
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Sel_Bit_End                                                     1
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Sel_Mask                                                  cBit1_0
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Sel_Shift                                                       0
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Sel_MaxVal                                                    0x3
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Sel_MinVal                                                    0x0
#define cAf6_vcat5g_ddrdumpctrl_pen_DDR_Dump_Sel_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DDR Dump Data Buffer
Reg Addr   : 0x01100-0x011FF
Reg Formula: 0x01100 + $dump_id
    Where  : 
           + $dump_id(0-255)
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_ddrdumpdat_pen_Base                                                             0x01100
#define cAf6Reg_vcat5g_ddrdumpdat_pen(dumpid)                                               (0x01100+(dumpid))
#define cAf6Reg_vcat5g_ddrdumpdat_pen_WidthVal                                                              32
#define cAf6Reg_vcat5g_ddrdumpdat_pen_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: DDR_Dump_Data
BitField Type: R/W
BitField Desc: DDR DUmp Data
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_vcat5g_ddrdumpdat_pen_DDR_Dump_Data_Bit_Start                                                   0
#define cAf6_vcat5g_ddrdumpdat_pen_DDR_Dump_Data_Bit_End                                                    31
#define cAf6_vcat5g_ddrdumpdat_pen_DDR_Dump_Data_Mask                                                 cBit31_0
#define cAf6_vcat5g_ddrdumpdat_pen_DDR_Dump_Data_Shift                                                       0
#define cAf6_vcat5g_ddrdumpdat_pen_DDR_Dump_Data_MaxVal                                             0xffffffff
#define cAf6_vcat5g_ddrdumpdat_pen_DDR_Dump_Data_MinVal                                                    0x0
#define cAf6_vcat5g_ddrdumpdat_pen_DDR_Dump_Data_RstVal                                                    0x0

/*------------------------------------------------------------------------------
Reg Name   : Source Enable Parity Error Control
Reg Addr   : 0x20110
Reg Formula: 
    Where  : 
Reg Desc   : 
This register used to enable error parity force for control registers.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_soparena_pen_Base                                                               0x20110
#define cAf6Reg_vcat5g_soparena_pen                                                                    0x20110
#define cAf6Reg_vcat5g_soparena_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_soparena_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: SOXC_CTRL_PARENA
BitField Type: R/W
BitField Desc: Enable Force STS XC Control Register
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_soparena_pen_SOXC_CTRL_PARENA_Bit_Start                                                  2
#define cAf6_vcat5g_soparena_pen_SOXC_CTRL_PARENA_Bit_End                                                    2
#define cAf6_vcat5g_soparena_pen_SOXC_CTRL_PARENA_Mask                                                   cBit2
#define cAf6_vcat5g_soparena_pen_SOXC_CTRL_PARENA_Shift                                                      2
#define cAf6_vcat5g_soparena_pen_SOXC_CTRL_PARENA_MaxVal                                                   0x1
#define cAf6_vcat5g_soparena_pen_SOXC_CTRL_PARENA_MinVal                                                   0x0
#define cAf6_vcat5g_soparena_pen_SOXC_CTRL_PARENA_RstVal                                                   0x0

/*--------------------------------------
BitField Name: SOMEM_CTRL_PARENA
BitField Type: R/W
BitField Desc: Enable Force Source Member Control Register
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_soparena_pen_SOMEM_CTRL_PARENA_Bit_Start                                                 1
#define cAf6_vcat5g_soparena_pen_SOMEM_CTRL_PARENA_Bit_End                                                   1
#define cAf6_vcat5g_soparena_pen_SOMEM_CTRL_PARENA_Mask                                                  cBit1
#define cAf6_vcat5g_soparena_pen_SOMEM_CTRL_PARENA_Shift                                                     1
#define cAf6_vcat5g_soparena_pen_SOMEM_CTRL_PARENA_MaxVal                                                  0x1
#define cAf6_vcat5g_soparena_pen_SOMEM_CTRL_PARENA_MinVal                                                  0x0
#define cAf6_vcat5g_soparena_pen_SOMEM_CTRL_PARENA_RstVal                                                  0x0

/*--------------------------------------
BitField Name: SOSEQ_CTRL_PARENA
BitField Type: R/W
BitField Desc: Enable Force Source Member Sequence Register
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_soparena_pen_SOSEQ_CTRL_PARENA_Bit_Start                                                 0
#define cAf6_vcat5g_soparena_pen_SOSEQ_CTRL_PARENA_Bit_End                                                   0
#define cAf6_vcat5g_soparena_pen_SOSEQ_CTRL_PARENA_Mask                                                  cBit0
#define cAf6_vcat5g_soparena_pen_SOSEQ_CTRL_PARENA_Shift                                                     0
#define cAf6_vcat5g_soparena_pen_SOSEQ_CTRL_PARENA_MaxVal                                                  0x1
#define cAf6_vcat5g_soparena_pen_SOSEQ_CTRL_PARENA_MinVal                                                  0x0
#define cAf6_vcat5g_soparena_pen_SOSEQ_CTRL_PARENA_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Source Disable Parity Error Control
Reg Addr   : 0x20111
Reg Formula: 
    Where  : 
Reg Desc   : 
This register used to disable parity error for control registers.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_sopardis_pen_Base                                                               0x20111
#define cAf6Reg_vcat5g_sopardis_pen                                                                    0x20111
#define cAf6Reg_vcat5g_sopardis_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_sopardis_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: SOXC_CTRL_PARDIS
BitField Type: R/W
BitField Desc: Disable STS XC Control Register
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_sopardis_pen_SOXC_CTRL_PARDIS_Bit_Start                                                  2
#define cAf6_vcat5g_sopardis_pen_SOXC_CTRL_PARDIS_Bit_End                                                    2
#define cAf6_vcat5g_sopardis_pen_SOXC_CTRL_PARDIS_Mask                                                   cBit2
#define cAf6_vcat5g_sopardis_pen_SOXC_CTRL_PARDIS_Shift                                                      2
#define cAf6_vcat5g_sopardis_pen_SOXC_CTRL_PARDIS_MaxVal                                                   0x1
#define cAf6_vcat5g_sopardis_pen_SOXC_CTRL_PARDIS_MinVal                                                   0x0
#define cAf6_vcat5g_sopardis_pen_SOXC_CTRL_PARDIS_RstVal                                                   0x0

/*--------------------------------------
BitField Name: SOMEM_CTRL_PARDIS
BitField Type: R/W
BitField Desc: Disable Source Member Control Register
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_sopardis_pen_SOMEM_CTRL_PARDIS_Bit_Start                                                 1
#define cAf6_vcat5g_sopardis_pen_SOMEM_CTRL_PARDIS_Bit_End                                                   1
#define cAf6_vcat5g_sopardis_pen_SOMEM_CTRL_PARDIS_Mask                                                  cBit1
#define cAf6_vcat5g_sopardis_pen_SOMEM_CTRL_PARDIS_Shift                                                     1
#define cAf6_vcat5g_sopardis_pen_SOMEM_CTRL_PARDIS_MaxVal                                                  0x1
#define cAf6_vcat5g_sopardis_pen_SOMEM_CTRL_PARDIS_MinVal                                                  0x0
#define cAf6_vcat5g_sopardis_pen_SOMEM_CTRL_PARDIS_RstVal                                                  0x0

/*--------------------------------------
BitField Name: SOSEQ_CTRL_PARDIS
BitField Type: R/W
BitField Desc: Disable Source Member Sequence Register
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_sopardis_pen_SOSEQ_CTRL_PARDIS_Bit_Start                                                 0
#define cAf6_vcat5g_sopardis_pen_SOSEQ_CTRL_PARDIS_Bit_End                                                   0
#define cAf6_vcat5g_sopardis_pen_SOSEQ_CTRL_PARDIS_Mask                                                  cBit0
#define cAf6_vcat5g_sopardis_pen_SOSEQ_CTRL_PARDIS_Shift                                                     0
#define cAf6_vcat5g_sopardis_pen_SOSEQ_CTRL_PARDIS_MaxVal                                                  0x1
#define cAf6_vcat5g_sopardis_pen_SOSEQ_CTRL_PARDIS_MinVal                                                  0x0
#define cAf6_vcat5g_sopardis_pen_SOSEQ_CTRL_PARDIS_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Source Parity Error Sticky
Reg Addr   : 0x20112
Reg Formula: 
    Where  : 
Reg Desc   : 
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_soparstk_pen_Base                                                               0x20112
#define cAf6Reg_vcat5g_soparstk_pen                                                                    0x20112
#define cAf6Reg_vcat5g_soparstk_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_soparstk_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: SOXC_CTRL_PARERR
BitField Type: R/W
BitField Desc: Source STS XC Control Register Parity Error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_soparstk_pen_SOXC_CTRL_PARERR_Bit_Start                                                  2
#define cAf6_vcat5g_soparstk_pen_SOXC_CTRL_PARERR_Bit_End                                                    2
#define cAf6_vcat5g_soparstk_pen_SOXC_CTRL_PARERR_Mask                                                   cBit2
#define cAf6_vcat5g_soparstk_pen_SOXC_CTRL_PARERR_Shift                                                      2
#define cAf6_vcat5g_soparstk_pen_SOXC_CTRL_PARERR_MaxVal                                                   0x1
#define cAf6_vcat5g_soparstk_pen_SOXC_CTRL_PARERR_MinVal                                                   0x0
#define cAf6_vcat5g_soparstk_pen_SOXC_CTRL_PARERR_RstVal                                                   0x0

/*--------------------------------------
BitField Name: SOMEM_CTRL_PARERR
BitField Type: R/W
BitField Desc: Source Member Control Register Parity Error
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_soparstk_pen_SOMEM_CTRL_PARERR_Bit_Start                                                 1
#define cAf6_vcat5g_soparstk_pen_SOMEM_CTRL_PARERR_Bit_End                                                   1
#define cAf6_vcat5g_soparstk_pen_SOMEM_CTRL_PARERR_Mask                                                  cBit1
#define cAf6_vcat5g_soparstk_pen_SOMEM_CTRL_PARERR_Shift                                                     1
#define cAf6_vcat5g_soparstk_pen_SOMEM_CTRL_PARERR_MaxVal                                                  0x1
#define cAf6_vcat5g_soparstk_pen_SOMEM_CTRL_PARERR_MinVal                                                  0x0
#define cAf6_vcat5g_soparstk_pen_SOMEM_CTRL_PARERR_RstVal                                                  0x0

/*--------------------------------------
BitField Name: SOSEQ_CTRL_PARERR
BitField Type: R/W
BitField Desc: Source Sequence Control Register Parity Error
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_soparstk_pen_SOSEQ_CTRL_PARERR_Bit_Start                                                 0
#define cAf6_vcat5g_soparstk_pen_SOSEQ_CTRL_PARERR_Bit_End                                                   0
#define cAf6_vcat5g_soparstk_pen_SOSEQ_CTRL_PARERR_Mask                                                  cBit0
#define cAf6_vcat5g_soparstk_pen_SOSEQ_CTRL_PARERR_Shift                                                     0
#define cAf6_vcat5g_soparstk_pen_SOSEQ_CTRL_PARERR_MaxVal                                                  0x1
#define cAf6_vcat5g_soparstk_pen_SOSEQ_CTRL_PARERR_MinVal                                                  0x0
#define cAf6_vcat5g_soparstk_pen_SOSEQ_CTRL_PARERR_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Enable Parity Error Control
Reg Addr   : 0x40110
Reg Formula:
    Where  :
Reg Desc   :
This register used to enable error parity force for control registers.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_skparena_pen_Base                                                               0x40110
#define cAf6Reg_vcat5g_skparena_pen                                                                    0x40110
#define cAf6Reg_vcat5g_skparena_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_skparena_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: SKXC_CTRL_PARENA
BitField Type: R/W
BitField Desc: Enable Force Sink STS XC Control Register
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_skparena_pen_SKXC_CTRL_PARENA_Bit_Start                                                  2
#define cAf6_vcat5g_skparena_pen_SKXC_CTRL_PARENA_Bit_End                                                    2
#define cAf6_vcat5g_skparena_pen_SKXC_CTRL_PARENA_Mask                                                   cBit2
#define cAf6_vcat5g_skparena_pen_SKXC_CTRL_PARENA_Shift                                                      2
#define cAf6_vcat5g_skparena_pen_SKXC_CTRL_PARENA_MaxVal                                                   0x1
#define cAf6_vcat5g_skparena_pen_SKXC_CTRL_PARENA_MinVal                                                   0x0
#define cAf6_vcat5g_skparena_pen_SKXC_CTRL_PARENA_RstVal                                                   0x0

/*--------------------------------------
BitField Name: SKMEM_CTRL_PARENA
BitField Type: R/W
BitField Desc: Enable Force Sink Member Control Register
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_skparena_pen_SKMEM_CTRL_PARENA_Bit_Start                                                 1
#define cAf6_vcat5g_skparena_pen_SKMEM_CTRL_PARENA_Bit_End                                                   1
#define cAf6_vcat5g_skparena_pen_SKMEM_CTRL_PARENA_Mask                                                  cBit1
#define cAf6_vcat5g_skparena_pen_SKMEM_CTRL_PARENA_Shift                                                     1
#define cAf6_vcat5g_skparena_pen_SKMEM_CTRL_PARENA_MaxVal                                                  0x1
#define cAf6_vcat5g_skparena_pen_SKMEM_CTRL_PARENA_MinVal                                                  0x0
#define cAf6_vcat5g_skparena_pen_SKMEM_CTRL_PARENA_RstVal                                                  0x0

/*--------------------------------------
BitField Name: SKSEQ_CTRL_PARENA
BitField Type: R/W
BitField Desc: Enable Force Sink Member Sequence Register
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_skparena_pen_SKSEQ_CTRL_PARENA_Bit_Start                                                 0
#define cAf6_vcat5g_skparena_pen_SKSEQ_CTRL_PARENA_Bit_End                                                   0
#define cAf6_vcat5g_skparena_pen_SKSEQ_CTRL_PARENA_Mask                                                  cBit0
#define cAf6_vcat5g_skparena_pen_SKSEQ_CTRL_PARENA_Shift                                                     0
#define cAf6_vcat5g_skparena_pen_SKSEQ_CTRL_PARENA_MaxVal                                                  0x1
#define cAf6_vcat5g_skparena_pen_SKSEQ_CTRL_PARENA_MinVal                                                  0x0
#define cAf6_vcat5g_skparena_pen_SKSEQ_CTRL_PARENA_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Disable Parity Error Control
Reg Addr   : 0x40111
Reg Formula:
    Where  :
Reg Desc   :
This register used to disable parity error for control registers.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_skpardis_pen_Base                                                               0x40111
#define cAf6Reg_vcat5g_skpardis_pen                                                                    0x40111
#define cAf6Reg_vcat5g_skpardis_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_skpardis_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: SKXC_CTRL_PARDIS
BitField Type: R/W
BitField Desc: Disable Sink STS XC Control Register
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_skpardis_pen_SKXC_CTRL_PARDIS_Bit_Start                                                  2
#define cAf6_vcat5g_skpardis_pen_SKXC_CTRL_PARDIS_Bit_End                                                    2
#define cAf6_vcat5g_skpardis_pen_SKXC_CTRL_PARDIS_Mask                                                   cBit2
#define cAf6_vcat5g_skpardis_pen_SKXC_CTRL_PARDIS_Shift                                                      2
#define cAf6_vcat5g_skpardis_pen_SKXC_CTRL_PARDIS_MaxVal                                                   0x1
#define cAf6_vcat5g_skpardis_pen_SKXC_CTRL_PARDIS_MinVal                                                   0x0
#define cAf6_vcat5g_skpardis_pen_SKXC_CTRL_PARDIS_RstVal                                                   0x0

/*--------------------------------------
BitField Name: SKMEM_CTRL_PARDIS
BitField Type: R/W
BitField Desc: Disable Sink Member Control Register
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_skpardis_pen_SKMEM_CTRL_PARDIS_Bit_Start                                                 1
#define cAf6_vcat5g_skpardis_pen_SKMEM_CTRL_PARDIS_Bit_End                                                   1
#define cAf6_vcat5g_skpardis_pen_SKMEM_CTRL_PARDIS_Mask                                                  cBit1
#define cAf6_vcat5g_skpardis_pen_SKMEM_CTRL_PARDIS_Shift                                                     1
#define cAf6_vcat5g_skpardis_pen_SKMEM_CTRL_PARDIS_MaxVal                                                  0x1
#define cAf6_vcat5g_skpardis_pen_SKMEM_CTRL_PARDIS_MinVal                                                  0x0
#define cAf6_vcat5g_skpardis_pen_SKMEM_CTRL_PARDIS_RstVal                                                  0x0

/*--------------------------------------
BitField Name: SKSEQ_CTRL_PARDIS
BitField Type: R/W
BitField Desc: Disable Sink Member Sequence Register
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_skpardis_pen_SKSEQ_CTRL_PARDIS_Bit_Start                                                 0
#define cAf6_vcat5g_skpardis_pen_SKSEQ_CTRL_PARDIS_Bit_End                                                   0
#define cAf6_vcat5g_skpardis_pen_SKSEQ_CTRL_PARDIS_Mask                                                  cBit0
#define cAf6_vcat5g_skpardis_pen_SKSEQ_CTRL_PARDIS_Shift                                                     0
#define cAf6_vcat5g_skpardis_pen_SKSEQ_CTRL_PARDIS_MaxVal                                                  0x1
#define cAf6_vcat5g_skpardis_pen_SKSEQ_CTRL_PARDIS_MinVal                                                  0x0
#define cAf6_vcat5g_skpardis_pen_SKSEQ_CTRL_PARDIS_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Sink Parity Error Sticky
Reg Addr   : 0x40112
Reg Formula:
    Where  :
Reg Desc   :
.

------------------------------------------------------------------------------*/
#define cAf6Reg_vcat5g_skparstk_pen_Base                                                               0x40112
#define cAf6Reg_vcat5g_skparstk_pen                                                                    0x40112
#define cAf6Reg_vcat5g_skparstk_pen_WidthVal                                                                32
#define cAf6Reg_vcat5g_skparstk_pen_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: SKXC_CTRL_PARERR
BitField Type: R/W
BitField Desc: Sink STS XC Control Register Parity Error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_vcat5g_skparstk_pen_SKXC_CTRL_PARERR_Bit_Start                                                  2
#define cAf6_vcat5g_skparstk_pen_SKXC_CTRL_PARERR_Bit_End                                                    2
#define cAf6_vcat5g_skparstk_pen_SKXC_CTRL_PARERR_Mask                                                   cBit2
#define cAf6_vcat5g_skparstk_pen_SKXC_CTRL_PARERR_Shift                                                      2
#define cAf6_vcat5g_skparstk_pen_SKXC_CTRL_PARERR_MaxVal                                                   0x1
#define cAf6_vcat5g_skparstk_pen_SKXC_CTRL_PARERR_MinVal                                                   0x0
#define cAf6_vcat5g_skparstk_pen_SKXC_CTRL_PARERR_RstVal                                                   0x0

/*--------------------------------------
BitField Name: SKMEM_CTRL_PARERR
BitField Type: R/W
BitField Desc: Sink Member Control Register Parity Error
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_vcat5g_skparstk_pen_SKMEM_CTRL_PARERR_Bit_Start                                                 1
#define cAf6_vcat5g_skparstk_pen_SKMEM_CTRL_PARERR_Bit_End                                                   1
#define cAf6_vcat5g_skparstk_pen_SKMEM_CTRL_PARERR_Mask                                                  cBit1
#define cAf6_vcat5g_skparstk_pen_SKMEM_CTRL_PARERR_Shift                                                     1
#define cAf6_vcat5g_skparstk_pen_SKMEM_CTRL_PARERR_MaxVal                                                  0x1
#define cAf6_vcat5g_skparstk_pen_SKMEM_CTRL_PARERR_MinVal                                                  0x0
#define cAf6_vcat5g_skparstk_pen_SKMEM_CTRL_PARERR_RstVal                                                  0x0

/*--------------------------------------
BitField Name: SKSEQ_CTRL_PARERR
BitField Type: R/W
BitField Desc: Sink Sequence Control Register Parity Error
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_vcat5g_skparstk_pen_SKSEQ_CTRL_PARERR_Bit_Start                                                 0
#define cAf6_vcat5g_skparstk_pen_SKSEQ_CTRL_PARERR_Bit_End                                                   0
#define cAf6_vcat5g_skparstk_pen_SKSEQ_CTRL_PARERR_Mask                                                  cBit0
#define cAf6_vcat5g_skparstk_pen_SKSEQ_CTRL_PARERR_Shift                                                     0
#define cAf6_vcat5g_skparstk_pen_SKSEQ_CTRL_PARERR_MaxVal                                                  0x1
#define cAf6_vcat5g_skparstk_pen_SKSEQ_CTRL_PARERR_MinVal                                                  0x0
#define cAf6_vcat5g_skparstk_pen_SKSEQ_CTRL_PARERR_RstVal                                                  0x0
#endif /* _AF6_REG_AF6CCI0012_RD_VCAT_H_ */

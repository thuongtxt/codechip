/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CONCATE
 *
 * File        : Tha60210012VcgNonVcat.c
 *
 * Created Date: Sep 16, 2015
 *
 * Description : VCG Non-VCAT of 60210012 product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/concate/AtConcateGroupInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../mpeg/Tha60210012ModuleMpeg.h"
#include "Tha60210012Vcg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012VcgNonVcat
    {
    tAtConcateGroup super;
    }tTha60210012VcgNonVcat;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60210012VcgCommon.h"

static void Override(AtConcateGroup self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtConcateGroup(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012VcgNonVcat);
    }

static AtConcateGroup ObjectInit(AtConcateGroup self, AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtConcateGroupObjectInit(self, concateModule, vcgId, memberType, concateType) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtConcateGroup Tha60210012VcgNonVcatNew(AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtConcateGroup newVcg = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newVcg == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newVcg, concateModule, vcgId, memberType, concateType);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : Tha60210012ModuleConcate.h
 * 
 * Created Date: Sep 14, 2015
 *
 * Description : Concatenation module of Tha60210012
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULECONCATE_H_
#define _THA60210012MODULECONCATE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleConcate.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleConcate * Tha60210012ModuleConcate;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleConcate Tha60210012ModuleConcateNew(AtDevice device);
uint32 Tha60210012ModuleConcateBaseAddress(Tha60210012ModuleConcate self);
uint8 Tha60210012ModuleConcateHwStsSlice24ToMapStsId(Tha60210012ModuleConcate self, uint8 hwPdhSliceId, uint8 hwPdhStsId);
uint8 Tha60210012ModuleConcateHwStsSlice48ToMapStsId(Tha60210012ModuleConcate self, uint8 slice48, uint8 hwSts48);

void Tha60210012ModuleConcateMapStsIdToStsAndSlice24(Tha60210012ModuleConcate self, uint8 sts96, uint8 *slice24, uint8 *sts24);
void Tha60210012ModuleConcateDataDump(AtModuleConcate self, uint8 dumpMode, uint8 selBus);
void Tha60210012ModuleConcateDdrDump(AtModuleConcate self, uint32 lineId, uint8 selBus);

void Tha60210012ModuleConcateGlobalConcateTypeSet(AtModuleConcate self, eAtConcateGroupType concateType);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULECONCATE_H_ */


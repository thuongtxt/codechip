/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha60210012ModuleOcn.c
 *
 * Created Date: Mar 01, 2016
 *
 * Description : Module OCN
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/cdr/ThaModuleCdrInternal.h"
#include "../../../default/cdr/ThaModuleCdrStm.h"
#include "../ram/Tha60210012ModuleRam.h"
#include "../man/Tha60210012Device.h"
#include "Tha60210012ModuleOcnReg.h"
#include "Tha60210012ModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tThaModuleOcnMethods m_ThaModuleOcnOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsOcnRegister(AtModuleDeviceGet(self), address);
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
        "OCN Rx Bridge and Roll SXC Control 0",
        "OCN Rx Bridge and Roll SXC Control 1",
        "OCN Tx Bridge and Roll SXC Control 0",
        "OCN Tx Bridge and Roll SXC Control 1",
        "OCN STS Pointer Interpreter Per Channel Control 0",
        "OCN STS Pointer Interpreter Per Channel Control 1",
        "OCN STS Pointer Generator Per Channel Control 0",
        "OCN STS Pointer Generator Per Channel Control 1",
        "OCN Rx SXC Control 0",
        "OCN Rx SXC Control 1",
        "OCN Rx SXC Control 2",
        "OCN Rx SXC Control 3",
        "OCN Tx SXC Control 0",
        "OCN Tx SXC Control 1",
        "OCN Rx High Order Map concatenate configuration 0",
        "OCN Rx High Order Map concatenate configuration 1",
        "OCN RXPP Per STS payload Control 0",
        "OCN RXPP Per STS payload Control 1",
        "OCN VTTU Pointer Interpreter Per Channel Control 0",
        "OCN VTTU Pointer Interpreter Per Channel Control 1",
        "OCN TXPP Per STS Multiplexing Control 0",
        "OCN TXPP Per STS Multiplexing Control 1",
        "OCN VTTU Pointer Generator Per Channel Control 0",
        "OCN VTTU Pointer Generator Per Channel Control 1"
        };

    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    return Tha60210012InternalRamOcnNew(self, ramId, localRamId);
    }

static eBool CanMovePathAisRxForcingPoint(ThaModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 supportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x9, 0x1015);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= supportedVersion) ? cAtTrue : cAtFalse;
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, mMethodsGet(ocnModule), sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, CanMovePathAisRxForcingPoint);
        }

    mMethodsSet(ocnModule, &m_ThaModuleOcnOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleOcn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleOcn);
    }

AtModule Tha60210012ModuleOcnObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210012ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210012ModuleOcnObjectInit(newModule, device);
    }

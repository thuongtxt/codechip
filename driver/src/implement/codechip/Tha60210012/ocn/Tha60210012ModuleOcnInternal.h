/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60210012ModuleOcnInternal.h
 * 
 * Created Date: Aug 19, 2016
 *
 * Description : Module OCN internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEOCNINTERNAL_H_
#define _THA60210012MODULEOCNINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/ocn/Tha60210011ModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleOcn * Tha60210012ModuleOcn;
typedef struct tTha60210012ModuleOcn
    {
    tTha60210011ModuleOcn super;
    }tTha60210012ModuleOcn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210012ModuleOcnObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEOCNINTERNAL_H_ */


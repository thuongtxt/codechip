/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210012PdhNxDs0.c
 *
 * Created Date: Apr 8, 2016
 *
 * Description : NxDs0 Implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/pdh/ThaPdhNxDs0.h"
#include "../pwe/Tha60210012ModulePweBlockManager.h"
#include "../encap/Tha60210012PhysicalHdlcChannel.h"
#include "../common/Tha60210012Channel.h"
#include "Tha60210012ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha60210012PdhNxDs0 *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012PdhNxDs0
    {
    tThaPdhNxDs0 super;
    }tTha60210012PdhNxDs0;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet HwResourceAllocate(AtChannel self)
    {
    return Tha60210012PwCircuitNxDs0BlockAllocate(self);
    }

static eAtRet HwResourceDeallocate(AtChannel self)
    {
    return Tha60210012ChannelBlockDeallocate(self);
    }

static eAtRet TxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    const uint8 cDe1PayloadType = 0x0;
    AtHdlcChannel logicChannel = (AtHdlcChannel)AtChannelBoundEncapChannel(self);

    if (enable)
        {
        ret |= m_AtChannelMethods->TxEncapConnectionEnable(self, enable);
        ret |= Tha60210012TxHdlcServiceSetup(logicChannel, enable, cDe1PayloadType);

        return ret;
        }

    ret |= Tha60210012TxHdlcServiceSetup(logicChannel, enable, 0);
    ret |= m_AtChannelMethods->TxEncapConnectionEnable(self, enable);

    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    Tha60210012ChannelSerializeHwResource((AtChannel)self, encoder);
    }

static void OverrideAtObject(AtPdhNxDS0 self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPdhNxDS0 self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, HwResourceAllocate);
        mMethodOverride(m_AtChannelOverride, HwResourceDeallocate);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPdhNxDS0 self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012PdhNxDs0);
    }

static AtPdhNxDS0 ObjectInit(AtPdhNxDS0 self, AtPdhDe1 de1, uint32 mask)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (ThaPdhNxDs0ObjectInit(self, de1, mask) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhNxDS0 Tha60210012PdhNxDs0New(AtPdhDe1 de1, uint32 mask)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhNxDS0 newDs0 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDs0 == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDs0, de1, mask);
    }

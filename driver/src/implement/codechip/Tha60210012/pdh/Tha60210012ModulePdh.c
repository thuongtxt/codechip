/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210012ModulePdh.c
 *
 * Created Date: May 6, 2015
 *
 * Description : PDH module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/prm/ThaPdhPrmInterruptManager.h"
#include "../../../default/pdh/mdl/ThaPdhMdlInterruptManager.h"
#include "../../Tha60210031/pdh/Tha60210031PdhDe1.h"
#include "../../Tha60210011/ram/Tha60210011InternalRam.h"
#include "../man/Tha60210012Device.h"
#include "../ram/Tha60210012ModuleRam.h"
#include "Tha60210012ModulePdhInternal.h"
#include "Tha60210012ModulePdh.h"
#include "Tha60210012PdhPrmController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tAtModulePdhMethods          m_AtModulePdhOverride;
static tThaModulePdhMethods         m_ThaModulePdhOverride;
static tTha60210031ModulePdhMethods m_Tha60210031ModulePdhOverride;
static tTha60210011ModulePdhMethods m_Tha60210011ModulePdhOverride;

/* Save super implementation */
static const tAtModulePdhMethods     *m_AtModulePdhMethods     = NULL;
static const tThaModulePdhMethods    *m_ThaModulePdhMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return 0x0;
    }

static eBool MdlIsSupported(ThaModulePdh self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportAlarmForwardingStatus(ThaModulePdh self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return 0x0;
    }

static eBool ForcedAisCorrectionIsSupported(ThaModulePdh self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportDe3Disabling(ThaModulePdh self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return 0x0;
    }

static uint32 StartVersionSupportAisDownStream(ThaModulePdh self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return 0x0;
    }

static AtPdhDe3 VcDe3Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    uint8 slice, hwSts;
    ThaSdhChannel2HwMasterStsId(sdhVc, cAtModulePdh, &slice, &hwSts);
    return Tha60210012PdhDe3New(hwSts, self);
    }

static AtPdhDe1 De2De1Create(AtModulePdh self, AtPdhDe2 de2, uint32 de1Id)
    {
    AtUnused(de2);
    return Tha60210012PdhDe2De1New(de1Id, self);
    }

static AtPdhDe1 VcDe1Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    return Tha60210012PdhVcDe1New((AtSdhVc)sdhVc, self);
    }

static uint32 StartVersionSupportMdlFilterMessageByType(ThaModulePdh self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return 0x0;
    }

static uint32 MdlRxBufWordOffset(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return 128UL;
    }

static AtPdhNxDS0 NxDs0Create(AtModulePdh self, AtPdhDe1 de1, uint32 timeslotBitmap)
    {
    AtUnused(self);
    return Tha60210012PdhNxDs0New(de1, timeslotBitmap);
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsPdhRegister(AtModuleDeviceGet(self), address);
    }

static ThaVersionReader VersionReader(ThaModulePdh self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static eBool DatalinkFcsBitOrderIsSupported(ThaModulePdh self)
    {
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    if (hwVersion >= ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x0, 0x1016))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PrmStsAndTug2Offset(ThaModulePdh self)
    {
    /*((STSID[4:3]*7 + VTGID)*128) + (sliceID*32 + STSID[2:0]*4 + VTID)*/
    AtUnused(self);
    return 128UL;
    }

static uint8 NumSlices(ThaModulePdh self)
    {
    AtUnused(self);
    return 4;
    }

static const char** TxPrmMdlRamsDescription(Tha60210011ModulePdh self, uint32* numRams)
    {
    static const char *txRamsDescription[] =
        {
        "DLK TX PRM configuration",
        "DLK TX PRM LB configuration",
        "DLK TX MDL configuration, slice 0",
        "DLK TX MDL buffer 1, slice 0",
        "DLK TX MDL buffer 2, slice 0",
        "DLK TX MDL configuration, slice 1",
        "DLK TX MDL buffer 1, slice 1",
        "DLK TX MDL buffer 2, slice 1",
        "DLK TX MDL configuration, slice 2",
        "DLK TX MDL buffer 1, slice 2",
        "DLK TX MDL buffer 2, slice 2",
        "DLK TX MDL configuration, slice 3",
        "DLK TX MDL buffer 1, slice 3",
        "DLK TX MDL buffer 2, slice 3",
        };

    AtUnused(self);
    if (numRams)
        *numRams = mCount(txRamsDescription);

    return txRamsDescription;
    }

static uint32 NumPdhRams(ThaModulePdh self)
    {
    uint32 numSlices = mMethodsGet(self)->NumSlices(self);
    return Tha60210011ModulePdhNumPdhRamsInSlice((Tha60210011ModulePdh)self) * numSlices; /* 13x12 */
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    if (localRamId < NumPdhRams((ThaModulePdh)self))
        return Tha60210012InternalRamPdhNew(self, ramId, localRamId);

    return Tha60210011InternalRamDatalinkNew(self, ramId, localRamId);
    }

static eBool De3FramedSatopIsSupported(ThaModulePdh self)
    {
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    uint32 startSupportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x7, 0x1041);
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static eBool De3AisSelectablePatternIsSupported(ThaModulePdh self)
    {
    uint32 startSupportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x7, 0x1041);
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static AtPdhPrmController PrmControllerObjectCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return Tha60210012PdhPrmControllerNew(de1);
    }

static AtInterruptManager PrmInterruptManagerObjectCreate(ThaModulePdh self)
    {
    return Tha60210012PdhPrmInterruptManagerNew((AtModule)self);
    }

static AtInterruptManager MdlInterruptManagerObjectCreate(ThaModulePdh self)
    {
    return Tha60210012PdhMdlInterruptManagerNew((AtModule)self);
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule pdh = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(pdh), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        }

    mMethodsSet(pdh, &m_AtModuleOverride);
    }

static uint32 StartVersionSupportDe1LomfConsequentialAction(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x9, 0x1089);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, m_AtModulePdhMethods, sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, De2De1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe3Create);
        mMethodOverride(m_AtModulePdhOverride, NxDs0Create);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(pdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportPrm);
        mMethodOverride(m_ThaModulePdhOverride, MdlIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportAlarmForwardingStatus);
        mMethodOverride(m_ThaModulePdhOverride, ForcedAisCorrectionIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3Disabling);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportAisDownStream);
        mMethodOverride(m_ThaModulePdhOverride, PrmStsAndTug2Offset);
        mMethodOverride(m_ThaModulePdhOverride, NumSlices);
        mMethodOverride(m_ThaModulePdhOverride, De3FramedSatopIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De3AisSelectablePatternIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, PrmControllerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, PrmInterruptManagerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, MdlInterruptManagerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1LomfConsequentialAction);
        }

    mMethodsSet(pdhModule, &m_ThaModulePdhOverride);
    }

static void OverrideTha60210031ModulePdh(AtModulePdh self)
    {
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031ModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_Tha60210031ModulePdhOverride));

        mMethodOverride(m_Tha60210031ModulePdhOverride, StartVersionSupportMdlFilterMessageByType);
        mMethodOverride(m_Tha60210031ModulePdhOverride, DatalinkFcsBitOrderIsSupported);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxBufWordOffset);
        }

    mMethodsSet(pdhModule, &m_Tha60210031ModulePdhOverride);
    }

static void OverrideTha60210011ModulePdh(AtModulePdh self)
    {
    Tha60210011ModulePdh pdhModule = (Tha60210011ModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_Tha60210011ModulePdhOverride));

        mMethodOverride(m_Tha60210011ModulePdhOverride, TxPrmMdlRamsDescription);
        }

    mMethodsSet(pdhModule, &m_Tha60210011ModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModule(self);
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    OverrideTha60210031ModulePdh(self);
    OverrideTha60210011ModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModulePdh);
    }

AtModulePdh Tha60210012ModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60210012ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModulePdhObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210012PdhMdlInterruptManager.c
 *
 * Created Date: Sep 21, 2017
 *
 * Description : MDL interrupt manager implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pdh/ThaModulePdh.h"
#include "../../../Tha60210011/pdh/mdl/Tha60210011PdhMdlInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012PdhMdlInterruptManager
    {
    tTha60210011PdhMdlInterruptManager super;
    }tTha60210012PdhMdlInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaInterruptManagerMethods       m_ThaInterruptManagerOverride;
static tThaPdhMdlInterruptManagerMethods m_ThaPdhMdlInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 InterruptStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x29080UL;
    }

static uint32 GlbInterruptEnableRegister(ThaPdhMdlInterruptManager self)
    {
    AtUnused(self);
    return 0x291FEUL;
    }

static uint32 GlbInterruptStatusRegister(ThaPdhMdlInterruptManager self)
    {
    AtUnused(self);
    return 0x291FFUL;
    }

static uint32 SliceInterruptStatusRegister(ThaPdhMdlInterruptManager self)
    {
    AtUnused(self);
    return 0x29180UL;
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptStatusRegister);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideThaPdhMdlInterruptManager(AtInterruptManager self)
    {
    ThaPdhMdlInterruptManager manager = (ThaPdhMdlInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhMdlInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaPdhMdlInterruptManagerOverride));

        mMethodOverride(m_ThaPdhMdlInterruptManagerOverride, GlbInterruptEnableRegister);
        mMethodOverride(m_ThaPdhMdlInterruptManagerOverride, GlbInterruptStatusRegister);
        mMethodOverride(m_ThaPdhMdlInterruptManagerOverride, SliceInterruptStatusRegister);
        }

    mMethodsSet(manager, &m_ThaPdhMdlInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideThaInterruptManager(self);
    OverrideThaPdhMdlInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PdhMdlInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PdhMdlInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60210012PdhMdlInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newManager, module);
    }

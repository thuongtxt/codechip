/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210012ModulePdhInternal.h
 * 
 * Created Date: Jun 17, 2016
 *
 * Description : 60210012 Module PDH internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEPDHINTERNAL_H_
#define _THA60210012MODULEPDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/pdh/Tha60210051ModulePdhInternal.h"
#include "Tha60210012ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModulePdh
    {
    tTha60210051ModulePdh super;
    }tTha60210012ModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh Tha60210012ModulePdhObjectInit(AtModulePdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEPDHINTERNAL_H_ */


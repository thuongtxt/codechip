/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210012PdhPrmController.h
 * 
 * Created Date: Aug 07, 2017
 *
 * Description : PRM controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PDHPRMCONTROLLER_H_
#define _THA60210012PDHPRMCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhPrmController Tha60210012PdhPrmControllerNew(AtPdhDe1 de1);

#endif /* _THA60210012PDHPRMCONTROLLER_H_ */


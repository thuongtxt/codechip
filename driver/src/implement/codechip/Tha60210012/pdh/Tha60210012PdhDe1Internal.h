/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : PDH
 *
 * File        : Tha60210012PdhDe1.h
 *
 * Created Date: MAR 15, 2016
 *
 * Description : PDH DS1/E1 header.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PDHDE1_H_
#define _THA60210012PDHDE1_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031PdhVcDe1Internal.h"
#include "../../Tha60210031/pdh/Tha60210031PdhDe2De1Internal.h"
#include "../common/Tha60210012Channel.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012PdhVcDe1
    {
    tTha60210031PdhVcDe1 super;
    }tTha60210012PdhVcDe1;

typedef struct tTha60210012PdhDe2De1
    {
    tTha60210031PdhDe2De1 super;
    }tTha60210012PdhDe2De1;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
AtPdhDe1 Tha60210012PdhDe2De1New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha60210012PdhVcDe1New(AtSdhVc vc1x, AtModulePdh module);
AtPdhDe1 Tha60210012PdhVcDe1ObjectInit(AtPdhDe1 self, AtSdhVc vc1x, AtModulePdh module);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210012PDHDE1_H_ */

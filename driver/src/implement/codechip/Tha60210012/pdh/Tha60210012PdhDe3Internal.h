/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210012PdhDe3Internal.h
 * 
 * Created Date: Jun 23, 2016
 *
 * Description : 60210012 PDH DE3 internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PDHDE3INTERNAL_H_
#define _THA60210012PDHDE3INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031PdhDe3Internal.h"
#include "../common/Tha60210012Channel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012PdhDe3
    {
    tTha60210031PdhDe3 super;
    }tTha60210012PdhDe3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe3 Tha60210012PdhDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PDHDE3INTERNAL_H_ */


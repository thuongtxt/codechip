/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210012PdhDe1Common.h
 *
 * Created Date: Aug, 2015
 *
 * Description : DE1 common functions
 *
 * Notes       : This will share between M13/VC-De1 function.
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PDHDE1COMMON_H_
#define _THA60210012PDHDE1COMMON_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/concate/binder/AtVcgBinder.h"
#include "../../../../generic/concate/AtModuleConcateInternal.h"
#include "../../../default/encap/dynamic/ThaDynamicHdlcChannel.h"
#include "../../Tha60210031/pdh/Tha60210031PdhDe1.h"
#include "../pwe/Tha60210012ModulePweBlockManager.h"
#include "../pda/Tha60210012ModulePda.h"
#include "../encap/Tha60210012PhysicalHdlcChannel.h"
#include "Tha60210012ModulePdh.h"
#include "Tha60210012PdhDe1Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;

/* Super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleConcate ConcateModule(AtChannel self)
    {
    return (AtModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleConcate);
    }

static AtVcgBinder VcgBinderCreate(AtChannel self)
    {
    return AtModuleConcateCreateVcgBinderForDe1(ConcateModule(self), (AtPdhDe1)self);
    }

static eAtRet HwResourceAllocate(AtChannel self)
    {
    return Tha60210012PwCircuitDe1BlockAllocate(self);
    }

static eAtRet HwResourceDeallocate(AtChannel self)
    {
    return Tha60210012ChannelBlockDeallocate(self);
    }

static eAtRet TxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    const uint8 cDe1PayloadType = 0x0;
    AtHdlcChannel logicChannel = (AtHdlcChannel)AtChannelBoundEncapChannel(self);

    if (enable)
        {
        ret |= m_AtChannelMethods->TxEncapConnectionEnable(self, enable);
        ret |= Tha60210012TxHdlcServiceSetup(logicChannel, enable, cDe1PayloadType);

        return ret;
        }

    ret |= Tha60210012TxHdlcServiceSetup(logicChannel, enable, 0);
    ret |= m_AtChannelMethods->TxEncapConnectionEnable(self, enable);

    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    Tha60210012ChannelSerializeHwResource((AtChannel)self, encoder);
    }

static void OverrideAtChannel(AtPdhDe1 self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, VcgBinderCreate);
        mMethodOverride(m_AtChannelOverride, HwResourceAllocate);
        mMethodOverride(m_AtChannelOverride, HwResourceDeallocate);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtPdhDe1 self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

#endif /* _THA60210012PDHDE1COMMON_H_ */

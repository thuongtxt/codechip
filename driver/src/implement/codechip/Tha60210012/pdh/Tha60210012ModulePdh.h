/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210012ModulePdh.h
 * 
 * Created Date: MAR 16, 2016
 *
 * Description : PDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEPDH_H_
#define _THA60210012MODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe3 Tha60210012PdhDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha60210012PdhDe2De1New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha60210012PdhVcDe1New(AtSdhVc vc1x, AtModulePdh module);
AtPdhNxDS0 Tha60210012PdhNxDs0New(AtPdhDe1 de1, uint32 mask);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEPDH_H_ */


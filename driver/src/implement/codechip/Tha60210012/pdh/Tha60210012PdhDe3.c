/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210012PdhDe3.c
 *
 * Created Date: Mar 4, 2015
 *
 * Description : DS3/E3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/concate/AtModuleConcateInternal.h"
#include "../../../../generic/concate/binder/AtVcgBinder.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/encap/dynamic/ThaDynamicHdlcChannel.h"
#include "../../../default/man/ThaDevice.h"
#include "../pwe/Tha60210012ModulePweBlockManager.h"
#include "../pda/Tha60210012ModulePda.h"
#include "../common/Tha60210012Channel.h"
#include "../encap/Tha60210012PhysicalHdlcChannel.h"
#include "Tha60210012ModulePdh.h"
#include "Tha60210012PdhDe3Internal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha60210012PdhDe3 *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tThaPdhDe3Methods    m_ThaPdhDe3Override;
static tAtPdhChannelMethods m_AtPdhChannelOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods     = NULL;
static const tAtChannelMethods      *m_AtChannelMethods   = NULL;
static const tAtPdhChannelMethods   *m_AtPdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleConcate ConcateModule(AtChannel self)
    {
    return (AtModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleConcate);
    }

static AtVcgBinder VcgBinderCreate(AtChannel self)
    {
    return AtModuleConcateCreateVcgBinderForDe3(ConcateModule(self), (AtPdhDe3)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    Tha60210012ChannelSerializeHwResource((AtChannel)self, encoder);
    }

static eAtRet HwResourceAllocate(AtChannel self)
    {
    return Tha60210012PwCircuitDe3BlockAllocate(self);
    }

static eAtRet HwResourceDeallocate(AtChannel self)
    {
    return Tha60210012ChannelBlockDeallocate(self);
    }

static eAtRet TxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    uint8 hwSts, hwSlice;
    const uint8 cDe3PayloadType = 0xF;
    AtHdlcChannel logicChannel = (AtHdlcChannel)AtChannelBoundEncapChannel(self);

    if (enable)
        {
        ret |= m_AtChannelMethods->TxEncapConnectionEnable(self, enable);
        ret |= Tha60210012TxHdlcServiceSetup(logicChannel, enable, cDe3PayloadType);
        ThaPdhChannelHwIdGet((AtPdhChannel)self, cThaModulePda, &hwSlice, &hwSts);
        ret |= Tha60210012ModulePdaHdlcHwStsIdSet(ThaHdlcChannelPhysicalChannelGet(logicChannel), hwSts);

        return ret;
        }

    ret |= Tha60210012TxHdlcServiceSetup(logicChannel, enable, 0);
    ret |= m_AtChannelMethods->TxEncapConnectionEnable(self, enable);

    return ret;
    }

static eBool IsManagedByModulePdh(ThaPdhDe3 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasLineLayer(AtPdhChannel self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtObject(AtPdhDe3 self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPdhDe3 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, VcgBinderCreate);
        mMethodOverride(m_AtChannelOverride, HwResourceAllocate);
        mMethodOverride(m_AtChannelOverride, HwResourceDeallocate);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPdhChannel(AtPdhDe3 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(m_AtPdhChannelOverride));

        mMethodOverride(m_AtPdhChannelOverride, HasLineLayer);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideThaPdhDe3(AtPdhDe3 self)
    {
    ThaPdhDe3 channel = (ThaPdhDe3)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe3Override, mMethodsGet(channel), sizeof(m_ThaPdhDe3Override));

        mMethodOverride(m_ThaPdhDe3Override, IsManagedByModulePdh);
        }

    mMethodsSet(channel, &m_ThaPdhDe3Override);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    OverrideThaPdhDe3(self);
    }

AtPdhDe3 Tha60210012PdhDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60210012PdhDe3));

    /* Supper constructor */
    if (Tha60210031PdhDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe3 Tha60210012PdhDe3New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha60210012PdhDe3));
    if (newDe3 == NULL)
        return NULL;

    /* construct it */
    return Tha60210012PdhDe3ObjectInit(newDe3, channelId, module);
    }

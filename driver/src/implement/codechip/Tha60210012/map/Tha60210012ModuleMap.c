/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60210012ModuleMap.c
 *
 * Created Date: Feb 29, 2016
 *
 * Description : 60210012 mapping
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pw/ThaModulePw.h"
#include "../../../default/encap/dynamic/ThaEncapDynamicBinder.h"
#include "../../Tha60210011/pw/activator/Tha60210011HwPw.h"
#include "../../Tha60210011/map/Tha60210011ModuleMapLoReg.h"
#include "../../Tha60210011/ram/Tha60210011InternalRam.h"
#include "../man/Tha60210012Device.h"
#include "Tha60210012ModuleMapHoReg.h"
#include "Tha60210012ModuleMapInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cVcgMapType  2

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tTha60210011ModuleMapMethods m_Tha60210011ModuleMapOverride;
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet BindHoLineVcToPseudowire(Tha60210011ModuleMap self, AtSdhVc vc, AtPw pw)
    {
    const uint8 cCepMapType = 0;
    AtSdhChannel sdhChannel = (AtSdhChannel)vc;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    ThaModuleAbstractMap abstractMap = (ThaModuleAbstractMap)self;
    eBool enable = mMethodsGet(abstractMap)->NeedEnableAfterBindToPw(abstractMap, pw);
    uint8 sts_i;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 sts1Id = (uint8)(startSts + sts_i);
        uint8 hwSlice = 0, hwSts = 0;
        uint32 regAddr, regVal, offset;

        if (ThaSdhChannelHwStsGet(sdhChannel, cThaModuleMap, sts1Id, &hwSlice, &hwSts) != cAtOk)
            mChannelLog(sdhChannel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

        offset  = Tha60210011ModuleMapDemapHoSliceOffset(abstractMap, hwSlice) + hwSts;
        regAddr = cAf6Reg_map_line_ctrl_Base + offset;
        regVal  = mChannelHwRead(vc, regAddr, cThaModuleMap);

        mRegFieldSet(regVal, cAf6_map_line_ctrl_MapChEn_, pw ? mBoolToBin(enable) : 0);
        mRegFieldSet(regVal, cAf6_map_line_ctrl_MapChType_, cCepMapType);

        mChannelHwWrite(vc, regAddr, regVal, cThaModuleMap);
        }

    return cAtOk;
    }

static eAtRet BindHoVcToChannel(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtChannel channel, uint8 mapType)
    {
    uint8 sts_i;
    eBool enable;
    AtSdhChannel sdhChannel = (AtSdhChannel)sdhVc;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    ThaModuleAbstractMap abstractMap = (ThaModuleAbstractMap)self;
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);
    uint8 srcType = (channelType == cAtSdhChannelTypeVc3) ? 0 : 1;

    /* When bind physical to encap channel some product,
     * need disable first and then enable in after step */
	if (mapType == cVcgMapType)
        enable = channel ? cAtTrue : cAtFalse;
	else
	    enable = mMethodsGet(self)->NeedEnableAfterBindEncapChannel(self, channel);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 sts1Id = (uint8)(startSts + sts_i);
        uint8 hwSlice = 0, hwSts = 0;
        uint32 regAddr, regVal, offset;

        if (ThaSdhChannelHwStsGet(sdhChannel, cThaModuleMap, sts1Id, &hwSlice, &hwSts) != cAtOk)
            mChannelLog(sdhChannel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

        offset  = Tha60210011ModuleMapDemapHoSliceOffset(abstractMap, hwSlice) + hwSts;
        regAddr = cAf6Reg_map_line_ctrl_Base + offset;
        regVal  = mChannelHwRead(sdhVc, regAddr, cThaModuleMap);
        mRegFieldSet(regVal, cAf6_map_line_ctrl_MapChEn_, mBoolToBin(enable));
        mRegFieldSet(regVal, cAf6_map_line_ctrl_MapChType_, mapType);

        if ((channelType == cAtSdhChannelTypeVc4_4c) || (numSts == 12))
            mRegFieldSet(regVal, cAf6_map_line_ctrl_Mapsrc_vc3n3c_, (sts_i < 4) ? 1 : 0);
        else if ((channelType == cAtSdhChannelTypeVc4_16c) || (numSts == 48))
            mRegFieldSet(regVal, cAf6_map_line_ctrl_Mapsrc_vc3n3c_, (sts_i < 16) ? 1 : 0);
        else
            mRegFieldSet(regVal, cAf6_map_line_ctrl_Mapsrc_vc3n3c_, (sts_i == 0) ? 1 : 0);

        mRegFieldSet(regVal, cAf6_map_line_ctrl_Mapsrc_type_, srcType);
        mChannelHwWrite(sdhVc, regAddr, regVal, cThaModuleMap);
        }

    return cAtOk;
    }

static eAtRet BindHoVcToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
    const uint8 cEncapMapType = 1;
    return BindHoVcToChannel(self, sdhVc, (AtChannel)encapChannel, cEncapMapType);
    }

static eAtRet BindAuVcToConcateGroup(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtConcateGroup concateGroup)
    {
    return BindHoVcToChannel(self, sdhVc, (AtChannel)concateGroup, cVcgMapType);
    }

static uint32 Tu3VcDefaultOffset(ThaModuleAbstractMap self, AtSdhVc tu3Vc)
    {
    uint8 hwSlice, hwSts;
    uint32 sliceOffset;

    /* Get MAP STS ID */
    ThaSdhChannelHwStsGet((AtSdhChannel)tu3Vc, cThaModuleMap, AtSdhChannelSts1Get((AtSdhChannel)tu3Vc), &hwSlice, &hwSts);
    sliceOffset = Tha60210011ModuleMapDemapLoSliceOffset(self, hwSlice);
    return ((hwSts * 672UL) + sliceOffset);
    }

static eAtRet BindTu3VcToEncap(ThaModuleAbstractMap self, AtSdhVc tu3Vc, AtEncapChannel encapChannel)
    {
    uint32 regAddr, regVal = 0;
    eBool enable = mMethodsGet(self)->NeedEnableAfterBindEncapChannel(self, (AtChannel)encapChannel);
    ThaModuleMap moduleMap = (ThaModuleMap)self;
    uint32 regBase = mMethodsGet((Tha60210011ModuleMap)self)->LomapChannelCtrlBase((Tha60210011ModuleMap)self);

    /* Need to disable hardware before reseting configuration */
    regAddr = regBase + Tu3VcDefaultOffset(self, tu3Vc);
    if (!enable)
        {
        regVal = mChannelHwRead(tu3Vc, regAddr, cThaModuleMap);
        mPolyRegFieldSet(regVal, moduleMap, MapTsEn, mBoolToBin(enable));
        mChannelHwWrite(tu3Vc, regAddr, regVal, cThaModuleMap);
        }

    /* Apply hardware configuration */
    regVal = mChannelHwRead(tu3Vc, regAddr, cThaModuleMap);
    mPolyRegFieldSet(regVal, moduleMap, MapFirstTs, 0);
    mPolyRegFieldSet(regVal, moduleMap, MapChnType, 4); /* HDLC/PPP/LAPS */
    mPolyRegFieldSet(regVal, moduleMap, MapTsEn, mBoolToBin(enable));
    mPolyRegFieldSet(regVal, moduleMap, MapPWIdField, AtChannelHwIdGet((AtChannel)encapChannel));
    mChannelHwWrite(tu3Vc, regAddr, regVal, cThaModuleMap);

    return cAtOk;
    }

static uint8 EncapHwChannelType(ThaModuleAbstractMap self, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(encapChannel);
    return 4;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsMapRegister(AtModuleDeviceGet(self), address);
    }

static ThaEncapDynamicBinder EncapBinder(Tha60210011ModuleMap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaEncapDynamicBinder)AtDeviceEncapBinder(device);
    }

static uint32 LoSliceHwPwIdGet(Tha60210011ModuleMap self, ThaPwAdapter pwAdapter)
    {
    AtChannel circuit = AtPwBoundCircuitGet((AtPw)pwAdapter);
    AtUnused(self);

    return AtChannelBoundPwHwIdGet(circuit);
    }

static uint32 LoSliceLocalPwIdRestore(Tha60210011ModuleMap self, ThaPwAdapter pwAdapter)
    {
    ThaHwPw hwPw = ThaPwAdapterHwPwGet(pwAdapter);
    uint8 loSlice = Tha60210011HwPwSliceGet(hwPw);
    uint32 localId = LoSliceHwPwIdGet(self, pwAdapter);

    Tha60210011HwPwTdmIdSet(hwPw, localId);
    ThaEncapDynamicBinderChannelUse(EncapBinder(self), loSlice, localId);

    return 0;
    }

static uint32 Tu3MapLineOffset(ThaModuleAbstractMap self, AtSdhChannel sdhChannel)
    {
    uint8 slice, hwSts;

    ThaSdhChannelHwStsGet(sdhChannel, cThaModuleMap, AtSdhChannelSts1Get(sdhChannel), &slice, &hwSts);
    return (uint32)(hwSts << 5) + ThaModuleAbstractMapSdhSliceOffset(self, sdhChannel, slice);
    }

static eAtRet Tu3VcSignalTypeSet(ThaModuleAbstractMap self, AtSdhVc tu3Vc)
    {
    uint32 regAddr = cAf6Reg_lomap_line_ctrl_Base + Tu3MapLineOffset(self, (AtSdhChannel)tu3Vc);
    uint32 regVal = mChannelHwRead(tu3Vc, regAddr, cThaModuleMap);

    mRegFieldSet(regVal, cAf6_lomap_line_ctrl_MapFrameType_, 0x3); /* TU3 */
    mRegFieldSet(regVal, cAf6_lomap_line_ctrl_MapSigType_, 0xc); /* CEP basic mode or
                                                                        VT with POH/STS with POH,
                                                                        Stuff/DS1,E1,DS3,E3 unframe mode */
    mChannelHwWrite(tu3Vc, regAddr, regVal, cThaModuleMap);

    return cAtOk;
    }

static eBool NeedEnableAfterBindEncapChannel(ThaModuleAbstractMap self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtFalse;
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
        "MAPHO Thalassa Map Line Control Slice 0",
        "MAPHO Thalassa Map Line Control Slice 1",
        "MAPHO Thalassa Demap Line Control Slice 0",
        "MAPHO Thalassa Demap Line Control Slice 1",

        "MAPLO Thalassa Map Channel Control slice 0",
        "MAPLO Thalassa Map Line Control slice 0",
        "MAPLO Thalassa Demap Channel Control slice 0",

        "MAPLO Thalassa Map Channel Control slice 1",
        "MAPLO Thalassa Map Line Control slice 1",
        "MAPLO Thalassa Demap Channel Control slice 1",

        "MAPLO Thalassa Map Channel Control slice 2",
        "MAPLO Thalassa Map Line Control slice 2",
        "MAPLO Thalassa Demap Channel Control slice 2",

        "MAPLO Thalassa Map Channel Control slice 3",
        "MAPLO Thalassa Map Line Control slice 3",
        "MAPLO Thalassa Demap Channel Control slice 3",
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideTha60210011ModuleMap(AtModule self)
    {
    Tha60210011ModuleMap module = (Tha60210011ModuleMap)self;
    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleMapOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleMapOverride));

        mMethodOverride(m_Tha60210011ModuleMapOverride, BindHoLineVcToPseudowire);
        mMethodOverride(m_Tha60210011ModuleMapOverride, LoSliceLocalPwIdRestore);
        }

    mMethodsSet(module, &m_Tha60210011ModuleMapOverride);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, BindHoVcToEncap);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindTu3VcToEncap);
        mMethodOverride(m_ThaModuleAbstractMapOverride, EncapHwChannelType);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindAuVcToConcateGroup);
        mMethodOverride(m_ThaModuleAbstractMapOverride, Tu3VcSignalTypeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, NeedEnableAfterBindEncapChannel);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleAbstractMap(self);
    OverrideTha60210011ModuleMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleMap);
    }

AtModule Tha60210012ModuleMapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210012ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModuleMapObjectInit(newModule, device);
    }

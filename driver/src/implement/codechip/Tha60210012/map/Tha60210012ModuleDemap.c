/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60210012ModuleDemap.c
 *
 * Created Date: Mar 1, 2016
 *
 * Description : 60210012 De-mapping
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/
#include "../../Tha60210011/map/Tha60210011ModuleMapLoReg.h"
#include "../man/Tha60210012Device.h"
#include "Tha60210012ModuleMapInternal.h"
#include "Tha60210012ModuleMapHoReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods               m_AtModuleOverride;
static tTha60210011ModuleDemapMethods m_Tha60210011ModuleDemapOverride;
static tThaModuleAbstractMapMethods   m_ThaModuleAbstractMapOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet BindHoLineVcToPseudowire(Tha60210011ModuleDemap self, AtSdhVc vc, AtPw pw)
    {
    const uint8 cCepMapType = 0;
    AtSdhChannel sdhChannel = (AtSdhChannel)vc;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    ThaModuleAbstractMap abstractMap = (ThaModuleAbstractMap)self;
    eBool enable = mMethodsGet(abstractMap)->NeedEnableAfterBindToPw(abstractMap, pw);
    uint8 sts_i;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 sts1Id = (uint8)(startSts + sts_i);
        uint8 hwSlice = 0, hwSts = 0;
        uint32 regAddr, regVal;

        if (ThaSdhChannelHwStsGet(sdhChannel, cThaModuleDemap, sts1Id, &hwSlice, &hwSts) != cAtOk)
            mChannelLog(sdhChannel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

        regAddr = cAf6Reg_demap_channel_ctrl_Base + Tha60210011ModuleMapDemapHoSliceOffset(abstractMap, hwSlice) + hwSts;
        regVal  = mChannelHwRead(vc, regAddr, cThaModuleDemap);
        mRegFieldSet(regVal, cAf6_demap_channel_ctrl_DemapChEn_, pw ? enable : 0);
        mRegFieldSet(regVal, cAf6_demap_channel_ctrl_DemapChType_, cCepMapType);
        mChannelHwWrite(vc, regAddr, regVal, cThaModuleDemap);
        }

    return cAtOk;
    }

static eAtRet BindHoVcToChannel(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtChannel channel, uint8 mapType)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)sdhVc;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    ThaModuleAbstractMap abstractMap = (ThaModuleAbstractMap)self;
    eBool enable;
    uint8 sts_i;
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);
    uint8 srcType = (channelType == cAtSdhChannelTypeVc3) ? 0 : 1;

    /* When bind physical to encap channel some product,
     * need disable first and then enable in after step */
    if (mapType == cBindVcgType)
        enable = channel ? cAtTrue : cAtFalse;
	else
	    enable = mMethodsGet(self)->NeedEnableAfterBindEncapChannel(self, channel);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 sts1Id = (uint8)(startSts + sts_i);
        uint8 hwSlice = 0, hwSts = 0;
        uint32 regAddr, regVal;

        if (ThaSdhChannelHwStsGet(sdhChannel, cThaModuleDemap, sts1Id, &hwSlice, &hwSts) != cAtOk)
            mChannelLog(sdhChannel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

        regAddr = cAf6Reg_demap_channel_ctrl_Base + Tha60210011ModuleMapDemapHoSliceOffset(abstractMap, hwSlice) + hwSts;
        regVal  = mChannelHwRead(sdhVc, regAddr, cThaModuleDemap);
        mRegFieldSet(regVal, cAf6_demap_channel_ctrl_DemapChEn_, enable);
        mRegFieldSet(regVal, cAf6_demap_channel_ctrl_DemapChType_, mapType);

        if ((channelType == cAtSdhChannelTypeVc4_4c) || (numSts == 12))
            mRegFieldSet(regVal, cAf6_demap_channel_ctrl_Demapsr_vc3n3c_, (sts_i < 4) ? 1 : 0);
        else if ((channelType == cAtSdhChannelTypeVc4_16c) || (numSts == 48))
            mRegFieldSet(regVal, cAf6_demap_channel_ctrl_Demapsr_vc3n3c_, (sts_i < 16) ? 1 : 0);
        else
            mRegFieldSet(regVal, cAf6_demap_channel_ctrl_Demapsr_vc3n3c_, (sts_i == 0) ? 1 : 0);

        mRegFieldSet(regVal, cAf6_demap_channel_ctrl_Demapsrctype_, srcType);

        mChannelHwWrite(sdhVc, regAddr, regVal, cThaModuleDemap);
        }

    return cAtOk;
    }

static eAtRet BindHoVcToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
    const uint8 cEncapMapType = 1;	
    return BindHoVcToChannel(self, sdhVc, (AtChannel)encapChannel, cEncapMapType);
    }

static eAtRet BindAuVcToConcateGroup(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtConcateGroup concateGroup)
    {
    return BindHoVcToChannel(self, sdhVc, (AtChannel)concateGroup, cBindVcgType);
    }

static uint32 Tu3VcDefaultOffset(ThaModuleAbstractMap self, AtSdhVc tu3Vc)
    {
    uint8 hwSlice, hwSts;
    uint32 sliceOffset;

    /* Get MAP STS ID */
    ThaSdhChannelHwStsGet((AtSdhChannel)tu3Vc, cThaModuleDemap, AtSdhChannelSts1Get((AtSdhChannel)tu3Vc), &hwSlice, &hwSts);
    sliceOffset = Tha60210011ModuleMapDemapLoSliceOffset(self, hwSlice);
    return ((hwSts * 672UL) + sliceOffset);
    }

static eAtRet BindTu3VcToEncap(ThaModuleAbstractMap self, AtSdhVc tu3Vc, AtEncapChannel encapChannel)
    {
    uint32 regAddr, regVal = 0;
    eBool enable = mMethodsGet(self)->NeedEnableAfterBindEncapChannel(self, (AtChannel)encapChannel);
    ThaModuleDemap moduleDemap = (ThaModuleDemap)self;

    /* Need to disable hardware before reseting configuration */
    regAddr = cAf6Reg_lodemap_channel_ctrl_Base + Tu3VcDefaultOffset(self, tu3Vc);
    if (!enable)
        {
        regVal = mChannelHwRead(tu3Vc, regAddr, cThaModuleDemap);
        mPolyRegFieldSet(regVal, moduleDemap, DmapTsEn, mBoolToBin(enable));
        mChannelHwWrite(tu3Vc, regAddr, regVal, cThaModuleDemap);
        }

    /* Apply hardware configuration */
    regVal = mChannelHwRead(tu3Vc, regAddr, cThaModuleDemap);
    mPolyRegFieldSet(regVal, moduleDemap, DmapFirstTs, 0);
    mPolyRegFieldSet(regVal, moduleDemap, DmapChnType, 4); /* HDLC/PPP/LAPS */
    mPolyRegFieldSet(regVal, moduleDemap, DmapTsEn, mBoolToBin(enable));
    mPolyRegFieldSet(regVal, moduleDemap, DmapPwId, AtChannelHwIdGet((AtChannel)encapChannel));

    mChannelHwWrite(tu3Vc, regAddr, regVal, cThaModuleDemap);

    return cAtOk;
    }

static uint8 EncapHwChannelType(ThaModuleAbstractMap self, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(encapChannel);
    return 4;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsDemapRegister(AtModuleDeviceGet(self), address);
    }

static eBool NeedEnableAfterBindEncapChannel(ThaModuleAbstractMap self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtFalse;
    }

static void OverrideTha60210011ModuleDemap(AtModule self)
    {
    Tha60210011ModuleDemap module = (Tha60210011ModuleDemap)self;
    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleDemapOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleDemapOverride));

        mMethodOverride(m_Tha60210011ModuleDemapOverride, BindHoLineVcToPseudowire);
        }

    mMethodsSet(module, &m_Tha60210011ModuleDemapOverride);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, BindHoVcToEncap);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindTu3VcToEncap);
        mMethodOverride(m_ThaModuleAbstractMapOverride, EncapHwChannelType);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindAuVcToConcateGroup);
        mMethodOverride(m_ThaModuleAbstractMapOverride, NeedEnableAfterBindEncapChannel);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleDemap);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleAbstractMap(self);
    OverrideTha60210011ModuleDemap(self);
    }

AtModule Tha60210012ModuleDemapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210012ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModuleDemapObjectInit(newModule, device);
    }

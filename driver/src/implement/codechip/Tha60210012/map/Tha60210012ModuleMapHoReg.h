/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0012_RD_MAP_HO_H_
#define _AF6_REG_AF6CCI0012_RD_MAP_HO_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Demap Channel Control
Reg Addr   : 0x0000-0x007FF
Reg Formula: 0x0000 + 256*slice + stsid
    Where  : 
           + $slice(0-0): is OC48 slice
           + $stsid(0-47): is STS in OC48
Reg Desc   : 
The registers are used by the hardware to configure PW channel

------------------------------------------------------------------------------*/
#define cAf6Reg_demap_channel_ctrl_Base                                                                 0x0000
#define cAf6Reg_demap_channel_ctrl(slice, stsid)                              (0x0000UL+256UL*(slice)+(stsid))
#define cAf6Reg_demap_channel_ctrl_WidthVal                                                                 32
#define cAf6Reg_demap_channel_ctrl_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: DemapChType
BitField Type: RW
BitField Desc: 0:CEP,  1:HDLC/PPP/ML, 2:VCAT/LCAS
BitField Bits: [4:3]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapChType_Bit_Start                                                        3
#define cAf6_demap_channel_ctrl_DemapChType_Bit_End                                                          4
#define cAf6_demap_channel_ctrl_DemapChType_Mask                                                       cBit4_3
#define cAf6_demap_channel_ctrl_DemapChType_Shift                                                            3
#define cAf6_demap_channel_ctrl_DemapChType_MaxVal                                                         0x3
#define cAf6_demap_channel_ctrl_DemapChType_MinVal                                                         0x0
#define cAf6_demap_channel_ctrl_DemapChType_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Demapsr_vc3n3c
BitField Type: RW
BitField Desc: 0:slave of VC3_N3c  1:master of VC3-N3c
BitField Bits: [2]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_Bit_Start                                                     2
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_Bit_End                                                       2
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_Mask                                                      cBit2
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_Shift                                                         2
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_MaxVal                                                      0x1
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_MinVal                                                      0x0
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_RstVal                                                      0x0

/*--------------------------------------
BitField Name: Demapsrctype
BitField Type: RW
BitField Desc: 0:VC3 1:VC3-3c
BitField Bits: [1]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_Demapsrctype_Bit_Start                                                       1
#define cAf6_demap_channel_ctrl_Demapsrctype_Bit_End                                                         1
#define cAf6_demap_channel_ctrl_Demapsrctype_Mask                                                        cBit1
#define cAf6_demap_channel_ctrl_Demapsrctype_Shift                                                           1
#define cAf6_demap_channel_ctrl_Demapsrctype_MaxVal                                                        0x1
#define cAf6_demap_channel_ctrl_Demapsrctype_MinVal                                                        0x0
#define cAf6_demap_channel_ctrl_Demapsrctype_RstVal                                                        0x0

/*--------------------------------------
BitField Name: DemapChEn
BitField Type: RW
BitField Desc: PW/Channels Enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapChEn_Bit_Start                                                          0
#define cAf6_demap_channel_ctrl_DemapChEn_Bit_End                                                            0
#define cAf6_demap_channel_ctrl_DemapChEn_Mask                                                           cBit0
#define cAf6_demap_channel_ctrl_DemapChEn_Shift                                                              0
#define cAf6_demap_channel_ctrl_DemapChEn_MaxVal                                                           0x1
#define cAf6_demap_channel_ctrl_DemapChEn_MinVal                                                           0x0
#define cAf6_demap_channel_ctrl_DemapChEn_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Map Line Control
Reg Addr   : 0x4000-0x47FF
Reg Formula: 0x4000 + 256*slice + stsid
    Where  : 
           + $slice(0-0): is OC48 slice
           + $stsid(0-47): is STS in OC48
Reg Desc   : 
The registers provide the per line configurations for STS

------------------------------------------------------------------------------*/
#define cAf6Reg_map_line_ctrl_Base                                                                      0x4000
#define cAf6Reg_map_line_ctrl(slice, stsid)                                   (0x4000UL+256UL*(slice)+(stsid))
#define cAf6Reg_map_line_ctrl_WidthVal                                                                      32
#define cAf6Reg_map_line_ctrl_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: MapChType
BitField Type: RW
BitField Desc: 0:CEP,  1:HDLC/PPP/ML, 2:VCAT/LCAS
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapChType_Bit_Start                                                              10
#define cAf6_map_line_ctrl_MapChType_Bit_End                                                                11
#define cAf6_map_line_ctrl_MapChType_Mask                                                            cBit11_10
#define cAf6_map_line_ctrl_MapChType_Shift                                                                  10
#define cAf6_map_line_ctrl_MapChType_MaxVal                                                                0x3
#define cAf6_map_line_ctrl_MapChType_MinVal                                                                0x0
#define cAf6_map_line_ctrl_MapChType_RstVal                                                                0x0

/*--------------------------------------
BitField Name: Mapsrc_vc3n3c
BitField Type: RW
BitField Desc: 0:slave of VC3_N3c  1:master of VC3-N3c
BitField Bits: [9]
--------------------------------------*/
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_Bit_Start                                                           9
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_Bit_End                                                             9
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_Mask                                                            cBit9
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_Shift                                                               9
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_MaxVal                                                            0x1
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_MinVal                                                            0x0
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_RstVal                                                            0x0

/*--------------------------------------
BitField Name: Mapsrc_type
BitField Type: RW
BitField Desc: 0:VC3 1:VC3-3c
BitField Bits: [8]
--------------------------------------*/
#define cAf6_map_line_ctrl_Mapsrc_type_Bit_Start                                                             8
#define cAf6_map_line_ctrl_Mapsrc_type_Bit_End                                                               8
#define cAf6_map_line_ctrl_Mapsrc_type_Mask                                                              cBit8
#define cAf6_map_line_ctrl_Mapsrc_type_Shift                                                                 8
#define cAf6_map_line_ctrl_Mapsrc_type_MaxVal                                                              0x1
#define cAf6_map_line_ctrl_Mapsrc_type_MinVal                                                              0x0
#define cAf6_map_line_ctrl_Mapsrc_type_RstVal                                                              0x0

/*--------------------------------------
BitField Name: MapChEn
BitField Type: RW
BitField Desc: PW/Channels Enable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapChEn_Bit_Start                                                                 7
#define cAf6_map_line_ctrl_MapChEn_Bit_End                                                                   7
#define cAf6_map_line_ctrl_MapChEn_Mask                                                                  cBit7
#define cAf6_map_line_ctrl_MapChEn_Shift                                                                     7
#define cAf6_map_line_ctrl_MapChEn_MaxVal                                                                  0x1
#define cAf6_map_line_ctrl_MapChEn_MinVal                                                                  0x0
#define cAf6_map_line_ctrl_MapChEn_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: MapTimeSrcMaster
BitField Type: RW
BitField Desc: This bit is used to indicate the master timing or the master VC3
in VC4/VC4-Xc
BitField Bits: [6]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapTimeSrcMaster_Bit_Start                                                        6
#define cAf6_map_line_ctrl_MapTimeSrcMaster_Bit_End                                                          6
#define cAf6_map_line_ctrl_MapTimeSrcMaster_Mask                                                         cBit6
#define cAf6_map_line_ctrl_MapTimeSrcMaster_Shift                                                            6
#define cAf6_map_line_ctrl_MapTimeSrcMaster_MaxVal                                                         0x1
#define cAf6_map_line_ctrl_MapTimeSrcMaster_MinVal                                                         0x0
#define cAf6_map_line_ctrl_MapTimeSrcMaster_RstVal                                                         0x0

/*--------------------------------------
BitField Name: MapTimeSrcId
BitField Type: RW
BitField Desc: The reference line ID used for timing reference or the master VC3
ID in VC4/VC4-Xc
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapTimeSrcId_Bit_Start                                                            0
#define cAf6_map_line_ctrl_MapTimeSrcId_Bit_End                                                              5
#define cAf6_map_line_ctrl_MapTimeSrcId_Mask                                                           cBit5_0
#define cAf6_map_line_ctrl_MapTimeSrcId_Shift                                                                0
#define cAf6_map_line_ctrl_MapTimeSrcId_MaxVal                                                            0x3f
#define cAf6_map_line_ctrl_MapTimeSrcId_MinVal                                                             0x0
#define cAf6_map_line_ctrl_MapTimeSrcId_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert Gen
Reg Addr   : 0x8_200 - 0xB_A00
Reg Formula: 0x8_200 + 2048*engid
    Where  : 
           + $engid(0-0): id of HO_BERT
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_gen_Base                                                                    0x8200
#define cAf6Reg_sel_ho_bert_gen(engid)                                               (0x8200UL+2048UL*(engid))
#define cAf6Reg_sel_ho_bert_gen_WidthVal                                                                    32
#define cAf6Reg_sel_ho_bert_gen_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: gen_en
BitField Type: RW
BitField Desc: set "1" to enable bert gen
BitField Bits: [9]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen_gen_en_Bit_Start                                                                9
#define cAf6_sel_ho_bert_gen_gen_en_Bit_End                                                                  9
#define cAf6_sel_ho_bert_gen_gen_en_Mask                                                                 cBit9
#define cAf6_sel_ho_bert_gen_gen_en_Shift                                                                    9
#define cAf6_sel_ho_bert_gen_gen_en_MaxVal                                                                 0x1
#define cAf6_sel_ho_bert_gen_gen_en_MinVal                                                                 0x0
#define cAf6_sel_ho_bert_gen_gen_en_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: line_id
BitField Type: RW
BitField Desc: line id OC48
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen_line_id_Bit_Start                                                               6
#define cAf6_sel_ho_bert_gen_line_id_Bit_End                                                                 8
#define cAf6_sel_ho_bert_gen_line_id_Mask                                                              cBit8_6
#define cAf6_sel_ho_bert_gen_line_id_Shift                                                                   6
#define cAf6_sel_ho_bert_gen_line_id_MaxVal                                                                0x7
#define cAf6_sel_ho_bert_gen_line_id_MinVal                                                                0x0
#define cAf6_sel_ho_bert_gen_line_id_RstVal                                                                0x0

/*--------------------------------------
BitField Name: stsid
BitField Type: RW
BitField Desc: STS ID
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen_stsid_Bit_Start                                                                 0
#define cAf6_sel_ho_bert_gen_stsid_Bit_End                                                                   5
#define cAf6_sel_ho_bert_gen_stsid_Mask                                                                cBit5_0
#define cAf6_sel_ho_bert_gen_stsid_Shift                                                                     0
#define cAf6_sel_ho_bert_gen_stsid_MaxVal                                                                 0x3f
#define cAf6_sel_ho_bert_gen_stsid_MinVal                                                                  0x0
#define cAf6_sel_ho_bert_gen_stsid_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Sel Mode Bert Gen
Reg Addr   : 0x8_300 - 0xB_B00
Reg Formula: 0x8_300 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers select mode bert gen

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_pen_gen_Base                                                                       0x8300
#define cAf6Reg_ctrl_pen_gen(slice)                                                  (0x8300UL+2048UL*(slice))
#define cAf6Reg_ctrl_pen_gen_WidthVal                                                                       32
#define cAf6Reg_ctrl_pen_gen_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: swapmode
BitField Type: RW
BitField Desc: swap data
BitField Bits: [09]
--------------------------------------*/
#define cAf6_ctrl_pen_gen_swapmode_Bit_Start                                                                 9
#define cAf6_ctrl_pen_gen_swapmode_Bit_End                                                                   9
#define cAf6_ctrl_pen_gen_swapmode_Mask                                                                  cBit9
#define cAf6_ctrl_pen_gen_swapmode_Shift                                                                     9
#define cAf6_ctrl_pen_gen_swapmode_MaxVal                                                                  0x1
#define cAf6_ctrl_pen_gen_swapmode_MinVal                                                                  0x0
#define cAf6_ctrl_pen_gen_swapmode_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: invmode
BitField Type: RW
BitField Desc: invert data
BitField Bits: [08]
--------------------------------------*/
#define cAf6_ctrl_pen_gen_invmode_Bit_Start                                                                  8
#define cAf6_ctrl_pen_gen_invmode_Bit_End                                                                    8
#define cAf6_ctrl_pen_gen_invmode_Mask                                                                   cBit8
#define cAf6_ctrl_pen_gen_invmode_Shift                                                                      8
#define cAf6_ctrl_pen_gen_invmode_MaxVal                                                                   0x1
#define cAf6_ctrl_pen_gen_invmode_MinVal                                                                   0x0
#define cAf6_ctrl_pen_gen_invmode_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: patt_mode
BitField Type: RW
BitField Desc: sel pattern gen # 0x01 : all0 # 0x02 : prbs15 # 0x04 : prbs20r #
0x08 : prbs20 # 0x10 : prbs23 # 0x20 : prbs31 # other: all1
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ctrl_pen_gen_patt_mode_Bit_Start                                                                0
#define cAf6_ctrl_pen_gen_patt_mode_Bit_End                                                                  7
#define cAf6_ctrl_pen_gen_patt_mode_Mask                                                               cBit7_0
#define cAf6_ctrl_pen_gen_patt_mode_Shift                                                                    0
#define cAf6_ctrl_pen_gen_patt_mode_MaxVal                                                                0xff
#define cAf6_ctrl_pen_gen_patt_mode_MinVal                                                                 0x0
#define cAf6_ctrl_pen_gen_patt_mode_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Inser Error
Reg Addr   : 0x8_320 - 0xB_B20
Reg Formula: 0x8_320 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers select rate inser error

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_ber_pen_Base                                                                       0x8320
#define cAf6Reg_ctrl_ber_pen(slice)                                                  (0x8320UL+2048UL*(slice))
#define cAf6Reg_ctrl_ber_pen_WidthVal                                                                       32
#define cAf6Reg_ctrl_ber_pen_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: ber_rate
BitField Type: RW
BitField Desc: TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to
Pattern Generator [31:0] == 32'd0          :disable  BER_level_val BER_level
1000:         BER 10e-3 10_000:       BER 10e-4 100_000:      BER 10e-5
1_000_000:    BER 10e-6 10_000_000:   BER 10e-7 100_000_000:  BER 10e-8
1_000_000_000 : BER 10e-9
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ctrl_ber_pen_ber_rate_Bit_Start                                                                 0
#define cAf6_ctrl_ber_pen_ber_rate_Bit_End                                                                  31
#define cAf6_ctrl_ber_pen_ber_rate_Mask                                                               cBit31_0
#define cAf6_ctrl_ber_pen_ber_rate_Shift                                                                     0
#define cAf6_ctrl_ber_pen_ber_rate_MaxVal                                                           0xffffffff
#define cAf6_ctrl_ber_pen_ber_rate_MinVal                                                                  0x0
#define cAf6_ctrl_ber_pen_ber_rate_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter num of bit gen
Reg Addr   : 0x8_380 - 0xB_B80
Reg Formula: 0x8_380 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers counter bit genertaie in tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_ber_pen_Base                                                                    0x8380
#define cAf6Reg_goodbit_ber_pen(slice)                                               (0x8380UL+2048UL*(slice))
#define cAf6Reg_goodbit_ber_pen_WidthVal                                                                    32
#define cAf6Reg_goodbit_ber_pen_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: goodbit
BitField Type: R2C
BitField Desc: counter goodbit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_ber_pen_goodbit_Bit_Start                                                               0
#define cAf6_goodbit_ber_pen_goodbit_Bit_End                                                                31
#define cAf6_goodbit_ber_pen_goodbit_Mask                                                             cBit31_0
#define cAf6_goodbit_ber_pen_goodbit_Shift                                                                   0
#define cAf6_goodbit_ber_pen_goodbit_MaxVal                                                         0xffffffff
#define cAf6_goodbit_ber_pen_goodbit_MinVal                                                                0x0
#define cAf6_goodbit_ber_pen_goodbit_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert TDM Mon
Reg Addr   : 0x8_400 - 0xB_C00
Reg Formula: 0x8_400 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_tdm_mon_Base                                                                0x8400
#define cAf6Reg_sel_ho_bert_tdm_mon(slice)                                           (0x8400UL+2048UL*(slice))
#define cAf6Reg_sel_ho_bert_tdm_mon_WidthVal                                                                32
#define cAf6Reg_sel_ho_bert_tdm_mon_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: montdmen
BitField Type: RW
BitField Desc: set "1" to enable bert tdm mon
BitField Bits: [10]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon_montdmen_Bit_Start                                                         10
#define cAf6_sel_ho_bert_tdm_mon_montdmen_Bit_End                                                           10
#define cAf6_sel_ho_bert_tdm_mon_montdmen_Mask                                                          cBit10
#define cAf6_sel_ho_bert_tdm_mon_montdmen_Shift                                                             10
#define cAf6_sel_ho_bert_tdm_mon_montdmen_MaxVal                                                           0x1
#define cAf6_sel_ho_bert_tdm_mon_montdmen_MinVal                                                           0x0
#define cAf6_sel_ho_bert_tdm_mon_montdmen_RstVal                                                           0x0

/*--------------------------------------
BitField Name: masterID
BitField Type: RW
BitField Desc: chanel ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon_masterID_Bit_Start                                                          0
#define cAf6_sel_ho_bert_tdm_mon_masterID_Bit_End                                                            9
#define cAf6_sel_ho_bert_tdm_mon_masterID_Mask                                                         cBit9_0
#define cAf6_sel_ho_bert_tdm_mon_masterID_Shift                                                              0
#define cAf6_sel_ho_bert_tdm_mon_masterID_MaxVal                                                         0x3ff
#define cAf6_sel_ho_bert_tdm_mon_masterID_MinVal                                                           0x0
#define cAf6_sel_ho_bert_tdm_mon_masterID_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert PW Mon
Reg Addr   : 0x8_600 - 0xB_E00
Reg Formula: 0x8_600 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_pw_mon_Base                                                                 0x8600
#define cAf6Reg_sel_ho_bert_pw_mon(slice)                                            (0x8600UL+2048UL*(slice))
#define cAf6Reg_sel_ho_bert_pw_mon_WidthVal                                                                 32
#define cAf6Reg_sel_ho_bert_pw_mon_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: monpwen
BitField Type: RW
BitField Desc: set "1" to enable bert pw mon
BitField Bits: [10]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_monpwen_Bit_Start                                                           10
#define cAf6_sel_ho_bert_pw_mon_monpwen_Bit_End                                                             10
#define cAf6_sel_ho_bert_pw_mon_monpwen_Mask                                                            cBit10
#define cAf6_sel_ho_bert_pw_mon_monpwen_Shift                                                               10
#define cAf6_sel_ho_bert_pw_mon_monpwen_MaxVal                                                             0x1
#define cAf6_sel_ho_bert_pw_mon_monpwen_MinVal                                                             0x0
#define cAf6_sel_ho_bert_pw_mon_monpwen_RstVal                                                             0x0

/*--------------------------------------
BitField Name: masterID
BitField Type: RW
BitField Desc: chanel ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_masterID_Bit_Start                                                           0
#define cAf6_sel_ho_bert_pw_mon_masterID_Bit_End                                                             9
#define cAf6_sel_ho_bert_pw_mon_masterID_Mask                                                          cBit9_0
#define cAf6_sel_ho_bert_pw_mon_masterID_Shift                                                               0
#define cAf6_sel_ho_bert_pw_mon_masterID_MaxVal                                                          0x3ff
#define cAf6_sel_ho_bert_pw_mon_masterID_MinVal                                                            0x0
#define cAf6_sel_ho_bert_pw_mon_masterID_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Sel Mode Bert TDM mon
Reg Addr   : 0x8_510 - 0xB_D10
Reg Formula: 0x8_510 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers select mode bert mon in tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_pen_tdm_mon_Base                                                                   0x8510
#define cAf6Reg_ctrl_pen_tdm_mon(slice)                                              (0x8510UL+2048UL*(slice))
#define cAf6Reg_ctrl_pen_tdm_mon_WidthVal                                                                   32
#define cAf6Reg_ctrl_pen_tdm_mon_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: thrhold pattern sync
BitField Type: RW
BitField Desc: minimum number of byte sync to go sync state
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_Bit_Start                                                16
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_Bit_End                                                  19
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_Mask                                              cBit19_16
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_Shift                                                    16
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_MaxVal                                                  0xf
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_MinVal                                                  0x0
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_RstVal                                                  0x0

/*--------------------------------------
BitField Name: thrhold error
BitField Type: RW
BitField Desc: maximum err in state sync, if more than thrhold go loss sync
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_Bit_Start                                                       12
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_Bit_End                                                         15
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_Mask                                                     cBit15_12
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_Shift                                                           12
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_MaxVal                                                         0xf
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_MinVal                                                         0x0
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_RstVal                                                         0x0

/*--------------------------------------
BitField Name: reservee
BitField Type: RW
BitField Desc: reserve
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_reservee_Bit_Start                                                            10
#define cAf6_ctrl_pen_tdm_mon_reservee_Bit_End                                                              11
#define cAf6_ctrl_pen_tdm_mon_reservee_Mask                                                          cBit11_10
#define cAf6_ctrl_pen_tdm_mon_reservee_Shift                                                                10
#define cAf6_ctrl_pen_tdm_mon_reservee_MaxVal                                                              0x3
#define cAf6_ctrl_pen_tdm_mon_reservee_MinVal                                                              0x0
#define cAf6_ctrl_pen_tdm_mon_reservee_RstVal                                                              0x0

/*--------------------------------------
BitField Name: swapmode
BitField Type: RW
BitField Desc: swap data
BitField Bits: [09]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_swapmode_Bit_Start                                                             9
#define cAf6_ctrl_pen_tdm_mon_swapmode_Bit_End                                                               9
#define cAf6_ctrl_pen_tdm_mon_swapmode_Mask                                                              cBit9
#define cAf6_ctrl_pen_tdm_mon_swapmode_Shift                                                                 9
#define cAf6_ctrl_pen_tdm_mon_swapmode_MaxVal                                                              0x1
#define cAf6_ctrl_pen_tdm_mon_swapmode_MinVal                                                              0x0
#define cAf6_ctrl_pen_tdm_mon_swapmode_RstVal                                                              0x0

/*--------------------------------------
BitField Name: invmode
BitField Type: RW
BitField Desc: invert data
BitField Bits: [08]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_invmode_Bit_Start                                                              8
#define cAf6_ctrl_pen_tdm_mon_invmode_Bit_End                                                                8
#define cAf6_ctrl_pen_tdm_mon_invmode_Mask                                                               cBit8
#define cAf6_ctrl_pen_tdm_mon_invmode_Shift                                                                  8
#define cAf6_ctrl_pen_tdm_mon_invmode_MaxVal                                                               0x1
#define cAf6_ctrl_pen_tdm_mon_invmode_MinVal                                                               0x0
#define cAf6_ctrl_pen_tdm_mon_invmode_RstVal                                                               0x0

/*--------------------------------------
BitField Name: patt_mode
BitField Type: RW
BitField Desc: sel pattern gen # 0x01 : all1 # 0x02 : all0 # 0x04 : prbs15 #
0x08 : prbs20r # 0x10 : prbs20 # 0x20 : prss23 # 0x40 : prbs31
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_patt_mode_Bit_Start                                                            0
#define cAf6_ctrl_pen_tdm_mon_patt_mode_Bit_End                                                              7
#define cAf6_ctrl_pen_tdm_mon_patt_mode_Mask                                                           cBit7_0
#define cAf6_ctrl_pen_tdm_mon_patt_mode_Shift                                                                0
#define cAf6_ctrl_pen_tdm_mon_patt_mode_MaxVal                                                            0xff
#define cAf6_ctrl_pen_tdm_mon_patt_mode_MinVal                                                             0x0
#define cAf6_ctrl_pen_tdm_mon_patt_mode_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Sel Mode Bert PW mon
Reg Addr   : 0x8_710 - 0xB_F10
Reg Formula: 0x8_710 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers select mode bert mon in pw side

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_pen_pw_mon_Base                                                                    0x8710
#define cAf6Reg_ctrl_pen_pw_mon(slice)                                               (0x8710UL+2048UL*(slice))
#define cAf6Reg_ctrl_pen_pw_mon_WidthVal                                                                    32
#define cAf6Reg_ctrl_pen_pw_mon_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: thrhold pattern sync
BitField Type: RW
BitField Desc: minimum number of byte sync to go sync state
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_Bit_Start                                                 16
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_Bit_End                                                   19
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_Mask                                               cBit19_16
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_Shift                                                     16
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_MaxVal                                                   0xf
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_MinVal                                                   0x0
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_RstVal                                                   0x0

/*--------------------------------------
BitField Name: thrhold error
BitField Type: RW
BitField Desc: maximum err in state sync, if more than thrhold go loss sync
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_thrhold_error_Bit_Start                                                        12
#define cAf6_ctrl_pen_pw_mon_thrhold_error_Bit_End                                                          15
#define cAf6_ctrl_pen_pw_mon_thrhold_error_Mask                                                      cBit15_12
#define cAf6_ctrl_pen_pw_mon_thrhold_error_Shift                                                            12
#define cAf6_ctrl_pen_pw_mon_thrhold_error_MaxVal                                                          0xf
#define cAf6_ctrl_pen_pw_mon_thrhold_error_MinVal                                                          0x0
#define cAf6_ctrl_pen_pw_mon_thrhold_error_RstVal                                                          0x0

/*--------------------------------------
BitField Name: pw_reserve
BitField Type: RW
BitField Desc: pw reserve
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_pw_reserve_Bit_Start                                                           10
#define cAf6_ctrl_pen_pw_mon_pw_reserve_Bit_End                                                             11
#define cAf6_ctrl_pen_pw_mon_pw_reserve_Mask                                                         cBit11_10
#define cAf6_ctrl_pen_pw_mon_pw_reserve_Shift                                                               10
#define cAf6_ctrl_pen_pw_mon_pw_reserve_MaxVal                                                             0x3
#define cAf6_ctrl_pen_pw_mon_pw_reserve_MinVal                                                             0x0
#define cAf6_ctrl_pen_pw_mon_pw_reserve_RstVal                                                             0x0

/*--------------------------------------
BitField Name: swapmode
BitField Type: RW
BitField Desc: swap data
BitField Bits: [09]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_swapmode_Bit_Start                                                              9
#define cAf6_ctrl_pen_pw_mon_swapmode_Bit_End                                                                9
#define cAf6_ctrl_pen_pw_mon_swapmode_Mask                                                               cBit9
#define cAf6_ctrl_pen_pw_mon_swapmode_Shift                                                                  9
#define cAf6_ctrl_pen_pw_mon_swapmode_MaxVal                                                               0x1
#define cAf6_ctrl_pen_pw_mon_swapmode_MinVal                                                               0x0
#define cAf6_ctrl_pen_pw_mon_swapmode_RstVal                                                               0x0

/*--------------------------------------
BitField Name: invmode
BitField Type: RW
BitField Desc: invert data
BitField Bits: [08]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_invmode_Bit_Start                                                               8
#define cAf6_ctrl_pen_pw_mon_invmode_Bit_End                                                                 8
#define cAf6_ctrl_pen_pw_mon_invmode_Mask                                                                cBit8
#define cAf6_ctrl_pen_pw_mon_invmode_Shift                                                                   8
#define cAf6_ctrl_pen_pw_mon_invmode_MaxVal                                                                0x1
#define cAf6_ctrl_pen_pw_mon_invmode_MinVal                                                                0x0
#define cAf6_ctrl_pen_pw_mon_invmode_RstVal                                                                0x0

/*--------------------------------------
BitField Name: patt_mode
BitField Type: RW
BitField Desc: sel pattern gen # 0x01 : all1 # 0x02 : all0 # 0x04 : prbs15 #
0x08 : prbs20r # 0x10 : prbs20 # 0x20 : prss23 # 0x40 : prbs31
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_patt_mode_Bit_Start                                                             0
#define cAf6_ctrl_pen_pw_mon_patt_mode_Bit_End                                                               7
#define cAf6_ctrl_pen_pw_mon_patt_mode_Mask                                                            cBit7_0
#define cAf6_ctrl_pen_pw_mon_patt_mode_Shift                                                                 0
#define cAf6_ctrl_pen_pw_mon_patt_mode_MaxVal                                                             0xff
#define cAf6_ctrl_pen_pw_mon_patt_mode_MinVal                                                              0x0
#define cAf6_ctrl_pen_pw_mon_patt_mode_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : TDM loss sync
Reg Addr   : 0x8_502 - 0xB_D02
Reg Formula: 0x8_502 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers indicate bert mon loss sync in tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_loss_tdm_mon_Base                                                                       0x8502
#define cAf6Reg_loss_tdm_mon(slice)                                                  (0x8502UL+2048UL*(slice))
#define cAf6Reg_loss_tdm_mon_WidthVal                                                                       32
#define cAf6Reg_loss_tdm_mon_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: sticky_err
BitField Type: W2C
BitField Desc: "1" indicate loss sync
BitField Bits: [0]
--------------------------------------*/
#define cAf6_loss_tdm_mon_sticky_err_Bit_Start                                                               0
#define cAf6_loss_tdm_mon_sticky_err_Bit_End                                                                 0
#define cAf6_loss_tdm_mon_sticky_err_Mask                                                                cBit0
#define cAf6_loss_tdm_mon_sticky_err_Shift                                                                   0
#define cAf6_loss_tdm_mon_sticky_err_MaxVal                                                                0x1
#define cAf6_loss_tdm_mon_sticky_err_MinVal                                                                0x0
#define cAf6_loss_tdm_mon_sticky_err_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : PW loss sync
Reg Addr   : 0x8_702 - 0xB_F02
Reg Formula: 0x8_702 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers indicate bert mon loss sync in pw side

------------------------------------------------------------------------------*/
#define cAf6Reg_loss_pw_mon_Base                                                                        0x8702
#define cAf6Reg_loss_pw_mon(slice)                                                   (0x8702UL+2048UL*(slice))
#define cAf6Reg_loss_pw_mon_WidthVal                                                                        32
#define cAf6Reg_loss_pw_mon_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: sticky_err
BitField Type: W2C
BitField Desc: "1" indicate loss sync
BitField Bits: [0]
--------------------------------------*/
#define cAf6_loss_pw_mon_sticky_err_Bit_Start                                                                0
#define cAf6_loss_pw_mon_sticky_err_Bit_End                                                                  0
#define cAf6_loss_pw_mon_sticky_err_Mask                                                                 cBit0
#define cAf6_loss_pw_mon_sticky_err_Shift                                                                    0
#define cAf6_loss_pw_mon_sticky_err_MaxVal                                                                 0x1
#define cAf6_loss_pw_mon_sticky_err_MinVal                                                                 0x0
#define cAf6_loss_pw_mon_sticky_err_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : counter good bit TDM mon
Reg Addr   : 0x8_580 - 0xB_D80
Reg Formula: 0x8_580 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_tdm_mon_Base                                                                0x8580
#define cAf6Reg_goodbit_pen_tdm_mon(slice)                                           (0x8580UL+2048UL*(slice))
#define cAf6Reg_goodbit_pen_tdm_mon_WidthVal                                                                32
#define cAf6Reg_goodbit_pen_tdm_mon_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cnt_goodbit
BitField Type: R2C
BitField Desc: counter goodbit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_Bit_Start                                                       0
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_Bit_End                                                        31
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_Mask                                                     cBit31_0
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_Shift                                                           0
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_MaxVal                                                 0xffffffff
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_MinVal                                                        0x0
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : counter error bit in TDM mon
Reg Addr   : 0x8_560 - 0xB_D60
Reg Formula: 0x8_560 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_err_pen_tdm_mon_Base                                                                    0x8560
#define cAf6Reg_err_pen_tdm_mon(slice)                                               (0x8560UL+2048UL*(slice))
#define cAf6Reg_err_pen_tdm_mon_WidthVal                                                                    32
#define cAf6Reg_err_pen_tdm_mon_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: cnt_errbit
BitField Type: R2C
BitField Desc: counter err bit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_err_pen_tdm_mon_cnt_errbit_Bit_Start                                                            0
#define cAf6_err_pen_tdm_mon_cnt_errbit_Bit_End                                                             31
#define cAf6_err_pen_tdm_mon_cnt_errbit_Mask                                                          cBit31_0
#define cAf6_err_pen_tdm_mon_cnt_errbit_Shift                                                                0
#define cAf6_err_pen_tdm_mon_cnt_errbit_MaxVal                                                      0xffffffff
#define cAf6_err_pen_tdm_mon_cnt_errbit_MinVal                                                             0x0
#define cAf6_err_pen_tdm_mon_cnt_errbit_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter loss bit in TDM mon
Reg Addr   : 0x8_5A0 - 0xB_DA0
Reg Formula: 0x8_5A0 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_lossbit_pen_tdm_mon_Base                                                                0x85A0
#define cAf6Reg_lossbit_pen_tdm_mon(slice)                                           (0x85A0UL+2048UL*(slice))
#define cAf6Reg_lossbit_pen_tdm_mon_WidthVal                                                                32
#define cAf6Reg_lossbit_pen_tdm_mon_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cnt_errbit
BitField Type: R2C
BitField Desc: counter err bit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_Bit_Start                                                        0
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_Bit_End                                                         31
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_Mask                                                      cBit31_0
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_Shift                                                            0
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_MaxVal                                                  0xffffffff
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_MinVal                                                         0x0
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : counter good bit TDM mon
Reg Addr   : 0x8_780 - 0xB_F80
Reg Formula: 0x8_780 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_pw_mon_Base                                                                 0x8780
#define cAf6Reg_goodbit_pen_pw_mon(slice)                                            (0x8780UL+2048UL*(slice))
#define cAf6Reg_goodbit_pen_pw_mon_WidthVal                                                                 32
#define cAf6Reg_goodbit_pen_pw_mon_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: cnt_pwgoodbit
BitField Type: R2C
BitField Desc: counter goodbit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_Bit_Start                                                      0
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_Bit_End                                                       31
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_Mask                                                    cBit31_0
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_Shift                                                          0
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_MaxVal                                                0xffffffff
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_MinVal                                                       0x0
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : counter error bit in TDM mon
Reg Addr   : 0x8_760 - 0xB_F60
Reg Formula: 0x8_760 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_errbit_pen_pw_mon_Base                                                                  0x8760
#define cAf6Reg_errbit_pen_pw_mon(slice)                                             (0x8760UL+2048UL*(slice))
#define cAf6Reg_errbit_pen_pw_mon_WidthVal                                                                  32
#define cAf6Reg_errbit_pen_pw_mon_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: cnt_pwerrbit
BitField Type: R2C
BitField Desc: counter err bit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_Bit_Start                                                        0
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_Bit_End                                                         31
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_Mask                                                      cBit31_0
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_Shift                                                            0
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_MaxVal                                                  0xffffffff
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_MinVal                                                         0x0
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter loss bit in TDM mon
Reg Addr   : 0x8_7A0 - 0xB_FA0
Reg Formula: 0x8_7A0 + 2048*slice
    Where  : 
           + $slice(0-0): is OC48 slice
Reg Desc   : 
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_lossbit_pen_pw_mon_Base                                                                 0x87A0
#define cAf6Reg_lossbit_pen_pw_mon(slice)                                            (0x87A0UL+2048UL*(slice))
#define cAf6Reg_lossbit_pen_pw_mon_WidthVal                                                                 32
#define cAf6Reg_lossbit_pen_pw_mon_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: cnt_pwlossbi
BitField Type: R2C
BitField Desc: counter loss bit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_Bit_Start                                                       0
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_Bit_End                                                        31
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_Mask                                                     cBit31_0
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_Shift                                                           0
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_MaxVal                                                 0xffffffff
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_MinVal                                                        0x0
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM Map Parity Force Control
Reg Addr   : 0x4808
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Map_Parity_Force_Control_Base                                                       0x4808
#define cAf6Reg_RAM_Map_Parity_Force_Control                                                          0x4808UL
#define cAf6Reg_RAM_Map_Parity_Force_Control_WidthVal                                                       32
#define cAf6Reg_RAM_Map_Parity_Force_Control_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: MAPSlc7Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice7"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_Bit_Start                                       7
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_Bit_End                                       7
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_Mask                                     cBit7
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_Shift                                        7
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc6Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice6"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_Bit_Start                                       6
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_Bit_End                                       6
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_Mask                                     cBit6
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_Shift                                        6
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc5Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice5"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_Bit_Start                                       5
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_Bit_End                                       5
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_Mask                                     cBit5
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_Shift                                        5
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc4Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice4"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_Bit_Start                                       4
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_Bit_End                                       4
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_Mask                                     cBit4
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_Shift                                        4
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc3Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice3"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_Bit_Start                                       3
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_Bit_End                                       3
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_Mask                                     cBit3
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_Shift                                        3
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc2Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice2"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_Bit_Start                                       2
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_Bit_End                                       2
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_Mask                                     cBit2
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_Shift                                        2
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc1Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice1"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_Bit_Start                                       1
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_Bit_End                                       1
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_Mask                                     cBit1
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_Shift                                        1
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc0Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice0"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_Bit_Start                                       0
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_Bit_End                                       0
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_Mask                                     cBit0
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_Shift                                        0
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM Map Parity Disable Control
Reg Addr   : 0x4809
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Map_Parity_Disable_Control_Base                                                     0x4809
#define cAf6Reg_RAM_Map_Parity_Disable_Control                                                        0x4809UL
#define cAf6Reg_RAM_Map_Parity_Disable_Control_WidthVal                                                     32
#define cAf6Reg_RAM_Map_Parity_Disable_Control_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: MAPSlc7Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice7"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_Bit_Start                                       7
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_Bit_End                                       7
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_Mask                                   cBit7
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_Shift                                       7
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc6Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice6"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_Bit_Start                                       6
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_Bit_End                                       6
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_Mask                                   cBit6
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_Shift                                       6
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc5Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice5"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_Bit_Start                                       5
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_Bit_End                                       5
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_Mask                                   cBit5
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_Shift                                       5
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc4Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice4"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_Bit_Start                                       4
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_Bit_End                                       4
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_Mask                                   cBit4
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_Shift                                       4
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc3Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice3"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_Bit_Start                                       3
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_Bit_End                                       3
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_Mask                                   cBit3
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_Shift                                       3
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc2Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice2"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_Bit_Start                                       2
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_Bit_End                                       2
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_Mask                                   cBit2
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_Shift                                       2
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc1Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice1"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_Bit_Start                                       1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_Bit_End                                       1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_Mask                                   cBit1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_Shift                                       1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc0Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice0"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_Bit_Start                                       0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_Bit_End                                       0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_Mask                                   cBit0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_Shift                                       0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM Map parity Error Sticky
Reg Addr   : 0x480a
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Map_Parity_Error_Sticky_Base                                                        0x480a
#define cAf6Reg_RAM_Map_Parity_Error_Sticky                                                           0x480aUL
#define cAf6Reg_RAM_Map_Parity_Error_Sticky_WidthVal                                                        32
#define cAf6Reg_RAM_Map_Parity_Error_Sticky_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: MAPSlc7Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice7"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_Bit_Start                                       7
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_Bit_End                                       7
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_Mask                                      cBit7
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_Shift                                         7
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_MaxVal                                      0x1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_RstVal                                      0x0

/*--------------------------------------
BitField Name: MAPSlc6Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice6"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_Bit_Start                                       6
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_Bit_End                                       6
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_Mask                                      cBit6
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_Shift                                         6
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_MaxVal                                      0x1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_RstVal                                      0x0

/*--------------------------------------
BitField Name: MAPSlc5Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice5"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_Bit_Start                                       5
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_Bit_End                                       5
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_Mask                                      cBit5
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_Shift                                         5
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_MaxVal                                      0x1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_RstVal                                      0x0

/*--------------------------------------
BitField Name: MAPSlc4Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice4"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_Bit_Start                                       4
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_Bit_End                                       4
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_Mask                                      cBit4
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_Shift                                         4
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_MaxVal                                      0x1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_RstVal                                      0x0

/*--------------------------------------
BitField Name: MAPSlc3Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice3"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_Bit_Start                                       3
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_Bit_End                                       3
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_Mask                                      cBit3
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_Shift                                         3
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_MaxVal                                      0x1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_RstVal                                      0x0

/*--------------------------------------
BitField Name: MAPSlc2Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice2"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_Bit_Start                                       2
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_Bit_End                                       2
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_Mask                                      cBit2
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_Shift                                         2
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_MaxVal                                      0x1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_RstVal                                      0x0

/*--------------------------------------
BitField Name: MAPSlc1Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice1"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_Bit_Start                                       1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_Bit_End                                       1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_Mask                                      cBit1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_Shift                                         1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_MaxVal                                      0x1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_RstVal                                      0x0

/*--------------------------------------
BitField Name: MAPSlc0Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice0"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_Bit_Start                                       0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_Bit_End                                       0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_Mask                                      cBit0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_Shift                                         0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_MaxVal                                      0x1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_MinVal                                      0x0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_RstVal                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM DeMap Parity Force Control
Reg Addr   : 0x0808
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_DeMap_Parity_Force_Control_Base                                                     0x0808
#define cAf6Reg_RAM_DeMap_Parity_Force_Control                                                        0x0808UL
#define cAf6Reg_RAM_DeMap_Parity_Force_Control_WidthVal                                                     32
#define cAf6Reg_RAM_DeMap_Parity_Force_Control_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: MAPSlc7Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice7"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_Bit_Start                                       7
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_Bit_End                                       7
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_Mask                                   cBit7
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_Shift                                       7
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc6Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice6"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_Bit_Start                                       6
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_Bit_End                                       6
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_Mask                                   cBit6
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_Shift                                       6
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc5Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice5"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_Bit_Start                                       5
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_Bit_End                                       5
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_Mask                                   cBit5
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_Shift                                       5
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc4Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice4"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_Bit_Start                                       4
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_Bit_End                                       4
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_Mask                                   cBit4
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_Shift                                       4
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc3Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice3"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_Bit_Start                                       3
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_Bit_End                                       3
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_Mask                                   cBit3
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_Shift                                       3
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc2Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice2"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_Bit_Start                                       2
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_Bit_End                                       2
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_Mask                                   cBit2
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_Shift                                       2
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc1Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice1"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_Bit_Start                                       1
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_Bit_End                                       1
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_Mask                                   cBit1
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_Shift                                       1
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc0Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice0"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_Bit_Start                                       0
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_Bit_End                                       0
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_Mask                                   cBit0
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_Shift                                       0
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM DeMap Parity Disable Control
Reg Addr   : 0x0809
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_DeMap_Parity_Disable_Control_Base                                                   0x0809
#define cAf6Reg_RAM_DeMap_Parity_Disable_Control                                                      0x0809UL
#define cAf6Reg_RAM_DeMap_Parity_Disable_Control_WidthVal                                                   32
#define cAf6Reg_RAM_DeMap_Parity_Disable_Control_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: MAPSlc7Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice7"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_Bit_Start                                       7
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_Bit_End                                       7
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_Mask                                   cBit7
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_Shift                                       7
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc6Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice6"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_Bit_Start                                       6
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_Bit_End                                       6
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_Mask                                   cBit6
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_Shift                                       6
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc5Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice5"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_Bit_Start                                       5
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_Bit_End                                       5
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_Mask                                   cBit5
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_Shift                                       5
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc4Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice4"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_Bit_Start                                       4
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_Bit_End                                       4
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_Mask                                   cBit4
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_Shift                                       4
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc3Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice3"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_Bit_Start                                       3
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_Bit_End                                       3
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_Mask                                   cBit3
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_Shift                                       3
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc2Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice2"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_Bit_Start                                       2
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_Bit_End                                       2
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_Mask                                   cBit2
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_Shift                                       2
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc1Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice1"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_Bit_Start                                       1
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_Bit_End                                       1
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_Mask                                   cBit1
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_Shift                                       1
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc0Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice0"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_Bit_Start                                       0
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_Bit_End                                       0
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_Mask                                   cBit0
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_Shift                                       0
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM DeMap parity Error Sticky
Reg Addr   : 0x080a
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_DeMap_Parity_Error_Sticky_Base                                                      0x080a
#define cAf6Reg_RAM_DeMap_Parity_Error_Sticky                                                         0x080aUL
#define cAf6Reg_RAM_DeMap_Parity_Error_Sticky_WidthVal                                                      32
#define cAf6Reg_RAM_DeMap_Parity_Error_Sticky_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: MAPSlc7Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice7"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_Bit_Start                                       7
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_Bit_End                                       7
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_Mask                                    cBit7
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_Shift                                       7
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc6Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice6"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_Bit_Start                                       6
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_Bit_End                                       6
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_Mask                                    cBit6
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_Shift                                       6
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc5Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice5"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_Bit_Start                                       5
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_Bit_End                                       5
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_Mask                                    cBit5
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_Shift                                       5
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc4Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice4"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_Bit_Start                                       4
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_Bit_End                                       4
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_Mask                                    cBit4
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_Shift                                       4
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc3Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice3"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_Bit_Start                                       3
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_Bit_End                                       3
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_Mask                                    cBit3
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_Shift                                       3
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc2Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice2"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_Bit_Start                                       2
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_Bit_End                                       2
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_Mask                                    cBit2
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_Shift                                       2
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc1Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice1"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_Bit_Start                                       1
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_Bit_End                                       1
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_Mask                                    cBit1
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_Shift                                       1
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_RstVal                                     0x0

/*--------------------------------------
BitField Name: MAPSlc0Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice0"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_Bit_Start                                       0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_Bit_End                                       0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_Mask                                    cBit0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_Shift                                       0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_MaxVal                                     0x1
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_MinVal                                     0x0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_RstVal                                     0x0

#endif /* _AF6_REG_AF6CCI0012_RD_MAP_HO_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : Tha60210012ModuleMapInternal.h
 * 
 * Created Date: Feb 29, 2016
 *
 * Description : 60210012 module MAP internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/
#ifndef _THA60210012MODULEMAPINTERNAL_H_
#define _THA60210012MODULEMAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/map/Tha60210011ModuleMapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleDemap
    {
    tTha60210011ModuleDemap super;
    }tTha60210012ModuleDemap;

typedef struct tTha60210012ModuleMap
    {
    tTha60210011ModuleMap super;
    }tTha60210012ModuleMap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210012ModuleMapObjectInit(AtModule self, AtDevice device);
AtModule Tha60210012ModuleDemapObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEMAPINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210012InternalRamPdh.c
 *
 * Created Date: Oct 19, 2016
 *
 * Description : Internal RAM of module PDH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ram/Tha60210011InternalRamInternal.h"
#include "../../Tha60210011/pdh/Tha60210011ModulePdh.h"
#include "Tha60210012ModuleRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012InternalRamPdh
    {
    tTha60210011InternalRamPdh super;
    }tTha60210012InternalRamPdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tAtInternalRamMethods  m_AtInternalRamOverride;

/* Save super implementations */
static const tAtInternalRamMethods *m_AtInternalRamMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModulePdh ModulePdh(AtInternalRam self)
    {
    return (Tha60210011ModulePdh)AtInternalRamPhyModuleGet(self);
    }

static uint32 NumRamsInSlice(AtInternalRam self)
    {
    return Tha60210011ModulePdhNumPdhRamsInSlice(ModulePdh(self));
    }

static uint32 LocalIdInSlice(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    return AtInternalRamLocalIdGet(internalRam) % NumRamsInSlice(internalRam);
    }

static eBool IsReserved(AtInternalRam self)
    {
    if (m_AtInternalRamMethods->IsReserved(self))
        return cAtTrue;

    /* PDH DS1/E1/J1 Rx Framer Mux Control is reserved */
    return (LocalIdInSlice((ThaInternalRam)self) == (NumRamsInSlice(self) - 1)) ? cAtTrue : cAtFalse;
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtInternalRamMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, m_AtInternalRamMethods, sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, IsReserved);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideAtInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012InternalRamPdh);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011InternalRamPdhObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210012InternalRamPdhNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

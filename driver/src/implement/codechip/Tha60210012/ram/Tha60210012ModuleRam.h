/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      :  
 * 
 * File        : Tha60210012ModuleRam.h
 * 
 * Created Date: Mar 23, 2016
 *
 * Author: phuongnm
 *
 * Description : To Do
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/
 
#ifndef _THA60210012MODULERAM_H_
#define _THA60210012MODULERAM_H_



/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha60210012ModuleRamNew(AtDevice device);

/* Internal RAMs */
AtInternalRam Tha60210012InternalRamClaNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210012InternalRamOcnNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210012InternalRamPlaNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210012InternalRamPdhNew(AtModule phyModule, uint32 ramId, uint32 localId);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210012MODULERAM_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210012ModuleRamInternal.h
 * 
 * Created Date: Oct 20, 2016
 *
 * Description : RAM internal
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULERAMINTERNAL_H_
#define _THA60210012MODULERAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/ram/Tha60210011ModuleRamInternal.h"
#include "Tha60210012ModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleRam
    {
    tTha60210011ModuleRam super;
    }tTha60210012ModuleRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha60210012ModuleRamObjectInit(AtModuleRam self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULERAMINTERNAL_H_ */


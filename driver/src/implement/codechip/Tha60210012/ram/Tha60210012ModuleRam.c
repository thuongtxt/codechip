/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      :  
 *
 * File        : Tha60210012ModuleRam.c
 *
 * Created Date: Mar 23, 2016
 *
 * Description : Module RAM of 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ram/Tha60210011ModuleRamInternal.h"
#include "Tha60210012ModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
 
/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleRamMethods m_ThaModuleRamOverride;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool InternalRamMonitoringIsSupported(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModuleRam(AtModuleRam self)
    {
    ThaModuleRam ramModule = (ThaModuleRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleRamOverride, mMethodsGet(ramModule), sizeof(m_ThaModuleRamOverride));

        mMethodOverride(m_ThaModuleRamOverride, InternalRamMonitoringIsSupported);
        }

    mMethodsSet(ramModule, &m_ThaModuleRamOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideThaModuleRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleRam);
    }

AtModuleRam Tha60210012ModuleRamObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleRamObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam Tha60210012ModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModuleRamObjectInit(newModule, device);
    }

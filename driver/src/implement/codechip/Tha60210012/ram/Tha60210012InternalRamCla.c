/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210012InternalRamCla.c
 *
 * Created Date: Jul 27, 2016
 *
 * Description : Internal RAM of module CLA
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ram/Tha60210011InternalRamInternal.h"
#include "../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "Tha60210012ModuleRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012InternalRamCla
    {
    tTha60210011InternalRamCla super;
    }tTha60210012InternalRamCla;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsReserved(AtInternalRam self)
    {
    AtUnused(self);
    /* No reserved ram */
    return cAtFalse;
    }

static uint32 FirstCellOffset(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);

    /* Classify HBCE Looking Up Information Control 1 */
    if (localId < 4)
        return (uint32)(0x80000 + localId * 0x10000);

    /* Classify VLAN looking Up Control */
    if (localId == 4)
        return 0x50000;

    /* Classify Per Pseudowire Type Control */
    if (localId == 5)
        return 0x8000;

    /* Classify Per Group Enable Control */
    if (localId == 6)
        return 0x70000;

    /* Classify HBCE Looking Up Information Control 2 */
    if (localId < 15)
        return (uint32)(0x40000 + (localId - 7) * 0x1000);

    /* Classify HBCE Hashing Table Control */
    if (localId == 15)
        return 0x20000;

    /* Classify Per Pseudowire Identification to CDR Control */
    return 0xC0000;
    }

static uint32 BaseAddress(ThaInternalRam self)
    {
    return Tha60210011ModuleClaBaseAddress((ThaModuleCla)AtInternalRamPhyModuleGet((AtInternalRam)self));
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    if (AtInternalRamIsReserved((AtInternalRam)self))
        return cInvalidUint32;

    return (uint32)(BaseAddress(self) + FirstCellOffset(self));
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012InternalRamCla);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, IsReserved);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideAtInternalRam(self);
    OverrideThaInternalRam(self);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011InternalRamClaObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210012InternalRamClaNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }



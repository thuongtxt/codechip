/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210012InternalRamOcn.c
 *
 * Created Date: Jul 27, 2016
 *
 * Description : Internal RAM of module OCN
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../Tha60210011/sdh/Tha60210011ModuleSdh.h"
#include "../../Tha60210011/ram/Tha60210011InternalRamInternal.h"
#include "../ram/Tha60210012ModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#define cStsPpMaxRamLocalId     8
#define cSxcMaxRamLocalId       16

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012InternalRamOcn
    {
    tTha60210011InternalRamOcn super;
    }tTha60210012InternalRamOcn;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtInternalRam self)
    {
    return Tha60210011ModuleOcnBaseAddress((ThaModuleOcn)AtInternalRamPhyModuleGet(self));
    }

static eBool IsReserved(AtInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    uint32 localId = AtInternalRamLocalIdGet(internalRam);

    if (localId < cStsPpMaxRamLocalId)
        return BaseAddress(internalRam) + 0x10;

    if (localId < cSxcMaxRamLocalId)
        return BaseAddress(internalRam) + 0x14;

    return BaseAddress(internalRam) + 0x18;
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);

    /* OCN Rx Bridge and Roll SXC Control */
    if (localId < 2)
        return cBit0 << localId;

    /* OCN Tx Bridge and Roll SXC Control */
    if (localId < 4)
        return cBit8 << (localId - 2);

    /* OCN STS Pointer Interpreter Per Channel Control */
    if (localId < 6)
        return cBit16 << (localId - 4);

    /* OCN STS Pointer Generator Per Channel Control */
    if (localId < 8)
        return cBit24 << (localId - 6);

    /* OCN Rx SXC Control */
    if (localId < 12)
        return cBit0 << (localId - 8);

    /* OCN Tx SXC Control */
    if (localId < 14)
        return cBit14 << (localId - 12);

    /* OCN Rx High Order Map concatenate configuration */
    if (localId < 16)
        return cBit22 << (localId - 14);

    /* OCN RXPP Per STS payload Control */
    if (localId < 18)
        return cBit0 << (localId - 16);

    /* OCN VTTU Pointer Interpreter Per Channel Control */
    if (localId < 20)
        return cBit6 << (localId - 18);

    /* OCN TXPP Per STS Multiplexing Control */
    if (localId < 22)
        return cBit12 << (localId - 20);

    /* OCN VTTU Pointer Generator Per Channel Control */
    if (localId < 24)
        return cBit18 << (localId - 22);

    return 0;
    }

static AtModuleSdh ModuleSdh(ThaInternalRam self)
    {
    AtDevice device = AtModuleDeviceGet(AtInternalRamPhyModuleGet((AtInternalRam)self));
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static uint32 NumHoSlice(ThaInternalRam self)
    {
    return Tha60210011ModuleSdhMaxNumHoLines(ModuleSdh(self));
    }

static uint32 NumLoSlice(ThaInternalRam self)
    {
    return Tha60210011ModuleSdhMaxNumLoLines(ModuleSdh(self));
    }

static uint32 HoSliceIdGet(ThaInternalRam self, uint32 offset)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    return (uint32)((localId - offset) % NumHoSlice(self));
    }

static uint32 LoSliceIdGet(ThaInternalRam self, uint32 offset)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    return (uint32)((localId - offset) % NumLoSlice(self));
    }

static uint32 FirstCellOffset(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);

    if (IsReserved((AtInternalRam)self))
        return cInvalidUint32;

    /* OCN Rx Bridge and Roll SXC Control */
    if (localId < 2)
        return (uint32)(HoSliceIdGet(self, 0) * 0x100 + 0x26000);

    /* OCN Tx Bridge and Roll SXC Control */
    if (localId < 4)
        return (uint32)(HoSliceIdGet(self, 2) * 0x100 + 0x27000);

    /* OCN STS Pointer Interpreter Per Channel Control */
    if (localId < 6)
        return (uint32)(HoSliceIdGet(self, 4) * 0x200 + 0x22000);

    /* OCN STS Pointer Generator Per Channel Control */
    if (localId < 8)
        return (uint32)(HoSliceIdGet(self, 6) * 0x200 + 0x23000);

    /* OCN Rx SXC Control */
    if (localId < 12)
        return (uint32)(((localId - 8) % 4) * 0x100 + 0x24000);

    /* OCN Tx SXC Control */
    if (localId < 14)
        return (uint32)(HoSliceIdGet(self, 12) * 0x100 + 0x25000);

    /* OCN Rx High Order Map concatenate configuration */
    if (localId < 16)
        return (uint32)(HoSliceIdGet(self, 14) * 0x200 + 0x28000);

    /* OCN RXPP Per STS payload Control */
    if (localId < 18)
        return (uint32)(LoSliceIdGet(self, 16) * 0x4000 + 0x40000);

    /* OCN VTTU Pointer Interpreter Per Channel Control */
    if (localId < 20)
        return (uint32)(LoSliceIdGet(self, 18) * 0x4000 + 0x40800);

    /* OCN TXPP Per STS Multiplexing Control */
    if (localId < 22)
        return (uint32)(LoSliceIdGet(self, 20) * 0x4000 + 0x60000);

    /* OCN VTTU Pointer Generator Per Channel Control */
    if (localId < 24)
        return (uint32)(LoSliceIdGet(self, 22) * 0x4000 + 0x60800);

    return cInvalidUint32;
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    uint32 offset = FirstCellOffset(self);
    if (offset == cInvalidUint32)
        return cInvalidUint32;

    return (uint32)(BaseAddress((AtInternalRam)self) + offset);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, IsReserved);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    OverrideAtInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012InternalRamOcn);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011InternalRamOcnObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210012InternalRamOcnNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

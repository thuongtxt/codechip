/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210012InternalRamPla.c
 *
 * Created Date: Aug 3, 2016
 *
 * Description : Internal RAM of module PLA
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ram/Tha60210011InternalRamInternal.h"
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "Tha60210012ModuleRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012InternalRamPla
    {
    tTha60210011InternalRamPla super;
    }tTha60210012InternalRamPla;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tThaInternalRamMethods m_ThaInternalRamOverride;

/* Save super implementation */
static const tThaInternalRamMethods *m_ThaInternalRamMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModulePwe ModulePwe(ThaInternalRam self)
    {
    return (Tha60210011ModulePwe)AtInternalRamPhyModuleGet((AtInternalRam)self);
    }

static uint32 CrcRamLocalId(ThaInternalRam self)
    {
    return Tha60210011ModulePweStartCrcRamLocalId(ModulePwe(self));
    }

static uint32 HSPWRamLocalStartId(ThaInternalRam self)
    {
    return Tha60210011ModulePweStartHSPWRamLocalId(ModulePwe(self));
    }

static uint32 HoPlaRamLocalStartId(ThaInternalRam self)
    {
    return Tha60210011ModulePweStartHoPlaRamLocalId(ModulePwe(self));
    }

static eBool RamIsParity(ThaInternalRam self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) != CrcRamLocalId(self))
        return cAtTrue;

    return cAtFalse;
    }

static eBool RamIsLoPla(ThaInternalRam self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) < HoPlaRamLocalStartId(self))
        return cAtTrue;

    return cAtFalse;
    }

static eBool RamIsHoPla(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    if ((localId >= HoPlaRamLocalStartId(self)) && (localId < HSPWRamLocalStartId(self)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool RamIsPlaProtection(ThaInternalRam self)
    {
    if (AtInternalRamLocalIdGet((AtInternalRam)self) < CrcRamLocalId(self))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    if (RamIsParity(self))
        return Tha60210011ModulePlaBaseAddress() + 0x21030;

    /* CRC PLA */
    return Tha60210011ModulePlaBaseAddress() + 0x21034;
    }

static uint32 LoPlaLocalIdMask(uint32 localId)
    {
    return cBit0 << (((localId / 3) * 4) + (localId % 3)) ;
    }

static uint32 HoPlaLocalIdMask(uint32 localId)
    {
    return cBit8 << (((localId / 3) * 4) + (localId % 3)) ;
    }

static uint32 HspwUpsrLocalIdMask(uint32 localId)
    {
    return cBit16 << localId;
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    if (RamIsLoPla(self))
        return LoPlaLocalIdMask(localId);

    if (RamIsHoPla(self))
        return HoPlaLocalIdMask(localId - HoPlaRamLocalStartId(self));

    if (RamIsPlaProtection(self))
        return HspwUpsrLocalIdMask(localId - HSPWRamLocalStartId(self));

    return cBit0;
    }

static AtErrorGenerator ErrorGeneratorObjectCreate(ThaInternalRam self)
    {
    AtUnused(self);
    /* will open when HW ready */
    return NULL;
    }

static uint32 LoSliceOffsetGet(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    return (uint32)(0x8000 * (localId / 3));
    }

static uint32 HoSliceOffsetGet(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    return (uint32)(0x100 * ((localId - HoPlaRamLocalStartId(self)) / 3));
    }

static uint32 FirstCellOffset(ThaInternalRam self)
    {
    uint32 ramId = AtInternalRamLocalIdGet((AtInternalRam)self);

    if (RamIsLoPla(self))
        {
        static const uint32 cellAddressOfLoPla[] = {0x1000, 0x3000, 0x4000};
        return cellAddressOfLoPla[ramId % 3] + LoSliceOffsetGet(self);
        }

    if (RamIsHoPla(self))
        {
        static const uint32 cellAddressOfHoPla[] = {0x10080, 0x10180, 0x10200};
        return cellAddressOfHoPla[ramId % 3] + HoSliceOffsetGet(self);
        }

    if (RamIsPlaProtection(self))
        {
        static const uint32 cellAddressOfHspwUpsr[] = {0x28000, 0x30000, 0x38000, 0x34000, 0x18000, 0x2c000, 0x24000};
        uint32 localId = ramId - HSPWRamLocalStartId(self);
        if (localId >= mCount(cellAddressOfHspwUpsr))
            return cInvalidUint32;

        return cellAddressOfHspwUpsr[localId];
        }

    return cInvalidUint32;
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    uint32 offset = FirstCellOffset(self);
    if (offset == cInvalidUint32)
        return cInvalidUint32;

    return (uint32)(Tha60210011ModulePlaBaseAddress() + offset);
    }

static eBool ErrorGeneratorIsSupported(ThaInternalRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaInternalRamMethods = mMethodsGet(ram);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorGeneratorObjectCreate);
        mMethodOverride(m_ThaInternalRamOverride, ErrorGeneratorIsSupported);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012InternalRamPla);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011InternalRamPlaObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210012InternalRamPlaNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      : MPEG
 *                                                                              
 * File        : Tha60210012MpegReg.h
 *                                                                              
 * Created Date: Mar 15, 2016
 *                                                                              
 * Description : This file contain all constant definitions of MPEG block.
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0012_RD_MPEG_H_
#define _AF6_REG_AF6CCI0012_RD_MPEG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Bundle Queue Threshold Control
Reg Addr   : 0x20000-0x207FF
Reg Formula: 0x20000+$qid
    Where  : 
           + qid(0-2047) : Queue ID
Reg Desc   : 
Used to configure threshold for Bundle Queue
: {qid(0000-2047)} -> For Bundle Queue,512 Bundles, 4 Queues per Bundle

------------------------------------------------------------------------------*/
#define cAf6Reg_cfg0qthsh_pen_Base                                                                     0x20000
#define cAf6Reg_cfg0qthsh_pen(qid)                                                             (0x20000+(qid))
#define cAf6Reg_cfg0qthsh_pen_WidthVal                                                                      32
#define cAf6Reg_cfg0qthsh_pen_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: Queue_Enb
BitField Type: R/W
BitField Desc: Queue Enable 0: Disable Queue and flush Queue 1: Enable Queue
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_cfg0qthsh_pen_Queue_Enb_Bit_Start                                                              20
#define cAf6_cfg0qthsh_pen_Queue_Enb_Bit_End                                                                20
#define cAf6_cfg0qthsh_pen_Queue_Enb_Mask                                                               cBit20
#define cAf6_cfg0qthsh_pen_Queue_Enb_Shift                                                                  20
#define cAf6_cfg0qthsh_pen_Queue_Enb_MaxVal                                                                0x1
#define cAf6_cfg0qthsh_pen_Queue_Enb_MinVal                                                                0x0
#define cAf6_cfg0qthsh_pen_Queue_Enb_RstVal                                                                0x0

/*--------------------------------------
BitField Name: EFFQthsh_max
BitField Type: R/W
BitField Desc: External FiFo maximun queue threshold, step 128 pkts
BitField Bits: [19:15]
--------------------------------------*/
#define cAf6_cfg0qthsh_pen_EFFQthsh_max_Bit_Start                                                           15
#define cAf6_cfg0qthsh_pen_EFFQthsh_max_Bit_End                                                             19
#define cAf6_cfg0qthsh_pen_EFFQthsh_max_Mask                                                         cBit19_15
#define cAf6_cfg0qthsh_pen_EFFQthsh_max_Shift                                                               15
#define cAf6_cfg0qthsh_pen_EFFQthsh_max_MaxVal                                                            0x1f
#define cAf6_cfg0qthsh_pen_EFFQthsh_max_MinVal                                                             0x0
#define cAf6_cfg0qthsh_pen_EFFQthsh_max_RstVal                                                             0x8

/*--------------------------------------
BitField Name: EFFQthsh_min
BitField Type: R/W
BitField Desc: External FiFo minimun queue threshold, step 128 pkts
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_Bit_Start                                                           10
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_Bit_End                                                             14
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_Mask                                                         cBit14_10
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_Shift                                                               10
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_MaxVal                                                            0x1f
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_MinVal                                                             0x0
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_RstVal                                                             0x2

/*--------------------------------------
BitField Name: IFFQthsh_max
BitField Type: R/W
BitField Desc: Internal FiFo maximun queue threshold, step 1 pkts
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_cfg0qthsh_pen_IFFQthsh_max_Bit_Start                                                            5
#define cAf6_cfg0qthsh_pen_IFFQthsh_max_Bit_End                                                              9
#define cAf6_cfg0qthsh_pen_IFFQthsh_max_Mask                                                           cBit9_5
#define cAf6_cfg0qthsh_pen_IFFQthsh_max_Shift                                                                5
#define cAf6_cfg0qthsh_pen_IFFQthsh_max_MaxVal                                                            0x1f
#define cAf6_cfg0qthsh_pen_IFFQthsh_max_MinVal                                                             0x0
#define cAf6_cfg0qthsh_pen_IFFQthsh_max_RstVal                                                            0x31

/*--------------------------------------
BitField Name: IFFQthsh_min
BitField Type: R/W
BitField Desc: Internal FiFo minimun queue threshold, step 1 pkts
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_Bit_Start                                                            0
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_Bit_End                                                              4
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_Mask                                                           cBit4_0
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_Shift                                                                0
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_MaxVal                                                            0x1f
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_MinVal                                                             0x0
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_RstVal                                                             0x7


/*------------------------------------------------------------------------------
Reg Name   : Vcat Queue Threshold Control
Reg Addr   : 0x21000-0x211FF
Reg Formula: 0x21000+$qid
    Where  : 
           + qid(0-511) : Queue ID
Reg Desc   : 
Used to configure threshold for Vcat Queue
: {qid(0-511)} -> For Vcat Queue,256 VCG, 2 queues per VCG (Queue#0: in use,Queue#1: reserved)

------------------------------------------------------------------------------*/
#define cAf6Reg_cfg1qthsh_pen_Base                                                                     0x21000
#define cAf6Reg_cfg1qthsh_pen(qid)                                                             (0x21000+(qid))
#define cAf6Reg_cfg1qthsh_pen_WidthVal                                                                      32
#define cAf6Reg_cfg1qthsh_pen_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: Queue_Enb
BitField Type: R/W
BitField Desc: Queue Enable 0: Disable Queue and flush Queue 1: Enable Queue
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_cfg1qthsh_pen_Queue_Enb_Bit_Start                                                              20
#define cAf6_cfg1qthsh_pen_Queue_Enb_Bit_End                                                                20
#define cAf6_cfg1qthsh_pen_Queue_Enb_Mask                                                               cBit20
#define cAf6_cfg1qthsh_pen_Queue_Enb_Shift                                                                  20
#define cAf6_cfg1qthsh_pen_Queue_Enb_MaxVal                                                                0x1
#define cAf6_cfg1qthsh_pen_Queue_Enb_MinVal                                                                0x0
#define cAf6_cfg1qthsh_pen_Queue_Enb_RstVal                                                                0x0

/*--------------------------------------
BitField Name: EFFQthsh_max
BitField Type: R/W
BitField Desc: External FiFo maximun queue threshold, step 128 pkts
BitField Bits: [19:15]
--------------------------------------*/
#define cAf6_cfg1qthsh_pen_EFFQthsh_max_Bit_Start                                                           15
#define cAf6_cfg1qthsh_pen_EFFQthsh_max_Bit_End                                                             19
#define cAf6_cfg1qthsh_pen_EFFQthsh_max_Mask                                                         cBit19_15
#define cAf6_cfg1qthsh_pen_EFFQthsh_max_Shift                                                               15
#define cAf6_cfg1qthsh_pen_EFFQthsh_max_MaxVal                                                            0x1f
#define cAf6_cfg1qthsh_pen_EFFQthsh_max_MinVal                                                             0x0
#define cAf6_cfg1qthsh_pen_EFFQthsh_max_RstVal                                                             0x2

/*--------------------------------------
BitField Name: EFFQthsh_min
BitField Type: R/W
BitField Desc: External FiFo minimun queue threshold, step 128 pkts
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_cfg1qthsh_pen_EFFQthsh_min_Bit_Start                                                           10
#define cAf6_cfg1qthsh_pen_EFFQthsh_min_Bit_End                                                             14
#define cAf6_cfg1qthsh_pen_EFFQthsh_min_Mask                                                         cBit14_10
#define cAf6_cfg1qthsh_pen_EFFQthsh_min_Shift                                                               10
#define cAf6_cfg1qthsh_pen_EFFQthsh_min_MaxVal                                                            0x1f
#define cAf6_cfg1qthsh_pen_EFFQthsh_min_MinVal                                                             0x0
#define cAf6_cfg1qthsh_pen_EFFQthsh_min_RstVal                                                             0x2

/*--------------------------------------
BitField Name: IFFQthsh_max
BitField Type: R/W
BitField Desc: Internal FiFo maximun queue threshold, step 1 pkts
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_cfg1qthsh_pen_IFFQthsh_max_Bit_Start                                                            5
#define cAf6_cfg1qthsh_pen_IFFQthsh_max_Bit_End                                                              9
#define cAf6_cfg1qthsh_pen_IFFQthsh_max_Mask                                                           cBit9_5
#define cAf6_cfg1qthsh_pen_IFFQthsh_max_Shift                                                                5
#define cAf6_cfg1qthsh_pen_IFFQthsh_max_MaxVal                                                            0x1f
#define cAf6_cfg1qthsh_pen_IFFQthsh_max_MinVal                                                             0x0
#define cAf6_cfg1qthsh_pen_IFFQthsh_max_RstVal                                                             0x7

/*--------------------------------------
BitField Name: IFFQthsh_min
BitField Type: R/W
BitField Desc: Internal FiFo minimun queue threshold, step 1 pkts
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_cfg1qthsh_pen_IFFQthsh_min_Bit_Start                                                            0
#define cAf6_cfg1qthsh_pen_IFFQthsh_min_Bit_End                                                              4
#define cAf6_cfg1qthsh_pen_IFFQthsh_min_Mask                                                           cBit4_0
#define cAf6_cfg1qthsh_pen_IFFQthsh_min_Shift                                                                0
#define cAf6_cfg1qthsh_pen_IFFQthsh_min_MaxVal                                                            0x1f
#define cAf6_cfg1qthsh_pen_IFFQthsh_min_MinVal                                                             0x0
#define cAf6_cfg1qthsh_pen_IFFQthsh_min_RstVal                                                             0x7


/*------------------------------------------------------------------------------
Reg Name   : Link Queue Threshold Control
Reg Addr   : 0x22000-0x237FF
Reg Formula: 0x22000+$qid
    Where  : 
           + qid(0-6143) : Queue ID
Reg Desc   : 
Used to configure threshold for Link Queue
: {qid(0-6143)} -> For Link Queue, 3072 Links, 2 Queues per Link

------------------------------------------------------------------------------*/
#define cAf6Reg_cfg2qthsh_pen_Base                                                                     0x22000
#define cAf6Reg_cfg2qthsh_pen(qid)                                                             (0x22000+(qid))
#define cAf6Reg_cfg2qthsh_pen_WidthVal                                                                      32
#define cAf6Reg_cfg2qthsh_pen_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: Queue_Enb
BitField Type: R/W
BitField Desc: Queue Enable 0: Disable Queue and flush Queue 1: Enable Queue
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_cfg2qthsh_pen_Queue_Enb_Bit_Start                                                              20
#define cAf6_cfg2qthsh_pen_Queue_Enb_Bit_End                                                                20
#define cAf6_cfg2qthsh_pen_Queue_Enb_Mask                                                               cBit20
#define cAf6_cfg2qthsh_pen_Queue_Enb_Shift                                                                  20
#define cAf6_cfg2qthsh_pen_Queue_Enb_MaxVal                                                                0x1
#define cAf6_cfg2qthsh_pen_Queue_Enb_MinVal                                                                0x0
#define cAf6_cfg2qthsh_pen_Queue_Enb_RstVal                                                                0x0

/*--------------------------------------
BitField Name: EFFQthsh_max
BitField Type: R/W
BitField Desc: External FiFo maximun queue threshold, step 128 pkts
BitField Bits: [19:15]
--------------------------------------*/
#define cAf6_cfg2qthsh_pen_EFFQthsh_max_Bit_Start                                                           15
#define cAf6_cfg2qthsh_pen_EFFQthsh_max_Bit_End                                                             19
#define cAf6_cfg2qthsh_pen_EFFQthsh_max_Mask                                                         cBit19_15
#define cAf6_cfg2qthsh_pen_EFFQthsh_max_Shift                                                               15
#define cAf6_cfg2qthsh_pen_EFFQthsh_max_MaxVal                                                            0x1f
#define cAf6_cfg2qthsh_pen_EFFQthsh_max_MinVal                                                             0x0
#define cAf6_cfg2qthsh_pen_EFFQthsh_max_RstVal                                                             0x2

/*--------------------------------------
BitField Name: EFFQthsh_min
BitField Type: R/W
BitField Desc: External FiFo minimun queue threshold, step 128 pkts
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_cfg2qthsh_pen_EFFQthsh_min_Bit_Start                                                           10
#define cAf6_cfg2qthsh_pen_EFFQthsh_min_Bit_End                                                             14
#define cAf6_cfg2qthsh_pen_EFFQthsh_min_Mask                                                         cBit14_10
#define cAf6_cfg2qthsh_pen_EFFQthsh_min_Shift                                                               10
#define cAf6_cfg2qthsh_pen_EFFQthsh_min_MaxVal                                                            0x1f
#define cAf6_cfg2qthsh_pen_EFFQthsh_min_MinVal                                                             0x0
#define cAf6_cfg2qthsh_pen_EFFQthsh_min_RstVal                                                             0x2

/*--------------------------------------
BitField Name: IFFQthsh_max
BitField Type: R/W
BitField Desc: Internal FiFo maximun queue threshold, step 1 pkts
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_cfg2qthsh_pen_IFFQthsh_max_Bit_Start                                                            5
#define cAf6_cfg2qthsh_pen_IFFQthsh_max_Bit_End                                                              9
#define cAf6_cfg2qthsh_pen_IFFQthsh_max_Mask                                                           cBit9_5
#define cAf6_cfg2qthsh_pen_IFFQthsh_max_Shift                                                                5
#define cAf6_cfg2qthsh_pen_IFFQthsh_max_MaxVal                                                            0x1f
#define cAf6_cfg2qthsh_pen_IFFQthsh_max_MinVal                                                             0x0
#define cAf6_cfg2qthsh_pen_IFFQthsh_max_RstVal                                                             0x7

/*--------------------------------------
BitField Name: IFFQthsh_min
BitField Type: R/W
BitField Desc: Internal FiFo minimun queue threshold, step 1 pkts
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_cfg2qthsh_pen_IFFQthsh_min_Bit_Start                                                            0
#define cAf6_cfg2qthsh_pen_IFFQthsh_min_Bit_End                                                              4
#define cAf6_cfg2qthsh_pen_IFFQthsh_min_Mask                                                           cBit4_0
#define cAf6_cfg2qthsh_pen_IFFQthsh_min_Shift                                                                0
#define cAf6_cfg2qthsh_pen_IFFQthsh_min_MaxVal                                                            0x1f
#define cAf6_cfg2qthsh_pen_IFFQthsh_min_MinVal                                                             0x0
#define cAf6_cfg2qthsh_pen_IFFQthsh_min_RstVal                                                             0x7


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Bypass Queue Threshold Control
Reg Addr   : 0x24000-0x240015
Reg Formula: 0x24000+$qid
    Where  : 
           + qid(0-15) : Queue ID
Reg Desc   : 
Used to configure threshold for Link Queue
: {qid(0-15)} -> 16 Queues for Bypass Ethernet Port
: 0-7: for GE ethernet Bypass, 4 port GE, 2 queue each port
: 8-9: for eXAUI#0  Bypass
: 9-10: for eXAUI#1  Bypass

------------------------------------------------------------------------------*/
#define cAf6Reg_cfg3qthsh_pen_Base                                                                     0x24000
#define cAf6Reg_cfg3qthsh_pen(qid)                                                             (0x24000+(qid))
#define cAf6Reg_cfg3qthsh_pen_WidthVal                                                                      32
#define cAf6Reg_cfg3qthsh_pen_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: Queue_Enb
BitField Type: R/W
BitField Desc: Queue Enable 0: Disable Queue and flush Queue 1: Enable Queue
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_cfg3qthsh_pen_Queue_Enb_Bit_Start                                                              20
#define cAf6_cfg3qthsh_pen_Queue_Enb_Bit_End                                                                20
#define cAf6_cfg3qthsh_pen_Queue_Enb_Mask                                                               cBit20
#define cAf6_cfg3qthsh_pen_Queue_Enb_Shift                                                                  20
#define cAf6_cfg3qthsh_pen_Queue_Enb_MaxVal                                                                0x1
#define cAf6_cfg3qthsh_pen_Queue_Enb_MinVal                                                                0x0
#define cAf6_cfg3qthsh_pen_Queue_Enb_RstVal                                                                0x0

/*--------------------------------------
BitField Name: EFFQthsh_max
BitField Type: R/W
BitField Desc: External FiFo maximun queue threshold, step 128 pkts
BitField Bits: [19:15]
--------------------------------------*/
#define cAf6_cfg3qthsh_pen_EFFQthsh_max_Bit_Start                                                           15
#define cAf6_cfg3qthsh_pen_EFFQthsh_max_Bit_End                                                             19
#define cAf6_cfg3qthsh_pen_EFFQthsh_max_Mask                                                         cBit19_15
#define cAf6_cfg3qthsh_pen_EFFQthsh_max_Shift                                                               15
#define cAf6_cfg3qthsh_pen_EFFQthsh_max_MaxVal                                                            0x1f
#define cAf6_cfg3qthsh_pen_EFFQthsh_max_MinVal                                                             0x0
#define cAf6_cfg3qthsh_pen_EFFQthsh_max_RstVal                                                             0x2

/*--------------------------------------
BitField Name: EFFQthsh_min
BitField Type: R/W
BitField Desc: External FiFo minimun queue threshold, step 128 pkts
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_cfg3qthsh_pen_EFFQthsh_min_Bit_Start                                                           10
#define cAf6_cfg3qthsh_pen_EFFQthsh_min_Bit_End                                                             14
#define cAf6_cfg3qthsh_pen_EFFQthsh_min_Mask                                                         cBit14_10
#define cAf6_cfg3qthsh_pen_EFFQthsh_min_Shift                                                               10
#define cAf6_cfg3qthsh_pen_EFFQthsh_min_MaxVal                                                            0x1f
#define cAf6_cfg3qthsh_pen_EFFQthsh_min_MinVal                                                             0x0
#define cAf6_cfg3qthsh_pen_EFFQthsh_min_RstVal                                                             0x2

/*--------------------------------------
BitField Name: IFFQthsh_max
BitField Type: R/W
BitField Desc: Internal FiFo maximun queue threshold, step 1 pkts
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_cfg3qthsh_pen_IFFQthsh_max_Bit_Start                                                            5
#define cAf6_cfg3qthsh_pen_IFFQthsh_max_Bit_End                                                              9
#define cAf6_cfg3qthsh_pen_IFFQthsh_max_Mask                                                           cBit9_5
#define cAf6_cfg3qthsh_pen_IFFQthsh_max_Shift                                                                5
#define cAf6_cfg3qthsh_pen_IFFQthsh_max_MaxVal                                                            0x1f
#define cAf6_cfg3qthsh_pen_IFFQthsh_max_MinVal                                                             0x0
#define cAf6_cfg3qthsh_pen_IFFQthsh_max_RstVal                                                             0x7

/*--------------------------------------
BitField Name: IFFQthsh_min
BitField Type: R/W
BitField Desc: Internal FiFo minimun queue threshold, step 1 pkts
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_cfg3qthsh_pen_IFFQthsh_min_Bit_Start                                                            0
#define cAf6_cfg3qthsh_pen_IFFQthsh_min_Bit_End                                                              4
#define cAf6_cfg3qthsh_pen_IFFQthsh_min_Mask                                                           cBit4_0
#define cAf6_cfg3qthsh_pen_IFFQthsh_min_Shift                                                                0
#define cAf6_cfg3qthsh_pen_IFFQthsh_min_MaxVal                                                            0x1f
#define cAf6_cfg3qthsh_pen_IFFQthsh_min_MinVal                                                             0x0
#define cAf6_cfg3qthsh_pen_IFFQthsh_min_RstVal                                                             0x7


/*------------------------------------------------------------------------------
Reg Name   : Member ID of Bundle Configuration
Reg Addr   : 0x0C000-0x0CBFF
Reg Formula: 0x0C000+$lid
    Where  : 
           + lid(0-3071) : Link ID
Reg Desc   : 
Support 512 bundle, each Bundle have 32 Members. This used to configure Member Enable and Member ID of Bundle
Member ID is used to Link Distribution for Round-Robin mode and strict priority mode
Member #0 is higest priority

------------------------------------------------------------------------------*/
#define cAf6Reg_memidcfg0_pen_Base                                                                     0x0C000
#define cAf6Reg_memidcfg0_pen(lid)                                                             (0x0C000+(lid))
#define cAf6Reg_memidcfg0_pen_WidthVal                                                                      32
#define cAf6Reg_memidcfg0_pen_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: MemBundle_En
BitField Type: R/W
BitField Desc: Member Enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_memidcfg0_pen_MemBundle_En_Bit_Start                                                            5
#define cAf6_memidcfg0_pen_MemBundle_En_Bit_End                                                              5
#define cAf6_memidcfg0_pen_MemBundle_En_Mask                                                             cBit5
#define cAf6_memidcfg0_pen_MemBundle_En_Shift                                                                5
#define cAf6_memidcfg0_pen_MemBundle_En_MaxVal                                                             0x1
#define cAf6_memidcfg0_pen_MemBundle_En_MinVal                                                             0x0
#define cAf6_memidcfg0_pen_MemBundle_En_RstVal                                                             0x0

/*--------------------------------------
BitField Name: MemBundle_ID
BitField Type: R/W
BitField Desc: Member ID of Bundle
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_memidcfg0_pen_MemBundle_ID_Bit_Start                                                            0
#define cAf6_memidcfg0_pen_MemBundle_ID_Bit_End                                                              4
#define cAf6_memidcfg0_pen_MemBundle_ID_Mask                                                           cBit4_0
#define cAf6_memidcfg0_pen_MemBundle_ID_Shift                                                                0
#define cAf6_memidcfg0_pen_MemBundle_ID_MaxVal                                                            0x1f
#define cAf6_memidcfg0_pen_MemBundle_ID_MinVal                                                             0x0
#define cAf6_memidcfg0_pen_MemBundle_ID_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Link Distribution per Bundle
Reg Addr   : 0x10000-0x101FF
Reg Formula: 0x10000+$bid
    Where  : 
           + bid(0-511) : Bundle ID
Reg Desc   : 
Used to configure the mode of Link Distribution

------------------------------------------------------------------------------*/
#define cAf6Reg_lididcfg1_pen_Base                                                                     0x10000
#define cAf6Reg_lididcfg1_pen(bid)                                                             (0x10000+(bid))
#define cAf6Reg_lididcfg1_pen_WidthVal                                                                      32
#define cAf6Reg_lididcfg1_pen_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: LinkDisMode
BitField Type: R/W
BitField Desc: Link Distribution Mode - 0: Random - 1: Round Robin - 2: Strict
Priority - 3: Smart
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_lididcfg1_pen_LinkDisMode_Bit_Start                                                             0
#define cAf6_lididcfg1_pen_LinkDisMode_Bit_End                                                               2
#define cAf6_lididcfg1_pen_LinkDisMode_Mask                                                            cBit2_0
#define cAf6_lididcfg1_pen_LinkDisMode_Shift                                                                 0
#define cAf6_lididcfg1_pen_LinkDisMode_MaxVal                                                              0x7
#define cAf6_lididcfg1_pen_LinkDisMode_MinVal                                                              0x0
#define cAf6_lididcfg1_pen_LinkDisMode_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Member Bitmap of Bundle
Reg Addr   : 0x11000-0x111FF
Reg Formula: 0x11000+$bid
    Where  : 
           + bid(0-511) : Bundle ID
Reg Desc   : 
This is used to support HW engine Round-Robin and know the priority of Member
- For Round Robin mode of Link Distribution : this register is used to round robin
- For Strict mode of Link Distribution: this register is used to HW engine know the Member Priority

------------------------------------------------------------------------------*/
#define cAf6Reg_membmidcfg0_pen_Base                                                                   0x11000
#define cAf6Reg_membmidcfg0_pen(bid)                                                           (0x11000+(bid))
#define cAf6Reg_membmidcfg0_pen_WidthVal                                                                    32
#define cAf6Reg_membmidcfg0_pen_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: MemBundle_ID
BitField Type: R/W
BitField Desc: Member Bimap of Bundle
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_membmidcfg0_pen_MemBundle_ID_Bit_Start                                                          0
#define cAf6_membmidcfg0_pen_MemBundle_ID_Bit_End                                                           31
#define cAf6_membmidcfg0_pen_MemBundle_ID_Mask                                                        cBit31_0
#define cAf6_membmidcfg0_pen_MemBundle_ID_Shift                                                              0
#define cAf6_membmidcfg0_pen_MemBundle_ID_MaxVal                                                    0xffffffff
#define cAf6_membmidcfg0_pen_MemBundle_ID_MinVal                                                           0x0
#define cAf6_membmidcfg0_pen_MemBundle_ID_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Scheduler: Group priority Configuration Per Link
Reg Addr   : 0x02000-0x02BFF
Reg Formula: 0x02000 + $lid
    Where  : 
           + lid(0-3071) : Link ID
Reg Desc   : 
Used to configure the priority queue per Link.
- Support 3 group: Low Priority(LSP), High Priority(HSP), DWRR, mix mode.
- HSP: Higest Priority
- LSP: Lowest Priority
- DWRR: Medium Priority

------------------------------------------------------------------------------*/
#define cAf6Reg_schelinkicfg0_pen_Base                                                                 0x02000
#define cAf6Reg_schelinkicfg0_pen(lid)                                                         (0x02000+(lid))
#define cAf6Reg_schelinkicfg0_pen_WidthVal                                                                  32
#define cAf6Reg_schelinkicfg0_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: Hsp_grp
BitField Type: R/W
BitField Desc: HSP Queue
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_schelinkicfg0_pen_Hsp_grp_Bit_Start                                                             4
#define cAf6_schelinkicfg0_pen_Hsp_grp_Bit_End                                                               5
#define cAf6_schelinkicfg0_pen_Hsp_grp_Mask                                                            cBit5_4
#define cAf6_schelinkicfg0_pen_Hsp_grp_Shift                                                                 4
#define cAf6_schelinkicfg0_pen_Hsp_grp_MaxVal                                                              0x3
#define cAf6_schelinkicfg0_pen_Hsp_grp_MinVal                                                              0x0
#define cAf6_schelinkicfg0_pen_Hsp_grp_RstVal                                                              0x0

/*--------------------------------------
BitField Name: DWRR_grp
BitField Type: R/W
BitField Desc: DWRR Queue
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_schelinkicfg0_pen_DWRR_grp_Bit_Start                                                            2
#define cAf6_schelinkicfg0_pen_DWRR_grp_Bit_End                                                              3
#define cAf6_schelinkicfg0_pen_DWRR_grp_Mask                                                           cBit3_2
#define cAf6_schelinkicfg0_pen_DWRR_grp_Shift                                                                2
#define cAf6_schelinkicfg0_pen_DWRR_grp_MaxVal                                                             0x3
#define cAf6_schelinkicfg0_pen_DWRR_grp_MinVal                                                             0x0
#define cAf6_schelinkicfg0_pen_DWRR_grp_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Lsp_grp
BitField Type: R/W
BitField Desc: LSP Queue
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_schelinkicfg0_pen_Lsp_grp_Bit_Start                                                             0
#define cAf6_schelinkicfg0_pen_Lsp_grp_Bit_End                                                               1
#define cAf6_schelinkicfg0_pen_Lsp_grp_Mask                                                            cBit1_0
#define cAf6_schelinkicfg0_pen_Lsp_grp_Shift                                                                 0
#define cAf6_schelinkicfg0_pen_Lsp_grp_MaxVal                                                              0x3
#define cAf6_schelinkicfg0_pen_Lsp_grp_MinVal                                                              0x0
#define cAf6_schelinkicfg0_pen_Lsp_grp_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : DWRR Quantum Configuration
Reg Addr   : 0x00000-0x017FF
Reg Formula: 0x00000+$lqid
    Where  : 
           + lqid(0-6143) : Link Queue ID
Reg Desc   : 
Used to configure quantum for per Link queue in DWRR scheduling mode

------------------------------------------------------------------------------*/
#define cAf6Reg_schelinkicfg1_pen_Base                                                                 0x00000
#define cAf6Reg_schelinkicfg1_pen(lqid)                                                       (0x00000+(lqid))
#define cAf6Reg_schelinkicfg1_pen_WidthVal                                                                  32
#define cAf6Reg_schelinkicfg1_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: Linkshce_DWRR_QTum
BitField Type: R/W
BitField Desc: DWRR Quantum,Minximun MTU
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_Bit_Start                                                  0
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_Bit_End                                                   19
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_Mask                                                cBit19_0
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_Shift                                                      0
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_MaxVal                                               0xfffff
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_MinVal                                                   0x0
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Scheduler: Group priority Configuration Per Bundle
Reg Addr   : 0x06000-0x061FF
Reg Formula: 0x06000 + $bid
    Where  : 
           + bid(0-511) : Bundle ID
Reg Desc   : 
Used to configure the priority queue per Link.
- Support 3 group: Low Priority(LSP), High Priority(HSP), DWRR, mix mode.
- HSP: Higest Priority
- LSP: Lowest Priority
- DWRR: Medium Priority

------------------------------------------------------------------------------*/
#define cAf6Reg_schebundicfg0_pen_Base                                                                 0x06000
#define cAf6Reg_schebundicfg0_pen(bid)                                                         (0x06000+(bid))
#define cAf6Reg_schebundicfg0_pen_WidthVal                                                                  32
#define cAf6Reg_schebundicfg0_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: Hsp_grp
BitField Type: R/W
BitField Desc: HSP Queue
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_schebundicfg0_pen_Hsp_grp_Bit_Start                                                             8
#define cAf6_schebundicfg0_pen_Hsp_grp_Bit_End                                                              11
#define cAf6_schebundicfg0_pen_Hsp_grp_Mask                                                           cBit11_8
#define cAf6_schebundicfg0_pen_Hsp_grp_Shift                                                                 8
#define cAf6_schebundicfg0_pen_Hsp_grp_MaxVal                                                              0xf
#define cAf6_schebundicfg0_pen_Hsp_grp_MinVal                                                              0x0
#define cAf6_schebundicfg0_pen_Hsp_grp_RstVal                                                              0x0

/*--------------------------------------
BitField Name: DWRR_grp
BitField Type: R/W
BitField Desc: DWRR Queue
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_schebundicfg0_pen_DWRR_grp_Bit_Start                                                            4
#define cAf6_schebundicfg0_pen_DWRR_grp_Bit_End                                                              7
#define cAf6_schebundicfg0_pen_DWRR_grp_Mask                                                           cBit7_4
#define cAf6_schebundicfg0_pen_DWRR_grp_Shift                                                                4
#define cAf6_schebundicfg0_pen_DWRR_grp_MaxVal                                                             0xf
#define cAf6_schebundicfg0_pen_DWRR_grp_MinVal                                                             0x0
#define cAf6_schebundicfg0_pen_DWRR_grp_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Lsp_grp
BitField Type: R/W
BitField Desc: LSP Queue
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_schebundicfg0_pen_Lsp_grp_Bit_Start                                                             0
#define cAf6_schebundicfg0_pen_Lsp_grp_Bit_End                                                               3
#define cAf6_schebundicfg0_pen_Lsp_grp_Mask                                                            cBit3_0
#define cAf6_schebundicfg0_pen_Lsp_grp_Shift                                                                 0
#define cAf6_schebundicfg0_pen_Lsp_grp_MaxVal                                                              0xf
#define cAf6_schebundicfg0_pen_Lsp_grp_MinVal                                                              0x0
#define cAf6_schebundicfg0_pen_Lsp_grp_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : DWRR Quantum Configuration
Reg Addr   : 0x04000-0x051FF
Reg Formula: 0x04000+$bid
    Where  : 
           + bid(0-2047) : Bundle Queue ID
Reg Desc   : 
Used to configure quantum for per Bundle in DWRR scheduling mode

------------------------------------------------------------------------------*/
#define cAf6Reg_schebundcfg1_pen_Base                                                                  0x04000
#define cAf6Reg_schebundcfg1_pen(bid)                                                          (0x04000+(bid))
#define cAf6Reg_schebundcfg1_pen_WidthVal                                                                   32
#define cAf6Reg_schebundcfg1_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Bundshce_DWRR_QTum
BitField Type: R/W
BitField Desc: DWRR Quantum,Minximun MTU
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_Bit_Start                                                   0
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_Bit_End                                                    19
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_Mask                                                 cBit19_0
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_Shift                                                       0
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_MaxVal                                                0xfffff
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_MinVal                                                    0x0
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Scheduler: Group priority Configuration Per Vcat
Reg Addr   : 0x0A000-0x0A0FF
Reg Formula: 0x0A000 + $vid
    Where  : 
           + vid(0-255) : Vcat ID
Reg Desc   : 
Used to configure the priority queue per Link.
- Support 3 group: Low Priority(LSP), High Priority(HSP), DWRR, mix mode.
- HSP: Higest Priority
- LSP: Lowest Priority
- DWRR: Medium Priority

------------------------------------------------------------------------------*/
#define cAf6Reg_schevcaticfg0_pen_Base                                                                 0x0A000
#define cAf6Reg_schevcaticfg0_pen(vid)                                                         (0x0A000+(vid))
#define cAf6Reg_schevcaticfg0_pen_WidthVal                                                                  32
#define cAf6Reg_schevcaticfg0_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: Hsp_grp
BitField Type: R/W
BitField Desc: HSP Queue
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_schevcaticfg0_pen_Hsp_grp_Bit_Start                                                             4
#define cAf6_schevcaticfg0_pen_Hsp_grp_Bit_End                                                               5
#define cAf6_schevcaticfg0_pen_Hsp_grp_Mask                                                            cBit5_4
#define cAf6_schevcaticfg0_pen_Hsp_grp_Shift                                                                 4
#define cAf6_schevcaticfg0_pen_Hsp_grp_MaxVal                                                              0x3
#define cAf6_schevcaticfg0_pen_Hsp_grp_MinVal                                                              0x0
#define cAf6_schevcaticfg0_pen_Hsp_grp_RstVal                                                              0x0

/*--------------------------------------
BitField Name: DWRR_grp
BitField Type: R/W
BitField Desc: DWRR Queue
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_schevcaticfg0_pen_DWRR_grp_Bit_Start                                                            2
#define cAf6_schevcaticfg0_pen_DWRR_grp_Bit_End                                                              3
#define cAf6_schevcaticfg0_pen_DWRR_grp_Mask                                                           cBit3_2
#define cAf6_schevcaticfg0_pen_DWRR_grp_Shift                                                                2
#define cAf6_schevcaticfg0_pen_DWRR_grp_MaxVal                                                             0x3
#define cAf6_schevcaticfg0_pen_DWRR_grp_MinVal                                                             0x0
#define cAf6_schevcaticfg0_pen_DWRR_grp_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Lsp_grp
BitField Type: R/W
BitField Desc: LSP Queue
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_schevcaticfg0_pen_Lsp_grp_Bit_Start                                                             0
#define cAf6_schevcaticfg0_pen_Lsp_grp_Bit_End                                                               1
#define cAf6_schevcaticfg0_pen_Lsp_grp_Mask                                                            cBit1_0
#define cAf6_schevcaticfg0_pen_Lsp_grp_Shift                                                                 0
#define cAf6_schevcaticfg0_pen_Lsp_grp_MaxVal                                                              0x3
#define cAf6_schevcaticfg0_pen_Lsp_grp_MinVal                                                              0x0
#define cAf6_schevcaticfg0_pen_Lsp_grp_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : DWRR Quantum Configuration
Reg Addr   : 0x08000-0x081FF
Reg Formula: 0x08000+$vqid
    Where  : 
           + vqid(0-511) : Vcat Queue ID
Reg Desc   : 
Used to configure quantum for per Vcat Queue in DWRR scheduling mode

------------------------------------------------------------------------------*/
#define cAf6Reg_schevcatcfg1_pen_Base                                                                  0x08000
#define cAf6Reg_schevcatcfg1_pen(vqid)                                                        (0x08000+(vqid))
#define cAf6Reg_schevcatcfg1_pen_WidthVal                                                                   32
#define cAf6Reg_schevcatcfg1_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Vcatshce_DWRR_QTum
BitField Type: R/W
BitField Desc: DWRR Quantum,Minximun MTU
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_schevcatcfg1_pen_Vcatshce_DWRR_QTum_Bit_Start                                                   0
#define cAf6_schevcatcfg1_pen_Vcatshce_DWRR_QTum_Bit_End                                                    19
#define cAf6_schevcatcfg1_pen_Vcatshce_DWRR_QTum_Mask                                                 cBit19_0
#define cAf6_schevcatcfg1_pen_Vcatshce_DWRR_QTum_Shift                                                       0
#define cAf6_schevcatcfg1_pen_Vcatshce_DWRR_QTum_MaxVal                                                0xfffff
#define cAf6_schevcatcfg1_pen_Vcatshce_DWRR_QTum_MinVal                                                    0x0
#define cAf6_schevcatcfg1_pen_Vcatshce_DWRR_QTum_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Scheduler: Group priority Configuration Per Port
Reg Addr   : 0x16000-0x1600F
Reg Formula: 0x16000 + $eid
    Where  : 
           + eid(0-15) : Ethernet ID
Reg Desc   : 
Used to configure the priority queue per Link.
- Support 3 group: Low Priority(LSP), High Priority(HSP), DWRR, mix mode.
- HSP: Higest Priority
- LSP: Lowest Priority
- DWRR: Medium Priority
- 0-3:reserved
- 4:eXAUI#0
- 5:eXAUI#1

------------------------------------------------------------------------------*/
#define cAf6Reg_scheetheicfg0_pen_Base                                                                 0x16000
#define cAf6Reg_scheetheicfg0_pen(eid)                                                         (0x16000+(eid))
#define cAf6Reg_scheetheicfg0_pen_WidthVal                                                                  32
#define cAf6Reg_scheetheicfg0_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: Hsp_grp
BitField Type: R/W
BitField Desc: HSP Queue
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_scheetheicfg0_pen_Hsp_grp_Bit_Start                                                             4
#define cAf6_scheetheicfg0_pen_Hsp_grp_Bit_End                                                               5
#define cAf6_scheetheicfg0_pen_Hsp_grp_Mask                                                            cBit5_4
#define cAf6_scheetheicfg0_pen_Hsp_grp_Shift                                                                 4
#define cAf6_scheetheicfg0_pen_Hsp_grp_MaxVal                                                              0x3
#define cAf6_scheetheicfg0_pen_Hsp_grp_MinVal                                                              0x0
#define cAf6_scheetheicfg0_pen_Hsp_grp_RstVal                                                              0x0

/*--------------------------------------
BitField Name: DWRR_grp
BitField Type: R/W
BitField Desc: DWRR Queue
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_scheetheicfg0_pen_DWRR_grp_Bit_Start                                                            2
#define cAf6_scheetheicfg0_pen_DWRR_grp_Bit_End                                                              3
#define cAf6_scheetheicfg0_pen_DWRR_grp_Mask                                                           cBit3_2
#define cAf6_scheetheicfg0_pen_DWRR_grp_Shift                                                                2
#define cAf6_scheetheicfg0_pen_DWRR_grp_MaxVal                                                             0x3
#define cAf6_scheetheicfg0_pen_DWRR_grp_MinVal                                                             0x0
#define cAf6_scheetheicfg0_pen_DWRR_grp_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Lsp_grp
BitField Type: R/W
BitField Desc: LSP Queue
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_scheetheicfg0_pen_Lsp_grp_Bit_Start                                                             0
#define cAf6_scheetheicfg0_pen_Lsp_grp_Bit_End                                                               1
#define cAf6_scheetheicfg0_pen_Lsp_grp_Mask                                                            cBit1_0
#define cAf6_scheetheicfg0_pen_Lsp_grp_Shift                                                                 0
#define cAf6_scheetheicfg0_pen_Lsp_grp_MaxVal                                                              0x3
#define cAf6_scheetheicfg0_pen_Lsp_grp_MinVal                                                              0x0
#define cAf6_scheetheicfg0_pen_Lsp_grp_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : DWRR Quantum Configuration
Reg Addr   : 0x14000-0x1400F
Reg Formula: 0x14000+$eqid
    Where  : 
           + eqid(0-15) : Ethernet Queue ID
Reg Desc   : 
Used to configure quantum for per Ethernet Queue in DWRR scheduling mode

------------------------------------------------------------------------------*/
#define cAf6Reg_scheethercfg1_pen_Base                                                                 0x14000
#define cAf6Reg_scheethercfg1_pen(eqid)                                                       (0x14000+(eqid))
#define cAf6Reg_scheethercfg1_pen_WidthVal                                                                  32
#define cAf6Reg_scheethercfg1_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: Ethershce_DWRR_QTum
BitField Type: R/W
BitField Desc: DWRR Quantum,Minximun MTU
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_scheethercfg1_pen_Ethershce_DWRR_QTum_Bit_Start                                                 0
#define cAf6_scheethercfg1_pen_Ethershce_DWRR_QTum_Bit_End                                                  19
#define cAf6_scheethercfg1_pen_Ethershce_DWRR_QTum_Mask                                               cBit19_0
#define cAf6_scheethercfg1_pen_Ethershce_DWRR_QTum_Shift                                                     0
#define cAf6_scheethercfg1_pen_Ethershce_DWRR_QTum_MaxVal                                              0xfffff
#define cAf6_scheethercfg1_pen_Ethershce_DWRR_QTum_MinVal                                                  0x0
#define cAf6_scheethercfg1_pen_Ethershce_DWRR_QTum_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Stick 0
Reg Addr   : 0x40010
Reg Formula:
    Where  :
Reg Desc   :
Used to debug MPEG

------------------------------------------------------------------------------*/
#define cAf6Reg_stk0_pen_Base                                                                          0x40010
#define cAf6Reg_stk0_pen                                                                               0x40010
#define cAf6Reg_stk0_pen_WidthVal                                                                           32
#define cAf6Reg_stk0_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: MPEG_Ready
BitField Type: R/W
BitField Desc: MPEG Ready for PDA Reading
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_stk0_pen_MPEG_Ready_Bit_Start                                                                  31
#define cAf6_stk0_pen_MPEG_Ready_Bit_End                                                                    31
#define cAf6_stk0_pen_MPEG_Ready_Mask                                                                   cBit31
#define cAf6_stk0_pen_MPEG_Ready_Shift                                                                      31
#define cAf6_stk0_pen_MPEG_Ready_MaxVal                                                                    0x1
#define cAf6_stk0_pen_MPEG_Ready_MinVal                                                                    0x0
#define cAf6_stk0_pen_MPEG_Ready_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: CLA_Write
BitField Type: R/W
BitField Desc: CLA Enqueue MPEG
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_stk0_pen_CLA_Write_Bit_Start                                                                   30
#define cAf6_stk0_pen_CLA_Write_Bit_End                                                                     30
#define cAf6_stk0_pen_CLA_Write_Mask                                                                    cBit30
#define cAf6_stk0_pen_CLA_Write_Shift                                                                       30
#define cAf6_stk0_pen_CLA_Write_MaxVal                                                                     0x1
#define cAf6_stk0_pen_CLA_Write_MinVal                                                                     0x0
#define cAf6_stk0_pen_CLA_Write_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: PDA_Request
BitField Type: R/W
BitField Desc: PDA request Read
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_stk0_pen_PDA_Request_Bit_Start                                                                 29
#define cAf6_stk0_pen_PDA_Request_Bit_End                                                                   29
#define cAf6_stk0_pen_PDA_Request_Mask                                                                  cBit29
#define cAf6_stk0_pen_PDA_Request_Shift                                                                     29
#define cAf6_stk0_pen_PDA_Request_MaxVal                                                                   0x1
#define cAf6_stk0_pen_PDA_Request_MinVal                                                                   0x0
#define cAf6_stk0_pen_PDA_Request_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: MPEG_ACK
BitField Type: R/W
BitField Desc: MPEG return ACK to PDA
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_stk0_pen_MPEG_ACK_Bit_Start                                                                    28
#define cAf6_stk0_pen_MPEG_ACK_Bit_End                                                                      28
#define cAf6_stk0_pen_MPEG_ACK_Mask                                                                     cBit28
#define cAf6_stk0_pen_MPEG_ACK_Shift                                                                        28
#define cAf6_stk0_pen_MPEG_ACK_MaxVal                                                                      0x1
#define cAf6_stk0_pen_MPEG_ACK_MinVal                                                                      0x0
#define cAf6_stk0_pen_MPEG_ACK_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: External_Sta_err
BitField Type: R/W
BitField Desc: External Read/Write Status Fail
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_stk0_pen_External_Sta_err_Bit_Start                                                            24
#define cAf6_stk0_pen_External_Sta_err_Bit_End                                                              24
#define cAf6_stk0_pen_External_Sta_err_Mask                                                             cBit24
#define cAf6_stk0_pen_External_Sta_err_Shift                                                                24
#define cAf6_stk0_pen_External_Sta_err_MaxVal                                                              0x1
#define cAf6_stk0_pen_External_Sta_err_MinVal                                                              0x0
#define cAf6_stk0_pen_External_Sta_err_RstVal                                                              0x0

/*--------------------------------------
BitField Name: External_Write_Blk_Empty
BitField Type: R/W
BitField Desc: External Write but Block Empty
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_stk0_pen_External_Write_Blk_Empty_Bit_Start                                                    23
#define cAf6_stk0_pen_External_Write_Blk_Empty_Bit_End                                                      23
#define cAf6_stk0_pen_External_Write_Blk_Empty_Mask                                                     cBit23
#define cAf6_stk0_pen_External_Write_Blk_Empty_Shift                                                        23
#define cAf6_stk0_pen_External_Write_Blk_Empty_MaxVal                                                      0x1
#define cAf6_stk0_pen_External_Write_Blk_Empty_MinVal                                                      0x0
#define cAf6_stk0_pen_External_Write_Blk_Empty_RstVal                                                      0x0

/*--------------------------------------
BitField Name: External_Write_Blk_Dis
BitField Type: R/W
BitField Desc: External Write but Block Disable
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_stk0_pen_External_Write_Blk_Dis_Bit_Start                                                      22
#define cAf6_stk0_pen_External_Write_Blk_Dis_Bit_End                                                        22
#define cAf6_stk0_pen_External_Write_Blk_Dis_Mask                                                       cBit22
#define cAf6_stk0_pen_External_Write_Blk_Dis_Shift                                                          22
#define cAf6_stk0_pen_External_Write_Blk_Dis_MaxVal                                                        0x1
#define cAf6_stk0_pen_External_Write_Blk_Dis_MinVal                                                        0x0
#define cAf6_stk0_pen_External_Write_Blk_Dis_RstVal                                                        0x0

/*--------------------------------------
BitField Name: External_Write_Buffer_Full
BitField Type: R/W
BitField Desc: External Write DDR valid return but buffer full
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_stk0_pen_External_Write_Buffer_Full_Bit_Start                                                  21
#define cAf6_stk0_pen_External_Write_Buffer_Full_Bit_End                                                    21
#define cAf6_stk0_pen_External_Write_Buffer_Full_Mask                                                   cBit21
#define cAf6_stk0_pen_External_Write_Buffer_Full_Shift                                                      21
#define cAf6_stk0_pen_External_Write_Buffer_Full_MaxVal                                                    0x1
#define cAf6_stk0_pen_External_Write_Buffer_Full_MinVal                                                    0x0
#define cAf6_stk0_pen_External_Write_Buffer_Full_RstVal                                                    0x0

/*--------------------------------------
BitField Name: External_Read_Pending_Full
BitField Type: R/W
BitField Desc: External Read Pending Full
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_stk0_pen_External_Read_Pending_Full_Bit_Start                                                  20
#define cAf6_stk0_pen_External_Read_Pending_Full_Bit_End                                                    20
#define cAf6_stk0_pen_External_Read_Pending_Full_Mask                                                   cBit20
#define cAf6_stk0_pen_External_Read_Pending_Full_Shift                                                      20
#define cAf6_stk0_pen_External_Read_Pending_Full_MaxVal                                                    0x1
#define cAf6_stk0_pen_External_Read_Pending_Full_MinVal                                                    0x0
#define cAf6_stk0_pen_External_Read_Pending_Full_RstVal                                                    0x0

/*--------------------------------------
BitField Name: External_Write_Pending_Full
BitField Type: R/W
BitField Desc: External Write Pending Full
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_stk0_pen_External_Write_Pending_Full_Bit_Start                                                 19
#define cAf6_stk0_pen_External_Write_Pending_Full_Bit_End                                                   19
#define cAf6_stk0_pen_External_Write_Pending_Full_Mask                                                  cBit19
#define cAf6_stk0_pen_External_Write_Pending_Full_Shift                                                     19
#define cAf6_stk0_pen_External_Write_Pending_Full_MaxVal                                                   0x1
#define cAf6_stk0_pen_External_Write_Pending_Full_MinVal                                                   0x0
#define cAf6_stk0_pen_External_Write_Pending_Full_RstVal                                                   0x0

/*--------------------------------------
BitField Name: External_Blk_Empty
BitField Type: R/W
BitField Desc: External Block Empty
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_stk0_pen_External_Blk_Empty_Bit_Start                                                          18
#define cAf6_stk0_pen_External_Blk_Empty_Bit_End                                                            18
#define cAf6_stk0_pen_External_Blk_Empty_Mask                                                           cBit18
#define cAf6_stk0_pen_External_Blk_Empty_Shift                                                              18
#define cAf6_stk0_pen_External_Blk_Empty_MaxVal                                                            0x1
#define cAf6_stk0_pen_External_Blk_Empty_MinVal                                                            0x0
#define cAf6_stk0_pen_External_Blk_Empty_RstVal                                                            0x0

/*--------------------------------------
BitField Name: External_Blk_Error
BitField Type: R/W
BitField Desc: External Block Error
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_stk0_pen_External_Blk_Error_Bit_Start                                                          17
#define cAf6_stk0_pen_External_Blk_Error_Bit_End                                                            17
#define cAf6_stk0_pen_External_Blk_Error_Mask                                                           cBit17
#define cAf6_stk0_pen_External_Blk_Error_Shift                                                              17
#define cAf6_stk0_pen_External_Blk_Error_MaxVal                                                            0x1
#define cAf6_stk0_pen_External_Blk_Error_MinVal                                                            0x0
#define cAf6_stk0_pen_External_Blk_Error_RstVal                                                            0x0

/*--------------------------------------
BitField Name: External_Blk_Same
BitField Type: R/W
BitField Desc: External Block Same
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_stk0_pen_External_Blk_Same_Bit_Start                                                           16
#define cAf6_stk0_pen_External_Blk_Same_Bit_End                                                             16
#define cAf6_stk0_pen_External_Blk_Same_Mask                                                            cBit16
#define cAf6_stk0_pen_External_Blk_Same_Shift                                                               16
#define cAf6_stk0_pen_External_Blk_Same_MaxVal                                                             0x1
#define cAf6_stk0_pen_External_Blk_Same_MinVal                                                             0x0
#define cAf6_stk0_pen_External_Blk_Same_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Discard_Queue_Disable
BitField Type: R/W
BitField Desc: Discard because Queue CFG Disable
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_stk0_pen_Discard_Queue_Disable_Bit_Start                                                       15
#define cAf6_stk0_pen_Discard_Queue_Disable_Bit_End                                                         15
#define cAf6_stk0_pen_Discard_Queue_Disable_Mask                                                        cBit15
#define cAf6_stk0_pen_Discard_Queue_Disable_Shift                                                           15
#define cAf6_stk0_pen_Discard_Queue_Disable_MaxVal                                                         0x1
#define cAf6_stk0_pen_Discard_Queue_Disable_MinVal                                                         0x0
#define cAf6_stk0_pen_Discard_Queue_Disable_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Discard_CLA_Drop
BitField Type: R/W
BitField Desc: Discard because CLA declare Drop
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_stk0_pen_Discard_CLA_Drop_Bit_Start                                                            14
#define cAf6_stk0_pen_Discard_CLA_Drop_Bit_End                                                              14
#define cAf6_stk0_pen_Discard_CLA_Drop_Mask                                                             cBit14
#define cAf6_stk0_pen_Discard_CLA_Drop_Shift                                                                14
#define cAf6_stk0_pen_Discard_CLA_Drop_MaxVal                                                              0x1
#define cAf6_stk0_pen_Discard_CLA_Drop_MinVal                                                              0x0
#define cAf6_stk0_pen_Discard_CLA_Drop_RstVal                                                              0x0

/*--------------------------------------
BitField Name: Discard_Internal_FiFO
BitField Type: R/W
BitField Desc: Discard because internal FiFO full
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_stk0_pen_Discard_Internal_FiFO_Bit_Start                                                       13
#define cAf6_stk0_pen_Discard_Internal_FiFO_Bit_End                                                         13
#define cAf6_stk0_pen_Discard_Internal_FiFO_Mask                                                        cBit13
#define cAf6_stk0_pen_Discard_Internal_FiFO_Shift                                                           13
#define cAf6_stk0_pen_Discard_Internal_FiFO_MaxVal                                                         0x1
#define cAf6_stk0_pen_Discard_Internal_FiFO_MinVal                                                         0x0
#define cAf6_stk0_pen_Discard_Internal_FiFO_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Discard_External_FiFO
BitField Type: R/W
BitField Desc: Discard because external FiFO full
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_stk0_pen_Discard_External_FiFO_Bit_Start                                                       12
#define cAf6_stk0_pen_Discard_External_FiFO_Bit_End                                                         12
#define cAf6_stk0_pen_Discard_External_FiFO_Mask                                                        cBit12
#define cAf6_stk0_pen_Discard_External_FiFO_Shift                                                           12
#define cAf6_stk0_pen_Discard_External_FiFO_MaxVal                                                         0x1
#define cAf6_stk0_pen_Discard_External_FiFO_MinVal                                                         0x0
#define cAf6_stk0_pen_Discard_External_FiFO_RstVal                                                         0x0

/*--------------------------------------
BitField Name: External_FiFo_Engine_conflict
BitField Type: R/W
BitField Desc: External FiFo Engine Conflict
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_stk0_pen_External_FiFo_Engine_conflict_Bit_Start                                               11
#define cAf6_stk0_pen_External_FiFo_Engine_conflict_Bit_End                                                 11
#define cAf6_stk0_pen_External_FiFo_Engine_conflict_Mask                                                cBit11
#define cAf6_stk0_pen_External_FiFo_Engine_conflict_Shift                                                   11
#define cAf6_stk0_pen_External_FiFo_Engine_conflict_MaxVal                                                 0x1
#define cAf6_stk0_pen_External_FiFo_Engine_conflict_MinVal                                                 0x0
#define cAf6_stk0_pen_External_FiFo_Engine_conflict_RstVal                                                 0x0

/*--------------------------------------
BitField Name: Internal_FiFO_fail
BitField Type: R/W
BitField Desc: Internal FiFo Fail
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_stk0_pen_Internal_FiFO_fail_Bit_Start                                                          10
#define cAf6_stk0_pen_Internal_FiFO_fail_Bit_End                                                            10
#define cAf6_stk0_pen_Internal_FiFO_fail_Mask                                                           cBit10
#define cAf6_stk0_pen_Internal_FiFO_fail_Shift                                                              10
#define cAf6_stk0_pen_Internal_FiFO_fail_MaxVal                                                            0x1
#define cAf6_stk0_pen_Internal_FiFO_fail_MinVal                                                            0x0
#define cAf6_stk0_pen_Internal_FiFO_fail_RstVal                                                            0x0

/*--------------------------------------
BitField Name: Buffer_Write_Full_Thres
BitField Type: R/W
BitField Desc: Buffer Write Full Threshold CFG
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_stk0_pen_Buffer_Write_Full_Thres_Bit_Start                                                      9
#define cAf6_stk0_pen_Buffer_Write_Full_Thres_Bit_End                                                        9
#define cAf6_stk0_pen_Buffer_Write_Full_Thres_Mask                                                       cBit9
#define cAf6_stk0_pen_Buffer_Write_Full_Thres_Shift                                                          9
#define cAf6_stk0_pen_Buffer_Write_Full_Thres_MaxVal                                                       0x1
#define cAf6_stk0_pen_Buffer_Write_Full_Thres_MinVal                                                       0x0
#define cAf6_stk0_pen_Buffer_Write_Full_Thres_RstVal                                                       0x0

/*--------------------------------------
BitField Name: Buffer_Write_cache_full
BitField Type: R/W
BitField Desc: Buffer Write Cache Full
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_stk0_pen_Buffer_Write_cache_full_Bit_Start                                                      8
#define cAf6_stk0_pen_Buffer_Write_cache_full_Bit_End                                                        8
#define cAf6_stk0_pen_Buffer_Write_cache_full_Mask                                                       cBit8
#define cAf6_stk0_pen_Buffer_Write_cache_full_Shift                                                          8
#define cAf6_stk0_pen_Buffer_Write_cache_full_MaxVal                                                       0x1
#define cAf6_stk0_pen_Buffer_Write_cache_full_MinVal                                                       0x0
#define cAf6_stk0_pen_Buffer_Write_cache_full_RstVal                                                       0x0

/*--------------------------------------
BitField Name: Internal_FiFo_Engine_conflict
BitField Type: R/W
BitField Desc: Internal FiFo Engine Conflict
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_stk0_pen_Internal_FiFo_Engine_conflict_Bit_Start                                                7
#define cAf6_stk0_pen_Internal_FiFo_Engine_conflict_Bit_End                                                  7
#define cAf6_stk0_pen_Internal_FiFo_Engine_conflict_Mask                                                 cBit7
#define cAf6_stk0_pen_Internal_FiFo_Engine_conflict_Shift                                                    7
#define cAf6_stk0_pen_Internal_FiFo_Engine_conflict_MaxVal                                                 0x1
#define cAf6_stk0_pen_Internal_FiFo_Engine_conflict_MinVal                                                 0x0
#define cAf6_stk0_pen_Internal_FiFo_Engine_conflict_RstVal                                                 0x0

/*--------------------------------------
BitField Name: Buffer_External_Full
BitField Type: R/W
BitField Desc: External Buffer Full
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_stk0_pen_Buffer_External_Full_Bit_Start                                                         6
#define cAf6_stk0_pen_Buffer_External_Full_Bit_End                                                           6
#define cAf6_stk0_pen_Buffer_External_Full_Mask                                                          cBit6
#define cAf6_stk0_pen_Buffer_External_Full_Shift                                                             6
#define cAf6_stk0_pen_Buffer_External_Full_MaxVal                                                          0x1
#define cAf6_stk0_pen_Buffer_External_Full_MinVal                                                          0x0
#define cAf6_stk0_pen_Buffer_External_Full_RstVal                                                          0x0

/*--------------------------------------
BitField Name: Internal_FiFO_write_Error
BitField Type: R/W
BitField Desc: Internal FiFo write Error
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_stk0_pen_Internal_FiFO_write_Error_Bit_Start                                                    4
#define cAf6_stk0_pen_Internal_FiFO_write_Error_Bit_End                                                      4
#define cAf6_stk0_pen_Internal_FiFO_write_Error_Mask                                                     cBit4
#define cAf6_stk0_pen_Internal_FiFO_write_Error_Shift                                                        4
#define cAf6_stk0_pen_Internal_FiFO_write_Error_MaxVal                                                     0x1
#define cAf6_stk0_pen_Internal_FiFO_write_Error_MinVal                                                     0x0
#define cAf6_stk0_pen_Internal_FiFO_write_Error_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Internal_FiFo_full
BitField Type: R/W
BitField Desc: Internal FiFo full
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_stk0_pen_Internal_FiFo_full_Bit_Start                                                           3
#define cAf6_stk0_pen_Internal_FiFo_full_Bit_End                                                             3
#define cAf6_stk0_pen_Internal_FiFo_full_Mask                                                            cBit3
#define cAf6_stk0_pen_Internal_FiFo_full_Shift                                                               3
#define cAf6_stk0_pen_Internal_FiFo_full_MaxVal                                                            0x1
#define cAf6_stk0_pen_Internal_FiFo_full_MinVal                                                            0x0
#define cAf6_stk0_pen_Internal_FiFo_full_RstVal                                                            0x0

/*--------------------------------------
BitField Name: Internal_FiFO_write_error
BitField Type: R/W
BitField Desc: Internal FiFo write error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_stk0_pen_Internal_FiFO_write_error_Bit_Start                                                    2
#define cAf6_stk0_pen_Internal_FiFO_write_error_Bit_End                                                      2
#define cAf6_stk0_pen_Internal_FiFO_write_error_Mask                                                     cBit2
#define cAf6_stk0_pen_Internal_FiFO_write_error_Shift                                                        2
#define cAf6_stk0_pen_Internal_FiFO_write_error_MaxVal                                                     0x1
#define cAf6_stk0_pen_Internal_FiFO_write_error_MinVal                                                     0x0
#define cAf6_stk0_pen_Internal_FiFO_write_error_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Enqueue_Full
BitField Type: R/W
BitField Desc: Enqueue Full
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_stk0_pen_Enqueue_Full_Bit_Start                                                                 1
#define cAf6_stk0_pen_Enqueue_Full_Bit_End                                                                   1
#define cAf6_stk0_pen_Enqueue_Full_Mask                                                                  cBit1
#define cAf6_stk0_pen_Enqueue_Full_Shift                                                                     1
#define cAf6_stk0_pen_Enqueue_Full_MaxVal                                                                  0x1
#define cAf6_stk0_pen_Enqueue_Full_MinVal                                                                  0x0
#define cAf6_stk0_pen_Enqueue_Full_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: Internal_FiFo_read_error
BitField Type: R/W
BitField Desc: Internal_FiFo_read_error
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_stk0_pen_Internal_FiFo_read_error_Bit_Start                                                     0
#define cAf6_stk0_pen_Internal_FiFo_read_error_Bit_End                                                       0
#define cAf6_stk0_pen_Internal_FiFo_read_error_Mask                                                      cBit0
#define cAf6_stk0_pen_Internal_FiFo_read_error_Shift                                                         0
#define cAf6_stk0_pen_Internal_FiFo_read_error_MaxVal                                                      0x1
#define cAf6_stk0_pen_Internal_FiFo_read_error_MinVal                                                      0x0
#define cAf6_stk0_pen_Internal_FiFo_read_error_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Stick 1
Reg Addr   : 0x40011
Reg Formula:
    Where  :
Reg Desc   :
Used to debug MPEG

------------------------------------------------------------------------------*/
#define cAf6Reg_stk1_pen_Base                                                                          0x40011
#define cAf6Reg_stk1_pen                                                                               0x40011
#define cAf6Reg_stk1_pen_WidthVal                                                                           32
#define cAf6Reg_stk1_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: MPEG_Valid_to_PDA
BitField Type: R/W
BitField Desc: MPEG return Valid to PDA
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_stk1_pen_MPEG_Valid_to_PDA_Bit_Start                                                           24
#define cAf6_stk1_pen_MPEG_Valid_to_PDA_Bit_End                                                             24
#define cAf6_stk1_pen_MPEG_Valid_to_PDA_Mask                                                            cBit24
#define cAf6_stk1_pen_MPEG_Valid_to_PDA_Shift                                                               24
#define cAf6_stk1_pen_MPEG_Valid_to_PDA_MaxVal                                                             0x1
#define cAf6_stk1_pen_MPEG_Valid_to_PDA_MinVal                                                             0x0
#define cAf6_stk1_pen_MPEG_Valid_to_PDA_RstVal                                                             0x0

/*--------------------------------------
BitField Name: FiFo_Req_DDR_Write_err
BitField Type: R/W
BitField Desc: FiFo Request ACK DDR Write Error
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_stk1_pen_FiFo_Req_DDR_Write_err_Bit_Start                                                      23
#define cAf6_stk1_pen_FiFo_Req_DDR_Write_err_Bit_End                                                        23
#define cAf6_stk1_pen_FiFo_Req_DDR_Write_err_Mask                                                       cBit23
#define cAf6_stk1_pen_FiFo_Req_DDR_Write_err_Shift                                                          23
#define cAf6_stk1_pen_FiFo_Req_DDR_Write_err_MaxVal                                                        0x1
#define cAf6_stk1_pen_FiFo_Req_DDR_Write_err_MinVal                                                        0x0
#define cAf6_stk1_pen_FiFo_Req_DDR_Write_err_RstVal                                                        0x0

/*--------------------------------------
BitField Name: FiFo_Req_DDR_Read_err
BitField Type: R/W
BitField Desc: FiFo Request ACK DDR Read Error
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_stk1_pen_FiFo_Req_DDR_Read_err_Bit_Start                                                       22
#define cAf6_stk1_pen_FiFo_Req_DDR_Read_err_Bit_End                                                         22
#define cAf6_stk1_pen_FiFo_Req_DDR_Read_err_Mask                                                        cBit22
#define cAf6_stk1_pen_FiFo_Req_DDR_Read_err_Shift                                                           22
#define cAf6_stk1_pen_FiFo_Req_DDR_Read_err_MaxVal                                                         0x1
#define cAf6_stk1_pen_FiFo_Req_DDR_Read_err_MinVal                                                         0x0
#define cAf6_stk1_pen_FiFo_Req_DDR_Read_err_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PDA_Discard_Full
BitField Type: R/W
BitField Desc: PDA discard Full
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_stk1_pen_PDA_Discard_Full_Bit_Start                                                            21
#define cAf6_stk1_pen_PDA_Discard_Full_Bit_End                                                              21
#define cAf6_stk1_pen_PDA_Discard_Full_Mask                                                             cBit21
#define cAf6_stk1_pen_PDA_Discard_Full_Shift                                                                21
#define cAf6_stk1_pen_PDA_Discard_Full_MaxVal                                                              0x1
#define cAf6_stk1_pen_PDA_Discard_Full_MinVal                                                              0x0
#define cAf6_stk1_pen_PDA_Discard_Full_RstVal                                                              0x0

/*--------------------------------------
BitField Name: FiFo_Req_CLA_Write_err
BitField Type: R/W
BitField Desc: FiFo Request ACK CLA Write Error
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_stk1_pen_FiFo_Req_CLA_Write_err_Bit_Start                                                      19
#define cAf6_stk1_pen_FiFo_Req_CLA_Write_err_Bit_End                                                        19
#define cAf6_stk1_pen_FiFo_Req_CLA_Write_err_Mask                                                       cBit19
#define cAf6_stk1_pen_FiFo_Req_CLA_Write_err_Shift                                                          19
#define cAf6_stk1_pen_FiFo_Req_CLA_Write_err_MaxVal                                                        0x1
#define cAf6_stk1_pen_FiFo_Req_CLA_Write_err_MinVal                                                        0x0
#define cAf6_stk1_pen_FiFo_Req_CLA_Write_err_RstVal                                                        0x0

/*--------------------------------------
BitField Name: FiFo_Req_CLA_Read_err
BitField Type: R/W
BitField Desc: FiFo Request ACK CLA Read Error
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_stk1_pen_FiFo_Req_CLA_Read_err_Bit_Start                                                       18
#define cAf6_stk1_pen_FiFo_Req_CLA_Read_err_Bit_End                                                         18
#define cAf6_stk1_pen_FiFo_Req_CLA_Read_err_Mask                                                        cBit18
#define cAf6_stk1_pen_FiFo_Req_CLA_Read_err_Shift                                                           18
#define cAf6_stk1_pen_FiFo_Req_CLA_Read_err_MaxVal                                                         0x1
#define cAf6_stk1_pen_FiFo_Req_CLA_Read_err_MinVal                                                         0x0
#define cAf6_stk1_pen_FiFo_Req_CLA_Read_err_RstVal                                                         0x0

/*--------------------------------------
BitField Name: FiFo_Valid_DDR_Write_err
BitField Type: R/W
BitField Desc: FiFo Valid DDR Write Error
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_stk1_pen_FiFo_Valid_DDR_Write_err_Bit_Start                                                    17
#define cAf6_stk1_pen_FiFo_Valid_DDR_Write_err_Bit_End                                                      17
#define cAf6_stk1_pen_FiFo_Valid_DDR_Write_err_Mask                                                     cBit17
#define cAf6_stk1_pen_FiFo_Valid_DDR_Write_err_Shift                                                        17
#define cAf6_stk1_pen_FiFo_Valid_DDR_Write_err_MaxVal                                                      0x1
#define cAf6_stk1_pen_FiFo_Valid_DDR_Write_err_MinVal                                                      0x0
#define cAf6_stk1_pen_FiFo_Valid_DDR_Write_err_RstVal                                                      0x0

/*--------------------------------------
BitField Name: FiFo_Valid_DDR_Read_err
BitField Type: R/W
BitField Desc: FiFo Valid DDR Read Error
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_stk1_pen_FiFo_Valid_DDR_Read_err_Bit_Start                                                     16
#define cAf6_stk1_pen_FiFo_Valid_DDR_Read_err_Bit_End                                                       16
#define cAf6_stk1_pen_FiFo_Valid_DDR_Read_err_Mask                                                      cBit16
#define cAf6_stk1_pen_FiFo_Valid_DDR_Read_err_Shift                                                         16
#define cAf6_stk1_pen_FiFo_Valid_DDR_Read_err_MaxVal                                                       0x1
#define cAf6_stk1_pen_FiFo_Valid_DDR_Read_err_MinVal                                                       0x0
#define cAf6_stk1_pen_FiFo_Valid_DDR_Read_err_RstVal                                                       0x0

/*--------------------------------------
BitField Name: QMXFF_request_write_DDR
BitField Type: R/W
BitField Desc: QMXFF request write DDR
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_stk1_pen_QMXFF_request_write_DDR_Bit_Start                                                     15
#define cAf6_stk1_pen_QMXFF_request_write_DDR_Bit_End                                                       15
#define cAf6_stk1_pen_QMXFF_request_write_DDR_Mask                                                      cBit15
#define cAf6_stk1_pen_QMXFF_request_write_DDR_Shift                                                         15
#define cAf6_stk1_pen_QMXFF_request_write_DDR_MaxVal                                                       0x1
#define cAf6_stk1_pen_QMXFF_request_write_DDR_MinVal                                                       0x0
#define cAf6_stk1_pen_QMXFF_request_write_DDR_RstVal                                                       0x0

/*--------------------------------------
BitField Name: QMXFF_request_Read_DDR
BitField Type: R/W
BitField Desc: QMXFF request Read DDR
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_stk1_pen_QMXFF_request_Read_DDR_Bit_Start                                                      14
#define cAf6_stk1_pen_QMXFF_request_Read_DDR_Bit_End                                                        14
#define cAf6_stk1_pen_QMXFF_request_Read_DDR_Mask                                                       cBit14
#define cAf6_stk1_pen_QMXFF_request_Read_DDR_Shift                                                          14
#define cAf6_stk1_pen_QMXFF_request_Read_DDR_MaxVal                                                        0x1
#define cAf6_stk1_pen_QMXFF_request_Read_DDR_MinVal                                                        0x0
#define cAf6_stk1_pen_QMXFF_request_Read_DDR_RstVal                                                        0x0

/*--------------------------------------
BitField Name: QMXFF_Receive_DDR_ACK
BitField Type: R/W
BitField Desc: QMXFF receive DDR ACK
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_stk1_pen_QMXFF_Receive_DDR_ACK_Bit_Start                                                       13
#define cAf6_stk1_pen_QMXFF_Receive_DDR_ACK_Bit_End                                                         13
#define cAf6_stk1_pen_QMXFF_Receive_DDR_ACK_Mask                                                        cBit13
#define cAf6_stk1_pen_QMXFF_Receive_DDR_ACK_Shift                                                           13
#define cAf6_stk1_pen_QMXFF_Receive_DDR_ACK_MaxVal                                                         0x1
#define cAf6_stk1_pen_QMXFF_Receive_DDR_ACK_MinVal                                                         0x0
#define cAf6_stk1_pen_QMXFF_Receive_DDR_ACK_RstVal                                                         0x0

/*--------------------------------------
BitField Name: QMXFF_Receive_DDR_Valid_Write
BitField Type: R/W
BitField Desc: QMXFF receive DDR Write Valid
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Write_Bit_Start                                               12
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Write_Bit_End                                                 12
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Write_Mask                                                cBit12
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Write_Shift                                                   12
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Write_MaxVal                                                 0x1
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Write_MinVal                                                 0x0
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Write_RstVal                                                 0x0

/*--------------------------------------
BitField Name: QMXFF_Receive_DDR_Valid_Read
BitField Type: R/W
BitField Desc: QMXFF receive DDR Read Valid
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Read_Bit_Start                                                 8
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Read_Bit_End                                                   8
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Read_Mask                                                  cBit8
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Read_Shift                                                     8
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Read_MaxVal                                                  0x1
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Read_MinVal                                                  0x0
#define cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Read_RstVal                                                  0x0

/*--------------------------------------
BitField Name: Internal_FiFO_Engine_Error
BitField Type: R/W
BitField Desc: Internal FiFo Engine Error
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_stk1_pen_Internal_FiFO_Engine_Error_Bit_Start                                                   4
#define cAf6_stk1_pen_Internal_FiFO_Engine_Error_Bit_End                                                     4
#define cAf6_stk1_pen_Internal_FiFO_Engine_Error_Mask                                                    cBit4
#define cAf6_stk1_pen_Internal_FiFO_Engine_Error_Shift                                                       4
#define cAf6_stk1_pen_Internal_FiFO_Engine_Error_MaxVal                                                    0x1
#define cAf6_stk1_pen_Internal_FiFO_Engine_Error_MinVal                                                    0x0
#define cAf6_stk1_pen_Internal_FiFO_Engine_Error_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Internal_FiFo_Write_Err
BitField Type: R/W
BitField Desc: Internal FiFo Write Error
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_stk1_pen_Internal_FiFo_Write_Err_Bit_Start                                                      3
#define cAf6_stk1_pen_Internal_FiFo_Write_Err_Bit_End                                                        3
#define cAf6_stk1_pen_Internal_FiFo_Write_Err_Mask                                                       cBit3
#define cAf6_stk1_pen_Internal_FiFo_Write_Err_Shift                                                          3
#define cAf6_stk1_pen_Internal_FiFo_Write_Err_MaxVal                                                       0x1
#define cAf6_stk1_pen_Internal_FiFo_Write_Err_MinVal                                                       0x0
#define cAf6_stk1_pen_Internal_FiFo_Write_Err_RstVal                                                       0x0

/*--------------------------------------
BitField Name: Internal_FiFO_BLK_Empty
BitField Type: R/W
BitField Desc: Internal FiFo Block Empty
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_stk1_pen_Internal_FiFO_BLK_Empty_Bit_Start                                                      2
#define cAf6_stk1_pen_Internal_FiFO_BLK_Empty_Bit_End                                                        2
#define cAf6_stk1_pen_Internal_FiFO_BLK_Empty_Mask                                                       cBit2
#define cAf6_stk1_pen_Internal_FiFO_BLK_Empty_Shift                                                          2
#define cAf6_stk1_pen_Internal_FiFO_BLK_Empty_MaxVal                                                       0x1
#define cAf6_stk1_pen_Internal_FiFO_BLK_Empty_MinVal                                                       0x0
#define cAf6_stk1_pen_Internal_FiFO_BLK_Empty_RstVal                                                       0x0

/*--------------------------------------
BitField Name: Internal_FiFO_BLK_Err
BitField Type: R/W
BitField Desc: Internal FiFo Block Error
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_stk1_pen_Internal_FiFO_BLK_Err_Bit_Start                                                        1
#define cAf6_stk1_pen_Internal_FiFO_BLK_Err_Bit_End                                                          1
#define cAf6_stk1_pen_Internal_FiFO_BLK_Err_Mask                                                         cBit1
#define cAf6_stk1_pen_Internal_FiFO_BLK_Err_Shift                                                            1
#define cAf6_stk1_pen_Internal_FiFO_BLK_Err_MaxVal                                                         0x1
#define cAf6_stk1_pen_Internal_FiFO_BLK_Err_MinVal                                                         0x0
#define cAf6_stk1_pen_Internal_FiFO_BLK_Err_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Internal_FiFO_BLK_Same
BitField Type: R/W
BitField Desc: Internal FiFo Block Same
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_stk1_pen_Internal_FiFO_BLK_Same_Bit_Start                                                       0
#define cAf6_stk1_pen_Internal_FiFO_BLK_Same_Bit_End                                                         0
#define cAf6_stk1_pen_Internal_FiFO_BLK_Same_Mask                                                        cBit0
#define cAf6_stk1_pen_Internal_FiFO_BLK_Same_Shift                                                           0
#define cAf6_stk1_pen_Internal_FiFO_BLK_Same_MaxVal                                                        0x1
#define cAf6_stk1_pen_Internal_FiFO_BLK_Same_MinVal                                                        0x0
#define cAf6_stk1_pen_Internal_FiFO_BLK_Same_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Stick 2
Reg Addr   : 0x40012
Reg Formula:
    Where  :
Reg Desc   :
Used to debug MPEG

------------------------------------------------------------------------------*/
#define cAf6Reg_stk2_pen_Base                                                                          0x40012
#define cAf6Reg_stk2_pen                                                                               0x40012
#define cAf6Reg_stk2_pen_WidthVal                                                                           32
#define cAf6Reg_stk2_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: Link_Sche_CFG1_parity_Err
BitField Type: R/W
BitField Desc: Link Scheduler CFG1 Parity Error
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_stk2_pen_Link_Sche_CFG1_parity_Err_Bit_Start                                                   27
#define cAf6_stk2_pen_Link_Sche_CFG1_parity_Err_Bit_End                                                     27
#define cAf6_stk2_pen_Link_Sche_CFG1_parity_Err_Mask                                                    cBit27
#define cAf6_stk2_pen_Link_Sche_CFG1_parity_Err_Shift                                                       27
#define cAf6_stk2_pen_Link_Sche_CFG1_parity_Err_MaxVal                                                     0x1
#define cAf6_stk2_pen_Link_Sche_CFG1_parity_Err_MinVal                                                     0x0
#define cAf6_stk2_pen_Link_Sche_CFG1_parity_Err_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Link_Sche_CFG0_parity_Err
BitField Type: R/W
BitField Desc: Link Scheduler CFG0 Parity Error
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_stk2_pen_Link_Sche_CFG0_parity_Err_Bit_Start                                                   26
#define cAf6_stk2_pen_Link_Sche_CFG0_parity_Err_Bit_End                                                     26
#define cAf6_stk2_pen_Link_Sche_CFG0_parity_Err_Mask                                                    cBit26
#define cAf6_stk2_pen_Link_Sche_CFG0_parity_Err_Shift                                                       26
#define cAf6_stk2_pen_Link_Sche_CFG0_parity_Err_MaxVal                                                     0x1
#define cAf6_stk2_pen_Link_Sche_CFG0_parity_Err_MinVal                                                     0x0
#define cAf6_stk2_pen_Link_Sche_CFG0_parity_Err_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Link_Sche_STA1_parity_Err
BitField Type: R/W
BitField Desc: Link Scheduler STA1  Error
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_stk2_pen_Link_Sche_STA1_parity_Err_Bit_Start                                                   25
#define cAf6_stk2_pen_Link_Sche_STA1_parity_Err_Bit_End                                                     25
#define cAf6_stk2_pen_Link_Sche_STA1_parity_Err_Mask                                                    cBit25
#define cAf6_stk2_pen_Link_Sche_STA1_parity_Err_Shift                                                       25
#define cAf6_stk2_pen_Link_Sche_STA1_parity_Err_MaxVal                                                     0x1
#define cAf6_stk2_pen_Link_Sche_STA1_parity_Err_MinVal                                                     0x0
#define cAf6_stk2_pen_Link_Sche_STA1_parity_Err_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Link_Sche_STA0_parity_Err
BitField Type: R/W
BitField Desc: Link Scheduler STA0  Error
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_stk2_pen_Link_Sche_STA0_parity_Err_Bit_Start                                                   24
#define cAf6_stk2_pen_Link_Sche_STA0_parity_Err_Bit_End                                                     24
#define cAf6_stk2_pen_Link_Sche_STA0_parity_Err_Mask                                                    cBit24
#define cAf6_stk2_pen_Link_Sche_STA0_parity_Err_Shift                                                       24
#define cAf6_stk2_pen_Link_Sche_STA0_parity_Err_MaxVal                                                     0x1
#define cAf6_stk2_pen_Link_Sche_STA0_parity_Err_MinVal                                                     0x0
#define cAf6_stk2_pen_Link_Sche_STA0_parity_Err_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Bundle_Sche_CFG1_parity_Err
BitField Type: R/W
BitField Desc: Bundle Scheduler CFG1 Parity Error
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_stk2_pen_Bundle_Sche_CFG1_parity_Err_Bit_Start                                                 19
#define cAf6_stk2_pen_Bundle_Sche_CFG1_parity_Err_Bit_End                                                   19
#define cAf6_stk2_pen_Bundle_Sche_CFG1_parity_Err_Mask                                                  cBit19
#define cAf6_stk2_pen_Bundle_Sche_CFG1_parity_Err_Shift                                                     19
#define cAf6_stk2_pen_Bundle_Sche_CFG1_parity_Err_MaxVal                                                   0x1
#define cAf6_stk2_pen_Bundle_Sche_CFG1_parity_Err_MinVal                                                   0x0
#define cAf6_stk2_pen_Bundle_Sche_CFG1_parity_Err_RstVal                                                   0x0

/*--------------------------------------
BitField Name: Bundle_Sche_CFG0_parity_Err
BitField Type: R/W
BitField Desc: Bundle Scheduler CFG0 Parity Error
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_stk2_pen_Bundle_Sche_CFG0_parity_Err_Bit_Start                                                 18
#define cAf6_stk2_pen_Bundle_Sche_CFG0_parity_Err_Bit_End                                                   18
#define cAf6_stk2_pen_Bundle_Sche_CFG0_parity_Err_Mask                                                  cBit18
#define cAf6_stk2_pen_Bundle_Sche_CFG0_parity_Err_Shift                                                     18
#define cAf6_stk2_pen_Bundle_Sche_CFG0_parity_Err_MaxVal                                                   0x1
#define cAf6_stk2_pen_Bundle_Sche_CFG0_parity_Err_MinVal                                                   0x0
#define cAf6_stk2_pen_Bundle_Sche_CFG0_parity_Err_RstVal                                                   0x0

/*--------------------------------------
BitField Name: Bundle_Sche_STA1_parity_Err
BitField Type: R/W
BitField Desc: Bundle Scheduler STA1  Error
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_stk2_pen_Bundle_Sche_STA1_parity_Err_Bit_Start                                                 17
#define cAf6_stk2_pen_Bundle_Sche_STA1_parity_Err_Bit_End                                                   17
#define cAf6_stk2_pen_Bundle_Sche_STA1_parity_Err_Mask                                                  cBit17
#define cAf6_stk2_pen_Bundle_Sche_STA1_parity_Err_Shift                                                     17
#define cAf6_stk2_pen_Bundle_Sche_STA1_parity_Err_MaxVal                                                   0x1
#define cAf6_stk2_pen_Bundle_Sche_STA1_parity_Err_MinVal                                                   0x0
#define cAf6_stk2_pen_Bundle_Sche_STA1_parity_Err_RstVal                                                   0x0

/*--------------------------------------
BitField Name: Bundle_Sche_STA0_parity_Err
BitField Type: R/W
BitField Desc: Bundle Scheduler STA0  Error
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_stk2_pen_Bundle_Sche_STA0_parity_Err_Bit_Start                                                 16
#define cAf6_stk2_pen_Bundle_Sche_STA0_parity_Err_Bit_End                                                   16
#define cAf6_stk2_pen_Bundle_Sche_STA0_parity_Err_Mask                                                  cBit16
#define cAf6_stk2_pen_Bundle_Sche_STA0_parity_Err_Shift                                                     16
#define cAf6_stk2_pen_Bundle_Sche_STA0_parity_Err_MaxVal                                                   0x1
#define cAf6_stk2_pen_Bundle_Sche_STA0_parity_Err_MinVal                                                   0x0
#define cAf6_stk2_pen_Bundle_Sche_STA0_parity_Err_RstVal                                                   0x0

/*--------------------------------------
BitField Name: EtherBypass_Sche_CFG1_parity_Err
BitField Type: R/W
BitField Desc: EtherBypass Scheduler CFG1 Parity Error
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_stk2_pen_EtherBypass_Sche_CFG1_parity_Err_Bit_Start                                            11
#define cAf6_stk2_pen_EtherBypass_Sche_CFG1_parity_Err_Bit_End                                              11
#define cAf6_stk2_pen_EtherBypass_Sche_CFG1_parity_Err_Mask                                             cBit11
#define cAf6_stk2_pen_EtherBypass_Sche_CFG1_parity_Err_Shift                                                11
#define cAf6_stk2_pen_EtherBypass_Sche_CFG1_parity_Err_MaxVal                                              0x1
#define cAf6_stk2_pen_EtherBypass_Sche_CFG1_parity_Err_MinVal                                              0x0
#define cAf6_stk2_pen_EtherBypass_Sche_CFG1_parity_Err_RstVal                                              0x0

/*--------------------------------------
BitField Name: EtherBypass_Sche_CFG0_parity_Err
BitField Type: R/W
BitField Desc: EtherBypass Scheduler CFG0 Parity Error
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_stk2_pen_EtherBypass_Sche_CFG0_parity_Err_Bit_Start                                            10
#define cAf6_stk2_pen_EtherBypass_Sche_CFG0_parity_Err_Bit_End                                              10
#define cAf6_stk2_pen_EtherBypass_Sche_CFG0_parity_Err_Mask                                             cBit10
#define cAf6_stk2_pen_EtherBypass_Sche_CFG0_parity_Err_Shift                                                10
#define cAf6_stk2_pen_EtherBypass_Sche_CFG0_parity_Err_MaxVal                                              0x1
#define cAf6_stk2_pen_EtherBypass_Sche_CFG0_parity_Err_MinVal                                              0x0
#define cAf6_stk2_pen_EtherBypass_Sche_CFG0_parity_Err_RstVal                                              0x0

/*--------------------------------------
BitField Name: EtherBypass_Sche_STA1_parity_Err
BitField Type: R/W
BitField Desc: EtherBypass Scheduler STA1  Error
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_stk2_pen_EtherBypass_Sche_STA1_parity_Err_Bit_Start                                             9
#define cAf6_stk2_pen_EtherBypass_Sche_STA1_parity_Err_Bit_End                                               9
#define cAf6_stk2_pen_EtherBypass_Sche_STA1_parity_Err_Mask                                              cBit9
#define cAf6_stk2_pen_EtherBypass_Sche_STA1_parity_Err_Shift                                                 9
#define cAf6_stk2_pen_EtherBypass_Sche_STA1_parity_Err_MaxVal                                              0x1
#define cAf6_stk2_pen_EtherBypass_Sche_STA1_parity_Err_MinVal                                              0x0
#define cAf6_stk2_pen_EtherBypass_Sche_STA1_parity_Err_RstVal                                              0x0

/*--------------------------------------
BitField Name: EtherBypass_Sche_STA0_parity_Err
BitField Type: R/W
BitField Desc: EtherBypass Scheduler STA0  Error
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_stk2_pen_EtherBypass_Sche_STA0_parity_Err_Bit_Start                                             8
#define cAf6_stk2_pen_EtherBypass_Sche_STA0_parity_Err_Bit_End                                               8
#define cAf6_stk2_pen_EtherBypass_Sche_STA0_parity_Err_Mask                                              cBit8
#define cAf6_stk2_pen_EtherBypass_Sche_STA0_parity_Err_Shift                                                 8
#define cAf6_stk2_pen_EtherBypass_Sche_STA0_parity_Err_MaxVal                                              0x1
#define cAf6_stk2_pen_EtherBypass_Sche_STA0_parity_Err_MinVal                                              0x0
#define cAf6_stk2_pen_EtherBypass_Sche_STA0_parity_Err_RstVal                                              0x0

/*--------------------------------------
BitField Name: VCAT_Sche_Err
BitField Type: R/W
BitField Desc: VCAT Scheduler Err
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_stk2_pen_VCAT_Sche_Err_Bit_Start                                                                2
#define cAf6_stk2_pen_VCAT_Sche_Err_Bit_End                                                                  2
#define cAf6_stk2_pen_VCAT_Sche_Err_Mask                                                                 cBit2
#define cAf6_stk2_pen_VCAT_Sche_Err_Shift                                                                    2
#define cAf6_stk2_pen_VCAT_Sche_Err_MaxVal                                                                 0x1
#define cAf6_stk2_pen_VCAT_Sche_Err_MinVal                                                                 0x0
#define cAf6_stk2_pen_VCAT_Sche_Err_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: FiFO_PDA_Request_Write_Err
BitField Type: R/W
BitField Desc: FiFO PDA Request Write Err
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_stk2_pen_FiFO_PDA_Request_Write_Err_Bit_Start                                                   1
#define cAf6_stk2_pen_FiFO_PDA_Request_Write_Err_Bit_End                                                     1
#define cAf6_stk2_pen_FiFO_PDA_Request_Write_Err_Mask                                                    cBit1
#define cAf6_stk2_pen_FiFO_PDA_Request_Write_Err_Shift                                                       1
#define cAf6_stk2_pen_FiFO_PDA_Request_Write_Err_MaxVal                                                    0x1
#define cAf6_stk2_pen_FiFO_PDA_Request_Write_Err_MinVal                                                    0x0
#define cAf6_stk2_pen_FiFO_PDA_Request_Write_Err_RstVal                                                    0x0

/*--------------------------------------
BitField Name: FiFO_PDA_Request_Read_Err
BitField Type: R/W
BitField Desc: FiFO PDA Request Read Err
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_stk2_pen_FiFO_PDA_Request_Read_Err_Bit_Start                                                    0
#define cAf6_stk2_pen_FiFO_PDA_Request_Read_Err_Bit_End                                                      0
#define cAf6_stk2_pen_FiFO_PDA_Request_Read_Err_Mask                                                     cBit0
#define cAf6_stk2_pen_FiFO_PDA_Request_Read_Err_Shift                                                        0
#define cAf6_stk2_pen_FiFO_PDA_Request_Read_Err_MaxVal                                                     0x1
#define cAf6_stk2_pen_FiFO_PDA_Request_Read_Err_MinVal                                                     0x0
#define cAf6_stk2_pen_FiFO_PDA_Request_Read_Err_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Stick 3
Reg Addr   : 0x40013
Reg Formula:
    Where  :
Reg Desc   :
Used to debug MPEG

------------------------------------------------------------------------------*/
#define cAf6Reg_stk3_pen_Base                                                                          0x40013
#define cAf6Reg_stk3_pen                                                                               0x40013
#define cAf6Reg_stk3_pen_WidthVal                                                                           32
#define cAf6Reg_stk3_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: PDA_Request_Link_Buffer
BitField Type: R/W
BitField Desc: PDA Request Link Buffer
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_stk3_pen_PDA_Request_Link_Buffer_Bit_Start                                                     31
#define cAf6_stk3_pen_PDA_Request_Link_Buffer_Bit_End                                                       31
#define cAf6_stk3_pen_PDA_Request_Link_Buffer_Mask                                                      cBit31
#define cAf6_stk3_pen_PDA_Request_Link_Buffer_Shift                                                         31
#define cAf6_stk3_pen_PDA_Request_Link_Buffer_MaxVal                                                       0x1
#define cAf6_stk3_pen_PDA_Request_Link_Buffer_MinVal                                                       0x0
#define cAf6_stk3_pen_PDA_Request_Link_Buffer_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PDA_Request_VCAT_Buffer
BitField Type: R/W
BitField Desc: PDA Request VCAT Buffer
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_stk3_pen_PDA_Request_VCAT_Buffer_Bit_Start                                                     30
#define cAf6_stk3_pen_PDA_Request_VCAT_Buffer_Bit_End                                                       30
#define cAf6_stk3_pen_PDA_Request_VCAT_Buffer_Mask                                                      cBit30
#define cAf6_stk3_pen_PDA_Request_VCAT_Buffer_Shift                                                         30
#define cAf6_stk3_pen_PDA_Request_VCAT_Buffer_MaxVal                                                       0x1
#define cAf6_stk3_pen_PDA_Request_VCAT_Buffer_MinVal                                                       0x0
#define cAf6_stk3_pen_PDA_Request_VCAT_Buffer_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PDA_Request_GEBP_Buffer
BitField Type: R/W
BitField Desc: PDA Request GE Bypass Buffer
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_stk3_pen_PDA_Request_GEBP_Buffer_Bit_Start                                                     29
#define cAf6_stk3_pen_PDA_Request_GEBP_Buffer_Bit_End                                                       29
#define cAf6_stk3_pen_PDA_Request_GEBP_Buffer_Mask                                                      cBit29
#define cAf6_stk3_pen_PDA_Request_GEBP_Buffer_Shift                                                         29
#define cAf6_stk3_pen_PDA_Request_GEBP_Buffer_MaxVal                                                       0x1
#define cAf6_stk3_pen_PDA_Request_GEBP_Buffer_MinVal                                                       0x0
#define cAf6_stk3_pen_PDA_Request_GEBP_Buffer_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PDA_Request_BUND_Buffer
BitField Type: R/W
BitField Desc: PDA Request Bundle Buffer
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_stk3_pen_PDA_Request_BUND_Buffer_Bit_Start                                                     28
#define cAf6_stk3_pen_PDA_Request_BUND_Buffer_Bit_End                                                       28
#define cAf6_stk3_pen_PDA_Request_BUND_Buffer_Mask                                                      cBit28
#define cAf6_stk3_pen_PDA_Request_BUND_Buffer_Shift                                                         28
#define cAf6_stk3_pen_PDA_Request_BUND_Buffer_MaxVal                                                       0x1
#define cAf6_stk3_pen_PDA_Request_BUND_Buffer_MinVal                                                       0x0
#define cAf6_stk3_pen_PDA_Request_BUND_Buffer_RstVal                                                       0x0

/*--------------------------------------
BitField Name: MPEG_Return_Link_Valid_PDA
BitField Type: R/W
BitField Desc: MPEG Return Link Valid to PDA
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_stk3_pen_MPEG_Return_Link_Valid_PDA_Bit_Start                                                  27
#define cAf6_stk3_pen_MPEG_Return_Link_Valid_PDA_Bit_End                                                    27
#define cAf6_stk3_pen_MPEG_Return_Link_Valid_PDA_Mask                                                   cBit27
#define cAf6_stk3_pen_MPEG_Return_Link_Valid_PDA_Shift                                                      27
#define cAf6_stk3_pen_MPEG_Return_Link_Valid_PDA_MaxVal                                                    0x1
#define cAf6_stk3_pen_MPEG_Return_Link_Valid_PDA_MinVal                                                    0x0
#define cAf6_stk3_pen_MPEG_Return_Link_Valid_PDA_RstVal                                                    0x0

/*--------------------------------------
BitField Name: MPEG_Return_VCAT_Valid_PDA
BitField Type: R/W
BitField Desc: MPEG Return VCAT Valid to PDA
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_stk3_pen_MPEG_Return_VCAT_Valid_PDA_Bit_Start                                                  26
#define cAf6_stk3_pen_MPEG_Return_VCAT_Valid_PDA_Bit_End                                                    26
#define cAf6_stk3_pen_MPEG_Return_VCAT_Valid_PDA_Mask                                                   cBit26
#define cAf6_stk3_pen_MPEG_Return_VCAT_Valid_PDA_Shift                                                      26
#define cAf6_stk3_pen_MPEG_Return_VCAT_Valid_PDA_MaxVal                                                    0x1
#define cAf6_stk3_pen_MPEG_Return_VCAT_Valid_PDA_MinVal                                                    0x0
#define cAf6_stk3_pen_MPEG_Return_VCAT_Valid_PDA_RstVal                                                    0x0

/*--------------------------------------
BitField Name: MPEG_Return_GEBP_Valid_PDA
BitField Type: R/W
BitField Desc: MPEG Return GE Bypass Valid to PDA
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_stk3_pen_MPEG_Return_GEBP_Valid_PDA_Bit_Start                                                  25
#define cAf6_stk3_pen_MPEG_Return_GEBP_Valid_PDA_Bit_End                                                    25
#define cAf6_stk3_pen_MPEG_Return_GEBP_Valid_PDA_Mask                                                   cBit25
#define cAf6_stk3_pen_MPEG_Return_GEBP_Valid_PDA_Shift                                                      25
#define cAf6_stk3_pen_MPEG_Return_GEBP_Valid_PDA_MaxVal                                                    0x1
#define cAf6_stk3_pen_MPEG_Return_GEBP_Valid_PDA_MinVal                                                    0x0
#define cAf6_stk3_pen_MPEG_Return_GEBP_Valid_PDA_RstVal                                                    0x0

/*--------------------------------------
BitField Name: MPEG_Return_BUND_Valid_PDA
BitField Type: R/W
BitField Desc: MPEG Return Bundle Valid to PDA
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_stk3_pen_MPEG_Return_BUND_Valid_PDA_Bit_Start                                                  24
#define cAf6_stk3_pen_MPEG_Return_BUND_Valid_PDA_Bit_End                                                    24
#define cAf6_stk3_pen_MPEG_Return_BUND_Valid_PDA_Mask                                                   cBit24
#define cAf6_stk3_pen_MPEG_Return_BUND_Valid_PDA_Shift                                                      24
#define cAf6_stk3_pen_MPEG_Return_BUND_Valid_PDA_MaxVal                                                    0x1
#define cAf6_stk3_pen_MPEG_Return_BUND_Valid_PDA_MinVal                                                    0x0
#define cAf6_stk3_pen_MPEG_Return_BUND_Valid_PDA_RstVal                                                    0x0

/*--------------------------------------
BitField Name: MPEG_Return_Link_Valid_PDA
BitField Type: R/W
BitField Desc: MPEG Return Link ACK to PDA
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_stk3_pen_MPEG_Return_Link_Ack_PDA_Bit_Start                                                  23
#define cAf6_stk3_pen_MPEG_Return_Link_Ack_PDA_Bit_End                                                    23
#define cAf6_stk3_pen_MPEG_Return_Link_Ack_PDA_Mask                                                   cBit23
#define cAf6_stk3_pen_MPEG_Return_Link_Ack_PDA_Shift                                                      23
#define cAf6_stk3_pen_MPEG_Return_Link_Ack_PDA_MaxVal                                                    0x1
#define cAf6_stk3_pen_MPEG_Return_Link_Ack_PDA_MinVal                                                    0x0
#define cAf6_stk3_pen_MPEG_Return_Link_Ack_PDA_RstVal                                                    0x0

/*--------------------------------------
BitField Name: MPEG_Return_VCAT_Valid_PDA
BitField Type: R/W
BitField Desc: MPEG Return VCAT ACK to PDA
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_stk3_pen_MPEG_Return_VCAT_Ack_PDA_Bit_Start                                                  22
#define cAf6_stk3_pen_MPEG_Return_VCAT_Ack_PDA_Bit_End                                                    22
#define cAf6_stk3_pen_MPEG_Return_VCAT_Ack_PDA_Mask                                                   cBit22
#define cAf6_stk3_pen_MPEG_Return_VCAT_Ack_PDA_Shift                                                      22
#define cAf6_stk3_pen_MPEG_Return_VCAT_Ack_PDA_MaxVal                                                    0x1
#define cAf6_stk3_pen_MPEG_Return_VCAT_Ack_PDA_MinVal                                                    0x0
#define cAf6_stk3_pen_MPEG_Return_VCAT_Ack_PDA_RstVal                                                    0x0

/*--------------------------------------
BitField Name: MPEG_Return_GEBP_Valid_PDA
BitField Type: R/W
BitField Desc: MPEG Return GE Bypass ACK to PDA
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_stk3_pen_MPEG_Return_GEBP_Ack_PDA_Bit_Start                                                  21
#define cAf6_stk3_pen_MPEG_Return_GEBP_Ack_PDA_Bit_End                                                    21
#define cAf6_stk3_pen_MPEG_Return_GEBP_Ack_PDA_Mask                                                   cBit21
#define cAf6_stk3_pen_MPEG_Return_GEBP_Ack_PDA_Shift                                                      21
#define cAf6_stk3_pen_MPEG_Return_GEBP_Ack_PDA_MaxVal                                                    0x1
#define cAf6_stk3_pen_MPEG_Return_GEBP_Ack_PDA_MinVal                                                    0x0
#define cAf6_stk3_pen_MPEG_Return_GEBP_Ack_PDA_RstVal                                                    0x0

/*--------------------------------------
BitField Name: MPEG_Return_BUND_Valid_PDA
BitField Type: R/W
BitField Desc: MPEG Return Bundle ACK to PDA
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_stk3_pen_MPEG_Return_BUND_Ack_PDA_Bit_Start                                                  20
#define cAf6_stk3_pen_MPEG_Return_BUND_Ack_PDA_Bit_End                                                    20
#define cAf6_stk3_pen_MPEG_Return_BUND_Ack_PDA_Mask                                                   cBit20
#define cAf6_stk3_pen_MPEG_Return_BUND_Ack_PDA_Shift                                                      20
#define cAf6_stk3_pen_MPEG_Return_BUND_Ack_PDA_MaxVal                                                    0x1
#define cAf6_stk3_pen_MPEG_Return_BUND_Ack_PDA_MinVal                                                    0x0
#define cAf6_stk3_pen_MPEG_Return_BUND_Ack_PDA_RstVal                                                    0x0

/*--------------------------------------
BitField Name: CLA_Bund_enqueue
BitField Type: R/W
BitField Desc: CLA Bundle Enqueue
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_Bund_enqueue_Bit_Start                                                            19
#define cAf6_stk3_pen_CLA_Bund_enqueue_Bit_End                                                              19
#define cAf6_stk3_pen_CLA_Bund_enqueue_Mask                                                             cBit19
#define cAf6_stk3_pen_CLA_Bund_enqueue_Shift                                                                19
#define cAf6_stk3_pen_CLA_Bund_enqueue_MaxVal                                                              0x1
#define cAf6_stk3_pen_CLA_Bund_enqueue_MinVal                                                              0x0
#define cAf6_stk3_pen_CLA_Bund_enqueue_RstVal                                                              0x0

/*--------------------------------------
BitField Name: CLA_Vcat_enqueue
BitField Type: R/W
BitField Desc: CLA VCAT Enqueue
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_Vcat_enqueue_Bit_Start                                                            18
#define cAf6_stk3_pen_CLA_Vcat_enqueue_Bit_End                                                              18
#define cAf6_stk3_pen_CLA_Vcat_enqueue_Mask                                                             cBit18
#define cAf6_stk3_pen_CLA_Vcat_enqueue_Shift                                                                18
#define cAf6_stk3_pen_CLA_Vcat_enqueue_MaxVal                                                              0x1
#define cAf6_stk3_pen_CLA_Vcat_enqueue_MinVal                                                              0x0
#define cAf6_stk3_pen_CLA_Vcat_enqueue_RstVal                                                              0x0

/*--------------------------------------
BitField Name: CLA_Link_enqueue
BitField Type: R/W
BitField Desc: CLA Link Enqueue
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_Link_enqueue_Bit_Start                                                            17
#define cAf6_stk3_pen_CLA_Link_enqueue_Bit_End                                                              17
#define cAf6_stk3_pen_CLA_Link_enqueue_Mask                                                             cBit17
#define cAf6_stk3_pen_CLA_Link_enqueue_Shift                                                                17
#define cAf6_stk3_pen_CLA_Link_enqueue_MaxVal                                                              0x1
#define cAf6_stk3_pen_CLA_Link_enqueue_MinVal                                                              0x0
#define cAf6_stk3_pen_CLA_Link_enqueue_RstVal                                                              0x0

/*--------------------------------------
BitField Name: CLA_GEBP_enqueue
BitField Type: R/W
BitField Desc: CLA GE bypass Enqueue
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_GEBP_enqueue_Bit_Start                                                            16
#define cAf6_stk3_pen_CLA_GEBP_enqueue_Bit_End                                                              16
#define cAf6_stk3_pen_CLA_GEBP_enqueue_Mask                                                             cBit16
#define cAf6_stk3_pen_CLA_GEBP_enqueue_Shift                                                                16
#define cAf6_stk3_pen_CLA_GEBP_enqueue_MaxVal                                                              0x1
#define cAf6_stk3_pen_CLA_GEBP_enqueue_MinVal                                                              0x0
#define cAf6_stk3_pen_CLA_GEBP_enqueue_RstVal                                                              0x0

/*--------------------------------------
BitField Name: CLA_Discard
BitField Type: R/W
BitField Desc: CLA Discard
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_Discard_Bit_Start                                                                 14
#define cAf6_stk3_pen_CLA_Discard_Bit_End                                                                   14
#define cAf6_stk3_pen_CLA_Discard_Mask                                                                  cBit14
#define cAf6_stk3_pen_CLA_Discard_Shift                                                                     14
#define cAf6_stk3_pen_CLA_Discard_MaxVal                                                                   0x1
#define cAf6_stk3_pen_CLA_Discard_MinVal                                                                   0x0
#define cAf6_stk3_pen_CLA_Discard_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: CLA_CES
BitField Type: R/W
BitField Desc: CLA CES
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_CES_Bit_Start                                                                     13
#define cAf6_stk3_pen_CLA_CES_Bit_End                                                                       13
#define cAf6_stk3_pen_CLA_CES_Mask                                                                      cBit13
#define cAf6_stk3_pen_CLA_CES_Shift                                                                         13
#define cAf6_stk3_pen_CLA_CES_MaxVal                                                                       0x1
#define cAf6_stk3_pen_CLA_CES_MinVal                                                                       0x0
#define cAf6_stk3_pen_CLA_CES_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: CLA_EOP
BitField Type: R/W
BitField Desc: CLA EOP
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_EOP_Bit_Start                                                                     12
#define cAf6_stk3_pen_CLA_EOP_Bit_End                                                                       12
#define cAf6_stk3_pen_CLA_EOP_Mask                                                                      cBit12
#define cAf6_stk3_pen_CLA_EOP_Shift                                                                         12
#define cAf6_stk3_pen_CLA_EOP_MaxVal                                                                       0x1
#define cAf6_stk3_pen_CLA_EOP_MinVal                                                                       0x0
#define cAf6_stk3_pen_CLA_EOP_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: CLA_CEP
BitField Type: R/W
BitField Desc: CLA CEP
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_CEP_Bit_Start                                                                     11
#define cAf6_stk3_pen_CLA_CEP_Bit_End                                                                       11
#define cAf6_stk3_pen_CLA_CEP_Mask                                                                      cBit11
#define cAf6_stk3_pen_CLA_CEP_Shift                                                                         11
#define cAf6_stk3_pen_CLA_CEP_MaxVal                                                                       0x1
#define cAf6_stk3_pen_CLA_CEP_MinVal                                                                       0x0
#define cAf6_stk3_pen_CLA_CEP_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: CLA_HDLC
BitField Type: R/W
BitField Desc: CLA HDLC
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_HDLC_Bit_Start                                                                     10
#define cAf6_stk3_pen_CLA_HDLC_Bit_End                                                                       10
#define cAf6_stk3_pen_CLA_HDLC_Mask                                                                      cBit10
#define cAf6_stk3_pen_CLA_HDLC_Shift                                                                        10
#define cAf6_stk3_pen_CLA_HDLC_MaxVal                                                                      0x1
#define cAf6_stk3_pen_CLA_HDLC_MinVal                                                                      0x0
#define cAf6_stk3_pen_CLA_HDLC_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: CLA_GEBP
BitField Type: R/W
BitField Desc: CLA GEBP
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_GEBP_Bit_Start                                                                     9
#define cAf6_stk3_pen_CLA_GEBP_Bit_End                                                                       9
#define cAf6_stk3_pen_CLA_GEBP_Mask                                                                      cBit9
#define cAf6_stk3_pen_CLA_GEBP_Shift                                                                         9
#define cAf6_stk3_pen_CLA_GEBP_MaxVal                                                                      0x1
#define cAf6_stk3_pen_CLA_GEBP_MinVal                                                                      0x0
#define cAf6_stk3_pen_CLA_GEBP_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: CLA_CHDLC
BitField Type: R/W
BitField Desc: CLA CHDLC
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_CHDLC_Bit_Start                                                                    8
#define cAf6_stk3_pen_CLA_CHDLC_Bit_End                                                                      8
#define cAf6_stk3_pen_CLA_CHDLC_Mask                                                                     cBit8
#define cAf6_stk3_pen_CLA_CHDLC_Shift                                                                        8
#define cAf6_stk3_pen_CLA_CHDLC_MaxVal                                                                     0x1
#define cAf6_stk3_pen_CLA_CHDLC_MinVal                                                                     0x0
#define cAf6_stk3_pen_CLA_CHDLC_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: CLA_PPPOAM
BitField Type: R/W
BitField Desc: CLA PPPOAM
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_PPPOAM_Bit_Start                                                                   7
#define cAf6_stk3_pen_CLA_PPPOAM_Bit_End                                                                     7
#define cAf6_stk3_pen_CLA_PPPOAM_Mask                                                                    cBit7
#define cAf6_stk3_pen_CLA_PPPOAM_Shift                                                                       7
#define cAf6_stk3_pen_CLA_PPPOAM_MaxVal                                                                    0x1
#define cAf6_stk3_pen_CLA_PPPOAM_MinVal                                                                    0x0
#define cAf6_stk3_pen_CLA_PPPOAM_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: CLA_PPPDAT
BitField Type: R/W
BitField Desc: CLA PPPDAT
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_PPPDAT_Bit_Start                                                                   6
#define cAf6_stk3_pen_CLA_PPPDAT_Bit_End                                                                     6
#define cAf6_stk3_pen_CLA_PPPDAT_Mask                                                                    cBit6
#define cAf6_stk3_pen_CLA_PPPDAT_Shift                                                                       6
#define cAf6_stk3_pen_CLA_PPPDAT_MaxVal                                                                    0x1
#define cAf6_stk3_pen_CLA_PPPDAT_MinVal                                                                    0x0
#define cAf6_stk3_pen_CLA_PPPDAT_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: CLA_MLPPDAT
BitField Type: R/W
BitField Desc: CLA MLPPDAT
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_MLPPDAT_Bit_Start                                                                  5
#define cAf6_stk3_pen_CLA_MLPPDAT_Bit_End                                                                    5
#define cAf6_stk3_pen_CLA_MLPPDAT_Mask                                                                   cBit5
#define cAf6_stk3_pen_CLA_MLPPDAT_Shift                                                                      5
#define cAf6_stk3_pen_CLA_MLPPDAT_MaxVal                                                                   0x1
#define cAf6_stk3_pen_CLA_MLPPDAT_MinVal                                                                   0x0
#define cAf6_stk3_pen_CLA_MLPPDAT_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: CLA_MLPPOAM
BitField Type: R/W
BitField Desc: CLA MLPPOAM
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_MLPPOAM_Bit_Start                                                                  4
#define cAf6_stk3_pen_CLA_MLPPOAM_Bit_End                                                                    4
#define cAf6_stk3_pen_CLA_MLPPOAM_Mask                                                                   cBit4
#define cAf6_stk3_pen_CLA_MLPPOAM_Shift                                                                      4
#define cAf6_stk3_pen_CLA_MLPPOAM_MaxVal                                                                   0x1
#define cAf6_stk3_pen_CLA_MLPPOAM_MinVal                                                                   0x0
#define cAf6_stk3_pen_CLA_MLPPOAM_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: CLA_FROAM
BitField Type: R/W
BitField Desc: CLA FROAM
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_FROAM_Bit_Start                                                                    3
#define cAf6_stk3_pen_CLA_FROAM_Bit_End                                                                      3
#define cAf6_stk3_pen_CLA_FROAM_Mask                                                                     cBit3
#define cAf6_stk3_pen_CLA_FROAM_Shift                                                                        3
#define cAf6_stk3_pen_CLA_FROAM_MaxVal                                                                     0x1
#define cAf6_stk3_pen_CLA_FROAM_MinVal                                                                     0x0
#define cAf6_stk3_pen_CLA_FROAM_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: CLA_FRDAT
BitField Type: R/W
BitField Desc: CLA FRDAT
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_FRDAT_Bit_Start                                                                    2
#define cAf6_stk3_pen_CLA_FRDAT_Bit_End                                                                      2
#define cAf6_stk3_pen_CLA_FRDAT_Mask                                                                     cBit2
#define cAf6_stk3_pen_CLA_FRDAT_Shift                                                                        2
#define cAf6_stk3_pen_CLA_FRDAT_MaxVal                                                                     0x1
#define cAf6_stk3_pen_CLA_FRDAT_MinVal                                                                     0x0
#define cAf6_stk3_pen_CLA_FRDAT_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: CLA_MLFROAM
BitField Type: R/W
BitField Desc: CLA MLFROAM
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_MLFROAM_Bit_Start                                                                  1
#define cAf6_stk3_pen_CLA_MLFROAM_Bit_End                                                                    1
#define cAf6_stk3_pen_CLA_MLFROAM_Mask                                                                   cBit1
#define cAf6_stk3_pen_CLA_MLFROAM_Shift                                                                      1
#define cAf6_stk3_pen_CLA_MLFROAM_MaxVal                                                                   0x1
#define cAf6_stk3_pen_CLA_MLFROAM_MinVal                                                                   0x0
#define cAf6_stk3_pen_CLA_MLFROAM_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: CLA_MLFRDAT
BitField Type: R/W
BitField Desc: CLA MLFRDAT
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_stk3_pen_CLA_MLFRDAT_Bit_Start                                                                  0
#define cAf6_stk3_pen_CLA_MLFRDAT_Bit_End                                                                    0
#define cAf6_stk3_pen_CLA_MLFRDAT_Mask                                                                   cBit0
#define cAf6_stk3_pen_CLA_MLFRDAT_Shift                                                                      0
#define cAf6_stk3_pen_CLA_MLFRDAT_MaxVal                                                                   0x1
#define cAf6_stk3_pen_CLA_MLFRDAT_MinVal                                                                   0x0
#define cAf6_stk3_pen_CLA_MLFRDAT_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Global Debug Counter
Reg Addr   : 0x40030-0x4003F
Reg Formula: 0x40030+$cntid
    Where  :
           + $cntid(0-15) : counter id
Reg Desc   :
Support the following Queue Counter:
- cntid = 00 : Total good packet enqueue
- cntid = 01 : CLA Declare Drop
- cntid = 02 : MPEG enable for PDA read
- cntid = 03 : PDA request read MPEG
- cntid = 04 : MPEG return valid to PDA
- cntid = 05 : MPEG packet valid to PDA
- cntid = 06 : PDA discard full
- cntid = 07 : QMXFF request Write DDR
- cntid = 08 : QMXFF request Read DDR
- cntid = 09 : DDR return ACK to MPEG
- cntid = 10 : DDR return Valid Write to MPEG
- cntid = 11 : DDR return Valid Read to MPEG
- cntid = 12 : Reserved
- cntid = 13 : Reserved
- cntid = 14 : Reserved
- cntid = 15 : Reserved

------------------------------------------------------------------------------*/
#define cAf6Reg_glbcnt_debug_Base                                                                      0x40030
#define cAf6Reg_glbcnt_debug(cntid)                                                          (0x40030+(cntid))
#define cAf6Reg_glbcnt_debug_WidthVal                                                                       32
#define cAf6Reg_glbcnt_debug_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: glbcnt
BitField Type: R/W
BitField Desc: Value of counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_glbcnt_debug_glbcnt_Bit_Start                                                                   0
#define cAf6_glbcnt_debug_glbcnt_Bit_End                                                                    31
#define cAf6_glbcnt_debug_glbcnt_Mask                                                                 cBit31_0
#define cAf6_glbcnt_debug_glbcnt_Shift                                                                       0
#define cAf6_glbcnt_debug_glbcnt_MaxVal                                                             0xffffffff
#define cAf6_glbcnt_debug_glbcnt_MinVal                                                                    0x0
#define cAf6_glbcnt_debug_glbcnt_RstVal                                                                    0x0

#endif /* _AF6_REG_AF6CCI0012_RD_MPEG_H_ */

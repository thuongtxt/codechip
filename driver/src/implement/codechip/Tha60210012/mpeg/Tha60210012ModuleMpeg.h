/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPEG
 * 
 * File        : Tha60210012ModuleMpeg.h
 * 
 * Created Date: Mar 14, 2016
 *
 * Description : Module MPEG interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef THA60210012MODULEMPEG_H_
#define THA60210012MODULEMPEG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleMpeg * Tha60210012ModuleMpeg;

typedef enum eThaMpegQueuePriority
    {
    cThaMpegQueuePriorityUnknown,
    cThaMpegQueuePriorityHighest,
    cThaMpegQueuePriorityMedium,
    cThaMpegQueuePriorityLowest
    }eThaMpegQueuePriority;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210012ModuleMpegNew(AtDevice device);
uint32 Tha60210012ModuleMpegBaseAddress(Tha60210012ModuleMpeg self);
eAtRet Tha60210012ModuleMpegConcateQueueEnable(Tha60210012ModuleMpeg self, AtEncapChannel encapChannel, eBool enable);

/* For use when disable Queue */
eBool Tha60210012ModuleMpegQueueWaitBufferIsEmpty(Tha60210012ModuleMpeg self);

/* Queue attribute control */
/* Bundle */
eAtRet Tha60210012ModuleMpegBundleQueueMaxThresholdSet(Tha60210012ModuleMpeg self, uint32 bundleId, uint32 intThresholdInPkt, uint32 extThresholdIn128Pkts);
uint32 Tha60210012ModuleMpegBundleQueueMaxInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 bundleId);
uint32 Tha60210012ModuleMpegBundleQueueMaxExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 bundleId);
eAtRet Tha60210012ModuleMpegBundleQueueMinThresholdSet(Tha60210012ModuleMpeg self, uint32 bundleId, uint32 internalThreshold, uint32 externalThreshold);
uint32 Tha60210012ModuleMpegBundleQueueMinInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 bundleId);
uint32 Tha60210012ModuleMpegBundleQueueMinExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 bundleId);
eAtRet Tha60210012ModuleMpegBundleQueueQuantumSet(Tha60210012ModuleMpeg self, uint32 bundleId, uint32 quantumInByte);
uint32 Tha60210012ModuleMpegBundleQueueQuantumGet(Tha60210012ModuleMpeg self, uint32 bundleId);
eAtRet Tha60210012ModuleMpegQueueBundlePrioritySet(Tha60210012ModuleMpeg self, uint32 bundleId, eThaMpegQueuePriority priority);
eThaMpegQueuePriority Tha60210012ModuleMpegBundleQueuePriorityGet(Tha60210012ModuleMpeg self, uint32 bundleId);
eAtRet Tha60210012ModuleMpegBundleQueueEnable(Tha60210012ModuleMpeg self, uint32 bundleId, eBool enable);
eBool Tha60210012ModuleMpegBundleQueueIsEnabled(Tha60210012ModuleMpeg self, uint32 queueId);

/* Vcg */
eAtRet Tha60210012ModuleMpegVcgQueueMaxThresholdSet(Tha60210012ModuleMpeg self, uint32 vcgId, uint32 internalThreshold, uint32 externalThreshold);
uint32 Tha60210012ModuleMpegVcgQueueMaxInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 vcgId);
uint32 Tha60210012ModuleMpegVcgQueueMaxExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 vcgId);
eAtRet Tha60210012ModuleMpegVcgQueueMinThresholdSet(Tha60210012ModuleMpeg self, uint32 vcgId, uint32 internalThreshold, uint32 externalThreshold);
uint32 Tha60210012ModuleMpegVcgQueueMinInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 vcgId);
uint32 Tha60210012ModuleMpegVcgQueueMinExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 vcgId);
eAtRet Tha60210012ModuleMpegVcgQueueQuantumSet(Tha60210012ModuleMpeg self, uint32 vcgId, uint32 quantumInByte);
uint32 Tha60210012ModuleMpegVcgQueueQuantumGet(Tha60210012ModuleMpeg self, uint32 vcgId);
eAtRet Tha60210012ModuleMpegQueueVcgPrioritySet(Tha60210012ModuleMpeg self, uint32 vcgId, eThaMpegQueuePriority priority);
eThaMpegQueuePriority Tha60210012ModuleMpegVcgQueuePriorityGet(Tha60210012ModuleMpeg self, uint32 vcgId);
eAtRet Tha60210012ModuleMpegVcgQueueEnable(Tha60210012ModuleMpeg self, uint32 vcgId, eBool enable);
eBool Tha60210012ModuleMpegVcgQueueIsEnabled(Tha60210012ModuleMpeg self, uint32 vcgId);

/* PPP Link */
eAtRet Tha60210012ModuleMpegLinkQueueMaxThresholdSet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId, uint32 internalThreshold, uint32 externalThreshold);
uint32 Tha60210012ModuleMpegLinkQueueMaxInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId);
uint32 Tha60210012ModuleMpegLinkQueueMaxExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId);
eAtRet Tha60210012ModuleMpegLinkQueueMinThresholdSet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId, uint32 internalThreshold, uint32 externalThreshold);
uint32 Tha60210012ModuleMpegLinkQueueMinInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId);
uint32 Tha60210012ModuleMpegLinkQueueMinExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId);
eAtRet Tha60210012ModuleMpegLinkQueueQuantumSet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId, uint32 quantum);
uint32 Tha60210012ModuleMpegLinkQueueQuantumGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId);
eAtRet Tha60210012ModuleMpegLinkQueuePrioritySet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId, eThaMpegQueuePriority priority);
eThaMpegQueuePriority Tha60210012ModuleMpegLinkQueuePriorityGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId);
eAtRet Tha60210012ModuleMpegLinkQueueEnable(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId, eBool enable);
eBool Tha60210012ModuleMpegLinkQueueIsEnabled(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId);
uint32 Tha60210012ModuleMpegDebugCounterGet(Tha60210012ModuleMpeg self, uint32 counterId, eBool r2c);
uint8 Tha60210012ModuleMpegNumQueuesForLink(Tha60210012ModuleMpeg self);

/* Ethernet/eXAUI port bypass */
eAtRet Tha60210012ModuleMpegEthBypassQueueMaxThresholdSet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId, uint32 intThresholdInPkt, uint32 extThresholdIn128Pkts);
uint32 Tha60210012ModuleMpegEthBypassQueueMaxInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId);
uint32 Tha60210012ModuleMpegEthBypassQueueMaxExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId);
eAtRet Tha60210012ModuleMpegEthBypassQueueMinThresholdSet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId, uint32 intThresholdInPkt, uint32 extThresholdIn128Pkts);
uint32 Tha60210012ModuleMpegEthBypassQueueMinInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId);
uint32 Tha60210012ModuleMpegEthBypassQueueMinExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId);
eAtRet Tha60210012ModuleMpegEthByPassQueueQuantumSet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId, uint32 quantum);
uint32 Tha60210012ModuleMpegEthBypassQueueQuantumGet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId);
eAtRet Tha60210012ModuleMpegEthBypassQueuePrioritySet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId, eThaMpegQueuePriority priority);
eThaMpegQueuePriority Tha60210012ModuleMpegEthBypassQueuePriorityGet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId);
eAtRet Tha60210012ModuleMpegEthBypassQueueEnable(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId, eBool enable);
eBool Tha60210012ModuleMpegEthBypassQueueIsEnabled(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId);

/* eXAUI */
eAtRet Tha60210012ModuleMpegEXauiEnable(AtModule self, uint32 exauiId, eBool enabled);
eBool Tha60210012ModuleMpegEXauiIsEnabled(AtModule self, uint32 exauiId);

#ifdef __cplusplus
}
#endif
#endif /* THA60210012MODULEMPEG_H_ */


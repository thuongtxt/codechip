/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MPEG (internal)
 *
 * File        : Th60210012ModuleMpeg.c
 *
 * Created Date: Mar 14, 2016
 *
 * Description : MPEG internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../man/Tha60210012Device.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "Tha60210012ModuleMpegInternal.h"
#include "Tha60210012MpegReg.h"
#include "../man/Tha60210012Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210012ModuleMpeg)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012ModuleMpegMethods m_methods;

/* Override */
static tAtModuleMethods         m_AtModuleOverride;
static tThaModuleMpegMethods    m_ThaModuleMpegOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    if ((localAddress >= 0x240000) && (localAddress <= 0x27A000))
        return cAtTrue;

    return cAtFalse;
    }

static eBool MemoryCanBeTested(AtModule self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleMpegRegisterIteratorCreate(self);
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x240000, 0x240001, 0x240002, 0x240003};
	AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = 4;

    return holdRegisters;
    }

static uint32 BaseAddress(Tha60210012ModuleMpeg self)
    {
    AtUnused(self);
    return 0x1800000UL;
    }

static uint32 BundleQueueOffSet(Tha60210012ModuleMpeg self, uint32 bundleId)
    {
    return (Tha60210012ModuleMpegBaseAddress(self) + (bundleId * mMethodsGet(self)->NumQueuesForBundle(self)));
    }

static eBool ThresholdIsValid(uint32 threshod)
    {
    /* Base on RD, maximum step for threshold is 32 */
    return (threshod < 32) ? cAtTrue : cAtFalse;
    }

static uint32 VcgQueueOffset(Tha60210012ModuleMpeg self, uint32 vcgId)
    {
    return (Tha60210012ModuleMpegBaseAddress(self) + (vcgId << 1));
    }

static uint32 PppLinkQueueOffset(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId)
    {
    return (Tha60210012ModuleMpegBaseAddress(self) + (linkId * mMethodsGet(self)->NumQueuesForLink(self)) + queueId);
    }

static eAtRet ConcateQueueEnable(Tha60210012ModuleMpeg self, AtEncapChannel encapChannel, eBool enable)
    {
    AtUnused(self);
    AtUnused(encapChannel);
    AtUnused(enable);
    return cAtOk;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsMpegRegister(AtModuleDeviceGet(self), address);
    }

static uint32 MaxNumLinks(Tha60210012ModuleMpeg self)
    {
    AtUnused(self);
    return 3072;
    }

static eAtRet AllLinksQueuesDisable(Tha60210012ModuleMpeg self)
    {
    uint32 maxNumLinks = mMethodsGet(self)->MaxNumLinks(self);
    const uint32 maxNumQueuesPerLink = mMethodsGet(self)->NumQueuesForLink(self);
    uint32 link_i, queue_i;

    for (link_i = 0; link_i < maxNumLinks; link_i++)
        {
        for (queue_i = 0; queue_i < maxNumQueuesPerLink; queue_i++)
            Tha60210012ModuleMpegLinkQueueEnable(self, link_i, queue_i, cAtFalse);
        }

    return cAtOk;
    }

static eAtRet AllBundlesQueuesDisable(Tha60210012ModuleMpeg self)
    {
    const uint32 cMaxNumBundles = 512;
    uint32 bundle_i;

    for (bundle_i = 0; bundle_i < cMaxNumBundles; bundle_i++)
            Tha60210012ModuleMpegBundleQueueEnable(self, bundle_i, cAtFalse);

    return cAtOk;
    }

static eAtRet AllVcgsQueuesDisable(Tha60210012ModuleMpeg self)
    {
    const uint32 cMaxNumVcgs = 256;
    uint32 vcg_i;

    for (vcg_i = 0; vcg_i < cMaxNumVcgs; vcg_i++)
        Tha60210012ModuleMpegVcgQueueEnable(self, vcg_i, cAtFalse);

    return cAtOk;
    }

static eAtRet AllQueuesDisable(Tha60210012ModuleMpeg self)
    {
    eAtRet ret = AllLinksQueuesDisable(self);
    ret |= AllBundlesQueuesDisable(self);
    ret |= AllVcgsQueuesDisable(self);

    return ret;
    }

static uint32 EXauiHwId(uint32 exauiId)
    {
    return 4 + exauiId;
    }

static eAtRet EXauiDefaultSet(AtModule self, uint32 exauiId)
    {
    uint32 hwId = EXauiHwId(exauiId);

    Tha60210012ModuleMpegEthBypassQueueMaxThresholdSet(mThis(self), hwId, 0, 0x07, 0x07);
    Tha60210012ModuleMpegEthBypassQueueMinThresholdSet(mThis(self), hwId, 0, 0x02, 0x02);
    Tha60210012ModuleMpegEthBypassQueuePrioritySet(mThis(self), hwId, 0, cThaMpegQueuePriorityHighest);
    Tha60210012ModuleMpegEthBypassQueuePrioritySet(mThis(self), hwId, 1, cThaMpegQueuePriorityHighest);

    return cAtOk;
    }

static eAtRet EXauiEnable(AtModule self, uint32 exauiId, eBool enabled)
    {
    uint32 hwId = EXauiHwId(exauiId);
    Tha60210012ModuleMpeg mpeg = (Tha60210012ModuleMpeg)self;

    /* When enabling, need to toggle it */
    Tha60210012ModuleMpegEthBypassQueueEnable(mpeg, hwId, 0, cAtFalse);
    if (enabled)
        Tha60210012ModuleMpegEthBypassQueueEnable(mpeg, hwId, 0, cAtTrue);

    return cAtOk;
    }

static eBool EXauiIsEnabled(AtModule self, uint32 exauiId)
    {
    uint32 hwId = EXauiHwId(exauiId);
    return Tha60210012ModuleMpegEthBypassQueueIsEnabled(mThis(self), hwId, 0);
    }

static eAtRet EXauiInit(AtModule self, uint32 exauiId)
    {
    eAtRet ret = cAtOk;

    ret |= EXauiDefaultSet(self, exauiId);
    ret |= Tha60210012ModuleMpegEXauiEnable(self, exauiId, cAtFalse);

    return ret;
    }

static eAtRet AllEXauisInit(AtModule self)
    {
    eAtRet ret = cAtOk;
    uint32 exauiId;
    AtDevice device = AtModuleDeviceGet(self);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);

    for (exauiId = 0; exauiId < Tha60210012ModuleEthNumExauiGet(ethModule); exauiId++)
        ret |= EXauiInit(self, exauiId);

    return ret;
    }

static eAtRet EthPassthroughQueuesInit(Tha60210012ModuleMpeg self)
    {
    return AllEXauisInit((AtModule)self);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= mMethodsGet(mThis(self))->EthPassthroughQueuesInit(mThis(self));
    if (ret != cAtOk)
        return ret;

    return AllQueuesDisable(mThis(self));
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void ShowDebugSticky0(AtModule self)
    {
    uint32 regAddress, regValue;
    AtDebugger debugger = NULL;

    regAddress = cAf6Reg_stk0_pen_Base + Tha60210012ModuleMpegBaseAddress((Tha60210012ModuleMpeg)self);
    regValue   = mModuleHwRead(self, regAddress);

    Tha6021DebugPrintRegName (debugger, "* MPEG debug sticky 0", regAddress, regValue);
    Tha6021DebugPrintErrorBit(debugger, " Internal FiFO read err", regValue, cAf6_stk0_pen_Internal_FiFo_read_error_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Enqueue Full", regValue, cAf6_stk0_pen_Enqueue_Full_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Internal FiFO write err", regValue, cAf6_stk0_pen_Internal_FiFO_write_error_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Internal FiFo full", regValue, cAf6_stk0_pen_Internal_FiFo_full_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Internal FiFO write Err", regValue, cAf6_stk0_pen_Internal_FiFO_write_Error_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Buffer External Full", regValue, cAf6_stk0_pen_Buffer_External_Full_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Internal FiFo Engine conflict", regValue, cAf6_stk0_pen_Internal_FiFo_Engine_conflict_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Buffer Write cache full", regValue, cAf6_stk0_pen_Buffer_Write_cache_full_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Buffer Write Full Threshold", regValue, cAf6_stk0_pen_Buffer_Write_Full_Thres_Mask );
    Tha6021DebugPrintErrorBit(debugger, " Internal FiFO fail", regValue, cAf6_stk0_pen_Internal_FiFO_fail_Mask);
    Tha6021DebugPrintErrorBit(debugger, " External FiFO Engine conflict", regValue, cAf6_stk0_pen_External_FiFo_Engine_conflict_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Discard because external FiFO full", regValue, cAf6_stk0_pen_Discard_External_FiFO_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Discard because internal FiFO full", regValue, cAf6_stk0_pen_Discard_Internal_FiFO_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Discard because CLA declare Drop", regValue, cAf6_stk0_pen_Discard_CLA_Drop_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Discard because Queue CFG Disable", regValue, cAf6_stk0_pen_Discard_Queue_Disable_Mask);
    Tha6021DebugPrintErrorBit(debugger, " External Block Same", regValue, cAf6_stk0_pen_External_Blk_Same_Mask);
    Tha6021DebugPrintErrorBit(debugger, " External Block Err", regValue, cAf6_stk0_pen_External_Blk_Error_Mask);
    Tha6021DebugPrintErrorBit(debugger, " External Block Empty", regValue, cAf6_stk0_pen_External_Blk_Empty_Mask);
    Tha6021DebugPrintErrorBit(debugger, " External Write Pending Full", regValue, cAf6_stk0_pen_External_Write_Pending_Full_Mask);
    Tha6021DebugPrintErrorBit(debugger, " External Read Pending Full", regValue, cAf6_stk0_pen_External_Read_Pending_Full_Mask);
    Tha6021DebugPrintErrorBit(debugger, " External Write buffer full", regValue, cAf6_stk0_pen_External_Write_Buffer_Full_Mask);
    Tha6021DebugPrintErrorBit(debugger, " External Write but Block Disable", regValue, cAf6_stk0_pen_External_Write_Blk_Dis_Mask);
    Tha6021DebugPrintErrorBit(debugger, " External Write but Block Empty", regValue, cAf6_stk0_pen_External_Write_Blk_Empty_Mask);
    Tha6021DebugPrintErrorBit(debugger, " External Read/Write Status Fail", regValue, cAf6_stk0_pen_External_Sta_err_Mask);
    Tha6021DebugPrintOkBit("MPEG return ACK to PDA", regValue, cAf6_stk0_pen_MPEG_ACK_Mask);
    Tha6021DebugPrintOkBit("PDA request Read", regValue, cAf6_stk0_pen_PDA_Request_Mask);
    Tha6021DebugPrintOkBit("CLA Enqueue MPEG", regValue, cAf6_stk0_pen_CLA_Write_Mask);
    Tha6021DebugPrintOkBit("MPEG Ready for PDA Reading", regValue, cAf6_stk0_pen_MPEG_Ready_Mask);
    Tha6021DebugPrintStop(debugger);

    /* clear sticky */
    mModuleHwWrite(self, regAddress, regValue);
    }

static void ShowDebugSticky1(AtModule self)
    {
    uint32 regAddress, regValue;
    AtDebugger debugger = NULL;

    regAddress = cAf6Reg_stk1_pen_Base + Tha60210012ModuleMpegBaseAddress((Tha60210012ModuleMpeg)self);;
    regValue   = mModuleHwRead(self, regAddress);

    Tha6021DebugPrintRegName (debugger, "* MPEG debug sticky 1", regAddress, regValue);
    Tha6021DebugPrintErrorBit(debugger, " Internal FiFo Block Same", regValue, cAf6_stk1_pen_Internal_FiFO_BLK_Same_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Internal FiFo Block Err", regValue, cAf6_stk1_pen_Internal_FiFO_BLK_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Internal FiFo Block Empty", regValue, cAf6_stk1_pen_Internal_FiFO_BLK_Empty_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Internal FiFo Write Err", regValue, cAf6_stk1_pen_Internal_FiFo_Write_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Internal FiFo Engine Err", regValue, cAf6_stk1_pen_Internal_FiFO_Engine_Error_Mask);
    Tha6021DebugPrintErrorBit(debugger, " QMXFF receive DDR Read Valid", regValue, cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Read_Mask);
    Tha6021DebugPrintErrorBit(debugger, " QMXFF receive DDR Write Valid", regValue, cAf6_stk1_pen_QMXFF_Receive_DDR_Valid_Write_Mask);
    Tha6021DebugPrintErrorBit(debugger, " QMXFF receive DDR ACK", regValue, cAf6_stk1_pen_QMXFF_Receive_DDR_ACK_Mask);
    Tha6021DebugPrintErrorBit(debugger, " QMXFF request Read DDR", regValue, cAf6_stk1_pen_QMXFF_request_Read_DDR_Mask );
    Tha6021DebugPrintErrorBit(debugger, " QMXFF request write DDR", regValue, cAf6_stk1_pen_QMXFF_request_write_DDR_Mask);
    Tha6021DebugPrintErrorBit(debugger, " FiFo Valid DDR Read Error", regValue, cAf6_stk1_pen_FiFo_Valid_DDR_Read_err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " FiFo Valid DDR Write Error", regValue, cAf6_stk1_pen_FiFo_Valid_DDR_Write_err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " FiFo Request ACK CLA Read Err", regValue, cAf6_stk1_pen_FiFo_Req_CLA_Read_err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " FiFo Request ACK CLA Write Err", regValue, cAf6_stk1_pen_FiFo_Req_CLA_Write_err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " PDA discard Full", regValue, cAf6_stk1_pen_PDA_Discard_Full_Mask);
    Tha6021DebugPrintErrorBit(debugger, " FiFo Request ACK DDR Read Err", regValue, cAf6_stk1_pen_FiFo_Req_DDR_Read_err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " FiFo Request ACK DDR Write Error", regValue, cAf6_stk1_pen_FiFo_Req_DDR_Write_err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " MPEG return Valid to PDA", regValue, cAf6_stk1_pen_MPEG_Valid_to_PDA_Mask);
    Tha6021DebugPrintStop(debugger);

    /* clear sticky */
    mModuleHwWrite(self, regAddress, regValue);
    }

static void ShowDebugSticky2(AtModule self)
    {
    uint32 regAddress, regValue;
    AtDebugger debugger = NULL;

    regAddress = cAf6Reg_stk2_pen_Base + Tha60210012ModuleMpegBaseAddress((Tha60210012ModuleMpeg)self);;
    regValue   = mModuleHwRead(self, regAddress);

    Tha6021DebugPrintRegName (debugger, "* MPEG debug sticky 2", regAddress, regValue);
    Tha6021DebugPrintErrorBit(debugger, " FiFO PDA Request Read Err            ", regValue, cAf6_stk2_pen_FiFO_PDA_Request_Read_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " FiFO PDA Request Write Err           ", regValue, cAf6_stk2_pen_FiFO_PDA_Request_Write_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " VCAT Scheduler Err                   ", regValue, cAf6_stk2_pen_VCAT_Sche_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " EtherBypass Scheduler STA0 Err       ", regValue, cAf6_stk2_pen_EtherBypass_Sche_STA0_parity_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " EtherBypass Scheduler STA1 Err       ", regValue, cAf6_stk2_pen_EtherBypass_Sche_STA1_parity_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " EtherBypass Scheduler CFG0 parity Err", regValue, cAf6_stk2_pen_EtherBypass_Sche_CFG0_parity_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " EtherBypass Scheduler CFG1 Parity Err", regValue, cAf6_stk2_pen_EtherBypass_Sche_CFG1_parity_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Bundle Scheduler STA0 Err            ", regValue, cAf6_stk2_pen_Bundle_Sche_STA0_parity_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Bundle Scheduler STA1 Err            ", regValue, cAf6_stk2_pen_Bundle_Sche_STA1_parity_Err_Mask );
    Tha6021DebugPrintErrorBit(debugger, " Bundle Scheduler CFG0 Parity Err     ", regValue, cAf6_stk2_pen_Bundle_Sche_CFG0_parity_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Bundle Scheduler CFG1 Parity Err     ", regValue, cAf6_stk2_pen_Bundle_Sche_CFG1_parity_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Link Scheduler STA0 Err              ", regValue, cAf6_stk2_pen_Link_Sche_STA0_parity_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Link Scheduler STA1 Err              ", regValue, cAf6_stk2_pen_Link_Sche_STA1_parity_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Link Scheduler CFG0 Parity Err       ", regValue, cAf6_stk2_pen_Link_Sche_CFG0_parity_Err_Mask);
    Tha6021DebugPrintErrorBit(debugger, " Link Scheduler CFG1 Parity Err       ", regValue, cAf6_stk2_pen_Link_Sche_CFG1_parity_Err_Mask);
    Tha6021DebugPrintStop(debugger);

    /* clear sticky */
    mModuleHwWrite(self, regAddress, regValue);
    }

static void ShowDebugSticky3(AtModule self)
    {
    uint32 regAddress, regValue;
    AtDebugger debugger = NULL;

    regAddress = cAf6Reg_stk3_pen_Base + Tha60210012ModuleMpegBaseAddress((Tha60210012ModuleMpeg)self);;
    regValue   = mModuleHwRead(self, regAddress);

    Tha6021DebugPrintRegName (debugger, "* MPEG debug sticky 3", regAddress, regValue);
    Tha6021DebugPrintErrorBit(debugger, " CLA MLFRDAT", regValue, cAf6_stk3_pen_CLA_MLFRDAT_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA MLFROAM", regValue, cAf6_stk3_pen_CLA_MLFROAM_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA FRDAT", regValue, cAf6_stk3_pen_CLA_FRDAT_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA FROAM", regValue, cAf6_stk3_pen_CLA_FROAM_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA MLPPOAM", regValue, cAf6_stk3_pen_CLA_MLPPOAM_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA MLPPDAT", regValue, cAf6_stk3_pen_CLA_MLPPDAT_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA PPPDAT", regValue, cAf6_stk3_pen_CLA_PPPDAT_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA PPPOAM", regValue, cAf6_stk3_pen_CLA_PPPOAM_Mask);
    Tha6021DebugPrintOkBit("CLA CHDLC", regValue, cAf6_stk3_pen_CLA_CHDLC_Mask );
    Tha6021DebugPrintErrorBit(debugger, " CLA GEBP", regValue, cAf6_stk3_pen_CLA_GEBP_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA HDLC", regValue, cAf6_stk3_pen_CLA_HDLC_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA CEP", regValue, cAf6_stk3_pen_CLA_CEP_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA EOP", regValue, cAf6_stk3_pen_CLA_EOP_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA CES", regValue, cAf6_stk3_pen_CLA_CES_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA Discard", regValue, cAf6_stk3_pen_CLA_Discard_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA GE bypass Enqueue", regValue, cAf6_stk3_pen_CLA_GEBP_enqueue_Mask);
    Tha6021DebugPrintOkBit("CLA Link Enqueue", regValue, cAf6_stk3_pen_CLA_Link_enqueue_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA VCAT Enqueue", regValue, cAf6_stk3_pen_CLA_Vcat_enqueue_Mask);
    Tha6021DebugPrintErrorBit(debugger, " CLA Bundle Enqueue", regValue, cAf6_stk3_pen_CLA_Bund_enqueue_Mask);
    Tha6021DebugPrintErrorBit(debugger, " MPEG Return Bundle ACK to PDA", regValue, cAf6_stk3_pen_MPEG_Return_BUND_Ack_PDA_Mask);
    Tha6021DebugPrintErrorBit(debugger, " MPEG Return GE Bypass ACK to PDA", regValue, cAf6_stk3_pen_MPEG_Return_GEBP_Ack_PDA_Mask);
    Tha6021DebugPrintErrorBit(debugger, " MPEG Return VCAT ACK to PDA ", regValue, cAf6_stk3_pen_MPEG_Return_VCAT_Ack_PDA_Mask);
    Tha6021DebugPrintOkBit("MPEG Return Link ACK to PDA", regValue, cAf6_stk3_pen_MPEG_Return_Link_Ack_PDA_Mask);
    Tha6021DebugPrintErrorBit(debugger, " MPEG Return Bundle Valid to PDA", regValue, cAf6_stk3_pen_MPEG_Return_BUND_Valid_PDA_Mask);
    Tha6021DebugPrintErrorBit(debugger, " MPEG Return GE Bypass Valid to PDA", regValue, cAf6_stk3_pen_MPEG_Return_GEBP_Valid_PDA_Mask);
    Tha6021DebugPrintErrorBit(debugger, " MPEG Return VCAT Valid to PDA", regValue, cAf6_stk3_pen_MPEG_Return_VCAT_Valid_PDA_Mask);
    Tha6021DebugPrintErrorBit(debugger, " MPEG Return Link Valid to PDA", regValue, cAf6_stk3_pen_MPEG_Return_Link_Valid_PDA_Mask);
    Tha6021DebugPrintErrorBit(debugger, " PDA Request Bundle Buffer", regValue, cAf6_stk3_pen_PDA_Request_BUND_Buffer_Mask);
    Tha6021DebugPrintErrorBit(debugger, " PDA Request GE Bypass Buffer", regValue, cAf6_stk3_pen_PDA_Request_GEBP_Buffer_Mask);
    Tha6021DebugPrintErrorBit(debugger, " PDA Request VCAT Buffer", regValue, cAf6_stk3_pen_PDA_Request_VCAT_Buffer_Mask);
    Tha6021DebugPrintOkBit("PDA Request Link Buffer", regValue, cAf6_stk3_pen_PDA_Request_Link_Buffer_Mask);
    Tha6021DebugPrintStop(debugger);

    /* clear sticky */
    mModuleHwWrite(self, regAddress, regValue);
    }

static void EncapChannelRegsShow(ThaModuleMpeg self, AtEncapChannel channel)
    {
    uint32 regAddress, offset, regValue;
    uint32 linkId, queueId;
    AtHdlcLink hdlcLink;

    if (AtEncapChannelEncapTypeGet(channel) != cAtEncapHdlc)
        {
        AtPrintc(cSevCritical, "ERROR: Link Hdlc has not been created\r\n");
        return;
        }

    /* Get Hdlc ppplink */
    hdlcLink = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)channel);
    linkId = AtChannelIdGet((AtChannel)hdlcLink);

    for (queueId = 0; queueId < mMethodsGet(mThis(self))->NumQueuesForLink(mThis(self)); queueId++)
        {
        regAddress = cAf6Reg_cfg2qthsh_pen_Base + PppLinkQueueOffset(mThis(self), linkId, queueId);
        regValue = mModuleHwRead(self, regAddress);
        Tha6021DebugPrintRegName(NULL, "Link Queue Threshold Control", regAddress, regValue);
        }

    offset = Tha60210012ModuleMpegBaseAddress(mThis(self)) + linkId;
    regAddress = cAf6Reg_schelinkicfg0_pen_Base + offset;
    regValue = mModuleHwRead(self, regAddress);
    Tha6021DebugPrintRegName(NULL, "Group priority Configuration Per Link", regAddress, regValue);
    }

static void HdlcBundleRegsShow(ThaModuleMpeg self, AtHdlcBundle channel)
    {
    uint32 addr, regVal, queueId;
    uint32 bundleId = AtChannelIdGet((AtChannel)channel);

    for (queueId = 0; queueId < mMethodsGet(mThis(self))->NumQueuesForBundle(mThis(self)); queueId++)
        {
        addr = BundleQueueOffSet(mThis(self), bundleId) + queueId + cAf6Reg_cfg0qthsh_pen_Base;
        regVal = mModuleHwRead(self, addr);
        Tha6021DebugPrintRegName(NULL, "Bundle Queue Threshold Control", addr, regVal);
        }

    for (queueId = 0; queueId < mMethodsGet(mThis(self))->NumQueuesForBundle(mThis(self)); queueId++)
        {
        addr = BundleQueueOffSet(mThis(self), bundleId) + cAf6Reg_schebundcfg1_pen_Base + queueId;
        regVal = mModuleHwRead(self, addr);
        Tha6021DebugPrintRegName(NULL, "DWRR Quantum Configuration", addr, regVal);
        }
    }

static eAtRet Debug(AtModule self)
    {
    ShowDebugSticky0(self);
    ShowDebugSticky1(self);
    ShowDebugSticky2(self);
    ShowDebugSticky3(self);
    AtPrintc(cSevNormal, "\r\n");

    return cAtOk;
    }

static uint32 EthBypassQueueOffset(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId)
    {
    return (Tha60210012ModuleMpegBaseAddress(self) + (portId << 1) + queueId);
    }

static uint8 NumQueuesForBundle(Tha60210012ModuleMpeg self)
    {
    AtUnused(self);
    return 4;
    }

static uint8 NumQueuesForLink(Tha60210012ModuleMpeg self)
    {
    AtUnused(self);
    return 2;
    }

static void MethodsInit(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, EthPassthroughQueuesInit);
        mMethodOverride(m_methods, NumQueuesForBundle);
        mMethodOverride(m_methods, NumQueuesForLink);
        mMethodOverride(m_methods, MaxNumLinks);
        mMethodOverride(m_methods, BaseAddress);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideThaModuleMpeg(AtModule self)
    {
    ThaModuleMpeg mpegModule = (ThaModuleMpeg)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMpegOverride, mMethodsGet(mpegModule), sizeof(m_ThaModuleMpegOverride));

        mMethodOverride(m_ThaModuleMpegOverride, EncapChannelRegsShow);
        mMethodOverride(m_ThaModuleMpegOverride, HdlcBundleRegsShow);
        }

    mMethodsSet(mpegModule, &m_ThaModuleMpegOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        mMethodOverride(m_AtModuleOverride, MemoryCanBeTested);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleMpeg(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleMpeg);
    }

AtModule Tha60210012ModuleMpegObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleMpegObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210012ModuleMpegNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModuleMpegObjectInit(newModule, device);
    }

uint32 Tha60210012ModuleMpegBaseAddress(Tha60210012ModuleMpeg self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);

    return 0;
    }

eAtRet Tha60210012ModuleMpegConcateQueueEnable(Tha60210012ModuleMpeg self, AtEncapChannel encapChannel, eBool enable)
    {
    if (self)
        return ConcateQueueEnable(self, encapChannel, enable);

    return cAtErrorNullPointer;
    }

eAtRet Tha60210012ModuleMpegBundleQueueMaxThresholdSet(Tha60210012ModuleMpeg self, uint32 bundleId, uint32 intThresholdInPkt, uint32 extThresholdIn128Pkts)
    {
    uint32 address, regValue;

    if (!ThresholdIsValid(intThresholdInPkt) || !ThresholdIsValid(extThresholdIn128Pkts))
        return cAtErrorOutOfRangParm;

    address = BundleQueueOffSet(self, bundleId) + cAf6Reg_cfg0qthsh_pen_Base;
    regValue = mModuleHwRead(self, address);
    mRegFieldSet(regValue, cAf6_cfg0qthsh_pen_IFFQthsh_max_, intThresholdInPkt);
    mRegFieldSet(regValue, cAf6_cfg0qthsh_pen_EFFQthsh_max_, extThresholdIn128Pkts);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

uint32 Tha60210012ModuleMpegBundleQueueMaxInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 bundleId)
    {
    uint32 address = BundleQueueOffSet(self, bundleId) + cAf6Reg_cfg0qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg0qthsh_pen_IFFQthsh_max_);
    }

uint32 Tha60210012ModuleMpegBundleQueueMaxExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 bundleId)
    {
    uint32 address = BundleQueueOffSet(self, bundleId) + cAf6Reg_cfg0qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg0qthsh_pen_EFFQthsh_max_);
    }

eAtRet Tha60210012ModuleMpegBundleQueueMinThresholdSet(Tha60210012ModuleMpeg self, uint32 bundleId, uint32 intThresholdInPkt, uint32 extThresholdIn128Pkts)
    {
    uint32 address, regValue;

    if (!ThresholdIsValid(intThresholdInPkt) || !ThresholdIsValid(extThresholdIn128Pkts))
        return cAtErrorOutOfRangParm;

    address = BundleQueueOffSet(self, bundleId) + cAf6Reg_cfg0qthsh_pen_Base;
    regValue = mModuleHwRead(self, address);
    mRegFieldSet(regValue, cAf6_cfg0qthsh_pen_IFFQthsh_min_, intThresholdInPkt);
    mRegFieldSet(regValue, cAf6_cfg0qthsh_pen_EFFQthsh_min_, extThresholdIn128Pkts);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

uint32 Tha60210012ModuleMpegBundleQueueMinInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 bundleId)
    {
    uint32 address = BundleQueueOffSet(self, bundleId) + cAf6Reg_cfg0qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg0qthsh_pen_IFFQthsh_min_);
    }

uint32 Tha60210012ModuleMpegBundleQueueMinExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 bundleId)
    {
    uint32 address = BundleQueueOffSet(self, bundleId) + cAf6Reg_cfg0qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg0qthsh_pen_EFFQthsh_min_);
    }

eAtRet Tha60210012ModuleMpegBundleQueueQuantumSet(Tha60210012ModuleMpeg self, uint32 bundleId, uint32 quantumInByte)
    {
    uint32 address = BundleQueueOffSet(self, bundleId) + cAf6Reg_schebundcfg1_pen_Base;
    uint32  regValue = mModuleHwRead(self, address);

    mRegFieldSet(regValue, cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_, quantumInByte);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

uint32 Tha60210012ModuleMpegBundleQueueQuantumGet(Tha60210012ModuleMpeg self, uint32 bundleId)
    {
    uint32 address = BundleQueueOffSet(self, bundleId) + cAf6Reg_schebundcfg1_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_);
    }

eAtRet Tha60210012ModuleMpegQueueBundlePrioritySet(Tha60210012ModuleMpeg self, uint32 bundleId, eThaMpegQueuePriority priority)
    {
    uint32 offset = Tha60210012ModuleMpegBaseAddress(self) + bundleId;
    uint32 address = offset + cAf6Reg_schebundicfg0_pen_Base;
    uint32 regValue = 0;

    switch(priority)
        {
        case cThaMpegQueuePriorityLowest:
            {
            mRegFieldSet(regValue, cAf6_schebundicfg0_pen_Lsp_grp_, 1);
            mRegFieldSet(regValue, cAf6_schebundicfg0_pen_DWRR_grp_, 0);
            mRegFieldSet(regValue, cAf6_schebundicfg0_pen_Hsp_grp_, 0);
            break;
            }
        case cThaMpegQueuePriorityMedium:
            {
            mRegFieldSet(regValue, cAf6_schebundicfg0_pen_Lsp_grp_, 0);
            mRegFieldSet(regValue, cAf6_schebundicfg0_pen_DWRR_grp_, 1);
            mRegFieldSet(regValue, cAf6_schebundicfg0_pen_Hsp_grp_, 0);
            break;
            }
        case cThaMpegQueuePriorityHighest:
            {
            mRegFieldSet(regValue, cAf6_schebundicfg0_pen_Lsp_grp_, 0);
            mRegFieldSet(regValue, cAf6_schebundicfg0_pen_DWRR_grp_, 0);
            mRegFieldSet(regValue, cAf6_schebundicfg0_pen_Hsp_grp_, 1);
            break;
            }
        case cThaMpegQueuePriorityUnknown:
        default:
            return cAtErrorModeNotSupport;
        }

    mModuleHwWrite(self, address, regValue);
    return cAtOk;
    }

eThaMpegQueuePriority Tha60210012ModuleMpegBundleQueuePriorityGet(Tha60210012ModuleMpeg self, uint32 bundleId)
    {
    uint32 offset = Tha60210012ModuleMpegBaseAddress(self) + bundleId;
    uint32 address = offset + cAf6Reg_schebundicfg0_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    if (mRegField(regValue, cAf6_schebundicfg0_pen_Lsp_grp_))  return cThaMpegQueuePriorityLowest;
    if (mRegField(regValue, cAf6_schebundicfg0_pen_DWRR_grp_)) return cThaMpegQueuePriorityMedium;
    if (mRegField(regValue, cAf6_schebundicfg0_pen_Hsp_grp_))  return cThaMpegQueuePriorityHighest;

    return cThaMpegQueuePriorityUnknown;
    }

eAtRet Tha60210012ModuleMpegVcgQueueMaxThresholdSet(Tha60210012ModuleMpeg self, uint32 vcgId, uint32 intThresholdInPkt, uint32 extThresholdIn128Pkts)
    {
    uint32 address, regValue;

    if (!ThresholdIsValid(intThresholdInPkt) || !ThresholdIsValid(extThresholdIn128Pkts))
        return cAtErrorOutOfRangParm;

    address = VcgQueueOffset(self, vcgId) + cAf6Reg_cfg1qthsh_pen_Base;
    regValue = mModuleHwRead(self, address);
    mRegFieldSet(regValue, cAf6_cfg1qthsh_pen_IFFQthsh_max_, intThresholdInPkt);
    mRegFieldSet(regValue, cAf6_cfg1qthsh_pen_EFFQthsh_max_, extThresholdIn128Pkts);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

uint32 Tha60210012ModuleMpegVcgQueueMaxInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 vcgId)
    {
    uint32 address = VcgQueueOffset(self, vcgId) + cAf6Reg_cfg1qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg1qthsh_pen_IFFQthsh_max_);
    }

uint32 Tha60210012ModuleMpegVcgQueueMaxExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 vcgId)
    {
    uint32 address = VcgQueueOffset(self, vcgId) + cAf6Reg_cfg1qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg1qthsh_pen_EFFQthsh_max_);
    }

eAtRet Tha60210012ModuleMpegVcgQueueMinThresholdSet(Tha60210012ModuleMpeg self, uint32 vcgId, uint32 intThresholdInPkt, uint32 extThresholdIn128Pkts)
    {
    uint32 address, regValue;

    if (!ThresholdIsValid(intThresholdInPkt) || !ThresholdIsValid(extThresholdIn128Pkts))
        return cAtErrorOutOfRangParm;

    address = VcgQueueOffset(self, vcgId) + cAf6Reg_cfg1qthsh_pen_Base;
    regValue = mModuleHwRead(self, address);
    mRegFieldSet(regValue, cAf6_cfg1qthsh_pen_IFFQthsh_min_, intThresholdInPkt);
    mRegFieldSet(regValue, cAf6_cfg1qthsh_pen_EFFQthsh_min_, extThresholdIn128Pkts);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

uint32 Tha60210012ModuleMpegVcgQueueMinInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 vcgId)
    {
    uint32 address = VcgQueueOffset(self, vcgId) + cAf6Reg_cfg1qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg1qthsh_pen_IFFQthsh_min_);
    }

uint32 Tha60210012ModuleMpegVcgQueueMinExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 vcgId)
    {
    uint32 address = VcgQueueOffset(self, vcgId) + cAf6Reg_cfg1qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg1qthsh_pen_EFFQthsh_min_);
    }

eAtRet Tha60210012ModuleMpegVcgQueueQuantumSet(Tha60210012ModuleMpeg self, uint32 vcgId, uint32 quantumInByte)
    {
    uint32 address = VcgQueueOffset(self, vcgId) + cAf6Reg_schevcatcfg1_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    mRegFieldSet(regValue, cAf6_schevcatcfg1_pen_Vcatshce_DWRR_QTum_, quantumInByte);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

uint32 Tha60210012ModuleMpegVcgQueueQuantumGet(Tha60210012ModuleMpeg self, uint32 vcgId)
    {
    uint32 address = VcgQueueOffset(self, vcgId) + cAf6Reg_schevcatcfg1_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_schevcatcfg1_pen_Vcatshce_DWRR_QTum_);
    }

eAtRet Tha60210012ModuleMpegQueueVcgPrioritySet(Tha60210012ModuleMpeg self, uint32 vcgId, eThaMpegQueuePriority priority)
    {
    uint32 offset = Tha60210012ModuleMpegBaseAddress(self) + vcgId;
    uint32 address = offset + cAf6Reg_schevcaticfg0_pen_Base;
    uint32 regValue = 0;

    switch(priority)
        {
        case cThaMpegQueuePriorityLowest:
            {
            mRegFieldSet(regValue, cAf6_schevcaticfg0_pen_Lsp_grp_, 1);
            mRegFieldSet(regValue, cAf6_schevcaticfg0_pen_DWRR_grp_, 0);
            mRegFieldSet(regValue, cAf6_schevcaticfg0_pen_Hsp_grp_, 0);
            break;
            }
        case cThaMpegQueuePriorityMedium:
            {
            mRegFieldSet(regValue, cAf6_schevcaticfg0_pen_Lsp_grp_, 0);
            mRegFieldSet(regValue, cAf6_schevcaticfg0_pen_DWRR_grp_, 1);
            mRegFieldSet(regValue, cAf6_schevcaticfg0_pen_Hsp_grp_, 0);
            break;
            }
        case cThaMpegQueuePriorityHighest:
            {
            mRegFieldSet(regValue, cAf6_schevcaticfg0_pen_Lsp_grp_, 0);
            mRegFieldSet(regValue, cAf6_schevcaticfg0_pen_DWRR_grp_, 0);
            mRegFieldSet(regValue, cAf6_schevcaticfg0_pen_Hsp_grp_, 1);
            break;
            }
        case cThaMpegQueuePriorityUnknown:
            {
            mRegFieldSet(regValue, cAf6_schevcaticfg0_pen_Lsp_grp_, 0);
            mRegFieldSet(regValue, cAf6_schevcaticfg0_pen_DWRR_grp_, 0);
            mRegFieldSet(regValue, cAf6_schevcaticfg0_pen_Hsp_grp_, 0);
            break;
            }
        default:
            return cAtErrorModeNotSupport;
        }

    mModuleHwWrite(self, address, regValue);
    return cAtOk;
    }

eThaMpegQueuePriority Tha60210012ModuleMpegVcgQueuePriorityGet(Tha60210012ModuleMpeg self, uint32 vcgId)
    {
    uint32 offset = Tha60210012ModuleMpegBaseAddress(self) + vcgId;
    uint32 address = offset + cAf6Reg_schevcaticfg0_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    if (mRegField(regValue, cAf6_schevcaticfg0_pen_Lsp_grp_))  return cThaMpegQueuePriorityLowest;
    if (mRegField(regValue, cAf6_schevcaticfg0_pen_DWRR_grp_)) return cThaMpegQueuePriorityMedium;
    if (mRegField(regValue, cAf6_schevcaticfg0_pen_Hsp_grp_))  return cThaMpegQueuePriorityHighest;

    return cThaMpegQueuePriorityUnknown;
    }

eAtRet Tha60210012ModuleMpegLinkQueueMaxThresholdSet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId, uint32 intThresholdInPkt, uint32 extThresholdIn128Pkts)
    {
    uint32 address, regValue;

    if (!ThresholdIsValid(intThresholdInPkt) || !ThresholdIsValid(extThresholdIn128Pkts))
        return cAtErrorOutOfRangParm;

    address = PppLinkQueueOffset(self, linkId, queueId) + cAf6Reg_cfg2qthsh_pen_Base;
    regValue = mModuleHwRead(self, address);
    mRegFieldSet(regValue, cAf6_cfg2qthsh_pen_IFFQthsh_max_, intThresholdInPkt);
    mRegFieldSet(regValue, cAf6_cfg2qthsh_pen_EFFQthsh_max_, extThresholdIn128Pkts);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

uint32 Tha60210012ModuleMpegLinkQueueMaxInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId)
    {
    uint32 address = PppLinkQueueOffset(self, linkId, queueId) + cAf6Reg_cfg2qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg2qthsh_pen_IFFQthsh_max_);
    }

uint32 Tha60210012ModuleMpegLinkQueueMaxExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId)
    {
    uint32 address = PppLinkQueueOffset(self, linkId, queueId) + cAf6Reg_cfg2qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg2qthsh_pen_EFFQthsh_max_);
    }

eAtRet Tha60210012ModuleMpegLinkQueueMinThresholdSet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId, uint32 intThresholdInPkt, uint32 extThresholdIn128Pkts)
    {
    uint32 address, regValue;

    if (!ThresholdIsValid(intThresholdInPkt) || !ThresholdIsValid(extThresholdIn128Pkts))
        return cAtErrorOutOfRangParm;

    address = PppLinkQueueOffset(self, linkId, queueId) + cAf6Reg_cfg2qthsh_pen_Base;
    regValue = mModuleHwRead(self, address);
    mRegFieldSet(regValue, cAf6_cfg2qthsh_pen_IFFQthsh_min_, intThresholdInPkt);
    mRegFieldSet(regValue, cAf6_cfg2qthsh_pen_EFFQthsh_min_, extThresholdIn128Pkts);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

uint32 Tha60210012ModuleMpegLinkQueueMinInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId)
    {
    uint32 address = PppLinkQueueOffset(self, linkId, queueId) + cAf6Reg_cfg2qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg2qthsh_pen_IFFQthsh_min_);
    }

uint32 Tha60210012ModuleMpegLinkQueueMinExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId)
    {
    uint32 address = PppLinkQueueOffset(self, linkId, queueId) + cAf6Reg_cfg2qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg2qthsh_pen_EFFQthsh_min_);
    }

eAtRet Tha60210012ModuleMpegLinkQueueQuantumSet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId, uint32 quantum)
    {
    uint32 address = PppLinkQueueOffset(self, linkId, queueId) + cAf6Reg_schelinkicfg1_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    mRegFieldSet(regValue, cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_, quantum);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

uint32 Tha60210012ModuleMpegLinkQueueQuantumGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId)
    {
    uint32 address = PppLinkQueueOffset(self, linkId, queueId) + cAf6Reg_schelinkicfg1_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_);
    }

eAtRet Tha60210012ModuleMpegLinkQueuePrioritySet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId, eThaMpegQueuePriority priority)
    {
    uint32 offset = Tha60210012ModuleMpegBaseAddress(self) + linkId;
    uint32 address = offset + cAf6Reg_schelinkicfg0_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);
    uint8  hwPriority = (uint8)(1 << queueId);
    uint8 curHwPriority;

    switch(priority)
        {
        case cThaMpegQueuePriorityLowest:
            {
            curHwPriority = (uint8)(mRegField(regValue, cAf6_schelinkicfg0_pen_Lsp_grp_) | hwPriority);
            mRegFieldSet(regValue, cAf6_schelinkicfg0_pen_Lsp_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_schelinkicfg0_pen_DWRR_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_schelinkicfg0_pen_DWRR_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_schelinkicfg0_pen_Hsp_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_schelinkicfg0_pen_Hsp_grp_, curHwPriority);
            break;
            }

        case cThaMpegQueuePriorityMedium:
            {
            curHwPriority = (uint8)(mRegField(regValue, cAf6_schelinkicfg0_pen_Lsp_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_schelinkicfg0_pen_Lsp_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_schelinkicfg0_pen_DWRR_grp_) | hwPriority);
            mRegFieldSet(regValue, cAf6_schelinkicfg0_pen_DWRR_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_schelinkicfg0_pen_Hsp_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_schelinkicfg0_pen_Hsp_grp_, curHwPriority);
            break;
            }

        case cThaMpegQueuePriorityHighest:
            {
            curHwPriority = (uint8)(mRegField(regValue, cAf6_schelinkicfg0_pen_Lsp_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_schelinkicfg0_pen_Lsp_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_schelinkicfg0_pen_DWRR_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_schelinkicfg0_pen_DWRR_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_schelinkicfg0_pen_Hsp_grp_) | hwPriority);
            mRegFieldSet(regValue, cAf6_schelinkicfg0_pen_Hsp_grp_, curHwPriority);
            break;
            }

        case cThaMpegQueuePriorityUnknown:
            {
            curHwPriority = (uint8)(mRegField(regValue, cAf6_schelinkicfg0_pen_Lsp_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_schelinkicfg0_pen_Lsp_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_schelinkicfg0_pen_DWRR_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_schelinkicfg0_pen_DWRR_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_schelinkicfg0_pen_Hsp_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_schelinkicfg0_pen_Hsp_grp_, curHwPriority);
            break;
            }
        default:
            return cAtErrorModeNotSupport;
        }

    mModuleHwWrite(self, address, regValue);
    return cAtOk;
    }

eThaMpegQueuePriority Tha60210012ModuleMpegLinkQueuePriorityGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId)
    {
    uint32 offset = Tha60210012ModuleMpegBaseAddress(self) + linkId;
    uint32 address = offset + cAf6Reg_schelinkicfg0_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    if (mRegField(regValue, cAf6_schevcaticfg0_pen_Lsp_grp_) & (cBit0 << queueId))  return cThaMpegQueuePriorityLowest;
    if (mRegField(regValue, cAf6_schevcaticfg0_pen_DWRR_grp_) & (cBit0 << queueId)) return cThaMpegQueuePriorityMedium;
    if (mRegField(regValue, cAf6_schevcaticfg0_pen_Hsp_grp_) & (cBit0 << queueId))  return cThaMpegQueuePriorityHighest;

    return cThaMpegQueuePriorityUnknown;
    }

eAtRet Tha60210012ModuleMpegBundleQueueEnable(Tha60210012ModuleMpeg self, uint32 bundleId, eBool enable)
    {
    uint32 address = BundleQueueOffSet(self, bundleId) + cAf6Reg_cfg0qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    mRegFieldSet(regValue, cAf6_cfg0qthsh_pen_Queue_Enb_, mBoolToBin(enable));
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

eBool Tha60210012ModuleMpegBundleQueueIsEnabled(Tha60210012ModuleMpeg self, uint32 bundleId)
    {
    uint32 address = BundleQueueOffSet(self, bundleId) + cAf6Reg_cfg0qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);
    
    return mRegField(regValue, cAf6_cfg0qthsh_pen_Queue_Enb_) ? cAtTrue : cAtFalse;
    }

eAtRet Tha60210012ModuleMpegVcgQueueEnable(Tha60210012ModuleMpeg self, uint32 vcgId, eBool enable)
    {
    uint32 address = VcgQueueOffset(self, vcgId) + cAf6Reg_cfg1qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);
    mRegFieldSet(regValue, cAf6_cfg1qthsh_pen_Queue_Enb_, mBoolToBin(enable));
    mRegFieldSet(regValue, cAf6_cfg1qthsh_pen_EFFQthsh_max_, 0x1f);
    mRegFieldSet(regValue, cAf6_cfg1qthsh_pen_EFFQthsh_min_, 0x1f);
    mRegFieldSet(regValue, cAf6_cfg1qthsh_pen_IFFQthsh_max_, 0x1f);
    mRegFieldSet(regValue, cAf6_cfg1qthsh_pen_IFFQthsh_min_, 0x1f);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

eBool Tha60210012ModuleMpegVcgQueueIsEnabled(Tha60210012ModuleMpeg self, uint32 vcgId)
    {
    uint32 address = VcgQueueOffset(self, vcgId) + cAf6Reg_cfg1qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);
    
    return mRegField(regValue, cAf6_cfg1qthsh_pen_Queue_Enb_) ? cAtTrue : cAtFalse;
    }

eAtRet Tha60210012ModuleMpegLinkQueueEnable(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId, eBool enable)
    {
    uint32 address = PppLinkQueueOffset(self, linkId, queueId) + cAf6Reg_cfg2qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    mRegFieldSet(regValue, cAf6_cfg2qthsh_pen_Queue_Enb_, mBoolToBin(enable));
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

eBool Tha60210012ModuleMpegLinkQueueIsEnabled(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId)
    {
    uint32 address = PppLinkQueueOffset(self, linkId, queueId) + cAf6Reg_cfg2qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);
    
    return mRegField(regValue, cAf6_cfg2qthsh_pen_Queue_Enb_) ? cAtTrue : cAtFalse;
    }

uint32 Tha60210012ModuleMpegDebugCounterGet(Tha60210012ModuleMpeg self, uint32 counterId, eBool r2c)
    {
    uint32 offset = Tha60210012ModuleMpegBaseAddress(self) + counterId;
    uint32 address = offset + cAf6Reg_glbcnt_debug_Base;
    uint32 regValue = mModuleHwRead(self, address);

    if (r2c)
        mModuleHwWrite(self, address, 0);

    return mRegField(regValue, cAf6_glbcnt_debug_glbcnt_);
    }

eAtRet Tha60210012ModuleMpegEthBypassQueueMaxThresholdSet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId, uint32 intThresholdInPkt, uint32 extThresholdIn128Pkts)
    {
    uint32 address, regValue;

    if (!ThresholdIsValid(intThresholdInPkt) || !ThresholdIsValid(extThresholdIn128Pkts))
        return cAtErrorOutOfRangParm;

    address = EthBypassQueueOffset(self, portId, queueId) + cAf6Reg_cfg3qthsh_pen_Base;
    regValue = mModuleHwRead(self, address);
    mRegFieldSet(regValue, cAf6_cfg3qthsh_pen_IFFQthsh_max_, intThresholdInPkt);
    mRegFieldSet(regValue, cAf6_cfg3qthsh_pen_EFFQthsh_max_, extThresholdIn128Pkts);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

uint32 Tha60210012ModuleMpegEthBypassQueueMaxInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId)
    {
    uint32 address = EthBypassQueueOffset(self, portId, queueId) + cAf6Reg_cfg3qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);
    return mRegField(regValue, cAf6_cfg3qthsh_pen_IFFQthsh_max_);
    }

uint32 Tha60210012ModuleMpegEthBypassQueueMaxExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId)
    {
    uint32 address = EthBypassQueueOffset(self, portId, queueId) + cAf6Reg_cfg3qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg3qthsh_pen_EFFQthsh_max_);
    }

eAtRet Tha60210012ModuleMpegEthBypassQueueMinThresholdSet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId, uint32 intThresholdInPkt, uint32 extThresholdIn128Pkts)
    {
    uint32 address, regValue;

    if (!ThresholdIsValid(intThresholdInPkt) || !ThresholdIsValid(extThresholdIn128Pkts))
        return cAtErrorOutOfRangParm;

    address = EthBypassQueueOffset(self, portId, queueId) + cAf6Reg_cfg3qthsh_pen_Base;
    regValue = mModuleHwRead(self, address);
    mRegFieldSet(regValue, cAf6_cfg3qthsh_pen_IFFQthsh_min_, intThresholdInPkt);
    mRegFieldSet(regValue, cAf6_cfg3qthsh_pen_EFFQthsh_min_, extThresholdIn128Pkts);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

uint32 Tha60210012ModuleMpegEthBypassQueueMinInternalThresholdGet(Tha60210012ModuleMpeg self, uint32 linkId, uint32 queueId)
    {
    uint32 address = EthBypassQueueOffset(self, linkId, queueId) + cAf6Reg_cfg3qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg3qthsh_pen_IFFQthsh_min_);
    }

uint32 Tha60210012ModuleMpegEthBypassQueueMinExternalThresholdGet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId)
    {
    uint32 address = EthBypassQueueOffset(self, portId, queueId) + cAf6Reg_cfg3qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg3qthsh_pen_EFFQthsh_min_);
    }

eAtRet Tha60210012ModuleMpegEthByPassQueueQuantumSet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId, uint32 quantum)
    {
    uint32 address = EthBypassQueueOffset(self, portId, queueId) + cAf6Reg_scheethercfg1_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    mRegFieldSet(regValue, cAf6_scheethercfg1_pen_Ethershce_DWRR_QTum_, quantum);
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

uint32 Tha60210012ModuleMpegEthBypassQueueQuantumGet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId)
    {
    uint32 address = EthBypassQueueOffset(self, portId, queueId) + cAf6Reg_scheethercfg1_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_scheethercfg1_pen_Ethershce_DWRR_QTum_);
    }

eAtRet Tha60210012ModuleMpegEthBypassQueuePrioritySet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId, eThaMpegQueuePriority priority)
    {
    uint32 offset = Tha60210012ModuleMpegBaseAddress(self) + portId;
    uint32 address = offset + cAf6Reg_scheetheicfg0_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);
    uint8  hwPriority = (uint8)(1 << queueId);
    uint8 curHwPriority;

    switch(priority)
        {
        case cThaMpegQueuePriorityLowest:
            {
            curHwPriority = (uint8)(mRegField(regValue, cAf6_scheetheicfg0_pen_Lsp_grp_) | (uint8)(hwPriority));;
            mRegFieldSet(regValue, cAf6_scheetheicfg0_pen_Lsp_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_scheetheicfg0_pen_DWRR_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_scheetheicfg0_pen_DWRR_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_scheetheicfg0_pen_Hsp_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_scheetheicfg0_pen_Hsp_grp_, curHwPriority);
            break;
            }

        case cThaMpegQueuePriorityMedium:
            {
            curHwPriority = (uint8)(mRegField(regValue, cAf6_scheetheicfg0_pen_Lsp_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_scheetheicfg0_pen_Lsp_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_scheetheicfg0_pen_DWRR_grp_) | (uint8)(hwPriority));;
            mRegFieldSet(regValue, cAf6_scheetheicfg0_pen_DWRR_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_scheetheicfg0_pen_Hsp_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_scheetheicfg0_pen_Hsp_grp_, curHwPriority);
            break;
            }

        case cThaMpegQueuePriorityHighest:
            {
            curHwPriority = (uint8)(mRegField(regValue, cAf6_scheetheicfg0_pen_Lsp_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_scheetheicfg0_pen_Lsp_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_scheetheicfg0_pen_DWRR_grp_) & (uint8)(~hwPriority));
            mRegFieldSet(regValue, cAf6_scheetheicfg0_pen_DWRR_grp_, curHwPriority);
            curHwPriority = (uint8)(mRegField(regValue, cAf6_scheetheicfg0_pen_Hsp_grp_) | (uint8)(hwPriority));;
            mRegFieldSet(regValue, cAf6_scheetheicfg0_pen_Hsp_grp_, curHwPriority);
            break;
            }

        case cThaMpegQueuePriorityUnknown:
        default:
            return cAtErrorModeNotSupport;
        }

    mModuleHwWrite(self, address, regValue);
    return cAtOk;
    }

eThaMpegQueuePriority Tha60210012ModuleMpegEthBypassQueuePriorityGet(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId)
    {
    uint32 offset = Tha60210012ModuleMpegBaseAddress(self) + portId;
    uint32 address = offset + cAf6Reg_scheetheicfg0_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    if ((mRegField(regValue, cAf6_scheetheicfg0_pen_Lsp_grp_) >> queueId) == 1)  return cThaMpegQueuePriorityLowest;
    if ((mRegField(regValue, cAf6_scheetheicfg0_pen_DWRR_grp_) >> queueId) == 1) return cThaMpegQueuePriorityMedium;
    if ((mRegField(regValue, cAf6_scheetheicfg0_pen_Hsp_grp_) >> queueId) == 1)  return cThaMpegQueuePriorityHighest;

    return cThaMpegQueuePriorityUnknown;
    }

eAtRet Tha60210012ModuleMpegEthBypassQueueEnable(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId, eBool enable)
    {
    uint32 address = EthBypassQueueOffset(self, portId, queueId) + cAf6Reg_cfg3qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    mRegFieldSet(regValue, cAf6_cfg3qthsh_pen_Queue_Enb_, mBoolToBin(enable));
    mModuleHwWrite(self, address, regValue);

    return cAtOk;
    }

eBool Tha60210012ModuleMpegEthBypassQueueIsEnabled(Tha60210012ModuleMpeg self, uint32 portId, uint32 queueId)
    {
    uint32 address = EthBypassQueueOffset(self, portId, queueId) + cAf6Reg_cfg3qthsh_pen_Base;
    uint32 regValue = mModuleHwRead(self, address);

    return mRegField(regValue, cAf6_cfg3qthsh_pen_Queue_Enb_) ? cAtTrue : cAtFalse;
    }

eAtRet Tha60210012ModuleMpegEXauiEnable(AtModule self, uint32 exauiId, eBool enabled)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!Tha60210012DeviceEXauiIsReady(AtModuleDeviceGet(self)))
        return (enabled) ? cAtErrorNotReady : cAtOk;

    return EXauiEnable(self, exauiId, enabled);
    }

eBool Tha60210012ModuleMpegEXauiIsEnabled(AtModule self, uint32 exauiId)
    {
    if (self == NULL)
        return cAtFalse;

    if (Tha60210012DeviceEXauiIsReady(AtModuleDeviceGet(self)))
        return EXauiIsEnabled(self, exauiId);

    return cAtFalse;
    }

eBool Tha60210012ModuleMpegQueueWaitBufferIsEmpty(Tha60210012ModuleMpeg self)
    {
    AtUnused(self);
    AtOsalUSleep(20000);
    return cAtTrue;
    }

uint8 Tha60210012ModuleMpegNumQueuesForLink(Tha60210012ModuleMpeg self)
    {
    if (self)
        return mMethodsGet(self)->NumQueuesForLink(self);
    return 0;
    }

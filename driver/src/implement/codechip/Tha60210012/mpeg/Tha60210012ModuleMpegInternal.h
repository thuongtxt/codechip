/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPEG
 * 
 * File        : Tha60210012ModuleMpegInternal.h
 * 
 * Created Date: Jan 4, 2017
 *
 * Description : MPEG internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEMPEGINTERNAL_H_
#define _THA60210012MODULEMPEGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/mpeg/ThaModuleMpegInternal.h"
#include "Tha60210012ModuleMpeg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleMpegMethods
    {
    eAtRet (*EthPassthroughQueuesInit)(Tha60210012ModuleMpeg self);
    uint8 (*NumQueuesForBundle)(Tha60210012ModuleMpeg self);
    uint8 (*NumQueuesForLink)(Tha60210012ModuleMpeg self);
    uint32 (*MaxNumLinks)(Tha60210012ModuleMpeg self);
    uint32 (*BaseAddress)(Tha60210012ModuleMpeg self);
    }tTha60210012ModuleMpegMethods;

typedef struct tTha60210012ModuleMpeg
    {
    tThaModuleMpeg super;
    const tTha60210012ModuleMpegMethods * methods;

    }tTha60210012ModuleMpeg;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210012ModuleMpegObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEMPEGINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      :  
 *
 * File        : Tha60210012InterruptController.c
 *
 * Created Date: Mar 23, 2016
 *
 * Description : 60210012 interrupt controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210012InterruptControllerInternal.h"
 
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
 
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Reference to super implementation */
static const tThaIpCoreMethods *m_ThaIpCoreImplement = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool InterruptRestoreIsSupported(ThaIpCore self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanEnableHwInterruptPin(ThaIpCore self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaIpCore(ThaIntrController self)
    {
    ThaIpCore core = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(core);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));

        mMethodOverride(m_ThaIpCoreOverride, CanEnableHwInterruptPin);
        mMethodOverride(m_ThaIpCoreOverride, InterruptRestoreIsSupported);
        }

    mMethodsSet(core, &m_ThaIpCoreOverride);
    }

static void Override(ThaIntrController self)
    {
    OverrideThaIpCore(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012InterruptController);
    }

ThaIntrController Tha60210012IntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011IntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaIntrController Tha60210012IntrControllerNew(AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012IntrControllerObjectInit(newController, core);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device
 * 
 * File        : Tha60210012InterruptControllerInternal.h
 * 
 * Created Date: Oct 4, 2016
 *
 * Description : 60210012 interrupt controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012INTERRUPTCONTROLLERINTERNAL_H_
#define _THA60210012INTERRUPTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/man/Tha60210011InterruptControllerInternal.h"
#include "Tha60210012InterruptController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012InterruptController
    {
    tTha60210011InterruptController super;
    }tTha60210012InterruptController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaIntrController Tha60210012IntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012INTERRUPTCONTROLLERINTERNAL_H_ */


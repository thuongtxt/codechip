/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210012Device.c
 *
 * Created Date: Sep 8, 2015
 *
 * Description : Product 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtFrVirtualCircuit.h"
#include "AtMfrBundle.h"
#include "AtHdlcChannel.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/prbs/ThaModulePrbs.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../../../default/binder/ThaEncapBinder.h"
#include "../../Tha60210011/common/Tha602100xxCommon.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../concate/Tha60210012ModuleConcate.h"
#include "../sdh/Tha60210012ModuleSdh.h"
#include "../poh/Tha60210012ModulePoh.h"
#include "../ber/Tha60210012ModuleBer.h"
#include "../xc/Tha60210012ModuleXc.h"
#include "../pdh/Tha60210012ModulePdh.h"
#include "../mpeg/Tha60210012ModuleMpeg.h"
#include "../mpig/Tha60210012ModuleMpig.h"
#include "../ppp/Tha60210012ModulePpp.h"
#include "../cdr/Tha60210012ModuleCdr.h"
#include "../ram/Tha60210012ModuleRam.h"
#include "../encap/Tha60210012ModuleEncap.h"
#include "../encap/resequence/Tha60210012ResequenceManager.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "Tha60210012InterruptController.h"
#include "Tha60210012DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210012Device)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012DeviceMethods m_methods;

/* Override */
static tAtObjectMethods          m_AtObjectOverride;
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60210011DeviceMethods m_Tha60210011DeviceOverride;
static tTha60210051DeviceMethods m_Tha60210051DeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;

/* Super implementation */
static const tAtObjectMethods          *m_AtObjectMethods          = NULL;
static const tThaDeviceMethods         *m_ThaDeviceMethods         = NULL;
static const tAtDeviceMethods          *m_AtDeviceMethods          = NULL;
static const tTha60150011DeviceMethods *m_Tha60150011DeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = (eThaPhyModule)moduleId;

    /* Public modules */
    if (moduleId  == cAtModuleEncap)       return (AtModule)Tha60210012ModuleEncapNew(self);
    if (moduleId  == cAtModuleEth)         return (AtModule)Tha60210012ModuleEthNew(self);
    if (moduleId  == cAtModuleConcate)     return (AtModule)Tha60210012ModuleConcateNew(self);
    if (moduleId  == cAtModuleSdh)         return (AtModule)Tha60210012ModuleSdhNew(self);
    if (moduleId  == cAtModuleBer)         return (AtModule)Tha60210012ModuleBerNew(self);
    if (moduleId  == cAtModulePw)          return (AtModule)Tha60210012ModulePwNew(self);
    if (moduleId  == cAtModuleXc)          return (AtModule)Tha60210012ModuleXcNew(self);
    if (moduleId  == cAtModulePdh)         return (AtModule)Tha60210012ModulePdhNew(self);
    if (moduleId  == cAtModulePpp)         return (AtModule)Tha60210012ModulePppNew(self);
    if (moduleId  == cAtModulePrbs)        return (AtModule)Tha60210012ModulePrbsNew(self);
    if (moduleId  == cAtModulePktAnalyzer) return (AtModule)Tha60210012ModulePktAnalyzerNew(self);
    if (moduleId  == cAtModuleSur)         return (AtModule)Tha60210012ModuleSurNew(self);
    if (moduleId  == cAtModuleRam)         return (AtModule)Tha60210012ModuleRamNew(self);

    /* Physical modules */
    if (phyModule == cThaModulePwe)    return Tha60210012ModulePweNew(self);
    if (phyModule  == cThaModuleCla)   return Tha60210012ModuleClaNew(self);
    if (phyModule == cThaModuleCos)    return Tha60210012ModuleCosNew(self);
    if (phyModule == cThaModuleOcn)    return Tha60210012ModuleOcnNew(self);
    if (phyModule == cThaModulePoh)    return Tha60210012ModulePohNew(self);
    if (phyModule == cThaModulePda)    return Tha60210012ModulePdaNew(self);
    if (phyModule == cThaModuleMap)    return Tha60210012ModuleMapNew(self);
    if (phyModule == cThaModuleDemap)  return Tha60210012ModuleDemapNew(self);
    if (phyModule == cThaModuleMpeg)   return Tha60210012ModuleMpegNew(self);
    if (phyModule == cThaModuleMpig)   return Tha60210012ModuleMpigNew(self);
    if (phyModule == cThaModuleCdr)    return Tha60210012ModuleCdrNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleXc,
                                                 cAtModuleSdh,
                                                 cAtModuleSur,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleBer,
                                                 cAtModulePrbs,
                                                 cAtModuleClock,
                                                 cAtModuleEncap,
                                                 cAtModuleConcate,
                                                 cAtModulePpp};
    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleValue = moduleId;

    switch (moduleValue)
        {
        case cAtModuleConcate: return cAtTrue;
        case cAtModuleEncap  : return cAtTrue;
        case cAtModulePpp    : return cAtTrue;
        case cThaModuleMpig  : return cAtTrue;
        case cThaModuleMpeg  : return cAtTrue;
        default:
            return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
        }
    }

static ThaVersionReader VersionReaderCreate(ThaDevice self)
    {
    return Tha60210012VersionReaderNew((AtDevice)self);
    }

static uint32 StartVersionSupportDistinctErdi(AtDevice self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return 0x0;
    }

static uint32 StartVersionSupportPowerControl(AtDevice self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return 0x0;
    }

static uint32 StartVersionSupportNewPsn(AtDevice self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return 0x0;
    }

static eBool CanRunDiagnostic(AtDevice self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedToDisableJnRequestDdr(AtDevice self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static eBool XfiDiagPerGroupIsSupported(AtDevice self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportFullPwCounters(AtDevice self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return 0x0;
    }

static uint32 StartVersionSupportUart(Tha60150011Device self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x4, 0x1007);
    }

static eBool SemUartIsSupported(Tha60150011Device self)
    {
    uint32 startVersionHasThis = StartVersionSupportUart(self);
    return (AtDeviceVersionNumber((AtDevice)self) >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 StartVersionSupportSurveillance(ThaDevice self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return 0x0;
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtUnused(self);
    return Tha60210012IntrControllerNew(core);
    }

static uint8 NumLoSlice(AtDevice self)
    {
    return (uint8)(Tha60210011DeviceNumUsedOc48Slices((Tha60210011Device)self) * 2);
    }

static uint32 NumChannelPerLoSlice(Tha60210012Device self)
    {
    Tha60210011ModulePw pwModule = (Tha60210011ModulePw)AtDeviceModuleGet((AtDevice)self, cAtModulePw);
    return Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule);
    }

static AtEncapBinder EncapBinder(AtDevice self)
    {
    return ThaEncapDynamicBinderNew(NumLoSlice(self), self, mMethodsGet(mThis(self))->NumChannelPerLoSlice(mThis(self)));
    }

static eBool UseModuleDefault(void)
    {
    return Tha602100xxHwAccessOptimized();
    }

static eAtRet MapHoHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    /* FIXME: this is bad, but necessary for debugging */
    /* LO path loopback */
    ThaDeviceMemoryFlush(device, 0x100005, 0x100005, 0x0);

    /* HO MAP */
    ThaDeviceMemoryFlush(device, 0x60000, 0x601ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x64000, 0x641ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x68200, 0x68200, 0x0);
    ThaDeviceMemoryFlush(device, 0x68300, 0x68300, 0x0);
    ThaDeviceMemoryFlush(device, 0x68B00, 0x68B00, 0x0);
    ThaDeviceMemoryFlush(device, 0x68320, 0x68320, 0x0);
    ThaDeviceMemoryFlush(device, 0x68B20, 0x68B20, 0x0);

    return cAtOk;
    }

static eAtRet CdrHoHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    /* HO CDR */
    ThaDeviceMemoryFlush(device, 0x300100, 0x30011f, 0x0);
    ThaDeviceMemoryFlush(device, 0x320800, 0x32082f, 0x0);
    ThaDeviceMemoryFlush(device, 0x320840, 0x32086f, 0x0);

    return cAtOk;
    }


static eAtRet Pdh1_1HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    /* PDH: OC24#0 */
    ThaDeviceMemoryFlush(device, 0x1090002, 0x1090003, 0x0);
    ThaDeviceMemoryFlush(device, 0x1030020, 0x103003f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1024000, 0x10243ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1076000, 0x10763ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1040000, 0x104001f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1080000, 0x108001f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1041000, 0x10410ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1081000, 0x10810ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1054000, 0x10543ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1094000, 0x10943ff, 0x0);

    return cAtOk;
    }

static eAtRet Pdh1_2HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    /* PDH: OC24#01 */
    ThaDeviceMemoryFlush(device, 0x1190002, 0x1190003, 0x0);
    ThaDeviceMemoryFlush(device, 0x1130020, 0x113003f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1124000, 0x11243ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1176000, 0x11763ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1140000, 0x114001f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1180000, 0x118001f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1141000, 0x11410ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1181000, 0x11810ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1154000, 0x11543ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1194000, 0x11943ff, 0x0);

    return cAtOk;
    }

static eAtRet Pdh2_1HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    /* PDH: OC24#02 */
    ThaDeviceMemoryFlush(device, 0x1290002, 0x1290003, 0x0);
    ThaDeviceMemoryFlush(device, 0x1230020, 0x123003f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1224000, 0x12243ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1276000, 0x12763ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1240000, 0x124001f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1280000, 0x128001f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1241000, 0x12410ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1281000, 0x12810ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1254000, 0x12543ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1294000, 0x12943ff, 0x0);

    return cAtOk;
    }

static eAtRet Pdh2_2HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    /* PDH: OC24#03 */
    ThaDeviceMemoryFlush(device, 0x1390002, 0x1390003, 0x0);
    ThaDeviceMemoryFlush(device, 0x1330020, 0x133003f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1324000, 0x13243ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1376000, 0x13763ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1340000, 0x134001f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1380000, 0x138001f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1341000, 0x13410ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1381000, 0x13810ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1354000, 0x13543ff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1394000, 0x13943ff, 0x0);

    return cAtOk;
    }

static eAtRet Map1_1HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x800000, 0x800FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x801000, 0x801FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x802000, 0x802FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x803000, 0x803FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x810000, 0x8103FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x814000, 0x814FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x815000, 0x815FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x816000, 0x816FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x817000, 0x817FFF, 0x0);

    return cAtOk;
    }

static eAtRet Map1_2HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x840000, 0x840FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x841000, 0x841FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x842000, 0x842FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x843000, 0x843FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x850000, 0x8503FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x854000, 0x854FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x855000, 0x855FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x856000, 0x856FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x857000, 0x857FFF, 0x0);

    return cAtOk;
    }

static eAtRet Map2_1HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x880000, 0x880FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x881000, 0x881FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x882000, 0x882FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x883000, 0x883FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x890000, 0x8903FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x894000, 0x894FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x895000, 0x895FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x896000, 0x896FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x897000, 0x897FFF, 0x0);

    return cAtOk;
    }

static eAtRet Map2_2111HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x8c0000, 0x8c05FF, 0x0);
    return cAtOk;
    }

static eAtRet Map2_2112HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x8c0600, 0x8c0BFF, 0x0);
    return cAtOk;
    }

static eAtRet Map2_2113HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x8c0C00, 0x8c0FFF, 0x0);
    return cAtOk;
    }

static eAtRet Map2_212HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x8c1000, 0x8c1FFF, 0x0);
    return cAtOk;
    }

static eAtRet Map2_22HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x8c2000, 0x8c2FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x8c3000, 0x8c3FFF, 0x0);
    return cAtOk;
    }

static eAtRet Map2_23HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x8d0000, 0x8d03FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x8d4000, 0x8d4FFF, 0x0);
    return cAtOk;
    }

static eAtRet Map2_24HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x8d5000, 0x8d5FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x8d6000, 0x8d6FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x8d7000, 0x8d7FFF, 0x0);
    return cAtOk;
    }

static eAtRet Cdr1_1HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryFlush(device, 0xc00800, 0xc0081f, 0x0);
    ThaDeviceMemoryFlush(device, 0xc00c00, 0xc00fff, 0x0);
    ThaDeviceMemoryFlush(device, 0xc20800, 0xc20bff, 0x0);
    ThaDeviceMemoryFlush(device, 0xc21000, 0xc213ff, 0x0);

    return cAtOk;
    }

static eAtRet Cdr1_2HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryFlush(device, 0xc40800, 0xc4081f, 0x0);
    ThaDeviceMemoryFlush(device, 0xc40c00, 0xc40fff, 0x0);
    ThaDeviceMemoryFlush(device, 0xc60800, 0xc60bff, 0x0);
    ThaDeviceMemoryFlush(device, 0xc61000, 0xc613ff, 0x0);

    return cAtOk;
    }

static eAtRet Cdr2_1HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryFlush(device, 0xc80800, 0xc8081f, 0x0);
    ThaDeviceMemoryFlush(device, 0xc80c00, 0xc80fff, 0x0);
    ThaDeviceMemoryFlush(device, 0xca0800, 0xca0bff, 0x0);
    ThaDeviceMemoryFlush(device, 0xca1000, 0xca13ff, 0x0);

    return cAtOk;
    }

static eAtRet Cdr2_2HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryFlush(device, 0xcc0800, 0xcc081f, 0x0);
    ThaDeviceMemoryFlush(device, 0xcc0c00, 0xcc0fff, 0x0);
    ThaDeviceMemoryFlush(device, 0xce0800, 0xce0bff, 0x0);
    ThaDeviceMemoryFlush(device, 0xce1000, 0xce13ff, 0x0);

    return cAtOk;
    }

static eAtRet PweHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    /* PWE */
    ThaDeviceMemoryFlush(device, 0x0070000, 0x0070FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x0071000, 0x0071FFF, 0x0);

    return cAtOk;
    }

static eAtRet PlaHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    /* Payload Assembler Low-Order(LO) Payload Control */
    ThaDeviceMemoryFlush(device, 0x401000, 0x4013FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x403000, 0x4033FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x409000, 0x4093FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x40B000, 0x40B3FF, 0x0);

    /* Payload Assembler Low-Order(LO) Add/Remove Pseudowire Protocol Control */
    ThaDeviceMemoryFlush(device, 0x401800, 0x401BFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x403800, 0x403BFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x409800, 0x409BFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x40B800, 0x40BBFF, 0x0);

    /* Payload Assembler Low-Order(LO) Lookup Control */
    ThaDeviceMemoryFlush(device, 0x404000, 0x4043FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x404400, 0x4047FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x40C000, 0x40C3FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x40C400, 0x40C7FF, 0x0);

    /* Payload Assembler High-Order(HO) Payload Control */
    ThaDeviceMemoryFlush(device, 0x410080, 0x4100AF, 0x0);
    ThaDeviceMemoryFlush(device, 0x410180, 0x4101AF, 0x0);

    /* Payload Assembler High-Order(HO) Add/Remove Pseudowire Protocol Control */
    ThaDeviceMemoryFlush(device, 0x4100C0, 0x4100EF, 0x0);
    ThaDeviceMemoryFlush(device, 0x4101C0, 0x4101EF, 0x0);

    /* Payload Assembler High-Order(HO) Lookup Control */
    ThaDeviceMemoryFlush(device, 0x410200, 0x41022F, 0x0);
    ThaDeviceMemoryFlush(device, 0x410240, 0x41026F, 0x0);

    /* Payload Assembler Common Pseudowire Control */
    ThaDeviceMemoryFlush(device, 0x418000, 0x418BFF, 0x0);

    /* Payload Assembler Protection Flow Control */
    ThaDeviceMemoryFlush(device, 0x430000, 0x430FFF, 0x0);

    /* Payload Assembler Flow UPSR Control */
    ThaDeviceMemoryFlush(device, 0x434000, 0x434FFF, 0x0);

    /* Payload Assembler Flow HSPW Control */
    ThaDeviceMemoryFlush(device, 0x438000, 0x4387FF, 0x0);

    /* Payload Assembler Block Buffer Control */
    ThaDeviceMemoryFlush(device, 0x424000, 0x4263FF, 0x0);

    /* Payload Assembler Lookup Flow Control */
    ThaDeviceMemoryFlush(device, 0x42C000, 0x42CCFF, 0x0);

    /* Payload Assembler Output Flow Control */
    ThaDeviceMemoryFlush(device, 0x428000, 0x428FFF, 0x0);

    /* Payload Assembler Block Buffer Control */
    ThaDeviceMemoryFlush(device, 0x424000, 0x4263FF, cBit14_0);

    return cAtOk;
    }

static eAtRet PdaHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (Tha602100xxHwAccessOptimized())
        return cAtOk;

    /* Pseudowire PDA Jitter Buffer Control   */
    ThaDeviceMemoryLongFlush(device, 0x00510000, 0x00510BFF);

    /* Pseudowire PDA Reorder Control */
    ThaDeviceMemoryFlush(device, 0x00520000, 0x00520BFF, 0);

    /* Pseudowire PDA per OC48 Low Order TDM Mode Control */
    ThaDeviceMemoryFlush(device, 0x00530000, 0x005303FF, 0);
    ThaDeviceMemoryFlush(device, 0x00530400, 0x005307FF, 0);
    ThaDeviceMemoryFlush(device, 0x00531000, 0x005313FF, 0);
    ThaDeviceMemoryFlush(device, 0x00531400, 0x005317FF, 0);

    /* Pseudowire PDA Low Order TDM or MLPPP Look Up Control */
    ThaDeviceMemoryFlush(device, 0x00540000, 0x005403FF, 0);
    ThaDeviceMemoryFlush(device, 0x00540400, 0x005407FF, 0);
    ThaDeviceMemoryFlush(device, 0x00540800, 0x00540BFF, 0);
    ThaDeviceMemoryFlush(device, 0x00540C00, 0x00540FFF, 0);

    /* Pseudowire PDA VCAT Look Up Control */
    ThaDeviceMemoryFlush(device, 0x00541000, 0x005410FF, 0);

    /* Pseudowire PDA High Order TDM Look Up Control */
    ThaDeviceMemoryFlush(device, 0x00541100, 0x0054112F, 0);
    ThaDeviceMemoryFlush(device, 0x00541140, 0x0054116F, 0);

    /* Pseudowire PDA per OC48 High Order TDM Mode Control */
    ThaDeviceMemoryFlush(device, 0x00550000, 0x0055002F, 0);
    ThaDeviceMemoryFlush(device, 0x00550100, 0x0055012F, 0);
    return cAtOk;
    }

static eAtRet OcnHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    /* OCN */
    ThaDeviceMemoryFlush(device, 0x00160800, 0x00160DFF, 0);
    ThaDeviceMemoryFlush(device, 0x00164800, 0x00164DFF, 0);
    ThaDeviceMemoryFlush(device, 0x00168800, 0x00168DFF, 0);
    ThaDeviceMemoryFlush(device, 0x0016C800, 0x0016CDFF, 0);

    ThaDeviceMemoryFlush(device, 0x00123000, 0x0012302f, 0);
    ThaDeviceMemoryFlush(device, 0x00123200, 0x0012322f, 0);
    ThaDeviceMemoryFlush(device, 0x00123400, 0x0012342f, 0);
    ThaDeviceMemoryFlush(device, 0x00123600, 0x0012362f, 0);
    return cAtOk;
    }

static eAtRet ClaHbceLookupExtra0_31HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryLongFlush(device, 0x640000, 0x6407FF);
    ThaDeviceMemoryLongFlush(device, 0x641000, 0x6417FF);
    return cAtOk;
    }

static eAtRet ClaHbceLookupExtra0_32HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryLongFlush(device, 0x642000, 0x6427FF);
    ThaDeviceMemoryLongFlush(device, 0x643000, 0x6437FF);
    return cAtOk;
    }

static eAtRet ClaHbceLookupExtra4_71HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryLongFlush(device, 0x644000, 0x6447FF);
    ThaDeviceMemoryLongFlush(device, 0x645000, 0x6457FF);
    return cAtOk;
    }

static eAtRet ClaHbceLookupExtra4_72HwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryLongFlush(device, 0x646000, 0x6467FF);
    ThaDeviceMemoryLongFlush(device, 0x647000, 0x6477FF);
    return cAtOk;
    }

static eAtRet ClaHbceHashTableHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryFlush(device, 0x620000, 0x621FFF, 0);
    return cAtOk;
    }

static eAtRet ClaHbceLookupCollisionHwFlush(Tha60210011Device self, uint32 start, uint32 stop)
    {
    if (UseModuleDefault())
        return cAtOk;

    ThaDeviceMemoryLongFlush((ThaDevice)self, start, stop);

    return cAtOk;
    }

static eAtRet ClaHbceLookupCollision01HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x680000, 0x6808FF);
    }

static eAtRet ClaHbceLookupCollision02HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x680900, 0x680FFF);
    }

static eAtRet ClaHbceLookupCollision03HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x681000, 0x6818FF);
    }

static eAtRet ClaHbceLookupCollision04HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x681900, 0x681FFF);
    }

static eAtRet ClaHbceLookupCollision11HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x690000, 0x6908FF);
    }

static eAtRet ClaHbceLookupCollision12HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x690900, 0x690FFF);
    }

static eAtRet ClaHbceLookupCollision13HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x691000, 0x6918FF);
    }

static eAtRet ClaHbceLookupCollision14HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x691900, 0x691FFF);
    }

static eAtRet ClaHbceLookupCollision21HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x6A0000, 0x6A08FF);
    }

static eAtRet ClaHbceLookupCollision22HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x6A0900, 0x6A0FFF);
    }

static eAtRet ClaHbceLookupCollision23HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x6A1000, 0x6A18FF);
    }

static eAtRet ClaHbceLookupCollision24HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x6A19FF, 0x6A1FFF);
    }

static eAtRet ClaHbceLookupCollision31HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x6B0000, 0x6B08FF);
    }

static eAtRet ClaHbceLookupCollision32HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x6B0900, 0x6B0FFF);
    }

static eAtRet ClaHbceLookupCollision33HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x6B1000, 0x6B18FF);
    }

static eAtRet ClaHbceLookupCollision34HwFlush(Tha60210011Device self)
    {
    return ClaHbceLookupCollisionHwFlush(self, 0x6B1900, 0x6B1FFF);
    }

static eAtRet ClaHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!UseModuleDefault())
        ThaDeviceMemoryFlush(device, 0x670000, 0x670FFF, 0);

    ThaDeviceMemoryFlush(device, 0x650000, 0x650FFF, 0);
    ThaDeviceMemoryFlush(device, 0x6D0000, 0x6D0000, 0);
    ThaDeviceMemoryFlush(device, 0x6C0000, 0x6C0BFF, 0);
    ThaDeviceMemoryFlush(device, 0x60D000, 0x60DFFF, 0);

    if (Tha602100xxHwAccessOptimized())
        ThaDeviceMemoryFlush(device, 0x604000, 0x604FFF, 0);
    else
        {
        ThaDeviceMemoryLongFlush(device, 0x608000, 0x608BFF);
        ThaDeviceMemoryLongFlush(device, 0x604000, 0x604FFF);
        }

    ClaHbceHashTableHwFlush(self);
    ClaHbceLookupCollision01HwFlush(self);
    ClaHbceLookupCollision02HwFlush(self);
    ClaHbceLookupCollision03HwFlush(self);
    ClaHbceLookupCollision04HwFlush(self);
    ClaHbceLookupCollision11HwFlush(self);
    ClaHbceLookupCollision12HwFlush(self);
    ClaHbceLookupCollision13HwFlush(self);
    ClaHbceLookupCollision14HwFlush(self);
    ClaHbceLookupCollision21HwFlush(self);
    ClaHbceLookupCollision22HwFlush(self);
    ClaHbceLookupCollision23HwFlush(self);
    ClaHbceLookupCollision24HwFlush(self);
    ClaHbceLookupCollision31HwFlush(self);
    ClaHbceLookupCollision32HwFlush(self);
    ClaHbceLookupCollision33HwFlush(self);
    ClaHbceLookupCollision34HwFlush(self);
    ClaHbceLookupExtra0_31HwFlush(self);
    ClaHbceLookupExtra0_32HwFlush(self);
    ClaHbceLookupExtra4_71HwFlush(self);
    ClaHbceLookupExtra4_72HwFlush(self);
    return cAtOk;
    }

static eAtRet Ber1HwFlush(Tha60210011Device self)
    {
    if (!Tha602100xxHwAccessOptimized())
        ThaDeviceMemoryLongFlush((ThaDevice)self, 0x262000, 0x263FFF);
    return cAtOk;
    }

static eAtRet Ber2HwFlush(Tha60210011Device self)
    {
    if (!Tha602100xxHwAccessOptimized())
        ThaDeviceMemoryLongFlush((ThaDevice)self, 0x260400, 0x2607FF);
    return cAtOk;
    }

static eAtRet BerHwFlush(Tha60210011Device self)
    {
    Ber1HwFlush(self);
    Ber2HwFlush(self);
    return cAtOk;
    }

static eAtRet PohHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    if (Tha602100xxHwAccessOptimized())
        return cAtOk;

    ThaDeviceMemoryFlush(device, 0x240400, 0x2407FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x244000, 0x247FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x228000, 0x229FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x22A000, 0x22BFFF, 0x0);

    return cAtOk;
    }

static eAtRet VcatHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;

    ThaDeviceMemoryFlush(device, 0x1d21200, 0x1d2125f, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d21400, 0x1d2145F, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d22000, 0x1d22bff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d24000, 0x1d24bff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40800, 0x1d40817, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40840, 0x1d40857, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40900, 0x1d40917, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40940, 0x1d40957, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40a00, 0x1d40a17, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40a40, 0x1d40a57, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40b00, 0x1d40b17, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d40b40, 0x1d40b57, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d49000, 0x1d49bff, 0x0);
    ThaDeviceMemoryFlush(device, 0x1d51000, 0x1d51bff, 0x0);

    return cAtOk;
    }

static void HwFlush(Tha60150011Device self)
    {
    Tha60210011Device device = (Tha60210011Device)self;
    Map1_1HwFlush(device);
    Map1_2HwFlush(device);
    Map2_1HwFlush(device);
    Map2_2111HwFlush(device);
    Map2_2112HwFlush(device);
    Map2_2113HwFlush(device);
    Map2_212HwFlush(device);
    Map2_22HwFlush(device);
    Map2_23HwFlush(device);
    Map2_24HwFlush(device);
    Cdr1_1HwFlush(device);
    Cdr1_2HwFlush(device);
    Cdr2_1HwFlush(device);
    Cdr2_2HwFlush(device);
    Pdh1_1HwFlush(device);
    Pdh1_2HwFlush(device);
    Pdh2_1HwFlush(device);
    Pdh2_2HwFlush(device);
    mMethodsGet(device)->PweHwFlush(device);
    mMethodsGet(device)->PlaHwFlush(device);
    mMethodsGet(device)->PdaHwFlush(device);
    mMethodsGet(device)->OcnHwFlush(device);
    mMethodsGet(device)->ClaHwFlush(device);
    BerHwFlush(device);
    PohHwFlush(device);
    VcatHwFlush(device);
    }

static tAtAsyncOperationFunc NextFlushOperation(AtDevice self, uint32 state)
    {
    Tha60210012Device device = (Tha60210012Device)self;
    return mMethodsGet(device)->AsyncFlushOpGet(device, state);
    }

static eAtRet HwAsyncFlush(Tha60210011Device self)
    {
    return AtDeviceAsyncProcess((AtDevice)self, &(mThis(self)->asyncInitHwFlushState), NextFlushOperation);
    }

static uint32 StartVersionSupportEXaui(Tha60210012Device self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader((AtDevice)self), 0x16, 0x05, 0x12, 0x40);
    }

static eBool EXauiIsReady(AtDevice self)
    {
    return AtDeviceVersionNumber(self) >= mMethodsGet(mThis(self))->StartVersionSupportEXaui(mThis(self));
    }

static eBool PtchServiceIsReady(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ErrorGeneratorIsSupported(ThaDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtResequenceManager ResequenceManagerObjectCreate(ThaDevice self)
    {
    return Tha60210012ResequenceManagerNew((AtDevice)self);
    }

static eAtRet UnbindAllHdlcPws(AtDevice self)
    {
    uint16 channel_i;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(self, cAtModuleEncap);

    for (channel_i = 0; channel_i < AtModuleEncapMaxChannelsGet(encapModule); channel_i++)
        {
        AtPw pw;
        AtEncapChannel hdlcChannel = AtModuleEncapChannelGet(encapModule, channel_i);
        if (hdlcChannel && AtEncapChannelEncapTypeGet(hdlcChannel) == cAtEncapHdlc)
            {
            pw = (AtPw)AtChannelBoundPwGet((AtChannel)hdlcChannel);
            if (pw != NULL)
                {
                ThaHdlcPwAdapterCircuitUnbind((AtPw)ThaPwAdapterGet(pw));
                AtObjectDelete((AtObject)pw);
                }
            }
        }

    return cAtOk;
    }

static eAtRet UnbindAllMlpppPws(AtDevice self)
    {
    uint16 bundle_i;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(self, cAtModuleEncap);

    if (!encapModule)
        return cAtOk;

    for (bundle_i = 0; bundle_i < AtModuleEncapMaxBundlesGet(encapModule); bundle_i++)
        {
        AtPw pw;
        AtHdlcBundle bundle = AtModuleEncapBundleGet(encapModule, bundle_i);
        if (bundle)
            {
            pw = (AtPw)AtChannelBoundPwGet((AtChannel)bundle);
            if (pw != NULL)
            	{
                ThaHdlcPwAdapterCircuitUnbind((AtPw)ThaPwAdapterGet(pw));
                AtObjectDelete((AtObject)pw);
                }
            }
        }

    return cAtOk;
    }

static eAtRet UnbindAllFrVirtualCircuitFlows(AtDevice self)
    {
    uint16 idx;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(self, cAtModuleEncap);

    if (!encapModule)
        return cAtOk;

    for (idx = 0; idx < AtModuleEncapMaxChannelsGet(encapModule); idx++)
        {
        AtHdlcChannel hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, idx);
        if (hdlcChannel && AtHdlcChannelFrameTypeGet(hdlcChannel) == cAtHdlcFrmFr)
            {
            AtFrLink link = (AtFrLink)AtHdlcChannelHdlcLinkGet(hdlcChannel);
            if (link)
                {
                AtFrVirtualCircuit dlciCircuit;
                AtIterator iter = AtFrLinkVirtualCircuitIteratorGet(link);
                while ((dlciCircuit = (AtFrVirtualCircuit)AtIteratorNext(iter)) != NULL)
                    AtFrVirtualCircuitFlowBind(dlciCircuit, NULL);
                }
            }
        }

    return cAtOk;
    }

static eAtRet UnbindAllMfrVirtualCircuitFlows(AtDevice self)
    {
    uint16 idx;
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(self, cAtModuleEncap);

    if (!encapModule)
        return cAtOk;

    for (idx = 0; idx < AtModuleEncapMaxBundlesGet(encapModule); idx++)
        {
        AtHdlcBundle hdlcBundle = (AtHdlcBundle)AtModuleEncapBundleGet(encapModule, idx);
        if (hdlcBundle && AtEncapBundleTypeGet((AtEncapBundle)hdlcBundle) == cAtEncapBundleTypeMfr)
            {
            AtFrVirtualCircuit dlciCircuit;
            AtIterator iterator = AtMfrBundleVirtualCircuitIteratorGet((AtMfrBundle)hdlcBundle);
            while ((dlciCircuit = (AtFrVirtualCircuit)AtIteratorNext(iterator)) != NULL)
                AtFrVirtualCircuitFlowBind(dlciCircuit, NULL);
            }
        }

    return cAtOk;
    }

static eAtRet EthPassthroughActive(Tha60210012Device self, eBool enable)
    {
    Tha60210012ModuleEthEXauiActive((ThaModuleEth) AtDeviceModuleGet((AtDevice)self, cAtModuleEth), enable);
    return cAtOk;
    }

static eAtRet EthPassthoughActivate(Tha60210012Device self)
    {
    if (self)
        return mMethodsGet(self)->EthPassthroughActive(self, cAtTrue);
    return cAtErrorNullPointer;
    }

static eAtRet EthPassthoughDeactivate(Tha60210012Device self)
    {
    if (self)
        return mMethodsGet(self)->EthPassthroughActive(self, cAtFalse);
    return cAtErrorNullPointer;
    }

static eAtRet AllServiceRemove(Tha60210012Device self)
    {
    ThaModuleEth ethModule = (ThaModuleEth) AtDeviceModuleGet((AtDevice)self, cAtModuleEth);

    if (!ethModule)
        return cAtOk;

    return Tha60210012ModuleEthAllServicesRemove(ethModule);
    }

static eAtRet ResequenceManagerCreate(Tha60210012Device self)
    {
    if (!mMethodsGet((ThaDevice)self)->ResequenceManagerIsSupported((ThaDevice)self))
        return cAtOk;

    if (ThaDeviceResequenceManagerCreate((ThaDevice)self) == NULL)
        return cAtErrorNullPointer;

    return cAtOk;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret;
    Tha60210012Device device = mThis(self);

    /* HW recommended, this must be done at the first step */
    EthPassthoughDeactivate(device);

    ret = AllServiceRemove(device);
    if (ret != cAtOk)
        return ret;

    /* Unbind all Hdlc Pws */
    UnbindAllHdlcPws(self);
    UnbindAllMlpppPws(self);

    /* Unbind all Virtual Circuits FLow */
    UnbindAllFrVirtualCircuitFlows(self);
    UnbindAllMfrVirtualCircuitFlows(self);

    ret = m_AtDeviceMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Active EXAUI */
    EthPassthoughActivate(device);

    return ResequenceManagerCreate(device);
    }

static eAtRet SuperAsyncInit(AtObject self)
    {
    return m_AtDeviceMethods->AsyncInit((AtDevice)self);
    }

static tAtAsyncOperationFunc* AllAsyncOps(uint32 *numOps)
    {
    static tAtAsyncOperationFunc m_AsyncOperations[] = {(tAtAsyncOperationFunc)EthPassthoughDeactivate,
                                                        (tAtAsyncOperationFunc)AllServiceRemove,
                                                        (tAtAsyncOperationFunc)UnbindAllHdlcPws,
                                                        (tAtAsyncOperationFunc)UnbindAllFrVirtualCircuitFlows,
                                                        (tAtAsyncOperationFunc)UnbindAllMfrVirtualCircuitFlows,
                                                        (tAtAsyncOperationFunc)SuperAsyncInit,
                                                        (tAtAsyncOperationFunc)EthPassthoughActivate,
                                                        (tAtAsyncOperationFunc)ResequenceManagerCreate};

    if (numOps) *numOps = mCount(m_AsyncOperations);
    return m_AsyncOperations;
    }

static uint32 NumAsyncOperation(AtDevice self)
    {
    uint32 numOps;
    AtUnused(self);

    AllAsyncOps(&numOps);
    return numOps;
    }

static tAtAsyncOperationFunc NextInitOperation(AtDevice self, uint32 state)
    {
    if (state >= NumAsyncOperation(self))
        return NULL;

    return AllAsyncOps(NULL)[state];
    }

static eAtRet AsyncInit(AtDevice self)
    {
    return AtDeviceAsyncProcess(self, &(self->asyncInitState), NextInitOperation);
    }

static eBool RamErrorGeneratorIsSupported(AtDevice self)
    {
    AtUnused(self);
    /* Will be opened when HW ready */
    return cAtFalse;
    }

static tAtAsyncOperationFunc AsyncFlushOpGet(Tha60210012Device self, uint32 state)
    {
    static tAtAsyncOperationFunc functions[] = {(tAtAsyncOperationFunc)MapHoHwFlush,
                                                (tAtAsyncOperationFunc)CdrHoHwFlush,
                                                (tAtAsyncOperationFunc)Map1_1HwFlush,
                                                (tAtAsyncOperationFunc)Map1_2HwFlush,
                                                (tAtAsyncOperationFunc)Map2_1HwFlush,
                                                (tAtAsyncOperationFunc)Map2_2111HwFlush,
                                                (tAtAsyncOperationFunc)Map2_2112HwFlush,
                                                (tAtAsyncOperationFunc)Map2_2113HwFlush,
                                                (tAtAsyncOperationFunc)Map2_212HwFlush,
                                                (tAtAsyncOperationFunc)Map2_22HwFlush,
                                                (tAtAsyncOperationFunc)Map2_23HwFlush,
                                                (tAtAsyncOperationFunc)Map2_24HwFlush,
                                                (tAtAsyncOperationFunc)Cdr1_1HwFlush,
                                                (tAtAsyncOperationFunc)Cdr1_2HwFlush,
                                                (tAtAsyncOperationFunc)Cdr2_1HwFlush,
                                                (tAtAsyncOperationFunc)Cdr2_2HwFlush,
                                                (tAtAsyncOperationFunc)Pdh1_1HwFlush,
                                                (tAtAsyncOperationFunc)Pdh1_2HwFlush,
                                                (tAtAsyncOperationFunc)Pdh2_1HwFlush,
                                                (tAtAsyncOperationFunc)Pdh2_2HwFlush,
                                                (tAtAsyncOperationFunc)PweHwFlush,
                                                (tAtAsyncOperationFunc)PlaHwFlush,
                                                (tAtAsyncOperationFunc)PdaHwFlush,
                                                (tAtAsyncOperationFunc)OcnHwFlush,
                                                (tAtAsyncOperationFunc)ClaHwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceHashTableHwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision01HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision02HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision03HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision04HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision11HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision12HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision13HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision14HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision21HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision22HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision23HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision24HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision31HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision32HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision33HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupCollision34HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupExtra0_31HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupExtra0_32HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupExtra4_71HwFlush,
                                                (tAtAsyncOperationFunc)ClaHbceLookupExtra4_72HwFlush,
                                                (tAtAsyncOperationFunc)Ber1HwFlush,
                                                (tAtAsyncOperationFunc)Ber2HwFlush,
                                                (tAtAsyncOperationFunc)PohHwFlush,
                                                (tAtAsyncOperationFunc)VcatHwFlush};

    AtUnused(self);
    if (state >= mCount(functions))
        return NULL;

    return functions[state];
    }

static void MethodsInit(Tha60210012Device self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, AsyncFlushOpGet);
        mMethodOverride(m_methods, StartVersionSupportEXaui);
        mMethodOverride(m_methods, EthPassthroughActive);
        mMethodOverride(m_methods, NumChannelPerLoSlice);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210012Device object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(asyncInitHwFlushState);
    }

static uint32 StartVersionApplyNewReset(Tha60210051Device self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x9, 0x1011);
    }

static eBool EthIntfCheckingIsSupported(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaModuleEncap ModuleEncap(ThaDevice self)
    {
    return (ThaModuleEncap)AtDeviceModuleGet((AtDevice)self, cAtModuleEncap);
    }

static eBool ResequenceManagerIsSupported(ThaDevice self)
    {
    ThaModuleEncap encap = ModuleEncap(self);

    if (!encap)
        return cAtFalse;

    return Tha60210012ModuleEncapResequenceManagerIsSupported(encap);
    }

static void OverrideAtObject(AtDevice self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(m_AtDeviceOverride));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, EncapBinder);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, AsyncInit);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        mMethodOverride(m_ThaDeviceOverride, VersionReaderCreate);
        mMethodOverride(m_ThaDeviceOverride, StartVersionSupportSurveillance);
        mMethodOverride(m_ThaDeviceOverride, ErrorGeneratorIsSupported);
        mMethodOverride(m_ThaDeviceOverride, ResequenceManagerObjectCreate);
        mMethodOverride(m_ThaDeviceOverride, ResequenceManagerIsSupported);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideTha60210011Device(AtDevice self)
    {
    Tha60210011Device this = (Tha60210011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011DeviceOverride, mMethodsGet(this), sizeof(m_Tha60210011DeviceOverride));

        mMethodOverride(m_Tha60210011DeviceOverride, CanRunDiagnostic);
        mMethodOverride(m_Tha60210011DeviceOverride, NeedToDisableJnRequestDdr);
        mMethodOverride(m_Tha60210011DeviceOverride, XfiDiagPerGroupIsSupported);
        mMethodOverride(m_Tha60210011DeviceOverride, StartVersionSupportFullPwCounters);
        mMethodOverride(m_Tha60210011DeviceOverride, PweHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, ClaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, PdaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, PlaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, OcnHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, HwAsyncFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, RamErrorGeneratorIsSupported);
        mMethodOverride(m_Tha60210011DeviceOverride, EthIntfCheckingIsSupported);
        }

    mMethodsSet(this, &m_Tha60210011DeviceOverride);
    }

static void OverrideTha60210051Device(AtDevice self)
    {
    Tha60210051Device device = (Tha60210051Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210051DeviceOverride));

        mMethodOverride(m_Tha60210051DeviceOverride, StartVersionSupportDistinctErdi);
        mMethodOverride(m_Tha60210051DeviceOverride, StartVersionSupportPowerControl);
        mMethodOverride(m_Tha60210051DeviceOverride, StartVersionSupportNewPsn);
        mMethodOverride(m_Tha60210051DeviceOverride, StartVersionApplyNewReset);
        }

    mMethodsSet(device, &m_Tha60210051DeviceOverride);
    }

static void OverrideTha6015011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60150011DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, SemUartIsSupported);
        mMethodOverride(m_Tha60150011DeviceOverride, HwFlush);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtObject(self);
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60210011Device(self);
    OverrideTha60210051Device(self);
    OverrideTha6015011Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012Device);
    }

AtDevice Tha60210012DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60210012DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012DeviceObjectInit(newDevice, driver, productCode);
    }

eBool Tha60210012DeviceEXauiIsReady(AtDevice self)
    {
    if (self)
        return EXauiIsReady(self);
    return cAtFalse;
    }

eBool Tha60210012DevicePtchServiceIsReady(AtDevice self)
    {
    if (self)
        return PtchServiceIsReady(self);
    return cAtFalse;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210012DeviceRegister.c
 *
 * Created Date: Apr 12, 2016
 *
 * Description : Product 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "Tha60210012Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool Tha60210012IsCdrHoRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0300000, 0x033FFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210012IsCdrLoRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);

    if (!mOutOfRange(address, 0x0C00000, 0x0C3FFFF) ||
        !mOutOfRange(address, 0x0C40000, 0x0C7FFFF) ||
        !mOutOfRange(address, 0x0C80000, 0x0CBFFFF) ||
        !mOutOfRange(address, 0x0CC0000, 0x0CFFFFF) ||
        !mOutOfRange(address, 0x0D00000, 0x0D3FFFF) ||
        !mOutOfRange(address, 0x0D40000, 0x0D7FFFF) ||
        !mOutOfRange(address, 0x0D80000, 0x0DBFFFF) ||
        !mOutOfRange(address, 0x0DC0000, 0x0DFFFFF))
        return cAtTrue;
    return cAtFalse;
    }

eBool Tha60210012IsCdrRegister(AtDevice self, uint32 address)
    {
    return Tha60210012IsCdrHoRegister(self, address) || Tha60210012IsCdrLoRegister(self, address);
    }

eBool Tha60210012IsClaRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0600000, 0x06FFFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210012IsEthRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);

    if (!mOutOfRange(address, 0x0021000, 0x002FFFF) ||
        !mOutOfRange(address, 0x0031000, 0x003FFFF) ||
        !mOutOfRange(address, 0x0041000, 0x004FFFF) ||
        !mOutOfRange(address, 0x0051000, 0x005FFFF) ||
        !mOutOfRange(address, 0xF00040, 0xF00041))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210012IsMapHoRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0060000, 0x006FFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210012IsMapLoRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    if (!mOutOfRange(address, 0x0800000, 0x083FFFF) ||
        !mOutOfRange(address, 0x0840000, 0x087FFFF) ||
        !mOutOfRange(address, 0x0880000, 0x08BFFFF) ||
        !mOutOfRange(address, 0x08C0000, 0x08FFFFF) ||
        !mOutOfRange(address, 0x0900000, 0x093FFFF) ||
        !mOutOfRange(address, 0x0940000, 0x097FFFF) ||
        !mOutOfRange(address, 0x0980000, 0x09BFFFF) ||
        !mOutOfRange(address, 0x09C0000, 0x09FFFFF))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210012IsMapRegister(AtDevice self, uint32 address)
    {
    return Tha60210012IsMapHoRegister(self, address) || Tha60210012IsMapLoRegister(self, address);
    }

eBool Tha60210012IsDemapRegister(AtDevice self, uint32 address)
    {
    return Tha60210012IsMapRegister(self, address);
    }

eBool Tha60210012IsOcnRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0100000, 0x01FFFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210012IsPdhRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    if (!mOutOfRange(address, 0x1000000, 0x10FFFFF) ||
        !mOutOfRange(address, 0x1100000, 0x11FFFFF) ||
        !mOutOfRange(address, 0x1200000, 0x12FFFFF) ||
        !mOutOfRange(address, 0x1300000, 0x13FFFFF) ||
        !mOutOfRange(address, 0x1400000, 0x14FFFFF) ||
        !mOutOfRange(address, 0x1500000, 0x15FFFFF) ||
        !mOutOfRange(address, 0x1600000, 0x16FFFFF) ||
        !mOutOfRange(address, 0x1700000, 0x17FFFFF))
        return cAtTrue;
    return cAtFalse;
    }

eBool Tha60210012IsPohRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0200000, 0x02FFFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210012IsPlaRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0400000, 0x04FFFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210012IsPweRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    if (!mOutOfRange(address, 0x0070000, 0x007FFFF) ||
        !mOutOfRange(address, 0x0080000, 0x008FFFF) ||
        !mOutOfRange(address, 0x0090000, 0x009FFFF) ||
        !mOutOfRange(address, 0x00A0000, 0x00AFFFF))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210012IsPdaRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);

    if (!mOutOfRange(address, 0x0500000, 0x05FFFFF))
        return cAtTrue;
    return cAtFalse;
    }

eBool Tha60210012IsBerRegister(AtDevice self, uint32 address)
    {
    return Tha60210012IsPohRegister(self, address);
    }

eBool Tha60210012IsPmcRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0700000, 0x07FFFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210012IsEncapHoRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    if (!mOutOfRange(address, 0x0E30000, 0x0E30FFF) ||
        !mOutOfRange(address, 0x0E31000, 0x0E31FFF) ||
        !mOutOfRange(address, 0x0E34000, 0x0E34FFF) ||
        !mOutOfRange(address, 0x0E35000, 0x0E35FFF))
        return cAtTrue;
    return cAtFalse;
    }

eBool Tha60210012IsEncapLoRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    if (!mOutOfRange(address, 0x0E00000, 0x0E03FFF) ||
        !mOutOfRange(address, 0x0E04000, 0x0E07FFF) ||
        !mOutOfRange(address, 0x0E08000, 0x0E0BFFF) ||
        !mOutOfRange(address, 0x0E0C000, 0x0E0FFFF) ||
        !mOutOfRange(address, 0x0E20000, 0x0E23FFF) ||
        !mOutOfRange(address, 0x0E24000, 0x0E27FFF) ||
        !mOutOfRange(address, 0x0E28000, 0x0E2BFFF) ||
        !mOutOfRange(address, 0x0E2C000, 0x0E2FFFF))
        return cAtTrue;
    return cAtFalse;
    }

eBool Tha60210012IsEncapRegister(AtDevice self, uint32 address)
    {
    return Tha60210012IsEncapHoRegister(self, address) || Tha60210012IsEncapLoRegister(self, address);
    }

eBool Tha60210012IsDecapRegister(AtDevice self, uint32 address)
    {
    return Tha60210012IsEncapRegister(self, address);
    }

eBool Tha60210012IsMpegRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x1800000, 0x18FFFFF) ? cAtFalse : cAtTrue;
    }

eBool Tha60210012IsMpigRegister(AtDevice self, uint32 address)
    {
    AtUnused(self);
    return mOutOfRange(address, 0x0A20000, 0x0A2FFFF) ? cAtFalse : cAtTrue;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210012DeviceInternal.h
 * 
 * Created Date: Jun 16, 2016
 *
 * Description : 60210012 Device internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012DEVICEINTERNAL_H_
#define _THA60210012DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/man/Tha60210051DeviceInternal.h"
#include "Tha60210012Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012Device *Tha60210012Device;
typedef struct tTha60210012DeviceMethods
    {
    tAtAsyncOperationFunc (*AsyncFlushOpGet)(Tha60210012Device self, uint32 state);
    uint32 (*StartVersionSupportEXaui)(Tha60210012Device self);
    eAtRet (*EthPassthroughActive)(Tha60210012Device self, eBool enable);
    uint32 (*NumChannelPerLoSlice)(Tha60210012Device self);
    }tTha60210012DeviceMethods;

typedef struct tTha60210012Device
    {
    tTha60210051Device super;
    const tTha60210012DeviceMethods *methods;

    uint32 asyncInitHwFlushState;
    }tTha60210012Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60210012DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012DEVICEINTERNAL_H_ */


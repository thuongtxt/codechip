/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210012VersionReader.c
 *
 * Created Date: Mar 23, 2016
 *
 * Description : Version reader of 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/man/Tha60210051VersionReaderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012VersionReader
    {
    tTha60210051VersionReader super;
    }tTha60210012VersionReader;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011VersionReaderMethods m_Tha60210011VersionReaderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionIgnoreDate(ThaVersionReader self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint32 StartBuiltNumberIgnoreDate(ThaVersionReader self)
    {
    AtUnused(self);
    return 0x0;
    }

static void OverrideTha60210011VersionReader(ThaVersionReader self)
    {
    Tha60210011VersionReader reader = (Tha60210011VersionReader)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011VersionReaderOverride, mMethodsGet(reader), sizeof(m_Tha60210011VersionReaderOverride));

        mMethodOverride(m_Tha60210011VersionReaderOverride, StartVersionIgnoreDate);
        mMethodOverride(m_Tha60210011VersionReaderOverride, StartBuiltNumberIgnoreDate);
        }

    mMethodsSet(reader, &m_Tha60210011VersionReaderOverride);
    }

static void Override(ThaVersionReader self)
    {
    OverrideTha60210011VersionReader(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012VersionReader);
    }

static ThaVersionReader ObjectInit(ThaVersionReader self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (device == NULL)
        return NULL;

    /* Super constructor */
    if (Tha60210051VersionReaderObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaVersionReader Tha60210012VersionReaderNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaVersionReader newReader = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newReader == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newReader, device);
    }

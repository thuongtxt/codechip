/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Management
 * 
 * File        : Tha60210012Device.h
 * 
 * Created Date: May 12, 2016
 *
 * Description : Device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012DEVICE_H_
#define _THA60210012DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60210012IsCdrHoRegister(AtDevice self, uint32 address);
eBool Tha60210012IsCdrLoRegister(AtDevice self, uint32 address);
eBool Tha60210012IsCdrRegister(AtDevice self, uint32 address);
eBool Tha60210012IsClaRegister(AtDevice self, uint32 address);
eBool Tha60210012IsEthRegister(AtDevice self, uint32 address);
eBool Tha60210012IsMapHoRegister(AtDevice self, uint32 address);
eBool Tha60210012IsMapLoRegister(AtDevice self, uint32 address);
eBool Tha60210012IsMapRegister(AtDevice self, uint32 address);
eBool Tha60210012IsDemapRegister(AtDevice self, uint32 address);
eBool Tha60210012IsOcnRegister(AtDevice self, uint32 address);
eBool Tha60210012IsPdhRegister(AtDevice self, uint32 address);
eBool Tha60210012IsPohRegister(AtDevice self, uint32 address);
eBool Tha60210012IsPlaRegister(AtDevice self, uint32 address);
eBool Tha60210012IsPweRegister(AtDevice self, uint32 address);
eBool Tha60210012IsPdaRegister(AtDevice self, uint32 address);
eBool Tha60210012IsBerRegister(AtDevice self, uint32 address);
eBool Tha60210012IsPmcRegister(AtDevice self, uint32 address);
eBool Tha60210012IsEncapHoRegister(AtDevice self, uint32 address);
eBool Tha60210012IsEncapLoRegister(AtDevice self, uint32 address);
eBool Tha60210012IsEncapRegister(AtDevice self, uint32 address);
eBool Tha60210012IsDecapRegister(AtDevice self, uint32 address);
eBool Tha60210012IsMpegRegister(AtDevice self, uint32 address);
eBool Tha60210012IsMpigRegister(AtDevice self, uint32 address);
eBool Tha60210012DeviceEXauiIsReady(AtDevice self);
eBool Tha60210012DevicePtchServiceIsReady(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012DEVICE_H_ */


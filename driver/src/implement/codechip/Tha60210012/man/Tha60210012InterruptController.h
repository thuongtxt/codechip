/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      :  
 * 
 * File        : Tha60210012InterruptController.h
 * 
 * Created Date: Mar 23, 2016
 *
 * Description : 60210012 Interrupt controller utilities
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/
 
#ifndef _THA60210012INTERRUPTCONTROLLER_H_
#define _THA60210012INTERRUPTCONTROLLER_H_



/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
ThaIntrController Tha60210012IntrControllerNew(AtIpCore core);

/*--------------------------- Entries ----------------------------------------*/


#ifdef __cplusplus
}
#endif

#endif /* _THA60210012INTERRUPTCONTROLLER_H_ */

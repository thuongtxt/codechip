/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60210012ModulePda.h
 *
 * Created Date: Apr 14, 2016
 *
 * Description : PDA module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEPDA_H_
#define _THA60210012MODULEPDA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcChannel.h"
#include "AtHdlcLink.h"
#include "../../../default/pda/ThaModulePda.h"
#include "../../Tha60210051/pda/Tha60210051ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModulePda * Tha60210012ModulePda;
typedef struct tTha60210012ModulePdaMethods
    {
    eBool (*UseNewLookUpControlRegister)(Tha60210012ModulePda self);
    }tTha60210012ModulePdaMethods;

typedef struct tTha60210012ModulePda
    {
    tTha60210051ModulePda super;
    const tTha60210012ModulePdaMethods *methods;
    }tTha60210012ModulePda;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210012ModulePdaObjectInit(AtModule self, AtDevice device);

eAtRet Tha60210012ModulePdaHdlcServiceLoopkupSet(AtHdlcChannel physicalChannel);
eAtRet Tha60210012ModulePdaHdlcServiceLoopkupReset(AtHdlcChannel physicalChannel);

eAtRet Tha60210012ModulePdaHdlcTdmPayloadMode(AtHdlcChannel physicalChannel, uint8 hwPayloadType);
eAtRet Tha60210012ModulePdaHdlcTdmPayloadModeReset(AtHdlcChannel physicalChannel);

eAtRet Tha60210012ModulePdaVcgLookUpControl(ThaModulePda self, uint32 vcgId, AtEthFlow flow, eBool enable);
eAtRet Tha60210012ModulePdaPppLinkMpBundleLookupSet(AtHdlcLink physicalLink, AtHdlcBundle bundle);
eAtRet Tha60210012ModulePdaPppLinkMpBundleLookupReSet(AtHdlcLink physicalLink);
eAtRet Tha60210012ModulePdaPppLinkMpBundleLookupEnable(AtHdlcLink physicalLink, eBool enable);
eAtRet Tha60210012ModulePdaHdlcHwStsIdSet(AtHdlcChannel physicalChannel, uint8 hwSts);

eAtRet Tha60210012ModulePdaEXauiEnable(AtModule self, uint32 exauiId, eBool enabled);
eBool Tha60210012ModulePdaEXauiIsEnabled(AtModule self, uint32 exauiId);
uint32 Tha60210012ModulePdaGeBypassLookUpControlReg(ThaModulePda self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEPDA_H_ */

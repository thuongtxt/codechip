/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60210012ModulePda.c
 *
 * Created Date: Feb 29, 2016
 *
 * Description : PDA module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/encap/ThaHdlcChannel.h"
#include "../../../default/encap/dynamic/ThaDynamicHdlcChannel.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../man/Tha60210012Device.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "../pw/Tha60210012ModulePw.h"
#include "../encap/Tha60210012PhysicalHdlcChannel.h"
#include "Tha60210012ModulePdaReg.h"
#include "Tha60210012ModulePda.h"

/*--------------------------- Define -----------------------------------------*/
#define cPdaDebugSticky2 0x500005
#define cPdaHoSlice48_0EncapFifoEmpty           cBit0 /*HO slice48 #0 encap fifo empty        */
#define cPdaHoSlice48_1EncapFifoEmpty           cBit1 /*HO slice48 #1 encap fifo empty        */
#define cPdaHoSlide48_0EncapFifoFull            cBit2 /*HO slice48 #0 encap fifo full         */
#define cPdaHoSlide48_1EncapFifoFull            cBit3 /*HO slice48 #1 encap fifo full         */
#define cPdaHoSlide48_0EncapFifoSopErr          cBit4 /*HO slice48 #0 encap fifo SOP error    */
#define cPdaHoSlide48_1EncapFifoSopErr          cBit5 /*HO slice48 #1 encap fifo SOP error    */
#define cPdaUnused                              cBit7_6/* unused                             */
#define cPdaLoSlide48_0EncapFifoEmpty           cBit8 /*LO slice48 #0 encap fifo empty        */
#define cPdaLoSlide48_1EncapFifoEmpty           cBit9 /*LO slice48 #1 encap fifo empty        */
#define cPdaLoSlide48_0EncapFifoFull            cBit10 /*LO slice48 #0 encap fifo full         */
#define cPdaLoSlide48_1EncapFifoFull            cBit11 /*LO slice48 #1 encap fifo full         */
#define cPdaLoSlide48_0EncapFifoCacheSame       cBit12 /*LO slice48 #0 encap fifo cache same   */
#define cPdaLoSlide48_1EncapFifoCacheSame       cBit13 /*LO slice48 #1 encap fifo cache same   */
#define cPdaLoSlide48_0EncapFifoCacheEmpty      cBit14 /*LO slice48 #0 encap fifo cache empty  */
#define cPdaLoSlide48_1EncapFifoCacheEmpty      cBit15 /*LO slice48 #1 encap fifo cache empty  */
#define cPdaLoSlide48_0EncapFifoCacheGap        cBit16 /*LO slice48 #0 encap fifo cache gap    */
#define cPdaLoSlide48_1EncapFifoCacheGap        cBit17 /*LO slice48 #1 encap fifo cache gap    */
#define cPdaBypassGeXauiReadFifosFull           cBit18 /*Bypass GE/XAUI read fifos full        */
#define cPdaTdmMuxReqVcatFifoFull               cBit19 /*TDM mux request VCAT fifo full        */
#define cPdaTdmMuxReqGeXauiFifoFull             cBit20 /*TDM mux request GE/XAUI fifo full     */
#define cPdaReadCacheMpegValidFifoFull          cBit21 /*Read cache MPEG valid fifo full       */
#define cPdaReadCacheLinkListValidFifofull      cBit22 /*Read cache link list valid fifo full  */
#define cPdaReadCachePacketInfoValidFifoFull    cBit23 /*Read cache packet info valid fifo full*/
#define cPdaReadCacheWorkValidFifoFull          cBit24 /*Read cache WROK valid fifo full       */
#define cPdaReadCacheSameId                     cBit25 /*Read cache same id                    */

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210012ModulePda)self)
#define mModuleBaseAddress(self) ThaModulePdaBaseAddress((ThaModulePda)self)
#define mChannelPdaBaseAddress(channel) PdaBaseAddress((AtChannel)channel)

/* This values need confirm with hardware */
#define cPdaTdmLookupHwResetValue 0xBFF

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012ModulePdaMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModulePdaMethods         m_ThaModulePdaOverride;
static tTha60210011ModulePdaMethods m_Tha60210011ModulePdaOverride;
static tTha60210051ModulePdaMethods m_Tha60210051ModulePdaOverride;

/* Save super implementation */
static const tAtModuleMethods             *m_AtModuleMethods             = NULL;
static const tThaModulePdaMethods *m_ThaModulePdaMethods = NULL;
static const tTha60210011ModulePdaMethods *m_Tha60210011ModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Ramlotdmlkupcfg(Tha60210011ModulePda self, AtPw pw)
    {
    uint32 offset = mModuleBaseAddress(self) + mMethodsGet(self)->PwLoTdmLookupOffset(self, pw);
    return cAf6Reg_ramlotdmlkupcfg_Base + offset;
    }

static eAtRet PwLoTdmLookupSet(Tha60210011ModulePda self, AtPw pw)
    {
    uint32 address = Ramlotdmlkupcfg(self, pw);
    uint32 regValue = mChannelHwRead(pw, address, cThaModulePda);

    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_PwLkEnable_, 0x1);/* Enable lookup */
    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_PwID_, AtChannelIdGet((AtChannel)pw));
    mChannelHwWrite(pw, address, regValue, cThaModulePda);

    return cAtOk;
    }

static eBool UseNewLookUpControlRegister(Tha60210012ModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return Tha60210012DeviceEXauiIsReady(device);
    }

static Tha60210011ModulePw ModulePw(ThaModulePda self)
    {
    return (Tha60210011ModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    }

static uint32 HoTdmLookupCtrl(Tha60210011ModulePda self)
    {
    Tha60210011ModulePw pwModule = ModulePw((ThaModulePda)self);

    if (Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule) == Tha60210011ModulePwOriginalNumPwsPerSts24Slice(pwModule))
    return mMethodsGet(mThis(self))->UseNewLookUpControlRegister(mThis(self)) ? cAf6Reg_ramhotdmlkupcfg_BaseNew : cAf6Reg_ramhotdmlkupcfg_Base;

    return 0x42100;
    }

static uint32 HoTdmLookupControl(Tha60210011ModulePda self, AtPw pw)
    {
    uint32 offset = mModuleBaseAddress(self) + mMethodsGet(self)->PwHoTdmLookupOffset(self, pw);
    return mMethodsGet(self)->HoTdmLookupCtrl(self) + offset;
    }

static eAtRet PwHoTdmLookupSet(Tha60210011ModulePda self, AtPw pw)
    {
    uint32 address = HoTdmLookupControl(self, pw);
    uint32 regValue = mChannelHwRead(pw, address, cThaModulePda);

    mRegFieldSet(regValue, cAf6_ramhotdmlkupcfg_PwLkEnable_, 0x1);/* Enable lookup */
    mRegFieldSet(regValue, cAf6_ramhotdmlkupcfg_PwID_, AtChannelIdGet((AtChannel)pw));
    mChannelHwWrite(pw, address, regValue, cThaModulePda);

    return cAtOk;
    }

static uint32 TdmLookupResetValue(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return cPdaTdmLookupHwResetValue;
    }

static eAtRet PwLoTdmLookupReset(Tha60210011ModulePda self, AtPw pw)
    {
    uint32 address = Ramlotdmlkupcfg(self, pw);
    uint32 regValue = 0;

    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_PwID_, cPdaTdmLookupHwResetValue);
    mChannelHwWrite(pw, address, regValue, cThaModulePda);
    return cAtOk;
    }

static eAtRet PwHoTdmLookupReset(Tha60210011ModulePda self, AtPw pw)
    {
    uint32 address = HoTdmLookupControl(self, pw);
    uint32 regValue = 0;

    mRegFieldSet(regValue, cAf6_ramhotdmlkupcfg_PwID_, cPdaTdmLookupHwResetValue);
    mChannelHwWrite(pw, address, regValue, cThaModulePda);
    return cAtOk;
    }

static eAtRet PwLoServiceEnable(Tha60210011ModulePda self, AtPw pw, eBool enable)
    {
    uint32 address = Ramlotdmlkupcfg(self, pw);
    uint32 regValue = mChannelHwRead(pw, address, cThaModulePda);

    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_PwCesEnable_, enable ? 0x1 : 0x0);
    mChannelHwWrite(pw, address, regValue, cThaModulePda);
    return cAtOk;
    }

static eAtRet PwHoServiceEnable(Tha60210011ModulePda self, AtPw pw, eBool enable)
    {
    uint32 address = HoTdmLookupControl(self, pw);
    uint32 regValue = mChannelHwRead(pw, address, cThaModulePda);

    mRegFieldSet(regValue, cAf6_ramhotdmlkupcfg_PwCesEnable_, enable ? 0x1 : 0x0);
    mChannelHwWrite(pw, address, regValue, cThaModulePda);
    return cAtOk;
    }

static eAtRet CesCepPwServiceEnable(Tha60210011ModulePda self, AtPw pw, eBool enable)
    {
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return PwLoServiceEnable(self, pw, enable);

    return PwHoServiceEnable(self, pw, enable);
    }

static eAtRet PwCircuitIdSet(ThaModulePda self, AtPw pw)
    {
    if (ThaPwTypeIsCesCep(pw))
        {
        eAtRet ret = m_ThaModulePdaMethods->PwCircuitIdSet(self, pw);
        ret |= CesCepPwServiceEnable((Tha60210011ModulePda)self, pw, cAtTrue);

        return ret;
        }

    return cAtOk;
    }

static eBool JitterBufferCenterIsSupported(ThaModulePda self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static eBool HwValueUnit1PacketIsSupported(Tha60210051ModulePda self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet PwCircuitIdReset(ThaModulePda self, AtPw pw)
    {
    eAtRet ret;

    if (AtPwTypeGet(pw) == cAtPwTypeMlppp)
        return cAtOk;

    ret = m_ThaModulePdaMethods->PwCircuitIdReset(self, pw);
    if (ThaPwTypeIsCesCep(pw))
        ret |= CesCepPwServiceEnable((Tha60210011ModulePda)self, pw, cAtFalse);

    return ret;
    }

static eAtRet PwDefaultHwConfigure(Tha60210011ModulePda self, AtPw pwAdapter)
    {
    if (ThaPwTypeIsCesCep(pwAdapter))
        return m_Tha60210011ModulePdaMethods->PwDefaultHwConfigure(self, pwAdapter);

    return cAtOk;
    }

static void PwBufferRegsShow(Tha60210011ModulePda self, AtPw pw)
    {
    if (ThaPwTypeIsCesCep(pw))
        m_Tha60210011ModulePdaMethods->PwBufferRegsShow(self, pw);
    }

static AtHdlcChannel HdlcChannelFromPw(AtPw pw)
    {
    eAtPwType pwType = AtPwTypeGet(pw);

    if (pwType == cAtPwTypeHdlc)
        return (AtHdlcChannel)AtPwBoundCircuitGet(pw);

    if ((pwType == cAtPwTypePpp) || (pwType == cAtPwTypeFr))
        return AtHdlcLinkHdlcChannelGet((AtHdlcLink)AtPwBoundCircuitGet(pw));

    return NULL;
    }

static AtHdlcBundle HdlcBundleFromPw(AtPw pw)
    {
    if (AtPwTypeGet(pw) != cAtPwTypeMlppp)
        return NULL;

    return (AtHdlcBundle)AtPwBoundCircuitGet(pw);
    }

static void PwCircuitRegsShow(Tha60210011ModulePda self, AtPw pw)
    {
    uint16 linkId;
    AtHdlcLink link;
    AtHdlcBundle bundle;
    AtHdlcChannel hdlcChannel;
    ThaModulePda modulePda = (ThaModulePda)self;

    if (ThaPwTypeIsCesCep(pw))
        {
        m_Tha60210011ModulePdaMethods->PwCircuitRegsShow(self, pw);
        return;
        }

    /* PPP/FR/HDLC type */
    hdlcChannel = HdlcChannelFromPw(pw);
    if (hdlcChannel)
        {
        mMethodsGet(modulePda)->HdlcChannelRegsShow(modulePda, hdlcChannel);
        return;
        }

    /* Mlppp type */
    bundle = HdlcBundleFromPw(pw);
    if (bundle == NULL)
        return;

    for (linkId = 0; linkId < AtHdlcBundleNumberOfLinksGet(bundle); linkId++)
        {
        link = AtHdlcBundleLinkGet(bundle, linkId);
        hdlcChannel = AtHdlcLinkHdlcChannelGet(link);
        mMethodsGet(modulePda)->HdlcChannelRegsShow(modulePda, hdlcChannel);
        }
    }

static void OneRegShow(AtHdlcChannel channel, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay((AtChannel)channel, address, cThaModulePda);
    }

static Tha60210011ModulePda ModulePda(AtHdlcChannel channel)
    {
    return (Tha60210011ModulePda)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModulePda);
    }

static uint32 HdlcServiceHoTdmLookupControl(AtHdlcChannel channel)
    {
    return HoTdmLookupCtrl(ModulePda(channel));
    }

static uint32 PdaBaseAddress(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModulePda pdaModule = (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
    return ThaModulePdaBaseAddress(pdaModule);
    }

static uint32 HdlcServiceLoTdmLookupOffset(AtHdlcChannel physicalChannel)
    {
    uint32 oc48Id, oc24SliceId, offset;
    uint8 slice = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)physicalChannel);
    Tha60210011ModulePda pdaModule = (Tha60210011ModulePda)ModulePda(physicalChannel);

    oc48Id = slice / 2;
    oc24SliceId = slice % 2;
    offset = mMethodsGet(pdaModule)->LoTdmLookupOffset(pdaModule, oc48Id, oc24SliceId, AtChannelHwIdGet((AtChannel)physicalChannel));
    return offset + ThaModulePdaBaseAddress((ThaModulePda)pdaModule);
    }

static uint32 HdlcServiceLoTdmModeOffset(AtHdlcChannel physicalChannel)
    {
    uint32 oc48Id, oc24SliceId, offset;
    uint8 slice = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)physicalChannel);
    Tha60210011ModulePda pdaModule = (Tha60210011ModulePda)ModulePda(physicalChannel);

    oc48Id = slice / 2;
    oc24SliceId = slice % 2;
    offset = mMethodsGet(pdaModule)->LoTdmModeOffset(pdaModule, oc48Id, oc24SliceId, AtChannelHwIdGet((AtChannel)physicalChannel));
    return offset + ThaModulePdaBaseAddress((ThaModulePda)pdaModule);
    }

static uint32 HdlcServiceHoTdmLookupOffset(AtHdlcChannel physicalChannel)
    {
    uint8 slice = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)physicalChannel);
    return (slice * 0x40UL + AtChannelHwIdGet((AtChannel)physicalChannel)) + mChannelPdaBaseAddress(physicalChannel);
    }

static uint32 HdlcServiceHoTdmModeOffset(AtHdlcChannel physicalChannel)
    {
    uint8 slice = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)physicalChannel);
    return (slice * 0x100UL + AtChannelHwIdGet((AtChannel)physicalChannel)) + mChannelPdaBaseAddress(physicalChannel);
    }

static void HdlcChannelRegsShow(ThaModulePda self, AtHdlcChannel channel)
    {
    uint8 slice = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)channel);
    uint32 hwIdInSlice = AtChannelHwIdGet((AtChannel)channel);
    uint32 address, offset;
    AtUnused(self);

    AtPrintc(cSevInfo, "Show register %s of flow:\r\n", AtObjectToString((AtObject)channel));

    if (ThaHdlcChannelCircuitIsHo((ThaHdlcChannel)channel))
        {
        offset = HdlcServiceHoTdmLookupOffset(channel);
        address = HdlcServiceHoTdmLookupControl(channel) + offset;
        OneRegShow(channel, "Pseudowire PDA High Order TDM Look Up Control", address);

        offset = HdlcServiceHoTdmModeOffset(channel);
        address = cAf6Reg_ramhotdmmodecfg_Base + offset;
        OneRegShow(channel, "Pseudowire PDA High Order TDM mode Control", address);
        }
    else
        {
        uint8 oc48Id = slice / 2;
        uint8 oc24SliceId = slice % 2;
        Tha60210011ModulePda modulePda = (Tha60210011ModulePda)self;

        offset = mMethodsGet(modulePda)->LoTdmLookupOffset(modulePda, oc48Id, oc24SliceId, hwIdInSlice);
        address = mModuleBaseAddress(self) + cAf6Reg_ramlotdmlkupcfg_Base + offset;
        OneRegShow(channel, "Pseudowire PDA Low Order TDM or MLPPP Look Up Control", address);

        offset = mMethodsGet(modulePda)->LoTdmModeOffset(modulePda, oc48Id, oc24SliceId, hwIdInSlice);
        address = mModuleBaseAddress(self) + cAf6Reg_ramlotdmmodecfg_Base + offset;
        OneRegShow(channel, "Pseudowire PDA Low Order TDM mode Control", address);
        }
    }

static void DebugPrintErrorBit(const char* title, uint32 hwValue, uint32 mask, const char *raiseString, const char *clearString)
    {
    AtPrintc(cSevNormal, "    %-40s: ", title);
    if (hwValue & mask)
        AtPrintc(cSevCritical, "%-10s\r\n", raiseString);
    else
        AtPrintc(cSevInfo, "%-10s\r\n", clearString);
    }

static void ShowDebugSticky2(AtModule self)
    {
    uint32 regAddress;
    uint32 longRegValue[cThaLongRegMaxSize];
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), AtModuleDefaultCoreGet((AtModule)self));
    regAddress = cPdaDebugSticky2;

    mModuleHwLongRead(self, regAddress, longRegValue, cThaLongRegMaxSize, core);
    AtPrintc(cSevInfo, "\r\n* PDA debug sticky 2: : (0x%08x = 0x%08x.0x%08x)\r\n", regAddress, longRegValue[1], longRegValue[0]);
    DebugPrintErrorBit("HO slice48 #0 encap fifo empty",         longRegValue[1], cPdaHoSlice48_0EncapFifoEmpty       , "RAISE", "CLEAR");
    DebugPrintErrorBit("HO slice48 #1 encap fifo empty",         longRegValue[1], cPdaHoSlice48_1EncapFifoEmpty       , "RAISE", "CLEAR");
    DebugPrintErrorBit("HO slice48 #0 encap fifo full",          longRegValue[1], cPdaHoSlide48_0EncapFifoFull        , "RAISE", "CLEAR");
    DebugPrintErrorBit("HO slice48 #1 encap fifo full",          longRegValue[1], cPdaHoSlide48_1EncapFifoFull        , "RAISE", "CLEAR");
    DebugPrintErrorBit("HO slice48 #0 encap fifo SOP error",     longRegValue[1], cPdaHoSlide48_0EncapFifoSopErr      , "RAISE", "CLEAR");
    DebugPrintErrorBit("HO slice48 #1 encap fifo SOP error",     longRegValue[1], cPdaHoSlide48_1EncapFifoSopErr      , "RAISE", "CLEAR");
    DebugPrintErrorBit("LO slice48 #0 encap fifo empty",         longRegValue[1], cPdaLoSlide48_0EncapFifoEmpty       , "RAISE", "CLEAR");
    DebugPrintErrorBit("LO slice48 #1 encap fifo empty",         longRegValue[1], cPdaLoSlide48_1EncapFifoEmpty       , "RAISE", "CLEAR");
    DebugPrintErrorBit("LO slice48 #0 encap fifo full",          longRegValue[1], cPdaLoSlide48_0EncapFifoFull        , "RAISE", "CLEAR");
    DebugPrintErrorBit("LO slice48 #1 encap fifo full",          longRegValue[1], cPdaLoSlide48_1EncapFifoFull        , "RAISE", "CLEAR");
    DebugPrintErrorBit("LO slice48 #0 encap fifo cache same",    longRegValue[1], cPdaLoSlide48_0EncapFifoCacheSame   , "RAISE", "CLEAR");
    DebugPrintErrorBit("LO slice48 #1 encap fifo cache same",    longRegValue[1], cPdaLoSlide48_1EncapFifoCacheSame   , "RAISE", "CLEAR");
    DebugPrintErrorBit("LO slice48 #0 encap fifo cache empty",   longRegValue[1], cPdaLoSlide48_0EncapFifoCacheEmpty  , "RAISE", "CLEAR");
    DebugPrintErrorBit("LO slice48 #1 encap fifo cache empty",   longRegValue[1], cPdaLoSlide48_1EncapFifoCacheEmpty  , "RAISE", "CLEAR");
    DebugPrintErrorBit("LO slice48 #0 encap fifo cache gap",     longRegValue[1], cPdaLoSlide48_0EncapFifoCacheGap    , "RAISE", "CLEAR");
    DebugPrintErrorBit("LO slice48 #1 encap fifo cache gap",     longRegValue[1], cPdaLoSlide48_1EncapFifoCacheGap    , "RAISE", "CLEAR");
    DebugPrintErrorBit("Bypass GE/XAUI read fifos full",         longRegValue[1], cPdaBypassGeXauiReadFifosFull       , "RAISE", "CLEAR");
    DebugPrintErrorBit("TDM mux request VCAT fifo full",         longRegValue[1], cPdaTdmMuxReqVcatFifoFull           , "RAISE", "CLEAR");
    DebugPrintErrorBit("TDM mux request GE/XAUI fifo full",      longRegValue[1], cPdaTdmMuxReqGeXauiFifoFull         , "RAISE", "CLEAR");
    DebugPrintErrorBit("Read cache MPEG valid fifo full",        longRegValue[1], cPdaReadCacheMpegValidFifoFull      , "RAISE", "CLEAR");
    DebugPrintErrorBit("Read cache link list valid fifo full",   longRegValue[1], cPdaReadCacheLinkListValidFifofull  , "RAISE", "CLEAR");
    DebugPrintErrorBit("Read cache packet info valid fifo full", longRegValue[1], cPdaReadCachePacketInfoValidFifoFull, "RAISE", "CLEAR");
    DebugPrintErrorBit("Read cache WORK valid fifo full",        longRegValue[1], cPdaReadCacheWorkValidFifoFull      , "RAISE", "CLEAR");
    DebugPrintErrorBit("Read cache same id",                     longRegValue[1], cPdaReadCacheSameId                 , "RAISE", "CLEAR");
    AtPrintc(cSevNormal, "\r\n");

    /* clear sticky */
    mModuleHwLongWrite(self, regAddress, longRegValue, cThaLongRegMaxSize, core);
    }

static eAtRet Debug(AtModule self)
    {
    m_AtModuleMethods->Debug(self);
    ShowDebugSticky2(self);
    return cAtOk;
    }

static uint32 HwEXauiId(uint32 exauiId)
    {
    return 4 + exauiId;
    }

static uint32 GeBypassLookUpControlReg(AtModule self)
    {
    Tha60210011ModulePw pwModule = ModulePw((ThaModulePda)self);

    if (Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule) == Tha60210011ModulePwOriginalNumPwsPerSts24Slice(pwModule))
        return cAf6Reg_ramgelkupcfg_Base_New;

    return 0x42180;
    }

static eAtRet EXauiEnable(AtModule self, uint32 exauiId, eBool enabled)
    {
    uint32 hwId = HwEXauiId(exauiId);
    uint32 regAddr = mModuleBaseAddress(self) + GeBypassLookUpControlReg(self) + hwId;
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_ramgelkupcfg_GeLkEnable_, enabled ? 1 : 0);
    mRegFieldSet(regVal, cAf6_ramgelkupcfg_GeServiceID_, hwId);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool EXauiIsEnabled(AtModule self, uint32 exauiId)
    {
    uint32 hwId = HwEXauiId(exauiId);
    uint32 regAddr = mModuleBaseAddress(self) + GeBypassLookUpControlReg(self) + hwId;
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & cAf6_ramgelkupcfg_GeLkEnable_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet AllEXauisInit(AtModule self)
    {
    eAtRet ret = cAtOk;
    uint32 exauiId;
    AtDevice device = AtModuleDeviceGet(self);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);

    for (exauiId = 0; exauiId < Tha60210012ModuleEthNumExauiGet(ethModule); exauiId++)
        ret |= Tha60210012ModulePdaEXauiEnable(self, exauiId, cAtFalse);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret;

    /* Call super */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Disable eXAUI */
    return AllEXauisInit(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static eAtRet LoHdlcServiceLookupSet(AtHdlcChannel physicalChannel)
    {
    uint32 offset, regAddress, regValue;
    uint32 linkId = AtChannelHwIdGet((AtChannel)AtHdlcChannelHdlcLinkGet(physicalChannel));

    offset = HdlcServiceLoTdmLookupOffset(physicalChannel);
    regAddress = cAf6Reg_ramlotdmlkupcfg_Base + offset;
    regValue = mChannelHwRead(physicalChannel, regAddress, cThaModulePda);

    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_PwCesEnable_, 0);
    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_PwLkEnable_, 1); /* Enable lookup */
    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_PwID_, linkId);

    mChannelHwWrite(physicalChannel, regAddress, regValue, cThaModulePda);

    return cAtOk;
    }

static eAtRet LoHdlcServiceLookupReset(AtHdlcChannel physicalChannel)
    {
    uint32 offset = HdlcServiceLoTdmLookupOffset(physicalChannel);
    uint32 regAddress = cAf6Reg_ramlotdmlkupcfg_Base + offset;
    uint32 regValue = mChannelHwRead(physicalChannel, regAddress, cThaModulePda);

    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_PwLkEnable_, 0); /* Disable lookup */
    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_PwID_, cPdaTdmLookupHwResetValue);
    mChannelHwWrite(physicalChannel, regAddress, regValue, cThaModulePda);

    return cAtOk;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsPdaRegister(AtModuleDeviceGet(self), address);
    }

static eAtRet HoHdlcServiceLoopkupSet(AtHdlcChannel physicalChannel)
    {
    uint32 offset = HdlcServiceHoTdmLookupOffset(physicalChannel);
    uint32 address = HdlcServiceHoTdmLookupControl(physicalChannel) + offset;
    uint32 regValue = mChannelHwRead(physicalChannel, address, cThaModulePda);
    uint32 linkId = AtChannelHwIdGet((AtChannel)AtHdlcChannelHdlcLinkGet(physicalChannel));

    mRegFieldSet(regValue, cAf6_ramhotdmlkupcfg_PwCesEnable_, 0x0);
    mRegFieldSet(regValue, cAf6_ramhotdmlkupcfg_PwLkEnable_, 0x1);/* Enable lookup */
    mRegFieldSet(regValue, cAf6_ramhotdmlkupcfg_PwID_, linkId);
    mChannelHwWrite(physicalChannel, address, regValue, cThaModulePda);

    return cAtOk;
    }

static eAtRet HoHdlcServiceLoopkupReset(AtHdlcChannel physicalChannel)
    {
    uint32 offset = HdlcServiceHoTdmLookupOffset(physicalChannel);
    uint32 address = HdlcServiceHoTdmLookupControl(physicalChannel) + offset;
    uint32 regValue = mChannelHwRead(physicalChannel, address, cThaModulePda);

    mRegFieldSet(regValue, cAf6_ramhotdmlkupcfg_PwLkEnable_, 0x0);/* Disable lookup */
    mRegFieldSet(regValue, cAf6_ramhotdmlkupcfg_PwID_, cPdaTdmLookupHwResetValue);
    mChannelHwWrite(physicalChannel, address, regValue, cThaModulePda);

    return cAtOk;
    }

static eAtRet LoHdlcTdmPayloadMode(AtHdlcChannel physicalChannel, uint8 hwPayloadType)
    {
    uint32 regAddress = cAf6Reg_ramlotdmmodecfg_Base + HdlcServiceLoTdmModeOffset(physicalChannel);
    uint32 regValue = mChannelHwRead(physicalChannel, regAddress, cThaModulePda);

    mRegFieldSet(regValue, cAf6_ramlotdmmodecfg_PDAMode_, hwPayloadType);
    mChannelHwWrite(physicalChannel, regAddress, regValue, cThaModulePda);

    return cAtOk;
    }

static uint8 HoHwPdaMode(AtHdlcChannel physicalChannel)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtEncapChannelBoundPhyGet((AtEncapChannel)physicalChannel);
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);

    if ((numSts == 1) || ((numSts % 3) != 0))
        return 0;

    if ((numSts == 3) || (numSts == 6))
        return 1;

    if ((numSts == 9) || (numSts == 12) || (numSts == 15))
        return 2;

    if (numSts <= 48)
        return 3;

    return 0;
    }

static eAtRet HoHdlcTdmPayloadMode(AtHdlcChannel physicalChannel, uint8 payloadType)
    {
    uint32 regAddress = cAf6Reg_ramhotdmmodecfg_Base + HdlcServiceHoTdmModeOffset(physicalChannel);
    uint32 regValue = mChannelHwRead(physicalChannel, regAddress, cThaModulePda);

    mRegFieldSet(regValue, cAf6_ramhotdmmodecfg_PDAHoMode_, payloadType);
    mChannelHwWrite(physicalChannel, regAddress, regValue, cThaModulePda);

    return cAtOk;
    }

static uint32 StartVersionSupportLowRateCesop(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x5, 0x1012);
    }

static ThaModuleEth ModuleEth(AtPw pw)
    {
    return (ThaModuleEth)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pw), cAtModuleEth);
    }

static uint32 PwRxBytesGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    const uint8 cRxByteCounterType = 1;

    if (ThaPwTypeIsHdlcPppFr(pw))
        return Tha60210012ModuleEthHwFlowCounterGet(ModuleEth(pw), Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pw), cRxByteCounterType, clear);

    return m_ThaModulePdaMethods->PwRxBytesGet(self, pw, clear);
    }

static ThaHdlcChannel HdlcChannelGet(AtPw adapter)
    {
    AtChannel channelBoundPw = AtPwBoundCircuitGet(adapter);

    if (AtPwTypeGet(adapter) == cAtPwTypeHdlc)
        return (ThaHdlcChannel)channelBoundPw;

    if (AtPwTypeGet(adapter) == cAtPwTypePpp)
        {
        ThaHdlcChannel channel = (ThaHdlcChannel)AtHdlcLinkHdlcChannelGet((AtHdlcLink)channelBoundPw);
        return channel;
        }

    return NULL;
    }

static eAtRet HdlcPwPayloadTypeSet(ThaModulePda self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    ThaHdlcChannel hdlcChannel = HdlcChannelGet(adapter);
    AtUnused(self);
    return ThaHdlcChannelHdlcPwPayloadTypeSet(hdlcChannel, payloadType);
    }

static eAtRet MlpppPwPayloadTypeSet(ThaModulePda self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    uint16 linkId, numberLinkInBundle;
    AtHdlcBundle hdlcBundle;
    eAtRet ret = cAtOk;

    AtUnused(self);
    hdlcBundle = (AtHdlcBundle)AtPwBoundCircuitGet(adapter);
    numberLinkInBundle = AtHdlcBundleNumberOfLinksGet(hdlcBundle);

    for (linkId = 0; linkId < numberLinkInBundle; linkId++)
        {
        AtHdlcLink link = AtHdlcBundleLinkGet(hdlcBundle, linkId);
        ThaHdlcChannel hdlcChannel = (ThaHdlcChannel)AtHdlcLinkHdlcChannelGet(link);
        ret |= ThaHdlcChannelHdlcPwPayloadTypeSet(hdlcChannel, payloadType);
        }

    return ret;
    }

static eBool PwCwAutoRxMBitIsConfigurable(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startVersionSupport = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x9, 0x1040);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(device));
    return (currentVersion >= startVersionSupport) ? cAtTrue : cAtFalse;
    }

static uint32 MaxNumJitterBufferBlocks(ThaModulePda self)
    {
    AtUnused(self);
    return 1048576;
    }

static uint32 NumFreeBlocksHwGet(ThaModulePda self)
    {
    /* address 0x500007, bit_field[19:0], expected value 0 (2^20 will roll to 0) */
    uint32 regVal = mModuleHwRead(self, cHwBlockCheckReg) & cBit19_0;
    if (regVal == 0)
        return MaxNumJitterBufferBlocks(self);

    return regVal;
    }

static eBool HwPwIdCacheIsSupported(Tha60210051ModulePda self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 VcgLookupControlReg(ThaModulePda self)
    {
    Tha60210011ModulePw pwModule = ModulePw(self);

    if (Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule) == Tha60210011ModulePwOriginalNumPwsPerSts24Slice(pwModule))
        return cAf6Reg_ramvcatlkupcfg_Base;

    return 0x42000;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, m_ThaModulePdaMethods, sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, PwCircuitIdSet);
        mMethodOverride(m_ThaModulePdaOverride, HdlcChannelRegsShow);
        mMethodOverride(m_ThaModulePdaOverride, PwCircuitIdReset);
        mMethodOverride(m_ThaModulePdaOverride, PwRxBytesGet);
        mMethodOverride(m_ThaModulePdaOverride, HdlcPwPayloadTypeSet);
        mMethodOverride(m_ThaModulePdaOverride, JitterBufferCenterIsSupported);
        mMethodOverride(m_ThaModulePdaOverride, MlpppPwPayloadTypeSet);
        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitIsConfigurable);
        mMethodOverride(m_ThaModulePdaOverride, MaxNumJitterBufferBlocks);
        mMethodOverride(m_ThaModulePdaOverride, NumFreeBlocksHwGet);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void OverrideTha60210011ModulePda(AtModule self)
    {
    Tha60210011ModulePda pdaModule = (Tha60210011ModulePda)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePdaOverride, m_Tha60210011ModulePdaMethods, sizeof(m_Tha60210011ModulePdaOverride));

        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLoTdmLookupSet);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwHoTdmLookupSet);
        mMethodOverride(m_Tha60210011ModulePdaOverride, TdmLookupResetValue);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwLoTdmLookupReset);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwHoTdmLookupReset);
        mMethodOverride(m_Tha60210011ModulePdaOverride, HoTdmLookupCtrl);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwDefaultHwConfigure);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwBufferRegsShow);
        mMethodOverride(m_Tha60210011ModulePdaOverride, PwCircuitRegsShow);
        mMethodOverride(m_Tha60210011ModulePdaOverride, StartVersionSupportLowRateCesop);
        }

    mMethodsSet(pdaModule, &m_Tha60210011ModulePdaOverride);
    }

static void OverrideTha60210051ModulePda(AtModule self)
    {
    Tha60210051ModulePda pdaModule = (Tha60210051ModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051ModulePdaOverride, mMethodsGet(pdaModule), sizeof(m_Tha60210051ModulePdaOverride));

        mMethodOverride(m_Tha60210051ModulePdaOverride, HwValueUnit1PacketIsSupported);
        mMethodOverride(m_Tha60210051ModulePdaOverride, HwPwIdCacheIsSupported);
        }

    mMethodsSet(pdaModule, &m_Tha60210051ModulePdaOverride);
    }

static void MethodsInit(AtModule self)
    {
    Tha60210012ModulePda module = (Tha60210012ModulePda)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, UseNewLookUpControlRegister);
        }

    mMethodsSet(module, &m_methods);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePda(self);
    OverrideTha60210051ModulePda(self);
    OverrideTha60210011ModulePda(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModulePda);
    }

AtModule Tha60210012ModulePdaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210012ModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModulePdaObjectInit(newModule, device);
    }

eAtRet Tha60210012ModulePdaVcgLookUpControl(ThaModulePda self, uint32 vcgId, AtEthFlow flow, eBool enable)
    {
    uint32 flowId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
    uint32 address = mModuleBaseAddress(self) + VcgLookupControlReg(self) + vcgId;
    uint32 regValue = mModuleHwRead(self, address);

    mRegFieldSet(regValue, cAf6_ramvcatlkupcfg_VcgLkEnable_, enable ? 0x1 : 0x0);
    mRegFieldSet(regValue, cAf6_ramvcatlkupcfg_FlowId_, flowId);

    mModuleHwWrite(self, address, regValue);
    return cAtOk;
    }

eAtRet Tha60210012ModulePdaHdlcServiceLoopkupSet(AtHdlcChannel physicalChannel)
    {
    if (ThaHdlcChannelCircuitIsHo((ThaHdlcChannel)physicalChannel))
        return HoHdlcServiceLoopkupSet(physicalChannel);

    return LoHdlcServiceLookupSet(physicalChannel);
    }

eAtRet Tha60210012ModulePdaHdlcServiceLoopkupReset(AtHdlcChannel physicalChannel)
    {
    if (ThaHdlcChannelCircuitIsHo((ThaHdlcChannel)physicalChannel))
        return HoHdlcServiceLoopkupReset(physicalChannel);

    return LoHdlcServiceLookupReset(physicalChannel);
    }

eAtRet Tha60210012ModulePdaHdlcTdmPayloadMode(AtHdlcChannel physicalChannel, uint8 hwPayloadType)
    {
    if (ThaHdlcChannelCircuitIsHo((ThaHdlcChannel)physicalChannel))
        return HoHdlcTdmPayloadMode(physicalChannel, HoHwPdaMode(physicalChannel));

    return LoHdlcTdmPayloadMode(physicalChannel, hwPayloadType);
    }

eAtRet Tha60210012ModulePdaHdlcTdmPayloadModeReset(AtHdlcChannel physicalChannel)
    {
    /* As hardware recommend, reset all service type to type 0 */
    if (ThaHdlcChannelCircuitIsHo((ThaHdlcChannel)physicalChannel))
        return HoHdlcTdmPayloadMode(physicalChannel, 0);

    return LoHdlcTdmPayloadMode(physicalChannel, 0);
    }

eAtRet Tha60210012ModulePdaPppLinkMpBundleLookupSet(AtHdlcLink link, AtHdlcBundle bundle)
    {
    AtHdlcChannel hdlcChannel = AtHdlcLinkHdlcChannelGet(link);
    uint32 offset = HdlcServiceLoTdmLookupOffset(hdlcChannel);
    uint32 regAddress = cAf6Reg_ramlotdmlkupcfg_Base + offset;
    uint32 regValue = mChannelHwRead(hdlcChannel, regAddress, cThaModulePda);

    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_PwCesEnable_, 0); /* Run MLPPP/PPP service */
    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_MlpppBundId_, AtChannelIdGet((AtChannel)bundle));
    mChannelHwWrite(hdlcChannel, regAddress, regValue, cThaModulePda);

    return cAtOk;
    }

eAtRet Tha60210012ModulePdaPppLinkMpBundleLookupReSet(AtHdlcLink link)
    {
    uint32 offset, regAddress, regValue;
    AtHdlcChannel hdlcChannel = AtHdlcLinkHdlcChannelGet(link);
    static const uint16 cResetChannelId = 0x1FF;

    offset = HdlcServiceLoTdmLookupOffset(hdlcChannel);
    regAddress = cAf6Reg_ramlotdmlkupcfg_Base + offset;
    regValue = mChannelHwRead(hdlcChannel, regAddress, cThaModulePda);
    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_PwCesEnable_, 0); /* Run MLPPP/PPP service */
    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_MlpppBundId_, cResetChannelId);
    mChannelHwWrite(hdlcChannel, regAddress, regValue, cThaModulePda);

    return cAtOk;
    }

eAtRet Tha60210012ModulePdaPppLinkMpBundleLookupEnable(AtHdlcLink link, eBool enable)
    {
    uint32 offset, regAddress, regValue;
    AtHdlcChannel hdlcChannel = AtHdlcLinkHdlcChannelGet(link);

    offset = HdlcServiceLoTdmLookupOffset(hdlcChannel);
    regAddress = cAf6Reg_ramlotdmlkupcfg_Base + offset;
    regValue = mChannelHwRead(hdlcChannel, regAddress, cThaModulePda);
    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_PwLkEnable_, mBoolToBin(enable));
    mRegFieldSet(regValue, cAf6_ramlotdmlkupcfg_MlpppEn_, mBoolToBin(enable));
    mChannelHwWrite(hdlcChannel, regAddress, regValue, cThaModulePda);

    return cAtOk;
    }

eAtRet Tha60210012ModulePdaEXauiEnable(AtModule self, uint32 exauiId, eBool enabled)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!Tha60210012DeviceEXauiIsReady(AtModuleDeviceGet(self)))
        return ((enabled) ? cAtErrorNotReady : cAtOk);

    return EXauiEnable(self, exauiId, enabled);
    }

eBool Tha60210012ModulePdaEXauiIsEnabled(AtModule self, uint32 exauiId)
    {
    if (self == NULL)
        return cAtFalse;

    if (Tha60210012DeviceEXauiIsReady(AtModuleDeviceGet(self)))
        return EXauiIsEnabled(self, exauiId);

    return cAtFalse;
    }

eAtRet Tha60210012ModulePdaHdlcHwStsIdSet(AtHdlcChannel hdlcChannel, uint8 hwSts)
    {
    uint32 regAddress, regValue;
    if (ThaHdlcChannelCircuitIsHo((ThaHdlcChannel)hdlcChannel))
        return cAtOk;

    regAddress = cAf6Reg_ramlotdmmodecfg_Base + HdlcServiceLoTdmModeOffset(hdlcChannel);
    regValue = mChannelHwRead(hdlcChannel, regAddress, cThaModulePda);
    mRegFieldSet(regValue, cAf6_ramlotdmmodecfg_PDAStsId_, hwSts);
    mChannelHwWrite(hdlcChannel, regAddress, regValue, cThaModulePda);

    return cAtOk;
    }

uint32 Tha60210012ModulePdaGeBypassLookUpControlReg(ThaModulePda self)
    {
    return GeBypassLookUpControlReg((AtModule)self);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60210012ModulePdaReg.h
 * 
 * Created Date: Feb 29, 2016
 *
 * Description : PDA Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEPDAREG_H_
#define _THA60210012MODULEPDAREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Jitter Buffer Control
Reg Addr   : 0x00010000 - 0x00011FFF #The address format for these registers is 0x00010000 + PWID
Reg Formula: 0x00010000 +  PWID
    Where  :
           + $PWID(0-3071): Pseudowire ID
Reg Desc   :
This register configures jitter buffer parameters per pseudo-wire

------------------------------------------------------------------------------*/
#define cAf6Reg_ramjitbufcfg_Base                                                                   0x00010000
#define cAf6Reg_ramjitbufcfg(PWID)                                                         (0x00010000+(PWID))
#define cAf6Reg_ramjitbufcfg_WidthVal                                                                       64
#define cAf6Reg_ramjitbufcfg_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: PwCEPMode
BitField Type: RW
BitField Desc: Pseudo-wire CEP mode indication
BitField Bits: [55]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwCEPMode_Bit_Start                                                               55
#define cAf6_ramjitbufcfg_PwCEPMode_Bit_End                                                                 55
#define cAf6_ramjitbufcfg_PwCEPMode_Mask                                                                cBit23
#define cAf6_ramjitbufcfg_PwCEPMode_Shift                                                                   23
#define cAf6_ramjitbufcfg_PwCEPMode_MaxVal                                                                 0x0
#define cAf6_ramjitbufcfg_PwCEPMode_MinVal                                                                 0x0
#define cAf6_ramjitbufcfg_PwCEPMode_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: PwHoLoOc48Id
BitField Type: RW
BitField Desc: Indicate 2x Hi order OC48 or 2x Low order OC48 slice
BitField Bits: [54:53]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_Bit_Start                                                            53
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_Bit_End                                                              54
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_Mask                                                          cBit22_21
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_Shift                                                                21
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_MaxVal                                                              0x0
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_MinVal                                                              0x0
#define cAf6_ramjitbufcfg_PwHoLoOc48Id_RstVal                                                              0x0

/*--------------------------------------
BitField Name: PwLoSlice24Sel
BitField Type: RW
BitField Desc: Pseudo-wire Low order OC24 slice selection, only valid in Lo
Pseudo-wire 1: This PW belong to Slice24 that trasnport STS 1,3,5,...,47 within
a Lo OC48 0: This PW belong to Slice24 that trasnport STS 0,2,4,...,46 within a
Lo OC48
BitField Bits: [52]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_Bit_Start                                                          52
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_Bit_End                                                            52
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_Mask                                                           cBit20
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_Shift                                                              20
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_MaxVal                                                            0x0
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_MinVal                                                            0x0
#define cAf6_ramjitbufcfg_PwLoSlice24Sel_RstVal                                                            0x0

/*--------------------------------------
BitField Name: PwTdmLineId
BitField Type: RW
BitField Desc: Pseudo-wire(PW) corresponding TDM line ID. If the PW belong to
Low order path, this is the OC24 TDM line ID that is using in Lo CDR,PDH and
MAP. If the PW belong to Hi order CEP path, this is the OC48 master STS ID
BitField Bits: [51:42]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwTdmLineId_Bit_Start                                                             42
#define cAf6_ramjitbufcfg_PwTdmLineId_Bit_End                                                               51
#define cAf6_ramjitbufcfg_PwTdmLineId_Mask                                                           cBit19_10
#define cAf6_ramjitbufcfg_PwTdmLineId_Shift                                                                 10
#define cAf6_ramjitbufcfg_PwTdmLineId_MaxVal                                                               0x0
#define cAf6_ramjitbufcfg_PwTdmLineId_MinVal                                                               0x0
#define cAf6_ramjitbufcfg_PwTdmLineId_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PwEparEn
BitField Type: RW
BitField Desc: Pseudo-wire EPAR timing mode enable 1: Enable EPAR timing mode 0:
Disable EPAR timing mode
BitField Bits: [41]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwEparEn_Bit_Start                                                                41
#define cAf6_ramjitbufcfg_PwEparEn_Bit_End                                                                  41
#define cAf6_ramjitbufcfg_PwEparEn_Mask                                                                  cBit9
#define cAf6_ramjitbufcfg_PwEparEn_Shift                                                                     9
#define cAf6_ramjitbufcfg_PwEparEn_MaxVal                                                                  0x0
#define cAf6_ramjitbufcfg_PwEparEn_MinVal                                                                  0x0
#define cAf6_ramjitbufcfg_PwEparEn_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: PwHiLoPathInd
BitField Type: RW
BitField Desc: Pseudo-wire belong to Hi-order or Lo-order path 1: Pseudo-wire
belong to Hi-order path 0: Pseudo-wire belong to Lo-order path
BitField Bits: [40]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwHiLoPathInd_Bit_Start                                                           40
#define cAf6_ramjitbufcfg_PwHiLoPathInd_Bit_End                                                             40
#define cAf6_ramjitbufcfg_PwHiLoPathInd_Mask                                                             cBit8
#define cAf6_ramjitbufcfg_PwHiLoPathInd_Shift                                                                8
#define cAf6_ramjitbufcfg_PwHiLoPathInd_MaxVal                                                             0x0
#define cAf6_ramjitbufcfg_PwHiLoPathInd_MinVal                                                             0x0
#define cAf6_ramjitbufcfg_PwHiLoPathInd_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PwPayloadLen
BitField Type: RW
BitField Desc: TDM Payload of pseudo-wire in byte unit
BitField Bits: [39:26]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PwPayloadLen_Bit_Start                                                            26
#define cAf6_ramjitbufcfg_PwPayloadLen_Bit_End                                                              39
#define cAf6_ramjitbufcfg_PwPayloadLen_Mask_01                                                       cBit31_26
#define cAf6_ramjitbufcfg_PwPayloadLen_Shift_01                                                             26
#define cAf6_ramjitbufcfg_PwPayloadLen_Mask_02                                                         cBit7_0
#define cAf6_ramjitbufcfg_PwPayloadLen_Shift_02                                                              0

/*--------------------------------------
BitField Name: PdvSizeInPkUnit
BitField Type: RW
BitField Desc: Pdv size in packet unit. This parameter is to prevent packet
delay variation(PDV) from PSN. PdvSizePk is packet delay variation measured in
packet unit. The formula is as below: PdvSizeInPkUnit = (PwSpeedinKbps *
PdvInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps *
PdvInUsus)%(8000*PwPayloadLen) !=0)
BitField Bits: [25:13]
--------------------------------------*/
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_Bit_Start                                                         13
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_Bit_End                                                           25
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_Mask                                                       cBit25_13
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_Shift                                                             13
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_MaxVal                                                        0x1fff
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_MinVal                                                           0x0
#define cAf6_ramjitbufcfg_PdvSizeInPkUnit_RstVal                                                           0x0

/*--------------------------------------
BitField Name: JitBufSizeInPkUnit
BitField Type: RW
BitField Desc: Jitter buffer size in packet unit. This parameter is mostly
double of PdvSizeInPkUnit. The formula is as below: JitBufSizeInPkUnit =
(PwSpeedinKbps * JitBufInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps *
JitBufInUsus)%(8000*PwPayloadLen) !=0)
BitField Bits: [12:0]
--------------------------------------*/
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_Bit_Start                                                       0
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_Bit_End                                                        12
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_Mask                                                     cBit12_0
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_Shift                                                           0
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_MaxVal                                                     0x1fff
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_MinVal                                                        0x0
#define cAf6_ramjitbufcfg_JitBufSizeInPkUnit_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Jitter Buffer Status
Reg Addr   : 0x00014000 - 0x00015FFF #The address format for these registers is 0x00014000 + PWID
Reg Formula: 0x00014000 +  PWID
    Where  :
           + $PWID(0-3071): Pseudowire ID
Reg Desc   :
This register shows jitter buffer status per pseudo-wire

------------------------------------------------------------------------------*/
#define cAf6Reg_ramjitbufsta_Base                                                                   0x00014000
#define cAf6Reg_ramjitbufsta(PWID)                                                         (0x00014000+(PWID))
#define cAf6Reg_ramjitbufsta_WidthVal                                                                       64
#define cAf6Reg_ramjitbufsta_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: JitBufHwComSta
BitField Type: RO
BitField Desc: Harware debug only status
BitField Bits: [54:20]
--------------------------------------*/
#define cAf6_ramjitbufsta_JitBufHwComSta_Bit_Start                                                          20
#define cAf6_ramjitbufsta_JitBufHwComSta_Bit_End                                                            54
#define cAf6_ramjitbufsta_JitBufHwComSta_Mask_01                                                     cBit31_20
#define cAf6_ramjitbufsta_JitBufHwComSta_Shift_01                                                           20
#define cAf6_ramjitbufsta_JitBufHwComSta_Mask_02                                                      cBit22_0
#define cAf6_ramjitbufsta_JitBufHwComSta_Shift_02                                                            0

/*--------------------------------------
BitField Name: JitBufNumPk
BitField Type: RW
BitField Desc: Current number of packet in jitter buffer
BitField Bits: [19:4]
--------------------------------------*/
#define cAf6_ramjitbufsta_JitBufNumPk_Bit_Start                                                              4
#define cAf6_ramjitbufsta_JitBufNumPk_Bit_End                                                               19
#define cAf6_ramjitbufsta_JitBufNumPk_Mask                                                            cBit19_4
#define cAf6_ramjitbufsta_JitBufNumPk_Shift                                                                  4
#define cAf6_ramjitbufsta_JitBufNumPk_MaxVal                                                            0xffff
#define cAf6_ramjitbufsta_JitBufNumPk_MinVal                                                               0x0
#define cAf6_ramjitbufsta_JitBufNumPk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: JitBufFull
BitField Type: RW
BitField Desc: Jitter Buffer full status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ramjitbufsta_JitBufFull_Bit_Start                                                               3
#define cAf6_ramjitbufsta_JitBufFull_Bit_End                                                                 3
#define cAf6_ramjitbufsta_JitBufFull_Mask                                                                cBit3
#define cAf6_ramjitbufsta_JitBufFull_Shift                                                                   3
#define cAf6_ramjitbufsta_JitBufFull_MaxVal                                                                0x1
#define cAf6_ramjitbufsta_JitBufFull_MinVal                                                                0x0
#define cAf6_ramjitbufsta_JitBufFull_RstVal                                                                0x0

/*--------------------------------------
BitField Name: JitBufState
BitField Type: RW
BitField Desc: Jitter buffer state machine status 0: START 1: FILL 2: READY 3:
READ 4: LOST 5: NEAR_EMPTY Others: NOT VALID
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ramjitbufsta_JitBufState_Bit_Start                                                              0
#define cAf6_ramjitbufsta_JitBufState_Bit_End                                                                2
#define cAf6_ramjitbufsta_JitBufState_Mask                                                             cBit2_0
#define cAf6_ramjitbufsta_JitBufState_Shift                                                                  0
#define cAf6_ramjitbufsta_JitBufState_MaxVal                                                               0x7
#define cAf6_ramjitbufsta_JitBufState_MinVal                                                               0x0
#define cAf6_ramjitbufsta_JitBufState_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Reorder Control
Reg Addr   : 0x00020000 - 0x00021FFF #The address format for these registers is 0x00020000 + PWID
Reg Formula: 0x00020000 +  PWID
    Where  :
           + $PWID(0-3071): Pseudowire ID
Reg Desc   :
This register configures reorder parameters per pseudo-wire

------------------------------------------------------------------------------*/
#define cAf6Reg_ramreorcfg_Base                                                                     0x00020000
#define cAf6Reg_ramreorcfg(PWID)                                                           (0x00020000+(PWID))
#define cAf6Reg_ramreorcfg_WidthVal                                                                         32
#define cAf6Reg_ramreorcfg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: PwSetLofsInPk
BitField Type: RW
BitField Desc: Number of consecutive lost packet to declare lost of packet state
BitField Bits: [25:17]
--------------------------------------*/
#define cAf6_ramreorcfg_PwSetLofsInPk_Bit_Start                                                             17
#define cAf6_ramreorcfg_PwSetLofsInPk_Bit_End                                                               25
#define cAf6_ramreorcfg_PwSetLofsInPk_Mask                                                           cBit25_17
#define cAf6_ramreorcfg_PwSetLofsInPk_Shift                                                                 17
#define cAf6_ramreorcfg_PwSetLofsInPk_MaxVal                                                             0x1ff
#define cAf6_ramreorcfg_PwSetLofsInPk_MinVal                                                               0x0
#define cAf6_ramreorcfg_PwSetLofsInPk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PwSetLopsInMsec
BitField Type: RW
BitField Desc: Number of empty time to declare lost of packet synchronization
BitField Bits: [16:9]
--------------------------------------*/
#define cAf6_ramreorcfg_PwSetLopsInMsec_Bit_Start                                                            9
#define cAf6_ramreorcfg_PwSetLopsInMsec_Bit_End                                                             16
#define cAf6_ramreorcfg_PwSetLopsInMsec_Mask                                                          cBit16_9
#define cAf6_ramreorcfg_PwSetLopsInMsec_Shift                                                                9
#define cAf6_ramreorcfg_PwSetLopsInMsec_MaxVal                                                            0xff
#define cAf6_ramreorcfg_PwSetLopsInMsec_MinVal                                                             0x0
#define cAf6_ramreorcfg_PwSetLopsInMsec_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PwReorEn
BitField Type: RW
BitField Desc: Set 1 to enable reorder
BitField Bits: [8]
--------------------------------------*/
#define cAf6_ramreorcfg_PwReorEn_Bit_Start                                                                   8
#define cAf6_ramreorcfg_PwReorEn_Bit_End                                                                     8
#define cAf6_ramreorcfg_PwReorEn_Mask                                                                    cBit8
#define cAf6_ramreorcfg_PwReorEn_Shift                                                                       8
#define cAf6_ramreorcfg_PwReorEn_MaxVal                                                                    0x1
#define cAf6_ramreorcfg_PwReorEn_MinVal                                                                    0x0
#define cAf6_ramreorcfg_PwReorEn_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: PwReorTimeout
BitField Type: RW
BitField Desc: Reorder timeout in 512us unit to detect lost packets. The formula
is as below: ReorTimeout = ((Min(31,PdvSizeInPk) * PwPayloadLen *
16)/PwSpeedInKbps
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_ramreorcfg_PwReorTimeout_Bit_Start                                                              0
#define cAf6_ramreorcfg_PwReorTimeout_Bit_End                                                                7
#define cAf6_ramreorcfg_PwReorTimeout_Mask                                                             cBit7_0
#define cAf6_ramreorcfg_PwReorTimeout_Shift                                                                  0
#define cAf6_ramreorcfg_PwReorTimeout_MaxVal                                                              0xff
#define cAf6_ramreorcfg_PwReorTimeout_MinVal                                                               0x0
#define cAf6_ramreorcfg_PwReorTimeout_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Low Order TDM mode Control
Reg Addr   : 0x00030000 - 0x0003FFFF #The address format for low order path is 0x00030000 +  LoOc48ID*0x1000 + LoOrderoc24Slice*0x400 + TdmOc24Pwid
Reg Formula: 0x00030000 + $LoOc48ID*0x1000 + $LoOrderoc24Slice*0x400 + $TdmOc24Pwid
    Where  :
           + $LoOc48ID(0-1): LoOC48 ID
           + $LoOrderoc24Slice(0-1): Lo-order slice oc24
           + $TdmOc24Pwid(0-1023): TDM OC24 slice PWID
Reg Desc   :
This register configure TDM mode for interworking between Pseudowire and Lo TDM

------------------------------------------------------------------------------*/
#define cAf6Reg_ramlotdmmodecfg_Base                                                                0x00030000
#define cAf6Reg_ramlotdmmodecfg(LoOc48ID, LoOrderoc24Slice, TdmOc24Pwid)(0x00030000+(LoOc48ID)*0x1000+(LoOrderoc24Slice)*0x400+(TdmOc24Pwid))
#define cAf6Reg_ramlotdmmodecfg_WidthVal                                                                    32
#define cAf6Reg_ramlotdmmodecfg_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PDANxDS0
BitField Type: RO
BitField Desc: Number of DS0 allocated for CESoP PW
BitField Bits: [27:22]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDANxDS0_Bit_Start                                                             22
#define cAf6_ramlotdmmodecfg_PDANxDS0_Bit_End                                                               27
#define cAf6_ramlotdmmodecfg_PDANxDS0_Mask                                                           cBit27_22
#define cAf6_ramlotdmmodecfg_PDANxDS0_Shift                                                                 22
#define cAf6_ramlotdmmodecfg_PDANxDS0_MaxVal                                                              0x3f
#define cAf6_ramlotdmmodecfg_PDANxDS0_MinVal                                                               0x0
#define cAf6_ramlotdmmodecfg_PDANxDS0_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PDAIdleCode
BitField Type: RW
BitField Desc: Idle pattern to replace data in case of Lost/Lbit packet
BitField Bits: [21:14]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDAIdleCode_Bit_Start                                                          14
#define cAf6_ramlotdmmodecfg_PDAIdleCode_Bit_End                                                            21
#define cAf6_ramlotdmmodecfg_PDAIdleCode_Mask                                                        cBit21_14
#define cAf6_ramlotdmmodecfg_PDAIdleCode_Shift                                                              14
#define cAf6_ramlotdmmodecfg_PDAIdleCode_MaxVal                                                           0xff
#define cAf6_ramlotdmmodecfg_PDAIdleCode_MinVal                                                            0x0
#define cAf6_ramlotdmmodecfg_PDAIdleCode_RstVal                                                            0x0

/*--------------------------------------
BitField Name: PDAAisRdiOff_BundEn
BitField Type: RW
BitField Desc: Set 1 to indicate the link is joined in MLPPP bundle or disable
sending AIS/Unequip/RDI from CES PW to TDM
BitField Bits: [13]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDAAisRdiOff_BundEn_Bit_Start                                                  13
#define cAf6_ramlotdmmodecfg_PDAAisRdiOff_BundEn_Bit_End                                                    13
#define cAf6_ramlotdmmodecfg_PDAAisRdiOff_BundEn_Mask                                                   cBit13
#define cAf6_ramlotdmmodecfg_PDAAisRdiOff_BundEn_Shift                                                      13
#define cAf6_ramlotdmmodecfg_PDAAisRdiOff_BundEn_MaxVal                                                    0x1
#define cAf6_ramlotdmmodecfg_PDAAisRdiOff_BundEn_MinVal                                                    0x0
#define cAf6_ramlotdmmodecfg_PDAAisRdiOff_BundEn_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PDARepMode
BitField Type: RW
BitField Desc: Mode to replace lost and Lbit packet 0: Replace by replaying
previous good packet (often for voice or video application) 1: Replace by AIS
(default) 2: Replace by a configuration idle code Replace by a configuration
idle code
BitField Bits: [12:11]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDARepMode_Bit_Start                                                           11
#define cAf6_ramlotdmmodecfg_PDARepMode_Bit_End                                                             12
#define cAf6_ramlotdmmodecfg_PDARepMode_Mask                                                         cBit12_11
#define cAf6_ramlotdmmodecfg_PDARepMode_Shift                                                               11
#define cAf6_ramlotdmmodecfg_PDARepMode_MaxVal                                                             0x3
#define cAf6_ramlotdmmodecfg_PDARepMode_MinVal                                                             0x0
#define cAf6_ramlotdmmodecfg_PDARepMode_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PDAStsId
BitField Type: RW
BitField Desc: Only used in DS3/E3 SAToP and TU3/VC3 CEP basic, corresponding
Slice OC24 STS ID of PW circuit
BitField Bits: [9:5]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDAStsId_Bit_Start                                                              5
#define cAf6_ramlotdmmodecfg_PDAStsId_Bit_End                                                                9
#define cAf6_ramlotdmmodecfg_PDAStsId_Mask                                                             cBit9_5
#define cAf6_ramlotdmmodecfg_PDAStsId_Shift                                                                  5
#define cAf6_ramlotdmmodecfg_PDAStsId_MaxVal                                                              0x1f
#define cAf6_ramlotdmmodecfg_PDAStsId_MinVal                                                               0x0
#define cAf6_ramlotdmmodecfg_PDAStsId_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PDAMode
BitField Type: RW
BitField Desc: TDM Payload De-Assembler modes 0: DS1/E1 SAToP or MLPPP/PPP over
DS1/E1 2: CESoP without CAS 8: VC3/TU3 CEP basic 12: VC11/VC12 CEP basic 15:
DS3/E3 SAToP or MLPPP/PPP over DS1/E1
BitField Bits: [4:1]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDAMode_Bit_Start                                                               1
#define cAf6_ramlotdmmodecfg_PDAMode_Bit_End                                                                 4
#define cAf6_ramlotdmmodecfg_PDAMode_Mask                                                              cBit4_1
#define cAf6_ramlotdmmodecfg_PDAMode_Shift                                                                   1
#define cAf6_ramlotdmmodecfg_PDAMode_MaxVal                                                                0xf
#define cAf6_ramlotdmmodecfg_PDAMode_MinVal                                                                0x0
#define cAf6_ramlotdmmodecfg_PDAMode_RstVal                                                                0x0

/*--------------------------------------
BitField Name: PDAFracEbmEn
BitField Type: RO
BitField Desc: EBM enable in CEP fractional mode 0: EBM disable 1: EBM enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_Bit_Start                                                          0
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_Bit_End                                                            0
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_Mask                                                           cBit0
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_Shift                                                              0
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_MaxVal                                                           0x1
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_MinVal                                                           0x0
#define cAf6_ramlotdmmodecfg_PDAFracEbmEn_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Low Order TDM Look Up Control
Reg Addr   : 0x00040000 - 0x00040FFF #The address format for low order path is 0x00040000 +  LoOc48ID*0x800 + LoOc24Slice*0x400 + TdmOc24Pwid
Reg Formula: 0x00040000 + LoOc48ID*0x800 + LoOc24Slice*0x400 + TdmOc24PwID
    Where  :
           + $LoOc48ID(0-1): LoOC48 ID
           + $LoOc24Slice(0-1): Lo-order slice oc24
           + TdmOc24PwID(0-1023): TDM OC24 slice PWID
Reg Desc   :
This register configure lookup from TDM PWID to global 3072 PWID

------------------------------------------------------------------------------*/
#define cAf6Reg_ramlotdmlkupcfg_Base                                                                0x00040000
#define cAf6Reg_ramlotdmlkupcfg(LoOc48ID, LoOc24Slice, TdmOc24PwID)   (0x00040000+(LoOc48ID)*0x800+(LoOc24Slice)*0x400+(TdmOc24PwID))
#define cAf6Reg_ramlotdmlkupcfg_WidthVal                                                                    32
#define cAf6Reg_ramlotdmlkupcfg_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: MlpppBundId
BitField Type: RW
BitField Desc: MLPPP bundle ID
BitField Bits: [23:15]
--------------------------------------*/
#define cAf6_ramlotdmlkupcfg_MlpppBundId_Bit_Start                                                          15
#define cAf6_ramlotdmlkupcfg_MlpppBundId_Bit_End                                                            23
#define cAf6_ramlotdmlkupcfg_MlpppBundId_Mask                                                        cBit23_15
#define cAf6_ramlotdmlkupcfg_MlpppBundId_Shift                                                              15
#define cAf6_ramlotdmlkupcfg_MlpppBundId_MaxVal                                                          0x1ff
#define cAf6_ramlotdmlkupcfg_MlpppBundId_MinVal                                                            0x0
#define cAf6_ramlotdmlkupcfg_MlpppBundId_RstVal                                                            0x0

/*--------------------------------------
BitField Name: MlpppEn
BitField Type: RW
BitField Desc: MLPPP enable 0: Link run PPP mode 1: Link join in a bundle in
MLPPP mode
BitField Bits: [14]
--------------------------------------*/
#define cAf6_ramlotdmlkupcfg_MlpppEn_Bit_Start                                                              14
#define cAf6_ramlotdmlkupcfg_MlpppEn_Bit_End                                                                14
#define cAf6_ramlotdmlkupcfg_MlpppEn_Mask                                                               cBit14
#define cAf6_ramlotdmlkupcfg_MlpppEn_Shift                                                                  14
#define cAf6_ramlotdmlkupcfg_MlpppEn_MaxVal                                                                0x1
#define cAf6_ramlotdmlkupcfg_MlpppEn_MinVal                                                                0x0
#define cAf6_ramlotdmlkupcfg_MlpppEn_RstVal                                                                0x0

/*--------------------------------------
BitField Name: PwCesEnable
BitField Type: RW
BitField Desc: CES PW Enable 0: MLPPP/PPP mode 1: CES PW mode
BitField Bits: [13]
--------------------------------------*/
#define cAf6_ramlotdmlkupcfg_PwCesEnable_Bit_Start                                                          13
#define cAf6_ramlotdmlkupcfg_PwCesEnable_Bit_End                                                            13
#define cAf6_ramlotdmlkupcfg_PwCesEnable_Mask                                                           cBit13
#define cAf6_ramlotdmlkupcfg_PwCesEnable_Shift                                                              13
#define cAf6_ramlotdmlkupcfg_PwCesEnable_MaxVal                                                            0x1
#define cAf6_ramlotdmlkupcfg_PwCesEnable_MinVal                                                            0x0
#define cAf6_ramlotdmlkupcfg_PwCesEnable_RstVal                                                            0x0

/*--------------------------------------
BitField Name: PwLkEnable
BitField Type: RW
BitField Desc: Enable Lookup
BitField Bits: [12]
--------------------------------------*/
#define cAf6_ramlotdmlkupcfg_PwLkEnable_Bit_Start                                                           12
#define cAf6_ramlotdmlkupcfg_PwLkEnable_Bit_End                                                             12
#define cAf6_ramlotdmlkupcfg_PwLkEnable_Mask                                                            cBit12
#define cAf6_ramlotdmlkupcfg_PwLkEnable_Shift                                                               12
#define cAf6_ramlotdmlkupcfg_PwLkEnable_MaxVal                                                             0x1
#define cAf6_ramlotdmlkupcfg_PwLkEnable_MinVal                                                             0x0
#define cAf6_ramlotdmlkupcfg_PwLkEnable_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PwID
BitField Type: RW
BitField Desc: Flat 3072 CES PWID or other service ID
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_ramlotdmlkupcfg_PwID_Bit_Start                                                                  0
#define cAf6_ramlotdmlkupcfg_PwID_Bit_End                                                                   11
#define cAf6_ramlotdmlkupcfg_PwID_Mask                                                                cBit11_0
#define cAf6_ramlotdmlkupcfg_PwID_Shift                                                                      0
#define cAf6_ramlotdmlkupcfg_PwID_MaxVal                                                                 0xfff
#define cAf6_ramlotdmlkupcfg_PwID_MinVal                                                                   0x0
#define cAf6_ramlotdmlkupcfg_PwID_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA VCAT Look Up Control
Reg Addr   : 0x00041000 - 0x000410FF #The address format for low order path is 0x00041000 +  VcgId
Reg Formula: 0x00041000 + VcgID
    Where  :
           + VcgID(0-255): VCAT VC group ID
Reg Desc   :
This register configure lookup enable of VCAT VC group

------------------------------------------------------------------------------*/
#define cAf6Reg_ramvcatlkupcfg_Base                                                                 0x00041000
#define cAf6Reg_ramvcatlkupcfg(VcgID)                                                     (0x00041000+(VcgID))
#define cAf6Reg_ramvcatlkupcfg_WidthVal                                                                     32
#define cAf6Reg_ramvcatlkupcfg_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: PwReserve
BitField Type: RW
BitField Desc: Must set to zero
BitField Bits: [17:13]
--------------------------------------*/
#define cAf6_ramvcatlkupcfg_PwReserve_Bit_Start                                                             13
#define cAf6_ramvcatlkupcfg_PwReserve_Bit_End                                                               17
#define cAf6_ramvcatlkupcfg_PwReserve_Mask                                                           cBit17_13
#define cAf6_ramvcatlkupcfg_PwReserve_Shift                                                                 13
#define cAf6_ramvcatlkupcfg_PwReserve_MaxVal                                                              0x1f
#define cAf6_ramvcatlkupcfg_PwReserve_MinVal                                                               0x0
#define cAf6_ramvcatlkupcfg_PwReserve_RstVal                                                               0x0

/*--------------------------------------
BitField Name: VcgLkEnable
BitField Type: RW
BitField Desc: VC group lookup enable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_ramvcatlkupcfg_VcgLkEnable_Bit_Start                                                           12
#define cAf6_ramvcatlkupcfg_VcgLkEnable_Bit_End                                                             12
#define cAf6_ramvcatlkupcfg_VcgLkEnable_Mask                                                            cBit12
#define cAf6_ramvcatlkupcfg_VcgLkEnable_Shift                                                               12
#define cAf6_ramvcatlkupcfg_VcgLkEnable_MaxVal                                                             0x1
#define cAf6_ramvcatlkupcfg_VcgLkEnable_MinVal                                                             0x0
#define cAf6_ramvcatlkupcfg_VcgLkEnable_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA High Order TDM Look Up Control
Reg Addr   : 0x00041100 - 0x0004112F #The address format for high order path is 0x00041800 +  HoOc48ID*0x40 + HoMasSTS
Reg Formula: 0x00041100 + HoOc48ID*0x40 + HoMasSTS
    Where  :
           + $HoOc48ID(0-1): HoOC48 ID
           + $HoMasSTS(0-47): HO TDM per OC48 master STS ID
Reg Desc   :
This register configure lookup from HO TDM master STSIS to global 6144 PWID

------------------------------------------------------------------------------*/
#define cAf6Reg_ramhotdmlkupcfg_Base                                                                0x00041800
#define cAf6Reg_ramhotdmlkupcfg_BaseNew                                                             0x00041100
#define cAf6Reg_ramhotdmlkupcfg(HoOc48ID, HoMasSTS)                    (0x00041800+(HoOc48ID)*0x40+(HoMasSTS))
#define cAf6Reg_ramhotdmlkupcfg_WidthVal                                                                    32
#define cAf6Reg_ramhotdmlkupcfg_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PwReserve
BitField Type: RW
BitField Desc: Must set to zero
BitField Bits: [16:14]
--------------------------------------*/
#define cAf6_ramhotdmlkupcfg_PwReserve_Bit_Start                                                            14
#define cAf6_ramhotdmlkupcfg_PwReserve_Bit_End                                                              16
#define cAf6_ramhotdmlkupcfg_PwReserve_Mask                                                          cBit16_14
#define cAf6_ramhotdmlkupcfg_PwReserve_Shift                                                                14
#define cAf6_ramhotdmlkupcfg_PwReserve_MaxVal                                                              0x7
#define cAf6_ramhotdmlkupcfg_PwReserve_MinVal                                                              0x0
#define cAf6_ramhotdmlkupcfg_PwReserve_RstVal                                                              0x0

/*--------------------------------------
BitField Name: PwCesEnable
BitField Type: RW
BitField Desc: CES PW Enable 0: MLPPP/PPP mode 1: CES PW mode
BitField Bits: [13]
--------------------------------------*/
#define cAf6_ramhotdmlkupcfg_PwCesEnable_Bit_Start                                                          13
#define cAf6_ramhotdmlkupcfg_PwCesEnable_Bit_End                                                            13
#define cAf6_ramhotdmlkupcfg_PwCesEnable_Mask                                                           cBit13
#define cAf6_ramhotdmlkupcfg_PwCesEnable_Shift                                                              13
#define cAf6_ramhotdmlkupcfg_PwCesEnable_MaxVal                                                            0x1
#define cAf6_ramhotdmlkupcfg_PwCesEnable_MinVal                                                            0x0
#define cAf6_ramhotdmlkupcfg_PwCesEnable_RstVal                                                            0x0

/*--------------------------------------
BitField Name: PwLkEnable
BitField Type: RW
BitField Desc: Enable Lookup
BitField Bits: [12]
--------------------------------------*/
#define cAf6_ramhotdmlkupcfg_PwLkEnable_Bit_Start                                                           12
#define cAf6_ramhotdmlkupcfg_PwLkEnable_Bit_End                                                             12
#define cAf6_ramhotdmlkupcfg_PwLkEnable_Mask                                                            cBit12
#define cAf6_ramhotdmlkupcfg_PwLkEnable_Shift                                                               12
#define cAf6_ramhotdmlkupcfg_PwLkEnable_MaxVal                                                             0x1
#define cAf6_ramhotdmlkupcfg_PwLkEnable_MinVal                                                             0x0
#define cAf6_ramhotdmlkupcfg_PwLkEnable_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PwID
BitField Type: RW
BitField Desc: Flat 3072 PWID
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_ramhotdmlkupcfg_PwID_Bit_Start                                                                  0
#define cAf6_ramhotdmlkupcfg_PwID_Bit_End                                                                   11
#define cAf6_ramhotdmlkupcfg_PwID_Mask                                                                cBit11_0
#define cAf6_ramhotdmlkupcfg_PwID_Shift                                                                      0
#define cAf6_ramhotdmlkupcfg_PwID_MaxVal                                                                 0xfff
#define cAf6_ramhotdmlkupcfg_PwID_MinVal                                                                   0x0
#define cAf6_ramhotdmlkupcfg_PwID_RstVal                                                                   0x0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA VCAT Look Up Control
Reg Addr   : 0x00041000 - 0x000410FF #The address format for low order path is 0x00041000 +  VcgId
Reg Formula: 0x00041000 + VcgID
    Where  : 
           + VcgID(0-255): VCAT VC group ID
Reg Desc   : 
This register configure lookup enable of VCAT VC group

------------------------------------------------------------------------------*/
#define cAf6Reg_ramvcatlkupcfg_Base                                                                 0x00041000
#define cAf6Reg_ramvcatlkupcfg(VcgID)                                                     (0x00041000+(VcgID))
#define cAf6Reg_ramvcatlkupcfg_WidthVal                                                                     32
#define cAf6Reg_ramvcatlkupcfg_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: PwReserve
BitField Type: RW
BitField Desc: Must set to zero
BitField Bits: [17:13]
--------------------------------------*/
#define cAf6_ramvcatlkupcfg_PwReserve_Bit_Start                                                             13
#define cAf6_ramvcatlkupcfg_PwReserve_Bit_End                                                               17
#define cAf6_ramvcatlkupcfg_PwReserve_Mask                                                           cBit17_13
#define cAf6_ramvcatlkupcfg_PwReserve_Shift                                                                 13
#define cAf6_ramvcatlkupcfg_PwReserve_MaxVal                                                              0x1f
#define cAf6_ramvcatlkupcfg_PwReserve_MinVal                                                               0x0
#define cAf6_ramvcatlkupcfg_PwReserve_RstVal                                                               0x0

/*--------------------------------------
BitField Name: VcgLkEnable
BitField Type: RW
BitField Desc: VC group lookup enable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_ramvcatlkupcfg_VcgLkEnable_Bit_Start                                                           12
#define cAf6_ramvcatlkupcfg_VcgLkEnable_Bit_End                                                             12
#define cAf6_ramvcatlkupcfg_VcgLkEnable_Mask                                                            cBit12
#define cAf6_ramvcatlkupcfg_VcgLkEnable_Shift                                                               12
#define cAf6_ramvcatlkupcfg_VcgLkEnable_MaxVal                                                             0x1
#define cAf6_ramvcatlkupcfg_VcgLkEnable_MinVal                                                             0x0
#define cAf6_ramvcatlkupcfg_VcgLkEnable_RstVal                                                             0x0
#define cAf6_ramvcatlkupcfg_FlowId_Mask                                                                 cBit11_0
#define cAf6_ramvcatlkupcfg_FlowId_Shift                                                                   0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA GE Bypass Look Up Control
Reg Addr   : 0x00041180 - 0x00041805 #The address format for high order path is 0x00041C00 +  GeID
Reg Formula: 0x00041180 + GeId
    Where  :
           + $GeId(0-3): GE bypass ID
Reg Desc   :
This register configure lookup enable of GE bypass

------------------------------------------------------------------------------*/
#define cAf6Reg_ramgelkupcfg_Base_New                                                               0x00041180
#define cAf6Reg_ramgelkupcfg(GeId)                                                         (0x00041C00+(GeId))
#define cAf6Reg_ramgelkupcfg_WidthVal                                                                       32
#define cAf6Reg_ramgelkupcfg_WriteMask                                                                     0x0
/*--------------------------------------
BitField Name: GeLkEnable
BitField Type: RW
BitField Desc: Enable Lookup
BitField Bits: [12]
--------------------------------------*/
#define cAf6_ramgelkupcfg_GeLkEnable_Bit_Start                                                              12
#define cAf6_ramgelkupcfg_GeLkEnable_Bit_End                                                                12
#define cAf6_ramgelkupcfg_GeLkEnable_Mask                                                               cBit12
#define cAf6_ramgelkupcfg_GeLkEnable_Shift                                                                  12
#define cAf6_ramgelkupcfg_GeLkEnable_MaxVal                                                                0x1
#define cAf6_ramgelkupcfg_GeLkEnable_MinVal                                                                0x0
#define cAf6_ramgelkupcfg_GeLkEnable_RstVal                                                                0x0

/*--------------------------------------
BitField Name: GeServiceID
BitField Type: RW
BitField Desc: Service ID configured in CLA
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_ramgelkupcfg_GeServiceID_Bit_Start                                                              0
#define cAf6_ramgelkupcfg_GeServiceID_Bit_End                                                               11
#define cAf6_ramgelkupcfg_GeServiceID_Mask                                                            cBit11_0
#define cAf6_ramgelkupcfg_GeServiceID_Shift                                                                  0
#define cAf6_ramgelkupcfg_GeServiceID_MaxVal                                                             0xfff
#define cAf6_ramgelkupcfg_GeServiceID_MinVal                                                               0x0
#define cAf6_ramgelkupcfg_GeServiceID_RstVal                                                               0x0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA High Order TDM mode Control
Reg Addr   : 0x00050000 - 0x000501FF #The address format is 0x00050000 +  HoOc48ID*0x100 + HoMasSTS
Reg Formula: 0x00050000 + HoOc48ID*0x100 + HoMasSTS
    Where  :
           + $HoOc48ID(0-1): HoOC48 ID
           + $HoMasSTS(0-47): HO TDM per OC48 master STS ID
Reg Desc   :
This register configure TDM mode for interworking between Pseudowire and Ho TDM

------------------------------------------------------------------------------*/
#define cAf6Reg_ramhotdmmodecfg_Base                                                                0x00050000
#define cAf6Reg_ramhotdmmodecfg(HoOc48ID, HoMasSTS)                   (0x00050000+(HoOc48ID)*0x100+(HoMasSTS))
#define cAf6Reg_ramhotdmmodecfg_WidthVal                                                                    32
#define cAf6Reg_ramhotdmmodecfg_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PDAHoIdleCode
BitField Type: RW
BitField Desc: Idle pattern to replace data in case of Lost/Lbit packet
BitField Bits: [12:5]
--------------------------------------*/
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_Bit_Start                                                         5
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_Bit_End                                                          12
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_Mask                                                       cBit12_5
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_Shift                                                             5
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_MaxVal                                                         0xff
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_MinVal                                                          0x0
#define cAf6_ramhotdmmodecfg_PDAHoIdleCode_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PDAHoAisUneqOff
BitField Type: RW
BitField Desc: Set 1 to disable sending AIS/Unequip from PW to Ho TDM
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_Bit_Start                                                       4
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_Bit_End                                                         4
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_Mask                                                        cBit4
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_Shift                                                           4
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_MaxVal                                                        0x1
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_MinVal                                                        0x0
#define cAf6_ramhotdmmodecfg_PDAHoAisUneqOff_RstVal                                                        0x0

/*--------------------------------------
BitField Name: PDAHoRepMode
BitField Type: RW
BitField Desc: HO Mode to replace lost and Lbit packet 0: Replace by replaying
previous good packet (often for voice or video application) 1: Replace by AIS
(default) 2: Replace by a configuration idle code Replace by a configuration
idle code
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_Bit_Start                                                          2
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_Bit_End                                                            3
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_Mask                                                         cBit3_2
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_Shift                                                              2
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_MaxVal                                                           0x3
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_MinVal                                                           0x0
#define cAf6_ramhotdmmodecfg_PDAHoRepMode_RstVal                                                           0x0

/*--------------------------------------
BitField Name: PDAHoMode
BitField Type: RW
BitField Desc: TDM HO TDM mode 0: STS1 CEP basic 1: STS3/STS6 CEP basic 2:
STS9/STS12/STS15 CEP basic 3: STS18 to STS48 CEP basic
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ramhotdmmodecfg_PDAHoMode_Bit_Start                                                             0
#define cAf6_ramhotdmmodecfg_PDAHoMode_Bit_End                                                               1
#define cAf6_ramhotdmmodecfg_PDAHoMode_Mask                                                            cBit1_0
#define cAf6_ramhotdmmodecfg_PDAHoMode_Shift                                                                 0
#define cAf6_ramhotdmmodecfg_PDAHoMode_MaxVal                                                              0x3
#define cAf6_ramhotdmmodecfg_PDAHoMode_MinVal                                                              0x0
#define cAf6_ramhotdmmodecfg_PDAHoMode_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : PDA Hold Register Status
Reg Addr   : 0x000000 - 0x000002
Reg Formula: 0x000000 +  HID
    Where  :
           + $HID(0-2): Hold ID
Reg Desc   :
This register using for hold remain that more than 128bits

------------------------------------------------------------------------------*/
#define cAf6Reg_pda_hold_status_Base                                                                  0x000000
#define cAf6Reg_pda_hold_status(HID)                                                          (0x000000+(HID))
#define cAf6Reg_pda_hold_status_WidthVal                                                                    32
#define cAf6Reg_pda_hold_status_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PdaHoldStatus
BitField Type: RW
BitField Desc: Hold 32bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_pda_hold_status_PdaHoldStatus_Bit_Start                                                         0
#define cAf6_pda_hold_status_PdaHoldStatus_Bit_End                                                          31
#define cAf6_pda_hold_status_PdaHoldStatus_Mask                                                       cBit31_0
#define cAf6_pda_hold_status_PdaHoldStatus_Shift                                                             0
#define cAf6_pda_hold_status_PdaHoldStatus_MaxVal                                                   0xffffffff
#define cAf6_pda_hold_status_PdaHoldStatus_MinVal                                                          0x0
#define cAf6_pda_hold_status_PdaHoldStatus_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA per OC48 Low Order TDM PRBS J1 V5 Monitor Status
Reg Addr   : 0x00080800 - 0x0008FFFF
Reg Formula: 0x00080800 + LoOc48ID*0x2000 + LoOc24Slice*0x1000 + TdmOc24PwID
    Where  :
           + $LoOc48ID(0-3): LoOC48 ID
           + $LoOc24Slice(0-1): Lo-order slice oc24
           + TdmOc24PwID(0-1023): TDM OC24 slice PWID
Reg Desc   :
This register show the PRBS monitor status after PDA

------------------------------------------------------------------------------*/
#define cAf6Reg_ramlotdmprbsmon_Base                                                                0x00080800
#define cAf6Reg_ramlotdmprbsmon(LoOc48ID, LoOc24Slice, TdmOc24PwID)   (0x00080800+(LoOc48ID)*0x2000+(LoOc24Slice)*0x1000+(TdmOc24PwID))
#define cAf6Reg_ramlotdmprbsmon_WidthVal                                                                    32
#define cAf6Reg_ramlotdmprbsmon_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PDAPrbsB3V5Err
BitField Type: RC
BitField Desc: PDA PRBS or B3 or V5 monitor error
BitField Bits: [17]
--------------------------------------*/
#define cAf6_ramlotdmprbsmon_PDAPrbsB3V5Err_Bit_Start                                                       17
#define cAf6_ramlotdmprbsmon_PDAPrbsB3V5Err_Bit_End                                                         17
#define cAf6_ramlotdmprbsmon_PDAPrbsB3V5Err_Mask                                                        cBit17
#define cAf6_ramlotdmprbsmon_PDAPrbsB3V5Err_Shift                                                           17
#define cAf6_ramlotdmprbsmon_PDAPrbsB3V5Err_MaxVal                                                         0x1
#define cAf6_ramlotdmprbsmon_PDAPrbsB3V5Err_MinVal                                                         0x0
#define cAf6_ramlotdmprbsmon_PDAPrbsB3V5Err_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PDAPrbsSyncSta
BitField Type: RW
BitField Desc: PDA PRBS sync status
BitField Bits: [16]
--------------------------------------*/
#define cAf6_ramlotdmprbsmon_PDAPrbsSyncSta_Bit_Start                                                       16
#define cAf6_ramlotdmprbsmon_PDAPrbsSyncSta_Bit_End                                                         16
#define cAf6_ramlotdmprbsmon_PDAPrbsSyncSta_Mask                                                        cBit16
#define cAf6_ramlotdmprbsmon_PDAPrbsSyncSta_Shift                                                           16
#define cAf6_ramlotdmprbsmon_PDAPrbsSyncSta_MaxVal                                                         0x1
#define cAf6_ramlotdmprbsmon_PDAPrbsSyncSta_MinVal                                                         0x0
#define cAf6_ramlotdmprbsmon_PDAPrbsSyncSta_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PDAPrbsValue
BitField Type: RW
BitField Desc: PDA PRBS monitor value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ramlotdmprbsmon_PDAPrbsValue_Bit_Start                                                          0
#define cAf6_ramlotdmprbsmon_PDAPrbsValue_Bit_End                                                           15
#define cAf6_ramlotdmprbsmon_PDAPrbsValue_Mask                                                        cBit15_0
#define cAf6_ramlotdmprbsmon_PDAPrbsValue_Shift                                                              0
#define cAf6_ramlotdmprbsmon_PDAPrbsValue_MaxVal                                                        0xffff
#define cAf6_ramlotdmprbsmon_PDAPrbsValue_MinVal                                                           0x0
#define cAf6_ramlotdmprbsmon_PDAPrbsValue_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA per OC48 Low Order TDM PRBS Generator Control
Reg Addr   : 0x00080400 - 0x0008FFFF
Reg Formula: 0x00080400 + LoOc48ID*0x2000 + LoOc24Slice*0x1000 + TdmOc24PwID
    Where  :
           + $LoOc48ID(0-3): LoOC48 ID
           + $LoOc24Slice(0-1): Lo-order slice oc24
           + TdmOc24PwID(0-1023): TDM OC24 slice PWID
Reg Desc   :
This register show the PRBS monitor status after PDA

------------------------------------------------------------------------------*/
#define cAf6Reg_ramlotdmprbsgen_Base                                                                0x00080400
#define cAf6Reg_ramlotdmprbsgen(LoOc48ID, LoOc24Slice, TdmOc24PwID)   (0x00080400+(LoOc48ID)*0x2000+(LoOc24Slice)*0x1000+(TdmOc24PwID))
#define cAf6Reg_ramlotdmprbsgen_WidthVal                                                                    32
#define cAf6Reg_ramlotdmprbsgen_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PDASeqmode
BitField Type: RW
BitField Desc: PDA Sequential data mode
BitField Bits: [1]
--------------------------------------*/
#define cAf6_ramlotdmprbsgen_PDASeqmode_Bit_Start                                                            1
#define cAf6_ramlotdmprbsgen_PDASeqmode_Bit_End                                                              1
#define cAf6_ramlotdmprbsgen_PDASeqmode_Mask                                                             cBit1
#define cAf6_ramlotdmprbsgen_PDASeqmode_Shift                                                                1
#define cAf6_ramlotdmprbsgen_PDASeqmode_MaxVal                                                             0x1
#define cAf6_ramlotdmprbsgen_PDASeqmode_MinVal                                                             0x0
#define cAf6_ramlotdmprbsgen_PDASeqmode_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PDAPrbsGenEn
BitField Type: RW
BitField Desc: PDA PRBS Generation enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ramlotdmprbsgen_PDAPrbsGenEn_Bit_Start                                                          0
#define cAf6_ramlotdmprbsgen_PDAPrbsGenEn_Bit_End                                                            0
#define cAf6_ramlotdmprbsgen_PDAPrbsGenEn_Mask                                                           cBit0
#define cAf6_ramlotdmprbsgen_PDAPrbsGenEn_Shift                                                              0
#define cAf6_ramlotdmprbsgen_PDAPrbsGenEn_MaxVal                                                           0x1
#define cAf6_ramlotdmprbsgen_PDAPrbsGenEn_MinVal                                                           0x0
#define cAf6_ramlotdmprbsgen_PDAPrbsGenEn_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA ECC CRC Parity Control
Reg Addr   : 0x00008
Reg Formula:
    Where  :
Reg Desc   :
This register configures PDA ECC CRC and Parity.

------------------------------------------------------------------------------*/
#define cAf6Reg_pda_config_ecc_crc_parity_control_Base                                                 0x00008
#define cAf6Reg_pda_config_ecc_crc_parity_control                                                      0x00008
#define cAf6Reg_pda_config_ecc_crc_parity_control_WidthVal                                                  32
#define cAf6Reg_pda_config_ecc_crc_parity_control_WriteMask                                                0x0

/*--------------------------------------
BitField Name: PDAForceEccCor
BitField Type: RW
BitField Desc: PDA force link list Ecc error correctable   1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccCor_Bit_Start                                       6
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccCor_Bit_End                                        6
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccCor_Mask                                       cBit6
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccCor_Shift                                          6
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccCor_MaxVal                                       0x1
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccCor_MinVal                                       0x0
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccCor_RstVal                                       0x0

/*--------------------------------------
BitField Name: PDAForceEccErr
BitField Type: RW
BitField Desc: PDA force link list Ecc error noncorrectable   1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccErr_Bit_Start                                       5
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccErr_Bit_End                                        5
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccErr_Mask                                       cBit5
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccErr_Shift                                          5
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccErr_MaxVal                                       0x1
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccErr_MinVal                                       0x0
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceEccErr_RstVal                                       0x0

/*--------------------------------------
BitField Name: PDAForceCrcErr
BitField Type: RW
BitField Desc: PDA force data CRC error  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceCrcErr_Bit_Start                                       4
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceCrcErr_Bit_End                                        4
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceCrcErr_Mask                                       cBit4
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceCrcErr_Shift                                          4
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceCrcErr_MaxVal                                       0x1
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceCrcErr_MinVal                                       0x0
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceCrcErr_RstVal                                       0x0

/*--------------------------------------
BitField Name: PDAForceLoTdmParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Low Order TDM mode Control force parity error  1:
Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceLoTdmParErr_Bit_Start                                       3
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceLoTdmParErr_Bit_End                                       3
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceLoTdmParErr_Mask                                   cBit3
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceLoTdmParErr_Shift                                       3
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceLoTdmParErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceLoTdmParErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceLoTdmParErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDAForceTdmLkParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Lo and Ho TDM Look Up Control force parity error
1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceTdmLkParErr_Bit_Start                                       2
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceTdmLkParErr_Bit_End                                       2
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceTdmLkParErr_Mask                                   cBit2
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceTdmLkParErr_Shift                                       2
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceTdmLkParErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceTdmLkParErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceTdmLkParErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDAForceJitBufParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Jitter Buffer Control force parity error  1: Set
0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceJitBufParErr_Bit_Start                                       1
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceJitBufParErr_Bit_End                                       1
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceJitBufParErr_Mask                                   cBit1
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceJitBufParErr_Shift                                       1
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceJitBufParErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceJitBufParErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceJitBufParErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDAForceReorderParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Reorder Control force parity error  1: Set  0:
Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceReorderParErr_Bit_Start                                       0
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceReorderParErr_Bit_End                                       0
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceReorderParErr_Mask                                   cBit0
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceReorderParErr_Shift                                       0
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceReorderParErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceReorderParErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_control_PDAForceReorderParErr_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA ECC CRC Parity Disable Control
Reg Addr   : 0x00009
Reg Formula:
    Where  :
Reg Desc   :
This register configures PDA ECC CRC and Parity Disable.

------------------------------------------------------------------------------*/
#define cAf6Reg_pda_config_ecc_crc_parity_disable_control_Base                                         0x00009
#define cAf6Reg_pda_config_ecc_crc_parity_disable_control                                              0x00009
#define cAf6Reg_pda_config_ecc_crc_parity_disable_control_WidthVal                                          32
#define cAf6Reg_pda_config_ecc_crc_parity_disable_control_WriteMask                                        0x0

/*--------------------------------------
BitField Name: PDADisableEccCor
BitField Type: RW
BitField Desc: PDA disable link list Ecc error correctable   1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccCor_Bit_Start                                       6
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccCor_Bit_End                                       6
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccCor_Mask                                   cBit6
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccCor_Shift                                       6
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccCor_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccCor_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccCor_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDADisableEccErr
BitField Type: RW
BitField Desc: PDA disable link list Ecc error noncorrectable   1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccErr_Bit_Start                                       5
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccErr_Bit_End                                       5
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccErr_Mask                                   cBit5
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccErr_Shift                                       5
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableEccErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDADisableCrcErr
BitField Type: RW
BitField Desc: PDA disable data CRC error  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableCrcErr_Bit_Start                                       4
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableCrcErr_Bit_End                                       4
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableCrcErr_Mask                                   cBit4
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableCrcErr_Shift                                       4
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableCrcErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableCrcErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableCrcErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDADisableLoTdmParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Low Order TDM mode Control disable parity error
1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableLoTdmParErr_Bit_Start                                       3
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableLoTdmParErr_Bit_End                                       3
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableLoTdmParErr_Mask                                   cBit3
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableLoTdmParErr_Shift                                       3
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableLoTdmParErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableLoTdmParErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableLoTdmParErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDADisableTdmLkParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Lo and Ho TDM Look Up Control disable parity error
1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableTdmLkParErr_Bit_Start                                       2
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableTdmLkParErr_Bit_End                                       2
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableTdmLkParErr_Mask                                   cBit2
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableTdmLkParErr_Shift                                       2
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableTdmLkParErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableTdmLkParErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableTdmLkParErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDADisableJitBufParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Jitter Buffer Control disable parity error  1: Set
0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableJitBufParErr_Bit_Start                                       1
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableJitBufParErr_Bit_End                                       1
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableJitBufParErr_Mask                                   cBit1
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableJitBufParErr_Shift                                       1
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableJitBufParErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableJitBufParErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableJitBufParErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDADisableReorderParErr
BitField Type: RW
BitField Desc: Pseudowire PDA Reorder Control disable parity error  1: Set  0:
Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableReorderParErr_Bit_Start                                       0
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableReorderParErr_Bit_End                                       0
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableReorderParErr_Mask                                   cBit0
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableReorderParErr_Shift                                       0
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableReorderParErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableReorderParErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_disable_control_PDADisableReorderParErr_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA ECC CRC Parity Sticky
Reg Addr   : 0x0000A
Reg Formula:
    Where  :
Reg Desc   :
This register configures PDA ECC CRC and Parity.

------------------------------------------------------------------------------*/
#define cAf6Reg_pda_config_ecc_crc_parity_sticky_Base                                                  0x0000A
#define cAf6Reg_pda_config_ecc_crc_parity_sticky                                                       0x0000A
#define cAf6Reg_pda_config_ecc_crc_parity_sticky_WidthVal                                                   32
#define cAf6Reg_pda_config_ecc_crc_parity_sticky_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: PDAStickyEccCor
BitField Type: W1C
BitField Desc: PDA sticky link list Ecc error correctable   1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccCor_Bit_Start                                       6
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccCor_Bit_End                                        6
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccCor_Mask                                       cBit6
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccCor_Shift                                          6
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccCor_MaxVal                                       0x1
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccCor_MinVal                                       0x0
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccCor_RstVal                                       0x0

/*--------------------------------------
BitField Name: PDAStickyEccErr
BitField Type: W1C
BitField Desc: PDA sticky link list Ecc error noncorrectable   1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccErr_Bit_Start                                       5
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccErr_Bit_End                                        5
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccErr_Mask                                       cBit5
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccErr_Shift                                          5
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccErr_MaxVal                                       0x1
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccErr_MinVal                                       0x0
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyEccErr_RstVal                                       0x0

/*--------------------------------------
BitField Name: PDAStickyCrcErr
BitField Type: W1C
BitField Desc: PDA sticky data CRC error  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyCrcErr_Bit_Start                                       4
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyCrcErr_Bit_End                                        4
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyCrcErr_Mask                                       cBit4
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyCrcErr_Shift                                          4
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyCrcErr_MaxVal                                       0x1
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyCrcErr_MinVal                                       0x0
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyCrcErr_RstVal                                       0x0

/*--------------------------------------
BitField Name: PDAStickyLoTdmParErr
BitField Type: W1C
BitField Desc: Pseudowire PDA Low Order TDM mode Control sticky parity error  1:
Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyLoTdmParErr_Bit_Start                                       3
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyLoTdmParErr_Bit_End                                       3
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyLoTdmParErr_Mask                                   cBit3
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyLoTdmParErr_Shift                                       3
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyLoTdmParErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyLoTdmParErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyLoTdmParErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDAStickyTdmLkParErr
BitField Type: W1C
BitField Desc: Pseudowire PDA Lo and Ho TDM Look Up Control sticky parity error
1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyTdmLkParErr_Bit_Start                                       2
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyTdmLkParErr_Bit_End                                       2
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyTdmLkParErr_Mask                                   cBit2
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyTdmLkParErr_Shift                                       2
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyTdmLkParErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyTdmLkParErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyTdmLkParErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDAStickyJitBufParErr
BitField Type: W1C
BitField Desc: Pseudowire PDA Jitter Buffer Control sticky parity error  1: Set
0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyJitBufParErr_Bit_Start                                       1
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyJitBufParErr_Bit_End                                       1
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyJitBufParErr_Mask                                   cBit1
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyJitBufParErr_Shift                                       1
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyJitBufParErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyJitBufParErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyJitBufParErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDAStickyReorderParErr
BitField Type: W1C
BitField Desc: Pseudowire PDA Reorder Control sticky parity error  1: Set  0:
Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyReorderParErr_Bit_Start                                       0
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyReorderParErr_Bit_End                                       0
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyReorderParErr_Mask                                   cBit0
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyReorderParErr_Shift                                       0
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyReorderParErr_MaxVal                                     0x1
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyReorderParErr_MinVal                                     0x0
#define cAf6_pda_config_ecc_crc_parity_sticky_PDAStickyReorderParErr_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit3_0 Control
Reg Addr   : 0x01000
Reg Formula: 0x01000 + HaAddr3_0
    Where  :
           + $HaAddr3_0(0-15): HA Address Bit3_0
Reg Desc   :
This register is used to send HA read address bit3_0 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha3_0_control_Base                                                                   0x01000
#define cAf6Reg_rdha3_0_control(HaAddr30)                                                 (0x01000+(HaAddr30))
#define cAf6Reg_rdha3_0_control_WidthVal                                                                    32
#define cAf6Reg_rdha3_0_control_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: ReadAddr3_0
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr3_0
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha3_0_control_ReadAddr3_0_Bit_Start                                                           0
#define cAf6_rdha3_0_control_ReadAddr3_0_Bit_End                                                            19
#define cAf6_rdha3_0_control_ReadAddr3_0_Mask                                                         cBit19_0
#define cAf6_rdha3_0_control_ReadAddr3_0_Shift                                                               0
#define cAf6_rdha3_0_control_ReadAddr3_0_MaxVal                                                        0xfffff
#define cAf6_rdha3_0_control_ReadAddr3_0_MinVal                                                            0x0
#define cAf6_rdha3_0_control_ReadAddr3_0_RstVal                                                            0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit7_4 Control
Reg Addr   : 0x01010
Reg Formula: 0x01010 + HaAddr7_4
    Where  :
           + $HaAddr7_4(0-15): HA Address Bit7_4
Reg Desc   :
This register is used to send HA read address bit7_4 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha7_4_control_Base                                                                   0x01010
#define cAf6Reg_rdha7_4_control(HaAddr74)                                                 (0x01010+(HaAddr74))
#define cAf6Reg_rdha7_4_control_WidthVal                                                                    32
#define cAf6Reg_rdha7_4_control_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: ReadAddr7_4
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr7_4
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha7_4_control_ReadAddr7_4_Bit_Start                                                           0
#define cAf6_rdha7_4_control_ReadAddr7_4_Bit_End                                                            19
#define cAf6_rdha7_4_control_ReadAddr7_4_Mask                                                         cBit19_0
#define cAf6_rdha7_4_control_ReadAddr7_4_Shift                                                               0
#define cAf6_rdha7_4_control_ReadAddr7_4_MaxVal                                                        0xfffff
#define cAf6_rdha7_4_control_ReadAddr7_4_MinVal                                                            0x0
#define cAf6_rdha7_4_control_ReadAddr7_4_RstVal                                                            0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit11_8 Control
Reg Addr   : 0x01020
Reg Formula: 0x01020 + HaAddr11_8
    Where  :
           + $HaAddr11_8(0-15): HA Address Bit11_8
Reg Desc   :
This register is used to send HA read address bit11_8 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha11_8_control_Base                                                                  0x01020
#define cAf6Reg_rdha11_8_control(HaAddr118)                                              (0x01020+(HaAddr118))
#define cAf6Reg_rdha11_8_control_WidthVal                                                                   32
#define cAf6Reg_rdha11_8_control_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: ReadAddr11_8
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr11_8
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha11_8_control_ReadAddr11_8_Bit_Start                                                         0
#define cAf6_rdha11_8_control_ReadAddr11_8_Bit_End                                                          19
#define cAf6_rdha11_8_control_ReadAddr11_8_Mask                                                       cBit19_0
#define cAf6_rdha11_8_control_ReadAddr11_8_Shift                                                             0
#define cAf6_rdha11_8_control_ReadAddr11_8_MaxVal                                                      0xfffff
#define cAf6_rdha11_8_control_ReadAddr11_8_MinVal                                                          0x0
#define cAf6_rdha11_8_control_ReadAddr11_8_RstVal                                                          0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit15_12 Control
Reg Addr   : 0x01030
Reg Formula: 0x01030 + HaAddr15_12
    Where  :
           + $HaAddr15_12(0-15): HA Address Bit15_12
Reg Desc   :
This register is used to send HA read address bit15_12 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha15_12_control_Base                                                                 0x01030
#define cAf6Reg_rdha15_12_control(HaAddr1512)                                           (0x01030+(HaAddr1512))
#define cAf6Reg_rdha15_12_control_WidthVal                                                                  32
#define cAf6Reg_rdha15_12_control_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ReadAddr15_12
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr15_12
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha15_12_control_ReadAddr15_12_Bit_Start                                                       0
#define cAf6_rdha15_12_control_ReadAddr15_12_Bit_End                                                        19
#define cAf6_rdha15_12_control_ReadAddr15_12_Mask                                                     cBit19_0
#define cAf6_rdha15_12_control_ReadAddr15_12_Shift                                                           0
#define cAf6_rdha15_12_control_ReadAddr15_12_MaxVal                                                    0xfffff
#define cAf6_rdha15_12_control_ReadAddr15_12_MinVal                                                        0x0
#define cAf6_rdha15_12_control_ReadAddr15_12_RstVal                                                        0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit19_16 Control
Reg Addr   : 0x01040
Reg Formula: 0x01040 + HaAddr19_16
    Where  :
           + $HaAddr19_16(0-15): HA Address Bit19_16
Reg Desc   :
This register is used to send HA read address bit19_16 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha19_16_control_Base                                                                 0x01040
#define cAf6Reg_rdha19_16_control(HaAddr1916)                                           (0x01040+(HaAddr1916))
#define cAf6Reg_rdha19_16_control_WidthVal                                                                  32
#define cAf6Reg_rdha19_16_control_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ReadAddr19_16
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr19_16
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha19_16_control_ReadAddr19_16_Bit_Start                                                       0
#define cAf6_rdha19_16_control_ReadAddr19_16_Bit_End                                                        19
#define cAf6_rdha19_16_control_ReadAddr19_16_Mask                                                     cBit19_0
#define cAf6_rdha19_16_control_ReadAddr19_16_Shift                                                           0
#define cAf6_rdha19_16_control_ReadAddr19_16_MaxVal                                                    0xfffff
#define cAf6_rdha19_16_control_ReadAddr19_16_MinVal                                                        0x0
#define cAf6_rdha19_16_control_ReadAddr19_16_RstVal                                                        0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit23_20 Control
Reg Addr   : 0x01050
Reg Formula: 0x01050 + HaAddr23_20
    Where  :
           + $HaAddr23_20(0-15): HA Address Bit23_20
Reg Desc   :
This register is used to send HA read address bit23_20 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha23_20_control_Base                                                                 0x01050
#define cAf6Reg_rdha23_20_control(HaAddr2320)                                           (0x01050+(HaAddr2320))
#define cAf6Reg_rdha23_20_control_WidthVal                                                                  32
#define cAf6Reg_rdha23_20_control_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ReadAddr23_20
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr23_20
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha23_20_control_ReadAddr23_20_Bit_Start                                                       0
#define cAf6_rdha23_20_control_ReadAddr23_20_Bit_End                                                        19
#define cAf6_rdha23_20_control_ReadAddr23_20_Mask                                                     cBit19_0
#define cAf6_rdha23_20_control_ReadAddr23_20_Shift                                                           0
#define cAf6_rdha23_20_control_ReadAddr23_20_MaxVal                                                    0xfffff
#define cAf6_rdha23_20_control_ReadAddr23_20_MinVal                                                        0x0
#define cAf6_rdha23_20_control_ReadAddr23_20_RstVal                                                        0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit24 and Data Control
Reg Addr   : 0x01060
Reg Formula: 0x01060 + HaAddr24
    Where  :
           + $HaAddr24(0-1): HA Address Bit24
Reg Desc   :
This register is used to send HA read address bit24 to HA engine to read data

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha24data_control_Base                                                                0x01060
#define cAf6Reg_rdha24data_control(HaAddr24)                                              (0x01060+(HaAddr24))
#define cAf6Reg_rdha24data_control_WidthVal                                                                 32
#define cAf6Reg_rdha24data_control_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: ReadHaData31_0
BitField Type: RW
BitField Desc: HA read data bit31_0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha24data_control_ReadHaData31_0_Bit_Start                                                     0
#define cAf6_rdha24data_control_ReadHaData31_0_Bit_End                                                      31
#define cAf6_rdha24data_control_ReadHaData31_0_Mask                                                   cBit31_0
#define cAf6_rdha24data_control_ReadHaData31_0_Shift                                                         0
#define cAf6_rdha24data_control_ReadHaData31_0_MaxVal                                               0xffffffff
#define cAf6_rdha24data_control_ReadHaData31_0_MinVal                                                      0x0
#define cAf6_rdha24data_control_ReadHaData31_0_RstVal                                                      0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data63_32
Reg Addr   : 0x01070
Reg Formula:
    Where  :
Reg Desc   :
This register is used to read HA dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha_hold63_32_Base                                                                    0x01070
#define cAf6Reg_rdha_hold63_32                                                                         0x01070
#define cAf6Reg_rdha_hold63_32_WidthVal                                                                     32
#define cAf6Reg_rdha_hold63_32_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: ReadHaData63_32
BitField Type: RW
BitField Desc: HA read data bit63_32
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha_hold63_32_ReadHaData63_32_Bit_Start                                                        0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Bit_End                                                         31
#define cAf6_rdha_hold63_32_ReadHaData63_32_Mask                                                      cBit31_0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Shift                                                            0
#define cAf6_rdha_hold63_32_ReadHaData63_32_MaxVal                                                  0xffffffff
#define cAf6_rdha_hold63_32_ReadHaData63_32_MinVal                                                         0x0
#define cAf6_rdha_hold63_32_ReadHaData63_32_RstVal                                                         0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data95_64
Reg Addr   : 0x01071
Reg Formula:
    Where  :
Reg Desc   :
This register is used to read HA dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold95_64_Base                                                                  0x01071
#define cAf6Reg_rdindr_hold95_64                                                                       0x01071
#define cAf6Reg_rdindr_hold95_64_WidthVal                                                                   32
#define cAf6Reg_rdindr_hold95_64_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: ReadHaData95_64
BitField Type: RW
BitField Desc: HA read data bit95_64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Bit_Start                                                      0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Bit_End                                                       31
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Mask                                                    cBit31_0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Shift                                                          0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_MaxVal                                                0xffffffff
#define cAf6_rdindr_hold95_64_ReadHaData95_64_MinVal                                                       0x0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_RstVal                                                       0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data127_96
Reg Addr   : 0x01072
Reg Formula:
    Where  :
Reg Desc   :
This register is used to read HA dword4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold127_96_Base                                                                 0x01072
#define cAf6Reg_rdindr_hold127_96                                                                      0x01072
#define cAf6Reg_rdindr_hold127_96_WidthVal                                                                  32
#define cAf6Reg_rdindr_hold127_96_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ReadHaData127_96
BitField Type: RW
BitField Desc: HA read data bit127_96
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Bit_Start                                                    0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Bit_End                                                     31
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Mask                                                  cBit31_0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Shift                                                        0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_MaxVal                                              0xffffffff
#define cAf6_rdindr_hold127_96_ReadHaData127_96_MinVal                                                     0x0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_RstVal                                                     0xx

/*------------------------------------------------------------------------------
Reg Name   : PDA Dump Input Service ID control
------------------------------------------------------------------------------*/
#define cAf6Reg_PdaDumpInputSevIdCtrl   0x000C00

#define cAf6Reg_PdaDumpSevIdMask   cBit11_0
#define cAf6Reg_PdaDumpSevIdShift  0

/*------------------------------------------------------------------------------
Reg Name   : PDA Dump Input Service Data control
------------------------------------------------------------------------------*/
#define cAf6Reg_PdaDumpInDatStartReg    0x000E00
#define cAf6Reg_PdaDumpInDatStopReg    0x000EFF

#define cAf6Reg_PdaDumpInSopMask     cBit3
#define cAf6Reg_PdaDumpInSopShift    3

#define cAf6Reg_PdaDumpInEopMask     cBit2
#define cAf6Reg_PdaDumpInEopShift    2

#define cAf6Reg_PdaDumpInNobMask     cBit1_0
#define cAf6Reg_PdaDumpInNobShift    0

#define cAf6Reg_PdaDumpInDataMask    cBit31_0
#define cAf6Reg_PdaDumpInDataShift   0

/*------------------------------------------------------------------------------
Reg Name   : PDA Dump Output Service ID control
------------------------------------------------------------------------------*/
#define cAf6Reg_PdaDumpOutputSevIdCtrl  0x00064000

/*------------------------------------------------------------------------------
Reg Name   : PDA Dump Output Service Data control
------------------------------------------------------------------------------*/
#define cAf6Reg_PdaDumpOutDatStartReg    0x66000
#define cAf6Reg_PdaDumpOutDatStopReg     0x667FF

#define cAf6Reg_PdaDumpOutSopMask     cBit18
#define cAf6Reg_PdaDumpOutSopShift    18

#define cAf6Reg_PdaDumpOutEopMask     cBit17
#define cAf6Reg_PdaDumpOutEopShift    17

#define cAf6Reg_PdaDumpOutNobMask     cBit16
#define cAf6Reg_PdaDumpOutNobShift    16

#define cAf6Reg_PdaDumpOutDataMask    cBit15_0
#define cAf6Reg_PdaDumpOutDataShift   0

#define cAf6Reg_PdaDumpDataMask(byte_i)    (cBit31_24 >> ((byte_i) * 8))
#define cAf6Reg_PdaDumpDataShift(byte_i)   (24 - ((byte_i) * 8))

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEPDAREG_H_ */


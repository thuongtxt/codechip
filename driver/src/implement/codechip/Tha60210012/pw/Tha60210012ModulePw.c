/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : Tha60210012ModulePw.c
 *
 * Created Date: Feb 29, 2016
 *
 * Description : Module PW of product 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/encap/ThaHdlcChannel.h"
#include "../encap/Tha60210012ModuleDecapParserReg.h"
#include "../encap/Tha60210012ModuleEncap.h"
#include "../encap/Tha60210012PmcInternalReg.h"
#include "activator/Tha60210012PwActivator.h"
#include "Tha60210012ModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210012ModulePw)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012ModulePwMethods m_methods;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tAtModulePwMethods           m_AtModulePwOverride;
static tThaModulePwMethods          m_ThaModulePwOverride;
static tTha60210011ModulePwMethods  m_Tha60210011ModulePwOverride;

/* Save super implementation */
static const tAtObjectMethods            *m_AtObjectMethods            = NULL;
static const tAtModuleMethods            *m_AtModuleMethods            = NULL;
static const tAtModulePwMethods          *m_AtModulePwMethods          = NULL;
static const tTha60210011ModulePwMethods *m_Tha60210011ModulePwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 3072; /* 3K TDM CES/CEP */
    }

static uint32 MaxApsGroupsGet(AtModulePw self)
    {
    AtUnused(self);
    return 4096;
    }

static uint32 MaxHsGroupsGet(AtModulePw self)
    {
    AtUnused(self);
    return 2048;
    }

static ThaPwActivator PwActivatorCreate(ThaModulePw self)
    {
    return Tha60210012PwDynamicActivatorNew((AtModulePw)self);
    }

static uint32 DefaultDefectModule(ThaModulePw self)
    {
    return AtModuleTypeGet((AtModule)self);
    }

static eBool PwCircuitBelongsToLoLine(Tha60210011ModulePw self, AtPw pw)
    {
    eAtPwType pwType;
    AtHdlcChannel hdlcChannel = NULL;
    eBool isLoLine;

    if (ThaPwTypeIsCesCep(pw))
        return m_Tha60210011ModulePwMethods->PwCircuitBelongsToLoLine(self, pw);

    pwType = AtPwTypeGet(pw);
    if (pwType == cAtPwTypeHdlc)
        hdlcChannel = (AtHdlcChannel)AtPwBoundCircuitGet(pw);

    if ((pwType == cAtPwTypePpp) || (pwType == cAtPwTypeFr))
        hdlcChannel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)AtPwBoundCircuitGet(pw));

    if (hdlcChannel == NULL)
        {
        mChannelLog(pw, cAtLogLevelWarning, "Bound circuit is NULL");
        return cAtFalse;
        }

    isLoLine = ThaHdlcChannelCircuitIsHo((ThaHdlcChannel)hdlcChannel) ? cAtFalse : cAtTrue;
    return isLoLine;
    }

static eAtRet PwCircuitSliceAndHwIdInSliceGet(Tha60210011ModulePw self, AtPw pw, uint8* slice, uint32 *hwIdInSlice)
    {
    eAtPwType pwType;
    AtHdlcChannel hdlcChannel = NULL;

    if (ThaPwTypeIsCesCep(pw))
        return m_Tha60210011ModulePwMethods->PwCircuitSliceAndHwIdInSliceGet(self, pw, slice, hwIdInSlice);

    pwType = AtPwTypeGet(pw);
    if (pwType == cAtPwTypeHdlc)
        hdlcChannel = (AtHdlcChannel)AtPwBoundCircuitGet(pw);

    if ((pwType == cAtPwTypePpp) || (pwType == cAtPwTypeFr))
        hdlcChannel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)AtPwBoundCircuitGet(pw));

    if (hdlcChannel == NULL)
        {
        mChannelLog(pw, cAtLogLevelWarning, "Bound circuit is NULL");
        return cAtError;
        }

    if (slice) *slice = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)hdlcChannel);
    if (hwIdInSlice) *hwIdInSlice = AtChannelHwIdGet((AtChannel)hdlcChannel);

    return cAtOk;
    }

static uint32 EncapBaseAddressFromPwAdapter(AtPw adapter)
    {
    uint8 slice;
    uint32 hwSts;

    Tha60210011PwCircuitSliceAndHwIdInSliceGet(adapter, &slice, &hwSts);

    if (Tha60210011PwCircuitBelongsToLoLine(adapter))
        return ((uint32)(0x4000 * slice) + Tha60210012ModuleEncapLoBaseAddress());

    return (uint32)(0x1000 * slice) + Tha60210012ModuleEncapHoBaseAddress();
    }

static eAtRet DeleteHdlcPwObject(AtModulePw self, AtPw pw)
    {
    AtPw adapter = (AtPw)ThaPwAdapterGet(pw);
    AtEthPort boundEthPort;

    if (pw == NULL)
        return cAtOk;

    boundEthPort = AtPwEthPortGet(adapter);
    if (boundEthPort)
        Tha60210051PwEthPortBinderDeregisterListener(adapter, boundEthPort);

    return m_AtModulePwMethods->DeleteHdlcPwObject(self, pw);
    }

static uint32 PmcMuxBaseAddress(void)
    {
    return 0x1C00000;
    }

static uint32 HwCounterPoolIdGet(AtPw self)
    {
    ThaModulePw modulePw;
    uint32 regVal, address;

    if (self == NULL)
        return cThaInternalCounterPoolInvalid;

    modulePw = (ThaModulePw)AtChannelModuleGet((AtChannel)self);
    if ((modulePw == NULL) || (!mMethodsGet(mThis(modulePw))->InternalCounterIsSupported(mThis(modulePw))))
        return cThaInternalCounterPoolInvalid;

    address = cAf6Reg_ethcntidlk_pen_Base + PmcMuxBaseAddress() + AtChannelIdGet((AtChannel)self);
    regVal = mChannelHwRead(self, address, cAtModulePw);
    if (mRegField(regVal, cAf6_ethcntidlk_pen_ETH_PCNT_EN_))
        return mRegField(regVal, cAf6_ethcntidlk_pen_ETH_PCNT_ID_);

    return cThaInternalCounterPoolInvalid;
    }

static void HwCounterPoolIdSet(AtPw self, uint32 poolId, eBool enable)
    {
    uint32 regVal, address;

    /* TX */
    address = cAf6Reg_ethcntidlk_pen_Base + PmcMuxBaseAddress() + AtChannelIdGet((AtChannel)self);
    regVal = mChannelHwRead(self, address, cAtModulePw);
    mRegFieldSet(regVal, cAf6_ethcntidlk_pen_ETH_PCNT_EN_, mBoolToBin(enable));
    mRegFieldSet(regVal, cAf6_ethcntidlk_pen_ETH_PCNT_ID_, poolId);
    mChannelHwWrite(self, address, regVal, cAtModulePw);

    /* RX */
    address += 0x1000;
    regVal = mChannelHwRead(self, address, cAtModulePw);
    mRegFieldSet(regVal, cAf6_ethcntidlk_pen_ETH_PCNT_EN_, mBoolToBin(enable));
    mRegFieldSet(regVal, cAf6_ethcntidlk_pen_ETH_PCNT_ID_, poolId);
    mChannelHwWrite(self, address, regVal, cAtModulePw);
    }

static uint32 PoolGet(ThaModulePw self)
    {
    return self ? mThis(self)->poolList : 0;
    }

static void PoolSet(ThaModulePw self, uint32 value)
    {
    if (self)
        mThis(self)->poolList = value;
    }

static uint32 PwInternalTxCounterGet(AtPw self, uint32 counterType, uint32 poolId, eBool r2c)
    {
    uint32 regVal, address;

    address = cAf6Reg_igethpkttypcnt_pen_Base + PmcMuxBaseAddress() + (mBoolToBin(r2c) << 9) + (counterType << 5) + poolId;
    regVal = mChannelHwRead(self, address, cAtModulePw);

    return mRegField(regVal, cAf6_igethpkttypcnt_pen_IGETH_PktTyp_CNT_);
    }

static uint32 PwInternalRxCounterGet(AtPw self, uint32 counterType, uint32 poolId, eBool r2c)
    {
    uint32 regVal, address;

    address = cAf6Reg_egethpkttypcnt_pen_Base + PmcMuxBaseAddress() + (mBoolToBin(r2c) << 5) + (counterType << 6) + poolId;
    regVal = mChannelHwRead(self, address, cAtModulePw);

    return mRegField(regVal, cAf6_egethpkttypcnt_pen_EGETH_CNT_);
    }

static void PoolIdInusedSet(ThaModulePw self, uint32 index, eBool enable)
    {
    uint32 poolList = PoolGet(self);
    mBitIns(&poolList, index, mBoolToBin(enable));
    PoolSet(self, poolList);
    }

static eBool PoolIdIsInused(ThaModulePw self, uint32 index)
    {
    uint32 poolList = PoolGet(self);
    return mBitGet(poolList, index);
    }

static eBool PoolIdIsValid(uint32 poolId)
    {
    return (poolId < cThaInternalCounterMaxPoolNum) ? cAtTrue : cAtFalse;
    }

static eBool InternalCounterIsSupported(Tha60210012ModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210012ModulePw object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(poolList);
    }

static eAtRet Debug(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Debug(self);
    AtPrintc(cSevNormal, "* poolList: 0x%x\r\n", mThis(self)->poolList);
    return ret;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Setup(self);
    mThis(self)->poolList = 0;
    return ret;
    }

static ThaPwHeaderProvider PwHeaderProviderCreate(ThaModulePw self)
    {
    AtUnused(self);
    return Tha60210012PwHeaderProviderNew();
    }

static uint32 StartVersionHas1344PwsPerSts24Slice(Tha60210011ModulePw self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x9, 0x1060);
    }

static void OverrideAtObject(AtModulePw self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModulePw self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, Setup);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, m_AtModulePwMethods, sizeof(tAtModulePwMethods));

        mMethodOverride(m_AtModulePwOverride, MaxApsGroupsGet);
        mMethodOverride(m_AtModulePwOverride, MaxHsGroupsGet);
        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        mMethodOverride(m_AtModulePwOverride, DeleteHdlcPwObject);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void OverrideThaModulePw(ThaModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(self), sizeof(tThaModulePwMethods));

        mMethodOverride(m_ThaModulePwOverride, PwActivatorCreate);
        mMethodOverride(m_ThaModulePwOverride, DefaultDefectModule);
        mMethodOverride(m_ThaModulePwOverride, PwHeaderProviderCreate);
        }

    mMethodsSet(self, &m_ThaModulePwOverride);
    }

static void OverrideTha60210011ModulePw(AtModulePw self)
    {
    Tha60210011ModulePw module = (Tha60210011ModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePwMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePwOverride, mMethodsGet(module), sizeof(m_Tha60210011ModulePwOverride));

        mMethodOverride(m_Tha60210011ModulePwOverride, PwCircuitBelongsToLoLine);
        mMethodOverride(m_Tha60210011ModulePwOverride, PwCircuitSliceAndHwIdInSliceGet);
        mMethodOverride(m_Tha60210011ModulePwOverride, StartVersionHas1344PwsPerSts24Slice);
        }

    mMethodsSet(module, &m_Tha60210011ModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModulePw(self);
    OverrideThaModulePw((ThaModulePw)self);
    OverrideTha60210011ModulePw(self);
    }

static void MethodsInit(Tha60210012ModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, InternalCounterIsSupported);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModulePw);
    }

AtModulePw Tha60210012ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60210012ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModulePwObjectInit(newModule, device);
    }

uint32 Tha60210012PwAdapterHwFlowIdGet(ThaPwAdapter adapter)
    {
    return Tha60210012HwPwHwFlowId(ThaPwAdapterHwPwGet(adapter));
    }

AtEthFlow Tha60210012PwAdapterHwFlowGet(ThaPwAdapter adapter)
    {
    return Tha60210012HwPwHwFlowGet(ThaPwAdapterHwPwGet(adapter));
    }

eAtRet Tha60210012PseudowireStsNcEncapByPass(AtPw adapter, AtSdhChannel auVc)
    {
    uint32 startOffset = EncapBaseAddressFromPwAdapter(adapter);
    return Tha60210012ModuleEncapStsNcBoundHwChannelTypeSet(auVc, startOffset, 0);
    }

static uint32 FirstFreePoolIdGet(ThaModulePw self)
    {
    uint32 pool_i, poolList;

    poolList = PoolGet(self);
    for (pool_i = 0; pool_i < cThaInternalCounterMaxPoolNum; pool_i++)
        {
        if (!mBitGet(poolList, pool_i))
            return pool_i;
        }

    return cThaInternalCounterPoolInvalid;
    }

eAtRet Tha60210012ModulePwDebugCounterPoolIdAllocate(AtPw pw)
    {
    uint32 poolId;
    ThaModulePw pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)pw);
    if ((pwModule == NULL) || (!mMethodsGet(mThis(pwModule))->InternalCounterIsSupported(mThis(pwModule))))
        return cAtOk;

    poolId = FirstFreePoolIdGet(pwModule);
    if (PoolIdIsValid(poolId))
        {
        PoolIdInusedSet(pwModule, poolId, cAtTrue);
        HwCounterPoolIdSet(pw, poolId, cAtTrue);
        return cAtOk;
        }

    return cAtErrorRsrcNoAvail;
    }

eAtRet ThaDebug60210012ModulePwFreePoolResource(AtPw pw)
    {
    uint32 poolId;
    ThaModulePw modulePw = (ThaModulePw)AtChannelModuleGet((AtChannel)pw);

    if ((modulePw == NULL) || (!mMethodsGet(mThis(modulePw))->InternalCounterIsSupported(mThis(modulePw))))
        return cAtOk;

    poolId = HwCounterPoolIdGet(pw);
    if (poolId != cThaInternalCounterPoolInvalid)
       {
       PoolIdInusedSet(modulePw, poolId, cAtFalse);
       HwCounterPoolIdSet(pw, 0, cAtFalse);
       }

    return cAtOk;
    }

eAtRet Tha60210012DebugPwInternalCounterGet(AtPw self, tThaDebugPwInternalCnt *pCounters, eBool r2c)
    {
    uint32 poolId = HwCounterPoolIdGet(self);
    ThaModulePw modulePw = (ThaModulePw)AtChannelModuleGet((AtChannel)self);

    AtOsalMemInit(pCounters, 0 , sizeof(tThaDebugPwInternalCnt));
    if ((modulePw == NULL) || (!PoolIdIsValid(poolId) || !mMethodsGet(mThis(modulePw))->InternalCounterIsSupported(mThis(modulePw))))
        return cAtOk;

    pCounters->txPackets = PwInternalTxCounterGet(self, cAf6RegDebugPwCntTypeTxPackets, poolId, r2c);
    pCounters->txPayloadBytes = PwInternalTxCounterGet(self, cAf6RegDebugPwCntTypeTxPayloadBytes, poolId, r2c);
    pCounters->txPacketFragments = PwInternalTxCounterGet(self, cAf6RegDebugPwCntTypeTxPacketFragments, poolId, r2c);
    pCounters->txLbitPackets = PwInternalTxCounterGet(self, cAf6RegDebugPwCntTypeTxLbitPackets, poolId, r2c);
    pCounters->txRbitPackets = PwInternalTxCounterGet(self, cAf6RegDebugPwCntTypeTxRbitPackets, poolId, r2c);
    pCounters->txMbitPackets = PwInternalTxCounterGet(self, cAf6RegDebugPwCntTypeTxMbitPackets, poolId, r2c);
    pCounters->txPbitPackets = PwInternalTxCounterGet(self, cAf6RegDebugPwCntTypeTxPbitPackets, poolId, r2c);

    pCounters->rxPackets = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxPackets, poolId, r2c);
    pCounters->rxPayloadBytes = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxPayloadBytes, poolId, r2c);
    pCounters->rxLofsPackets = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxLofsPackets, poolId, r2c);
    pCounters->rxLbitPackets = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxLbitPackets, poolId, r2c);
    pCounters->rxNbitPackets = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxNbitPackets, poolId, r2c);
    pCounters->rxPbitPackets = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxPbitPackets, poolId, r2c);
    pCounters->rxRbitPackets = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxRbitPackets, poolId, r2c);
    pCounters->rxLatePackets = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxLatePackets, poolId, r2c);
    pCounters->rxEarlyPackets = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxEarlyPackets, poolId, r2c);
    pCounters->rxLostPackets = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxLostPackets, poolId, r2c);
    pCounters->rxOverrunPackets = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxOverrunPackets, poolId, r2c);
    pCounters->rxUnderrunPackets = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxUnderrunPackets, poolId, r2c);
    pCounters->rxMalformPackets = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxMalformPackets, poolId, r2c);
    pCounters->rxStrayPackets = PwInternalRxCounterGet(self, cAf6RegDebugPwCntTypeRxStrayPackets, poolId, r2c);

    return cAtOk;
    }

uint32 Tha60210012DebugPwInternalCounterPoolIdGet(AtPw self)
    {
    return HwCounterPoolIdGet(self);
    }

eAtRet Tha60210012DebugPwInternalCounterPoolIdSet(AtPw self, uint32 poolId, eBool enable)
    {
    uint32 hwPoolId = HwCounterPoolIdGet(self);
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet((AtDevice)AtChannelDeviceGet((AtChannel)self), cAtModulePw);

    if ((pwModule == NULL) || (!mMethodsGet(mThis(pwModule))->InternalCounterIsSupported(mThis(pwModule))))
        return cAtOk;

    if (enable)
        {
        if (PoolIdIsValid(poolId))
            {
            /* Allocated already */
            if (poolId == hwPoolId)
                return cAtOk;

            if (PoolIdIsInused(pwModule, poolId))
                return cAtErrorRsrcNoAvail;
            }
        else
            return Tha60210012ModulePwDebugCounterPoolIdAllocate(self);
        }
    else
        {
        if (!PoolIdIsValid(hwPoolId))
            return cAtOk;

        poolId = hwPoolId;
        }

    PoolIdInusedSet(pwModule, poolId, enable);
    HwCounterPoolIdSet(self, enable ? poolId : cInvalidUint8, enable);

    return cAtOk;
    }

void Tha60210012DebugPwInternalMemoryPoolGet(ThaModulePw self, uint32 *poolList)
    {
    uint32 pw_i, maxPw, poolId, numPool;
    AtPw pw;
    AtModulePw pwModule = (AtModulePw)self;

    if ((pwModule == NULL) || (!mMethodsGet(mThis(pwModule))->InternalCounterIsSupported(mThis(pwModule))))
        return;

    maxPw = AtModulePwMaxPwsGet(pwModule);
    numPool = 0;

    for (pw_i = 0; pw_i < maxPw; pw_i++)
        {
        if (numPool == cThaInternalCounterMaxPoolNum)
            break;

        pw = AtModulePwGetPw(pwModule, pw_i);
        if (pw)
            {
            poolId = HwCounterPoolIdGet(pw);
            if (poolId != cThaInternalCounterPoolInvalid)
                {
                poolList[poolId] = pw_i;
                numPool++;
                }
            }
        }
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210012PwHeaderProvider.c
 *
 * Created Date: May 4, 2017
 *
 * Description : PW header provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pw/headerprovider/ThaPwHeaderProviderInternal.h"
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../../../default/util/ThaUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cVlanSizeInBytes 4
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012PwHeaderProvider
    {
    tThaPwHeaderProvider super;
    }tTha60210012PwHeaderProvider;
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwHeaderProviderMethods m_ThaPwHeaderProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleEth EthModule(ThaPwAdapter pw)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)pw);
    return (ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static tAtEthVlanTag* SVlanGetFromBuffer(ThaPwHeaderProvider self, ThaPwAdapter pw, tAtEthVlanTag *sVlan, uint8* header, uint8 headerLength)
    {
    uint8 *headerBuffer = header;
    uint16 vlanType;
    AtUnused(self);

    /* Plus 4 bytes SVLAN and 2 bytes CVLAN header */
    if (headerLength < (cMacSizeInBytes + 6))
        return NULL;

    headerBuffer = headerBuffer + cMacSizeInBytes;
    vlanType = ThaPktUtilVlanTypeFromBuffer(headerBuffer);

    if (vlanType != ThaModuleEthSVlanTpid(EthModule(pw), ThaPwAdapterEthPortGet(pw)))
        return NULL;

    headerBuffer += cVlanTypeSizeInBytes;
    ThaPktUtilVlanTagFromBuffer(headerBuffer, sVlan);

    headerBuffer += 2;
    vlanType = ThaPktUtilVlanTypeFromBuffer(headerBuffer);
    if (vlanType != ThaModuleEthCVlanTpid(EthModule(pw)))
        return NULL;

    return sVlan;
    }

static tAtEthVlanTag* CVlanGetFromBuffer(ThaPwHeaderProvider self, ThaPwAdapter pw, tAtEthVlanTag *cVlan, uint8* header, uint8 headerLength)
    {
    uint8* headerBuffer = header;
    uint16 vlanType;
    AtUnused(self);

    if (headerLength < (cMacSizeInBytes + cVlanTypeSizeInBytes))
        return NULL;

    headerBuffer = headerBuffer + cMacSizeInBytes;
    vlanType = ThaPktUtilVlanTypeFromBuffer(headerBuffer);

    /* If next two byte is cThaEthTypeSvlan */
    if (vlanType == ThaModuleEthSVlanTpid(EthModule(pw), ThaPwAdapterEthPortGet(pw)))
        {
        headerBuffer += cVlanTypeSizeInBytes;
        ThaPktUtilVlanTagFromBuffer(headerBuffer, cVlan);

        if (headerLength < (cMacSizeInBytes + 6))
            return cVlan;

        headerBuffer += 2;
        vlanType = ThaPktUtilVlanTypeFromBuffer(headerBuffer);
        if (vlanType != ThaModuleEthCVlanTpid(EthModule(pw)))
            return cVlan;
        }

    else if (vlanType != ThaModuleEthCVlanTpid(EthModule(pw)))
        return NULL;

    headerBuffer += cVlanTypeSizeInBytes;
    return ThaPktUtilVlanTagFromBuffer(headerBuffer, cVlan);
    }

static void OverrideThaPwHeaderProvider(ThaPwHeaderProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwHeaderProviderOverride, mMethodsGet(self), sizeof(m_ThaPwHeaderProviderOverride));

        mMethodOverride(m_ThaPwHeaderProviderOverride, SVlanGetFromBuffer);
        mMethodOverride(m_ThaPwHeaderProviderOverride, CVlanGetFromBuffer);
        }

    mMethodsSet(self, &m_ThaPwHeaderProviderOverride);
    }

static void Override(ThaPwHeaderProvider self)
    {
    OverrideThaPwHeaderProvider(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012PwHeaderProvider);
    }

static ThaPwHeaderProvider ObjectInit(ThaPwHeaderProvider self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwHeaderProviderObjectInit(self) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwHeaderProvider Tha60210012PwHeaderProviderNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderProvider newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newProvider);
    }

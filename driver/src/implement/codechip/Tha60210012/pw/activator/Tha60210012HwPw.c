/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210012HwPw.c
 *
 * Created Date: Feb 29, 2016
 *
 * Description : 60210012 HW PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../util/coder/AtCoderUtil.h"
#include "../../../Tha60210011/pw/activator/Tha60210011HwPwInternal.h"
#include "../../eth/Tha60210012EthFlow.h"
#include "../../common/Tha60210012Channel.h"
#include "../Tha60210012ModulePw.h"
#include "Tha60210012PwActivator.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210012HwPw *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012HwPw
    {
    tTha60210011HwPw super;

    /* Private date */
    AtEthFlow hwFlow;
    uint32 hwFlowId;
    }tTha60210012HwPw;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tThaHwPwMethods      m_ThaHwPwOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tThaHwPwMethods  *m_ThaHwPwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210012HwPw *object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(hwFlow);
    mEncodeUInt(hwFlowId);
    }

static AtModuleEth ModuleEth(ThaHwPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self->adapter);
    return (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->hwFlow);
    mThis(self)->hwFlow = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Debug(ThaHwPw self)
    {
    AtChannel channel = AtPwBoundCircuitGet((AtPw)ThaHwPwAdapterGet(self));

    m_ThaHwPwMethods->Debug(self);
    AtPrintc(cSevNormal, "* HW Flow : %d\r\n", mThis(self)->hwFlowId);
    AtPrintc(cSevNormal, "* PLA BlockId: %u\r\n", Tha60210012ChannelPlaBlockIdGet(channel));
    }

static void OverrideAtObject(ThaHwPw self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaHwPw(ThaHwPw self)
    {
    ThaHwPw hwPw = (ThaHwPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHwPwMethods = mMethodsGet(hwPw);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHwPwOverride, m_ThaHwPwMethods, sizeof(m_ThaHwPwOverride));

        mMethodOverride(m_ThaHwPwOverride, Debug);
        }

    mMethodsSet(hwPw, &m_ThaHwPwOverride);
    }

static void Override(ThaHwPw self)
    {
    OverrideAtObject(self);
    OverrideThaHwPw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012HwPw);
    }

static ThaHwPw ObjectInit(ThaHwPw self, ThaPwAdapter adapter, uint8 slice, uint32 tdmPwId, eBool isLo)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011HwPwObjectInit(self, adapter, slice, tdmPwId, isLo) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->hwFlowId = cInvalidUint32;

    /* Correct HW ID for the purpose of mix HDLC/PPP/FR and CES/CEP
     * That is AtChannelIdGet of ADAPTER object returns:
     *  - Value in range [0..3k] flat PW ID in CES/CEP
     *  - Corresponding link ID (also encap channel software ID) in HDLC/PPP/FR PW */
    if (AtPwTypeGet((AtPw)adapter) == cAtPwTypeHdlc)
        {
        self->hwPwId = AtChannelHwIdGet((AtChannel)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)AtPwBoundCircuitGet((AtPw)adapter)));
        return self;
        }

    if (ThaPwTypeIsHdlcPppFr((AtPw)adapter))
        self->hwPwId = AtChannelIdGet((AtChannel)AtPwBoundCircuitGet((AtPw)adapter));

    return self;
    }

ThaHwPw Tha60210012HwPwNew(ThaPwAdapter adapter, uint8 slice, uint32 tdmPwId, eBool isLo)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHwPw newHwPw = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newHwPw == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHwPw, adapter, slice, tdmPwId, isLo);
    }

uint32 Tha60210012HwPwHwFlowId(ThaHwPw self)
    {
    return self ? mThis(self)->hwFlowId : cInvalidUint32;
    }

void Tha60210012HwPwHwFlowIdSet(ThaHwPw self, uint32 hwFlowId)
    {
	if (self)
        mThis(self)->hwFlowId = hwFlowId;
	}

AtEthFlow Tha60210012HwPwHwFlowGet(ThaHwPw self)
    {
    if (self == NULL)
        return NULL;

    if (mThis(self)->hwFlow == NULL)
        mThis(self)->hwFlow = Tha60210012EthFlowNew(mThis(self)->hwFlowId, ModuleEth(self));

    ThaEthFlowHwFlowIdSet((ThaEthFlow)(mThis(self)->hwFlow), mThis(self)->hwFlowId);
    return mThis(self)->hwFlow;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210012PwActivator.c
 *
 * Created Date: Feb 29, 2016
 *
 * Description : 60210012 PW activator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtFrVirtualCircuit.h"
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/encap/dynamic/ThaEncapDynamicBinder.h"
#include "../../../Tha60210011/pw/activator/Tha60210011HwPw.h"
#include "../../cla/Tha60210012ModuleCla.h"
#include "../../pwe/Tha60210012ModulePwe.h"
#include "../../encap/Tha60210012ModuleEncap.h"
#include "../../sur/Tha60210012ModuleSur.h"
#include "../../ppp/Tha60210012MpBundle.h"
#include "../Tha60210012ModulePw.h"
#include "Tha60210012PwActivator.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210012PwActivator)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwActivatorMethods         m_ThaPwActivatorOverride;
static tThaPwDynamicActivatorMethods  m_ThaPwDynamicActivatorOverride;
static tTha60210011PwActivatorMethods m_Tha60210011PwActivatorOverride;

/* Save super implementation */
static const tThaPwActivatorMethods * m_ThaPwActivatorMethods = NULL;
static const tThaPwDynamicActivatorMethods *m_ThaPwDynamicActivatorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleCla ClaModule(ThaPwAdapter adapter)
    {
    return (ThaModuleCla)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)adapter), cThaModuleCla);
    }

static ThaModulePwe PweModule(ThaPwAdapter adapter)
    {
    return (ThaModulePwe)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)adapter), cThaModulePwe);
    }

static ThaModuleHardSur SurModule(ThaPwAdapter adapter)
    {
    return (ThaModuleHardSur)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)adapter), cAtModuleSur);
    }

static eAtRet ActivateConfiguration(ThaPwDynamicActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret = cAtOk;
    ThaModulePwe modulePwe = PweModule(adapter);
    ThaModuleHardSur moduleSur;
    AtPw pw = (AtPw)adapter;

    if (Tha60210012ModulePwePtchEnablePerChannelIsSupported(modulePwe))
        {
        ret = AtPwPtchInsertionModeSet(pw, Tha60210012ModulePweDefaultPtchMode());
        ret |= AtPwPtchServiceEnable(pw, cAtTrue);
        }

    ret |= m_ThaPwDynamicActivatorMethods->ActivateConfiguration(self, adapter);
    if (ret != cAtOk)
        return ret;

    /* Allocate internal counter pool for debugging */
    Tha60210012ModulePwDebugCounterPoolIdAllocate(pw);

    ret = Tha60210012ModulePwePwActivateConfiguration(modulePwe, adapter);
    moduleSur = SurModule(adapter);
    if (moduleSur)
        ret |= Tha60210012ModuleSurPwCounterModeSet(moduleSur, pw);

    if (AtPwTypeGet(pw) == cAtPwTypeMlppp)
        ret |= Tha60210012MpBundleRsqEngineActivate((AtHdlcBundle)AtPwBoundCircuitGet(pw), Tha60210012PwAdapterHwFlowGet(adapter));

    return ret;
    }

static eAtRet DeactivateConfiguration(ThaPwDynamicActivator self, ThaPwAdapter adapter)
    {
    ThaModulePwe modulePwe = PweModule(adapter);
    eAtRet ret = Tha60210012ModulePwePwDeactivateConfiguration(modulePwe, adapter);
    AtPw pw = (AtPw)adapter;

    if (AtPwTypeGet(pw) == cAtPwTypeMlppp)
        ret |= Tha60210012MpBundleRsqEngineDeactivate((AtHdlcBundle)AtPwBoundCircuitGet(pw), Tha60210012PwAdapterHwFlowGet(adapter));

    ret |= m_ThaPwDynamicActivatorMethods->DeactivateConfiguration(self, adapter);
    if (ret != cAtOk)
        return ret;

    /* De-allocate internal counter pool */
    ThaDebug60210012ModulePwFreePoolResource(pw);

    if (Tha60210012ModulePwePtchEnablePerChannelIsSupported(modulePwe))
        return AtPwPtchServiceEnable(pw, cAtFalse);

    return cAtOk;
    }

static ThaHwPw HwPwObjectCreate(Tha60210011PwActivator self, ThaPwAdapter adapter, uint8 slice, uint32 tdmPwId, eBool isLo)
    {
    AtUnused(self);
    return Tha60210012HwPwNew(adapter, slice, tdmPwId, isLo);
    }

static ThaEncapDynamicBinder EncapBinder(ThaPwAdapter adapter)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)adapter);
    return (ThaEncapDynamicBinder)AtDeviceEncapBinder(device);
    }

static ThaHwPw HwPwAllocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    uint32 freeHwFlowId;
    ThaHwPw hwPw;
    ThaEncapDynamicBinder encapBinder = EncapBinder(adapter);
    Tha60210011PwActivator activator = (Tha60210011PwActivator)self;

    uint32 tdmPwId = AtChannelEncapHwIdAllocate(AtPwBoundCircuitGet((AtPw)adapter));
    uint8 slice = ThaEncapDynamicBinderHwSlice(encapBinder, tdmPwId);
    eBool isHo  = ThaEncapDynamicBinderHwIsHo(encapBinder, tdmPwId);

    if (tdmPwId == cInvalidUint32)
        return NULL;

    hwPw = mMethodsGet(activator)->HwPwObjectCreate(activator, adapter, slice,
                                                      ThaEncapDynamicBinderHwChannelId(encapBinder, tdmPwId),
                                                      (isHo) ? cAtFalse : cAtTrue);
    if (hwPw == NULL)
        return NULL;

    if (Tha60210012HwPwHwFlowId(hwPw) == cInvalidUint32)
        {
        freeHwFlowId = Tha60210012ModuleClaHwFlowIdAllocate(ClaModule(adapter), AtChannelIdGet((AtChannel)adapter));
        if (AtModuleAccessible((AtModule)ThaPwActivatorModuleGet(self)) && !Tha60210012ModuleClaHwFlowIdIsValid(freeHwFlowId))
            {
            m_ThaPwActivatorMethods->HwPwDeallocate(self, adapter);
            return NULL;
            }

        Tha60210012HwPwHwFlowIdSet(hwPw, freeHwFlowId);
        }

    return hwPw;
    }

static eAtRet HwPwDeallocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    ThaHwPw hwPw = ThaPwAdapterHwPwGet(adapter);
    ThaEncapDynamicBinder encapBinder;
    uint32 hwId;

    if (hwPw == NULL)
        return cAtOk;

    encapBinder = EncapBinder(adapter);
    hwId = ThaEncapDynamicBinderCompressedHwId(encapBinder,
                                               Tha60210011HwPwTdmIdGet(hwPw),
                                               Tha60210011HwPwSliceGet(hwPw),
                                               (Tha60210011HwPwIsLo(hwPw) ? cAtFalse : cAtTrue));

    AtEncapBinderEncapHwIdDeallocate((AtEncapBinder)encapBinder, hwId);
    Tha60210012ModuleClaHwFlowIdDeallocate(ClaModule(adapter), Tha60210012PwAdapterHwFlowIdGet(adapter));

    return m_ThaPwActivatorMethods->HwPwDeallocate(self, adapter);
    }

static eBool VlanTagsAreUsed(ThaPwActivator self, ThaPwAdapter adapter, tAtEthVlanTag *expectedCVlan, tAtEthVlanTag *expectedSVlan)
    {
    /* TODO: VLAN tags in this product are not only resource of PWs
     * Need to redirect to a common function to check the VLAN tags resource here */
    AtUnused(self);
    AtUnused(adapter);
    AtUnused(expectedCVlan);
    AtUnused(expectedSVlan);
    return cAtFalse;
    }

static void OverrideThaPwActivator(ThaPwActivator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwActivatorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwActivatorOverride, m_ThaPwActivatorMethods, sizeof(m_ThaPwActivatorOverride));

        mMethodOverride(m_ThaPwActivatorOverride, VlanTagsAreUsed);
        mMethodOverride(m_ThaPwActivatorOverride, HwPwDeallocate);
        mMethodOverride(m_ThaPwActivatorOverride, HwPwAllocate);
        }

    mMethodsSet(self, &m_ThaPwActivatorOverride);
    }

static void OverrideThaPwDynamicActivator(ThaPwActivator self)
    {
    ThaPwDynamicActivator activator = (ThaPwDynamicActivator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwDynamicActivatorMethods = mMethodsGet(activator);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDynamicActivatorOverride, m_ThaPwDynamicActivatorMethods, sizeof(m_ThaPwDynamicActivatorOverride));

        mMethodOverride(m_ThaPwDynamicActivatorOverride, ActivateConfiguration);
        mMethodOverride(m_ThaPwDynamicActivatorOverride, DeactivateConfiguration);
        }

    mMethodsSet(activator, &m_ThaPwDynamicActivatorOverride);
    }

static void OverrideTha60210011PwActivator(ThaPwActivator self)
    {
    Tha60210011PwActivator activator = (Tha60210011PwActivator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011PwActivatorOverride, mMethodsGet(activator), sizeof(m_Tha60210011PwActivatorOverride));

        mMethodOverride(m_Tha60210011PwActivatorOverride, HwPwObjectCreate);
        }

    mMethodsSet(activator, &m_Tha60210011PwActivatorOverride);
    }

static void Override(ThaPwActivator self)
    {
    OverrideThaPwActivator(self);
    OverrideThaPwDynamicActivator(self);
    OverrideTha60210011PwActivator(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012PwActivator);
    }

ThaPwActivator Tha60210012PwActivatorObjectInit(ThaPwActivator self, AtModulePw pwModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PwDynamicActivatorObjectInit(self, pwModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwActivator Tha60210012PwDynamicActivatorNew(AtModulePw pwModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwActivator newActivator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newActivator == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012PwActivatorObjectInit(newActivator, pwModule);
    }

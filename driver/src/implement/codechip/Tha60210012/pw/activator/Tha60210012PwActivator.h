/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210012PwActivator.h
 * 
 * Created Date: Feb 29, 2016
 *
 * Description : 60210012 PW activator APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PWACTIVATOR_H_
#define _THA60210012PWACTIVATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/ThaModulePw.h"
#include "../../../../default/pw/adapters/ThaPwAdapter.h"
#include "../../../Tha60210011/pw/activator/Tha60210011PwActivatorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012PwActivator * Tha60210012PwActivator;

typedef struct tTha60210012PwActivator
    {
    tTha60210011PwActivator super;
    }tTha60210012PwActivator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwActivator Tha60210012PwActivatorObjectInit(ThaPwActivator self, AtModulePw pwModule);
ThaHwPw Tha60210012HwPwNew(ThaPwAdapter adapter, uint8 slice, uint32 tdmPwId, eBool isLo);
uint32 Tha60210012HwPwHwFlowId(ThaHwPw self);
void Tha60210012HwPwHwFlowIdSet(ThaHwPw self, uint32 hwFlowId);
AtEthFlow Tha60210012HwPwHwFlowGet(ThaHwPw self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PWACTIVATOR_H_ */


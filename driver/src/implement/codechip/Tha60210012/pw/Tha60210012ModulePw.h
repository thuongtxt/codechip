/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Pseudowire
 * 
 * File        : Tha60210012ModulePw.h
 * 
 * Created Date: Apr 11, 2016
 *
 * Description : 60210012 module pw utilities
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEPW_H_
#define _THA60210012MODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/pw/Tha60210051ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cThaInternalCounterMaxPoolNum 32
#define cThaInternalCounterPoolInvalid cBit31_0
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModulePw * Tha60210012ModulePw;
typedef struct tTha60210012ModulePwMethods
    {
    eBool (*InternalCounterIsSupported)(Tha60210012ModulePw self);
    }tTha60210012ModulePwMethods;

typedef struct tTha60210012ModulePw
    {
    tTha60210051ModulePw super;
    const tTha60210012ModulePwMethods *methods;

    /* Handle memory pool for internal debug counter */
    uint32 poolList;
    }tTha60210012ModulePw;

typedef struct tThaDebugPwInternalCnt
    {
    uint32 txPackets;
    uint32 txPayloadBytes;
    uint32 txPacketFragments;
    uint32 txLbitPackets;
    uint32 txRbitPackets;
    uint32 txMbitPackets;
    uint32 txPbitPackets;

    uint32 rxPackets;
    uint32 rxPayloadBytes;
    uint32 rxLofsPackets;
    uint32 rxLbitPackets;
    uint32 rxNbitPackets;
    uint32 rxPbitPackets;
    uint32 rxRbitPackets;
    uint32 rxLatePackets;
    uint32 rxEarlyPackets;
    uint32 rxLostPackets;
    uint32 rxOverrunPackets;
    uint32 rxUnderrunPackets;
    uint32 rxMalformPackets;
    uint32 rxStrayPackets;
    }tThaDebugPwInternalCnt;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw Tha60210012ModulePwObjectInit(AtModulePw self, AtDevice device);

uint32 Tha60210012PwAdapterHwFlowIdGet(ThaPwAdapter adapter);
AtEthFlow Tha60210012PwAdapterHwFlowGet(ThaPwAdapter adapter);
eAtRet Tha60210012PseudowireStsNcEncapByPass(AtPw adapter, AtSdhChannel auVc);
eAtRet Tha60210012DebugPwInternalCounterGet(AtPw self, tThaDebugPwInternalCnt *pCounters, eBool r2c);
uint32 Tha60210012DebugPwInternalCounterPoolIdGet(AtPw self);
eAtRet Tha60210012DebugPwInternalCounterPoolIdSet(AtPw self, uint32 poolId, eBool enable);
eAtRet Tha60210012ModulePwDebugCounterPoolIdAllocate(AtPw pw);
eAtRet ThaDebug60210012ModulePwFreePoolResource(AtPw pw);
void Tha60210012DebugPwInternalMemoryPoolGet(ThaModulePw self, uint32 *poolList);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEPW_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60210012ModuleSur.h
 *
 * Created Date: Aug 3, 2016
 *
 * Description : 60210012 module sur interfaces
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULESUR_H_
#define _THA60210012MODULESUR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPw.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../../Tha60210011/sur/Tha60210011ModuleSurInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleSur * Tha60210012ModuleSur;
typedef struct tTha60210012ModuleSurMethods
    {
    eBool (*ExternalCounterIsSupported)(Tha60210012ModuleSur self);
    }tTha60210012ModuleSurMethods;

typedef struct tTha60210012ModuleSur
    {
    tTha60210011ModuleSur super;
    const tTha60210012ModuleSurMethods * methods;

    }tTha60210012ModuleSur;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSur Tha60210012ModuleSurObjectInit(AtModuleSur self, AtDevice device);
eAtRet Tha60210012ModuleSurPwCounterModeSet(ThaModuleHardSur self, AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULESUR_H_ */


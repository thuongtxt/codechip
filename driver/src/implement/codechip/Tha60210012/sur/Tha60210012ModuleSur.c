/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60210012ModuleSur.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : SUR Module of product 60210012
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/sur/hard/ThaModuleHardSurReg.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "Tha60210012ModuleSur.h"
#include "../pw/Tha60210012ModulePw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210012ModuleSur)self)
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012ModuleSurMethods m_methods;

/* Override */
static tAtModuleSurMethods       m_AtModuleSurOverride;
static tThaModuleHardSurMethods  m_ThaModuleHardSurOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportExternalCounter(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x2, 0x0);
    }

static eBool ExternalCounterIsSupported(Tha60210012ModuleSur self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (currentVersion >= StartVersionSupportExternalCounter())
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet PwCounterModeSet(ThaModuleHardSur self, AtPw pw)
    {
    uint8 oddOrEven;
    uint32 address, regVal;
    AtChannel channel = AtPwBoundCircuitGet(pw);
    eBool isHo;
    uint8 lineId  = 0;
    uint32 hwStsId = 0;

    /* Cannot determine if circuit has not been bound */
    if (channel == NULL)
        return cAtOk;

    if (ThaPwTypeIsHdlcPppFr(pw))
        return cAtOk;

    Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &lineId, &hwStsId);
    isHo = mMethodsGet(self)->IsHoPw(self, pw);

    if (isHo)
        {
        oddOrEven = hwStsId % 2;
        hwStsId = hwStsId >> 1;
        }
    else
        {
        oddOrEven = lineId % 2;
        lineId = lineId / 2;
        }

    address = ThaModuleHardSurBaseAddress(self) + cAf6_FMPM_PW_HO_Counter_Mode + AtChannelIdGet((AtChannel)pw);
    regVal = mModuleHwRead(self, address);

    isHo = mMethodsGet(self)->IsHoPw(self, pw);
    mRegFieldSet(regVal, cAf6_FMPM_PW_HO_Counter_Mode_Pwho_Enb_, mBoolToBin(isHo));
    mRegFieldSet(regVal, cAf6_FMPM_PW_HO_Counter_Mode_Pwho_Line_, lineId);
    mRegFieldSet(regVal, cAf6_FMPM_PW_HO_Counter_Mode_Pwho_Slice_, oddOrEven);
    mRegFieldSet(regVal, cAf6_FMPM_PW_HO_Counter_Mode_Pwho_Sts_, hwStsId);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

static uint32 IdOfHdlcPw(ThaModuleHardSur self, AtPw pw)
    {
    AtUnused(self);
    return Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pw);
    }

static eBool ShouldActivateFeatureFromVersion(AtModuleSur self, uint32 major, uint32 minor, uint32 betaBuild)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(major, minor, betaBuild);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool FailureTimersConfigurable(AtModuleSur self)
    {
    return ShouldActivateFeatureFromVersion(self, 0x5, 0x9, 0x1031);
    }

static eBool FailureHoldOffTimerConfigurable(AtModuleSur self)
    {
    return FailureTimersConfigurable(self);
    }

static eBool FailureHoldOnTimerConfigurable(AtModuleSur self)
    {
    return FailureTimersConfigurable(self);
    }

static eBool FailureForwardingStatusIsSupported(ThaModuleHardSur self)
    {
    return ShouldActivateFeatureFromVersion((AtModuleSur)self, 0x5, 0x9, 0x1040);
    }

static void OverrideAtModuleSur(AtModuleSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSurOverride, mMethodsGet(self), sizeof(m_AtModuleSurOverride));

        mMethodOverride(m_AtModuleSurOverride, FailureHoldOffTimerConfigurable);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOnTimerConfigurable);
        }

    mMethodsSet(self, &m_AtModuleSurOverride);
    }

static void OverrideThaModuleHardSur(AtModuleSur self)
    {
    ThaModuleHardSur ethModule = (ThaModuleHardSur)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardSurOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleHardSurOverride));

        mMethodOverride(m_ThaModuleHardSurOverride, IdOfHdlcPw);
        mMethodOverride(m_ThaModuleHardSurOverride, FailureForwardingStatusIsSupported);
        }

    mMethodsSet(ethModule, &m_ThaModuleHardSurOverride);
    }

static void MethodsInit(Tha60210012ModuleSur self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Override methods */
        mMethodOverride(m_methods, ExternalCounterIsSupported);
        }

   mMethodsSet(self, &m_methods);
   }

static void Override(AtModuleSur self)
    {
    OverrideAtModuleSur(self);
    OverrideThaModuleHardSur(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleSur);
    }

AtModuleSur Tha60210012ModuleSurObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSur Tha60210012ModuleSurNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (module == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModuleSurObjectInit(module, device);
    }

eAtRet Tha60210012ModuleSurPwCounterModeSet(ThaModuleHardSur self, AtPw pw)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (mMethodsGet(mThis(self))->ExternalCounterIsSupported(mThis(self)))
        return PwCounterModeSet(self, pw);

    return cAtOk;
    }

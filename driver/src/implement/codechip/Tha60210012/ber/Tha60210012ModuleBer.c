/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210012ModuleBer.c
 *
 * Created Date: Feb 29, 2016
 *
 * Description : Module BER interface
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../../generic/pdh/AtPdhChannelInternal.h"
#include "Tha60210012ModuleBer.h"
#include "../man/Tha60210012Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tAtModuleBerMethods          m_AtModuleBerOverride;
static tThaModuleHardBerMethods     m_ThaModuleHardBerOverride;
static tTha60210011ModuleBerMethods m_Tha60210011ModuleBerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtBerController AuVcBerControllerCreate(ThaModuleHardBer self, AtSdhChannel auVc)
    {
    return Tha60210012SdhAuVcBerControllerNew(AtChannelIdGet((AtChannel)auVc), (AtChannel)auVc, (AtModuleBer)self);
    }

static AtBerController Tu3VcBerControllerCreate(ThaModuleHardBer self, AtSdhChannel tu3Vc)
    {
    return Tha60210012SdhTu3VcBerControllerNew(AtChannelIdGet((AtChannel)tu3Vc), (AtChannel)tu3Vc, (AtModuleBer)self);
    }

static AtBerController Vc1xBerControllerCreate(ThaModuleHardBer self, AtSdhChannel vc1x)
    {
    return Tha60210012SdhVc1xBerControllerNew(AtChannelIdGet((AtChannel)vc1x), (AtChannel)vc1x, (AtModuleBer)self);
    }

static AtBerController De1PathBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de1)
    {
    return Tha60210012PdhDe1BerControllerNew(AtChannelIdGet((AtChannel)de1), (AtChannel)de1, (AtModuleBer)self);
    }

static AtBerController De3PathBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de3)
    {
    return Tha60210012PdhDe3BerControllerNew(AtChannelIdGet((AtChannel)de3), (AtChannel)de3, (AtModuleBer)self);
    }

static uint32 Threshold1AddressByRate(Tha60210011ModuleBer self, uint32 rate)
    {
    return 0x62180U + rate + AtModuleBerBaseAddress((AtModuleBer)self);
    }

static eBool ChannelBerShouldBeEnabledByDefault(AtModuleBer self)
    {
    /* BER may have issues and hardware may want to disable it by default for
     * every channel. Use this attribute to handle this. */
    AtUnused(self);
    return cAtTrue;
    }

static uint32 BerStsTu3ReportBaseAddress(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return 0x69500;
    }

static uint32 BerMeasureStsChannelOffset(Tha60210011ModuleBer self)
    {
    AtUnused(self);
    return 2688UL;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsBerRegister(AtModuleDeviceGet(self), address);
    }

static void OverrideAtModule(AtModuleBer self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleBer(AtModuleBer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleBerOverride, mMethodsGet(self), sizeof(m_AtModuleBerOverride));

        mMethodOverride(m_AtModuleBerOverride, ChannelBerShouldBeEnabledByDefault);
        }

    mMethodsSet(self, &m_AtModuleBerOverride);
    }

static void OverrideThaModuleHardBer(AtModuleBer self)
    {
    ThaModuleHardBer module = (ThaModuleHardBer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardBerOverride, mMethodsGet(module), sizeof(m_ThaModuleHardBerOverride));

        mMethodOverride(m_ThaModuleHardBerOverride, AuVcBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, Tu3VcBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, Vc1xBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, De1PathBerControllerCreate);
        mMethodOverride(m_ThaModuleHardBerOverride, De3PathBerControllerCreate);
        }

    mMethodsSet(module, &m_ThaModuleHardBerOverride);
    }

static void OverrideTha60210011ModuleBer(AtModuleBer self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleBerOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleBerOverride));

        /* Setup methods */
        mMethodOverride(m_Tha60210011ModuleBerOverride, Threshold1AddressByRate);
        mMethodOverride(m_Tha60210011ModuleBerOverride, BerStsTu3ReportBaseAddress);
        mMethodOverride(m_Tha60210011ModuleBerOverride, BerMeasureStsChannelOffset);
        }

    mMethodsSet(module, &m_Tha60210011ModuleBerOverride);
    }

static void Override(AtModuleBer self)
    {
    OverrideAtModule(self);
    OverrideAtModuleBer(self);
    OverrideThaModuleHardBer(self);
    OverrideTha60210011ModuleBer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleBer);
    }

AtModuleBer Tha60210012ModuleBerObjectInit(AtModuleBer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModuleBerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleBer Tha60210012ModuleBerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleBer newBerModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBerModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModuleBerObjectInit(newBerModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210012PdhDe1BerController.c
 *
 * Created Date: Aug 4, 2015
 *
 * Description : DS1/E1 BER controller implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaPdhDe2De1.h"
#include "../../Tha60210011/ber/Tha60210011SdhAuVcBerControllerInternal.h"
#include "Tha60210012ModuleBer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012PdhDe1BerController
    {
    tTha60210011PdhDe1BerController super;
    }tTha60210012PdhDe1BerController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerControllerMethods                 m_AtBerControllerOverride;
static tTha60210011SdhAuVcBerControllerMethods m_Tha60210011SdhAuVcBerControllerOverride;

/* Cache super */
static const tAtBerControllerMethods           *m_AtBerControllerMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    uint8 tug2Id;
    uint8 slice = 0, hwSts = 0;
    AtPdhChannel de1 = (AtPdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(de1);

    if (sdhChannel)
        tug2Id = AtSdhChannelTug2Get(sdhChannel);
    else
        tug2Id = ThaPdhDe2De1HwDe2IdGet((ThaPdhDe1)de1);

    ThaPdhChannelHwIdGet(de1, cThaModulePoh, &slice, &hwSts);
    return (hwSts * 8UL + (slice + 2UL) * 512UL + tug2Id);
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    AtPdhChannel de1 = (AtPdhChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(de1);
    uint8 slice = 0, hwSts = 0;
    uint8 tug2Id;
    uint8 tuId;

    ThaPdhChannelHwIdGet(de1, cThaModulePoh, &slice, &hwSts);

    if (sdhChannel)
        {
        tug2Id = AtSdhChannelTug2Get(sdhChannel);
        tuId = AtSdhChannelTu1xGet(sdhChannel);
        }
    else
        {
        tug2Id = ThaPdhDe2De1HwDe2IdGet((ThaPdhDe1)de1);
        tuId = ThaPdhDe2De1HwDe1IdGet((ThaPdhDe1)de1);
        }

    return hwSts * 28UL + (slice + 2UL) * 1344UL + tug2Id * 4UL + tuId;
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    return CurrentBerOffset((Tha60210011SdhAuVcBerController)self);
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtBerControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, m_AtBerControllerMethods, sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, MeasureEngineId);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    OverrideTha60210011SdhAuVcBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PdhDe1BerController);
    }

static AtBerController ObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PdhDe1BerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210012PdhDe1BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, controllerId, channel, berModule);
    }

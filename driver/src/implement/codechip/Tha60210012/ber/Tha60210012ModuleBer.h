/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60210012ModuleBer.h
 * 
 * Created Date: Feb 29, 2016
 *
 * Description : BER module interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEBER_H_
#define _THA60210012MODULEBER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/ber/Tha60210051ModuleBer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleBer
    {
    tTha60210051ModuleBer super;
    }tTha60210012ModuleBer;

/*--------------------------- Forward declarations ---------------------------*/
AtModuleBer Tha60210012ModuleBerNew(AtDevice device);
AtModuleBer Tha60210012ModuleBerObjectInit(AtModuleBer self, AtDevice device);

AtBerController Tha60210012SdhAuVcBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210012SdhTu3VcBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210012SdhVc1xBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210012PdhDe1BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210012PdhDe3BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEBER_H_ */


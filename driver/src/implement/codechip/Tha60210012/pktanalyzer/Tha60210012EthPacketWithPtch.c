/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60210012EthPacketWithPtch.c
 *
 * Created Date: Jul 14, 2016
 *
 * Description : 60210012 Ethernet packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/pktanalyzer/AtEthPacketInternal.h"
#include "Tha60210012InternalPktAnalyzer.h"
#include "Tha60210012EthPacketWithPtchInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210012EthPacketWithPtch*)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;
static tAtPacketMethods m_AtPacketOverride;
static tAtEthPacketMethods m_AtEthPacketOverride;

/* Super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DisplayPtch(AtEthPacket self, const char *name, uint8 *ptch, uint8 numPtchByte, uint8 level)
    {
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* %s: ", name);
    AtPacketDisplayBufferInHex((AtPacket)self, ptch, numPtchByte, ".");
    AtPrintc(cSevNormal, "\r\n");
    }

static uint8 PtchLength(AtPacket self)
    {
    AtUnused(self);
    return 1;
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    AtEthPacket packet = (AtEthPacket)self;
    uint8 preambleLen;
    uint8 ptchLength = PtchLength(self);

    mMethodsGet(packet)->DisplayPreamble(packet, level);

    preambleLen = AtEthPacketPreambleLen(packet);
    DisplayPtch(packet, "PTCH", &(AtPacketDataBuffer(self, NULL)[preambleLen]), ptchLength, level);
    AtEthPacketDisplayMac(packet, "DMAC", &(AtPacketDataBuffer(self, NULL)[preambleLen]) + ptchLength, level);
    AtEthPacketDisplayMac(packet, "SMAC", &(AtPacketDataBuffer(self, NULL)[preambleLen + ptchLength + cMacLength]), level);
    AtEthPacketDisplayVlans(packet, level);
    AtEthPacketDisplayEthTypeLength(packet, level);
    AtEthPacketDisplayCrc(packet, level);
    }

static uint16 FirstVlanOffset(AtEthPacket self)
    {
    return (uint16)(AtEthPacketPreambleLen(self) + (uint32)(cMacLength * 2) + PtchLength((AtPacket)self));
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210012EthPacketWithPtch* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(direction);
    }

static void OverrideAtObject(AtPacket self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void OverrideAtEthPacket(AtPacket self)
    {
    AtEthPacket ethPacket = (AtEthPacket)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPacketOverride, mMethodsGet(ethPacket), sizeof(m_AtEthPacketOverride));

        mMethodOverride(m_AtEthPacketOverride, FirstVlanOffset);
        }

    mMethodsSet(ethPacket, &m_AtEthPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtObject(self);
    OverrideAtPacket(self);
    OverrideAtEthPacket(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012EthPacketWithPtch);
    }

AtPacket Tha60210012EthPacketWithPtchObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPktAnalyzerDirection direction)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEthPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    mThis(self)->direction = direction;

    return self;
    }

AtPacket Tha60210012EthPacketWithPtchNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPktAnalyzerDirection direction)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210012EthPacketWithPtchObjectInit(newPacket, dataBuffer, length, cacheMode, direction);
    }

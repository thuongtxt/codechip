/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210012PtchService.h
 *
 * Created Date: May 7, 2016
 *
 * Description : PTCH service
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012INTERNALPKTANALYZER_H_
#define _THA60210012INTERNALPKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePktAnalyzer.h"
#include "AtPktAnalyzer.h"
#include "../../../default/pktanalyzer/ThaEthPortPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaInternalPlaPacketHeader
    {
	uint16 ethFlowId;
	uint8 numFcsByte;
    uint8 numPadding;
    uint8 fcsEn;
    uint8 headerLen;
    uint8 eop;
    uint8 forwardType;
    uint8 networkProtoId;
    uint8 psnAddress;
    uint8 pktService;
    uint8 psnLen;
    uint16 pktLen;
    uint8 fragmentFcn;
    }tThaInternalPlaPacketHeader;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60210012InternalPktAnalyzerServiceIdSet(AtPktAnalyzer self, uint32 serviceId);
void Tha60210012InternalPktAnalyzerDirectionSet(AtPktAnalyzer self, eThaPktAnalyzerPktDumpMode direction);
eAtRet Tha60210012InternalPktAnalyzerTxStartEntrySet(AtPktAnalyzer self, uint16 startEntry);
eAtRet Tha60210012InternalPktAnalyzerTxLastEntrySet(AtPktAnalyzer self, uint16 lastEntry);
uint16 Tha60210012InternalPktAnalyzerTxStartEntryGet(AtPktAnalyzer self);
uint16 Tha60210012InternalPktAnalyzerTxLastEntryGet(AtPktAnalyzer self);
eAtRet Tha60210012InternalTxPktAnalyzer(AtPktAnalyzer self);
eAtRet Tha60210012InternalTxPlaPktHeaderAnalyze(AtPktAnalyzer self, uint16 entryId, tThaInternalPlaPacketHeader *header);
eAtRet Tha60210012InternalRxPwePktHeaderAnalyze(AtPktAnalyzer self, uint16 entryId, tThaInternalPlaPacketHeader *header);

AtPktAnalyzer Tha60210012InternalPacketAnalyzerGet(AtModulePktAnalyzer self);
AtPacket Tha60210012EthPacketWithPtchNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPktAnalyzerDirection direction);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012INTERNALPKTANALYZER_H_ */


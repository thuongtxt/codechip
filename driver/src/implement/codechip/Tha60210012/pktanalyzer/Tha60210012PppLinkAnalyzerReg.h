/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet Analyzer
 * 
 * File        : Tha60210012PppLinkAnalyzerReg.h
 * 
 * Created Date: Jul 25, 2016
 *
 * Description : Register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PPPLINKANALYZERREG_H_
#define _THA60210012PPPLINKANALYZERREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : IgDump Control
Reg Addr   : 0x00_0800
Reg Formula:
    Where  :
Reg Desc   :
This register is used to config control dump engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pkdump_ctl_Base                                                                  0x000800
#define cAf6Reg_upen_pkdump_ctl                                                                       0x000800
#define cAf6Reg_upen_pkdump_ctl_WidthVal                                                                    32
#define cAf6Reg_upen_pkdump_ctl_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: Dump_Psize
BitField Type: RW
BitField Desc: Dump packet size 0x0 : dump full packet 0x1 : dump 32 bytes
header 0x2 : dump 64 bytes header 0x3 : dump 128 bytes header 0x4 : dump 256
bytes header 0x5 : dump 512 bytes header 0x6 : dump 1024 bytes header other :
dump 2048 bytes header
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_upen_pkdump_ctl_Dump_Psize_Bit_Start                                                           12
#define cAf6_upen_pkdump_ctl_Dump_Psize_Bit_End                                                             15
#define cAf6_upen_pkdump_ctl_Dump_Psize_Mask                                                         cBit15_12
#define cAf6_upen_pkdump_ctl_Dump_Psize_Shift                                                               12
#define cAf6_upen_pkdump_ctl_Dump_Psize_MaxVal                                                             0xf
#define cAf6_upen_pkdump_ctl_Dump_Psize_MinVal                                                             0x0
#define cAf6_upen_pkdump_ctl_Dump_Psize_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Dump_Keep
BitField Type: RW
BitField Desc: Dump keep status 0x0 : Clear Buffer when cpu write the register
0x1 : Keep buffer and continous dump next
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_pkdump_ctl_Dump_Keep_Bit_Start                                                            11
#define cAf6_upen_pkdump_ctl_Dump_Keep_Bit_End                                                              11
#define cAf6_upen_pkdump_ctl_Dump_Keep_Mask                                                             cBit11
#define cAf6_upen_pkdump_ctl_Dump_Keep_Shift                                                                11
#define cAf6_upen_pkdump_ctl_Dump_Keep_MaxVal                                                              0x1
#define cAf6_upen_pkdump_ctl_Dump_Keep_MinVal                                                              0x0
#define cAf6_upen_pkdump_ctl_Dump_Keep_RstVal                                                              0x0

/*--------------------------------------
BitField Name: Dump_Roll
BitField Type: RW
BitField Desc: Dump Roll buffer 0x0 : Stop dump when buffer full 0x1 :
Continuous dump when buffer full
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_pkdump_ctl_Dump_Roll_Bit_Start                                                            10
#define cAf6_upen_pkdump_ctl_Dump_Roll_Bit_End                                                              10
#define cAf6_upen_pkdump_ctl_Dump_Roll_Mask                                                             cBit10
#define cAf6_upen_pkdump_ctl_Dump_Roll_Shift                                                                10
#define cAf6_upen_pkdump_ctl_Dump_Roll_MaxVal                                                              0x1
#define cAf6_upen_pkdump_ctl_Dump_Roll_MinVal                                                              0x0
#define cAf6_upen_pkdump_ctl_Dump_Roll_RstVal                                                              0x0

/*--------------------------------------
BitField Name: Dump_Type
BitField Type: RW
BitField Desc: Dump Type 0x0 : Dump all packets 0x1 : only dump error packets
0x2 : only dump good packets 0x3 : disable dump
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_upen_pkdump_ctl_Dump_Type_Bit_Start                                                             8
#define cAf6_upen_pkdump_ctl_Dump_Type_Bit_End                                                               9
#define cAf6_upen_pkdump_ctl_Dump_Type_Mask                                                            cBit9_8
#define cAf6_upen_pkdump_ctl_Dump_Type_Shift                                                                 8
#define cAf6_upen_pkdump_ctl_Dump_Type_MaxVal                                                              0x3
#define cAf6_upen_pkdump_ctl_Dump_Type_MinVal                                                              0x0
#define cAf6_upen_pkdump_ctl_Dump_Type_RstVal                                                              0x0

/*--------------------------------------
BitField Name: Dump_Src
BitField Type: RW
BitField Desc: Dump source 0x0 : Dump packets 0x1 : Dump output info 0x2 : Dump
input info other : Unused
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_upen_pkdump_ctl_Dump_Src_Bit_Start                                                              4
#define cAf6_upen_pkdump_ctl_Dump_Src_Bit_End                                                                7
#define cAf6_upen_pkdump_ctl_Dump_Src_Mask                                                             cBit7_4
#define cAf6_upen_pkdump_ctl_Dump_Src_Shift                                                                  4
#define cAf6_upen_pkdump_ctl_Dump_Src_MaxVal                                                               0xf
#define cAf6_upen_pkdump_ctl_Dump_Src_MinVal                                                               0x0
#define cAf6_upen_pkdump_ctl_Dump_Src_RstVal                                                               0x0

/*--------------------------------------
BitField Name: Dump_Sel
BitField Type: RW
BitField Desc: Dump Seclect ID 0x0 : TDM_ID selected, Connection ID [15:0] ~
Map_ID, [19:16]~xbus 0x1 : Link_ID selected, Connection ID [15:0]~ output link
id 0x2 : Mix Bundle_Link_ID selected, Connection ID [15:0] ~link id, [31:16] ~
bundle_id 0x3 : Ethernet Flow ID selected, Connection ID [15:0] ~link id, other
: Unused End:
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_pkdump_ctl_Dump_Sel_Bit_Start                                                              0
#define cAf6_upen_pkdump_ctl_Dump_Sel_Bit_End                                                                3
#define cAf6_upen_pkdump_ctl_Dump_Sel_Mask                                                             cBit3_0
#define cAf6_upen_pkdump_ctl_Dump_Sel_Shift                                                                  0
#define cAf6_upen_pkdump_ctl_Dump_Sel_MaxVal                                                               0xf
#define cAf6_upen_pkdump_ctl_Dump_Sel_MinVal                                                               0x0
#define cAf6_upen_pkdump_ctl_Dump_Sel_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : IgDump Connection
Reg Addr   : 0x00_0801
Reg Formula:
    Where  :
Reg Desc   :
This register is used to config control dump engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pkdump_sta_Base                                                                  0x000801
#define cAf6Reg_upen_pkdump_sta                                                                       0x000801
#define cAf6Reg_upen_pkdump_sta_WidthVal                                                                    32
#define cAf6Reg_upen_pkdump_sta_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: Dump_Add
BitField Type: Type
BitField Desc: Dump current address
BitField Bits: [31:04]
--------------------------------------*/
#define cAf6_upen_pkdump_sta_Dump_Add_Bit_Start                                                              4
#define cAf6_upen_pkdump_sta_Dump_Add_Bit_End                                                               31
#define cAf6_upen_pkdump_sta_Dump_Add_Mask                                                            cBit31_4
#define cAf6_upen_pkdump_sta_Dump_Add_Shift                                                                  4
#define cAf6_upen_pkdump_sta_Dump_Add_MaxVal                                                         0xfffffff
#define cAf6_upen_pkdump_sta_Dump_Add_MinVal                                                               0x0
#define cAf6_upen_pkdump_sta_Dump_Add_RstVal                                                          SW_Reset

/*--------------------------------------
BitField Name: Dump_Fsm
BitField Type: RO
BitField Desc: Dump Fsm 0x0: IDLE 0x1: READY 0x2: CLEAR 0x3: START 0x4: STOP
0x5: DUMP 0x6: WIN 0x7: FAIL 0x8: WEOP 0xF: DONE
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_pkdump_sta_Dump_Fsm_Bit_Start                                                              0
#define cAf6_upen_pkdump_sta_Dump_Fsm_Bit_End                                                                3
#define cAf6_upen_pkdump_sta_Dump_Fsm_Mask                                                             cBit3_0
#define cAf6_upen_pkdump_sta_Dump_Fsm_Shift                                                                  0
#define cAf6_upen_pkdump_sta_Dump_Fsm_MaxVal                                                               0xf
#define cAf6_upen_pkdump_sta_Dump_Fsm_MinVal                                                               0x0
#define cAf6_upen_pkdump_sta_Dump_Fsm_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : IgDump Connection
Reg Addr   : 0x00_0802
Reg Formula:
    Where  :
Reg Desc   :
This register is used to config control dump engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pkdump_cid_Base                                                                  0x000802
#define cAf6Reg_upen_pkdump_cid                                                                       0x000802
#define cAf6Reg_upen_pkdump_cid_WidthVal                                                                    32
#define cAf6Reg_upen_pkdump_cid_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: Dump_CID
BitField Type: RW
BitField Desc: Dump Connection ID, depend Dump_Sel field
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_pkdump_cid_Dump_CID_Bit_Start                                                              0
#define cAf6_upen_pkdump_cid_Dump_CID_Bit_End                                                               31
#define cAf6_upen_pkdump_cid_Dump_CID_Mask                                                            cBit31_0
#define cAf6_upen_pkdump_cid_Dump_CID_Shift                                                                  0
#define cAf6_upen_pkdump_cid_Dump_CID_MaxVal                                                        0xffffffff
#define cAf6_upen_pkdump_cid_Dump_CID_MinVal                                                               0x0
#define cAf6_upen_pkdump_cid_Dump_CID_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : IgDump Pkt counter
Reg Addr   : 0x00_0804
Reg Formula:
    Where  :
Reg Desc   :
This register is used to config control dump engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pkdump_pkc_Base                                                                  0x000804
#define cAf6Reg_upen_pkdump_pkc                                                                       0x000804
#define cAf6Reg_upen_pkdump_pkc_WidthVal                                                                    32
#define cAf6Reg_upen_pkdump_pkc_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: Dump_Pkts
BitField Type: RW
BitField Desc: Dump packet counters
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_pkdump_pkc_Dump_Pkts_Bit_Start                                                             0
#define cAf6_upen_pkdump_pkc_Dump_Pkts_Bit_End                                                              31
#define cAf6_upen_pkdump_pkc_Dump_Pkts_Mask                                                           cBit31_0
#define cAf6_upen_pkdump_pkc_Dump_Pkts_Shift                                                                 0
#define cAf6_upen_pkdump_pkc_Dump_Pkts_MaxVal                                                       0xffffffff
#define cAf6_upen_pkdump_pkc_Dump_Pkts_MinVal                                                              0x0
#define cAf6_upen_pkdump_pkc_Dump_Pkts_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : IgDump Pkt Information
Reg Addr   : 0x00_0900
Reg Formula: 0x00_0900 + $pkt_id
    Where  :
           + $pkt_id (0-255): packet_id
Reg Desc   :
This register is used to read packet information, packet_id equa 0 is first packet dump.
: each address corresspond for 64 bits data (8 bytes)
: number of address Pkt_info_NAdd = Pkt_Info_Len / 8
: location to read data packet dump is {Pkt_info_SAdd,Pkt_info_SAdd+Pkt_info_NAdd}.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pkdump_pks_Base                                                                  0x000900
#define cAf6Reg_upen_pkdump_pks(pktid)                                                      (0x000900+(pktid))
#define cAf6Reg_upen_pkdump_pks_WidthVal                                                                    32
#define cAf6Reg_upen_pkdump_pks_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: Pkt_Info_Len
BitField Type: RO
BitField Desc: Packet length 0x0: zero byte valid 0x1: one byte valid 0x2: two
byte valid, ...
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_pkdump_pks_Pkt_Info_Len_Bit_Start                                                         16
#define cAf6_upen_pkdump_pks_Pkt_Info_Len_Bit_End                                                           31
#define cAf6_upen_pkdump_pks_Pkt_Info_Len_Mask                                                       cBit31_16
#define cAf6_upen_pkdump_pks_Pkt_Info_Len_Shift                                                             16
#define cAf6_upen_pkdump_pks_Pkt_Info_Len_MaxVal                                                        0xffff
#define cAf6_upen_pkdump_pks_Pkt_Info_Len_MinVal                                                           0x0
#define cAf6_upen_pkdump_pks_Pkt_Info_Len_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Pkt_Info_Typ
BitField Type: RO
BitField Desc: Packet infor type 0x0: Packet 0x1: Word
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_pkdump_pks_Pkt_Info_Typ_Bit_Start                                                         12
#define cAf6_upen_pkdump_pks_Pkt_Info_Typ_Bit_End                                                           12
#define cAf6_upen_pkdump_pks_Pkt_Info_Typ_Mask                                                          cBit12
#define cAf6_upen_pkdump_pks_Pkt_Info_Typ_Shift                                                             12
#define cAf6_upen_pkdump_pks_Pkt_Info_Typ_MaxVal                                                           0x1
#define cAf6_upen_pkdump_pks_Pkt_Info_Typ_MinVal                                                           0x0
#define cAf6_upen_pkdump_pks_Pkt_Info_Typ_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Pkt_Info_SAdd
BitField Type: RO
BitField Desc: Start address of packet dump
BitField Bits: [10:00]
--------------------------------------*/
#define cAf6_upen_pkdump_pks_Pkt_Info_SAdd_Bit_Start                                                         0
#define cAf6_upen_pkdump_pks_Pkt_Info_SAdd_Bit_End                                                          10
#define cAf6_upen_pkdump_pks_Pkt_Info_SAdd_Mask                                                       cBit10_0
#define cAf6_upen_pkdump_pks_Pkt_Info_SAdd_Shift                                                             0
#define cAf6_upen_pkdump_pks_Pkt_Info_SAdd_MaxVal                                                        0x7ff
#define cAf6_upen_pkdump_pks_Pkt_Info_SAdd_MinVal                                                          0x0
#define cAf6_upen_pkdump_pks_Pkt_Info_SAdd_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : IgDump Pkt Data
Reg Addr   : 0x00_0C00
Reg Formula: 0x00_0C00 + $add_id*2 + dword_id
    Where  :
           + $add_id (0-1023): add_id
           + $dword_id(0-1)
Reg Desc   :
This register is used to read packet data, each address corresspond 2xdword (64 bits) data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pkdump_pkd_Base                                                                  0x000C00
#define cAf6Reg_upen_pkdump_pkd(addid, dwordid)                                 (0x000C00+(addid)*2+(dwordid))
#define cAf6Reg_upen_pkdump_pkd_WidthVal                                                                    32
#define cAf6Reg_upen_pkdump_pkd_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: Pkt_Dump_Data
BitField Type: RO
BitField Desc: Packet data, dword_id = 0 ~ data[63:32] dword_id = 1 ~
data[31:00]
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_pkdump_pkd_Pkt_Dump_Data_Bit_Start                                                         0
#define cAf6_upen_pkdump_pkd_Pkt_Dump_Data_Bit_End                                                          31
#define cAf6_upen_pkdump_pkd_Pkt_Dump_Data_Mask                                                       cBit31_0
#define cAf6_upen_pkdump_pkd_Pkt_Dump_Data_Shift                                                             0
#define cAf6_upen_pkdump_pkd_Pkt_Dump_Data_MaxVal                                                   0xffffffff
#define cAf6_upen_pkdump_pkd_Pkt_Dump_Data_MinVal                                                          0x0
#define cAf6_upen_pkdump_pkd_Pkt_Dump_Data_RstVal                                                          0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PPPLINKANALYZERREG_H_ */


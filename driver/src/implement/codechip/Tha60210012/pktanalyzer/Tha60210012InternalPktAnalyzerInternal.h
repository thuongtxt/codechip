/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : Tha60210012InternalPktAnalyzerInternal.h
 * 
 * Created Date: May 26, 2016
 *
 * Description : Packet analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012INTERNALPKTANALYZERINTERNAL_H_
#define _THA60210012INTERNALPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60210012InternalPktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer Tha60210012InternalPktAnalyzerNew(AtModulePktAnalyzer self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012INTERNALPKTANALYZERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60210012EthPortPktAnalyzer.c
 *
 * Created Date: May 9, 2016
 *
 * Description : Port Analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPtchService.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "../eth/Tha60210012EthPort.h"
#include "Tha60210012InternalPktAnalyzer.h"
#include "Tha60210012EthPortPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;
static tAtPktAnalyzerMethods m_AtPktAnalyzerOverride;

/* Supper Implementation */
static const tThaPktAnalyzerMethods *m_ThaPktAnalyzerMethods = NULL;
static const tAtPktAnalyzerMethods *m_AtPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartAddress(ThaPktAnalyzer self)
    {
    AtChannel port = AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    uint32 portId = AtChannelIdGet((AtChannel)port);
    uint32 portBase = portId * 0x10000;

    /* XFI Port */
    if (portId == 0)
        return (0x2F000 + portBase);

    /* Two XAUI port */
    portBase = (portId - 1) * 0x10000;
    return (0xA8F000 + portBase);
    }

static uint8 PortIdToAnalyze(ThaPktAnalyzer self)
    {
    AtEthPort port = (AtEthPort)AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    return (uint8)AtChannelIdGet((AtChannel)port);
    }

static uint32 XAUIPortHwDumpMode(eThaPktAnalyzerPktDumpMode pktDumpMode, uint8 portId)
    {
    AtUnused(portId);
    if (pktDumpMode == cThaPktAnalyzerDumpGbeTx)
        return 0x800;

    if (pktDumpMode == cThaPktAnalyzerDumpGbeRx)
        return 0x801;

    /* Invalid value */
    return 0x8;
    }

static uint32 EthPortHwDumpMode(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    uint8 portId = PortIdToAnalyze(self);

    /* XFI Port */
    if (portId == 0)
        return m_ThaPktAnalyzerMethods->EthPortHwDumpMode(self, pktDumpMode);

    /* XAUI Port */
    return XAUIPortHwDumpMode(pktDumpMode, portId);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    AtEthPort port = (AtEthPort)AtPktAnalyzerChannelGet(self);
    AtPacketFactory factory = AtPktAnalyzerPacketFactory(self);
    uint32 portId = AtChannelIdGet((AtChannel)port);

    /* XFI port */
    if (portId == 0)
        {
        if (!Tha60210012EthPortPtchIsEnabled(port))
            return AtPacketFactoryEthPacketCreate(factory, data, length, cAtPacketCacheModeCacheData, direction);

        return Tha60210012EthPacketWithPtchNew(data, length, cAtPacketCacheModeCacheData, direction);
        }

    /* XAUI port */
    return AtPacketFactoryXauiPacketCreate(factory, data, length, cAtPacketCacheModeCacheData, direction, portId);
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPktAnalyzerMethods = mMethodsGet(analyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, mMethodsGet(analyzer), sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, StartAddress);
        mMethodOverride(m_ThaPktAnalyzerOverride, EthPortHwDumpMode);
        }

    mMethodsSet(analyzer, &m_ThaPktAnalyzerOverride);
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideThaPktAnalyzer(self);
    OverrideAtPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012EthPortPktAnalyzer);
    }

AtPktAnalyzer Tha60210012EthPortPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011EthPortPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60210012EthPortPktAnalyzerNew(AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012EthPortPktAnalyzerObjectInit(newPktAnalyzer, port);
    }

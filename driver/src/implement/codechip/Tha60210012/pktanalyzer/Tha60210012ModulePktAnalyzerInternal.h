/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet Analyzer
 * 
 * File        : Tha60210012ModulePktAnalyzerInternal.h
 * 
 * Created Date: Dec 9, 2016
 *
 * Description : Packet Analyzer for 60210012
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEPKTANALYZERINTERNAL_H_
#define _THA60210012MODULEPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/pktanalyzer/Tha60210011ModulePktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModulePktAnalyzer
    {
    tTha60210011ModulePktAnalyzer super;

    /* Private data */
    AtPktAnalyzer internalPktAnalyzer; /* Lazy create */
    }tTha60210012ModulePktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePktAnalyzer Tha60210012ModulePktAnalyzerObjectInit(AtModulePktAnalyzer self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEPKTANALYZERINTERNAL_H_ */


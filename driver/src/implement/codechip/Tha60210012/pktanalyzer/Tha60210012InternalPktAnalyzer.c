/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtPktAnalyzer.c
 *
 * Created Date: May 16, 2016
 *
 * Description : Thalassa internal packet analyzer
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPacket.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/pktanalyzer/AtPktAnalyzerInternal.h"
#include "../../../../generic/pktanalyzer/AtPacketUtil.h"
#include "../../../default/pktanalyzer/ThaPacketFactory.h"
#include "../../../default/pktanalyzer/ThaPktAnalyzerInternal.h"
#include "../../../default/pktanalyzer/ThaPacketReaderInternal.h"
#include "../../Tha60210011/pda/Tha60210011ModulePda.h"
#include "../pda/Tha60210012ModulePdaReg.h"
#include "Tha60210012InternalPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaTxInternalPktMaxEntryId 2047

/* Register for internal packet analyzer */
#define cAf6Reg_PweDumpModeGlbCtrl   0x3
#define cAf6Reg_PweDumpModeMask      cBit3_2
#define cAf6Reg_PweDumpModeShift     2

#define cAf6Reg_PweDumpModeCtrl      0x6
#define cAf6Reg_PweDumpData          0x800

/* RX */
#define cAf6Reg_PweDumpRxEthFlowIdMask      cBit29_17
#define cAf6Reg_PweDumpRxEthFlowIdShift     17

#define cAf6Reg_PweDumpRxHdrFcsNumByteMask      cBit16_15
#define cAf6Reg_PweDumpRxHdrFcsNumByteShift     15

#define cAf6Reg_PweDumpRxHdrPadNumMask      cBit14_11
#define cAf6Reg_PweDumpRxHdrPadNumShift     11

#define cAf6Reg_PweDumpRxHdrFcsLanEnMask      cBit10
#define cAf6Reg_PweDumpRxHdrFcsLanEnShift     10

#define cAf6Reg_PweDumpRxHdrLenMask      cBit9_6
#define cAf6Reg_PweDumpRxHdrLenShift     6

#define cAf6Reg_PweDumpRxHdrEopMask      cBit5
#define cAf6Reg_PweDumpRxHdrEopShift     5

#define cAf6Reg_PweDumpRxHdrForwardTypeMask      cBit4_2
#define cAf6Reg_PweDumpRxHdrForwardTypeShift     2

#define cAf6Reg_PweDumpRxHdrInLpIdHiMask      cBit1_0
#define cAf6Reg_PweDumpRxHdrInLpIdHiShift     0

#define cAf6Reg_PweDumpRxHdrInLpIdLoMask      cBit31_30
#define cAf6Reg_PweDumpRxHdrInLpIdLoShift     30

#define cAf6Reg_PweDumpRxHdrPsnAdrMask      cBit29_24
#define cAf6Reg_PweDumpRxHdrPsnAdrShift     24

#define cAf6Reg_PweDumpRxHdrPktServiceMask      cBit23_21
#define cAf6Reg_PweDumpRxHdrPktServiceShift     21

#define cAf6Reg_PweDumpRxHdrPsnLenMask          cBit20_14
#define cAf6Reg_PweDumpRxHdrPsnLenShift         14

#define cAf6Reg_PweDumpRxHdrPktLenMask          cBit13_0
#define cAf6Reg_PweDumpRxHdrPktLenShift         0

/* TX */
#define cAf6Reg_PweDumpTxEthFlowIdMask      cBit21_10
#define cAf6Reg_PweDumpTxEthFlowIdShift		10

#define cAf6Reg_PweDumpTxHdrEopMask      cBit9
#define cAf6Reg_PweDumpTxHdrEopShift     9

#define cAf6Reg_PweDumpTxHdrPktLenHiMask        cBit8_0
#define cAf6Reg_PweDumpTxHdrPktLenHiShift       0

#define cAf6Reg_PweDumpTxHdrPktLenLoMask        cBit31_27
#define cAf6Reg_PweDumpTxHdrPktLenLoShift       27

#define cAf6Reg_PweDumpTxHdrOutLpIdMask      cBit26_23
#define cAf6Reg_PweDumpTxHdrOutLpIdShift     23

#define cAf6Reg_PweDumpTxHdrPsnAdrMask      cBit22_17
#define cAf6Reg_PweDumpTxHdrPsnAdrShift     17

#define cAf6Reg_PweDumpTxHdrPsnLenMask          cBit16_10
#define cAf6Reg_PweDumpTxHdrPsnLenShift         10

#define cAf6Reg_PweDumpTxHdrPktServiceMask      cBit9_7
#define cAf6Reg_PweDumpTxHdrPktServiceShift     7

#define cAf6Reg_PweDumpTxHdrForwardTypeMask      cBit6_4
#define cAf6Reg_PweDumpTxHdrForwardTypeShift     4

#define cAf6Reg_PweDumpTxHdrFragmentFcnMask         cBit3_0
#define cAf6Reg_PweDumpTxHdrFragmentFcnShift        0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210012InternalPktAnalyzer *)self)
#define mNumBytePerEntry(dumpMode) (dumpMode == cThaPktAnalyzerDumpClaToPda ? 4 : 2)
#define mMaxDataEntry(dumpMode) (dumpMode == cThaPktAnalyzerDumpClaToPda ? 256 : 2048)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012InternalPktAnalyzer
    {
    tThaEthPortPktAnalyzerV2 super;

    /* Private data */
    eThaPktAnalyzerPktDumpMode direction;
    AtModulePktAnalyzer module;
    uint16 startEntry;
    uint16 lastEntry;
    }tTha60210012InternalPktAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                m_AtObjectOverride;
static tAtPktAnalyzerMethods           m_AtPktAnalyzerOverride;
static tThaPktAnalyzerMethods          m_ThaPktAnalyzerOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;
static const tAtPktAnalyzerMethods  *m_AtPktAnalyzerMethods  = NULL;
static const tThaPktAnalyzerMethods *m_ThaPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePktAnalyzer ModuleGet(AtPktAnalyzer self)
    {
    return mThis(self)->module;
    }

static void LongRead(AtPktAnalyzer self, uint32 address, uint32 *longValue)
    {
    AtModule module = (AtModule)ModuleGet(self);
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet(module), AtModuleDefaultCoreGet(module));
    mModuleHwLongRead(module, address, longValue, cThaLongRegMaxSize, core);
    }

static void LongWrite(AtPktAnalyzer self, uint32 address, uint32 *longValue)
    {
    AtModule module = (AtModule)ModuleGet(self);
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet(module), AtModuleDefaultCoreGet(module));
    mModuleHwLongWrite(module, address, longValue, cThaLongRegMaxSize, core);
    }

static eBool IsTxPacketDumpMode(AtPktAnalyzer self)
    {
    if ((mThis(self)->direction == cThaPktAnalyzerDumpPlaOut)      ||
        (mThis(self)->direction == cThaPktAnalyzerDumpPweRx)       ||
        (mThis(self)->direction == cThaPktAnalyzerDumpPlaTxHeader) ||
        (mThis(self)->direction == cThaPktAnalyzerDumpPweRxHeader))
        return cAtTrue;

    return cAtFalse;
    }

static AtModule ModulePda(AtPktAnalyzer self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)mThis(self)->module);
    return AtDeviceModuleGet(device, cThaModulePda);
    }

static void Write(AtPktAnalyzer self, uint32 address, uint32 value)
    {
    mModuleHwWrite(ModuleGet(self), address, value);
    }

static uint8 PktDumpModeToHwVal(eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    if (pktDumpMode == cThaPktAnalyzerDumpPlaOut)      return 0;
    if (pktDumpMode == cThaPktAnalyzerDumpPweRx)       return 1;
    if (pktDumpMode == cThaPktAnalyzerDumpPlaTxHeader) return 2;
    if (pktDumpMode == cThaPktAnalyzerDumpPweRxHeader) return 3;

    return 0;
    }

static void DirectionSet(AtPktAnalyzer self, eThaPktAnalyzerPktDumpMode direction)
    {
    if ((direction == cThaPktAnalyzerDumpPlaOut)  ||
        (direction == cThaPktAnalyzerDumpPweRx)   ||
        (direction == cThaPktAnalyzerDumpPlaTxHeader) ||
		(direction == cThaPktAnalyzerDumpPweRxHeader))
        {
        uint32 longRegVal[cThaLongRegMaxSize], address;
        uint32 baseAddress = ThaPktAnalyzerBaseAddress((ThaPktAnalyzer)self);

        address = cAf6Reg_PweDumpModeGlbCtrl + baseAddress;
        LongRead(self, address, longRegVal);
        mRegFieldSet(longRegVal[0], cAf6Reg_PweDumpMode, PktDumpModeToHwVal(direction));
        LongWrite(self, address, longRegVal);

        /* Write 0 then 1 to trigger HW update */
        Write(self, cAf6Reg_PweDumpModeCtrl + baseAddress, 0);
        Write(self, cAf6Reg_PweDumpModeCtrl + baseAddress, 1);
        }

    mThis(self)->direction = direction;
    }

static eThaPktAnalyzerPktDumpMode DirectionGet(AtPktAnalyzer self)
    {
    return mThis(self)->direction;
    }

static uint32 PdaRead(AtPktAnalyzer self, uint32 address)
    {
    return mModuleHwRead(ModulePda(self), address);
    }

static void PdaLongRead(AtPktAnalyzer self, uint32 address, uint32 *longValue)
    {
    AtModule module = (AtModule)ModulePda(self);
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet(module), AtModuleDefaultCoreGet(module));
    mModuleHwLongRead(module, address, longValue, cThaLongRegMaxSize, core);
    }

static uint32 SevIdCtrlAddress(AtPktAnalyzer self)
    {
    eThaPktAnalyzerPktDumpMode direction = DirectionGet(self);

    if (direction == cThaPktAnalyzerDumpClaToPda)
        return cAf6Reg_PdaDumpInputSevIdCtrl;

    if (direction == cThaPktAnalyzerDumpPdaToMap)
        return cAf6Reg_PdaDumpOutputSevIdCtrl;

    return cInvalidUint32;
    }

static uint32 AddressWithOffset(AtPktAnalyzer self, uint32 offset)
    {
    return offset + ThaPktAnalyzerBaseAddress((ThaPktAnalyzer)self);
    }

static void ServiceIdSet(AtPktAnalyzer self, uint32 serviceId)
    {
    Write(self, AddressWithOffset(self, SevIdCtrlAddress(self)), serviceId);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    return AtPacketFactoryRawPacketCreate(AtPktAnalyzerPacketFactory(self), data, length, cAtPacketCacheModeCacheData, direction);
    }

static void Recapture(AtPktAnalyzer self)
    {
    AtUnused(self);
    return;
    }

static eBool DumpModeIsSupported(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    eThaPktAnalyzerPktDumpMode direction = DirectionGet((AtPktAnalyzer)self);
    AtUnused(pktDumpMode);

    if ((direction == cThaPktAnalyzerDumpClaToPda)  ||
        (direction == cThaPktAnalyzerDumpPdaToMap)  ||
        (direction == cThaPktAnalyzerDumpPlaOut)    ||
        (direction == cThaPktAnalyzerDumpPweRx)     ||
        (direction == cThaPktAnalyzerDumpPlaTxHeader) ||
        (direction == cThaPktAnalyzerDumpPweRxHeader))
        return cAtTrue;

    return cAtFalse;
    }

static void DumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    AtUnused(self);
    AtUnused(pktDumpMode);
    }

static void AllRxPacketsRead(ThaPktAnalyzer self)
	{
	ThaPacketReaderPacketRead(ThaPktAnalyzerRxPacketReaderGet((ThaPktAnalyzer)self), DirectionGet((AtPktAnalyzer)self));
	}
	    
static void GetDataEntry(AtPktAnalyzer self, uint32 address, uint32 *longValue)
    {
    AtPktAnalyzer analyzer = (AtPktAnalyzer)self;
    eThaPktAnalyzerPktDumpMode direction = DirectionGet(analyzer);

    if (direction == cThaPktAnalyzerDumpClaToPda)
        {
        PdaLongRead(analyzer, address, longValue);
        return;
        }

    if (direction == cThaPktAnalyzerDumpPdaToMap)
        {
        longValue[1] = PdaRead(analyzer, address);
        longValue[0] = longValue[1] << 16;
        return;
        }
    }

static uint32 FirstAddressOfBuffer(ThaPktAnalyzer self)
	{
	uint32 address = (DirectionGet((AtPktAnalyzer)self) == cThaPktAnalyzerDumpClaToPda) ? cAf6Reg_PdaDumpInDatStartReg : cAf6Reg_PdaDumpOutDatStartReg;
	return (ThaPktAnalyzerBaseAddress(self) + address);
	}

static uint32 LastAddressOfBuffer(ThaPktAnalyzer self)
	{
	uint32 address = (DirectionGet((AtPktAnalyzer)self) == cThaPktAnalyzerDumpClaToPda) ? cAf6Reg_PdaDumpInDatStopReg : cAf6Reg_PdaDumpOutDatStopReg;
	return (ThaPktAnalyzerBaseAddress(self) + address);
	}

static eBool IsStartOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
	{
	uint32 longRegVal[cThaLongRegMaxSize];
	AtPktAnalyzer analyzer = (AtPktAnalyzer)self;
	uint32 sopMask = (DirectionGet(analyzer) == cThaPktAnalyzerDumpClaToPda) ? cAf6Reg_PdaDumpInSopMask : cAf6Reg_PdaDumpOutSopMask;
	AtUnused(data);
    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
	GetDataEntry(analyzer, address, longRegVal);
	return (longRegVal[mMetaDwordIndex(self)] & sopMask) ? cAtTrue : cAtFalse;
	}

static eBool IsEndOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
	{
	uint32 longRegVal[cThaLongRegMaxSize];
	AtPktAnalyzer analyzer = (AtPktAnalyzer)self;
	uint32 eopMask = (DirectionGet(analyzer) == cThaPktAnalyzerDumpClaToPda) ? cAf6Reg_PdaDumpInEopMask : cAf6Reg_PdaDumpOutEopMask;
	AtUnused(data);
	AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
	GetDataEntry(analyzer, address, longRegVal);
	return (longRegVal[mMetaDwordIndex(self)] & eopMask) ? cAtTrue : cAtFalse;
	}

static uint8 EntryRead(ThaPktAnalyzer self, uint32 address, uint32 *data)
	{
	uint8 numBytes;
	AtPktAnalyzer analyzer = (AtPktAnalyzer)self;
	uint32 nobMask = (DirectionGet(analyzer) == cThaPktAnalyzerDumpClaToPda) ? cAf6Reg_PdaDumpInNobMask : cAf6Reg_PdaDumpOutNobMask;
	uint32 nobShift = (DirectionGet(analyzer) == cThaPktAnalyzerDumpClaToPda) ? cAf6Reg_PdaDumpInNobShift : cAf6Reg_PdaDumpOutNobShift;
    AtOsalMemInit(data, 0, sizeof(data));

	GetDataEntry(analyzer, address, data);
	mFieldGet(data[mMetaDwordIndex(self)], nobMask, nobShift, uint8, &numBytes);
	numBytes++;

	return numBytes;
	}

static uint32 StartAddress(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 PartOffset(ThaPktAnalyzer self)
    {
    AtUnused(self);

    if (IsTxPacketDumpMode((AtPktAnalyzer)self))
        return 0xA00000;

    return ThaModulePdaBaseAddress((ThaModulePda)ModulePda((AtPktAnalyzer)self));
    }

static eBool JustDisplayRawPacket(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint16 StartEntryGet(AtPktAnalyzer self)
    {
    return mThis(self)->startEntry;
    }

static uint16 LastEntryGet(AtPktAnalyzer self)
    {
    return mThis(self)->lastEntry;
    }

static void DisplayData(uint8 *buffer, uint32 length)
    {
    uint32 byte_i;

    AtPrintc(cSevNormal, "\r\n");
    for (byte_i = 0; byte_i < length; byte_i++)
        {
        AtPrintc(cSevNormal,"%02X ", buffer[byte_i]);

        if ((byte_i % 32) == 31)
            AtPrintc(cSevNormal, "\r\n");
        else if (byte_i % 4 == 3)
            AtPrintc(cSevNormal, " ");
        }

    AtPrintc(cSevNormal, "\r\n");
    }

static eAtRet TxPktPlaToPweAnalyze(AtPktAnalyzer self)
    {
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address, bufferSize;
    uint16 entry_i, dword_i;
    uint8 *pBuffer = AtPktAnalyzerBufferGet(self, &bufferSize);
    uint32 length = 0;
    uint16 start_entry = StartEntryGet(self);
    uint16 last_entry  = LastEntryGet(self);
    uint32 baseAddress = ThaPktAnalyzerBaseAddress((ThaPktAnalyzer)self);

    for (entry_i = start_entry; entry_i <= last_entry; entry_i++)
        {
        address = cAf6Reg_PweDumpData + baseAddress + entry_i;
        LongRead(self, address, longReg);

        for (dword_i = 4; dword_i > 0; dword_i--)
            {
            uint8 byte_i = 0;
            while (byte_i < 4)
                {
                mFieldGet(longReg[dword_i - 1],
                          cThaDebugDumPktByteGetMask(byte_i),
                          cThaDebugDumPktByteGetShift(byte_i),
                          uint8, &(pBuffer[length++]));
                byte_i++;
                }
            }

        if (length == bufferSize)
            {
            DisplayData(pBuffer, length);
            length = 0;
            }
        }

    DisplayData(pBuffer, length);
    AtPrintc(cSevNormal, "* Address offset: %d-%d \n", start_entry, last_entry);
    AtPrintc(cSevNormal, "* Data length: %d bytes\r\n", (last_entry - start_entry + 1) * 16);

    return cAtOk;
    }

static eAtRet RxPweHeaderAnalyze(AtPktAnalyzer self, uint16 entryId, tThaInternalPlaPacketHeader *header)
    {
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address;

    /* Get data */
    address = cAf6Reg_PweDumpData + ThaPktAnalyzerBaseAddress((ThaPktAnalyzer)self) + entryId;
    LongRead(self, address, longReg);

    header->ethFlowId   = (uint8)mRegField(longReg[1], cAf6Reg_PweDumpRxEthFlowId);
    header->numFcsByte  = (uint8)mRegField(longReg[1], cAf6Reg_PweDumpRxHdrFcsNumByte);
    header->numPadding  = (uint8)mRegField(longReg[1], cAf6Reg_PweDumpRxHdrPadNum);
    header->fcsEn       = (uint8)mRegField(longReg[1], cAf6Reg_PweDumpRxHdrFcsLanEn);
    header->headerLen   = (uint8)mRegField(longReg[1], cAf6Reg_PweDumpRxHdrLen);
    header->eop         = (uint8)mRegField(longReg[1], cAf6Reg_PweDumpRxHdrEop);
    header->forwardType = (uint8)mRegField(longReg[1], cAf6Reg_PweDumpRxHdrForwardType);
    header->networkProtoId  = (uint8)((mRegField(longReg[1], cAf6Reg_PweDumpRxHdrInLpIdHi) << 2) | mRegField(longReg[0], cAf6Reg_PweDumpRxHdrInLpIdLo));
    header->psnAddress  = (uint8)mRegField(longReg[0], cAf6Reg_PweDumpRxHdrPsnAdr);
    header->pktService  = (uint8)mRegField(longReg[0], cAf6Reg_PweDumpRxHdrPktService);
    header->psnLen      = (uint8)mRegField(longReg[0], cAf6Reg_PweDumpRxHdrPsnLen);
    header->pktLen      = (uint16)mRegField(longReg[0], cAf6Reg_PweDumpRxHdrPktLen);

    return cAtOk;
    }

static eAtRet TxPktPlaHeaderAnalyze(AtPktAnalyzer self, uint16 entryId, tThaInternalPlaPacketHeader *header)
    {
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address;

    /* Get data */
    address = cAf6Reg_PweDumpData + ThaPktAnalyzerBaseAddress((ThaPktAnalyzer)self) + entryId;
    LongRead(self, address, longReg);

    header->ethFlowId   = (uint8)mRegField(longReg[1], cAf6Reg_PweDumpTxEthFlowId);
    header->eop         = (uint8)mRegField(longReg[1], cAf6Reg_PweDumpTxHdrEop);
    header->pktLen      = (uint16)((mRegField(longReg[1], cAf6Reg_PweDumpTxHdrPktLenHi) << 5) | mRegField(longReg[0], cAf6Reg_PweDumpTxHdrPktLenLo));
    header->networkProtoId  = (uint8)mRegField(longReg[0], cAf6Reg_PweDumpTxHdrOutLpId);
    header->psnAddress  = (uint8)mRegField(longReg[0], cAf6Reg_PweDumpTxHdrPsnAdr);
    header->psnLen      = (uint8)mRegField(longReg[0], cAf6Reg_PweDumpTxHdrPsnLen);
    header->pktService  = (uint8)mRegField(longReg[0], cAf6Reg_PweDumpTxHdrPktService);
    header->forwardType = (uint8)mRegField(longReg[0], cAf6Reg_PweDumpTxHdrForwardType);
    header->fragmentFcn = (uint8)mRegField(longReg[0], cAf6Reg_PweDumpTxHdrFragmentFcn);

    return cAtOk;
    }

static eAtRet TxPacketAnalyzer(AtPktAnalyzer self, eThaPktAnalyzerPktDumpMode direction)
    {
    if ((direction == cThaPktAnalyzerDumpPlaOut) ||
        (direction == cThaPktAnalyzerDumpPweRx))
        return TxPktPlaToPweAnalyze(self);

    return cAtOk;
    }

static void DefaultSet(AtPktAnalyzer self)
    {
    const uint16 cDefaultStartEntry = 0;
    const uint16 cDefaultLastEntry = 127;

    DirectionSet(self, cThaPktAnalyzerDumpClaToPda);
    Tha60210012InternalPktAnalyzerTxStartEntrySet(self, cDefaultStartEntry);
    Tha60210012InternalPktAnalyzerTxLastEntrySet(self, cDefaultLastEntry);
    }

static void Init(AtPktAnalyzer self)
    {
    m_AtPktAnalyzerMethods->Init(self);
    DefaultSet(self);
    }

static eBool SkipLastInCompletePacket(ThaPktAnalyzer self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210012InternalPktAnalyzer* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(direction);
    mEncodeObjectDescription(module);
    mEncodeUInt(startEntry);
    mEncodeUInt(lastEntry);
    }

static void OverrideAtObject(AtPktAnalyzer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPktAnalyzerMethods = mMethodsGet(analyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, m_ThaPktAnalyzerMethods, sizeof(m_ThaPktAnalyzerOverride));
        mMethodOverride(m_ThaPktAnalyzerOverride, StartAddress);
        mMethodOverride(m_ThaPktAnalyzerOverride, PartOffset);
        mMethodOverride(m_ThaPktAnalyzerOverride, JustDisplayRawPacket);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeIsSupported);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeSet);
        mMethodOverride(m_ThaPktAnalyzerOverride, FirstAddressOfBuffer);
        mMethodOverride(m_ThaPktAnalyzerOverride, LastAddressOfBuffer);
		mMethodOverride(m_ThaPktAnalyzerOverride, IsStartOfPacket);
		mMethodOverride(m_ThaPktAnalyzerOverride, IsEndOfPacket);
		mMethodOverride(m_ThaPktAnalyzerOverride, EntryRead);
		mMethodOverride(m_ThaPktAnalyzerOverride, AllRxPacketsRead);
		mMethodOverride(m_ThaPktAnalyzerOverride, SkipLastInCompletePacket);
        }

    mMethodsSet(analyzer, &m_ThaPktAnalyzerOverride);
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    AtPktAnalyzer pktAnalyzer = (AtPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(pktAnalyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(m_AtPktAnalyzerOverride));
        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        mMethodOverride(m_AtPktAnalyzerOverride, Recapture);
        mMethodOverride(m_AtPktAnalyzerOverride, Init);
        }

    mMethodsSet(pktAnalyzer, &m_AtPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtObject(self);
    OverrideAtPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012InternalPktAnalyzer);
    }

static AtPktAnalyzer ObjectInit(AtPktAnalyzer self, AtModulePktAnalyzer module, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    AtUnused(port);

    /* Super constructor */
    if (ThaEthPortPktAnalyzerV2ObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->module = module;

    return self;
    }

AtPktAnalyzer Tha60210012InternalPktAnalyzerNew(AtModulePktAnalyzer module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPktAnalyzer, module, NULL);
    }

void Tha60210012InternalPktAnalyzerServiceIdSet(AtPktAnalyzer self, uint32 serviceId)
    {
    if (self)
        ServiceIdSet(self, serviceId);
    }

void Tha60210012InternalPktAnalyzerDirectionSet(AtPktAnalyzer self, eThaPktAnalyzerPktDumpMode direction)
    {
    if (self)
        DirectionSet(self, direction);
    }

eAtRet Tha60210012InternalPktAnalyzerTxStartEntrySet(AtPktAnalyzer self, uint16 startEntry)
    {
    if ((startEntry > cThaTxInternalPktMaxEntryId) || (startEntry > LastEntryGet(self)))
        return cAtErrorInvlParm;

    mThis(self)->startEntry = startEntry;
    return cAtOk;
    }

eAtRet Tha60210012InternalPktAnalyzerTxLastEntrySet(AtPktAnalyzer self, uint16 lastEntry)
    {
    if ((lastEntry > cThaTxInternalPktMaxEntryId) || (lastEntry < StartEntryGet(self)))
        return cAtErrorInvlParm;

    mThis(self)->lastEntry = lastEntry;
    return cAtOk;
    }

uint16 Tha60210012InternalPktAnalyzerTxStartEntryGet(AtPktAnalyzer self)
    {
    return StartEntryGet(self);
    }

uint16 Tha60210012InternalPktAnalyzerTxLastEntryGet(AtPktAnalyzer self)
    {
    return LastEntryGet(self);
    }

eAtRet Tha60210012InternalTxPktAnalyzer(AtPktAnalyzer self)
    {
    if (self)
        return TxPacketAnalyzer(self, DirectionGet(self));

    return cAtOk;
    }

eAtRet Tha60210012InternalRxPwePktHeaderAnalyze(AtPktAnalyzer self, uint16 entryId, tThaInternalPlaPacketHeader *header)
    {
    if (self)
       return RxPweHeaderAnalyze(self, entryId, header);

    return cAtOk;
    }

eAtRet Tha60210012InternalTxPlaPktHeaderAnalyze(AtPktAnalyzer self, uint16 entryId, tThaInternalPlaPacketHeader *header)
    {
    if (self)
       return TxPktPlaHeaderAnalyze(self, entryId, header);

    return cAtOk;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60210012PppLinkPktAnalyzer.c
 *
 * Created Date: Jul 25, 2016
 *
 * Description : PPP Link packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/pktanalyzer/ThaPppLinkPktAnalyzerInternal.h"
#include "../../../default/encap/dynamic/ThaDynamicHdlcChannel.h"
#include "../../../default/encap/ThaHdlcChannel.h"
#include "../../Tha60210011/pda/Tha60210011ModulePda.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "../pda/Tha60210012ModulePda.h"
#include "../pda/Tha60210012ModulePdaReg.h"
#include "../encap/Tha60210012ModuleEncap.h"
#include "Tha60210012PppLinkAnalyzerReg.h"

/*--------------------------- Define -----------------------------------------*/
/* Packet meta data */
#define cNumBytesMask       cBit1_0
#define cNumBytesShift      0
#define cEndOfPacketMask    cBit2
#define cStartOfPacketMask  cBit3

#define cStateFindStartOfPacket 0
#define cStateReading           1
#define cStateEndOfPacket       2

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210012PppLinkPktAnalyzer)self)
/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012PppLinkPktAnalyzer
    {
    tThaPppLinkPktAnalyzer super;

    /* Private data */
    eThaPktAnalyzerPktDumpMode dumpMode;
    }tTha60210012PppLinkPktAnalyzer;

typedef struct tTha60210012PppLinkPktAnalyzer * Tha60210012PppLinkPktAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtPktAnalyzerMethods  m_AtPktAnalyzerOverride;
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;
static const tAtPktAnalyzerMethods  *m_AtPktAnalyzerMethods  = NULL;
static const tThaPktAnalyzerMethods *m_ThaPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Init(AtPktAnalyzer self)
    {
    /* Super */
    m_AtPktAnalyzerMethods->Init(self);
    AtPktAnalyzerPacketDumpTypeSet(self, cAtPktAnalyzerDumpAllPkt);
    mMethodsGet(self)->Recapture(self);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    AtPacketFactory factory = AtPktAnalyzerPacketFactory(self);
    return AtPacketFactoryHdlcPacketCreate(factory, data, length, cAtPacketCacheModeCacheData, direction);
    }

static AtModule PdaModule(ThaPktAnalyzer self)
    {
    AtDevice device = AtModuleDeviceGet(AtChannelModuleGet(AtPktAnalyzerChannelGet((AtPktAnalyzer)self)));
    return AtDeviceModuleGet(device, cThaModulePda);
    }

static uint32 FirstAddressOfBuffer(ThaPktAnalyzer self)
	{
	return (ThaPktAnalyzerBaseAddress(self) + cAf6Reg_PdaDumpOutDatStartReg);
	}

static uint32 LastAddressOfBuffer(ThaPktAnalyzer self)
	{
	return (ThaPktAnalyzerBaseAddress(self) + cAf6Reg_PdaDumpOutDatStopReg);
	}

static eBool IsStartOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
	{
	uint32 longRegVal[cThaLongRegMaxSize];
	uint8 metaDwIndex = mMetaDwordIndex(self);
	AtUnused(data);
	longRegVal[metaDwIndex] = mModuleHwRead(PdaModule(self), address);
	return (longRegVal[metaDwIndex] & cAf6Reg_PdaDumpOutSopMask) ? cAtTrue : cAtFalse;
	}

static eBool IsEndOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
	{
	uint32 longRegVal[cThaLongRegMaxSize];
	uint8 metaDwIndex = mMetaDwordIndex(self);
	AtUnused(data);
	longRegVal[metaDwIndex] = mModuleHwRead(PdaModule(self), address);
	return (longRegVal[metaDwIndex] & cAf6Reg_PdaDumpOutEopMask) ? cAtTrue : cAtFalse;
	}

static uint8 EntryRead(ThaPktAnalyzer self, uint32 address, uint32 *data)
	{
	data[1] = mModuleHwRead(PdaModule(self), address);
	data[0] = data[1] << 16;

	return (uint8)(mRegField(data[mMetaDwordIndex(self)], cAf6Reg_PdaDumpOutNob) + 1);
	}

static uint32 PartOffset(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 NumPacketInBuffer(ThaPktAnalyzer self)
    {
    uint32 regAddr = cAf6Reg_upen_pkdump_pkc_Base + ThaPktAnalyzerBaseAddress(self);
    uint32 regVal = ThaPktAnalyzerRead(self, regAddr);
    return mRegField(regVal, cAf6_upen_pkdump_pkc_Dump_Pkts_);
    }

static void PacketDumpTypeSet(AtPktAnalyzer self, eAtPktAnalyzerPktDumpType packetDumpType)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;
    uint32 regAddr = cAf6Reg_upen_pkdump_ctl_Base + ThaPktAnalyzerBaseAddress(analyzer);
    uint32 regVal = ThaPktAnalyzerRead(analyzer, regAddr);

    mRegFieldSet(regVal, cAf6_upen_pkdump_ctl_Dump_Type_, packetDumpType);
    ThaPktAnalyzerWrite(analyzer, regAddr, regVal);
    }

static uint8 PacketLengthModeSw2Hw(uint32 packetLength)
    {
    if (packetLength == 0)    return 0; /* Full packet */
    if (packetLength <= 32)   return 1;
    if (packetLength <= 64)   return 2;
    if (packetLength <= 128)  return 3;
    if (packetLength <= 256)  return 4;
    if (packetLength <= 512)  return 5;
    if (packetLength <= 1024) return 6;

    /* Default dump 2048 bytes */
    return 7;
    }

static void PacketLengthSet(AtPktAnalyzer self, uint32 packetLength)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;
    uint32 regAddr = cAf6Reg_upen_pkdump_ctl_Base + ThaPktAnalyzerBaseAddress(analyzer);
    uint32 regVal = ThaPktAnalyzerRead(analyzer, regAddr);

    mRegFieldSet(regVal, cAf6_upen_pkdump_ctl_Dump_Psize_, PacketLengthModeSw2Hw(packetLength));
    ThaPktAnalyzerWrite(analyzer, regAddr,  regVal);
    }

static uint32 PacketLenghthGet(ThaPktAnalyzer self, uint32 packetId, uint16 *startAddress)
    {
    uint32 offset    = packetId + ThaPktAnalyzerBaseAddress(self);
    uint32 regValue  = ThaPktAnalyzerRead(self, cAf6Reg_upen_pkdump_pks_Base + offset);

    *startAddress = (uint16)mRegField(regValue, cAf6_upen_pkdump_pks_Pkt_Info_SAdd_);
    return mRegField(regValue, cAf6_upen_pkdump_pks_Pkt_Info_Len_);
    }

static uint32 PacketDumpDataRegister(ThaPktAnalyzer self, uint32 address_i)
    {
    AtUnused(self);
    return (cAf6Reg_upen_pkdump_pkd_Base + address_i);
    }

static void DumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    uint32 regAddr, regVal;

    mThis(self)->dumpMode = pktDumpMode;
    if (pktDumpMode != cThaPktAnalyzerDumpDecToMpig)
        return;

    regAddr = cAf6Reg_upen_pkdump_ctl_Base + ThaPktAnalyzerBaseAddress(self);
    regVal = ThaPktAnalyzerRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_pkdump_ctl_Dump_Psize_, 0); /* Dump full packet */
    mRegFieldSet(regVal, cAf6_upen_pkdump_ctl_Dump_Keep_, 0);  /* Clear buffer */
    mRegFieldSet(regVal, cAf6_upen_pkdump_ctl_Dump_Roll_, 0);  /* Stop dump when buffer full */
    mRegFieldSet(regVal, cAf6_upen_pkdump_ctl_Dump_Type_, 0);  /* Dump all packet */
    ThaPktAnalyzerWrite(self, regAddr, regVal);
    }

static uint32 DumpModeGet(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return mThis(self)->dumpMode ;
    }

static eBool DumpModeIsSupported(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
	AtUnused(self);
	if ((pktDumpMode == cThaPktAnalyzerDumpMpegToEnc) ||
		(pktDumpMode == cThaPktAnalyzerDumpDecToMpig))
		return cAtTrue;
    return cAtFalse;
    }

static uint32 StartAddress(ThaPktAnalyzer self)
    {
    if (DumpModeGet(self) == cThaPktAnalyzerDumpMpegToEnc)
    	return ThaModulePdaBaseAddress((ThaModulePda)PdaModule(self));

    return Tha60210012ModuleEthFlowLookupBaseAddress();
    }

static void RxChannelIdSet(ThaPktAnalyzer self)
    {
    AtChannel link = AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    uint32 regAddress, regValue;

    /* Select channel ID */
    regAddress = cAf6Reg_upen_pkdump_ctl_Base + ThaPktAnalyzerBaseAddress(self);
    regValue   = ThaPktAnalyzerRead(self, regAddress);
    mRegFieldSet(regValue, cAf6_upen_pkdump_ctl_Dump_Sel_, 1); /* Select run ppp link */
    ThaPktAnalyzerWrite(self, regAddress, regValue);

    regAddress = cAf6Reg_upen_pkdump_cid_Base + ThaPktAnalyzerBaseAddress(self);
    regValue   = ThaPktAnalyzerRead(self, regAddress);
    mRegFieldSet(regValue, cAf6_upen_pkdump_cid_Dump_CID_, AtChannelHwIdGet(link));
    ThaPktAnalyzerWrite(self, regAddress, regValue);
    }

static Tha60210012ModuleEncap ModuleEncap(ThaPktAnalyzer self)
    {
    AtDevice device = AtModuleDeviceGet(AtChannelModuleGet(AtPktAnalyzerChannelGet((AtPktAnalyzer)self)));
    return (Tha60210012ModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static uint32 ServiceIdHoOffset(ThaPktAnalyzer self, uint8 slice, uint32 hwIdInSlice)
    {
    uint32 offset = 0x1100;

    if (Tha60210012ModuleEncapRegistersNeedShiftUp(ModuleEncap(self)))
        offset = 0x2100;

    return (uint32)(slice * 0x40UL + hwIdInSlice + offset);
    }

static uint32 ServiceIdLoOffset(ThaPktAnalyzer self, uint8 slice, uint32 hwIdInSlice)
    {
    uint8 oc48Id = slice / 2;
    uint8 oc24SliceId = slice % 2;
    uint32 oc48Offset = 0x800UL;
    uint32 oc24Offset = 0x400UL;

    if (Tha60210012ModuleEncapRegistersNeedShiftUp(ModuleEncap(self)))
        {
        oc48Offset = 0x1000UL;
        oc24Offset = 0x800UL;
        }

    return (uint32)((oc48Id * oc48Offset) + (oc24SliceId * oc24Offset) + hwIdInSlice);
    }

static void TxChannelIdSet(ThaPktAnalyzer self)
	{
    uint32 address, serviceId;
    AtHdlcLink link = (AtHdlcLink)AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    AtHdlcChannel hdlcChannel = AtHdlcLinkHdlcChannelGet(link);
    uint8 slice = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)hdlcChannel);
    uint32 hwIdInSlice = AtChannelHwIdGet((AtChannel)hdlcChannel);

    serviceId = 0;
    if (ThaHdlcChannelCircuitIsHo((ThaHdlcChannel)hdlcChannel))
        serviceId = ServiceIdHoOffset(self, slice, hwIdInSlice);
    else
        serviceId = ServiceIdLoOffset(self, slice, hwIdInSlice);

    address = ThaModulePdaBaseAddress((ThaModulePda)PdaModule(self)) + cAf6Reg_PdaDumpOutputSevIdCtrl;
    mModuleHwWrite(PdaModule(self), address, serviceId);
	}

static void ChannelIdSet(ThaPktAnalyzer self)
    {
    TxChannelIdSet(self);
    RxChannelIdSet(self);
    }

static void PppPacketModeSet(AtPktAnalyzer self, eAtPktAnalyzerPppPktType packetMode)
    {
    AtUnused(self);
    AtUnused(packetMode);
    }

static uint8 PortIdToAnalyze(ThaPktAnalyzer self)
    {
    AtChannel link = AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    return (uint8)AtChannelHwIdGet(link);
    }

static uint8 CurrentPortId(ThaPktAnalyzer self)
    {
    uint32 regAddress = cAf6Reg_upen_pkdump_cid_Base + ThaPktAnalyzerBaseAddress(self);
    uint32 regValue   = ThaPktAnalyzerRead(self, regAddress);
    return (uint8)mRegField(regValue, cAf6_upen_pkdump_cid_Dump_CID_);
    }

static eBool ShouldRecapture(ThaPktAnalyzer self, uint8 newPktDumpMode, uint8 currentDumpMode)
    {
    if (PortIdToAnalyze(self) != CurrentPortId(self))
        return cAtTrue;

    if ((currentDumpMode != newPktDumpMode) && (currentDumpMode != cThaPktAnalyzerDumpUnknown))
        return cAtTrue;

    return cAtFalse;
    }

static void Recapture(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;
    uint32 regAddr = cAf6Reg_upen_pkdump_ctl_Base + ThaPktAnalyzerBaseAddress(analyzer);
    uint32 regVal = ThaPktAnalyzerRead(analyzer, regAddr);

    mRegFieldSet(regVal, cAf6_upen_pkdump_ctl_Dump_Keep_, 0);  /* Clear buffer */
    ThaPktAnalyzerWrite(analyzer, regAddr, regVal);
    }

static ThaPacketReader TxPacketReaderCreate(ThaPktAnalyzer self)
	{
	return ThaPacketReaderV2New((AtPktAnalyzer)self);
	}

static void AllTxPacketsRead(ThaPktAnalyzer self)
	{
	TxChannelIdSet(self);
	m_ThaPktAnalyzerMethods->AllTxPacketsRead(self);
	}

static eBool SkipLastInCompletePacket(ThaPktAnalyzer self)
	{
	AtUnused(self);
	return cAtFalse;
	}

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210012PppLinkPktAnalyzer* object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(dumpMode);
    }

static void OverrideAtObject(AtPktAnalyzer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer pktAnalyzer = (ThaPktAnalyzer)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPktAnalyzerMethods = mMethodsGet(pktAnalyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, m_ThaPktAnalyzerMethods, sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, PartOffset);
        mMethodOverride(m_ThaPktAnalyzerOverride, NumPacketInBuffer);
        mMethodOverride(m_ThaPktAnalyzerOverride, PacketLenghthGet);
        mMethodOverride(m_ThaPktAnalyzerOverride, PacketDumpDataRegister);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeGet);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeSet);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeIsSupported);
        mMethodOverride(m_ThaPktAnalyzerOverride, StartAddress);
        mMethodOverride(m_ThaPktAnalyzerOverride, ChannelIdSet);
        mMethodOverride(m_ThaPktAnalyzerOverride, ShouldRecapture);
        mMethodOverride(m_ThaPktAnalyzerOverride, FirstAddressOfBuffer);
		mMethodOverride(m_ThaPktAnalyzerOverride, LastAddressOfBuffer);
		mMethodOverride(m_ThaPktAnalyzerOverride, IsStartOfPacket);
		mMethodOverride(m_ThaPktAnalyzerOverride, IsEndOfPacket);
		mMethodOverride(m_ThaPktAnalyzerOverride, EntryRead);
		mMethodOverride(m_ThaPktAnalyzerOverride, TxPacketReaderCreate);
		mMethodOverride(m_ThaPktAnalyzerOverride, AllTxPacketsRead);
		mMethodOverride(m_ThaPktAnalyzerOverride, SkipLastInCompletePacket);
        }

    mMethodsSet(pktAnalyzer, &m_ThaPktAnalyzerOverride);
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(m_AtPktAnalyzerOverride));
        mMethodOverride(m_AtPktAnalyzerOverride, Init);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketDumpTypeSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketLengthSet);
        mMethodOverride(m_AtPktAnalyzerOverride, PppPacketModeSet);
        mMethodOverride(m_AtPktAnalyzerOverride, Recapture);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtObject(self);
    OverrideAtPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012PppLinkPktAnalyzer);
    }

static AtPktAnalyzer ObjectInit(AtPktAnalyzer self, AtPppLink link)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPppLinkPktAnalyzerObjectInit(self, link) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60210012PppLinkPktAnalyzerNew(AtPppLink link)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPktAnalyzer, link);
    }

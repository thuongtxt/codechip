/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet Analyzer
 * 
 * File        : Tha60210012EthPacketWithPtchInternal.h
 * 
 * Created Date: Feb 6, 2017
 *
 * Description : 60210012 Ethernet packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012ETHPACKETWITHPTCHINTERNAL_H_
#define _THA60210012ETHPACKETWITHPTCHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/pktanalyzer/AtEthPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012EthPacketWithPtch
    {
    tAtEthPacket super;
    eAtPktAnalyzerDirection direction;
    }tTha60210012EthPacketWithPtch;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket Tha60210012EthPacketWithPtchObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPktAnalyzerDirection direction);

#endif /* _THA60210012ETHPACKETWITHPTCHINTERNAL_H_ */


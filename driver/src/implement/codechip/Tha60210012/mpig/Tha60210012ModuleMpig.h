/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPIG
 * 
 * File        : Tha60210012ModuleMpig.h
 * 
 * Created Date: Mar 14, 2016
 *
 * Description : MPIG internal module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef THA60210012MODULEMPIG_H_
#define THA60210012MODULEMPIG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cTha60210012MpigPktEnqueueCounterNum           8
#define cTha60210012MpigByteEnqueueCountersNum         8
#define cTha60210012MpigByteDequeueCountersNum         3
#define cTha60210012MpigDdrAccessQueueCountersNum      16

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleMpig * Tha60210012ModuleMpig;

typedef enum eThaMpigQueuePriority
    {
    cThaMpigQueuePriorityUnknown,
    cThaMpigQueuePriorityHighest,
    cThaMpigQueuePriorityMedium,
    cThaMpigQueuePriorityLowest
    }eThaMpigQueuePriority ;

typedef enum eThaMpigQueueCounterType
    {
    cThaMpigQueueCounterInputPackets,
    cThaMpigQueueCounterInputBytes,
    cThaMpigQueueCounterOutputBytes
    }eThaMpigQueueCounterType;

typedef enum eTha60210012MpigEnqueueCntIdx
    {
    cThaMpigTotalGood,
    cThaMpigInternalPktBypass2InternalFifoDirectly,
    cThaMpigInternalPktBypass2InternalFifoFromTailFifo,
    cThaMpigExternalPkt2InternalFifo,
    cThaMpigQueueDisCfgDiscard,
    cThaMpigInputDropDiscard,
    cThaMpigInternalFifoFullDiscard,
    cThaMpigExternalFifoFullDiscard
    }eTha60210012MpigEnqueueCntIdx;

typedef enum eTha60210012MpigDequeueCntIdx
    {
    TotalGoodPkt,
    TotalGoodEndOfPkt,
    TotalGoodByte
    }eTha60210012MpigDequeueCntIdx;

typedef struct tTha60210012MpigQueueCounter
    {
    uint32 pktEnqueueCnt[cTha60210012MpigPktEnqueueCounterNum];
    uint32 byteEnqueueCnt[cTha60210012MpigByteEnqueueCountersNum];
    uint32 byteDequeueCnt[cTha60210012MpigByteDequeueCountersNum];
    }tTha60210012MpigQueueCounter;

typedef struct tTha60210012MpigGlobalCounter
    {
    uint32 ddrAccessQueueCnt[cTha60210012MpigDdrAccessQueueCountersNum];
    }tTha60210012MpigGlobalCounter;

/*--------------------------- Forward declarations ---------------------------*/
AtModule Tha60210012ModuleMpigNew(AtDevice device);

eAtRet Tha60210012ModuleMpigQueueMinThresholdSet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, uint32 internalThreshold, uint32 externalThreshold);
uint32 Tha60210012ModuleMpigQueueMinInternalThresholdGet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);
uint32 Tha60210012ModuleMpigQueueMinExternalThresholdGet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);

eAtRet Tha60210012ModuleMpigQueueEnable(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, eBool enable);
eBool Tha60210012ModuleMpigQueueIsEnabled(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);

eAtRet Tha60210012ModuleMpigHybridQueueingEnable(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, eBool enable);
eBool Tha60210012ModuleMpigHybribQueueingIsEnabled(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);

eAtRet Tha60210012ModuleMpigQueueQuantumSet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, uint32 quantumInByte);
uint32 Tha60210012ModuleMpigQueueQuantumGet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);

eAtRet Tha60210012ModuleMpigQueuePrioritySet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, eThaMpigQueuePriority priority);
eThaMpigQueuePriority Tha60210012ModuleMpigQueuePriorityGet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);

eAtRet Tha60210012ModuleMpigQueueCounterDebug(Tha60210012ModuleMpig self, uint32 queueId, tTha60210012MpigQueueCounter* counters, eBool r2c);
eAtRet Tha60210012ModuleMpigGlobalCounterDebug(Tha60210012ModuleMpig self, tTha60210012MpigGlobalCounter* counters, eBool r2c);

eAtRet Tha60210012ModuleMpigDumpInfoDebugEnable(Tha60210012ModuleMpig self, eBool enable);
eAtRet Tha60210012ModuleMpigDumpInfoDebug(Tha60210012ModuleMpig self);

eAtRet Tha60210012ModuleMpigTransmitOneshotEnable(Tha60210012ModuleMpig self, eBool enable);
eAtRet Tha60210012ModuleMpigTransmitOneshotNumPktSet(Tha60210012ModuleMpig self, uint32 numPkt);
eAtRet Tha60210012ModuleMpigTransmitOneshotNumPktStoreBeforeTransmitSet(Tha60210012ModuleMpig self, uint32 numPkt);

eAtRet Tha60210012ModuleMpigMonitorPacketSet(Tha60210012ModuleMpig self, uint16 channelId, uint16 pktLen);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* THA60210012MODULEMPIG_H_ */


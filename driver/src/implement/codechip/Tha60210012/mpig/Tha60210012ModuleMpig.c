/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MPIG (internal)
 *
 * File        : Tha60210012ModuleMpig.c
 *
 * Created Date: Mar 14, 2016
 *
 * Description : MPIG internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/mpig/ThaModuleMpigInternal.h"
#include "../man/Tha60210012Device.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "Tha60210012ModuleMpigInternal.h"
#include "Tha60210012ModuleMpigReg.h"
#include "../encap/resequence/Tha60210012ResequenceReg.h"
#include "../encap/resequence/Tha60210012ResequenceManager.h"
#include "../eth/Tha60210012EthFlow.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"

/*--------------------------- Define -----------------------------------------*/
#define MAX_MPIG_QUEUE   8
#define cTha60210012MemEntriesMax               512

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)  ((Tha60210012ModuleMpig)self)
#define mBaseAddress(self) mMethodsGet(mThis(self))->BaseAddress(mThis(self))
#define mQueueOffset(self, ethPortId, queueId)                                  \
    mMethodsGet(mThis(self))->QueueThresholdOffset(mThis(self), ethPortId, queueId)

#define mQueueIdIsValid(queue) ((queue < MAX_MPIG_QUEUE) ? 1 : 0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012ModuleMpigMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tThaModuleMpigMethods         m_ThaModuleMpigOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Activate(AtModule self, eBool active)
    {
    uint32 regAddr = mBaseAddress(self) + cAf6Reg_cfg0_pen_Base;
    uint32 regVal  = mModuleHwRead(self, regAddr);

    if (active)
        {
        mRegFieldSet(regVal, cAf6_cfg0_pen_MPIG_active_, 0);
        mModuleHwWrite(self, regAddr, regVal);

        regVal = mModuleHwRead(self, regAddr);
        mRegFieldSet(regVal, cAf6_cfg0_pen_MPIG_active_, 1);
        mModuleHwWrite(self, regAddr, regVal);
        }
    else
        {
        mRegFieldSet(regVal, cAf6_cfg0_pen_MPIG_active_, 0);
        mModuleHwWrite(self, regAddr, regVal);
        }

    return cAtOk;
    }

static eAtRet QueueMinThresholdSet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, uint32 internalThreshold, uint32 externalThreshold)
    {
    uint32 regAddr;
    uint32 regVal;

    if (!mQueueIdIsValid(queueId))
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_cfg0qthsh_pen_Base + mQueueOffset(self, ethPortId, queueId);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_cfg0qthsh_pen_IFFQthsh_min_, internalThreshold);
    mRegFieldSet(regVal, cAf6_cfg0qthsh_pen_EFFQthsh_min_, externalThreshold);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 QueueMinInternalThresholdGet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId)
    {
    uint32 regAddr;
    uint32 regVal;

    if (!mQueueIdIsValid(queueId))
        return 0;

    regAddr = cAf6Reg_cfg0qthsh_pen_Base + mQueueOffset(self, ethPortId, queueId);
    regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_cfg0qthsh_pen_IFFQthsh_min_);
    }

static uint32 QueueMinExternalThresholdGet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId)
    {
    uint32 regAddr;
    uint32 regVal;

    if (!mQueueIdIsValid(queueId))
        return 0;

    regAddr = cAf6Reg_cfg0qthsh_pen_Base + mQueueOffset(self, ethPortId, queueId);
    regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_cfg0qthsh_pen_EFFQthsh_min_);
    }

static eAtRet QueueEnable(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, eBool enable)
    {
    uint32 regAddr;
    uint32 regVal;

    if (!mQueueIdIsValid(queueId))
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_cfg0qthsh_pen_Base + mQueueOffset(self, ethPortId, queueId);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_cfg0qthsh_pen_Queue_Enb_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool QueueIsEnabled(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId)
    {
    uint32 regAddr;
    uint32 regVal;
    uint8 enable;

    if (!mQueueIdIsValid(queueId))
        return 0;

    regAddr = cAf6Reg_cfg0qthsh_pen_Base + mQueueOffset(self, ethPortId, queueId);
    regVal = mModuleHwRead(self, regAddr);
    mFieldGet(regVal,
              cAf6_cfg0qthsh_pen_Queue_Enb_Mask,
              cAf6_cfg0qthsh_pen_Queue_Enb_Shift,
              uint8, &enable);

    return (eBool)((enable == 1) ? cAtTrue : cAtFalse);
    }

static eAtRet QueueHybridEnable(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, eBool enable)
    {
    uint32 regAddr;
    uint32 regVal;

    if (!mQueueIdIsValid(queueId))
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_cfg0qthsh_pen_Base + mQueueOffset(self, ethPortId, queueId);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_cfg0qthsh_pen_Queue_Hyb_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool QueueHybridIsEnabled(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId)
    {
    uint32 regAddr;
    uint32 regVal;

    if (!mQueueIdIsValid(queueId))
        return 0;

    regAddr = cAf6Reg_cfg0qthsh_pen_Base + mQueueOffset(self, ethPortId, queueId);
    regVal = mModuleHwRead(self, regAddr);
    return (eBool)((mRegField(regVal, cAf6_cfg0qthsh_pen_Queue_Hyb_) == 1) ? cAtTrue : cAtFalse);
    }

static eAtRet QueueQuantumInByteSet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, uint32 quantumInByte)
    {
    uint32 regAddr;
    uint32 regVal;

    if (!mQueueIdIsValid(queueId))
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_schelinkicfg1_pen_Base + mQueueOffset(self, ethPortId, queueId);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_, quantumInByte);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 QueueQuantumInByteGet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId)
    {
    uint32 regAddr;
    uint32 regVal;

    if (!mQueueIdIsValid(queueId))
        return 0;

    regAddr = cAf6Reg_schelinkicfg1_pen_Base + mQueueOffset(self, ethPortId, queueId);
    regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_);
    }

static eAtRet QueuePrioritySet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, eThaMpigQueuePriority priority)
    {
    uint32 regAddr;
    uint32 regVal;

    if (!mQueueIdIsValid(queueId))
        return cAtErrorOutOfRangParm;

    regAddr = mBaseAddress(self) + cAf6Reg_schelcfg0_pen_Base + ethPortId;
    regVal = mModuleHwRead(self, regAddr);

    switch(priority)
        {
        case cThaMpigQueuePriorityHighest:
            mBitSet(&regVal, queueId + cAf6_schelcfg0_pen_Hsp_grp_Shift);
            mBitClear(&regVal, queueId + cAf6_schelcfg0_pen_DWRR_grp_Shift);
            mBitClear(&regVal, queueId + cAf6_schelcfg0_pen_Lsp_grp_Shift);
            break;
        case cThaMpigQueuePriorityMedium:
            mBitClear(&regVal, queueId + cAf6_schelcfg0_pen_Hsp_grp_Shift);
            mBitSet(&regVal, queueId + cAf6_schelcfg0_pen_DWRR_grp_Shift);
            mBitClear(&regVal, queueId + cAf6_schelcfg0_pen_Lsp_grp_Shift);
            break;
        case cThaMpigQueuePriorityLowest:
            mBitClear(&regVal, queueId + cAf6_schelcfg0_pen_Hsp_grp_Shift);
            mBitClear(&regVal, queueId + cAf6_schelcfg0_pen_DWRR_grp_Shift);
            mBitSet(&regVal, queueId + cAf6_schelcfg0_pen_Lsp_grp_Shift);
            break;
        case cThaMpigQueuePriorityUnknown:
        default:
            break;
        }

    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eThaMpigQueuePriority QueuePriorityGet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId)
    {
    uint32 regAddr;
    uint32 regVal;
    uint8 higest = 0;
    uint8 lowest = 0;
    uint8 medium = 0;

    if (!mQueueIdIsValid(queueId))
        return cThaMpigQueuePriorityUnknown;

    regAddr = mBaseAddress(self) + cAf6Reg_schelcfg0_pen_Base + ethPortId;;
    regVal = mModuleHwRead(self, regAddr);
    higest = mBitGet(regVal, queueId + cAf6_schelcfg0_pen_Hsp_grp_Shift);
    medium = mBitGet(regVal, queueId + cAf6_schelcfg0_pen_DWRR_grp_Shift);
    lowest = mBitGet(regVal, queueId + cAf6_schelcfg0_pen_Lsp_grp_Shift);

    if((higest == 1) && (lowest == 0) && (medium == 0))
        return cThaMpigQueuePriorityHighest;

    if((higest == 0) && (lowest == 1) && (medium == 0))
        return cThaMpigQueuePriorityLowest;

    if((higest == 0) && (lowest == 0) && (medium == 1))
        return cThaMpigQueuePriorityMedium;

    return cThaMpigQueuePriorityUnknown;
    }

static eAtRet QueueCounterDebug(Tha60210012ModuleMpig self, uint32 queueId, tTha60210012MpigQueueCounter* counters, eBool r2c)
    {
    uint32 regAddr;
    uint32 cntId;
    uint8 hwRead2Clear;
    uint32 baseAddress = mBaseAddress(self);

    if (!mQueueIdIsValid(queueId))
        return cAtErrorOutOfRangParm;

    if (counters == NULL)
        return cAtErrorNullPointer;

    hwRead2Clear = r2c ? 0 : 1;
    /* Enqueue packet counter */
    for (cntId = 0; cntId < cTha60210012MpigPktEnqueueCounterNum; cntId++)
        {
        regAddr = baseAddress + cAf6Reg_pmcnt1_pen(hwRead2Clear, queueId, cntId);
        counters->pktEnqueueCnt[cntId] = mModuleHwRead(self, regAddr);
        }

    /* Enqueue byte counter */
    for (cntId = 0; cntId < cTha60210012MpigByteEnqueueCountersNum; cntId++)
        {
        regAddr = baseAddress + cAf6Reg_pmcnt2_pen(hwRead2Clear, queueId, cntId);
        counters->byteEnqueueCnt[cntId] = mModuleHwRead(self, regAddr);
        }

    /* Dequeue byte counter */
    for (cntId = 0; cntId < cTha60210012MpigByteDequeueCountersNum; cntId++)
        {
        regAddr = baseAddress + cAf6Reg_pmcnt3_pen(hwRead2Clear, queueId, cntId);
        counters->byteDequeueCnt[cntId] = mModuleHwRead(self, regAddr);
        }

    return cAtOk;
    }

static eAtRet GlobalCounterDebug(Tha60210012ModuleMpig self, tTha60210012MpigGlobalCounter* counters, eBool r2c)
    {
    uint32 counter_i;

    AtUnused(self);
    AtUnused(r2c);

    if(counters == NULL)
        return cAtErrorNullPointer;

    /* DDR Access Queue counter */
    for(counter_i = 0; counter_i < cTha60210012MpigDdrAccessQueueCountersNum; counter_i++)
        {
        uint32 regAddr = mBaseAddress(self) + cAf6Reg_pmcnt4_pen(counter_i);
        counters->ddrAccessQueueCnt[counter_i] = mModuleHwRead(self, regAddr);
        if(r2c)
            mModuleHwWrite(self, regAddr, 0x0);
        }

    return cAtOk;
    }

static eAtRet DumpInfoDebugEnable(Tha60210012ModuleMpig self, eBool enable)
    {
    uint32 regAddr;
    uint32 regVal;

    regAddr = mBaseAddress(self) + cAf6Reg_cfg2_pen_Base;
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_cfg2_pen_MPIG_dump_en_, enable);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static void DumpInfoInputPacket(Tha60210012ModuleMpig self)
    {
    uint32 entry_i;
    const char* inforStr[] = {"Input_PLA_Packet_Length_ID",
                              "Input_PLA_Channel_ID"};

    AtPrintc(cSevDebug, "+ Dump MPIG Input Packet Information\r\n");
    AtPrintc(cSevNormal, "%-30s%-30s%-30s\r\n", " ", inforStr[0], inforStr[1]);

    for (entry_i = 0; entry_i < cTha60210012MemEntriesMax; entry_i++)
        {
        uint32 regAddr = mBaseAddress(self) + cAf6Reg_dum0_pen(entry_i);
        uint32 regVal = mModuleHwRead(self, regAddr);
        uint16 plaPktLen = (uint16)mRegField(regVal, cAf6_dum0_pen_Input_PLA_Packet_Length_ID_);
        uint16 plaChnId = (uint16)mRegField(regVal, cAf6_dum0_pen_Input_PLA_Channel_ID_);
        AtPrintc(cSevNormal, "Address-0x%08x: ", regAddr);
        AtPrintc(cSevInfo, "%-10s%-30d%-30d\r\n", " ", plaPktLen, plaChnId);
        }
    }

static void DumpInfoOutputPacket(Tha60210012ModuleMpig self)
    {
    uint32 entry_i;
    const char* inforStr[] = {"Output_PLA_Packet_Length_ID",
                              "Output_PLA_Channel_ID"};

    AtPrintc(cSevDebug, "+ Dump MPIG Input Packet Information\r\n");
    AtPrintc(cSevNormal, "%-30s%-30s%-30s\r\n", " ", inforStr[0], inforStr[1]);

    for(entry_i = 0; entry_i < cTha60210012MemEntriesMax; entry_i++)
        {
        uint32 regAddr = mBaseAddress(self) + cAf6Reg_dum1_pen(entry_i);
        uint32 regVal  = mModuleHwRead(self, regAddr);
        uint16 plaPktLen = (uint16)mRegField(regVal, cAf6_dum1_pen_Output_PLA_Packet_Length_ID_);
        uint16 plaChnId  = (uint16)mRegField(regVal, cAf6_dum1_pen_Output_PLA_Channel_ID_);
        AtPrintc(cSevNormal, "Address-0x%08x: ", regAddr);
        AtPrintc(cSevInfo, "%-10s%-30d%-30d\r\n", " ", plaPktLen, plaChnId);
        }
    }

static void DumpInfoWriteDdrPacket(Tha60210012ModuleMpig self)
    {
    uint32 entry_i;
    uint8 cachedQueue = 0;
    uint16 cachedSequence = 0;
    const char* inforStr[] = {"DDR_Write_Packet_Length",
                              "DDR_Write_Queue_ID",
                              "DDR_Write_Sequence"};

    AtPrintc(cSevDebug, "+ Dump MPIG Input Packet Information\r\n");
    AtPrintc(cSevNormal, "%-30s%-30s%-30s%-30s\r\n", " ", inforStr[0], inforStr[1], inforStr[2]);

    for(entry_i = 0; entry_i < cTha60210012MemEntriesMax; entry_i++)
        {
        uint32 regAddr = mBaseAddress(self) + cAf6Reg_dum2_pen(entry_i);
        uint32 regVal = mModuleHwRead(self, regAddr);
        uint16 ddrPktLen = (uint16)mRegField(regVal, cAf6_dum2_pen_DDR_Write_Packet_Length_);
        uint8  ddrQueueId = (uint8)mRegField(regVal, cAf6_dum2_pen_DDR_Write_Queue_ID_);
        uint8 ddrSequence = (uint8)mRegField(regVal, cAf6_dum2_pen_DDR_Write_Sequence_);

        AtPrintc(cSevNormal, "Address-0x%08x: ", regAddr);
        AtPrintc(cSevInfo, "%-10s%-30d%-30d%-30d", " ", ddrPktLen, ddrQueueId, ddrSequence);
        if ((entry_i != 0) && (ddrQueueId == cachedQueue) && ((ddrSequence - cachedSequence) > 1))
            AtPrintc(cSevCritical, ">>>> Wrong sequence number");
        AtPrintc(cSevNormal, "\r\n");

        cachedQueue = ddrQueueId;
        cachedSequence = ddrSequence;
        }
    }

static void DumpInfoReadDdrPacket(Tha60210012ModuleMpig self)
    {
    uint32 entry_i;
    uint8 cachedQueue = 0;
    uint16 cachedSequence = 0;
    const char* inforStr[] = {"DDR_Read_Packet_Length",
                              "DDR_Read_Queue_ID",
                              "DDR_Read_Sequence"};

    AtPrintc(cSevDebug, "+ Dump MPIG Input Packet Information\r\n");
    AtPrintc(cSevNormal, "%-30s%-30s%-30s%-30s\r\n", " ", inforStr[0], inforStr[1], inforStr[2]);

    for (entry_i = 0; entry_i < cTha60210012MemEntriesMax; entry_i++)
        {
        uint32 regAddr = mBaseAddress(self) + cAf6Reg_dum3_pen(entry_i);
        uint32 regVal  = mModuleHwRead(self, regAddr);
        uint16 ddrPktLen = (uint16)mRegField(regVal, cAf6_dum3_pen_DDR_Read_Packet_Length_);
        uint8 ddrQueueId = (uint8)mRegField(regVal, cAf6_dum3_pen_DDR_Read_Queue_ID_);
        uint8 ddrSequence = (uint8)mRegField(regVal, cAf6_dum3_pen_DDR_Read_Sequence_);

        AtPrintc(cSevNormal, "Address-0x%08x: ", regAddr);
        AtPrintc(cSevInfo, "%-10s%-30d%-30d%-30d", " ", ddrPktLen, ddrQueueId, ddrSequence);
        if ((entry_i != 0) && (ddrQueueId == cachedQueue) && ((ddrSequence - cachedSequence) > 1))
            AtPrintc(cSevCritical, ">>>> Wrong sequence number");
        AtPrintc(cSevNormal, "\r\n");

        cachedQueue = ddrQueueId;
        cachedSequence = ddrSequence;
        }
    }

static eAtRet DumpInfoDebug(Tha60210012ModuleMpig self)
    {
    AtPrintc(cSevInfo, "Dump MPIG Packet Information\r\n");
    AtPrintc(cSevNormal, "---------------------------------------------------------\r\n");
    DumpInfoInputPacket(self);
    AtPrintc(cSevNormal, "---------------------------------------------------------\r\n");
    DumpInfoOutputPacket(self);
    AtPrintc(cSevNormal, "---------------------------------------------------------\r\n");
    DumpInfoWriteDdrPacket(self);
    AtPrintc(cSevNormal, "---------------------------------------------------------\r\n");
    DumpInfoReadDdrPacket(self);
    AtPrintc(cSevNormal, "---------------------------------------------------------\r\n");
    return cAtOk;
    }

static eAtRet TransmitOneshotEnable(Tha60210012ModuleMpig self, eBool enable)
    {
    uint32 regAddr = mBaseAddress(self) + cAf6Reg_cfg2_pen_Base;
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_cfg2_pen_MPIG_oneshot_en_, enable);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet TransmitOneshotNumPktSet(Tha60210012ModuleMpig self, uint32 numPkt)
    {
    uint32 regAddr = mBaseAddress(self) + cAf6Reg_cfg2_pen_Base;
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_cfg2_pen_MPIG_NumPkt_Accept_, numPkt);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet TransmitOneshotNumPktStoreBeforeTransmitSet(Tha60210012ModuleMpig self, uint32 numPkt)
    {
    uint32 regAddr;
    uint32 regVal;

    regAddr = mBaseAddress(self) + cAf6Reg_cfg2_pen_Base;
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_cfg2_pen_MPIG_NumPkt_Store_Before_Transmit_, numPkt);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet MonitorPacketSet(Tha60210012ModuleMpig self, uint16 channelId, uint16 pktLen)
    {
    uint32 regAddr;
    uint32 regVal;

    regAddr = mBaseAddress(self) + cAf6Reg_cfg3_pen_Base;
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_cfg3_pen_MPIG_ChannelID_, channelId);
    mRegFieldSet(regVal, cAf6_cfg3_pen_MPIG_Length_, pktLen);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    if ((localAddress >= 0x841002) && (localAddress <= 0x87FFFF))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x840010, 0x840011, 0x840012};
	AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = 3;

    return holdRegisters;
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleMpigRegisterIteratorCreate(self);
    }

static eBool MemoryCanBeTested(AtModule self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eThaMpigQueuePriority DefaultPriorityOfQueue(Tha60210012ModuleMpig self, uint32 queueId)
    {
    const uint32 cQueueForPwService             = 0;
    const uint32 cQueueForXAUI0      = 6;

    AtUnused(self);
    if ((queueId == cQueueForPwService) || (queueId == cQueueForXAUI0))
        return cThaMpigQueuePriorityHighest;

    return cThaMpigQueuePriorityMedium;
    }

static uint8 NumberXfiPort(Tha60210012ModuleMpig self)
    {
    AtUnused(self);
    return 1;
    }

static eAtRet AllModuleInitPerPortDefault(AtModule self, uint8 ethPortId)
    {
    const uint32 cQueueNumbers = 8;
    eThaMpigQueuePriority priority;
    uint32 queueId;
    eAtRet ret = cAtOk;

    for (queueId = 0; queueId < cQueueNumbers; queueId++)
        {
        ret |= mMethodsGet(mThis(self))->QueueHybridEnable(mThis(self), ethPortId, queueId, cAtFalse);
        ret |= mMethodsGet(mThis(self))->QueueEnable(mThis(self), ethPortId, queueId, cAtFalse);
        }

    for (queueId = 0; queueId < cQueueNumbers; queueId++)
        {
        priority = mMethodsGet(mThis(self))->DefaultPriorityOfQueue(mThis(self), queueId);

        ret |= mMethodsGet(mThis(self))->QueueQuantumInByteSet(mThis(self), ethPortId, queueId, 9600);
        ret |= mMethodsGet(mThis(self))->QueuePrioritySet(mThis(self), ethPortId, queueId, priority);
        ret |= mMethodsGet(mThis(self))->QueueMinThresholdSet(mThis(self), ethPortId, queueId, 0x1FF, 0x1FF0);
        ret |= mMethodsGet(mThis(self))->QueueHybridEnable(mThis(self), ethPortId, queueId, cAtTrue);
        }

    if (!AtDeviceIsSimulated(AtModuleDeviceGet(self)))
        AtOsalUSleep(10000);

    /* Enable after set up All configurations */
    for (queueId = 0; queueId < cQueueNumbers; queueId++)
        ret |= mMethodsGet(mThis(self))->QueueEnable(mThis(self), ethPortId, queueId, cAtTrue);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    uint8 numXfiPort = NumberXfiPort(mThis(self));
    uint8 ethPortId;
    
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Active MPIG */
    Activate(self, cAtTrue);

    /* Disable All Queue to flush hardware */
    for (ethPortId = 0; ethPortId < numXfiPort; ethPortId++)
        ret |= AllModuleInitPerPortDefault(self, ethPortId);

    return ret;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsMpigRegister(AtModuleDeviceGet(self), address);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void DebugStickyPrint(const char* str, eBool sticky)
    {
    AtPrintc(cSevNormal, "%-40s: ", str);
    AtPrintc((sticky) ? cSevCritical : cSevInfo, "%s\r\n", (sticky) ? "SET" : "CLEAR");
    }

static void DebugSticky(AtModule self, uint32 address, tBitFieldInfo* bitInfo, uint8 numOfStk)
    {
    uint32 regAddr = mBaseAddress(self) + address;
    uint32 regVal = mModuleHwRead(self, regAddr);
    eBool sticky;
    uint8 i;

    if(bitInfo == NULL)
        return;

    AtPrintc(cSevNormal, "0x%08X: 0x%08x\r\n\r\n", regAddr, regVal);

    for (i = 0; i < numOfStk; i++)
        {
        mFieldGet(regVal, bitInfo[i].mask, bitInfo[i].shift, eBool, &sticky);
        DebugStickyPrint(bitInfo[i].name, sticky);
        }

    AtPrintc(cSevNormal, "----------------------------------------\r\n");

    /* Clear sticky */
    mModuleHwWrite(self, regAddr, regVal);
    }

static void ShowStick0(AtModule self)
    {
    tBitFieldInfo stickies[] = {
                         {"FiFo_Enqueue_Read_Error", cAf6_stk0_pen_FiFo_Enqueue_Write_Error_Mask, cAf6_stk0_pen_FiFo_Enqueue_Write_Error_Shift},
                         {"DDR_Read_Cache_Write"   , cAf6_stk0_pen_FiFo_Enqueue_Read_Error_Mask , cAf6_stk0_pen_FiFo_Enqueue_Read_Error_Shift },
                         {"DDR_Write_Cache_Read"   , cAf6_stk0_pen_DDR_Read_Cache_Write_Mask    , cAf6_stk0_pen_DDR_Read_Cache_Write_Shift    },
                         {"DDR_Return_Write_Valid" , cAf6_stk0_pen_DDR_Return_Write_Valid_Mask  , cAf6_stk0_pen_DDR_Return_Write_Valid_Shift  },
                         {"MPIG_Request_Write"     , cAf6_stk0_pen_MPIG_Request_Write_Mask      , cAf6_stk0_pen_MPIG_Request_Write_Shift      },
                         {"MPIG_Request_Read"      , cAf6_stk0_pen_MPIG_Request_Read_Mask       , cAf6_stk0_pen_MPIG_Request_Read_Shift       },
                         {"DDR_Return_ACK"         , cAf6_stk0_pen_DDR_Return_ACK_Mask          , cAf6_stk0_pen_DDR_Return_ACK_Shift          },
                         {"DDR_Return_Read_Valid"  , cAf6_stk0_pen_DDR_Return_Read_Valid_Mask   , cAf6_stk0_pen_DDR_Return_Read_Valid_Shift   },
                         {"MPIG_Output_to_RSQ"     , cAf6_stk0_pen_MPIG_Output_to_RSQ_Mask      , cAf6_stk0_pen_MPIG_Output_to_RSQ_Shift      },
                         {"PLA_Error"              , cAf6_stk0_pen_PLA_Error_Mask               , cAf6_stk0_pen_PLA_Error_Shift               },
                         {"Input_RSQ"              , cAf6_stk0_pen_Input_RSQ_Mask               , cAf6_stk0_pen_Input_RSQ_Shift               },
                         {"PLA_Request_Read_Enable", cAf6_stk0_pen_PLA_Request_Read_Enable_Mask , cAf6_stk0_pen_PLA_Request_Read_Enable_Shift },
                         {"MPIG_ouput_to_PLA"      , cAf6_stk0_pen_MPIG_ouput_to_PLA_Mask       , cAf6_stk0_pen_MPIG_ouput_to_PLA_Shift       },
                         {"PLA_Enqueue_Enable"     , cAf6_stk0_pen_PLA_Enqueue_Enable_Mask      , cAf6_stk0_pen_PLA_Enqueue_Enable_Shift      }
                        };

    DebugSticky(self, cAf6Reg_stk0_pen_Base, stickies, mCount(stickies));
    }

static void ShowStick1(AtModule self)
    {
    tBitFieldInfo stickies[] = {
                                {"Sche_DWRRCurrent_Queue7", cAf6_stk1en_Sche_DWRRCurrent_Queue7_Mask, cAf6_stk1en_Sche_DWRRCurrent_Queue7_Shift},
                                {"Sche_DWRRCurrent_Queue6", cAf6_stk1en_Sche_DWRRCurrent_Queue6_Mask, cAf6_stk1en_Sche_DWRRCurrent_Queue6_Shift},
                                {"Sche_DWRRCurrent_Queue5", cAf6_stk1en_Sche_DWRRCurrent_Queue5_Mask, cAf6_stk1en_Sche_DWRRCurrent_Queue5_Shift},
                                {"Sche_DWRRCurrent_Queue4", cAf6_stk1en_Sche_DWRRCurrent_Queue4_Mask, cAf6_stk1en_Sche_DWRRCurrent_Queue4_Shift},
                                {"Sche_DWRRCurrent_Queue3", cAf6_stk1en_Sche_DWRRCurrent_Queue3_Mask, cAf6_stk1en_Sche_DWRRCurrent_Queue3_Shift},
                                {"Sche_DWRRCurrent_Queue2", cAf6_stk1en_Sche_DWRRCurrent_Queue2_Mask, cAf6_stk1en_Sche_DWRRCurrent_Queue2_Shift},
                                {"Sche_DWRRCurrent_Queue1", cAf6_stk1en_Sche_DWRRCurrent_Queue1_Mask, cAf6_stk1en_Sche_DWRRCurrent_Queue1_Shift},
                                {"Sche_DWRRCurrent_Queue0", cAf6_stk1en_Sche_DWRRCurrent_Queue0_Mask, cAf6_stk1en_Sche_DWRRCurrent_Queue0_Shift},
                                {"Sche_HSPCurrent_Queue7" , cAf6_stk1en_Sche_HSPCurrent_Queue7_Mask , cAf6_stk1en_Sche_HSPCurrent_Queue7_Shift },
                                {"Sche_HSPCurrent_Queue6" , cAf6_stk1en_Sche_HSPCurrent_Queue6_Mask , cAf6_stk1en_Sche_HSPCurrent_Queue6_Shift },
                                {"Sche_HSPCurrent_Queue5" , cAf6_stk1en_Sche_HSPCurrent_Queue5_Mask , cAf6_stk1en_Sche_HSPCurrent_Queue5_Shift },
                                {"Sche_HSPCurrent_Queue4" , cAf6_stk1en_Sche_HSPCurrent_Queue4_Mask , cAf6_stk1en_Sche_HSPCurrent_Queue4_Shift },
                                {"Sche_HSPCurrent_Queue3" , cAf6_stk1en_Sche_HSPCurrent_Queue3_Mask , cAf6_stk1en_Sche_HSPCurrent_Queue3_Shift },
                                {"Sche_HSPCurrent_Queue2" , cAf6_stk1en_Sche_HSPCurrent_Queue2_Mask , cAf6_stk1en_Sche_HSPCurrent_Queue2_Shift },
                                {"Sche_HSPCurrent_Queue1" , cAf6_stk1en_Sche_HSPCurrent_Queue1_Mask , cAf6_stk1en_Sche_HSPCurrent_Queue1_Shift },
                                {"Sche_HSPCurrent_Queue0" , cAf6_stk1en_Sche_HSPCurrent_Queue0_Mask , cAf6_stk1en_Sche_HSPCurrent_Queue0_Shift },
                                {"Sche_Current_Queue7"    , cAf6_stk1en_Sche_Current_Queue7_Mask    , cAf6_stk1en_Sche_Current_Queue7_Shift    },
                                {"Sche_Current_Queue6"    , cAf6_stk1en_Sche_Current_Queue6_Mask    , cAf6_stk1en_Sche_Current_Queue6_Shift    },
                                {"Sche_Current_Queue5"    , cAf6_stk1en_Sche_Current_Queue5_Mask    , cAf6_stk1en_Sche_Current_Queue5_Shift    },
                                {"Sche_Current_Queue4"    , cAf6_stk1en_Sche_Current_Queue4_Mask    , cAf6_stk1en_Sche_Current_Queue4_Shift    },
                                {"Sche_Current_Queue3"    , cAf6_stk1en_Sche_Current_Queue3_Mask    , cAf6_stk1en_Sche_Current_Queue3_Shift    },
                                {"Sche_Current_Queue2"    , cAf6_stk1en_Sche_Current_Queue2_Mask    , cAf6_stk1en_Sche_Current_Queue2_Shift    },
                                {"Sche_Current_Queue1"    , cAf6_stk1en_Sche_Current_Queue1_Mask    , cAf6_stk1en_Sche_Current_Queue1_Shift    },
                                {"Sche_Current_Queue0"    , cAf6_stk1en_Sche_Current_Queue0_Mask    , cAf6_stk1en_Sche_Current_Queue0_Shift    },
                                {"Sche_Request_QMXFF"     , cAf6_stk1en_Sche_Request_QMXFF_Mask     , cAf6_stk1en_Sche_Request_QMXFF_Shift     },
                                {"Sche_Return_Valid"      , cAf6_stk1en_Sche_Return_Valid_Mask      , cAf6_stk1en_Sche_Return_Valid_Shift      },
                                {"Sche_Wrempt_Update"     , cAf6_stk1en_Sche_Wrempt_Update_Mask     , cAf6_stk1en_Sche_Wrempt_Update_Shift     },
                                {"Sche_Sta1_err"          , cAf6_stk1en_Sche_Sta1_err_Mask          , cAf6_stk1en_Sche_Sta1_err_Shift          },
                                {"Sche_Sta0_err"          , cAf6_stk1en_Sche_Sta0_err_Mask          , cAf6_stk1en_Sche_Sta0_err_Shift          }
                               };
    DebugSticky(self, cAf6Reg_stk1en_Base, stickies, mCount(stickies));
    }

static void ShowStick2(AtModule self)
    {
    tBitFieldInfo stickies[] = {
                            {"MPIG_Discard"               , cAf6_stk2pen_MPIG_Discard_Mask               , cAf6_stk2pen_MPIG_Discard_Shift               },
                            {"Output_Internal_Q7_Seq_Fail", cAf6_stk2pen_Output_Internal_Q7_Seq_Fail_Mask, cAf6_stk2pen_Output_Internal_Q7_Seq_Fail_Shift},
                            {"Output_Internal_Q6_Seq_Fail", cAf6_stk2pen_Output_Internal_Q6_Seq_Fail_Mask, cAf6_stk2pen_Output_Internal_Q6_Seq_Fail_Shift},
                            {"Output_Internal_Q5_Seq_Fail", cAf6_stk2pen_Output_Internal_Q5_Seq_Fail_Mask, cAf6_stk2pen_Output_Internal_Q5_Seq_Fail_Shift},
                            {"Output_Internal_Q4_Seq_Fail", cAf6_stk2pen_Output_Internal_Q4_Seq_Fail_Mask, cAf6_stk2pen_Output_Internal_Q4_Seq_Fail_Shift},
                            {"Output_Internal_Q3_Seq_Fail", cAf6_stk2pen_Output_Internal_Q3_Seq_Fail_Mask, cAf6_stk2pen_Output_Internal_Q3_Seq_Fail_Shift},
                            {"Output_Internal_Q2_Seq_Fail", cAf6_stk2pen_Output_Internal_Q2_Seq_Fail_Mask, cAf6_stk2pen_Output_Internal_Q2_Seq_Fail_Shift},
                            {"Output_Internal_Q1_Seq_Fail", cAf6_stk2pen_Output_Internal_Q1_Seq_Fail_Mask, cAf6_stk2pen_Output_Internal_Q1_Seq_Fail_Shift},
                            {"Output_Internal_Q0_Seq_Fail", cAf6_stk2pen_Output_Internal_Q0_Seq_Fail_Mask, cAf6_stk2pen_Output_Internal_Q0_Seq_Fail_Shift},
                            {"Output_External_Q7_Seq_Fail", cAf6_stk2pen_Output_External_Q7_Seq_Fail_Mask, cAf6_stk2pen_Output_External_Q7_Seq_Fail_Shift},
                            {"Output_External_Q6_Seq_Fail", cAf6_stk2pen_Output_External_Q6_Seq_Fail_Mask, cAf6_stk2pen_Output_External_Q6_Seq_Fail_Shift},
                            {"Output_External_Q5_Seq_Fail", cAf6_stk2pen_Output_External_Q5_Seq_Fail_Mask, cAf6_stk2pen_Output_External_Q5_Seq_Fail_Shift},
                            {"Output_External_Q4_Seq_Fail", cAf6_stk2pen_Output_External_Q4_Seq_Fail_Mask, cAf6_stk2pen_Output_External_Q4_Seq_Fail_Shift},
                            {"Output_External_Q3_Seq_Fail", cAf6_stk2pen_Output_External_Q3_Seq_Fail_Mask, cAf6_stk2pen_Output_External_Q3_Seq_Fail_Shift},
                            {"Output_External_Q2_Seq_Fail", cAf6_stk2pen_Output_External_Q2_Seq_Fail_Mask, cAf6_stk2pen_Output_External_Q2_Seq_Fail_Shift},
                            {"Output_External_Q1_Seq_Fail", cAf6_stk2pen_Output_External_Q1_Seq_Fail_Mask, cAf6_stk2pen_Output_External_Q1_Seq_Fail_Shift},
                            {"Output_External_Q0_Seq_Fail", cAf6_stk2pen_Output_External_Q0_Seq_Fail_Mask, cAf6_stk2pen_Output_External_Q0_Seq_Fail_Shift}
    };

    DebugSticky(self, cAf6Reg_stk2pen_Base, stickies, mCount(stickies));
    }

static void ShowStick3(AtModule self)
    {
    tBitFieldInfo stickies[] = {
                            {"Input_PLA_length_Fail"          , cAf6_stk3en_Input_PLA_length_Fail_Mask          , cAf6_stk3en_Input_PLA_length_Fail_Shift          },
                            {"Ouput_PLA_Length_Fail"          , cAf6_stk3en_Ouput_PLA_Length_Fail_Mask          , cAf6_stk3en_Ouput_PLA_Length_Fail_Shift          },
                            {"FiFo_Enqueue_Write_Error"       , cAf6_stk3en_FiFo_Enqueue_Write_Error_Mask       , cAf6_stk3en_FiFo_Enqueue_Write_Error_Shift       },
                            {"FiFo_Enqueue_Read_Error"        , cAf6_stk3en_FiFo_Enqueue_Read_Error_Mask        , cAf6_stk3en_FiFo_Enqueue_Read_Error_Shift        },
                            {"External_Engine_Err"            , cAf6_stk3en_External_Engine_Err_Mask            , cAf6_stk3en_External_Engine_Err_Shift            },
                            {"External_Write_Req_Buff_Full"   , cAf6_stk3en_External_Write_Req_Buff_Full_Mask   , cAf6_stk3en_External_Write_Req_Buff_Full_Shift   },
                            {"External_Write_Valid_Buff_Full" , cAf6_stk3en_External_Write_Valid_Buff_Full_Mask , cAf6_stk3en_External_Write_Valid_Buff_Full_Shift },
                            {"External_Read_Req_Cache_Full"   , cAf6_stk3en_External_Read_Req_Cache_Full_Mask   , cAf6_stk3en_External_Read_Req_Cache_Full_Shift   },
                            {"External_Write_Req_Cache_Full"  , cAf6_stk3en_External_Write_Req_Cache_Full_Mask  , cAf6_stk3en_External_Write_Req_Cache_Full_Shift  },
                            {"Head_FiFo_Write_Error"          , cAf6_stk3en_Head_FiFo_Write_Error_Mask          , cAf6_stk3en_Head_FiFo_Write_Error_Shift          },
                            {"Head_FiFo_Read_Error"           , cAf6_stk3en_Head_FiFo_Read_Error_Mask           , cAf6_stk3en_Head_FiFo_Read_Error_Shift           },
                            {"FiFO_Request_DDR_Write_Err"     , cAf6_stk3en_FiFO_Request_DDR_Write_Err_Mask     , cAf6_stk3en_FiFO_Request_DDR_Write_Err_Shift     },
                            {"FiFO_Request_DDR_Read_Err"      , cAf6_stk3en_FiFO_Request_DDR_Read_Err_Mask      , cAf6_stk3en_FiFO_Request_DDR_Read_Err_Shift      },
                            {"External_FiFO_Engine_Conflict"  , cAf6_stk3en_External_FiFO_Engine_Conflict_Mask  , cAf6_stk3en_External_FiFO_Engine_Conflict_Shift  },
                            {"QMXFF_Read_Cache_Error"         , cAf6_stk3en_QMXFF_Read_Cache_Error_Mask         , cAf6_stk3en_QMXFF_Read_Cache_Error_Shift         },
                            {"External_FiFO_Full_Thres_CFG"   , cAf6_stk3en_External_FiFO_Full_Thres_CFG_Mask   , cAf6_stk3en_External_FiFO_Full_Thres_CFG_Shift   },
                            {"FiFo_DDR_Valid_Write_Err"       , cAf6_stk3en_FiFo_DDR_Valid_Write_Err_Mask       , cAf6_stk3en_FiFo_DDR_Valid_Write_Err_Shift       },
                            {"Internal_FiFO_Engine_Conflict"  , cAf6_stk3en_Internal_FiFO_Engine_Conflict_Mask  , cAf6_stk3en_Internal_FiFO_Engine_Conflict_Shift  },
                            {"Discard_Queue_CFG_Disable"      , cAf6_stk3en_Discard_Queue_CFG_Disable_Mask      , cAf6_stk3en_Discard_Queue_CFG_Disable_Shift      },
                            {"Discard_PLA_Drop"               , cAf6_stk3en_Discard_PLA_Drop_Mask               , cAf6_stk3en_Discard_PLA_Drop_Shift               },
                            {"Internal_FiFO_Write_Conflict"   , cAf6_stk3en_Internal_FiFO_Write_Conflict_Mask   , cAf6_stk3en_Internal_FiFO_Write_Conflict_Shift   },
                            {"FiFo_DDR_Valid_Read_Err"        , cAf6_stk3en_FiFo_DDR_Valid_Read_Err_Mask        , cAf6_stk3en_FiFo_DDR_Valid_Read_Err_Shift        },
                            {"Discard_Head_FiFo_Write_Error"  , cAf6_stk3en_Discard_Head_FiFo_Write_Error_Mask  , cAf6_stk3en_Discard_Head_FiFo_Write_Error_Shift  },
                            {"Discard_Internal_External_Full0", cAf6_stk3en_Discard_Internal_External_Full0_Mask, cAf6_stk3en_Discard_Internal_External_Full0_Shift},
                            {"Discard_Internal_External_Full1", cAf6_stk3en_Discard_Internal_External_Full1_Mask, cAf6_stk3en_Discard_Internal_External_Full1_Shift}
                           };

    DebugSticky(self, cAf6Reg_stk3en_Base, stickies, mCount(stickies));
    }

static eAtRet Debug(AtModule self)
    {
    AtPrintc(cSevDebug, "\r\n MPIG DEBUG information: \r\n");
    AtPrintc(cSevNormal, "=========================================================================== \r\n");
    AtPrintc(cSevNormal, "  + Sticky 0 - "); ShowStick0(self);
    AtPrintc(cSevNormal, "  + Sticky 1 - "); ShowStick1(self);
    AtPrintc(cSevNormal, "  + Sticky 2 - "); ShowStick2(self);
    AtPrintc(cSevNormal, "  + Sticky 3 - "); ShowStick3(self);
    AtPrintc(cSevNormal, "=========================================================================== \r\n");

    return cAtOk;
    }

static AtResequenceManager Manager(AtResequenceEngine self)
    {
    return AtResequenceEngineManagerGet(self);
    }

static uint32 FlowControlOffset(AtEthFlow flow, AtResequenceEngine self)
    {
    return (AtChannelHwIdGet((AtChannel)flow) + AtResequenceManagerBaseAddress(Manager(self)));
    }

static uint32 ResequenceOffset(AtResequenceEngine self)
    {
    return (AtResequenceEngineIdGet(self) + AtResequenceManagerBaseAddress(Manager(self)));
    }

static void EthFlowRegsShow(ThaModuleMpig self, AtEthFlow flow)
    {
    uint32 regAddr, regVal;
    AtResequenceEngine engine = Tha60210012EthFlowResequenceEngineGet(flow);
    AtUnused(self);

    regAddr = cAf6Reg_upen_cfgrslk_Base + FlowControlOffset(flow, engine);
    regVal  = AtResequenceEngineRead(engine, regAddr);
    Tha6021DebugPrintRegName(NULL, "IGRS Eth_flow control", regAddr, regVal);

    regAddr = cAf6Reg_upen_cfgrfsc_Base + ResequenceOffset(engine);;
    regVal  = AtResequenceEngineRead(engine, regAddr);
    Tha6021DebugPrintRegName(NULL, "IGRS Re-Order Buffer Space", regAddr, regVal);

    regAddr = cAf6Reg_upen_cfgrsqc_Base + ResequenceOffset(engine);
    regVal  = AtResequenceEngineRead(engine, regAddr);
    Tha6021DebugPrintRegName(NULL, "IGRS Re-Order Control", regAddr, regVal);

    regAddr = cAf6Reg_upen_cfgpkac_Base + ResequenceOffset(engine);
    regVal  = AtResequenceEngineRead(engine, regAddr);
    Tha6021DebugPrintRegName(NULL, "IGRS Pkt-Assemble Control", regAddr, regVal);

    regAddr = cAf6Reg_upen_igrsctl_0_Base + AtResequenceManagerBaseAddress(Manager(engine));
    regVal  = AtResequenceEngineRead(engine, regAddr);
    Tha6021DebugPrintRegName(NULL, "IGRS Control 0", regAddr, regVal);
    }

static uint32 BaseAddress(Tha60210012ModuleMpig self)
    {
    AtUnused(self);
    return 0xA20000UL;
    }

static uint32 QueueThresholdOffset(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId)
    {
    AtUnused(ethPortId);
    return mMethodsGet(self)->BaseAddress(self) + queueId;
    }

static void MethodsInit(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, QueueMinThresholdSet);
        mMethodOverride(m_methods, QueueMinInternalThresholdGet);
        mMethodOverride(m_methods, QueueMinExternalThresholdGet);
        mMethodOverride(m_methods, QueueEnable);
        mMethodOverride(m_methods, QueueIsEnabled);
        mMethodOverride(m_methods, QueueHybridEnable);
        mMethodOverride(m_methods, QueueHybridIsEnabled);
        mMethodOverride(m_methods, QueueQuantumInByteSet);
        mMethodOverride(m_methods, QueueQuantumInByteGet);
        mMethodOverride(m_methods, QueuePrioritySet);
        mMethodOverride(m_methods, QueuePriorityGet);
        mMethodOverride(m_methods, QueueCounterDebug);
        mMethodOverride(m_methods, DefaultPriorityOfQueue);
        mMethodOverride(m_methods, QueueThresholdOffset);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        mMethodOverride(m_AtModuleOverride, MemoryCanBeTested);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleMpig(AtModule self)
    {
    ThaModuleMpig MpigModule = (ThaModuleMpig)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMpigOverride, mMethodsGet(MpigModule), sizeof(m_ThaModuleMpigOverride));

        mMethodOverride(m_ThaModuleMpigOverride, EthFlowRegsShow);
        }

    mMethodsSet(MpigModule, &m_ThaModuleMpigOverride);
    }


static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleMpig(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleMpig);
    }

AtModule Tha60210012ModuleMpigObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleMpigObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210012ModuleMpigNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModuleMpigObjectInit(newModule, device);
    }

eAtRet Tha60210012ModuleMpigQueueMinThresholdSet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, uint32 internalThreshold, uint32 externalThreshold)
    {
    if (self)
        return mMethodsGet(self)->QueueMinThresholdSet(self, ethPortId, queueId, internalThreshold, externalThreshold);

    return cAtErrorObjectNotExist;
    }

uint32 Tha60210012ModuleMpigQueueMinInternalThresholdGet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId)
    {
    if (self)
        return mMethodsGet(self)->QueueMinInternalThresholdGet(self, ethPortId, queueId);

    return 0;
    }

uint32 Tha60210012ModuleMpigQueueMinExternalThresholdGet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId)
    {
    if (self)
        return mMethodsGet(self)->QueueMinExternalThresholdGet(self, ethPortId, queueId);

    return 0;
    }

eAtRet Tha60210012ModuleMpigQueueEnable(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->QueueEnable(self, ethPortId, queueId, enable);

    return cAtErrorObjectNotExist;
    }

eBool Tha60210012ModuleMpigQueueIsEnabled(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId)
    {
    if (self)
        return mMethodsGet(self)->QueueIsEnabled(self, ethPortId, queueId);

    return 0;
    }

eAtRet Tha60210012ModuleMpigHybridQueueingEnable(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->QueueHybridEnable(self, ethPortId, queueId, enable);

    return cAtErrorObjectNotExist;
    }

eBool Tha60210012ModuleMpigHybribQueueingIsEnabled(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId)
    {
    if (self)
        return mMethodsGet(self)->QueueHybridIsEnabled(self, ethPortId, queueId);

    return 0;
    }

eAtRet Tha60210012ModuleMpigQueueQuantumSet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, uint32 quantumInByte)
    {
    if (self)
        return mMethodsGet(self)->QueueQuantumInByteSet(self, ethPortId, queueId, quantumInByte);

    return cAtErrorObjectNotExist;
    }

uint32 Tha60210012ModuleMpigQueueQuantumGet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId)
    {
    if (self)
        return mMethodsGet(self)->QueueQuantumInByteGet(self, ethPortId, queueId);

    return 0;
    }

eAtRet Tha60210012ModuleMpigQueuePrioritySet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, eThaMpigQueuePriority priority)
    {
    if (self)
        return mMethodsGet(self)->QueuePrioritySet(self, ethPortId, queueId, priority);

    return cAtErrorObjectNotExist;
    }

eThaMpigQueuePriority Tha60210012ModuleMpigQueuePriorityGet(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId)
    {
    if (self)
        return mMethodsGet(self)->QueuePriorityGet(self, ethPortId, queueId);

    return cThaMpigQueuePriorityUnknown;
    }

eAtRet Tha60210012ModuleMpigQueueCounterDebug(Tha60210012ModuleMpig self, uint32 queueId, tTha60210012MpigQueueCounter* counters, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->QueueCounterDebug(self, queueId, counters, r2c);

    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ModuleMpigGlobalCounterDebug(Tha60210012ModuleMpig self, tTha60210012MpigGlobalCounter* counters, eBool r2c)
    {
    if (self)
        return GlobalCounterDebug(self, counters, r2c);

    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ModuleMpigDumpInfoDebugEnable(Tha60210012ModuleMpig self, eBool enable)
    {
    if (self)
        return DumpInfoDebugEnable(self, enable);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ModuleMpigDumpInfoDebug(Tha60210012ModuleMpig self)
    {
    if (self)
        return DumpInfoDebug(self);

    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ModuleMpigTransmitOneshotEnable(Tha60210012ModuleMpig self, eBool enable)
    {
    if (self)
        return TransmitOneshotEnable(self, enable);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ModuleMpigTransmitOneshotNumPktSet(Tha60210012ModuleMpig self, uint32 numPkt)
    {
    if (self)
        return TransmitOneshotNumPktSet(self, numPkt);

    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ModuleMpigTransmitOneshotNumPktStoreBeforeTransmitSet(Tha60210012ModuleMpig self, uint32 numPkt)
    {
    if (self)
        return TransmitOneshotNumPktStoreBeforeTransmitSet(self, numPkt);

    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ModuleMpigMonitorPacketSet(Tha60210012ModuleMpig self, uint16 channelId, uint16 pktLen)
    {
    if (self)
        return MonitorPacketSet(self, channelId, pktLen);

    return cAtErrorObjectNotExist;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Mpig
 * 
 * File        : Tha60210012ModuleMpigReg.h
 * 
 * Created Date: Mar 16, 2016
 *
 * Description : Mpig module registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEMPIGREG_H_
#define _THA60210012MODULEMPIGREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : MPIG Global Configuration
Reg Addr   : 0x9001
Reg Formula: 0x9001
    Where  :
Reg Desc   :
Used to configure MPIG global

------------------------------------------------------------------------------*/
#define cAf6Reg_cfg0_pen_Base                                                                           0x9001
#define cAf6Reg_cfg0_pen                                                                              0x9001UL
#define cAf6Reg_cfg0_pen_WidthVal                                                                           32
#define cAf6Reg_cfg0_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: MPIG_active
BitField Type: R/W
BitField Desc: MPIG Active
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_cfg0_pen_MPIG_active_Bit_Start                                                                  0
#define cAf6_cfg0_pen_MPIG_active_Bit_End                                                                    0
#define cAf6_cfg0_pen_MPIG_active_Mask                                                                   cBit0
#define cAf6_cfg0_pen_MPIG_active_Shift                                                                      0
#define cAf6_cfg0_pen_MPIG_active_MaxVal                                                                   0x1
#define cAf6_cfg0_pen_MPIG_active_MinVal                                                                   0x0
#define cAf6_cfg0_pen_MPIG_active_RstVal                                                                   0x1

/*------------------------------------------------------------------------------
Reg Name   : MPIG Configure 1 for debug
Reg Addr   : 0x2902
Reg Formula: 0x2902
    Where  :
Reg Desc   :
Used to Dump Enable, test on shot for engine MPIG

------------------------------------------------------------------------------*/
#define cAf6Reg_cfg2_pen_Base                                                                           0x2902
#define cAf6Reg_cfg2_pen                                                                              0x2902UL
#define cAf6Reg_cfg2_pen_WidthVal                                                                           32
#define cAf6Reg_cfg2_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: MPIG_dump_en
BitField Type: R/W
BitField Desc: MPIG Dump Enable
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_cfg2_pen_MPIG_dump_en_Bit_Start                                                                25
#define cAf6_cfg2_pen_MPIG_dump_en_Bit_End                                                                  25
#define cAf6_cfg2_pen_MPIG_dump_en_Mask                                                                 cBit25
#define cAf6_cfg2_pen_MPIG_dump_en_Shift                                                                    25
#define cAf6_cfg2_pen_MPIG_dump_en_MaxVal                                                                  0x1
#define cAf6_cfg2_pen_MPIG_dump_en_MinVal                                                                  0x0
#define cAf6_cfg2_pen_MPIG_dump_en_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: MPIG_oneshot_en
BitField Type: R/W
BitField Desc: MPIG One Shot Enable
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_cfg2_pen_MPIG_oneshot_en_Bit_Start                                                             24
#define cAf6_cfg2_pen_MPIG_oneshot_en_Bit_End                                                               24
#define cAf6_cfg2_pen_MPIG_oneshot_en_Mask                                                              cBit24
#define cAf6_cfg2_pen_MPIG_oneshot_en_Shift                                                                 24
#define cAf6_cfg2_pen_MPIG_oneshot_en_MaxVal                                                               0x1
#define cAf6_cfg2_pen_MPIG_oneshot_en_MinVal                                                               0x0
#define cAf6_cfg2_pen_MPIG_oneshot_en_RstVal                                                               0x0

/*--------------------------------------
BitField Name: MPIG_NumPkt_Accept
BitField Type: R/W
BitField Desc: MPIG Number Packet Accept,
BitField Bits: [23:08]
--------------------------------------*/
#define cAf6_cfg2_pen_MPIG_NumPkt_Accept_Bit_Start                                                           8
#define cAf6_cfg2_pen_MPIG_NumPkt_Accept_Bit_End                                                            23
#define cAf6_cfg2_pen_MPIG_NumPkt_Accept_Mask                                                         cBit23_8
#define cAf6_cfg2_pen_MPIG_NumPkt_Accept_Shift                                                               8
#define cAf6_cfg2_pen_MPIG_NumPkt_Accept_MaxVal                                                         0xffff
#define cAf6_cfg2_pen_MPIG_NumPkt_Accept_MinVal                                                            0x0
#define cAf6_cfg2_pen_MPIG_NumPkt_Accept_RstVal                                                            0x0

/*--------------------------------------
BitField Name: MPIG_NumPkt_Store_Before_Transmit
BitField Type: R/W
BitField Desc: MPIG Number Packet Store before transmiting,
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_cfg2_pen_MPIG_NumPkt_Store_Before_Transmit_Bit_Start                                            0
#define cAf6_cfg2_pen_MPIG_NumPkt_Store_Before_Transmit_Bit_End                                              7
#define cAf6_cfg2_pen_MPIG_NumPkt_Store_Before_Transmit_Mask                                           cBit7_0
#define cAf6_cfg2_pen_MPIG_NumPkt_Store_Before_Transmit_Shift                                                0
#define cAf6_cfg2_pen_MPIG_NumPkt_Store_Before_Transmit_MaxVal                                            0xff
#define cAf6_cfg2_pen_MPIG_NumPkt_Store_Before_Transmit_MinVal                                             0x0
#define cAf6_cfg2_pen_MPIG_NumPkt_Store_Before_Transmit_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : MPIG Global Configuration
Reg Addr   : 0x2903
Reg Formula: 0x2903
    Where  :
Reg Desc   :
Used to Configure Monitor length input and output MPIG and sticky other register

------------------------------------------------------------------------------*/
#define cAf6Reg_cfg3_pen_Base                                                                           0x2903
#define cAf6Reg_cfg3_pen                                                                              0x2903UL
#define cAf6Reg_cfg3_pen_WidthVal                                                                           32
#define cAf6Reg_cfg3_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: MPIG_ChannelID
BitField Type: R/W
BitField Desc: MPIG Configure Channel ID for Monitor
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_cfg3_pen_MPIG_ChannelID_Bit_Start                                                              16
#define cAf6_cfg3_pen_MPIG_ChannelID_Bit_End                                                                31
#define cAf6_cfg3_pen_MPIG_ChannelID_Mask                                                            cBit31_16
#define cAf6_cfg3_pen_MPIG_ChannelID_Shift                                                                  16
#define cAf6_cfg3_pen_MPIG_ChannelID_MaxVal                                                             0xffff
#define cAf6_cfg3_pen_MPIG_ChannelID_MinVal                                                                0x0
#define cAf6_cfg3_pen_MPIG_ChannelID_RstVal                                                                0x0

/*--------------------------------------
BitField Name: MPIG_Length
BitField Type: R/W
BitField Desc: MPIG Configure length for Monitor
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_cfg3_pen_MPIG_Length_Bit_Start                                                                  0
#define cAf6_cfg3_pen_MPIG_Length_Bit_End                                                                   15
#define cAf6_cfg3_pen_MPIG_Length_Mask                                                                cBit15_0
#define cAf6_cfg3_pen_MPIG_Length_Shift                                                                      0
#define cAf6_cfg3_pen_MPIG_Length_MaxVal                                                                0xffff
#define cAf6_cfg3_pen_MPIG_Length_MinVal                                                                   0x0
#define cAf6_cfg3_pen_MPIG_Length_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : MPIG Input Packet Information Dump
Reg Addr   : 0xA000-0xA0FF
Reg Formula: 0xA000+mem_id
    Where  :
           + mem_id : Memory address
Reg Desc   :
Used to Dump Input Packet Information

------------------------------------------------------------------------------*/
#define cAf6Reg_dum0_pen_Base                                                                           0xA000
#define cAf6Reg_dum0_pen(memid)                                                            (0xA000UL+(memid))
#define cAf6Reg_dum0_pen_WidthVal                                                                           32
#define cAf6Reg_dum0_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: Input_PLA_Packet_Length_ID
BitField Type: R/W
BitField Desc: Input PLA packet Length
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_dum0_pen_Input_PLA_Packet_Length_ID_Bit_Start                                                  16
#define cAf6_dum0_pen_Input_PLA_Packet_Length_ID_Bit_End                                                    31
#define cAf6_dum0_pen_Input_PLA_Packet_Length_ID_Mask                                                cBit31_16
#define cAf6_dum0_pen_Input_PLA_Packet_Length_ID_Shift                                                      16
#define cAf6_dum0_pen_Input_PLA_Packet_Length_ID_MaxVal                                                 0xffff
#define cAf6_dum0_pen_Input_PLA_Packet_Length_ID_MinVal                                                    0x0
#define cAf6_dum0_pen_Input_PLA_Packet_Length_ID_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Input_PLA_Channel_ID
BitField Type: R/W
BitField Desc: Input PLA channel ID
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_dum0_pen_Input_PLA_Channel_ID_Bit_Start                                                         0
#define cAf6_dum0_pen_Input_PLA_Channel_ID_Bit_End                                                          15
#define cAf6_dum0_pen_Input_PLA_Channel_ID_Mask                                                       cBit15_0
#define cAf6_dum0_pen_Input_PLA_Channel_ID_Shift                                                             0
#define cAf6_dum0_pen_Input_PLA_Channel_ID_MaxVal                                                       0xffff
#define cAf6_dum0_pen_Input_PLA_Channel_ID_MinVal                                                          0x0
#define cAf6_dum0_pen_Input_PLA_Channel_ID_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : MPIG Output Packet Information Dump
Reg Addr   : 0xA100-0xA1FF
Reg Formula: 0xA100+mem_id
    Where  :
           + mem_id : Memory address
Reg Desc   :
Used to Dump Output Packet Information

------------------------------------------------------------------------------*/
#define cAf6Reg_dum1_pen_Base                                                                           0xA100
#define cAf6Reg_dum1_pen(memid)                                                            (0xA100UL+(memid))
#define cAf6Reg_dum1_pen_WidthVal                                                                           32
#define cAf6Reg_dum1_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: Output_PLA_Packet_Length_ID
BitField Type: R/W
BitField Desc: Output PLA packet Length
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_dum1_pen_Output_PLA_Packet_Length_ID_Bit_Start                                                 16
#define cAf6_dum1_pen_Output_PLA_Packet_Length_ID_Bit_End                                                   31
#define cAf6_dum1_pen_Output_PLA_Packet_Length_ID_Mask                                               cBit31_16
#define cAf6_dum1_pen_Output_PLA_Packet_Length_ID_Shift                                                     16
#define cAf6_dum1_pen_Output_PLA_Packet_Length_ID_MaxVal                                                0xffff
#define cAf6_dum1_pen_Output_PLA_Packet_Length_ID_MinVal                                                   0x0
#define cAf6_dum1_pen_Output_PLA_Packet_Length_ID_RstVal                                                   0x0

/*--------------------------------------
BitField Name: Output_PLA_Channel_ID
BitField Type: R/W
BitField Desc: Output PLA channel ID
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_dum1_pen_Output_PLA_Channel_ID_Bit_Start                                                        0
#define cAf6_dum1_pen_Output_PLA_Channel_ID_Bit_End                                                         15
#define cAf6_dum1_pen_Output_PLA_Channel_ID_Mask                                                      cBit15_0
#define cAf6_dum1_pen_Output_PLA_Channel_ID_Shift                                                            0
#define cAf6_dum1_pen_Output_PLA_Channel_ID_MaxVal                                                      0xffff
#define cAf6_dum1_pen_Output_PLA_Channel_ID_MinVal                                                         0x0
#define cAf6_dum1_pen_Output_PLA_Channel_ID_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : MPIG Input Packet Information Dump
Reg Addr   : 0xA200-0xA2FF
Reg Formula: 0xA200+mem_id
    Where  :
           + mem_id : Memory address
Reg Desc   :
Used to Dump Output Packet Information

------------------------------------------------------------------------------*/
#define cAf6Reg_dum2_pen_Base                                                                           0xA200
#define cAf6Reg_dum2_pen(memid)                                                            (0xA200UL+(memid))
#define cAf6Reg_dum2_pen_WidthVal                                                                           32
#define cAf6Reg_dum2_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: DDR_Write_Packet_Length
BitField Type: R/W
BitField Desc: DDR_Write Packet Length
BitField Bits: [27:12]
--------------------------------------*/
#define cAf6_dum2_pen_DDR_Write_Packet_Length_Bit_Start                                                     12
#define cAf6_dum2_pen_DDR_Write_Packet_Length_Bit_End                                                       27
#define cAf6_dum2_pen_DDR_Write_Packet_Length_Mask                                                   cBit27_12
#define cAf6_dum2_pen_DDR_Write_Packet_Length_Shift                                                         12
#define cAf6_dum2_pen_DDR_Write_Packet_Length_MaxVal                                                    0xffff
#define cAf6_dum2_pen_DDR_Write_Packet_Length_MinVal                                                       0x0
#define cAf6_dum2_pen_DDR_Write_Packet_Length_RstVal                                                       0x0

/*--------------------------------------
BitField Name: DDR_Write_Queue_ID
BitField Type: R/W
BitField Desc: DDR_Write MPIG Queue ID
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_dum2_pen_DDR_Write_Queue_ID_Bit_Start                                                           8
#define cAf6_dum2_pen_DDR_Write_Queue_ID_Bit_End                                                            11
#define cAf6_dum2_pen_DDR_Write_Queue_ID_Mask                                                         cBit11_8
#define cAf6_dum2_pen_DDR_Write_Queue_ID_Shift                                                               8
#define cAf6_dum2_pen_DDR_Write_Queue_ID_MaxVal                                                            0xf
#define cAf6_dum2_pen_DDR_Write_Queue_ID_MinVal                                                            0x0
#define cAf6_dum2_pen_DDR_Write_Queue_ID_RstVal                                                            0x0

/*--------------------------------------
BitField Name: DDR_Write_Sequence
BitField Type: R/W
BitField Desc: DDR Write Sequence
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_dum2_pen_DDR_Write_Sequence_Bit_Start                                                           0
#define cAf6_dum2_pen_DDR_Write_Sequence_Bit_End                                                             7
#define cAf6_dum2_pen_DDR_Write_Sequence_Mask                                                          cBit7_0
#define cAf6_dum2_pen_DDR_Write_Sequence_Shift                                                               0
#define cAf6_dum2_pen_DDR_Write_Sequence_MaxVal                                                           0xff
#define cAf6_dum2_pen_DDR_Write_Sequence_MinVal                                                            0x0
#define cAf6_dum2_pen_DDR_Write_Sequence_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : MPIG Input Packet Information Dump
Reg Addr   : 0xA300-0xA3FF
Reg Formula: 0xA300+mem_id
    Where  :
           + mem_id : Memory address
Reg Desc   :
Used to Dump Output Packet Information

------------------------------------------------------------------------------*/
#define cAf6Reg_dum3_pen_Base                                                                           0xA300
#define cAf6Reg_dum3_pen(memid)                                                            (0xA300UL+(memid))
#define cAf6Reg_dum3_pen_WidthVal                                                                           32
#define cAf6Reg_dum3_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: DDR_Read_Packet_Length
BitField Type: R/W
BitField Desc: DDR_Write Packet Length
BitField Bits: [27:12]
--------------------------------------*/
#define cAf6_dum3_pen_DDR_Read_Packet_Length_Bit_Start                                                      12
#define cAf6_dum3_pen_DDR_Read_Packet_Length_Bit_End                                                        27
#define cAf6_dum3_pen_DDR_Read_Packet_Length_Mask                                                    cBit27_12
#define cAf6_dum3_pen_DDR_Read_Packet_Length_Shift                                                          12
#define cAf6_dum3_pen_DDR_Read_Packet_Length_MaxVal                                                     0xffff
#define cAf6_dum3_pen_DDR_Read_Packet_Length_MinVal                                                        0x0
#define cAf6_dum3_pen_DDR_Read_Packet_Length_RstVal                                                        0x0

/*--------------------------------------
BitField Name: DDR_Read_Queue_ID
BitField Type: R/W
BitField Desc: DDR_Write MPIG Queue ID
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_dum3_pen_DDR_Read_Queue_ID_Bit_Start                                                            8
#define cAf6_dum3_pen_DDR_Read_Queue_ID_Bit_End                                                             11
#define cAf6_dum3_pen_DDR_Read_Queue_ID_Mask                                                          cBit11_8
#define cAf6_dum3_pen_DDR_Read_Queue_ID_Shift                                                                8
#define cAf6_dum3_pen_DDR_Read_Queue_ID_MaxVal                                                             0xf
#define cAf6_dum3_pen_DDR_Read_Queue_ID_MinVal                                                             0x0
#define cAf6_dum3_pen_DDR_Read_Queue_ID_RstVal                                                             0x0

/*--------------------------------------
BitField Name: DDR_Read_Sequence
BitField Type: R/W
BitField Desc: DDR Write Sequence
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_dum3_pen_DDR_Read_Sequence_Bit_Start                                                            0
#define cAf6_dum3_pen_DDR_Read_Sequence_Bit_End                                                              7
#define cAf6_dum3_pen_DDR_Read_Sequence_Mask                                                           cBit7_0
#define cAf6_dum3_pen_DDR_Read_Sequence_Shift                                                                0
#define cAf6_dum3_pen_DDR_Read_Sequence_MaxVal                                                            0xff
#define cAf6_dum3_pen_DDR_Read_Sequence_MinVal                                                             0x0
#define cAf6_dum3_pen_DDR_Read_Sequence_RstVal                                                             0x0

/*------------------------------------------------------------------------------
Reg Name   : Queue Threshold Control
Reg Addr   : 0x1000-0x1007
Reg Formula: 0x1000+$qid
    Where  :
           + qid(0-7) : Queue ID
Reg Desc   :
Used to configure threshold for Queue
: {qid(0-7)} -> 8 Queues

------------------------------------------------------------------------------*/
#define cAf6Reg_cfg0qthsh_pen_Base                                                                      0x1000
#define cAf6Reg_cfg0qthsh_pen(qid)                                                            (0x1000UL+(qid))
#define cAf6Reg_cfg0qthsh_pen_WidthVal                                                                      32
#define cAf6Reg_cfg0qthsh_pen_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: Queue_Hyb
BitField Type: R/W
BitField Desc: Hybrib Queueing 0: Internal Queue Only (256 packets per Queue) 1:
Hybrib Queueing, External (256K packets per Queue) , Internal Queue (256 packets
per Queue)
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_cfg0qthsh_pen_Queue_Hyb_Bit_Start                                                              31
#define cAf6_cfg0qthsh_pen_Queue_Hyb_Bit_End                                                                31
#define cAf6_cfg0qthsh_pen_Queue_Hyb_Mask                                                               cBit31
#define cAf6_cfg0qthsh_pen_Queue_Hyb_Shift                                                                  31
#define cAf6_cfg0qthsh_pen_Queue_Hyb_MaxVal                                                                0x1
#define cAf6_cfg0qthsh_pen_Queue_Hyb_MinVal                                                                0x0
#define cAf6_cfg0qthsh_pen_Queue_Hyb_RstVal                                                                0x0

/*--------------------------------------
BitField Name: Queue_Enb
BitField Type: R/W
BitField Desc: Queue Enable 0: Disable Queue and flush Queue 1: Enable Queue
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_cfg0qthsh_pen_Queue_Enb_Bit_Start                                                              30
#define cAf6_cfg0qthsh_pen_Queue_Enb_Bit_End                                                                30
#define cAf6_cfg0qthsh_pen_Queue_Enb_Mask                                                               cBit30
#define cAf6_cfg0qthsh_pen_Queue_Enb_Shift                                                                  30
#define cAf6_cfg0qthsh_pen_Queue_Enb_MaxVal                                                                0x1
#define cAf6_cfg0qthsh_pen_Queue_Enb_MinVal                                                                0x0
#define cAf6_cfg0qthsh_pen_Queue_Enb_RstVal                                                                0x0

/*--------------------------------------
BitField Name: EFFQthsh_min
BitField Type: R/W
BitField Desc: External FiFo minimun queue threshold, step 32 pkts
BitField Bits: [29:10]
--------------------------------------*/
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_Bit_Start                                                           10
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_Bit_End                                                             29
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_Mask                                                         cBit29_10
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_Shift                                                               10
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_MaxVal                                                         0xfffff
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_MinVal                                                             0x0
#define cAf6_cfg0qthsh_pen_EFFQthsh_min_RstVal                                                          0x1FF0

/*--------------------------------------
BitField Name: IFFQthsh_min
BitField Type: R/W
BitField Desc: Internal FiFo minimun queue threshold, step 1 pkts
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_Bit_Start                                                            0
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_Bit_End                                                              9
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_Mask                                                           cBit9_0
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_Shift                                                                0
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_MaxVal                                                           0x3ff
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_MinVal                                                             0x0
#define cAf6_cfg0qthsh_pen_IFFQthsh_min_RstVal                                                            0xF0


/*------------------------------------------------------------------------------
Reg Name   : Scheduler: Group priority Configuration
Reg Addr   : 0x0080
Reg Formula: 0x0080
    Where  :
Reg Desc   :
Used to configure the priority queue
- Support 3 group: Low Priority(LSP), High Priority(HSP), DWRR, mix mode.
- HSP: Higest Priority
- LSP: Lowest Priority
- DWRR: Medium Priority

------------------------------------------------------------------------------*/
#define cAf6Reg_schelcfg0_pen_Base                                                                      0x0080
#define cAf6Reg_schelcfg0_pen                                                                         0x0080UL
#define cAf6Reg_schelcfg0_pen_WidthVal                                                                      32
#define cAf6Reg_schelcfg0_pen_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: Hsp_grp
BitField Type: R/W
BitField Desc: HSP Queue
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_schelcfg0_pen_Hsp_grp_Bit_Start                                                                16
#define cAf6_schelcfg0_pen_Hsp_grp_Bit_End                                                                  23
#define cAf6_schelcfg0_pen_Hsp_grp_Mask                                                              cBit23_16
#define cAf6_schelcfg0_pen_Hsp_grp_Shift                                                                    16
#define cAf6_schelcfg0_pen_Hsp_grp_MaxVal                                                                 0xff
#define cAf6_schelcfg0_pen_Hsp_grp_MinVal                                                                  0x0
#define cAf6_schelcfg0_pen_Hsp_grp_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: DWRR_grp
BitField Type: R/W
BitField Desc: DWRR Queue
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_schelcfg0_pen_DWRR_grp_Bit_Start                                                                8
#define cAf6_schelcfg0_pen_DWRR_grp_Bit_End                                                                 15
#define cAf6_schelcfg0_pen_DWRR_grp_Mask                                                              cBit15_8
#define cAf6_schelcfg0_pen_DWRR_grp_Shift                                                                    8
#define cAf6_schelcfg0_pen_DWRR_grp_MaxVal                                                                0xff
#define cAf6_schelcfg0_pen_DWRR_grp_MinVal                                                                 0x0
#define cAf6_schelcfg0_pen_DWRR_grp_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: Lsp_grp
BitField Type: R/W
BitField Desc: LSP Queue
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_schelcfg0_pen_Lsp_grp_Bit_Start                                                                 0
#define cAf6_schelcfg0_pen_Lsp_grp_Bit_End                                                                   7
#define cAf6_schelcfg0_pen_Lsp_grp_Mask                                                                cBit7_0
#define cAf6_schelcfg0_pen_Lsp_grp_Shift                                                                     0
#define cAf6_schelcfg0_pen_Lsp_grp_MaxVal                                                                 0xff
#define cAf6_schelcfg0_pen_Lsp_grp_MinVal                                                                  0x0
#define cAf6_schelcfg0_pen_Lsp_grp_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : DWRR Quantum Configuration
Reg Addr   : 0x0000-0x0007
Reg Formula: 0x0000+$qid
    Where  :
           + qid(0-7) : Queue ID
Reg Desc   :
Used to configure quantum for queue in DWRR scheduling mode

------------------------------------------------------------------------------*/
#define cAf6Reg_schelinkicfg1_pen_Base                                                                  0x0000
#define cAf6Reg_schelinkicfg1_pen(qid)                                                        (0x0000UL+(qid))
#define cAf6Reg_schelinkicfg1_pen_WidthVal                                                                  32
#define cAf6Reg_schelinkicfg1_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: Linkshce_DWRR_QTum
BitField Type: R/W
BitField Desc: DWRR Quantum,Minximun MTU
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_Bit_Start                                                  0
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_Bit_End                                                   19
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_Mask                                                cBit19_0
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_Shift                                                      0
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_MaxVal                                               0xfffff
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_MinVal                                                   0x0
#define cAf6_schelinkicfg1_pen_Linkshce_DWRR_QTum_RstVal                                                   0x0

/*------------------------------------------------------------------------------
Reg Name   : HW Internal Queue Status
Reg Addr   : 0x29010-0x29017
Reg Formula: 0x29010+$qid
    Where  :
           + qid(0-7) : Queue ID
Reg Desc   :
Used to read HW External Queue Status
: {qid(0-7)} -> 8 Queues

------------------------------------------------------------------------------*/
#define cAf6Reg_sta0_upen_Base                                                                         0x29010
#define cAf6Reg_sta0_upen(qid)                                                               (0x29010UL+(qid))
#define cAf6Reg_sta0_upen_WidthVal                                                                          32
#define cAf6Reg_sta0_upen_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: IFFQ_Write_Cnt
BitField Type: RO
BitField Desc: Internal FiFo Queue Write Counter
BitField Bits: [26:19]
--------------------------------------*/
#define cAf6_sta0_upen_IFFQ_Write_Cnt_Bit_Start                                                             19
#define cAf6_sta0_upen_IFFQ_Write_Cnt_Bit_End                                                               26
#define cAf6_sta0_upen_IFFQ_Write_Cnt_Mask                                                           cBit26_19
#define cAf6_sta0_upen_IFFQ_Write_Cnt_Shift                                                                 19
#define cAf6_sta0_upen_IFFQ_Write_Cnt_MaxVal                                                              0xff
#define cAf6_sta0_upen_IFFQ_Write_Cnt_MinVal                                                               0x0
#define cAf6_sta0_upen_IFFQ_Write_Cnt_RstVal                                                              0x00

/*--------------------------------------
BitField Name: IFFQ_Write_Len
BitField Type: RO
BitField Desc: Internal FiFo Queue Write Lenght
BitField Bits: [18:10]
--------------------------------------*/
#define cAf6_sta0_upen_IFFQ_Write_Len_Bit_Start                                                             10
#define cAf6_sta0_upen_IFFQ_Write_Len_Bit_End                                                               18
#define cAf6_sta0_upen_IFFQ_Write_Len_Mask                                                           cBit18_10
#define cAf6_sta0_upen_IFFQ_Write_Len_Shift                                                                 10
#define cAf6_sta0_upen_IFFQ_Write_Len_MaxVal                                                             0x1ff
#define cAf6_sta0_upen_IFFQ_Write_Len_MinVal                                                               0x0
#define cAf6_sta0_upen_IFFQ_Write_Len_RstVal                                                              0x00

/*--------------------------------------
BitField Name: IFFQ_Read_Len
BitField Type: RO
BitField Desc: Internal FiFo Queue Read Lenght
BitField Bits: [09:01]
--------------------------------------*/
#define cAf6_sta0_upen_IFFQ_Read_Len_Bit_Start                                                               1
#define cAf6_sta0_upen_IFFQ_Read_Len_Bit_End                                                                 9
#define cAf6_sta0_upen_IFFQ_Read_Len_Mask                                                              cBit9_1
#define cAf6_sta0_upen_IFFQ_Read_Len_Shift                                                                   1
#define cAf6_sta0_upen_IFFQ_Read_Len_MaxVal                                                              0x1ff
#define cAf6_sta0_upen_IFFQ_Read_Len_MinVal                                                                0x0
#define cAf6_sta0_upen_IFFQ_Read_Len_RstVal                                                               0x00

/*--------------------------------------
BitField Name: IFFQ_Ready
BitField Type: RO
BitField Desc: Internal FiFo Queue Ready
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_sta0_upen_IFFQ_Ready_Bit_Start                                                                  0
#define cAf6_sta0_upen_IFFQ_Ready_Bit_End                                                                    0
#define cAf6_sta0_upen_IFFQ_Ready_Mask                                                                   cBit0
#define cAf6_sta0_upen_IFFQ_Ready_Shift                                                                      0
#define cAf6_sta0_upen_IFFQ_Ready_MaxVal                                                                   0x1
#define cAf6_sta0_upen_IFFQ_Ready_MinVal                                                                   0x0
#define cAf6_sta0_upen_IFFQ_Ready_RstVal                                                                  0x00


/*------------------------------------------------------------------------------
Reg Name   : HW External Queue Status
Reg Addr   : 0x29018-0x2901F
Reg Formula: 0x29018+$qid
    Where  :
           + qid(0-7) : Queue ID
Reg Desc   :
Used to read HW External Queue Status
: {qid(0-7)} -> 8 Queues

------------------------------------------------------------------------------*/
#define cAf6Reg_sta1_upen_Base                                                                         0x29018
#define cAf6Reg_sta1_upen(qid)                                                               (0x29018UL+(qid))
#define cAf6Reg_sta1_upen_WidthVal                                                                          32
#define cAf6Reg_sta1_upen_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: EFFQ_Write_Pen_Len
BitField Type: RO
BitField Desc: External FiFo Queue Write Pending Lenght
BitField Bits: [21:18]
--------------------------------------*/
#define cAf6_sta1_upen_EFFQ_Write_Pen_Len_Bit_Start                                                         18
#define cAf6_sta1_upen_EFFQ_Write_Pen_Len_Bit_End                                                           21
#define cAf6_sta1_upen_EFFQ_Write_Pen_Len_Mask                                                       cBit21_18
#define cAf6_sta1_upen_EFFQ_Write_Pen_Len_Shift                                                             18
#define cAf6_sta1_upen_EFFQ_Write_Pen_Len_MaxVal                                                           0xf
#define cAf6_sta1_upen_EFFQ_Write_Pen_Len_MinVal                                                           0x0
#define cAf6_sta1_upen_EFFQ_Write_Pen_Len_RstVal                                                          0x00

/*--------------------------------------
BitField Name: EFFQ_Read_Pen_Len
BitField Type: RO
BitField Desc: External FiFo Queue Read Pending Lenght
BitField Bits: [17:14]
--------------------------------------*/
#define cAf6_sta1_upen_EFFQ_Read_Pen_Len_Bit_Start                                                          14
#define cAf6_sta1_upen_EFFQ_Read_Pen_Len_Bit_End                                                            17
#define cAf6_sta1_upen_EFFQ_Read_Pen_Len_Mask                                                        cBit17_14
#define cAf6_sta1_upen_EFFQ_Read_Pen_Len_Shift                                                              14
#define cAf6_sta1_upen_EFFQ_Read_Pen_Len_MaxVal                                                            0xf
#define cAf6_sta1_upen_EFFQ_Read_Pen_Len_MinVal                                                            0x0
#define cAf6_sta1_upen_EFFQ_Read_Pen_Len_RstVal                                                           0x00

/*--------------------------------------
BitField Name: EFFQ_Buf_Len
BitField Type: RO
BitField Desc: External FiFo Queue DDR Buffer Lenght
BitField Bits: [13:00]
--------------------------------------*/
#define cAf6_sta1_upen_EFFQ_Buf_Len_Bit_Start                                                                0
#define cAf6_sta1_upen_EFFQ_Buf_Len_Bit_End                                                                 13
#define cAf6_sta1_upen_EFFQ_Buf_Len_Mask                                                              cBit13_0
#define cAf6_sta1_upen_EFFQ_Buf_Len_Shift                                                                    0
#define cAf6_sta1_upen_EFFQ_Buf_Len_MaxVal                                                              0x3fff
#define cAf6_sta1_upen_EFFQ_Buf_Len_MinVal                                                                 0x0
#define cAf6_sta1_upen_EFFQ_Buf_Len_RstVal                                                                0x00

/*------------------------------------------------------------------------------
Reg Name   : Queue Packet Counter
Reg Addr   : 0x1400-0x147F
Reg Formula: 0x1400+ $r2c*64+$qid*8+$cntid
    Where  :
           + $r2c(0-1): read_to_clear 0-ro,1-r2c
           + $qid(0-7): queue id
           + $cntid(0-7) : counter id
Reg Desc   :
Support the following Queue Counter:
- cntid = 00 : Total good packet enqueue
- cntid = 01 : Internal Packet bypass to Internal FiFo directly
- cntid = 02 : Internal Packet bypass to Internal FiFo from Taill FiFo
- cntid = 03 : External Packet to Internal FiFo
- cntid = 04 : Discard because Queue Disable CFG
- cntid = 05 : Discard because Input(PLA,write cache) delare drop
- cntid = 06 : Discard because Internal FiFo full
- cntid = 07 : Discard because External FiFo full

------------------------------------------------------------------------------*/
#define cAf6Reg_pmcnt1_pen_Base                                                                         0x1400
#define cAf6Reg_pmcnt1_pen(r2c, qid, cntid)                            (0x1400UL+(r2c)*64UL+(qid)*8UL+(cntid))
#define cAf6Reg_pmcnt1_pen_WidthVal                                                                         32
#define cAf6Reg_pmcnt1_pen_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: queue_cnt
BitField Type: R/W
BitField Desc: Value of counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_pmcnt1_pen_queue_cnt_Bit_Start                                                                  0
#define cAf6_pmcnt1_pen_queue_cnt_Bit_End                                                                   31
#define cAf6_pmcnt1_pen_queue_cnt_Mask                                                                cBit31_0
#define cAf6_pmcnt1_pen_queue_cnt_Shift                                                                      0
#define cAf6_pmcnt1_pen_queue_cnt_MaxVal                                                            0xffffffff
#define cAf6_pmcnt1_pen_queue_cnt_MinVal                                                                   0x0
#define cAf6_pmcnt1_pen_queue_cnt_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Queue Byte Counter
Reg Addr   : 0x1500-0x157F
Reg Formula: 0x1500+ $r2c*64+$qid*8+$cntid
    Where  :
           + $r2c(0-1): read_to_clear 0-ro,1-r2c
           + $qid(0-7): queue id
           + $cntid(0-7) : counter id
Reg Desc   :
Support the following Queue Counter:
- cntid = 00 : Total good packet enqueue
- cntid = 01 : Internal Packet bypass to Internal FiFo directly
- cntid = 02 : Internal Packet bypass to Internal FiFo from Taill FiFo
- cntid = 03 : External Packet to Internal FiFo
- cntid = 04 : Discard because Queue Disable CFG
- cntid = 05 : Discard because Input(PLA,write cache) delare drop
- cntid = 06 : Discard because Internal FiFo full
- cntid = 07 : Discard because External FiFo full

------------------------------------------------------------------------------*/
#define cAf6Reg_pmcnt2_pen_Base                                                                         0x1500
#define cAf6Reg_pmcnt2_pen(r2c, qid, cntid)                            (0x1500UL+(r2c)*64UL+(qid)*8UL+(cntid))
#define cAf6Reg_pmcnt2_pen_WidthVal                                                                         32
#define cAf6Reg_pmcnt2_pen_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: queue_cnt
BitField Type: R/W
BitField Desc: Value of counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_pmcnt2_pen_queue_cnt_Bit_Start                                                                  0
#define cAf6_pmcnt2_pen_queue_cnt_Bit_End                                                                   31
#define cAf6_pmcnt2_pen_queue_cnt_Mask                                                                cBit31_0
#define cAf6_pmcnt2_pen_queue_cnt_Shift                                                                      0
#define cAf6_pmcnt2_pen_queue_cnt_MaxVal                                                            0xffffffff
#define cAf6_pmcnt2_pen_queue_cnt_MinVal                                                                   0x0
#define cAf6_pmcnt2_pen_queue_cnt_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Queue Byte Counter
Reg Addr   : 0x1600-0x167F
Reg Formula: 0x1600+ $r2c*64+$qid*8+$cntid
    Where  :
           + $r2c(0-1): read_to_clear 0-ro,1-r2c
           + $qid(0-7): queue id
           + $cntid(0-7) : counter id
Reg Desc   :
Support the following Queue Counter:
- cntid = 00 : Total good packet dequeue
- cntid = 01 : Total good end of packetdequeue
- cntid = 02 : Total good byte dequeue
- cntid = 03 : Reserved
- cntid = 04 : Reserved
- cntid = 05 : Reserved
- cntid = 06 : Reserved
- cntid = 07 : Reserved

------------------------------------------------------------------------------*/
#define cAf6Reg_pmcnt3_pen_Base                                                                         0x1600
#define cAf6Reg_pmcnt3_pen(r2c, qid, cntid)                            (0x1600UL+(r2c)*64UL+(qid)*8UL+(cntid))
#define cAf6Reg_pmcnt3_pen_WidthVal                                                                         32
#define cAf6Reg_pmcnt3_pen_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: queue_cnt
BitField Type: R/W
BitField Desc: Value of counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_pmcnt3_pen_queue_cnt_Bit_Start                                                                  0
#define cAf6_pmcnt3_pen_queue_cnt_Bit_End                                                                   31
#define cAf6_pmcnt3_pen_queue_cnt_Mask                                                                cBit31_0
#define cAf6_pmcnt3_pen_queue_cnt_Shift                                                                      0
#define cAf6_pmcnt3_pen_queue_cnt_MaxVal                                                            0xffffffff
#define cAf6_pmcnt3_pen_queue_cnt_MinVal                                                                   0x0
#define cAf6_pmcnt3_pen_queue_cnt_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : DDR Access Queue Counter
Reg Addr   : 0x9020-0x902F
Reg Formula: 0x9020+ $cntid
    Where  :
           + $cntid(0-15) : counter id
Reg Desc   :
Support Global Counter:
- cntid = 00 : MPIG enqueue Good Packet
- cntid = 01 : MPIG enqueue Drop Packet
- cntid = 02 : MPIG dequeue Good Packet
- cntid = 03 : MPIG request Write DDR
- cntid = 04 : MPIG request Read DDR
- cntid = 05 : MPIG Valid Write DDR
- cntid = 06 : MPIG Valid Read DDR
- cntid = 07 : MPIG ACK DDR
- cntid = 08 : QMXFF Write get Valid
- cntid = 09 : QMXFF Read get Valid(=3xMPIG request Read DDR)
- cntid = 10 : QMXFF Read get first Valid
- cntid = 11 : QMXFF Read get end Valid
- cntid = 12 : Total Packet to Head internal FiFo
- cntid = 13 : Packet Directly to head FiFo
- cntid = 14 : Packet from tail FiFo to head FiFo
- cntid = 15 : Total Discard

------------------------------------------------------------------------------*/
#define cAf6Reg_pmcnt4_pen_Base                                                                         0x9020
#define cAf6Reg_pmcnt4_pen(cntid)                                                           (0x9020UL+(cntid))
#define cAf6Reg_pmcnt4_pen_WidthVal                                                                         32
#define cAf6Reg_pmcnt4_pen_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: queue_cnt
BitField Type: R/W
BitField Desc: Value of counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_pmcnt4_pen_queue_cnt_Bit_Start                                                                  0
#define cAf6_pmcnt4_pen_queue_cnt_Bit_End                                                                   31
#define cAf6_pmcnt4_pen_queue_cnt_Mask                                                                cBit31_0
#define cAf6_pmcnt4_pen_queue_cnt_Shift                                                                      0
#define cAf6_pmcnt4_pen_queue_cnt_MaxVal                                                            0xffffffff
#define cAf6_pmcnt4_pen_queue_cnt_MinVal                                                                   0x0
#define cAf6_pmcnt4_pen_queue_cnt_RstVal                                                                   0x0

/*------------------------------------------------------------------------------
Reg Name   : Stick 0
Reg Addr   : 0x8000
Reg Formula:
    Where  :
Reg Desc   :
Used to debug MPEG

------------------------------------------------------------------------------*/
#define cAf6Reg_stk0_pen_Base                                                                           0x8000
#define cAf6Reg_stk0_pen                                                                              0x8000UL
#define cAf6Reg_stk0_pen_WidthVal                                                                           32
#define cAf6Reg_stk0_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: FiFo_Enqueue_Write_Error
BitField Type: R/W
BitField Desc: FiFo store enqueue QMXXF Write Err
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_stk0_pen_FiFo_Enqueue_Write_Error_Bit_Start                                                    16
#define cAf6_stk0_pen_FiFo_Enqueue_Write_Error_Bit_End                                                      16
#define cAf6_stk0_pen_FiFo_Enqueue_Write_Error_Mask                                                     cBit16
#define cAf6_stk0_pen_FiFo_Enqueue_Write_Error_Shift                                                        16
#define cAf6_stk0_pen_FiFo_Enqueue_Write_Error_MaxVal                                                      0x1
#define cAf6_stk0_pen_FiFo_Enqueue_Write_Error_MinVal                                                      0x0
#define cAf6_stk0_pen_FiFo_Enqueue_Write_Error_RstVal                                                      0x0

/*--------------------------------------
BitField Name: FiFo_Enqueue_Read_Error
BitField Type: R/W
BitField Desc: FiFo store enqueue QMXXF Read Err
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_stk0_pen_FiFo_Enqueue_Read_Error_Bit_Start                                                     15
#define cAf6_stk0_pen_FiFo_Enqueue_Read_Error_Bit_End                                                       15
#define cAf6_stk0_pen_FiFo_Enqueue_Read_Error_Mask                                                      cBit15
#define cAf6_stk0_pen_FiFo_Enqueue_Read_Error_Shift                                                         15
#define cAf6_stk0_pen_FiFo_Enqueue_Read_Error_MaxVal                                                       0x1
#define cAf6_stk0_pen_FiFo_Enqueue_Read_Error_MinVal                                                       0x0
#define cAf6_stk0_pen_FiFo_Enqueue_Read_Error_RstVal                                                       0x0

/*--------------------------------------
BitField Name: DDR_Read_Cache_Write
BitField Type: R/W
BitField Desc: DDR Read Cache Write
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_stk0_pen_DDR_Read_Cache_Write_Bit_Start                                                        14
#define cAf6_stk0_pen_DDR_Read_Cache_Write_Bit_End                                                          14
#define cAf6_stk0_pen_DDR_Read_Cache_Write_Mask                                                         cBit14
#define cAf6_stk0_pen_DDR_Read_Cache_Write_Shift                                                            14
#define cAf6_stk0_pen_DDR_Read_Cache_Write_MaxVal                                                          0x1
#define cAf6_stk0_pen_DDR_Read_Cache_Write_MinVal                                                          0x0
#define cAf6_stk0_pen_DDR_Read_Cache_Write_RstVal                                                          0x0

/*--------------------------------------
BitField Name: DDR_Write_Cache_Read
BitField Type: R/W
BitField Desc: DDR Write Cache Read
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_stk0_pen_DDR_Write_Cache_Read_Bit_Start                                                        13
#define cAf6_stk0_pen_DDR_Write_Cache_Read_Bit_End                                                          13
#define cAf6_stk0_pen_DDR_Write_Cache_Read_Mask                                                         cBit13
#define cAf6_stk0_pen_DDR_Write_Cache_Read_Shift                                                            13
#define cAf6_stk0_pen_DDR_Write_Cache_Read_MaxVal                                                          0x1
#define cAf6_stk0_pen_DDR_Write_Cache_Read_MinVal                                                          0x0
#define cAf6_stk0_pen_DDR_Write_Cache_Read_RstVal                                                          0x0

/*--------------------------------------
BitField Name: DDR_Return_Write_Valid
BitField Type: R/W
BitField Desc: DDR return Write Valid to MPIG
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_stk0_pen_DDR_Return_Write_Valid_Bit_Start                                                      12
#define cAf6_stk0_pen_DDR_Return_Write_Valid_Bit_End                                                        12
#define cAf6_stk0_pen_DDR_Return_Write_Valid_Mask                                                       cBit12
#define cAf6_stk0_pen_DDR_Return_Write_Valid_Shift                                                          12
#define cAf6_stk0_pen_DDR_Return_Write_Valid_MaxVal                                                        0x1
#define cAf6_stk0_pen_DDR_Return_Write_Valid_MinVal                                                        0x0
#define cAf6_stk0_pen_DDR_Return_Write_Valid_RstVal                                                        0x0

/*--------------------------------------
BitField Name: MPIG_Request_Write
BitField Type: R/W
BitField Desc: MPIG request Write DDR
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_stk0_pen_MPIG_Request_Write_Bit_Start                                                          11
#define cAf6_stk0_pen_MPIG_Request_Write_Bit_End                                                            11
#define cAf6_stk0_pen_MPIG_Request_Write_Mask                                                           cBit11
#define cAf6_stk0_pen_MPIG_Request_Write_Shift                                                              11
#define cAf6_stk0_pen_MPIG_Request_Write_MaxVal                                                            0x1
#define cAf6_stk0_pen_MPIG_Request_Write_MinVal                                                            0x0
#define cAf6_stk0_pen_MPIG_Request_Write_RstVal                                                            0x0

/*--------------------------------------
BitField Name: MPIG_Request_Read
BitField Type: R/W
BitField Desc: MPIG request Read  DDR
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_stk0_pen_MPIG_Request_Read_Bit_Start                                                           10
#define cAf6_stk0_pen_MPIG_Request_Read_Bit_End                                                             10
#define cAf6_stk0_pen_MPIG_Request_Read_Mask                                                            cBit10
#define cAf6_stk0_pen_MPIG_Request_Read_Shift                                                               10
#define cAf6_stk0_pen_MPIG_Request_Read_MaxVal                                                             0x1
#define cAf6_stk0_pen_MPIG_Request_Read_MinVal                                                             0x0
#define cAf6_stk0_pen_MPIG_Request_Read_RstVal                                                             0x0

/*--------------------------------------
BitField Name: DDR_Return_ACK
BitField Type: R/W
BitField Desc: DDR return ACK to MPIG
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_stk0_pen_DDR_Return_ACK_Bit_Start                                                               9
#define cAf6_stk0_pen_DDR_Return_ACK_Bit_End                                                                 9
#define cAf6_stk0_pen_DDR_Return_ACK_Mask                                                                cBit9
#define cAf6_stk0_pen_DDR_Return_ACK_Shift                                                                   9
#define cAf6_stk0_pen_DDR_Return_ACK_MaxVal                                                                0x1
#define cAf6_stk0_pen_DDR_Return_ACK_MinVal                                                                0x0
#define cAf6_stk0_pen_DDR_Return_ACK_RstVal                                                                0x0

/*--------------------------------------
BitField Name: DDR_Return_Read_Valid
BitField Type: R/W
BitField Desc: DDR return Read  Valid to MPIG
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_stk0_pen_DDR_Return_Read_Valid_Bit_Start                                                        8
#define cAf6_stk0_pen_DDR_Return_Read_Valid_Bit_End                                                          8
#define cAf6_stk0_pen_DDR_Return_Read_Valid_Mask                                                         cBit8
#define cAf6_stk0_pen_DDR_Return_Read_Valid_Shift                                                            8
#define cAf6_stk0_pen_DDR_Return_Read_Valid_MaxVal                                                         0x1
#define cAf6_stk0_pen_DDR_Return_Read_Valid_MinVal                                                         0x0
#define cAf6_stk0_pen_DDR_Return_Read_Valid_RstVal                                                         0x0

/*--------------------------------------
BitField Name: MPIG_Output_to_RSQ
BitField Type: R/W
BitField Desc: MPIG output to RSQ engine
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_stk0_pen_MPIG_Output_to_RSQ_Bit_Start                                                           5
#define cAf6_stk0_pen_MPIG_Output_to_RSQ_Bit_End                                                             5
#define cAf6_stk0_pen_MPIG_Output_to_RSQ_Mask                                                            cBit5
#define cAf6_stk0_pen_MPIG_Output_to_RSQ_Shift                                                               5
#define cAf6_stk0_pen_MPIG_Output_to_RSQ_MaxVal                                                            0x1
#define cAf6_stk0_pen_MPIG_Output_to_RSQ_MinVal                                                            0x0
#define cAf6_stk0_pen_MPIG_Output_to_RSQ_RstVal                                                            0x0

/*--------------------------------------
BitField Name: PLA_Error
BitField Type: R/W
BitField Desc: PLA declare error
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_stk0_pen_PLA_Error_Bit_Start                                                                    4
#define cAf6_stk0_pen_PLA_Error_Bit_End                                                                      4
#define cAf6_stk0_pen_PLA_Error_Mask                                                                     cBit4
#define cAf6_stk0_pen_PLA_Error_Shift                                                                        4
#define cAf6_stk0_pen_PLA_Error_MaxVal                                                                     0x1
#define cAf6_stk0_pen_PLA_Error_MinVal                                                                     0x0
#define cAf6_stk0_pen_PLA_Error_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: Input_RSQ
BitField Type: R/W
BitField Desc: Input RSQ to MPIG
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_stk0_pen_Input_RSQ_Bit_Start                                                                    3
#define cAf6_stk0_pen_Input_RSQ_Bit_End                                                                      3
#define cAf6_stk0_pen_Input_RSQ_Mask                                                                     cBit3
#define cAf6_stk0_pen_Input_RSQ_Shift                                                                        3
#define cAf6_stk0_pen_Input_RSQ_MaxVal                                                                     0x1
#define cAf6_stk0_pen_Input_RSQ_MinVal                                                                     0x0
#define cAf6_stk0_pen_Input_RSQ_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: PLA_Request_Read_Enable
BitField Type: R/W
BitField Desc: PLA request read enable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_stk0_pen_PLA_Request_Read_Enable_Bit_Start                                                      2
#define cAf6_stk0_pen_PLA_Request_Read_Enable_Bit_End                                                        2
#define cAf6_stk0_pen_PLA_Request_Read_Enable_Mask                                                       cBit2
#define cAf6_stk0_pen_PLA_Request_Read_Enable_Shift                                                          2
#define cAf6_stk0_pen_PLA_Request_Read_Enable_MaxVal                                                       0x1
#define cAf6_stk0_pen_PLA_Request_Read_Enable_MinVal                                                       0x0
#define cAf6_stk0_pen_PLA_Request_Read_Enable_RstVal                                                       0x0

/*--------------------------------------
BitField Name: MPIG_ouput_to_PLA
BitField Type: R/W
BitField Desc: MPIG output to PLA
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_stk0_pen_MPIG_ouput_to_PLA_Bit_Start                                                            1
#define cAf6_stk0_pen_MPIG_ouput_to_PLA_Bit_End                                                              1
#define cAf6_stk0_pen_MPIG_ouput_to_PLA_Mask                                                             cBit1
#define cAf6_stk0_pen_MPIG_ouput_to_PLA_Shift                                                                1
#define cAf6_stk0_pen_MPIG_ouput_to_PLA_MaxVal                                                             0x1
#define cAf6_stk0_pen_MPIG_ouput_to_PLA_MinVal                                                             0x0
#define cAf6_stk0_pen_MPIG_ouput_to_PLA_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PLA_Enqueue_Enable
BitField Type: R/W
BitField Desc: PLA Enqueue to MPIG
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_stk0_pen_PLA_Enqueue_Enable_Bit_Start                                                           0
#define cAf6_stk0_pen_PLA_Enqueue_Enable_Bit_End                                                             0
#define cAf6_stk0_pen_PLA_Enqueue_Enable_Mask                                                            cBit0
#define cAf6_stk0_pen_PLA_Enqueue_Enable_Shift                                                               0
#define cAf6_stk0_pen_PLA_Enqueue_Enable_MaxVal                                                            0x1
#define cAf6_stk0_pen_PLA_Enqueue_Enable_MinVal                                                            0x0
#define cAf6_stk0_pen_PLA_Enqueue_Enable_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Stick 1
Reg Addr   : 0x8001
Reg Formula:
    Where  :
Reg Desc   :
Used to debug MPEG

------------------------------------------------------------------------------*/
#define cAf6Reg_stk1en_Base                                                                             0x8001
#define cAf6Reg_stk1en                                                                                0x8001UL
#define cAf6Reg_stk1en_WidthVal                                                                             32
#define cAf6Reg_stk1en_WriteMask                                                                           0x0

/*--------------------------------------
BitField Name: Sche_DWRRCurrent_Queue7
BitField Type: R/W
BitField Desc: Scheduler DWRRCurrent Queue 7
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_stk1en_Sche_DWRRCurrent_Queue7_Bit_Start                                                       31
#define cAf6_stk1en_Sche_DWRRCurrent_Queue7_Bit_End                                                         31
#define cAf6_stk1en_Sche_DWRRCurrent_Queue7_Mask                                                        cBit31
#define cAf6_stk1en_Sche_DWRRCurrent_Queue7_Shift                                                           31
#define cAf6_stk1en_Sche_DWRRCurrent_Queue7_MaxVal                                                         0x1
#define cAf6_stk1en_Sche_DWRRCurrent_Queue7_MinVal                                                         0x0
#define cAf6_stk1en_Sche_DWRRCurrent_Queue7_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Sche_DWRRCurrent_Queue6
BitField Type: R/W
BitField Desc: Scheduler DWRRCurrent Queue 6
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_stk1en_Sche_DWRRCurrent_Queue6_Bit_Start                                                       30
#define cAf6_stk1en_Sche_DWRRCurrent_Queue6_Bit_End                                                         30
#define cAf6_stk1en_Sche_DWRRCurrent_Queue6_Mask                                                        cBit30
#define cAf6_stk1en_Sche_DWRRCurrent_Queue6_Shift                                                           30
#define cAf6_stk1en_Sche_DWRRCurrent_Queue6_MaxVal                                                         0x1
#define cAf6_stk1en_Sche_DWRRCurrent_Queue6_MinVal                                                         0x0
#define cAf6_stk1en_Sche_DWRRCurrent_Queue6_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Sche_DWRRCurrent_Queue5
BitField Type: R/W
BitField Desc: Scheduler DWRRCurrent Queue 5
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_stk1en_Sche_DWRRCurrent_Queue5_Bit_Start                                                       29
#define cAf6_stk1en_Sche_DWRRCurrent_Queue5_Bit_End                                                         29
#define cAf6_stk1en_Sche_DWRRCurrent_Queue5_Mask                                                        cBit29
#define cAf6_stk1en_Sche_DWRRCurrent_Queue5_Shift                                                           29
#define cAf6_stk1en_Sche_DWRRCurrent_Queue5_MaxVal                                                         0x1
#define cAf6_stk1en_Sche_DWRRCurrent_Queue5_MinVal                                                         0x0
#define cAf6_stk1en_Sche_DWRRCurrent_Queue5_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Sche_DWRRCurrent_Queue4
BitField Type: R/W
BitField Desc: Scheduler DWRRCurrent Queue 4
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_stk1en_Sche_DWRRCurrent_Queue4_Bit_Start                                                       28
#define cAf6_stk1en_Sche_DWRRCurrent_Queue4_Bit_End                                                         28
#define cAf6_stk1en_Sche_DWRRCurrent_Queue4_Mask                                                        cBit28
#define cAf6_stk1en_Sche_DWRRCurrent_Queue4_Shift                                                           28
#define cAf6_stk1en_Sche_DWRRCurrent_Queue4_MaxVal                                                         0x1
#define cAf6_stk1en_Sche_DWRRCurrent_Queue4_MinVal                                                         0x0
#define cAf6_stk1en_Sche_DWRRCurrent_Queue4_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Sche_DWRRCurrent_Queue3
BitField Type: R/W
BitField Desc: Scheduler DWRRCurrent Queue 3
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_stk1en_Sche_DWRRCurrent_Queue3_Bit_Start                                                       27
#define cAf6_stk1en_Sche_DWRRCurrent_Queue3_Bit_End                                                         27
#define cAf6_stk1en_Sche_DWRRCurrent_Queue3_Mask                                                        cBit27
#define cAf6_stk1en_Sche_DWRRCurrent_Queue3_Shift                                                           27
#define cAf6_stk1en_Sche_DWRRCurrent_Queue3_MaxVal                                                         0x1
#define cAf6_stk1en_Sche_DWRRCurrent_Queue3_MinVal                                                         0x0
#define cAf6_stk1en_Sche_DWRRCurrent_Queue3_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Sche_DWRRCurrent_Queue2
BitField Type: R/W
BitField Desc: Scheduler DWRRCurrent Queue 2
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_stk1en_Sche_DWRRCurrent_Queue2_Bit_Start                                                       26
#define cAf6_stk1en_Sche_DWRRCurrent_Queue2_Bit_End                                                         26
#define cAf6_stk1en_Sche_DWRRCurrent_Queue2_Mask                                                        cBit26
#define cAf6_stk1en_Sche_DWRRCurrent_Queue2_Shift                                                           26
#define cAf6_stk1en_Sche_DWRRCurrent_Queue2_MaxVal                                                         0x1
#define cAf6_stk1en_Sche_DWRRCurrent_Queue2_MinVal                                                         0x0
#define cAf6_stk1en_Sche_DWRRCurrent_Queue2_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Sche_DWRRCurrent_Queue1
BitField Type: R/W
BitField Desc: Scheduler DWRRCurrent Queue 1
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_stk1en_Sche_DWRRCurrent_Queue1_Bit_Start                                                       25
#define cAf6_stk1en_Sche_DWRRCurrent_Queue1_Bit_End                                                         25
#define cAf6_stk1en_Sche_DWRRCurrent_Queue1_Mask                                                        cBit25
#define cAf6_stk1en_Sche_DWRRCurrent_Queue1_Shift                                                           25
#define cAf6_stk1en_Sche_DWRRCurrent_Queue1_MaxVal                                                         0x1
#define cAf6_stk1en_Sche_DWRRCurrent_Queue1_MinVal                                                         0x0
#define cAf6_stk1en_Sche_DWRRCurrent_Queue1_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Sche_DWRRCurrent_Queue0
BitField Type: R/W
BitField Desc: Scheduler DWRRCurrent Queue 0
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_stk1en_Sche_DWRRCurrent_Queue0_Bit_Start                                                       24
#define cAf6_stk1en_Sche_DWRRCurrent_Queue0_Bit_End                                                         24
#define cAf6_stk1en_Sche_DWRRCurrent_Queue0_Mask                                                        cBit24
#define cAf6_stk1en_Sche_DWRRCurrent_Queue0_Shift                                                           24
#define cAf6_stk1en_Sche_DWRRCurrent_Queue0_MaxVal                                                         0x1
#define cAf6_stk1en_Sche_DWRRCurrent_Queue0_MinVal                                                         0x0
#define cAf6_stk1en_Sche_DWRRCurrent_Queue0_RstVal                                                         0x0

/*--------------------------------------
BitField Name: Sche_HSPCurrent_Queue7
BitField Type: R/W
BitField Desc: Scheduler HSPCurrent Queue 7
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_stk1en_Sche_HSPCurrent_Queue7_Bit_Start                                                        23
#define cAf6_stk1en_Sche_HSPCurrent_Queue7_Bit_End                                                          23
#define cAf6_stk1en_Sche_HSPCurrent_Queue7_Mask                                                         cBit23
#define cAf6_stk1en_Sche_HSPCurrent_Queue7_Shift                                                            23
#define cAf6_stk1en_Sche_HSPCurrent_Queue7_MaxVal                                                          0x1
#define cAf6_stk1en_Sche_HSPCurrent_Queue7_MinVal                                                          0x0
#define cAf6_stk1en_Sche_HSPCurrent_Queue7_RstVal                                                          0x0

/*--------------------------------------
BitField Name: Sche_HSPCurrent_Queue6
BitField Type: R/W
BitField Desc: Scheduler HSPCurrent Queue 6
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_stk1en_Sche_HSPCurrent_Queue6_Bit_Start                                                        22
#define cAf6_stk1en_Sche_HSPCurrent_Queue6_Bit_End                                                          22
#define cAf6_stk1en_Sche_HSPCurrent_Queue6_Mask                                                         cBit22
#define cAf6_stk1en_Sche_HSPCurrent_Queue6_Shift                                                            22
#define cAf6_stk1en_Sche_HSPCurrent_Queue6_MaxVal                                                          0x1
#define cAf6_stk1en_Sche_HSPCurrent_Queue6_MinVal                                                          0x0
#define cAf6_stk1en_Sche_HSPCurrent_Queue6_RstVal                                                          0x0

/*--------------------------------------
BitField Name: Sche_HSPCurrent_Queue5
BitField Type: R/W
BitField Desc: Scheduler HSPCurrent Queue 5
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_stk1en_Sche_HSPCurrent_Queue5_Bit_Start                                                        21
#define cAf6_stk1en_Sche_HSPCurrent_Queue5_Bit_End                                                          21
#define cAf6_stk1en_Sche_HSPCurrent_Queue5_Mask                                                         cBit21
#define cAf6_stk1en_Sche_HSPCurrent_Queue5_Shift                                                            21
#define cAf6_stk1en_Sche_HSPCurrent_Queue5_MaxVal                                                          0x1
#define cAf6_stk1en_Sche_HSPCurrent_Queue5_MinVal                                                          0x0
#define cAf6_stk1en_Sche_HSPCurrent_Queue5_RstVal                                                          0x0

/*--------------------------------------
BitField Name: Sche_HSPCurrent_Queue4
BitField Type: R/W
BitField Desc: Scheduler HSPCurrent Queue 4
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_stk1en_Sche_HSPCurrent_Queue4_Bit_Start                                                        20
#define cAf6_stk1en_Sche_HSPCurrent_Queue4_Bit_End                                                          20
#define cAf6_stk1en_Sche_HSPCurrent_Queue4_Mask                                                         cBit20
#define cAf6_stk1en_Sche_HSPCurrent_Queue4_Shift                                                            20
#define cAf6_stk1en_Sche_HSPCurrent_Queue4_MaxVal                                                          0x1
#define cAf6_stk1en_Sche_HSPCurrent_Queue4_MinVal                                                          0x0
#define cAf6_stk1en_Sche_HSPCurrent_Queue4_RstVal                                                          0x0

/*--------------------------------------
BitField Name: Sche_HSPCurrent_Queue3
BitField Type: R/W
BitField Desc: Scheduler HSPCurrent Queue 3
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_stk1en_Sche_HSPCurrent_Queue3_Bit_Start                                                        19
#define cAf6_stk1en_Sche_HSPCurrent_Queue3_Bit_End                                                          19
#define cAf6_stk1en_Sche_HSPCurrent_Queue3_Mask                                                         cBit19
#define cAf6_stk1en_Sche_HSPCurrent_Queue3_Shift                                                            19
#define cAf6_stk1en_Sche_HSPCurrent_Queue3_MaxVal                                                          0x1
#define cAf6_stk1en_Sche_HSPCurrent_Queue3_MinVal                                                          0x0
#define cAf6_stk1en_Sche_HSPCurrent_Queue3_RstVal                                                          0x0

/*--------------------------------------
BitField Name: Sche_HSPCurrent_Queue2
BitField Type: R/W
BitField Desc: Scheduler HSPCurrent Queue 2
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_stk1en_Sche_HSPCurrent_Queue2_Bit_Start                                                        18
#define cAf6_stk1en_Sche_HSPCurrent_Queue2_Bit_End                                                          18
#define cAf6_stk1en_Sche_HSPCurrent_Queue2_Mask                                                         cBit18
#define cAf6_stk1en_Sche_HSPCurrent_Queue2_Shift                                                            18
#define cAf6_stk1en_Sche_HSPCurrent_Queue2_MaxVal                                                          0x1
#define cAf6_stk1en_Sche_HSPCurrent_Queue2_MinVal                                                          0x0
#define cAf6_stk1en_Sche_HSPCurrent_Queue2_RstVal                                                          0x0

/*--------------------------------------
BitField Name: Sche_HSPCurrent_Queue1
BitField Type: R/W
BitField Desc: Scheduler HSPCurrent Queue 1
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_stk1en_Sche_HSPCurrent_Queue1_Bit_Start                                                        17
#define cAf6_stk1en_Sche_HSPCurrent_Queue1_Bit_End                                                          17
#define cAf6_stk1en_Sche_HSPCurrent_Queue1_Mask                                                         cBit17
#define cAf6_stk1en_Sche_HSPCurrent_Queue1_Shift                                                            17
#define cAf6_stk1en_Sche_HSPCurrent_Queue1_MaxVal                                                          0x1
#define cAf6_stk1en_Sche_HSPCurrent_Queue1_MinVal                                                          0x0
#define cAf6_stk1en_Sche_HSPCurrent_Queue1_RstVal                                                          0x0

/*--------------------------------------
BitField Name: Sche_HSPCurrent_Queue0
BitField Type: R/W
BitField Desc: Scheduler HSPCurrent Queue 0
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_stk1en_Sche_HSPCurrent_Queue0_Bit_Start                                                        16
#define cAf6_stk1en_Sche_HSPCurrent_Queue0_Bit_End                                                          16
#define cAf6_stk1en_Sche_HSPCurrent_Queue0_Mask                                                         cBit16
#define cAf6_stk1en_Sche_HSPCurrent_Queue0_Shift                                                            16
#define cAf6_stk1en_Sche_HSPCurrent_Queue0_MaxVal                                                          0x1
#define cAf6_stk1en_Sche_HSPCurrent_Queue0_MinVal                                                          0x0
#define cAf6_stk1en_Sche_HSPCurrent_Queue0_RstVal                                                          0x0

/*--------------------------------------
BitField Name: Sche_Current_Queue7
BitField Type: R/W
BitField Desc: Scheduler Current Queue 7
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_stk1en_Sche_Current_Queue7_Bit_Start                                                           15
#define cAf6_stk1en_Sche_Current_Queue7_Bit_End                                                             15
#define cAf6_stk1en_Sche_Current_Queue7_Mask                                                            cBit15
#define cAf6_stk1en_Sche_Current_Queue7_Shift                                                               15
#define cAf6_stk1en_Sche_Current_Queue7_MaxVal                                                             0x1
#define cAf6_stk1en_Sche_Current_Queue7_MinVal                                                             0x0
#define cAf6_stk1en_Sche_Current_Queue7_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Sche_Current_Queue6
BitField Type: R/W
BitField Desc: Scheduler Current Queue 6
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_stk1en_Sche_Current_Queue6_Bit_Start                                                           14
#define cAf6_stk1en_Sche_Current_Queue6_Bit_End                                                             14
#define cAf6_stk1en_Sche_Current_Queue6_Mask                                                            cBit14
#define cAf6_stk1en_Sche_Current_Queue6_Shift                                                               14
#define cAf6_stk1en_Sche_Current_Queue6_MaxVal                                                             0x1
#define cAf6_stk1en_Sche_Current_Queue6_MinVal                                                             0x0
#define cAf6_stk1en_Sche_Current_Queue6_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Sche_Current_Queue5
BitField Type: R/W
BitField Desc: Scheduler Current Queue 5
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_stk1en_Sche_Current_Queue5_Bit_Start                                                           13
#define cAf6_stk1en_Sche_Current_Queue5_Bit_End                                                             13
#define cAf6_stk1en_Sche_Current_Queue5_Mask                                                            cBit13
#define cAf6_stk1en_Sche_Current_Queue5_Shift                                                               13
#define cAf6_stk1en_Sche_Current_Queue5_MaxVal                                                             0x1
#define cAf6_stk1en_Sche_Current_Queue5_MinVal                                                             0x0
#define cAf6_stk1en_Sche_Current_Queue5_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Sche_Current_Queue4
BitField Type: R/W
BitField Desc: Scheduler Current Queue 4
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_stk1en_Sche_Current_Queue4_Bit_Start                                                           12
#define cAf6_stk1en_Sche_Current_Queue4_Bit_End                                                             12
#define cAf6_stk1en_Sche_Current_Queue4_Mask                                                            cBit12
#define cAf6_stk1en_Sche_Current_Queue4_Shift                                                               12
#define cAf6_stk1en_Sche_Current_Queue4_MaxVal                                                             0x1
#define cAf6_stk1en_Sche_Current_Queue4_MinVal                                                             0x0
#define cAf6_stk1en_Sche_Current_Queue4_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Sche_Current_Queue3
BitField Type: R/W
BitField Desc: Scheduler Current Queue 3
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_stk1en_Sche_Current_Queue3_Bit_Start                                                           11
#define cAf6_stk1en_Sche_Current_Queue3_Bit_End                                                             11
#define cAf6_stk1en_Sche_Current_Queue3_Mask                                                            cBit11
#define cAf6_stk1en_Sche_Current_Queue3_Shift                                                               11
#define cAf6_stk1en_Sche_Current_Queue3_MaxVal                                                             0x1
#define cAf6_stk1en_Sche_Current_Queue3_MinVal                                                             0x0
#define cAf6_stk1en_Sche_Current_Queue3_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Sche_Current_Queue2
BitField Type: R/W
BitField Desc: Scheduler Current Queue 2
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_stk1en_Sche_Current_Queue2_Bit_Start                                                           10
#define cAf6_stk1en_Sche_Current_Queue2_Bit_End                                                             10
#define cAf6_stk1en_Sche_Current_Queue2_Mask                                                            cBit10
#define cAf6_stk1en_Sche_Current_Queue2_Shift                                                               10
#define cAf6_stk1en_Sche_Current_Queue2_MaxVal                                                             0x1
#define cAf6_stk1en_Sche_Current_Queue2_MinVal                                                             0x0
#define cAf6_stk1en_Sche_Current_Queue2_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Sche_Current_Queue1
BitField Type: R/W
BitField Desc: Scheduler Current Queue 1
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_stk1en_Sche_Current_Queue1_Bit_Start                                                            9
#define cAf6_stk1en_Sche_Current_Queue1_Bit_End                                                              9
#define cAf6_stk1en_Sche_Current_Queue1_Mask                                                             cBit9
#define cAf6_stk1en_Sche_Current_Queue1_Shift                                                                9
#define cAf6_stk1en_Sche_Current_Queue1_MaxVal                                                             0x1
#define cAf6_stk1en_Sche_Current_Queue1_MinVal                                                             0x0
#define cAf6_stk1en_Sche_Current_Queue1_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Sche_Current_Queue0
BitField Type: R/W
BitField Desc: Scheduler Current Queue 0
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_stk1en_Sche_Current_Queue0_Bit_Start                                                            8
#define cAf6_stk1en_Sche_Current_Queue0_Bit_End                                                              8
#define cAf6_stk1en_Sche_Current_Queue0_Mask                                                             cBit8
#define cAf6_stk1en_Sche_Current_Queue0_Shift                                                                8
#define cAf6_stk1en_Sche_Current_Queue0_MaxVal                                                             0x1
#define cAf6_stk1en_Sche_Current_Queue0_MinVal                                                             0x0
#define cAf6_stk1en_Sche_Current_Queue0_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Sche_Request_QMXFF
BitField Type: R/W
BitField Desc: Scheduler Request QMXFF
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_stk1en_Sche_Request_QMXFF_Bit_Start                                                             6
#define cAf6_stk1en_Sche_Request_QMXFF_Bit_End                                                               6
#define cAf6_stk1en_Sche_Request_QMXFF_Mask                                                              cBit6
#define cAf6_stk1en_Sche_Request_QMXFF_Shift                                                                 6
#define cAf6_stk1en_Sche_Request_QMXFF_MaxVal                                                              0x1
#define cAf6_stk1en_Sche_Request_QMXFF_MinVal                                                              0x0
#define cAf6_stk1en_Sche_Request_QMXFF_RstVal                                                              0x0

/*--------------------------------------
BitField Name: Sche_Return_Valid
BitField Type: R/W
BitField Desc: Scheduler Return Valid
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_stk1en_Sche_Return_Valid_Bit_Start                                                              5
#define cAf6_stk1en_Sche_Return_Valid_Bit_End                                                                5
#define cAf6_stk1en_Sche_Return_Valid_Mask                                                               cBit5
#define cAf6_stk1en_Sche_Return_Valid_Shift                                                                  5
#define cAf6_stk1en_Sche_Return_Valid_MaxVal                                                               0x1
#define cAf6_stk1en_Sche_Return_Valid_MinVal                                                               0x0
#define cAf6_stk1en_Sche_Return_Valid_RstVal                                                               0x0

/*--------------------------------------
BitField Name: Sche_Wrempt_Update
BitField Type: R/W
BitField Desc: Scheduler Write Empty Update
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_stk1en_Sche_Wrempt_Update_Bit_Start                                                             4
#define cAf6_stk1en_Sche_Wrempt_Update_Bit_End                                                               4
#define cAf6_stk1en_Sche_Wrempt_Update_Mask                                                              cBit4
#define cAf6_stk1en_Sche_Wrempt_Update_Shift                                                                 4
#define cAf6_stk1en_Sche_Wrempt_Update_MaxVal                                                              0x1
#define cAf6_stk1en_Sche_Wrempt_Update_MinVal                                                              0x0
#define cAf6_stk1en_Sche_Wrempt_Update_RstVal                                                              0x0

/*--------------------------------------
BitField Name: Sche_Sta1_err
BitField Type: R/W
BitField Desc: Schedule Status1 Error
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_stk1en_Sche_Sta1_err_Bit_Start                                                                  1
#define cAf6_stk1en_Sche_Sta1_err_Bit_End                                                                    1
#define cAf6_stk1en_Sche_Sta1_err_Mask                                                                   cBit1
#define cAf6_stk1en_Sche_Sta1_err_Shift                                                                      1
#define cAf6_stk1en_Sche_Sta1_err_MaxVal                                                                   0x1
#define cAf6_stk1en_Sche_Sta1_err_MinVal                                                                   0x0
#define cAf6_stk1en_Sche_Sta1_err_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: Sche_Sta0_err
BitField Type: R/W
BitField Desc: Schedule Status0 Error
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_stk1en_Sche_Sta0_err_Bit_Start                                                                  0
#define cAf6_stk1en_Sche_Sta0_err_Bit_End                                                                    0
#define cAf6_stk1en_Sche_Sta0_err_Mask                                                                   cBit0
#define cAf6_stk1en_Sche_Sta0_err_Shift                                                                      0
#define cAf6_stk1en_Sche_Sta0_err_MaxVal                                                                   0x1
#define cAf6_stk1en_Sche_Sta0_err_MinVal                                                                   0x0
#define cAf6_stk1en_Sche_Sta0_err_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Stick 2
Reg Addr   : 0x8002
Reg Formula:
    Where  :
Reg Desc   :
Used to debug MPEG

------------------------------------------------------------------------------*/
#define cAf6Reg_stk2pen_Base                                                                            0x8002
#define cAf6Reg_stk2pen                                                                               0x8002UL
#define cAf6Reg_stk2pen_WidthVal                                                                            32
#define cAf6Reg_stk2pen_WriteMask                                                                          0x0

/*--------------------------------------
BitField Name: MPIG_Discard
BitField Type: R/W
BitField Desc: MPEG Discard to PLA
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_stk2pen_MPIG_Discard_Bit_Start                                                                 16
#define cAf6_stk2pen_MPIG_Discard_Bit_End                                                                   16
#define cAf6_stk2pen_MPIG_Discard_Mask                                                                  cBit16
#define cAf6_stk2pen_MPIG_Discard_Shift                                                                     16
#define cAf6_stk2pen_MPIG_Discard_MaxVal                                                                   0x1
#define cAf6_stk2pen_MPIG_Discard_MinVal                                                                   0x0
#define cAf6_stk2pen_MPIG_Discard_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: Output_Internal_Q7_Seq_Fail
BitField Type: R/W
BitField Desc: Output Internal Queue0 Sequence Fail
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_stk2pen_Output_Internal_Q7_Seq_Fail_Bit_Start                                                  15
#define cAf6_stk2pen_Output_Internal_Q7_Seq_Fail_Bit_End                                                    15
#define cAf6_stk2pen_Output_Internal_Q7_Seq_Fail_Mask                                                   cBit15
#define cAf6_stk2pen_Output_Internal_Q7_Seq_Fail_Shift                                                      15
#define cAf6_stk2pen_Output_Internal_Q7_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_Internal_Q7_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_Internal_Q7_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_Internal_Q6_Seq_Fail
BitField Type: R/W
BitField Desc: Output Internal Queue1 Sequence Fail
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_stk2pen_Output_Internal_Q6_Seq_Fail_Bit_Start                                                  14
#define cAf6_stk2pen_Output_Internal_Q6_Seq_Fail_Bit_End                                                    14
#define cAf6_stk2pen_Output_Internal_Q6_Seq_Fail_Mask                                                   cBit14
#define cAf6_stk2pen_Output_Internal_Q6_Seq_Fail_Shift                                                      14
#define cAf6_stk2pen_Output_Internal_Q6_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_Internal_Q6_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_Internal_Q6_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_Internal_Q5_Seq_Fail
BitField Type: R/W
BitField Desc: Output Internal Queue2 Sequence Fail
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_stk2pen_Output_Internal_Q5_Seq_Fail_Bit_Start                                                  13
#define cAf6_stk2pen_Output_Internal_Q5_Seq_Fail_Bit_End                                                    13
#define cAf6_stk2pen_Output_Internal_Q5_Seq_Fail_Mask                                                   cBit13
#define cAf6_stk2pen_Output_Internal_Q5_Seq_Fail_Shift                                                      13
#define cAf6_stk2pen_Output_Internal_Q5_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_Internal_Q5_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_Internal_Q5_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_Internal_Q4_Seq_Fail
BitField Type: R/W
BitField Desc: Output Internal Queue3 Sequence Fail
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_stk2pen_Output_Internal_Q4_Seq_Fail_Bit_Start                                                  12
#define cAf6_stk2pen_Output_Internal_Q4_Seq_Fail_Bit_End                                                    12
#define cAf6_stk2pen_Output_Internal_Q4_Seq_Fail_Mask                                                   cBit12
#define cAf6_stk2pen_Output_Internal_Q4_Seq_Fail_Shift                                                      12
#define cAf6_stk2pen_Output_Internal_Q4_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_Internal_Q4_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_Internal_Q4_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_Internal_Q3_Seq_Fail
BitField Type: R/W
BitField Desc: Output Internal Queue4 Sequence Fail
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_stk2pen_Output_Internal_Q3_Seq_Fail_Bit_Start                                                  11
#define cAf6_stk2pen_Output_Internal_Q3_Seq_Fail_Bit_End                                                    11
#define cAf6_stk2pen_Output_Internal_Q3_Seq_Fail_Mask                                                   cBit11
#define cAf6_stk2pen_Output_Internal_Q3_Seq_Fail_Shift                                                      11
#define cAf6_stk2pen_Output_Internal_Q3_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_Internal_Q3_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_Internal_Q3_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_Internal_Q2_Seq_Fail
BitField Type: R/W
BitField Desc: Output Internal Queue5 Sequence Fail
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_stk2pen_Output_Internal_Q2_Seq_Fail_Bit_Start                                                  10
#define cAf6_stk2pen_Output_Internal_Q2_Seq_Fail_Bit_End                                                    10
#define cAf6_stk2pen_Output_Internal_Q2_Seq_Fail_Mask                                                   cBit10
#define cAf6_stk2pen_Output_Internal_Q2_Seq_Fail_Shift                                                      10
#define cAf6_stk2pen_Output_Internal_Q2_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_Internal_Q2_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_Internal_Q2_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_Internal_Q1_Seq_Fail
BitField Type: R/W
BitField Desc: Output Internal Queue6 Sequence Fail
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_stk2pen_Output_Internal_Q1_Seq_Fail_Bit_Start                                                   9
#define cAf6_stk2pen_Output_Internal_Q1_Seq_Fail_Bit_End                                                     9
#define cAf6_stk2pen_Output_Internal_Q1_Seq_Fail_Mask                                                    cBit9
#define cAf6_stk2pen_Output_Internal_Q1_Seq_Fail_Shift                                                       9
#define cAf6_stk2pen_Output_Internal_Q1_Seq_Fail_MaxVal                                                   0x1f
#define cAf6_stk2pen_Output_Internal_Q1_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_Internal_Q1_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_Internal_Q0_Seq_Fail
BitField Type: R/W
BitField Desc: Output Internal Queue7 Sequence Fail
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_stk2pen_Output_Internal_Q0_Seq_Fail_Bit_Start                                                   8
#define cAf6_stk2pen_Output_Internal_Q0_Seq_Fail_Bit_End                                                     8
#define cAf6_stk2pen_Output_Internal_Q0_Seq_Fail_Mask                                                    cBit8
#define cAf6_stk2pen_Output_Internal_Q0_Seq_Fail_Shift                                                       8
#define cAf6_stk2pen_Output_Internal_Q0_Seq_Fail_MaxVal                                                    0x0
#define cAf6_stk2pen_Output_Internal_Q0_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_Internal_Q0_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_External_Q7_Seq_Fail
BitField Type: R/W
BitField Desc: Output External Queue0 Sequence Fail
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_stk2pen_Output_External_Q7_Seq_Fail_Bit_Start                                                   7
#define cAf6_stk2pen_Output_External_Q7_Seq_Fail_Bit_End                                                     7
#define cAf6_stk2pen_Output_External_Q7_Seq_Fail_Mask                                                    cBit7
#define cAf6_stk2pen_Output_External_Q7_Seq_Fail_Shift                                                       7
#define cAf6_stk2pen_Output_External_Q7_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_External_Q7_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_External_Q7_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_External_Q6_Seq_Fail
BitField Type: R/W
BitField Desc: Output External Queue1 Sequence Fail
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_stk2pen_Output_External_Q6_Seq_Fail_Bit_Start                                                   6
#define cAf6_stk2pen_Output_External_Q6_Seq_Fail_Bit_End                                                     6
#define cAf6_stk2pen_Output_External_Q6_Seq_Fail_Mask                                                    cBit6
#define cAf6_stk2pen_Output_External_Q6_Seq_Fail_Shift                                                       6
#define cAf6_stk2pen_Output_External_Q6_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_External_Q6_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_External_Q6_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_External_Q5_Seq_Fail
BitField Type: R/W
BitField Desc: Output External Queue2 Sequence Fail
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_stk2pen_Output_External_Q5_Seq_Fail_Bit_Start                                                   5
#define cAf6_stk2pen_Output_External_Q5_Seq_Fail_Bit_End                                                     5
#define cAf6_stk2pen_Output_External_Q5_Seq_Fail_Mask                                                    cBit5
#define cAf6_stk2pen_Output_External_Q5_Seq_Fail_Shift                                                       5
#define cAf6_stk2pen_Output_External_Q5_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_External_Q5_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_External_Q5_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_External_Q4_Seq_Fail
BitField Type: R/W
BitField Desc: Output External Queue3 Sequence Fail
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_stk2pen_Output_External_Q4_Seq_Fail_Bit_Start                                                   4
#define cAf6_stk2pen_Output_External_Q4_Seq_Fail_Bit_End                                                     4
#define cAf6_stk2pen_Output_External_Q4_Seq_Fail_Mask                                                    cBit4
#define cAf6_stk2pen_Output_External_Q4_Seq_Fail_Shift                                                       4
#define cAf6_stk2pen_Output_External_Q4_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_External_Q4_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_External_Q4_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_External_Q3_Seq_Fail
BitField Type: R/W
BitField Desc: Output External Queue4 Sequence Fail
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_stk2pen_Output_External_Q3_Seq_Fail_Bit_Start                                                   3
#define cAf6_stk2pen_Output_External_Q3_Seq_Fail_Bit_End                                                     3
#define cAf6_stk2pen_Output_External_Q3_Seq_Fail_Mask                                                    cBit3
#define cAf6_stk2pen_Output_External_Q3_Seq_Fail_Shift                                                       3
#define cAf6_stk2pen_Output_External_Q3_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_External_Q3_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_External_Q3_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_External_Q2_Seq_Fail
BitField Type: R/W
BitField Desc: Output External Queue5 Sequence Fail
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_stk2pen_Output_External_Q2_Seq_Fail_Bit_Start                                                   2
#define cAf6_stk2pen_Output_External_Q2_Seq_Fail_Bit_End                                                     2
#define cAf6_stk2pen_Output_External_Q2_Seq_Fail_Mask                                                    cBit2
#define cAf6_stk2pen_Output_External_Q2_Seq_Fail_Shift                                                       2
#define cAf6_stk2pen_Output_External_Q2_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_External_Q2_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_External_Q2_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_External_Q1_Seq_Fail
BitField Type: R/W
BitField Desc: Output External Queue6 Sequence Fail
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_stk2pen_Output_External_Q1_Seq_Fail_Bit_Start                                                   1
#define cAf6_stk2pen_Output_External_Q1_Seq_Fail_Bit_End                                                     1
#define cAf6_stk2pen_Output_External_Q1_Seq_Fail_Mask                                                    cBit1
#define cAf6_stk2pen_Output_External_Q1_Seq_Fail_Shift                                                       1
#define cAf6_stk2pen_Output_External_Q1_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_External_Q1_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_External_Q1_Seq_Fail_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Output_External_Q0_Seq_Fail
BitField Type: R/W
BitField Desc: Output External Queue7 Sequence Fail
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_stk2pen_Output_External_Q0_Seq_Fail_Bit_Start                                                   0
#define cAf6_stk2pen_Output_External_Q0_Seq_Fail_Bit_End                                                     0
#define cAf6_stk2pen_Output_External_Q0_Seq_Fail_Mask                                                    cBit0
#define cAf6_stk2pen_Output_External_Q0_Seq_Fail_Shift                                                       0
#define cAf6_stk2pen_Output_External_Q0_Seq_Fail_MaxVal                                                    0x1
#define cAf6_stk2pen_Output_External_Q0_Seq_Fail_MinVal                                                    0x0
#define cAf6_stk2pen_Output_External_Q0_Seq_Fail_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Stick 3
Reg Addr   : 0x8003
Reg Formula:
    Where  :
Reg Desc   :
Used to debug MPEG

------------------------------------------------------------------------------*/
#define cAf6Reg_stk3en_Base                                                                             0x8003
#define cAf6Reg_stk3en                                                                                0x8003UL
#define cAf6Reg_stk3en_WidthVal                                                                             32
#define cAf6Reg_stk3en_WriteMask                                                                           0x0

/*--------------------------------------
BitField Name: Input_PLA_length_Fail
BitField Type: R/W
BitField Desc: Input PLA Length Fail
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_stk3en_Input_PLA_length_Fail_Bit_Start                                                         31
#define cAf6_stk3en_Input_PLA_length_Fail_Bit_End                                                           31
#define cAf6_stk3en_Input_PLA_length_Fail_Mask                                                          cBit31
#define cAf6_stk3en_Input_PLA_length_Fail_Shift                                                             31
#define cAf6_stk3en_Input_PLA_length_Fail_MaxVal                                                           0x1
#define cAf6_stk3en_Input_PLA_length_Fail_MinVal                                                           0x0
#define cAf6_stk3en_Input_PLA_length_Fail_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Ouput_PLA_Length_Fail
BitField Type: R/W
BitField Desc: Output PLA Length Fail
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_stk3en_Ouput_PLA_Length_Fail_Bit_Start                                                         30
#define cAf6_stk3en_Ouput_PLA_Length_Fail_Bit_End                                                           30
#define cAf6_stk3en_Ouput_PLA_Length_Fail_Mask                                                          cBit30
#define cAf6_stk3en_Ouput_PLA_Length_Fail_Shift                                                             30
#define cAf6_stk3en_Ouput_PLA_Length_Fail_MaxVal                                                           0x1
#define cAf6_stk3en_Ouput_PLA_Length_Fail_MinVal                                                           0x0
#define cAf6_stk3en_Ouput_PLA_Length_Fail_RstVal                                                           0x0

/*--------------------------------------
BitField Name: FiFo_Enqueue_Write_Error
BitField Type: R/W
BitField Desc: FiFo store enqueue QMXXF Write Err
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_stk3en_FiFo_Enqueue_Write_Error_Bit_Start                                                      29
#define cAf6_stk3en_FiFo_Enqueue_Write_Error_Bit_End                                                        29
#define cAf6_stk3en_FiFo_Enqueue_Write_Error_Mask                                                       cBit29
#define cAf6_stk3en_FiFo_Enqueue_Write_Error_Shift                                                          29
#define cAf6_stk3en_FiFo_Enqueue_Write_Error_MaxVal                                                        0x1
#define cAf6_stk3en_FiFo_Enqueue_Write_Error_MinVal                                                        0x0
#define cAf6_stk3en_FiFo_Enqueue_Write_Error_RstVal                                                        0x0

/*--------------------------------------
BitField Name: FiFo_Enqueue_Read_Error
BitField Type: R/W
BitField Desc: FiFo store enqueue QMXXF Read Err
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_stk3en_FiFo_Enqueue_Read_Error_Bit_Start                                                       28
#define cAf6_stk3en_FiFo_Enqueue_Read_Error_Bit_End                                                         28
#define cAf6_stk3en_FiFo_Enqueue_Read_Error_Mask                                                        cBit28
#define cAf6_stk3en_FiFo_Enqueue_Read_Error_Shift                                                           28
#define cAf6_stk3en_FiFo_Enqueue_Read_Error_MaxVal                                                         0x1
#define cAf6_stk3en_FiFo_Enqueue_Read_Error_MinVal                                                         0x0
#define cAf6_stk3en_FiFo_Enqueue_Read_Error_RstVal                                                         0x0

/*--------------------------------------
BitField Name: External_Engine_Err
BitField Type: R/W
BitField Desc: External FiFO Engine Error
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_stk3en_External_Engine_Err_Bit_Start                                                           24
#define cAf6_stk3en_External_Engine_Err_Bit_End                                                             24
#define cAf6_stk3en_External_Engine_Err_Mask                                                            cBit24
#define cAf6_stk3en_External_Engine_Err_Shift                                                               24
#define cAf6_stk3en_External_Engine_Err_MaxVal                                                             0x1
#define cAf6_stk3en_External_Engine_Err_MinVal                                                             0x0
#define cAf6_stk3en_External_Engine_Err_RstVal                                                             0x0

/*--------------------------------------
BitField Name: External_Write_Req_Buff_Full
BitField Type: R/W
BitField Desc: External Write Request Buffer Full
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_stk3en_External_Write_Req_Buff_Full_Bit_Start                                                  23
#define cAf6_stk3en_External_Write_Req_Buff_Full_Bit_End                                                    23
#define cAf6_stk3en_External_Write_Req_Buff_Full_Mask                                                   cBit23
#define cAf6_stk3en_External_Write_Req_Buff_Full_Shift                                                      23
#define cAf6_stk3en_External_Write_Req_Buff_Full_MaxVal                                                    0x1
#define cAf6_stk3en_External_Write_Req_Buff_Full_MinVal                                                    0x0
#define cAf6_stk3en_External_Write_Req_Buff_Full_RstVal                                                    0x0

/*--------------------------------------
BitField Name: External_Write_Valid_Buff_Full
BitField Type: R/W
BitField Desc: External Write Valid but Buffer Full
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_stk3en_External_Write_Valid_Buff_Full_Bit_Start                                                21
#define cAf6_stk3en_External_Write_Valid_Buff_Full_Bit_End                                                  21
#define cAf6_stk3en_External_Write_Valid_Buff_Full_Mask                                                 cBit21
#define cAf6_stk3en_External_Write_Valid_Buff_Full_Shift                                                    21
#define cAf6_stk3en_External_Write_Valid_Buff_Full_MaxVal                                                  0x1
#define cAf6_stk3en_External_Write_Valid_Buff_Full_MinVal                                                  0x0
#define cAf6_stk3en_External_Write_Valid_Buff_Full_RstVal                                                  0x0

/*--------------------------------------
BitField Name: External_Read_Req_Cache_Full
BitField Type: R/W
BitField Desc: External Read  Request Cache  Full
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_stk3en_External_Read_Req_Cache_Full_Bit_Start                                                  20
#define cAf6_stk3en_External_Read_Req_Cache_Full_Bit_End                                                    20
#define cAf6_stk3en_External_Read_Req_Cache_Full_Mask                                                   cBit20
#define cAf6_stk3en_External_Read_Req_Cache_Full_Shift                                                      20
#define cAf6_stk3en_External_Read_Req_Cache_Full_MaxVal                                                    0x1
#define cAf6_stk3en_External_Read_Req_Cache_Full_MinVal                                                    0x0
#define cAf6_stk3en_External_Read_Req_Cache_Full_RstVal                                                    0x0

/*--------------------------------------
BitField Name: External_Write_Req_Cache_Full
BitField Type: R/W
BitField Desc: External Write Request Cache  Full
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_stk3en_External_Write_Req_Cache_Full_Bit_Start                                                 16
#define cAf6_stk3en_External_Write_Req_Cache_Full_Bit_End                                                   16
#define cAf6_stk3en_External_Write_Req_Cache_Full_Mask                                                  cBit16
#define cAf6_stk3en_External_Write_Req_Cache_Full_Shift                                                     16
#define cAf6_stk3en_External_Write_Req_Cache_Full_MaxVal                                                   0x1
#define cAf6_stk3en_External_Write_Req_Cache_Full_MinVal                                                   0x0
#define cAf6_stk3en_External_Write_Req_Cache_Full_RstVal                                                   0x0

/*--------------------------------------
BitField Name: Head_FiFo_Write_Error
BitField Type: R/W
BitField Desc: Head FiFO Write Error
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_stk3en_Head_FiFo_Write_Error_Bit_Start                                                         15
#define cAf6_stk3en_Head_FiFo_Write_Error_Bit_End                                                           15
#define cAf6_stk3en_Head_FiFo_Write_Error_Mask                                                          cBit15
#define cAf6_stk3en_Head_FiFo_Write_Error_Shift                                                             15
#define cAf6_stk3en_Head_FiFo_Write_Error_MaxVal                                                           0x1
#define cAf6_stk3en_Head_FiFo_Write_Error_MinVal                                                           0x0
#define cAf6_stk3en_Head_FiFo_Write_Error_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Head_FiFo_Read_Error
BitField Type: R/W
BitField Desc: Head FiFO Read  Error
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_stk3en_Head_FiFo_Read_Error_Bit_Start                                                          14
#define cAf6_stk3en_Head_FiFo_Read_Error_Bit_End                                                            14
#define cAf6_stk3en_Head_FiFo_Read_Error_Mask                                                           cBit14
#define cAf6_stk3en_Head_FiFo_Read_Error_Shift                                                              14
#define cAf6_stk3en_Head_FiFo_Read_Error_MaxVal                                                            0x1
#define cAf6_stk3en_Head_FiFo_Read_Error_MinVal                                                            0x0
#define cAf6_stk3en_Head_FiFo_Read_Error_RstVal                                                            0x0

/*--------------------------------------
BitField Name: FiFO_Request_DDR_Write_Err
BitField Type: R/W
BitField Desc: FiFo Request DDR Write Error
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_stk3en_FiFO_Request_DDR_Write_Err_Bit_Start                                                    13
#define cAf6_stk3en_FiFO_Request_DDR_Write_Err_Bit_End                                                      13
#define cAf6_stk3en_FiFO_Request_DDR_Write_Err_Mask                                                     cBit13
#define cAf6_stk3en_FiFO_Request_DDR_Write_Err_Shift                                                        13
#define cAf6_stk3en_FiFO_Request_DDR_Write_Err_MaxVal                                                      0x1
#define cAf6_stk3en_FiFO_Request_DDR_Write_Err_MinVal                                                      0x0
#define cAf6_stk3en_FiFO_Request_DDR_Write_Err_RstVal                                                      0x0

/*--------------------------------------
BitField Name: FiFO_Request_DDR_Read_Err
BitField Type: R/W
BitField Desc: FiFo Request DDR Read  Error
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_stk3en_FiFO_Request_DDR_Read_Err_Bit_Start                                                     12
#define cAf6_stk3en_FiFO_Request_DDR_Read_Err_Bit_End                                                       12
#define cAf6_stk3en_FiFO_Request_DDR_Read_Err_Mask                                                      cBit12
#define cAf6_stk3en_FiFO_Request_DDR_Read_Err_Shift                                                         12
#define cAf6_stk3en_FiFO_Request_DDR_Read_Err_MaxVal                                                       0x1
#define cAf6_stk3en_FiFO_Request_DDR_Read_Err_MinVal                                                       0x0
#define cAf6_stk3en_FiFO_Request_DDR_Read_Err_RstVal                                                       0x0

/*--------------------------------------
BitField Name: External_FiFO_Engine_Conflict
BitField Type: R/W
BitField Desc: External FiFo Engine Conflict
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_stk3en_External_FiFO_Engine_Conflict_Bit_Start                                                 11
#define cAf6_stk3en_External_FiFO_Engine_Conflict_Bit_End                                                   11
#define cAf6_stk3en_External_FiFO_Engine_Conflict_Mask                                                  cBit11
#define cAf6_stk3en_External_FiFO_Engine_Conflict_Shift                                                     11
#define cAf6_stk3en_External_FiFO_Engine_Conflict_MaxVal                                                   0x1
#define cAf6_stk3en_External_FiFO_Engine_Conflict_MinVal                                                   0x0
#define cAf6_stk3en_External_FiFO_Engine_Conflict_RstVal                                                   0x0

/*--------------------------------------
BitField Name: QMXFF_Read_Cache_Error
BitField Type: R/W
BitField Desc: QMXFF Read Cache Error
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_stk3en_QMXFF_Read_Cache_Error_Bit_Start                                                        10
#define cAf6_stk3en_QMXFF_Read_Cache_Error_Bit_End                                                          10
#define cAf6_stk3en_QMXFF_Read_Cache_Error_Mask                                                         cBit10
#define cAf6_stk3en_QMXFF_Read_Cache_Error_Shift                                                            10
#define cAf6_stk3en_QMXFF_Read_Cache_Error_MaxVal                                                          0x1
#define cAf6_stk3en_QMXFF_Read_Cache_Error_MinVal                                                          0x0
#define cAf6_stk3en_QMXFF_Read_Cache_Error_RstVal                                                          0x0

/*--------------------------------------
BitField Name: External_FiFO_Full_Thres_CFG
BitField Type: R/W
BitField Desc: External FiFo Full Threshold CFG
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_stk3en_External_FiFO_Full_Thres_CFG_Bit_Start                                                   9
#define cAf6_stk3en_External_FiFO_Full_Thres_CFG_Bit_End                                                     9
#define cAf6_stk3en_External_FiFO_Full_Thres_CFG_Mask                                                    cBit9
#define cAf6_stk3en_External_FiFO_Full_Thres_CFG_Shift                                                       9
#define cAf6_stk3en_External_FiFO_Full_Thres_CFG_MaxVal                                                    0x1
#define cAf6_stk3en_External_FiFO_Full_Thres_CFG_MinVal                                                    0x0
#define cAf6_stk3en_External_FiFO_Full_Thres_CFG_RstVal                                                    0x0

/*--------------------------------------
BitField Name: FiFo_DDR_Valid_Write_Err
BitField Type: R/W
BitField Desc: FiFo store DDR Valid Write Error
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_stk3en_FiFo_DDR_Valid_Write_Err_Bit_Start                                                       8
#define cAf6_stk3en_FiFo_DDR_Valid_Write_Err_Bit_End                                                         8
#define cAf6_stk3en_FiFo_DDR_Valid_Write_Err_Mask                                                        cBit8
#define cAf6_stk3en_FiFo_DDR_Valid_Write_Err_Shift                                                           8
#define cAf6_stk3en_FiFo_DDR_Valid_Write_Err_MaxVal                                                        0x1
#define cAf6_stk3en_FiFo_DDR_Valid_Write_Err_MinVal                                                        0x0
#define cAf6_stk3en_FiFo_DDR_Valid_Write_Err_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Internal_FiFO_Engine_Conflict
BitField Type: R/W
BitField Desc: Internal FiFo Engine Conflict
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_stk3en_Internal_FiFO_Engine_Conflict_Bit_Start                                                  7
#define cAf6_stk3en_Internal_FiFO_Engine_Conflict_Bit_End                                                    7
#define cAf6_stk3en_Internal_FiFO_Engine_Conflict_Mask                                                   cBit7
#define cAf6_stk3en_Internal_FiFO_Engine_Conflict_Shift                                                      7
#define cAf6_stk3en_Internal_FiFO_Engine_Conflict_MaxVal                                                   0x1
#define cAf6_stk3en_Internal_FiFO_Engine_Conflict_MinVal                                                   0x0
#define cAf6_stk3en_Internal_FiFO_Engine_Conflict_RstVal                                                   0x0

/*--------------------------------------
BitField Name: Discard_Queue_CFG_Disable
BitField Type: R/W
BitField Desc: Discard becasue Queue Disable
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_stk3en_Discard_Queue_CFG_Disable_Bit_Start                                                      6
#define cAf6_stk3en_Discard_Queue_CFG_Disable_Bit_End                                                        6
#define cAf6_stk3en_Discard_Queue_CFG_Disable_Mask                                                       cBit6
#define cAf6_stk3en_Discard_Queue_CFG_Disable_Shift                                                          6
#define cAf6_stk3en_Discard_Queue_CFG_Disable_MaxVal                                                       0x1
#define cAf6_stk3en_Discard_Queue_CFG_Disable_MinVal                                                       0x0
#define cAf6_stk3en_Discard_Queue_CFG_Disable_RstVal                                                       0x0

/*--------------------------------------
BitField Name: Discard_PLA_Drop
BitField Type: R/W
BitField Desc: Discard because PLA declare Drop
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_stk3en_Discard_PLA_Drop_Bit_Start                                                               5
#define cAf6_stk3en_Discard_PLA_Drop_Bit_End                                                                 5
#define cAf6_stk3en_Discard_PLA_Drop_Mask                                                                cBit5
#define cAf6_stk3en_Discard_PLA_Drop_Shift                                                                   5
#define cAf6_stk3en_Discard_PLA_Drop_MaxVal                                                                0x1
#define cAf6_stk3en_Discard_PLA_Drop_MinVal                                                                0x0
#define cAf6_stk3en_Discard_PLA_Drop_RstVal                                                                0x0

/*--------------------------------------
BitField Name: Internal_FiFO_Write_Conflict
BitField Type: R/W
BitField Desc: Internal FiFo Write Conflict
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_stk3en_Internal_FiFO_Write_Conflict_Bit_Start                                                   4
#define cAf6_stk3en_Internal_FiFO_Write_Conflict_Bit_End                                                     4
#define cAf6_stk3en_Internal_FiFO_Write_Conflict_Mask                                                    cBit4
#define cAf6_stk3en_Internal_FiFO_Write_Conflict_Shift                                                       4
#define cAf6_stk3en_Internal_FiFO_Write_Conflict_MaxVal                                                    0x1
#define cAf6_stk3en_Internal_FiFO_Write_Conflict_MinVal                                                    0x0
#define cAf6_stk3en_Internal_FiFO_Write_Conflict_RstVal                                                    0x0

/*--------------------------------------
BitField Name: FiFo_DDR_Valid_Write_Err
BitField Type: R/W
BitField Desc: FiFo store DDR Valid Read Error
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_stk3en_FiFo_DDR_Valid_Read_Err_Bit_Start                                                       3
#define cAf6_stk3en_FiFo_DDR_Valid_Read_Err_Bit_End                                                         3
#define cAf6_stk3en_FiFo_DDR_Valid_Read_Err_Mask                                                        cBit3
#define cAf6_stk3en_FiFo_DDR_Valid_Read_Err_Shift                                                           3
#define cAf6_stk3en_FiFo_DDR_Valid_Read_Err_MaxVal                                                        0x1
#define cAf6_stk3en_FiFo_DDR_Valid_Read_Err_MinVal                                                        0x0
#define cAf6_stk3en_FiFo_DDR_Valid_Read_Err_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Discard_Head_FiFo_Write_Error
BitField Type: R/W
BitField Desc: Discard Because Head FiFO write Error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_stk3en_Discard_Head_FiFo_Write_Error_Bit_Start                                                  2
#define cAf6_stk3en_Discard_Head_FiFo_Write_Error_Bit_End                                                    2
#define cAf6_stk3en_Discard_Head_FiFo_Write_Error_Mask                                                   cBit2
#define cAf6_stk3en_Discard_Head_FiFo_Write_Error_Shift                                                      2
#define cAf6_stk3en_Discard_Head_FiFo_Write_Error_MaxVal                                                   0x1
#define cAf6_stk3en_Discard_Head_FiFo_Write_Error_MinVal                                                   0x0
#define cAf6_stk3en_Discard_Head_FiFo_Write_Error_RstVal                                                   0x0

/*--------------------------------------
BitField Name: Discard_Internal_External_Full0
BitField Type: R/W
BitField Desc: Discard Because Internal External Full0
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_stk3en_Discard_Internal_External_Full0_Bit_Start                                                 1
#define cAf6_stk3en_Discard_Internal_External_Full0_Bit_End                                                   1
#define cAf6_stk3en_Discard_Internal_External_Full0_Mask                                                  cBit1
#define cAf6_stk3en_Discard_Internal_External_Full0_Shift                                                     1
#define cAf6_stk3en_Discard_Internal_External_Full0_MaxVal                                                  0x1
#define cAf6_stk3en_Discard_Internal_External_Full0_MinVal                                                  0x0
#define cAf6_stk3en_Discard_Internal_External_Full0_RstVal                                                  0x0

/*--------------------------------------
BitField Name: Discard_Internal_External_Full1
BitField Type: R/W
BitField Desc: Discard Because Internal External Full1
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_stk3en_Discard_Internal_External_Full1_Bit_Start                                                 0
#define cAf6_stk3en_Discard_Internal_External_Full1_Bit_End                                                   0
#define cAf6_stk3en_Discard_Internal_External_Full1_Mask                                                  cBit0
#define cAf6_stk3en_Discard_Internal_External_Full1_Shift                                                     0
#define cAf6_stk3en_Discard_Internal_External_Full1_MaxVal                                                  0x1
#define cAf6_stk3en_Discard_Internal_External_Full1_MinVal                                                  0x0
#define cAf6_stk3en_Discard_Internal_External_Full1_RstVal                                                  0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEMPIGREG_H_ */


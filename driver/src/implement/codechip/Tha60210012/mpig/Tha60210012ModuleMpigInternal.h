/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPIG
 * 
 * File        : Tha60210012ModuleMpigInternal.h
 * 
 * Created Date: Jul 11, 2017
 *
 * Description : Module MPIG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEMPIGINTERNAL_H_
#define _THA60210012MODULEMPIGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/mpig/ThaModuleMpigInternal.h"
#include "Tha60210012ModuleMpig.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleMpigMethods
    {
    eAtRet (*QueueMinThresholdSet)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, uint32 internalThreshold, uint32 externalThreshold);
    uint32 (*QueueMinInternalThresholdGet)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);
    uint32 (*QueueMinExternalThresholdGet)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);

    eAtRet (*QueueMaxThresholdSet)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, uint32 internalThreshold, uint32 externalThreshold);
    uint32 (*QueueMaxInternalThresholdGet)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);
    uint32 (*QueueMaxExternalThresholdGet)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);

    eAtRet (*QueueEnable)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, eBool enable);
    eBool  (*QueueIsEnabled)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);

    eAtRet (*QueueHybridEnable)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, eBool enable);
    eBool  (*QueueHybridIsEnabled)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);

    eAtRet (*QueueQuantumInByteSet)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, uint32 quantumInByte);
    uint32 (*QueueQuantumInByteGet)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);

    eAtRet (*QueuePrioritySet)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId, eThaMpigQueuePriority priority);
    eThaMpigQueuePriority (*QueuePriorityGet)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);

    eAtRet (*QueueCounterDebug)(Tha60210012ModuleMpig self, uint32 queueId, tTha60210012MpigQueueCounter* counters, eBool r2c);
    eThaMpigQueuePriority (*DefaultPriorityOfQueue)(Tha60210012ModuleMpig self, uint32 queueId);

    uint32 (*BaseAddress)(Tha60210012ModuleMpig self);
    uint32 (*QueueThresholdOffset)(Tha60210012ModuleMpig self, uint8 ethPortId, uint32 queueId);

    }tTha60210012ModuleMpigMethods;

typedef struct tTha60210012ModuleMpig
    {
    tThaModuleMpig super;
    const tTha60210012ModuleMpigMethods* methods;

    }tTha60210012ModuleMpig;

typedef struct tBitFieldInfo
    {
    const char *name;
    uint32 mask;
    uint32 shift;
    }tBitFieldInfo;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210012ModuleMpigObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEMPIGINTERNAL_H_ */


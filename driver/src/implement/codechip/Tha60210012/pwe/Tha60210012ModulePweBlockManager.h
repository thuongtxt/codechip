/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60210012ModulePweBlockManager.h
 * 
 * Created Date: May 24, 2016
 *
 * Description : Block manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEPWEBLOCKMANAGER_H_
#define _THA60210012MODULEPWEBLOCKMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60210012ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cDe1BlockType   0
#define cNxDs0BlockType 0
#define cVc1xBlockType  0
#define cDe3BlockType   1
#define cSts1BlockType  1

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Blocks for circuits that bound to PW */
eAtRet Tha60210012PwCircuitNxDs0BlockAllocate(AtChannel self);
eAtRet Tha60210012PwCircuitVc1xBlockAllocate(AtChannel self);
eAtRet Tha60210012PwCircuitDe1BlockAllocate(AtChannel self);
eAtRet Tha60210012PwCircuitDe3BlockAllocate(AtChannel self);
eAtRet Tha60210012PwCircuitSts1VcBlockAllocate(AtChannel self);
eAtRet Tha60210012PwCircuitHoVcBlockAllocate(AtChannel self);

/* Blocks for channels that work with VCG */
eAtRet Tha60210012VcgVc1xMemberBlockAllocate(AtChannel self);
eAtRet Tha60210012VcgDe1MemberBlockAllocate(AtChannel self);
eAtRet Tha60210012VcgDe3MemberBlockAllocate(AtChannel self);
eAtRet Tha60210012VcgSts1MemberBlockAllocate(AtChannel self);

eAtRet Tha60210012ChannelBlockDeallocate(AtChannel channel);
eAtRet Tha60210012ChannelHwLoBlockDynamicAllocate(AtChannel channel, uint32 hwChannelId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEPWEBLOCKMANAGER_H_ */


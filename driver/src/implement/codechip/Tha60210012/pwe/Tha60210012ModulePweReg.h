/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0012_RD_PWE_H_
#define _AF6_REG_AF6CCI0012_RD_PWE_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Ethernet Header Mode Control
Reg Addr   : 0x00000000 - 0x001FFF #The address format for these registers is 0x00000000 + PWID
Reg Formula: 0x00000000 +  PWID
    Where  : 
           + $PWID(0-8191): Pseudowire ID
Reg Desc   : 
This register is used to control read/write Pseudowire Transmit Ethernet Header Value from/to DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_pw_txeth_hdr_mode_Base                                                              0x00000000
#define cAf6Reg_pw_txeth_hdr_mode(PWID)                                                    (0x00000000+(PWID))
#define cAf6Reg_pw_txeth_hdr_mode_WidthVal                                                                  32
#define cAf6Reg_pw_txeth_hdr_mode_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: TxEthCwEn
BitField Type: RW
BitField Desc: this is configuration to enable CW insertion
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthCwEn_Bit_Start                                                        12
#define cAf6_pw_txeth_hdr_mode_TxEthCwEn_Bit_End                                                          12
#define cAf6_pw_txeth_hdr_mode_TxEthCwEn_Mask                                                         cBit12
#define cAf6_pw_txeth_hdr_mode_TxEthCwEn_Shift                                                            12

/*--------------------------------------
BitField Name: TxEthPtch1Byte
BitField Type: RW
BitField Desc: this is configuration to enable PTCH insertion 1 byte (MSB)
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPtch1Byte_Bit_Start                                                     11
#define cAf6_pw_txeth_hdr_mode_TxEthPtch1Byte_Bit_End                                                       11
#define cAf6_pw_txeth_hdr_mode_TxEthPtch1Byte_Mask                                                      cBit11
#define cAf6_pw_txeth_hdr_mode_TxEthPtch1Byte_Shift                                                         11
#define cAf6_pw_txeth_hdr_mode_TxEthPtch1Byte_MaxVal                                                       0x1
#define cAf6_pw_txeth_hdr_mode_TxEthPtch1Byte_MinVal                                                       0x0
#define cAf6_pw_txeth_hdr_mode_TxEthPtch1Byte_RstVal                                                       0x0

/*--------------------------------------
BitField Name: TxEthPtchEn
BitField Type: RW
BitField Desc: this is configuration to enable PTCH insertion
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPtchEn_Bit_Start                                                        10
#define cAf6_pw_txeth_hdr_mode_TxEthPtchEn_Bit_End                                                          10
#define cAf6_pw_txeth_hdr_mode_TxEthPtchEn_Mask                                                         cBit10
#define cAf6_pw_txeth_hdr_mode_TxEthPtchEn_Shift                                                            10
#define cAf6_pw_txeth_hdr_mode_TxEthPtchEn_MaxVal                                                          0x1
#define cAf6_pw_txeth_hdr_mode_TxEthPtchEn_MinVal                                                          0x0
#define cAf6_pw_txeth_hdr_mode_TxEthPtchEn_RstVal                                                          0x0

/*--------------------------------------
BitField Name: TxEthMsPW
BitField Type: RW
BitField Desc: this is configuration for MS PW service (IPv4/ETH over MPLS)
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthMsPW_Bit_Start                                                           9
#define cAf6_pw_txeth_hdr_mode_TxEthMsPW_Bit_End                                                             9
#define cAf6_pw_txeth_hdr_mode_TxEthMsPW_Mask                                                            cBit9
#define cAf6_pw_txeth_hdr_mode_TxEthMsPW_Shift                                                               9
#define cAf6_pw_txeth_hdr_mode_TxEthMsPW_MaxVal                                                            0x1
#define cAf6_pw_txeth_hdr_mode_TxEthMsPW_MinVal                                                            0x0
#define cAf6_pw_txeth_hdr_mode_TxEthMsPW_RstVal                                                            0x0

/*--------------------------------------
BitField Name: TxEthPadMode
BitField Type: RW
BitField Desc: this is configuration for insertion PAD in short packets 0:
Insert PAD when total Ethernet packet length is less than 64 bytes. Refer to
IEEE 802.3 1: Insert PAD when control word plus payload length is less than 64
bytes 2: Insert PAD when total Ethernet packet length minus VLANs and MPLS outer
labels(if exist) is less than 64 bytes
BitField Bits: [8:7]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_Bit_Start                                                        7
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_Bit_End                                                          8
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_Mask                                                       cBit8_7
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_Shift                                                            7
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_MaxVal                                                         0x3
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_MinVal                                                         0x0
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_RstVal                                                         0x0

/*--------------------------------------
BitField Name: TxEthPwRtpEn
BitField Type: RW
BitField Desc: this is RTP enable for TDM PW packet 1: Enable RTP 0: Disable RTP
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_Bit_Start                                                        6
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_Bit_End                                                          6
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_Mask                                                         cBit6
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_Shift                                                            6
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_MaxVal                                                         0x1
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_MinVal                                                         0x0
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_RstVal                                                         0x0

/*--------------------------------------
BitField Name: TxEthPwPsnType
BitField Type: RW
BitField Desc: this is Transmit PSN header mode working 1: PW PSN header is
UDP/IPv4 2: PW PSN header is UDP/IPv6 3: PW MPLS no outer label over Ipv4 (total
1 MPLS label) 4: PW MPLS no outer label over Ipv6 (total 1 MPLS label) 5: PW
MPLS one outer label over Ipv4 (total 2 MPLS label) 6: PW MPLS one outer label
over Ipv6 (total 2 MPLS label) 7: PW MPLS two outer label over Ipv4 (total 3
MPLS label) 8: PW MPLS two outer label over Ipv6 (total 3 MPLS label) Others:
for other PW PSN header type
BitField Bits: [5:2]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_Bit_Start                                                      2
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_Bit_End                                                        5
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_Mask                                                     cBit5_2
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_Shift                                                          2
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_MaxVal                                                       0xf
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_MinVal                                                       0x0
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_RstVal                                                       0x0

/*--------------------------------------
BitField Name: TxEthPwNumVlan
BitField Type: RW
BitField Desc: This is number of vlan in Transmit Ethernet packet 0: no vlan 1:
1 vlan 2: 2 vlan
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_Bit_Start                                                      0
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_Bit_End                                                        1
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_Mask                                                     cBit1_0
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_Shift                                                          0
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_MaxVal                                                       0x3
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_MinVal                                                       0x0
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count0_64 bytes packet
Reg Addr   : 0x0004000(RO)
Reg Formula: 0x0004000 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt0_64_ro_Base                                                                  0x0004000
#define cAf6Reg_TxEth_cnt0_64_ro(ethport)                                                  (0x0004000+(ethport))
#define cAf6Reg_TxEth_cnt0_64_ro_WidthVal                                                                     32
#define cAf6Reg_TxEth_cnt0_64_ro_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: CLAEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt0_64_ro_CLAEthCnt0_64_Bit_Start                                                          0
#define cAf6_TxEth_cnt0_64_ro_CLAEthCnt0_64_Bit_End                                                           31
#define cAf6_TxEth_cnt0_64_ro_CLAEthCnt0_64_Mask                                                        cBit31_0
#define cAf6_TxEth_cnt0_64_ro_CLAEthCnt0_64_Shift                                                              0
#define cAf6_TxEth_cnt0_64_ro_CLAEthCnt0_64_MaxVal                                                    0xffffffff
#define cAf6_TxEth_cnt0_64_ro_CLAEthCnt0_64_MinVal                                                           0x0
#define cAf6_TxEth_cnt0_64_ro_CLAEthCnt0_64_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count0_64 bytes packet
Reg Addr   : 0x0004800(RC)
Reg Formula: 0x0004800 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt0_64_rc_Base                                                                  0x0004800
#define cAf6Reg_TxEth_cnt0_64_rc(ethport)                                                  (0x0004800+(ethport))
#define cAf6Reg_TxEth_cnt0_64_rc_WidthVal                                                                     32
#define cAf6Reg_TxEth_cnt0_64_rc_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: CLAEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt0_64_rc_CLAEthCnt0_64_Bit_Start                                                          0
#define cAf6_TxEth_cnt0_64_rc_CLAEthCnt0_64_Bit_End                                                           31
#define cAf6_TxEth_cnt0_64_rc_CLAEthCnt0_64_Mask                                                        cBit31_0
#define cAf6_TxEth_cnt0_64_rc_CLAEthCnt0_64_Shift                                                              0
#define cAf6_TxEth_cnt0_64_rc_CLAEthCnt0_64_MaxVal                                                    0xffffffff
#define cAf6_TxEth_cnt0_64_rc_CLAEthCnt0_64_MinVal                                                           0x0
#define cAf6_TxEth_cnt0_64_rc_CLAEthCnt0_64_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count65_127 bytes packet
Reg Addr   : 0x0004002(RO)
Reg Formula: 0x0004002 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt65_127_ro_Base                                                                0x0004002
#define cAf6Reg_TxEth_cnt65_127_ro(ethport)                                                (0x0004002+(ethport))
#define cAf6Reg_TxEth_cnt65_127_ro_WidthVal                                                                   32
#define cAf6Reg_TxEth_cnt65_127_ro_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt65_127_ro_CLAEthCnt65_127_Bit_Start                                                      0
#define cAf6_TxEth_cnt65_127_ro_CLAEthCnt65_127_Bit_End                                                       31
#define cAf6_TxEth_cnt65_127_ro_CLAEthCnt65_127_Mask                                                    cBit31_0
#define cAf6_TxEth_cnt65_127_ro_CLAEthCnt65_127_Shift                                                          0
#define cAf6_TxEth_cnt65_127_ro_CLAEthCnt65_127_MaxVal                                                0xffffffff
#define cAf6_TxEth_cnt65_127_ro_CLAEthCnt65_127_MinVal                                                       0x0
#define cAf6_TxEth_cnt65_127_ro_CLAEthCnt65_127_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count65_127 bytes packet
Reg Addr   : 0x0004802(RC)
Reg Formula: 0x0004802 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt65_127_rc_Base                                                                0x0004802
#define cAf6Reg_TxEth_cnt65_127_rc(ethport)                                                (0x0004802+(ethport))
#define cAf6Reg_TxEth_cnt65_127_rc_WidthVal                                                                   32
#define cAf6Reg_TxEth_cnt65_127_rc_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt65_127_rc_CLAEthCnt65_127_Bit_Start                                                      0
#define cAf6_TxEth_cnt65_127_rc_CLAEthCnt65_127_Bit_End                                                       31
#define cAf6_TxEth_cnt65_127_rc_CLAEthCnt65_127_Mask                                                    cBit31_0
#define cAf6_TxEth_cnt65_127_rc_CLAEthCnt65_127_Shift                                                          0
#define cAf6_TxEth_cnt65_127_rc_CLAEthCnt65_127_MaxVal                                                0xffffffff
#define cAf6_TxEth_cnt65_127_rc_CLAEthCnt65_127_MinVal                                                       0x0
#define cAf6_TxEth_cnt65_127_rc_CLAEthCnt65_127_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count128_255 bytes packet
Reg Addr   : 0x0004004(RO)
Reg Formula: 0x0004004 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt128_255_ro_Base                                                               0x0004004
#define cAf6Reg_TxEth_cnt128_255_ro(ethport)                                               (0x0004004+(ethport))
#define cAf6Reg_TxEth_cnt128_255_ro_WidthVal                                                                  32
#define cAf6Reg_TxEth_cnt128_255_ro_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt128_255_ro_CLAEthCnt128_255_Bit_Start                                                    0
#define cAf6_TxEth_cnt128_255_ro_CLAEthCnt128_255_Bit_End                                                     31
#define cAf6_TxEth_cnt128_255_ro_CLAEthCnt128_255_Mask                                                  cBit31_0
#define cAf6_TxEth_cnt128_255_ro_CLAEthCnt128_255_Shift                                                        0
#define cAf6_TxEth_cnt128_255_ro_CLAEthCnt128_255_MaxVal                                              0xffffffff
#define cAf6_TxEth_cnt128_255_ro_CLAEthCnt128_255_MinVal                                                     0x0
#define cAf6_TxEth_cnt128_255_ro_CLAEthCnt128_255_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count128_255 bytes packet
Reg Addr   : 0x0004804(RC)
Reg Formula: 0x0004804 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt128_255_rc_Base                                                               0x0004804
#define cAf6Reg_TxEth_cnt128_255_rc(ethport)                                               (0x0004804+(ethport))
#define cAf6Reg_TxEth_cnt128_255_rc_WidthVal                                                                  32
#define cAf6Reg_TxEth_cnt128_255_rc_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt128_255_rc_CLAEthCnt128_255_Bit_Start                                                    0
#define cAf6_TxEth_cnt128_255_rc_CLAEthCnt128_255_Bit_End                                                     31
#define cAf6_TxEth_cnt128_255_rc_CLAEthCnt128_255_Mask                                                  cBit31_0
#define cAf6_TxEth_cnt128_255_rc_CLAEthCnt128_255_Shift                                                        0
#define cAf6_TxEth_cnt128_255_rc_CLAEthCnt128_255_MaxVal                                              0xffffffff
#define cAf6_TxEth_cnt128_255_rc_CLAEthCnt128_255_MinVal                                                     0x0
#define cAf6_TxEth_cnt128_255_rc_CLAEthCnt128_255_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count256_511 bytes packet
Reg Addr   : 0x0004006(RO)
Reg Formula: 0x0004006 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt256_511_ro_Base                                                               0x0004006
#define cAf6Reg_TxEth_cnt256_511_ro(ethport)                                               (0x0004006+(ethport))
#define cAf6Reg_TxEth_cnt256_511_ro_WidthVal                                                                  32
#define cAf6Reg_TxEth_cnt256_511_ro_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt256_511_ro_CLAEthCnt256_511_Bit_Start                                                    0
#define cAf6_TxEth_cnt256_511_ro_CLAEthCnt256_511_Bit_End                                                     31
#define cAf6_TxEth_cnt256_511_ro_CLAEthCnt256_511_Mask                                                  cBit31_0
#define cAf6_TxEth_cnt256_511_ro_CLAEthCnt256_511_Shift                                                        0
#define cAf6_TxEth_cnt256_511_ro_CLAEthCnt256_511_MaxVal                                              0xffffffff
#define cAf6_TxEth_cnt256_511_ro_CLAEthCnt256_511_MinVal                                                     0x0
#define cAf6_TxEth_cnt256_511_ro_CLAEthCnt256_511_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count256_511 bytes packet
Reg Addr   : 0x0004806(RC)
Reg Formula: 0x0004806 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt256_511_rc_Base                                                               0x0004806
#define cAf6Reg_TxEth_cnt256_511_rc(ethport)                                               (0x0004806+(ethport))
#define cAf6Reg_TxEth_cnt256_511_rc_WidthVal                                                                  32
#define cAf6Reg_TxEth_cnt256_511_rc_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt256_511_rc_CLAEthCnt256_511_Bit_Start                                                    0
#define cAf6_TxEth_cnt256_511_rc_CLAEthCnt256_511_Bit_End                                                     31
#define cAf6_TxEth_cnt256_511_rc_CLAEthCnt256_511_Mask                                                  cBit31_0
#define cAf6_TxEth_cnt256_511_rc_CLAEthCnt256_511_Shift                                                        0
#define cAf6_TxEth_cnt256_511_rc_CLAEthCnt256_511_MaxVal                                              0xffffffff
#define cAf6_TxEth_cnt256_511_rc_CLAEthCnt256_511_MinVal                                                     0x0
#define cAf6_TxEth_cnt256_511_rc_CLAEthCnt256_511_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count512_1023 bytes packet
Reg Addr   : 0x0004008(RO)
Reg Formula: 0x0004008 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt512_1024_ro_Base                                                              0x0004008
#define cAf6Reg_TxEth_cnt512_1024_ro(ethport)                                              (0x0004008+(ethport))
#define cAf6Reg_TxEth_cnt512_1024_ro_WidthVal                                                                 32
#define cAf6Reg_TxEth_cnt512_1024_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCnt512_1024
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt512_1024_ro_CLAEthCnt512_1024_Bit_Start                                                  0
#define cAf6_TxEth_cnt512_1024_ro_CLAEthCnt512_1024_Bit_End                                                   31
#define cAf6_TxEth_cnt512_1024_ro_CLAEthCnt512_1024_Mask                                                cBit31_0
#define cAf6_TxEth_cnt512_1024_ro_CLAEthCnt512_1024_Shift                                                      0
#define cAf6_TxEth_cnt512_1024_ro_CLAEthCnt512_1024_MaxVal                                            0xffffffff
#define cAf6_TxEth_cnt512_1024_ro_CLAEthCnt512_1024_MinVal                                                   0x0
#define cAf6_TxEth_cnt512_1024_ro_CLAEthCnt512_1024_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count512_1023 bytes packet
Reg Addr   : 0x0004808(RC)
Reg Formula: 0x0004808 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt512_1024_rc_Base                                                              0x0004808
#define cAf6Reg_TxEth_cnt512_1024_rc(ethport)                                              (0x0004808+(ethport))
#define cAf6Reg_TxEth_cnt512_1024_rc_WidthVal                                                                 32
#define cAf6Reg_TxEth_cnt512_1024_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCnt512_1024
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt512_1024_rc_CLAEthCnt512_1024_Bit_Start                                                  0
#define cAf6_TxEth_cnt512_1024_rc_CLAEthCnt512_1024_Bit_End                                                   31
#define cAf6_TxEth_cnt512_1024_rc_CLAEthCnt512_1024_Mask                                                cBit31_0
#define cAf6_TxEth_cnt512_1024_rc_CLAEthCnt512_1024_Shift                                                      0
#define cAf6_TxEth_cnt512_1024_rc_CLAEthCnt512_1024_MaxVal                                            0xffffffff
#define cAf6_TxEth_cnt512_1024_rc_CLAEthCnt512_1024_MinVal                                                   0x0
#define cAf6_TxEth_cnt512_1024_rc_CLAEthCnt512_1024_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1024_1518 bytes packet
Reg Addr   : 0x000400A(RO)
Reg Formula: 0x000400A + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt1025_1528_ro_Base                                                             0x000400A
#define cAf6Reg_TxEth_cnt1025_1528_ro(ethport)                                             (0x000400A+(ethport))
#define cAf6Reg_TxEth_cnt1025_1528_ro_WidthVal                                                                32
#define cAf6Reg_TxEth_cnt1025_1528_ro_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1025_1528
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt1025_1528_ro_CLAEthCnt1025_1528_Bit_Start                                                0
#define cAf6_TxEth_cnt1025_1528_ro_CLAEthCnt1025_1528_Bit_End                                                 31
#define cAf6_TxEth_cnt1025_1528_ro_CLAEthCnt1025_1528_Mask                                              cBit31_0
#define cAf6_TxEth_cnt1025_1528_ro_CLAEthCnt1025_1528_Shift                                                    0
#define cAf6_TxEth_cnt1025_1528_ro_CLAEthCnt1025_1528_MaxVal                                          0xffffffff
#define cAf6_TxEth_cnt1025_1528_ro_CLAEthCnt1025_1528_MinVal                                                 0x0
#define cAf6_TxEth_cnt1025_1528_ro_CLAEthCnt1025_1528_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1024_1518 bytes packet
Reg Addr   : 0x000480A(RC)
Reg Formula: 0x000480A + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt1025_1528_rc_Base                                                             0x000480A
#define cAf6Reg_TxEth_cnt1025_1528_rc(ethport)                                             (0x000480A+(ethport))
#define cAf6Reg_TxEth_cnt1025_1528_rc_WidthVal                                                                32
#define cAf6Reg_TxEth_cnt1025_1528_rc_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1025_1528
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt1025_1528_rc_CLAEthCnt1025_1528_Bit_Start                                                0
#define cAf6_TxEth_cnt1025_1528_rc_CLAEthCnt1025_1528_Bit_End                                                 31
#define cAf6_TxEth_cnt1025_1528_rc_CLAEthCnt1025_1528_Mask                                              cBit31_0
#define cAf6_TxEth_cnt1025_1528_rc_CLAEthCnt1025_1528_Shift                                                    0
#define cAf6_TxEth_cnt1025_1528_rc_CLAEthCnt1025_1528_MaxVal                                          0xffffffff
#define cAf6_TxEth_cnt1025_1528_rc_CLAEthCnt1025_1528_MinVal                                                 0x0
#define cAf6_TxEth_cnt1025_1528_rc_CLAEthCnt1025_1528_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1519_2047 bytes packet
Reg Addr   : 0x000400C(RO)
Reg Formula: 0x000400C + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt1529_2047_ro_Base                                                             0x000400C
#define cAf6Reg_TxEth_cnt1529_2047_ro(ethport)                                             (0x000400C+(ethport))
#define cAf6Reg_TxEth_cnt1529_2047_ro_WidthVal                                                                32
#define cAf6Reg_TxEth_cnt1529_2047_ro_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1529_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt1529_2047_ro_CLAEthCnt1529_2047_Bit_Start                                                0
#define cAf6_TxEth_cnt1529_2047_ro_CLAEthCnt1529_2047_Bit_End                                                 31
#define cAf6_TxEth_cnt1529_2047_ro_CLAEthCnt1529_2047_Mask                                              cBit31_0
#define cAf6_TxEth_cnt1529_2047_ro_CLAEthCnt1529_2047_Shift                                                    0
#define cAf6_TxEth_cnt1529_2047_ro_CLAEthCnt1529_2047_MaxVal                                          0xffffffff
#define cAf6_TxEth_cnt1529_2047_ro_CLAEthCnt1529_2047_MinVal                                                 0x0
#define cAf6_TxEth_cnt1529_2047_ro_CLAEthCnt1529_2047_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1519_2047 bytes packet
Reg Addr   : 0x000480C(RC)
Reg Formula: 0x000480C + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt1529_2047_rc_Base                                                             0x000480C
#define cAf6Reg_TxEth_cnt1529_2047_rc(ethport)                                             (0x000480C+(ethport))
#define cAf6Reg_TxEth_cnt1529_2047_rc_WidthVal                                                                32
#define cAf6Reg_TxEth_cnt1529_2047_rc_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1529_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt1529_2047_rc_CLAEthCnt1529_2047_Bit_Start                                                0
#define cAf6_TxEth_cnt1529_2047_rc_CLAEthCnt1529_2047_Bit_End                                                 31
#define cAf6_TxEth_cnt1529_2047_rc_CLAEthCnt1529_2047_Mask                                              cBit31_0
#define cAf6_TxEth_cnt1529_2047_rc_CLAEthCnt1529_2047_Shift                                                    0
#define cAf6_TxEth_cnt1529_2047_rc_CLAEthCnt1529_2047_MaxVal                                          0xffffffff
#define cAf6_TxEth_cnt1529_2047_rc_CLAEthCnt1529_2047_MinVal                                                 0x0
#define cAf6_TxEth_cnt1529_2047_rc_CLAEthCnt1529_2047_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Jumbo packet
Reg Addr   : 0x000400E(RO)
Reg Formula: 0x000400E + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt_jumbo_ro_Base                                                                0x000400E
#define cAf6Reg_TxEth_cnt_jumbo_ro(ethport)                                                (0x000400E+(ethport))
#define cAf6Reg_TxEth_cnt_jumbo_ro_WidthVal                                                                   32
#define cAf6Reg_TxEth_cnt_jumbo_ro_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt_jumbo_ro_CLAEthCntJumbo_Bit_Start                                                       0
#define cAf6_TxEth_cnt_jumbo_ro_CLAEthCntJumbo_Bit_End                                                        31
#define cAf6_TxEth_cnt_jumbo_ro_CLAEthCntJumbo_Mask                                                     cBit31_0
#define cAf6_TxEth_cnt_jumbo_ro_CLAEthCntJumbo_Shift                                                           0
#define cAf6_TxEth_cnt_jumbo_ro_CLAEthCntJumbo_MaxVal                                                 0xffffffff
#define cAf6_TxEth_cnt_jumbo_ro_CLAEthCntJumbo_MinVal                                                        0x0
#define cAf6_TxEth_cnt_jumbo_ro_CLAEthCntJumbo_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Jumbo packet
Reg Addr   : 0x000480E(RC)
Reg Formula: 0x000480E + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt_jumbo_rc_Base                                                                0x000480E
#define cAf6Reg_TxEth_cnt_jumbo_rc(ethport)                                                (0x000480E+(ethport))
#define cAf6Reg_TxEth_cnt_jumbo_rc_WidthVal                                                                   32
#define cAf6Reg_TxEth_cnt_jumbo_rc_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt_jumbo_rc_CLAEthCntJumbo_Bit_Start                                                       0
#define cAf6_TxEth_cnt_jumbo_rc_CLAEthCntJumbo_Bit_End                                                        31
#define cAf6_TxEth_cnt_jumbo_rc_CLAEthCntJumbo_Mask                                                     cBit31_0
#define cAf6_TxEth_cnt_jumbo_rc_CLAEthCntJumbo_Shift                                                           0
#define cAf6_TxEth_cnt_jumbo_rc_CLAEthCntJumbo_MaxVal                                                 0xffffffff
#define cAf6_TxEth_cnt_jumbo_rc_CLAEthCntJumbo_MinVal                                                        0x0
#define cAf6_TxEth_cnt_jumbo_rc_CLAEthCntJumbo_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Unicast packet
Reg Addr   : 0x0004010(RO)
Reg Formula: 0x0004010 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the unicast packet

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt_Unicast_ro_Base                                                              0x0004010
#define cAf6Reg_TxEth_cnt_Unicast_ro(ethport)                                              (0x0004010+(ethport))
#define cAf6Reg_TxEth_cnt_Unicast_ro_WidthVal                                                                 32
#define cAf6Reg_TxEth_cnt_Unicast_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntUnicast
BitField Type: RO
BitField Desc: This is statistic counter for the unicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt_Unicast_ro_CLAEthCntUnicast_Bit_Start                                                   0
#define cAf6_TxEth_cnt_Unicast_ro_CLAEthCntUnicast_Bit_End                                                    31
#define cAf6_TxEth_cnt_Unicast_ro_CLAEthCntUnicast_Mask                                                 cBit31_0
#define cAf6_TxEth_cnt_Unicast_ro_CLAEthCntUnicast_Shift                                                       0
#define cAf6_TxEth_cnt_Unicast_ro_CLAEthCntUnicast_MaxVal                                             0xffffffff
#define cAf6_TxEth_cnt_Unicast_ro_CLAEthCntUnicast_MinVal                                                    0x0
#define cAf6_TxEth_cnt_Unicast_ro_CLAEthCntUnicast_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Unicast packet
Reg Addr   : 0x0004810(RC)
Reg Formula: 0x0004810 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the unicast packet

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt_Unicast_rc_Base                                                              0x0004810
#define cAf6Reg_TxEth_cnt_Unicast_rc(ethport)                                              (0x0004810+(ethport))
#define cAf6Reg_TxEth_cnt_Unicast_rc_WidthVal                                                                 32
#define cAf6Reg_TxEth_cnt_Unicast_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntUnicast
BitField Type: RO
BitField Desc: This is statistic counter for the unicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt_Unicast_rc_CLAEthCntUnicast_Bit_Start                                                   0
#define cAf6_TxEth_cnt_Unicast_rc_CLAEthCntUnicast_Bit_End                                                    31
#define cAf6_TxEth_cnt_Unicast_rc_CLAEthCntUnicast_Mask                                                 cBit31_0
#define cAf6_TxEth_cnt_Unicast_rc_CLAEthCntUnicast_Shift                                                       0
#define cAf6_TxEth_cnt_Unicast_rc_CLAEthCntUnicast_MaxVal                                             0xffffffff
#define cAf6_TxEth_cnt_Unicast_rc_CLAEthCntUnicast_MinVal                                                    0x0
#define cAf6_TxEth_cnt_Unicast_rc_CLAEthCntUnicast_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Total packet
Reg Addr   : 0x0004012(RO)
Reg Formula: 0x0004012 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the total packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_pkt_cnt_ro_Base                                                               0x0004012
#define cAf6Reg_eth_tx_pkt_cnt_ro(ethport)                                               (0x0004012+(ethport))
#define cAf6Reg_eth_tx_pkt_cnt_ro_WidthVal                                                                  32
#define cAf6Reg_eth_tx_pkt_cnt_ro_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_Bit_Start                                                      0
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_Bit_End                                                       31
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_Mask                                                    cBit31_0
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_Shift                                                          0
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_MaxVal                                                0xffffffff
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_MinVal                                                       0x0
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Total packet
Reg Addr   : 0x0004812(RC)
Reg Formula: 0x0004812 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the total packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_pkt_cnt_rc_Base                                                               0x0004812
#define cAf6Reg_eth_tx_pkt_cnt_rc(ethport)                                               (0x0004812+(ethport))
#define cAf6Reg_eth_tx_pkt_cnt_rc_WidthVal                                                                  32
#define cAf6Reg_eth_tx_pkt_cnt_rc_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_Bit_Start                                                      0
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_Bit_End                                                       31
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_Mask                                                    cBit31_0
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_Shift                                                          0
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_MaxVal                                                0xffffffff
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_MinVal                                                       0x0
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Broadcast packet
Reg Addr   : 0x0004014(RO)
Reg Formula: 0x0004014 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the Broadcast packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_bcast_pkt_cnt_ro_Base                                                         0x0004014
#define cAf6Reg_eth_tx_bcast_pkt_cnt_ro(ethport)                                         (0x0004014+(ethport))
#define cAf6Reg_eth_tx_bcast_pkt_cnt_ro_WidthVal                                                            32
#define cAf6Reg_eth_tx_bcast_pkt_cnt_ro_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: CLAEthCntBroadcast
BitField Type: RO
BitField Desc: This is statistic counter broadcast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Bit_Start                                            0
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Bit_End                                             31
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Mask                                          cBit31_0
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Shift                                                0
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_MaxVal                                      0xffffffff
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_MinVal                                             0x0
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Broadcast packet
Reg Addr   : 0x0004814(RC)
Reg Formula: 0x0004814 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the Broadcast packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_bcast_pkt_cnt_rc_Base                                                         0x0004814
#define cAf6Reg_eth_tx_bcast_pkt_cnt_rc(ethport)                                         (0x0004814+(ethport))
#define cAf6Reg_eth_tx_bcast_pkt_cnt_rc_WidthVal                                                            32
#define cAf6Reg_eth_tx_bcast_pkt_cnt_rc_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: CLAEthCntBroadcast
BitField Type: RO
BitField Desc: This is statistic counter broadcast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Bit_Start                                            0
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Bit_End                                             31
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Mask                                          cBit31_0
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Shift                                                0
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_MaxVal                                      0xffffffff
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_MinVal                                             0x0
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Multicast packet
Reg Addr   : 0x0004016(RO)
Reg Formula: 0x0004016 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the Multicast packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_mcast_pkt_cnt_ro_Base                                                         0x0004016
#define cAf6Reg_eth_tx_mcast_pkt_cnt_ro(ethport)                                         (0x0004016+(ethport))
#define cAf6Reg_eth_tx_mcast_pkt_cnt_ro_WidthVal                                                            32
#define cAf6Reg_eth_tx_mcast_pkt_cnt_ro_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: CLAEthCntMulticast
BitField Type: RO
BitField Desc: This is statistic counter multicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_Bit_Start                                            0
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_Bit_End                                             31
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_Mask                                          cBit31_0
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_Shift                                                0
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_MaxVal                                      0xffffffff
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_MinVal                                             0x0
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Multicast packet
Reg Addr   : 0x0004816(RC)
Reg Formula: 0x0004816 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the Multicast packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_mcast_pkt_cnt_rc_Base                                                         0x0004816
#define cAf6Reg_eth_tx_mcast_pkt_cnt_rc(ethport)                                         (0x0004816+(ethport))
#define cAf6Reg_eth_tx_mcast_pkt_cnt_rc_WidthVal                                                            32
#define cAf6Reg_eth_tx_mcast_pkt_cnt_rc_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: CLAEthCntMulticast
BitField Type: RO
BitField Desc: This is statistic counter multicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_Bit_Start                                            0
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_Bit_End                                             31
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_Mask                                          cBit31_0
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_Shift                                                0
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_MaxVal                                      0xffffffff
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_MinVal                                             0x0
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count number of bytes of packet
Reg Addr   : 0x000401E(RO)
Reg Formula: 0x000401E + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic count number of bytes of packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt_byte_ro_Base                                                                 0x000401E
#define cAf6Reg_TxEth_cnt_byte_ro(ethport)                                                 (0x000401E+(ethport))
#define cAf6Reg_TxEth_cnt_byte_ro_WidthVal                                                                    32
#define cAf6Reg_TxEth_cnt_byte_ro_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: CLAEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt_byte_ro_CLAEthCntByte_Bit_Start                                                         0
#define cAf6_TxEth_cnt_byte_ro_CLAEthCntByte_Bit_End                                                          31
#define cAf6_TxEth_cnt_byte_ro_CLAEthCntByte_Mask                                                       cBit31_0
#define cAf6_TxEth_cnt_byte_ro_CLAEthCntByte_Shift                                                             0
#define cAf6_TxEth_cnt_byte_ro_CLAEthCntByte_MaxVal                                                   0xffffffff
#define cAf6_TxEth_cnt_byte_ro_CLAEthCntByte_MinVal                                                          0x0
#define cAf6_TxEth_cnt_byte_ro_CLAEthCntByte_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count number of bytes of packet
Reg Addr   : 0x000481E(RC)
Reg Formula: 0x000481E + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic count number of bytes of packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt_byte_rc_Base                                                                 0x000481E
#define cAf6Reg_TxEth_cnt_byte_rc(ethport)                                                 (0x000481E+(ethport))
#define cAf6Reg_TxEth_cnt_byte_rc_WidthVal                                                                    32
#define cAf6Reg_TxEth_cnt_byte_rc_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: CLAEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt_byte_rc_CLAEthCntByte_Bit_Start                                                         0
#define cAf6_TxEth_cnt_byte_rc_CLAEthCntByte_Bit_End                                                          31
#define cAf6_TxEth_cnt_byte_rc_CLAEthCntByte_Mask                                                       cBit31_0
#define cAf6_TxEth_cnt_byte_rc_CLAEthCntByte_Shift                                                             0
#define cAf6_TxEth_cnt_byte_rc_CLAEthCntByte_MaxVal                                                   0xffffffff
#define cAf6_TxEth_cnt_byte_rc_CLAEthCntByte_MinVal                                                          0x0
#define cAf6_TxEth_cnt_byte_rc_CLAEthCntByte_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Hold Register Status
Reg Addr   : 0x00C00A - 0x00C00D
Reg Formula: 0x00C00A +  HID
    Where  : 
           + $HID(0-3): Hold ID
Reg Desc   : 
This register using for hold remain that more than 128bits

------------------------------------------------------------------------------*/
#define cAf6Reg_pwe_hold_status_Base                                                                  0x00C00A
#define cAf6Reg_pwe_hold_status(HID)                                                          (0x00C00A+(HID))
#define cAf6Reg_pwe_hold_status_WidthVal                                                                    32
#define cAf6Reg_pwe_hold_status_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PwHoldStatus
BitField Type: RW
BitField Desc: Hold 32bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_pwe_hold_status_PwHoldStatus_Bit_Start                                                          0
#define cAf6_pwe_hold_status_PwHoldStatus_Bit_End                                                           31
#define cAf6_pwe_hold_status_PwHoldStatus_Mask                                                        cBit31_0
#define cAf6_pwe_hold_status_PwHoldStatus_Shift                                                              0
#define cAf6_pwe_hold_status_PwHoldStatus_MaxVal                                                    0xffffffff
#define cAf6_pwe_hold_status_PwHoldStatus_MinVal                                                           0x0
#define cAf6_pwe_hold_status_PwHoldStatus_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Parity Register Control
Reg Addr   : 0x000F000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register using for Force Parity

------------------------------------------------------------------------------*/
#define cAf6Reg_pwe_Parity_control_Base                                                              0x000F000
#define cAf6Reg_pwe_Parity_control                                                                   0x000F000
#define cAf6Reg_pwe_Parity_control_WidthVal                                                                 32
#define cAf6Reg_pwe_Parity_control_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: PwForceErr
BitField Type: RW
BitField Desc: Force parity error enable for "Pseudowire Transmit Ethernet
Header Mode Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pwe_Parity_control_PwForceErr_Bit_Start                                                         0
#define cAf6_pwe_Parity_control_PwForceErr_Bit_End                                                           0
#define cAf6_pwe_Parity_control_PwForceErr_Mask                                                          cBit0
#define cAf6_pwe_Parity_control_PwForceErr_Shift                                                             0
#define cAf6_pwe_Parity_control_PwForceErr_MaxVal                                                          0x1
#define cAf6_pwe_Parity_control_PwForceErr_MinVal                                                          0x0
#define cAf6_pwe_Parity_control_PwForceErr_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Parity Disable register Control
Reg Addr   : 0x000F001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register using for Disable Parity

------------------------------------------------------------------------------*/
#define cAf6Reg_pwe_Parity_Disable_control_Base                                                      0x000F001
#define cAf6Reg_pwe_Parity_Disable_control                                                           0x000F001
#define cAf6Reg_pwe_Parity_Disable_control_WidthVal                                                         32
#define cAf6Reg_pwe_Parity_Disable_control_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: PwDisChkErr
BitField Type: RW
BitField Desc: Disable parity error check for "Pseudowire Transmit Ethernet
Header Mode Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pwe_Parity_Disable_control_PwDisChkErr_Bit_Start                                                0
#define cAf6_pwe_Parity_Disable_control_PwDisChkErr_Bit_End                                                  0
#define cAf6_pwe_Parity_Disable_control_PwDisChkErr_Mask                                                 cBit0
#define cAf6_pwe_Parity_Disable_control_PwDisChkErr_Shift                                                    0
#define cAf6_pwe_Parity_Disable_control_PwDisChkErr_MaxVal                                                 0x1
#define cAf6_pwe_Parity_Disable_control_PwDisChkErr_MinVal                                                 0x0
#define cAf6_pwe_Parity_Disable_control_PwDisChkErr_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Parity sticky error
Reg Addr   : 0x000F002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register using for checking sticky error

------------------------------------------------------------------------------*/
#define cAf6Reg_pwe_Parity_stk_err_Base                                                              0x000F002
#define cAf6Reg_pwe_Parity_stk_err                                                                   0x000F002
#define cAf6Reg_pwe_Parity_stk_err_WidthVal                                                                 32
#define cAf6Reg_pwe_Parity_stk_err_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: PwStkErr
BitField Type: RW
BitField Desc: parity error for "Pseudowire Transmit Ethernet Header Mode
Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pwe_Parity_stk_err_PwStkErr_Bit_Start                                                           0
#define cAf6_pwe_Parity_stk_err_PwStkErr_Bit_End                                                             0
#define cAf6_pwe_Parity_stk_err_PwStkErr_Mask                                                            cBit0
#define cAf6_pwe_Parity_stk_err_PwStkErr_Shift                                                               0
#define cAf6_pwe_Parity_stk_err_PwStkErr_MaxVal                                                            0x1
#define cAf6_pwe_Parity_stk_err_PwStkErr_MinVal                                                            0x0
#define cAf6_pwe_Parity_stk_err_PwStkErr_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PTCH insertion
Reg Addr   : 0x0000D000 - 0x00D003
Reg Formula: 0x0000D000 +  Source_ID
    Where  : 
           + $Source_ID(0-3): Source service ID corresponding to CEM/iMS/xeaui0/xeaui1
Reg Desc   : 
This register is used to configuration PTCH

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_Ptch_Base                                                                     0x0000D000
#define cAf6Reg_TxEth_Ptch(SourceID)                                                 (0x0000D000UL+(SourceID))
#define cAf6Reg_TxEth_Ptch_WidthVal                                                                         32
#define cAf6Reg_TxEth_Ptch_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: TxEthPtch
BitField Type: RW
BitField Desc: this is configuration for PTCH insertion
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_TxEth_Ptch_TxEthPtch_Bit_Start                                                                  0
#define cAf6_TxEth_Ptch_TxEthPtch_Bit_End                                                                   15
#define cAf6_TxEth_Ptch_TxEthPtch_Mask                                                                cBit15_0
#define cAf6_TxEth_Ptch_TxEthPtch_Shift                                                                      0
#define cAf6_TxEth_Ptch_TxEthPtch_MaxVal                                                                0xffff
#define cAf6_TxEth_Ptch_TxEthPtch_MinVal                                                                   0x0
#define cAf6_TxEth_Ptch_TxEthPtch_RstVal                                                                   0x0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PSN Header OAM
Reg Addr   : 0x0000D100 - 0x00D105
Reg Formula: 0x0000D100 +  Location_ID
    Where  :
           + $Location_ID(0-5): Location ID corresponding to whole PSN start from DA 6 bytes, SA 6 bytes, VLAN Control 4 bytes, VLAN Circuit 4 bytes and Ethernet Type 2 bytes
Reg Desc   :
This register is used to configuration PSN header for OAM

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_PSNOAM_Base                                                                   0x0000D100
#define cAf6Reg_TxEth_PSNOAM(LocationID)                                           (0x0000D100UL+(LocationID))
#define cAf6Reg_TxEth_PSNOAM_WidthVal                                                                       32
#define cAf6Reg_TxEth_PSNOAM_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: TxEthPSNOAM
BitField Type: RW
BitField Desc: this is configuration for OAM pass through
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_PSNOAM_TxEthPSNOAM_Bit_Start                                                              0
#define cAf6_TxEth_PSNOAM_TxEthPSNOAM_Bit_End                                                               31
#define cAf6_TxEth_PSNOAM_TxEthPSNOAM_Mask                                                            cBit31_0
#define cAf6_TxEth_PSNOAM_TxEthPSNOAM_Shift                                                                  0
#define cAf6_TxEth_PSNOAM_TxEthPSNOAM_MaxVal                                                        0xffffffff
#define cAf6_TxEth_PSNOAM_TxEthPSNOAM_MinVal                                                               0x0
#define cAf6_TxEth_PSNOAM_TxEthPSNOAM_RstVal                                                               0x0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PSN Header OAM Error
Reg Addr   : 0x0000D200 - 0x00D205
Reg Formula: 0x0000D200 +  Location_ID
    Where  :
           + $Location_ID(0-5): Location ID corresponding to whole PSN start from DA 6 bytes, SA 6 bytes, VLAN Control 4 bytes, VLAN Circuit 4 bytes and Ethernet Type 2 bytes
Reg Desc   :
This register is used to configuration PSN header for OAM Error

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_PSNOAMErr_Base                                                                0x0000D200
#define cAf6Reg_TxEth_PSNOAMErr(LocationID)                                        (0x0000D200UL+(LocationID))
#define cAf6Reg_TxEth_PSNOAMErr_WidthVal                                                                    32
#define cAf6Reg_TxEth_PSNOAMErr_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: TxEthPSNOAMErr
BitField Type: RW
BitField Desc: this is configuration for OAM Error
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_PSNOAMErr_TxEthPSNOAMErr_Bit_Start                                                        0
#define cAf6_TxEth_PSNOAMErr_TxEthPSNOAMErr_Bit_End                                                         31
#define cAf6_TxEth_PSNOAMErr_TxEthPSNOAMErr_Mask                                                      cBit31_0
#define cAf6_TxEth_PSNOAMErr_TxEthPSNOAMErr_Shift                                                            0
#define cAf6_TxEth_PSNOAMErr_TxEthPSNOAMErr_MaxVal                                                  0xffffffff
#define cAf6_TxEth_PSNOAMErr_TxEthPSNOAMErr_MinVal                                                         0x0
#define cAf6_TxEth_PSNOAMErr_TxEthPSNOAMErr_RstVal                                                         0x0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PSN Header Ethernet Type
Reg Addr   : 0x0000DF00 - 0x00DF0F
Reg Formula: 0x0000DF00 +  NLP_ID
    Where  :
           + $NLP_ID(0-15): Network layer protocol ID
Reg Desc   :
This register is used to configuration Ethernet type base on NLP_ID

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_PSNChgEthType_Base                                                            0x0000DF00
#define cAf6Reg_TxEth_PSNChgEthType(NLPID)                                              (0x0000DF00UL+(NLPID))
#define cAf6Reg_TxEth_PSNChgEthType_WidthVal                                                                32
#define cAf6Reg_TxEth_PSNChgEthType_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: TxEth_PSNChgEthType
BitField Type: RW
BitField Desc: this is configuration Ethernet Type
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_TxEth_PSNChgEthType_TxEth_PSNChgEthType_Bit_Start                                               0
#define cAf6_TxEth_PSNChgEthType_TxEth_PSNChgEthType_Bit_End                                                15
#define cAf6_TxEth_PSNChgEthType_TxEth_PSNChgEthType_Mask                                             cBit15_0
#define cAf6_TxEth_PSNChgEthType_TxEth_PSNChgEthType_Shift                                                   0
#define cAf6_TxEth_PSNChgEthType_TxEth_PSNChgEthType_MaxVal                                             0xffff
#define cAf6_TxEth_PSNChgEthType_TxEth_PSNChgEthType_MinVal                                                0x0
#define cAf6_TxEth_PSNChgEthType_TxEth_PSNChgEthType_RstVal                                                0x0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire VLAN Insertion
Reg Addr   : 0x0000E000 - 0x00E00F
Reg Formula: 0x0000E000 +  CH_ID
    Where  :
           + $CH_ID(0-255): Channel ID of exaui1
Reg Desc   :
This register is used to configuration VLAN insertion base on CH_ID

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_VlanIns_Base                                                                  0x0000E000
#define cAf6Reg_TxEth_VlanIns(CHID)                                                      (0x0000E000UL+(CHID))
#define cAf6Reg_TxEth_VlanIns_WidthVal                                                                      32
#define cAf6Reg_TxEth_VlanIns_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: TxEth_VlanIns
BitField Type: RW
BitField Desc: this is configuration VLAN insertion
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_VlanIns_TxEth_VlanIns_Bit_Start                                                           0
#define cAf6_TxEth_VlanIns_TxEth_VlanIns_Bit_End                                                            31
#define cAf6_TxEth_VlanIns_TxEth_VlanIns_Mask                                                         cBit31_0
#define cAf6_TxEth_VlanIns_TxEth_VlanIns_Shift                                                               0
#define cAf6_TxEth_VlanIns_TxEth_VlanIns_MaxVal                                                     0xffffffff
#define cAf6_TxEth_VlanIns_TxEth_VlanIns_MinVal                                                            0x0
#define cAf6_TxEth_VlanIns_TxEth_VlanIns_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PSN PTCH Enable Control
Reg Addr   : 0x0000D300
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configuration PSN PTCH enable

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_PSNPtchEn_Base                                                                0x0000D300
#define cAf6Reg_TxEth_PSNPtchEn                                                                     0x0000D300
#define cAf6Reg_TxEth_PSNPtchEn_WidthVal                                                                    32
#define cAf6Reg_TxEth_PSNPtchEn_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: TxEthPtch1Byte
BitField Type: RW
BitField Desc: this is configuration to enable PTCH insertion 1 byte (MSB) 1:
PTCH 1 byte 0: PTCH 2 byte
BitField Bits: [1]
--------------------------------------*/
#define cAf6_TxEth_PSNPtchEn_TxEthPtch1Byte_Bit_Start                                                        1
#define cAf6_TxEth_PSNPtchEn_TxEthPtch1Byte_Bit_End                                                          1
#define cAf6_TxEth_PSNPtchEn_TxEthPtch1Byte_Mask                                                         cBit1
#define cAf6_TxEth_PSNPtchEn_TxEthPtch1Byte_Shift                                                            1
#define cAf6_TxEth_PSNPtchEn_TxEthPtch1Byte_MaxVal                                                         0x1
#define cAf6_TxEth_PSNPtchEn_TxEthPtch1Byte_MinVal                                                         0x0
#define cAf6_TxEth_PSNPtchEn_TxEthPtch1Byte_RstVal                                                         0x0

/*--------------------------------------
BitField Name: TxEthPtchEn
BitField Type: RW
BitField Desc: this is configuration to enable PTCH insertion 1: Enable PTCH 0:
Disable PTCH
BitField Bits: [0]
--------------------------------------*/
#define cAf6_TxEth_PSNPtchEn_TxEthPtchEn_Bit_Start                                                           0
#define cAf6_TxEth_PSNPtchEn_TxEthPtchEn_Bit_End                                                             0
#define cAf6_TxEth_PSNPtchEn_TxEthPtchEn_Mask                                                            cBit0
#define cAf6_TxEth_PSNPtchEn_TxEthPtchEn_Shift                                                               0
#define cAf6_TxEth_PSNPtchEn_TxEthPtchEn_MaxVal                                                            0x1
#define cAf6_TxEth_PSNPtchEn_TxEthPtchEn_MinVal                                                            0x0
#define cAf6_TxEth_PSNPtchEn_TxEthPtchEn_RstVal                                                            0x0

#endif /* _AF6_REG_AF6CCI0012_RD_PWE_H_ */

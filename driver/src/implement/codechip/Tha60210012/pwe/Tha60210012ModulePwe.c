/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60210012ModulePwe.c
 *
 * Created Date: Feb 29, 2016
 *
 * Description : Module PWE of 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhChannel.h"
#include "AtSdhChannel.h"
#include "AtHdlcChannel.h"
#include "AtHdlcLink.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/encap/dynamic/ThaDynamicHdlcLink.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../../Tha60210011/ram/Tha60210011InternalRam.h"
#include "../pw/activator/Tha60210012PwActivator.h"
#include "../pw/Tha60210012ModulePw.h"
#include "../eth/Tha60210012EXaui.h"
#include "../man/Tha60210012Device.h"
#include "../concate/Tha60210012ModuleConcate.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "../ram/Tha60210012ModuleRam.h"
#include "../cla/Tha60210012ModuleCla.h"
#include "Tha60210012ModulePweInternal.h"
#include "Tha60210012ModulePlaReg.h"
#include "Tha60210012ModulePlaDebugReg.h"
#include "Tha60210012ModulePweReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumHspwPerGroupOf16Hspw 	16
#define cNumAdditionalByte 			16

#define cTha60210012ModulePweEXauiPortCounterBase 0x2000
#define cTha60210012ModulePweXfiPortCounterBase   0x4000

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210012ModulePwe)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012PlaDbgBitFieldInfo
    {
    const char *name;
    uint32 mask;
    }tTha60210012PlaDbgBitFieldInfo;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012ModulePweMethods m_methods;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tThaModulePweMethods         m_ThaModulePweOverride;
static tThaModulePweV2Methods       m_ThaModulePweV2Override;
static tTha60210051ModulePweMethods m_Tha60210051ModulePweOverride;
static tTha60210011ModulePweMethods m_Tha60210011ModulePweOverride;

/* Save super implementations */
static const tAtObjectMethods    			*m_AtObjectMethods             = NULL;
static const tAtModuleMethods    			*m_AtModuleMethods             = NULL;
static const tThaModulePweMethods         	*m_ThaModulePweMethods         = NULL;
static const tThaModulePweV2Methods       	*m_ThaModulePweV2Methods       = NULL;
static const tTha60210011ModulePweMethods 	*m_Tha60210011ModulePweMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Setup(AtModule self)
    {
    eAtRet ret;

    /* Super job */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    ThaBitMaskInit(mThis(self)->blockUsedMask);

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->blockUsedMask);
    mThis(self)->blockUsedMask = NULL;
    m_AtObjectMethods->Delete(self);
    }

static uint32 LoPayloadCtrlOffset(Tha60210011ModulePwe self, uint32 loOc48Slice, uint32 loOc24Slice, uint32 loPwid)
    {
    AtUnused(self);
    return (loOc48Slice * 32768UL) + (loOc24Slice * 8192UL) + loPwid;
    }

static uint32 LoPayloadCtrl(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 0x1000;
    }

static uint32 HoPayloadCtrlOffset(Tha60210011ModulePwe self, uint32 hoOc96Slice, uint32 hoOc48Slice, uint32 hoPwid)
    {
    AtUnused(self);
    AtUnused(hoOc96Slice);
    return (hoOc48Slice * 256UL) + hoPwid;
    }

static uint32 HoPayloadCtrl(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 0x10080;
    }

static uint32 LoAddRemoveProtocolOffset(Tha60210011ModulePwe self, uint32 loOc48Slice, uint32 loOc24Slice, uint32 loPwid)
    {
    AtUnused(self);
    return (loOc48Slice * 32768UL) + (loOc24Slice * 8192UL) + loPwid;
    }

static uint32 LoAddRemoveProtocolReg(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 0x1800;
    }

static uint32 HoAddRemoveProtocolOffset(Tha60210011ModulePwe self, uint32 hoOc96Slice, uint32 hoOc48Slice, uint32 hoPwid)
    {
    AtUnused(self);
    AtUnused(hoOc96Slice);
    return (hoOc48Slice * 256Ul) + hoPwid;
    }

static uint32 HoAddRemoveProtocolReg(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 0x100C0;
    }

static uint32 PwIdCommonControlAddress(ThaModulePwe self, uint32 pwId)
    {
    AtUnused(self);
    return cAf6Reg_pla_comm_pw_ctrl_Base + pwId + Tha60210011ModulePlaBaseAddress();
    }

static uint32 PwCommonControlAddress(ThaModulePwe self, AtPw pw)
    {
    return PwIdCommonControlAddress(self, AtChannelIdGet((AtChannel)pw));
    }

static eAtRet PwCwAutoTxLBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = PwCommonControlAddress(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    /* Bit field in HO and LO are the same */
    mRegFieldSet(regVal, cAf6_pla_comm_pw_ctrl_PlaPwLbitDisCtrl_, enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxLBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = PwCommonControlAddress(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_comm_pw_ctrl_PlaPwLbitDisCtrl_Mask) ? cAtFalse : cAtTrue;
    }

static eAtRet PwCwAutoTxRBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regVal;
    uint32 regAddr = PwCommonControlAddress(self, pw);
    uint8 hwEnable = (enable) ? 0 : 1;

    regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_comm_pw_ctrl_PlaPwRbitDisCtrl_, hwEnable);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxRBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regVal;
    uint32 regAddr = PwCommonControlAddress(self, pw);

    regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (mRegField(regVal, cAf6_pla_comm_pw_ctrl_PlaPwRbitDisCtrl_) ? cAtFalse : cAtTrue);
    }

static eAtRet PwCwAutoTxMBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = PwCommonControlAddress(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_comm_pw_ctrl_PlaPwMbitDisCtrl_, enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxMBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = PwCommonControlAddress(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_comm_pw_ctrl_PlaPwMbitDisCtrl_Mask) ? cAtFalse : cAtTrue;
    }

static eAtRet PwCwAutoTxNPBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    return PwCwAutoTxMBitEnable(self, pw, enable);
    }

static eBool PwCwAutoTxNPBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    return PwCwAutoTxMBitIsEnabled(self, pw);
    }

static eAtRet PwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    if (ThaPwTypeIsCesCep(pw))
        {
        uint32 regAddr, regVal;

        regAddr = PwCommonControlAddress(self, pw);
        regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
        mRegFieldSet(regVal, cAf6_pla_comm_pw_ctrl_PlaPwEnCtrl_, enable ? 1 : 0);
        mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);
        }

    return cAtOk;
    }

static eBool CircuitRxTrafficIsEnabled(AtPw pw)
    {
    return AtChannelRxIsEnabled(AtPwBoundCircuitGet(pw));
    }

static eBool CircuitTxTrafficIsEnabled(AtPw pw)
    {
    return AtChannelTxIsEnabled(AtPwBoundCircuitGet(pw));
    }

static eBool PwIsEnabled(ThaModulePwe self, AtPw pw)
    {
    if (ThaPwTypeIsCesCep(pw))
        {
        uint32 regAddr, regVal;

        regAddr = PwCommonControlAddress(self, pw);
        regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);

        return (regVal & cAf6_pla_comm_pw_ctrl_PlaPwEnCtrl_Mask) ? cAtTrue : cAtFalse;
        }

	return (CircuitRxTrafficIsEnabled(pw) && CircuitTxTrafficIsEnabled(pw));
    }

static eAtRet PwTxLBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = PwCommonControlAddress(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cAf6_pla_comm_pw_ctrl_PlaPwLbitCPUCtrl_, mBoolToBin(force));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxLBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwCommonControlAddress(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_comm_pw_ctrl_PlaPwLbitCPUCtrl_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwTxRBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regVal;
    uint32 regAddr = PwCommonControlAddress(self, pw);

    regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_comm_pw_ctrl_PlaPwRbitCPUCtrl_, mBoolToBin(force));

    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxRBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regVal;
    uint32 regAddr = PwCommonControlAddress(self, pw);

    regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);

    return (mRegField(regVal, cAf6_pla_comm_pw_ctrl_PlaPwRbitCPUCtrl_) ? cAtTrue : cAtFalse);
    }

static eAtRet PwTxMBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = PwCommonControlAddress(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    uint8 mBitVal = (force == cAtTrue) ? 0x2 : 0x0;

    mRegFieldSet(regVal, cAf6_pla_comm_pw_ctrl_PlaPwMbitCPUCtrl_, mBitVal);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxMBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwCommonControlAddress(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cAf6_pla_comm_pw_ctrl_PlaPwMbitCPUCtrl_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwSupressEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr = PwCommonControlAddress(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    /* Bit field in HO and LO are the same */
    mRegFieldSet(regVal, cAf6_pla_comm_pw_ctrl_PlaLoPwSuprCtrl_, mBoolToBin(enable));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwSupressIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = PwCommonControlAddress(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    /* Bit field in HO and LO are the same */
    return mBinToBool(mRegField(regVal, cAf6_pla_comm_pw_ctrl_PlaLoPwSuprCtrl_));
    }

static void AllCircuitPwDisable(ThaModulePwe self)
    {
    uint32 pwId = 0;
    uint32 address;
    uint32 numPw = AtModulePwMaxPwsGet((AtModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw));
    for (pwId = 0; pwId < numPw; pwId++)
        {
        address = PwIdCommonControlAddress(self, pwId);
        mModuleHwWrite(self, address, 0);
        }
    }

static eAtRet DefaultSet(ThaModulePwe self)
    {
    AllCircuitPwDisable(self);
    return cAtOk;
    }

static uint32 PsnControlReg(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_out_psnpro_ctrl_Base;
    }

static uint32 PsnChannelIdMask(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Mask;
    }

static uint32 PsnChannelIdShift(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_Shift;
    }

static uint32 PsnPwHwId(Tha60210011ModulePwe self, AtPw adapter)
    {
    AtUnused(self);
    return Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)adapter);
    }

static uint32 PsnBufferCtrlReg(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return cAf6Reg_pla_out_psnbuf_ctrl_Base;
    }

static eAtRet PwTypeSet(ThaModulePwe self, AtPw pw, eAtPwType pwType)
    {
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return Tha60210011PlaLoLineCircuitPwTypeSet(self, pw, pwType);
    return cAtOk;
    }

static uint32 PlaLookupControlReg(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        return cAf6Reg_pla_lo_lk_ctrl_Base;

    return cAf6Reg_pla_ho_lk_ctrl_Base;
    }

static Tha60210011ModulePw ModulePw(ThaModulePwe self)
    {
    return (Tha60210011ModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    }

static uint32 LoPwControlOffset(ThaModulePwe self, uint8 loOc48Slice, uint8 loOc24Slice, uint32 pwIdInSlice)
    {
    uint32 oc24Offset;
    Tha60210011ModulePw pwModule = ModulePw(self);

    if (Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule) == Tha60210011ModulePwOriginalNumPwsPerSts24Slice(pwModule))
        oc24Offset = loOc24Slice * 1024UL;
    else
        oc24Offset = loOc24Slice * 2048UL;

    return (loOc48Slice * 32768UL) + oc24Offset + pwIdInSlice + Tha60210011ModulePlaBaseAddress();
    }

static uint32 HoPwControlOffset(uint8 hiOc96Slice, uint8 hiOc48Slice, uint32 pwIdInSlice)
    {
    AtUnused(hiOc96Slice);
    return (hiOc48Slice * 64UL) + pwIdInSlice + Tha60210011ModulePlaBaseAddress();
    }

static uint32 PlaLookupControlOffset(ThaModulePwe self, AtPw pw)
    {
    uint8 slice;
    uint8 hiOc96Slice, hiOc48Slice;

    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, NULL) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        {
        /* Common converting, slice in lo-line will be 0..11 */
        uint8 loOc48Slice = slice / 2;
        uint8 loOc24Slice = slice % 2;

        return LoPwControlOffset(self, loOc48Slice, loOc24Slice, AtChannelHwIdGet((AtChannel)pw));
        }

    /* Common converting, slice in hi-line will be 0..7 */
    hiOc96Slice = slice / 2;
    hiOc48Slice = slice % 2;

    return HoPwControlOffset(hiOc96Slice, hiOc48Slice, AtChannelHwIdGet((AtChannel)pw));
    }

static uint32 PlaPwLookupControl(ThaModulePwe self, AtPw pw)
    {
    return PlaLookupControlReg(self, pw) + PlaLookupControlOffset(self, pw);
    }

static uint32 PwLookupIdMask(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_lo_lk_ctrl_PlaLoLkPWIDCtrl_Mask;
    }

static uint32 PwLookupIdShift(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_lo_lk_ctrl_PlaLoLkPWIDCtrl_Shift;
    }

static void PlaPwDefaultHwConfigure(Tha60210011ModulePwe self, ThaPwAdapter pwAdapter)
    {
    uint32 regAddr, regVal;
    uint32 pwIdLookupMask, pwIdLookupShift;

    if ((self == NULL) || (!ThaPwTypeIsCesCep((AtPw)pwAdapter)))
        return;

    regAddr = PlaPwLookupControl((ThaModulePwe)self, (AtPw)pwAdapter);
    regVal  = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);

    /* Bit field in HO and LO are the same */
    pwIdLookupMask = mMethodsGet(mThis(self))->PwLookupIdMask(mThis(self));
    pwIdLookupShift = mMethodsGet(mThis(self))->PwLookupIdShift(mThis(self));

    mRegFieldSet(regVal, pwIdLookup, AtChannelIdGet((AtChannel)pwAdapter));
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);
    }

static uint32 PlaPwProtectionControl(ThaModulePweV2 self, AtPw pwAdapter)
    {
    AtUnused(self);
    return cAf6Reg_pla_prot_flow_ctrl_Base + Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pwAdapter) + Tha60210011ModulePlaBaseAddress();
    }

static uint32 UpsrGroupMask(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_prot_flow_ctrl_PlaOutUpsrGrpCtr_Mask;
    }

static uint32 UpsrGroupShift(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_prot_flow_ctrl_PlaOutUpsrGrpCtr_Shift;
    }

static eAtRet ApsGroupPwAdd(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 regAddr = PlaPwProtectionControl(self, pwAdapter);
    uint32 groupMask  = mMethodsGet(mThis(self))->UpsrGroupMask(mThis(self));
    uint32 groupShift = mMethodsGet(mThis(self))->UpsrGroupShift(mThis(self));
    uint32 regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cAf6_pla_prot_flow_ctrl_PlaFlowUPSRUsedCtrl_, 1);
    mRegFieldSet(regVal, group, AtPwGroupIdGet(pwGroup));

    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet ApsGroupPwRemove(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 regVal;
    uint32 regAddr = PlaPwProtectionControl(self, pwAdapter);
    AtUnused(pwGroup);

    regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_prot_flow_ctrl_PlaFlowUPSRUsedCtrl_, 0);

    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint32 HspwGroupMask(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_prot_flow_ctrl_PlaOutHspwGrpCtr_Mask;
    }

static uint32 HspwGroupShift(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return cAf6_pla_prot_flow_ctrl_PlaOutHspwGrpCtr_Shift;
    }

static eAtRet HsGroupPwAdd(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 regAddr    = PlaPwProtectionControl(self, pwAdapter);
    uint32 regVal     = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);
    uint32 groupMask  = mMethodsGet(mThis(self))->HspwGroupMask(mThis(self));
    uint32 groupShift = mMethodsGet(mThis(self))->HspwGroupShift(mThis(self));

    mRegFieldSet(regVal, cAf6_pla_prot_flow_ctrl_PlaFlowHSPWUsedCtrl_, 1);
    mRegFieldSet(regVal, group, AtPwGroupIdGet(pwGroup));

    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet HsGroupPwRemove(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    uint32 regVal;
    uint32 regAddr = PlaPwProtectionControl(self, pwAdapter);
    AtUnused(pwGroup);

    regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_prot_flow_ctrl_PlaFlowHSPWUsedCtrl_, 0);

    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtRet HsGroupEnable(ThaModulePweV2 self, AtPwGroup pwGroup, eBool enable)
    {
    AtUnused(self);
    AtUnused(pwGroup);
    AtUnused(enable);

    return cAtOk;
    }

static eBool HsGroupIsEnabled(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    AtUnused(self);
    AtUnused(pwGroup);
    return cAtTrue;
    }

static uint32 *PlaHoldRegistersGet(Tha60210051ModulePwe self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x421000, 0x421001, 0x421002};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static uint32 FlatPwOffset(AtPw pw)
    {
    return AtChannelIdGet((AtChannel)pw);
    }

static eAtRet FlowForPwSet(ThaPwAdapter pw)
    {
    uint32 flowId, pwId;
    uint32 address, regVal;

    pwId = AtChannelIdGet((AtChannel)pw);
    flowId = Tha60210012PwAdapterHwFlowIdGet(pw);

    address = cAf6Reg_pla_lk_flow_ctrl_Base + pwId +  Tha60210011ModulePlaBaseAddress();
    regVal = mChannelHwRead(pw, address, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_, flowId);

    mChannelHwWrite(pw, address, regVal, cThaModulePwe);
    return cAtOk;
    }

static void OneRegShow(AtPw pw, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay((AtChannel)pw, address, cThaModulePwe);
    }

static void PwPlaRegsShow(Tha60210011ModulePwe self, AtPw pw)
    {
    AtPw pwAdapter = (AtPw)ThaPwAdapterGet(pw);
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    ThaModulePwe modulePwe = (ThaModulePwe)self;

    if (circuit == NULL)
        {
        AtPrintc(cSevInfo, "   - Pw has not bound circuit\r\n");
        return;
        }

    Tha60210011ModulePwCircuitInfoDisplay(pwAdapter);
    if (Tha60210011PwCircuitBelongsToLoLine(pw))
        {
        OneRegShow(pw, "Payload Assembler Low-Order Payload Control",
                   LoPayloadCtrl(self) + Tha60210011ModulePlaPldControlOffset(modulePwe, pwAdapter));
        OneRegShow(pw, "Payload Assembler Low-Order Add/Remove Pseudowire Protocol Control",
                   LoAddRemoveProtocolReg(self) + Tha60210011ModulePlaAddRemoveProtocolOffset(modulePwe, pw));
        OneRegShow(pw, "Payload Assembler Low-Order Lookup Control", PlaPwLookupControl(modulePwe, pwAdapter));
        }
    else
        {
        OneRegShow(pw, "Payload Assembler High-Order Payload Control",
                   HoPayloadCtrl(self) + Tha60210011ModulePlaPldControlOffset(modulePwe, pwAdapter));
        OneRegShow(pw, "Payload Assembler High-Order Add/Remove Pseudowire Protocol Control",
                   HoAddRemoveProtocolReg(self) + Tha60210011ModulePlaAddRemoveProtocolOffset(modulePwe, pw));
        OneRegShow(pw, "Payload Assembler high-Order Lookup Control", PlaPwLookupControl(modulePwe, pwAdapter));
        }

    OneRegShow(pw, "Payload Assembler Common Pseudowire Control", PwCommonControlAddress(modulePwe, pwAdapter));
    OneRegShow(pw, "Payload Assembler Protection Pseudowire Control", PlaPwProtectionControl((ThaModulePweV2)modulePwe, pwAdapter));
    OneRegShow(pw, "Payload Assembler Block Buffer Control", cAf6Reg_pla_blk_buf_ctrl_Base + FlatPwOffset(pwAdapter) + Tha60210011ModulePlaBaseAddress());
    OneRegShow(pw, "Payload Assembler Lookup Flow Control", cAf6Reg_pla_lk_flow_ctrl_Base + FlatPwOffset(pwAdapter) + Tha60210011ModulePlaBaseAddress());
    }

static eBool NewPsnIsSupported(Tha60210051ModulePwe self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 ByteMask(uint8 byte_i)
    {
    return (cBit7_0 << (byte_i * 8));
    }

static uint8 ByteShift(uint8 byte_i)
    {
    return (uint8)(byte_i * 8);
    }

static uint32 PutByteToDword(uint8 value, uint8 byte_i)
    {
    return (((uint32)value) << ByteShift(byte_i)) & ByteMask(byte_i);
    }

static AtIpCore Core(ThaModulePwe self)
    {
    return AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);
    }

static eAtRet WaitForHwDone(ThaModulePwe self)
    {
    const uint32 cHeaderProcessTimeOutInMs = 100;
    tAtOsalCurTime curTime, startTime;
    uint32 elapse = 0;
    uint32 regVal = 0;
    uint32 regAddr;

    regAddr = cAf6Reg_pla_out_psnpro_ctrl_Base + Tha60210011ModulePlaBaseAddress();
    AtOsalCurTimeGet(&startTime);
    while (elapse < cHeaderProcessTimeOutInMs)
        {
        regVal = mModuleHwRead(self, regAddr);

        if (AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)self)))
            return cAtOk;

        if ((regVal & cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_Mask) == 0)
            return cAtOk;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorIndrAcsTimeOut;
    }

static eAtRet ReadRequest(ThaModulePwe self, AtEthFlow flow, uint8 page, uint8 headerLenInByte)
    {
    const uint8 cReadCommand = 1;
    const uint8 cRequest = 1;
    uint32 regVal = 0;
    uint32 regAddr = cAf6Reg_pla_out_psnpro_ctrl_Base + Tha60210011ModulePlaBaseAddress();
    eAtRet ret;

    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_, AtChannelHwIdGet((AtChannel)flow));
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_, page);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_, cReadCommand);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_, cRequest);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_, headerLenInByte);

    mModuleHwWrite(self, regAddr, regVal);

    ret = WaitForHwDone(self);
    if (ret != cAtOk)
        AtChannelLog((AtChannel)flow, cAtLogLevelCritical, __FILE__, __LINE__, "Read request fail, header length: %u bytes, page %u (0x%08X = 0x%08X)\r\n",
                     headerLenInByte, page, regAddr, mModuleHwRead(self, regAddr));
    return ret;
    }

static eAtRet ReadHeaderFromBuffer(ThaModulePwe self, uint8 *buffer, uint8 headerLenInByte)
    {
    const uint8 cNumDwordInSegment = 4;
    uint32 num16BytesSegment, segment_i;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 byte_i, byteInDw_i, numAdditionalByte, numIgnoreDword;
    int8 dword_i;
    uint32 regAddr = cAf6Reg_pla_out_psnbuf_ctrl_Base + Tha60210011ModulePlaBaseAddress();
    uint32 numRequestedByte;

    numAdditionalByte = cNumAdditionalByte;
    numRequestedByte  = (uint32)(headerLenInByte + numAdditionalByte);
    numIgnoreDword    = (uint8)(numAdditionalByte / 4);
    num16BytesSegment = numRequestedByte / 16;
    byte_i = 0;

    for (segment_i = 0; segment_i < num16BytesSegment; segment_i++)
        {
        mModuleHwLongRead(self, regAddr + segment_i, longRegVal, 4, Core(self));
        for (dword_i = 3; dword_i >= 0; dword_i--)
            {
            /* Some first dwords of first segment need to be ignored */
            if (segment_i == 0)
                {
                if (dword_i >= (int8)(cNumDwordInSegment - numIgnoreDword))
                    continue;
                }

            buffer[byte_i++] = (uint8)(longRegVal[dword_i] >> 24);
            buffer[byte_i++] = (uint8)(longRegVal[dword_i] >> 16);
            buffer[byte_i++] = (uint8)(longRegVal[dword_i] >> 8);
            buffer[byte_i++] = (uint8)(longRegVal[dword_i]);
            }
        }

    if (byte_i >= headerLenInByte)
        return cAtOk;

    /* Last segment */
    mModuleHwLongRead(self, regAddr + segment_i, longRegVal, 4, Core(self));
    byteInDw_i = 3;
    dword_i = 3;
    while (byte_i < headerLenInByte)
        {
        buffer[byte_i++] = (uint8)(longRegVal[dword_i] >> ByteShift(byteInDw_i));
        if (byteInDw_i == 0)
            {
            byteInDw_i = 3;
            dword_i--;
            }
        else
            byteInDw_i = (uint8)(byteInDw_i - 1);
        }

    return cAtOk;
    }

static eAtRet WriteHeaderToBuffer(ThaModulePwe self, AtEthFlow flow, uint8 *buffer, uint8 headerLenInByte)
    {
    uint32 num16BytesSegment, segment_i, writtenNumDword, writtenNumByte;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 byteInDword_i;
    int8 dword_i;
    uint32 regAddr = cAf6Reg_pla_out_psnbuf_ctrl_Base + Tha60210011ModulePlaBaseAddress();
    uint32 numByteToWrite = (uint32)(headerLenInByte + cNumAdditionalByte);

    if (numByteToWrite > cMaxSupportedHeaderLengthInByte)
        {
        mChannelLog(flow, cAtLogLevelCritical, "PSN header is too long");
        return cAtErrorModeNotSupport;
        }

    num16BytesSegment = numByteToWrite / 16;
    writtenNumDword = 0;
    for (segment_i = 0; segment_i < num16BytesSegment; segment_i++)
        {
        if (segment_i == 0)
            {
            AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
            mModuleHwLongWrite(self, regAddr + segment_i, longRegVal, 4, Core(self));
            continue;
            }

        for (dword_i = 3; dword_i >= 0; dword_i--)
            {
            longRegVal[dword_i] = ThaPktUtilPutDataToRegister(buffer, (uint8)writtenNumDword);
            writtenNumDword = writtenNumDword + 1;
            }

        mModuleHwLongWrite(self, regAddr + segment_i, longRegVal, 4, Core(self));
        }

    /* Maybe there are still some last bytes if headerLenInByte is not multiple of 16 */
    writtenNumByte = writtenNumDword * 4;
    if (writtenNumByte >= headerLenInByte)
        return cAtOk;

    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
    byteInDword_i = 3;
    dword_i = 4;

    while ((writtenNumByte < headerLenInByte) && (dword_i > 0))
        {
        longRegVal[dword_i - 1] |= PutByteToDword(buffer[writtenNumByte], byteInDword_i);

        writtenNumByte++;
        if (byteInDword_i == 0)
            {
            byteInDword_i = 3;
            dword_i--;
            }
        else
            byteInDword_i = (uint8)(byteInDword_i - 1);
        }

    mModuleHwLongWrite(self, regAddr + segment_i, longRegVal, 4, Core(self));

    return cAtOk;
    }

static eAtRet WriteRequest(ThaModulePwe self, AtEthFlow flow, uint8 page, uint8 headerLenInByte)
    {
    const uint8 cWriteCommand = 0;
    const uint8 cRequest = 1;
    uint32 regVal = 0;
    uint32 regAddr = cAf6Reg_pla_out_psnpro_ctrl_Base + Tha60210011ModulePlaBaseAddress();
    eAtRet ret;

    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProFlowCtr_, AtChannelHwIdGet((AtChannel)flow));
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProPageCtr_, page);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProLenCtr_, headerLenInByte);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProRnWCtr_, cWriteCommand);
    mRegFieldSet(regVal, cAf6_pla_out_psnpro_ctrl_PlaOutPsnProReqCtr_, cRequest);

    mModuleHwWrite(self, regAddr, regVal);

    ret = WaitForHwDone(self);
    if (ret != cAtOk)
        AtChannelLog((AtChannel)flow, cAtLogLevelCritical, __FILE__, __LINE__, "Write request fail, header length: %u bytes, page %u (0x%08X = 0x%08X)\r\n",
                     headerLenInByte, page, regAddr, mModuleHwRead(self, regAddr));

    return ret;
    }

static uint8 RealHeaderLength(uint8 hwValue)
    {
    if (hwValue < cNumAdditionalByte)
        return 0;

    return (uint8)(hwValue - cNumAdditionalByte);
    }

static uint8 HeaderLengthWriteToHw(uint8 hdrLenInByte)
    {
    return (uint8)(hdrLenInByte + cNumAdditionalByte);
    }

static uint32 PlaDebugStickyRead(AtModule self, uint32 regAddr)
    {
    return mModuleHwRead(self, regAddr + Tha60210011ModulePlaBaseAddress());
    }

static void PlaDebugStickyInforShow(AtModule self, uint32 regVal, uint32 mask, const char *infor)
    {
    eBool stickyIsRaised;
    AtUnused(self);
    stickyIsRaised = mBinToBool(regVal & mask);

    AtPrintc(cSevNormal, "   %-30s: ", infor);
    AtPrintc((stickyIsRaised) ? cSevCritical : cSevInfo, "%s\r\n", (stickyIsRaised) ? "RAISE" : "CLEAR");
    }

static void PlaDebugStickyCommon(AtModule self, uint32 address, const tTha60210012PlaDbgBitFieldInfo* bitInfo, uint8 numOfStk)
    {
    uint32 regVal, i;

    regVal = PlaDebugStickyRead(self, address);
    for (i = 0; i < numOfStk; i++)
        {
        PlaDebugStickyInforShow(self, regVal, bitInfo[i].mask, bitInfo[i].name);
        }

    /* Clear sticky */
    mModuleHwWrite(self, Tha60210011ModulePlaBaseAddress() + address, regVal);
    }

static void DebugPlaLoSliceClk155Stk(AtModule self, uint8 oc48, uint8 oc24)
    {
    const tTha60210012PlaDbgBitFieldInfo m_PlaLoClk155Stk[] = {
                                                   {"OC24 convert clock Error",   cBit8},
                                                   {"OC24 input of expected pwid",cBit7},
                                                   {"OC24 input data from demap", cBit6},
                                                   {"OC24 input first-timeslot",  cBit5},
                                                   {"OC24 input AIS",             cBit4},
                                                   {"OC24 input RDI",             cBit3},
                                                   {"OC24 input CEP Pos Pointer", cBit2},
                                                   {"OC24 input CEP Neg Pointer", cBit1},
                                                   {"OC24 input CEP J1",          cBit0}
    };
    uint32 address = cAf6Reg_pla_lo_clk155_stk(oc48, oc24);

    AtPrintc(cSevDebug, "\r\n+ Payload Assembler Low-Order clk155 Sticky (LoOc48Slice:%d, LoOc24Slice:%d)\r\n", oc48, oc24);
    PlaDebugStickyCommon(self, address, m_PlaLoClk155Stk, mCount(m_PlaLoClk155Stk));
    }

static void DebugPlaLoClk155Stk(AtModule self)
    {
    uint8 slice48;

    for (slice48 = 0; slice48 < mMethodsGet(mThis(self))->NumSlice48(mThis(self)); slice48++)
        {
        DebugPlaLoSliceClk155Stk(self, slice48, 0);
        DebugPlaLoSliceClk155Stk(self, slice48, 1);
        }
    }

static void DebugPlaHoSliceClk311Stk(AtModule self, uint8 oc48)
    {
    const tTha60210012PlaDbgBitFieldInfo m_PlaHoClk311Stk[] = {
                                                   {"OC48 convert clock Error",    cBit8},
                                                   {"OC48 input of expected pwid", cBit7},
                                                   {"OC4824 input data from demap",cBit6},
                                                   {"OC24 input AIS",              cBit4},
                                                   {"OC48 input CEP Pos Pointer",  cBit2},
                                                   {"OC48 input CEP Neg Pointer",  cBit1},
                                                   {"OC48 input CEP J1",           cBit0}
    };
    uint32 address = cAf6Reg_pla_ho_clk311_stk(oc48);

    AtPrintc(cSevDebug, "\r\n+ Payload Assembler High-Order clk311 Sticky (HoOc48Slice:%d) \r\n", oc48);
    PlaDebugStickyCommon(self, address, m_PlaHoClk311Stk, mCount(m_PlaHoClk311Stk));
    }

static void DebugPlaHoClk311Stk(AtModule self)
    {
    uint8 slice48;

    for (slice48 = 0; slice48 < mMethodsGet(mThis(self))->NumSlice48(mThis(self)); slice48++)
        DebugPlaHoSliceClk311Stk(self, slice48);

    }

static void DebugPlaIntf1Stk(AtModule self)
    {
    const tTha60210012PlaDbgBitFieldInfo m_PlaIntf1Stk[] = {
                                                {"Output PWE expected Flow",     cBit31},
                                                {"Output Request to PWE",        cBit30},
                                                {"Input PWE get data",           cBit29},
                                                {"Output Valid to PWE",          cBit28},
                                                {"Input Info from RSQ",          cBit27},
                                                {"Output Enable to RSQ",         cBit26},
                                                {"Output Info to RSQ",           cBit25},
                                                {"Input MSG Error",              cBit24},
                                                {"Input MSG Start Packet",       cBit23},
                                                {"Input MSG End Packet",         cBit22},
                                                {"Input MSG expected Lid",       cBit21},
                                                {"Input MSG Valid",              cBit20},
                                                {"Input VCG Start Packet",       cBit19},
                                                {"Input VCG End Packet",         cBit18},
                                                {"Input VCG expected Lid",       cBit17},
                                                {"Input VCG Valid",              cBit16},
                                                {"Input VCG Error",              cBit15},
                                                {"Output VCAT Pkt expected Lid", cBit13},
                                                {"Output VCAT Pkt Vld",          cBit12},
                                                {"Output VCAT Pkt Enable",       cBit10},
                                                {"Input VCAT Pkt expected Lid",  cBit9},
                                                {"Input VCAT Pkt Valid",         cBit8},
                                                {"Output VCAT TDM expected Lid", cBit5},
                                                {"Output VCAT TDM Valid",        cBit4},
                                                {"Input VCAT TDM Start Packet",  cBit3},
                                                {"Input VCAT TDM End Packet",    cBit2},
                                                {"Input VCAT TDM expected Lid",  cBit1},
                                                {"Input VCAT TDM Valid",         cBit0}
    };

    AtPrintc(cSevDebug, "\r\n+ Payload Assembler Interface#1 Sticky \r\n");
    PlaDebugStickyCommon(self, cAf6Reg_pla_interface1_stk_Base, m_PlaIntf1Stk, mCount(m_PlaIntf1Stk));
    }

static void DebugPlaIntf2Stk(AtModule self)
    {
    const tTha60210012PlaDbgBitFieldInfo m_PlaIntf2Stk[] = {
                                                {"ReadDDR VLD fifo err",        cBit27},
                                                {"ReadDDR ACK fifo err",        cBit26},
                                                {"ReadDDR Data Req fifo err",   cBit25},
                                                {"ReadDDR VCAT Req fifo err",   cBit24},
                                                {"ReadPort PW pkt VLD fifo err",cBit23},
                                                {"ReadPort pkt VLD fifo err",   cBit22},
                                                {"ReadPort pkt data fifo err",  cBit21},
                                                {"ReadPort pkt info fifo err",  cBit20},
                                                {"ReadBuf pkt VLD fifo err",    cBit19},
                                                {"ReadBuf pkt info fifo err",   cBit18},
                                                {"ReadBuf PSN ACK fifo err",    cBit17},
                                                {"ReadBuf PSN VLD fifo err",    cBit16},
                                                {"Output PW Core Start Packet", cBit14},
                                                {"Output PW Core End Packet",   cBit13},
                                                {"Output PW Core Valid",        cBit12},
                                                {"Input PSN Vld",               cBit10},
                                                {"Input PSN Ack",               cBit9},
                                                {"Output PSN Request",          cBit8},
                                                {"Input Read DDR Vld",          cBit6},
                                                {"Input Read DDR Ack",          cBit5},
                                                {"Output Read DDR Request",     cBit4},
                                                {"Input Write DDR Vld",         cBit2},
                                                {"Input Write DDR Ack",         cBit1},
                                                {"Output Write DDR Request",    cBit0}
    };

    AtPrintc(cSevDebug, "\r\n+ Payload Assembler Interface#2 Sticky \r\n");
    PlaDebugStickyCommon(self, cAf6Reg_pla_interface2_stk_Base, m_PlaIntf2Stk, mCount(m_PlaIntf2Stk));
    }

static void DebugPlaStk(AtModule self)
    {
    const tTha60210012PlaDbgBitFieldInfo m_PlaDebugStk[] = {
                                                {"CRC check error",             cBit31},
                                                {"Write Cache same err",        cBit30},
                                                {"Write Cache empty err",       cBit29},
                                                {"Write Cache err",             cBit28},
                                                {"Enque VCG Block empty err",   cBit26},
                                                {"Enque VCG Block same err",    cBit25},
                                                {"WriteDDR ACK fifo err",       cBit23},
                                                {"Enque Block Cfg err",         cBit22},
                                                {"WriteDDR VLD ready fifo err", cBit21},
                                                {"WriteDDR VLD fifo err",       cBit20},
                                                {"Mux PW Core HO err",          cBit17},
                                                {"Mux PW Core LO err",          cBit16},
                                                {"Mux PW LO OC48_2 err",        cBit13},
                                                {"Mux PW LO OC48_1 err",        cBit12},
                                                {"Mux VCG input2 MSG err",      cBit6},
                                                {"Mux VCG input2 VCAT err",     cBit5},
                                                {"Mux VCG input1 PW err",       cBit4},
                                                {"Mux VCG input2 VCG err",      cBit1},
                                                {"Mux VCG input1 PW err",       cBit0},
    };

    AtPrintc(cSevDebug, "\r\n+ Payload Assembler debug#1 Sticky \r\n");
    PlaDebugStickyCommon(self, cAf6Reg_pla_debug1_stk_Base, m_PlaDebugStk, mCount(m_PlaDebugStk));
    }

static void DebugPlaStk2(AtModule self)
    {
    const tTha60210012PlaDbgBitFieldInfo m_PlaDebugStk[] = {
                                                            {"Discard due to upsr disable", cBit23},
                                                            {"Sequence buffer error      ", cBit22},
                                                            {"eXAUI1 data buffer error   ", cBit21},
                                                            {"eXAUI0 data buffer error   ", cBit20},
                                                            {"eXAUI1 Sequence Timeout    ", cBit18},
                                                            {"eXAUI1 Sequence Bad        ", cBit17},
                                                            {"eXAUI1 Sequence Good       ", cBit16},
                                                            {"eXAUI0 Sequence Timeout    ", cBit14},
                                                            {"eXAUI0 Sequence Bad        ", cBit13},
                                                            {"eXAUI0 Sequence Good       ", cBit12},
                                                            {"PW Sequence Timeout        ", cBit10},
                                                            {"PW Sequence Bad            ", cBit9},
                                                            {"PW Sequence Good           ", cBit8},
                                                            {"eXAUI1 buffer data enable  ", cBit7},
                                                            {"Data valid from eXAUI1     ", cBit6},
                                                            {"eXAUI1 buffer packet enable", cBit5},
                                                            {"packet info valid to eXAUI0", cBit4},
                                                            {"eXAUI0 buffer data enable  ", cBit3},
                                                            {"Data valid from eXAUI0     ", cBit2},
                                                            {"eXAUI0 buffer packet enable", cBit1},
                                                            {"Packet info valid to eXAUI0", cBit0}
    };

    AtPrintc(cSevDebug, "\r\n+ Payload Assembler debug#2 Sticky \r\n");
    PlaDebugStickyCommon(self, cAf6Reg_pla_debug2_stk_Base, m_PlaDebugStk, mCount(m_PlaDebugStk));
    }

static void DebugPlaStatus(AtModule self)
    {
    const char* m_PlaDebugStatus[] = {
                                   "Current VCG free block number",
                                   "Current write cache number"
    };
    uint32 regVal, regAddr, num, reg_i = 0;
    AtPrintc(cSevDebug, "\r\n+ Payload Assembler debug status \r\n");

    regAddr = cAf6Reg_pla_debug_sta_Base;
    regVal = PlaDebugStickyRead(self, regAddr);

    num = mRegField(regVal, cAf6_pla_debug_sta_VCG_free_block_number_);
    AtPrintc(cSevNormal, "   %-30s: ", m_PlaDebugStatus[reg_i]);
    AtPrintc(cSevInfo, "%u\r\n", num);
    reg_i++;

    num = mRegField(regVal, cAf6_pla_debug_sta_write_cache_number_);
    AtPrintc(cSevNormal, "   %-30s: ", m_PlaDebugStatus[reg_i]);
    AtPrintc(cSevInfo, "%u\r\n", num);
    }

static eAtRet Debug(AtModule self)
    {
    AtPrintc(cSevDebug, "\r\n PLA DEBUG information: \r\n");
    AtPrintc(cSevNormal, "=========================================================================== \r\n");
    DebugPlaLoClk155Stk(self);
    AtPrintc(cSevNormal, "--------------------------------------------------------------------------- \r\n");
    DebugPlaHoClk311Stk(self);
    AtPrintc(cSevNormal, "--------------------------------------------------------------------------- \r\n");
    DebugPlaIntf1Stk(self);
    AtPrintc(cSevNormal, "--------------------------------------------------------------------------- \r\n");
    DebugPlaIntf2Stk(self);
    AtPrintc(cSevNormal, "--------------------------------------------------------------------------- \r\n");
    DebugPlaStk(self);
    AtPrintc(cSevNormal, "--------------------------------------------------------------------------- \r\n");
    DebugPlaStk2(self);
    AtPrintc(cSevNormal, "--------------------------------------------------------------------------- \r\n");
    DebugPlaStatus(self);
    AtPrintc(cSevNormal, "--------------------------------------------------------------------------- \r\n");
    AtPrintc(cSevNormal, "\r\n- Block Allocation low rate: \r\n");
    ThaBitMaskDebug(mThis(self)->blockUsedMask);
    AtPrintc(cSevNormal, "=========================================================================== \r\n");

    return cAtOk;
    }

static eBool ShouldReactivateWhenCacheInUnsafe(Tha60210051ModulePwe self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 PlaModuleUpsrPwGroupDefaultOffset(ThaModulePwe self, AtPwGroup group)
    {
    AtUnused(self);
    return AtPwGroupIdGet(group) + Tha60210011ModulePlaBaseAddress();
    }

static uint32 PlaModuleHsPwGroupDefaultOffset(ThaModulePwe self, AtPwGroup group)
    {
    AtUnused(self);
    return AtPwGroupIdGet(group) + Tha60210011ModulePlaBaseAddress();
    }

static eBool PwGroupingShow(Tha60210011ModulePwe self, AtPw pw)
    {
    eBool pwIsInGroup = cAtFalse;
    AtPwGroup pwGroup = AtPwApsGroupGet(pw);
    ThaModulePwe modulePwe = (ThaModulePwe)self;

    if (pwGroup)
        {
        OneRegShow(pw, "Payload Assembler Flow UPSR Control",
                   cAf6Reg_pla_flow_upsr_ctrl_Base + PlaModuleUpsrPwGroupDefaultOffset(modulePwe, pwGroup));
        pwIsInGroup = cAtTrue;
        }

    pwGroup = AtPwHsGroupGet(pw);
    if (pwGroup)
        {
        OneRegShow(pw, "Payload Assembler Flow HSPW Control",
                   cAf6Reg_pla_flow_hspw_ctrl_Base + PlaModuleHsPwGroupDefaultOffset(modulePwe, pwGroup));
        pwIsInGroup = cAtTrue;
        }

    return pwIsInGroup;
    }

static eAtRet ApsGroupEnable(ThaModulePweV2 self, AtPwGroup pwGroup, eBool enable)
    {
    uint32 regAddr = cAf6Reg_pla_flow_upsr_ctrl_Base + PlaModuleUpsrPwGroupDefaultOffset((ThaModulePwe)self, pwGroup);
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_pla_flow_upsr_ctrl_PlaFlowUpsrEnCtr_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsPlaRegister(AtModuleDeviceGet(self), address) ||
           Tha60210012IsPweRegister(AtModuleDeviceGet(self), address);
    }

static eBool ApsGroupIsEnabled(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    uint32 regAddr = cAf6Reg_pla_flow_upsr_ctrl_Base + PlaModuleUpsrPwGroupDefaultOffset((ThaModulePwe)self, pwGroup);
    uint32 regVal  = mModuleHwRead(self, regAddr);

    return mBinToBool(mRegField(regVal, cAf6_pla_flow_upsr_ctrl_PlaFlowUpsrEnCtr_));
    }

static uint32 GroupLabelSetSw2Hw(eAtPwGroupLabelSet labelSet)
    {
    return (labelSet == cAtPwGroupLabelSetPrimary) ? 0 : 1;
    }

static eAtPwGroupLabelSet GroupLabelSetHw2Sw(uint32 labelSet)
    {
    return (labelSet == 0) ? cAtPwGroupLabelSetPrimary : cAtPwGroupLabelSetBackup;
    }

static eAtRet HsGroupLabelSetSelect(ThaModulePweV2 self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet)
    {
    uint32 regAddr = cAf6Reg_pla_flow_hspw_ctrl_Base + PlaModuleHsPwGroupDefaultOffset((ThaModulePwe)self, pwGroup);
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_pla_flow_hspw_ctrl_PlaPWHSPWPSNCtr_, GroupLabelSetSw2Hw(labelSet));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HsGroupSelectedLabelSetGet(ThaModulePweV2 self, AtPwGroup pwGroup)
    {
    uint32 regAddr = cAf6Reg_pla_flow_hspw_ctrl_Base + PlaModuleHsPwGroupDefaultOffset((ThaModulePwe)self, pwGroup);
    uint32 regVal  = mModuleHwRead(self, regAddr);

    return GroupLabelSetHw2Sw(mRegField(regVal, cAf6_pla_flow_hspw_ctrl_PlaPWHSPWPSNCtr_));
    }

static eBool PtchInsertModeIsSupported(eAtPtchMode insertMode)
    {
    switch (insertMode)
        {
        case cAtPtchModeOneByte : return cAtTrue;
        case cAtPtchModeTwoBytes: return cAtTrue;
        case cAtPtchModeUnknown:
        default:
            return cAtFalse;
        }
    }

static eAtRet PwPtchInsertionModeSet(ThaModulePwe self, AtPw pw, eAtPtchMode insertMode)
    {
    uint32 regAddr, regVal;

    if (!PtchInsertModeIsSupported(insertMode))
        return cAtErrorModeNotSupport;

    regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(self, pw);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_pw_txeth_hdr_mode_TxEthPtch1Byte_, (insertMode == cAtPtchModeOneByte) ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtPtchMode PwPtchInsertionModeGet(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(self, pw);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & cAf6_pw_txeth_hdr_mode_TxEthPtch1Byte_Mask) ? cAtPtchModeOneByte : cAtPtchModeTwoBytes;
    }

static uint32 InternalMacPortCounterNewOffset(Tha60210011ModulePwe self, AtEthPort port, eBool r2c)
    {
    static const uint32 cXfiPortCounterOffset = 0x3000;
    static const uint32 cPwePortCounterOffset = 0x4000;
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)port);
    uint32 baseAddress = cXfiPortCounterOffset + ThaModuleEthMacBaseAddress(ethModule, port) - cPwePortCounterOffset - 0x1000;

    AtUnused(self);

    return ((r2c) ? 0x800 : 0x0) + baseAddress;
    }

static uint32 EXauiCounterOffset(Tha60210011ModulePwe self, AtEthPort port, eBool r2c)
    {
    uint32 baseAddress = Tha60210012EthEXauiPortBaseAddress(port) + cTha60210012ModulePweEXauiPortCounterBase - cTha60210012ModulePweXfiPortCounterBase;
    AtUnused(self);
    return (((r2c) ? 0x800 : 0x0) + baseAddress);
    }

static uint32 PortCounterOffset(Tha60210011ModulePwe self, AtEthPort port, eBool r2c)
    {
    if (AtChannelIdGet((AtChannel)port) == 0)
        {
        AtDevice device = AtChannelDeviceGet((AtChannel)port);

        /* From version 4.x, hardware changes to use new registers */
        if (Tha60210012DeviceEXauiIsReady(device))
            return InternalMacPortCounterNewOffset(self, port, r2c);

        return m_Tha60210011ModulePweMethods->PortCounterOffset(self, port, r2c);
        }

    return EXauiCounterOffset(self, port, r2c);
    }

static eAtRet PwRemovingStart(ThaModulePwe self, AtPw pw)
    {
    /* Only TDM PW need this */
    if (AtPwIsTdmPw(pw))
        return m_ThaModulePweMethods->PwRemovingStart(self, pw);

    return cAtOk;
    }

static eBool InsertEXauiIsSupported(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion >= mMethodsGet(mThis(self))->StartVersionSupportInsertEXaui(mThis(self)))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 FlowHeaderOffSet(ThaModulePwe self, AtEthFlow flow)
    {
    AtUnused(self);
    return AtChannelHwIdGet((AtChannel)flow) + Tha60210011ModulePweBaseAddress();
    }

static eAtRet EthFlowPtchInsertionModeSet(ThaModulePwe self, AtEthFlow flow, eAtPtchMode insertMode)
    {
    uint32 regAddr, regVal;

    if (!PtchInsertModeIsSupported(insertMode))
        return cAtErrorModeNotSupport;

    regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + FlowHeaderOffSet(self, flow);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_pw_txeth_hdr_mode_TxEthPtch1Byte_, (insertMode == cAtPtchModeOneByte) ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtPtchMode EthFlowPtchInsertionModeGet(ThaModulePwe self, AtEthFlow flow)
    {
    uint32 regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + FlowHeaderOffSet(self, flow);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & cAf6_pw_txeth_hdr_mode_TxEthPtch1Byte_Mask) ? cAtPtchModeOneByte : cAtPtchModeTwoBytes;
    }

static void PrintDword(uint32 dword, uint32 color)
    {
    AtPrintc(color, "  %02x", (uint8)(dword >> 24));
    AtPrintc(color, "  %02x", (uint8)(dword >> 16));
    AtPrintc(color, "  %02x", (uint8)(dword >> 8));
    AtPrintc(color, "  %02x", (uint8)(dword));
    }

static eAtRet ShowHeaderFromBuffer(ThaModulePwe self, uint8 headerLenInByte)
    {
    uint32 num16BytesSegment, segment_i;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 byte_i, byteInDw_i;
    int8 dword_i;
    uint32 regAddr = cAf6Reg_pla_out_psnbuf_ctrl_Base + Tha60210011ModulePlaBaseAddress();
    uint32 numRequestedByte = headerLenInByte;

    numRequestedByte  = numRequestedByte + cNumAdditionalByte;
    num16BytesSegment = numRequestedByte / 16;
    byte_i = 0;

    for (segment_i = 0; segment_i < num16BytesSegment; segment_i++)
        {
        mModuleHwLongRead(self, regAddr + segment_i, longRegVal, 4, Core(self));
        for (dword_i = 3; dword_i >= 0; dword_i--)
            PrintDword(longRegVal[dword_i], (segment_i == 0) ? cSevWarning : cSevNormal);

        AtPrintc(cSevNormal, "\r\n");
        byte_i = (uint8)(byte_i + 16);
        }

    if (byte_i >= numRequestedByte)
        return cAtOk;

    /* Last segment */
    mModuleHwLongRead(self, regAddr + segment_i, longRegVal, 4, Core(self));
    byteInDw_i = 3;
    dword_i = 3;
    while (byte_i < numRequestedByte)
        {
        AtPrintc(cSevNormal, "  %02x", (uint8)(longRegVal[dword_i] >> ByteShift(byteInDw_i)));
        byte_i++;
        if (byteInDw_i == 0)
            {
            byteInDw_i = 3;
            dword_i--;
            }
        else
            byteInDw_i = (uint8)(byteInDw_i - 1);
        }

    AtPrintc(cSevNormal, "\r\n");
    return cAtOk;
    }

static eAtRet EthFlowHeaderShow(ThaModulePwe self, AtEthFlow flow, uint8 page, uint8 headerLenInByte)
    {
    uint32 lengthToRequest = (uint32)(headerLenInByte  + cNumAdditionalByte);

    /* It 's not superfluous, request 0 byte will make HW hung */
    if (lengthToRequest == 0)
        return cAtOk;

    if (ReadRequest(self, flow, page, (uint8)lengthToRequest) != cAtOk)
        mChannelReturn(flow, cAtErrorIndrAcsTimeOut);

    return ShowHeaderFromBuffer(self, headerLenInByte);
    }

static AtModuleEth ModuleEth(ThaModulePwe self)
    {
    return (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEth);
    }

static eAtRet PtchEnable(AtModule self)
    {
    ThaModulePwe modulePwe = (ThaModulePwe)self;

    if (Tha60210012ModulePwePtchEnableOnModuleIsSupported(modulePwe))
        return Tha60210012ModulePwePtchInsertionModeSet(modulePwe, Tha60210012ModulePweDefaultPtchMode());

    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return PtchEnable(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static uint32 PwTxPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    const uint8 cTxPacketCounterType = 8;
    if (ThaPwTypeIsHdlcPppFr(pw))
        return Tha60210012ModuleEthHwFlowCounterGet((ThaModuleEth)ModuleEth(self), Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pw), cTxPacketCounterType, clear);
    return m_ThaModulePweMethods->PwTxPacketsGet(self, pw, clear);
    }

static uint32 PwTxBytesGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    const uint8 cTxByteCounterType = 9;
    if (ThaPwTypeIsHdlcPppFr(pw))
        return Tha60210012ModuleEthHwFlowCounterGet((ThaModuleEth)ModuleEth(self), Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pw), cTxByteCounterType, clear);
    return m_ThaModulePweMethods->PwTxBytesGet(self, pw, clear);
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "LO PLA Payload Control Register of OC48#0 OC#24#0",
         "LO PLA Payload Control Register of OC48#0 OC#24#1",
         "LO Lookup Control Register of OC48#0",
         "LO PLA Payload Control Register of OC48#1 OC#24#0",
         "LO PLA Payload Control Register of OC48#1 OC#24#1",
         "LO Lookup Control Register of OC48#1",
         "HO PLA Payload Control Register of OC96#0 OC#48#0",
         "HO PLA Payload Control Register of OC96#0 OC#48#1",
         "HO Lookup Control Register of OC96#0",
         "PLA Output Flow Control Register",
         "PLA Protection Flow Control Register",
         "PLA HSPW Flow Control Register",
         "PLA UPSR Flow Control Register",
         "PLA Common Pseudowire Control Register",
         "PLA Lookup Flow Control Register",
         "PLA Block Buffer Control Register",
         "PLA CRC Error Control",
         "PWE Pseudowire Transmit Ethernet Header Mode Control slice 0"
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static uint32 StartCrcRamLocalId(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 16;
    }

static uint32 StartHSPWRamLocalId(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 9;
    }

static uint32 StartHoPlaRamLocalId(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 6;
    }

static uint32 StartPweRamLocalId(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 17;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    if (localRamId < mMethodsGet((Tha60210011ModulePwe)self)->StartPweRamLocalId((Tha60210011ModulePwe)self))
        return Tha60210012InternalRamPlaNew(self, ramId, localRamId);

    return Tha60210011InternalRamPweNew(self, ramId, localRamId);
    }

static uint32 StartVersionControlJitterAttenuator(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x2, 0x0);
    }

static uint8 PayloadTypeHwMode(ThaPwHdlcAdapter adapter, eAtPwHdlcPayloadType payloadType)
    {
    if ((payloadType == cAtPwHdlcPayloadTypePdu) && (AtPwPsnGet((AtPw)adapter)))
        return 1;

    return 0;
    }

static eAtRet HdlcPwPayloadTypeSet(ThaModulePwe self, ThaPwHdlcAdapter adapter, eAtPwHdlcPayloadType payloadType)
    {
    uint32 regAddr  = cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(self, (AtPw)adapter);
    uint32 regValue = mChannelHwRead(adapter, regAddr, cThaModulePwe);

    mRegFieldSet(regValue, cAf6_pw_txeth_hdr_mode_TxEthMsPW_, PayloadTypeHwMode(adapter, payloadType));
    mChannelHwWrite(adapter, regAddr, regValue, cThaModulePwe);

    return cAtOk;
    }

static uint8 NumSlice48(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return 2;
    }

static eBool PtchEnableOnModuleIsSupported(Tha60210012ModulePwe self)
    {
    return Tha60210012ModuleEthReduceTo4Serdes(ModuleEth((ThaModulePwe)self));
    }

static eAtRet PwCwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    if (ThaPwTypeIsCesCep(pw))
        return m_ThaModulePweMethods->PwCwEnable(self, pw, enable);

    regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pw_txeth_hdr_mode_TxEthCwEn_, mBoolToBin(enable));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return mRegField(regVal, cAf6_pw_txeth_hdr_mode_TxEthCwEn_) ? cAtTrue : cAtFalse;
    }

static AtHdlcLink PppLinkFromPppPw(AtPw adapter)
    {
    return (AtHdlcLink)AtPwBoundCircuitGet(adapter);
    }

static eBool ServiceIsBcpPw(AtPw adapter)
    {
    if (AtPwTypeGet(adapter) != cAtPwTypePpp)
        return cAtFalse;

    if ((AtPwHdlcPayloadTypeGet((AtPwHdlc)ThaPwAdapterPwGet((ThaPwAdapter)adapter)) == cAtPwHdlcPayloadTypePdu) &&
        (AtHdlcLinkPduTypeGet(PppLinkFromPppPw(adapter)) == cAtHdlcPduTypeEthernet))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 StartVersionSupportLanFcsRemove(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x4, 0x1016);
    }

static uint32 StartVersionSupportInsertEXaui(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x0, 0x1016);
    }

static eBool LanFcsRemoveIsSupported(ThaModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersionSupported = mMethodsGet(mThis(self))->StartVersionSupportLanFcsRemove(mThis(self));

    if (hwVersion >= startVersionSupported)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet PwPtchServiceEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(self, pw);
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_pw_txeth_hdr_mode_TxEthPtchEn_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool PwPtchServiceIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(self, pw);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_pw_txeth_hdr_mode_TxEthPtchEn_) ? cAtTrue : cAtFalse;
    }

static eAtRet EthFlowPtchServiceEnable(ThaModulePwe self, AtEthFlow flow, eBool enable)
    {
    uint32 regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + FlowHeaderOffSet(self, flow);
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_pw_txeth_hdr_mode_TxEthPtchEn_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool EthFlowPtchServiceIsEnabled(ThaModulePwe self, AtEthFlow flow)
    {
    uint32 regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + FlowHeaderOffSet(self, flow);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_pw_txeth_hdr_mode_TxEthPtchEn_) ? cAtTrue : cAtFalse;
    }

static uint32 VcgFlowLookUpOffset(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return 3072UL;
    }

static uint32 NumLoBlocks(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return 4096UL;
    }

static uint32 IMsgLinkAddressOffset(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return 3072UL;
    }

static uint32 VcgAddressOffset(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return 6144UL;
    }

static void EthFlowRegsShow(ThaModulePwe self, AtEthFlow flow)
    {
    uint32 regAddr, regVal;
    AtEncapChannel channel = AtEthFlowBoundEncapChannelGet(flow);
    uint32 channelId = AtChannelIdGet((AtChannel)channel);
    uint32 flowId = AtChannelHwIdGet((AtChannel)flow);

    regAddr = cAf6Reg_TxEth_PSNPtchEn_Base + Tha60210011ModulePweBaseAddress();
    regVal  = mModuleHwRead(self, regAddr);
    Tha6021DebugPrintRegName(NULL, "Pseudowire PSN PTCH Enable Control", regAddr, regVal);

    regAddr = cAf6Reg_pla_blk_buf_ctrl_Base + channelId + Tha60210011ModulePlaBaseAddress();
    regVal  = mModuleHwRead(self, regAddr);
    Tha6021DebugPrintRegName(NULL, "Payload Assembler Block Buffer Control", regAddr, regVal);

    regAddr = cAf6Reg_pla_out_flow_ctrl_Base + flowId + Tha60210011ModulePlaBaseAddress();
    regVal  = mModuleHwRead(self, regAddr);
    Tha6021DebugPrintRegName(NULL, "Payload Assembler Output Flow Control", regAddr, regVal);

    regAddr = cAf6Reg_pw_txeth_hdr_mode_Base + FlowHeaderOffSet(self, flow);
    regVal  = mModuleHwRead(self, regAddr);
    Tha6021DebugPrintRegName(NULL, "Pseudowire Transmit Ethernet Header Mode Control", regAddr, regVal);

    regAddr = cAf6Reg_TxEth_PSNPtchEn_Base + Tha60210011ModulePweBaseAddress();
    regVal  = mModuleHwRead(self, regAddr);
    Tha6021DebugPrintRegName(NULL, "Pseudowire PSN PTCH Enable Control", regAddr, regVal);
    }

static uint32 MaxChannelHeader(Tha60210011ModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleEth moduleEth = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return AtModuleEthMaxFlowsGet(moduleEth);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210012ModulePwe object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(blockUsedMask);
    }

static eAtRet HwResourceCheck(AtModule self)
    {
    uint32 regVal = mModuleHwRead(self, cAf6Reg_pla_debug_sta_Base + Tha60210011ModulePlaBaseAddress());
    uint32 freeVcgBlock = mRegField(regVal, cAf6_pla_debug_sta_VCG_free_block_number_);
    uint32 freeWriteCache = mRegField(regVal, cAf6_pla_debug_sta_write_cache_number_);

    if (AtDeviceIsSimulated(AtModuleDeviceGet(self)))
        return cAtOk;

    AtModuleResourceDiminishedLog(self, "VCG blocks", freeVcgBlock, mMethodsGet(mThis(self))->MaxVcgBlocks(mThis(self)));
    AtModuleResourceDiminishedLog(self, "Write caches", freeWriteCache, mMethodsGet(mThis(self))->MaxWriteCaches(mThis(self)));

    return cAtOk;
    }

static uint32 MaxVcgBlocks(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return 1024;
    }

static uint32 MaxWriteCaches(Tha60210012ModulePwe self)
    {
    AtUnused(self);
    return 0x2000;
    }

static void DebuggerEntriesFill(AtModule self, AtDebugger debugger)
    {
    uint32 regVal = mModuleHwRead(self, cAf6Reg_pla_debug_sta_Base + Tha60210011ModulePlaBaseAddress());
    uint32 freeVcgBlock = mRegField(regVal, cAf6_pla_debug_sta_VCG_free_block_number_);
    uint32 freeWriteCache = mRegField(regVal, cAf6_pla_debug_sta_write_cache_number_);

    m_AtModuleMethods->DebuggerEntriesFill(self, debugger);
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoResourceNew("VCG blocks", mMethodsGet(mThis(self))->MaxVcgBlocks(mThis(self)), freeVcgBlock));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoResourceNew("Write caches", mMethodsGet(mThis(self))->MaxWriteCaches(mThis(self)), freeWriteCache));
    }

static void DebugInforLowOrderClock155(Tha60210011ModulePwe self, AtPw pw)
    {
    if (ThaPwTypeIsCesCep(pw))
        m_Tha60210011ModulePweMethods->DebugInforLowOrderClock155(self, pw);
    }

static uint32 PwPweDefaultOffset(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    return Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pw) + Tha60210011ModulePweBaseAddress();
    }

static void OverrideTha60210011ModulePwe(AtModule self)
    {
    Tha60210011ModulePwe modulePwe = (Tha60210011ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePweMethods = mMethodsGet(modulePwe);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePweOverride, mMethodsGet(modulePwe), sizeof(m_Tha60210011ModulePweOverride));
        
        mMethodOverride(m_Tha60210011ModulePweOverride, LoPayloadCtrl);
        mMethodOverride(m_Tha60210011ModulePweOverride, LoPayloadCtrlOffset);
        mMethodOverride(m_Tha60210011ModulePweOverride, HoPayloadCtrl);
        mMethodOverride(m_Tha60210011ModulePweOverride, HoPayloadCtrlOffset);

        mMethodOverride(m_Tha60210011ModulePweOverride, LoAddRemoveProtocolReg);
        mMethodOverride(m_Tha60210011ModulePweOverride, LoAddRemoveProtocolOffset);
        mMethodOverride(m_Tha60210011ModulePweOverride, HoAddRemoveProtocolOffset);
        mMethodOverride(m_Tha60210011ModulePweOverride, HoAddRemoveProtocolReg);

        mMethodOverride(m_Tha60210011ModulePweOverride, PsnControlReg);
        mMethodOverride(m_Tha60210011ModulePweOverride, PsnChannelIdMask);
        mMethodOverride(m_Tha60210011ModulePweOverride, PsnChannelIdShift);
        mMethodOverride(m_Tha60210011ModulePweOverride, PsnPwHwId);
        mMethodOverride(m_Tha60210011ModulePweOverride, PsnBufferCtrlReg);
        mMethodOverride(m_Tha60210011ModulePweOverride, PlaPwDefaultHwConfigure);
        mMethodOverride(m_Tha60210011ModulePweOverride, PwPlaRegsShow);
        mMethodOverride(m_Tha60210011ModulePweOverride, PwGroupingShow);
        mMethodOverride(m_Tha60210011ModulePweOverride, MaxChannelHeader);
        mMethodOverride(m_Tha60210011ModulePweOverride, DebugInforLowOrderClock155);

        mMethodOverride(m_Tha60210011ModulePweOverride, PortCounterOffset);
        mMethodOverride(m_Tha60210011ModulePweOverride, StartHoPlaRamLocalId);
        mMethodOverride(m_Tha60210011ModulePweOverride, StartHSPWRamLocalId);
        mMethodOverride(m_Tha60210011ModulePweOverride, StartCrcRamLocalId);
        mMethodOverride(m_Tha60210011ModulePweOverride, StartPweRamLocalId);

        mMethodOverride(m_Tha60210011ModulePweOverride, StartVersionControlJitterAttenuator);
        }

    mMethodsSet(modulePwe, &m_Tha60210011ModulePweOverride);
    }

static void OverrideThaModulePweV2(AtModule self)
    {
    ThaModulePweV2 pweModule = (ThaModulePweV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweV2Methods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweV2Override, m_ThaModulePweV2Methods, sizeof(m_ThaModulePweV2Override));

        mMethodOverride(m_ThaModulePweV2Override, ApsGroupPwAdd);
        mMethodOverride(m_ThaModulePweV2Override, ApsGroupPwRemove);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupPwAdd);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupPwRemove);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupLabelSetSelect);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupSelectedLabelSetGet);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupEnable);
        mMethodOverride(m_ThaModulePweV2Override, HsGroupIsEnabled);
        mMethodOverride(m_ThaModulePweV2Override, ApsGroupEnable);
        mMethodOverride(m_ThaModulePweV2Override, ApsGroupIsEnabled);
        }

    mMethodsSet(pweModule, &m_ThaModulePweV2Override);
    }

static void OverrideTha60210051ModulePwe(AtModule self)
    {
    Tha60210051ModulePwe modulePwe = (Tha60210051ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051ModulePweOverride, mMethodsGet(modulePwe), sizeof(m_Tha60210051ModulePweOverride));
        mMethodOverride(m_Tha60210051ModulePweOverride, PlaHoldRegistersGet);
        mMethodOverride(m_Tha60210051ModulePweOverride, NewPsnIsSupported);
        mMethodOverride(m_Tha60210051ModulePweOverride, ShouldReactivateWhenCacheInUnsafe);
        }

    mMethodsSet(modulePwe, &m_Tha60210051ModulePweOverride);
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, m_ThaModulePweMethods, sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PwEnable);
        mMethodOverride(m_ThaModulePweOverride, PwIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxLBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxLBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxRBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxRBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxMBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxMBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwTxLBitForce);
        mMethodOverride(m_ThaModulePweOverride, PwTxLBitIsForced);
        mMethodOverride(m_ThaModulePweOverride, PwTxRBitForce);
        mMethodOverride(m_ThaModulePweOverride, PwTxRBitIsForced);
        mMethodOverride(m_ThaModulePweOverride, PwTxMBitForce);
        mMethodOverride(m_ThaModulePweOverride, PwTxMBitIsForced);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxNPBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxNPBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwSupressEnable);
        mMethodOverride(m_ThaModulePweOverride, PwSupressIsEnabled);

        mMethodOverride(m_ThaModulePweOverride, DefaultSet);
        mMethodOverride(m_ThaModulePweOverride, PwTypeSet);
        mMethodOverride(m_ThaModulePweOverride, PwPtchInsertionModeSet);
        mMethodOverride(m_ThaModulePweOverride, PwPtchInsertionModeGet);
        mMethodOverride(m_ThaModulePweOverride, PwRemovingStart);
        mMethodOverride(m_ThaModulePweOverride, EthFlowPtchInsertionModeSet);
        mMethodOverride(m_ThaModulePweOverride, EthFlowPtchInsertionModeGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxBytesGet);
        mMethodOverride(m_ThaModulePweOverride, HdlcPwPayloadTypeSet);
        mMethodOverride(m_ThaModulePweOverride, PwCwEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwPtchServiceEnable);
        mMethodOverride(m_ThaModulePweOverride, PwPtchServiceIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, EthFlowPtchServiceEnable);
        mMethodOverride(m_ThaModulePweOverride, EthFlowPtchServiceIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, EthFlowRegsShow);
        mMethodOverride(m_ThaModulePweOverride, PwPweDefaultOffset);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, HwResourceCheck);
        mMethodOverride(m_AtModuleOverride, DebuggerEntriesFill);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void MethodsInit(Tha60210012ModulePwe self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Override methods */
        mMethodOverride(m_methods, PwLookupIdMask);
        mMethodOverride(m_methods, PwLookupIdShift);
        mMethodOverride(m_methods, HspwGroupMask);
        mMethodOverride(m_methods, HspwGroupShift);
        mMethodOverride(m_methods, UpsrGroupMask);
        mMethodOverride(m_methods, UpsrGroupShift);
        mMethodOverride(m_methods, NumSlice48);
        mMethodOverride(m_methods, PtchEnableOnModuleIsSupported);
        mMethodOverride(m_methods, StartVersionSupportLanFcsRemove);
        mMethodOverride(m_methods, StartVersionSupportInsertEXaui);
        mMethodOverride(m_methods, VcgFlowLookUpOffset);
        mMethodOverride(m_methods, NumLoBlocks);
        mMethodOverride(m_methods, VcgAddressOffset);
        mMethodOverride(m_methods, IMsgLinkAddressOffset);
        mMethodOverride(m_methods, MaxVcgBlocks);
        mMethodOverride(m_methods, MaxWriteCaches);
        }

   mMethodsSet(self, &m_methods);
   }

static void Override(AtModule self)
    {
	OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideThaModulePwe(self);
    OverrideThaModulePweV2(self);
    OverrideTha60210051ModulePwe(self);
    OverrideTha60210011ModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModulePwe);
    }

AtModule Tha60210012ModulePweObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210012ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModulePweObjectInit(newModule, device);
    }

eAtRet Tha60210012ModulePwePwActivateConfiguration(ThaModulePwe self, ThaPwAdapter adapter)
    {
    eAtRet ret = cAtOk;
    AtPw pw = (AtPw)adapter;
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    AtEthPort ethPort = AtPwEthPortGet(pw);
    ThaModuleCla claModule;

    if (circuit == NULL || ethPort == NULL)
        return cAtOk;

    /* With HDLC/PPP/FR block allocated when physical bound with encap channel */
    if (ThaPwTypeIsCesCep(pw))
        {
        ret = AtChannelHwResourceAllocate(circuit);
        if (ret != cAtOk)
            return ret;

    ret = FlowForPwSet(adapter);
        }

    if (!LanFcsRemoveIsSupported(self))
        return ret;

    if (ServiceIsBcpPw(pw))
        {
        AtPppLink link = (AtPppLink)PppLinkFromPppPw(pw);
        ret |= AtPppLinkBcpLanFcsInsertionEnable(link, ThaDynamicPppLinkLanFcsCacheGet((AtHdlcLink)link));
        }
    else
        {
        claModule = (ThaModuleCla)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)ethPort), cThaModuleCla);
        ret |= Tha60210012ModuleClaPwLanFcsRemoveEnable(claModule, (AtPw)adapter, cAtTrue);
        }

    return ret;
    }

eAtRet Tha60210012ModulePwePwDeactivateConfiguration(ThaModulePwe self, ThaPwAdapter adapter)
    {
    AtPw pw = (AtPw)adapter;
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    AtEthPort ethPort = AtPwEthPortGet(pw);

    AtUnused(self);

    if (circuit == NULL || ethPort == NULL)
        return cAtOk;

    /* May need to handle another jobs at here! */
    return AtChannelHwResourceDeallocate(circuit);
    }

eAtRet Tha60210012ModulePweFlowHeaderRead(ThaModulePwe self, AtEthFlow flow, uint8 page, uint8 *buffer, uint8 headerLenInByte)
    {
    uint32 lengthToRequest = (uint32)(headerLenInByte  + cNumAdditionalByte);

    if (ReadRequest(self, flow, page, (uint8)lengthToRequest) != cAtOk)
        mChannelReturn(flow, cAtErrorIndrAcsTimeOut);

    /* But only get header, ignore RTP */
    return ReadHeaderFromBuffer(self, buffer, headerLenInByte);
    }

eAtRet Tha60210012ModulePweFlowHeaderWrite(ThaModulePwe self, AtEthFlow flow, uint8 page, uint8 *buffer, uint8 headerLenInByte)
    {
    eAtRet ret;

    if (WriteHeaderToBuffer(self, flow, buffer, headerLenInByte) != cAtOk)
        return cAtErrorModeNotSupport;

    ret = WriteRequest(self, flow, page, (uint8)(headerLenInByte + cNumAdditionalByte));
    if (ret != cAtOk)
        mChannelReturn(flow, ret);

    return ret;
    }

uint8 Tha60210012ModulePweFlowHeaderLengthGet(ThaModulePwe self, AtEthFlow flow)
    {
    uint32 regVal;
    uint32 flowId = AtChannelHwIdGet((AtChannel)flow);
    uint32 regAddr = cAf6Reg_pla_out_flow_ctrl_Base + flowId + Tha60210011ModulePlaBaseAddress();
    AtUnused(self);

    regVal = mChannelHwRead(flow, regAddr, cThaModulePwe);
    return RealHeaderLength((uint8)mRegField(regVal, cAf6_pla_out_flow_ctrl_PlaOutPSNLenCtrl_));
    }

eAtRet Tha60210012ModulePweFlowHeaderLengthSet(ThaModulePwe self, AtEthFlow flow, uint8 hdrLenInByte)
    {
    uint32 flowId = AtChannelHwIdGet((AtChannel)flow);
    uint32 regAddr = cAf6Reg_pla_out_flow_ctrl_Base + flowId + Tha60210011ModulePlaBaseAddress();
    uint32 regVal = mChannelHwRead(flow, regAddr, cThaModulePwe);

    AtUnused(self);
    mRegFieldSet(regVal, cAf6_pla_out_flow_ctrl_PlaOutPSNLenCtrl_, HeaderLengthWriteToHw(hdrLenInByte));
    mChannelHwWrite(flow, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

eAtRet Tha60210012ModulePweEthFlowVcgLookup(ThaModulePwe self, uint32 vcgId, AtEthFlow flow)
    {
    uint32 cVcgFlowOffset =  mMethodsGet(mThis(self))->VcgFlowLookUpOffset(mThis(self));
    uint32 flowId = AtChannelHwIdGet((AtChannel)flow);
    uint32 address = cAf6Reg_pla_lk_flow_ctrl_Base + vcgId + cVcgFlowOffset + Tha60210011ModulePlaBaseAddress();
    uint32 regVal = mChannelHwRead(flow, address, cThaModulePwe);

    AtUnused(self);

    mRegFieldSet(regVal, cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_, flowId);
    mChannelHwWrite(flow, address, regVal, cThaModulePwe);
    return cAtOk;
    }

uint32 Tha60210012ModulePweEthFlowOfPwGet(ThaPwAdapter pw)
    {
    uint32 pwId = AtChannelIdGet((AtChannel)pw);
    uint32 address = cAf6Reg_pla_lk_flow_ctrl_Base + pwId +  Tha60210011ModulePlaBaseAddress();
    uint32 regVal = mChannelHwRead(pw, address, cThaModulePwe);

    return mRegField(regVal, cAf6_pla_lk_flow_ctrl_PlaOutLkFlowCtrl_);
    }

eBool Tha60210012ModulePweInsertEXauiIsSupported(AtModule self)
    {
    if (self)
        return InsertEXauiIsSupported(self);
    return cAtFalse;
    }

uint32 Tha60210012ModulePweStartVersionSupportInsertEXaui(ThaModulePwe self)
    {
    return mMethodsGet(mThis(self))->StartVersionSupportInsertEXaui(mThis(self));
    }

void Tha60210012ModulePweEthFlowHeaderShow(ThaModulePwe self, AtEthFlow flow)
    {
    uint8 headerLengthInByte = Tha60210012ModulePweFlowHeaderLengthGet(self, flow);

    AtPrintc(cSevInfo, "* Header length: ");
    AtPrintc(cSevNormal, "%u bytes (Not include additional bytes)\r\n", headerLengthInByte);
    AtPrintc(cSevInfo, "* Header bytes : (bytes in yellow are additional bytes)\r\n");
    EthFlowHeaderShow(self, flow, cWorkingPsnPage, headerLengthInByte);
    }

eBool Tha60210012ModulePwePtchEnableOnModuleIsSupported(ThaModulePwe self)
    {
    if (self)
        return mMethodsGet(mThis(self))->PtchEnableOnModuleIsSupported(mThis(self));
    return cAtFalse;
    }

eBool Tha60210012ModulePwePtchEnablePerChannelIsSupported(ThaModulePwe self)
    {
    return !Tha60210012ModulePwePtchEnableOnModuleIsSupported(self);
    }

eAtRet Tha60210012ModulePwePtchInsertionModeSet(ThaModulePwe self, eAtPtchMode insertMode)
    {
    uint32 regAddr = cAf6Reg_TxEth_PSNPtchEn_Base + Tha60210011ModulePweBaseAddress();
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_TxEth_PSNPtchEn_TxEthPtch1Byte_, (insertMode == cAtPtchModeOneByte) ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

eAtPtchMode Tha60210012ModulePwePtchInsertionModeGet(ThaModulePwe self)
    {
    uint32 regAddr = cAf6Reg_TxEth_PSNPtchEn_Base + Tha60210011ModulePweBaseAddress();
    uint32 regVal  = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_TxEth_PSNPtchEn_TxEthPtch1Byte_) ? cAtPtchModeOneByte : cAtPtchModeTwoBytes;
    }

eAtPtchMode Tha60210012ModulePweDefaultPtchMode(void)
    {
    return cAtPtchModeOneByte;
    }

eAtRet Tha60210012ModulePwePtchInsertionEnable(ThaModulePwe self, eBool enable)
    {
    uint32 regAddr = cAf6Reg_TxEth_PSNPtchEn_Base + Tha60210011ModulePweBaseAddress();
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_TxEth_PSNPtchEn_TxEthPtchEn_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

eBool Tha60210012ModulePwePtchInsertionIsEnabled(ThaModulePwe self)
    {
    uint32 regAddr = cAf6Reg_TxEth_PSNPtchEn_Base + Tha60210011ModulePweBaseAddress();
    uint32 regVal  = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_TxEth_PSNPtchEn_TxEthPtchEn_) ? cAtTrue : cAtFalse;
    }

uint32 Tha60210012ModulePweNumLoBlocks(Tha60210012ModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->NumLoBlocks(self);
    return 0;
    }

uint32 Tha60210012ModulePweVcgAddressOffset(Tha60210012ModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->VcgAddressOffset(self);
    return 0;
    }

uint32 Tha60210012ModulePweIMsgLinkAddressOffset(Tha60210012ModulePwe self)
    {
    if (self)
        return mMethodsGet(self)->IMsgLinkAddressOffset(self);
    return 0;
    }

eAtRet Tha60210012ModulePweTdmToPsnEthTypeSet(Tha60210012ModulePwe self, uint32 profileId, uint32 ethType)
    {
    uint32 offset     = Tha60210011ModulePweBaseAddress() + profileId;
    uint32 regAddress = cAf6Reg_TxEth_PSNChgEthType_Base + offset;
    uint32 regValue   = mModuleHwRead(self, regAddress);

    mRegFieldSet(regValue, cAf6_TxEth_PSNChgEthType_TxEth_PSNChgEthType_, ethType);
    mModuleHwWrite(self, regAddress, regValue);

    return cAtOk;
    }

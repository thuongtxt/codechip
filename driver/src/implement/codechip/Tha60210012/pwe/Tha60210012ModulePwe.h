/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60210012ModulePwe.h
 * 
 * Created Date: Mar 9, 2016
 *
 * Description : Module PWE of 60210012
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEPWE_H_
#define _THA60210012MODULEPWE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPw.h"
#include "../../../default/pwe/ThaModulePwe.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModulePwe *Tha60210012ModulePwe;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210012ModulePwePwActivateConfiguration(ThaModulePwe self, ThaPwAdapter pw);
eAtRet Tha60210012ModulePwePwDeactivateConfiguration(ThaModulePwe self, ThaPwAdapter adapter);
eAtRet Tha60210012ModulePweFlowHeaderRead(ThaModulePwe self, AtEthFlow flow, uint8 page, uint8 *buffer, uint8 headerLenInByte);
eAtRet Tha60210012ModulePweFlowHeaderWrite(ThaModulePwe self, AtEthFlow flow, uint8 page, uint8 *buffer, uint8 headerLenInByte);
uint8 Tha60210012ModulePweFlowHeaderLengthGet(ThaModulePwe self, AtEthFlow flow);
eAtRet Tha60210012ModulePweFlowHeaderLengthSet(ThaModulePwe self, AtEthFlow flow, uint8 hdrLenInByte);
uint32 Tha60210012ModulePweEthFlowOfPwGet(ThaPwAdapter pw);

/* FLow VCG Lookup */
eAtRet Tha60210012ModulePweEthFlowVcgLookup(ThaModulePwe self, uint32 vcgId, AtEthFlow flow);

eBool Tha60210012ModulePweInsertEXauiIsSupported(AtModule self);
uint32 Tha60210012ModulePweStartVersionSupportInsertEXaui(ThaModulePwe self);
eBool Tha60210012ModulePwePtchEnableOnModuleIsSupported(ThaModulePwe self);
eBool Tha60210012ModulePwePtchEnablePerChannelIsSupported(ThaModulePwe self);
void Tha60210012ModulePweEthFlowHeaderShow(ThaModulePwe self, AtEthFlow flow);
eAtRet Tha60210012ModulePwePtchInsertionModeSet(ThaModulePwe self, eAtPtchMode insertMode);
eAtPtchMode Tha60210012ModulePwePtchInsertionModeGet(ThaModulePwe self);
eAtRet Tha60210012ModulePwePtchInsertionEnable(ThaModulePwe self, eBool enable);
eBool Tha60210012ModulePwePtchInsertionIsEnabled(ThaModulePwe self);
eAtPtchMode Tha60210012ModulePweDefaultPtchMode(void);

uint32 Tha60210012ModulePweNumLoBlocks(Tha60210012ModulePwe self);
uint32 Tha60210012ModulePweVcgAddressOffset(Tha60210012ModulePwe self);
uint32 Tha60210012ModulePweIMsgLinkAddressOffset(Tha60210012ModulePwe self);
eAtRet Tha60210012PwCircuitDe3ExpectedBlockAssign(AtChannel self, uint32 expectedBlock);
eAtRet Tha60210012ModulePweTdmToPsnEthTypeSet(Tha60210012ModulePwe self, uint32 profileId, uint32 ethType);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEPWE_H_ */


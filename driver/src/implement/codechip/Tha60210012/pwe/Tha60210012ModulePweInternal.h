/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60210012ModulePweInternal.h
 * 
 * Created Date: May 24, 2016
 *
 * Description : PWE module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEPWEINTERNAL_H_
#define _THA60210012MODULEPWEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/pwe/Tha60210051ModulePweInternal.h"
#include "Tha60210012ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModulePweMethods
    {
    uint32 (*PwLookupIdMask)(Tha60210012ModulePwe self);
    uint32 (*PwLookupIdShift)(Tha60210012ModulePwe self);
    uint32 (*HspwGroupMask)(Tha60210012ModulePwe self);
    uint32 (*HspwGroupShift)(Tha60210012ModulePwe self);
    uint32 (*UpsrGroupMask)(Tha60210012ModulePwe self);
    uint32 (*UpsrGroupShift)(Tha60210012ModulePwe self);
    uint8  (*NumSlice48)(Tha60210012ModulePwe self);
    eBool  (*PtchEnableOnModuleIsSupported)(Tha60210012ModulePwe self);
    uint32 (*StartVersionSupportLanFcsRemove)(Tha60210012ModulePwe self);
    uint32 (*StartVersionSupportInsertEXaui)(Tha60210012ModulePwe self);

    uint32 (*VcgFlowLookUpOffset)(Tha60210012ModulePwe self);
    uint32 (*NumLoBlocks)(Tha60210012ModulePwe self);
    uint32 (*VcgAddressOffset)(Tha60210012ModulePwe self);
    uint32 (*IMsgLinkAddressOffset)(Tha60210012ModulePwe self);

    uint32 (*MaxVcgBlocks)(Tha60210012ModulePwe self);
    uint32 (*MaxWriteCaches)(Tha60210012ModulePwe self);
    }tTha60210012ModulePweMethods;

typedef struct tTha60210012ModulePwe
    {
    tTha60210051ModulePwe super;
    const tTha60210012ModulePweMethods * methods;

    /* Private data */
    ThaBitMask blockUsedMask;
    }tTha60210012ModulePwe;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210012ModulePweObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEPWEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60210012ModulePweBlockManager.c
 *
 * Created Date: May 24, 2016
 *
 * Description : Block manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "../man/Tha60210012Device.h"
#include "../pdh/Tha60210012ModulePdh.h"
#include "../sdh/Tha60210012ModuleSdh.h"
#include "../common/Tha60210012Channel.h"
#include "../concate/Tha60210012ModuleConcate.h"
#include "Tha60210012ModulePlaReg.h"
#include "Tha60210012ModulePweBlockManager.h"
#include "Tha60210012ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210012ModulePwe*)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePwe ModulePwe(AtChannel channel)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static uint32 NumBlocks(Tha60210012ModulePwe self)
    {
    return Tha60210012ModulePweNumLoBlocks(self);
    }

static uint32 VcatStartBlockOffset(AtChannel channel)
    {
    return Tha60210012ModulePweVcgAddressOffset((Tha60210012ModulePwe)ModulePwe(channel));
    }

static uint32 IMsgStartBlockOffset(AtChannel channel)
    {
    return Tha60210012ModulePweIMsgLinkAddressOffset((Tha60210012ModulePwe)ModulePwe(channel));
    }

static ThaBitMask BlockUsedMask(Tha60210012ModulePwe self)
    {
    if (self->blockUsedMask == NULL)
        self->blockUsedMask = ThaBitMaskNew(NumBlocks(self));
    return self->blockUsedMask;
    }

static eAtRet HwBufferSet(AtChannel channel, uint32 channelHwOffset, uint32 hwBlockType, uint32 bufferId)
    {
    uint32 address, regVal;
    address = cAf6Reg_pla_blk_buf_ctrl_Base + channelHwOffset + Tha60210011ModulePlaBaseAddress();

    regVal = mChannelHwRead(channel, address, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_blk_buf_ctrl_PlaBlkTypeCtr_, hwBlockType);
    mRegFieldSet(regVal, cAf6_pla_blk_buf_ctrl_PlaBlkIDCtr_, bufferId);

    mChannelHwWrite(channel, address, regVal, cThaModulePwe);
    return cAtOk;
    }

static uint32 FreeHwBlockId(Tha60210012ModulePwe self)
    {
    ThaBitMask mask = BlockUsedMask(self);
    uint32 freeBit = ThaBitMaskLastZeroBit(mask);

    if (ThaBitMaskBitPositionIsValid(mask, freeBit))
        {
        ThaBitMaskSetBit(mask, freeBit);
        return freeBit;
        }

    return cInvalidUint32;
    }

static eAtRet HwBlockAssign(AtChannel channel, uint32 hwChannelId, uint32 blockType, uint32 blockId)
    {
    HwBufferSet(channel, hwChannelId, blockType, blockId);
    return Tha60210012ChannelPlaBlockIdSet(channel, blockId);
    }

static uint32 ExpectLoRateBlockIdGet(uint8 slice48, uint8 hwSts48, uint8 hwVtg, uint8 hwVt)
    {
    return (slice48 * 1344UL) +  hwSts48 * 28UL +  hwVtg * 4UL + hwVt;
    }

static uint32 LoRateDe1BlockIdGet(ThaPdhDe1 de1)
    {
    uint8 slice48, hwSts48, hwVtg, hwVt;
    ThaPdhDe1HwSlice48AndSts48IdGet(de1, &slice48, &hwSts48, &hwVtg, &hwVt);
    return ExpectLoRateBlockIdGet(slice48, hwSts48, hwVtg, hwVt);
    }

static uint32 LoRateVc1xBlockIdGet(AtSdhChannel vc1x)
    {
    uint8 slice48, hwSts48, hwVtg, hwVt;
    ThaSdhChannel2HwMasterStsId(vc1x, cThaModuleOcn, &slice48, &hwSts48);
    hwVtg = AtSdhChannelTug2Get(vc1x);
    hwVt = AtSdhChannelTu1xGet(vc1x);
    return ExpectLoRateBlockIdGet(slice48, hwSts48, hwVtg, hwVt);
    }

static uint32 HwBlockIdAllocate(ThaModulePwe self, uint32 expectHwBlockId)
    {
    ThaBitMask mask = BlockUsedMask(mThis(self));

    /* To avoid dynamic allocation as much as possible, if the expected
     * resource has not been used, just use it */
    if (ThaBitMaskBitPositionIsValid(mask, expectHwBlockId) &&
        (ThaBitMaskBitVal(mask, expectHwBlockId) == 0))
        {
        ThaBitMaskSetBit(mask, expectHwBlockId);
        return expectHwBlockId;
        }

    /* And just simply allocate new one if that expected resource is being used */
    return FreeHwBlockId(mThis(self));
    }

static uint32 NxDs0BlockIdAllocate(AtChannel nxds0)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)AtPdhNxDS0De1Get((AtPdhNxDS0)nxds0);
    uint32 expectHwBlockId = LoRateDe1BlockIdGet(de1);
    return HwBlockIdAllocate(ModulePwe(nxds0), expectHwBlockId);
    }

static eBool BlockIsValid(uint32 hwBlockId)
    {
    return (hwBlockId != cInvalidUint32) ? cAtTrue : cAtFalse;
    }

static eAtRet NxDs0PwBlockAllocate(AtChannel nxds0, AtPw pw)
    {
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);
    uint32 hwChannelId = AtChannelIdGet((AtChannel)adapter);
    uint32 blockId = NxDs0BlockIdAllocate(nxds0);

    if (BlockIsValid(blockId))
        return HwBlockAssign(nxds0, hwChannelId, cNxDs0BlockType, blockId);

    return cAtErrorRsrcNoAvail;
    }

static eAtRet NxDs0iMsgBlockAllocate(AtChannel nxds0, AtHdlcChannel channel)
    {
    uint32 hwChannelId = AtChannelIdGet((AtChannel)channel) + IMsgStartBlockOffset(nxds0);
    uint32 blockId = NxDs0BlockIdAllocate(nxds0);

    if (BlockIsValid(blockId))
        return HwBlockAssign(nxds0, hwChannelId, cNxDs0BlockType, blockId);

    return cAtErrorRsrcNoAvail;
    }

static uint32 De1BlockIdAllocate(AtChannel de1)
    {
    uint32 expectHwBlockId = LoRateDe1BlockIdGet((ThaPdhDe1)de1);
    return HwBlockIdAllocate(ModulePwe(de1), expectHwBlockId);
    }

static eAtRet De1BlockIdSet(AtChannel de1, uint32 hwChannelId, uint32 blockId)
    {
    if (BlockIsValid(blockId))
        return HwBlockAssign(de1, hwChannelId, cDe1BlockType, blockId);

    return cAtErrorRsrcNoAvail;
    }

static eAtRet De1PwBlockAllocate(AtChannel de1, AtPw pw)
    {
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);
    return De1BlockIdSet(de1, AtChannelIdGet((AtChannel)adapter), De1BlockIdAllocate(de1));
    }

static eAtRet De1iMsgBlockAllocate(AtChannel de1, AtHdlcChannel channel)
    {
    return De1BlockIdSet(de1, AtChannelIdGet((AtChannel)channel) + IMsgStartBlockOffset(de1), De1BlockIdAllocate(de1));
    }

static uint32 Vc1xBlockIdAllocate(AtChannel vc1x)
    {
    uint32 expectHwBlockId = LoRateVc1xBlockIdGet((AtSdhChannel)vc1x);
    return HwBlockIdAllocate(ModulePwe(vc1x), expectHwBlockId);
    }

static uint32 Vc1xBlockIdSet(AtChannel vc1x, uint32 hwChannelId, uint32 blockId)
    {
    if (BlockIsValid(blockId))
        return HwBlockAssign(vc1x, hwChannelId, cVc1xBlockType, blockId);

    return cAtErrorRsrcNoAvail;
    }

static eAtRet Vc1xPwBlockAllocate(AtChannel vc1x, AtPw pw)
    {
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);
    return Vc1xBlockIdSet(vc1x, AtChannelIdGet((AtChannel)adapter), Vc1xBlockIdAllocate(vc1x));
    }

static eAtRet Vc1xiMsgBlockAllocate(AtChannel vc1x, AtHdlcChannel channel)
    {
    return Vc1xBlockIdSet(vc1x, AtChannelIdGet((AtChannel)channel) + IMsgStartBlockOffset(vc1x), Vc1xBlockIdAllocate(vc1x));
    }

static uint32 ExpectHoBlockId(uint8 sts12, uint8 slice12, uint8 slice24, uint8 slice48)
    {
    return (uint32) (sts12 * 8UL + slice12 * 4UL + slice24 *2UL + slice48);
    }

static void BlockSts24andSlice24ToSts12AndSlice12(uint8 sts24, uint8 *slice12, uint8 *sts12)
    {
    *slice12 = (uint8) (sts24 / 12);
    *sts12 = (uint8) (sts24 % 12);
    }

static uint8 Sts48FromChannel(AtSdhChannel vc)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet(vc);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    uint8 sts48 = AtSdhChannelSts1Get(vc);
    uint8 lineId;

    if (rate == cAtSdhLineRateStm16)
        {
        if (AtSdhChannelTypeGet(vc) == cAtSdhChannelTypeVc4_nc)
            return (uint8)(sts48 * 3);
        return sts48;
        }

    lineId = (uint8)AtChannelIdGet((AtChannel)line);
    return (uint8)(sts48 + (lineId * 12));
    }

static uint32 VcExpectBlockId(AtChannel vc)
    {
    uint8  slice48, sts48;
    uint8 slice24, sts24;
    uint8 slice12, sts12;
    AtSdhChannel channel = (AtSdhChannel) vc;

    ThaSdhChannel2HwMasterStsId(channel, cThaModuleOcn, &slice48, &sts48);
    sts48 = Sts48FromChannel(channel);

    /* Convert to Slice-24 & Sts-24 */
    slice24      = (uint8)(sts48 / 24);
    sts24        = (uint8)(sts48 % 24);

    /* Convert to Slice-12 & Sts-12 */
    BlockSts24andSlice24ToSts12AndSlice12(sts24, &slice12, &sts12);

    return ExpectHoBlockId(sts12, slice12, slice24, slice48);
    }

static uint32 De3LiuExpectBlockId(AtPdhChannel de3)
    {
    return (AtChannelIdGet((AtChannel)de3) + 48); /* Consider for this with 3G */
    }

static uint32 De3ExpectBlockId(AtChannel de3)
    {
    AtChannel vc = (AtChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de3);
    if (vc)
        return VcExpectBlockId((AtChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de3));

    return De3LiuExpectBlockId((AtPdhChannel)de3);
    }
    
static uint32 VcHwBlockType(AtSdhChannel channel)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(channel);

    if (vcType == cAtSdhChannelTypeVc3)
        return 1;
    if (vcType == cAtSdhChannelTypeVc4)
        return 2;
    if (vcType == cAtSdhChannelTypeVc4_4c)
        return 3;
    if (vcType == cAtSdhChannelTypeVc4_16c)
        return 5;
    if ((vcType == cAtSdhChannelTypeVc4_nc) && (AtSdhChannelNumSts(channel) == 24))
        return 4;

    return 0;
    }

static Tha60210012ModuleConcate ModuleConcate(AtChannel channel)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    return (Tha60210012ModuleConcate)AtDeviceModuleGet(device, cAtModuleConcate);
    }

static uint32 De1VcatChannelBlockOffset(AtChannel channel)
    {
    uint8 mapStsId, slice, hwSts, hwVtg, hwVt;
    ThaPdhDe1HwIdGet((ThaPdhDe1)channel, cThaModuleDemap, &slice, &hwSts, &hwVtg, &hwVt);
    mapStsId = Tha60210012ModuleConcateHwStsSlice24ToMapStsId(ModuleConcate(channel), slice, hwSts);
    return  mapStsId * 32UL +  hwVtg * 4UL + hwVt;
    }

static uint32 Vc1xVcatChannelBlockOffset(AtChannel channel)
    {
    uint8 mapStsId, slice48, hwSts48, hwVtg, hwVt;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel) channel, cThaModuleOcn, &slice48, &hwSts48);
    hwVtg = AtSdhChannelTug2Get((AtSdhChannel) channel);
    hwVt = AtSdhChannelTu1xGet((AtSdhChannel) channel);
    mapStsId = Tha60210012ModuleConcateHwStsSlice48ToMapStsId(ModuleConcate(channel), slice48, hwSts48);

    return  mapStsId * 32UL +  hwVtg * 4UL + hwVt;
    }

static uint32 De3VcatChannelBlockOffset(AtChannel channel)
    {
    uint8 slice, hwSts,  mapStsId;
    ThaPdhChannelHwIdGet((AtPdhChannel)channel, cAtModulePdh,  &slice, &hwSts);
    mapStsId = Tha60210012ModuleConcateHwStsSlice24ToMapStsId(ModuleConcate(channel), slice, hwSts);
    return (uint32)(mapStsId * 32UL);
    }

static uint32 Sts1VcatChannelBlockOffset(AtChannel channel)
    {
    uint8 slice48, hwSts48,  sts96;
    ThaSdhChannel2HwMasterStsId((AtSdhChannel) channel, cThaModuleOcn, &slice48, &hwSts48);
    sts96 = Tha60210012ModuleConcateHwStsSlice48ToMapStsId(ModuleConcate(channel), slice48, hwSts48);
    return (uint32)(sts96 * 32UL);
    }

static eAtRet ChannelBlockDeallocate(AtChannel channel)
    {
    uint32 blockId = Tha60210012ChannelPlaBlockIdGet(channel);

    if (BlockIsValid(blockId))
        {
        ThaModulePwe pweModule = ModulePwe(channel);
        ThaBitMaskClearBit(BlockUsedMask(mThis(pweModule)), blockId);
        return Tha60210012ChannelPlaBlockIdSet(channel, cInvalidUint32);
        }

    return cAtErrorRsrcNoAvail;
    }

eAtRet Tha60210012PwCircuitNxDs0BlockAllocate(AtChannel self)
    {
    AtPw pw = AtChannelBoundPwGet(self);
    AtHdlcChannel hdlcChannel;

    /* CES/CEP application, circuit bound PW directly */
    if (pw)
        return NxDs0PwBlockAllocate(self, pw);

    hdlcChannel = (AtHdlcChannel)AtChannelBoundEncapChannelGet(self);
    if (hdlcChannel)
        return NxDs0iMsgBlockAllocate(self, hdlcChannel);

    mChannelError(self, cAtErrorRsrcNoAvail);
    }

eAtRet Tha60210012PwCircuitDe1BlockAllocate(AtChannel self)
    {
    AtPw pw = AtChannelBoundPwGet(self);
    AtHdlcChannel hdlcChannel;

    /* CES/CEP application, circuit bound PW directly */
    if (pw)
        return De1PwBlockAllocate(self, pw);

    hdlcChannel = (AtHdlcChannel)AtChannelBoundEncapChannelGet(self);
    if (hdlcChannel)
        return De1iMsgBlockAllocate(self, hdlcChannel);

    mChannelError(self, cAtErrorRsrcNoAvail);
    }

eAtRet Tha60210012PwCircuitVc1xBlockAllocate(AtChannel self)
    {
    AtPw pw = AtChannelBoundPwGet(self);
    AtHdlcChannel hdlcChannel;

    /* CES/CEP application, circuit bound PW directly */
    if (pw)
        return Vc1xPwBlockAllocate(self, pw);

    hdlcChannel = (AtHdlcChannel)AtChannelBoundEncapChannelGet(self);
    if (hdlcChannel)
        return Vc1xiMsgBlockAllocate(self, hdlcChannel);

    mChannelError(self, cAtErrorRsrcNoAvail);
    }

eAtRet Tha60210012PwCircuitDe3ExpectedBlockAssign(AtChannel self, uint32 expectedBlock)
    {
    AtEncapChannel channel;
    AtPw pw = AtChannelBoundPwGet(self);
    uint32 hwChannelId = cInvalidUint32;

    channel = AtChannelBoundEncapChannelGet(self);
    if (pw)
        hwChannelId = AtChannelIdGet((AtChannel)ThaPwAdapterGet(pw));
    else if (channel)
        hwChannelId = AtChannelIdGet((AtChannel)channel) + IMsgStartBlockOffset(self);

    if (hwChannelId == cInvalidUint32)
        mChannelError(self, cAtErrorRsrcNoAvail);

    if (!BlockIsValid(expectedBlock))
        return cAtErrorRsrcNoAvail;

    return HwBlockAssign(self, hwChannelId, cDe3BlockType, expectedBlock);
    }

eAtRet Tha60210012PwCircuitDe3BlockAllocate(AtChannel self)
    {
    return Tha60210012PwCircuitDe3ExpectedBlockAssign(self, De3ExpectBlockId(self));
    }

eAtRet Tha60210012PwCircuitSts1VcBlockAllocate(AtChannel vc)
    {
    const uint32 sts1BlockType = 1;
    AtEncapChannel channel;
    AtPw pw = AtChannelBoundPwGet(vc);
    uint32 hwChannelId = cInvalidUint32;
    uint32 blockId;

    channel = AtChannelBoundEncapChannelGet(vc);
    if (pw)
        hwChannelId = AtChannelIdGet((AtChannel)ThaPwAdapterGet(pw));
    else if (channel)
        hwChannelId = AtChannelIdGet((AtChannel)channel) + IMsgStartBlockOffset(vc);

    if (hwChannelId == cInvalidUint32)
        mChannelError(vc, cAtErrorRsrcNoAvail);

    blockId = VcExpectBlockId(vc);
    if (!BlockIsValid(blockId))
        return cAtErrorRsrcNoAvail;

    return HwBlockAssign(vc, hwChannelId, sts1BlockType, blockId);
    }

eAtRet Tha60210012PwCircuitHoVcBlockAllocate(AtChannel vc)
    {
    AtEncapChannel channel;
    AtPw pw = AtChannelBoundPwGet(vc);
    uint32 hwChannelId = cInvalidUint32;
    uint32 expectBlockId;

    channel = AtChannelBoundEncapChannelGet(vc);
    if (pw)
        hwChannelId = AtChannelIdGet((AtChannel)ThaPwAdapterGet(pw));
    else if (channel)
        hwChannelId = AtChannelIdGet((AtChannel)channel) + IMsgStartBlockOffset(vc);

    if (hwChannelId == cInvalidUint32)
        mChannelError(vc, cAtErrorRsrcNoAvail);

    expectBlockId = VcExpectBlockId(vc);
    if (BlockIsValid(expectBlockId))
        return HwBlockAssign(vc, hwChannelId, VcHwBlockType((AtSdhChannel)vc), expectBlockId);

    return cAtErrorRsrcNoAvail;
    }

eAtRet Tha60210012VcgVc1xMemberBlockAllocate(AtChannel self)
    {
    uint32 blockId = Vc1xBlockIdAllocate(self);
    uint32 hwChannelId = Vc1xVcatChannelBlockOffset(self);

    if (BlockIsValid(blockId))
        return HwBlockAssign(self, VcatStartBlockOffset(self) + hwChannelId, cDe1BlockType, blockId);

    return cAtErrorRsrcNoAvail;
    }

eAtRet Tha60210012VcgDe1MemberBlockAllocate(AtChannel de1)
    {
    uint32 blockId = De1BlockIdAllocate(de1);
    uint32 hwChannelId = De1VcatChannelBlockOffset(de1);

    if (BlockIsValid(blockId))
        return HwBlockAssign(de1, VcatStartBlockOffset(de1) + hwChannelId, cDe1BlockType, blockId);

    return cAtErrorRsrcNoAvail;
    }

eAtRet Tha60210012VcgDe3MemberBlockAllocate(AtChannel self)
    {
    uint32 blockId = De3ExpectBlockId(self);
    uint32 hwChannelId = De3VcatChannelBlockOffset(self);

    if (BlockIsValid(blockId))
        return HwBlockAssign(self, VcatStartBlockOffset(self) + hwChannelId, cDe3BlockType, blockId);

    return cAtErrorRsrcNoAvail;
    }

eAtRet Tha60210012VcgSts1MemberBlockAllocate(AtChannel self)
    {
    uint32 blockId = VcExpectBlockId(self);
    uint32 hwChannelId = Sts1VcatChannelBlockOffset(self);

    if (BlockIsValid(blockId))
        return HwBlockAssign(self, VcatStartBlockOffset(self) + hwChannelId, cSts1BlockType, blockId);

    return cAtErrorRsrcNoAvail;
    }

eAtRet Tha60210012ChannelBlockDeallocate(AtChannel channel)
    {
    if (channel)
        return ChannelBlockDeallocate(channel);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ChannelHwLoBlockDynamicAllocate(AtChannel channel, uint32 hwChannelId)
    {
    if (channel)
        {
        uint32 blockId = HwBlockIdAllocate(ModulePwe(channel), cInvalidUint32);
        if (BlockIsValid(blockId))
            return HwBlockAssign(channel, hwChannelId, cDe1BlockType, blockId);

        return cAtErrorRsrcNoAvail;
        }

    return cAtErrorNullPointer;
    }

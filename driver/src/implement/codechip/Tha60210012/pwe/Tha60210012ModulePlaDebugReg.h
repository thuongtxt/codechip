/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PLA
 * 
 * File        : Tha60210012ModulePlaDebugReg.h
 * 
 * Created Date: Apr 1, 2016
 *
 * Description : Debug registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEPLADEBUGREG_H_
#define _THA60210012MODULEPLADEBUGREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Low-Order clk155 Sticky
Reg Addr   :
Reg Formula: 0x0_0001 + $LoOc48Slice*32768 + $LoOc24Slice*8192
    Where  :
           + $LoOc48Slice(0-1): LO OC-48 slices, there are total 2xOC-48 slices for LO 5G
           + $LoOc24Slice(0-1): LO OC-24 slices, there are total 2xOC-24 slices per LO OC-48 slice
Reg Desc   :
This register is used to used to sticky some alarms for debug per OC-24 Lo-order @clk155.52 domain

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_lo_clk155_stk_Base
#define cAf6Reg_pla_lo_clk155_stk(LoOc48Slice, LoOc24Slice)           (0x00001UL+(LoOc48Slice)*32768UL+(LoOc24Slice)*8192UL)
#define cAf6Reg_pla_lo_clk155_stk_WidthVal                                                                  32
#define cAf6Reg_pla_lo_clk155_stk_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: PlaLo155OC24ConvErr
BitField Type: W2C
BitField Desc: OC24 convert clock Error
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24ConvErr_Bit_Start                                                 8
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24ConvErr_Bit_End                                                   8
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24ConvErr_Mask                                                  cBit8
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24ConvErr_Shift                                                     8
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24ConvErr_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24ConvErr_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24ConvErr_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo155OC24InPwVld
BitField Type: W2C
BitField Desc: OC24 input of expected pwid
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPwVld_Bit_Start                                                 7
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPwVld_Bit_End                                                   7
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPwVld_Mask                                                  cBit7
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPwVld_Shift                                                     7
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPwVld_MaxVal                                                  0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPwVld_MinVal                                                  0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPwVld_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo155OC24InVld
BitField Type: W2C
BitField Desc: OC24 input data from demap
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InVld_Bit_Start                                                   6
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InVld_Bit_End                                                     6
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InVld_Mask                                                    cBit6
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InVld_Shift                                                       6
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InVld_MaxVal                                                    0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InVld_MinVal                                                    0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InVld_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaLo155OC24InFst
BitField Type: W2C
BitField Desc: OC24 input first-timeslot
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InFst_Bit_Start                                                   5
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InFst_Bit_End                                                     5
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InFst_Mask                                                    cBit5
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InFst_Shift                                                       5
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InFst_MaxVal                                                    0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InFst_MinVal                                                    0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InFst_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaLo155OC24InAIS
BitField Type: W2C
BitField Desc: OC24 input AIS
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InAIS_Bit_Start                                                   4
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InAIS_Bit_End                                                     4
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InAIS_Mask                                                    cBit4
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InAIS_Shift                                                       4
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InAIS_MaxVal                                                    0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InAIS_MinVal                                                    0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InAIS_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaLo155OC24InRDI
BitField Type: W2C
BitField Desc: OC24 input RDI
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InRDI_Bit_Start                                                   3
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InRDI_Bit_End                                                     3
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InRDI_Mask                                                    cBit3
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InRDI_Shift                                                       3
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InRDI_MaxVal                                                    0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InRDI_MinVal                                                    0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InRDI_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaLo155OC24InPos
BitField Type: W2C
BitField Desc: OC24 input CEP Pos Pointer
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPos_Bit_Start                                                   2
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPos_Bit_End                                                     2
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPos_Mask                                                    cBit2
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPos_Shift                                                       2
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPos_MaxVal                                                    0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPos_MinVal                                                    0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InPos_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaLo155OC24InNeg
BitField Type: W2C
BitField Desc: OC24 input CEP Neg Pointer
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InNeg_Bit_Start                                                   1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InNeg_Bit_End                                                     1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InNeg_Mask                                                    cBit1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InNeg_Shift                                                       1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InNeg_MaxVal                                                    0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InNeg_MinVal                                                    0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InNeg_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaLo155OC24InJ1
BitField Type: W2C
BitField Desc: OC24 input CEP J1
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InJ1_Bit_Start                                                    0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InJ1_Bit_End                                                      0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InJ1_Mask                                                     cBit0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InJ1_Shift                                                        0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InJ1_MaxVal                                                     0x1
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InJ1_MinVal                                                     0x0
#define cAf6_pla_lo_clk155_stk_PlaLo155OC24InJ1_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler High-Order clk311 Sticky
Reg Addr   :
Reg Formula: 0x1_0001 + $HoOc48Slice*256
    Where  :
           + $HoOc48Slice(0-1): HO OC-48 slices, there are total 2xOC-48 slices per HO OC-96 slice
Reg Desc   :
This register is used to used to sticky some alarms for debug per OC-48 Ho-order @clk311.04 domain

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_ho_clk311_stk_Base
#define cAf6Reg_pla_ho_clk311_stk(HoOc48Slice)                                 (0x10001UL+(HoOc48Slice)*256UL)
#define cAf6Reg_pla_ho_clk311_stk_WidthVal                                                                  32
#define cAf6Reg_pla_ho_clk311_stk_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: PlaLo311OC24ConvErr
BitField Type: W2C
BitField Desc: OC48 convert clock Error
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24ConvErr_Bit_Start                                                 8
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24ConvErr_Bit_End                                                   8
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24ConvErr_Mask                                                  cBit8
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24ConvErr_Shift                                                     8
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24ConvErr_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24ConvErr_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24ConvErr_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo311OC24InPwVld
BitField Type: W2C
BitField Desc: OC48 input of expected pwid
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPwVld_Bit_Start                                                 7
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPwVld_Bit_End                                                   7
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPwVld_Mask                                                  cBit7
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPwVld_Shift                                                     7
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPwVld_MaxVal                                                  0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPwVld_MinVal                                                  0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPwVld_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaLo311OC24InVld
BitField Type: W2C
BitField Desc: OC4824 input data from demap
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InVld_Bit_Start                                                   6
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InVld_Bit_End                                                     6
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InVld_Mask                                                    cBit6
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InVld_Shift                                                       6
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InVld_MaxVal                                                    0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InVld_MinVal                                                    0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InVld_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaLo311OC24InAIS
BitField Type: W2C
BitField Desc: OC24 input AIS
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InAIS_Bit_Start                                                   4
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InAIS_Bit_End                                                     4
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InAIS_Mask                                                    cBit4
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InAIS_Shift                                                       4
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InAIS_MaxVal                                                    0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InAIS_MinVal                                                    0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InAIS_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaLo311OC24InPos
BitField Type: W2C
BitField Desc: OC48 input CEP Pos Pointer
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPos_Bit_Start                                                   2
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPos_Bit_End                                                     2
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPos_Mask                                                    cBit2
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPos_Shift                                                       2
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPos_MaxVal                                                    0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPos_MinVal                                                    0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InPos_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaLo311OC24InNeg
BitField Type: W2C
BitField Desc: OC48 input CEP Neg Pointer
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InNeg_Bit_Start                                                   1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InNeg_Bit_End                                                     1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InNeg_Mask                                                    cBit1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InNeg_Shift                                                       1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InNeg_MaxVal                                                    0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InNeg_MinVal                                                    0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InNeg_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaLo311OC24InJ1
BitField Type: W2C
BitField Desc: OC48 input CEP J1
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InJ1_Bit_Start                                                    0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InJ1_Bit_End                                                      0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InJ1_Mask                                                     cBit0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InJ1_Shift                                                        0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InJ1_MaxVal                                                     0x1
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InJ1_MinVal                                                     0x0
#define cAf6_pla_ho_clk311_stk_PlaLo311OC24InJ1_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Interface#1 Sticky
Reg Addr   : 0x2_1010
Reg Formula:
    Where  :
Reg Desc   :
This register is used to used to sticky some intefrace of PLA

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_interface1_stk_Base                                                                0x21010
#define cAf6Reg_pla_interface1_stk                                                                   0x21010UL
#define cAf6Reg_pla_interface1_stk_WidthVal                                                                 32
#define cAf6Reg_pla_interface1_stk_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: PlaOutPWEFlowVld
BitField Type: W2C
BitField Desc: Output PWE expected Flow
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutPWEFlowVld_Bit_Start                                                  31
#define cAf6_pla_interface1_stk_PlaOutPWEFlowVld_Bit_End                                                    31
#define cAf6_pla_interface1_stk_PlaOutPWEFlowVld_Mask                                                   cBit31
#define cAf6_pla_interface1_stk_PlaOutPWEFlowVld_Shift                                                      31
#define cAf6_pla_interface1_stk_PlaOutPWEFlowVld_MaxVal                                                    0x1
#define cAf6_pla_interface1_stk_PlaOutPWEFlowVld_MinVal                                                    0x0
#define cAf6_pla_interface1_stk_PlaOutPWEFlowVld_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaOutPWEReq
BitField Type: W2C
BitField Desc: Output Request to PWE
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutPWEReq_Bit_Start                                                      30
#define cAf6_pla_interface1_stk_PlaOutPWEReq_Bit_End                                                        30
#define cAf6_pla_interface1_stk_PlaOutPWEReq_Mask                                                       cBit30
#define cAf6_pla_interface1_stk_PlaOutPWEReq_Shift                                                          30
#define cAf6_pla_interface1_stk_PlaOutPWEReq_MaxVal                                                        0x1
#define cAf6_pla_interface1_stk_PlaOutPWEReq_MinVal                                                        0x0
#define cAf6_pla_interface1_stk_PlaOutPWEReq_RstVal                                                        0x0

/*--------------------------------------
BitField Name: PlaInPWEGet
BitField Type: W2C
BitField Desc: Input PWE get data
BitField Bits: [29]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInPWEGet_Bit_Start                                                       29
#define cAf6_pla_interface1_stk_PlaInPWEGet_Bit_End                                                         29
#define cAf6_pla_interface1_stk_PlaInPWEGet_Mask                                                        cBit29
#define cAf6_pla_interface1_stk_PlaInPWEGet_Shift                                                           29
#define cAf6_pla_interface1_stk_PlaInPWEGet_MaxVal                                                         0x1
#define cAf6_pla_interface1_stk_PlaInPWEGet_MinVal                                                         0x0
#define cAf6_pla_interface1_stk_PlaInPWEGet_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaOutPWEVld
BitField Type: W2C
BitField Desc: Output Valid to PWE
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutPWEVld_Bit_Start                                                      28
#define cAf6_pla_interface1_stk_PlaOutPWEVld_Bit_End                                                        28
#define cAf6_pla_interface1_stk_PlaOutPWEVld_Mask                                                       cBit28
#define cAf6_pla_interface1_stk_PlaOutPWEVld_Shift                                                          28
#define cAf6_pla_interface1_stk_PlaOutPWEVld_MaxVal                                                        0x1
#define cAf6_pla_interface1_stk_PlaOutPWEVld_MinVal                                                        0x0
#define cAf6_pla_interface1_stk_PlaOutPWEVld_RstVal                                                        0x0

/*--------------------------------------
BitField Name: PlaInVldFromRSQ
BitField Type: W2C
BitField Desc: Input Info from RSQ
BitField Bits: [27]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVldFromRSQ_Bit_Start                                                   27
#define cAf6_pla_interface1_stk_PlaInVldFromRSQ_Bit_End                                                     27
#define cAf6_pla_interface1_stk_PlaInVldFromRSQ_Mask                                                    cBit27
#define cAf6_pla_interface1_stk_PlaInVldFromRSQ_Shift                                                       27
#define cAf6_pla_interface1_stk_PlaInVldFromRSQ_MaxVal                                                     0x1
#define cAf6_pla_interface1_stk_PlaInVldFromRSQ_MinVal                                                     0x0
#define cAf6_pla_interface1_stk_PlaInVldFromRSQ_RstVal                                                     0x0

/*--------------------------------------
BitField Name: PlaOutEn2RSQ
BitField Type: W2C
BitField Desc: Output Enable to RSQ
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutEn2RSQ_Bit_Start                                                      26
#define cAf6_pla_interface1_stk_PlaOutEn2RSQ_Bit_End                                                        26
#define cAf6_pla_interface1_stk_PlaOutEn2RSQ_Mask                                                       cBit26
#define cAf6_pla_interface1_stk_PlaOutEn2RSQ_Shift                                                          26
#define cAf6_pla_interface1_stk_PlaOutEn2RSQ_MaxVal                                                        0x1
#define cAf6_pla_interface1_stk_PlaOutEn2RSQ_MinVal                                                        0x0
#define cAf6_pla_interface1_stk_PlaOutEn2RSQ_RstVal                                                        0x0

/*--------------------------------------
BitField Name: PlaOutVld2RSQ
BitField Type: W2C
BitField Desc: Output Info to RSQ
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutVld2RSQ_Bit_Start                                                     25
#define cAf6_pla_interface1_stk_PlaOutVld2RSQ_Bit_End                                                       25
#define cAf6_pla_interface1_stk_PlaOutVld2RSQ_Mask                                                      cBit25
#define cAf6_pla_interface1_stk_PlaOutVld2RSQ_Shift                                                         25
#define cAf6_pla_interface1_stk_PlaOutVld2RSQ_MaxVal                                                       0x1
#define cAf6_pla_interface1_stk_PlaOutVld2RSQ_MinVal                                                       0x0
#define cAf6_pla_interface1_stk_PlaOutVld2RSQ_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaInMSGErr
BitField Type: W2C
BitField Desc: Input MSG Error
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInMSGErr_Bit_Start                                                       24
#define cAf6_pla_interface1_stk_PlaInMSGErr_Bit_End                                                         24
#define cAf6_pla_interface1_stk_PlaInMSGErr_Mask                                                        cBit24
#define cAf6_pla_interface1_stk_PlaInMSGErr_Shift                                                           24
#define cAf6_pla_interface1_stk_PlaInMSGErr_MaxVal                                                         0x1
#define cAf6_pla_interface1_stk_PlaInMSGErr_MinVal                                                         0x0
#define cAf6_pla_interface1_stk_PlaInMSGErr_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaInMSGSoP
BitField Type: W2C
BitField Desc: Input MSG Start Packet
BitField Bits: [23]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInMSGSoP_Bit_Start                                                       23
#define cAf6_pla_interface1_stk_PlaInMSGSoP_Bit_End                                                         23
#define cAf6_pla_interface1_stk_PlaInMSGSoP_Mask                                                        cBit23
#define cAf6_pla_interface1_stk_PlaInMSGSoP_Shift                                                           23
#define cAf6_pla_interface1_stk_PlaInMSGSoP_MaxVal                                                         0x1
#define cAf6_pla_interface1_stk_PlaInMSGSoP_MinVal                                                         0x0
#define cAf6_pla_interface1_stk_PlaInMSGSoP_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaInMSGEoP
BitField Type: W2C
BitField Desc: Input MSG End Packet
BitField Bits: [22]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInMSGEoP_Bit_Start                                                       22
#define cAf6_pla_interface1_stk_PlaInMSGEoP_Bit_End                                                         22
#define cAf6_pla_interface1_stk_PlaInMSGEoP_Mask                                                        cBit22
#define cAf6_pla_interface1_stk_PlaInMSGEoP_Shift                                                           22
#define cAf6_pla_interface1_stk_PlaInMSGEoP_MaxVal                                                         0x1
#define cAf6_pla_interface1_stk_PlaInMSGEoP_MinVal                                                         0x0
#define cAf6_pla_interface1_stk_PlaInMSGEoP_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaInMSGLidVld
BitField Type: W2C
BitField Desc: Input MSG expected Lid
BitField Bits: [21]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInMSGLidVld_Bit_Start                                                    21
#define cAf6_pla_interface1_stk_PlaInMSGLidVld_Bit_End                                                      21
#define cAf6_pla_interface1_stk_PlaInMSGLidVld_Mask                                                     cBit21
#define cAf6_pla_interface1_stk_PlaInMSGLidVld_Shift                                                        21
#define cAf6_pla_interface1_stk_PlaInMSGLidVld_MaxVal                                                      0x1
#define cAf6_pla_interface1_stk_PlaInMSGLidVld_MinVal                                                      0x0
#define cAf6_pla_interface1_stk_PlaInMSGLidVld_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaInMSGVld
BitField Type: W2C
BitField Desc: Input MSG Valid
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInMSGVld_Bit_Start                                                       20
#define cAf6_pla_interface1_stk_PlaInMSGVld_Bit_End                                                         20
#define cAf6_pla_interface1_stk_PlaInMSGVld_Mask                                                        cBit20
#define cAf6_pla_interface1_stk_PlaInMSGVld_Shift                                                           20
#define cAf6_pla_interface1_stk_PlaInMSGVld_MaxVal                                                         0x1
#define cAf6_pla_interface1_stk_PlaInMSGVld_MinVal                                                         0x0
#define cAf6_pla_interface1_stk_PlaInMSGVld_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaInVCGSoP
BitField Type: W2C
BitField Desc: Input VCG Start Packet
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVCGSoP_Bit_Start                                                       19
#define cAf6_pla_interface1_stk_PlaInVCGSoP_Bit_End                                                         19
#define cAf6_pla_interface1_stk_PlaInVCGSoP_Mask                                                        cBit19
#define cAf6_pla_interface1_stk_PlaInVCGSoP_Shift                                                           19
#define cAf6_pla_interface1_stk_PlaInVCGSoP_MaxVal                                                         0x1
#define cAf6_pla_interface1_stk_PlaInVCGSoP_MinVal                                                         0x0
#define cAf6_pla_interface1_stk_PlaInVCGSoP_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaInVCGEoP
BitField Type: W2C
BitField Desc: Input VCG End Packet
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVCGEoP_Bit_Start                                                       18
#define cAf6_pla_interface1_stk_PlaInVCGEoP_Bit_End                                                         18
#define cAf6_pla_interface1_stk_PlaInVCGEoP_Mask                                                        cBit18
#define cAf6_pla_interface1_stk_PlaInVCGEoP_Shift                                                           18
#define cAf6_pla_interface1_stk_PlaInVCGEoP_MaxVal                                                         0x1
#define cAf6_pla_interface1_stk_PlaInVCGEoP_MinVal                                                         0x0
#define cAf6_pla_interface1_stk_PlaInVCGEoP_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaInVCGLidVld
BitField Type: W2C
BitField Desc: Input VCG expected Lid
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVCGLidVld_Bit_Start                                                    17
#define cAf6_pla_interface1_stk_PlaInVCGLidVld_Bit_End                                                      17
#define cAf6_pla_interface1_stk_PlaInVCGLidVld_Mask                                                     cBit17
#define cAf6_pla_interface1_stk_PlaInVCGLidVld_Shift                                                        17
#define cAf6_pla_interface1_stk_PlaInVCGLidVld_MaxVal                                                      0x1
#define cAf6_pla_interface1_stk_PlaInVCGLidVld_MinVal                                                      0x0
#define cAf6_pla_interface1_stk_PlaInVCGLidVld_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaInVCGVld
BitField Type: W2C
BitField Desc: Input VCG Valid
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVCGVld_Bit_Start                                                       16
#define cAf6_pla_interface1_stk_PlaInVCGVld_Bit_End                                                         16
#define cAf6_pla_interface1_stk_PlaInVCGVld_Mask                                                        cBit16
#define cAf6_pla_interface1_stk_PlaInVCGVld_Shift                                                           16
#define cAf6_pla_interface1_stk_PlaInVCGVld_MaxVal                                                         0x1
#define cAf6_pla_interface1_stk_PlaInVCGVld_MinVal                                                         0x0
#define cAf6_pla_interface1_stk_PlaInVCGVld_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaInVCGErr
BitField Type: W2C
BitField Desc: Input VCG Error
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVCGErr_Bit_Start                                                       15
#define cAf6_pla_interface1_stk_PlaInVCGErr_Bit_End                                                         15
#define cAf6_pla_interface1_stk_PlaInVCGErr_Mask                                                        cBit15
#define cAf6_pla_interface1_stk_PlaInVCGErr_Shift                                                           15
#define cAf6_pla_interface1_stk_PlaInVCGErr_MaxVal                                                         0x1
#define cAf6_pla_interface1_stk_PlaInVCGErr_MinVal                                                         0x0
#define cAf6_pla_interface1_stk_PlaInVCGErr_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaOutVcatPkLidVld
BitField Type: W2C
BitField Desc: Output VCAT Pkt expected Lid
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutVcatPkLidVld_Bit_Start                                                13
#define cAf6_pla_interface1_stk_PlaOutVcatPkLidVld_Bit_End                                                  13
#define cAf6_pla_interface1_stk_PlaOutVcatPkLidVld_Mask                                                 cBit13
#define cAf6_pla_interface1_stk_PlaOutVcatPkLidVld_Shift                                                    13
#define cAf6_pla_interface1_stk_PlaOutVcatPkLidVld_MaxVal                                                  0x1
#define cAf6_pla_interface1_stk_PlaOutVcatPkLidVld_MinVal                                                  0x0
#define cAf6_pla_interface1_stk_PlaOutVcatPkLidVld_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaOutVcatPkVld
BitField Type: W2C
BitField Desc: Output VCAT Pkt Vld
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutVcatPkVld_Bit_Start                                                   12
#define cAf6_pla_interface1_stk_PlaOutVcatPkVld_Bit_End                                                     12
#define cAf6_pla_interface1_stk_PlaOutVcatPkVld_Mask                                                    cBit12
#define cAf6_pla_interface1_stk_PlaOutVcatPkVld_Shift                                                       12
#define cAf6_pla_interface1_stk_PlaOutVcatPkVld_MaxVal                                                     0x1
#define cAf6_pla_interface1_stk_PlaOutVcatPkVld_MinVal                                                     0x0
#define cAf6_pla_interface1_stk_PlaOutVcatPkVld_RstVal                                                     0x0

/*--------------------------------------
BitField Name: PlaOutVcatPkEn
BitField Type: W2C
BitField Desc: Output VCAT Pkt Enable
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutVcatPkEn_Bit_Start                                                    10
#define cAf6_pla_interface1_stk_PlaOutVcatPkEn_Bit_End                                                      10
#define cAf6_pla_interface1_stk_PlaOutVcatPkEn_Mask                                                     cBit10
#define cAf6_pla_interface1_stk_PlaOutVcatPkEn_Shift                                                        10
#define cAf6_pla_interface1_stk_PlaOutVcatPkEn_MaxVal                                                      0x1
#define cAf6_pla_interface1_stk_PlaOutVcatPkEn_MinVal                                                      0x0
#define cAf6_pla_interface1_stk_PlaOutVcatPkEn_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaInVcatPkLidVld
BitField Type: W2C
BitField Desc: Input VCAT Pkt expected Lid
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVcatPkLidVld_Bit_Start                                                  9
#define cAf6_pla_interface1_stk_PlaInVcatPkLidVld_Bit_End                                                    9
#define cAf6_pla_interface1_stk_PlaInVcatPkLidVld_Mask                                                   cBit9
#define cAf6_pla_interface1_stk_PlaInVcatPkLidVld_Shift                                                      9
#define cAf6_pla_interface1_stk_PlaInVcatPkLidVld_MaxVal                                                   0x1
#define cAf6_pla_interface1_stk_PlaInVcatPkLidVld_MinVal                                                   0x0
#define cAf6_pla_interface1_stk_PlaInVcatPkLidVld_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PlaInVcatPkVld
BitField Type: W2C
BitField Desc: Input VCAT Pkt Valid
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVcatPkVld_Bit_Start                                                     8
#define cAf6_pla_interface1_stk_PlaInVcatPkVld_Bit_End                                                       8
#define cAf6_pla_interface1_stk_PlaInVcatPkVld_Mask                                                      cBit8
#define cAf6_pla_interface1_stk_PlaInVcatPkVld_Shift                                                         8
#define cAf6_pla_interface1_stk_PlaInVcatPkVld_MaxVal                                                      0x1
#define cAf6_pla_interface1_stk_PlaInVcatPkVld_MinVal                                                      0x0
#define cAf6_pla_interface1_stk_PlaInVcatPkVld_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaOutVCATTDMLidVld
BitField Type: W2C
BitField Desc: Output VCAT TDM expected Lid
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutVCATTDMLidVld_Bit_Start                                                5
#define cAf6_pla_interface1_stk_PlaOutVCATTDMLidVld_Bit_End                                                  5
#define cAf6_pla_interface1_stk_PlaOutVCATTDMLidVld_Mask                                                 cBit5
#define cAf6_pla_interface1_stk_PlaOutVCATTDMLidVld_Shift                                                    5
#define cAf6_pla_interface1_stk_PlaOutVCATTDMLidVld_MaxVal                                                 0x1
#define cAf6_pla_interface1_stk_PlaOutVCATTDMLidVld_MinVal                                                 0x0
#define cAf6_pla_interface1_stk_PlaOutVCATTDMLidVld_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaOutVcatTDMVld
BitField Type: W2C
BitField Desc: Output VCAT TDM Valid
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaOutVcatTDMVld_Bit_Start                                                   4
#define cAf6_pla_interface1_stk_PlaOutVcatTDMVld_Bit_End                                                     4
#define cAf6_pla_interface1_stk_PlaOutVcatTDMVld_Mask                                                    cBit4
#define cAf6_pla_interface1_stk_PlaOutVcatTDMVld_Shift                                                       4
#define cAf6_pla_interface1_stk_PlaOutVcatTDMVld_MaxVal                                                    0x1
#define cAf6_pla_interface1_stk_PlaOutVcatTDMVld_MinVal                                                    0x0
#define cAf6_pla_interface1_stk_PlaOutVcatTDMVld_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaInVcatTDMSoP
BitField Type: W2C
BitField Desc: Input VCAT TDM Start Packet
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVcatTDMSoP_Bit_Start                                                    3
#define cAf6_pla_interface1_stk_PlaInVcatTDMSoP_Bit_End                                                      3
#define cAf6_pla_interface1_stk_PlaInVcatTDMSoP_Mask                                                     cBit3
#define cAf6_pla_interface1_stk_PlaInVcatTDMSoP_Shift                                                        3
#define cAf6_pla_interface1_stk_PlaInVcatTDMSoP_MaxVal                                                     0x1
#define cAf6_pla_interface1_stk_PlaInVcatTDMSoP_MinVal                                                     0x0
#define cAf6_pla_interface1_stk_PlaInVcatTDMSoP_RstVal                                                     0x0

/*--------------------------------------
BitField Name: PlaInVcatTDMEoP
BitField Type: W2C
BitField Desc: Input VCAT TDM End Packet
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVcatTDMEoP_Bit_Start                                                    2
#define cAf6_pla_interface1_stk_PlaInVcatTDMEoP_Bit_End                                                      2
#define cAf6_pla_interface1_stk_PlaInVcatTDMEoP_Mask                                                     cBit2
#define cAf6_pla_interface1_stk_PlaInVcatTDMEoP_Shift                                                        2
#define cAf6_pla_interface1_stk_PlaInVcatTDMEoP_MaxVal                                                     0x1
#define cAf6_pla_interface1_stk_PlaInVcatTDMEoP_MinVal                                                     0x0
#define cAf6_pla_interface1_stk_PlaInVcatTDMEoP_RstVal                                                     0x0

/*--------------------------------------
BitField Name: PlaInVcatTDMLidVld
BitField Type: W2C
BitField Desc: Input VCAT TDM expected Lid
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVcatTDMLidVld_Bit_Start                                                 1
#define cAf6_pla_interface1_stk_PlaInVcatTDMLidVld_Bit_End                                                   1
#define cAf6_pla_interface1_stk_PlaInVcatTDMLidVld_Mask                                                  cBit1
#define cAf6_pla_interface1_stk_PlaInVcatTDMLidVld_Shift                                                     1
#define cAf6_pla_interface1_stk_PlaInVcatTDMLidVld_MaxVal                                                  0x1
#define cAf6_pla_interface1_stk_PlaInVcatTDMLidVld_MinVal                                                  0x0
#define cAf6_pla_interface1_stk_PlaInVcatTDMLidVld_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaInVcatTDMVld
BitField Type: W2C
BitField Desc: Input VCAT TDM Valid
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_interface1_stk_PlaInVcatTDMVld_Bit_Start                                                    0
#define cAf6_pla_interface1_stk_PlaInVcatTDMVld_Bit_End                                                      0
#define cAf6_pla_interface1_stk_PlaInVcatTDMVld_Mask                                                     cBit0
#define cAf6_pla_interface1_stk_PlaInVcatTDMVld_Shift                                                        0
#define cAf6_pla_interface1_stk_PlaInVcatTDMVld_MaxVal                                                     0x1
#define cAf6_pla_interface1_stk_PlaInVcatTDMVld_MinVal                                                     0x0
#define cAf6_pla_interface1_stk_PlaInVcatTDMVld_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler Interface#2 Sticky
Reg Addr   : 0x2_1011
Reg Formula:
    Where  :
Reg Desc   :
This register is used to used to sticky some intefrace of PLA

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_interface2_stk_Base                                                                0x21011
#define cAf6Reg_pla_interface2_stk                                                                   0x21011UL
#define cAf6Reg_pla_interface2_stk_WidthVal                                                                 32
#define cAf6Reg_pla_interface2_stk_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: PlaRdDDRVldFFErr
BitField Type: W2C
BitField Desc: ReadDDR VLD fifo err
BitField Bits: [27]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdDDRVldFFErr_Bit_Start                                                  27
#define cAf6_pla_interface2_stk_PlaRdDDRVldFFErr_Bit_End                                                    27
#define cAf6_pla_interface2_stk_PlaRdDDRVldFFErr_Mask                                                   cBit27
#define cAf6_pla_interface2_stk_PlaRdDDRVldFFErr_Shift                                                      27
#define cAf6_pla_interface2_stk_PlaRdDDRVldFFErr_MaxVal                                                    0x1
#define cAf6_pla_interface2_stk_PlaRdDDRVldFFErr_MinVal                                                    0x0
#define cAf6_pla_interface2_stk_PlaRdDDRVldFFErr_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaRdDDRAckFFErr
BitField Type: W2C
BitField Desc: ReadDDR ACK fifo err
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdDDRAckFFErr_Bit_Start                                                  26
#define cAf6_pla_interface2_stk_PlaRdDDRAckFFErr_Bit_End                                                    26
#define cAf6_pla_interface2_stk_PlaRdDDRAckFFErr_Mask                                                   cBit26
#define cAf6_pla_interface2_stk_PlaRdDDRAckFFErr_Shift                                                      26
#define cAf6_pla_interface2_stk_PlaRdDDRAckFFErr_MaxVal                                                    0x1
#define cAf6_pla_interface2_stk_PlaRdDDRAckFFErr_MinVal                                                    0x0
#define cAf6_pla_interface2_stk_PlaRdDDRAckFFErr_RstVal                                                    0x0

/*--------------------------------------
BitField Name: PlaRdDDRDatReqFFErr
BitField Type: W2C
BitField Desc: ReadDDR Data Req fifo err
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdDDRDatReqFFErr_Bit_Start                                               25
#define cAf6_pla_interface2_stk_PlaRdDDRDatReqFFErr_Bit_End                                                 25
#define cAf6_pla_interface2_stk_PlaRdDDRDatReqFFErr_Mask                                                cBit25
#define cAf6_pla_interface2_stk_PlaRdDDRDatReqFFErr_Shift                                                   25
#define cAf6_pla_interface2_stk_PlaRdDDRDatReqFFErr_MaxVal                                                 0x1
#define cAf6_pla_interface2_stk_PlaRdDDRDatReqFFErr_MinVal                                                 0x0
#define cAf6_pla_interface2_stk_PlaRdDDRDatReqFFErr_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaRdDDRVCATReqFFErr
BitField Type: W2C
BitField Desc: ReadDDR VCAT Req fifo err
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdDDRVCATReqFFErr_Bit_Start                                              24
#define cAf6_pla_interface2_stk_PlaRdDDRVCATReqFFErr_Bit_End                                                24
#define cAf6_pla_interface2_stk_PlaRdDDRVCATReqFFErr_Mask                                               cBit24
#define cAf6_pla_interface2_stk_PlaRdDDRVCATReqFFErr_Shift                                                  24
#define cAf6_pla_interface2_stk_PlaRdDDRVCATReqFFErr_MaxVal                                                0x1
#define cAf6_pla_interface2_stk_PlaRdDDRVCATReqFFErr_MinVal                                                0x0
#define cAf6_pla_interface2_stk_PlaRdDDRVCATReqFFErr_RstVal                                                0x0

/*--------------------------------------
BitField Name: PlaRdPortPWPktVldFFErr
BitField Type: W2C
BitField Desc: ReadPort PW pkt VLD fifo err
BitField Bits: [23]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdPortPWPktVldFFErr_Bit_Start                                            23
#define cAf6_pla_interface2_stk_PlaRdPortPWPktVldFFErr_Bit_End                                              23
#define cAf6_pla_interface2_stk_PlaRdPortPWPktVldFFErr_Mask                                             cBit23
#define cAf6_pla_interface2_stk_PlaRdPortPWPktVldFFErr_Shift                                                23
#define cAf6_pla_interface2_stk_PlaRdPortPWPktVldFFErr_MaxVal                                              0x1
#define cAf6_pla_interface2_stk_PlaRdPortPWPktVldFFErr_MinVal                                              0x0
#define cAf6_pla_interface2_stk_PlaRdPortPWPktVldFFErr_RstVal                                              0x0

/*--------------------------------------
BitField Name: PlaRdPortPktVldFFErr
BitField Type: W2C
BitField Desc: ReadPort pkt VLD fifo err
BitField Bits: [22]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdPortPktVldFFErr_Bit_Start                                              22
#define cAf6_pla_interface2_stk_PlaRdPortPktVldFFErr_Bit_End                                                22
#define cAf6_pla_interface2_stk_PlaRdPortPktVldFFErr_Mask                                               cBit22
#define cAf6_pla_interface2_stk_PlaRdPortPktVldFFErr_Shift                                                  22
#define cAf6_pla_interface2_stk_PlaRdPortPktVldFFErr_MaxVal                                                0x1
#define cAf6_pla_interface2_stk_PlaRdPortPktVldFFErr_MinVal                                                0x0
#define cAf6_pla_interface2_stk_PlaRdPortPktVldFFErr_RstVal                                                0x0

/*--------------------------------------
BitField Name: PlaRdPortDatFFErr
BitField Type: W2C
BitField Desc: ReadPort pkt data fifo err
BitField Bits: [21]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdPortDatFFErr_Bit_Start                                                 21
#define cAf6_pla_interface2_stk_PlaRdPortDatFFErr_Bit_End                                                   21
#define cAf6_pla_interface2_stk_PlaRdPortDatFFErr_Mask                                                  cBit21
#define cAf6_pla_interface2_stk_PlaRdPortDatFFErr_Shift                                                     21
#define cAf6_pla_interface2_stk_PlaRdPortDatFFErr_MaxVal                                                   0x1
#define cAf6_pla_interface2_stk_PlaRdPortDatFFErr_MinVal                                                   0x0
#define cAf6_pla_interface2_stk_PlaRdPortDatFFErr_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PlaRdPortInfFFErr
BitField Type: W2C
BitField Desc: ReadPort pkt info fifo err
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdPortInfFFErr_Bit_Start                                                 20
#define cAf6_pla_interface2_stk_PlaRdPortInfFFErr_Bit_End                                                   20
#define cAf6_pla_interface2_stk_PlaRdPortInfFFErr_Mask                                                  cBit20
#define cAf6_pla_interface2_stk_PlaRdPortInfFFErr_Shift                                                     20
#define cAf6_pla_interface2_stk_PlaRdPortInfFFErr_MaxVal                                                   0x1
#define cAf6_pla_interface2_stk_PlaRdPortInfFFErr_MinVal                                                   0x0
#define cAf6_pla_interface2_stk_PlaRdPortInfFFErr_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PlaRdBufPkFFErr
BitField Type: W2C
BitField Desc: ReadBuf pkt VLD fifo err
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdBufPkFFErr_Bit_Start                                                   19
#define cAf6_pla_interface2_stk_PlaRdBufPkFFErr_Bit_End                                                     19
#define cAf6_pla_interface2_stk_PlaRdBufPkFFErr_Mask                                                    cBit19
#define cAf6_pla_interface2_stk_PlaRdBufPkFFErr_Shift                                                       19
#define cAf6_pla_interface2_stk_PlaRdBufPkFFErr_MaxVal                                                     0x1
#define cAf6_pla_interface2_stk_PlaRdBufPkFFErr_MinVal                                                     0x0
#define cAf6_pla_interface2_stk_PlaRdBufPkFFErr_RstVal                                                     0x0

/*--------------------------------------
BitField Name: PlaRdBufPkInfFFErr
BitField Type: W2C
BitField Desc: ReadBuf pkt info fifo err
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdBufPkInfFFErr_Bit_Start                                                18
#define cAf6_pla_interface2_stk_PlaRdBufPkInfFFErr_Bit_End                                                  18
#define cAf6_pla_interface2_stk_PlaRdBufPkInfFFErr_Mask                                                 cBit18
#define cAf6_pla_interface2_stk_PlaRdBufPkInfFFErr_Shift                                                    18
#define cAf6_pla_interface2_stk_PlaRdBufPkInfFFErr_MaxVal                                                  0x1
#define cAf6_pla_interface2_stk_PlaRdBufPkInfFFErr_MinVal                                                  0x0
#define cAf6_pla_interface2_stk_PlaRdBufPkInfFFErr_RstVal                                                  0x0

/*--------------------------------------
BitField Name: PlaRdBufPSNAckFFErr
BitField Type: W2C
BitField Desc: ReadBuf PSN ACK fifo err
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdBufPSNAckFFErr_Bit_Start                                               17
#define cAf6_pla_interface2_stk_PlaRdBufPSNAckFFErr_Bit_End                                                 17
#define cAf6_pla_interface2_stk_PlaRdBufPSNAckFFErr_Mask                                                cBit17
#define cAf6_pla_interface2_stk_PlaRdBufPSNAckFFErr_Shift                                                   17
#define cAf6_pla_interface2_stk_PlaRdBufPSNAckFFErr_MaxVal                                                 0x1
#define cAf6_pla_interface2_stk_PlaRdBufPSNAckFFErr_MinVal                                                 0x0
#define cAf6_pla_interface2_stk_PlaRdBufPSNAckFFErr_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaRdBufPSNVldFFErr
BitField Type: W2C
BitField Desc: ReadBuf PSN VLD fifo err
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaRdBufPSNVldFFErr_Bit_Start                                               16
#define cAf6_pla_interface2_stk_PlaRdBufPSNVldFFErr_Bit_End                                                 16
#define cAf6_pla_interface2_stk_PlaRdBufPSNVldFFErr_Mask                                                cBit16
#define cAf6_pla_interface2_stk_PlaRdBufPSNVldFFErr_Shift                                                   16
#define cAf6_pla_interface2_stk_PlaRdBufPSNVldFFErr_MaxVal                                                 0x1
#define cAf6_pla_interface2_stk_PlaRdBufPSNVldFFErr_MinVal                                                 0x0
#define cAf6_pla_interface2_stk_PlaRdBufPSNVldFFErr_RstVal                                                 0x0

/*--------------------------------------
BitField Name: PlaPWCoreSoP
BitField Type: W2C
BitField Desc: Output PW Core Start Packet
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaPWCoreSoP_Bit_Start                                                      14
#define cAf6_pla_interface2_stk_PlaPWCoreSoP_Bit_End                                                        14
#define cAf6_pla_interface2_stk_PlaPWCoreSoP_Mask                                                       cBit14
#define cAf6_pla_interface2_stk_PlaPWCoreSoP_Shift                                                          14
#define cAf6_pla_interface2_stk_PlaPWCoreSoP_MaxVal                                                        0x1
#define cAf6_pla_interface2_stk_PlaPWCoreSoP_MinVal                                                        0x0
#define cAf6_pla_interface2_stk_PlaPWCoreSoP_RstVal                                                        0x0

/*--------------------------------------
BitField Name: PlaPWCoreEoP
BitField Type: W2C
BitField Desc: Output PW Core End Packet
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaPWCoreEoP_Bit_Start                                                      13
#define cAf6_pla_interface2_stk_PlaPWCoreEoP_Bit_End                                                        13
#define cAf6_pla_interface2_stk_PlaPWCoreEoP_Mask                                                       cBit13
#define cAf6_pla_interface2_stk_PlaPWCoreEoP_Shift                                                          13
#define cAf6_pla_interface2_stk_PlaPWCoreEoP_MaxVal                                                        0x1
#define cAf6_pla_interface2_stk_PlaPWCoreEoP_MinVal                                                        0x0
#define cAf6_pla_interface2_stk_PlaPWCoreEoP_RstVal                                                        0x0

/*--------------------------------------
BitField Name: PlaPWCoreVld
BitField Type: W2C
BitField Desc: Output PW Core Valid
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaPWCoreVld_Bit_Start                                                      12
#define cAf6_pla_interface2_stk_PlaPWCoreVld_Bit_End                                                        12
#define cAf6_pla_interface2_stk_PlaPWCoreVld_Mask                                                       cBit12
#define cAf6_pla_interface2_stk_PlaPWCoreVld_Shift                                                          12
#define cAf6_pla_interface2_stk_PlaPWCoreVld_MaxVal                                                        0x1
#define cAf6_pla_interface2_stk_PlaPWCoreVld_MinVal                                                        0x0
#define cAf6_pla_interface2_stk_PlaPWCoreVld_RstVal                                                        0x0

/*--------------------------------------
BitField Name: PlaInPSNVld
BitField Type: W2C
BitField Desc: Input PSN Vld
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaInPSNVld_Bit_Start                                                       10
#define cAf6_pla_interface2_stk_PlaInPSNVld_Bit_End                                                         10
#define cAf6_pla_interface2_stk_PlaInPSNVld_Mask                                                        cBit10
#define cAf6_pla_interface2_stk_PlaInPSNVld_Shift                                                           10
#define cAf6_pla_interface2_stk_PlaInPSNVld_MaxVal                                                         0x1
#define cAf6_pla_interface2_stk_PlaInPSNVld_MinVal                                                         0x0
#define cAf6_pla_interface2_stk_PlaInPSNVld_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaInPSNAck
BitField Type: W2C
BitField Desc: Input PSN Ack
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaInPSNAck_Bit_Start                                                        9
#define cAf6_pla_interface2_stk_PlaInPSNAck_Bit_End                                                          9
#define cAf6_pla_interface2_stk_PlaInPSNAck_Mask                                                         cBit9
#define cAf6_pla_interface2_stk_PlaInPSNAck_Shift                                                            9
#define cAf6_pla_interface2_stk_PlaInPSNAck_MaxVal                                                         0x1
#define cAf6_pla_interface2_stk_PlaInPSNAck_MinVal                                                         0x0
#define cAf6_pla_interface2_stk_PlaInPSNAck_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaOutPSNReq
BitField Type: W2C
BitField Desc: Output PSN Request
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaOutPSNReq_Bit_Start                                                       8
#define cAf6_pla_interface2_stk_PlaOutPSNReq_Bit_End                                                         8
#define cAf6_pla_interface2_stk_PlaOutPSNReq_Mask                                                        cBit8
#define cAf6_pla_interface2_stk_PlaOutPSNReq_Shift                                                           8
#define cAf6_pla_interface2_stk_PlaOutPSNReq_MaxVal                                                        0x1
#define cAf6_pla_interface2_stk_PlaOutPSNReq_MinVal                                                        0x0
#define cAf6_pla_interface2_stk_PlaOutPSNReq_RstVal                                                        0x0

/*--------------------------------------
BitField Name: PlaInDDRRdVld
BitField Type: W2C
BitField Desc: Input Read DDR Vld
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaInDDRRdVld_Bit_Start                                                      6
#define cAf6_pla_interface2_stk_PlaInDDRRdVld_Bit_End                                                        6
#define cAf6_pla_interface2_stk_PlaInDDRRdVld_Mask                                                       cBit6
#define cAf6_pla_interface2_stk_PlaInDDRRdVld_Shift                                                          6
#define cAf6_pla_interface2_stk_PlaInDDRRdVld_MaxVal                                                       0x1
#define cAf6_pla_interface2_stk_PlaInDDRRdVld_MinVal                                                       0x0
#define cAf6_pla_interface2_stk_PlaInDDRRdVld_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaInDDRRdAck
BitField Type: W2C
BitField Desc: Input Read DDR Ack
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaInDDRRdAck_Bit_Start                                                      5
#define cAf6_pla_interface2_stk_PlaInDDRRdAck_Bit_End                                                        5
#define cAf6_pla_interface2_stk_PlaInDDRRdAck_Mask                                                       cBit5
#define cAf6_pla_interface2_stk_PlaInDDRRdAck_Shift                                                          5
#define cAf6_pla_interface2_stk_PlaInDDRRdAck_MaxVal                                                       0x1
#define cAf6_pla_interface2_stk_PlaInDDRRdAck_MinVal                                                       0x0
#define cAf6_pla_interface2_stk_PlaInDDRRdAck_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaOutDDRRdReq
BitField Type: W2C
BitField Desc: Output Read DDR Request
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaOutDDRRdReq_Bit_Start                                                     4
#define cAf6_pla_interface2_stk_PlaOutDDRRdReq_Bit_End                                                       4
#define cAf6_pla_interface2_stk_PlaOutDDRRdReq_Mask                                                      cBit4
#define cAf6_pla_interface2_stk_PlaOutDDRRdReq_Shift                                                         4
#define cAf6_pla_interface2_stk_PlaOutDDRRdReq_MaxVal                                                      0x1
#define cAf6_pla_interface2_stk_PlaOutDDRRdReq_MinVal                                                      0x0
#define cAf6_pla_interface2_stk_PlaOutDDRRdReq_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaInDDRWrVld
BitField Type: W2C
BitField Desc: Input Write DDR Vld
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaInDDRWrVld_Bit_Start                                                      2
#define cAf6_pla_interface2_stk_PlaInDDRWrVld_Bit_End                                                        2
#define cAf6_pla_interface2_stk_PlaInDDRWrVld_Mask                                                       cBit2
#define cAf6_pla_interface2_stk_PlaInDDRWrVld_Shift                                                          2
#define cAf6_pla_interface2_stk_PlaInDDRWrVld_MaxVal                                                       0x1
#define cAf6_pla_interface2_stk_PlaInDDRWrVld_MinVal                                                       0x0
#define cAf6_pla_interface2_stk_PlaInDDRWrVld_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaInDDRWrAck
BitField Type: W2C
BitField Desc: Input Write DDR Ack
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaInDDRWrAck_Bit_Start                                                      1
#define cAf6_pla_interface2_stk_PlaInDDRWrAck_Bit_End                                                        1
#define cAf6_pla_interface2_stk_PlaInDDRWrAck_Mask                                                       cBit1
#define cAf6_pla_interface2_stk_PlaInDDRWrAck_Shift                                                          1
#define cAf6_pla_interface2_stk_PlaInDDRWrAck_MaxVal                                                       0x1
#define cAf6_pla_interface2_stk_PlaInDDRWrAck_MinVal                                                       0x0
#define cAf6_pla_interface2_stk_PlaInDDRWrAck_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaOutDDRWrReq
BitField Type: W2C
BitField Desc: Output Write DDR Request
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_interface2_stk_PlaOutDDRWrReq_Bit_Start                                                     0
#define cAf6_pla_interface2_stk_PlaOutDDRWrReq_Bit_End                                                       0
#define cAf6_pla_interface2_stk_PlaOutDDRWrReq_Mask                                                      cBit0
#define cAf6_pla_interface2_stk_PlaOutDDRWrReq_Shift                                                         0
#define cAf6_pla_interface2_stk_PlaOutDDRWrReq_MaxVal                                                      0x1
#define cAf6_pla_interface2_stk_PlaOutDDRWrReq_MinVal                                                      0x0
#define cAf6_pla_interface2_stk_PlaOutDDRWrReq_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler debug Sticky
Reg Addr   : 0x2_1012
Reg Formula:
    Where  :
Reg Desc   :
This register is used to used to sticky some debug info of PLA

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_debug1_stk_Base                                                                    0x21012
#define cAf6Reg_pla_debug1_stk                                                                       0x21012UL
#define cAf6Reg_pla_debug1_stk_WidthVal                                                                     32
#define cAf6Reg_pla_debug1_stk_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: PlaCRCCheckErr
BitField Type: W2C
BitField Desc: CRC check error
BitField Bits: [31]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaCRCCheckErr_Bit_Start                                                        31
#define cAf6_pla_debug1_stk_PlaCRCCheckErr_Bit_End                                                          31
#define cAf6_pla_debug1_stk_PlaCRCCheckErr_Mask                                                         cBit31
#define cAf6_pla_debug1_stk_PlaCRCCheckErr_Shift                                                            31
#define cAf6_pla_debug1_stk_PlaCRCCheckErr_MaxVal                                                          0x1
#define cAf6_pla_debug1_stk_PlaCRCCheckErr_MinVal                                                          0x0
#define cAf6_pla_debug1_stk_PlaCRCCheckErr_RstVal                                                          0x0

/*--------------------------------------
BitField Name: PlaWrCacheSameErr
BitField Type: W2C
BitField Desc: Write Cache same err
BitField Bits: [30]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaWrCacheSameErr_Bit_Start                                                     30
#define cAf6_pla_debug1_stk_PlaWrCacheSameErr_Bit_End                                                       30
#define cAf6_pla_debug1_stk_PlaWrCacheSameErr_Mask                                                      cBit30
#define cAf6_pla_debug1_stk_PlaWrCacheSameErr_Shift                                                         30
#define cAf6_pla_debug1_stk_PlaWrCacheSameErr_MaxVal                                                       0x1
#define cAf6_pla_debug1_stk_PlaWrCacheSameErr_MinVal                                                       0x0
#define cAf6_pla_debug1_stk_PlaWrCacheSameErr_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaWrCacheEmpErr
BitField Type: W2C
BitField Desc: Write Cache empty err
BitField Bits: [29]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaWrCacheEmpErr_Bit_Start                                                      29
#define cAf6_pla_debug1_stk_PlaWrCacheEmpErr_Bit_End                                                        29
#define cAf6_pla_debug1_stk_PlaWrCacheEmpErr_Mask                                                       cBit29
#define cAf6_pla_debug1_stk_PlaWrCacheEmpErr_Shift                                                          29
#define cAf6_pla_debug1_stk_PlaWrCacheEmpErr_MaxVal                                                        0x1
#define cAf6_pla_debug1_stk_PlaWrCacheEmpErr_MinVal                                                        0x0
#define cAf6_pla_debug1_stk_PlaWrCacheEmpErr_RstVal                                                        0x0

/*--------------------------------------
BitField Name: PlaWrCacheErr
BitField Type: W2C
BitField Desc: Write Cache err
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaWrCacheErr_Bit_Start                                                         28
#define cAf6_pla_debug1_stk_PlaWrCacheErr_Bit_End                                                           28
#define cAf6_pla_debug1_stk_PlaWrCacheErr_Mask                                                          cBit28
#define cAf6_pla_debug1_stk_PlaWrCacheErr_Shift                                                             28
#define cAf6_pla_debug1_stk_PlaWrCacheErr_MaxVal                                                           0x1
#define cAf6_pla_debug1_stk_PlaWrCacheErr_MinVal                                                           0x0
#define cAf6_pla_debug1_stk_PlaWrCacheErr_RstVal                                                           0x0

/*--------------------------------------
BitField Name: PlaEnqVCGBlkEmpErr
BitField Type: W2C
BitField Desc: Enque VCG Block empty err
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkEmpErr_Bit_Start                                                    26
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkEmpErr_Bit_End                                                      26
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkEmpErr_Mask                                                     cBit26
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkEmpErr_Shift                                                        26
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkEmpErr_MaxVal                                                      0x1
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkEmpErr_MinVal                                                      0x0
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkEmpErr_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PlaEnqVCGBlkSameErr
BitField Type: W2C
BitField Desc: Enque VCG Block same err
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkSameErr_Bit_Start                                                   25
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkSameErr_Bit_End                                                     25
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkSameErr_Mask                                                    cBit25
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkSameErr_Shift                                                       25
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkSameErr_MaxVal                                                     0x1
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkSameErr_MinVal                                                     0x0
#define cAf6_pla_debug1_stk_PlaEnqVCGBlkSameErr_RstVal                                                     0x0

/*--------------------------------------
BitField Name: PlaWrDDRACKFFErr
BitField Type: W2C
BitField Desc: WriteDDR ACK fifo err
BitField Bits: [23]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaWrDDRACKFFErr_Bit_Start                                                      23
#define cAf6_pla_debug1_stk_PlaWrDDRACKFFErr_Bit_End                                                        23
#define cAf6_pla_debug1_stk_PlaWrDDRACKFFErr_Mask                                                       cBit23
#define cAf6_pla_debug1_stk_PlaWrDDRACKFFErr_Shift                                                          23
#define cAf6_pla_debug1_stk_PlaWrDDRACKFFErr_MaxVal                                                        0x1
#define cAf6_pla_debug1_stk_PlaWrDDRACKFFErr_MinVal                                                        0x0
#define cAf6_pla_debug1_stk_PlaWrDDRACKFFErr_RstVal                                                        0x0

/*--------------------------------------
BitField Name: PlaEnqBlkCfgErr
BitField Type: W2C
BitField Desc: Enque Block Cfg err
BitField Bits: [22]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaEnqBlkCfgErr_Bit_Start                                                       22
#define cAf6_pla_debug1_stk_PlaEnqBlkCfgErr_Bit_End                                                         22
#define cAf6_pla_debug1_stk_PlaEnqBlkCfgErr_Mask                                                        cBit22
#define cAf6_pla_debug1_stk_PlaEnqBlkCfgErr_Shift                                                           22
#define cAf6_pla_debug1_stk_PlaEnqBlkCfgErr_MaxVal                                                         0x1
#define cAf6_pla_debug1_stk_PlaEnqBlkCfgErr_MinVal                                                         0x0
#define cAf6_pla_debug1_stk_PlaEnqBlkCfgErr_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaWrDDRVldRdyFFErr
BitField Type: W2C
BitField Desc: WriteDDR VLD ready fifo err
BitField Bits: [21]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaWrDDRVldRdyFFErr_Bit_Start                                                   21
#define cAf6_pla_debug1_stk_PlaWrDDRVldRdyFFErr_Bit_End                                                     21
#define cAf6_pla_debug1_stk_PlaWrDDRVldRdyFFErr_Mask                                                    cBit21
#define cAf6_pla_debug1_stk_PlaWrDDRVldRdyFFErr_Shift                                                       21
#define cAf6_pla_debug1_stk_PlaWrDDRVldRdyFFErr_MaxVal                                                     0x1
#define cAf6_pla_debug1_stk_PlaWrDDRVldRdyFFErr_MinVal                                                     0x0
#define cAf6_pla_debug1_stk_PlaWrDDRVldRdyFFErr_RstVal                                                     0x0

/*--------------------------------------
BitField Name: PlaWrDDRVldFFErr
BitField Type: W2C
BitField Desc: WriteDDR VLD fifo err
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaWrDDRVldFFErr_Bit_Start                                                      20
#define cAf6_pla_debug1_stk_PlaWrDDRVldFFErr_Bit_End                                                        20
#define cAf6_pla_debug1_stk_PlaWrDDRVldFFErr_Mask                                                       cBit20
#define cAf6_pla_debug1_stk_PlaWrDDRVldFFErr_Shift                                                          20
#define cAf6_pla_debug1_stk_PlaWrDDRVldFFErr_MaxVal                                                        0x1
#define cAf6_pla_debug1_stk_PlaWrDDRVldFFErr_MinVal                                                        0x0
#define cAf6_pla_debug1_stk_PlaWrDDRVldFFErr_RstVal                                                        0x0

/*--------------------------------------
BitField Name: PlaMuxPWCoreHOErr
BitField Type: W2C
BitField Desc: Mux PW Core HO err
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxPWCoreHOErr_Bit_Start                                                     17
#define cAf6_pla_debug1_stk_PlaMuxPWCoreHOErr_Bit_End                                                       17
#define cAf6_pla_debug1_stk_PlaMuxPWCoreHOErr_Mask                                                      cBit17
#define cAf6_pla_debug1_stk_PlaMuxPWCoreHOErr_Shift                                                         17
#define cAf6_pla_debug1_stk_PlaMuxPWCoreHOErr_MaxVal                                                       0x1
#define cAf6_pla_debug1_stk_PlaMuxPWCoreHOErr_MinVal                                                       0x0
#define cAf6_pla_debug1_stk_PlaMuxPWCoreHOErr_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaMuxPWCoreLOErr
BitField Type: W2C
BitField Desc: Mux PW Core LO err
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxPWCoreLOErr_Bit_Start                                                     16
#define cAf6_pla_debug1_stk_PlaMuxPWCoreLOErr_Bit_End                                                       16
#define cAf6_pla_debug1_stk_PlaMuxPWCoreLOErr_Mask                                                      cBit16
#define cAf6_pla_debug1_stk_PlaMuxPWCoreLOErr_Shift                                                         16
#define cAf6_pla_debug1_stk_PlaMuxPWCoreLOErr_MaxVal                                                       0x1
#define cAf6_pla_debug1_stk_PlaMuxPWCoreLOErr_MinVal                                                       0x0
#define cAf6_pla_debug1_stk_PlaMuxPWCoreLOErr_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PlaMuxPWLOOC48_2Err
BitField Type: W2C
BitField Desc: Mux PW LO OC48_2 err
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_2Err_Bit_Start                                                   13
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_2Err_Bit_End                                                     13
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_2Err_Mask                                                    cBit13
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_2Err_Shift                                                       13
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_2Err_MaxVal                                                     0x1
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_2Err_MinVal                                                     0x0
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_2Err_RstVal                                                     0x0

/*--------------------------------------
BitField Name: PlaMuxPWLOOC48_1Err
BitField Type: W2C
BitField Desc: Mux PW LO OC48_1 err
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_1Err_Bit_Start                                                   12
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_1Err_Bit_End                                                     12
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_1Err_Mask                                                    cBit12
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_1Err_Shift                                                       12
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_1Err_MaxVal                                                     0x1
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_1Err_MinVal                                                     0x0
#define cAf6_pla_debug1_stk_PlaMuxPWLOOC48_1Err_RstVal                                                     0x0

/*--------------------------------------
BitField Name: PlaMuxSrcIn3Err
BitField Type: W2C
BitField Desc: Mux VCG input2 MSG err
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxSrcIn3Err_Bit_Start                                                        6
#define cAf6_pla_debug1_stk_PlaMuxSrcIn3Err_Bit_End                                                          6
#define cAf6_pla_debug1_stk_PlaMuxSrcIn3Err_Mask                                                         cBit6
#define cAf6_pla_debug1_stk_PlaMuxSrcIn3Err_Shift                                                            6
#define cAf6_pla_debug1_stk_PlaMuxSrcIn3Err_MaxVal                                                         0x1
#define cAf6_pla_debug1_stk_PlaMuxSrcIn3Err_MinVal                                                         0x0
#define cAf6_pla_debug1_stk_PlaMuxSrcIn3Err_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaMuxSrcIn2Err
BitField Type: W2C
BitField Desc: Mux VCG input2 VCAT err
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxSrcIn2Err_Bit_Start                                                        5
#define cAf6_pla_debug1_stk_PlaMuxSrcIn2Err_Bit_End                                                          5
#define cAf6_pla_debug1_stk_PlaMuxSrcIn2Err_Mask                                                         cBit5
#define cAf6_pla_debug1_stk_PlaMuxSrcIn2Err_Shift                                                            5
#define cAf6_pla_debug1_stk_PlaMuxSrcIn2Err_MaxVal                                                         0x1
#define cAf6_pla_debug1_stk_PlaMuxSrcIn2Err_MinVal                                                         0x0
#define cAf6_pla_debug1_stk_PlaMuxSrcIn2Err_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaMuxSrcIn1Err
BitField Type: W2C
BitField Desc: Mux VCG input1 PW err
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxSrcIn1Err_Bit_Start                                                        4
#define cAf6_pla_debug1_stk_PlaMuxSrcIn1Err_Bit_End                                                          4
#define cAf6_pla_debug1_stk_PlaMuxSrcIn1Err_Mask                                                         cBit4
#define cAf6_pla_debug1_stk_PlaMuxSrcIn1Err_Shift                                                            4
#define cAf6_pla_debug1_stk_PlaMuxSrcIn1Err_MaxVal                                                         0x1
#define cAf6_pla_debug1_stk_PlaMuxSrcIn1Err_MinVal                                                         0x0
#define cAf6_pla_debug1_stk_PlaMuxSrcIn1Err_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaMuxVCGIn2Err
BitField Type: W2C
BitField Desc: Mux VCG input2 VCG err
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxVCGIn2Err_Bit_Start                                                        1
#define cAf6_pla_debug1_stk_PlaMuxVCGIn2Err_Bit_End                                                          1
#define cAf6_pla_debug1_stk_PlaMuxVCGIn2Err_Mask                                                         cBit1
#define cAf6_pla_debug1_stk_PlaMuxVCGIn2Err_Shift                                                            1
#define cAf6_pla_debug1_stk_PlaMuxVCGIn2Err_MaxVal                                                         0x1
#define cAf6_pla_debug1_stk_PlaMuxVCGIn2Err_MinVal                                                         0x0
#define cAf6_pla_debug1_stk_PlaMuxVCGIn2Err_RstVal                                                         0x0

/*--------------------------------------
BitField Name: PlaMuxVCGIn1Err
BitField Type: W2C
BitField Desc: Mux VCG input1 PW err
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_debug1_stk_PlaMuxVCGIn1Err_Bit_Start                                                        0
#define cAf6_pla_debug1_stk_PlaMuxVCGIn1Err_Bit_End                                                          0
#define cAf6_pla_debug1_stk_PlaMuxVCGIn1Err_Mask                                                         cBit0
#define cAf6_pla_debug1_stk_PlaMuxVCGIn1Err_Shift                                                            0
#define cAf6_pla_debug1_stk_PlaMuxVCGIn1Err_MaxVal                                                         0x1
#define cAf6_pla_debug1_stk_PlaMuxVCGIn1Err_MinVal                                                         0x0
#define cAf6_pla_debug1_stk_PlaMuxVCGIn1Err_RstVal                                                         0x0

/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler debug Sticky#2
Reg Addr   : 0x2_1013
Reg Formula:
    Where  :
Reg Desc   :
This register is used to used to sticky some debug info of PLA

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_debug2_stk_Base                                                                    0x21013
#define cAf6Reg_pla_debug2_stk                                                                       0x21013UL
#define cAf6Reg_pla_debug2_stk_WidthVal                                                                     32
#define cAf6Reg_pla_debug2_stk_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: DiscardUPSR
BitField Type: W2C
BitField Desc: discard due to upsr disable
BitField Bits: [23]
--------------------------------------*/
#define cAf6_pla_debug2_stk_DiscardUPSR_Bit_Start                                                           23
#define cAf6_pla_debug2_stk_DiscardUPSR_Bit_End                                                             23
#define cAf6_pla_debug2_stk_DiscardUPSR_Mask                                                            cBit23
#define cAf6_pla_debug2_stk_DiscardUPSR_Shift                                                               23
#define cAf6_pla_debug2_stk_DiscardUPSR_MaxVal                                                             0x1
#define cAf6_pla_debug2_stk_DiscardUPSR_MinVal                                                             0x0
#define cAf6_pla_debug2_stk_DiscardUPSR_RstVal                                                             0x0

/*--------------------------------------
BitField Name: SeqBufErr
BitField Type: W2C
BitField Desc: Sequence buffer error
BitField Bits: [22]
--------------------------------------*/
#define cAf6_pla_debug2_stk_SeqBufErr_Bit_Start                                                             22
#define cAf6_pla_debug2_stk_SeqBufErr_Bit_End                                                               22
#define cAf6_pla_debug2_stk_SeqBufErr_Mask                                                              cBit22
#define cAf6_pla_debug2_stk_SeqBufErr_Shift                                                                 22
#define cAf6_pla_debug2_stk_SeqBufErr_MaxVal                                                               0x1
#define cAf6_pla_debug2_stk_SeqBufErr_MinVal                                                               0x0
#define cAf6_pla_debug2_stk_SeqBufErr_RstVal                                                               0x0

/*--------------------------------------
BitField Name: eXAUI1DatBufErr
BitField Type: W2C
BitField Desc: eXAUI1 data buffer error
BitField Bits: [21]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI1DatBufErr_Bit_Start                                                       21
#define cAf6_pla_debug2_stk_eXAUI1DatBufErr_Bit_End                                                         21
#define cAf6_pla_debug2_stk_eXAUI1DatBufErr_Mask                                                        cBit21
#define cAf6_pla_debug2_stk_eXAUI1DatBufErr_Shift                                                           21
#define cAf6_pla_debug2_stk_eXAUI1DatBufErr_MaxVal                                                         0x1
#define cAf6_pla_debug2_stk_eXAUI1DatBufErr_MinVal                                                         0x0
#define cAf6_pla_debug2_stk_eXAUI1DatBufErr_RstVal                                                         0x0

/*--------------------------------------
BitField Name: eXAUI0DatBufErr
BitField Type: W2C
BitField Desc: eXAUI0 data buffer error
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI0DatBufErr_Bit_Start                                                       20
#define cAf6_pla_debug2_stk_eXAUI0DatBufErr_Bit_End                                                         20
#define cAf6_pla_debug2_stk_eXAUI0DatBufErr_Mask                                                        cBit20
#define cAf6_pla_debug2_stk_eXAUI0DatBufErr_Shift                                                           20
#define cAf6_pla_debug2_stk_eXAUI0DatBufErr_MaxVal                                                         0x1
#define cAf6_pla_debug2_stk_eXAUI0DatBufErr_MinVal                                                         0x0
#define cAf6_pla_debug2_stk_eXAUI0DatBufErr_RstVal                                                         0x0

/*--------------------------------------
BitField Name: eXAUI1SeqTimeout
BitField Type: W2C
BitField Desc: eXAUI1 Sequence Timeout
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI1SeqTimeout_Bit_Start                                                      18
#define cAf6_pla_debug2_stk_eXAUI1SeqTimeout_Bit_End                                                        18
#define cAf6_pla_debug2_stk_eXAUI1SeqTimeout_Mask                                                       cBit18
#define cAf6_pla_debug2_stk_eXAUI1SeqTimeout_Shift                                                          18
#define cAf6_pla_debug2_stk_eXAUI1SeqTimeout_MaxVal                                                        0x1
#define cAf6_pla_debug2_stk_eXAUI1SeqTimeout_MinVal                                                        0x0
#define cAf6_pla_debug2_stk_eXAUI1SeqTimeout_RstVal                                                        0x0

/*--------------------------------------
BitField Name: eXAUI1SeqBad
BitField Type: W2C
BitField Desc: eXAUI1 Sequence Bad
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI1SeqBad_Bit_Start                                                          17
#define cAf6_pla_debug2_stk_eXAUI1SeqBad_Bit_End                                                            17
#define cAf6_pla_debug2_stk_eXAUI1SeqBad_Mask                                                           cBit17
#define cAf6_pla_debug2_stk_eXAUI1SeqBad_Shift                                                              17
#define cAf6_pla_debug2_stk_eXAUI1SeqBad_MaxVal                                                            0x1
#define cAf6_pla_debug2_stk_eXAUI1SeqBad_MinVal                                                            0x0
#define cAf6_pla_debug2_stk_eXAUI1SeqBad_RstVal                                                            0x0

/*--------------------------------------
BitField Name: eXAUI1SeqGood
BitField Type: W2C
BitField Desc: eXAUI1 Sequence Good
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI1SeqGood_Bit_Start                                                         16
#define cAf6_pla_debug2_stk_eXAUI1SeqGood_Bit_End                                                           16
#define cAf6_pla_debug2_stk_eXAUI1SeqGood_Mask                                                          cBit16
#define cAf6_pla_debug2_stk_eXAUI1SeqGood_Shift                                                             16
#define cAf6_pla_debug2_stk_eXAUI1SeqGood_MaxVal                                                           0x1
#define cAf6_pla_debug2_stk_eXAUI1SeqGood_MinVal                                                           0x0
#define cAf6_pla_debug2_stk_eXAUI1SeqGood_RstVal                                                           0x0

/*--------------------------------------
BitField Name: eXAUI0SeqTimeout
BitField Type: W2C
BitField Desc: eXAUI0 Sequence Timeout
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI0SeqTimeout_Bit_Start                                                      14
#define cAf6_pla_debug2_stk_eXAUI0SeqTimeout_Bit_End                                                        14
#define cAf6_pla_debug2_stk_eXAUI0SeqTimeout_Mask                                                       cBit14
#define cAf6_pla_debug2_stk_eXAUI0SeqTimeout_Shift                                                          14
#define cAf6_pla_debug2_stk_eXAUI0SeqTimeout_MaxVal                                                        0x1
#define cAf6_pla_debug2_stk_eXAUI0SeqTimeout_MinVal                                                        0x0
#define cAf6_pla_debug2_stk_eXAUI0SeqTimeout_RstVal                                                        0x0

/*--------------------------------------
BitField Name: eXAUI0SeqBad
BitField Type: W2C
BitField Desc: eXAUI0 Sequence Bad
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI0SeqBad_Bit_Start                                                          13
#define cAf6_pla_debug2_stk_eXAUI0SeqBad_Bit_End                                                            13
#define cAf6_pla_debug2_stk_eXAUI0SeqBad_Mask                                                           cBit13
#define cAf6_pla_debug2_stk_eXAUI0SeqBad_Shift                                                              13
#define cAf6_pla_debug2_stk_eXAUI0SeqBad_MaxVal                                                            0x1
#define cAf6_pla_debug2_stk_eXAUI0SeqBad_MinVal                                                            0x0
#define cAf6_pla_debug2_stk_eXAUI0SeqBad_RstVal                                                            0x0

/*--------------------------------------
BitField Name: eXAUI0SeqGood
BitField Type: W2C
BitField Desc: eXAUI0 Sequence Good
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI0SeqGood_Bit_Start                                                         12
#define cAf6_pla_debug2_stk_eXAUI0SeqGood_Bit_End                                                           12
#define cAf6_pla_debug2_stk_eXAUI0SeqGood_Mask                                                          cBit12
#define cAf6_pla_debug2_stk_eXAUI0SeqGood_Shift                                                             12
#define cAf6_pla_debug2_stk_eXAUI0SeqGood_MaxVal                                                           0x1
#define cAf6_pla_debug2_stk_eXAUI0SeqGood_MinVal                                                           0x0
#define cAf6_pla_debug2_stk_eXAUI0SeqGood_RstVal                                                           0x0

/*--------------------------------------
BitField Name: PWSeqTimeout
BitField Type: W2C
BitField Desc: PW Sequence Timeout
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PWSeqTimeout_Bit_Start                                                          10
#define cAf6_pla_debug2_stk_PWSeqTimeout_Bit_End                                                            10
#define cAf6_pla_debug2_stk_PWSeqTimeout_Mask                                                           cBit10
#define cAf6_pla_debug2_stk_PWSeqTimeout_Shift                                                              10
#define cAf6_pla_debug2_stk_PWSeqTimeout_MaxVal                                                            0x1
#define cAf6_pla_debug2_stk_PWSeqTimeout_MinVal                                                            0x0
#define cAf6_pla_debug2_stk_PWSeqTimeout_RstVal                                                            0x0

/*--------------------------------------
BitField Name: PWSeqBad
BitField Type: W2C
BitField Desc: PW Sequence Bad
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PWSeqBad_Bit_Start                                                               9
#define cAf6_pla_debug2_stk_PWSeqBad_Bit_End                                                                 9
#define cAf6_pla_debug2_stk_PWSeqBad_Mask                                                                cBit9
#define cAf6_pla_debug2_stk_PWSeqBad_Shift                                                                   9
#define cAf6_pla_debug2_stk_PWSeqBad_MaxVal                                                                0x1
#define cAf6_pla_debug2_stk_PWSeqBad_MinVal                                                                0x0
#define cAf6_pla_debug2_stk_PWSeqBad_RstVal                                                                0x0

/*--------------------------------------
BitField Name: PWSeqGood
BitField Type: W2C
BitField Desc: PW Sequence Good
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pla_debug2_stk_PWSeqGood_Bit_Start                                                              8
#define cAf6_pla_debug2_stk_PWSeqGood_Bit_End                                                                8
#define cAf6_pla_debug2_stk_PWSeqGood_Mask                                                               cBit8
#define cAf6_pla_debug2_stk_PWSeqGood_Shift                                                                  8
#define cAf6_pla_debug2_stk_PWSeqGood_MaxVal                                                               0x1
#define cAf6_pla_debug2_stk_PWSeqGood_MinVal                                                               0x0
#define cAf6_pla_debug2_stk_PWSeqGood_RstVal                                                               0x0

/*--------------------------------------
BitField Name: eXAUI1DatEn
BitField Type: W2C
BitField Desc: eXAUI1 buffer data enable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI1DatEn_Bit_Start                                                            7
#define cAf6_pla_debug2_stk_eXAUI1DatEn_Bit_End                                                              7
#define cAf6_pla_debug2_stk_eXAUI1DatEn_Mask                                                             cBit7
#define cAf6_pla_debug2_stk_eXAUI1DatEn_Shift                                                                7
#define cAf6_pla_debug2_stk_eXAUI1DatEn_MaxVal                                                             0x1
#define cAf6_pla_debug2_stk_eXAUI1DatEn_MinVal                                                             0x0
#define cAf6_pla_debug2_stk_eXAUI1DatEn_RstVal                                                             0x0

/*--------------------------------------
BitField Name: eXAUI1DatVld
BitField Type: W2C
BitField Desc: data valid from eXAUI1
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI1DatVld_Bit_Start                                                           6
#define cAf6_pla_debug2_stk_eXAUI1DatVld_Bit_End                                                             6
#define cAf6_pla_debug2_stk_eXAUI1DatVld_Mask                                                            cBit6
#define cAf6_pla_debug2_stk_eXAUI1DatVld_Shift                                                               6
#define cAf6_pla_debug2_stk_eXAUI1DatVld_MaxVal                                                            0x1
#define cAf6_pla_debug2_stk_eXAUI1DatVld_MinVal                                                            0x0
#define cAf6_pla_debug2_stk_eXAUI1DatVld_RstVal                                                            0x0

/*--------------------------------------
BitField Name: eXAUI1PktEn
BitField Type: W2C
BitField Desc: eXAUI1 buffer packet enable
BitField Bits: [5]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI1PktEn_Bit_Start                                                            5
#define cAf6_pla_debug2_stk_eXAUI1PktEn_Bit_End                                                              5
#define cAf6_pla_debug2_stk_eXAUI1PktEn_Mask                                                             cBit5
#define cAf6_pla_debug2_stk_eXAUI1PktEn_Shift                                                                5
#define cAf6_pla_debug2_stk_eXAUI1PktEn_MaxVal                                                             0x1
#define cAf6_pla_debug2_stk_eXAUI1PktEn_MinVal                                                             0x0
#define cAf6_pla_debug2_stk_eXAUI1PktEn_RstVal                                                             0x0

/*--------------------------------------
BitField Name: eXAUI1PktVld
BitField Type: W2C
BitField Desc: packet info valid to eXAUI0
BitField Bits: [4]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI1PktVld_Bit_Start                                                           4
#define cAf6_pla_debug2_stk_eXAUI1PktVld_Bit_End                                                             4
#define cAf6_pla_debug2_stk_eXAUI1PktVld_Mask                                                            cBit4
#define cAf6_pla_debug2_stk_eXAUI1PktVld_Shift                                                               4
#define cAf6_pla_debug2_stk_eXAUI1PktVld_MaxVal                                                            0x1
#define cAf6_pla_debug2_stk_eXAUI1PktVld_MinVal                                                            0x0
#define cAf6_pla_debug2_stk_eXAUI1PktVld_RstVal                                                            0x0

/*--------------------------------------
BitField Name: eXAUI0DatEn
BitField Type: W2C
BitField Desc: eXAUI0 buffer data enable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI0DatEn_Bit_Start                                                            3
#define cAf6_pla_debug2_stk_eXAUI0DatEn_Bit_End                                                              3
#define cAf6_pla_debug2_stk_eXAUI0DatEn_Mask                                                             cBit3
#define cAf6_pla_debug2_stk_eXAUI0DatEn_Shift                                                                3
#define cAf6_pla_debug2_stk_eXAUI0DatEn_MaxVal                                                             0x1
#define cAf6_pla_debug2_stk_eXAUI0DatEn_MinVal                                                             0x0
#define cAf6_pla_debug2_stk_eXAUI0DatEn_RstVal                                                             0x0

/*--------------------------------------
BitField Name: eXAUI0DatVld
BitField Type: W2C
BitField Desc: data valid from eXAUI0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI0DatVld_Bit_Start                                                           2
#define cAf6_pla_debug2_stk_eXAUI0DatVld_Bit_End                                                             2
#define cAf6_pla_debug2_stk_eXAUI0DatVld_Mask                                                            cBit2
#define cAf6_pla_debug2_stk_eXAUI0DatVld_Shift                                                               2
#define cAf6_pla_debug2_stk_eXAUI0DatVld_MaxVal                                                            0x1
#define cAf6_pla_debug2_stk_eXAUI0DatVld_MinVal                                                            0x0
#define cAf6_pla_debug2_stk_eXAUI0DatVld_RstVal                                                            0x0

/*--------------------------------------
BitField Name: eXAUI0PktEn
BitField Type: W2C
BitField Desc: eXAUI0 buffer packet enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI0PktEn_Bit_Start                                                            1
#define cAf6_pla_debug2_stk_eXAUI0PktEn_Bit_End                                                              1
#define cAf6_pla_debug2_stk_eXAUI0PktEn_Mask                                                             cBit1
#define cAf6_pla_debug2_stk_eXAUI0PktEn_Shift                                                                1
#define cAf6_pla_debug2_stk_eXAUI0PktEn_MaxVal                                                             0x1
#define cAf6_pla_debug2_stk_eXAUI0PktEn_MinVal                                                             0x0
#define cAf6_pla_debug2_stk_eXAUI0PktEn_RstVal                                                             0x0

/*--------------------------------------
BitField Name: eXAUI0PktVld
BitField Type: W2C
BitField Desc: packet info valid to eXAUI0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pla_debug2_stk_eXAUI0PktVld_Bit_Start                                                           0
#define cAf6_pla_debug2_stk_eXAUI0PktVld_Bit_End                                                             0
#define cAf6_pla_debug2_stk_eXAUI0PktVld_Mask                                                            cBit0
#define cAf6_pla_debug2_stk_eXAUI0PktVld_Shift                                                               0
#define cAf6_pla_debug2_stk_eXAUI0PktVld_MaxVal                                                            0x1
#define cAf6_pla_debug2_stk_eXAUI0PktVld_MinVal                                                            0x0
#define cAf6_pla_debug2_stk_eXAUI0PktVld_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Payload Assembler debug Status
Reg Addr   : 0x2_1028
Reg Formula:
    Where  :
Reg Desc   :
This register is used to used to show some debug info of PLA

------------------------------------------------------------------------------*/
#define cAf6Reg_pla_debug_sta_Base                                                                     0x21028
#define cAf6Reg_pla_debug_sta                                                                        0x21028UL
#define cAf6Reg_pla_debug_sta_WidthVal                                                                      32
#define cAf6Reg_pla_debug_sta_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: VCG free block number
BitField Type: RO
BitField Desc: current VCG free block number
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_pla_debug_sta_VCG_free_block_number_Bit_Start                                                  16
#define cAf6_pla_debug_sta_VCG_free_block_number_Bit_End                                                    31
#define cAf6_pla_debug_sta_VCG_free_block_number_Mask                                                cBit31_16
#define cAf6_pla_debug_sta_VCG_free_block_number_Shift                                                      16
#define cAf6_pla_debug_sta_VCG_free_block_number_MaxVal                                                 0xffff
#define cAf6_pla_debug_sta_VCG_free_block_number_MinVal                                                    0x0
#define cAf6_pla_debug_sta_VCG_free_block_number_RstVal                                                    0x0

/*--------------------------------------
BitField Name: write cache number
BitField Type: RO
BitField Desc: current write cache number
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_pla_debug_sta_write_cache_number_Bit_Start                                                      0
#define cAf6_pla_debug_sta_write_cache_number_Bit_End                                                       15
#define cAf6_pla_debug_sta_write_cache_number_Mask                                                    cBit15_0
#define cAf6_pla_debug_sta_write_cache_number_Shift                                                          0
#define cAf6_pla_debug_sta_write_cache_number_MaxVal                                                    0xffff
#define cAf6_pla_debug_sta_write_cache_number_MinVal                                                       0x0
#define cAf6_pla_debug_sta_write_cache_number_RstVal                                                       0x0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEPLADEBUGREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210012ConcateGroupPrbsEngine.c
 *
 * Created Date: Aug 7, 2016
 *
 * Description : PRBS
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "../../../default/pdh/ThaModulePdh.h"
#include "../../../default/prbs/ThaModulePrbsInternal.h"
#include "../../../default/prbs/ThaPrbsEngineInternal.h"
#include "../concate/Tha60210012ModuleConcate.h"
#include "../concate/Tha60210012ModuleConcateLoVcatReg.h"
#include "Tha60210012ConcateGroupPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012ConcateGroupPrbsEngine
    {
    tAtPrbsEngine super;
    }tTha60210012ConcateGroupPrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods = NULL;
static const tAtPrbsEngineMethods   *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    return AtPrbsEngineEnable(self, enable);
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    return AtPrbsEngineIsEnabled(self);
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    return AtPrbsEngineEnable(self, enable);
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    return AtPrbsEngineIsEnabled(self);
    }

static AtDevice Device(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    return AtChannelDeviceGet(channel);
    }

static Tha60210012ModuleConcate ConcateModule(AtPrbsEngine self)
    {
    return (Tha60210012ModuleConcate)AtDeviceModuleGet(Device(self), cAtModuleConcate);
    }

static uint32 VcgIdGet(AtPrbsEngine self)
    {
    AtConcateGroup group = (AtConcateGroup)AtPrbsEngineChannelGet(self);
    return AtChannelIdGet((AtChannel) group);
    }

static uint32 BaseAddress(AtPrbsEngine self)
    {
    return Tha60210012ModuleConcateBaseAddress(ConcateModule(self));
    }

static eAtModulePrbsRet HwDefaultSet(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_vcat5g_sodat_pen_Base + BaseAddress(self);
    AtPrbsEngineWrite(self, regAddress, 0x0, cAtModulePrbs);
    return cAtOk;
    }

static eAtModulePrbsRet HwChannelSet(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_vcat5g_sodat_pen_Base + BaseAddress(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6_vcat5g_sodat_pen_Cfg_Dat_VCG_, VcgIdGet(self));
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);
    return cAtOk;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    HwDefaultSet(self);
    AtPrbsEngineEnable(self, cAtFalse);
    AtPrbsEngineErrorForce(self, cAtFalse);
    AtPrbsEngineModeSet(self, cAtPrbsModePrbs15);
    return HwChannelSet(self);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddress = cAf6Reg_vcat5g_sodat_pen_Base + BaseAddress(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6_vcat5g_sodat_pen_Cfg_Dat_Ena_, mBoolToBin(enable));
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);
    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_vcat5g_sodat_pen_Base + BaseAddress(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);

    return (regValue & cAf6_vcat5g_sodat_pen_Cfg_Dat_Ena_Mask) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddress = cAf6Reg_vcat5g_sodat_pen_Base + BaseAddress(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    
    mRegFieldSet(regValue, cAf6_vcat5g_sodat_pen_Cfg_Ber_Ena_, mBoolToBin(force));
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);
    
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_vcat5g_sodat_pen_Base + BaseAddress(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);

    return (regValue & cAf6_vcat5g_sodat_pen_Cfg_Ber_Ena_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 ModeSw2Hw(uint32 prbsMode)
    {
    switch (prbsMode)
        {
        case cAtPrbsModePrbs11:                     return 0;
        case cAtPrbsModePrbs15:                     return 1;
        case cAtPrbsModePrbs20StdO151:              return 2;
        case cAtPrbsModePrbs23:                     return 4;
        case cAtPrbsModePrbsFixedPattern4Bytes:     return 5;
        case cAtPrbsModePrbsSeq:                    return 7;
        default :                                   return 3;
        }
    }

static uint32 ModeHw2Sw(uint32 prbsMode)
    {
    switch (prbsMode)
        {
        case 0:     return cAtPrbsModePrbs11;
        case 1:     return cAtPrbsModePrbs15;
        case 2:     return cAtPrbsModePrbs20StdO151;
        case 4:     return cAtPrbsModePrbs23;
        case 5:     return cAtPrbsModePrbsFixedPattern4Bytes;
        case 7:     return cAtPrbsModePrbsSeq;
        default :   return cAtPrbsModeInvalid;
        }
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddress = cAf6Reg_vcat5g_sodat_pen_Base + BaseAddress(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cAf6_vcat5g_sodat_pen_Cfg_Dat_Mod_, ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_vcat5g_sodat_pen_Base + BaseAddress(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    uint32 hwMode = mRegField(regValue, cAf6_vcat5g_sodat_pen_Cfg_Dat_Mod_);

    return ModeHw2Sw(hwMode);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    switch ((uint32) prbsMode)
        {
        case cAtPrbsModePrbs11:                     return cAtTrue;
        case cAtPrbsModePrbs15:                     return cAtTrue;
        case cAtPrbsModePrbs20StdO151:              return cAtTrue;
        case cAtPrbsModePrbs23:                     return cAtTrue;
        case cAtPrbsModePrbsFixedPattern4Bytes:     return cAtTrue;
        case cAtPrbsModePrbsSeq:                    return cAtTrue;
        default :                                   return cAtFalse;
        }
    }

static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
    uint32 regAddress = cAf6Reg_vcat5g_sodat_pen_Base + BaseAddress(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    
    mRegFieldSet(regValue, cAf6_vcat5g_sodat_pen_Cfg_Inv_Mod_, mBoolToBin(invert));
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);
    
    return cAtOk;
    }

static eBool IsInverted(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_vcat5g_sodat_pen_Base + BaseAddress(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);

    return (regValue & cAf6_vcat5g_sodat_pen_Cfg_Inv_Mod_Mask) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
    {
    return Invert(self, invert);
    }

static eBool TxIsInverted(AtPrbsEngine self)
    {
    return IsInverted(self);
    }

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
    {
    return Invert(self, invert);
    }

static eBool RxIsInverted(AtPrbsEngine self)
    {
    return IsInverted(self);
    }

static uint32 RxBitErrorCounterGet(AtPrbsEngine self, eBool r2c)
    {
    uint32 offset = (r2c) ? 1 : 0;
    uint32 regAddress = cAf6Reg_vcat5g_sodatmon_pen_Base + offset + BaseAddress(self);
    
    return AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    if (counterType == cAtPrbsEngineCounterRxBitError)
        return RxBitErrorCounterGet(self, cAtFalse);

    return 0x0;
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    if (counterType == cAtPrbsEngineCounterRxBitError)
        return RxBitErrorCounterGet(self, cAtTrue);

    return 0x0;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool clear)
    {
    uint32 alarm = cAtPrbsEngineAlarmTypeNone;
    uint32 regAddress = cAf6Reg_vcat5g_dbdatstk_pen_Base  + BaseAddress(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);

    if (regValue & cAf6_vcat5g_dbdatstk_pen_Dat_Err_Stk_Mask)
        alarm = cAtPrbsEngineAlarmTypeError;

    if (clear && (alarm != cAtPrbsEngineAlarmTypeNone))
        AtPrbsEngineWrite(self, regAddress, cAf6_vcat5g_dbdatstk_pen_Dat_Err_Stk_Mask, cAtModulePrbs);

    return alarm;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 alarm = cAtPrbsEngineAlarmTypeNone;
    uint32 regAddress = cAf6Reg_vcat5g_dbdatstk_pen_Base  + BaseAddress(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);

    if (regValue & cAf6_vcat5g_dbdatstk_pen_Dat_Err_Sta_Mask)
        alarm = cAtPrbsEngineAlarmTypeError;

    return alarm;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return AtPrbsEngineModeSet(self, prbsMode);
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineModeGet(self);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return AtPrbsEngineModeSet(self, prbsMode);
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineModeGet(self);
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eBool ErrorForcingRateIsSupported(AtPrbsEngine self, eAtBerRate errorRate)
    {
    AtUnused(self);
    if (errorRate == cAtBerRateUnknown)
        return cAtFalse;
        
    if (errorRate > cAtBerRate1E9)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 ErrorRateSw2Hw(eAtBerRate errorRate)
    {
    switch ((uint32)errorRate)
        {
        case cAtBerRate1E3: return 0;
        case cAtBerRate1E4: return 1;
        case cAtBerRate1E5: return 2;
        case cAtBerRate1E6: return 3;
        case cAtBerRate1E7: return 4;
        case cAtBerRate1E8: return 5;
        case cAtBerRate1E9: return 6;
        default:            return 7;
        }
    }

static eAtBerRate ErrorRateHw2Sw(uint32 errorRate)
    {
    switch (errorRate)
        {
        case 0:  return cAtBerRate1E3;
        case 1:  return cAtBerRate1E4;
        case 2:  return cAtBerRate1E5;
        case 3:  return cAtBerRate1E6;
        case 4:  return cAtBerRate1E7;
        case 5:  return cAtBerRate1E8;
        case 6:  return cAtBerRate1E9;
        default: return cAtBerRateUnknown;
        }
    }

static eAtModulePrbsRet HwTxErrorRateSet(AtPrbsEngine self, eAtBerRate errorRate)
    {
    uint32 regAddress = cAf6Reg_vcat5g_sodat_pen_Base + BaseAddress(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    
    mRegFieldSet(regValue, cAf6_vcat5g_sodat_pen_Cfg_Ber_Mod_, ErrorRateSw2Hw(errorRate));
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);
    
    return cAtOk;
    }

static eAtModulePrbsRet TxErrorRateSet(AtPrbsEngine self, eAtBerRate errorRate)
    {
    if (!AtPrbsEngineErrorForcingRateIsSupported(self, errorRate))
        return cAtErrorModeNotSupport;

    return HwTxErrorRateSet(self, errorRate);
    }

static eAtBerRate TxErrorRateGet(AtPrbsEngine self)
    {
    uint32 regAddress = cAf6Reg_vcat5g_sodat_pen_Base + BaseAddress(self);
    uint32 regValue = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);

    if (!(regValue & cAf6_vcat5g_sodat_pen_Cfg_Ber_Ena_Mask))
        return cAtBerRateUnknown;

    return (eAtBerRate)ErrorRateHw2Sw(mRegField(regValue, cAf6_vcat5g_sodat_pen_Cfg_Ber_Mod_));
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ConcateGroupPrbsEngine);
    }

static void Delete(AtObject self)
    {
    AtConcateGroup group = (AtConcateGroup) AtPrbsEngineChannelGet((AtPrbsEngine) self);
    AtChannelPrbsEngineSet((AtChannel) group, NULL);
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, Invert);
        mMethodOverride(m_AtPrbsEngineOverride, IsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, TxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, RxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForcingRateIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtChannel channel, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, channel, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210012ConcateGroupPrbsEngineNew(AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, channel, 0);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210012ModulePrbsInternal.h
 * 
 * Created Date: Jun 18, 2016
 *
 * Description : 60210012 module PRBS internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEPRBSINTERNAL_H_
#define _THA60210012MODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/prbs/Tha60210051ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModulePrbs
    {
    tTha60210051ModulePrbs super;

    /* Only one prbs engine for all VCGs */
    AtPrbsEngine prbsEngine;
    }tTha60210012ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha60210012ModulePrbsObjectInit(AtModulePrbs self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEPRBSINTERNAL_H_ */


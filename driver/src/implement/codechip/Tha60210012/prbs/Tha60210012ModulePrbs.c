/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      :  
 *
 * File        : Tha60210012ModulePrbs.c
 *
 * Created Date: Mar 23, 2016
 *
 * Author: phuongnm
 *
 * Description : PRBS
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/concate/AtConcateGroupInternal.h"
#include "Tha60210012ModulePrbsInternal.h"
#include "Tha60210012ConcateGroupPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210012ModulePrbs *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tAtModulePrbsMethods  m_AtModulePrbsOverride;
static tThaModulePrbsMethods m_ThaModulePrbsOverride;

/* Save super implementation */
static const tAtObjectMethods      *m_AtObjectMethods      = NULL;
static const tAtModulePrbsMethods  *m_AtModulePrbsMethods  = NULL;
static const tThaModulePrbsMethods *m_ThaModulePrbsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PwPrbsIsSupported(ThaModulePrbs self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static eBool HoVcPrbsIsSupported(ThaModulePrbs self)
    {
    /* This product supports this feature from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static AtPrbsEngine PwPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPw pw)
    {
    if (!AtPwIsTdmPw(pw))
        return NULL;

    return m_ThaModulePrbsMethods->PwPrbsEngineObjectCreate(self, engineId, pw);
    }

static AtPrbsEngine ConcateGroupPrbsEngineCreate(AtModulePrbs self, AtConcateGroup group)
    {
    AtPrbsEngine engine;
    if (AtChannelPrbsEngineGet((AtChannel)group))
        return NULL;

    if (mThis(self)->prbsEngine)
        return NULL;

    engine = Tha60210012ConcateGroupPrbsEngineNew((AtChannel)group);
    AtPrbsEngineInit(engine);
    AtChannelPrbsEngineSet((AtChannel)group, engine);

    mThis(self)->prbsEngine = engine;
    return engine;
    }

static eAtRet ConcateGroupPrbsEngineDelete(AtModulePrbs self, AtPrbsEngine engine)
    {
    AtChannel group;
    if (mThis(self)->prbsEngine != engine)
        return cAtErrorInvlParm;

    group = AtPrbsEngineChannelGet(engine);
    AtChannelPrbsEngineSet(group, NULL);

    AtObjectDelete((AtObject)engine);
    mThis(self)->prbsEngine = NULL;

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject) mThis(self)->prbsEngine);
    mThis(self)->prbsEngine = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210012ModulePrbs *object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(prbsEngine);
    }

static void OverrideAtObject(AtModulePrbs self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, m_AtModulePrbsMethods, sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, ConcateGroupPrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, ConcateGroupPrbsEngineDelete);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideThaModulePrbs(ThaModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePrbsOverride, m_ThaModulePrbsMethods, sizeof(m_ThaModulePrbsOverride));

        mMethodOverride(m_ThaModulePrbsOverride, PwPrbsIsSupported);
        mMethodOverride(m_ThaModulePrbsOverride, HoVcPrbsIsSupported);
        mMethodOverride(m_ThaModulePrbsOverride, PwPrbsEngineObjectCreate);
        }

    mMethodsSet(self, &m_ThaModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtObject(self);
    OverrideAtModulePrbs(self);
    OverrideThaModulePrbs((ThaModulePrbs)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModulePrbs);
    }

AtModulePrbs Tha60210012ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha60210012ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModulePrbsObjectInit(newModule, device);
    }

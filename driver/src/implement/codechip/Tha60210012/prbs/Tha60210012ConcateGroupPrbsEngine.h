/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210012ConcateGroupPrbsEngine.h
 * 
 * Created Date: Aug 7, 2016
 *
 * Description : PRBS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012CONCATEGROUPPRBSENGINE_H_
#define _THA60210012CONCATEGROUPPRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60210012ConcateGroupPrbsEngineNew(AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012CONCATEGROUPPRBSENGINE_H_ */


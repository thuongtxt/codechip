/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP/MLPPP
 *
 * File        : Tha60210012ModulePpp.c
 *
 * Created Date: Mar 14, 2016
 *
 * Description : PPP/MLPPP module implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/ppp/ThaModulePppInternal.h"
#include "Tha60210012ModulePpp.h"
#include "Tha60210012MpBundle.h"
#include "Tha60210012PidTable.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210012ModulePpp *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012ModulePpp
    {
    tThaModulePpp super;

    /* Private data */
    uint32 counterModule;
    }tTha60210012ModulePpp;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtModulePppMethods  m_AtModulePppOverride;
static tThaModulePppMethods m_ThaModulePppOverride;

/* Cache super implementation */
static const tAtObjectMethods       *m_AtObjectMethods     = NULL;
static const tThaModulePppMethods   *m_ThaModulePppMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtMpBundle MpBundleObjectCreate(AtModulePpp self, uint32 bundleId)
    {
    AtUnused(self);
    AtUnused(bundleId);
    return NULL;
    }

static uint32 MaxNumOfMpBundleGet(AtModulePpp self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 MaxNumQueuesPerLink(ThaModulePpp self)
    {
    AtUnused(self);
    return 2;
    }

static AtPidTable PidTableObjectCreate(AtModulePpp self)
    {
    return Tha60210012PidTableNew((AtModule)self);
    }

static eAtRet DefaultSet(ThaModulePpp self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210012ModulePpp *object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(counterModule);
    }

static void OverrideAtObject(AtModulePpp self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaModulePpp(AtModulePpp self)
    {
    ThaModulePpp pppModule = (ThaModulePpp)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePppMethods = mMethodsGet(pppModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePppOverride, m_ThaModulePppMethods, sizeof(m_ThaModulePppOverride));
        mMethodOverride(m_ThaModulePppOverride, MaxNumQueuesPerLink);
        mMethodOverride(m_ThaModulePppOverride, DefaultSet);
        }

    mMethodsSet(pppModule, &m_ThaModulePppOverride);
    }

static void OverrideAtModulePpp(AtModulePpp self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePppOverride, mMethodsGet(self), sizeof(m_AtModulePppOverride));

        mMethodOverride(m_AtModulePppOverride, MpBundleObjectCreate);
        mMethodOverride(m_AtModulePppOverride, MaxNumOfMpBundleGet);
        mMethodOverride(m_AtModulePppOverride, PidTableObjectCreate);
        }

    mMethodsSet(self, &m_AtModulePppOverride);
    }

static void Override(AtModulePpp self)
    {
    OverrideAtObject(self);
    OverrideThaModulePpp(self);
    OverrideAtModulePpp(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModulePpp);
    }

static AtModulePpp ObjectInit(AtModulePpp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePppObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    (mThis(self))->counterModule = cAtModuleSur;
    m_methodsInit = 1;

    return self;
    }

AtModulePpp Tha60210012ModulePppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePpp newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eAtRet ThaModulePppDebugCountersModuleSet(ThaModulePpp self, uint32 module)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if ((module == cAtModulePpp) || (module == cAtModuleSur))
        {
        mThis(self)->counterModule = module;
        return cAtOk;
        }

    return cAtErrorModeNotSupport;
    }

uint32 ThaModulePppDebugCountersModuleGet(ThaModulePpp self)
    {
    return self ? mThis(self)->counterModule : cAtModuleStart;
    }

eBool ThaModulePppShouldReadCountersFromSurModule(ThaModulePpp self)
    {
    return ((ThaModulePppDebugCountersModuleGet(self) == cAtModuleSur) ? cAtTrue : cAtFalse);
    }

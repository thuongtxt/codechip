/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP/MLPPP
 * 
 * File        : Tha60210012ModulePpp.h
 * 
 * Created Date: Mar 14, 2016
 *
 * Description : PPP/MLPPP module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEPPP_H_
#define _THA60210012MODULEPPP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ppp/ThaModulePppInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
AtModulePpp Tha60210012ModulePppNew(AtDevice device);

/* For debugging counter from SUR module */
eAtRet ThaModulePppDebugCountersModuleSet(ThaModulePpp self, uint32 module);
uint32 ThaModulePppDebugCountersModuleGet(ThaModulePpp self);
eBool ThaModulePppShouldReadCountersFromSurModule(ThaModulePpp self);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEPPP_H_ */


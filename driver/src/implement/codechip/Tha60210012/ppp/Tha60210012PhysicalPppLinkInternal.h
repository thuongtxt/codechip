/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP/MLPPP
 * 
 * File        : Tha60210012PhysicalPppLinkInternal.h
 * 
 * Created Date: Feb 8, 2017
 *
 * Description : Physical PPP Link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PHYSICALPPPLINKINTERNAL_H_
#define _THA60210012PHYSICALPPPLINKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ppp/dynamic/ThaPhysicalPppLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012PhysicalPppLink
    {
    tThaPhysicalPppLink super;
    }tTha60210012PhysicalPppLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPppLink Tha60210012PhysicalPppLinkObjectInit(AtPppLink self, AtHdlcChannel hdlcChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PHYSICALPPPLINKINTERNAL_H_ */


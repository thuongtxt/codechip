/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60210012PhysicalPppLink.c
 *
 * Created Date: Mar 14, 2016
 *
 * Description : PPP link of 60210012 product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtMpBundle.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/encap/dynamic/ThaPhysicalHdlcChannel.h"
#include "../../../default/encap/dynamic/ThaDynamicHdlcChannel.h"
#include "../../../default/encap/dynamic/ThaModuleDynamicEncap.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../mpeg/Tha60210012ModuleMpeg.h"
#include "../mpeg/Tha60210012MpegReg.h"
#include "../encap/Tha60210012ModuleDecapParserReg.h"
#include "../encap/Tha60210012PhysicalHdlcChannel.h"
#include "../encap/Tha60210012PhysicalCiscoHdlcLink.h"
#include "../encap/Tha60210012ModuleEncap.h"
#include "../encap/profile/Tha60210012ProfileReg.h"
#include "../pda/Tha60210012ModulePda.h"
#include "../eth/Tha60210012ModuleEthFlowLookupReg.h"
#include "../eth/Tha60210012EthFlow.h"
#include "../cla/Tha60210012ModuleCla.h"
#include "../ppp/Tha60210012ModulePpp.h"
#include "Tha60210012PhysicalPppLink.h"
#include "Tha60210012PhysicalPppLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods          m_AtChannelOverride;
static tAtHdlcLinkMethods         m_AtHdlcLinkOverride;
static tThaPppLinkMethods         m_ThaPppLinkOverride;
static tAtPppLinkMethods          m_AtPppLinkOverride;
static tThaPhysicalPppLinkMethods m_ThaPhysicalPppLinkOverride;

/* Save super implementation. */
static const tAtChannelMethods          *m_AtChannelMethods       = NULL;
static const tAtHdlcLinkMethods         *m_AtHdlcLinkMethods      = NULL;
static const tThaPhysicalPppLinkMethods *m_PhysicalPppLinkMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsSimulated(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceIsSimulated(device);
    }

static eBool ShouldReadCounterFromSurModule(AtChannel self)
    {
    ThaModulePpp pppModule = (ThaModulePpp)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePpp);

    if (pppModule == NULL)
        return cAtFalse;

    return ThaModulePppShouldReadCountersFromSurModule(pppModule);
    }

static eAtRet CountersRxGet(AtChannel self, tAtPppLinkCounters *pCounter, eBool r2c)
    {
    if (!self)
        return cAtErrorNullPointer;

    if (ShouldReadCounterFromSurModule(self))
        {
        ThaModuleHardSur surModule = mModuleSur(self);

        pCounter->rxFragment    = ThaModuleHardSurPppLinkRxFragmentGet(surModule, self, r2c);
        pCounter->rxByte        = ThaModuleHardSurPppLinkRxByteGet(surModule, self, r2c);
        pCounter->rxGoodPkt     = ThaModuleHardSurPppLinkRxGoodPktGet(surModule, self, r2c);
        pCounter->rxDiscardPkt  = ThaModuleHardSurPppLinkRxDiscardPktGet(surModule, self, r2c);
        pCounter->rxDiscardByte = ThaModuleHardSurPppLinkRxDiscardByteGet(surModule, self, r2c);
        }

    return cAtOk;
    }

static eAtRet CountersTxGet(AtChannel self, tAtPppLinkCounters *pCounter, eBool r2c)
    {
    if (!self)
        return cAtErrorNullPointer;

    if (ShouldReadCounterFromSurModule(self))
        {
        ThaModuleHardSur surModule = mModuleSur(self);

        pCounter->txByte        = ThaModuleHardSurPppLinkTxByteGet(surModule, self, r2c);
        pCounter->txFragment    = ThaModuleHardSurPppLinkTxFragmentGet(surModule, self, r2c);
        pCounter->txGoodPkt     = ThaModuleHardSurPppLinkTxGoodPktGet(surModule, self, r2c);
        pCounter->txDiscardPkt  = ThaModuleHardSurPppLinkTxDiscardPktGet(surModule, self, r2c);
        pCounter->txDiscardByte = ThaModuleHardSurPppLinkTxDiscardByteGet(surModule, self, r2c);
        }

    return cAtOk;
    }

static eAtRet LinkCountersGet(AtChannel self, void *pAllCounters, eBool clear)
    {
    eAtRet ret;

    /* Init value counter */
    if (pAllCounters)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, pAllCounters, 0, sizeof(tAtPppLinkCounters));
        }

    if (IsSimulated(self))
        return cAtOk;

    /* MPIG */
    ret = CountersRxGet(self, (tAtPppLinkCounters*)pAllCounters, clear);
    if (ret != cAtOk)
        return ret;

    /* MPEG */
    ret = CountersTxGet(self, (tAtPppLinkCounters*)pAllCounters, clear);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return LinkCountersGet(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    eAtRet ret = LinkCountersGet(self, pAllCounters, cAtTrue);
    ThaPppLinkDebugCountersClear((ThaPppLink)self);
    return ret;
    }

static eAtRet PppPacketModeSet(ThaPppLink self)
    {
    AtUnused(self);
    return cAtOk;
    }

static uint32 DefaultOffset(ThaPppLink self)
    {
    ThaHdlcChannel channel = (ThaHdlcChannel)AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    uint32 hwId = AtChannelHwIdGet((AtChannel)channel);
    uint32 slice = (uint32)ThaHdlcChannelCircuitSliceGet(channel);

    if (ThaHdlcChannelCircuitIsHo(channel))
        return (uint32)((0x1000 * slice) + hwId);

    return (uint32)((0x4000 * slice) + hwId);
    }

static eAtRet HOProtocolCompress(AtHdlcLink self, eBool compress)
    {
    uint32 offset = DefaultOffset((ThaPppLink)self) + Tha60210012ModuleDecapHoBaseAddress();
    uint32 regAddress = cAf6Reg_upen_cidcfg_HoCfg_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_upen_cidhocfg_afccfg_, mBoolToBin(compress));
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);
    return cAtOk;
    }

static eAtRet LOProtocolCompress(AtHdlcLink self, eBool compress)
    {
    uint32 offset = DefaultOffset((ThaPppLink)self) + Tha60210012ModuleDecapLoBaseAddress();
    uint32 regAddress = cAf6Reg_upen_cidcfg_locfg_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_upen_cidlocfg_pfccfg_, mBoolToBin(compress));
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);
    return cAtOk;
    }

static eAtModulePppRet ProtocolCompress(AtHdlcLink self, eBool compress)
    {
    if (ThaHdlcChannelCircuitIsHo((ThaHdlcChannel)AtHdlcLinkHdlcChannelGet(self)))
        return HOProtocolCompress(self, compress);

    return LOProtocolCompress(self, compress);
    }

static eBool HOProtocolIsCompressed(AtHdlcLink self)
    {
    uint32 offset = DefaultOffset((ThaPppLink)self) + Tha60210012ModuleDecapHoBaseAddress();
    uint32 regAddress = cAf6Reg_upen_cidcfg_HoCfg_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);
    return mRegField(regValue, cAf6_upen_cidhocfg_afccfg_) ? cAtTrue : cAtFalse;
    }

static eBool LOProtocolIsCompressed(AtHdlcLink self)
    {
    uint32 offset = DefaultOffset((ThaPppLink)self) + Tha60210012ModuleDecapLoBaseAddress();
    uint32 regAddress = cAf6Reg_upen_cidcfg_locfg_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);
    return mRegField(regValue, cAf6_upen_cidlocfg_pfccfg_) ? cAtTrue : cAtFalse;
    }

static eBool ProtocolIsCompressed(AtHdlcLink self)
    {
    if (ThaHdlcChannelCircuitIsHo((ThaHdlcChannel)AtHdlcLinkHdlcChannelGet(self)))
        return HOProtocolIsCompressed(self);

    return LOProtocolIsCompressed(self);
    }

static AtModuleEncap ModuleEncap(AtChannel self)
    {
    return (AtModuleEncap)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleEncap);
    }

static eAtRet ErrorProfileOnBinding(AtChannel self)
    {
    ThaProfileManager manager      = ThaModuleEncapProfileManagerGet(ModuleEncap(self));
    ThaProfileFinder errorFinder   = ThaProfileManagerErrorProfileFinderGet(manager);

    ThaProfileFinderRuleSet(errorFinder, cThaProfileErrorTypeMultilink | cThaProfileErrorTypeMac | cThaProfileErrorTypeLookup, cThaProfileRuleData);
    return ThaPhysicalPppLinkErrorProfileSet((ThaPhysicalPppLink)self, ThaProfileFinderProfileForLinkFind(errorFinder, (AtHdlcLink)self));
    }

static eAtRet ErrorProfileOnUnbinding(AtChannel self)
    {
    ThaProfileManager manager      = ThaModuleEncapProfileManagerGet(ModuleEncap(self));
    ThaProfileFinder errorFinder   = ThaProfileManagerErrorProfileFinderGet(manager);

    ThaProfileFinderRuleSet(errorFinder, cThaProfileErrorTypeAll, cThaProfileRuleDrop);
    return ThaPhysicalPppLinkErrorProfileSet((ThaPhysicalPppLink)self, ThaProfileFinderProfileForLinkFind(errorFinder, (AtHdlcLink)self));
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtDevice device;
    Tha60210012ModuleEncap encapModule;
    eAtRet ret;

    if (pseudowire == NULL)
        {
        AtHdlcChannel physicalChannel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
        ret = AtHdlcChannelFrameTypeSet(physicalChannel, AtHdlcChannelFrameTypeGet(physicalChannel));
        ret |= ErrorProfileOnUnbinding(self);

        return ret;
        }

    device = AtChannelDeviceGet(self);
    encapModule = (Tha60210012ModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);

    ret = Tha60210012ModuleEncapHdlcChannelFlowConnect(encapModule, (AtPw)pseudowire, self);
    ret |= ErrorProfileOnBinding(self);

    return ret;
    }

static eAtRet OamProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    eAtRet ret = Tha60210012LinkOamProfileHwSet(self, profile);
    if (ret != cAtOk)
        return ret;

    /* Deal with database */
    return m_PhysicalPppLinkMethods->OamProfileSet(self, profile);
    }

static eAtRet NetworkProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    eAtRet ret = Tha60210012LinkNetworkProfileHwSet(self, profile);
    if (ret != cAtOk)
        return ret;

    /* Deal with database */
    return m_PhysicalPppLinkMethods->NetworkProfileSet(self, profile);
    }

static eAtRet ErrorProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    eAtRet ret = Tha60210012LinkErrorProfileHwSet(self, profile);
    if (ret != cAtOk)
        return ret;

    /* Deal with database */
    return m_PhysicalPppLinkMethods->ErrorProfileSet(self, profile);
    }

static eAtRet BcpProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    eAtRet ret = Tha60210012LinkBcpProfileHwSet(self, profile);
    if (ret != cAtOk)
        return ret;

    /* Deal with database */
    return m_PhysicalPppLinkMethods->BcpProfileSet(self, profile);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= AtChannelTxTrafficEnable(self, enable);
    ret |= AtChannelRxTrafficEnable(self, enable);

    return ret;
    }

static eBool IsEnabled(AtChannel self)
    {
    return AtChannelTxTrafficIsEnabled(self);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(defectMask);
    return (enableMask) ? cAtErrorModeNotSupport : cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet OamPacketModeSet(AtHdlcLink self, eAtHdlcLinkOamMode oamPacketMode)
    {
    AtUnused(self);
    return (oamPacketMode == cAtHdlcLinkOamModeToPsn) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtHdlcLinkOamMode OamPacketModeGet(AtHdlcLink self)
    {
    AtUnused(self);
    return cAtHdlcLinkOamModeToPsn;
    }

static eAtRet PppPacketEnable(ThaPppLink self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    ThaEthFlow flow = (ThaEthFlow)AtHdlcLinkBoundFlowGet((AtHdlcLink)self);
    if (flow)
        return ThaEthFlowTxTrafficIsEnabled(flow);
    return cAtFalse;
    }

static eAtRet OamSend(AtHdlcLink self, uint8 *buffer, uint32 bufferLength)
    {
    AtUnused(self);
    AtUnused(buffer);
    AtUnused(bufferLength);
    return cAtErrorModeNotSupport;
    }

static uint32 OamReceive(AtHdlcLink self, uint8 *buffer, uint32 bufferLength)
    {
    AtUnused(self);
    AtUnused(buffer);
    AtUnused(bufferLength);
    return 0;
    }

static eAtRet PidByPass(AtHdlcLink self, eBool pidByPass)
    {
    AtUnused(self);
    AtUnused(pidByPass);
    return cAtOk;
    }

static eAtModulePppRet TxProtocolFieldCompress(AtPppLink self, eBool compress)
    {
    return ProtocolCompress((AtHdlcLink)self, compress);
    }

static eBool TxProtocolFieldIsCompressed(AtPppLink self)
    {
    return ProtocolIsCompressed((AtHdlcLink)self);
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);
    return Tha60210012EncapLinkDebug((AtHdlcLink)self);
    }

static eAtRet HoRxSequenceModeSet(AtHdlcLink self, uint8 sequenceNumber)
    {
    uint32 offset   = Tha60210012ModuleDecapHoBaseAddress() + DefaultOffset((ThaPppLink)self);
    uint32 address  = cAf6Reg_upen_cidcfg_HoCfg_Base + offset;
    uint32 regValue = mChannelHwRead(self, address, cAtModuleEncap);
    uint8 hwSeqNumber = 1; /* Default long sequence */

    if (sequenceNumber == cAtMpSequenceModeShort)
        hwSeqNumber = 0;

    mRegFieldSet(regValue, cAf6_upen_cidhocfg_ml_typ_, hwSeqNumber);
    mChannelHwWrite(self, address, regValue, cAtModuleEncap);
    return cAtOk;
    }

static uint8 HoRxSequenceModeGet(AtHdlcLink self)
    {
    uint32 offset   = Tha60210012ModuleDecapHoBaseAddress() + DefaultOffset((ThaPppLink)self);
    uint32 address  = cAf6Reg_upen_cidcfg_HoCfg_Base + offset;
    uint32 regValue = mChannelHwRead(self, address, cAtModuleEncap);
    return mRegField(regValue, cAf6_upen_cidhocfg_ml_typ_) ? cAtMpSequenceModeLong : cAtMpSequenceModeShort;
    }

static eAtRet LoRxSequenceModeSet(AtHdlcLink self, uint8 sequenceNumber)
    {
    uint32 offset   = Tha60210012ModuleDecapLoBaseAddress() + DefaultOffset((ThaPppLink)self);
    uint32 address  = cAf6Reg_upen_cidcfg_locfg_Base + offset;
    uint32 regValue = mChannelHwRead(self, address, cAtModuleEncap);
    uint8 hwSeqNumber = 1; /* Default long sequence */

    if (sequenceNumber == cAtMpSequenceModeShort)
        hwSeqNumber = 0;

    mRegFieldSet(regValue, cAf6_upen_cidlocfg_ml_typ_, hwSeqNumber);
    mChannelHwWrite(self, address, regValue, cAtModuleEncap);
    return cAtOk;
    }

static uint8 LoRxSequenceModeGet(AtHdlcLink self)
    {
    uint32 offset   = Tha60210012ModuleDecapLoBaseAddress() + DefaultOffset((ThaPppLink)self);
    uint32 address  = cAf6Reg_upen_cidcfg_locfg_Base + offset;
    uint32 regValue = mChannelHwRead(self, address, cAtModuleEncap);
    return mRegField(regValue, cAf6_upen_cidlocfg_ml_typ_) ? cAtMpSequenceModeLong : cAtMpSequenceModeShort;
    }

static void MlpppEnableOnLink(ThaPppLink self, eBool enable)
    {
    uint32 offset   = Tha60210012ModuleDecapLoBaseAddress() + DefaultOffset(self);
    uint32 address  = cAf6Reg_upen_cidcfg_locfg_Base + offset;
    uint32 regValue = mChannelHwRead(self, address, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_upen_cidlocfg_ml_enb_, mBoolToBin(enable));
    mChannelHwWrite(self, address, regValue, cAtModuleEncap);
    }

static eAtRet IgMlpppEnable(ThaPppLink self, eBool enable)
    {
    eAtRet ret;
    Tha60210012PhysicalHdlcChannel phyHdlcChannel = (Tha60210012PhysicalHdlcChannel)AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);

    MlpppEnableOnLink(self, enable);
    ret = Tha60210012PhysicalHdlcChannelLinkLookupEnable(phyHdlcChannel, enable);
    ret |= Tha60210012ModulePdaPppLinkMpBundleLookupEnable((AtHdlcLink)self, enable);
    return ret;
    }

static eAtRet JoinBundle(ThaPppLink self, ThaMpBundle bundle)
    {
    eAtRet ret;

    /* Configure lookup tdm to psn */
    ret = Tha60210012ModuleEncapLinkBundleSet((AtHdlcLink)self, (AtHdlcBundle)bundle);

    /* Configure loopkup psn to tdm */
    ret |= Tha60210012ModulePdaPppLinkMpBundleLookupSet((AtHdlcLink)self, (AtHdlcBundle)bundle);

    return cAtOk;
    }

static eAtRet EgMlpppEnable(ThaPppLink self, eBool enable)
    {
    return Tha60210012ModuleEncapLinkBundleLookupEnable((AtHdlcLink)self, enable);
    }

static Tha60210012ModuleMpeg MpegModuleGet(ThaPppLink self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (Tha60210012ModuleMpeg)AtDeviceModuleGet(device, cThaModuleMpeg);
    }

static eAtRet MemberLocalIdInBundleSet(ThaPppLink self , uint16 localMemberId)
    {
    uint32 offset = Tha60210012ModuleMpegBaseAddress(MpegModuleGet(self)) + AtChannelIdGet((AtChannel)self);
    uint32 address  = cAf6Reg_memidcfg0_pen_Base + offset;
    uint32 regValue = mChannelHwRead(self, address, cThaModuleMpeg);

    mRegFieldSet(regValue, cAf6_memidcfg0_pen_MemBundle_ID_, localMemberId);
    mRegFieldSet(regValue, cAf6_memidcfg0_pen_MemBundle_En_, 1); /* Enable member */
    mChannelHwWrite(self, address, regValue, cThaModuleMpeg);

    offset = cThaModuleEthFlowLookupBaseAddress + AtChannelIdGet((AtChannel)self);
    address = cAf6Reg_bundle_lup_table_Base + offset;
    regValue = mChannelHwRead(self, address, cAtModulePpp);

    mRegFieldSet(regValue, cAf6_bundle_lup_table_HDLC_Member_Id_, localMemberId);
    mChannelHwWrite(self, address, regValue, cAtModulePpp);

    return cAtOk;
    }

static eAtRet QueueEnable(AtChannel self, eBool enable)
    {
    return Tha60210012HdlcLinkQueueEnable(self, enable);
    }

static void DebugStickyFLowLookupShow(uint32 regValues, uint32 bitOffset, const char *regName)
    {
    eBool stickyIsRaised = mBinToBool(regValues & bitOffset);
    AtPrintc(cSevNormal, "   - %-30s: ", regName);
    AtPrintc(stickyIsRaised ? cSevCritical : cSevInfo, "%s\r\n", stickyIsRaised? "SET" : "CLEAR");
    }

static uint32 LinkProfileBaseAddress(void)
    {
    return cThaModuleEthFlowLookupBaseAddress;
    }

static ThaModuleCla ClaModule(AtPppLink self)
    {
    return (ThaModuleCla)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleCla);
    }

static eAtModulePppRet BcpLanFcsInsertionEnable(AtPppLink self, eBool enable)
    {
    AtPw pw = AtChannelBoundPwGet((AtChannel)self);
    AtEthFlow flow = AtHdlcLinkBoundFlowGet((AtHdlcLink)self);
    ThaModuleCla claModule = ClaModule(self);
    eBool fcsRemoved = enable ? cAtFalse : cAtTrue;

    /* PW Bridge */
    if (pw)
        return Tha60210012ModuleClaPwLanFcsRemoveEnable(claModule, (AtPw)ThaPwAdapterGet(pw), fcsRemoved);

    /* Vlan bridge */
    if (flow)
        return Tha60210012ModuleClaEthFlowLanFcsRemoveEnable(claModule, flow, fcsRemoved);

    return cAtErrorModeNotSupport;
    }

static eBool BcpLanFcsInsertionIsEnabled(AtPppLink self)
    {
    ThaModuleCla claModule = ClaModule(self);
    AtPw pw = AtChannelBoundPwGet((AtChannel)self);
    AtEthFlow flow = AtHdlcLinkBoundFlowGet((AtHdlcLink)self);
    eBool fcsRemoved = cAtTrue;

    /* PW Bridge */
    if (pw)
        fcsRemoved = Tha60210012ModuleClaPwLanFcsRemoveIsEnabled(claModule, (AtPw)ThaPwAdapterGet(pw));

    /* Vlan bridge */
    else if (flow)
        fcsRemoved = Tha60210012ModuleClaEthFlowLanFcsRemoveIsEnabled(claModule, flow);

    return fcsRemoved ? cAtFalse : cAtTrue;
    }

static eAtModulePwRet PduTypeSet(AtHdlcLink self, eAtHdlcPduType pduType)
    {
    Tha60210012PhysicalPppLinkFlowServiceTypeUpdate(self, AtHdlcLinkBoundFlowGet(self));
    return m_AtHdlcLinkMethods->PduTypeSet(self, pduType);
    }

static void OverrideAtChannel(AtPppLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, QueueEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaPppLink(AtPppLink self)
    {
    ThaPppLink link = (ThaPppLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPppLinkOverride, mMethodsGet(link), sizeof(m_ThaPppLinkOverride));

        mMethodOverride(m_ThaPppLinkOverride, DefaultOffset);
        mMethodOverride(m_ThaPppLinkOverride, PppPacketModeSet);
        mMethodOverride(m_ThaPppLinkOverride, PppPacketEnable);
        mMethodOverride(m_ThaPppLinkOverride, IgMlpppEnable);
        mMethodOverride(m_ThaPppLinkOverride, EgMlpppEnable);
        mMethodOverride(m_ThaPppLinkOverride, JoinBundle);
        mMethodOverride(m_ThaPppLinkOverride, MemberLocalIdInBundleSet);
        }

    mMethodsSet(link, &m_ThaPppLinkOverride);
    }

static void OverrideAtHdlcLink(AtPppLink self)
    {
    AtHdlcLink hdlcLink = (AtHdlcLink)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcLinkMethods = mMethodsGet(hdlcLink) ;
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcLinkOverride, m_AtHdlcLinkMethods, sizeof(m_AtHdlcLinkOverride));

        mMethodOverride(m_AtHdlcLinkOverride, ProtocolCompress);
        mMethodOverride(m_AtHdlcLinkOverride, ProtocolIsCompressed);
        mMethodOverride(m_AtHdlcLinkOverride, OamPacketModeSet);
        mMethodOverride(m_AtHdlcLinkOverride, OamPacketModeGet);
        mMethodOverride(m_AtHdlcLinkOverride, OamSend);
        mMethodOverride(m_AtHdlcLinkOverride, OamReceive);
        mMethodOverride(m_AtHdlcLinkOverride, PidByPass);
        mMethodOverride(m_AtHdlcLinkOverride, PduTypeSet);
        }

    mMethodsSet(hdlcLink, &m_AtHdlcLinkOverride);
    }

static void OverrideAtPppLink(AtPppLink self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPppLinkOverride, mMethodsGet(self), sizeof(m_AtPppLinkOverride));

        mMethodOverride(m_AtPppLinkOverride, TxProtocolFieldCompress);
        mMethodOverride(m_AtPppLinkOverride, TxProtocolFieldIsCompressed);
        mMethodOverride(m_AtPppLinkOverride, BcpLanFcsInsertionEnable);
        mMethodOverride(m_AtPppLinkOverride, BcpLanFcsInsertionIsEnabled);
        }

    mMethodsSet(self, &m_AtPppLinkOverride);
    }

static void OverrideThaPhysicalPppLink(AtPppLink self)
    {
    ThaPhysicalPppLink link = (ThaPhysicalPppLink)self;
    
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_PhysicalPppLinkMethods = mMethodsGet(link);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPhysicalPppLinkOverride, mMethodsGet(link), sizeof(m_ThaPhysicalPppLinkOverride));

        mMethodOverride(m_ThaPhysicalPppLinkOverride, BcpProfileSet);
        mMethodOverride(m_ThaPhysicalPppLinkOverride, ErrorProfileSet);
        mMethodOverride(m_ThaPhysicalPppLinkOverride, NetworkProfileSet);
        mMethodOverride(m_ThaPhysicalPppLinkOverride, OamProfileSet);
        }

    mMethodsSet(link, &m_ThaPhysicalPppLinkOverride);
    }

static void Override(AtPppLink self)
    {
    OverrideAtChannel(self);
    OverrideAtHdlcLink(self);
    OverrideThaPppLink(self);
    OverrideAtPppLink(self);
    OverrideThaPhysicalPppLink(self);	
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012PhysicalPppLink);
    }

AtPppLink Tha60210012PhysicalPppLinkObjectInit(AtPppLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPhysicalPppLinkObjectInit((AtPppLink)self, hdlcChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPppLink Tha60210012PhysicalPppLinkNew(AtHdlcChannel hdlcChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012PhysicalPppLinkObjectInit(newLink, hdlcChannel);
    }

eAtRet Tha60210012PhysicalPppLinkRxSequenceModeSet(AtHdlcLink self, eAtMpSequenceMode sequenceMode)
    {
    ThaHdlcChannel channel = (ThaHdlcChannel)AtHdlcLinkHdlcChannelGet(self);
    if ((channel == NULL) || (self == NULL))
        return cAtErrorNullPointer;

    if (sequenceMode == cAtMpSequenceModeUnknown)
        return cAtErrorModeNotSupport;

    if (ThaHdlcChannelCircuitIsHo(channel))
        return HoRxSequenceModeSet(self, sequenceMode);

    return LoRxSequenceModeSet(self, sequenceMode);
    }

uint8 Tha60210012PhysicalPppLinkRxSequenceModeGet(AtHdlcLink self)
    {
    ThaHdlcChannel channel = (ThaHdlcChannel)AtHdlcLinkHdlcChannelGet(self);

    if ((channel == NULL) || (self == NULL))
        return cAtErrorNullPointer;

    if (ThaHdlcChannelCircuitIsHo(channel))
        return HoRxSequenceModeGet(self);

    return LoRxSequenceModeGet(self);
    }

static void LinkFlowLookupDebug(AtHdlcLink self, eBool r2c)
    {
    uint32 offset = cThaModuleEthFlowLookupBaseAddress + AtChannelHwIdGet((AtChannel)self) + (uint32)(r2c * 4096);
    uint32 regAddress = offset + cAf6Reg_link_stk_Base;
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleEth);

    AtPrintc(cSevNormal, "\r\n* Ethernet Flow Look up Link Sticky");
    AtPrintc(cSevInfo, " (0x%08X: 0x%08x)\r\n", regAddress, regValue);

    DebugStickyFLowLookupShow(regValue, cBit0, "Control");
    DebugStickyFLowLookupShow(regValue, cBit1, "Pseudowire");
    DebugStickyFLowLookupShow(regValue, cBit2, "Data");
    DebugStickyFLowLookupShow(regValue, cBit3, "Corrupt");
    DebugStickyFLowLookupShow(regValue, cBit4, "Mlppp DControl");
    DebugStickyFLowLookupShow(regValue, cBit5, "Mlppp Control");
    DebugStickyFLowLookupShow(regValue, cBit6, "Mlppp Data");
    DebugStickyFLowLookupShow(regValue, cBit7, "Mlppp Drop");
    DebugStickyFLowLookupShow(regValue, cBit8, "FCS Error");
    DebugStickyFLowLookupShow(regValue, cBit9, "Abort packets");
    DebugStickyFLowLookupShow(regValue, cBit10, "Address Error");
    DebugStickyFLowLookupShow(regValue, cBit11, "Control Error");
    DebugStickyFLowLookupShow(regValue, cBit12, "Protocol Error");
    DebugStickyFLowLookupShow(regValue, cBit13, "Multilink Error");
    DebugStickyFLowLookupShow(regValue, cBit14, "MAC Error");
    DebugStickyFLowLookupShow(regValue, cBit15, "Lookup Error");
    DebugStickyFLowLookupShow(regValue, cBit16, "Rules Out Drop");
    }

eAtRet Tha60210012LinkOamProfileHwSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    uint32 offset = LinkProfileBaseAddress() + AtChannelHwIdGet((AtChannel)self);
    uint32 regAddress = cAf6Reg_upen_link_id_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    if (profile)
        {
        mRegFieldSet(regValue, cAf6_upen_link_id_OAM_Profile_Id_, ThaProfileProfileIdGet(profile));
        mRegFieldSet(regValue, cAf6_upen_link_id_OAM_FwdChk_En_, 1); /* Enable */
        }
    else
        mRegFieldSet(regValue, cAf6_upen_link_id_OAM_FwdChk_En_, 0); /* Disable */

    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);
    return cAtOk;
    }

eAtRet Tha60210012LinkNetworkProfileHwSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    uint32 offset = LinkProfileBaseAddress() + AtChannelHwIdGet((AtChannel)self);
    uint32 regAddress = cAf6Reg_upen_link_id_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    if (profile)
        {
        mRegFieldSet(regValue, cAf6_upen_link_id_NLP_Profile_Id_, ThaProfileProfileIdGet(profile));
        mRegFieldSet(regValue, cAf6_upen_link_id_NLP_FwdChk_En_, 1); /* Enable */
        }
    else
        mRegFieldSet(regValue, cAf6_upen_link_id_NLP_FwdChk_En_, 0); /* Disable */

    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);
    return cAtOk;
    }

eAtRet Tha60210012LinkErrorProfileHwSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    uint32 offset = LinkProfileBaseAddress() + AtChannelHwIdGet((AtChannel)self);
    uint32 regAddress = cAf6Reg_upen_link_id_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    if (profile)
        {
        mRegFieldSet(regValue, cAf6_upen_link_id_ERR_Profile_Id_, ThaProfileProfileIdGet(profile));
        mRegFieldSet(regValue, cAf6_upen_link_id_ERR_FwdChk_En_, 1); /* Enable */
        }
    else
        mRegFieldSet(regValue, cAf6_upen_link_id_ERR_FwdChk_En_, 0); /* Disable */

    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);
    return cAtOk;
    }

eAtRet Tha60210012LinkBcpProfileHwSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    uint32 offset = LinkProfileBaseAddress() + AtChannelHwIdGet((AtChannel)self);
    uint32 regAddress = cAf6Reg_upen_link_id_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    if (profile)
        {
        mRegFieldSet(regValue, cAf6_upen_link_id_BCP_Profile_Id_, ThaProfileProfileIdGet(profile));
        mRegFieldSet(regValue, cAf6_upen_link_id_BCP_FwdChk_En_, 1); /* Enable */
        }
    else
        mRegFieldSet(regValue, cAf6_upen_link_id_BCP_FwdChk_En_, 0); /* Disable */

    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);
    return cAtOk;
    }

void Tha60210012PhysicalPppLinkFlowServiceTypeUpdate(AtHdlcLink link, AtEthFlow flow)
    {
    uint8 serviceType;
    const uint8 cPppService = 9; /* PPP data */
    const uint8 cPppBridgeService = 7; /* PPP bridged */

    serviceType = (uint8)((AtHdlcLinkPduTypeGet(link) == cAtHdlcPduTypeEthernet) ? cPppBridgeService : cPppService);
    Tha60210012EthFlowServiceTypeCache(flow, serviceType);
    }

eAtRet Tha60210012EncapLinkDebug(AtHdlcLink self)
    {
    ThaHdlcChannel hdlcChannel = (ThaHdlcChannel)AtHdlcLinkHdlcChannelGet(self);
    uint32 offset, address;

    AtPrintc(cSevNormal, "\r\n* Link mapping: \r\n");
    AtPrintc(cSevInfo, "   - Link HW ID: %u\r\n", AtChannelHwIdGet((AtChannel)self));

    if (ThaHdlcChannelCircuitIsHo(hdlcChannel))
        {
        offset = Tha60210012ModuleDecapHoBaseAddress() + DefaultOffset((ThaPppLink)self);
        address = cAf6Reg_upen_cidcfg_HoCfg_Base + offset;
        ThaDeviceRegNameDisplay("Ho-order Channel ID Configure For Parser");
        ThaDeviceChannelRegValueDisplay((AtChannel)self, address, cAtModuleEncap);
        }
    else
        {
        offset = Tha60210012ModuleDecapLoBaseAddress() + DefaultOffset((ThaPppLink)self);
        address = cAf6Reg_upen_cidcfg_locfg_Base + offset;
        ThaDeviceRegNameDisplay("Lo-order Channel ID Configure For Parser");
        ThaDeviceChannelRegValueDisplay((AtChannel)self, address, cAtModuleEncap);
        }

    LinkFlowLookupDebug(self, cAtTrue);

    return cAtOk;
    }

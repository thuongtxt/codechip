/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : Tha60210012PidTable.h
 * 
 * Created Date: Apr 7, 2016
 *
 * Description : PID Table
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PIDTABLE_H_
#define _THA60210012PIDTABLE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
/* ETH Type */
#define cThaPidEthernetTypeIpV4                 0x0800
#define cThaPidEthernetTypeIpV6                 0x86DD
#define cThaPidEthernetTypeMPLSUnicast          0x8847
#define cThaPidEthernetTypeMPLSMulticastPacket  0x8848
#define cThaPidEthernetTypeIS_ISPacket          0xFEFE
#define cThaPidEthernetTypeAppleTalkPacket      0x809B
#define cThaPidEthernetTypeNovelIPXPacket       0x8137
#define cThaPidEthernetTypeXeroxNSIDPPacket     0x0600
#define cThaPidEthernetTypeCiscosystemPacket    0x8929
#define cThaPidEthernetTypeCiscoDiscorProPacket 0x2000
#define cThaPidEthernetTypeNetBiosFramingPacket 0x888E
#define cThaPidEthernetTypeIpV6HdrCompresPacket 0x8149
#define cThaPidEthernetType802_1DPacket         0x8863
#define cThaPidEthernetTypeBPDUPacket           0x4242
#define cThaPidEthernetTypeLanbridgePacket      0x8038
#define cThaPidEthernetTypeBCPPacket            0x6558

/* PID */
#define cThaPidIpV4                 0x0021
#define cThaPidIpV6                 0x0057
#define cThaPidMPLSUnicast          0x0281
#define cThaPidMPLSMulticastPacket  0x0283
#define cThaPidIS_ISPacket          0x0023
#define cThaPidAppleTalkPacket      0x0029
#define cThaPidNovelIPXPacket       0x002b
#define cThaPidXeroxNSIDPPacket     0x0025
#define cThaPidCiscosystemPacket    0x0041
#define cThaPidCiscoDiscorProPacket 0x0207
#define cThaPidNetBiosFramingPacket 0x003F
#define cThaPidIpV6HdrCompresPacket 0x004F
#define cThaPid802_1DPacket         0x0201
#define cThaPidBPDUPacket           0x0203
#define cThaPidLanbridgePacket      0x0205
#define cThaPidBCPPacket            0x0031

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPidTable Tha60210012PidTableNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PIDTABLE_H_ */


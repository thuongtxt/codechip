/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60210012PidTable.c
 *
 * Created Date: Apr 7, 2016
 *
 * Description : PID Table entry
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/ppp/AtPidTableInternal.h"
#include "Tha60210012PidTableEntry.h"
#include "Tha60210012PidTable.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012PidTable
    {
    tAtPidTable super;
    }tTha60210012PidTable;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPidTableMethods m_AtPidTableOverride;

/* To save super implementation */
static const tAtPidTableMethods *m_AtPidTableMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPidTableEntry EntryObjectCreate(AtPidTable self, uint32 entryIndex)
    {
    return Tha60210012PidTableEntryNew(self, entryIndex);
    }

static uint32 MaxNumEntries(AtPidTable self)
    {
    AtUnused(self);
    return 16;
    }

static uint32 EntryDefaultPid(uint32 entryIndex)
    {
    if (entryIndex == 0)  return cThaPidIpV4;
    if (entryIndex == 1)  return cThaPidIpV6;
    if (entryIndex == 2)  return cThaPidMPLSUnicast;
    if (entryIndex == 3)  return cThaPidMPLSMulticastPacket;
    if (entryIndex == 4)  return cThaPidIS_ISPacket;
    if (entryIndex == 5)  return cThaPidAppleTalkPacket;
    if (entryIndex == 6)  return cThaPidNovelIPXPacket;
    if (entryIndex == 7)  return cThaPidXeroxNSIDPPacket;
    if (entryIndex == 8)  return cThaPidCiscosystemPacket;
    if (entryIndex == 9)  return cThaPidCiscoDiscorProPacket;
    if (entryIndex == 10) return cThaPidNetBiosFramingPacket;
    if (entryIndex == 11) return cThaPidIpV6HdrCompresPacket;
    if (entryIndex == 12) return cThaPid802_1DPacket;
    if (entryIndex == 13) return cThaPidBPDUPacket;
    if (entryIndex == 14) return cThaPidLanbridgePacket;
    if (entryIndex == 15) return cThaPidBCPPacket;

    return cInvalidUint32;
    }

static uint32 EntryDefaultEthType(uint32 entryIndex)
    {
    if (entryIndex == 0)  return cThaPidEthernetTypeIpV4;
    if (entryIndex == 1)  return cThaPidEthernetTypeIpV6;
    if (entryIndex == 2)  return cThaPidEthernetTypeMPLSUnicast;
    if (entryIndex == 3)  return cThaPidEthernetTypeMPLSMulticastPacket;
    if (entryIndex == 4)  return cThaPidEthernetTypeIS_ISPacket;
    if (entryIndex == 5)  return cThaPidEthernetTypeAppleTalkPacket;
    if (entryIndex == 6)  return cThaPidEthernetTypeNovelIPXPacket;
    if (entryIndex == 7)  return cThaPidEthernetTypeXeroxNSIDPPacket;
    if (entryIndex == 8)  return cThaPidEthernetTypeCiscosystemPacket;
    if (entryIndex == 9)  return cThaPidEthernetTypeCiscoDiscorProPacket;
    if (entryIndex == 10) return cThaPidEthernetTypeNetBiosFramingPacket;
    if (entryIndex == 11) return cThaPidEthernetTypeIpV6HdrCompresPacket;
    if (entryIndex == 12) return cThaPidEthernetType802_1DPacket;
    if (entryIndex == 13) return cThaPidEthernetTypeBPDUPacket;
    if (entryIndex == 14) return cThaPidEthernetTypeLanbridgePacket;
    if (entryIndex == 15) return cThaPidEthernetTypeBCPPacket;

    return cInvalidUint32;
    }

static eAtRet EntryPsnToTdmInit(AtPidTableEntry entry, uint32 entryIndex)
    {
    AtUnused(entry);
    AtUnused(entryIndex);
    /* Update later when hardware support it */
    return cAtOk;
    }

static eAtRet EntryTdmToPsnInit(AtPidTableEntry entry, uint32 entryIndex)
    {
    eAtRet ret = cAtOk;
    uint32 ethTypeValue, pidValue;

    ethTypeValue = EntryDefaultEthType(entryIndex);
    pidValue     = EntryDefaultPid(entryIndex);
    ret |= AtPidTableEntryTdmToPsnPidSet(entry, pidValue);
    ret |= AtPidTableEntryTdmToPsnEthTypeSet(entry, ethTypeValue);

    return ret;
    }

static eAtRet EntryDefaultSet(AtPidTable self)
    {
    eAtRet ret = cAtOk;
    uint32 numEntries = AtPidTableMaxNumEntries(self);
    uint32 entry_i;

    for (entry_i = 0; entry_i < numEntries; entry_i++)
        {
        AtPidTableEntry entry = AtPidTableEntryGet(self, entry_i);
        if (!AtPidTableEntryIsEditable(self, entry_i))
            continue;

        ret |= EntryPsnToTdmInit(entry, entry_i);
        ret |= EntryTdmToPsnInit(entry, entry_i);
        }

    return ret;
    }

static eBool NeedDefaultSettings(AtPidTable self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtPidTable(AtPidTable self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPidTableMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPidTableOverride, m_AtPidTableMethods, sizeof(m_AtPidTableOverride));

        mMethodOverride(m_AtPidTableOverride, EntryObjectCreate);
        mMethodOverride(m_AtPidTableOverride, EntryDefaultSet);
        mMethodOverride(m_AtPidTableOverride, NeedDefaultSettings);
        mMethodOverride(m_AtPidTableOverride, MaxNumEntries);
        }

    mMethodsSet(self, &m_AtPidTableOverride);
    }

static void Override(AtPidTable self)
    {
    OverrideAtPidTable(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012PidTable);
    }

static AtPidTable ObjectInit(AtPidTable self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPidTableObjectInit((AtPidTable)self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPidTable Tha60210012PidTableNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPidTable table = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (table == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(table, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP/MLPPP
 * 
 * File        : Tha60210012PhysicalPppLink.h
 * 
 * Created Date: Mar 16, 2016
 *
 * Description : PPP link interface of 60210012 product.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PHYSICALPPPLINK_H_
#define _THA60210012PHYSICALPPPLINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtMpBundle.h"
#include "../../../default/ppp/dynamic/ThaPhysicalPppLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mModuleSur(self) ((ThaModuleHardSur)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleSur))

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012PhysicalPppLink * Tha60210012PhysicalPppLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPppLink Tha60210012PhysicalPppLinkNew(AtHdlcChannel hdlcChannel);

eAtRet Tha60210012PhysicalPppLinkRxSequenceModeSet(AtHdlcLink self, eAtMpSequenceMode sequenceMode);
uint8 Tha60210012PhysicalPppLinkRxSequenceModeGet(AtHdlcLink self);
void Tha60210012PhysicalPppLinkFlowServiceTypeUpdate(AtHdlcLink logicLink, AtEthFlow flow);
eAtRet Tha60210012EncapLinkDebug(AtHdlcLink self);

eAtRet Tha60210012LinkErrorProfileHwSet(ThaPhysicalPppLink self, ThaProfile profile);
eAtRet Tha60210012LinkNetworkProfileHwSet(ThaPhysicalPppLink self, ThaProfile profile);
eAtRet Tha60210012LinkOamProfileHwSet(ThaPhysicalPppLink self, ThaProfile profile);
eAtRet Tha60210012LinkBcpProfileHwSet(ThaPhysicalPppLink self, ThaProfile profile);
eAtRet Tha60210012PhysicalLinkNetworkActiveProfileSet(ThaPhysicalPppLink self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PHYSICALPPPLINK_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : Tha60210012MpBundle.h
 * 
 * Created Date: Mar 15, 2016
 *
 * Description : MLPPP interface of 60210012 product.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MPBUNDLE_H_
#define _THA60210012MPBUNDLE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/encap/resequence/AtResequenceEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012MpBundle * Tha60210012MpBundle;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtMpBundle Tha60210012MpBundleNew(uint32 channelId, AtModule module);
eAtRet Tha60210012MpBundleRsqEngineActivate(AtHdlcBundle self, AtEthFlow flow);
eAtRet Tha60210012MpBundleRsqEngineDeactivate(AtHdlcBundle self, AtEthFlow flow);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MPBUNDLE_H_ */


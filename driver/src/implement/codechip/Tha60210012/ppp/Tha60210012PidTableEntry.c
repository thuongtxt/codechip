/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60210012PidTableEntry.c
 *
 * Created Date: Apr 7, 2016
 *
 * Description : PID Table entry
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/ppp/AtPidTableEntryInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../encap/Tha60210012ModuleDecapParserReg.h"
#include "../encap/Tha60210012ModuleEncap.h"
#include "../pwe/Tha60210012ModulePweReg.h"
#include "../pwe/Tha60210012ModulePwe.h"
#include "Tha60210012PidTableEntry.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012PidTableEntry
    {
    tAtPidTableEntry super;
    }tTha60210012PidTableEntry;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPidTableEntryMethods m_AtPidTableEntryOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePpp ModulePpp(AtPidTableEntry self)
    {
    return (AtModulePpp)AtPidTableModuleGet(AtPidTableEntryTableGet(self));
    }

static AtModuleEncap ModuleEncap(AtPidTableEntry self)
    {
    return (AtModuleEncap)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)ModulePpp(self)), cAtModuleEncap);
    }

static uint32 MaxNumLOSlice(AtPidTableEntry self)
    {
    return Tha60210012ModuleEncapNumSlice24(ModuleEncap(self));
    }

static uint32 MaxNumHOSlice(AtPidTableEntry self)
    {
    return Tha60210012ModuleEncapNumSlice48(ModuleEncap(self));
    }

static uint32 LoDecapBaseAddress(uint8 slice)
    {
    return (uint32)(Tha60210012ModuleDecapLoBaseAddress() + (uint32)(0x4000 * slice));
    }

static uint32 HoDecapBaseAddress(uint8 slice)
    {
    return (uint32)(Tha60210012ModuleDecapHoBaseAddress() + (uint32)(0x1000 * slice));
    }

static uint32 Read(AtPidTableEntry self, uint32 address, eAtModule moduleId)
    {
    uint8 part_i;
    ThaDevice device;
    AtModule moduleToRead;

    device = (ThaDevice)AtModuleDeviceGet((AtModule)ModulePpp(self));
    moduleToRead = AtDeviceModuleGet((AtDevice)device, moduleId);
    for (part_i = 0; part_i < ThaDeviceMaxNumParts(device); part_i++)
        {
        uint32 offset = ThaDeviceModulePartOffset(device, moduleId, part_i);
        return mModuleHwRead(moduleToRead, address + offset);
        }

    return 0;
    }

static void Write(AtPidTableEntry self, uint32 address, uint32 value, eAtModule moduleId)
    {
    uint8 part_i;
    ThaDevice device;
    AtModule moduleToWrite;

    device = (ThaDevice)AtModuleDeviceGet((AtModule)ModulePpp(self));
    moduleToWrite = AtDeviceModuleGet((AtDevice)device, moduleId);
    for (part_i = 0; part_i < ThaDeviceMaxNumParts(device); part_i++)
        {
        uint32 offset = ThaDeviceModulePartOffset(device, moduleId, part_i);
        mModuleHwWrite(moduleToWrite, address + offset, value);
        }
    }

static eBool Editable(AtPidTableEntry self)
    {
    AtPidTable table = AtPidTableEntryTableGet(self);
    return AtPidTableEntryIsEditable(table, AtPidTableEntryIndexGet(self));
    }

static eAtRet PsnToTdmPidSet(AtPidTableEntry self, uint32 pid)
    {
    AtUnused(self);
    AtUnused(pid);
    return cAtErrorModeNotSupport;
    }

static eAtRet PsnToTdmEthTypeSet(AtPidTableEntry self, uint32 ethType)
    {
    AtUnused(self);
    AtUnused(ethType);
    return cAtErrorModeNotSupport;
    }

static eAtRet PsnToTdmEnable(AtPidTableEntry self, eBool enable)
    {
    if (!Editable(self))
        return cAtErrorNotEditable;
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static uint32 PsnToTdmPidGet(AtPidTableEntry self)
    {
    AtUnused(self);
    return 0xFFFF;
    }

static uint32 PsnToTdmEthTypeGet(AtPidTableEntry self)
    {
    AtUnused(self);
    return 0xFFFF;
    }

static eBool PsnToTdmIsEnabled(AtPidTableEntry self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet TdmToPsnPidSet(AtPidTableEntry self, uint32 pid)
    {
    uint32 hwEntry = AtPidTableEntryIndexGet(self);
    uint32 regVal, regAddr, baseAddress;
    uint8 slice_i;

    if (!Editable(self))
        return cAtErrorNotEditable;

    /* LO */
    for (slice_i = 0; slice_i < MaxNumLOSlice(self); slice_i++)
        {
        baseAddress = LoDecapBaseAddress(slice_i) + hwEntry;
        regAddr = cAf6Reg_upen_ppppid_locfg_Base + baseAddress;
        regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
        mRegFieldSet(regVal, cAf6_upen_ppppid_locfg_ppppid_val_, pid);
        AtPidTableEntryWrite(self, regAddr, regVal, cAtModulePpp);
        }

    /* HO */
    for (slice_i = 0; slice_i < MaxNumHOSlice(self); slice_i++)
        {
        baseAddress = HoDecapBaseAddress(slice_i) + hwEntry;
        regAddr = cAf6Reg_upen_ppppid_HoCfg_Base + baseAddress;
        regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
        mRegFieldSet(regVal, cAf6_upen_ppppid_HoCfg_ppppid_val_, pid);
        AtPidTableEntryWrite(self, regAddr, regVal, cAtModulePpp);
        }

    return cAtOk;
    }

static Tha60210012ModulePwe ModulePwe(AtPidTableEntry self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ModulePpp(self));
    return (Tha60210012ModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static eAtRet TdmToPsnEthTypeSet(AtPidTableEntry self, uint32 ethType)
    {
    uint32 regVal, regAddr, baseAddress;
    uint8 slice_i;

    if (!Editable(self))
        return cAtErrorNotEditable;

    /* LO */
    for (slice_i = 0; slice_i < MaxNumLOSlice(self); slice_i++)
        {
        baseAddress = LoDecapBaseAddress(slice_i) + AtPidTableEntryIndexGet(self);
        regAddr = cAf6Reg_upen_ethtyp_locfg_Base + baseAddress;
        regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
        mRegFieldSet(regVal, cAf6_upen_ethtyp_locfg_ethtyp_val_, ethType);
        AtPidTableEntryWrite(self, regAddr, regVal, cAtModulePpp);
        }

    /* HO */
    for (slice_i = 0; slice_i < MaxNumHOSlice(self); slice_i++)
        {
        baseAddress = HoDecapBaseAddress(slice_i) + AtPidTableEntryIndexGet(self);
        regAddr = cAf6Reg_upen_ethtyp_HoCfg_Base + baseAddress;
        regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
        mRegFieldSet(regVal, cAf6_upen_ethtyp_HoCfg_ethtyp_val_, ethType);
        AtPidTableEntryWrite(self, regAddr, regVal, cAtModulePpp);
        }

    return Tha60210012ModulePweTdmToPsnEthTypeSet(ModulePwe(self), AtPidTableEntryIndexGet(self), ethType);
    }

static eAtRet TdmToPsnEnable(AtPidTableEntry self, eBool enable)
    {
    if (!Editable(self))
        return cAtErrorNotEditable;
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static uint32 TdmToPsnPidGet(AtPidTableEntry self)
    {
    uint32 regAddr = cAf6Reg_upen_ppppid_locfg_Base + AtPidTableEntryIndexGet(self) + LoDecapBaseAddress(0);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    return mRegField(regVal, cAf6_upen_ppppid_locfg_ppppid_val_);
    }

static uint32 TdmToPsnEthTypeGet(AtPidTableEntry self)
    {
    uint32 regAddr = cAf6Reg_upen_ethtyp_locfg_Base + AtPidTableEntryIndexGet(self) + LoDecapBaseAddress(0);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    return mRegField(regVal, cAf6_upen_ethtyp_locfg_ethtyp_val_);
    }

static eBool TdmToPsnIsEnabled(AtPidTableEntry self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet TdmToPsnPidMaskSet(AtPidTableEntry self, uint16 pidMask)
    {
    uint32 regVal, regAddr, baseAddress;
    uint8 slice_i;

    if (!Editable(self))
        return cAtErrorNotEditable;

    /* LO */
    for (slice_i = 0; slice_i < MaxNumLOSlice(self); slice_i++)
        {
        baseAddress = LoDecapBaseAddress(slice_i) + AtPidTableEntryIndexGet(self);
        regAddr = cAf6Reg_upen_ppppid_locfg_Base + baseAddress;
        regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
        mRegFieldSet(regVal, cAf6_upen_ppppid_locfg_ppppid_mask_, pidMask);
        AtPidTableEntryWrite(self, regAddr, regVal, cAtModulePpp);
        }

    /* HO */
    for (slice_i = 0; slice_i < MaxNumHOSlice(self); slice_i++)
        {
        baseAddress = HoDecapBaseAddress(slice_i) + AtPidTableEntryIndexGet(self);
        regAddr = cAf6Reg_upen_ppppid_HoCfg_Base + baseAddress;
        regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
        mRegFieldSet(regVal, cAf6_upen_ppppid_HoCfg_ppppid_mask_, pidMask);
        AtPidTableEntryWrite(self, regAddr, regVal, cAtModulePpp);
        }

    return cAtOk;
    }

static uint32 TdmToPsnPidMaskGet(AtPidTableEntry self)
    {
    uint32 regAddr = cAf6Reg_upen_ppppid_locfg_Base + AtPidTableEntryIndexGet(self) + LoDecapBaseAddress(0);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    return mRegField(regVal, cAf6_upen_ppppid_locfg_ppppid_mask_);
    }

static void OverrideAtPidEntryTable(AtPidTableEntry self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPidTableEntryOverride, mMethodsGet(self), sizeof(m_AtPidTableEntryOverride));

        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmPidSet);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmEthTypeSet);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmEnable);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmPidGet);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmEthTypeGet);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmIsEnabled);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnPidSet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnEthTypeSet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnEnable);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnEthTypeGet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnIsEnabled);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnPidGet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnPidMaskSet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnPidMaskGet);
        mMethodOverride(m_AtPidTableEntryOverride, Read);
        mMethodOverride(m_AtPidTableEntryOverride, Write);
        }

    mMethodsSet(self, &m_AtPidTableEntryOverride);
    }

static void Override(AtPidTableEntry self)
    {
    OverrideAtPidEntryTable(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012PidTableEntry);
    }

static AtPidTableEntry ObjectInit(AtPidTableEntry self, uint32 entryIndex, AtPidTable table)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPidTableEntryObjectInit(self, entryIndex, table) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPidTableEntry Tha60210012PidTableEntryNew(AtPidTable self, uint32 entryIndex)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPidTableEntry entry = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (entry == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(entry, entryIndex, self);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : Tha60210012PidTableEntry.h
 * 
 * Created Date: Apr 7, 2016
 *
 * Description : PID Table
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PIDTABLEENTRY_H_
#define _THA60210012PIDTABLEENTRY_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPidTableEntry Tha60210012PidTableEntryNew(AtPidTable self, uint32 entryIndex);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PIDTABLEENTRY_H_ */


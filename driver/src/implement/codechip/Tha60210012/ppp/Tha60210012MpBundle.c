/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60210012PppBundle.c
 *
 * Created Date: Mar 14, 2016
 *
 * Description : MLPPP bundle of 60210012 product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHdlcChannel.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/ppp/ThaStmMpBundleInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/encap/dynamic/ThaDynamicHdlcLink.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../mpeg/Tha60210012MpegReg.h"
#include "../mpeg/Tha60210012ModuleMpeg.h"
#include "../cla/Tha60210012ModuleCla.h"
#include "../ppp/Tha60210012PhysicalPppLink.h"
#include "../encap/Tha60210012PhysicalHdlcChannel.h"
#include "../encap/resequence/Tha60210012ResequenceManager.h"
#include "../encap/Tha60210012ModuleEncap.h"
#include "../eth/Tha60210012EthFlow.h"
#include "../eth/Tha60210012ModuleEthFlowLookupReg.h"
#include "../ppp/Tha60210012ModulePpp.h"
#include "Tha60210012MpBundle.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)          ((Tha60210012MpBundle)self)
#define mDefaultOffset(self) mMethodsGet((ThaMpBundle)self)->DefaultOffset((ThaMpBundle)self)

#define mApiCheck(apiCheck)        \
    do                             \
        {                          \
        ret = apiCheck;            \
        if (ret != cAtOk)          \
            return ret;            \
        } while(0)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012MpBundle
    {
    tThaStmMpBundle super;

    /* Private data */
    eBool txEnable;
    eBool rxEnable;
    uint8 txseqNumberMode;
    uint8 rxseqNumberMode;
    uint32 mrru;
    uint8 maxDelay;
    uint8 pduType;
    AtResequenceEngine rsqEngine;
    }tTha60210012MpBundle;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;
static tAtChannelMethods   m_AtChannelOverride;
static tAtMpBundleMethods  m_AtMpBundleOverride;
static tThaMpBundleMethods m_ThaMpBundleOverride;
static tAtHdlcBundleMethods   m_AtHdlcBundleOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods  *m_AtChannelMethods  = NULL;
static const tAtMpBundleMethods *m_AtMpBundleMethods = NULL;
static const tThaMpBundleMethods *m_ThaMpBundleMethods = NULL;
static const tAtHdlcBundleMethods *m_AtHdlcBundleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleMpeg MpegModule(AtMpBundle self)
    {
    return (ThaModuleMpeg)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleMpeg);
    }

static uint32 MpegBaseAddress(AtMpBundle self)
    {
    return Tha60210012ModuleMpegBaseAddress((Tha60210012ModuleMpeg)MpegModule(self));
    }

static eAtRet LinkInBundleTxEnable(AtChannel link, eBool enable)
    {
    AtHdlcBundle bundle = AtHdlcLinkBundleGet((AtHdlcLink)link);
    uint32 linkId = AtChannelHwIdGet(link);
    uint32 regAddr = MpegBaseAddress((AtMpBundle)bundle) + cAf6Reg_memidcfg0_pen(linkId);
    uint32 regVal;

    regVal = mChannelHwRead(link, regAddr, cThaModuleMpeg);
    mRegFieldSet(regVal, cAf6_memidcfg0_pen_MemBundle_En_, enable ? 1 : 0);
    mChannelHwWrite(link, regAddr, regVal, cThaModuleMpig);

    return cAtOk;
    }

static uint32 HwIdGet(AtChannel self)
    {
    return self->channelId;
    }

static eAtRet AllTxMlpppTrafficEnable(AtMpBundle self, eBool enable)
    {
    AtChannel flow;
    eAtRet ret = cAtOk;
    AtIterator flowIterator = AtMpBundleFlowIteratorCreate(self);

    while ((flow = (AtChannel)AtIteratorNext(flowIterator)) != NULL)
        ret |= ThaEthFlowRxTrafficEnable((ThaEthFlow)flow, enable);

    AtObjectDelete((AtObject)flowIterator);
    return ret;
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    AtChannel link;
    AtIterator linkIterator = AtHdlcBundleLinkIteratorCreate((AtHdlcBundle)self);

    /* Enable all links was bound to the bundle. */
    while ((link = (AtChannel)AtIteratorNext(linkIterator)) != NULL)
        ret |= LinkInBundleTxEnable(link, enable);

    AtObjectDelete((AtObject)linkIterator);
    AllTxMlpppTrafficEnable((AtMpBundle)self, enable);
    mThis(self)->txEnable = enable;

    return cAtOk;
    }

static eBool TxIsEnabled(AtChannel self)
    {
    return mThis(self)->txEnable;
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    mThis(self)->rxEnable = enable;
    return cAtOk;
    }

static eBool RxIsEnabled(AtChannel self)
    {
    return mThis(self)->rxEnable;
    }

static eBool ShouldReadCounterFromSurModule(AtChannel self)
    {
    ThaModulePpp pppModule = (ThaModulePpp)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePpp);

    if (pppModule == NULL)
        return cAtFalse;

    return ThaModulePppShouldReadCountersFromSurModule(pppModule);
    }

static eAtRet MlpppCounterMpigGet(AtChannel self, tAtHdlcBundleCounters *pCounters, eBool r2c)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (ShouldReadCounterFromSurModule(self))
        {
        ThaModuleHardSur surModule = mModuleSur(self);

        pCounters->rxGoodByte = ThaModuleHardSurMpBundleRxGoodByteGet(surModule, self, r2c);
        pCounters->rxGoodPkt = ThaModuleHardSurMpBundleRxGoodPktGet(surModule, self, r2c);
        pCounters->rxDiscardPkt = ThaModuleHardSurMpBundleRxDiscardPktGet(surModule, self, r2c);
        pCounters->rxFragmentPkt = ThaModuleHardSurMpBundleRxFragmentPktGet(surModule, self, r2c);
        }

    return cAtOk;
    }

static eAtRet MlpppCounterMpegGet(AtChannel self, tAtHdlcBundleCounters *pCounters, eBool r2c)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (ShouldReadCounterFromSurModule(self))
        {
        ThaModuleHardSur surModule = mModuleSur(self);

        pCounters->txGoodByte = ThaModuleHardSurMpBundleTxGoodByteGet(surModule, self, r2c);
        pCounters->txGoodPkt = ThaModuleHardSurMpBundleTxGoodPktGet(surModule, self, r2c);
        pCounters->txDiscardPkt = ThaModuleHardSurMpBundleTxDiscardPktGet(surModule, self, r2c);
        pCounters->txFragmentPkt = ThaModuleHardSurMpBundleTxFragmentPktGet(surModule, self, r2c);
        }

    return cAtOk;
    }

static eAtRet AllCountersReadToClear(AtChannel self, void *pAllCounters, eBool clear)
    {
    eAtRet ret;
    AtOsal osal;

    /* NULL counter structure has not been handled yet */
    if (pAllCounters == NULL)
        return cAtErrorNullPointer;

    /* Init value counter */
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, pAllCounters, 0, sizeof(tAtHdlcBundleCounters));

    /* MPIG */
    ret = MlpppCounterMpigGet(self, (tAtHdlcBundleCounters *)pAllCounters, clear);
    if (ret != cAtOk)
        return cAtError;

    /* MPEG */
    ret = MlpppCounterMpegGet(self, (tAtHdlcBundleCounters *)pAllCounters, clear);
    if (ret != cAtOk)
        return cAtError;

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtTrue);
    }

static eAtHdlcBundleSchedulingMode SchedulingModeGet(AtHdlcBundle self)
    {
    uint32 regAddr, regValue;
    uint8 fieldVal;

    regAddr = MpegBaseAddress((AtMpBundle)self) + cAf6Reg_lididcfg1_pen_Base + mDefaultOffset(self);
    regValue = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    fieldVal = (uint8)mRegField(regValue, cAf6_lididcfg1_pen_LinkDisMode_);

    if (fieldVal == 0) return cAtHdlcBundleSchedulingModeRandom;
    if (fieldVal == 1) return cAtHdlcBundleSchedulingModeFairRR;
    if (fieldVal == 2) return cAtHdlcBundleSchedulingModeStrictRR;
    if (fieldVal == 3) return cAtHdlcBundleSchedulingModeSmartRR;

    return cAtHdlcBundleSchedulingModeRandom;
    }

static uint8 HwSchedulingMode(eAtHdlcBundleSchedulingMode schedulingMode)
    {
    if (schedulingMode == cAtHdlcBundleSchedulingModeFairRR)    return 1;
    if (schedulingMode == cAtHdlcBundleSchedulingModeStrictRR)  return 2;
    if (schedulingMode == cAtHdlcBundleSchedulingModeSmartRR)   return 3;
    return 0;
    }

static eAtRet SchedulingModeSet(AtHdlcBundle self, eAtHdlcBundleSchedulingMode schedulingMode)
    {
    uint32 regAddr, regValue;

    regAddr = MpegBaseAddress((AtMpBundle)self) + cAf6Reg_lididcfg1_pen_Base + mDefaultOffset(self);
    regValue = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    mRegFieldSet(regValue, cAf6_lididcfg1_pen_LinkDisMode_, HwSchedulingMode(schedulingMode));
    mChannelHwWrite(self, regAddr, regValue, cThaModuleMpeg);

    return cAtOk;
    }

static uint32 LinkRateInBytesPerMs(AtHdlcLink link)
    {
    AtHdlcChannel channel = AtHdlcLinkHdlcChannelGet(link);
    AtChannel physical;

    physical = AtEncapChannelBoundPhyGet((AtEncapChannel)channel);
    return AtChannelDataRateInBytesPerMs(physical);
    }

static uint32 TotalBandwidthOfAllLinksInBytePerMs(AtHdlcBundle self)
    {
    uint16 linkId;
    uint32 totalBandwidth = 0;

    for (linkId = 0; linkId < AtHdlcBundleNumberOfLinksGet(self); linkId++)
        totalBandwidth = totalBandwidth + LinkRateInBytesPerMs(AtHdlcBundleLinkGet(self, linkId));

    return totalBandwidth;
    }

static uint32 LinkDelayCalculate(AtHdlcBundle self, AtHdlcLink link)
    {
    /* Maximum delay is maximum time waiting expected sequence numbers, when timer expired DM, SE have will
     * declare is lost.
     * DM (ms) = Maximum link delay (ms) + (MRU * 8 * 1000) / Link_rate (bps).
     * Exam: Links rate is 8xDs0, MRU = 1024 bytes, Maximum link delay = 30ms.
     * DM = 30ms + (1024*8*1000) / (8*64K) = 46ms
     */
    const uint32 cLinkMru = 1500;
    uint32 maxLinkDelay = AtHdlcBundleMaxDelayGet(self);

    return maxLinkDelay + (cLinkMru / LinkRateInBytesPerMs(link));
    }

static uint32 ResequenceTimeoutCalculate(AtHdlcBundle self)
    {
    /* As HW suggested, re-sequence timeout is minimum of all links delay */
    uint32 timeout = cBit31_0;
    AtIterator linkIterator = AtHdlcBundleLinkIteratorCreate(self);
    AtHdlcLink link;

    while ((link = (AtHdlcLink)AtIteratorNext(linkIterator)) != NULL)
        {
        uint32 linkDelay = LinkDelayCalculate(self, link);
        if (linkDelay < timeout)
            timeout = linkDelay;
        }

    AtObjectDelete((AtObject)linkIterator);
    return timeout;
    }

static uint32 ResequenceThresholdCalculate(AtHdlcBundle self)
    {
    uint32 maxLinkDelay      = ResequenceTimeoutCalculate(self);
    uint32 physicalBandwidth = TotalBandwidthOfAllLinksInBytePerMs(self);
    static const uint8 cFrameAverageSize = 64;

    /* A Formula of hardware:
     * BT = Flow_rate (Mbps) * Maximum Delay (ms) / (frame_avg_size * 8 * 1000).
     * Exam: MLPPP bundle have 8 E1 links, Maximum delay is 40ms, frame_avg_size(default) = 64 bytes
     *       BT = 16M * 40 / (64 * 8 * 1000) = 1250.
     */
    return ((physicalBandwidth * maxLinkDelay) / (uint32)(cFrameAverageSize));
    }

static eBool SequenceModeIsValid(AtMpBundle self, eAtMpSequenceMode sequenceMode)
    {
    AtUnused(self);
    if ((sequenceMode == cAtMpSequenceModeLong) ||
        (sequenceMode == cAtMpSequenceModeShort))
        return cAtTrue;
    return cAtFalse;
    }

static AtResequenceEngine ResequenceEngine(AtMpBundle self)
    {
    return mThis(self)->rsqEngine;
    }

static eAtModulePppRet RxSequenceModeSet(AtMpBundle self, eAtMpSequenceMode sequenceMode)
    {
    uint8 linkId;
    eAtRet ret = cAtOk;

    if (!SequenceModeIsValid(self, sequenceMode))
        return cAtErrorModeNotSupport;

    for (linkId = 0; linkId < AtHdlcBundleNumberOfLinksGet((AtHdlcBundle)self); linkId++)
        {
        AtHdlcLink link = AtHdlcBundleLinkGet((AtHdlcBundle)self, linkId);
        ret |= Tha60210012PhysicalPppLinkRxSequenceModeSet(link, sequenceMode);
        }

    mThis(self)->rxseqNumberMode = sequenceMode;
    return ret;
    }

static ThaModuleCla ClaModule(AtMpBundle self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static eAtModulePppRet TxSequenceModeSet(AtMpBundle self, eAtMpSequenceMode sequenceMode)
    {
    AtIterator flowIterator;
    AtEthFlow flow;
    eAtRet ret = cAtOk;

    /* Check if bundle not existing */
    if (self == NULL)
        return cAtErrorInvlParm;

    if (!SequenceModeIsValid(self, sequenceMode))
        return cAtErrorModeNotSupport;

    flowIterator = AtMpBundleFlowIteratorCreate(self);
    while ((flow = (AtEthFlow)AtIteratorNext(flowIterator)) != NULL)
        ret |= Tha60210012ModuleClaFlowSequenceNumberModeSet(ClaModule(self), flow, sequenceMode);

    AtObjectDelete((AtObject)flowIterator);
    mThis(self)->txseqNumberMode = sequenceMode;

    return ret;
    }

static eAtMpSequenceMode RxSequenceModeGet(AtMpBundle self)
    {
    return mThis(self)->rxseqNumberMode;
    }

static eAtMpSequenceMode TxSequenceModeGet(AtMpBundle self)
    {
    return mThis(self)->txseqNumberMode;
    }

static eAtModulePppRet WorkingModeSet(AtMpBundle self, eAtMpWorkingMode workingMode)
    {
    AtUnused(self);
    return (workingMode == cAtMpWorkingModeLfi) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtMpWorkingMode WorkingModeGet(AtMpBundle self)
    {
    AtUnused(self);
    return cAtMpWorkingModeLfi;
    }

static uint32 DefaultOffset(ThaMpBundle self)
    {
    return AtChannelIdGet((AtChannel)self);
    }

static uint32 QueueDefaultOffset(ThaMpBundle self, uint8 queueId)
    {
    return (uint32)(DefaultOffset(self) * 4 + queueId);
    }

static uint8 MaxNumQueues(ThaMpBundle self)
    {
    AtUnused(self);
    return 1;
    }

static Tha60210012ModuleMpeg ModuleMpeg(ThaMpBundle self)
    {
    return (Tha60210012ModuleMpeg)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleMpeg);
    }

static eAtRet QueueEnable(ThaMpBundle self, uint8 queueId, eBool enable)
    {
    eAtRet ret;
    uint32 channelId = AtChannelIdGet((AtChannel)self);
    Tha60210012ModuleMpeg moduleMpeg = ModuleMpeg(self);
    AtUnused(queueId);

    ret  = Tha60210012ModuleMpegBundleQueueMaxThresholdSet(moduleMpeg, channelId, 0x7, 0x2);
    ret |= Tha60210012ModuleMpegBundleQueueMinThresholdSet(moduleMpeg, channelId, 0x7, 0x2);
    ret |= Tha60210012ModuleMpegQueueBundlePrioritySet(moduleMpeg, channelId, cThaMpegQueuePriorityLowest);
    ret |= Tha60210012ModuleMpegBundleQueueEnable(ModuleMpeg(self), channelId, enable);

    return ret;
    }

static eAtModulePppRet MrruSet(AtMpBundle self, uint32 mrru)
    {
    const uint8 cMinMrru = 64;
    AtResequenceEngine resequenceEngine;

    if (mrru < cMinMrru)
        return cAtErrorModeNotSupport;

    resequenceEngine = ResequenceEngine(self);
    if (resequenceEngine == NULL)
        {
        mThis(self)->mrru = mrru;
        return cAtOk;
        }

    return AtResequenceEngineMrruSet(resequenceEngine, mrru);
    }

static uint32 MrruGet(AtMpBundle self)
    {
    AtResequenceEngine resequenceEngine = ResequenceEngine(self);
    if (resequenceEngine == NULL)
        return mThis(self)->mrru;

    return AtResequenceEngineMrruGet(resequenceEngine);
    }

static eAtModulePppRet SequenceNumberSet(AtMpBundle self, uint8 classNumber, uint32 sequenceNumber)
    {
    AtUnused(self);
    AtUnused(classNumber);
    AtUnused(sequenceNumber);
    return cAtErrorModeNotSupport;
    }

static eBool MaxDelayIsInRange(AtHdlcBundle self, uint8 maxDelay)
    {
    AtUnused(self);
    return !mOutOfRange(maxDelay, 4, 100);
    }

static eAtRet MaxDelaySet(AtHdlcBundle self, uint8 maxDelay)
    {
    AtResequenceEngine engine;

    if (!MaxDelayIsInRange(self, maxDelay))
        return cAtErrorOutOfRangParm;

    engine = ResequenceEngine((AtMpBundle)self);
    mThis(self)->maxDelay = maxDelay;
    if (engine == NULL)
        return cAtOk;

    Tha60210012ResequenceThresholdSet(engine, ResequenceThresholdCalculate(self));
    return AtResequenceEngineTimeoutSet(engine, ResequenceTimeoutCalculate(self));
    }

static uint8 MaxDelayGet(AtHdlcBundle self)
    {
    return mThis(self)->maxDelay;
    }

static eAtModulePppRet FlowClassAssign(ThaMpBundle self, AtEthFlow flow, uint8 classNumber)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(classNumber);
    return cAtOk;
    }

static eAtModulePppRet FlowClassDeassign(ThaMpBundle self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cAtOk;
    }

static eBool HasFlow(AtHdlcBundle self)
    {
    return ((AtMpBundleNumberOfFlowsGet((AtMpBundle)self) > 0) ? cAtTrue : cAtFalse);
    }

static eBool FlowCanJoin(ThaMpBundle self, AtEthFlow flow, uint8 classNumber)
    {
    AtUnused(flow);

    /* This product do not support multi-class */
    if (HasFlow((AtHdlcBundle)self))
        return cAtFalse;

    /* Because this product just support LFI mode, so default class number must be was 0 */
    if (classNumber != 0)
        return cAtFalse;

    return cAtTrue;
    }

static void QueueBlockSizeSet(ThaMpBundle self, uint8 queueId, uint8 rangeId)
    {
    AtUnused(self);
    AtUnused(queueId);
    AtUnused(rangeId);
    }

static uint32 DefaultQueue(void)
    {
    return 0;
    }

static uint32 DefaultMsru(void)
    {
    return cBit6_0; /* Max value HW can support */
    }

static eAtRet ResequenceEngineActivate(AtHdlcBundle self, AtEthFlow flow)
    {
    eAtRet ret;
    AtResequenceManager manager = ThaDeviceResequenceManagerGet((ThaDevice)AtChannelDeviceGet((AtChannel)self));
    AtResequenceEngine freeEngine = AtResequenceManagerFreeEngineGet(manager);
    uint32 threshold;

    /* Bundle work without re-sequence engine */
    if (freeEngine == NULL)
        return cAtOk;

    /* Assign to this engine */
    mApiCheck(AtResequenceEngineHdlcBundleSet(freeEngine, self));
    mApiCheck(AtResequenceEngineEthFlowSet(freeEngine, flow));

    /* Enable engine */
    mApiCheck(AtResequenceEngineEnable(freeEngine, cAtTrue));
    mApiCheck(AtResequenceEngineOutputQueueEnable(freeEngine, cAtTrue));
    mApiCheck(AtResequenceEngineOutputQueueSet(freeEngine, DefaultQueue()));

    mApiCheck(AtResequenceEngineTimeoutSet(freeEngine, ResequenceTimeoutCalculate(self)));
    mApiCheck(AtResequenceEngineMsruSet(freeEngine, DefaultMsru()));
    mApiCheck(AtResequenceEngineMrruSet(freeEngine, AtMpBundleMrruGet((AtMpBundle)self)));

    /* Updated threshold re-sequence */
    threshold = ResequenceThresholdCalculate(self);
    Tha60210012ResequenceThresholdSet(freeEngine, threshold);

    /* Update engine for this flow */
    Tha60210012EthFlowResequenceEngineSet(flow, freeEngine);
    mThis(self)->rsqEngine = freeEngine;

    return cAtOk;
    }

static eAtModulePppRet FlowAdd(AtMpBundle self, AtEthFlow flow, uint8 classNumber)
    {
    eAtRet ret = m_AtMpBundleMethods->FlowAdd(self, flow, classNumber);

    if (ret != cAtOk)
        return ret;

    ret = Tha60210012ModuleClaFlowFragmentSizeSet(ClaModule(self), flow, AtHdlcBundleFragmentSizeGet((AtHdlcBundle)self));
    ret |= Tha60210012ModuleClaFlowSequenceNumberModeSet(ClaModule(self), flow, AtMpBundleSequenceModeGet(self));
    ret |= ResequenceEngineActivate((AtHdlcBundle)self, flow);

    return ret;
    }

static eAtRet ResequenceEngineDeactivate(AtHdlcBundle self, AtEthFlow flow)
    {
    AtResequenceManager manager = ThaDeviceResequenceManagerGet((ThaDevice)AtChannelDeviceGet((AtChannel)self));
    AtResequenceEngine engine = Tha60210012EthFlowResequenceEngineGet(flow);
    eAtRet ret;

    mApiCheck(AtResequenceEngineDeActivate(engine));
    mApiCheck(AtResequenceManagerEngineUnuse(manager, engine));

    /* Reset database */
    Tha60210012EthFlowResequenceEngineSet(flow, NULL);
    mApiCheck(AtResequenceEngineHdlcBundleSet(engine, NULL));
    mApiCheck(AtResequenceEngineEthFlowSet(engine, NULL));
    mThis(self)->rsqEngine = NULL;

    return cAtOk;
    }

static eAtModulePppRet FlowRemove(AtMpBundle self, AtEthFlow flow)
    {
    eAtRet ret = m_AtMpBundleMethods->FlowRemove(self, flow);
    if (ret != cAtOk)
        return ret;

    return ResequenceEngineDeactivate((AtHdlcBundle)self, flow);
    }

static eAtRet ResequenceEngineUpdate(AtHdlcBundle self)
    {
    if (HasFlow(self) || (AtChannelBoundPwGet((AtChannel)self)))
        {
        AtResequenceEngine engine = ResequenceEngine((AtMpBundle)self);

        Tha60210012ResequenceNumLinksSet(engine, AtHdlcBundleNumberOfLinksGet(self));
        Tha60210012ResequenceThresholdSet(engine, ResequenceThresholdCalculate(self));
        return AtResequenceEngineTimeoutSet(engine, ResequenceTimeoutCalculate(self));
        }

    return cAtOk;
    }

static eAtRet HardwareBundleRemove(ThaMpBundle self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet LinkAdd(AtHdlcBundle self, AtHdlcLink link)
    {
    eAtRet ret = m_AtHdlcBundleMethods->LinkAdd(self, link);
    if (ret != cAtOk)
        return ret;

    /* Update threshold and buffer re-sequence engine */
    ret |= ResequenceEngineUpdate(self);
    ret |= Tha60210012PhysicalPppLinkRxSequenceModeSet(link, AtMpBundleSequenceModeGet((AtMpBundle)self));

    return ret;
    }

static eAtRet LinkHwRemove(ThaMpBundle self, AtHdlcLink link)
    {
    eAtRet ret;

    ret = m_ThaMpBundleMethods->LinkHwRemove(self, ThaDynamicHdlcLinkPhysicalHdlcLinkGet(link));
    if (ret != cAtOk)
        return ret;

    /* Update threshold and buffer re-sequence engine */
    ret |= ResequenceEngineUpdate((AtHdlcBundle)self);

    return ret;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtDevice device = AtChannelDeviceGet(self);
    Tha60210012ModuleEncap encapModule = (Tha60210012ModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    eAtRet ret = m_AtChannelMethods->BindToPseudowire(self, pseudowire);
    if (ret != cAtOk)
        return ret;

    if (pseudowire)
        return Tha60210012ModuleEncapBundlePwFlowConnect(encapModule, (AtMpBundle)self, pseudowire);

    return Tha60210012ModuleEncapMpBundleFlowReset(encapModule, (AtMpBundle)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210012MpBundle object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(txEnable);
    mEncodeUInt(txseqNumberMode);
    mEncodeUInt(rxseqNumberMode);
    mEncodeUInt(mrru);
    mEncodeUInt(maxDelay);
    mEncodeUInt(rxEnable);
    mEncodeUInt(pduType);
    mEncodeObjectDescription(rsqEngine);
    }

static eAtModulePwRet PduTypeSet(AtHdlcBundle self, eAtHdlcPduType pduType)
    {
    mThis(self)->pduType = pduType;
    return cAtOk;
    }

static eAtHdlcPduType PduTypeGet(AtHdlcBundle self)
    {
    return mThis(self)->pduType;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    AtHdlcBundlePduTypeSet((AtHdlcBundle)self, cAtHdlcPduTypeAny);
    return cAtOk;
    }

static eAtRet Debug(AtChannel self)
    {
    if (HasFlow((AtHdlcBundle)self))
        {
        AtResequenceEngine engine = ResequenceEngine((AtMpBundle)self);

        if (engine == NULL)
            return cAtOk;

        Tha60210012ResequenceFsmStatusDisplay(engine);
        Tha60210012ResequenceStickyDisplay(engine);
        Tha60210012ResequenceCounterDisplay(engine);
        }
    return cAtOk;
    }

static uint32 MaxNumOfFlowsGet(AtMpBundle self)
    {
    AtUnused(self);
    return 1; /* Do not support multiclass */
    }

static eAtRet FragmentSizeSet(AtHdlcBundle self, eAtFragmentSize fragmentSize)
    {
    AtUnused(self);
    return (fragmentSize == cAtNoFragment) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtFragmentSize FragmentSizeGet(AtHdlcBundle self)
    {
    AtUnused(self);
    return cAtNoFragment;
    }

static eAtRet NumberLinksSet(ThaMpBundle self, uint16 numLinkInBundle)
    {
    AtUnused(self);
    AtUnused(numLinkInBundle);
    return cAtOk;
    }

static eAtMpSequenceMode DefaultSequenceModeGet(AtMpBundle self)
    {
    AtUnused(self);
    return cAtMpSequenceModeLong;
    }

static uint8 DefaultMaxLinkDelay(AtHdlcBundle self)
    {
    AtUnused(self);
    return 4;
    }

static eBool CanConfigureSequenceNumber(AtMpBundle self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtObject(AtMpBundle self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtMpBundle self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtMpBundle(AtMpBundle self)
    {
    AtMpBundle bundle = (AtMpBundle)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtMpBundleMethods = mMethodsGet(bundle);
        mMethodsGet(osal)->MemCpy(osal, &m_AtMpBundleOverride, m_AtMpBundleMethods, sizeof(m_AtMpBundleOverride));

        mMethodOverride(m_AtMpBundleOverride, RxSequenceModeSet);
        mMethodOverride(m_AtMpBundleOverride, RxSequenceModeGet);
        mMethodOverride(m_AtMpBundleOverride, TxSequenceModeSet);
        mMethodOverride(m_AtMpBundleOverride, TxSequenceModeGet);
        mMethodOverride(m_AtMpBundleOverride, WorkingModeSet);
        mMethodOverride(m_AtMpBundleOverride, WorkingModeGet);
        mMethodOverride(m_AtMpBundleOverride, MrruSet);
        mMethodOverride(m_AtMpBundleOverride, MrruGet);
        mMethodOverride(m_AtMpBundleOverride, SequenceNumberSet);
        mMethodOverride(m_AtMpBundleOverride, FlowAdd);
        mMethodOverride(m_AtMpBundleOverride, FlowRemove);
        mMethodOverride(m_AtMpBundleOverride, MaxNumOfFlowsGet);
        mMethodOverride(m_AtMpBundleOverride, DefaultSequenceModeGet);
        mMethodOverride(m_AtMpBundleOverride, CanConfigureSequenceNumber);
        }

    mMethodsSet(bundle, &m_AtMpBundleOverride);
    }

static void OverrideThaMpBundle(AtMpBundle self)
    {
    ThaMpBundle bundle = (ThaMpBundle)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaMpBundleMethods = mMethodsGet(bundle);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaMpBundleOverride, mMethodsGet(bundle), sizeof(m_ThaMpBundleOverride));

        mMethodOverride(m_ThaMpBundleOverride, DefaultOffset);
        mMethodOverride(m_ThaMpBundleOverride, QueueDefaultOffset);
        mMethodOverride(m_ThaMpBundleOverride, MaxNumQueues);
        mMethodOverride(m_ThaMpBundleOverride, QueueEnable);
        mMethodOverride(m_ThaMpBundleOverride, FlowClassAssign);
        mMethodOverride(m_ThaMpBundleOverride, FlowClassDeassign);
        mMethodOverride(m_ThaMpBundleOverride, FlowCanJoin);
        mMethodOverride(m_ThaMpBundleOverride, LinkHwRemove);
        mMethodOverride(m_ThaMpBundleOverride, QueueBlockSizeSet);
        mMethodOverride(m_ThaMpBundleOverride, HardwareBundleRemove);
        mMethodOverride(m_ThaMpBundleOverride, NumberLinksSet);
        }

    mMethodsSet(bundle, &m_ThaMpBundleOverride);
    }

static void OverrideAtHdlcBundle(AtMpBundle self)
    {
    AtHdlcBundle bundle = (AtHdlcBundle)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcBundleMethods = mMethodsGet(bundle);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcBundleOverride, mMethodsGet(bundle), sizeof(m_AtHdlcBundleOverride));

        mMethodOverride(m_AtHdlcBundleOverride, MaxDelaySet);
        mMethodOverride(m_AtHdlcBundleOverride, MaxDelayGet);
        mMethodOverride(m_AtHdlcBundleOverride, FragmentSizeSet);
        mMethodOverride(m_AtHdlcBundleOverride, FragmentSizeGet);
        mMethodOverride(m_AtHdlcBundleOverride, LinkAdd);
        mMethodOverride(m_AtHdlcBundleOverride, SchedulingModeGet);
        mMethodOverride(m_AtHdlcBundleOverride, SchedulingModeSet);
        mMethodOverride(m_AtHdlcBundleOverride, PduTypeSet);
        mMethodOverride(m_AtHdlcBundleOverride, PduTypeGet);
        mMethodOverride(m_AtHdlcBundleOverride, DefaultMaxLinkDelay);
        }

    mMethodsSet(bundle, &m_AtHdlcBundleOverride);
    }

static void Override(AtMpBundle self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtMpBundle(self);
    OverrideThaMpBundle(self);
    OverrideAtHdlcBundle(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012MpBundle);
    }

static AtMpBundle ObjectInit(AtMpBundle self, uint32 channelId, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmMpBundleObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtMpBundle Tha60210012MpBundleNew(uint32 channelId, AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtMpBundle newBundle = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBundle == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newBundle, channelId, module);
    }

eAtRet Tha60210012MpBundleRsqEngineActivate(AtHdlcBundle self, AtEthFlow flow)
    {
    return ResequenceEngineActivate(self, flow);
    }

eAtRet Tha60210012MpBundleRsqEngineDeactivate(AtHdlcBundle self, AtEthFlow flow)
    {
    return ResequenceEngineDeactivate(self, flow);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : Tha60210012ModuleXc.c
 *
 * Created Date: Mar 1, 2016
 *
 * Description : Module XC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ocn/Tha60210012ModuleOcnReg.h"
#include "Tha60210012ModuleXc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011ModuleXcMethods m_Tha60210011ModuleXcOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 RxSxcLineIdMask(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_rxsxcramctl_RxSxcLineId_Mask;
    }

static uint8 RxSxcLineIdShift(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_rxsxcramctl_RxSxcLineId_Shift;
    }

static uint32 TxSxcLineIdMask(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_txsxcramctl_TxSxcLineId_Mask;
    }

static uint8 TxSxcLineIdShift(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_txsxcramctl_TxSxcLineId_Shift;
    }

static uint8 StartHoLineId(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return 2;
    }

static void OverrideTha60210011ModuleXc(AtModuleXc self)
    {
    Tha60210011ModuleXc module = (Tha60210011ModuleXc)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleXcOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleXcOverride));

        mMethodOverride(m_Tha60210011ModuleXcOverride, RxSxcLineIdMask);
        mMethodOverride(m_Tha60210011ModuleXcOverride, RxSxcLineIdShift);
        mMethodOverride(m_Tha60210011ModuleXcOverride, TxSxcLineIdMask);
        mMethodOverride(m_Tha60210011ModuleXcOverride, TxSxcLineIdShift);
        mMethodOverride(m_Tha60210011ModuleXcOverride, StartHoLineId);
        }

    mMethodsSet(module, &m_Tha60210011ModuleXcOverride);
    }

static void Override(AtModuleXc self)
    {
    OverrideTha60210011ModuleXc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleXc);
    }

AtModuleXc Tha60210012ModuleXcObjectInit(AtModuleXc self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleXcObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleXc Tha60210012ModuleXcNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleXc newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210012ModuleXcObjectInit(newModule, device);
    }

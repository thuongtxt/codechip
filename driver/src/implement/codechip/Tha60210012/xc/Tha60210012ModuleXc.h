/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : Tha60210012ModuleXc.h
 * 
 * Created Date: Mar 1, 2016
 *
 * Description : 60210012 module XC APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEXC_H_
#define _THA60210012MODULEXC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleXc.h"
#include "../../Tha60210011/xc/Tha60210011ModuleXcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleXc * Tha60210012ModuleXc;
typedef struct tTha60210012ModuleXc
    {
    tTha60210011ModuleXc super;
    }tTha60210012ModuleXc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleXc Tha60210012ModuleXcObjectInit(AtModuleXc self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEXC_H_ */


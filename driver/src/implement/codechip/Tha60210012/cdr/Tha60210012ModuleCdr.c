/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      :  
 *
 * File        : Tha60210012ModuleCdr.c
 *
 * Created Date: Mar 23, 2016
 *
 * Description : Module CDR of 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ram/Tha60210011InternalRam.h"
#include "../man/Tha60210012Device.h"
#include "Tha60210012ModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleCdrMethods         m_ThaModuleCdrOverride;
static tTha60210011ModuleCdrMethods m_Tha60210011ModuleCdrOverride;

/* Save super implementation */
static const tThaModuleCdrMethods *m_ThaModuleCdrMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportInterrupt(ThaModuleCdr self)
    {
    AtUnused(self);
    return 0x0;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsCdrRegister(AtModuleDeviceGet(self), address);
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
        "HO CDR STS Timing control",
        "HO CDR ACR Engine Timing control, slice 0",
        "HO CDR ACR Engine Timing control, slice 1",
        "LO CDR STS Timing control, slice 0",
        "LO CDR VT Timing control, slice 0",
        "LO CDR ACR Engine Timing control, slice 0",
        "LO CDR STS Timing control, slice 1",
        "LO CDR VT Timing control, slice 1",
        "LO CDR ACR Engine Timing control, slice 1",
        "LO CDR STS Timing control, slice 2",
        "LO CDR VT Timing control, slice 2",
        "LO CDR ACR Engine Timing control, slice 2",
        "LO CDR STS Timing control, slice 3",
        "LO CDR VT Timing control, slice 3",
        "LO CDR ACR Engine Timing control, slice 3",
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static uint32 StartLoRamLocalId(Tha60210011ModuleCdr self)
    {
    AtUnused(self);
    return 3;
    }

static eBool ShouldEnableJitterAttenuator(ThaModuleCdr self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 supportedJaVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x9, 0x00);

    if (hwVersion >= supportedJaVersion)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrMethods = mMethodsGet(cdrModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, m_ThaModuleCdrMethods, sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, StartVersionSupportInterrupt);
        mMethodOverride(m_ThaModuleCdrOverride, ShouldEnableJitterAttenuator);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void OverrideTha60210011ModuleCdr(AtModule self)
    {
    Tha60210011ModuleCdr cdrModule = (Tha60210011ModuleCdr) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleCdrOverride, mMethodsGet(cdrModule), sizeof(m_Tha60210011ModuleCdrOverride));

        mMethodOverride(m_Tha60210011ModuleCdrOverride, StartLoRamLocalId);
        }

    mMethodsSet(cdrModule, &m_Tha60210011ModuleCdrOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleCdr(self);
    OverrideTha60210011ModuleCdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleCdr);
    }

AtModule Tha60210012ModuleCdrObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210012ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModuleCdrObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      :  CDR
 * 
 * File        : Tha60210012ModuleCdr.h
 * 
 * Created Date: Mar 23, 2016
 *
 * Description : Module CDR of 60210012
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/
#ifndef _THA60210012MODULECDR_H_
#define _THA60210012MODULECDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/cdr/Tha60210051ModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleCdr
    {
    tTha60210051ModuleCdr super;
    } tTha60210012ModuleCdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210012ModuleCdrNew(AtDevice device);
AtModule Tha60210012ModuleCdrObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210012MODULECDR_H_ */

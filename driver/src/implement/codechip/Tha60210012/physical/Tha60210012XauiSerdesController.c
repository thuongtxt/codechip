/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210012XauiSerdesController.c
 *
 * Created Date: May 24, 2017
 *
 * Description : Tha60210012 Xaui serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/physical/ThaSerdesControllerInternal.h"
#include "../../../default/physical/ThaSerdesController.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "Tha60210012XauiSerdesControllerReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumLane 4
#define cBaseAddressSerdesXaui 0x2F8000

#define cAf6_SERDES_POWER_DOWN_RXPD_Mask(laneId)  (cBit1_0 << ((laneId) * 2))
#define cAf6_SERDES_POWER_DOWN_RXPD_Shift(laneId) ((laneId) * 2)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012XauiSerdesController
    {
    tAtSerdesController super;
    }tTha60210012XauiSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods  m_AtSerdesControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 SerdesOffset(AtSerdesController self)
    {
    return (uint32)(cBaseAddressSerdesXaui + (AtSerdesControllerIdGet(self) - cStartXauiSerdesId) * 0x2000);
    }

static uint16 Sw2HwLoopbackMode(uint16 modeId)
    {
    switch (modeId)
        {
        case cAtLoopbackModeRelease: return 0x0;
        case cAtLoopbackModeLocal:   return 0x2;
        case cAtLoopbackModeRemote:  return 0x4;
        default:                     return 0x0;
        }
    }

static eAtLoopbackMode Hw2SwLoopbackMode(uint32 hwValue)
    {
    switch (hwValue)
        {
        case 0x0: return cAtLoopbackModeRelease;
        case 0x2: return cAtLoopbackModeLocal;
        case 0x4: return cAtLoopbackModeRemote;
        default:  return 0x0;
        }
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet HwSerdesLoopbackSet(AtSerdesController self, uint16 hwLoopbackMode)
    {
    uint8 laneId;
    uint32 addr = cAf6Reg_SERDES_LoopBack_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, addr , cAtModuleEth);

    for (laneId = 0; laneId < cMaxNumLane; laneId++)
        {
        mFieldIns(&regVal,
                  cAf6_SERDES_LoopBack_lpback_subport0_Mask << (laneId * 4),
                  cAf6_SERDES_LoopBack_lpback_subport0_Shift + (laneId * 4),
                  hwLoopbackMode);
        }

    AtSerdesControllerWrite(self, addr, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint16 HwSerdesLoopbackGet(AtSerdesController self)
    {
    uint16 hwLoopbackMode;
    uint32 addr = cAf6Reg_SERDES_LoopBack_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, addr, cAtModuleEth);

    hwLoopbackMode = (uint16)mRegField(regVal, cAf6_SERDES_LoopBack_lpback_subport3_);
    return hwLoopbackMode;
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    uint16 hwLoopbackMode = Sw2HwLoopbackMode(loopbackMode);

    if (!enable)
        return HwSerdesLoopbackSet(self, 0x0);

    return HwSerdesLoopbackSet(self, hwLoopbackMode);
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    if (loopbackMode == Hw2SwLoopbackMode(HwSerdesLoopbackGet(self)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    AtUnused(loopbackMode);
    return cAtTrue;
    }

static void PowerDownMaskSet(AtSerdesController self, eBool powerDown)
    {
    uint32 regAddress = cAf6Reg_SERDES_POWER_DOWN_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);
    uint8 laneId;

    for (laneId = 0; laneId < 4; laneId++)
        {
        /* power down for rx */
        mFieldIns(&regVal, cAf6_SERDES_POWER_DOWN_RXPD_Mask(laneId), cAf6_SERDES_POWER_DOWN_RXPD_Shift(laneId), powerDown ? 0x3 : 0x0);

        /* power down for tx */
        mFieldIns(&regVal, cAf6_SERDES_POWER_DOWN_RXPD_Mask(laneId + 8), cAf6_SERDES_POWER_DOWN_RXPD_Shift(laneId + 8), powerDown ? 0x3 : 0x0);
        }

    AtSerdesControllerWrite(self, regAddress, regVal, cAtModuleEth);
    }

static eAtRet PowerDown(AtSerdesController self, eBool powerDown)
    {
    PowerDownMaskSet(self, powerDown);
    return cAtOk;
    }

static eBool PowerIsDown(AtSerdesController self)
    {
    uint32 regAddress = cAf6Reg_SERDES_POWER_DOWN_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);
    uint32 hwPowerdown;

    hwPowerdown = mRegField(regVal, cAf6_SERDES_POWER_DOWN_RXPD0_);
    hwPowerdown &= mRegField(regVal, cAf6_SERDES_POWER_DOWN_TXPD0_);

    return (hwPowerdown == 0x3) ? cAtTrue : cAtFalse;
    }

static eBool PllChanged(AtSerdesController self)
    {
    uint32 regAddress = cAf6Reg_SERDES_PLL_Status_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);
    uint32 hwPllGet;

    hwPllGet  = mRegField(regVal, cAf6_SERDES_PLL_Status_CPLL_Lock_Change_);
    hwPllGet |= mRegField(regVal, cAf6_SERDES_PLL_Status_QPLL0_Lock_change_);
    hwPllGet |= mRegField(regVal, cAf6_SERDES_PLL_Status_QPLL1_Lock_change_);

    AtSerdesControllerWrite(self, regAddress, regVal, cAtModuleEth);

    return (hwPllGet) ? cAtTrue : cAtFalse;
    }

static void Debug(AtSerdesController self)
    {
    if (PllChanged(self))
        AtPrintc(cSevInfo, "PLL: changed \r\n");
    else
        AtPrintc(cSevInfo, "PLL: not changed \r\n");
    }

static eBool PllIsLocked(AtSerdesController self)
    {
    uint32 regAddress = cAf6Reg_SERDES_PLL_Status_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);
    uint32 hwPllIsLocked;

    hwPllIsLocked = mRegField(regVal, cAf6_SERDES_PLL_Status_CPLL_Lock_);
    hwPllIsLocked &= mRegField(regVal, cAf6_SERDES_PLL_Status_QPLL0_Lock_);
    hwPllIsLocked &= mRegField(regVal, cAf6_SERDES_PLL_Status_QPLL1_Lock_);

    return (hwPllIsLocked) ? cAtTrue : cAtFalse;
    }

static eBool PowerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EqualizerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtSerdesEqualizerMode DefaultEqualizerMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesEqualizerModeLpm;
    }

static eBool EqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    AtUnused(self);
    if (mode == cAtSerdesEqualizerModeDfe) return cAtTrue;
    if (mode == cAtSerdesEqualizerModeLpm) return cAtTrue;
    return cAtFalse;
    }

static eBool IsSimulated(AtSerdesController self)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    return AtDeviceIsSimulated(device);
    }

static void LpmDfeReset(AtSerdesController self)
    {
    const uint32 cTriggerTimeInMs = 200;
    uint32 regAddr = cAf6Reg_SERDES_LPMDFE_Reset_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_, 0x1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    if (!IsSimulated(self))
        AtOsalUSleep(cTriggerTimeInMs * 1000);

    mRegFieldSet(regVal, cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_, 0x0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    }

static uint32 SerdesEqualizerModeSw2Hw(eAtSerdesEqualizerMode mode)
    {
    if (mode == cAtSerdesEqualizerModeDfe)  return 0;
    if (mode == cAtSerdesEqualizerModeLpm)  return 1;

    return 0x0;
    }

static eAtRet EqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    uint32 regAddress = cAf6Reg_SERDES_LPMDFE_Mode_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);

    if (!AtSerdesControllerEqualizerModeIsSupported(self, mode))
        return cAtErrorModeNotSupport;

    mRegFieldSet(regVal, cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_, SerdesEqualizerModeSw2Hw(mode));
    AtSerdesControllerWrite(self, regAddress, regVal, cAtModuleEth);

    LpmDfeReset(self);
    return cAtOk;
    }

static eAtSerdesEqualizerMode EqualizerModeHw2Sw(uint32 mode)
    {
    if (mode == 0) return cAtSerdesEqualizerModeDfe;
    if (mode == 1) return cAtSerdesEqualizerModeLpm;

    return cAtSerdesEqualizerModeUnknown;
    }

static eAtSerdesEqualizerMode EqualizerModeGet(AtSerdesController self)
    {
    uint32 regAddress = cAf6Reg_SERDES_LPMDFE_Mode_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);

    return EqualizerModeHw2Sw(mRegField(regVal, cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_));
    }

static eAtRet HwTxPostCursorSet(AtSerdesController self, uint32 value)
    {
    uint32 regAddress = cAf6Reg_SERDES_TXPOSTCURSOR_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport3_, value);
    mRegFieldSet(regVal, cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport2_, value);
    mRegFieldSet(regVal, cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_, value);
    mRegFieldSet(regVal, cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_, value);

    AtSerdesControllerWrite(self, regAddress, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 HwTxPostCursorGet(AtSerdesController self)
    {
    uint32 value, valueFist, landeId;
    uint32 regAddress = cAf6Reg_SERDES_TXPOSTCURSOR_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);

    mFieldGet(regVal, cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport3_Mask, cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport3_Shift, uint32, &valueFist);
    for (landeId = 0; landeId < 3; landeId ++)
        {
        mFieldGet(regVal,
                  cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Mask << (landeId * 5),
                  cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Shift + (landeId * 5),
                  uint32,
                  &value);

        if (value != valueFist)
            return cInvalidUint32;
        }

    return value;
    }

static eAtRet HwTxPreCursorSet(AtSerdesController self, uint32 value)
    {
    uint32 regAddress = cAf6Reg_SERDES_TXPRECURSOR_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport3_, value);
    mRegFieldSet(regVal, cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport2_, value);
    mRegFieldSet(regVal, cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport1_, value);
    mRegFieldSet(regVal, cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_, value);

    AtSerdesControllerWrite(self, regAddress, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 HwTxPreCursorGet(AtSerdesController self)
    {
    uint32 value, valueFist, landeId;
    uint32 regAddress = cAf6Reg_SERDES_TXPRECURSOR_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);

    mFieldGet(regVal, cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport3_Mask, cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport3_Shift, uint32, &valueFist);

    for (landeId = 0; landeId < 3; landeId ++)
        {
        mFieldGet(regVal,
                  cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_Mask << (landeId * 5),
                  cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_Shift + (landeId * 5),
                  uint32,
                  &value);

        if (value != valueFist)
            return cInvalidUint32;
        }

    return value;
    }

static eAtRet HwTxDiffCtrlSet(AtSerdesController self, uint32 value)
    {
    uint32 regAddress = cAf6Reg_SERDES_TXDIFFCTRL_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport3_, value);
    mRegFieldSet(regVal, cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport2_, value);
    mRegFieldSet(regVal, cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport1_, value);
    mRegFieldSet(regVal, cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_, value);

    AtSerdesControllerWrite(self, regAddress, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 HwTxDiffCtrlGet(AtSerdesController self)
    {
    uint32 value, valueFist, landeId;
    uint32 regAddress = cAf6Reg_SERDES_TXDIFFCTRL_Base + SerdesOffset(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);

    mFieldGet(regVal, cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport3_Mask, cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport3_Shift, uint32, &value);

    for (landeId = 0; landeId < 3; landeId++)
        {
        mFieldGet(regVal, cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_Mask << (landeId * 4), cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_Shift + (landeId * 4), uint32, &valueFist);

        if (value != valueFist)
            return cInvalidUint32;
        }

    return value;
    }

static eAtRet RxResetDone(AtSerdesController self)
    {
    tAtOsalCurTime curTime, startTime;
    const uint32 cResetTimeoutInMs = 5000;
    uint32 elapse = 0;
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    uint32 rxRegAddr = cAf6Reg_SERDES_RX_Reset_Base + SerdesOffset(self);

    AtOsalCurTimeGet(&startTime);
    while (elapse < cResetTimeoutInMs)
        {
        uint32 rxRegVal  = AtSerdesControllerRead(self, rxRegAddr, cAtModuleEth);

        if (AtDeviceIsSimulated(device))
            return cAtOk;

        if ((rxRegVal & cAf6_SERDES_RX_Reset_rxrst_done_Mask) == cAf6_SERDES_RX_Reset_rxrst_done_Mask)
            return cAtOk;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorSerdesResetTimeout;
    }

static eAtRet TxPmaResetDone(AtSerdesController self)
    {
    tAtOsalCurTime curTime, startTime;
    const uint32 cResetTimeoutInMs = 5000;
    uint32 elapse = 0;
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    uint32 txRegAddr = cAf6Reg_SERDES_TX_Reset_Base + SerdesOffset(self);

    AtOsalCurTimeGet(&startTime);
    while (elapse < cResetTimeoutInMs)
        {
        uint32 txRegVal = AtSerdesControllerRead(self, txRegAddr, cAtModuleEth);

        if (AtDeviceIsSimulated(device))
            return cAtOk;

        if (((txRegVal & cAf6_SERDES_TX_Reset_txrst_done_Mask) == cAf6_SERDES_TX_Reset_txrst_done_Mask))
            return cAtOk;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorSerdesResetTimeout;
    }

static eAtRet TxPMAReset(AtSerdesController self)
    {
    const uint32 cTriggerTimeInMs = 300;
    uint32 regAddress, regVal;

    if (AtSerdesControllerInAccessible(self))
        return cAtOk;

    regAddress = cAf6Reg_SERDES_TX_Reset_Base + SerdesOffset(self);
    regVal = AtSerdesControllerRead(self, regAddress, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_SERDES_TX_Reset_txrst_trig_, 0xF);
    AtSerdesControllerWrite(self, regAddress, regVal, cAtModuleEth);

    if (!IsSimulated(self))
        AtOsalUSleep(cTriggerTimeInMs * 1000);

    mRegFieldSet(regVal, cAf6_SERDES_TX_Reset_txrst_trig_, 0x0);
    AtSerdesControllerWrite(self, regAddress, regVal, cAtModuleEth);

    return TxPmaResetDone(self);
    }

static eAtRet HwPhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    eAtRet ret = cAtErrorInvlParm;
    if (param == cAtSerdesParamTxPreCursor)
        ret = HwTxPreCursorSet(self, value);

    if (param == cAtSerdesParamTxPostCursor)
        ret = HwTxPostCursorSet(self, value);

    if (param == cAtSerdesParamTxDiffCtrl)
        ret = HwTxDiffCtrlSet(self, value);

    if (ret != cAtOk)
        return ret;

    return TxPMAReset(self);
    }

static uint32 HwPhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    if (param == cAtSerdesParamTxPreCursor)
        return HwTxPreCursorGet(self);

    if (param == cAtSerdesParamTxPostCursor)
        return HwTxPostCursorGet(self);

    if (param == cAtSerdesParamTxDiffCtrl)
        return HwTxDiffCtrlGet(self);

    return cInvalidUint32;
    }

static eAtRet Reset(AtSerdesController self)
    {
    const uint32 cTriggerTimeInMs = 300;
    uint32 rxPmaAddr = cAf6Reg_SERDES_RX_Reset_Base + SerdesOffset(self);
    uint32 rxPmaVal = AtSerdesControllerRead(self, rxPmaAddr, cAtModuleEth);

    if (AtSerdesControllerInAccessible(self))
        return cAtOk;

    mRegFieldSet(rxPmaVal, cAf6_SERDES_RX_Reset_rxrst_trig_, 0xF);
    AtSerdesControllerWrite(self, rxPmaAddr, rxPmaVal, cAtModuleEth);

    if (!IsSimulated(self))
        AtOsalUSleep(cTriggerTimeInMs * 1000);

    mRegFieldSet(rxPmaVal, cAf6_SERDES_RX_Reset_rxrst_trig_, 0x0);
    AtSerdesControllerWrite(self, rxPmaAddr, rxPmaVal, cAtModuleEth);

    return RxResetDone(self);
    }

static eBool PhysicalParamValueIsInRange(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    AtUnused(self);

    if (param == cAtSerdesParamTxPreCursor)
        return (value > 32) ? cAtFalse : cAtTrue;

    if (param == cAtSerdesParamTxPostCursor)
        return (value > 32) ? cAtFalse : cAtTrue;

    if (param == cAtSerdesParamTxDiffCtrl)
        return (value > 32) ? cAtFalse : cAtTrue;

    return cAtFalse;
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (!mMethodsGet(self)->PhysicalParamValueIsInRange(self, param, value))
        return cAtErrorOutOfRangParm;

    if (AtSerdesControllerPhysicalParamGet(self, param) == value)
        return cAtOk;

    return HwPhysicalParamSet(self, param, value);
    }

static uint32 PhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    return HwPhysicalParamGet(self, param);
    }

static eBool PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
    AtUnused(self);

    switch ((uint32)param)
        {
        case cAtSerdesParamTxPreCursor : return cAtTrue;
        case cAtSerdesParamTxPostCursor: return cAtTrue;
        case cAtSerdesParamTxDiffCtrl  : return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012XauiSerdesController);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, Debug);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, PowerDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerIsDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, PllChanged);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultEqualizerMode);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamSet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamGet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamValueIsInRange);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSerdesControllerObjectInit(self, (AtChannel)ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210012XauiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ethPort, serdesId);
    }

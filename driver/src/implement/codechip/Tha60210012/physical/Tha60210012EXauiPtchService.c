/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210012EXauiPtchService.c
 *
 * Created Date: May 14, 2016
 *
 * Description : eXAUI PTCH service
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "../pda/Tha60210012ModulePda.h"
#include "../mpeg/Tha60210012ModuleMpeg.h"
#include "../eth/Tha60210012EXaui.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "../eth/Tha60210012EXauiReg.h"
#include "../pwe/Tha60210012ModulePwe.h"
#include "../pwe/Tha60210012ModulePweReg.h"
#include "../cla/Tha60210012ModuleCla.h"
#include "../cla/Tha60210012ModuleClaReg.h"
#include "Tha60210012PtchServiceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012EXauiPtchService
    {
    tTha60210012PtchService super;
    }tTha60210012EXauiPtchService;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPtchServiceMethods          m_AtPtchServiceOverride;
static tTha60210012PtchServiceMethods m_Tha60210012PtchServiceOverride;

/* Save super implementation */
static const tAtPtchServiceMethods   *m_AtPtchServiceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 EXauiId(AtPtchService self)
    {
    uint32 service = AtPtchServiceTypeGet(self);

    switch (service)
        {
        case cAtPtchServiceTypeEXAUI_0: return 0;
        case cAtPtchServiceTypeEXAUI_1: return 1;
        default:
            return (uint8)cBit7_0;
        }
    }

static AtModule PdaModule(AtPtchService self)
    {
    AtDevice device = AtModuleDeviceGet(AtPtchServiceModuleGet(self));
    return AtDeviceModuleGet(device, cThaModulePda);
    }

static AtModule MpegModule(AtPtchService self)
    {
    AtDevice device = AtModuleDeviceGet(AtPtchServiceModuleGet(self));
    return AtDeviceModuleGet(device, cThaModuleMpeg);
    }

static eAtRet Enable(AtPtchService self, eBool enabled)
    {
    eAtRet ret = cAtOk;
    uint32 exauiId = EXauiId(self);

    ret |= Tha60210012ModulePdaEXauiEnable(PdaModule(self), exauiId, enabled);
    ret |= Tha60210012ModuleMpegEXauiEnable(MpegModule(self), exauiId, enabled);

    return ret;
    }

static eBool IsEnabled(AtPtchService self)
    {
    eBool enabled = cAtTrue;
    uint32 exauiId = EXauiId(self);

    enabled &= Tha60210012ModulePdaEXauiIsEnabled(PdaModule(self), exauiId);
    enabled &= Tha60210012ModuleMpegEXauiIsEnabled(PdaModule(self), exauiId);

    return enabled ? cAtTrue : cAtFalse;
    }

static uint32 HwServiceType(Tha60210012PtchService self)
    {
    return (AtPtchServiceTypeGet((AtPtchService)self) == cAtPtchServiceTypeEXAUI_0) ? 2 : 3;
    }

static AtModuleEth ModuleEthGet(AtDevice self)
    {
    return (AtModuleEth)AtDeviceModuleGet(self, cAtModuleEth);
    }

static AtModule ModulePwe(AtPtchService self)
    {
    AtDevice device = AtModuleDeviceGet(AtPtchServiceModuleGet(self));
    return AtDeviceModuleGet(device, cThaModulePwe);
    }

static eBool PweInsertEXauiIsSupported(AtPtchService self)
    {
    return Tha60210012ModulePweInsertEXauiIsSupported(ModulePwe(self));
    }

static eAtRet PrivateIngressPtchSet(AtPtchService self, uint16 ptch)
    {
    AtModule module = AtPtchServiceModuleGet(self);
    uint32 regAddr = cAf6Reg_cfg2_pen_Base + Tha60210012ModuleEthEXaui1BaseAddress(ModuleEthGet(AtModuleDeviceGet(module)));
    uint32 regVal = mModuleHwRead(module, regAddr);

    mRegFieldSet(regVal, cAf6_cfg2_pen_PTCHID_eXAUI1_, ptch);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint16 PrivateIngressPtchGet(AtPtchService self)
    {
    AtModule module = AtPtchServiceModuleGet(self);
    uint32 regAddr = cAf6Reg_cfg2_pen_Base + Tha60210012ModuleEthEXaui1BaseAddress(ModuleEthGet(AtModuleDeviceGet(module)));
    uint32 regVal = mModuleHwRead(module, regAddr);
    return (uint16)mRegField(regVal, cAf6_cfg2_pen_PTCHID_eXAUI1_);
    }

static eAtRet IngressPtchSet(AtPtchService self, uint16 ptch)
    {
    if (AtPtchServiceTypeGet(self) == cAtPtchServiceTypeEXAUI_0)
        return cAtErrorModeNotSupport;

    if (PweInsertEXauiIsSupported(self))
        return m_AtPtchServiceMethods->IngressPtchSet(self, ptch);

    return PrivateIngressPtchSet(self, ptch);
    }

static uint16 IngressPtchGet(AtPtchService self)
    {
    if (AtPtchServiceTypeGet(self) == cAtPtchServiceTypeEXAUI_0)
        return 0;

    if (PweInsertEXauiIsSupported(self))
        return m_AtPtchServiceMethods->IngressPtchGet(self);

    return PrivateIngressPtchGet(self);
    }

static eBool OffsetIsApplicable(AtPtchService self)
    {
    if (AtPtchServiceTypeGet(self) == cAtPtchServiceTypeEXAUI_0)
        return cAtTrue;
    return cAtFalse;
    }

static AtDevice Device(AtPtchService self)
    {
    return AtModuleDeviceGet(AtPtchServiceModuleGet(self));
    }

static AtIpCore Core(AtPtchService self)
    {
    AtModule module = AtPtchServiceModuleGet(self);
    return AtDeviceIpCoreGet(Device(self), AtModuleDefaultCoreGet(module));
    }

static uint32 ClaBaseAddress(AtPtchService self)
    {
    AtDevice device = AtModuleDeviceGet(AtPtchServiceModuleGet(self));
    AtModule cla = AtDeviceModuleGet(device, cThaModuleCla);
    return Tha60210011ModuleClaBaseAddress((ThaModuleCla)cla);
    }

static AtModule ModuleCla(AtPtchService self)
    {
    return AtDeviceModuleGet(AtModuleDeviceGet(AtPtchServiceModuleGet(self)), cThaModuleCla);
    }

static eAtRet IngressOffsetSet(AtPtchService self, uint32 offset)
    {
    uint32 longRegVal[4];
    uint32 regAddr;
    AtModule moduleCla;
    AtIpCore core;

    if (!OffsetIsApplicable(self))
        return cAtErrorNotApplicable;

    if (offset > cBit7_0)
        return cAtErrorOutOfRangParm;

    moduleCla = ModuleCla(self);
    core = Core(self);
    regAddr = cAf6Reg_cla_glb_psn_Base + ClaBaseAddress(self);
    mModuleHwLongRead(moduleCla, regAddr, longRegVal, 4, core);
    mRegFieldSet(longRegVal[2], cAf6_cla_glb_psn_RxPTCHOffset_, offset);
    mModuleHwLongWrite(moduleCla, regAddr, longRegVal, 4, core);

    return cAtOk;
    }

static uint32 IngressOffsetGet(AtPtchService self)
    {
    uint32 longRegVal[4];
    uint32 regAddr;
    AtIpCore core;
    AtModule module;

    if (!OffsetIsApplicable(self))
        return 0;

    module = ModuleCla(self);
    core = Core(self);
    regAddr = cAf6Reg_cla_glb_psn_Base + ClaBaseAddress(self);
    mModuleHwLongRead(module, regAddr, longRegVal, 4, core);
    return (uint32)mRegField(longRegVal[2], cAf6_cla_glb_psn_RxPTCHOffset_);
    }

static eAtRet EgressOffsetSet(AtPtchService self, uint32 offset)
    {
    uint32 regVal, regAddr;
    AtModule module;
    uint32 sourceId;

    if (!OffsetIsApplicable(self))
        return cAtErrorNotApplicable;

    if (offset > cBit7_0)
        return cAtErrorOutOfRangParm;

    module = AtPtchServiceModuleGet(self);

    if (PweInsertEXauiIsSupported(self))
        {
        sourceId = Tha60210012PtchServiceHwSourceId(self);
        regAddr = cAf6Reg_TxEth_Ptch(sourceId) + Tha60210011ModulePweBaseAddress();
        regVal = mModuleHwRead(module, regAddr);
        mRegFieldSet(regVal, cAf6_TxEth_Ptch_TxEthPtch_, offset);
        }
    else
        {
        regAddr = cAf6Reg_cfg2_pen_Base + Tha60210012ModuleEthEXaui1BaseAddress(ModuleEthGet(AtModuleDeviceGet(module)));
        regVal = mModuleHwRead(module, regAddr);
        mRegFieldSet(regVal, cAf6_cfg2_pen_OFFSET_eXAUI0_, offset);
        }

    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint32 EgressOffsetGet(AtPtchService self)
    {
    uint32 regVal, regAddr;
    AtModule module;
    uint32 sourceId;
    uint32 offset;

    if (!OffsetIsApplicable(self))
        return 0;

    module = AtPtchServiceModuleGet(self);

    if(PweInsertEXauiIsSupported(self))
        {
        sourceId = Tha60210012PtchServiceHwSourceId(self);
        regAddr = cAf6Reg_TxEth_Ptch(sourceId) + Tha60210011ModulePweBaseAddress();
        regVal = mModuleHwRead(module, regAddr);
        offset = mRegField(regVal, cAf6_TxEth_Ptch_TxEthPtch_);
        }
    else
        {
        regAddr = cAf6Reg_cfg2_pen_Base + Tha60210012ModuleEthEXaui1BaseAddress(ModuleEthGet(AtModuleDeviceGet(module)));
        regVal = mModuleHwRead(module, regAddr);
        offset = mRegField(regVal, cAf6_cfg2_pen_OFFSET_eXAUI0_);
        }

    return offset;
    }

static void OverrideAtPtchService(AtPtchService self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPtchServiceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPtchServiceOverride, mMethodsGet(self), sizeof(m_AtPtchServiceOverride));

        mMethodOverride(m_AtPtchServiceOverride, Enable);
        mMethodOverride(m_AtPtchServiceOverride, IsEnabled);
        mMethodOverride(m_AtPtchServiceOverride, IngressPtchSet);
        mMethodOverride(m_AtPtchServiceOverride, IngressPtchGet);
        mMethodOverride(m_AtPtchServiceOverride, IngressOffsetSet);
        mMethodOverride(m_AtPtchServiceOverride, IngressOffsetGet);
        mMethodOverride(m_AtPtchServiceOverride, EgressOffsetSet);
        mMethodOverride(m_AtPtchServiceOverride, EgressOffsetGet);
        }

    mMethodsSet(self, &m_AtPtchServiceOverride);
    }

static void OverrideTha60210012PtchService(AtPtchService self)
    {
    Tha60210012PtchService service = (Tha60210012PtchService)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012PtchServiceOverride, mMethodsGet(service), sizeof(m_Tha60210012PtchServiceOverride));

        mMethodOverride(m_Tha60210012PtchServiceOverride, HwServiceType);
        }

    mMethodsSet(service, &m_Tha60210012PtchServiceOverride);
    }

static void Override(AtPtchService self)
    {
    OverrideAtPtchService(self);
    OverrideTha60210012PtchService(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012EXauiPtchService);
    }

static AtPtchService ObjectInit(AtPtchService self, uint32 serviceType, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012PtchServiceObjectInit(self, serviceType, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPtchService Tha60210012EXauiPtchServiceNew(AtModuleEth ethModule, uint32 serviceType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPtchService newPtchService = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    return ObjectInit(newPtchService, serviceType, ethModule);
    }

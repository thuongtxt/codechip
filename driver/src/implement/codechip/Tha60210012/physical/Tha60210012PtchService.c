/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210012PtchService.c
 *
 * Created Date: May 7, 2016
 *
 * Description : PTCH service
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "../cla/Tha60210012ModuleClaReg.h"
#include "../cla/Tha60210012ModuleCla.h"
#include "../pwe/Tha60210012ModulePweReg.h"
#include "../pwe/Tha60210012ModulePwe.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "Tha60210012PtchServiceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)  ((Tha60210012PtchService)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012PtchServiceMethods m_methods;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tAtPtchServiceMethods m_AtPtchServiceOverride;

/* Save super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HwServiceType(Tha60210012PtchService self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet IngressPtchSet(AtPtchService self, uint16 ptch)
    {
    AtModule module = AtPtchServiceModuleGet(self);
    uint32 sourceId = mMethodsGet(mThis(self))->HwServiceType(mThis(self));
    uint32 regAddr = cAf6Reg_TxEth_Ptch(sourceId) + Tha60210011ModulePweBaseAddress();
    uint32 regVal = mModuleHwRead(module, regAddr);

    mRegFieldSet(regVal, cAf6_TxEth_Ptch_TxEthPtch_, ptch);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static uint16 IngressPtchGet(AtPtchService self)
    {
    AtModule module = AtPtchServiceModuleGet(self);
    uint32 sourceId = mMethodsGet(mThis(self))->HwServiceType(mThis(self));
    uint32 regAddr = cAf6Reg_TxEth_Ptch(sourceId) + Tha60210011ModulePweBaseAddress();
    uint32 regVal = mModuleHwRead(module, regAddr);
    return (uint16)mRegField(regVal, cAf6_TxEth_Ptch_TxEthPtch_);
    }

static uint32 ClaBaseAddress(AtPtchService self)
    {
    AtDevice device = AtModuleDeviceGet(AtPtchServiceModuleGet(self));
    AtModule cla = AtDeviceModuleGet(device, cThaModuleCla);
    return Tha60210011ModuleClaBaseAddress((ThaModuleCla)cla);
    }

static eAtRet EgressPtchHwSet(Tha60210012PtchService self, uint16 ptch)
    {
    AtModule module = AtPtchServiceModuleGet((AtPtchService)self);
    uint32 regAddr = cAf6Reg_cla_tpch_table_service((uint32)ptch) + ClaBaseAddress((AtPtchService)self);
    uint32 regVal = mModuleHwRead(module, regAddr);

    mRegFieldSet(regVal, cAf6_cla_tpch_table_service_CLAPTCHService_, mMethodsGet(self)->HwServiceType(self));
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static eAtRet EgressPtchHwClear(Tha60210012PtchService self, uint16 ptch)
    {
    AtModule module = AtPtchServiceModuleGet((AtPtchService)self);
    uint32 regAddr = cAf6Reg_cla_tpch_table_service((uint32)ptch) + ClaBaseAddress((AtPtchService)self);
    uint32 regVal = mModuleHwRead(module, regAddr);

    mRegFieldSet(regVal, cAf6_cla_tpch_table_service_CLAPTCHService_, cAf6_cla_tpch_table_service_CLAPTCHService_Mask);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static eAtRet EgressPtchSet(AtPtchService self, uint16 ptch)
    {
    Tha60210012PtchService service = mThis(self);
    eAtRet ret = cAtOk;

    if (service->egressPtchSet)
        ret = mMethodsGet(service)->EgressPtchHwClear(service, service->ptchId);

    ret |= mMethodsGet(service)->EgressPtchHwSet(service, ptch);

    /* Cache Data */
    mThis(self)->ptchId = ptch;
    mThis(self)->egressPtchSet = cAtTrue;

    return ret;
    }

static uint16 EgressPtchGet(AtPtchService self)
    {
    return mThis(self)->ptchId;
    }

static AtDevice Device(AtPtchService self)
    {
    return AtModuleDeviceGet(AtPtchServiceModuleGet(self));
    }

static ThaModulePwe ModulePwe(AtPtchService self)
    {
    return (ThaModulePwe)AtDeviceModuleGet(Device(self), cThaModulePwe);
    }

static AtModuleEth ModuleEth(AtPtchService self)
    {
    return (AtModuleEth)AtPtchServiceModuleGet(self);
    }

static eAtRet Enable(AtPtchService self, eBool enable)
    {
    eAtRet ret;
    ThaModulePwe modulePwe = ModulePwe(self);

    if (!Tha60210012ModulePwePtchEnableOnModuleIsSupported(modulePwe))
        return enable ? cAtErrorNotImplemented : cAtOk;

    ret = Tha60210012ModulePwePtchInsertionEnable(modulePwe, enable);
    ret |= Tha60210012ModuleEthPtchLookupEnable(ModuleEth(self), enable);

    return ret;
    }

static eBool IsEnabled(AtPtchService self)
    {
    if (!Tha60210012ModulePwePtchEnableOnModuleIsSupported(ModulePwe(self)))
        return cAtTrue;

    return Tha60210012ModuleEthPtchLookupIsEnabled(ModuleEth(self));
    }

static eAtRet IngressModeSet(AtPtchService self, eAtPtchMode mode)
    {
    ThaModulePwe modulePwe = ModulePwe(self);

    if (!Tha60210012ModulePwePtchEnableOnModuleIsSupported(modulePwe))
        return cAtErrorNotImplemented;

    if (mode == cAtPtchModeUnknown)
        return cAtErrorModeNotSupport;

    return Tha60210012ModulePwePtchInsertionModeSet(modulePwe, mode);
    }

static eAtPtchMode IngressModeGet(AtPtchService self)
    {
    ThaModulePwe modulePwe = ModulePwe(self);

    if (!Tha60210012ModulePwePtchEnableOnModuleIsSupported(modulePwe))
        return cAtPtchModeUnknown;

    return Tha60210012ModulePwePtchInsertionModeGet(modulePwe);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210012PtchService object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(ptchId);
    mEncodeUInt(egressPtchSet);
    }

static void OverrideAtObject(AtPtchService self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPtchService(AtPtchService self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPtchServiceOverride, mMethodsGet(self), sizeof(m_AtPtchServiceOverride));

        mMethodOverride(m_AtPtchServiceOverride, IngressPtchSet);
        mMethodOverride(m_AtPtchServiceOverride, IngressPtchGet);
        mMethodOverride(m_AtPtchServiceOverride, EgressPtchSet);
        mMethodOverride(m_AtPtchServiceOverride, EgressPtchGet);
        mMethodOverride(m_AtPtchServiceOverride, Enable);
        mMethodOverride(m_AtPtchServiceOverride, IsEnabled);
        mMethodOverride(m_AtPtchServiceOverride, IngressModeSet);
        mMethodOverride(m_AtPtchServiceOverride, IngressModeGet);
        }

    mMethodsSet(self, &m_AtPtchServiceOverride);
    }

static void Override(AtPtchService self)
    {
    OverrideAtObject(self);
    OverrideAtPtchService(self);
    }

static void MethodsInit(AtPtchService self)
    {
    Tha60210012PtchService service = (Tha60210012PtchService)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HwServiceType);
        mMethodOverride(m_methods, EgressPtchHwSet);
        mMethodOverride(m_methods, EgressPtchHwClear);
        }

    mMethodsSet(service, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012PtchService);
    }

AtPtchService Tha60210012PtchServiceObjectInit(AtPtchService self, uint32 serviceType, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPtchServiceObjectInit(self, serviceType, (AtModule)ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

uint32 Tha60210012PtchServiceHwSourceId(AtPtchService self)
    {
    if (self)
        return mMethodsGet(mThis(self))->HwServiceType(mThis(self));
    return cInvalidUint32;
    }

void Tha60210012PtchServiceEgressPtchHwClear(AtPtchService self)
    {
    if ((self) && (mThis(self)->egressPtchSet))
        mMethodsGet(mThis(self))->EgressPtchHwClear(mThis(self), mThis(self)->ptchId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210012PtchServiceInternal.h
 * 
 * Created Date: May 14, 2016
 *
 * Description : PTCH service
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PTCHSERVICEINTERNAL_H_
#define _THA60210012PTCHSERVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/physical/AtPtchServiceInternal.h"
#include "Tha60210012PtchService.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012PtchServiceMethods
    {
    uint32 (*HwServiceType)(Tha60210012PtchService self);
    eAtRet (*EgressPtchHwClear)(Tha60210012PtchService self, uint16 ptch);
    eAtRet (*EgressPtchHwSet)(Tha60210012PtchService self, uint16 ptch);
    }tTha60210012PtchServiceMethods;

typedef struct tTha60210012PtchService
    {
    tAtPtchService super;
    const tTha60210012PtchServiceMethods *methods;

    /* Data */
    uint16 ptchId;
    eBool egressPtchSet;
    }tTha60210012PtchService;

typedef struct tTha60210012CemPtchService
    {
    tTha60210012PtchService super;
    }tTha60210012CemPtchService;

typedef struct tTha60210012ImsgEopPtchService
    {
    tTha60210012PtchService super;
    }tTha60210012ImsgEopPtchService;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPtchService Tha60210012PtchServiceObjectInit(AtPtchService self, uint32 serviceType, AtModuleEth ethModule);
AtPtchService Tha60210012CemPtchServiceObjectInit(AtPtchService self, AtModuleEth ethModule);
AtPtchService Tha60210012ImsgEopPtchServiceObjectInit(AtPtchService self, AtModuleEth ethModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PTCHSERVICEINTERNAL_H_ */


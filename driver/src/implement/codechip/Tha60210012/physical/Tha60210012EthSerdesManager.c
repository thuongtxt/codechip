/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210011EthSerdesManager.c
 *
 * Created Date: Jul 18, 2015
 *
 * Description : SERDES manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210051/physical/Tha60210051EthPortSerdesManagerInternal.h"
#include "../../Tha60210051/eth/Tha60210051ModuleEth.h"
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "../../../default/physical/ThaSerdesController.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012EthSerdesManager
    {
    tTha60210051EthPortSerdesManager super;
    }tTha60210012EthSerdesManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthSerdesManagerMethods         m_ThaEthSerdesManagerOverride;
static tTha60210051EthPortSerdesManagerMethods m_Tha60210051EthPortSerdesManagerOverride;

/* Save super implementation */
static const tThaEthSerdesManagerMethods *m_ThaEthSerdesManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth ModuleEth(ThaEthSerdesManager self)
    {
    return ThaEthSerdesManagerModuleGet(self);
    }

static uint32 NumSerdesController(ThaEthSerdesManager self)
    {
    AtModuleEth moduleEth = ModuleEth(self);
    if (Tha60210012ModuleEthXauiSerdesTuningIsSupported(moduleEth))
        return 6;

    if (Tha60210012ModuleEthReduceTo4Serdes(moduleEth))
        return 4;

    return m_ThaEthSerdesManagerMethods->NumSerdesController(self);
    }

static uint32 StartHwVersionCanControlEqualizer(Tha60210051EthPortSerdesManager self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0, 0, 0);
    }

static AtSerdesController SerdesControllerCreate(ThaEthSerdesManager self, AtEthPort port, uint32 serdesId)
    {
    AtModuleEth moduleEth = ModuleEth(self);
    if (serdesId >= cStartXauiSerdesId)
        return Tha60210012XauiSerdesControllerNew(port, serdesId);

    if (Tha60210012ModuleEthReduceTo4Serdes(moduleEth))
        return Tha60210012EthPortXfiSerdesControllerNew(port, serdesId);

    return Tha60210051EthPortXfiSerdesControllerNew(port, serdesId);
    }

static const uint32 *XfisForGroup(ThaEthSerdesManager self, uint32 groupId, uint32 *numPorts)
    {
    static const uint32 group0Ports[] = {0, 2};
    static const uint32 group1Ports[] = {1, 3};

    AtUnused(self);

    if (numPorts)
        *numPorts = mCount(group0Ports);

    switch (groupId)
        {
        case 0: return group0Ports;
        case 1: return group1Ports;
        default:
            return NULL;
        }
    }

static void OverrideThaEthSerdesManager(ThaEthSerdesManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthSerdesManagerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthSerdesManagerOverride, m_ThaEthSerdesManagerMethods, sizeof(m_ThaEthSerdesManagerOverride));

        mMethodOverride(m_ThaEthSerdesManagerOverride, XfisForGroup);
        mMethodOverride(m_ThaEthSerdesManagerOverride, NumSerdesController);
        mMethodOverride(m_ThaEthSerdesManagerOverride, SerdesControllerCreate);
        }

    mMethodsSet(self, &m_ThaEthSerdesManagerOverride);
    }

static void OverrideTha60210051EthPortSerdesManager(ThaEthSerdesManager self)
    {
    Tha60210051EthPortSerdesManager manager = (Tha60210051EthPortSerdesManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051EthPortSerdesManagerOverride, mMethodsGet(manager), sizeof(m_Tha60210051EthPortSerdesManagerOverride));

        mMethodOverride(m_Tha60210051EthPortSerdesManagerOverride, StartHwVersionCanControlEqualizer);
        }

    mMethodsSet(manager, &m_Tha60210051EthPortSerdesManagerOverride);
    }
    
static void Override(ThaEthSerdesManager self)
    {
    OverrideThaEthSerdesManager(self);
	OverrideTha60210051EthPortSerdesManager(self);	
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012EthSerdesManager);
    }
    
static ThaEthSerdesManager ObjectInit(ThaEthSerdesManager self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051EthPortSerdesManagerObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthSerdesManager Tha60210012EthSerdesManagerNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthSerdesManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    return ObjectInit(newManager, ethModule);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210012PtchService.h
 * 
 * Created Date: May 7, 2016
 *
 * Description : PTCH service
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PTCHSERVICE_H_
#define _THA60210012PTCHSERVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012PtchService * Tha60210012PtchService;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPtchService Tha60210012EXauiPtchServiceNew(AtModuleEth ethModule, uint32 serviceType);
AtPtchService Tha60210012ImsgEopPtchServiceNew(AtModuleEth ethModule);
AtPtchService Tha60210012CemPtchServiceNew(AtModuleEth ethModule);

uint32 Tha60210012PtchServiceHwSourceId(AtPtchService self);
void Tha60210012PtchServiceEgressPtchHwClear(AtPtchService self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PTCHSERVICE_H_ */


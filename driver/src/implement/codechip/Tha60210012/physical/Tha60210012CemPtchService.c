/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210012CemPtchService.c
 *
 * Created Date: May 14, 2016
 *
 * Description : eXAUI PTCH service
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../pwe/Tha60210012ModulePwe.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "Tha60210012PtchServiceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210012PtchServiceMethods m_Tha60210012PtchServiceOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HwServiceType(Tha60210012PtchService self)
    {
    AtUnused(self);
    return 0;
    }

static void OverrideTha60210012PtchService(AtPtchService self)
    {
    Tha60210012PtchService service = (Tha60210012PtchService)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012PtchServiceOverride, mMethodsGet(service), sizeof(m_Tha60210012PtchServiceOverride));

        mMethodOverride(m_Tha60210012PtchServiceOverride, HwServiceType);
        }

    mMethodsSet(service, &m_Tha60210012PtchServiceOverride);
    }

static void Override(AtPtchService self)
    {
    OverrideTha60210012PtchService(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012CemPtchService);
    }

AtPtchService Tha60210012CemPtchServiceObjectInit(AtPtchService self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012PtchServiceObjectInit(self, cAtPtchServiceTypeCEM, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPtchService Tha60210012CemPtchServiceNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPtchService newPtchService = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    return Tha60210012CemPtchServiceObjectInit(newPtchService, ethModule);
    }

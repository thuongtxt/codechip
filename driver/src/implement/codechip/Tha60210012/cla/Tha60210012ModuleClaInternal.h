/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210012ModuleClaInternal.h
 * 
 * Created Date: Aug 10, 2017
 *
 * Description : 60210012 module cla internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULECLAINTERNAL_H_
#define _THA60210012MODULECLAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/cla/Tha60210051ModuleClaInternal.h"
#include "../../../default/util/ThaBitMask.h"
#include "Tha60210012ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleClaMethods
    {
    uint32 (*RxPwTableLanFcsRmvMask)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableLanFcsRmvShift)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableQ922lenMask1)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableQ922lenShift1)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableQ922lenMask2)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableQ922lenShift2)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableQ922FieldMask1)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableQ922FieldShift1)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableQ922FieldMask2)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableQ922FieldShift2)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableSubSerTypeMask)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableSubSerTypeShift)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableTdmActMask)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableTdmActShift)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableEncActMask)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableEncActShift)(Tha60210012ModuleCla self);
    uint32 (*RxPwTablePsnActMask)(Tha60210012ModuleCla self);
    uint32 (*RxPwTablePsnActShift)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableFrgActMask)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableFrgActShift)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableSerTypeMask)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableSerTypeShift)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableSeridMask)(Tha60210012ModuleCla self);
    uint32 (*RxPwTableSeridShift)(Tha60210012ModuleCla self);
    uint32 (*StartVersionUseNewAddressRxErrorPsnPacketCounter)(Tha60210012ModuleCla self);
    mDefineMaskShiftField(Tha60210012ModuleCla, CLAVlanLabelEn)
    mDefineMaskShiftField(Tha60210012ModuleCla, CLAVlanEn)
    mDefineMaskShiftField(Tha60210012ModuleCla, CLAVlanFlowDirect)
    mDefineMaskShiftField(Tha60210012ModuleCla, CLAVlanGrpWorking)
    mDefineMaskShiftField(Tha60210012ModuleCla, CLAVlanGrpIDFlow)
    mDefineMaskShiftField(Tha60210012ModuleCla, CLAVlanFlowID)

    ThaBitMask (*FlowsMaskCreate)(Tha60210012ModuleCla self);
    }tTha60210012ModuleClaMethods;

typedef struct tTha60210012ModuleCla
    {
    tTha60210051ModuleCla super;
    const tTha60210012ModuleClaMethods * methods;

    /* Private data */
    ThaBitMask hwFlowsMask;
    uint32 asyncInitState;
    }tTha60210012ModuleCla;

/*--------------------------- Forward declarations ---------------------------*/
AtModule Tha60210012ModuleClaObjectInit(AtModule self, AtDevice device);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULECLAINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210012ModuleCla.c
 *
 * Created Date: Feb 29, 2016
 *
 * Description : CLA module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtMpBundle.h"
#include "AtHdlcLink.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../pw/activator/Tha60210012PwActivator.h"
#include "../pw/Tha60210012ModulePw.h"
#include "../eth/Tha60210012EXaui.h"
#include "../eth/Tha60210012EthFlow.h"
#include "../pwe/Tha60210012ModulePwe.h"
#include "../ram/Tha60210012ModuleRam.h"
#include "../man/Tha60210012Device.h"
#include "../ppp/Tha60210012PhysicalPppLink.h"
#include "Tha60210012ModuleClaReg.h"
#include "controller/Tha60210012ClaPwEthPortController.h"
#include "Tha60210012ModuleClaAsyncInit.h"
#include "Tha60210012ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cTha60210012ModuleClaPortCounterBase 0x3000

#define cThaCLAVlanLabelEnMask      cAf6_cla_vlan_lk_CLAVlanLabelEn_Mask
#define cThaCLAVlanLabelEnShift     cAf6_cla_vlan_lk_CLAVlanLabelEn_Shift
#define cThaCLAVlanEnMask           cAf6_cla_vlan_lk_CLAVlanEn_Mask
#define cThaCLAVlanEnShift          cAf6_cla_vlan_lk_CLAVlanEn_Shift
#define cThaCLAVlanFlowDirectMask   cAf6_cla_vlan_lk_CLAVlanFlowDirect_Mask
#define cThaCLAVlanFlowDirectShift  cAf6_cla_vlan_lk_CLAVlanFlowDirect_Shift
#define cThaCLAVlanGrpWorkingMask   cAf6_cla_vlan_lk_CLAVlanGrpWorking_Mask
#define cThaCLAVlanGrpWorkingShift  cAf6_cla_vlan_lk_CLAVlanGrpWorking_Shift
#define cThaCLAVlanGrpIDFlowMask    cAf6_cla_vlan_lk_CLAVlanGrpIDFlow_Mask
#define cThaCLAVlanGrpIDFlowShift   cAf6_cla_vlan_lk_CLAVlanGrpIDFlow_Shift
#define cThaCLAVlanFlowIDMask       cAf6_cla_vlan_lk_CLAVlanFlowID_Mask
#define cThaCLAVlanFlowIDShift      cAf6_cla_vlan_lk_CLAVlanFlowID_Shift

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210012ModuleCla)self)

#define cTha60210012ModuleClaEXauiPortCounterBase 0x3000
#define cTha60210012ModuleClaXfiPortCounterBase   0xD1000

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012ModuleClaMethods m_methods;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleClaMethods         m_ThaModuleClaOverride;
static tThaModuleClaPwMethods       m_ThaModuleClaPwOverride;
static tThaModuleClaPwV2Methods     m_ThaModuleClaPwV2Override;
static tTha60210011ModuleClaMethods m_Tha60210011ModuleClaOverride;

/* Save super implementations */
static const tAtObjectMethods    *m_AtObjectMethods = NULL;
static const tAtModuleMethods    *m_AtModuleMethods = NULL;
static const tTha60210011ModuleClaMethods *m_Tha60210011ModuleClaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ClaPwTypeCtrlReg(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cAf6Reg_cla_per_pw_ctrl_Base + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self);
    }

static uint32 HbceFlowDirectMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Mask;
    }

static uint32 HbceFlowDirectShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Shift;
    }

static uint32 HbceGroupWorkingMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Mask;
    }

static uint32 HbceGroupWorkingShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Shift;
    }

static uint32 HbceGroupIdFlowMask1(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Mask_01;
    }

static uint32 HbceGroupIdFlowShift1(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Shift_01;
    }

static uint32 HbceGroupIdFlowMask2(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Mask_02;
    }

static uint32 HbceGroupIdFlowShift2(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Shift_02;
    }

static uint32 HbceFlowIdMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Mask;
    }

static uint32 HbceFlowIdShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Shift;
    }

static uint32 HbceFlowEnableMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Mask;
    }

static uint32 HbceFlowEnableShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Shift;
    }

static uint32 HbceStoreIdMask(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Mask;
    }

static uint32 HbceStoreIdShift(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Shift;
    }

mDefineMaskShift(Tha60210012ModuleCla, CLAVlanLabelEn)
mDefineMaskShift(Tha60210012ModuleCla, CLAVlanEn)
mDefineMaskShift(Tha60210012ModuleCla, CLAVlanFlowDirect)
mDefineMaskShift(Tha60210012ModuleCla, CLAVlanGrpWorking)
mDefineMaskShift(Tha60210012ModuleCla, CLAVlanGrpIDFlow)
mDefineMaskShift(Tha60210012ModuleCla, CLAVlanFlowID)

static uint32 FlowControlReg(Tha60210011ModuleCla self, ThaPwAdapter pwAdapter)
    {
    return cAf6Reg_cla_per_flow_table_Base +
           Tha60210012PwAdapterHwFlowIdGet(pwAdapter) +
           Tha60210011ModuleClaBaseAddress((ThaModuleCla)self);
    }

static uint32 RxPwTableLanFcsRmvMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_Mask;
    }

static uint32 RxPwTableLanFcsRmvShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_Shift;
    }

static uint32 RxPwTableQ922lenMask1(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableQ922len_Mask;
    }

static uint32 RxPwTableQ922lenShift1(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableQ922len_Shift;
    }

static uint32 RxPwTableQ922lenMask2(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 RxPwTableQ922lenShift2(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 RxPwTableQ922FieldMask1(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableQ922Field_Mask;
    }

static uint32 RxPwTableQ922FieldShift1(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableQ922Field_Shift;
    }

static uint32 RxPwTableQ922FieldMask2(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 RxPwTableQ922FieldShift2(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 RxPwTableSubSerTypeMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableSubSerType_Mask;
    }

static uint32 RxPwTableSubSerTypeShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableSubSerType_Shift;
    }

static uint32 RxPwTableTdmActMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableTdmAct_Mask;
    }

static uint32 RxPwTableTdmActShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableTdmAct_Shift;
    }

static uint32 RxPwTableEncActMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableEncAct_Mask;
    }

static uint32 RxPwTableEncActShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableEncAct_Shift;
    }

static uint32 RxPwTablePsnActMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTablePsnAct_Mask;
    }

static uint32 RxPwTablePsnActShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTablePsnAct_Shift;
    }

static uint32 RxPwTableFrgActMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableFrgAct_Mask;
    }

static uint32 RxPwTableFrgActShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableFrgAct_Shift;
    }

static uint32 RxPwTableSerTypeMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableSerType_Mask;
    }

static uint32 RxPwTableSerTypeShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableSerType_Shift;
    }

static uint32 RxPwTableSeridMask(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableSerid_Mask;
    }

static uint32 RxPwTableSeridShift(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return cAf6_cla_per_flow_table_RxPwTableSerid_Shift;
    }

static void DebugErrorPrint(uint32 reg_i, uint32 cBit_i, const char *debugString)
    {
    eBool stickyRaised = mBinToBool(reg_i & cBit_i);
    eAtSevLevel color = stickyRaised ? cSevCritical : cSevInfo;
    AtPrintc(cSevNormal, "%-30s: ", debugString);
    AtPrintc(color, "%s\r\n", stickyRaised ? "SET" : "CLEAR");
    }

static void DebugOkPrint(uint32 reg_i, uint32 cBit_i, const char *debugString)
    {
    eBool stickyRaised = mBinToBool(reg_i & cBit_i);
    eAtSevLevel color = stickyRaised ? cSevInfo : cSevNormal;
    AtPrintc(cSevNormal, "%-30s: ", debugString);
    AtPrintc(color, "%s\r\n", stickyRaised ? "OK" : "CLEAR");
    }

static eAtRet Debug(AtModule self)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_cla_debug_infor_Base + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self);
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), AtModuleDefaultCoreGet((AtModule)self));

    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, core);

    AtPrintc(cSevInfo, "\r\n* CLA debug information of device %X:\r\n", AtDeviceProductCodeGet(AtModuleDeviceGet(self)));
    AtPrintc(cSevInfo, "========================================================\r\n");
    AtPrintc(cSevNormal, "0x%08X: 0x%08x.%08x.%08x.%08x\r\n\r\n", regAddr, longRegVal[3], longRegVal[2], longRegVal[1], longRegVal[0]);
    DebugErrorPrint(longRegVal[0], cBit1,  "eth port gray eth full");
    DebugErrorPrint(longRegVal[0], cBit2,  "eth port pkt over     ");
    DebugErrorPrint(longRegVal[0], cBit3,  "eth port pkt short    ");
    DebugErrorPrint(longRegVal[0], cBit4,  "eth port1 lost eop    ");
    DebugErrorPrint(longRegVal[0], cBit5,  "eth port1 lost sop    ");
    AtPrintc(cSevNormal, "\r\n");
    DebugErrorPrint(longRegVal[0], cBit8,  "cla2pda_2eop_err      ");
    DebugErrorPrint(longRegVal[0], cBit9,  "cla2pda_2sop_err      ");
    DebugOkPrint(longRegVal[0],    cBit10, "cla2pda_eop_ok        ");
    DebugOkPrint(longRegVal[0],    cBit11, "cla2pda_sop_ok        ");
    DebugErrorPrint(longRegVal[0], cBit12, "cla2pda_err           ");
    DebugErrorPrint(longRegVal[0], cBit13, "cla2pda_rtperr        ");
    DebugErrorPrint(longRegVal[0], cBit14, "cla2pda_malform       ");
    DebugErrorPrint(longRegVal[0], cBit20, "cla2pda_discard       ");
    DebugOkPrint(longRegVal[0],    cBit21, "cla2pda_CES           ");
    DebugOkPrint(longRegVal[0],    cBit22, "cla2pda_EoP           ");
    DebugOkPrint(longRegVal[0],    cBit23, "cla2pda_CEP           ");
    DebugOkPrint(longRegVal[0],    cBit24, "cla2pda_HDLC          ");
    DebugOkPrint(longRegVal[0],    cBit25, "cla2pda_ETH           ");
    DebugOkPrint(longRegVal[0],    cBit26, "cla2pda_cHDLC         ");
    DebugOkPrint(longRegVal[0],    cBit27, "cla2pda_PPPB          ");
    DebugOkPrint(longRegVal[0],    cBit28, "cla2pda_PPPOAM        ");
    DebugOkPrint(longRegVal[0],    cBit29, "cla2pda_PPP           ");
    DebugOkPrint(longRegVal[0],    cBit30, "cla2pda_MLOAM         ");
    DebugOkPrint(longRegVal[0],    cBit31, "cla2pda_MLPPP         ");

    DebugOkPrint(longRegVal[1],    cBit0,  "cla2pda_FROAM         ");
    DebugOkPrint(longRegVal[1],    cBit1,  "cla2pda_FR            ");
    DebugOkPrint(longRegVal[1],    cBit2,  "cla2pda_MLFROAM       ");
    DebugOkPrint(longRegVal[1],    cBit3,  "cla2pda_MLFR          ");
    DebugErrorPrint(longRegVal[1], cBit4,  "cla2pda_lbit          ");
    DebugErrorPrint(longRegVal[1], cBit5,  "cla2pda_rbit          ");
    DebugErrorPrint(longRegVal[1], cBit6,  "cla2pda_uneq          ");
    DebugErrorPrint(longRegVal[1], cBit7,  "cla2cdr_err           ");
    AtPrintc(cSevNormal, "\r\n");
    DebugErrorPrint(longRegVal[0], cBit15, "eth2cla_phy_err       ");
    DebugOkPrint(longRegVal[0],    cBit16, "eth2cla_eop_ok        ");
    DebugOkPrint(longRegVal[0],    cBit17, "eth2cla_sop_ok        ");
    AtPrintc(cSevNormal, "\r\n");
    DebugErrorPrint(longRegVal[1], cBit8,  "cla_lookup_fail       ");
    DebugOkPrint(longRegVal[1],    cBit9,  "cla_lookup_win        ");
    DebugOkPrint(longRegVal[1],    cBit10,  "cla_lookup_en        ");
    AtPrintc(cSevNormal, "\r\n");
    DebugOkPrint(longRegVal[1],    cBit12,  "PTCH_CEM        ");
    DebugOkPrint(longRegVal[1],    cBit13,  "PTCH_iMS        ");
    DebugOkPrint(longRegVal[1],    cBit14,  "PTCH_eXAUI#0    ");
    DebugOkPrint(longRegVal[1],    cBit15,  "PTCH_eXAUI#1    ");
    DebugOkPrint(longRegVal[1],    cBit16,  "PTCH_DIS        ");
    DebugErrorPrint(longRegVal[1], cBit17,  "PTCH_Drop       ");
    DebugOkPrint(longRegVal[1],    cBit19,  "PTCH_ETH        ");
    AtPrintc(cSevNormal, "\r\n");
    DebugErrorPrint(longRegVal[2], cBit8,  "clabuf_full        ");
    DebugErrorPrint(longRegVal[2], cBit9,  "clabuf_infofull    ");
    DebugErrorPrint(longRegVal[2], cBit10,  "clabuf_lkfull     ");
    DebugErrorPrint(longRegVal[3], cBit4,  "mac_error     ");

    AtPrintc(cSevNormal, "\r\n");
    /* CLear after read sticky */
    mModuleHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, core);
    return cAtOk;
    }

static void RxPTCHDisable(AtModule self)
    {
    uint32 regAddr = cAf6Reg_cla_glb_psn_Base + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self);
    uint32 regValue = mModuleHwRead(self, regAddr);

    mRegFieldSet(regValue, cAf6_cla_glb_psn_RxPTCHDis_, 1);
    mModuleHwWrite(self, regAddr, regValue);
    }

static void EthernetTypeCheckingEnable(AtModule self)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet(self), AtModuleDefaultCoreGet(self));
    uint32 regAddr = cAf6Reg_cla_glb_psn_Base + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self);

    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[2], cAf6_cla_glb_psn_EthTypeDisable_, 1);
    mModuleHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, core);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    RxPTCHDisable(self);
    EthernetTypeCheckingEnable(self);
    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Tha60210012ModuleClaAsyncInit((AtObject)self);
    }

static uint32 MaxEthFlows(Tha60210012ModuleCla self)
    {
    return AtModuleEthMaxFlowsGet((AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEth));
    }

static ThaBitMask FlowsMaskCreate(Tha60210012ModuleCla self)
    {
    return ThaBitMaskNew(MaxEthFlows(self));
    }

static ThaBitMask HwFlowsMask(Tha60210012ModuleCla self)
    {
    if (self->hwFlowsMask == NULL)
        self->hwFlowsMask = mMethodsGet(mThis(self))->FlowsMaskCreate(self);
    return self->hwFlowsMask;
    }

static uint32 FreeHwFlowId(Tha60210012ModuleCla self)
    {
    /* Note, during debugging, developers/testers want to use first flows. So to
     * avoid dynamic allocating as much as possible, the new flows are allocated
     * from the last one */
    ThaBitMask mask = HwFlowsMask(self);
    uint32 freeBit = ThaBitMaskLastZeroBit(mask);

    if (!ThaBitMaskBitPositionIsValid(mask, freeBit))
        return cInvalidUint32;

    ThaBitMaskSetBit(mask, freeBit);
    return freeBit;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret;
    uint32 bit_i;
    uint32 numBits = MaxEthFlows(mThis(self));

    /* Super job */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;
    if (mThis(self)->hwFlowsMask != NULL)
        {
        for (bit_i = 0; bit_i < numBits; bit_i++)
            ThaBitMaskClearBit(mThis(self)->hwFlowsMask, bit_i);
        }

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->hwFlowsMask);
    mThis(self)->hwFlowsMask = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210012ModuleCla object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(hwFlowsMask);
    mEncodeUInt(asyncInitState);
    }

static void OneLongRegShow(AtPw pw, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegLongValueDisplay((AtChannel)pw, address, cThaModuleCla);
    }

static void PwClaRegShow(Tha60210011ModuleCla self, AtPw pw)
    {
    ThaPwAdapter pwAdapter = ThaPwAdapterGet(pw);

    AtPrintc(cSevInfo, "   - Flow ID: %d\r\n", Tha60210012PwAdapterHwFlowIdGet(pwAdapter));
    if (ThaPwTypeIsCesCep((AtPw)pwAdapter))
        m_Tha60210011ModuleClaMethods->PwClaRegShow(self, pw);

    OneLongRegShow((AtPw)pwAdapter, "Classify Per Flow Table Control", FlowControlReg(self, pwAdapter));

    Tha60210012ClaPwControllerExpectedCVlanRegShow((Tha60210012ClaPwController)ThaPwAdapterClaPwController(pwAdapter), pw);
    }

static ThaClaEthPortController EthPortControllerCreate(ThaModuleCla self)
    {
    return Tha60210012ClaPwEthPortControllerNew(self);
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return Tha60210012ClaPwControllerNew(self);
    }

static void EthFlowRegsShow(ThaModuleCla self, AtEthFlow flow)
    {
    uint32 vlanIndex, regAddress, regValues, offset;
    tAtEthVlanDesc vlanDesc;
    uint32 flowId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
    uint32 baseAddress = Tha60210011ModuleClaBaseAddress(self);
    uint32 numVlans = AtEthFlowIngressNumVlansGet(flow);

    regAddress = cAf6Reg_cla_per_flow_table_Base + baseAddress + flowId;
    regValues = mChannelHwRead(flow, regAddress, cThaModuleCla);
    Tha6021DebugPrintRegName(NULL, "Classify Per flow Table Control", regAddress, regValues);

    regAddress = cAf6Reg_cla_glb_psn_Base + baseAddress;
    regValues = mChannelHwRead(flow, regAddress, cThaModuleCla);
    Tha6021DebugPrintRegName(NULL, "Classify Global PSN Control", regAddress, regValues);

    for (vlanIndex = 0; vlanIndex < numVlans; vlanIndex++)
        {
        AtEthFlowIngressVlanAtIndex(flow, vlanIndex, &vlanDesc);
        offset = Tha60210011ModuleClaBaseAddress(self) + vlanDesc.vlans[vlanIndex].vlanId;
        regAddress = cAf6Reg_cla_vlan_lk_Base + offset;
        regValues = mChannelHwRead(flow, regAddress, cThaModuleCla);
        Tha6021DebugPrintRegName(NULL, "Classify Vlan Lookup Control", regAddress, regValues);
        }
    }

static uint32 GroupEnableControlWorkingOffset(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return 0x800;
    }

static uint32 PortCounterOffset(Tha60210011ModuleCla self, AtEthPort port, eBool r2c)
    {
    uint32 portBaseAddress;

    if (AtChannelIdGet((AtChannel)port) == 0)
        return m_Tha60210011ModuleClaMethods->PortCounterOffset(self, port, r2c);

    /* With eXAUI port all address counters is the same XFI port, just difference on base address.
     * So on, for don't changed any counter address of current XFI port,
     * need updated baseAddress of eXAUI port to re-used of XFI counter code */
    portBaseAddress = Tha60210012EthEXauiPortBaseAddress(port) + cTha60210012ModuleClaEXauiPortCounterBase - cTha60210012ModuleClaXfiPortCounterBase;
    return (((r2c) ? 0x800 : 0x0) + portBaseAddress);
    }

static eAtRet VcgServiceEnable(ThaModuleCla self,  uint32 vcgId, AtEthFlow flow, eBool enable)
    {
	const uint8 cEopService = 2;
    uint32 longRegVal[cThaLongRegMaxSize];
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), AtModuleDefaultCoreGet((AtModule)self));
    uint32 flowId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
    uint32 regAddr = cAf6Reg_cla_per_flow_table_Base + Tha60210011ModuleClaBaseAddress(self) + flowId;
    uint8 hwService =  (uint8) (enable ? cEopService : 0);

    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, core);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableSerType, hwService);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableSerid, vcgId);
    mModuleHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, core);
    Tha60210012EthFlowServiceTypeCache(flow, hwService);
    return cAtOk;
    }

static eAtRet EthFlowExpectedCVlanSet(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *cVlan, eBool enable)
    {
    uint32 regVal = 0;
    uint32 address = cAf6Reg_cla_vlan_lk_Base + Tha60210011ModuleClaBaseAddress(self) + cVlan->vlanId;
    uint32 flowId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);

    mPolyRegFieldSet(regVal, mThis(self), CLAVlanFlowID, flowId);
    mPolyRegFieldSet(regVal, mThis(self), CLAVlanEn, enable ? 1 : 0);
    mPolyRegFieldSet(regVal, mThis(self), CLAVlanLabelEn, 0);
    mPolyRegFieldSet(regVal, mThis(self), CLAVlanFlowDirect, enable ? 1 : 0);
    mChannelHwWrite(flow, address, regVal, cThaModuleCla);

    return cAtOk;
    }

static AtModulePw ModulePw(AtModule self)
    {
    return (AtModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    }

static uint32 Restore(AtModule self)
    {
    AtIterator pwIterator;
    AtPw pw;
    uint32 remained = m_AtModuleMethods->Restore(self);

    pwIterator = AtModulePwIteratorCreate(ModulePw(self));
    while ((pw = (AtPw)AtIteratorNext(pwIterator)) != NULL)
        remained = remained + Tha60210012ModuleClaPwEthFlowHaRestore(mThis(self), pw);

    AtObjectDelete((AtObject)pwIterator);

    return remained;
    }

static uint32 HbceFlowIdFromPw(Tha60210011ModuleCla self, ThaPwAdapter pw)
    {
    AtUnused(self);
    return Tha60210012PwAdapterHwFlowIdGet(pw);
    }

static eBool FlowIsBoundTdmPw(uint32 serviceType)
    {
    if ((serviceType == 1) || (serviceType == 3))
        return cAtTrue;

    return cAtFalse;
    }

static eBool FlowIsBoundHdlcPw(uint32 serviceType)
    {
    if (serviceType == 4)
        return cAtTrue;

    return cAtFalse;
    }

static AtModulePw PwModule(ThaModuleCla self)
    {
    return (AtModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    }

static AtModuleEncap EncapModule(ThaModuleCla self)
    {
    return (AtModuleEncap)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEncap);
    }

static ThaPwAdapter HbcePwFromFlowId(ThaModuleCla self, uint32 flowId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), AtModuleDefaultCoreGet((AtModule)self));
    uint32 regAddr = cAf6Reg_cla_per_flow_table_Base + Tha60210011ModuleClaBaseAddress((ThaModuleCla)self) + flowId;
    uint32 serviceType;

    mModuleHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, core);
    serviceType = mPolyRegField(longRegVal[0], mThis(self), RxPwTableSerType);

    if (FlowIsBoundTdmPw(serviceType))
        {
        uint32 pwId = mPolyRegField(longRegVal[0], mThis(self), RxPwTableSerid);
        return ThaPwAdapterGet(AtModulePwGetPw(PwModule(self), pwId));
        }

    if (FlowIsBoundHdlcPw(serviceType))
        {
        uint16 linkId = (uint16)mPolyRegField(longRegVal[0], mThis(self), RxPwTableSerid);
        AtEncapChannel hdlcChannel = AtModuleEncapChannelGet(EncapModule(self), linkId);

        return ThaPwAdapterGet(AtChannelBoundPwGet((AtChannel)hdlcChannel));
        }

    /* Will consider for other services type */
    return NULL;
    }

static uint32 PwEthFlowHaRestore(Tha60210012ModuleCla self, AtPw pw)
    {
    ThaPwAdapter adapter = ThaPwAdapterGet(pw);
    ThaHwPw hwPw = ThaPwAdapterHwPwGet(adapter);
    uint32 flowOfPw;

    if (hwPw == NULL)
        return 0;

    flowOfPw = Tha60210012ModulePweEthFlowOfPwGet(adapter);
    Tha60210012HwPwHwFlowIdSet(hwPw, flowOfPw);
    ThaBitMaskSetBit(HwFlowsMask(self), flowOfPw);

    return 0;
    }

static uint8 FragmentSizeSw2Hw(eAtFragmentSize fragSize)
    {
    switch (fragSize)
        {
        case cAtNoFragment:      return 0;
        case cAtFragmentSize128: return 1;
        case cAtFragmentSize256: return 2;
        case cAtFragmentSize512: return 3;
        case cAtFragmentSize64:
        default:                 return 4;
        }
    }

static uint8 SequenceNumberSw2Hw(eAtMpSequenceMode sequence)
    {
    if (sequence == cAtMpSequenceModeShort) return 1;
    if (sequence == cAtMpSequenceModeLong)  return 2;

    /* Invalid */
    return 0;
    }

static uint8 SequenceNumberHw2Sw(uint8 sequence)
    {
    if (sequence == 1) return cAtMpSequenceModeShort;
    if (sequence == 2) return cAtMpSequenceModeLong;
    return cAtMpSequenceModeUnknown;
    }

static uint8 FragmentSizeHw2Sw(uint8 fragSize)
    {
    switch (fragSize)
        {
        case 0: return cAtNoFragment;
        case 1: return cAtFragmentSize128;
        case 2: return cAtFragmentSize256;
        case 3: return cAtFragmentSize512;
        default:return cAtNoFragment;
        }
    }

static uint32 StartHwVersionControlSuppression(void)
    {
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x5, 0x1012);
    }

static eBool CanControlPwSuppression(ThaModuleClaPw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion >= StartHwVersionControlSuppression())
        return cAtTrue;

    return cAtFalse;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsClaRegister(AtModuleDeviceGet(self), address);
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "Classify HBCE Looking Up Information Control 1, Group 0",
         "Classify HBCE Looking Up Information Control 1, Group 1",
         "Classify HBCE Looking Up Information Control 1, Group 2",
         "Classify HBCE Looking Up Information Control 1, Group 3",
         "Classify VLAN looking Up Control",
         "Classify Per Pseudowire Type Control",
         "Classify Per Group Enable Control",
         "Classify HBCE Looking Up Information Control 2, Group 0",
         "Classify HBCE Looking Up Information Control 2, Group 1",
         "Classify HBCE Looking Up Information Control 2, Group 2",
         "Classify HBCE Looking Up Information Control 2, Group 3",
         "Classify HBCE Looking Up Information Control 2, Group 4",
         "Classify HBCE Looking Up Information Control 2, Group 5",
         "Classify HBCE Looking Up Information Control 2, Group 6",
         "Classify HBCE Looking Up Information Control 2, Group 7",
         "Classify HBCE Hashing Table Control",
         "Classify Per Pseudowire Identification to CDR Control",
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    return Tha60210012InternalRamClaNew(self, ramId, localRamId);
    }

static uint32 StartVersionUseNewAddressRxErrorPsnPacketCounter(Tha60210012ModuleCla self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x5, 0x1000);
    }

static eAtRet OamServiceEnable(AtChannel self, uint16 vlanId, uint8 serviceType)
    {
    ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleCla);
    uint32 baseAddress = Tha60210011ModuleClaBaseAddress(claModule) + vlanId;
    uint32 regAddress  = cAf6Reg_cla_oam_link_Base + baseAddress;
    uint32 regValues   = mChannelHwRead(self, regAddress, cThaModuleCla);

    mRegFieldSet(regValues, cAf6_cla_oam_link_CLAOamSrv_, serviceType);
    mRegFieldSet(regValues, cAf6_cla_oam_link_CLAOamLink_, AtChannelHwIdGet(self));
    mChannelHwWrite(self, regAddress, regValues, cThaModuleCla);

    return cAtOk;
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Restore);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, EthPortControllerCreate);
        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        mMethodOverride(m_ThaModuleClaOverride, EthFlowRegsShow);
        mMethodOverride(m_ThaModuleClaOverride, HbcePwFromFlowId);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void OverrideThaModuleClaPw(AtModule self)
    {
    ThaModuleClaPw claModule = (ThaModuleClaPw)self;
    
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaPwOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaPwOverride));

        mMethodOverride(m_ThaModuleClaPwOverride, CanControlPwSuppression);
        }

    mMethodsSet(claModule, &m_ThaModuleClaPwOverride);
    }

static void OverrideThaModuleClaPwV2(AtModule self)
    {
    ThaModuleClaPwV2 claModuleV2 = (ThaModuleClaPwV2)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaPwV2Override, mMethodsGet(claModuleV2), sizeof(m_ThaModuleClaPwV2Override));

        mMethodOverride(m_ThaModuleClaPwV2Override, ClaPwTypeCtrlReg);
        }

    mMethodsSet(claModuleV2, &m_ThaModuleClaPwV2Override);
    }

static void OverrideTha60210011ModuleCla(AtModule self)
    {
    Tha60210011ModuleCla claModule = (Tha60210011ModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleClaMethods = mMethodsGet(claModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleClaOverride, m_Tha60210011ModuleClaMethods, sizeof(m_Tha60210011ModuleClaOverride));

        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowDirectMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowDirectShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupWorkingMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupWorkingShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupIdFlowMask1);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupIdFlowShift1);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupIdFlowMask2);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceGroupIdFlowShift2);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowIdMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowIdShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowEnableMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowEnableShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceStoreIdMask);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceStoreIdShift);
        mMethodOverride(m_Tha60210011ModuleClaOverride, PwClaRegShow);
        mMethodOverride(m_Tha60210011ModuleClaOverride, GroupEnableControlWorkingOffset);
        mMethodOverride(m_Tha60210011ModuleClaOverride, PortCounterOffset);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HbceFlowIdFromPw);
        }

    mMethodsSet(claModule, &m_Tha60210011ModuleClaOverride);
    }

static void MethodsInit(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, RxPwTableLanFcsRmvMask);
        mMethodOverride(m_methods, RxPwTableLanFcsRmvShift);
        mMethodOverride(m_methods, RxPwTableQ922lenMask1);
        mMethodOverride(m_methods, RxPwTableQ922lenShift1);
        mMethodOverride(m_methods, RxPwTableQ922lenMask2);
        mMethodOverride(m_methods, RxPwTableQ922lenShift2);
        mMethodOverride(m_methods, RxPwTableQ922FieldMask1);
        mMethodOverride(m_methods, RxPwTableQ922FieldShift1);
        mMethodOverride(m_methods, RxPwTableQ922FieldMask2);
        mMethodOverride(m_methods, RxPwTableQ922FieldShift2);
        mMethodOverride(m_methods, RxPwTableSubSerTypeMask);
        mMethodOverride(m_methods, RxPwTableSubSerTypeShift);
        mMethodOverride(m_methods, RxPwTableTdmActMask);
        mMethodOverride(m_methods, RxPwTableTdmActShift);
        mMethodOverride(m_methods, RxPwTableEncActMask);
        mMethodOverride(m_methods, RxPwTableEncActShift);
        mMethodOverride(m_methods, RxPwTablePsnActMask);
        mMethodOverride(m_methods, RxPwTablePsnActShift);
        mMethodOverride(m_methods, RxPwTableFrgActMask);
        mMethodOverride(m_methods, RxPwTableFrgActShift);
        mMethodOverride(m_methods, RxPwTableSerTypeMask);
        mMethodOverride(m_methods, RxPwTableSerTypeShift);
        mMethodOverride(m_methods, RxPwTableSeridMask);
        mMethodOverride(m_methods, RxPwTableSeridShift);
        mMethodOverride(m_methods, StartVersionUseNewAddressRxErrorPsnPacketCounter);
        mMethodOverride(m_methods, FlowsMaskCreate);

        mBitFieldOverride(ThaModuleCla, m_methods, CLAVlanLabelEn)
        mBitFieldOverride(ThaModuleCla, m_methods, CLAVlanEn)
        mBitFieldOverride(ThaModuleCla, m_methods, CLAVlanFlowDirect)
        mBitFieldOverride(ThaModuleCla, m_methods, CLAVlanGrpWorking)
        mBitFieldOverride(ThaModuleCla, m_methods, CLAVlanGrpIDFlow)
        mBitFieldOverride(ThaModuleCla, m_methods, CLAVlanFlowID)

        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideThaModuleCla(self);
    OverrideThaModuleClaPw(self);
    OverrideThaModuleClaPwV2(self);
    OverrideTha60210011ModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleCla);
    }

AtModule Tha60210012ModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210012ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModuleClaObjectInit(newModule, device);
    }

uint32 Tha60210012ModuleClaHwFlowIdAllocate(ThaModuleCla self, uint32 expectHwFlowId)
    {
    ThaBitMask mask = HwFlowsMask(mThis(self));

    /* Standby driver is not allowed to access hardware */
    if (AtModuleInAccessible((AtModule)self))
        return cInvalidUint32;

    /* To avoid dynamic allocation as much as possible, if the expected
     * resource has not been used, just use it */
    if (ThaBitMaskBitPositionIsValid(mask, expectHwFlowId) &&
        (ThaBitMaskBitVal(mask, expectHwFlowId) == 0))
        {
        ThaBitMaskSetBit(mask, expectHwFlowId);
        return expectHwFlowId;
        }

    /* And just simply allocate new one if that expected resource is being used */
    return FreeHwFlowId(mThis(self));
    }

void Tha60210012ModuleClaHwFlowIdDeallocate(ThaModuleCla self, uint32 hwFlowId)
    {
    ThaBitMaskClearBit(HwFlowsMask(mThis(self)), hwFlowId);
    }

eBool Tha60210012ModuleClaHwFlowIdIsValid(uint32 hwFlowId)
    {
    return ((hwFlowId != cInvalidUint32) ? cAtTrue : cAtFalse);
    }

eAtRet Tha60210012ModuleClaEthFlowExpectedCVlanSet(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *cVlan, eBool enable)
    {
    if (self)
        return EthFlowExpectedCVlanSet(self, flow, cVlan, enable);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ModuleClaVcgServiceEnable(ThaModuleCla self, uint32 vcgId, AtEthFlow flow, eBool enable)
    {
    if (self)
        return VcgServiceEnable(self, vcgId, flow, enable);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ModuleClaEthFlowServiceTypeSet(ThaModuleCla self, AtEthFlow flow, uint8 serviceType)
    {
    uint32 flowId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
    uint32 baseAddress = Tha60210011ModuleClaBaseAddress(self) + flowId;
    uint32 regAddress  = cAf6Reg_cla_per_flow_table_Base + baseAddress;
    uint32 regValues   = mChannelHwRead(flow, regAddress, cThaModuleCla);

    mPolyRegFieldSet(regValues, mThis(self), RxPwTableSerType, serviceType);
    mChannelHwWrite(flow, regAddress, regValues, cThaModuleCla);

    return cAtOk;
    }

eAtRet Tha60210012ModuleClaFlowPppLinkSet(ThaModuleCla self, AtEthFlow flow, AtHdlcLink link)
    {
    const uint8 cKeepPsn = 0;
    const uint8 cEncActionInvalid = 0xF;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 offset = Tha60210011ModuleClaBaseAddress(self) + AtChannelHwIdGet((AtChannel)flow);
    uint32 regAddress = cAf6Reg_cla_per_flow_table_Base + offset;

    mChannelHwLongRead(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableSerType, 0);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableSubSerType, 0);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableTdmAct, 0);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableEncAct, cEncActionInvalid);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTablePsnAct, cKeepPsn);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableSerid, AtChannelIdGet((AtChannel)link));

    mChannelHwLongWrite(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    Tha60210012PhysicalPppLinkFlowServiceTypeUpdate(link, flow);
    return cAtOk;
    }

eAtRet Tha60210012ModuleClaFlowReset(ThaModuleCla self, AtEthFlow flow)
    {
    const uint16 cIdReset = cBit11_0;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 offset = Tha60210011ModuleClaBaseAddress(self) + AtChannelHwIdGet((AtChannel)flow);
    uint32 regAddress = cAf6Reg_cla_per_flow_table_Base + offset;

    mChannelHwLongRead(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableSerType, 0);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableSerid, cIdReset);

    mChannelHwLongWrite(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    Tha60210012EthFlowServiceTypeCache(flow, 0);

    return cAtOk;
    }

eAtRet Tha60210012ModuleClaFlowMpBundleSet(ThaModuleCla self, AtEthFlow flow, AtMpBundle bundle)
    {
    const uint8 cKeepPsn = 0;
    const uint8 cMlpppService = 0xB; /* MLPPP data */
    const uint8 cEncActionInvalid = 0xF;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 offset = Tha60210011ModuleClaBaseAddress(self) + AtChannelHwIdGet((AtChannel)flow);
    uint32 regAddress = cAf6Reg_cla_per_flow_table_Base + offset;
    uint8 hwFragmentSize = FragmentSizeSw2Hw(AtHdlcBundleFragmentSizeGet((AtHdlcBundle)bundle));

    mChannelHwLongRead(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableSerType, 0);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableSubSerType, 0);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableEncAct, cEncActionInvalid);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTablePsnAct, cKeepPsn);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableFrgAct, hwFragmentSize);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableSerid, AtChannelIdGet((AtChannel)bundle));

    mChannelHwLongWrite(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    Tha60210012EthFlowServiceTypeCache(flow, cMlpppService);

    return cAtOk;
    }

eAtRet Tha60210012ModuleClaFlowSequenceNumberModeSet(ThaModuleCla self, AtEthFlow flow, uint8 sequence)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 offset = Tha60210011ModuleClaBaseAddress(self) + AtChannelHwIdGet((AtChannel)flow);
    uint32 regAddress = cAf6Reg_cla_per_flow_table_Base + offset;
    uint8 hwSequenceNumber = SequenceNumberSw2Hw(sequence);

    mChannelHwLongRead(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableTdmAct, hwSequenceNumber);
    mChannelHwLongWrite(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

uint8 Tha60210012ModuleClaFlowSequenceNumberModeGet(ThaModuleCla self, AtEthFlow flow)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 offset = Tha60210011ModuleClaBaseAddress(self) + AtChannelHwIdGet((AtChannel)flow);
    uint32 regAddress = cAf6Reg_cla_per_flow_table_Base + offset;

    mChannelHwLongRead(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return SequenceNumberHw2Sw((uint8)mPolyRegField(longRegVal[0], mThis(self), RxPwTableTdmAct));
    }

eAtRet Tha60210012ModuleClaFlowFragmentSizeSet(ThaModuleCla self, AtEthFlow flow, uint8 fragSize)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 offset = Tha60210011ModuleClaBaseAddress(self) + AtChannelHwIdGet((AtChannel)flow);
    uint32 regAddress = cAf6Reg_cla_per_flow_table_Base + offset;
    uint8 hwFragmentSize = FragmentSizeSw2Hw(fragSize);

    mChannelHwLongRead(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mPolyRegFieldSet(longRegVal[0], mThis(self), RxPwTableFrgAct, hwFragmentSize);
    mChannelHwLongWrite(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

uint8 Tha60210012ModuleClaFlowFragmentSizeGet(ThaModuleCla self, AtEthFlow flow)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 offset = Tha60210011ModuleClaBaseAddress(self) + AtChannelHwIdGet((AtChannel)flow);
    uint32 regAddress = cAf6Reg_cla_per_flow_table_Base + offset;
    uint8 fragSize;

    mChannelHwLongRead(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    fragSize = (uint8)mPolyRegField(longRegVal[0], mThis(self), RxPwTableFrgAct);
    return FragmentSizeHw2Sw(fragSize);
    }

uint32 Tha60210012ModuleClaPwEthFlowHaRestore(Tha60210012ModuleCla self, AtPw pw)
    {
    if (self)
        return PwEthFlowHaRestore(self, pw);
    return 0;
    }

eAtRet Tha60210012ModuleClaEthFlowLanFcsRemoveEnable(ThaModuleCla self, AtEthFlow flow, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 hwFlowId, regAddress, baseAddress;

    if (self == NULL)
        return cAtErrorNullPointer;

    hwFlowId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
    baseAddress = Tha60210011ModuleClaBaseAddress(self) + hwFlowId;
    regAddress  = cAf6Reg_cla_per_flow_table_Base + baseAddress;
    mChannelHwLongRead(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldIns(&longRegVal[cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_DwdIndex],
              mMethodsGet(mThis(self))->RxPwTableLanFcsRmvMask(mThis(self)),
              mMethodsGet(mThis(self))->RxPwTableLanFcsRmvShift(mThis(self)),
              mBoolToBin(enable));
    mChannelHwLongWrite(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return cAtOk;
    }

eBool Tha60210012ModuleClaEthFlowLanFcsRemoveIsEnabled(ThaModuleCla self, AtEthFlow flow)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 hwFlowId, regAddress, baseAddress;
    uint8 hwEnable;

    if (self == NULL)
        return cAtFalse;

    hwFlowId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
    baseAddress = Tha60210011ModuleClaBaseAddress(self) + hwFlowId;
    regAddress  = cAf6Reg_cla_per_flow_table_Base + baseAddress;
    mChannelHwLongRead(flow, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldGet(longRegVal[cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_DwdIndex],
              mMethodsGet(mThis(self))->RxPwTableLanFcsRmvMask(mThis(self)),
              mMethodsGet(mThis(self))->RxPwTableLanFcsRmvShift(mThis(self)),
              uint8,
              &hwEnable);
    return hwEnable ? cAtTrue : cAtFalse;
    }

eAtRet Tha60210012ModuleClaPwLanFcsRemoveEnable(ThaModuleCla self, AtPw pwAdapter, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 hwFlowId, baseAddress, regAddress;

    if (self == NULL)
        return cAtErrorNullPointer;

    hwFlowId = Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pwAdapter);
    baseAddress = Tha60210011ModuleClaBaseAddress(self) + hwFlowId;
    regAddress  = cAf6Reg_cla_per_flow_table_Base + baseAddress;
    mChannelHwLongRead(pwAdapter, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldIns(&longRegVal[cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_DwdIndex],
              mMethodsGet(mThis(self))->RxPwTableLanFcsRmvMask(mThis(self)),
              mMethodsGet(mThis(self))->RxPwTableLanFcsRmvShift(mThis(self)),
              mBoolToBin(enable));
    mChannelHwLongWrite(pwAdapter, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

eBool Tha60210012ModuleClaPwLanFcsRemoveIsEnabled(ThaModuleCla self, AtPw pwAdapter)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 hwFlowId, baseAddress, regAddress;
    uint8 hwEnable;

    if (self == NULL)
        return cAtFalse;

    hwFlowId = Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pwAdapter);
    baseAddress = Tha60210011ModuleClaBaseAddress(self) + hwFlowId;
    regAddress  = cAf6Reg_cla_per_flow_table_Base + baseAddress;
    mChannelHwLongRead(pwAdapter, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldGet(longRegVal[cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_DwdIndex],
              mMethodsGet(mThis(self))->RxPwTableLanFcsRmvMask(mThis(self)),
              mMethodsGet(mThis(self))->RxPwTableLanFcsRmvShift(mThis(self)),
              uint8,
              &hwEnable);
    return hwEnable ? cAtTrue : cAtFalse;
    }

eAtRet Tha60210012ModuleClaPppOamServiceEnable(AtHdlcLink self, uint16 vlanId)
    {
    static const uint8 cPppOam = 2;
    return OamServiceEnable((AtChannel)self, vlanId, cPppOam);
    }

eAtRet Tha60210012ModuleClaPppOamServiceDisable(AtHdlcLink self, uint16 vlanId)
    {
    static const uint8 cInvalidValues = 0xF;
    return OamServiceEnable((AtChannel)self, vlanId, cInvalidValues);
    }

eAtRet Tha60210012ModuleClaMlpppOamServiceEnable(AtHdlcBundle self, uint16 vlanId)
    {
    static const uint8 cMlpppOam = 3;
    return OamServiceEnable((AtChannel)self, vlanId, cMlpppOam);
    }

eAtRet Tha60210012ModuleClaMlpppOamServiceDisable(AtHdlcBundle self, uint16 vlanId)
    {
    static const uint8 cInvalidValues = 0xF;
    return OamServiceEnable((AtChannel)self, vlanId, cInvalidValues);
    }

eAtRet Tha60210011ModuleClaSuperAsynInit(AtObject self)
    {
    return m_AtModuleMethods->AsyncInit((AtModule)self);
    }

eAtRet Tha60210012ModuleClaRxPTCHDisable(AtObject self)
    {
    RxPTCHDisable((AtModule)self);
    return cAtOk;
    }

eAtRet Tha60210012ModuleClaEthernetTypeCheckingEnable(AtObject self)
    {
    EthernetTypeCheckingEnable((AtModule)self);
    return cAtOk;
    }

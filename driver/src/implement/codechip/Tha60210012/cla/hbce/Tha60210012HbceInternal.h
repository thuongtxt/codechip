/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210012HbceInternal.h
 * 
 * Created Date: Aug 23, 2016
 *
 * Description : Internal HBCE
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012HBCEINTERNAL_H_
#define _THA60210012HBCEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210011/cla/hbce/Tha60210011HbceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012Hbce
    {
    tTha60210011Hbce super;
    }tTha60210012Hbce;

/*--------------------------- Forward declarations ---------------------------*/
ThaHbce Tha60210012HbceObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012HBCEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210012Hbce.c
 *
 * Created Date: Feb 29, 2016
 *
 * Description : 60210012 HBCE
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/cla/hbce/ThaHbceMemoryPool.h"
#include "Tha60210012HbceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaHbcePwLabelMask                            cBit21_2
#define cThaHbcePwLabelShift                           2

#define cThaHbcePsnTypeMask                            cBit1_0
#define cThaHbcePsnTypeShift                           0

#define cThaHbcePatern21_13Mask                        cBit21_13
#define cThaHbcePatern21_13Shift                       13
#define cThaHbcePatern21_13DwIndex                     0

#define cThaHbcePatern12_0Mask                         cBit12_0
#define cThaHbcePatern12_0Shift                        0
#define cThaHbcePatern12_0DwIndex                      0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaHbceMethods         m_ThaHbceOverride;
static tTha60210011HbceMethods m_Tha60210011HbceOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumEntries(ThaHbce self)
    {
    AtUnused(self);
    return 8192;
    }

static ThaHbceMemoryPool MemoryPoolCreate(ThaHbce self)
    {
    return Tha60210012HbceMemoryPoolNew(self);
    }

static uint32 NumExtraMemoryPointer(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 2048;
    }

static uint32 MaxNumHashCells(Tha60210011Hbce self)
    {
    AtUnused(self);
    return 32768;
    }

static uint8 HbcePktType(eAtPwPsnType psnType)
    {
    if (psnType == cAtPwPsnTypeUdp)  return 1;
    if (psnType == cAtPwPsnTypeMpls) return 2;
    if (psnType == cAtPwPsnTypeMef)  return 0;

    return 0;
    }

static uint32 HwHbceLabel(ThaHbce self, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType)
    {
    uint32 hwLabel = 0;

    AtUnused(self);
    AtUnused(ethPort);

    mRegFieldSet(hwLabel, cThaHbcePwLabel, label);
    mRegFieldSet(hwLabel, cThaHbcePsnType, HbcePktType(psnType));

    return hwLabel;
    }

static eAtRet RemainBitsGet(uint32 hwLabel, uint32 *remainBits)
    {
    remainBits[0] = mRegField(hwLabel, cThaHbcePatern21_13);
    return cAtOk;
    }

static uint32 HashIndexGet(ThaHbce self, uint32 hwLabel)
    {
    uint32 swHbcePatern21_13 = mRegField(hwLabel, cThaHbcePatern21_13);
    uint32 swHbcePatern12_0  = mRegField(hwLabel, cThaHbcePatern12_0);
    AtUnused(self);

    /* Calculate hash index */
    return swHbcePatern12_0 ^ swHbcePatern21_13;
    }

static uint32 PsnHash(ThaHbce self, uint8 partId, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType, uint32 *remainBits)
    {
    uint32 hwLabel = HwHbceLabel(self, ethPort, label, psnType);
    uint32 hashIndex = HashIndexGet(self, hwLabel);

    AtUnused(partId);

    if (remainBits)
        RemainBitsGet(hwLabel, remainBits);

    return hashIndex;
    }

static void OverrideThaHbce(ThaHbce self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceOverride, mMethodsGet(self), sizeof(m_ThaHbceOverride));

        mMethodOverride(m_ThaHbceOverride, MaxNumEntries);
        mMethodOverride(m_ThaHbceOverride, MemoryPoolCreate);
        mMethodOverride(m_ThaHbceOverride, PsnHash);
        }

    mMethodsSet(self, &m_ThaHbceOverride);
    }

static void Override60210011ThaHbce(ThaHbce self)
    {
    Tha60210011Hbce hbce = (Tha60210011Hbce)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011HbceOverride, mMethodsGet(hbce), sizeof(m_Tha60210011HbceOverride));

        mMethodOverride(m_Tha60210011HbceOverride, NumExtraMemoryPointer);
        mMethodOverride(m_Tha60210011HbceOverride, MaxNumHashCells);
        }

    mMethodsSet(hbce, &m_Tha60210011HbceOverride);
    }

static void Override(ThaHbce self)
    {
    OverrideThaHbce(self);
    Override60210011ThaHbce(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011Hbce);
    }

ThaHbce Tha60210012HbceObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011HbceObjectInit(self, hbceId, claModule, core) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbce Tha60210012HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbce newHbce = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newHbce == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012HbceObjectInit(newHbce, hbceId, claModule, core);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HBCE
 *
 * File        : Tha60210012HbceMemoryPool.c
 *
 * Created Date: Feb 29, 2016
 *
 * Description : HBCE memory pool
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryPool.h"
#include "../../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "../../../Tha60210011/cla/hbce/Tha60210011HbceInternal.h"
#include "../Tha60210012ModuleClaReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012HbceMemoryPool
    {
    tTha60210011HbceMemoryPool super;
    }tTha60210012HbceMemoryPool;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override  */
static tThaHbceMemoryPoolMethods m_ThaHbceMemoryPoolOverride;
static tTha60210011HbceMemoryPoolMethods m_Tha60210011HbceMemoryPoolOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumCells(ThaHbceMemoryPool self)
    {
    AtUnused(self);
    return 48 * 1024;
    }

static uint32 ExtraCellLookupInformationCtrl(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    ThaHbce hbce = self->hbce;
    uint32 indexInExtraPool = cellIndex - Tha60210011HbceMaxNumHashCells(hbce);
    uint32 collisHashIDExtra = indexInExtraPool / Tha60210011HbceNumExtraMemoryPointer(hbce);
    uint32 extraPointer = indexInExtraPool % Tha60210011HbceNumExtraMemoryPointer(hbce);

    return cThaRegClaHbceLookingUpInformationCtr2 + collisHashIDExtra * 0x1000 + extraPointer;
    }

static void OverrideTha60210011HbceMemoryPool(ThaHbceMemoryPool self)
    {
    Tha60210011HbceMemoryPool pool = (Tha60210011HbceMemoryPool)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011HbceMemoryPoolOverride, mMethodsGet(pool), sizeof(m_Tha60210011HbceMemoryPoolOverride));

        mMethodOverride(m_Tha60210011HbceMemoryPoolOverride, ExtraCellLookupInformationCtrl);
        }

    mMethodsSet(pool, &m_Tha60210011HbceMemoryPoolOverride);
    }

static void OverrideThaHbceMemoryPool(ThaHbceMemoryPool self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceMemoryPoolOverride, mMethodsGet(self), sizeof(m_ThaHbceMemoryPoolOverride));

        mMethodOverride(m_ThaHbceMemoryPoolOverride, MaxNumCells);
        }

    mMethodsSet(self, &m_ThaHbceMemoryPoolOverride);
    }

static void Override(ThaHbceMemoryPool self)
    {
    OverrideThaHbceMemoryPool(self);
    OverrideTha60210011HbceMemoryPool(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012HbceMemoryPool);
    }

static ThaHbceMemoryPool ObjectInit(ThaHbceMemoryPool self, ThaHbce hbce)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011HbceMemoryPoolObjectInit(self, hbce) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbceMemoryPool Tha60210012HbceMemoryPoolNew(ThaHbce hbce)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceMemoryPool newPool = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPool == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPool, hbce);
    }

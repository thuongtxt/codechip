/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210012ModuleCla.h
 * 
 * Created Date: Feb 29, 2016
 *
 * Description : 60210012 module CLA utilities
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULECLA_H_
#define _THA60210012MODULECLA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/cla/Tha60210051ModuleClaInternal.h"
#include "controller/Tha60210012ClaPwEthPortController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleCla * Tha60210012ModuleCla;
typedef struct tTha60210012ClaPwController * Tha60210012ClaPwController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60210012ModuleClaHwFlowIdAllocate(ThaModuleCla self, uint32 expectHwFlowId);
void Tha60210012ModuleClaHwFlowIdDeallocate(ThaModuleCla self, uint32 hwFlowId);
eBool Tha60210012ModuleClaHwFlowIdIsValid(uint32 hwFlowId);
eAtRet Tha60210012ModuleClaEthFlowExpectedCVlanSet(ThaModuleCla self, AtEthFlow flow, tAtEthVlanTag *cVlan, eBool enable);
eAtRet Tha60210012ModuleClaEthFlowServiceTypeSet(ThaModuleCla self, AtEthFlow flow, uint8 serviceType);
eAtRet Tha60210012ModuleClaFlowPppLinkSet(ThaModuleCla self, AtEthFlow flow, AtHdlcLink logicLink);
eAtRet Tha60210012ModuleClaFlowReset(ThaModuleCla self, AtEthFlow flow);
eAtRet Tha60210012ModuleClaFlowMpBundleSet(ThaModuleCla self, AtEthFlow flow, AtMpBundle bundle);
eAtRet Tha60210012ModuleClaFlowSequenceNumberModeSet(ThaModuleCla self, AtEthFlow flow, uint8 sequence);
uint8 Tha60210012ModuleClaFlowSequenceNumberModeGet(ThaModuleCla self, AtEthFlow flow);
eAtRet Tha60210012ModuleClaFlowFragmentSizeSet(ThaModuleCla self, AtEthFlow flow, uint8 sequence);
uint8 Tha60210012ModuleClaFlowFragmentSizeGet(ThaModuleCla self, AtEthFlow flow);
eAtRet Tha60210012ModuleClaVcgServiceEnable(ThaModuleCla self, uint32 vcgId, AtEthFlow flow, eBool enable);
uint32 Tha60210012ModuleClaPwEthFlowHaRestore(Tha60210012ModuleCla self, AtPw pw);
void Tha60210012ClaPwControllerExpectedCVlanRegShow(Tha60210012ClaPwController self, AtPw pw);

/* Lan FCS control */
eAtRet Tha60210012ModuleClaEthFlowLanFcsRemoveEnable(ThaModuleCla self, AtEthFlow flow, eBool enable);
eBool Tha60210012ModuleClaEthFlowLanFcsRemoveIsEnabled(ThaModuleCla self, AtEthFlow flow);
eAtRet Tha60210012ModuleClaPwLanFcsRemoveEnable(ThaModuleCla self, AtPw pwAdapter, eBool enable);
eBool Tha60210012ModuleClaPwLanFcsRemoveIsEnabled(ThaModuleCla self, AtPw pwAdapter);

/* OAM */
eAtRet Tha60210012ModuleClaPppOamServiceEnable(AtHdlcLink self, uint16 vlanId);
eAtRet Tha60210012ModuleClaPppOamServiceDisable(AtHdlcLink self, uint16 vlanId);
eAtRet Tha60210012ModuleClaMlpppOamServiceEnable(AtHdlcBundle self, uint16 vlanId);
eAtRet Tha60210012ModuleClaMlpppOamServiceDisable(AtHdlcBundle self, uint16 vlanId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULECLA_H_ */


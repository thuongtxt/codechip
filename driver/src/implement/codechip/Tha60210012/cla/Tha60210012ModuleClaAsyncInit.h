/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210012ModuleClaAsyncInit.h
 * 
 * Created Date: Aug 23, 2016
 *
 * Description : AsyncInit
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULECLAASYNCINIT_H_
#define _THA60210012MODULECLAASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210012ModuleClaAsyncInit(AtObject self);
eAtRet Tha60210011ModuleClaSuperAsynInit(AtObject self);
eAtRet Tha60210012ModuleClaRxPTCHDisable(AtObject self);
eAtRet Tha60210012ModuleClaEthernetTypeCheckingEnable(AtObject self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULECLAASYNCINIT_H_ */


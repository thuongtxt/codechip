/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60210012DeviceAsyncInit.c
 *
 * Created Date: Aug 23, 2016
 *
 * Description : Asyncinit
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtMpBundle.h"
#include "../pw/activator/Tha60210012PwActivator.h"
#include "../pw/Tha60210012ModulePw.h"
#include "../eth/Tha60210012EXaui.h"
#include "../eth/Tha60210012EthFlow.h"
#include "../pwe/Tha60210012ModulePwe.h"
#include "../ram/Tha60210012ModuleRam.h"
#include "Tha60210012ModuleClaReg.h"
#include "Tha60210012ModuleClaInternal.h"
#include "controller/Tha60210012ClaPwEthPortController.h"
#include "Tha60210012ModuleClaAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*tAtAsycnOperationFunc)(AtObject self);


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtAsycnOperationFunc functions[] = {Tha60210011ModuleClaSuperAsynInit,
                                            Tha60210012ModuleClaRxPTCHDisable,
                                            Tha60210012ModuleClaEthernetTypeCheckingEnable};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 AsyncMaxState(AtObject self)
    {
    AtUnused(self);
    return mCount(functions);
    }

static uint32 AsyncStateGet(AtObject self)
    {
    return ((Tha60210012ModuleCla)self)->asyncInitState;
    }

static void AsyncStateIncrease(AtObject self)
    {
    ((Tha60210012ModuleCla)self)->asyncInitState += 1;
    }

static void AsyncStateReset(AtObject self)
    {
    ((Tha60210012ModuleCla)self)->asyncInitState = 0;
    }

static tAtAsycnOperationFunc AsyncOperationGet(AtObject self, uint32 state)
    {
    if (state >= AsyncMaxState(self))
        return NULL;

    return functions[state];
    }

static eAtRet AsyncInitMain(AtObject self)
    {
    uint32 state = AsyncStateGet(self);
    tAtAsycnOperationFunc func = AsyncOperationGet(self, state);
    eAtRet ret = cAtOk;
    tAtOsalCurTime profileTime;

    char stateString[32];
    AtSprintf(stateString, "Async state: %d", state);
    AtOsalCurTimeGet(&profileTime);

    if (func == NULL)
        {
        AsyncStateReset(self);
        return cAtErrorNullPointer;
        }

    ret = func(self);
    AtDeviceAccessTimeInMsSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, &profileTime, __FILE__, __LINE__, stateString);
    AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, __FILE__, __LINE__, stateString);
    if (ret == cAtOk)
        {
        AsyncStateIncrease(self);
        ret = cAtErrorAgain;
        }

    else if (!AtDeviceAsyncRetValIsInState(ret))
        {
        AsyncStateReset(self);
        return ret;
        }

    if (AsyncStateGet(self) == AsyncMaxState(self))
        {
        ret = cAtOk;
        AsyncStateReset(self);
        }

    return ret;
    }

eAtRet Tha60210012ModuleClaAsyncInit(AtObject self)
    {
    return AsyncInitMain(self);
    }

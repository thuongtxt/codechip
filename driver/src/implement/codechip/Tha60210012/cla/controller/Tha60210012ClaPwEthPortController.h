/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210012ClaPwEthPortController.h
 * 
 * Created Date: Mar 21, 2016
 *
 * Description : 60210012 CLA Ethernet port controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012CLAPWETHPORTCONTROLLER_H_
#define _THA60210012CLAPWETHPORTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/cla/ThaModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210012ClaPwEthPortControllerPtchEnable(ThaClaEthPortController self, AtEthPort port, eBool enable);
eBool Tha60210012ClaPwEthPortControllerPtchIsEnabled(ThaClaEthPortController self, AtEthPort port);

eAtRet Tha60210012ClaPwEthPortControllerMaxPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 maxPacketSize);
eAtRet Tha60210012ClaPwEthPortControllerMinPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 minPacketSize);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012CLAPWETHPORTCONTROLLER_H_ */


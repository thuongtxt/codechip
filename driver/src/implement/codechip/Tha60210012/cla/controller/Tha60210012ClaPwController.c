/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210012ClaPwController.c
 *
 * Created Date: Feb 29, 2016
 *
 * Description : 60210011 CLA PW controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHdlcChannel.h"
#include "../../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "../../pw/activator/Tha60210012PwActivator.h"
#include "../../pw/Tha60210012ModulePw.h"
#include "../../eth/Tha60210012ModuleEth.h"
#include "../Tha60210012ModuleClaReg.h"
#include "../Tha60210012ModuleClaInternal.h"
#include "Tha60210012ClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaCLAHbceFlowNumMask                         cAf6_cla_hbce_hash_table_CLAHbceLink_Mask
#define cThaCLAHbceFlowNumShift                        cAf6_cla_hbce_hash_table_CLAHbceLink_Shift
#define cThaCLAHbceMemoryStartPtrMask                  cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Mask
#define cThaCLAHbceMemoryStartPtrShift                 cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Shift

/*--------------------------- Macros -----------------------------------------*/
#define mClaModule(self) ThaClaControllerModuleGet((ThaClaController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012ClaPwControllerMethods m_methods;

/* Override */
static tThaClaPwControllerMethods         m_ThaClaPwControllerOverride;
static tTha60210011ClaPwControllerMethods m_Tha60210011ClaPwControllerOverride;

/* Save super implementation */
static const tThaClaPwControllerMethods   *m_ThaClaPwControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaClaPwController, CLAHbceFlowNum)
mDefineMaskShift(ThaClaPwController, CLAHbceMemoryStartPtr)

static ThaHbce HbceCreate(ThaClaPwController self, uint8 hbceId, AtIpCore core)
    {
    return Tha60210012HbceNew(hbceId, (AtModule)mClaModule(self), core);
    }

static eBool PwLookupModeIsSupported(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode)
    {
    AtUnused(self);
    AtUnused(pw);

    if (lookupMode == cThaPwLookupModeInvalid)
        return cAtFalse;

    /* Return true in both VLAN look up and PSN mode to support look up at two layers */
    return cAtTrue;
    }

static ThaModuleCla ModuleCla(ThaClaPwController self)
    {
    return ThaClaControllerModuleGet((ThaClaController)self);
    }

static uint32 PwFlowControlReg(ThaClaPwController self, AtPw pwAdapter)
    {
    return cAf6Reg_cla_per_flow_table_Base +
           Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pwAdapter) +
           Tha60210011ModuleClaBaseAddress(ModuleCla(self));
    }

static eAtRet PwLabelSet(ThaClaPwController self, AtPw pw, AtPwPsn psn)
    {
    uint8 lookupByLabel = (psn != NULL) ? 1 : 0;
    Tha60210012ModuleCla moduleCla = (Tha60210012ModuleCla)ModuleCla(self);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address;

    /* Updated again remove psn header */
    address = PwFlowControlReg(self, pw);
    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mPolyRegFieldSet(longRegVal[0], moduleCla, RxPwTablePsnAct, (lookupByLabel) ? cRemovePsn : cKeepPsn);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet PwExpectedCVlanSet(ThaClaPwController self, AtPw pw, tAtEthVlanTag *cVlan)
    {
    uint32 address, regVal;
    AtPwGroup group;
    Tha60210012ModuleCla moduleCla = (Tha60210012ModuleCla)ThaClaControllerModuleGet((ThaClaController)self);

    if (cVlan == NULL)
        return cAtOk;

    address = cAf6Reg_cla_vlan_lk_Base + Tha60210011ModuleClaBaseAddress((ThaModuleCla)moduleCla) + cVlan->vlanId;
    regVal = mChannelHwRead(pw, address, cThaModuleCla);

    mPolyRegFieldSet(regVal, moduleCla, CLAVlanFlowID, 0); /* when lookup by label, this field is un-used */
    mPolyRegFieldSet(regVal, moduleCla, CLAVlanEn, 0);
    mPolyRegFieldSet(regVal, moduleCla, CLAVlanLabelEn, cLookupByLabel);

    group = AtPwHsGroupGet(ThaPwAdapterPwGet((ThaPwAdapter)pw));
    if (group)
        {
        mPolyRegFieldSet(regVal, moduleCla, CLAVlanGrpIDFlow, AtPwGroupIdGet(group));
        mPolyRegFieldSet(regVal, moduleCla, CLAVlanGrpWorking, 1);
        mPolyRegFieldSet(regVal, moduleCla, CLAVlanFlowDirect, 0);
        }
    else
        mPolyRegFieldSet(regVal, moduleCla, CLAVlanFlowDirect, 1);

    mChannelHwWrite(pw, address, regVal, cThaModuleCla);
    return cAtOk;
    }

static eBool SupportFullPwCounters(Tha60210011ClaPwController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PwTypeIsSupported(ThaClaPwController self, eAtPwType pwType)
    {
    if ((pwType == cAtPwTypeHdlc) ||
        (pwType == cAtPwTypePpp)  ||
        (pwType == cAtPwTypeFr)   ||
        (pwType == cAtPwTypeMlppp))
        return cAtTrue;

    return m_ThaClaPwControllerMethods->PwTypeIsSupported(self, pwType);
    }

static AtHdlcLink HdlcLinkFromPwAdapter(AtPw self)
    {
    AtChannel channel;
    if (ThaPwTypeIsCesCep(self))
        return NULL;

    channel = AtPwBoundCircuitGet(self);
    if (AtPwTypeGet(self) == cAtPwTypePpp)
        return (AtHdlcLink)channel;
        
    if (AtPwTypeGet(self) == cAtPwTypeHdlc)
        return AtHdlcChannelHdlcLinkGet((AtHdlcChannel)channel);

    return NULL;
    }

static uint8 CesServiceTypeSw2Hw(eAtPwType pwType)
    {
    if (pwType == cAtPwTypeCEP)  
    	return 0x3;
    	
    if ((pwType == cAtPwTypeSAToP) || (pwType == cAtPwTypeCESoP))
        return 0x1;

    /* Unknown mode */
    return 0;
    }

static eAtRet HwPwServiceTypeSet(ThaClaPwController self, AtPw adapter, eAtPwType pwType)
    {
    uint32 regAddress;
    uint32 longRegVal[cThaLongRegMaxSize];
    Tha60210012ModuleCla claModule = (Tha60210012ModuleCla)ModuleCla(self);
    uint8 lookupByLabel = AtPwPsnGet(adapter) ? 1 : 0;

    regAddress = PwFlowControlReg(self, adapter);
    mChannelHwLongRead(adapter, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    if (ThaPwTypeIsCesCep(adapter))
        mPolyRegFieldSet(longRegVal[0], claModule, RxPwTableSerType, CesServiceTypeSw2Hw(pwType));

    mPolyRegFieldSet(longRegVal[0], claModule, RxPwTablePsnAct, (lookupByLabel) ? cRemovePsn : cKeepPsn);
    mPolyRegFieldSet(longRegVal[0], claModule, RxPwTableSerid,  AtChannelIdGet((AtChannel)adapter));

    mChannelHwLongWrite(adapter, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet PwTypeSet(ThaClaPwController self, AtPw pw, eAtPwType pwType)
    {
    if (!ThaClaPwControllerPwTypeIsSupported(self, pwType))
        return cAtErrorModeNotSupport;

    HwPwServiceTypeSet(self, pw, pwType);

    /* CES/CEP */
    if (ThaPwTypeIsCesCep(pw))
        return m_ThaClaPwControllerMethods->PwTypeSet(self, pw, pwType);

    return cAtOk;
    }

static ThaModuleEth ModuleEth(AtPw pw)
    {
    return (ThaModuleEth)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pw), cAtModuleEth);
    }

static uint32 RxPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    const uint8 cRxPacketCounterType = 0;

    if (ThaPwTypeIsHdlcPppFr(pw))
        return Tha60210012ModuleEthHwFlowCounterGet(ModuleEth(pw), Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pw), cRxPacketCounterType, clear);

    return m_ThaClaPwControllerMethods->RxPacketsGet(self, pw, clear);
    }

static uint32 RxDiscardedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    const uint8 cRxDiscardedCounterType = 3;
    AtUnused(self);

    if (ThaPwTypeIsHdlcPppFr(pw))
        return Tha60210012ModuleEthHwFlowCounterGet(ModuleEth(pw), Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pw), cRxDiscardedCounterType, clear);

    return 0;
    }

static uint8 PayloadTypeOverMplsSw2Hw(ThaPwAdapter adapter, eAtPwHdlcPayloadType payloadType, eAtHdlcPduType pduType)
    {
    eAtPwType pwType = AtPwTypeGet((AtPw)adapter);

    if (ThaPwTypeIsCesCep(ThaPwAdapterPwGet(adapter)))
        return 0x0;

    if (payloadType == cAtPwHdlcPayloadTypeFull)
        {
        if (pwType == cAtPwTypePpp)  return 1;
        if (pwType == cAtPwTypeHdlc) return 0;
        return 0;
        }

    /* Payload is PDU */
    if (pduType == cAtHdlcPduTypeIPv4)     return 2;
    if (pduType == cAtHdlcPduTypeIPv6)     return 3;
    if (pduType == cAtHdlcPduTypeEthernet) return 4;

    return 0x0;
    }

static uint8 HdlcPayloadTypeOverMplsSw2Hw(ThaPwAdapter adapter, eAtPwHdlcPayloadType payloadType)
    {
    AtPw pw = ThaPwAdapterPwGet(adapter);
    return PayloadTypeOverMplsSw2Hw(adapter, payloadType, AtHdlcLinkPduTypeGet(HdlcLinkFromPwAdapter(pw)));
    }

static uint8 MlpppPayloadTypeOverMplsSw2Hw(ThaPwAdapter adapter, eAtPwHdlcPayloadType payloadType)
    {
    AtPw pw = ThaPwAdapterPwGet(adapter);
    AtChannel channel = AtPwBoundCircuitGet(pw);
    return PayloadTypeOverMplsSw2Hw(adapter, payloadType, AtHdlcBundlePduTypeGet((AtHdlcBundle)channel));
    }

static uint8 HdlcServiceTypeSw2Hw(AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    eAtPwType pwType = AtPwTypeGet(adapter);

    if (pwType == cAtPwTypePpp)
        return 0x9; /* PPP Data */

    if (pwType == cAtPwTypeFr)
        return 0xD; /* FR Data */

    if (pwType == cAtPwTypeHdlc)
        {
        if (payloadType == cAtPwHdlcPayloadTypeFull)
            return 0x4;

        if (payloadType == cAtPwHdlcPayloadTypePdu)
            {
            AtHdlcChannel hdlcChannel = (AtHdlcChannel)AtPwBoundCircuitGet(adapter);
            eAtHdlcFrameType frameType = AtHdlcChannelFrameTypeGet(hdlcChannel);

            if (frameType == cAtHdlcFrmCiscoHdlc)
                return 0x6;

            if (frameType == cAtHdlcFrmPpp)
                return 0x9;

            if (frameType == cAtHdlcFrmLapd)
                return 0x8; /* HW recommended, LAPD is considered OAM */

            return 0;
            }
        }

    /* Other mode */
    return 0;
    }

static eAtRet HwHdlcPwPayloadTypeSet(ThaClaPwController self, AtPw adapter, uint8 hwServiceType, uint8 hwPldType)
    {
    uint32 regAddress, offset;
    uint32 longRegVal[cThaLongRegMaxSize];
    Tha60210012ModuleCla claModule = (Tha60210012ModuleCla)ModuleCla(self);

    offset = Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)adapter) + Tha60210011ModuleClaBaseAddress(ModuleCla(self));
    regAddress = cAf6Reg_cla_per_flow_table_Base + offset;

    mChannelHwLongRead(adapter, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mPolyRegFieldSet(longRegVal[0], claModule, RxPwTableSerType, hwServiceType);
    mPolyRegFieldSet(longRegVal[0], claModule, RxPwTableSubSerType, hwPldType);
    mChannelHwLongWrite(adapter, regAddress, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet HdlcPwPayloadTypeSet(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    return HwHdlcPwPayloadTypeSet(self, adapter, HdlcServiceTypeSw2Hw(adapter, payloadType), HdlcPayloadTypeOverMplsSw2Hw((ThaPwAdapter)adapter, payloadType));
    }

static eAtRet PwCwEnable(ThaClaPwController self_, AtPw pw, eBool enable)
    {
    AtUnused(self_);

    /* This mode can enable/disable PW CW */
    if (ThaPwTypeIsHdlcPppFr(pw))
        return cAtOk;

    return m_ThaClaPwControllerMethods->PwCwEnable(self_, pw, enable);
    }

static eAtRet MlpppPwPayloadTypeSet(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    uint8 hwServiceType = 0xB;
    return HwHdlcPwPayloadTypeSet(self, adapter, hwServiceType, MlpppPayloadTypeOverMplsSw2Hw((ThaPwAdapter)adapter, payloadType));
    }

static void OneRegShow(AtPw pw, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay((AtChannel)pw, address, cThaModuleCla);
    }

static void ExpectedCVlanRegShow(Tha60210012ClaPwController self, AtPw pw)
    {
    tAtEthVlanTag cVlan;
    if (AtPwEthExpectedCVlanGet(pw, &cVlan))
        {
        ThaModuleCla moduleCla = ThaClaControllerModuleGet((ThaClaController)self);
        uint32 address = cAf6Reg_cla_vlan_lk_Base + Tha60210011ModuleClaBaseAddress(moduleCla) + cVlan.vlanId;
        OneRegShow(pw, "Classify vlan Lookup Control", address);
        }
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaPwControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, mMethodsGet(self), sizeof(m_ThaClaPwControllerOverride));

        mMethodOverride(m_ThaClaPwControllerOverride, HbceCreate);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLookupModeIsSupported);
        mMethodOverride(m_ThaClaPwControllerOverride, PwExpectedCVlanSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwTypeIsSupported);
        mMethodOverride(m_ThaClaPwControllerOverride, PwTypeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxDiscardedPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, HdlcPwPayloadTypeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwCwEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, MlpppPwPayloadTypeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLabelSet);

        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, CLAHbceFlowNum)
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, CLAHbceMemoryStartPtr)
        }

    mMethodsSet(self, &m_ThaClaPwControllerOverride);
    }

static void OverrideTha60210011ClaPwController(ThaClaPwController self)
    {
    Tha60210011ClaPwController controller = (Tha60210011ClaPwController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ClaPwControllerOverride, mMethodsGet(controller), sizeof(m_Tha60210011ClaPwControllerOverride));

        mMethodOverride(m_Tha60210011ClaPwControllerOverride, SupportFullPwCounters);
        }

    mMethodsSet(controller, &m_Tha60210011ClaPwControllerOverride);
    }

static void MethodsInit(ThaClaPwController self)
    {
    Tha60210012ClaPwController thisObject = (Tha60210012ClaPwController)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Initialize method */
        mMethodOverride(m_methods, ExpectedCVlanRegShow);
        }

    mMethodsSet(thisObject, &m_methods);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaPwController(self);
    OverrideTha60210011ClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ClaPwController);
    }

ThaClaPwController Tha60210012ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60210012ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ClaPwControllerObjectInit(newController, cla);
    }

void Tha60210012ClaPwControllerExpectedCVlanRegShow(Tha60210012ClaPwController self, AtPw pw)
    {
    if (self)
        mMethodsGet(self)->ExpectedCVlanRegShow(self, pw);
    }


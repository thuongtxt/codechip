/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210012ClaPwControllerInternal.h
 * 
 * Created Date: Aug 23, 2016
 *
 * Description : Internal CLA PW controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012CLAPWCONTROLLERINTERNAL_H_
#define _THA60210012CLAPWCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210011/cla/controllers/Tha60210011ClaPwControllerInternal.h"
#include "../Tha60210012ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cRemovePsn      1
#define cKeepPsn        0
#define cLookupByLabel  1
#define cLookupByVlanId 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ClaPwControllerMethods
    {
    void (*ExpectedCVlanRegShow)(Tha60210012ClaPwController self, AtPw pw);
    }tTha60210012ClaPwControllerMethods;

typedef struct tTha60210012ClaPwController
    {
    tTha60210011ClaPwController super;
    const tTha60210012ClaPwControllerMethods * methods;

    }tTha60210012ClaPwController;

/*--------------------------- Forward declarations ---------------------------*/
ThaClaPwController Tha60210012ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012CLAPWCONTROLLERINTERNAL_H_ */


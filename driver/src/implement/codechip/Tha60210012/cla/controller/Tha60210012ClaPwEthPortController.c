/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210012ClaPwEthPortController.c
 *
 * Created Date: Mar 21, 2016
 *
 * Description : CLA ETH Port controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60210051/cla/controllers/Tha60210051ClaControllerInternal.h"
#include "../../../Tha60210011/cla/Tha60210011ModuleClaInternal.h"
#include "../../eth/Tha60210012EXauiReg.h"
#include "../../eth/Tha60210012EXaui.h"
#include "../../eth/Tha60210012EthPort.h"
#include "../../man/Tha60210012Device.h"
#include "../Tha60210012ModuleClaReg.h"
#include "../Tha60210012ModuleClaInternal.h"
#include "Tha60210012ClaPwEthPortController.h"
#include "../../../Tha60210051/cla/controllers/Tha60210051ClaPwEthPortControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012ClaPwEthPortController
    {
    tTha60210051ClaPwEthPortController super;
    }tTha60210012ClaPwEthPortController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaEthPortControllerMethods m_ThaClaEthPortControllerOverride;

/* Save super implementation */
static const tThaClaEthPortControllerMethods *m_ThaClaEthPortControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PortOffset(AtEthPort port)
    {
    return AtChannelIdGet((AtChannel)port);
    }

static eAtRet MacSet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = ThaClaControllerClaGlbPsnCtrl(mClaController(self)) + PortOffset(port);

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    mFieldIns(&longRegVal[0], cAf6_cla_glb_psn_RxPsnDAExp_Mask_01, cAf6_cla_glb_psn_RxPsnDAExp_Shift_01, (macAddr[4] << 8 | macAddr[5]));

    longRegVal[1]  = (uint32)(macAddr[0] << 24);
    longRegVal[1] |= (uint32)(macAddr[1] << 16);
    longRegVal[1] |= (uint32)(macAddr[2] << 8);
    longRegVal[1] |= (uint32)(macAddr[3]);

    mChannelHwLongWrite(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet MacGet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = ThaClaControllerClaGlbPsnCtrl(mClaController(self)) + PortOffset(port);
    AtUnused(self);
    
    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    macAddr[0] = (uint8)(longRegVal[1] >> 24);
    macAddr[1] = (uint8)(longRegVal[1] >> 16);
    macAddr[2] = (uint8)(longRegVal[1] >> 8);
    macAddr[3] = (uint8)(longRegVal[1]);
    macAddr[4] = (uint8)(longRegVal[0] >> 24);
    macAddr[5] = (uint8)(longRegVal[0] >> 16);
    
    return cAtOk;
    }

static eAtRet EthPortMacCheckEnable(ThaClaEthPortController self, AtEthPort port, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = ThaClaControllerClaGlbPsnCtrl(mClaController(self)) + PortOffset(port);
    AtUnused(self);

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_cla_glb_psn_RxMacCheckDis_, (enable) ? 0 : 1);
    mChannelHwLongWrite(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eBool EthPortMacCheckIsEnabled(ThaClaEthPortController self, AtEthPort port)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = ThaClaControllerClaGlbPsnCtrl(mClaController(self)) + PortOffset(port);
    AtUnused(self);

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (mRegField(longRegVal[0], cAf6_cla_glb_psn_RxMacCheckDis_) == 1) ? cAtFalse : cAtTrue;
    }

static ThaModuleCla ClaModule(ThaClaEthPortController self)
    {
    return ThaClaControllerModuleGet(mClaController(self));
    }

static ThaVersionReader VersionReader(ThaModuleCla self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static eBool NewAddressRxErrorPsnPacketCounterIsSupported(Tha60210012ModuleCla self)
    {
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((ThaModuleCla)self));
    if (hwVersion >= mMethodsGet(self)->StartVersionUseNewAddressRxErrorPsnPacketCounter(self))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 RxErrPsnPacketsCounterBase(ThaClaEthPortController self, eBool r2c)
    {
    if (r2c)
        {
        if (NewAddressRxErrorPsnPacketCounterIsSupported((Tha60210012ModuleCla)ClaModule(self)))
            return cAf6Reg_Eth_cnt_psn_err_r2c_New_Base;

        return cAf6Reg_Eth_cnt_psn_err_r2c_Base;
        }

    /* Read Only Counters is not change base */
    return cAf6Reg_Eth_cnt_psn_err_ro_Base;
    }

static uint32 RxErrPsnPacketsRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    uint32 counterBase = RxErrPsnPacketsCounterBase(self, r2c);
    uint32 counterAddress = (AtChannelIdGet((AtChannel)port) + Tha60210011ModuleClaBaseAddress(ClaModule(self))) + counterBase;

    return mChannelHwRead(port, counterAddress, cThaModuleCla);
    }

static uint32 EthPktFcsErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (Tha60210012DeviceEXauiIsReady(AtChannelDeviceGet((AtChannel)port)) && Tha60210012EthPortIsEXauiPort(port))
        {
        uint32 portBaseAddress = ((r2c) ? 0x800 : 0x0) + Tha60210012EthEXauiPortBaseAddress(port);
        return mChannelHwRead(port, cAf6Reg_RxEth_cnt_fcs_err_ro_Base + portBaseAddress, cThaModuleCla);
        }

    return m_ThaClaEthPortControllerMethods->EthPktFcsErrRead2Clear(self, port, r2c);
    }

static eAtRet MaxPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 maxPacketSize)
    {
    uint32 regAddr, regValue;

    regAddr = cAf6Reg_cla_per_eth_enb_Base + Tha60210011ModuleClaBaseAddress(ClaModule((ThaClaEthPortController)self));
    regValue = mChannelHwRead(port, regAddr, cThaModuleCla);
    mRegFieldSet(regValue, cAf6_cla_per_eth_enb_CLAEthPktMax_, maxPacketSize);
    mChannelHwWrite(port, regAddr, regValue, cThaModuleCla);

    return cAtOk;
    }

static uint32 MaxPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
    uint32 regAddr = cAf6Reg_cla_per_eth_enb_Base + Tha60210011ModuleClaBaseAddress(ClaModule((ThaClaEthPortController)self));
    uint32 regValue = mChannelHwRead(port, regAddr, cThaModuleCla);
    return mRegField(regValue, cAf6_cla_per_eth_enb_CLAEthPktMax_);
    }

static eAtRet MinPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 minPacketSize)
    {
    uint32 regAddr, regValue;

    regAddr = cAf6Reg_cla_per_eth_enb_Base + Tha60210011ModuleClaBaseAddress(ClaModule((ThaClaEthPortController)self));
    regValue = mChannelHwRead(port, regAddr, cThaModuleCla);
    mRegFieldSet(regValue, cAf6_cla_per_eth_enb_CLAEthPktMin_, minPacketSize);
    mChannelHwWrite(port, regAddr, regValue, cThaModuleCla);

    return cAtOk;
    }

static uint32 MinPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
    uint32 regAddr = cAf6Reg_cla_per_eth_enb_Base + Tha60210011ModuleClaBaseAddress(ClaModule((ThaClaEthPortController)self));
    uint32 regValue = mChannelHwRead(port, regAddr, cThaModuleCla);
    return mRegField(regValue, cAf6_cla_per_eth_enb_CLAEthPktMin_);
    }

static uint16 SVlanTpid(ThaClaEthPortController self, AtEthPort port)
    {
    AtUnused(port);
    AtUnused(self);
    return 0x8100;
    }

static void OverrideThaClaEthPortController(ThaClaEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaEthPortControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthPortControllerOverride));

        /* Counters */
        mMethodOverride(m_ThaClaEthPortControllerOverride, MacSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MacGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPortMacCheckEnable);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPortMacCheckIsEnabled);
        mMethodOverride(m_ThaClaEthPortControllerOverride, RxErrPsnPacketsRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktFcsErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MaxPacketSizeGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MinPacketSizeGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, SVlanTpid);
        }

    mMethodsSet(self, &m_ThaClaEthPortControllerOverride);
    }

static void Override(ThaClaEthPortController self)
    {
    OverrideThaClaEthPortController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ClaPwEthPortController);
    }

static ThaClaEthPortController ObjectInit(ThaClaEthPortController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ClaPwEthPortControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthPortController Tha60210012ClaPwEthPortControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cla);
    }

eAtRet Tha60210012ClaPwEthPortControllerPtchEnable(ThaClaEthPortController self, AtEthPort port, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = ThaClaControllerClaGlbPsnCtrl(mClaController(self)) + PortOffset(port);
    AtUnused(self);

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_cla_glb_psn_RxPTCHDis_, (enable) ? 0 : 1);
    mRegFieldSet(longRegVal[2], cAf6_cla_glb_psn_EthPTCHExtraDisable_, (enable) ? 0 : 1);
    mChannelHwLongWrite(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

eBool Tha60210012ClaPwEthPortControllerPtchIsEnabled(ThaClaEthPortController self, AtEthPort port)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = ThaClaControllerClaGlbPsnCtrl(mClaController(self)) + PortOffset(port);
    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (longRegVal[0] & cAf6_cla_glb_psn_RxPTCHDis_Mask) ? cAtFalse : cAtTrue;
    }

eAtRet Tha60210012ClaPwEthPortControllerMaxPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 maxPacketSize)
    {
    if (self)
        return MaxPacketSizeSet(self, port, maxPacketSize);
    return cAtErrorNullPointer;
    }
    
eAtRet Tha60210012ClaPwEthPortControllerMinPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 minPacketSize)
    {
    if (self)
        return MinPacketSizeSet(self, port, minPacketSize);
    return cAtErrorNullPointer;
    }

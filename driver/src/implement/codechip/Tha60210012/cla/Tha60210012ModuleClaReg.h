/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210012ModuleClaReg.h
 * 
 * Created Date: Feb 29, 2016
 *
 * Description : CLA register description header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _CLA_THA60210012MODULECLAREG_H_
#define _CLA_THA60210012MODULECLAREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : Classify Global PSN Control
Reg Addr   : 0x00000000
Reg Formula:
    Where  :
Reg Desc   :
This register configures identification per Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_glb_psn_Base                                                                    0x00000000
#define cAf6Reg_cla_glb_psn                                                                         0x00000000
#define cAf6Reg_cla_glb_psn_WidthVal                                                                        96
#define cAf6Reg_cla_glb_psn_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: EthPTCHDisEx
BitField Type: RW
BitField Desc:
BitField Bits: [91]
  1: Disable (SW need to default)
  0: Enable %% RW %% 0x0 %% 0x0
--------------------------------------*/
#define cAf6_cla_glb_psn_EthPTCHExtraDisable_Bit_Start                                                        91
#define cAf6_cla_glb_psn_EthPTCHExtraDisable_Mask                                                         cBit27
#define cAf6_cla_glb_psn_EthPTCHExtraDisable_Shift                                                            27
#define cAf6_cla_glb_psn_EthPTCHExtraDisable_DwordIndex                                                       2

/*--------------------------------------
BitField Name: Vlan Control enable
BitField Type: RW
BitField Desc:
BitField Bits: [90]
--------------------------------------*/
#define cAf6_cla_glb_psn_VlanCtrlEnable_Bit_Start                                                        90
#define cAf6_cla_glb_psn_VlanCtrlEnable_Mask                                                         cBit26
#define cAf6_cla_glb_psn_VlanCtrlEnable_Shift                                                            26

/*--------------------------------------
BitField Name: EthTypeDis
BitField Type: RW
BitField Desc: The Eth_type for user define
BitField Bits: [88]
  1: Disable checking (SW need to default)
  0: Enable checking %% RW %% 0x0 %% 0x0
--------------------------------------*/
#define cAf6_cla_glb_psn_EthTypeDisable_Bit_Start                                                        88
#define cAf6_cla_glb_psn_EthTypeDisable_Bit_End                                                          88
#define cAf6_cla_glb_psn_EthTypeDisable_Mask                                                         cBit24
#define cAf6_cla_glb_psn_EthTypeDisable_Shift                                                            24
#define cAf6_cla_glb_psn_EthTypeDisable_DwordIndex                                                       2

/*--------------------------------------
BitField Name: UserDefineEthType
BitField Type: RW
BitField Desc: The Eth_type for user define
BitField Bits: [87:72]
--------------------------------------*/
#define cAf6_cla_glb_psn_UserDefineEthType_Bit_Start                                                        72
#define cAf6_cla_glb_psn_UserDefineEthType_Bit_End                                                          87
#define cAf6_cla_glb_psn_UserDefineEthType_Mask                                                       cBit23_8
#define cAf6_cla_glb_psn_UserDefineEthType_Shift                                                             8
#define cAf6_cla_glb_psn_UserDefineEthType_MaxVal                                                          0x0
#define cAf6_cla_glb_psn_UserDefineEthType_MinVal                                                          0x0
#define cAf6_cla_glb_psn_UserDefineEthType_RstVal                                                          0x0

/*--------------------------------------
BitField Name: RxPTCHOffset
BitField Type: RW
BitField Desc: The offset for channelized XAUI
BitField Bits: [71:64]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPTCHOffset_Bit_Start                                                             64
#define cAf6_cla_glb_psn_RxPTCHOffset_Bit_End                                                               71
#define cAf6_cla_glb_psn_RxPTCHOffset_Mask                                                             cBit7_0
#define cAf6_cla_glb_psn_RxPTCHOffset_Shift                                                                  0
#define cAf6_cla_glb_psn_RxPTCHOffset_MaxVal                                                               0x0
#define cAf6_cla_glb_psn_RxPTCHOffset_MinVal                                                               0x0
#define cAf6_cla_glb_psn_RxPTCHOffset_RstVal                                                               0x0

/*--------------------------------------
BitField Name: RxPsnDAExp
BitField Type: RW
BitField Desc: Mac Address expected at Rx Ethernet port
BitField Bits: [63:16]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnDAExp_Bit_Start                                                               16
#define cAf6_cla_glb_psn_RxPsnDAExp_Bit_End                                                                 63
#define cAf6_cla_glb_psn_RxPsnDAExp_Mask_01                                                          cBit31_16
#define cAf6_cla_glb_psn_RxPsnDAExp_Shift_01                                                                16
#define cAf6_cla_glb_psn_RxPsnDAExp_Mask_02                                                           cBit31_0
#define cAf6_cla_glb_psn_RxPsnDAExp_Shift_02                                                                 0

/*--------------------------------------
BitField Name: RxVlanCtrl
BitField Type: RW
BitField Desc: Vlan control to identify packet is OAM
BitField Bits: [15:4]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxVlanCtrl_Bit_Start                                                                4
#define cAf6_cla_glb_psn_RxVlanCtrl_Bit_End                                                                 15
#define cAf6_cla_glb_psn_RxVlanCtrl_Mask                                                              cBit15_4
#define cAf6_cla_glb_psn_RxVlanCtrl_Shift                                                                    4
#define cAf6_cla_glb_psn_RxVlanCtrl_MaxVal                                                               0xfff
#define cAf6_cla_glb_psn_RxVlanCtrl_MinVal                                                                 0x0
#define cAf6_cla_glb_psn_RxVlanCtrl_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: RxMacCheckDis
BitField Type: RW
BitField Desc: Disable to check MAC address at Ethernet port receive direction
1: Disable checking 0: Enable checking
BitField Bits: [3]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxMacCheckDis_Bit_Start                                                             3
#define cAf6_cla_glb_psn_RxMacCheckDis_Bit_End                                                               3
#define cAf6_cla_glb_psn_RxMacCheckDis_Mask                                                              cBit3
#define cAf6_cla_glb_psn_RxMacCheckDis_Shift                                                                 3
#define cAf6_cla_glb_psn_RxMacCheckDis_MaxVal                                                              0x1
#define cAf6_cla_glb_psn_RxMacCheckDis_MinVal                                                              0x0
#define cAf6_cla_glb_psn_RxMacCheckDis_RstVal                                                              0x0

/*--------------------------------------
BitField Name: RxPsnIpUdpMode
BitField Type: RW
BitField Desc: This bit is applicable for Ipv4/Ipv6 packet from Ethernet side 1:
Classify engine uses RxPsnIpUdpSel to decide which UDP port (Source or
Destination) is used to identify pseudowire packet 0: Classify engine will
automatically search for value 0x85E in source or destination UDP port. The
remaining UDP port is used to identify pseudowire packet
BitField Bits: [2]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_Bit_Start                                                            2
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_Bit_End                                                              2
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_Mask                                                             cBit2
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_Shift                                                                2
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_MaxVal                                                             0x1
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_MinVal                                                             0x0
#define cAf6_cla_glb_psn_RxPsnIpUdpMode_RstVal                                                             0x0

/*--------------------------------------
BitField Name: RxPsnIpUdpSel
BitField Type: RW
BitField Desc: This bit is applicable for Ipv4/Ipv6 using to select Source or
Destination to identify pseudowire packet from Ethernet side. It is not use when
RxPsnIpUdpMode is zero 1: Classify engine selects source UDP port to identify
pseudowire packet 0: Classify engine selects destination UDP port to identify
pseudowire packet
BitField Bits: [1]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_Bit_Start                                                             1
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_Bit_End                                                               1
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_Mask                                                              cBit1
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_Shift                                                                 1
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_MaxVal                                                              0x1
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_MinVal                                                              0x0
#define cAf6_cla_glb_psn_RxPsnIpUdpSel_RstVal                                                              0x0

/*--------------------------------------
BitField Name: RxPTCHDis
BitField Type: RW
BitField Desc: Disable PTCH 1: Disable PTCH 0: Enable PTCH
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cla_glb_psn_RxPTCHDis_Bit_Start                                                                 0
#define cAf6_cla_glb_psn_RxPTCHDis_Bit_End                                                                   0
#define cAf6_cla_glb_psn_RxPTCHDis_Mask                                                                  cBit0
#define cAf6_cla_glb_psn_RxPTCHDis_Shift                                                                     0
#define cAf6_cla_glb_psn_RxPTCHDis_MaxVal                                                                  0x1
#define cAf6_cla_glb_psn_RxPTCHDis_MinVal                                                                  0x0
#define cAf6_cla_glb_psn_RxPTCHDis_RstVal                                                                  0x0

/*------------------------------------------------------------------------------
Reg Name   : Classify Per Pseudowire Type Control
Reg Addr   : 0x0008000 - 0x00008FFF #The address format for these registers is 0x0008000 + PWID
Reg Formula: 0x0008000 +  PWID
    Where  :
           + $PWID(0-3071): Pseudowire ID
Reg Desc   :
This register configures identification types per pseudowire

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_per_pw_ctrl_Base                                                                 0x0008000
#define cAf6Reg_cla_per_pw_ctrl(PWID)                                                       (0x0008000+(PWID))
#define cAf6Reg_cla_per_pw_ctrl_WidthVal                                                                    64
#define cAf6Reg_cla_per_pw_ctrl_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: RxEthPwLen
BitField Type: RW
BitField Desc: length of packet to check malform
BitField Bits: [59:46]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_Bit_Start                                                           46
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_Bit_End                                                             59
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_Mask                                                         cBit27_14
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_Shift                                                               14
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_MaxVal                                                             0x0
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_MinVal                                                             0x0
#define cAf6_cla_per_pw_ctrl_RxEthPwLen_RstVal                                                             0x0

/*--------------------------------------
BitField Name: RxEthSupEn
BitField Type: RW
BitField Desc: Suppress enable
fractional
BitField Bits: [45]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_Bit_Start                                                         45
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_Bit_End                                                           45
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_Mask                                                          cBit13
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_Shift                                                             13
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_MaxVal                                                           0x0
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_MinVal                                                           0x0
#define cAf6_cla_per_pw_ctrl_RxEthSupEn_RstVal                                                           0x0

/*--------------------------------------
BitField Name: RxEthCepMode
BitField Type: RW
BitField Desc: CEP mode working 0,1: CEP basic
fractional
BitField Bits: [44]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_Bit_Start                                                         44
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_Bit_End                                                           44
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_Mask                                                          cBit12
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_Shift                                                             12
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_MaxVal                                                           0x0
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_MinVal                                                           0x0
#define cAf6_cla_per_pw_ctrl_RxEthCepMode_RstVal                                                           0x0

/*--------------------------------------
BitField Name: RxEthRtpSsrcValue
BitField Type: RW
BitField Desc: This value is used to compare with SSRC value in RTP header of
received TDM PW packets
BitField Bits: [43:12]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcValue_Bit_Start                                                    12
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcValue_Bit_End                                                      43
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcValue_Mask_01                                               cBit31_12
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcValue_Shift_01                                                     12
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcValue_Mask_02                                                cBit11_0
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcValue_Shift_02                                                      0

/*--------------------------------------
BitField Name: RxEthRtpPtValue
BitField Type: RW
BitField Desc: This value is used to compare with PT value in RTP header of
received TDM PW packets
BitField Bits: [11:5]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_Bit_Start                                                       5
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_Bit_End                                                        11
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_Mask                                                     cBit11_5
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_Shift                                                           5
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_MaxVal                                                       0x7f
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_MinVal                                                        0x0
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtValue_RstVal                                                        0x0

/*--------------------------------------
BitField Name: RxEthRtpEn
BitField Type: RW
BitField Desc: Enable RTP 1: Enable RTP 0: Disable RTP
BitField Bits: [4]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_Bit_Start                                                            4
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_Bit_End                                                              4
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_Mask                                                             cBit4
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_Shift                                                                4
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_MaxVal                                                             0x1
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_MinVal                                                             0x0
#define cAf6_cla_per_pw_ctrl_RxEthRtpEn_RstVal                                                             0x0

/*--------------------------------------
BitField Name: RxEthRtpSsrcChkEn
BitField Type: RW
BitField Desc: Enable checking SSRC field of RTP header in received TDM PW
packet 1: Enable checking 0: Disable checking
BitField Bits: [3]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_Bit_Start                                                     3
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_Bit_End                                                       3
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_Mask                                                      cBit3
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_Shift                                                         3
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_MaxVal                                                      0x1
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_MinVal                                                      0x0
#define cAf6_cla_per_pw_ctrl_RxEthRtpSsrcChkEn_RstVal                                                      0x0

/*--------------------------------------
BitField Name: RxEthRtpPtChkEn
BitField Type: RW
BitField Desc: Enable checking PT field of RTP header in received TDM PW packet
1: Enable checking 0: Disable checking
BitField Bits: [2]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_Bit_Start                                                       2
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_Bit_End                                                         2
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_Mask                                                        cBit2
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_Shift                                                           2
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_MaxVal                                                        0x1
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_MinVal                                                        0x0
#define cAf6_cla_per_pw_ctrl_RxEthRtpPtChkEn_RstVal                                                        0x0

/*--------------------------------------
BitField Name: RxEthPwType
BitField Type: RW
BitField Desc: this is PW type working 0: CES mode without CAS 1: CES mode with
CAS 2: CEP mode
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_cla_per_pw_ctrl_RxEthPwType_Bit_Start                                                           0
#define cAf6_cla_per_pw_ctrl_RxEthPwType_Bit_End                                                             1
#define cAf6_cla_per_pw_ctrl_RxEthPwType_Mask                                                          cBit1_0
#define cAf6_cla_per_pw_ctrl_RxEthPwType_Shift                                                               0
#define cAf6_cla_per_pw_ctrl_RxEthPwType_MaxVal                                                            0x3
#define cAf6_cla_per_pw_ctrl_RxEthPwType_MinVal                                                            0x0
#define cAf6_cla_per_pw_ctrl_RxEthPwType_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per flow Table Control
Reg Addr   : 0x0004000 - 0x0004FFF #The address format for these registers is 0x0004000 + FLOWID
Reg Formula: 0x0004000 +  FLOWID
    Where  :
           + $FLOWID(0-4095): Flow ID
Reg Desc   :
This register configures table action for flow traffic

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_per_flow_table_Base                                                              0x0004000
#define cAf6Reg_cla_per_flow_table(FLOWID)                                                (0x0004000+(FLOWID))
#define cAf6Reg_cla_per_flow_table_WidthVal                                                                 96
#define cAf6Reg_cla_per_flow_table_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: RxPwTableLanFcsRmv
BitField Type: RW
BitField Desc: Remove LAN FCS 1: Remove LAN FCS 0: Keep LAN FCS
BitField Bits: [66]
--------------------------------------*/
#define cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_Bit_Start                                                66
#define cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_Bit_End                                                  66
#define cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_Mask                                                  cBit2
#define cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_Shift                                                     2
#define cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_MaxVal                                                  0x0
#define cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_MinVal                                                  0x0
#define cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_RstVal                                                  0x0
#define cAf6_cla_per_flow_table_RxPwTableLanFcsRmv_DwdIndex                                                2

/*--------------------------------------
BitField Name: RxPwTableQ922len
BitField Type: RW
BitField Desc: Q922 length 2/3/4 bytes 0: 2 bytes 1: 3 bytes 2: 4 bytes
BitField Bits: [65:64]
--------------------------------------*/
#define cAf6_cla_per_flow_table_RxPwTableQ922len_Bit_Start                                                  64
#define cAf6_cla_per_flow_table_RxPwTableQ922len_Bit_End                                                    65
#define cAf6_cla_per_flow_table_RxPwTableQ922len_Mask                                                  cBit1_0
#define cAf6_cla_per_flow_table_RxPwTableQ922len_Shift                                                       0
#define cAf6_cla_per_flow_table_RxPwTableQ922len_MaxVal                                                    0x0
#define cAf6_cla_per_flow_table_RxPwTableQ922len_MinVal                                                    0x0
#define cAf6_cla_per_flow_table_RxPwTableQ922len_RstVal                                                    0x0

/*--------------------------------------
BitField Name: RxPwTableQ922Field
BitField Type: RW
BitField Desc: Q922 field 2/3/4 bytes (LSB)
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_cla_per_flow_table_RxPwTableQ922Field_Bit_Start                                                32
#define cAf6_cla_per_flow_table_RxPwTableQ922Field_Bit_End                                                  63
#define cAf6_cla_per_flow_table_RxPwTableQ922Field_Mask                                               cBit31_0
#define cAf6_cla_per_flow_table_RxPwTableQ922Field_Shift                                                     0
#define cAf6_cla_per_flow_table_RxPwTableQ922Field_MaxVal                                                  0x0
#define cAf6_cla_per_flow_table_RxPwTableQ922Field_MinVal                                                  0x0
#define cAf6_cla_per_flow_table_RxPwTableQ922Field_RstVal                                                  0x0

/*--------------------------------------
BitField Name: RxPwTableSubSerType
BitField Type: RW
BitField Desc: Sub-service type 0: None 1: PPPoMPLS 2: IPv4oMPLS 3: IPv6oMPLS 4:
ETHoMPLS 5: Reserve
BitField Bits: [31:28]
--------------------------------------*/
#define cAf6_cla_per_flow_table_RxPwTableSubSerType_Bit_Start                                               28
#define cAf6_cla_per_flow_table_RxPwTableSubSerType_Bit_End                                                 31
#define cAf6_cla_per_flow_table_RxPwTableSubSerType_Mask                                             cBit31_28
#define cAf6_cla_per_flow_table_RxPwTableSubSerType_Shift                                                   28
#define cAf6_cla_per_flow_table_RxPwTableSubSerType_MaxVal                                                 0xf
#define cAf6_cla_per_flow_table_RxPwTableSubSerType_MinVal                                                 0x0
#define cAf6_cla_per_flow_table_RxPwTableSubSerType_RstVal                                                 0x0

/*--------------------------------------
BitField Name: RxPwTableTdmAct
BitField Type: RW
BitField Desc: TDM header action 0: Nothing 1: MLPPP short mode 2: MLPPP long
mode 3: MLFR UNI/NNI mode 4: MLFR End2End mode other: Reserve
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_cla_per_flow_table_RxPwTableTdmAct_Bit_Start                                                   24
#define cAf6_cla_per_flow_table_RxPwTableTdmAct_Bit_End                                                     27
#define cAf6_cla_per_flow_table_RxPwTableTdmAct_Mask                                                 cBit27_24
#define cAf6_cla_per_flow_table_RxPwTableTdmAct_Shift                                                       24
#define cAf6_cla_per_flow_table_RxPwTableTdmAct_MaxVal                                                     0xf
#define cAf6_cla_per_flow_table_RxPwTableTdmAct_MinVal                                                     0x0
#define cAf6_cla_per_flow_table_RxPwTableTdmAct_RstVal                                                     0x0

/*--------------------------------------
BitField Name: RxPwTableEncAct
BitField Type: RW
BitField Desc: ENC header action 0: Frame relay routed frame NLPID 1: Frame
relay routed frame SNAP 2: Frame relay bridge frame with LAN FCS option 3: Frame
relay bridge frame without LAN FCS other: Reserve
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_cla_per_flow_table_RxPwTableEncAct_Bit_Start                                                   20
#define cAf6_cla_per_flow_table_RxPwTableEncAct_Bit_End                                                     23
#define cAf6_cla_per_flow_table_RxPwTableEncAct_Mask                                                 cBit23_20
#define cAf6_cla_per_flow_table_RxPwTableEncAct_Shift                                                       20
#define cAf6_cla_per_flow_table_RxPwTableEncAct_MaxVal                                                     0xf
#define cAf6_cla_per_flow_table_RxPwTableEncAct_MinVal                                                     0x0
#define cAf6_cla_per_flow_table_RxPwTableEncAct_RstVal                                                     0x0

/*--------------------------------------
BitField Name: RxPwTablePsnAct
BitField Type: RW
BitField Desc: PSN action 0: Keep PSN 1: Remove PSN in case
CES/CEP/IPv4oMPLS/IPv6oMPLS/ETHoMPLS/
BitField Bits: [19]
--------------------------------------*/
#define cAf6_cla_per_flow_table_RxPwTablePsnAct_Bit_Start                                                   19
#define cAf6_cla_per_flow_table_RxPwTablePsnAct_Bit_End                                                     19
#define cAf6_cla_per_flow_table_RxPwTablePsnAct_Mask                                                    cBit19
#define cAf6_cla_per_flow_table_RxPwTablePsnAct_Shift                                                       19
#define cAf6_cla_per_flow_table_RxPwTablePsnAct_MaxVal                                                     0x1
#define cAf6_cla_per_flow_table_RxPwTablePsnAct_MinVal                                                     0x0
#define cAf6_cla_per_flow_table_RxPwTablePsnAct_RstVal                                                     0x0

/*--------------------------------------
BitField Name: RxPwTableFrgAct
BitField Type: RW
BitField Desc: Fragment action for MLPPP/MLFR 0: Not fragment 1: Fragment 128
bytes 2: Fragment 256 bytes 3: Fragment 512 bytes 4: Reserve
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_cla_per_flow_table_RxPwTableFrgAct_Bit_Start                                                   16
#define cAf6_cla_per_flow_table_RxPwTableFrgAct_Bit_End                                                     18
#define cAf6_cla_per_flow_table_RxPwTableFrgAct_Mask                                                 cBit18_16
#define cAf6_cla_per_flow_table_RxPwTableFrgAct_Shift                                                       16
#define cAf6_cla_per_flow_table_RxPwTableFrgAct_MaxVal                                                     0x7
#define cAf6_cla_per_flow_table_RxPwTableFrgAct_MinVal                                                     0x0
#define cAf6_cla_per_flow_table_RxPwTableFrgAct_RstVal                                                     0x0

/*--------------------------------------
BitField Name: RxPwTableSerType
BitField Type: RW
BitField Desc: Service type 0: Discard 1: CES 2: EoP 3: CEP 4: HDLC 5: ETH Pass
through 6: cHDLC 7: Reserve 8: PPP OAM 9: PPP data 10: MLPPP OAM 11: MLPPP data
12: FR OAM 13: FR data 14: MLFR OAM 15: MLFR data
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_cla_per_flow_table_RxPwTableSerType_Bit_Start                                                  12
#define cAf6_cla_per_flow_table_RxPwTableSerType_Bit_End                                                    15
#define cAf6_cla_per_flow_table_RxPwTableSerType_Mask                                                cBit15_12
#define cAf6_cla_per_flow_table_RxPwTableSerType_Shift                                                      12
#define cAf6_cla_per_flow_table_RxPwTableSerType_MaxVal                                                    0xf
#define cAf6_cla_per_flow_table_RxPwTableSerType_MinVal                                                    0x0
#define cAf6_cla_per_flow_table_RxPwTableSerType_RstVal                                                    0x0

/*--------------------------------------
BitField Name: RxPwTableSerid
BitField Type: RW
BitField Desc: Service ID (VCG/Link/PW/Bundle)
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_cla_per_flow_table_RxPwTableSerid_Bit_Start                                                     0
#define cAf6_cla_per_flow_table_RxPwTableSerid_Bit_End                                                      11
#define cAf6_cla_per_flow_table_RxPwTableSerid_Mask                                                   cBit11_0
#define cAf6_cla_per_flow_table_RxPwTableSerid_Shift                                                         0
#define cAf6_cla_per_flow_table_RxPwTableSerid_MaxVal                                                    0xfff
#define cAf6_cla_per_flow_table_RxPwTableSerid_MinVal                                                      0x0
#define cAf6_cla_per_flow_table_RxPwTableSerid_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Hashing Table Control
Reg Addr   : 0x00020000 - 0x00021FFF
Reg Formula: 0x00020000 + HashID
    Where  :
           + $HashID(0-8191): HashID
Reg Desc   :
HBCE module uses 13 bits for tab-index. Tab-index is generated by hashing function applied to the original id. %%
Hashing function applies an XOR function to all bits of tab-index. %%
The indexes to the tab-index are generated by hashing function and therefore collisions may occur. %%
There are maximum fours (4) entries for every hash to identify flow traffic whether match or not. %%
If the collisions are more than fours (4), they are handled by pointer to link another memory. %%
The formula of hash pattern is {label ID(20bits), PSN mode (2bits)}, call HashPattern (22bits)%%
#The HashID formula has two case depend on CLAHbceCodingSelectedMode%%
#CLAHbceCodingSelectedMode = 1: HashID = HashPattern[13:0] XOR {4'd0,HashPattern[23:14]}, CLAHbceStoreID = HashPattern[23:14]%%
#CLAHbceCodingSelectedMode = 0: HashID = HashPattern[13:0], CLAHbceStoreID = HashPattern[23:14]
HashID = HashPattern[12:0] XOR {4'd0,HashPattern[21:13]}, CLAHbceStoreID = HashPattern[21:13]

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_hbce_hash_table_Base                                                            0x00020000
#define cAf6Reg_cla_hbce_hash_table(HashID)                                              (0x00020000+(HashID))
#define cAf6Reg_cla_hbce_hash_table_WidthVal                                                                32
#define cAf6Reg_cla_hbce_hash_table_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAHbceLink
BitField Type: RW
BitField Desc: this is pointer to link extra location for conflict hash more
than fours 1: Link to another memory 0: not link more
BitField Bits: [11]
--------------------------------------*/
#define cAf6_cla_hbce_hash_table_CLAHbceLink_Bit_Start                                                      11
#define cAf6_cla_hbce_hash_table_CLAHbceLink_Bit_End                                                        11
#define cAf6_cla_hbce_hash_table_CLAHbceLink_Mask                                                       cBit11
#define cAf6_cla_hbce_hash_table_CLAHbceLink_Shift                                                          11
#define cAf6_cla_hbce_hash_table_CLAHbceLink_MaxVal                                                        0x1
#define cAf6_cla_hbce_hash_table_CLAHbceLink_MinVal                                                        0x0
#define cAf6_cla_hbce_hash_table_CLAHbceLink_RstVal                                                        0x0

/*--------------------------------------
BitField Name: CLAHbceMemoryExtraStartPointer
BitField Type: RW
BitField Desc: this is a MemExtraPtr to read the Classify HBCE Looking Up
Information Extra Control in case the number of collisions are more than 4
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Bit_Start                                       0
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Bit_End                                      10
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Mask                                  cBit10_0
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_Shift                                        0
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_MaxVal                                   0x7ff
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_MinVal                                     0x0
#define cAf6_cla_hbce_hash_table_CLAHbceMemoryExtraStartPointer_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Looking Up Information Control
Reg Addr   : 0x00080000 - 0x00081FFF
Reg Formula: 0x00080000 + CollisHashID*0x10000 + HashID
    Where  :
           + $HashID(0-8191): HashID
           + $CollisHashID(0-3): Collision
Reg Desc   :
This memory contain 4 entries (collisions) to examine one Pseudowire label whether match or not.%%
In general, One hashing(HashID) contain 4 entries (collisions). If the collisions are over 4, it will jump to the another extra memory.

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_hbce_lkup_info_Base                                                             0x00080000
#define cAf6Reg_cla_hbce_lkup_info(HashID, CollisHashID)              (0x00080000+Collis(HashID)*0x10000+(HashID))
#define cAf6Reg_cla_hbce_lkup_info_WidthVal                                                                 64
#define cAf6Reg_cla_hbce_lkup_info_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAHbceFlowDirect
BitField Type: RW
BitField Desc: this configure FlowID working in normal mode (not in any group)
BitField Bits: [34]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Bit_Start                                                 34
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Bit_End                                                   34
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Mask                                                   cBit2
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_Shift                                                      2
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_MaxVal                                                   0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_MinVal                                                   0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowDirect_RstVal                                                   0x0

/*--------------------------------------
BitField Name: CLAHbceGrpWorking
BitField Type: RW
BitField Desc: this configure group working or protection
BitField Bits: [33]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Bit_Start                                                 33
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Bit_End                                                   33
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Mask                                                   cBit1
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_Shift                                                      1
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_MaxVal                                                   0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_MinVal                                                   0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpWorking_RstVal                                                   0x0

/*--------------------------------------
BitField Name: CLAHbceGrpIDFlow
BitField Type: RW
BitField Desc: this configure a group ID that FlowID following
BitField Bits: [32:22]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Bit_Start                                                  22
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Bit_End                                                    32
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Mask_01                                             cBit31_22
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Shift_01                                                   22
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Mask_02                                                 cBit0
#define cAf6_cla_hbce_lkup_info_CLAHbceGrpIDFlow_Shift_02                                                    0

/*--------------------------------------
BitField Name: CLAHbceFlowID
BitField Type: RW
BitField Desc: This is PW ID identification
BitField Bits: [21:10]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Bit_Start                                                     10
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Bit_End                                                       21
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Mask                                                   cBit21_10
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_Shift                                                         10
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_MaxVal                                                     0xfff
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_MinVal                                                       0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowID_RstVal                                                       0x0

/*--------------------------------------
BitField Name: CLAHbceFlowEnb
BitField Type: RW
BitField Desc: The flow is identified in this table, it mean the traffic is
identified 1: Flow identified 0: Flow look up fail
BitField Bits: [9]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Bit_Start                                                     9
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Bit_End                                                       9
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Mask                                                      cBit9
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_Shift                                                         9
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_MaxVal                                                      0x1
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_MinVal                                                      0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceFlowEnb_RstVal                                                      0x0

/*--------------------------------------
BitField Name: CLAHbceStoreID
BitField Type: RW
BitField Desc: this is saving some additional information to identify a certain
lookup address rule within a particular table entry HBCE and also to be able to
distinguish those that collide
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Bit_Start                                                     0
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Bit_End                                                       8
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Mask                                                    cBit8_0
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_Shift                                                         0
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_MaxVal                                                    0x1ff
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_MinVal                                                      0x0
#define cAf6_cla_hbce_lkup_info_CLAHbceStoreID_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Group Enable Control
Reg Addr   : 0x00070000 - 0x00070FFF
Reg Formula: 0x00070000 + Working_ID*0x800 + Grp_ID
    Where  :
           + $Working_ID(0-1): CLAHbceGrpWorking or CLAVlanGrpWorking
           + $Grp_ID(0-2047): CLAHbceGrpIDFlow or CLAVlanGrpIDFlow
Reg Desc   :
This register configures Group that Flowid (or Pseudowire ID) is enable or not

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_per_grp_enb_Base                                                                0x00070000
#define cAf6Reg_cla_per_grp_enb(WorkingID, GrpID)                      (0x00070000+(WorkingID)*0x800+(GrpID))
#define cAf6Reg_cla_per_grp_enb_WidthVal                                                                    32
#define cAf6Reg_cla_per_grp_enb_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: CLAGrpPWEn
BitField Type: RW
BitField Desc: This indicate the FlowID (or Pseudowire ID) is enable or not 1:
FlowID enable 0: FlowID disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Bit_Start                                                            0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Bit_End                                                              0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Mask                                                             cBit0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Shift                                                                0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_MaxVal                                                             0x1
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_MinVal                                                             0x0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify Vlan Lookup Control
Reg Addr   : 0x00050000 - 0x00050FFF
Reg Formula: 0x00050000 + VLAN_ID
    Where  :
           + $VLAN_ID(0-4095): VLAN ID
Reg Desc   :
This register configures VLAN Lookup

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_vlan_lk_Base                                                                    0x00050000
#define cAf6Reg_cla_vlan_lk(VLANID)                                                      (0x00050000+(VLANID))
#define cAf6Reg_cla_vlan_lk_WidthVal                                                                        32
#define cAf6Reg_cla_vlan_lk_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: CLAVlanLabelEn
BitField Type: RW
BitField Desc: This configuration to select between VLAN lookup and Label lookup
0: Vlan lookup to flow ID 1: Label lookup to flow ID
BitField Bits: [26]
--------------------------------------*/
#define cAf6_cla_vlan_lk_CLAVlanLabelEn_Bit_Start                                                           26
#define cAf6_cla_vlan_lk_CLAVlanLabelEn_Bit_End                                                             26
#define cAf6_cla_vlan_lk_CLAVlanLabelEn_Mask                                                            cBit26
#define cAf6_cla_vlan_lk_CLAVlanLabelEn_Shift                                                               26
#define cAf6_cla_vlan_lk_CLAVlanLabelEn_MaxVal                                                             0x1
#define cAf6_cla_vlan_lk_CLAVlanLabelEn_MinVal                                                             0x0
#define cAf6_cla_vlan_lk_CLAVlanLabelEn_RstVal                                                             0x0

/*--------------------------------------
BitField Name: CLAVlanEn
BitField Type: RW
BitField Desc: This indicate the VLAN ID enable or not 0: disable 1: enable
BitField Bits: [25]
--------------------------------------*/
#define cAf6_cla_vlan_lk_CLAVlanEn_Bit_Start                                                                25
#define cAf6_cla_vlan_lk_CLAVlanEn_Bit_End                                                                  25
#define cAf6_cla_vlan_lk_CLAVlanEn_Mask                                                                 cBit25
#define cAf6_cla_vlan_lk_CLAVlanEn_Shift                                                                    25
#define cAf6_cla_vlan_lk_CLAVlanEn_MaxVal                                                                  0x1
#define cAf6_cla_vlan_lk_CLAVlanEn_MinVal                                                                  0x0
#define cAf6_cla_vlan_lk_CLAVlanEn_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: CLAVlanFlowDirect
BitField Type: RW
BitField Desc: this configure FlowID working in normal mode (not in any group)
BitField Bits: [24]
--------------------------------------*/
#define cAf6_cla_vlan_lk_CLAVlanFlowDirect_Bit_Start                                                        24
#define cAf6_cla_vlan_lk_CLAVlanFlowDirect_Bit_End                                                          24
#define cAf6_cla_vlan_lk_CLAVlanFlowDirect_Mask                                                         cBit24
#define cAf6_cla_vlan_lk_CLAVlanFlowDirect_Shift                                                            24
#define cAf6_cla_vlan_lk_CLAVlanFlowDirect_MaxVal                                                          0x1
#define cAf6_cla_vlan_lk_CLAVlanFlowDirect_MinVal                                                          0x0
#define cAf6_cla_vlan_lk_CLAVlanFlowDirect_RstVal                                                          0x0

/*--------------------------------------
BitField Name: CLAVlanGrpWorking
BitField Type: RW
BitField Desc: this configure group working or protection
BitField Bits: [23]
--------------------------------------*/
#define cAf6_cla_vlan_lk_CLAVlanGrpWorking_Bit_Start                                                        23
#define cAf6_cla_vlan_lk_CLAVlanGrpWorking_Bit_End                                                          23
#define cAf6_cla_vlan_lk_CLAVlanGrpWorking_Mask                                                         cBit23
#define cAf6_cla_vlan_lk_CLAVlanGrpWorking_Shift                                                            23
#define cAf6_cla_vlan_lk_CLAVlanGrpWorking_MaxVal                                                          0x1
#define cAf6_cla_vlan_lk_CLAVlanGrpWorking_MinVal                                                          0x0
#define cAf6_cla_vlan_lk_CLAVlanGrpWorking_RstVal                                                          0x0

/*--------------------------------------
BitField Name: CLAVlanGrpIDFlow
BitField Type: RW
BitField Desc: this configure a group ID that FlowID following
BitField Bits: [22:12]
--------------------------------------*/
#define cAf6_cla_vlan_lk_CLAVlanGrpIDFlow_Bit_Start                                                         12
#define cAf6_cla_vlan_lk_CLAVlanGrpIDFlow_Bit_End                                                           22
#define cAf6_cla_vlan_lk_CLAVlanGrpIDFlow_Mask                                                       cBit22_12
#define cAf6_cla_vlan_lk_CLAVlanGrpIDFlow_Shift                                                             12
#define cAf6_cla_vlan_lk_CLAVlanGrpIDFlow_MaxVal                                                         0x7ff
#define cAf6_cla_vlan_lk_CLAVlanGrpIDFlow_MinVal                                                           0x0
#define cAf6_cla_vlan_lk_CLAVlanGrpIDFlow_RstVal                                                           0x0

/*--------------------------------------
BitField Name: CLAVlanFlowID
BitField Type: RW
BitField Desc: This indicate the FlowID base on VLAN ID, maximum is 4096 flow
supported
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_cla_vlan_lk_CLAVlanFlowID_Bit_Start                                                             0
#define cAf6_cla_vlan_lk_CLAVlanFlowID_Bit_End                                                              11
#define cAf6_cla_vlan_lk_CLAVlanFlowID_Mask                                                           cBit11_0
#define cAf6_cla_vlan_lk_CLAVlanFlowID_Shift                                                                 0
#define cAf6_cla_vlan_lk_CLAVlanFlowID_MaxVal                                                            0xfff
#define cAf6_cla_vlan_lk_CLAVlanFlowID_MinVal                                                              0x0
#define cAf6_cla_vlan_lk_CLAVlanFlowID_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Ethernet port Enable Control
Reg Addr   : 0x000D0000
Reg Formula:
    Where  :
Reg Desc   :
This register configures specific Ethernet port is enable or not

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_per_eth_enb_Base                                                                0x000D0000
#define cAf6Reg_cla_per_eth_enb                                                                     0x000D0000
#define cAf6Reg_cla_per_eth_enb_WidthVal                                                                    32
#define cAf6Reg_cla_per_eth_enb_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: CLAEthPktMax
BitField Type: RW
BitField Desc: This indicate Ethernet packet maximize
BitField Bits: [28:15]
--------------------------------------*/
#define cAf6_cla_per_eth_enb_CLAEthPktMax_Bit_Start                                                         15
#define cAf6_cla_per_eth_enb_CLAEthPktMax_Bit_End                                                           28
#define cAf6_cla_per_eth_enb_CLAEthPktMax_Mask                                                       cBit28_15
#define cAf6_cla_per_eth_enb_CLAEthPktMax_Shift                                                             15
#define cAf6_cla_per_eth_enb_CLAEthPktMax_MaxVal                                                        0x3fff
#define cAf6_cla_per_eth_enb_CLAEthPktMax_MinVal                                                           0x0
#define cAf6_cla_per_eth_enb_CLAEthPktMax_RstVal                                                           0x0

/*--------------------------------------
BitField Name: CLAEthPktMin
BitField Type: RW
BitField Desc: This indicate Ethernet packet minimize
BitField Bits: [14:1]
--------------------------------------*/
#define cAf6_cla_per_eth_enb_CLAEthPktMin_Bit_Start                                                          1
#define cAf6_cla_per_eth_enb_CLAEthPktMin_Bit_End                                                           14
#define cAf6_cla_per_eth_enb_CLAEthPktMin_Mask                                                        cBit14_1
#define cAf6_cla_per_eth_enb_CLAEthPktMin_Shift                                                              1
#define cAf6_cla_per_eth_enb_CLAEthPktMin_MaxVal                                                        0x3fff
#define cAf6_cla_per_eth_enb_CLAEthPktMin_MinVal                                                           0x0
#define cAf6_cla_per_eth_enb_CLAEthPktMin_RstVal                                                           0x0


/*--------------------------------------
BitField Name: CLAEthPort1En
BitField Type: RW
BitField Desc: This indicate Ethernet port 1 is enable or not 1: enable 0:
disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cla_per_eth_enb_CLAEthPort1En_Bit_Start                                                         0
#define cAf6_cla_per_eth_enb_CLAEthPort1En_Bit_End                                                           0
#define cAf6_cla_per_eth_enb_CLAEthPort1En_Mask                                                          cBit0
#define cAf6_cla_per_eth_enb_CLAEthPort1En_Shift                                                             0
#define cAf6_cla_per_eth_enb_CLAEthPort1En_MaxVal                                                          0x1
#define cAf6_cla_per_eth_enb_CLAEthPort1En_MinVal                                                          0x0
#define cAf6_cla_per_eth_enb_CLAEthPort1En_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count0_64 bytes packet
Reg Addr   : 0x000D1000(RO)
Reg Formula: 0x000D1000 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt0_64_ro_Base                                                                 0x000D1000
#define cAf6Reg_Eth_cnt0_64_ro(ethport)                                                 (0x000D1000+(ethport))
#define cAf6Reg_Eth_cnt0_64_ro_WidthVal                                                                     32
#define cAf6Reg_Eth_cnt0_64_ro_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: CLAEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Bit_Start                                                          0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Bit_End                                                           31
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Mask                                                        cBit31_0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Shift                                                              0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_MaxVal                                                    0xffffffff
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_MinVal                                                           0x0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count0_64 bytes packet
Reg Addr   : 0x000D1800(RC)
Reg Formula: 0x000D1800 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt0_64_rc_Base                                                                 0x000D1800
#define cAf6Reg_Eth_cnt0_64_rc(ethport)                                                 (0x000D1800+(ethport))
#define cAf6Reg_Eth_cnt0_64_rc_WidthVal                                                                     32
#define cAf6Reg_Eth_cnt0_64_rc_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: CLAEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Bit_Start                                                          0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Bit_End                                                           31
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Mask                                                        cBit31_0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Shift                                                              0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_MaxVal                                                    0xffffffff
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_MinVal                                                           0x0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count65_127 bytes packet
Reg Addr   : 0x000D1004(RO)
Reg Formula: 0x000D1004 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt65_127_ro_Base                                                               0x000D1004
#define cAf6Reg_Eth_cnt65_127_ro(ethport)                                               (0x000D1004+(ethport))
#define cAf6Reg_Eth_cnt65_127_ro_WidthVal                                                                   32
#define cAf6Reg_Eth_cnt65_127_ro_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Bit_Start                                                      0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Bit_End                                                       31
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Mask                                                    cBit31_0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Shift                                                          0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_MaxVal                                                0xffffffff
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_MinVal                                                       0x0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count65_127 bytes packet
Reg Addr   : 0x000D1804(RC)
Reg Formula: 0x000D1804 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt65_127_rc_Base                                                               0x000D1804
#define cAf6Reg_Eth_cnt65_127_rc(ethport)                                               (0x000D1804+(ethport))
#define cAf6Reg_Eth_cnt65_127_rc_WidthVal                                                                   32
#define cAf6Reg_Eth_cnt65_127_rc_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Bit_Start                                                      0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Bit_End                                                       31
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Mask                                                    cBit31_0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Shift                                                          0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_MaxVal                                                0xffffffff
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_MinVal                                                       0x0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count128_255 bytes packet
Reg Addr   : 0x000D1008(RO)
Reg Formula: 0x000D1008 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt128_255_ro_Base                                                              0x000D1008
#define cAf6Reg_Eth_cnt128_255_ro(ethport)                                              (0x000D1008+(ethport))
#define cAf6Reg_Eth_cnt128_255_ro_WidthVal                                                                  32
#define cAf6Reg_Eth_cnt128_255_ro_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Bit_Start                                                    0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Bit_End                                                     31
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Mask                                                  cBit31_0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Shift                                                        0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_MinVal                                                     0x0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count128_255 bytes packet
Reg Addr   : 0x000D1808(RC)
Reg Formula: 0x000D1808 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt128_255_rc_Base                                                              0x000D1808
#define cAf6Reg_Eth_cnt128_255_rc(ethport)                                              (0x000D1808+(ethport))
#define cAf6Reg_Eth_cnt128_255_rc_WidthVal                                                                  32
#define cAf6Reg_Eth_cnt128_255_rc_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Bit_Start                                                    0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Bit_End                                                     31
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Mask                                                  cBit31_0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Shift                                                        0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_MinVal                                                     0x0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count256_511 bytes packet
Reg Addr   : 0x000D100C(RO)
Reg Formula: 0x000D100C + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt256_511_ro_Base                                                              0x000D100C
#define cAf6Reg_Eth_cnt256_511_ro(ethport)                                              (0x000D100C+(ethport))
#define cAf6Reg_Eth_cnt256_511_ro_WidthVal                                                                  32
#define cAf6Reg_Eth_cnt256_511_ro_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Bit_Start                                                    0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Bit_End                                                     31
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Mask                                                  cBit31_0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Shift                                                        0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_MinVal                                                     0x0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count256_511 bytes packet
Reg Addr   : 0x000D180C(RC)
Reg Formula: 0x000D180C + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt256_511_rc_Base                                                              0x000D180C
#define cAf6Reg_Eth_cnt256_511_rc(ethport)                                              (0x000D180C+(ethport))
#define cAf6Reg_Eth_cnt256_511_rc_WidthVal                                                                  32
#define cAf6Reg_Eth_cnt256_511_rc_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CLAEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Bit_Start                                                    0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Bit_End                                                     31
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Mask                                                  cBit31_0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Shift                                                        0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_MinVal                                                     0x0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count512_1024 bytes packet
Reg Addr   : 0x000D1010(RO)
Reg Formula: 0x000D1010 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt512_1024_ro_Base                                                             0x000D1010
#define cAf6Reg_Eth_cnt512_1024_ro(ethport)                                             (0x000D1010+(ethport))
#define cAf6Reg_Eth_cnt512_1024_ro_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt512_1024_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCnt512_1024
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt512_1024_ro_CLAEthCnt512_1024_Bit_Start                                                  0
#define cAf6_Eth_cnt512_1024_ro_CLAEthCnt512_1024_Bit_End                                                   31
#define cAf6_Eth_cnt512_1024_ro_CLAEthCnt512_1024_Mask                                                cBit31_0
#define cAf6_Eth_cnt512_1024_ro_CLAEthCnt512_1024_Shift                                                      0
#define cAf6_Eth_cnt512_1024_ro_CLAEthCnt512_1024_MaxVal                                            0xffffffff
#define cAf6_Eth_cnt512_1024_ro_CLAEthCnt512_1024_MinVal                                                   0x0
#define cAf6_Eth_cnt512_1024_ro_CLAEthCnt512_1024_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count512_1024 bytes packet
Reg Addr   : 0x000D1810(RC)
Reg Formula: 0x000D1810 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt512_1024_rc_Base                                                             0x000D1810
#define cAf6Reg_Eth_cnt512_1024_rc(ethport)                                             (0x000D1810+(ethport))
#define cAf6Reg_Eth_cnt512_1024_rc_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt512_1024_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCnt512_1024
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt512_1024_rc_CLAEthCnt512_1024_Bit_Start                                                  0
#define cAf6_Eth_cnt512_1024_rc_CLAEthCnt512_1024_Bit_End                                                   31
#define cAf6_Eth_cnt512_1024_rc_CLAEthCnt512_1024_Mask                                                cBit31_0
#define cAf6_Eth_cnt512_1024_rc_CLAEthCnt512_1024_Shift                                                      0
#define cAf6_Eth_cnt512_1024_rc_CLAEthCnt512_1024_MaxVal                                            0xffffffff
#define cAf6_Eth_cnt512_1024_rc_CLAEthCnt512_1024_MinVal                                                   0x0
#define cAf6_Eth_cnt512_1024_rc_CLAEthCnt512_1024_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count1025_1528 bytes packet
Reg Addr   : 0x000D1014(RO)
Reg Formula: 0x000D1014 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1025_1528_ro_Base                                                            0x000D1014
#define cAf6Reg_Eth_cnt1025_1528_ro(ethport)                                            (0x000D1014+(ethport))
#define cAf6Reg_Eth_cnt1025_1528_ro_WidthVal                                                                32
#define cAf6Reg_Eth_cnt1025_1528_ro_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1025_1528
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1025_1528_ro_CLAEthCnt1025_1528_Bit_Start                                                0
#define cAf6_Eth_cnt1025_1528_ro_CLAEthCnt1025_1528_Bit_End                                                 31
#define cAf6_Eth_cnt1025_1528_ro_CLAEthCnt1025_1528_Mask                                              cBit31_0
#define cAf6_Eth_cnt1025_1528_ro_CLAEthCnt1025_1528_Shift                                                    0
#define cAf6_Eth_cnt1025_1528_ro_CLAEthCnt1025_1528_MaxVal                                          0xffffffff
#define cAf6_Eth_cnt1025_1528_ro_CLAEthCnt1025_1528_MinVal                                                 0x0
#define cAf6_Eth_cnt1025_1528_ro_CLAEthCnt1025_1528_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count1025_1528 bytes packet
Reg Addr   : 0x000D1814(RC)
Reg Formula: 0x000D1814 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1025_1528_rc_Base                                                            0x000D1814
#define cAf6Reg_Eth_cnt1025_1528_rc(ethport)                                            (0x000D1814+(ethport))
#define cAf6Reg_Eth_cnt1025_1528_rc_WidthVal                                                                32
#define cAf6Reg_Eth_cnt1025_1528_rc_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1025_1528
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1025_1528_rc_CLAEthCnt1025_1528_Bit_Start                                                0
#define cAf6_Eth_cnt1025_1528_rc_CLAEthCnt1025_1528_Bit_End                                                 31
#define cAf6_Eth_cnt1025_1528_rc_CLAEthCnt1025_1528_Mask                                              cBit31_0
#define cAf6_Eth_cnt1025_1528_rc_CLAEthCnt1025_1528_Shift                                                    0
#define cAf6_Eth_cnt1025_1528_rc_CLAEthCnt1025_1528_MaxVal                                          0xffffffff
#define cAf6_Eth_cnt1025_1528_rc_CLAEthCnt1025_1528_MinVal                                                 0x0
#define cAf6_Eth_cnt1025_1528_rc_CLAEthCnt1025_1528_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count1529_2047 bytes packet
Reg Addr   : 0x000D1018(RO)
Reg Formula: 0x000D1018 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1529_2047_ro_Base                                                            0x000D1018
#define cAf6Reg_Eth_cnt1529_2047_ro(ethport)                                            (0x000D1018+(ethport))
#define cAf6Reg_Eth_cnt1529_2047_ro_WidthVal                                                                32
#define cAf6Reg_Eth_cnt1529_2047_ro_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1529_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1529_2047_ro_CLAEthCnt1529_2047_Bit_Start                                                0
#define cAf6_Eth_cnt1529_2047_ro_CLAEthCnt1529_2047_Bit_End                                                 31
#define cAf6_Eth_cnt1529_2047_ro_CLAEthCnt1529_2047_Mask                                              cBit31_0
#define cAf6_Eth_cnt1529_2047_ro_CLAEthCnt1529_2047_Shift                                                    0
#define cAf6_Eth_cnt1529_2047_ro_CLAEthCnt1529_2047_MaxVal                                          0xffffffff
#define cAf6_Eth_cnt1529_2047_ro_CLAEthCnt1529_2047_MinVal                                                 0x0
#define cAf6_Eth_cnt1529_2047_ro_CLAEthCnt1529_2047_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count1529_2047 bytes packet
Reg Addr   : 0x000D1818(RC)
Reg Formula: 0x000D1818 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1529_2047_rc_Base                                                            0x000D1818
#define cAf6Reg_Eth_cnt1529_2047_rc(ethport)                                            (0x000D1818+(ethport))
#define cAf6Reg_Eth_cnt1529_2047_rc_WidthVal                                                                32
#define cAf6Reg_Eth_cnt1529_2047_rc_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: CLAEthCnt1529_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1529_2047_rc_CLAEthCnt1529_2047_Bit_Start                                                0
#define cAf6_Eth_cnt1529_2047_rc_CLAEthCnt1529_2047_Bit_End                                                 31
#define cAf6_Eth_cnt1529_2047_rc_CLAEthCnt1529_2047_Mask                                              cBit31_0
#define cAf6_Eth_cnt1529_2047_rc_CLAEthCnt1529_2047_Shift                                                    0
#define cAf6_Eth_cnt1529_2047_rc_CLAEthCnt1529_2047_MaxVal                                          0xffffffff
#define cAf6_Eth_cnt1529_2047_rc_CLAEthCnt1529_2047_MinVal                                                 0x0
#define cAf6_Eth_cnt1529_2047_rc_CLAEthCnt1529_2047_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Jumbo packet
Reg Addr   : 0x000D101C(RO)
Reg Formula: 0x000D101C + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_jumbo_ro_Base                                                               0x000D101C
#define cAf6Reg_Eth_cnt_jumbo_ro(ethport)                                               (0x000D101C+(ethport))
#define cAf6Reg_Eth_cnt_jumbo_ro_WidthVal                                                                   32
#define cAf6Reg_Eth_cnt_jumbo_ro_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Bit_Start                                                       0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Bit_End                                                        31
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Mask                                                     cBit31_0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Shift                                                           0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_MaxVal                                                 0xffffffff
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_MinVal                                                        0x0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Jumbo packet
Reg Addr   : 0x000D181C(RC)
Reg Formula: 0x000D181C + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_jumbo_rc_Base                                                               0x000D181C
#define cAf6Reg_Eth_cnt_jumbo_rc(ethport)                                               (0x000D181C+(ethport))
#define cAf6Reg_Eth_cnt_jumbo_rc_WidthVal                                                                   32
#define cAf6Reg_Eth_cnt_jumbo_rc_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: CLAEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Bit_Start                                                       0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Bit_End                                                        31
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Mask                                                     cBit31_0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Shift                                                           0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_MaxVal                                                 0xffffffff
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_MinVal                                                        0x0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Unicast packet
Reg Addr   : 0x000D1020(RO)
Reg Formula: 0x000D1020 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the unicast packet

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_Unicast_ro_Base                                                             0x000D1020
#define cAf6Reg_Eth_cnt_Unicast_ro(ethport)                                             (0x000D1020+(ethport))
#define cAf6Reg_Eth_cnt_Unicast_ro_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt_Unicast_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntUnicast
BitField Type: RO
BitField Desc: This is statistic counter for the unicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Bit_Start                                                   0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Bit_End                                                    31
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Mask                                                 cBit31_0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Shift                                                       0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_MaxVal                                             0xffffffff
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_MinVal                                                    0x0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Unicast packet
Reg Addr   : 0x000D1820(RC)
Reg Formula: 0x000D1820 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the unicast packet

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_Unicast_rc_Base                                                             0x000D1820
#define cAf6Reg_Eth_cnt_Unicast_rc(ethport)                                             (0x000D1820+(ethport))
#define cAf6Reg_Eth_cnt_Unicast_rc_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt_Unicast_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntUnicast
BitField Type: RO
BitField Desc: This is statistic counter for the unicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Bit_Start                                                   0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Bit_End                                                    31
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Mask                                                 cBit31_0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Shift                                                       0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_MaxVal                                             0xffffffff
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_MinVal                                                    0x0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Total packet
Reg Addr   : 0x000D1024(RO)
Reg Formula: 0x000D1024 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the total packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_pkt_cnt_ro_Base                                                             0x000D1024
#define cAf6Reg_rx_port_pkt_cnt_ro(ethport)                                             (0x000D1024+(ethport))
#define cAf6Reg_rx_port_pkt_cnt_ro_WidthVal                                                                 32
#define cAf6Reg_rx_port_pkt_cnt_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_Bit_Start                                                     0
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_Bit_End                                                      31
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_Mask                                                   cBit31_0
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_Shift                                                         0
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_MaxVal                                               0xffffffff
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_MinVal                                                      0x0
#define cAf6_rx_port_pkt_cnt_ro_CLAEthCntTotal_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Total packet
Reg Addr   : 0x000D1824(RC)
Reg Formula: 0x000D1824 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the total packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_pkt_cnt_rc_Base                                                             0x000D1824
#define cAf6Reg_rx_port_pkt_cnt_rc(ethport)                                             (0x000D1824+(ethport))
#define cAf6Reg_rx_port_pkt_cnt_rc_WidthVal                                                                 32
#define cAf6Reg_rx_port_pkt_cnt_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_Bit_Start                                                     0
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_Bit_End                                                      31
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_Mask                                                   cBit31_0
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_Shift                                                         0
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_MaxVal                                               0xffffffff
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_MinVal                                                      0x0
#define cAf6_rx_port_pkt_cnt_rc_CLAEthCntTotal_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Broadcast packet
Reg Addr   : 0x000D1028(RO)
Reg Formula: 0x000D1028 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the Broadcast packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_bcast_pkt_cnt_ro_Base                                                       0x000D1028
#define cAf6Reg_rx_port_bcast_pkt_cnt_ro(ethport)                                       (0x000D1028+(ethport))
#define cAf6Reg_rx_port_bcast_pkt_cnt_ro_WidthVal                                                           32
#define cAf6Reg_rx_port_bcast_pkt_cnt_ro_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CLAEthCntBroadcast
BitField Type: RO
BitField Desc: This is statistic counter broadcast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Bit_Start                                           0
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Bit_End                                            31
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Mask                                         cBit31_0
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Shift                                               0
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_MaxVal                                     0xffffffff
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_MinVal                                            0x0
#define cAf6_rx_port_bcast_pkt_cnt_ro_CLAEthCntBroadcast_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Broadcast packet
Reg Addr   : 0x000D1828(RC)
Reg Formula: 0x000D1828 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the Broadcast packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_bcast_pkt_cnt_rc_Base                                                       0x000D1828
#define cAf6Reg_rx_port_bcast_pkt_cnt_rc(ethport)                                       (0x000D1828+(ethport))
#define cAf6Reg_rx_port_bcast_pkt_cnt_rc_WidthVal                                                           32
#define cAf6Reg_rx_port_bcast_pkt_cnt_rc_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CLAEthCntBroadcast
BitField Type: RO
BitField Desc: This is statistic counter broadcast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Bit_Start                                           0
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Bit_End                                            31
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Mask                                         cBit31_0
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Shift                                               0
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_MaxVal                                     0xffffffff
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_MinVal                                            0x0
#define cAf6_rx_port_bcast_pkt_cnt_rc_CLAEthCntBroadcast_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Multicast packet
Reg Addr   : 0x000D102C(RO)
Reg Formula: 0x000D102C + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the Multicast packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_mcast_pkt_cnt_ro_Base                                                       0x000D102C
#define cAf6Reg_rx_port_mcast_pkt_cnt_ro(ethport)                                       (0x000D102C+(ethport))
#define cAf6Reg_rx_port_mcast_pkt_cnt_ro_WidthVal                                                           32
#define cAf6Reg_rx_port_mcast_pkt_cnt_ro_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CLAEthCntMulticast
BitField Type: RO
BitField Desc: This is statistic counter multicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_Bit_Start                                           0
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_Bit_End                                            31
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_Mask                                         cBit31_0
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_Shift                                               0
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_MaxVal                                     0xffffffff
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_MinVal                                            0x0
#define cAf6_rx_port_mcast_pkt_cnt_ro_CLAEthCntMulticast_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Multicast packet
Reg Addr   : 0x000D182C(RC)
Reg Formula: 0x000D182C + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the Multicast packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_mcast_pkt_cnt_rc_Base                                                       0x000D182C
#define cAf6Reg_rx_port_mcast_pkt_cnt_rc(ethport)                                       (0x000D182C+(ethport))
#define cAf6Reg_rx_port_mcast_pkt_cnt_rc_WidthVal                                                           32
#define cAf6Reg_rx_port_mcast_pkt_cnt_rc_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CLAEthCntMulticast
BitField Type: RO
BitField Desc: This is statistic counter multicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_Bit_Start                                           0
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_Bit_End                                            31
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_Mask                                         cBit31_0
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_Shift                                               0
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_MaxVal                                     0xffffffff
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_MinVal                                            0x0
#define cAf6_rx_port_mcast_pkt_cnt_rc_CLAEthCntMulticast_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Under size packet
Reg Addr   : 0x000D1030(RO)
Reg Formula: 0x000D1030 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the under size packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_under_size_pkt_cnt_ro_Base                                                  0x000D1030
#define cAf6Reg_rx_port_under_size_pkt_cnt_ro(ethport)                                  (0x000D1030+(ethport))
#define cAf6Reg_rx_port_under_size_pkt_cnt_ro_WidthVal                                                      32
#define cAf6Reg_rx_port_under_size_pkt_cnt_ro_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: CLAEthCntUnderSize
BitField Type: RO
BitField Desc: This is statistic counter under size packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_Bit_Start                                       0
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_Bit_End                                       31
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_Mask                                    cBit31_0
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_Shift                                          0
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_MaxVal                                0xffffffff
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_MinVal                                       0x0
#define cAf6_rx_port_under_size_pkt_cnt_ro_CLAEthCntUnderSize_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Under size packet
Reg Addr   : 0x000D1830(RC)
Reg Formula: 0x000D1830 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the under size packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_under_size_pkt_cnt_rc_Base                                                  0x000D1830
#define cAf6Reg_rx_port_under_size_pkt_cnt_rc(ethport)                                  (0x000D1830+(ethport))
#define cAf6Reg_rx_port_under_size_pkt_cnt_rc_WidthVal                                                      32
#define cAf6Reg_rx_port_under_size_pkt_cnt_rc_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: CLAEthCntUnderSize
BitField Type: RO
BitField Desc: This is statistic counter under size packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_Bit_Start                                       0
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_Bit_End                                       31
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_Mask                                    cBit31_0
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_Shift                                          0
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_MaxVal                                0xffffffff
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_MinVal                                       0x0
#define cAf6_rx_port_under_size_pkt_cnt_rc_CLAEthCntUnderSize_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Over size packet
Reg Addr   : 0x000D1034(RO)
Reg Formula: 0x000D1034 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the over size packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_over_size_pkt_cnt_ro_Base                                                   0x000D1034
#define cAf6Reg_rx_port_over_size_pkt_cnt_ro(ethport)                                   (0x000D1034+(ethport))
#define cAf6Reg_rx_port_over_size_pkt_cnt_ro_WidthVal                                                       32
#define cAf6Reg_rx_port_over_size_pkt_cnt_ro_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: CLAEthCntOverSize
BitField Type: RO
BitField Desc: This is statistic counter over size packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_Bit_Start                                        0
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_Bit_End                                         31
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_Mask                                      cBit31_0
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_Shift                                            0
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_MaxVal                                  0xffffffff
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_MinVal                                         0x0
#define cAf6_rx_port_over_size_pkt_cnt_ro_CLAEthCntOverSize_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count Over size packet
Reg Addr   : 0x000D1834(RC)
Reg Formula: 0x000D1834 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the over size packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_over_size_pkt_cnt_rc_Base                                                   0x000D1834
#define cAf6Reg_rx_port_over_size_pkt_cnt_rc(ethport)                                   (0x000D1834+(ethport))
#define cAf6Reg_rx_port_over_size_pkt_cnt_rc_WidthVal                                                       32
#define cAf6Reg_rx_port_over_size_pkt_cnt_rc_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: CLAEthCntOverSize
BitField Type: RO
BitField Desc: This is statistic counter over size packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_Bit_Start                                        0
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_Bit_End                                         31
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_Mask                                      cBit31_0
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_Shift                                            0
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_MaxVal                                  0xffffffff
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_MinVal                                         0x0
#define cAf6_rx_port_over_size_pkt_cnt_rc_CLAEthCntOverSize_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count FCS error packet
Reg Addr   : 0x000D1038(RO)
Reg Formula: 0x000D1038 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic count FCS error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_phy_err_ro_Base                                                             0x00000038
#define cAf6Reg_Eth_cnt_phy_err_ro(ethport)                                             (0x000D1038+(ethport))
#define cAf6Reg_Eth_cnt_phy_err_ro_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt_phy_err_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntPhyErr
BitField Type: RO
BitField Desc: This is statistic counter FCS error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_Bit_Start                                                    0
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_Bit_End                                                     31
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_Mask                                                  cBit31_0
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_Shift                                                        0
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_MinVal                                                     0x0
#define cAf6_Eth_cnt_phy_err_ro_CLAEthCntPhyErr_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count FCS error packet
Reg Addr   : 0x000D1838(RC)
Reg Formula: 0x000D1838 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic count FCS error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_phy_err_rc_Base                                                             0x000D1838
#define cAf6Reg_Eth_cnt_phy_err_rc(ethport)                                             (0x000D1838+(ethport))
#define cAf6Reg_Eth_cnt_phy_err_rc_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt_phy_err_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntPhyErr
BitField Type: RO
BitField Desc: This is statistic counter FCS error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_Bit_Start                                                    0
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_Bit_End                                                     31
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_Mask                                                  cBit31_0
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_Shift                                                        0
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_MinVal                                                     0x0
#define cAf6_Eth_cnt_phy_err_rc_CLAEthCntPhyErr_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count number of bytes of packet
Reg Addr   : 0x000D103C(RO)
Reg Formula: 0x000D103C + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic count number of bytes of packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_byte_ro_Base                                                                0x000D103C
#define cAf6Reg_Eth_cnt_byte_ro(ethport)                                                (0x000D103C+(ethport))
#define cAf6Reg_Eth_cnt_byte_ro_WidthVal                                                                    32
#define cAf6Reg_Eth_cnt_byte_ro_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: CLAEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Bit_Start                                                         0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Bit_End                                                          31
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Mask                                                       cBit31_0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Shift                                                             0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_MaxVal                                                   0xffffffff
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_MinVal                                                          0x0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count number of bytes of packet
Reg Addr   : 0x000D183C(RC)
Reg Formula: 0x000D183C + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic count number of bytes of packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_byte_rc_Base                                                                0x000D183C
#define cAf6Reg_Eth_cnt_byte_rc(ethport)                                                (0x000D183C+(ethport))
#define cAf6Reg_Eth_cnt_byte_rc_WidthVal                                                                    32
#define cAf6Reg_Eth_cnt_byte_rc_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: CLAEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Bit_Start                                                         0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Bit_End                                                          31
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Mask                                                       cBit31_0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Shift                                                             0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_MaxVal                                                   0xffffffff
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_MinVal                                                          0x0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify HBCE Looking Up Information Control2
Reg Addr   : 0x0040000 - 0x00477FF
Reg Formula: 0x0040000 + CollisHashIDExtra*0x1000  + MemExtraPtr
    Where  :
           + $MemExtraPtr(0-2047): MemExtraPtr
           + $CollisHashIDExtra (0-7): Collision
Reg Desc   :
This memory contain maximum 8 entries extra to examine one Pseudowire label whether match or not

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_hbce_lkup_info_extra_Base                                                        0x0040000
#define cAf6Reg_cla_hbce_lkup_info_extra(MemExtraPtr, CollisHashIDExtra)(0x0040000+(CollisHashIDExtra)*0x1000+(MemExtraPtr))
#define cAf6Reg_cla_hbce_lkup_info_extra_WidthVal                                                           64
#define cAf6Reg_cla_hbce_lkup_info_extra_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: CLAExtraHbceFlowDirect
BitField Type: RW
BitField Desc: this configure FlowID working in normal mode (not in any group)
BitField Bits: [34]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_Bit_Start                                      34
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_Bit_End                                        34
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_Mask                                        cBit2
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_Shift                                           2
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_MaxVal                                        0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_MinVal                                        0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowDirect_RstVal                                        0x0

/*--------------------------------------
BitField Name: CLAExtraHbceGrpWorking
BitField Type: RW
BitField Desc: this configure group working or protection
BitField Bits: [33]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_Bit_Start                                      33
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_Bit_End                                        33
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_Mask                                        cBit1
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_Shift                                           1
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_MaxVal                                        0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_MinVal                                        0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpWorking_RstVal                                        0x0

/*--------------------------------------
BitField Name: CLAExtraHbceGrpIDFlow
BitField Type: RW
BitField Desc: this configure a group ID that FlowID following
BitField Bits: [32:22]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_Bit_Start                                       22
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_Bit_End                                         32
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_Mask_01                                  cBit31_22
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_Shift_01                                        22
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_Mask_02                                      cBit0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceGrpIDFlow_Shift_02                                         0

/*--------------------------------------
BitField Name: CLAExtraHbceFlowID
BitField Type: RW
BitField Desc: This is PW ID identification
BitField Bits: [21:10]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_Bit_Start                                          10
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_Bit_End                                            21
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_Mask                                        cBit21_10
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_Shift                                              10
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_MaxVal                                          0xfff
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_MinVal                                            0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowID_RstVal                                            0x0

/*--------------------------------------
BitField Name: CLAExtraHbceFlowEnb
BitField Type: RW
BitField Desc: The flow is identified in this table, it mean the traffic is
identified 1: Flow identified 0: Flow look up fail
BitField Bits: [9]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_Bit_Start                                          9
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_Bit_End                                            9
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_Mask                                           cBit9
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_Shift                                              9
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_MaxVal                                           0x1
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_MinVal                                           0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceFlowEnb_RstVal                                           0x0

/*--------------------------------------
BitField Name: CLAExtraHbceStoreID
BitField Type: RW
BitField Desc: this is saving some additional information to identify a certain
lookup address rule within a particular table entry HBCE and also to be able to
distinguish those that collide
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_Bit_Start                                          0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_Bit_End                                            8
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_Mask                                         cBit8_0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_Shift                                              0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_MaxVal                                         0x1ff
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_MinVal                                           0x0
#define cAf6_cla_hbce_lkup_info_extra_CLAExtraHbceStoreID_RstVal                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Pseudowire Slice to CDR Control
Reg Addr   : 0x00C0000 - 0x00C0FFF
Reg Formula: 0x00C0000 +  PWID
    Where  :
           + $PWID(0-3071): Pseudowire ID
Reg Desc   :
This register configures Slice ID to CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_2_cdr_cfg_Base                                                                   0x00C0000
#define cAf6Reg_cla_2_cdr_cfg(PWID)                                                         (0x00C0000+(PWID))
#define cAf6Reg_cla_2_cdr_cfg_WidthVal                                                                      32
#define cAf6Reg_cla_2_cdr_cfg_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: PwCdrEn
BitField Type: RW
BitField Desc: Indicate Pseudowire enable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_cla_2_cdr_cfg_PwCdrEn_Bit_Start                                                                15
#define cAf6_cla_2_cdr_cfg_PwCdrEn_Bit_End                                                                  15
#define cAf6_cla_2_cdr_cfg_PwCdrEn_Mask                                                                 cBit15
#define cAf6_cla_2_cdr_cfg_PwCdrEn_Shift                                                                    15
#define cAf6_cla_2_cdr_cfg_PwCdrEn_MaxVal                                                                  0x1
#define cAf6_cla_2_cdr_cfg_PwCdrEn_MinVal                                                                  0x0
#define cAf6_cla_2_cdr_cfg_PwCdrEn_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: PwHoEn
BitField Type: RW
BitField Desc: Indicate 8x Hi order OC48 enable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_cla_2_cdr_cfg_PwHoEn_Bit_Start                                                                 14
#define cAf6_cla_2_cdr_cfg_PwHoEn_Bit_End                                                                   14
#define cAf6_cla_2_cdr_cfg_PwHoEn_Mask                                                                  cBit14
#define cAf6_cla_2_cdr_cfg_PwHoEn_Shift                                                                     14
#define cAf6_cla_2_cdr_cfg_PwHoEn_MaxVal                                                                   0x1
#define cAf6_cla_2_cdr_cfg_PwHoEn_MinVal                                                                   0x0
#define cAf6_cla_2_cdr_cfg_PwHoEn_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: PwHoLoOc48Id
BitField Type: RW
BitField Desc: Indicate 8x Hi order OC48 or 6x Low order OC48 slice
BitField Bits: [13:11]
--------------------------------------*/
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_Bit_Start                                                           11
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_Bit_End                                                             13
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_Mask                                                         cBit13_11
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_Shift                                                               11
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_MaxVal                                                             0x7
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_MinVal                                                             0x0
#define cAf6_cla_2_cdr_cfg_PwHoLoOc48Id_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PwLoSlice24Sel
BitField Type: RW
BitField Desc: Pseudo-wire Low order OC24 slice selection, only valid in Lo
Pseudo-wire 1: This PW belong to Slice24 that trasnport STS 1,3,5,...,47 within
a Lo OC48 0: This PW belong to Slice24 that trasnport STS 0,2,4,...,46 within a
Lo OC48
BitField Bits: [10]
--------------------------------------*/
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_Bit_Start                                                         10
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_Bit_End                                                           10
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_Mask                                                          cBit10
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_Shift                                                             10
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_MaxVal                                                           0x1
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_MinVal                                                           0x0
#define cAf6_cla_2_cdr_cfg_PwLoSlice24Sel_RstVal                                                           0x0

/*--------------------------------------
BitField Name: PwTdmLineId
BitField Type: RW
BitField Desc: Pseudo-wire(PW) corresponding TDM line ID. If the PW belong to
Low order path, this is the OC24 TDM line ID that is using in Lo CDR,PDH and
MAP. If the PW belong to Hi order CEP path, this is the OC48 master STS ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_Bit_Start                                                             0
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_Bit_End                                                               9
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_Mask                                                            cBit9_0
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_Shift                                                                 0
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_MaxVal                                                            0x3ff
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_MinVal                                                              0x0
#define cAf6_cla_2_cdr_cfg_PwTdmLineId_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify Hold Register Status
Reg Addr   : 0x00000A - 0x00000C
Reg Formula: 0x00000A +  HID
    Where  :
           + $HID(0-2): Hold ID
Reg Desc   :
This register using for hold remain that more than 128bits

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_hold_status_Base                                                                  0x00000A
#define cAf6Reg_cla_hold_status(HID)                                                          (0x00000A+(HID))
#define cAf6Reg_cla_hold_status_WidthVal                                                                    32
#define cAf6Reg_cla_hold_status_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: PwHoldStatus
BitField Type: RW
BitField Desc: Hold 32bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cla_hold_status_PwHoldStatus_Bit_Start                                                          0
#define cAf6_cla_hold_status_PwHoldStatus_Bit_End                                                           31
#define cAf6_cla_hold_status_PwHoldStatus_Mask                                                        cBit31_0
#define cAf6_cla_hold_status_PwHoldStatus_Shift                                                              0
#define cAf6_cla_hold_status_PwHoldStatus_MaxVal                                                    0xffffffff
#define cAf6_cla_hold_status_PwHoldStatus_MinVal                                                           0x0
#define cAf6_cla_hold_status_PwHoldStatus_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit3_0 Control
Reg Addr   : 0xE0000 - 0xE000F
Reg Formula: 0xE0000 + HaAddr3_0
    Where  :
           + $HaAddr3_0(0-15): HA Address Bit3_0
Reg Desc   :
This register is used to send HA read address bit3_0 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha3_0_control_Base                                                                   0xE0000
#define cAf6Reg_rdha3_0_control(HaAddr30)                                                 (0xE0000+(HaAddr30))
#define cAf6Reg_rdha3_0_control_WidthVal                                                                    32
#define cAf6Reg_rdha3_0_control_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: ReadAddr3_0
BitField Type: RW
BitField Desc: Read value will be 0xE00000 plus HaAddr3_0
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha3_0_control_ReadAddr3_0_Bit_Start                                                           0
#define cAf6_rdha3_0_control_ReadAddr3_0_Bit_End                                                            19
#define cAf6_rdha3_0_control_ReadAddr3_0_Mask                                                         cBit19_0
#define cAf6_rdha3_0_control_ReadAddr3_0_Shift                                                               0
#define cAf6_rdha3_0_control_ReadAddr3_0_MaxVal                                                        0xfffff
#define cAf6_rdha3_0_control_ReadAddr3_0_MinVal                                                            0x0
#define cAf6_rdha3_0_control_ReadAddr3_0_RstVal                                                            0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit7_4 Control
Reg Addr   : 0xE0010 - 0xE001F
Reg Formula: 0xE0010 + HaAddr7_4
    Where  :
           + $HaAddr7_4(0-15): HA Address Bit7_4
Reg Desc   :
This register is used to send HA read address bit7_4 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha7_4_control_Base                                                                   0xE0010
#define cAf6Reg_rdha7_4_control(HaAddr74)                                                 (0xE0010+(HaAddr74))
#define cAf6Reg_rdha7_4_control_WidthVal                                                                    32
#define cAf6Reg_rdha7_4_control_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: ReadAddr7_4
BitField Type: RW
BitField Desc: Read value will be 0xE00000 plus HaAddr7_4
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha7_4_control_ReadAddr7_4_Bit_Start                                                           0
#define cAf6_rdha7_4_control_ReadAddr7_4_Bit_End                                                            19
#define cAf6_rdha7_4_control_ReadAddr7_4_Mask                                                         cBit19_0
#define cAf6_rdha7_4_control_ReadAddr7_4_Shift                                                               0
#define cAf6_rdha7_4_control_ReadAddr7_4_MaxVal                                                        0xfffff
#define cAf6_rdha7_4_control_ReadAddr7_4_MinVal                                                            0x0
#define cAf6_rdha7_4_control_ReadAddr7_4_RstVal                                                            0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit11_8 Control
Reg Addr   : 0xE0020 - 0xE002F
Reg Formula: 0xE0020 + HaAddr11_8
    Where  :
           + $HaAddr11_8(0-15): HA Address Bit11_8
Reg Desc   :
This register is used to send HA read address bit11_8 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha11_8_control_Base                                                                  0xE0020
#define cAf6Reg_rdha11_8_control(HaAddr118)                                              (0xE0020+(HaAddr118))
#define cAf6Reg_rdha11_8_control_WidthVal                                                                   32
#define cAf6Reg_rdha11_8_control_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: ReadAddr11_8
BitField Type: RW
BitField Desc: Read value will be 0xE00000 plus HaAddr11_8
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha11_8_control_ReadAddr11_8_Bit_Start                                                         0
#define cAf6_rdha11_8_control_ReadAddr11_8_Bit_End                                                          19
#define cAf6_rdha11_8_control_ReadAddr11_8_Mask                                                       cBit19_0
#define cAf6_rdha11_8_control_ReadAddr11_8_Shift                                                             0
#define cAf6_rdha11_8_control_ReadAddr11_8_MaxVal                                                      0xfffff
#define cAf6_rdha11_8_control_ReadAddr11_8_MinVal                                                          0x0
#define cAf6_rdha11_8_control_ReadAddr11_8_RstVal                                                          0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit15_12 Control
Reg Addr   : 0xE0030 - 0xE003F
Reg Formula: 0xE0030 + HaAddr15_12
    Where  :
           + $HaAddr15_12(0-15): HA Address Bit15_12
Reg Desc   :
This register is used to send HA read address bit15_12 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha15_12_control_Base                                                                 0xE0030
#define cAf6Reg_rdha15_12_control(HaAddr1512)                                           (0xE0030+(HaAddr1512))
#define cAf6Reg_rdha15_12_control_WidthVal                                                                  32
#define cAf6Reg_rdha15_12_control_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ReadAddr15_12
BitField Type: RW
BitField Desc: Read value will be 0xE00000 plus HaAddr15_12
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha15_12_control_ReadAddr15_12_Bit_Start                                                       0
#define cAf6_rdha15_12_control_ReadAddr15_12_Bit_End                                                        19
#define cAf6_rdha15_12_control_ReadAddr15_12_Mask                                                     cBit19_0
#define cAf6_rdha15_12_control_ReadAddr15_12_Shift                                                           0
#define cAf6_rdha15_12_control_ReadAddr15_12_MaxVal                                                    0xfffff
#define cAf6_rdha15_12_control_ReadAddr15_12_MinVal                                                        0x0
#define cAf6_rdha15_12_control_ReadAddr15_12_RstVal                                                        0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit19_16 Control
Reg Addr   : 0xE0040 - 0xE004F
Reg Formula: 0xE0040 + HaAddr19_16
    Where  :
           + $HaAddr19_16(0-15): HA Address Bit19_16
Reg Desc   :
This register is used to send HA read address bit19_16 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha19_16_control_Base                                                                 0xE0040
#define cAf6Reg_rdha19_16_control(HaAddr1916)                                           (0xE0040+(HaAddr1916))
#define cAf6Reg_rdha19_16_control_WidthVal                                                                  32
#define cAf6Reg_rdha19_16_control_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ReadAddr19_16
BitField Type: RW
BitField Desc: Read value will be 0xE00000 plus HaAddr19_16
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha19_16_control_ReadAddr19_16_Bit_Start                                                       0
#define cAf6_rdha19_16_control_ReadAddr19_16_Bit_End                                                        19
#define cAf6_rdha19_16_control_ReadAddr19_16_Mask                                                     cBit19_0
#define cAf6_rdha19_16_control_ReadAddr19_16_Shift                                                           0
#define cAf6_rdha19_16_control_ReadAddr19_16_MaxVal                                                    0xfffff
#define cAf6_rdha19_16_control_ReadAddr19_16_MinVal                                                        0x0
#define cAf6_rdha19_16_control_ReadAddr19_16_RstVal                                                        0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit23_20 Control
Reg Addr   : 0xE0050 - 0xE005F
Reg Formula: 0xE0050 + HaAddr23_20
    Where  :
           + $HaAddr23_20(0-15): HA Address Bit23_20
Reg Desc   :
This register is used to send HA read address bit23_20 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha23_20_control_Base                                                                 0xE0050
#define cAf6Reg_rdha23_20_control(HaAddr2320)                                           (0xE0050+(HaAddr2320))
#define cAf6Reg_rdha23_20_control_WidthVal                                                                  32
#define cAf6Reg_rdha23_20_control_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ReadAddr23_20
BitField Type: RW
BitField Desc: Read value will be 0xE00000 plus HaAddr23_20
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha23_20_control_ReadAddr23_20_Bit_Start                                                       0
#define cAf6_rdha23_20_control_ReadAddr23_20_Bit_End                                                        19
#define cAf6_rdha23_20_control_ReadAddr23_20_Mask                                                     cBit19_0
#define cAf6_rdha23_20_control_ReadAddr23_20_Shift                                                           0
#define cAf6_rdha23_20_control_ReadAddr23_20_MaxVal                                                    0xfffff
#define cAf6_rdha23_20_control_ReadAddr23_20_MinVal                                                        0x0
#define cAf6_rdha23_20_control_ReadAddr23_20_RstVal                                                        0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit24 and Data Control
Reg Addr   : 0xE0060 - 0xE0061
Reg Formula: 0xE0060 + HaAddr24
    Where  :
           + $HaAddr24(0-1): HA Address Bit24
Reg Desc   :
This register is used to send HA read address bit24 to HA engine to read data

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha24data_control_Base                                                                0xE0060
#define cAf6Reg_rdha24data_control(HaAddr24)                                              (0xE0060+(HaAddr24))
#define cAf6Reg_rdha24data_control_WidthVal                                                                 32
#define cAf6Reg_rdha24data_control_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: ReadHaData31_0
BitField Type: RW
BitField Desc: HA read data bit31_0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha24data_control_ReadHaData31_0_Bit_Start                                                     0
#define cAf6_rdha24data_control_ReadHaData31_0_Bit_End                                                      31
#define cAf6_rdha24data_control_ReadHaData31_0_Mask                                                   cBit31_0
#define cAf6_rdha24data_control_ReadHaData31_0_Shift                                                         0
#define cAf6_rdha24data_control_ReadHaData31_0_MaxVal                                               0xffffffff
#define cAf6_rdha24data_control_ReadHaData31_0_MinVal                                                      0x0
#define cAf6_rdha24data_control_ReadHaData31_0_RstVal                                                      0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data63_32
Reg Addr   : 0xE0070
Reg Formula:
    Where  :
Reg Desc   :
This register is used to read HA dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha_hold63_32_Base                                                                    0xE0070
#define cAf6Reg_rdha_hold63_32                                                                         0xE0070
#define cAf6Reg_rdha_hold63_32_WidthVal                                                                     32
#define cAf6Reg_rdha_hold63_32_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: ReadHaData63_32
BitField Type: RW
BitField Desc: HA read data bit63_32
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha_hold63_32_ReadHaData63_32_Bit_Start                                                        0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Bit_End                                                         31
#define cAf6_rdha_hold63_32_ReadHaData63_32_Mask                                                      cBit31_0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Shift                                                            0
#define cAf6_rdha_hold63_32_ReadHaData63_32_MaxVal                                                  0xffffffff
#define cAf6_rdha_hold63_32_ReadHaData63_32_MinVal                                                         0x0
#define cAf6_rdha_hold63_32_ReadHaData63_32_RstVal                                                         0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data95_64
Reg Addr   : 0xE0071
Reg Formula:
    Where  :
Reg Desc   :
This register is used to read HA dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold95_64_Base                                                                  0xE0071
#define cAf6Reg_rdindr_hold95_64                                                                       0xE0071
#define cAf6Reg_rdindr_hold95_64_WidthVal                                                                   32
#define cAf6Reg_rdindr_hold95_64_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: ReadHaData95_64
BitField Type: RW
BitField Desc: HA read data bit95_64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Bit_Start                                                      0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Bit_End                                                       31
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Mask                                                    cBit31_0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Shift                                                          0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_MaxVal                                                0xffffffff
#define cAf6_rdindr_hold95_64_ReadHaData95_64_MinVal                                                       0x0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_RstVal                                                       0xx


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data127_96
Reg Addr   : 0xE0072
Reg Formula:
    Where  :
Reg Desc   :
This register is used to read HA dword4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold127_96_Base                                                                 0xE0072
#define cAf6Reg_rdindr_hold127_96                                                                      0xE0072
#define cAf6Reg_rdindr_hold127_96_WidthVal                                                                  32
#define cAf6Reg_rdindr_hold127_96_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ReadHaData127_96
BitField Type: RW
BitField Desc: HA read data bit127_96
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Bit_Start                                                    0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Bit_End                                                     31
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Mask                                                  cBit31_0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Shift                                                        0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_MaxVal                                              0xffffffff
#define cAf6_rdindr_hold127_96_ReadHaData127_96_MinVal                                                     0x0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_RstVal                                                     0xx


/*------------------------------------------------------------------------------
Reg Name   : Classify Parity Register Control
Reg Addr   : 0x000F8000
Reg Formula:
    Where  :
Reg Desc   :
This register using for Force Parity

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_Parity_control_Base                                                             0x000F8000
#define cAf6Reg_cla_Parity_control                                                                  0x000F8000
#define cAf6Reg_cla_Parity_control_WidthVal                                                                 32
#define cAf6Reg_cla_Parity_control_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: ClaForceErr8
BitField Type: RW
BitField Desc: Enable parity error force for "Classify Per Pseudowire Table
Control"
BitField Bits: [17]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr8_Bit_Start                                                      17
#define cAf6_cla_Parity_control_ClaForceErr8_Bit_End                                                        17
#define cAf6_cla_Parity_control_ClaForceErr8_Mask                                                       cBit17
#define cAf6_cla_Parity_control_ClaForceErr8_Shift                                                          17
#define cAf6_cla_Parity_control_ClaForceErr8_MaxVal                                                        0x1
#define cAf6_cla_Parity_control_ClaForceErr8_MinVal                                                        0x0
#define cAf6_cla_Parity_control_ClaForceErr8_RstVal                                                        0x0

/*--------------------------------------
BitField Name: ClaForceErr7
BitField Type: RW
BitField Desc: Enable parity error force for "Classify Per Pseudowire
Identification to CDR Control"
BitField Bits: [16]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr7_Bit_Start                                                      16
#define cAf6_cla_Parity_control_ClaForceErr7_Bit_End                                                        16
#define cAf6_cla_Parity_control_ClaForceErr7_Mask                                                       cBit16
#define cAf6_cla_Parity_control_ClaForceErr7_Shift                                                          16
#define cAf6_cla_Parity_control_ClaForceErr7_MaxVal                                                        0x1
#define cAf6_cla_Parity_control_ClaForceErr7_MinVal                                                        0x0
#define cAf6_cla_Parity_control_ClaForceErr7_RstVal                                                        0x0

/*--------------------------------------
BitField Name: ClaForceErr6
BitField Type: RW
BitField Desc: Enable parity error force for "Classify HBCE Hashing Table
Control"
BitField Bits: [15]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr6_Bit_Start                                                      15
#define cAf6_cla_Parity_control_ClaForceErr6_Bit_End                                                        15
#define cAf6_cla_Parity_control_ClaForceErr6_Mask                                                       cBit15
#define cAf6_cla_Parity_control_ClaForceErr6_Shift                                                          15
#define cAf6_cla_Parity_control_ClaForceErr6_MaxVal                                                        0x1
#define cAf6_cla_Parity_control_ClaForceErr6_MinVal                                                        0x0
#define cAf6_cla_Parity_control_ClaForceErr6_RstVal                                                        0x0

/*--------------------------------------
BitField Name: ClaForceErr5
BitField Type: RW
BitField Desc: Enable parity error force for "Classify HBCE Looking Up
Information Control2"
BitField Bits: [14:7]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr5_Bit_Start                                                       7
#define cAf6_cla_Parity_control_ClaForceErr5_Bit_End                                                        14
#define cAf6_cla_Parity_control_ClaForceErr5_Mask                                                     cBit14_7
#define cAf6_cla_Parity_control_ClaForceErr5_Shift                                                           7
#define cAf6_cla_Parity_control_ClaForceErr5_MaxVal                                                       0xff
#define cAf6_cla_Parity_control_ClaForceErr5_MinVal                                                        0x0
#define cAf6_cla_Parity_control_ClaForceErr5_RstVal                                                        0x0

/*--------------------------------------
BitField Name: ClaForceErr4
BitField Type: RW
BitField Desc: Enable parity error force for "Classify Per Group Enable Control"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr4_Bit_Start                                                       6
#define cAf6_cla_Parity_control_ClaForceErr4_Bit_End                                                         6
#define cAf6_cla_Parity_control_ClaForceErr4_Mask                                                        cBit6
#define cAf6_cla_Parity_control_ClaForceErr4_Shift                                                           6
#define cAf6_cla_Parity_control_ClaForceErr4_MaxVal                                                        0x1
#define cAf6_cla_Parity_control_ClaForceErr4_MinVal                                                        0x0
#define cAf6_cla_Parity_control_ClaForceErr4_RstVal                                                        0x0

/*--------------------------------------
BitField Name: ClaForceErr3
BitField Type: RW
BitField Desc: Enable parity error force for "Classify Per Pseudowire Type
Control"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr3_Bit_Start                                                       5
#define cAf6_cla_Parity_control_ClaForceErr3_Bit_End                                                         5
#define cAf6_cla_Parity_control_ClaForceErr3_Mask                                                        cBit5
#define cAf6_cla_Parity_control_ClaForceErr3_Shift                                                           5
#define cAf6_cla_Parity_control_ClaForceErr3_MaxVal                                                        0x1
#define cAf6_cla_Parity_control_ClaForceErr3_MinVal                                                        0x0
#define cAf6_cla_Parity_control_ClaForceErr3_RstVal                                                        0x0

/*--------------------------------------
BitField Name: ClaForceErr2
BitField Type: RW
BitField Desc: Enable parity error force for "Classify vlan Lookup Control"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr2_Bit_Start                                                       4
#define cAf6_cla_Parity_control_ClaForceErr2_Bit_End                                                         4
#define cAf6_cla_Parity_control_ClaForceErr2_Mask                                                        cBit4
#define cAf6_cla_Parity_control_ClaForceErr2_Shift                                                           4
#define cAf6_cla_Parity_control_ClaForceErr2_MaxVal                                                        0x1
#define cAf6_cla_Parity_control_ClaForceErr2_MinVal                                                        0x0
#define cAf6_cla_Parity_control_ClaForceErr2_RstVal                                                        0x0

/*--------------------------------------
BitField Name: ClaForceErr1
BitField Type: RW
BitField Desc: Enable parity error force for "Classify HBCE Looking Up
Information Control"
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cla_Parity_control_ClaForceErr1_Bit_Start                                                       0
#define cAf6_cla_Parity_control_ClaForceErr1_Bit_End                                                         3
#define cAf6_cla_Parity_control_ClaForceErr1_Mask                                                      cBit3_0
#define cAf6_cla_Parity_control_ClaForceErr1_Shift                                                           0
#define cAf6_cla_Parity_control_ClaForceErr1_MaxVal                                                        0xf
#define cAf6_cla_Parity_control_ClaForceErr1_MinVal                                                        0x0
#define cAf6_cla_Parity_control_ClaForceErr1_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify Parity Disable register Control
Reg Addr   : 0x000F8001
Reg Formula:
    Where  :
Reg Desc   :
This register using for Disable Parity

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_Parity_Disable_control_Base                                                     0x000F8001
#define cAf6Reg_cla_Parity_Disable_control                                                          0x000F8001
#define cAf6Reg_cla_Parity_Disable_control_WidthVal                                                         32
#define cAf6Reg_cla_Parity_Disable_control_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: ClaDisChkErr8
BitField Type: RW
BitField Desc: Disable parity error check for "Classify Per Pseudowire Table
Control"
BitField Bits: [17]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr8_Bit_Start                                             17
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr8_Bit_End                                               17
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr8_Mask                                              cBit17
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr8_Shift                                                 17
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr8_MaxVal                                               0x1
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr8_MinVal                                               0x0
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr8_RstVal                                               0x0

/*--------------------------------------
BitField Name: ClaDisChkErr7
BitField Type: RW
BitField Desc: Disable parity error check for "Classify Per Pseudowire
Identification to CDR Control"
BitField Bits: [16]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr7_Bit_Start                                             16
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr7_Bit_End                                               16
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr7_Mask                                              cBit16
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr7_Shift                                                 16
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr7_MaxVal                                               0x1
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr7_MinVal                                               0x0
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr7_RstVal                                               0x0

/*--------------------------------------
BitField Name: ClaDisChkErr6
BitField Type: RW
BitField Desc: Disable parity error check for "Classify HBCE Hashing Table
Control"
BitField Bits: [15]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr6_Bit_Start                                             15
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr6_Bit_End                                               15
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr6_Mask                                              cBit15
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr6_Shift                                                 15
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr6_MaxVal                                               0x1
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr6_MinVal                                               0x0
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr6_RstVal                                               0x0

/*--------------------------------------
BitField Name: ClaDisChkErr5
BitField Type: RW
BitField Desc: Disable parity error check for "Classify HBCE Looking Up
Information Control2"
BitField Bits: [14:7]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr5_Bit_Start                                              7
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr5_Bit_End                                               14
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr5_Mask                                            cBit14_7
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr5_Shift                                                  7
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr5_MaxVal                                              0xff
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr5_MinVal                                               0x0
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr5_RstVal                                               0x0

/*--------------------------------------
BitField Name: ClaDisChkErr4
BitField Type: RW
BitField Desc: Disable parity error check for "Classify Per Group Enable
Control"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr4_Bit_Start                                              6
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr4_Bit_End                                                6
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr4_Mask                                               cBit6
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr4_Shift                                                  6
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr4_MaxVal                                               0x1
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr4_MinVal                                               0x0
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr4_RstVal                                               0x0

/*--------------------------------------
BitField Name: ClaDisChkErr3
BitField Type: RW
BitField Desc: Disable parity error check for "Classify Per Pseudowire Type
Control"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr3_Bit_Start                                              5
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr3_Bit_End                                                5
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr3_Mask                                               cBit5
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr3_Shift                                                  5
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr3_MaxVal                                               0x1
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr3_MinVal                                               0x0
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr3_RstVal                                               0x0

/*--------------------------------------
BitField Name: ClaDisChkErr2
BitField Type: RW
BitField Desc: Disable parity error check for "Classify vlan Lookup Control"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr2_Bit_Start                                              4
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr2_Bit_End                                                4
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr2_Mask                                               cBit4
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr2_Shift                                                  4
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr2_MaxVal                                               0x1
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr2_MinVal                                               0x0
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr2_RstVal                                               0x0

/*--------------------------------------
BitField Name: ClaDisChkErr1
BitField Type: RW
BitField Desc: Disable parity error check for "Classify HBCE Looking Up
Information Control"
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr1_Bit_Start                                              0
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr1_Bit_End                                                3
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr1_Mask                                             cBit3_0
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr1_Shift                                                  0
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr1_MaxVal                                               0xf
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr1_MinVal                                               0x0
#define cAf6_cla_Parity_Disable_control_ClaDisChkErr1_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify Parity sticky error
Reg Addr   : 0x000F8002
Reg Formula:
    Where  :
Reg Desc   :
This register using for checking sticky error

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_Parity_stk_err_Base                                                             0x000F8002
#define cAf6Reg_cla_Parity_stk_err                                                                  0x000F8002
#define cAf6Reg_cla_Parity_stk_err_WidthVal                                                                 32
#define cAf6Reg_cla_Parity_stk_err_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: ClaParStkErr8
BitField Type: RW
BitField Desc: Parity sticky error check for "Classify Per Pseudowire Table
Control"
BitField Bits: [17]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr8_Bit_Start                                                     17
#define cAf6_cla_Parity_stk_err_ClaParStkErr8_Bit_End                                                       17
#define cAf6_cla_Parity_stk_err_ClaParStkErr8_Mask                                                      cBit17
#define cAf6_cla_Parity_stk_err_ClaParStkErr8_Shift                                                         17
#define cAf6_cla_Parity_stk_err_ClaParStkErr8_MaxVal                                                       0x1
#define cAf6_cla_Parity_stk_err_ClaParStkErr8_MinVal                                                       0x0
#define cAf6_cla_Parity_stk_err_ClaParStkErr8_RstVal                                                       0x0

/*--------------------------------------
BitField Name: ClaParStkErr7
BitField Type: RW
BitField Desc: Parity sticky error check for "Classify Per Pseudowire
Identification to CDR Control"
BitField Bits: [16]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr7_Bit_Start                                                     16
#define cAf6_cla_Parity_stk_err_ClaParStkErr7_Bit_End                                                       16
#define cAf6_cla_Parity_stk_err_ClaParStkErr7_Mask                                                      cBit16
#define cAf6_cla_Parity_stk_err_ClaParStkErr7_Shift                                                         16
#define cAf6_cla_Parity_stk_err_ClaParStkErr7_MaxVal                                                       0x1
#define cAf6_cla_Parity_stk_err_ClaParStkErr7_MinVal                                                       0x0
#define cAf6_cla_Parity_stk_err_ClaParStkErr7_RstVal                                                       0x0

/*--------------------------------------
BitField Name: ClaParStkErr6
BitField Type: RW
BitField Desc: Parity sticky error check for "Classify HBCE Hashing Table
Control"
BitField Bits: [15]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr6_Bit_Start                                                     15
#define cAf6_cla_Parity_stk_err_ClaParStkErr6_Bit_End                                                       15
#define cAf6_cla_Parity_stk_err_ClaParStkErr6_Mask                                                      cBit15
#define cAf6_cla_Parity_stk_err_ClaParStkErr6_Shift                                                         15
#define cAf6_cla_Parity_stk_err_ClaParStkErr6_MaxVal                                                       0x1
#define cAf6_cla_Parity_stk_err_ClaParStkErr6_MinVal                                                       0x0
#define cAf6_cla_Parity_stk_err_ClaParStkErr6_RstVal                                                       0x0

/*--------------------------------------
BitField Name: ClaParStkErr5
BitField Type: RW
BitField Desc: Parity sticky error check for "Classify HBCE Looking Up
Information Control2"
BitField Bits: [14:7]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr5_Bit_Start                                                      7
#define cAf6_cla_Parity_stk_err_ClaParStkErr5_Bit_End                                                       14
#define cAf6_cla_Parity_stk_err_ClaParStkErr5_Mask                                                    cBit14_7
#define cAf6_cla_Parity_stk_err_ClaParStkErr5_Shift                                                          7
#define cAf6_cla_Parity_stk_err_ClaParStkErr5_MaxVal                                                      0xff
#define cAf6_cla_Parity_stk_err_ClaParStkErr5_MinVal                                                       0x0
#define cAf6_cla_Parity_stk_err_ClaParStkErr5_RstVal                                                       0x0

/*--------------------------------------
BitField Name: ClaParStkErr4
BitField Type: RW
BitField Desc: Parity sticky error check for "Classify Per Group Enable Control"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr4_Bit_Start                                                      6
#define cAf6_cla_Parity_stk_err_ClaParStkErr4_Bit_End                                                        6
#define cAf6_cla_Parity_stk_err_ClaParStkErr4_Mask                                                       cBit6
#define cAf6_cla_Parity_stk_err_ClaParStkErr4_Shift                                                          6
#define cAf6_cla_Parity_stk_err_ClaParStkErr4_MaxVal                                                       0x1
#define cAf6_cla_Parity_stk_err_ClaParStkErr4_MinVal                                                       0x0
#define cAf6_cla_Parity_stk_err_ClaParStkErr4_RstVal                                                       0x0

/*--------------------------------------
BitField Name: ClaParStkErr3
BitField Type: RW
BitField Desc: Parity sticky error check for "Classify Per Pseudowire Type
Control"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr3_Bit_Start                                                      5
#define cAf6_cla_Parity_stk_err_ClaParStkErr3_Bit_End                                                        5
#define cAf6_cla_Parity_stk_err_ClaParStkErr3_Mask                                                       cBit5
#define cAf6_cla_Parity_stk_err_ClaParStkErr3_Shift                                                          5
#define cAf6_cla_Parity_stk_err_ClaParStkErr3_MaxVal                                                       0x1
#define cAf6_cla_Parity_stk_err_ClaParStkErr3_MinVal                                                       0x0
#define cAf6_cla_Parity_stk_err_ClaParStkErr3_RstVal                                                       0x0

/*--------------------------------------
BitField Name: ClaParStkErr2
BitField Type: RW
BitField Desc: Parity sticky error check for "Classify vlan Lookup Control"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr2_Bit_Start                                                      4
#define cAf6_cla_Parity_stk_err_ClaParStkErr2_Bit_End                                                        4
#define cAf6_cla_Parity_stk_err_ClaParStkErr2_Mask                                                       cBit4
#define cAf6_cla_Parity_stk_err_ClaParStkErr2_Shift                                                          4
#define cAf6_cla_Parity_stk_err_ClaParStkErr2_MaxVal                                                       0x1
#define cAf6_cla_Parity_stk_err_ClaParStkErr2_MinVal                                                       0x0
#define cAf6_cla_Parity_stk_err_ClaParStkErr2_RstVal                                                       0x0

/*--------------------------------------
BitField Name: ClaParStkErr1
BitField Type: RW
BitField Desc: Parity sticky error check for "Classify HBCE Looking Up
Information Control"
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cla_Parity_stk_err_ClaParStkErr1_Bit_Start                                                      0
#define cAf6_cla_Parity_stk_err_ClaParStkErr1_Bit_End                                                        3
#define cAf6_cla_Parity_stk_err_ClaParStkErr1_Mask                                                     cBit3_0
#define cAf6_cla_Parity_stk_err_ClaParStkErr1_Shift                                                          0
#define cAf6_cla_Parity_stk_err_ClaParStkErr1_MaxVal                                                       0xf
#define cAf6_cla_Parity_stk_err_ClaParStkErr1_MinVal                                                       0x0
#define cAf6_cla_Parity_stk_err_ClaParStkErr1_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify Queue Lookup Control
Reg Addr   : 0x000F0000 - 0x000F7FFF
Reg Formula: 0x000F0000 + SER_ID*0x8 + CoS
    Where  :
           + $SER_ID(0-3071): Service ID
           + $CoS(0-7): PCP or EXP field
Reg Desc   :
This register configures Queue Lookup

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_queue_lk_Base                                                                   0x000F0000
#define cAf6Reg_cla_queue_lk(SERID, CoS)                                        (0x000F0000+(SERID)*0x8+(CoS))
#define cAf6Reg_cla_queue_lk_WidthVal                                                                       32
#define cAf6Reg_cla_queue_lk_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: CLAQueueID
BitField Type: RW
BitField Desc: This indicate the Queue ID base on Service and QoS
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_cla_queue_lk_CLAQueueID_Bit_Start                                                               0
#define cAf6_cla_queue_lk_CLAQueueID_Bit_End                                                                 1
#define cAf6_cla_queue_lk_CLAQueueID_Mask                                                              cBit1_0
#define cAf6_cla_queue_lk_CLAQueueID_Shift                                                                   0
#define cAf6_cla_queue_lk_CLAQueueID_MaxVal                                                                0x3
#define cAf6_cla_queue_lk_CLAQueueID_MinVal                                                                0x0
#define cAf6_cla_queue_lk_CLAQueueID_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify PTCH Service Control
Reg Addr   : 0x000C000 - 0x000C0FF
Reg Formula: 0x000C000 + PTCH_Port
    Where  :
           + $PTCH_Port(0-255): PTCH port number
Reg Desc   :
This register configures service base on PTCH Port

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_tpch_table_service_Base                                                          0x000C000
#define cAf6Reg_cla_tpch_table_service(PTCHPort)                                        (0x000C000+(PTCHPort))
#define cAf6Reg_cla_tpch_table_service_WidthVal                                                             32
#define cAf6Reg_cla_tpch_table_service_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: CLAPTCHService
BitField Type: RW
BitField Desc: This indicate the service that the traffic following 0: CEM PW
service (always use label to lookup flow id) 1: MS service (use label or vlanid
to lookup to flow id) 2: eXAUI service (bypass) 3: EoS service (bypass) else:
reserve
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_cla_tpch_table_service_CLAPTCHService_Bit_Start                                                 0
#define cAf6_cla_tpch_table_service_CLAPTCHService_Bit_End                                                   2
#define cAf6_cla_tpch_table_service_CLAPTCHService_Mask                                                cBit2_0
#define cAf6_cla_tpch_table_service_CLAPTCHService_Shift                                                     0
#define cAf6_cla_tpch_table_service_CLAPTCHService_MaxVal                                                  0x7
#define cAf6_cla_tpch_table_service_CLAPTCHService_MinVal                                                  0x0
#define cAf6_cla_tpch_table_service_CLAPTCHService_RstVal                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Classify OAM link Control
Reg Addr   : 0x000D000 - 0x000DFFF
Reg Formula: 0x000D000 + Vlan_Ctrl
    Where  :
           + $Vlan_Ctrl(0-4095): VLAN field in VLAN control
Reg Desc   :
This register configures link for OAM when packet carry VLAN control

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_oam_link_Base                                                                    0x000D000
#define cAf6Reg_cla_oam_link(VlanCtrl)                                                  (0x000D000+(VlanCtrl))
#define cAf6Reg_cla_oam_link_WidthVal                                                                       32
#define cAf6Reg_cla_oam_link_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: CLAOamSrv
BitField Type: RW
BitField Desc: This indicate the link service of OAM
BitField Bits: [15:12]
       0: HDLC OAM
       1: cHDLC OAM
       2: PPP OAM
       3: MLPPP OAM
       4: Cisco FR OAM
       5: Cisco MLFR End2End OAM
       6: Cisco MLFR UNI/NNI OAM
       7: IETF FR OAM
       8: IETF MLFR End2End OAM
       9: IETF MLFR UNI/NNI OAM
--------------------------------------*/
#define cAf6_cla_oam_link_CLAOamSrv_Bit_Start                                                               12
#define cAf6_cla_oam_link_CLAOamSrv_Bit_End                                                                 15
#define cAf6_cla_oam_link_CLAOamSrv_Mask                                                             cBit15_12
#define cAf6_cla_oam_link_CLAOamSrv_Shift                                                                   12
#define cAf6_cla_oam_link_CLAOamSrv_MaxVal                                                                 0xf
#define cAf6_cla_oam_link_CLAOamSrv_MinVal                                                                 0x0
#define cAf6_cla_oam_link_CLAOamSrv_RstVal

/*--------------------------------------
BitField Name: CLAOamLink
BitField Type: RW
BitField Desc: This indicate the link of OAM
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_cla_oam_link_CLAOamLink_Bit_Start                                                               0
#define cAf6_cla_oam_link_CLAOamLink_Bit_End                                                                11
#define cAf6_cla_oam_link_CLAOamLink_Mask                                                             cBit11_0
#define cAf6_cla_oam_link_CLAOamLink_Shift                                                                   0
#define cAf6_cla_oam_link_CLAOamLink_MaxVal                                                              0xfff
#define cAf6_cla_oam_link_CLAOamLink_MinVal                                                                0x0
#define cAf6_cla_oam_link_CLAOamLink_RstVal                                                                0x0

/*------------------------------------------------------------------------------
Reg Name   : Classify PTCH channel for EoS Service Control
Reg Addr   : 0x000E000 - 0x000EFFF
Reg Formula: 0x000E000 + First_VLAN
    Where  : 
           + $First_VLAN(0-4096): The first VLAN ID
Reg Desc   : 
This register configures port number of EoS bypass service

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_tpch_eos_channel_Base                                                            0x000E000
#define cAf6Reg_cla_tpch_eos_channel(FirstVLAN)                                        (0x000E000+(FirstVLAN))
#define cAf6Reg_cla_tpch_eos_channel_WidthVal                                                               32
#define cAf6Reg_cla_tpch_eos_channel_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: CLAPTCHChlEoS
BitField Type: RW
BitField Desc: This indicate the channel for EoS bypass service
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_cla_tpch_eos_channel_CLAPTCHChlEoS_Bit_Start                                                    0
#define cAf6_cla_tpch_eos_channel_CLAPTCHChlEoS_Bit_End                                                      7
#define cAf6_cla_tpch_eos_channel_CLAPTCHChlEoS_Mask                                                   cBit7_0
#define cAf6_cla_tpch_eos_channel_CLAPTCHChlEoS_Shift                                                        0
#define cAf6_cla_tpch_eos_channel_CLAPTCHChlEoS_MaxVal                                                    0xff
#define cAf6_cla_tpch_eos_channel_CLAPTCHChlEoS_MinVal                                                     0x0
#define cAf6_cla_tpch_eos_channel_CLAPTCHChlEoS_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count PSN error packet
Reg Addr   : 0x00000001(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is statistic count PSN error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_psn_err_r2c_New_Base                                                        0x00000010
#define cAf6Reg_Eth_cnt_psn_err_r2c_Base                                                            0x00000002
#define cAf6Reg_Eth_cnt_psn_err_ro_Base                                                             0x00000001

#define cAf6Reg_Eth_cnt_psn_err_ro                                                                  0x00000001
#define cAf6Reg_Eth_cnt_psn_err_ro_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt_psn_err_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntPSNErr
BitField Type: RO
BitField Desc: This is statistic counter PSN error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_psn_err_ro_CLAEthCntPSNErr_Bit_Start                                                    0
#define cAf6_Eth_cnt_psn_err_ro_CLAEthCntPSNErr_Bit_End                                                     31
#define cAf6_Eth_cnt_psn_err_ro_CLAEthCntPSNErr_Mask                                                  cBit31_0
#define cAf6_Eth_cnt_psn_err_ro_CLAEthCntPSNErr_Shift                                                        0
#define cAf6_Eth_cnt_psn_err_ro_CLAEthCntPSNErr_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt_psn_err_ro_CLAEthCntPSNErr_MinVal                                                     0x0
#define cAf6_Eth_cnt_psn_err_ro_CLAEthCntPSNErr_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count PSN error packet
Reg Addr   : 0x00000002(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is statistic count PSN error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_psn_err_rc_Base                                                             0x00000002
#define cAf6Reg_Eth_cnt_psn_err_rc_Base_New                                                         0x00000010
#define cAf6Reg_Eth_cnt_psn_err_rc                                                                  0x00000002
#define cAf6Reg_Eth_cnt_psn_err_rc_WidthVal                                                                 32
#define cAf6Reg_Eth_cnt_psn_err_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: CLAEthCntPSNErr
BitField Type: RO
BitField Desc: This is statistic counter PSN error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_psn_err_rc_CLAEthCntPSNErr_Bit_Start                                                    0
#define cAf6_Eth_cnt_psn_err_rc_CLAEthCntPSNErr_Bit_End                                                     31
#define cAf6_Eth_cnt_psn_err_rc_CLAEthCntPSNErr_Mask                                                  cBit31_0
#define cAf6_Eth_cnt_psn_err_rc_CLAEthCntPSNErr_Shift                                                        0
#define cAf6_Eth_cnt_psn_err_rc_CLAEthCntPSNErr_MaxVal                                              0xffffffff
#define cAf6_Eth_cnt_psn_err_rc_CLAEthCntPSNErr_MinVal                                                     0x0
#define cAf6_Eth_cnt_psn_err_rc_CLAEthCntPSNErr_RstVal                                                     0x0

/*------------------------------------------------------------------------------
Reg Name   : Classify Debug Information
Reg Addr   : 0x000D8000
Reg Formula: 0x000D8000
Reg Desc   : This register contains debug information of CLA module
------------------------------------------------------------------------------*/
#define cAf6Reg_cla_debug_infor_Base                                                        0x000D8000

#ifdef __cplusplus
}
#endif
#endif /* _CLA_THA60210012MODULECLAREG_H_ */


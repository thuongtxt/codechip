/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : Tha60210012ModuleEncapInternal.h
 * 
 * Created Date: Dec 28, 2016
 *
 * Description : Tha60210012 module encap internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEENCAPINTERNAL_H_
#define _THA60210012MODULEENCAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/encap/ThaModuleEncapInternal.h"
#include "../../../default/encap/dynamic/ThaModuleDynamicEncapInternal.h"
#include "Tha60210012ModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleEncapMethods
    {
    uint32 (*StartVersionSupportProfileManager)(Tha60210012ModuleEncap self);
    uint32 (*StartVersionSupportResequenceManager)(Tha60210012ModuleEncap self);
    eBool  (*StsNcNeedSelect)(Tha60210012ModuleEncap self, AtSdhChannel sdhChannel);
    }tTha60210012ModuleEncapMethods;


typedef struct tTha60210012ModuleEncap
    {
    tThaModuleDynamicEncap super;
    const tTha60210012ModuleEncapMethods * methods;

    /* Private data */
    uint32 counterModule;

    /* async init*/
    uint32 asyncInitState;

    }tTha60210012ModuleEncap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEncap Tha60210012ModuleEncapObjectInit(AtModuleEncap self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEENCAPINTERNAL_H_ */


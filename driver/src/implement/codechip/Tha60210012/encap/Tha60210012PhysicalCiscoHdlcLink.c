/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encapsulation
 *
 * File        : Tha60210012PhysicalCiscoHdlcLink.c
 *
 * Created Date: Apr 11, 2016
 *
 * Description : 60210012 physical HDLC link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/encap/dynamic/ThaModuleDynamicEncap.h"
#include "../../../default/encap/profile/ThaProfileManager.h"
#include "../mpeg/Tha60210012ModuleMpeg.h"
#include "../ppp/Tha60210012PhysicalPppLink.h"
#include "Tha60210012ModuleDecapParserReg.h"
#include "Tha60210012PhysicalHdlcChannel.h"
#include "Tha60210012PhysicalCiscoHdlcLink.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods          m_AtChannelOverride;
static tThaPhysicalPppLinkMethods m_ThaPhysicalPppLinkOverride;

/* Save super implementation */
static const tAtChannelMethods          *m_AtChannelMethods       = NULL;
static const tThaPhysicalPppLinkMethods *m_PhysicalPppLinkMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210012ModuleMpeg ModuleMpeg(AtChannel self)
    {
    return (Tha60210012ModuleMpeg)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleMpeg);
    }

static eAtRet MpeqQueueEnable(AtChannel hdlcLink, uint32 queueId)
    {
    eAtRet ret = cAtOk;
    Tha60210012ModuleMpeg moduleMpeg = ModuleMpeg(hdlcLink);
    uint32 linkId = AtChannelHwIdGet(hdlcLink);

    ret |= Tha60210012ModuleMpegLinkQueueMaxThresholdSet(moduleMpeg, linkId, queueId, 0x7, 0x2);
    ret |= Tha60210012ModuleMpegLinkQueueMinThresholdSet(moduleMpeg, linkId, queueId, 0x7, 0x2);
    ret |= Tha60210012ModuleMpegLinkQueuePrioritySet(moduleMpeg, linkId, queueId, cThaMpegQueuePriorityLowest);
    ret |= Tha60210012ModuleMpegLinkQueueEnable(moduleMpeg, linkId, queueId, cAtTrue);

    return ret;
    }

static eAtRet OamProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    eAtRet ret = Tha60210012LinkOamProfileHwSet(self, profile);
    if (ret != cAtOk)
        return ret;

    /* Deal with database */
    return m_PhysicalPppLinkMethods->OamProfileSet(self, profile);
    }

static eAtRet NetworkProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    eAtRet ret =  Tha60210012LinkNetworkProfileHwSet(self, profile);
    if (ret != cAtOk)
        return ret;

    /* Deal with database */
    return m_PhysicalPppLinkMethods->NetworkProfileSet(self, profile);
    }

static eAtRet ErrorProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    eAtRet ret = Tha60210012LinkErrorProfileHwSet(self, profile);
    if (ret != cAtOk)
        return ret;

    /* Deal with database */
    return m_PhysicalPppLinkMethods->ErrorProfileSet(self, profile);
    }

static eAtRet MpeqQueueDisable(AtChannel hdlcLink, uint32 queueId)
    {
    eAtRet ret = cAtOk;
    Tha60210012ModuleMpeg moduleMpeg = ModuleMpeg(hdlcLink);
    uint32 linkId = AtChannelHwIdGet(hdlcLink);

    ret |= Tha60210012ModuleMpegLinkQueueEnable(moduleMpeg, linkId, queueId, cAtFalse);
    Tha60210012ModuleMpegQueueWaitBufferIsEmpty(moduleMpeg);
    ret |= Tha60210012ModuleMpegLinkQueuePrioritySet(moduleMpeg, linkId, queueId, cThaMpegQueuePriorityUnknown);

    return ret;
    }

static eAtRet QueueEnable(AtChannel self, eBool enable)
    {
    return Tha60210012HdlcLinkQueueEnable(self, enable);
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);
    return Tha60210012EncapLinkDebug((AtHdlcLink)self);
    }

static void OverrideAtChannel(AtPppLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, QueueEnable);
        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaPhysicalPppLink(AtPppLink self)
    {
    ThaPhysicalPppLink link = (ThaPhysicalPppLink)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_PhysicalPppLinkMethods = mMethodsGet(link);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPhysicalPppLinkOverride, mMethodsGet(link), sizeof(m_ThaPhysicalPppLinkOverride));

        mMethodOverride(m_ThaPhysicalPppLinkOverride, ErrorProfileSet);
        mMethodOverride(m_ThaPhysicalPppLinkOverride, NetworkProfileSet);
        mMethodOverride(m_ThaPhysicalPppLinkOverride, OamProfileSet);
        }

    mMethodsSet(link, &m_ThaPhysicalPppLinkOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012PhysicalCiscoHdlcLink);
    }

static void Override(AtPppLink self)
    {
    OverrideAtChannel(self);
    OverrideThaPhysicalPppLink(self);
    }

AtPppLink Tha60210012PhysicalCiscoHdlcLinkObjectInit(AtPppLink self, AtHdlcChannel physicalChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPhysicalCiscoHdlcLinkObjectInit(self, physicalChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPppLink Tha60210012PhysicalCiscoHdlcLinkNew(AtHdlcChannel physicalChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012PhysicalCiscoHdlcLinkObjectInit(newLink, physicalChannel);
    }

eAtRet Tha60210012HdlcLinkQueueEnable(AtChannel self, eBool enable)
    {
    uint8 numQueuePerLink = Tha60210012ModuleMpegNumQueuesForLink(ModuleMpeg(self));
    uint8 queue_i;
    eAtRet ret = cAtOk;

    for (queue_i = 0; queue_i < numQueuePerLink; queue_i++)
        {
        if (enable)
            ret |= MpeqQueueEnable(self, queue_i);
        else
            ret |= MpeqQueueDisable(self, queue_i);
        }

    return ret;
    }

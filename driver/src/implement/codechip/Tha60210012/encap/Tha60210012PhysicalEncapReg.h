/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encapsulation
 * 
 * File        : Tha60210012PhysicalEncapReg.h
 * 
 * Created Date: Mar 29, 2016
 *
 * Description : High order encapsulation register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PHYSICALHOENCAPREG_H_
#define _THA60210012PHYSICALHOENCAPREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : pencfg_lshift8
Reg Addr   : 0x0_0000
Reg Formula:
    Where  :
Reg Desc   :
This register is used to active low order stuff enc(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_pencfg_lshift8_Base                                                                    0x00000
#define cAf6Reg_pencfg_lshift8                                                                         0x00000
#define cAf6Reg_pencfg_lshift8_WidthVal                                                                     32
#define cAf6Reg_pencfg_lshift8_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: pwchid_db
BitField Type: R/W
BitField Desc: PW chid for sticky0
BitField Bits: [15:4]
--------------------------------------*/
#define cAf6_pencfg_lshift8_pwchid_db_Bit_Start                                                              4
#define cAf6_pencfg_lshift8_pwchid_db_Bit_End                                                               15
#define cAf6_pencfg_lshift8_pwchid_db_Mask                                                            cBit15_4
#define cAf6_pencfg_lshift8_pwchid_db_Shift                                                                  4
#define cAf6_pencfg_lshift8_pwchid_db_MaxVal                                                             0xfff
#define cAf6_pencfg_lshift8_pwchid_db_MinVal                                                               0x0
#define cAf6_pencfg_lshift8_pwchid_db_RstVal                                                               0x0

/*--------------------------------------
BitField Name: act_lenc
BitField Type: R/W
BitField Desc: 1: active enc 0: disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pencfg_lshift8_act_lenc_Bit_Start                                                               0
#define cAf6_pencfg_lshift8_act_lenc_Bit_End                                                                 0
#define cAf6_pencfg_lshift8_act_lenc_Mask                                                                cBit0
#define cAf6_pencfg_lshift8_act_lenc_Shift                                                                   0
#define cAf6_pencfg_lshift8_act_lenc_MaxVal                                                                0x1
#define cAf6_pencfg_lshift8_act_lenc_MinVal                                                                0x0
#define cAf6_pencfg_lshift8_act_lenc_RstVal                                                                0x0

/*------------------------------------------------------------------------------
Reg Name   : dhold0_pen
Reg Addr   : 0x0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to hold data when width bus > 32 bit (Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_dhold0_pen_Base                                                                         0x0002
#define cAf6Reg_dhold0_pen                                                                              0x0002
#define cAf6Reg_dhold0_pen_WidthVal                                                                         32
#define cAf6Reg_dhold0_pen_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: hold0
BitField Type: R/W
BitField Desc: hold from [63:32]
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dhold0_pen_hold0_Bit_Start                                                                      0
#define cAf6_dhold0_pen_hold0_Bit_End                                                                       31
#define cAf6_dhold0_pen_hold0_Mask                                                                    cBit31_0
#define cAf6_dhold0_pen_hold0_Shift                                                                          0
#define cAf6_dhold0_pen_hold0_MaxVal                                                                0xffffffff
#define cAf6_dhold0_pen_hold0_MinVal                                                                       0x0
#define cAf6_dhold0_pen_hold0_RstVal                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : dhold1_pen
Reg Addr   : 0x0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to hold data when width bus > 32 bit (Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_dhold1_pen_Base                                                                         0x0003
#define cAf6Reg_dhold1_pen                                                                              0x0003
#define cAf6Reg_dhold1_pen_WidthVal                                                                         32
#define cAf6Reg_dhold1_pen_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: hold1
BitField Type: R/W
BitField Desc: hold from [95:64]
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dhold1_pen_hold1_Bit_Start                                                                      0
#define cAf6_dhold1_pen_hold1_Bit_End                                                                       31
#define cAf6_dhold1_pen_hold1_Mask                                                                    cBit31_0
#define cAf6_dhold1_pen_hold1_Shift                                                                          0
#define cAf6_dhold1_pen_hold1_MaxVal                                                                0xffffffff
#define cAf6_dhold1_pen_hold1_MinVal                                                                       0x0
#define cAf6_dhold1_pen_hold1_RstVal                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : dhold2_pen
Reg Addr   : 0x0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to hold data when width bus > 32 bit (Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_dhold2_pen_Base                                                                         0x0004
#define cAf6Reg_dhold2_pen                                                                              0x0004
#define cAf6Reg_dhold2_pen_WidthVal                                                                         32
#define cAf6Reg_dhold2_pen_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: hold2
BitField Type: R/W
BitField Desc: hold from [127:96]
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dhold2_pen_hold2_Bit_Start                                                                      0
#define cAf6_dhold2_pen_hold2_Bit_End                                                                       31
#define cAf6_dhold2_pen_hold2_Mask                                                                    cBit31_0
#define cAf6_dhold2_pen_hold2_Shift                                                                          0
#define cAf6_dhold2_pen_hold2_MaxVal                                                                0xffffffff
#define cAf6_dhold2_pen_hold2_MinVal                                                                       0x0
#define cAf6_dhold2_pen_hold2_RstVal                                                                       0x0

/*------------------------------------------------------------------------------
Reg Name   : upenctr1
Reg Addr   : 0x0800 - 0x0BFF
Reg Formula: 0x0800+ $chid
    Where  : 
           + $chid(0-1023): chid
Reg Desc   : 
This register is used to configure as below(Add 25bit:TBD).
HDL_PATH: ramctr1.membuf.ram.ram[$chid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upenctr1_Lo_Base                                                                           0x0800
#define cAf6Reg_upenctr1_Lo(chid)                                                                 (0x0800+(chid))
#define cAf6Reg_upenctr1_Lo_WidthVal                                                                           32
#define cAf6Reg_upenctr1_Lo_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: scrable_en
BitField Type: R/W
BitField Desc: scrable_en
BitField Bits: [9]
--------------------------------------*/
#define cAf6_upenctr1_scrable_en_Bit_Start                                                                   9
#define cAf6_upenctr1_scrable_en_Bit_End                                                                     9
#define cAf6_upenctr1_scrable_en_Mask                                                                    cBit9
#define cAf6_upenctr1_scrable_en_Shift                                                                       9
#define cAf6_upenctr1_scrable_en_MaxVal                                                                    0x1
#define cAf6_upenctr1_scrable_en_MinVal                                                                    0x0
#define cAf6_upenctr1_scrable_en_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: cfg_lsbfirst
BitField Type: R/W
BitField Desc: receive is LSB first, (0) is default MSB
BitField Bits: [6]
--------------------------------------*/
#define cAf6_upenctr1_cfg_lsbfirst_Bit_Start                                                                 6
#define cAf6_upenctr1_cfg_lsbfirst_Bit_End                                                                   6
#define cAf6_upenctr1_cfg_lsbfirst_Mask                                                                  cBit6
#define cAf6_upenctr1_cfg_lsbfirst_Shift                                                                     6
#define cAf6_upenctr1_cfg_lsbfirst_MaxVal                                                                  0x1
#define cAf6_upenctr1_cfg_lsbfirst_MinVal                                                                  0x0
#define cAf6_upenctr1_cfg_lsbfirst_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: flagmod
BitField Type: R/W
BitField Desc: 0 : one flag 1: two flag
BitField Bits: [5]
--------------------------------------*/
#define cAf6_upenctr1_flagmod_Bit_Start                                                                      5
#define cAf6_upenctr1_flagmod_Bit_End                                                                        5
#define cAf6_upenctr1_flagmod_Mask                                                                       cBit5
#define cAf6_upenctr1_flagmod_Shift                                                                          5
#define cAf6_upenctr1_flagmod_MaxVal                                                                       0x1
#define cAf6_upenctr1_flagmod_MinVal                                                                       0x0
#define cAf6_upenctr1_flagmod_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: idle_mode
BitField Type: R/W
BitField Desc: 0: 0x7E 1:0xFF
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upenctr1_idle_mode_Bit_Start                                                                    4
#define cAf6_upenctr1_idle_mode_Bit_End                                                                      4
#define cAf6_upenctr1_idle_mode_Mask                                                                     cBit4
#define cAf6_upenctr1_idle_mode_Shift                                                                        4
#define cAf6_upenctr1_idle_mode_MaxVal                                                                     0x1
#define cAf6_upenctr1_idle_mode_MinVal                                                                     0x0
#define cAf6_upenctr1_idle_mode_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: fcsmod32
BitField Type: R/W
BitField Desc: fcsmod32
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upenctr1_fcsmod32_Bit_Start                                                                     3
#define cAf6_upenctr1_fcsmod32_Bit_End                                                                       3
#define cAf6_upenctr1_fcsmod32_Mask                                                                      cBit3
#define cAf6_upenctr1_fcsmod32_Shift                                                                         3
#define cAf6_upenctr1_fcsmod32_MaxVal                                                                      0x1
#define cAf6_upenctr1_fcsmod32_MinVal                                                                      0x0
#define cAf6_upenctr1_fcsmod32_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: fcsinscfg
BitField Type: R/W
BitField Desc: fcsinscfg
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upenctr1_fcsinscfg_Bit_Start                                                                    2
#define cAf6_upenctr1_fcsinscfg_Bit_End                                                                      2
#define cAf6_upenctr1_fcsinscfg_Mask                                                                     cBit2
#define cAf6_upenctr1_fcsinscfg_Shift                                                                        2
#define cAf6_upenctr1_fcsinscfg_MaxVal                                                                     0x1
#define cAf6_upenctr1_fcsinscfg_MinVal                                                                     0x0
#define cAf6_upenctr1_fcsinscfg_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: fcsmsb
BitField Type: R/W
BitField Desc: fcsmsb
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upenctr1_fcsmsb_Bit_Start                                                                       1
#define cAf6_upenctr1_fcsmsb_Bit_End                                                                         1
#define cAf6_upenctr1_fcsmsb_Mask                                                                        cBit1
#define cAf6_upenctr1_fcsmsb_Shift                                                                           1
#define cAf6_upenctr1_fcsmsb_MaxVal                                                                        0x1
#define cAf6_upenctr1_fcsmsb_MinVal                                                                        0x0
#define cAf6_upenctr1_fcsmsb_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: bit_stuff
BitField Type: R/W
BitField Desc: bit_stuff
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upenctr1_bit_stuff_Bit_Start                                                                    0
#define cAf6_upenctr1_bit_stuff_Bit_End                                                                      0
#define cAf6_upenctr1_bit_stuff_Mask                                                                     cBit0
#define cAf6_upenctr1_bit_stuff_Shift                                                                        0
#define cAf6_upenctr1_bit_stuff_MaxVal                                                                     0x1
#define cAf6_upenctr1_bit_stuff_MinVal                                                                     0x0
#define cAf6_upenctr1_bit_stuff_RstVal                                                                     0x0

/*------------------------------------------------------------------------------
Reg Name   : upensta0
Reg Addr   : 0x2800 - 0x2BFF
Reg Formula: 0x2800+ $chid
    Where  : 
           + $chid(0-1023): chid
Reg Desc   : 
This register save status , SW wr 0 all when reconfig enc (Add 25bit:TBD).
HDL_PATH: mem_ramsta0.ram.ram[$chid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upensta0_Base                                                                           0x2800
#define cAf6Reg_upensta0(chid)                                                                 (0x2800+(chid))
#define cAf6Reg_upensta0_WidthVal                                                                          128
#define cAf6Reg_upensta0_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: sta0
BitField Type: R/W
BitField Desc: sta0
BitField Bits: [109:0]
--------------------------------------*/
#define cAf6_upensta0_sta0_Bit_Start                                                                         0
#define cAf6_upensta0_sta0_Bit_End                                                                         109
#define cAf6_upensta0_sta0_Mask_01                                                                    cBit31_0
#define cAf6_upensta0_sta0_Shift_01                                                                          0
#define cAf6_upensta0_sta0_Mask_02                                                                    cBit31_0
#define cAf6_upensta0_sta0_Shift_02                                                                          0
#define cAf6_upensta0_sta0_Mask_03                                                                    cBit31_0
#define cAf6_upensta0_sta0_Shift_03                                                                          0
#define cAf6_upensta0_sta0_Mask_04                                                                    cBit13_0
#define cAf6_upensta0_sta0_Shift_04                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : upensta1
Reg Addr   : 0x3000 - 0x3BFF
Reg Formula: 0x3000+ $chid
    Where  : 
           + $chid(0-1023): chid
Reg Desc   : 
This register save status , SW wr 0 all when reconfig enc (Add 25bit:TBD).
HDL_PATH: mem_ramsta1.ram.ram[$chid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upensta1_Base                                                                           0x3000
#define cAf6Reg_upensta1(chid)                                                                 (0x3000+(chid))
#define cAf6Reg_upensta1_WidthVal                                                                           32
#define cAf6Reg_upensta1_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: sta1
BitField Type: R/W
BitField Desc: sta1
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_upensta1_sta1_Bit_Start                                                                         0
#define cAf6_upensta1_sta1_Bit_End                                                                           1
#define cAf6_upensta1_sta1_Mask                                                                        cBit1_0
#define cAf6_upensta1_sta1_Shift                                                                             0
#define cAf6_upensta1_sta1_MaxVal                                                                          0x3
#define cAf6_upensta1_sta1_MinVal                                                                          0x0
#define cAf6_upensta1_sta1_RstVal                                                                          0x0

/*------------------------------------------------------------------------------
Reg Name   : upenctr1
Reg Addr   : 0x0_0200 - 0x0_023F
Reg Formula: 0x0_0200 + $chid
    Where  : 
           + $chid(0-63): chid
Reg Desc   : 
This register is used to configure as below(Add 24bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upenctr1_Ho_Base                                                                          0x00200
#define cAf6Reg_upenctr1_Ho(chid)                                                                (0x00200+(chid))
#define cAf6Reg_upenctr1_Ho_WidthVal                                                                           32
#define cAf6Reg_upenctr1_Ho_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: scrable_en
BitField Type: R/W
BitField Desc: scrable_en
BitField Bits: [9]
--------------------------------------*/
#define cAf6_upenctr1_scrable_en_Bit_Start                                                                   9
#define cAf6_upenctr1_scrable_en_Bit_End                                                                     9
#define cAf6_upenctr1_scrable_en_Mask                                                                    cBit9
#define cAf6_upenctr1_scrable_en_Shift                                                                       9
#define cAf6_upenctr1_scrable_en_MaxVal                                                                    0x1
#define cAf6_upenctr1_scrable_en_MinVal                                                                    0x0
#define cAf6_upenctr1_scrable_en_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: idle_mode
BitField Type: R/W
BitField Desc: 0: 0x7E 1:0xFF
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upenctr1_idle_mode_Bit_Start                                                                    4
#define cAf6_upenctr1_idle_mode_Bit_End                                                                      4
#define cAf6_upenctr1_idle_mode_Mask                                                                     cBit4
#define cAf6_upenctr1_idle_mode_Shift                                                                        4
#define cAf6_upenctr1_idle_mode_MaxVal                                                                     0x1
#define cAf6_upenctr1_idle_mode_MinVal                                                                     0x0
#define cAf6_upenctr1_idle_mode_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: fcsmod32
BitField Type: R/W
BitField Desc: fcsmod32
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upenctr1_fcsmod32_Bit_Start                                                                     3
#define cAf6_upenctr1_fcsmod32_Bit_End                                                                       3
#define cAf6_upenctr1_fcsmod32_Mask                                                                      cBit3
#define cAf6_upenctr1_fcsmod32_Shift                                                                         3
#define cAf6_upenctr1_fcsmod32_MaxVal                                                                      0x1
#define cAf6_upenctr1_fcsmod32_MinVal                                                                      0x0
#define cAf6_upenctr1_fcsmod32_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: fcsinscfg
BitField Type: R/W
BitField Desc: fcsinscfg
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upenctr1_fcsinscfg_Bit_Start                                                                    2
#define cAf6_upenctr1_fcsinscfg_Bit_End                                                                      2
#define cAf6_upenctr1_fcsinscfg_Mask                                                                     cBit2
#define cAf6_upenctr1_fcsinscfg_Shift                                                                        2
#define cAf6_upenctr1_fcsinscfg_MaxVal                                                                     0x1
#define cAf6_upenctr1_fcsinscfg_MinVal                                                                     0x0
#define cAf6_upenctr1_fcsinscfg_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: fcsmsb
BitField Type: R/W
BitField Desc: fcsmsb
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upenctr1_fcsmsb_Bit_Start                                                                       1
#define cAf6_upenctr1_fcsmsb_Bit_End                                                                         1
#define cAf6_upenctr1_fcsmsb_Mask                                                                        cBit1
#define cAf6_upenctr1_fcsmsb_Shift                                                                           1
#define cAf6_upenctr1_fcsmsb_MaxVal                                                                        0x1
#define cAf6_upenctr1_fcsmsb_MinVal                                                                        0x0
#define cAf6_upenctr1_fcsmsb_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: bit_stuff
BitField Type: R/W
BitField Desc: bit_stuff
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upenctr1_bit_stuff_Bit_Start                                                                    0
#define cAf6_upenctr1_bit_stuff_Bit_End                                                                      0
#define cAf6_upenctr1_bit_stuff_Mask                                                                     cBit0
#define cAf6_upenctr1_bit_stuff_Shift                                                                        0
#define cAf6_upenctr1_bit_stuff_MaxVal                                                                     0x1
#define cAf6_upenctr1_bit_stuff_MinVal                                                                     0x0
#define cAf6_upenctr1_bit_stuff_RstVal                                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG HO ENC STSnc
Reg Addr   : 0xE00
Reg Formula: 0xE00
    Where  : 
Reg Desc   : 
Config stsnc

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ho_prm_txcfglb_Base                                                                    0xE00
#define cAf6Reg_upen_ho_prm_txcfglb                                                                         0xE00
#define cAf6Reg_upen_ho_prm_txcfglb_WidthVal                                                                   32
#define cAf6Reg_upen_ho_prm_txcfglb_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cfg_txenc_fcsmsd3
BitField Type: R/W
BitField Desc: config calculation FCS for group 3 (0) is LSB, (1) is MSB
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd3_Bit_Start                                                   23
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd3_Bit_End                                                     23
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd3_Mask                                                    cBit23
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd3_Shift                                                       23
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd3_MaxVal                                                     0x1
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd3_MinVal                                                     0x0
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd3_RstVal                                                     0x0

/*--------------------------------------
BitField Name: cfg_txenc_fcsmode3
BitField Type: R/W
BitField Desc: config fcs mode for group 3
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode3_Bit_Start                                                  22
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode3_Bit_End                                                    22
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode3_Mask                                                   cBit22
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode3_Shift                                                      22
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode3_MaxVal                                                    0x1
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode3_MinVal                                                    0x0
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode3_RstVal                                                    0x0

/*--------------------------------------
BitField Name: cfg_txenc_fcsins3
BitField Type: R/W
BitField Desc: config fcs insert for group 3
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins3_Bit_Start                                                   21
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins3_Bit_End                                                     21
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins3_Mask                                                    cBit21
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins3_Shift                                                       21
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins3_MaxVal                                                     0x1
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins3_MinVal                                                     0x0
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins3_RstVal                                                     0x0

/*--------------------------------------
BitField Name: cfg_txenc_fcsmsd2
BitField Type: R/W
BitField Desc: config calculation FCS for group 2 (0) is LSB, (1) is MSB
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd2_Bit_Start                                                   20
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd2_Bit_End                                                     20
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd2_Mask                                                    cBit20
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd2_Shift                                                       20
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd2_MaxVal                                                     0x1
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd2_MinVal                                                     0x0
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd2_RstVal                                                     0x0

/*--------------------------------------
BitField Name: cfg_txenc_fcsmode2
BitField Type: R/W
BitField Desc: config fcs mode for group 2
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode2_Bit_Start                                                  19
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode2_Bit_End                                                    19
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode2_Mask                                                   cBit19
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode2_Shift                                                      19
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode2_MaxVal                                                    0x1
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode2_MinVal                                                    0x0
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode2_RstVal                                                    0x0

/*--------------------------------------
BitField Name: cfg_txenc_fcsins2
BitField Type: R/W
BitField Desc: config fcs insert for group 2
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins2_Bit_Start                                                   18
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins2_Bit_End                                                     18
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins2_Mask                                                    cBit18
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins2_Shift                                                       18
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins2_MaxVal                                                     0x1
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins2_MinVal                                                     0x0
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins2_RstVal                                                     0x0

/*--------------------------------------
BitField Name: cfg_txenc_fcsmsd1
BitField Type: R/W
BitField Desc: config calculation FCS for group 1 (0) is LSB, (1) is MSB
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd1_Bit_Start                                                   17
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd1_Bit_End                                                     17
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd1_Mask                                                    cBit17
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd1_Shift                                                       17
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd1_MaxVal                                                     0x1
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd1_MinVal                                                     0x0
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd1_RstVal                                                     0x0

/*--------------------------------------
BitField Name: cfg_txenc_fcsmode1
BitField Type: R/W
BitField Desc: config fcs mode for group 1
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode1_Bit_Start                                                  16
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode1_Bit_End                                                    16
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode1_Mask                                                   cBit16
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode1_Shift                                                      16
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode1_MaxVal                                                    0x1
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode1_MinVal                                                    0x0
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode1_RstVal                                                    0x0

/*--------------------------------------
BitField Name: cfg_txenc_fcsins1
BitField Type: R/W
BitField Desc: config fcs insert for group 1
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins1_Bit_Start                                                   15
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins1_Bit_End                                                     15
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins1_Mask                                                    cBit15
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins1_Shift                                                       15
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins1_MaxVal                                                     0x1
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins1_MinVal                                                     0x0
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins1_RstVal                                                     0x0

/*--------------------------------------
BitField Name: cfg_txenc_fcsmsd0
BitField Type: R/W
BitField Desc: config calculation FCS for group 0 (0) is LSB, (1) is MSB
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd0_Bit_Start                                                   14
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd0_Bit_End                                                     14
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd0_Mask                                                    cBit14
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd0_Shift                                                       14
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd0_MaxVal                                                     0x1
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd0_MinVal                                                     0x0
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmsd0_RstVal                                                     0x0

/*--------------------------------------
BitField Name: cfg_txenc_fcsmode0
BitField Type: R/W
BitField Desc: config fcs mode for group 0
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode0_Bit_Start                                                  13
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode0_Bit_End                                                    13
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode0_Mask                                                   cBit13
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode0_Shift                                                      13
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode0_MaxVal                                                    0x1
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode0_MinVal                                                    0x0
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsmode0_RstVal                                                    0x0

/*--------------------------------------
BitField Name: cfg_txenc_fcsins0
BitField Type: R/W
BitField Desc: config fcs insert for group 0
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins0_Bit_Start                                                   12
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins0_Bit_End                                                     12
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins0_Mask                                                    cBit12
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins0_Shift                                                       12
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins0_MaxVal                                                     0x1
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins0_MinVal                                                     0x0
#define cAf6_upen_prm_txcfglb_cfg_txenc_fcsins0_RstVal                                                     0x0

/*--------------------------------------
BitField Name: cfg_threshold
BitField Type: R/W
BitField Desc: config threshold FIFO store data
BitField Bits: [11:07]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_threshold_Bit_Start                                                        7
#define cAf6_upen_prm_txcfglb_cfg_threshold_Bit_End                                                         11
#define cAf6_upen_prm_txcfglb_cfg_threshold_Mask                                                      cBit11_7
#define cAf6_upen_prm_txcfglb_cfg_threshold_Shift                                                            7
#define cAf6_upen_prm_txcfglb_cfg_threshold_MaxVal                                                        0x1f
#define cAf6_upen_prm_txcfglb_cfg_threshold_MinVal                                                         0x0
#define cAf6_upen_prm_txcfglb_cfg_threshold_RstVal                                                         0x0

/*--------------------------------------
BitField Name: cfg_sts48c
BitField Type: R/W
BitField Desc: enable sts48c, (0) is disable, (1) is enable
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_sts48c_Bit_Start                                                           6
#define cAf6_upen_prm_txcfglb_cfg_sts48c_Bit_End                                                             6
#define cAf6_upen_prm_txcfglb_cfg_sts48c_Mask                                                            cBit6
#define cAf6_upen_prm_txcfglb_cfg_sts48c_Shift                                                               6
#define cAf6_upen_prm_txcfglb_cfg_sts48c_MaxVal                                                            0x1
#define cAf6_upen_prm_txcfglb_cfg_sts48c_MinVal                                                            0x0
#define cAf6_upen_prm_txcfglb_cfg_sts48c_RstVal                                                            0x0

/*--------------------------------------
BitField Name: cfg_sts24c
BitField Type: R/W
BitField Desc: [4] : enable group 0 (0,2,4,6..), [5] : group 1 (1,3,5,7..), (0)
is disable, (1) is enable
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_sts24c_Bit_Start                                                           4
#define cAf6_upen_prm_txcfglb_cfg_sts24c_Bit_End                                                             5
#define cAf6_upen_prm_txcfglb_cfg_sts24c_Mask                                                          cBit5_4
#define cAf6_upen_prm_txcfglb_cfg_sts24c_Shift                                                               4
#define cAf6_upen_prm_txcfglb_cfg_sts24c_MaxVal                                                            0x3
#define cAf6_upen_prm_txcfglb_cfg_sts24c_MinVal                                                            0x0
#define cAf6_upen_prm_txcfglb_cfg_sts24c_RstVal                                                            0x0

/*--------------------------------------
BitField Name: cfg_sts12c
BitField Type: R/W
BitField Desc: [3] : group 3 (3,7,11..) , [2] : group 2 (2,6,10..),[1] : group 1
(1,5,9..), [0] : group 0 (0,4,8...), (0) is disable, (1) is enable
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_sts12c_Bit_Start                                                           0
#define cAf6_upen_prm_txcfglb_cfg_sts12c_Bit_End                                                             3
#define cAf6_upen_prm_txcfglb_cfg_sts12c_Mask                                                          cBit3_0
#define cAf6_upen_prm_txcfglb_cfg_sts12c_Shift                                                               0
#define cAf6_upen_prm_txcfglb_cfg_sts12c_MaxVal                                                            0xf
#define cAf6_upen_prm_txcfglb_cfg_sts12c_MinVal                                                            0x0
#define cAf6_upen_prm_txcfglb_cfg_sts12c_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_sop
Reg Addr   : 0x280-0x2BF(RC)
Reg Formula: 0x280+ $chid
    Where  : 
           + $chid(0-63): chid
Reg Desc   : 
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ho_cnt_sop_rc_Base                                                                     0x280
#define cAf6Reg_upen_ho_cnt_sop_rc(chid)                                                           (0x280+(chid))
#define cAf6Reg_upen_ho_cnt_sop_rc_WidthVal                                                                    32
#define cAf6Reg_upen_ho_cnt_sop_rc_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: updo_cnt_sop
BitField Type: RO
BitField Desc: sop cnt enc
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_ho_cnt_sop_rc_updo_cnt_sop_Bit_Start                                                          0
#define cAf6_upen_ho_cnt_sop_rc_updo_cnt_sop_Bit_End                                                           31
#define cAf6_upen_ho_cnt_sop_rc_updo_cnt_sop_Mask                                                        cBit31_0
#define cAf6_upen_ho_cnt_sop_rc_updo_cnt_sop_Shift                                                              0
#define cAf6_upen_ho_cnt_sop_rc_updo_cnt_sop_MaxVal                                                    0xffffffff
#define cAf6_upen_ho_cnt_sop_rc_updo_cnt_sop_MinVal                                                           0x0
#define cAf6_upen_ho_cnt_sop_rc_updo_cnt_sop_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_sop
Reg Addr   : 0x2C0-0x2FF(RO)
Reg Formula: 0x2C0+ $chid
    Where  : 
           + $chid(0-63): chid
Reg Desc   : 
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ho_cnt_sop_ro_Base                                                                     0x2C0
#define cAf6Reg_upen_ho_cnt_sop_ro(chid)                                                           (0x2C0+(chid))
#define cAf6Reg_upen_ho_cnt_sop_ro_WidthVal                                                                    32
#define cAf6Reg_upen_ho_cnt_sop_ro_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: updo_cnt_sop
BitField Type: RO
BitField Desc: sop cnt enc
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_ho_cnt_sop_ro_updo_cnt_sop_Bit_Start                                                          0
#define cAf6_upen_ho_cnt_sop_ro_updo_cnt_sop_Bit_End                                                           31
#define cAf6_upen_ho_cnt_sop_ro_updo_cnt_sop_Mask                                                        cBit31_0
#define cAf6_upen_ho_cnt_sop_ro_updo_cnt_sop_Shift                                                              0
#define cAf6_upen_ho_cnt_sop_ro_updo_cnt_sop_MaxVal                                                    0xffffffff
#define cAf6_upen_ho_cnt_sop_ro_updo_cnt_sop_MinVal                                                           0x0
#define cAf6_upen_ho_cnt_sop_ro_updo_cnt_sop_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_eop
Reg Addr   : 0x300-0x33F (RC)
Reg Formula: 0x300+ $chid
    Where  : 
           + $chid(0-63): chid
Reg Desc   : 
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ho_cnt_eop_rc_Base                                                                     0x300
#define cAf6Reg_upen_ho_cnt_eop_rc(chid)                                                           (0x300+(chid))
#define cAf6Reg_upen_ho_cnt_eop_rc_WidthVal                                                                    32
#define cAf6Reg_upen_ho_cnt_eop_rc_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: updo_cnt_eop
BitField Type: RO
BitField Desc: eop cnt enc
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_ho_cnt_eop_rc_updo_cnt_eop_Bit_Start                                                          0
#define cAf6_upen_ho_cnt_eop_rc_updo_cnt_eop_Bit_End                                                           31
#define cAf6_upen_ho_cnt_eop_rc_updo_cnt_eop_Mask                                                        cBit31_0
#define cAf6_upen_ho_cnt_eop_rc_updo_cnt_eop_Shift                                                              0
#define cAf6_upen_ho_cnt_eop_rc_updo_cnt_eop_MaxVal                                                    0xffffffff
#define cAf6_upen_ho_cnt_eop_rc_updo_cnt_eop_MinVal                                                           0x0
#define cAf6_upen_ho_cnt_eop_rc_updo_cnt_eop_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_eop
Reg Addr   : 0x340-0x37F(RO)
Reg Formula: 0x340+ $chid
    Where  : 
           + $chid(0-63): chid
Reg Desc   : 
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ho_cnt_eop_ro_Base                                                                     0x340
#define cAf6Reg_upen_ho_cnt_eop_ro(chid)                                                           (0x340+(chid))
#define cAf6Reg_upen_ho_cnt_eop_ro_WidthVal                                                                    32
#define cAf6Reg_upen_ho_cnt_eop_ro_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: updo_cnt_eop
BitField Type: RO
BitField Desc: eop cnt enc
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_ho_cnt_eop_ro_updo_cnt_eop_Bit_Start                                                          0
#define cAf6_upen_ho_cnt_eop_ro_updo_cnt_eop_Bit_End                                                           31
#define cAf6_upen_ho_cnt_eop_ro_updo_cnt_eop_Mask                                                        cBit31_0
#define cAf6_upen_ho_cnt_eop_ro_updo_cnt_eop_Shift                                                              0
#define cAf6_upen_ho_cnt_eop_ro_updo_cnt_eop_MaxVal                                                    0xffffffff
#define cAf6_upen_ho_cnt_eop_ro_updo_cnt_eop_MinVal                                                           0x0
#define cAf6_upen_ho_cnt_eop_ro_updo_cnt_eop_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_byte
Reg Addr   : 0x380-0x3BF (RC)
Reg Formula: 0x380+ $chid
    Where  : 
           + $chid(0-63): chid
Reg Desc   : 
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ho_cnt_byte_rc_Base                                                                    0x380
#define cAf6Reg_upen_ho_cnt_byte_rc(chid)                                                          (0x380+(chid))
#define cAf6Reg_upen_ho_cnt_byte_rc_WidthVal                                                                   32
#define cAf6Reg_upen_ho_cnt_byte_rc_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: updo_cnt_byte
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_ho_cnt_byte_rc_updo_cnt_byte_Bit_Start                                                        0
#define cAf6_upen_ho_cnt_byte_rc_updo_cnt_byte_Bit_End                                                         31
#define cAf6_upen_ho_cnt_byte_rc_updo_cnt_byte_Mask                                                      cBit31_0
#define cAf6_upen_ho_cnt_byte_rc_updo_cnt_byte_Shift                                                            0
#define cAf6_upen_ho_cnt_byte_rc_updo_cnt_byte_MaxVal                                                  0xffffffff
#define cAf6_upen_ho_cnt_byte_rc_updo_cnt_byte_MinVal                                                         0x0
#define cAf6_upen_ho_cnt_byte_rc_updo_cnt_byte_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_byte
Reg Addr   : 0x3C0-0x3FF (RO)
Reg Formula: 0x3C0+ $chid
    Where  : 
           + $chid(0-63): chid
Reg Desc   : 
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ho_cnt_byte_ro_Base                                                                    0x3C0
#define cAf6Reg_upen_ho_cnt_byte_ro(chid)                                                          (0x3C0+(chid))
#define cAf6Reg_upen_ho_cnt_byte_ro_WidthVal                                                                   32
#define cAf6Reg_upen_ho_cnt_byte_ro_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: updo_cnt_byte
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_ho_cnt_byte_ro_updo_cnt_byte_Bit_Start                                                        0
#define cAf6_upen_ho_cnt_byte_ro_updo_cnt_byte_Bit_End                                                         31
#define cAf6_upen_ho_cnt_byte_ro_updo_cnt_byte_Mask                                                      cBit31_0
#define cAf6_upen_ho_cnt_byte_ro_updo_cnt_byte_Shift                                                            0
#define cAf6_upen_ho_cnt_byte_ro_updo_cnt_byte_MaxVal                                                  0xffffffff
#define cAf6_upen_ho_cnt_byte_ro_updo_cnt_byte_MinVal                                                         0x0
#define cAf6_upen_ho_cnt_byte_ro_updo_cnt_byte_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_tdm_pkt
Reg Addr   : 0x1000-0x13FF (RC)
Reg Formula: 0x1000+ $chid
    Where  :
           + $chid(0-1023): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_tdm_pkt_rc_Base                                                                0x1000
#define cAf6Reg_upen_cnt_tdm_pkt_rc(chid)                                                      (0x1000+(chid))
#define cAf6Reg_upen_cnt_tdm_pkt_rc_WidthVal                                                                32
#define cAf6Reg_upen_cnt_tdm_pkt_rc_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: updo_cnt_sop
BitField Type: RO
BitField Desc: sop cnt enc
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_tdm_pkt_rc_updo_cnt_sop_Bit_Start                                                      0
#define cAf6_upen_cnt_tdm_pkt_rc_updo_cnt_sop_Bit_End                                                       31
#define cAf6_upen_cnt_tdm_pkt_rc_updo_cnt_sop_Mask                                                    cBit31_0
#define cAf6_upen_cnt_tdm_pkt_rc_updo_cnt_sop_Shift                                                          0
#define cAf6_upen_cnt_tdm_pkt_rc_updo_cnt_sop_MaxVal                                                0xffffffff
#define cAf6_upen_cnt_tdm_pkt_rc_updo_cnt_sop_MinVal                                                       0x0
#define cAf6_upen_cnt_tdm_pkt_rc_updo_cnt_sop_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_tdm_pkt
Reg Addr   : 0x1400-0x17FF (RO)
Reg Formula: 0x1400+ $chid
    Where  :
           + $chid(0-1023): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_tdm_pkt_ro_Base                                                                0x1400
#define cAf6Reg_upen_cnt_tdm_pkt_ro(chid)                                                      (0x1400+(chid))
#define cAf6Reg_upen_cnt_tdm_pkt_ro_WidthVal                                                                32
#define cAf6Reg_upen_cnt_tdm_pkt_ro_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: updo_cnt_sop
BitField Type: RO
BitField Desc: sop cnt enc
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_tdm_pkt_ro_updo_cnt_sop_Bit_Start                                                      0
#define cAf6_upen_cnt_tdm_pkt_ro_updo_cnt_sop_Bit_End                                                       31
#define cAf6_upen_cnt_tdm_pkt_ro_updo_cnt_sop_Mask                                                    cBit31_0
#define cAf6_upen_cnt_tdm_pkt_ro_updo_cnt_sop_Shift                                                          0
#define cAf6_upen_cnt_tdm_pkt_ro_updo_cnt_sop_MaxVal                                                0xffffffff
#define cAf6_upen_cnt_tdm_pkt_ro_updo_cnt_sop_MinVal                                                       0x0
#define cAf6_upen_cnt_tdm_pkt_ro_updo_cnt_sop_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_tdm_byte
Reg Addr   : 0x3800-0x3BFF(RC)
Reg Formula: 0x3800+ $chid
    Where  :
           + $chid(0-1023): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_tdm_byte_rc_Base                                                               0x3800
#define cAf6Reg_upen_cnt_tdm_byte_rc(chid)                                                     (0x3800+(chid))
#define cAf6Reg_upen_cnt_tdm_byte_rc_WidthVal                                                               32
#define cAf6Reg_upen_cnt_tdm_byte_rc_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: updo_cnt_tdm_byte
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_Bit_Start                                                0
#define cAf6_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_Bit_End                                                 31
#define cAf6_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_Mask                                              cBit31_0
#define cAf6_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_Shift                                                    0
#define cAf6_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_MaxVal                                          0xffffffff
#define cAf6_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_MinVal                                                 0x0
#define cAf6_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_tdm_byte
Reg Addr   : 0x3C00-0x3FFF(RO)
Reg Formula: 0x3C00+ $chid
    Where  :
           + $chid(0-1023): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_tdm_byte_ro_Base                                                               0x3C00
#define cAf6Reg_upen_cnt_tdm_byte_ro(chid)                                                     (0x3C00+(chid))
#define cAf6Reg_upen_cnt_tdm_byte_ro_WidthVal                                                               32
#define cAf6Reg_upen_cnt_tdm_byte_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: updo_cnt_tdm_byte
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_Bit_Start                                                0
#define cAf6_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_Bit_End                                                 31
#define cAf6_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_Mask                                              cBit31_0
#define cAf6_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_Shift                                                    0
#define cAf6_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_MaxVal                                          0xffffffff
#define cAf6_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_MinVal                                                 0x0
#define cAf6_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_eop
Reg Addr   : 0x1800-0x1BFF(RC)
Reg Formula: 0x1800+ $chid
    Where  :
           + $chid(0-1023): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_eop_rc_Base                                                                    0x1800
#define cAf6Reg_upen_cnt_eop_rc(chid)                                                          (0x1800+(chid))
#define cAf6Reg_upen_cnt_eop_rc_WidthVal                                                                    32
#define cAf6Reg_upen_cnt_eop_rc_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: updo_cnt_eop
BitField Type: RO
BitField Desc: eop cnt enc
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_eop_rc_updo_cnt_eop_Bit_Start                                                          0
#define cAf6_upen_cnt_eop_rc_updo_cnt_eop_Bit_End                                                           31
#define cAf6_upen_cnt_eop_rc_updo_cnt_eop_Mask                                                        cBit31_0
#define cAf6_upen_cnt_eop_rc_updo_cnt_eop_Shift                                                              0
#define cAf6_upen_cnt_eop_rc_updo_cnt_eop_MaxVal                                                    0xffffffff
#define cAf6_upen_cnt_eop_rc_updo_cnt_eop_MinVal                                                           0x0
#define cAf6_upen_cnt_eop_rc_updo_cnt_eop_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_eop
Reg Addr   : 0x1C00-0x1FFF (RO)
Reg Formula: 0x1C00+ $chid
    Where  :
           + $chid(0-1023): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_eop_ro_Base                                                                    0x1C00
#define cAf6Reg_upen_cnt_eop_ro(chid)                                                          (0x1C00+(chid))
#define cAf6Reg_upen_cnt_eop_ro_WidthVal                                                                    32
#define cAf6Reg_upen_cnt_eop_ro_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: updo_cnt_eop
BitField Type: RO
BitField Desc: eop cnt enc
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_eop_ro_updo_cnt_eop_Bit_Start                                                          0
#define cAf6_upen_cnt_eop_ro_updo_cnt_eop_Bit_End                                                           31
#define cAf6_upen_cnt_eop_ro_updo_cnt_eop_Mask                                                        cBit31_0
#define cAf6_upen_cnt_eop_ro_updo_cnt_eop_Shift                                                              0
#define cAf6_upen_cnt_eop_ro_updo_cnt_eop_MaxVal                                                    0xffffffff
#define cAf6_upen_cnt_eop_ro_updo_cnt_eop_MinVal                                                           0x0
#define cAf6_upen_cnt_eop_ro_updo_cnt_eop_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_byte
Reg Addr   : 0x2000-0x23FF(RC)
Reg Formula: 0x2000+ $chid
    Where  :
           + $chid(0-1023): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_byte_rc_Base                                                                   0x2000
#define cAf6Reg_upen_cnt_byte_rc(chid)                                                         (0x2000+(chid))
#define cAf6Reg_upen_cnt_byte_rc_WidthVal                                                                   32
#define cAf6Reg_upen_cnt_byte_rc_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: updo_cnt_byte
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_byte_rc_updo_cnt_byte_Bit_Start                                                        0
#define cAf6_upen_cnt_byte_rc_updo_cnt_byte_Bit_End                                                         31
#define cAf6_upen_cnt_byte_rc_updo_cnt_byte_Mask                                                      cBit31_0
#define cAf6_upen_cnt_byte_rc_updo_cnt_byte_Shift                                                            0
#define cAf6_upen_cnt_byte_rc_updo_cnt_byte_MaxVal                                                  0xffffffff
#define cAf6_upen_cnt_byte_rc_updo_cnt_byte_MinVal                                                         0x0
#define cAf6_upen_cnt_byte_rc_updo_cnt_byte_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_byte
Reg Addr   : 0x2400-0x27FF(RO)
Reg Formula: 0x2400+ $chid
    Where  :
           + $chid(0-1023): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_byte_ro_Base                                                                   0x2400
#define cAf6Reg_upen_cnt_byte_ro(chid)                                                         (0x2400+(chid))
#define cAf6Reg_upen_cnt_byte_ro_WidthVal                                                                   32
#define cAf6Reg_upen_cnt_byte_ro_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: updo_cnt_byte
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_byte_ro_updo_cnt_byte_Bit_Start                                                        0
#define cAf6_upen_cnt_byte_ro_updo_cnt_byte_Bit_End                                                         31
#define cAf6_upen_cnt_byte_ro_updo_cnt_byte_Mask                                                      cBit31_0
#define cAf6_upen_cnt_byte_ro_updo_cnt_byte_Shift                                                            0
#define cAf6_upen_cnt_byte_ro_updo_cnt_byte_MaxVal                                                  0xffffffff
#define cAf6_upen_cnt_byte_ro_updo_cnt_byte_MinVal                                                         0x0
#define cAf6_upen_cnt_byte_ro_updo_cnt_byte_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_byteotdm
Reg Addr   : 0x0010(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_byteotdm_rc_Base                                                           0x0010
#define cAf6Reg_upen_cnt_enc_byteotdm_rc                                                                0x0010
#define cAf6Reg_upen_cnt_enc_byteotdm_rc_WidthVal                                                           32
#define cAf6Reg_upen_cnt_enc_byteotdm_rc_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_byteotdm
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_Bit_Start                                        0
#define cAf6_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_Bit_End                                         31
#define cAf6_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_Mask                                      cBit31_0
#define cAf6_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_Shift                                            0
#define cAf6_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_MaxVal                                  0xffffffff
#define cAf6_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_MinVal                                         0x0
#define cAf6_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_byteotdm
Reg Addr   : 0x0012(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_byteotdm_ro_Base                                                           0x0012
#define cAf6Reg_upen_cnt_enc_byteotdm_ro                                                                0x0012
#define cAf6Reg_upen_cnt_enc_byteotdm_ro_WidthVal                                                           32
#define cAf6Reg_upen_cnt_enc_byteotdm_ro_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_byteotdm
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_Bit_Start                                        0
#define cAf6_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_Bit_End                                         31
#define cAf6_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_Mask                                      cBit31_0
#define cAf6_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_Shift                                            0
#define cAf6_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_MaxVal                                  0xffffffff
#define cAf6_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_MinVal                                         0x0
#define cAf6_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_pktotdm
Reg Addr   : 0x0020(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_pktotdm_rc_Base                                                            0x0020
#define cAf6Reg_upen_cnt_enc_pktotdm_rc                                                                 0x0020
#define cAf6Reg_upen_cnt_enc_pktotdm_rc_WidthVal                                                            32
#define cAf6Reg_upen_cnt_enc_pktotdm_rc_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_pktotdm
BitField Type: RO
BitField Desc: cnt pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_Bit_Start                                          0
#define cAf6_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_Bit_End                                           31
#define cAf6_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_Mask                                        cBit31_0
#define cAf6_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_Shift                                              0
#define cAf6_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_MaxVal                                    0xffffffff
#define cAf6_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_MinVal                                           0x0
#define cAf6_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_RstVal                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_pktotdm
Reg Addr   : 0x0022(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_pktotdm_ro_Base                                                            0x0022
#define cAf6Reg_upen_cnt_enc_pktotdm_ro                                                                 0x0022
#define cAf6Reg_upen_cnt_enc_pktotdm_ro_WidthVal                                                            32
#define cAf6Reg_upen_cnt_enc_pktotdm_ro_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_pktotdm
BitField Type: RO
BitField Desc: cnt pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_Bit_Start                                          0
#define cAf6_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_Bit_End                                           31
#define cAf6_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_Mask                                        cBit31_0
#define cAf6_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_Shift                                              0
#define cAf6_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_MaxVal                                    0xffffffff
#define cAf6_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_MinVal                                           0x0
#define cAf6_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_RstVal                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_eopfrmpda
Reg Addr   : 0x0014(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_eopfrmpda_rc_Base                                                          0x0014
#define cAf6Reg_upen_cnt_enc_eopfrmpda_rc                                                               0x0014
#define cAf6Reg_upen_cnt_enc_eopfrmpda_rc_WidthVal                                                          32
#define cAf6Reg_upen_cnt_enc_eopfrmpda_rc_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_eopfrmpda
BitField Type: RO
BitField Desc: cnt pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_Bit_Start                                       0
#define cAf6_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_Bit_End                                       31
#define cAf6_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_Mask                                    cBit31_0
#define cAf6_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_Shift                                          0
#define cAf6_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_MaxVal                                0xffffffff
#define cAf6_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_MinVal                                       0x0
#define cAf6_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_eopfrmpda
Reg Addr   : 0x0016(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_eopfrmpda_ro_Base                                                          0x0016
#define cAf6Reg_upen_cnt_enc_eopfrmpda_ro                                                               0x0016
#define cAf6Reg_upen_cnt_enc_eopfrmpda_ro_WidthVal                                                          32
#define cAf6Reg_upen_cnt_enc_eopfrmpda_ro_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_eopfrmpda
BitField Type: RO
BitField Desc: cnt pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_Bit_Start                                       0
#define cAf6_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_Bit_End                                       31
#define cAf6_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_Mask                                    cBit31_0
#define cAf6_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_Shift                                          0
#define cAf6_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_MaxVal                                0xffffffff
#define cAf6_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_MinVal                                       0x0
#define cAf6_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_sopfrmpda
Reg Addr   : 0x0018(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_sopfrmpda_rc_Base                                                          0x0018
#define cAf6Reg_upen_cnt_enc_sopfrmpda_rc                                                               0x0018
#define cAf6Reg_upen_cnt_enc_sopfrmpda_rc_WidthVal                                                          32
#define cAf6Reg_upen_cnt_enc_sopfrmpda_rc_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_sopfrmpda
BitField Type: RO
BitField Desc: cnt pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_Bit_Start                                       0
#define cAf6_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_Bit_End                                       31
#define cAf6_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_Mask                                    cBit31_0
#define cAf6_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_Shift                                          0
#define cAf6_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_MaxVal                                0xffffffff
#define cAf6_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_MinVal                                       0x0
#define cAf6_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_RstVal                                       0x0

/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_sopfrmpda
Reg Addr   : 0x001A(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_sopfrmpda_ro_Base                                                          0x001A
#define cAf6Reg_upen_cnt_enc_sopfrmpda_ro                                                               0x001A
#define cAf6Reg_upen_cnt_enc_sopfrmpda_ro_WidthVal                                                          32
#define cAf6Reg_upen_cnt_enc_sopfrmpda_ro_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_sopfrmpda
BitField Type: RO
BitField Desc: cnt pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_Bit_Start                                       0
#define cAf6_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_Bit_End                                       31
#define cAf6_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_Mask                                    cBit31_0
#define cAf6_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_Shift                                          0
#define cAf6_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_MaxVal                                0xffffffff
#define cAf6_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_MinVal                                       0x0
#define cAf6_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_bytefrmpda
Reg Addr   : 0x001C(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_bytefrmpda_rc_Base                                                         0x001C
#define cAf6Reg_upen_cnt_enc_bytefrmpda_rc                                                              0x001C
#define cAf6Reg_upen_cnt_enc_bytefrmpda_rc_WidthVal                                                         32
#define cAf6Reg_upen_cnt_enc_bytefrmpda_rc_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_bytefrmpda
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_Bit_Start                                       0
#define cAf6_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_Bit_End                                      31
#define cAf6_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_Mask                                  cBit31_0
#define cAf6_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_Shift                                        0
#define cAf6_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_MaxVal                              0xffffffff
#define cAf6_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_MinVal                                     0x0
#define cAf6_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_bytefrmpda
Reg Addr   : 0x001E(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_bytefrmpda_ro_Base                                                         0x001E
#define cAf6Reg_upen_cnt_enc_bytefrmpda_ro                                                              0x001E
#define cAf6Reg_upen_cnt_enc_bytefrmpda_ro_WidthVal                                                         32
#define cAf6Reg_upen_cnt_enc_bytefrmpda_ro_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_bytefrmpda
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_Bit_Start                                       0
#define cAf6_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_Bit_End                                      31
#define cAf6_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_Mask                                  cBit31_0
#define cAf6_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_Shift                                        0
#define cAf6_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_MaxVal                              0xffffffff
#define cAf6_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_MinVal                                     0x0
#define cAf6_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_abort
Reg Addr   : 0x0024(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_abort_rc_Base                                                              0x0024
#define cAf6Reg_upen_cnt_enc_abort_rc                                                                   0x0024
#define cAf6Reg_upen_cnt_enc_abort_rc_WidthVal                                                              32
#define cAf6Reg_upen_cnt_enc_abort_rc_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_abort
BitField Type: RO
BitField Desc: abort pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_Bit_Start                                              0
#define cAf6_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_Bit_End                                               31
#define cAf6_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_Mask                                            cBit31_0
#define cAf6_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_Shift                                                  0
#define cAf6_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_MaxVal                                        0xffffffff
#define cAf6_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_MinVal                                               0x0
#define cAf6_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_abort
Reg Addr   : 0x0026(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_abort_ro_Base                                                              0x0026
#define cAf6Reg_upen_cnt_enc_abort_ro                                                                   0x0026
#define cAf6Reg_upen_cnt_enc_abort_ro_WidthVal                                                              32
#define cAf6Reg_upen_cnt_enc_abort_ro_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_abort
BitField Type: RO
BitField Desc: abort pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_Bit_Start                                              0
#define cAf6_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_Bit_End                                               31
#define cAf6_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_Mask                                            cBit31_0
#define cAf6_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_Shift                                                  0
#define cAf6_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_MaxVal                                        0xffffffff
#define cAf6_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_MinVal                                               0x0
#define cAf6_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_nstuff
Reg Addr   : 0x0028(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_nstuff_rc_Base                                                             0x0028
#define cAf6Reg_upen_cnt_enc_nstuff_rc                                                                  0x0028
#define cAf6Reg_upen_cnt_enc_nstuff_rc_WidthVal                                                             32
#define cAf6Reg_upen_cnt_enc_nstuff_rc_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_nstuff
BitField Type: RO
BitField Desc: num byte stuff
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_Bit_Start                                            0
#define cAf6_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_Bit_End                                             31
#define cAf6_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_Mask                                          cBit31_0
#define cAf6_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_Shift                                                0
#define cAf6_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_MaxVal                                      0xffffffff
#define cAf6_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_MinVal                                             0x0
#define cAf6_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : upen_cnt_enc_nstuff
Reg Addr   : 0x002A(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_enc_nstuff_ro_Base                                                             0x002A
#define cAf6Reg_upen_cnt_enc_nstuff_ro                                                                  0x002A
#define cAf6Reg_upen_cnt_enc_nstuff_ro_WidthVal                                                             32
#define cAf6Reg_upen_cnt_enc_nstuff_ro_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_nstuff
BitField Type: RO
BitField Desc: num byte stuff
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_Bit_Start                                            0
#define cAf6_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_Bit_End                                             31
#define cAf6_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_Mask                                          cBit31_0
#define cAf6_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_Shift                                                0
#define cAf6_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_MaxVal                                      0xffffffff
#define cAf6_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_MinVal                                             0x0
#define cAf6_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_RstVal                                             0x0

/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_tdm_pkt
Reg Addr   : 0x280-0x2BF(RC)
Reg Formula: 0x280+ $chid
    Where  :
           + $chid(0-63): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_tdm_pkt_rc_Base                                                              0x280
#define cAf6Reg_ho_upen_cnt_tdm_pkt_rc(chid)                                                    (0x280+(chid))
#define cAf6Reg_ho_upen_cnt_tdm_pkt_rc_WidthVal                                                             32
#define cAf6Reg_ho_upen_cnt_tdm_pkt_rc_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: updo_cnt_sop
BitField Type: RO
BitField Desc: sop cnt enc
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_tdm_pkt_rc_updo_cnt_sop_Bit_Start                                                   0
#define cAf6_ho_upen_cnt_tdm_pkt_rc_updo_cnt_sop_Bit_End                                                    31
#define cAf6_ho_upen_cnt_tdm_pkt_rc_updo_cnt_sop_Mask                                                 cBit31_0
#define cAf6_ho_upen_cnt_tdm_pkt_rc_updo_cnt_sop_Shift                                                       0
#define cAf6_ho_upen_cnt_tdm_pkt_rc_updo_cnt_sop_MaxVal                                             0xffffffff
#define cAf6_ho_upen_cnt_tdm_pkt_rc_updo_cnt_sop_MinVal                                                    0x0
#define cAf6_ho_upen_cnt_tdm_pkt_rc_updo_cnt_sop_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_tdm_pkt
Reg Addr   : 0x2C0-0x2FF(RO)
Reg Formula: 0x2C0+ $chid
    Where  :
           + $chid(0-63): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_tdm_pkt_ro_Base                                                              0x2C0
#define cAf6Reg_ho_upen_cnt_tdm_pkt_ro(chid)                                                    (0x2C0+(chid))
#define cAf6Reg_ho_upen_cnt_tdm_pkt_ro_WidthVal                                                             32
#define cAf6Reg_ho_upen_cnt_tdm_pkt_ro_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: updo_cnt_sop
BitField Type: RO
BitField Desc: sop cnt enc
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_tdm_pkt_ro_updo_cnt_sop_Bit_Start                                                   0
#define cAf6_ho_upen_cnt_tdm_pkt_ro_updo_cnt_sop_Bit_End                                                    31
#define cAf6_ho_upen_cnt_tdm_pkt_ro_updo_cnt_sop_Mask                                                 cBit31_0
#define cAf6_ho_upen_cnt_tdm_pkt_ro_updo_cnt_sop_Shift                                                       0
#define cAf6_ho_upen_cnt_tdm_pkt_ro_updo_cnt_sop_MaxVal                                             0xffffffff
#define cAf6_ho_upen_cnt_tdm_pkt_ro_updo_cnt_sop_MinVal                                                    0x0
#define cAf6_ho_upen_cnt_tdm_pkt_ro_updo_cnt_sop_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_tdm_byte
Reg Addr   : 0x400-0x43F(RC)
Reg Formula: 0x400+ $chid
    Where  :
           + $chid(0-63): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_tdm_byte_rc_Base                                                             0x400
#define cAf6Reg_ho_upen_cnt_tdm_byte_rc(chid)                                                   (0x400+(chid))
#define cAf6Reg_ho_upen_cnt_tdm_byte_rc_WidthVal                                                            32
#define cAf6Reg_ho_upen_cnt_tdm_byte_rc_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: updo_cnt_tdm_byte
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_Bit_Start                                             0
#define cAf6_ho_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_Bit_End                                              31
#define cAf6_ho_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_Mask                                           cBit31_0
#define cAf6_ho_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_Shift                                                 0
#define cAf6_ho_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_MaxVal                                       0xffffffff
#define cAf6_ho_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_MinVal                                              0x0
#define cAf6_ho_upen_cnt_tdm_byte_rc_updo_cnt_tdm_byte_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_tdm_byte
Reg Addr   : 0x440-0x47F(RO)
Reg Formula: 0x440+ $chid
    Where  :
           + $chid(0-63): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_tdm_byte_ro_Base                                                             0x440
#define cAf6Reg_ho_upen_cnt_tdm_byte_ro(chid)                                                   (0x440+(chid))
#define cAf6Reg_ho_upen_cnt_tdm_byte_ro_WidthVal                                                            32
#define cAf6Reg_ho_upen_cnt_tdm_byte_ro_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: updo_cnt_tdm_byte
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_Bit_Start                                             0
#define cAf6_ho_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_Bit_End                                              31
#define cAf6_ho_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_Mask                                           cBit31_0
#define cAf6_ho_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_Shift                                                 0
#define cAf6_ho_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_MaxVal                                       0xffffffff
#define cAf6_ho_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_MinVal                                              0x0
#define cAf6_ho_upen_cnt_tdm_byte_ro_updo_cnt_tdm_byte_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_eop
Reg Addr   : 0x300-0x33F(RC)
Reg Formula: 0x300+ $chid
    Where  :
           + $chid(0-63): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_eop_rc_Base                                                                  0x300
#define cAf6Reg_ho_upen_cnt_eop_rc(chid)                                                        (0x300+(chid))
#define cAf6Reg_ho_upen_cnt_eop_rc_WidthVal                                                                 32
#define cAf6Reg_ho_upen_cnt_eop_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: updo_cnt_eop
BitField Type: RO
BitField Desc: eop cnt enc
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_eop_rc_updo_cnt_eop_Bit_Start                                                       0
#define cAf6_ho_upen_cnt_eop_rc_updo_cnt_eop_Bit_End                                                        31
#define cAf6_ho_upen_cnt_eop_rc_updo_cnt_eop_Mask                                                     cBit31_0
#define cAf6_ho_upen_cnt_eop_rc_updo_cnt_eop_Shift                                                           0
#define cAf6_ho_upen_cnt_eop_rc_updo_cnt_eop_MaxVal                                                 0xffffffff
#define cAf6_ho_upen_cnt_eop_rc_updo_cnt_eop_MinVal                                                        0x0
#define cAf6_ho_upen_cnt_eop_rc_updo_cnt_eop_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_eop
Reg Addr   : 0x340-0x37F(RO)
Reg Formula: 0x340+ $chid
    Where  :
           + $chid(0-63): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_eop_ro_Base                                                                  0x340
#define cAf6Reg_ho_upen_cnt_eop_ro(chid)                                                        (0x340+(chid))
#define cAf6Reg_ho_upen_cnt_eop_ro_WidthVal                                                                 32
#define cAf6Reg_ho_upen_cnt_eop_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: updo_cnt_eop
BitField Type: RO
BitField Desc: eop cnt enc
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_eop_ro_updo_cnt_eop_Bit_Start                                                       0
#define cAf6_ho_upen_cnt_eop_ro_updo_cnt_eop_Bit_End                                                        31
#define cAf6_ho_upen_cnt_eop_ro_updo_cnt_eop_Mask                                                     cBit31_0
#define cAf6_ho_upen_cnt_eop_ro_updo_cnt_eop_Shift                                                           0
#define cAf6_ho_upen_cnt_eop_ro_updo_cnt_eop_MaxVal                                                 0xffffffff
#define cAf6_ho_upen_cnt_eop_ro_updo_cnt_eop_MinVal                                                        0x0
#define cAf6_ho_upen_cnt_eop_ro_updo_cnt_eop_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_byte
Reg Addr   : 0x380-0x3BF(RC)
Reg Formula: 0x380+ $chid
    Where  :
           + $chid(0-63): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_byte_rc_Base                                                                 0x380
#define cAf6Reg_ho_upen_cnt_byte_rc(chid)                                                       (0x380+(chid))
#define cAf6Reg_ho_upen_cnt_byte_rc_WidthVal                                                                32
#define cAf6Reg_ho_upen_cnt_byte_rc_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: updo_cnt_byte
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_byte_rc_updo_cnt_byte_Bit_Start                                                     0
#define cAf6_ho_upen_cnt_byte_rc_updo_cnt_byte_Bit_End                                                      31
#define cAf6_ho_upen_cnt_byte_rc_updo_cnt_byte_Mask                                                   cBit31_0
#define cAf6_ho_upen_cnt_byte_rc_updo_cnt_byte_Shift                                                         0
#define cAf6_ho_upen_cnt_byte_rc_updo_cnt_byte_MaxVal                                               0xffffffff
#define cAf6_ho_upen_cnt_byte_rc_updo_cnt_byte_MinVal                                                      0x0
#define cAf6_ho_upen_cnt_byte_rc_updo_cnt_byte_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_byte
Reg Addr   : 0x3C0-0x3FF(RO)
Reg Formula: 0x3C0+ $chid
    Where  :
           + $chid(0-63): chid
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_byte_ro_Base                                                                 0x3C0
#define cAf6Reg_ho_upen_cnt_byte_ro(chid)                                                       (0x3C0+(chid))
#define cAf6Reg_ho_upen_cnt_byte_ro_WidthVal                                                                32
#define cAf6Reg_ho_upen_cnt_byte_ro_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: updo_cnt_byte
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_byte_ro_updo_cnt_byte_Bit_Start                                                     0
#define cAf6_ho_upen_cnt_byte_ro_updo_cnt_byte_Bit_End                                                      31
#define cAf6_ho_upen_cnt_byte_ro_updo_cnt_byte_Mask                                                   cBit31_0
#define cAf6_ho_upen_cnt_byte_ro_updo_cnt_byte_Shift                                                         0
#define cAf6_ho_upen_cnt_byte_ro_updo_cnt_byte_MaxVal                                               0xffffffff
#define cAf6_ho_upen_cnt_byte_ro_updo_cnt_byte_MinVal                                                      0x0
#define cAf6_ho_upen_cnt_byte_ro_updo_cnt_byte_RstVal                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_byteotdm
Reg Addr   : 0x0010(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_byteotdm_rc_Base                                                        0x0010
#define cAf6Reg_ho_upen_cnt_enc_byteotdm_rc                                                             0x0010
#define cAf6Reg_ho_upen_cnt_enc_byteotdm_rc_WidthVal                                                        32
#define cAf6Reg_ho_upen_cnt_enc_byteotdm_rc_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_byteotdm
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_Bit_Start                                       0
#define cAf6_ho_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_Bit_End                                      31
#define cAf6_ho_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_Mask                                   cBit31_0
#define cAf6_ho_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_Shift                                         0
#define cAf6_ho_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_MaxVal                               0xffffffff
#define cAf6_ho_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_MinVal                                      0x0
#define cAf6_ho_upen_cnt_enc_byteotdm_rc_updo_cnt_enc_byteotdm_RstVal                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_byteotdm
Reg Addr   : 0x0012(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_byteotdm_ro_Base                                                        0x0012
#define cAf6Reg_ho_upen_cnt_enc_byteotdm_ro                                                             0x0012
#define cAf6Reg_ho_upen_cnt_enc_byteotdm_ro_WidthVal                                                        32
#define cAf6Reg_ho_upen_cnt_enc_byteotdm_ro_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_byteotdm
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_Bit_Start                                       0
#define cAf6_ho_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_Bit_End                                      31
#define cAf6_ho_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_Mask                                   cBit31_0
#define cAf6_ho_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_Shift                                         0
#define cAf6_ho_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_MaxVal                               0xffffffff
#define cAf6_ho_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_MinVal                                      0x0
#define cAf6_ho_upen_cnt_enc_byteotdm_ro_updo_cnt_enc_byteotdm_RstVal                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_pktotdm
Reg Addr   : 0x0020(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_pktotdm_rc_Base                                                         0x0020
#define cAf6Reg_ho_upen_cnt_enc_pktotdm_rc                                                              0x0020
#define cAf6Reg_ho_upen_cnt_enc_pktotdm_rc_WidthVal                                                         32
#define cAf6Reg_ho_upen_cnt_enc_pktotdm_rc_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_pktotdm
BitField Type: RO
BitField Desc: cnt pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_Bit_Start                                       0
#define cAf6_ho_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_Bit_End                                        31
#define cAf6_ho_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_Mask                                     cBit31_0
#define cAf6_ho_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_Shift                                           0
#define cAf6_ho_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_MaxVal                                 0xffffffff
#define cAf6_ho_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_MinVal                                        0x0
#define cAf6_ho_upen_cnt_enc_pktotdm_rc_updo_cnt_enc_pktotdm_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_pktotdm
Reg Addr   : 0x0022(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_pktotdm_ro_Base                                                         0x0022
#define cAf6Reg_ho_upen_cnt_enc_pktotdm_ro                                                              0x0022
#define cAf6Reg_ho_upen_cnt_enc_pktotdm_ro_WidthVal                                                         32
#define cAf6Reg_ho_upen_cnt_enc_pktotdm_ro_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_pktotdm
BitField Type: RO
BitField Desc: cnt pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_Bit_Start                                       0
#define cAf6_ho_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_Bit_End                                        31
#define cAf6_ho_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_Mask                                     cBit31_0
#define cAf6_ho_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_Shift                                           0
#define cAf6_ho_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_MaxVal                                 0xffffffff
#define cAf6_ho_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_MinVal                                        0x0
#define cAf6_ho_upen_cnt_enc_pktotdm_ro_updo_cnt_enc_pktotdm_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_eopfrmpda
Reg Addr   : 0x0014(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_eopfrmpda_rc_Base                                                       0x0014
#define cAf6Reg_ho_upen_cnt_enc_eopfrmpda_rc                                                            0x0014
#define cAf6Reg_ho_upen_cnt_enc_eopfrmpda_rc_WidthVal                                                       32
#define cAf6Reg_ho_upen_cnt_enc_eopfrmpda_rc_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_eopfrmpda
BitField Type: RO
BitField Desc: cnt pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_Bit_Start                                       0
#define cAf6_ho_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_Bit_End                                      31
#define cAf6_ho_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_Mask                                 cBit31_0
#define cAf6_ho_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_Shift                                       0
#define cAf6_ho_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_MaxVal                              0xffffffff
#define cAf6_ho_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_MinVal                                     0x0
#define cAf6_ho_upen_cnt_enc_eopfrmpda_rc_updo_cnt_enc_eopfrmpda_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_eopfrmpda
Reg Addr   : 0x0016(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_eopfrmpda_ro_Base                                                       0x0016
#define cAf6Reg_ho_upen_cnt_enc_eopfrmpda_ro                                                            0x0016
#define cAf6Reg_ho_upen_cnt_enc_eopfrmpda_ro_WidthVal                                                       32
#define cAf6Reg_ho_upen_cnt_enc_eopfrmpda_ro_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_eopfrmpda
BitField Type: RO
BitField Desc: cnt pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_Bit_Start                                       0
#define cAf6_ho_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_Bit_End                                      31
#define cAf6_ho_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_Mask                                 cBit31_0
#define cAf6_ho_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_Shift                                       0
#define cAf6_ho_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_MaxVal                              0xffffffff
#define cAf6_ho_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_MinVal                                     0x0
#define cAf6_ho_upen_cnt_enc_eopfrmpda_ro_updo_cnt_enc_eopfrmpda_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_sopfrmpda
Reg Addr   : 0x0018(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_sopfrmpda_rc_Base                                                       0x0018
#define cAf6Reg_ho_upen_cnt_enc_sopfrmpda_rc                                                            0x0018
#define cAf6Reg_ho_upen_cnt_enc_sopfrmpda_rc_WidthVal                                                       32
#define cAf6Reg_ho_upen_cnt_enc_sopfrmpda_rc_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_sopfrmpda
BitField Type: RO
BitField Desc: cnt pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_Bit_Start                                       0
#define cAf6_ho_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_Bit_End                                      31
#define cAf6_ho_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_Mask                                 cBit31_0
#define cAf6_ho_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_Shift                                       0
#define cAf6_ho_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_MaxVal                              0xffffffff
#define cAf6_ho_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_MinVal                                     0x0
#define cAf6_ho_upen_cnt_enc_sopfrmpda_rc_updo_cnt_enc_sopfrmpda_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_sopfrmpda
Reg Addr   : 0x001A(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_sopfrmpda_ro_Base                                                       0x001A
#define cAf6Reg_ho_upen_cnt_enc_sopfrmpda_ro                                                            0x001A
#define cAf6Reg_ho_upen_cnt_enc_sopfrmpda_ro_WidthVal                                                       32
#define cAf6Reg_ho_upen_cnt_enc_sopfrmpda_ro_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_sopfrmpda
BitField Type: RO
BitField Desc: cnt pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_Bit_Start                                       0
#define cAf6_ho_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_Bit_End                                      31
#define cAf6_ho_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_Mask                                 cBit31_0
#define cAf6_ho_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_Shift                                       0
#define cAf6_ho_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_MaxVal                              0xffffffff
#define cAf6_ho_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_MinVal                                     0x0
#define cAf6_ho_upen_cnt_enc_sopfrmpda_ro_updo_cnt_enc_sopfrmpda_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_bytefrmpda
Reg Addr   : 0x001C(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_bytefrmpda_rc_Base                                                      0x001C
#define cAf6Reg_ho_upen_cnt_enc_bytefrmpda_rc                                                           0x001C
#define cAf6Reg_ho_upen_cnt_enc_bytefrmpda_rc_WidthVal                                                      32
#define cAf6Reg_ho_upen_cnt_enc_bytefrmpda_rc_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_bytefrmpda
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_Bit_Start                                       0
#define cAf6_ho_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_Bit_End                                      31
#define cAf6_ho_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_Mask                                cBit31_0
#define cAf6_ho_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_Shift                                       0
#define cAf6_ho_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_MaxVal                              0xffffffff
#define cAf6_ho_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_MinVal                                     0x0
#define cAf6_ho_upen_cnt_enc_bytefrmpda_rc_updo_cnt_enc_bytefrmpda_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_bytefrmpda
Reg Addr   : 0x001E(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_bytefrmpda_ro_Base                                                      0x001E
#define cAf6Reg_ho_upen_cnt_enc_bytefrmpda_ro                                                           0x001E
#define cAf6Reg_ho_upen_cnt_enc_bytefrmpda_ro_WidthVal                                                      32
#define cAf6Reg_ho_upen_cnt_enc_bytefrmpda_ro_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_bytefrmpda
BitField Type: RO
BitField Desc: cnt byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_Bit_Start                                       0
#define cAf6_ho_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_Bit_End                                      31
#define cAf6_ho_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_Mask                                cBit31_0
#define cAf6_ho_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_Shift                                       0
#define cAf6_ho_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_MaxVal                              0xffffffff
#define cAf6_ho_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_MinVal                                     0x0
#define cAf6_ho_upen_cnt_enc_bytefrmpda_ro_updo_cnt_enc_bytefrmpda_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_abort
Reg Addr   : 0x0024(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_abort_rc_Base                                                           0x0024
#define cAf6Reg_ho_upen_cnt_enc_abort_rc                                                                0x0024
#define cAf6Reg_ho_upen_cnt_enc_abort_rc_WidthVal                                                           32
#define cAf6Reg_ho_upen_cnt_enc_abort_rc_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_abort
BitField Type: RO
BitField Desc: abort pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_Bit_Start                                           0
#define cAf6_ho_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_Bit_End                                            31
#define cAf6_ho_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_Mask                                         cBit31_0
#define cAf6_ho_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_Shift                                               0
#define cAf6_ho_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_MaxVal                                     0xffffffff
#define cAf6_ho_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_MinVal                                            0x0
#define cAf6_ho_upen_cnt_enc_abort_rc_updo_cnt_enc_abort_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_abort
Reg Addr   : 0x0026(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_abort_ro_Base                                                           0x0026
#define cAf6Reg_ho_upen_cnt_enc_abort_ro                                                                0x0026
#define cAf6Reg_ho_upen_cnt_enc_abort_ro_WidthVal                                                           32
#define cAf6Reg_ho_upen_cnt_enc_abort_ro_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_abort
BitField Type: RO
BitField Desc: abort pkt
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_Bit_Start                                           0
#define cAf6_ho_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_Bit_End                                            31
#define cAf6_ho_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_Mask                                         cBit31_0
#define cAf6_ho_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_Shift                                               0
#define cAf6_ho_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_MaxVal                                     0xffffffff
#define cAf6_ho_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_MinVal                                            0x0
#define cAf6_ho_upen_cnt_enc_abort_ro_updo_cnt_enc_abort_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_nstuff
Reg Addr   : 0x0028(RC)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_nstuff_rc_Base                                                          0x0028
#define cAf6Reg_ho_upen_cnt_enc_nstuff_rc                                                               0x0028
#define cAf6Reg_ho_upen_cnt_enc_nstuff_rc_WidthVal                                                          32
#define cAf6Reg_ho_upen_cnt_enc_nstuff_rc_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_nstuff
BitField Type: RO
BitField Desc: num byte stuff
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_Bit_Start                                         0
#define cAf6_ho_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_Bit_End                                          31
#define cAf6_ho_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_Mask                                       cBit31_0
#define cAf6_ho_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_Shift                                             0
#define cAf6_ho_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_MaxVal                                   0xffffffff
#define cAf6_ho_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_MinVal                                          0x0
#define cAf6_ho_upen_cnt_enc_nstuff_rc_updo_cnt_enc_nstuff_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : ho_upen_cnt_enc_nstuff
Reg Addr   : 0x002A(RO)
Reg Formula:
    Where  :
Reg Desc   :
This register is used to configure as below(Add 25bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_ho_upen_cnt_enc_nstuff_ro_Base                                                          0x002A
#define cAf6Reg_ho_upen_cnt_enc_nstuff_ro                                                               0x002A
#define cAf6Reg_ho_upen_cnt_enc_nstuff_ro_WidthVal                                                          32
#define cAf6Reg_ho_upen_cnt_enc_nstuff_ro_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: updo_cnt_enc_nstuff
BitField Type: RO
BitField Desc: num byte stuff
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ho_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_Bit_Start                                         0
#define cAf6_ho_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_Bit_End                                          31
#define cAf6_ho_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_Mask                                       cBit31_0
#define cAf6_ho_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_Shift                                             0
#define cAf6_ho_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_MaxVal                                   0xffffffff
#define cAf6_ho_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_MinVal                                          0x0
#define cAf6_ho_upen_cnt_enc_nstuff_ro_updo_cnt_enc_nstuff_RstVal                                          0x0

/*------------------------------------------------------------------------------
Reg Name   : upen_sticky0
Reg Addr   : 0x0_0001
Reg Formula:
    Where  :
Reg Desc   :
This register is used to debug ENC as below(Add 25bit:TBD).
+ To use this stiky : At first write pwchid_db (add :0x0_0000 above) to select id debug. Then write 0xffffffff to sticky0 register to clear the events.
Then read this register again.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sticky0_Base                                                                      0x00001
#define cAf6Reg_upen_sticky0                                                                           0x00001
#define cAf6Reg_upen_sticky0_WidthVal                                                                       32
#define cAf6Reg_upen_sticky0_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: map_req_err
BitField Type: R/W
BitField Desc: 1: map req err 0: map req good
BitField Bits: [23]
--------------------------------------*/
#define cAf6_upen_sticky0_map_req_err_Bit_Start                                                             23
#define cAf6_upen_sticky0_map_req_err_Bit_End                                                               23
#define cAf6_upen_sticky0_map_req_err_Mask                                                              cBit23
#define cAf6_upen_sticky0_map_req_err_Shift                                                                 23
#define cAf6_upen_sticky0_map_req_err_MaxVal                                                               0x1
#define cAf6_upen_sticky0_map_req_err_MinVal                                                               0x0
#define cAf6_upen_sticky0_map_req_err_RstVal                                                               0x0

/*--------------------------------------
BitField Name: trans_state_check
BitField Type: R/W
BitField Desc: 1: process data ENC good 0: Unused
BitField Bits: [20]
--------------------------------------*/
#define cAf6_upen_sticky0_trans_state_check_Bit_Start                                                       20
#define cAf6_upen_sticky0_trans_state_check_Bit_End                                                         20
#define cAf6_upen_sticky0_trans_state_check_Mask                                                        cBit20
#define cAf6_upen_sticky0_trans_state_check_Shift                                                           20
#define cAf6_upen_sticky0_trans_state_check_MaxVal                                                         0x1
#define cAf6_upen_sticky0_trans_state_check_MinVal                                                         0x0
#define cAf6_upen_sticky0_trans_state_check_RstVal                                                         0x0

/*--------------------------------------
BitField Name: fcs_state_check
BitField Type: R/W
BitField Desc: 4'hF: FCS32 mode 4'hC: FCS16 mode 4'h0: disable FCS mode others :
FCS mode ERR
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_upen_sticky0_fcs_state_check_Bit_Start                                                         16
#define cAf6_upen_sticky0_fcs_state_check_Bit_End                                                           19
#define cAf6_upen_sticky0_fcs_state_check_Mask                                                       cBit19_16
#define cAf6_upen_sticky0_fcs_state_check_Shift                                                             16
#define cAf6_upen_sticky0_fcs_state_check_MaxVal                                                           0xf
#define cAf6_upen_sticky0_fcs_state_check_MinVal                                                           0x0
#define cAf6_upen_sticky0_fcs_state_check_RstVal                                                           0x0

/*--------------------------------------
BitField Name: pda_nemp
BitField Type: R/W
BitField Desc: 1: buffer PDA empty 0: Unused
BitField Bits: [15]
--------------------------------------*/
#define cAf6_upen_sticky0_pda_nemp_Bit_Start                                                                15
#define cAf6_upen_sticky0_pda_nemp_Bit_End                                                                  15
#define cAf6_upen_sticky0_pda_nemp_Mask                                                                 cBit15
#define cAf6_upen_sticky0_pda_nemp_Shift                                                                    15
#define cAf6_upen_sticky0_pda_nemp_MaxVal                                                                  0x1
#define cAf6_upen_sticky0_pda_nemp_MinVal                                                                  0x0
#define cAf6_upen_sticky0_pda_nemp_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: pda_emp
BitField Type: R/W
BitField Desc: 1: buffer PDA NOT empty 0: Unused
BitField Bits: [14]
--------------------------------------*/
#define cAf6_upen_sticky0_pda_emp_Bit_Start                                                                 14
#define cAf6_upen_sticky0_pda_emp_Bit_End                                                                   14
#define cAf6_upen_sticky0_pda_emp_Mask                                                                  cBit14
#define cAf6_upen_sticky0_pda_emp_Shift                                                                     14
#define cAf6_upen_sticky0_pda_emp_MaxVal                                                                   0x1
#define cAf6_upen_sticky0_pda_emp_MinVal                                                                   0x0
#define cAf6_upen_sticky0_pda_emp_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: pda_vld_other_id
BitField Type: R/W
BitField Desc: 1: There 're other ids return from PDA 0: There 're NOT other ids
return from PDA
BitField Bits: [11]
--------------------------------------*/
#define cAf6_upen_sticky0_pda_vld_other_id_Bit_Start                                                        11
#define cAf6_upen_sticky0_pda_vld_other_id_Bit_End                                                          11
#define cAf6_upen_sticky0_pda_vld_other_id_Mask                                                         cBit11
#define cAf6_upen_sticky0_pda_vld_other_id_Shift                                                            11
#define cAf6_upen_sticky0_pda_vld_other_id_MaxVal                                                          0x1
#define cAf6_upen_sticky0_pda_vld_other_id_MinVal                                                          0x0
#define cAf6_upen_sticky0_pda_vld_other_id_RstVal                                                          0x0

/*--------------------------------------
BitField Name: map_req_other_id
BitField Type: R/W
BitField Desc: 1: There 're other ids from map req 0: There 're NOT other ids
from map req
BitField Bits: [10]
--------------------------------------*/
#define cAf6_upen_sticky0_map_req_other_id_Bit_Start                                                        10
#define cAf6_upen_sticky0_map_req_other_id_Bit_End                                                          10
#define cAf6_upen_sticky0_map_req_other_id_Mask                                                         cBit10
#define cAf6_upen_sticky0_map_req_other_id_Shift                                                            10
#define cAf6_upen_sticky0_map_req_other_id_MaxVal                                                          0x1
#define cAf6_upen_sticky0_map_req_other_id_MinVal                                                          0x0
#define cAf6_upen_sticky0_map_req_other_id_RstVal                                                          0x0

/*--------------------------------------
BitField Name: pda_req
BitField Type: R/W
BitField Desc: 1: GOOD    ENC req PDA 0: NOTGOOD ENC req PDA
BitField Bits: [9]
--------------------------------------*/
#define cAf6_upen_sticky0_pda_req_Bit_Start                                                                  9
#define cAf6_upen_sticky0_pda_req_Bit_End                                                                    9
#define cAf6_upen_sticky0_pda_req_Mask                                                                   cBit9
#define cAf6_upen_sticky0_pda_req_Shift                                                                      9
#define cAf6_upen_sticky0_pda_req_MaxVal                                                                   0x1
#define cAf6_upen_sticky0_pda_req_MinVal                                                                   0x0
#define cAf6_upen_sticky0_pda_req_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: idle_good_same_pmc
BitField Type: R/W
BitField Desc: 1:Idle and good packet from ENC out to PMC SAME 0:Idle and good
packet from ENC out to PMC NOTSAME
BitField Bits: [7]
--------------------------------------*/
#define cAf6_upen_sticky0_idle_good_same_pmc_Bit_Start                                                       7
#define cAf6_upen_sticky0_idle_good_same_pmc_Bit_End                                                         7
#define cAf6_upen_sticky0_idle_good_same_pmc_Mask                                                        cBit7
#define cAf6_upen_sticky0_idle_good_same_pmc_Shift                                                           7
#define cAf6_upen_sticky0_idle_good_same_pmc_MaxVal                                                        0x1
#define cAf6_upen_sticky0_idle_good_same_pmc_MinVal                                                        0x0
#define cAf6_upen_sticky0_idle_good_same_pmc_RstVal                                                        0x0

/*--------------------------------------
BitField Name: map_ovld
BitField Type: R/W
BitField Desc: 1: GOOD vld to MAP 0: NOTGOOD  vld to MAP
BitField Bits: [5]
--------------------------------------*/
#define cAf6_upen_sticky0_map_ovld_Bit_Start                                                                 5
#define cAf6_upen_sticky0_map_ovld_Bit_End                                                                   5
#define cAf6_upen_sticky0_map_ovld_Mask                                                                  cBit5
#define cAf6_upen_sticky0_map_ovld_Shift                                                                     5
#define cAf6_upen_sticky0_map_ovld_MaxVal                                                                  0x1
#define cAf6_upen_sticky0_map_ovld_MinVal                                                                  0x0
#define cAf6_upen_sticky0_map_ovld_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: enc_bypass
BitField Type: R/W
BitField Desc: 1: bypass mode 0: normal mode
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upen_sticky0_enc_bypass_Bit_Start                                                               2
#define cAf6_upen_sticky0_enc_bypass_Bit_End                                                                 2
#define cAf6_upen_sticky0_enc_bypass_Mask                                                                cBit2
#define cAf6_upen_sticky0_enc_bypass_Shift                                                                   2
#define cAf6_upen_sticky0_enc_bypass_MaxVal                                                                0x1
#define cAf6_upen_sticky0_enc_bypass_MinVal                                                                0x0
#define cAf6_upen_sticky0_enc_bypass_RstVal                                                                0x0

/*--------------------------------------
BitField Name: map_req
BitField Type: R/W
BitField Desc: 1: map req 0: map NOT req
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_sticky0_map_req_Bit_Start                                                                  1
#define cAf6_upen_sticky0_map_req_Bit_End                                                                    1
#define cAf6_upen_sticky0_map_req_Mask                                                                   cBit1
#define cAf6_upen_sticky0_map_req_Shift                                                                      1
#define cAf6_upen_sticky0_map_req_MaxVal                                                                   0x1
#define cAf6_upen_sticky0_map_req_MinVal                                                                   0x0
#define cAf6_upen_sticky0_map_req_RstVal                                                                   0x0

/*------------------------------------------------------------------------------
Reg Name   : pencfg_sts3c
Reg Addr   : 0x0_0007
Reg Formula:
    Where  :
Reg Desc   :
This register is used to cfg STS3C mode (Add 24bit:TBD).

------------------------------------------------------------------------------*/
#define cAf6Reg_pencfg_sts3c_Base                                                                      0x00007
#define cAf6Reg_pencfg_sts3c                                                                           0x00007
#define cAf6Reg_pencfg_sts3c_WidthVal                                                                       32
#define cAf6Reg_pencfg_sts3c_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: sts3c_en
BitField Type: R/W
BitField Desc: [15]group15 (15,33,47)..., [1]:group1 (1,17,33) , [0]:group0
(0,16,32) 1: act 0: dis
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_pencfg_sts3c_sts3c_en_Bit_Start                                                                 0
#define cAf6_pencfg_sts3c_sts3c_en_Bit_End                                                                  15
#define cAf6_pencfg_sts3c_sts3c_en_Mask(hwStsId)                                                            (cBit0 << (hwStsId))
#define cAf6_pencfg_sts3c_sts3c_en_Shift(hwStsId)                                                           (hwStsId)
#define cAf6_pencfg_sts3c_sts3c_en_MaxVal                                                               0xffff
#define cAf6_pencfg_sts3c_sts3c_en_MinVal                                                                  0x0
#define cAf6_pencfg_sts3c_sts3c_en_RstVal                                                                  0x0

/*------------------------------------------------------------------------------
Reg Name   : ENC Table
Reg Addr   : 0x000200-0x0003FF
Reg Formula: 0x000200 + $VCG/$PWID/$ChannelId
    Where  :
           + $VCG/$PWID/$ChannelId(0-47) : $VCG/$PWID/$ChannelId - ID
Reg Desc   :
Per STS-ID Control

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_map_sts_set_Base                                                                        0x000200
#define cAf6Reg_upen_map_sts_Set(channelId)                                (0x000200+(channelId))
#define cAf6Reg_upen_map_sts_set_WidthVal                                                                          32
#define cAf6Reg_upen_map_sts_set_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: icfgscramble
BitField Type: R/W
BitField Desc: 1 is Enable, 0 is Disable
BitField Bits: [10]
--------------------------------------*/
#define cAf6_upen_mapf_icfgscramble_Bit_Start                                                                     10
#define cAf6_upen_mapf_icfgscramble_Bit_End                                                                       10
#define cAf6_upen_mapf_icfgscramble_Mask                                                                      cBit10
#define cAf6_upen_mapf_icfgscramble_Shift                                                                         10
#define cAf6_upen_mapf_icfgscramble_MaxVal                                                                      0x1
#define cAf6_upen_mapf_icfgscramble_MinVal                                                                      0x0
#define cAf6_upen_mapf_icfgscramble_RstVal                                                                      0x0


/*--------------------------------------
BitField Name: icfgcrc
BitField Type: R/W
BitField Desc: 1 is CRC-16, 0 is CRC-32
BitField Bits: [9]
--------------------------------------*/
#define cAf6_upen_mapf_icfgcrc_Bit_Start                                                                     9
#define cAf6_upen_mapf_icfgcrc_Bit_End                                                                       9
#define cAf6_upen_mapf_icfgcrc_Mask                                                                      cBit9
#define cAf6_upen_mapf_icfgcrc_Shift                                                                         9
#define cAf6_upen_mapf_icfgcrc_MaxVal                                                                      0x1
#define cAf6_upen_mapf_icfgcrc_MinVal                                                                      0x0
#define cAf6_upen_mapf_icfgcrc_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: encicfg
BitField Type: R/W
BitField Desc: 1 is STM-1, 2 is STS-3C, 3 is STS-12C, 4 is STS-24C, 5 is STS-48C
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_upen_mapf_encicfg_Bit_Start                                                                     6
#define cAf6_upen_mapf_encicfg_Bit_End                                                                       8
#define cAf6_upen_mapf_encicfg_Mask                                                                    cBit8_6
#define cAf6_upen_mapf_encicfg_Shift                                                                         6
#define cAf6_upen_mapf_encicfg_MaxVal                                                                      0x7
#define cAf6_upen_mapf_encicfg_MinVal                                                                      0x0
#define cAf6_upen_mapf_encicfg_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: encivcx
BitField Type: R/W
BitField Desc: STS-ID Conversion are Used in Engine
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_upen_mapf_encivcx_Bit_Start                                                                     0
#define cAf6_upen_mapf_encivcx_Bit_End                                                                       5
#define cAf6_upen_mapf_encivcx_Mask                                                                    cBit5_0
#define cAf6_upen_mapf_encivcx_Shift                                                                         0
#define cAf6_upen_mapf_encivcx_MaxVal                                                                     0x3f
#define cAf6_upen_mapf_encivcx_MinVal                                                                      0x0
#define cAf6_upen_mapf_encivcx_RstVal                                                                      0x0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PHYSICALHOENCAPREG_H_ */


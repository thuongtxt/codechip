/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : Tha60210012EncapChannel.h
 * 
 * Created Date: May 27, 2016
 *
 * Description : To have common declarations of all encap channel types
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012ENCAPCHANNEL_H_
#define _THA60210012ENCAPCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEncapChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtGfpChannel Tha60210012VcgGfpChannelNew(uint32 channelId, AtModuleEncap module);
AtHdlcChannel Tha60210012VcgHdlcChannelNew(uint32 channelId, AtModuleEncap module);
AtLapsLink Tha60210012VcgLapsLinkNew(AtHdlcChannel channel);

void Tha60210012EncapChannelScrambleEnable(AtEncapChannel self, eBool enable);
void Tha60210012DecapChannelScrambleEnable(AtEncapChannel self, eBool enable);
eBool Tha60210012EncapChannelScrambleIsEnabled(AtEncapChannel self);
eBool Tha60210012DecapChannelScrambleIsEnabled(AtEncapChannel self);
eAtRet Tha60210012EopEncapChannelFlowBind(AtEncapChannel self, AtEthFlow flow);
eAtRet Tha60210012EopEncapChannelQueueEnable(AtChannel self, eBool enable);
eAtRet Tha60210012EncapChannelBandwidthUpdate(AtEncapChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012ENCAPCHANNEL_H_ */


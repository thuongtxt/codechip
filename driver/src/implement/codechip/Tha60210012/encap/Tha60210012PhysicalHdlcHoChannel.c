/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60210012PhysicalHdlcHoChannel.c
 *
 * Created Date: Apr 7, 2016
 *
 * Description : 60210012 physical HDLC (High Path) channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../cla/Tha60210012ModuleCla.h"
#include "../eth/Tha60210012ModuleEthFlowLookupReg.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "Tha60210012ModuleEncap.h"
#include "Tha60210012PhysicalHdlcChannel.h"
#include "Tha60210012PhysicalHdlcChannelInternal.h"
#include "Tha60210012ModuleDecapParserReg.h"
#include "Tha60210012PhysicalEncapReg.h"
#include "Tha60210012PmcInternalReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mDefaultOffset(self) Tha60210012PhysicalHdlcChannelDefaultOffset((Tha60210012PhysicalHdlcChannel)self)
#define mThis(self) ((Tha60210012PhysicalHdlcHoChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods              m_AtChannelOverride;
static tAtHdlcChannelMethods          m_AtHdlcChannelOverride;
static tAtEncapChannelMethods         m_AtEncapChannelOverride;
static tTha60210012PhysicalHdlcChannelMethods m_Tha60210012PhysicalHdlcChannelOverride;

/* Save super implementation */
static const tAtChannelMethods      *m_AtChannelMethods = NULL;
static const tAtHdlcChannelMethods  *m_AtHdlcChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 LinkLookupOffset(Tha60210012PhysicalHdlcChannel physicalChannel)
    {
    uint32 baseAddress = cThaModuleEthFlowLookupBaseAddress;
    uint8 sliceId = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)physicalChannel);

    return (baseAddress + AtChannelHwIdGet((AtChannel)physicalChannel) + (uint32)(sliceId * 64));
    }

static uint32 SliceOffset(Tha60210012PhysicalHdlcChannel self)
    {
    uint32 slice = (uint32)ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)self);

    return (uint32)(0x1000 * slice);
    }

static eAtRet EncapTypeSet(Tha60210012PhysicalHdlcChannel self, uint8 encapType)
    {
    uint32 regAddress = Tha60210012PhysicalHdlcChannelDecapBaseAddress(self) +
                        cAf6Reg_upen_cidcfg_HoCfg_Base + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_upen_cidhocfg_enc_typ_, encapType);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

static uint32 EncapBaseAddress(Tha60210012PhysicalHdlcChannel self)
    {
    AtUnused(self);
    return Tha60210012ModuleEncapHoBaseAddress();
    }

static uint32 DecapBaseAddress(Tha60210012PhysicalHdlcChannel self)
    {
    AtUnused(self);
    return Tha60210012ModuleDecapHoBaseAddress();
    }

static uint32 HoLinkLookupReg(Tha60210012PhysicalHdlcChannel self)
    {
    if (Tha60210012ModuleEncapRegistersNeedShiftUp((Tha60210012ModuleEncap)AtChannelModuleGet((AtChannel)self)))
        return 0x07000;

    return cAf6Reg_holink_lup_table_Base;
    }

static eAtRet LinkLookupSet(Tha60210012PhysicalHdlcChannel self, AtHdlcLink physicalLink)
    {
    uint32 regAddress = HoLinkLookupReg(self) + LinkLookupOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_holink_lup_table_HDLC_Valid_, 0);/* Disable lookup */
    mRegFieldSet(regValue, cAf6_holink_lup_table_HDLC_LinkID_, AtChannelHwIdGet((AtChannel)physicalLink));
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

static eAtRet LinkLookupEnable(Tha60210012PhysicalHdlcChannel self, eBool enable)
    {
    uint32 regAddress = HoLinkLookupReg(self) + LinkLookupOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_holink_lup_table_HDLC_Valid_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

static eAtRet LinkLookupReset(Tha60210012PhysicalHdlcChannel self)
    {
    uint32 regAddress = HoLinkLookupReg(self) + LinkLookupOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_holink_lup_table_HDLC_Valid_, 0);/* Disable lookup */
    mRegFieldSet(regValue, cAf6_holink_lup_table_HDLC_LinkID_, 0xFFF); /* Reset ID */
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

static eAtRet Debug(AtChannel self)
    {
    uint32 address = HoLinkLookupReg((Tha60210012PhysicalHdlcChannel)self) +
                     LinkLookupOffset((Tha60210012PhysicalHdlcChannel)self);

    m_AtChannelMethods->Debug(self);

    ThaDeviceRegNameDisplay("Hi-order HDLC Link Lookup");
    ThaDeviceChannelRegValueDisplay(self, address, cAtModuleEncap);

    return cAtOk;
    }

static uint32 EncapControlAddress(Tha60210012PhysicalHdlcChannel self)
    {
    return Tha60210012PhysicalHdlcChannelEncapBaseAddress(self) + cAf6Reg_upenctr1_Ho_Base;
    }

static eAtRet HoScrambleEnableForStsNc(AtEncapChannel self, AtSdhChannel auVc, eBool enable)
    {
    uint8 numSts   = AtSdhChannelNumSts(auVc);
    uint8 startSts = AtSdhChannelSts1Get(auVc);
    uint8 hwSlice, hwSts, sts_i;
    uint32 regAddr, regValue, offset;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        ThaSdhChannelHwStsGet(auVc, cAtModuleEncap, (uint8)(startSts + sts_i), &hwSlice, &hwSts);

        offset   = SliceOffset((Tha60210012PhysicalHdlcChannel)self) + Tha60210012PhysicalHdlcChannelEncapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + hwSts;
        regAddr  = cAf6Reg_upen_map_sts_set_Base + offset;
        regValue = mChannelHwRead(self, regAddr, cAtModuleEncap);
        mRegFieldSet(regValue, cAf6_upen_mapf_icfgscramble_, mBoolToBin(enable));
        mChannelHwWrite(self, regAddr, regValue, cAtModuleEncap);
        }

    return cAtOk;
    }

static eAtRet ScrambleEnable(AtEncapChannel self, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 baseAddr;
    AtSdhChannel auVc = (AtSdhChannel)AtEncapChannelBoundPhyGet(self);
    eBool stsNcNeedSelect = Tha60210012ModuleEncapStsNcNeedSelect((AtModuleEncap)AtChannelModuleGet((AtChannel)self), auVc);

    /* Decap configuration */
    baseAddr = Tha60210012PhysicalHdlcChannelDecapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + cAf6Reg_upen_hdlc_Hocfg_Base;
    regAddr = baseAddr + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cAf6_upen_hdlc_Hocfg_cfg_scren_Mask, cAf6_upen_hdlc_Hocfg_cfg_scren_Shift, enable);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    /* With version that support select STS-NC and the channel is HO path
     * default disable scramble */
    if (stsNcNeedSelect)
        return HoScrambleEnableForStsNc(self, auVc, enable);

    /* Encap configuration */
    regAddr = EncapControlAddress((Tha60210012PhysicalHdlcChannel)self) + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mRegFieldSet(regVal, cAf6_upenctr1_scrable_en_, enable);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eBool ScrambleIsEnabled(AtEncapChannel self)
    {
    uint8 hwEnable;
    uint32 baseAddr = Tha60210012PhysicalHdlcChannelDecapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + cAf6Reg_upen_hdlc_Hocfg_Base;
    uint32 regAddr = baseAddr + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mFieldGet(regVal, cAf6_upen_hdlc_Hocfg_cfg_scren_Mask, cAf6_upen_hdlc_Hocfg_cfg_scren_Shift, uint8, &hwEnable);

    return hwEnable ? cAtTrue : cAtFalse;
    }

static eAtRet HoStsNcFcsModeSet(AtEncapChannel self, AtSdhChannel auVc, eAtHdlcFcsMode fcsMode)
    {
    uint8 numSts   = AtSdhChannelNumSts(auVc);
    uint8 startSts = AtSdhChannelSts1Get(auVc);
    uint8 hwSlice, hwSts, sts_i, hwFcsMode;
    uint32 regAddr, regValue, offset;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        ThaSdhChannelHwStsGet(auVc, cAtModuleEncap, (uint8)(startSts + sts_i), &hwSlice, &hwSts);

        offset   = SliceOffset((Tha60210012PhysicalHdlcChannel)self) + Tha60210012PhysicalHdlcChannelEncapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + hwSts;
        regAddr  = cAf6Reg_upen_map_sts_set_Base + offset;
        regValue = mChannelHwRead(self, regAddr, cAtModuleEncap);
        hwFcsMode = (fcsMode == cAtHdlcFcsModeFcs16) ? 1 : 0;
        mRegFieldSet(regValue, cAf6_upen_mapf_icfgcrc_, hwFcsMode);
        mChannelHwWrite(self, regAddr, regValue, cAtModuleEncap);
        }

    return cAtOk;
    }

static eAtRet FcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    uint32 baseAddr, regAddr, regVal;
    AtSdhChannel auVc = (AtSdhChannel)AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    eBool stsNcNeedSelect = Tha60210012ModuleEncapStsNcNeedSelect((AtModuleEncap)AtChannelModuleGet((AtChannel)self), auVc);

    if ((fcsMode != cAtHdlcFcsModeNoFcs) &&
        (fcsMode != cAtHdlcFcsModeFcs32) &&
        (fcsMode != cAtHdlcFcsModeFcs16))
        return cAtErrorModeNotSupport;

    /* With new FPGA that support STS NC, hardware do not support No FCS mode */
    if (stsNcNeedSelect && (fcsMode == cAtHdlcFcsModeNoFcs))
        return cAtErrorModeNotSupport;

    /* Decap configuration */
    baseAddr = Tha60210012PhysicalHdlcChannelDecapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + cAf6Reg_upen_hdlc_Hocfg_Base;
    regAddr = baseAddr + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cAf6_upen_hdlc_Hocfg_cfg_fcsmode_Mask, cAf6_upen_hdlc_Hocfg_cfg_fcsmode_Shift, (fcsMode == cAtHdlcFcsModeFcs32) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    /* Encap configuration */
    if (stsNcNeedSelect)
        return HoStsNcFcsModeSet((AtEncapChannel)self, auVc, fcsMode);

    return m_AtHdlcChannelMethods->FcsModeSet(self, fcsMode);
    }

static eAtHdlcFcsMode FcsModeGet(AtHdlcChannel self)
    {
    uint8 hwFcsMode;
    uint32 regAddr, regVal;
    AtSdhChannel auVc = (AtSdhChannel)AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    eBool stsNcNeedSelect = Tha60210012ModuleEncapStsNcNeedSelect((AtModuleEncap)AtChannelModuleGet((AtChannel)self), auVc);

    if (stsNcNeedSelect)
        {
        uint8 hwSlice, hwSts;
        uint32 offset;

        ThaSdhChannel2HwMasterStsId(auVc, cAtModuleEncap, &hwSlice, &hwSts);
        offset    = SliceOffset((Tha60210012PhysicalHdlcChannel)self) + Tha60210012PhysicalHdlcChannelEncapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + hwSts;
        regAddr   = cAf6Reg_upen_map_sts_set_Base + offset;
        regVal    = mChannelHwRead(self, regAddr, cAtModuleEncap);
        hwFcsMode = (uint8)mRegField(regVal, cAf6_upen_mapf_icfgcrc_);

        return (hwFcsMode == 1) ? cAtHdlcFcsModeFcs16 : cAtHdlcFcsModeFcs32;
        }

    return m_AtHdlcChannelMethods->FcsModeGet(self);
    }

static eAtRet StuffModeSet(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    uint32 regAddr, regVal;
    AtSdhChannel auVc = (AtSdhChannel)AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    eBool stsNcNeedSelect = Tha60210012ModuleEncapStsNcNeedSelect((AtModuleEncap)AtChannelModuleGet((AtChannel)self), auVc);

    if ((stuffMode != cAtHdlcStuffBit) &&
        (stuffMode != cAtHdlcStuffByte))
        return cAtErrorModeNotSupport;

    /* Encap configuration */
    /* Do nothing if version support STS NC and channel is HO path */
    if (stsNcNeedSelect)
        return cAtOk;

    regAddr = EncapControlAddress((Tha60210012PhysicalHdlcChannel)self) + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upenctr1_bit_stuff_, (stuffMode == cAtHdlcStuffBit) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eAtHdlcStuffMode StuffModeGet(AtHdlcChannel self)
    {
    /* With version that support select STS-NC and the channel is HO path
     * default hardware is stuff byte */
    AtSdhChannel auVc = (AtSdhChannel)AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    if (Tha60210012ModuleEncapStsNcNeedSelect((AtModuleEncap)AtChannelModuleGet((AtChannel)self), auVc))
        return cAtHdlcStuffByte;

    return m_AtHdlcChannelMethods->StuffModeGet(self);
    }

static uint32 ChannelCounterIdGet(Tha60210012PhysicalHdlcChannel self)
    {
    static const uint8 cNumSwStsInVc4_4C = 12;
    AtSdhChannel auVc = (AtSdhChannel)AtEncapChannelBoundPhyGet((AtEncapChannel)self);

    if (AtSdhChannelTypeGet(auVc) == cAtSdhChannelTypeVc4_4c)
        return (uint32)(AtSdhChannelSts1Get(auVc) / cNumSwStsInVc4_4C);

    return AtChannelHwIdGet((AtChannel)self);
    }

static uint32 PacketCounterOffset(Tha60210012PhysicalHdlcChannel self, eBool r2c)
    {
    uint8 slice = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)self);
    return ((uint32)(r2c * 256) + (uint32)(slice * 64) + ChannelCounterIdGet(self));
    }

static uint32 CounterBaseAddress(Tha60210012PhysicalHdlcChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return Tha60210012ModuleEthPMCInternalBaseAddress(ethModule);
    }

static uint32 TxCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 type)
    {
    uint32 offset     = PacketCounterOffset(self, r2c) + (cTotalCounter * 128)+ CounterBaseAddress(self);
    uint32 regAddress = (type == cPktCounter) ? cAf6Reg_hoencpktcnt_pen_Base : cAf6Reg_hoencbytecnt_pen_Base;
    return mChannelHwRead(self, regAddress + offset, cAtModuleEncap);
    }

static uint32 RxCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 type)
    {
    uint32 offset     = PacketCounterOffset(self, r2c) + (cTotalCounter * 128) + CounterBaseAddress(self);
    uint32 regAddress = (type == cPktCounter) ? cAf6Reg_hodecpktcnt_pen_Base : cAf6Reg_hodecbytecnt_pen_Base;
    return mChannelHwRead(self, regAddress + offset, cAtModuleEncap);
    }

static uint32 LinkCounterOffset(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset)
    {
    uint8 slice = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)self);
    return ((uint32)(r2c * 128) + (uint32)(counterOffset * 256) + (uint32)(slice * 64) + ChannelCounterIdGet(self));
    }

static uint32 TxLinkCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset)
    {
    uint32 offset = LinkCounterOffset(self, r2c, counterOffset) + CounterBaseAddress(self);
    return mChannelHwRead(self, cAf6Reg_hoencpkttypcnt_pen_Base + offset, cAtModuleEncap);
    }

static uint32 RxLinkCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset)
    {
    uint32 offset = LinkCounterOffset(self, r2c, counterOffset) + CounterBaseAddress(self);
    return mChannelHwRead(self, cAf6Reg_hodecpkttypcnt_pen_Base + offset, cAtModuleEncap);
    }

static uint32 ByteIdleCounterOffset(Tha60210012PhysicalHdlcChannel self, eBool r2c)
    {
    uint8 slice = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)self);

    return ((uint32)(r2c * 128) + (uint32)(slice * 64) + ChannelCounterIdGet(self));
    }

static uint32 TxByteIdleCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c)
    {
    uint32 offset = ByteIdleCounterOffset(self, r2c) + CounterBaseAddress(self);
    return mChannelHwRead(self, cAf6Reg_hoencidlebytecnt_pen_Base + offset, cAtModuleEncap);
    }

static uint32 RxByteIdleCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c)
    {
    uint32 offset = ByteIdleCounterOffset(self, r2c) + CounterBaseAddress(self);
    return mChannelHwRead(self, cAf6Reg_hodecidlebytecnt_pen_Base + offset, cAtModuleEncap);
    }

static uint32 TxGoodCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 type)
    {
    uint32 offset     = PacketCounterOffset(self, r2c) + CounterBaseAddress(self);
    uint32 regAddress = (type == cPktCounter) ? cAf6Reg_hoencpktcnt_pen_Base : cAf6Reg_hoencbytecnt_pen_Base;
    return mChannelHwRead(self, regAddress + offset, cAtModuleEncap);
    }

static uint32 RxGoodCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 type)
    {
    uint32 offset     = PacketCounterOffset(self, r2c) + CounterBaseAddress(self);
    uint32 regAddress = (type == cPktCounter) ? cAf6Reg_hodecpktcnt_pen_Base : cAf6Reg_hodecbytecnt_pen_Base;
    return mChannelHwRead(self, regAddress + offset, cAtModuleEncap);
    }

static eAtModuleEncapRet IdlePatternSet(AtHdlcChannel self, uint8 idlePattern)
    {
    eBool stsNcNeedSelect;
    uint32 regAddress, regValue, offset;
    AtSdhChannel auVc;
    uint8 hwMode = Tha60210012PhysicalHdlcChannelIdlePattern2HwMode(idlePattern);

    if (hwMode == cBit7_0)
        return cAtErrorModeNotSupport;

    if ((AtHdlcChannelStuffModeGet(self) == cAtHdlcStuffByte) && (idlePattern == 0xFF))
        return cAtErrorModeNotSupport;

    /* With version that support select STS-NC default hardware is idle 0x7E */
    auVc = (AtSdhChannel)AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    stsNcNeedSelect = Tha60210012ModuleEncapStsNcNeedSelect((AtModuleEncap)AtChannelModuleGet((AtChannel)self), auVc);
    if (stsNcNeedSelect)
        return cAtOk;

    offset = Tha60210012PhysicalHdlcChannelEncapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + mDefaultOffset(self);
    regAddress = cAf6Reg_upenctr1_Ho_Base + offset;
    regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);
    mRegFieldSet(regValue, cAf6_upenctr1_idle_mode_, hwMode);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

static uint8 IdlePatternGet(AtHdlcChannel self)
    {
    uint32 offset, regAddress, regValue;
    AtSdhChannel auVc = (AtSdhChannel)AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    eBool stsNcNeedSelect = Tha60210012ModuleEncapStsNcNeedSelect((AtModuleEncap)AtChannelModuleGet((AtChannel)self), auVc);

    if (stsNcNeedSelect)
        return 0x7E;

    offset = Tha60210012PhysicalHdlcChannelEncapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + mDefaultOffset(self);
    regAddress = cAf6Reg_upenctr1_Ho_Base + offset;
    regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    return (mRegField(regValue, cAf6_upenctr1_idle_mode_) == 1) ? 0xFF : 0x7E;
    }

static eAtRet RxAddressControlCompress(AtHdlcChannel self, eBool rxAddrCtrlCompress)
    {
    uint32 offset = mDefaultOffset(self) + Tha60210012ModuleDecapHoBaseAddress();
    uint32 regAddress = cAf6Reg_upen_cidcfg_HoCfg_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_upen_cidhocfg_addcfg_, mBoolToBin(rxAddrCtrlCompress));
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);
    return cAtOk;
    }

static eBool RxAddressControlIsCompressed(AtHdlcChannel self)
    {
    uint32 offset = mDefaultOffset(self) + Tha60210012ModuleDecapHoBaseAddress();
    uint32 regAddress = cAf6Reg_upen_cidcfg_HoCfg_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);
    return mRegField(regValue, cAf6_upen_cidhocfg_addcfg_) ? cAtTrue : cAtFalse;
    }

static eBool FcsCalculationModeIsSupported(eAtHdlcFcsCalculationMode mode)
    {
    if ((mode == cAtHdlcFcsCalculationModeLsb) ||
        (mode == cAtHdlcFcsCalculationModeMsb))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet FcsCalculationModeSet(AtHdlcChannel self, eAtHdlcFcsCalculationMode mode)
    {
    uint32 baseAddr, regAddr, regVal;

    if (!FcsCalculationModeIsSupported(mode))
        return cAtErrorModeNotSupport;

    /* Decap configuration */
    baseAddr = Tha60210012PhysicalHdlcChannelDecapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + cAf6Reg_upen_hdlc_Hocfg_Base;
    regAddr = baseAddr + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mRegFieldSet(regVal, cAf6_upen_hdlc_Hocfg_cfg_fcsmsb_, (mode == cAtHdlcFcsCalculationModeMsb) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eAtHdlcFcsCalculationMode FcsCalculationModeGet(AtHdlcChannel self)
    {
    uint8 hwFcsMode;
    uint32 regAddr, regVal, baseAddr;

    baseAddr = Tha60210012PhysicalHdlcChannelDecapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + cAf6Reg_upen_hdlc_Hocfg_Base;
    regAddr = baseAddr + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    hwFcsMode = (uint8)mRegField(regVal, cAf6_upen_hdlc_Hocfg_cfg_fcsmsb_);

    return (hwFcsMode == 1) ? cAtHdlcFcsCalculationModeMsb : cAtHdlcFcsCalculationModeLsb;
    }

static eAtRet FrameTypeSet(AtHdlcChannel self, uint8 frameType)
    {
    Tha60210012PhysicalHdlcChannel hdlcChannel = (Tha60210012PhysicalHdlcChannel)self;
    uint32 regAddress = Tha60210012PhysicalHdlcChannelDecapBaseAddress(hdlcChannel) +
                        cAf6Reg_upen_cidcfg_HoCfg_Base + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_upen_cidhocfg_frm_typ_, Tha60210012PhysicalHdlcChannelSw2HwFrameType(frameType));
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

static void OverrideAtChannel(AtHdlcChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, m_AtHdlcChannelMethods, sizeof(m_AtHdlcChannelOverride));

        mMethodOverride(m_AtHdlcChannelOverride, FcsModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, IdlePatternSet);
        mMethodOverride(m_AtHdlcChannelOverride, IdlePatternGet);
        mMethodOverride(m_AtHdlcChannelOverride, RxAddressControlCompress);
        mMethodOverride(m_AtHdlcChannelOverride, RxAddressControlIsCompressed);
        mMethodOverride(m_AtHdlcChannelOverride, FcsCalculationModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsCalculationModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, FrameTypeSet);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void OverrideAtEncapChannel(AtHdlcChannel self)
    {
    AtEncapChannel encap = (AtEncapChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapChannelOverride, mMethodsGet(encap), sizeof(m_AtEncapChannelOverride));

        mMethodOverride(m_AtEncapChannelOverride, ScrambleIsEnabled);
        mMethodOverride(m_AtEncapChannelOverride, ScrambleEnable);
        }

    mMethodsSet(encap, &m_AtEncapChannelOverride);
    }

static void OverrideTha60210012PhysicalHdlcChannel(AtHdlcChannel self)
    {
    Tha60210012PhysicalHdlcChannel channel = (Tha60210012PhysicalHdlcChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012PhysicalHdlcChannelOverride, mMethodsGet(channel), sizeof(m_Tha60210012PhysicalHdlcChannelOverride));

        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, EncapTypeSet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, EncapBaseAddress);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, DecapBaseAddress);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, LinkLookupSet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, LinkLookupEnable);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, LinkLookupReset);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, EncapControlAddress);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, SliceOffset);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, TxLinkCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, RxLinkCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, TxCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, RxCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, TxByteIdleCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, RxByteIdleCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, TxGoodCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, RxGoodCounterGet);
        }

    mMethodsSet(channel, &m_Tha60210012PhysicalHdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtChannel(self);
    OverrideAtHdlcChannel(self);
    OverrideAtEncapChannel(self);
    OverrideTha60210012PhysicalHdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012PhysicalHdlcHoChannel);
    }

AtHdlcChannel Tha60210012PhysicalHdlcHoChannelObjectInit(AtHdlcChannel self, uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012PhysicalHdlcChannelObjectInit(self, channelId, logicalChannel, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel Tha60210012PhysicalHdlcHoChannelNew(uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012PhysicalHdlcHoChannelObjectInit(newChannel, channelId, logicalChannel, module);
    }

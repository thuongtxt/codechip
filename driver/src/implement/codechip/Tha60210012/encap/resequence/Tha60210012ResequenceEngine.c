/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60210012ResequenceEngine.c
 *
 * Created Date: May 17, 2016
 *
 * Description : Re-sequence engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEthFlow.h"
#include "../../../../../generic/man/AtDeviceInternal.h"
#include "../../../../../generic/common/AtChannelInternal.h"
#include "../../../../../generic/encap/resequence/AtResequenceManagerInternal.h"
#include "../../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../../ppp/Tha60210012MpBundle.h"
#include "Tha60210012ResequenceReg.h"
#include "Tha60210012ResequenceManager.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxRangQueueId            7

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210012ResequenceEngine *)self)
#define mApiCheck(apiCheck)        \
    do                             \
        {                          \
        eAtRet ret = apiCheck;     \
        if (ret != cAtOk)          \
            return ret;            \
        } while(0)

#define cTha60210012ResequenceInitState    0
#define cTha60210012ResequenceRestartState 1

#define cTha6021CounterReoderInputFragmentCount        0
#define cTha6021CounterReoderInputExpectedFragCount    1
#define cTha6021CounterReoderTimeout                   2
#define cTha6021CounterReoderInputWithOverSeqWindow    3
#define cTha6021CounterReoderOutputGoodFrames          4
#define cTha6021CounterReoderOutputGoodPackets         5
#define cTha6021CounterReoderViolationMSRU             6
#define cTha6021CounterReoderViolationMRRU             7
#define cTha6021CounterReoderReceivedDiscard           8

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012ResequenceEngine
    {
    tAtResequenceEngine super;
    }tTha60210012ResequenceEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtResequenceEngineMethods  m_AtResequenceEngineOverride;

/* Super implementation */
static const tAtResequenceEngineMethods  *m_AtResequenceEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtResequenceManager Manager(AtResequenceEngine self)
    {
    return AtResequenceEngineManagerGet(self);
    }

static uint32 FlowControlOffset(AtResequenceEngine self)
    {
    AtEthFlow flow = AtResequenceEngineEthFlowGet(self);
    return (AtChannelHwIdGet((AtChannel)flow) + AtResequenceManagerBaseAddress(Manager(self)));
    }

static uint32 ResequenceOffset(AtResequenceEngine self)
    {
    return (AtResequenceEngineIdGet(self) + AtResequenceManagerBaseAddress(Manager(self)));
    }

static eAtRet Enable(AtResequenceEngine self, eBool enabled)
    {
    uint32 regAddr, regValue;

    /* Enable this select */
    regAddr = cAf6Reg_upen_cfgrsqc_Base + ResequenceOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    mRegFieldSet(regValue, cAf6_upen_cfgrsqc_RsqEnable_, mBoolToBin(enabled));
    AtResequenceEngineWrite(self, regAddr, regValue);

    /* Enable select engine */
    regAddr = cAf6Reg_upen_cfgrslk_Base + FlowControlOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    mRegFieldSet(regValue, cAf6_upen_cfgrslk_IGRSRsEna_, mBoolToBin(enabled));
    AtResequenceEngineWrite(self, regAddr, regValue);

    return cAtOk;
    }

static eBool IsEnabled(AtResequenceEngine self)
    {
    uint32 regAddr, regValue;

    regAddr = cAf6Reg_upen_cfgrslk_Base + FlowControlOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    return mRegField(regValue, cAf6_upen_cfgrslk_IGRSRsEna_) ? cAtTrue : cAtFalse;
    }

static eAtRet OutputQueueSet(AtResequenceEngine self, uint32 queueId)
    {
    uint32 regAddr, regValue;

    if (queueId > cMaxRangQueueId)
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_upen_cfgrslk_Base + FlowControlOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    mRegFieldSet(regValue, cAf6_upen_cfgrslk_IGRSoQueId_, queueId);
    AtResequenceEngineWrite(self, regAddr, regValue);

    return cAtOk;
    }

static uint32 OutputQueueGet(AtResequenceEngine self)
    {
    uint32 regAddr, regValue;

    regAddr = cAf6Reg_upen_cfgrslk_Base + FlowControlOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    return mRegField(regValue, cAf6_upen_cfgrslk_IGRSoQueId_);
    }

static eAtRet OutputQueueEnable(AtResequenceEngine self, eBool queueEnabled)
    {
    uint32 regAddr, regValue;

    regAddr = cAf6Reg_upen_cfgrslk_Base + FlowControlOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    mRegFieldSet(regValue, cAf6_upen_cfgrslk_IGRSoQueEna_, mBoolToBin(queueEnabled));
    AtResequenceEngineWrite(self, regAddr, regValue);

    return cAtOk;
    }

static eBool OutputQueueIsEnabled(AtResequenceEngine self)
    {
    uint32 regAddr, regValue;

    regAddr = cAf6Reg_upen_cfgrslk_Base + FlowControlOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    return mRegField(regValue, cAf6_upen_cfgrslk_IGRSoQueEna_) ? cAtTrue : cAtFalse;
    }

static eAtRet EthFlowSet(AtResequenceEngine self, AtEthFlow flow)
    {
    uint32 regAddr, regValue;

    /* Updated database  */
    m_AtResequenceEngineMethods->EthFlowSet(self, flow);

    regAddr = cAf6Reg_upen_cfgrslk_Base + FlowControlOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    mRegFieldSet(regValue, cAf6_upen_cfgrslk_IGRSRsId_, AtResequenceEngineIdGet(self));
    AtResequenceEngineWrite(self, regAddr, regValue);

    return cAtOk;
    }

static eAtRet TimeoutSet(AtResequenceEngine self, uint32 timeoutMs)
    {
    uint32 regAddr, regValue;

    regAddr = cAf6Reg_upen_cfgrsqc_Base + ResequenceOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    mRegFieldSet(regValue, cAf6_upen_cfgrsqc_RsqTimeout_, timeoutMs);
    AtResequenceEngineWrite(self, regAddr, regValue);

    return cAtOk;
    }

static uint32 TimeoutGet(AtResequenceEngine self)
    {
    uint32 regAddr, regValue;

    regAddr = cAf6Reg_upen_cfgrsqc_Base + ResequenceOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    return mRegField(regValue, cAf6_upen_cfgrsqc_RsqTimeout_);
    }

static eAtRet MsruSet(AtResequenceEngine self, uint32 msru)
    {
    uint32 regAddr, regValue;

    regAddr = cAf6Reg_upen_cfgpkac_Base + ResequenceOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    mRegFieldSet(regValue, cAf6_upen_cfgpkac_PkaMsru_, msru);
    AtResequenceEngineWrite(self, regAddr, regValue);

    return cAtOk;
    }

static uint32 MsruGet(AtResequenceEngine self)
    {
    uint32 regAddr, regValue;

    regAddr = cAf6Reg_upen_cfgpkac_Base + ResequenceOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    return mRegField(regValue, cAf6_upen_cfgpkac_PkaMsru_);
    }

static eAtRet MrruSet(AtResequenceEngine self, uint32 mrru)
    {
    uint32 regAddr, regValue;

    regAddr = cAf6Reg_upen_cfgpkac_Base + ResequenceOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    mRegFieldSet(regValue, cAf6_upen_cfgpkac_PkaMrru_, mrru);
    AtResequenceEngineWrite(self, regAddr, regValue);

    return cAtOk;
    }

static uint32 MrruGet(AtResequenceEngine self)
    {
    uint32 regAddr, regValue;

    regAddr = cAf6Reg_upen_cfgpkac_Base + ResequenceOffset(self);
    regValue = AtResequenceEngineRead(self, regAddr);
    return mRegField(regValue, cAf6_upen_cfgpkac_PkaMrru_);
    }

static eAtRet DeActivate(AtResequenceEngine self)
    {
    eAtRet ret;

    /* Re-start re-sequence engine */
    ret  = AtResequenceEngineEnable(self, cAtFalse);
    ret |= AtResequenceEngineOutputQueueEnable(self, cAtFalse);

    return ret;
    }

static uint32 ThesholdSpace(uint32 threshold)
    {
    /*
     * {0}: 4K Sequence Buffer space, when BT (0-3072)
     * {1}: 16K Sequence Buffer space, when BT (3072-12288)
     * {2}: 64K Sequence Buffer space (12288-49192) (unused now)
     */
    if (threshold < 3072)
        return 0;

    return 1;
    }

static eBool HardwareEngineIsReady(AtResequenceEngine self)
    {
    uint32 regAddress, regValue, hwRequenceState;
    AtDevice device = AtResequenceManagerDevice(AtResequenceEngineManagerGet(self));

    regAddress = cAf6Reg_upen_cfgrsqs_Base + ResequenceOffset(self);
    regValue   = AtResequenceEngineRead(self, regAddress);
    hwRequenceState = mRegField(regValue, cAf6_upen_cfgrsqs_RsqFsm_);

    if (AtDeviceIsSimulated(device))
        return cAtTrue;

    if ((hwRequenceState == cTha60210012ResequenceInitState) ||
        (hwRequenceState == cTha60210012ResequenceRestartState))
        return cAtTrue;

    return cAtFalse;
    }

static const char *HwFsmStateToString(uint8 fsmState)
    {
    switch (fsmState)
        {
        case 0: return  "FSM IDLE";
        case 1: return  "FSM READY";
        case 4: return  "Search A_State";
        case 5: return  "Search B_State";
        case 8: return  "Sync A_State";
        case 9: return  "Sync B_State";
        case 10: return "Engine Init reset state";
        case 11: return "CPU Init reset state";
        case 12: return "Engine reset state";
        case 13: return "Cpu reset state";
        case 14: return "Engine reset done";
        case 15: return "CPU reset done;";
        default:
            {
            AtPrintc(cSevNormal, "Hardware state: %u \r\n", fsmState);
            return "None";
            }
        }
    }

static uint32 Resolution(uint32 threshold)
    {
    /* 0x0: Resolution is 1  , when BT (0-768)
     * 0x1: Resolution is 4  , when BT (768-3072)
     * 0x2: Resolution is 16 , when BT (3072-12288)
     * 0x3: Resolution is 64 , when BT (12288-49192)
     */
    if (threshold < 768)   return 1;
    if (threshold < 3072)  return 4;
    if (threshold < 12288) return 16;

    return 64;
    }

static uint32 Resolution2Hw(uint32 resolution)
    {
    /* 0x0: Resolution is 1  , when BT (0-768)
     * 0x1: Resolution is 4  , when BT (768-3072)
     * 0x2: Resolution is 16 , when BT (3072-12288)
     * 0x3: Resolution is 64 , when BT (12288-49192)
     */
    if (resolution == 1)  return 0;
    if (resolution == 4)  return 1;
    if (resolution == 16) return 2;

    return 3;
    }

static uint32 Threshold2Hw(uint32 threshold, uint32 resolution)
    {
    return (threshold / resolution);
    }

static void BufferSpaceSet(AtResequenceEngine self, uint32 threshold)
    {
    uint32 regAddres = cAf6Reg_upen_cfgrfsc_Base + ResequenceOffset(self);
    uint32 regValue  = AtResequenceEngineRead(self, regAddres);

    mRegFieldSet(regValue, cAf6_upen_cfgrfsc_RsqRfsMd_, ThesholdSpace(threshold));

    AtResequenceEngineWrite(self, regAddres, regValue);
    }

static eAtRet DefaultSet(AtResequenceEngine self)
    {
    uint32 regAddres = cAf6Reg_upen_cfgrsqc_Base + ResequenceOffset(self);
    uint32 regValue  = AtResequenceEngineRead(self, regAddres);
    const uint8 cEnableAllLostDetectAlgorithm = 0;

    mRegFieldSet(regValue, cAf6_upen_cfgrsqc_RsqLDADis_, cEnableAllLostDetectAlgorithm);
    AtResequenceEngineWrite(self, regAddres, regValue);

    return cAtOk;
    }

static eAtRet Init(AtResequenceEngine self)
    {
    eAtRet ret = m_AtResequenceEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet(self);
    }

static void OverrideAtResequenceEngine(AtResequenceEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtResequenceEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtResequenceEngineOverride, mMethodsGet(self), sizeof(tAtResequenceEngineMethods));

        mMethodOverride(m_AtResequenceEngineOverride, Enable);
        mMethodOverride(m_AtResequenceEngineOverride, IsEnabled);
        mMethodOverride(m_AtResequenceEngineOverride, OutputQueueSet);
        mMethodOverride(m_AtResequenceEngineOverride, OutputQueueGet);
        mMethodOverride(m_AtResequenceEngineOverride, OutputQueueEnable);
        mMethodOverride(m_AtResequenceEngineOverride, OutputQueueIsEnabled);
        mMethodOverride(m_AtResequenceEngineOverride, EthFlowSet);
        mMethodOverride(m_AtResequenceEngineOverride, TimeoutSet);
        mMethodOverride(m_AtResequenceEngineOverride, TimeoutGet);
        mMethodOverride(m_AtResequenceEngineOverride, MsruSet);
        mMethodOverride(m_AtResequenceEngineOverride, MsruGet);
        mMethodOverride(m_AtResequenceEngineOverride, MrruSet);
        mMethodOverride(m_AtResequenceEngineOverride, MrruGet);
        mMethodOverride(m_AtResequenceEngineOverride, DeActivate);
        mMethodOverride(m_AtResequenceEngineOverride, HardwareEngineIsReady);
        mMethodOverride(m_AtResequenceEngineOverride, Init);
        }

    mMethodsSet(self, &m_AtResequenceEngineOverride);
    }

static void Override(AtResequenceEngine self)
    {
    OverrideAtResequenceEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ResequenceEngine);
    }

static AtResequenceEngine ObjectInit(AtResequenceEngine self, AtResequenceManager manager, uint32 engineId)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtResequenceEngineObjectInit(self, manager, engineId) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtResequenceEngine Tha60210012ResequenceEngineNew(AtResequenceManager manager, uint32 engineId)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtResequenceEngine engine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (engine == NULL)
        return NULL;

    return ObjectInit(engine, manager, engineId);
    }

void Tha60210012ResequenceThresholdSet(AtResequenceEngine self, uint32 threshold)
    {
    uint32 resolution = Resolution(threshold);
    uint32 regAddress = cAf6Reg_upen_cfgrsqc_Base + ResequenceOffset(self);
    uint32 regValue   = AtResequenceEngineRead(self, regAddress);

    mRegFieldSet(regValue, cAf6_upen_cfgrsqc_RsqResThsh_, Resolution2Hw(resolution));
    mRegFieldSet(regValue, cAf6_upen_cfgrsqc_RsqSeqThsh_, Threshold2Hw(threshold, resolution));

    AtResequenceEngineWrite(self, regAddress, regValue);

    BufferSpaceSet(self, threshold);
    }

void Tha60210012ResequenceFsmStatusDisplay(AtResequenceEngine self)
    {
    uint32 regAddress, regValues;

    AtPrintc(cSevInfo, "                     FSM Status:\r\n");
    regAddress = cAf6Reg_upen_cfgrsqs_Base + ResequenceOffset(self);
    regValues  = AtResequenceEngineRead(self, regAddress);
    AtPrintc(cSevNormal, "Expected sequence:         %u\r\n", mRegField(regValues, cAf6_upen_cfgrsqs_RsqExpt_));
    AtPrintc(cSevNormal, "Counter bad/good sequence: %u\r\n", mRegField(regValues, cAf6_upen_cfgrsqs_RsqCnt_));
    AtPrintc(cSevNormal, "FSM of Engine:             %s\r\n", HwFsmStateToString((uint8)mRegField(regValues, cAf6_upen_cfgrsqs_RsqFsm_)));
    }

void Tha60210012ResequenceStickyDisplay(AtResequenceEngine self)
    {
    uint32 regAddress, regValues, offset;
    eBool readToClear = cAtTrue;

    AtPrintc(cSevInfo, "                     Re-order sticky:\r\n");
    offset     = ResequenceOffset(self) + (uint32)(readToClear * 0x1000);
    regAddress = cAf6Reg_upen_cfgstky_Base + offset;
    regValues  = AtResequenceEngineRead(self, regAddress);

    Tha6021DebugPrintStart(NULL);
    Tha6021DebugPrintErrorBit(NULL, "Receive Discard Sequence or Disable Re-order", regValues, cBit0);
    Tha6021DebugPrintErrorBit(NULL, "Receive Early Sequence or M-algoritm", regValues, cBit1);
    Tha6021DebugPrintErrorBit(NULL, "Receive Violation Window Size", regValues, cBit2);
    Tha6021DebugPrintErrorBit(NULL, "Receive Unexpected Sequence", regValues, cBit3);
    Tha6021DebugPrintErrorBit(NULL, "Receive Late Sequence", regValues, cBit4);
    Tha6021DebugPrintErrorBit(NULL, "Scan With Exptected Timeout", regValues, cBit5);
    Tha6021DebugPrintErrorBit(NULL, "S0 timeout", regValues, cBit6);
    Tha6021DebugPrintErrorBit(NULL, "Re-Order Reset State", regValues, cBit7);
    Tha6021DebugPrintErrorBit(NULL, "Receive Duplicate Sequence", regValues, cBit8);
    Tha6021DebugPrintErrorBit(NULL, "Packet Assemble Sequence Error", regValues, cBit9);
    Tha6021DebugPrintErrorBit(NULL, "Packet Block Error", regValues, cBit10);
    Tha6021DebugPrintErrorBit(NULL, "Packet MSRU Error", regValues, cBit11);
    Tha6021DebugPrintErrorBit(NULL, "Packet MRRU Error", regValues, cBit12);
    Tha6021DebugPrintErrorBit(NULL, "Packet Memory Check Error", regValues, cBit13);
    Tha6021DebugPrintErrorBit(NULL, "Fifo Full", regValues, cBit14);
    Tha6021DebugPrintErrorBit(NULL, "Fifo With Empty Blocks", regValues, cBit15);
    Tha6021DebugPrintErrorBit(NULL, "Fragment Have Lenght Eque Zero and Silence Drop", regValues, cBit16);
    Tha6021DebugPrintErrorBit(NULL, "Fragment Declare Drop at Other Blocks Function", regValues, cBit17);

    Tha6021DebugPrintBitFieldVal(NULL, "Receive Expected Sequence", regValues, cBit20, 20);
    Tha6021DebugPrintBitFieldVal(NULL, "CPU Read", regValues, cBit21, 21);
    Tha6021DebugPrintBitFieldVal(NULL, "CPU Write", regValues, cBit22, 22);
    Tha6021DebugPrintStop(NULL);
    }

void Tha60210012ResequenceCounterDisplay(AtResequenceEngine self)
    {
    uint32 regAddress, offset;
    eBool readToClear = cAtTrue;

    AtPrintc(cSevInfo, "                     Re-oder Counter:\r\n");
    offset     = AtResequenceManagerBaseAddress(Manager(self)) + (uint32)(readToClear * 0x10000) +
                 (uint32)(AtResequenceEngineIdGet(self) * 0x10);

    regAddress = cAf6Reg_upen_cfgcntw_Base + offset + cTha6021CounterReoderInputFragmentCount;
    AtPrintc(cSevNormal, "Received total fragment:    %u\r\n", AtResequenceEngineRead(self, regAddress));

    regAddress = cAf6Reg_upen_cfgcntw_Base + offset + cTha6021CounterReoderInputExpectedFragCount;
    AtPrintc(cSevNormal, "Received Expected sequence: %u\r\n", AtResequenceEngineRead(self, regAddress));

    regAddress = cAf6Reg_upen_cfgcntw_Base + offset + cTha6021CounterReoderTimeout;
    AtPrintc(cSevNormal, "Received Re-oder timeout:   %u\r\n", AtResequenceEngineRead(self, regAddress));

    regAddress = cAf6Reg_upen_cfgcntw_Base + offset + cTha6021CounterReoderInputWithOverSeqWindow;
    AtPrintc(cSevNormal, "Received with Over Sequence:%u\r\n", AtResequenceEngineRead(self, regAddress));

    regAddress = cAf6Reg_upen_cfgcntw_Base + offset + cTha6021CounterReoderOutputGoodFrames;
    AtPrintc(cSevNormal, "Output good frames:         %u\r\n", AtResequenceEngineRead(self, regAddress));

    regAddress = cAf6Reg_upen_cfgcntw_Base + offset + cTha6021CounterReoderOutputGoodPackets;
    AtPrintc(cSevNormal, "Output good packets:        %u\r\n", AtResequenceEngineRead(self, regAddress));

    regAddress = cAf6Reg_upen_cfgcntw_Base + offset + cTha6021CounterReoderReceivedDiscard;
    AtPrintc(cSevNormal, "Violation MSRU Packet:      %u\r\n", AtResequenceEngineRead(self, regAddress));

    regAddress = cAf6Reg_upen_cfgcntw_Base + offset + cTha6021CounterReoderViolationMRRU;
    AtPrintc(cSevNormal, "Violation MRRU Packet:      %u\r\n", AtResequenceEngineRead(self, regAddress));

    regAddress = cAf6Reg_upen_cfgcntw_Base + offset + cTha6021CounterReoderReceivedDiscard;
    AtPrintc(cSevNormal, "Discard packets:            %u\r\n", AtResequenceEngineRead(self, regAddress));
    }

eAtRet Tha60210012ResequenceNumLinksSet(AtResequenceEngine self, uint32 numLinks)
    {
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_upen_cfgrsqc_Base + ResequenceOffset(self);
    regValue   = AtResequenceEngineRead(self, regAddress);
    mRegFieldSet(regValue, cAf6_upen_cfgrsqc_RsqNumLink_, numLinks - 1);

    AtResequenceEngineWrite(self, regAddress, regValue);
    return cAtOk;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60210012ResequenceManager.c
 *
 * Created Date: May 17, 2016
 *
 * Description : Re-sequence engine management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/man/AtDeviceInternal.h"
#include "../../../../default/encap/resequence/ThaResequenceManagerInternal.h"
#include "../../../../default/man/ThaDevice.h"
#include "Tha60210012ResequenceReg.h"
#include "Tha60210012ResequenceManager.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha60210012ResequenceManager *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012ResequenceManager
    {
    tThaResequenceManager super;
    }tTha60210012ResequenceManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtResequenceManagerMethods  m_AtResequenceManagerOverride;

/* Save super implementation */
static const tAtResequenceManagerMethods  *m_AtResequenceManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtResequenceEngine EngineObjectCreate(AtResequenceManager self, uint32 engineId)
    {
    return Tha60210012ResequenceEngineNew(self, engineId);
    }

static uint32 BaseAddress(AtResequenceManager self)
    {
    AtUnused(self);
    return cResequenceEngineBaseAddress;
    }

static uint32 Read(AtResequenceManager self, uint32 regAddr)
    {
    AtIpCore ipCore = AtDeviceIpCoreGet(AtResequenceManagerDevice(self), 0);
    AtHal hal = AtIpCoreHalGet(ipCore);
    return AtHalRead(hal, regAddr);
    }

static void Write(AtResequenceManager self, uint32 regAddr, uint32 regValue)
    {
    AtIpCore ipCore = AtDeviceIpCoreGet(AtResequenceManagerDevice(self), 0);
    AtHal hal = AtIpCoreHalGet(ipCore);
    AtHalWrite(hal, regAddr, regValue);
    }

static void HwFlush(AtResequenceManager self)
    {
    AtDevice device = AtResequenceManagerDevice(self);

    ThaDeviceMemoryFlush((ThaDevice)device, 0x1902000, 0x1902fff, 0x0);
    }
    
static eAtRet HardWareInitDone(AtResequenceManager self)
    {
    uint32 elapseTime;
    tAtOsalCurTime curTime;
    tAtOsalCurTime startTime;
    uint32 regAddress, regValue;
    AtOsal osal = AtSharedDriverOsalGet();
    static const uint16 cHardwareInitDone = 1000; /* ms */

    regAddress = cAf6Reg_upen_igrsctl_0_Base + BaseAddress(self);
    Write(self, regAddress, 0);
    HwFlush(self);
    regValue   = Read(self, regAddress);
    if (mRegField(regValue, cAf6_upen_igrsctl_0_RsqActEn_) == 0)
        {
        mRegFieldSet(regValue, cAf6_upen_igrsctl_0_RsqActEn_, 1);
        mRegFieldSet(regValue, cAf6_upen_igrsctl_0_RsqEnqEn_, 1);
        mRegFieldSet(regValue, cAf6_upen_igrsctl_0_RsqIntEn_, 1);
        Write(self, regAddress, regValue);
        }

    /* Timeout waiting for it */
    elapseTime = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < cHardwareInitDone)
        {
        regAddress = cAf6Reg_upen_igrssta_0_Base + BaseAddress(self);
        regValue = Read(self, regAddress);
        
        if (AtDeviceIsSimulated(AtResequenceManagerDevice(self)))
	        return cAtOk;
	        
        if (mRegField(regValue, cAf6_upen_igrssta_0_RsqInitdone_) == 1)
            return cAtOk;

        /* Calculate elapse time */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtDeviceLog(AtResequenceManagerDevice(self), cAtLogLevelCritical, AtSourceLocation,
                "Init Re-sequence fail regAddress = 0x%x, timeout=%u", regAddress, elapseTime);
    return cAtErrorIndrAcsTimeOut;
    }

static eAtModuleEncapRet Init(AtResequenceManager self)
    {
    if (AtDeviceAccessible(AtResequenceManagerDevice(self)))
        {
        eAtRet ret = HardWareInitDone(self);
        if (ret != cAtOk)
            return ret;
        }

    return m_AtResequenceManagerMethods->Init(self);
    }

static uint32 NumEngines(AtResequenceManager self)
    {
    AtUnused(self);
    return 1024;
    }

static void DebugVersionPrint(AtResequenceManager self)
    {
    uint32 regAddr, regValue;

    regAddr  = cAf6Reg_upen_igrsver_Base + AtResequenceManagerBaseAddress(self);
    regValue = Read(self, regAddr);

    AtPrintc(cSevInfo, "* IGRS version \r\n");
    AtPrintc(cSevNormal, "    Engine ID            = "); AtPrintc(cSevInfo, "%u\r\n", mRegField(regValue, cAf6_upen_igrsver_IGRSEngId_));
    AtPrintc(cSevNormal, "    Version ID           = "); AtPrintc(cSevInfo, "%u\r\n", mRegField(regValue, cAf6_upen_igrsver_IGRSVerId_));
    AtPrintc(cSevNormal, "    Editor ID            = "); AtPrintc(cSevInfo, "%u\r\n", mRegField(regValue, cAf6_upen_igrsver_IGRSEditId_));
    AtPrintc(cSevNormal, "    Months               = "); AtPrintc(cSevInfo, "%u\r\n", mRegField(regValue, cAf6_upen_igrsver_IGRSMonId_));
    AtPrintc(cSevNormal, "    Days                 = "); AtPrintc(cSevInfo, "%u\r\n", mRegField(regValue, cAf6_upen_igrsver_IGRSDayId_));
    }

static void DebugIGSRParamPrint(AtResequenceManager self)
    {
    uint32 regAddr, regValue;

    regAddr  = cAf6Reg_upen_igrsparam_Base + AtResequenceManagerBaseAddress(self);
    regValue = Read(self, regAddr);

    AtPrintc(cSevInfo, "* IGRS param \r\n");
    AtPrintc(cSevNormal, "    Fragment Block bit   = "); AtPrintc(cSevInfo, "%u\r\n", mRegField(regValue, cAf6_upen_igrsparam_FBlk_bit_));
    AtPrintc(cSevNormal, "    Queue Block bit      = "); AtPrintc(cSevInfo, "%u\r\n", mRegField(regValue, cAf6_upen_igrsparam_QBlk_bit_));
    AtPrintc(cSevNormal, "    Bitmap Block bit     = "); AtPrintc(cSevInfo, "%u\r\n", mRegField(regValue, cAf6_upen_igrsparam_BBlk_bit_));
    AtPrintc(cSevNormal, "    Bundle bit           = "); AtPrintc(cSevInfo, "%u\r\n", mRegField(regValue, cAf6_upen_igrsparam_Bund_bit_));
    AtPrintc(cSevNormal, "    Link bit             = "); AtPrintc(cSevInfo, "%u\r\n", mRegField(regValue, cAf6_upen_igrsparam_Link_bit_));
    AtPrintc(cSevNormal, "    Sequence space bit   = "); AtPrintc(cSevInfo, "%u\r\n", mRegField(regValue, cAf6_upen_igrsparam_Space_bit_));
    AtPrintc(cSevNormal, "    Re-Order Engine bit  = "); AtPrintc(cSevInfo, "%u\r\n", mRegField(regValue, cAf6_upen_igrsparam_RFlow_bit_));
    AtPrintc(cSevNormal, "    Ethernet flow bit    = "); AtPrintc(cSevInfo, "%u\r\n", mRegField(regValue, cAf6_upen_igrsparam_EFlow_bit_));
    }

static void Debug(AtResequenceManager self)
    {
    m_AtResequenceManagerMethods->Debug(self);
    DebugVersionPrint(self);
    DebugIGSRParamPrint(self);
    }

static void OverrideAtResequenceManager(AtResequenceManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtResequenceManagerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtResequenceManagerOverride, mMethodsGet(self), sizeof(tAtResequenceManagerMethods));

        mMethodOverride(m_AtResequenceManagerOverride, Init);
        mMethodOverride(m_AtResequenceManagerOverride, Debug);
        mMethodOverride(m_AtResequenceManagerOverride, NumEngines);
        mMethodOverride(m_AtResequenceManagerOverride, BaseAddress);
        mMethodOverride(m_AtResequenceManagerOverride, EngineObjectCreate);
        }

    mMethodsSet(self, &m_AtResequenceManagerOverride);
    }

static void Override(AtResequenceManager self)
    {
    OverrideAtResequenceManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ResequenceManager);
    }

static AtResequenceManager ObjectInit(AtResequenceManager self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaResequenceManagerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtResequenceManager Tha60210012ResequenceManagerNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtResequenceManager manager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (manager == NULL)
        return NULL;

    return ObjectInit(manager, device);
    }

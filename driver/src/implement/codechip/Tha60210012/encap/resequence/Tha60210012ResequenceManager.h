/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60210012ResequenceManager.h
 * 
 * Created Date: May 17, 2016
 *
 * Description : Re-sequence engine management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012RESEQUENCEMANAGER_H_
#define _THA60210012RESEQUENCEMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/encap/resequence/AtResequenceManager.h"
#include "../../../../default/encap/resequence/ThaResequenceManager.h"
#include "../../../../../generic/encap/resequence/AtResequenceEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtResequenceManager Tha60210012ResequenceManagerNew(AtDevice device);
void Tha60210012ResequenceThresholdSet(AtResequenceEngine self, uint32 threshold);
eAtRet Tha60210012ResequenceNumLinksSet(AtResequenceEngine self, uint32 numLinks);

void Tha60210012ResequenceFsmStatusDisplay(AtResequenceEngine self);
void Tha60210012ResequenceStickyDisplay(AtResequenceEngine self);
void Tha60210012ResequenceCounterDisplay(AtResequenceEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012RESEQUENCEMANAGER_H_ */


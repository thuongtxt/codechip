/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60210012ResequenceReg.h
 * 
 * Created Date: May 17, 2016
 *
 * Description : Register of re-sequence engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012RESEQUENCEREG_H_
#define _THA60210012RESEQUENCEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cResequenceEngineBaseAddress 0x1900000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : IGRS version
Reg Addr   : 0x00_0000
Reg Formula:
    Where  :
Reg Desc   :
This register is used to get Version of Ingress Re-Sequence Engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_igrsver_Base                                                                     0x000000
#define cAf6Reg_upen_igrsver                                                                          0x000000
#define cAf6Reg_upen_igrsver_WidthVal                                                                       32
#define cAf6Reg_upen_igrsver_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: IGRSEngId
BitField Type: RO
BitField Desc: Engine ID
BitField Bits: [31:20]
--------------------------------------*/
#define cAf6_upen_igrsver_IGRSEngId_Bit_Start                                                               20
#define cAf6_upen_igrsver_IGRSEngId_Bit_End                                                                 31
#define cAf6_upen_igrsver_IGRSEngId_Mask                                                             cBit31_20
#define cAf6_upen_igrsver_IGRSEngId_Shift                                                                   20
#define cAf6_upen_igrsver_IGRSEngId_MaxVal                                                               0xfff
#define cAf6_upen_igrsver_IGRSEngId_MinVal                                                                 0x0
#define cAf6_upen_igrsver_IGRSEngId_RstVal                                                               0xA42

/*--------------------------------------
BitField Name: IGRSVerId
BitField Type: RO
BitField Desc: Version ID
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_upen_igrsver_IGRSVerId_Bit_Start                                                               16
#define cAf6_upen_igrsver_IGRSVerId_Bit_End                                                                 19
#define cAf6_upen_igrsver_IGRSVerId_Mask                                                             cBit19_16
#define cAf6_upen_igrsver_IGRSVerId_Shift                                                                   16
#define cAf6_upen_igrsver_IGRSVerId_MaxVal                                                                 0xf
#define cAf6_upen_igrsver_IGRSVerId_MinVal                                                                 0x0
#define cAf6_upen_igrsver_IGRSVerId_RstVal                                                                 0x1

/*--------------------------------------
BitField Name: IGRSEditId
BitField Type: RO
BitField Desc: Editor ID
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_upen_igrsver_IGRSEditId_Bit_Start                                                              14
#define cAf6_upen_igrsver_IGRSEditId_Bit_End                                                                15
#define cAf6_upen_igrsver_IGRSEditId_Mask                                                            cBit15_14
#define cAf6_upen_igrsver_IGRSEditId_Shift                                                                  14
#define cAf6_upen_igrsver_IGRSEditId_MaxVal                                                                0x3
#define cAf6_upen_igrsver_IGRSEditId_MinVal                                                                0x0
#define cAf6_upen_igrsver_IGRSEditId_RstVal                                                                0x0

/*--------------------------------------
BitField Name: IGRSYearId
BitField Type: RO
BitField Desc: Year
BitField Bits: [13:09]
--------------------------------------*/
#define cAf6_upen_igrsver_IGRSYearId_Bit_Start                                                               9
#define cAf6_upen_igrsver_IGRSYearId_Bit_End                                                                13
#define cAf6_upen_igrsver_IGRSYearId_Mask                                                             cBit13_9
#define cAf6_upen_igrsver_IGRSYearId_Shift                                                                   9
#define cAf6_upen_igrsver_IGRSYearId_MaxVal                                                               0x1f
#define cAf6_upen_igrsver_IGRSYearId_MinVal                                                                0x0
#define cAf6_upen_igrsver_IGRSYearId_RstVal                                                               0x10

/*--------------------------------------
BitField Name: IGRSMonId
BitField Type: RO
BitField Desc: Month
BitField Bits: [08:05]
--------------------------------------*/
#define cAf6_upen_igrsver_IGRSMonId_Bit_Start                                                                5
#define cAf6_upen_igrsver_IGRSMonId_Bit_End                                                                  8
#define cAf6_upen_igrsver_IGRSMonId_Mask                                                               cBit8_5
#define cAf6_upen_igrsver_IGRSMonId_Shift                                                                    5
#define cAf6_upen_igrsver_IGRSMonId_MaxVal                                                                 0xf
#define cAf6_upen_igrsver_IGRSMonId_MinVal                                                                 0x0
#define cAf6_upen_igrsver_IGRSMonId_RstVal                                                                 0x3

/*--------------------------------------
BitField Name: IGRSDayId
BitField Type: RO
BitField Desc: Day
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_upen_igrsver_IGRSDayId_Bit_Start                                                                0
#define cAf6_upen_igrsver_IGRSDayId_Bit_End                                                                  4
#define cAf6_upen_igrsver_IGRSDayId_Mask                                                               cBit4_0
#define cAf6_upen_igrsver_IGRSDayId_Shift                                                                    0
#define cAf6_upen_igrsver_IGRSDayId_MaxVal                                                                0x1f
#define cAf6_upen_igrsver_IGRSDayId_MinVal                                                                 0x0
#define cAf6_upen_igrsver_IGRSDayId_RstVal                                                                 0x3


/*------------------------------------------------------------------------------
Reg Name   : IGRS Control 0
Reg Addr   : 0x00_0001
Reg Formula:
    Where  :
Reg Desc   :
This register is used to control operation of RS engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_igrsctl_0_Base                                                                   0x000001
#define cAf6Reg_upen_igrsctl_0                                                                        0x000001
#define cAf6Reg_upen_igrsctl_0_WidthVal                                                                     32
#define cAf6Reg_upen_igrsctl_0_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: RsqTimeDiv
BitField Type: RW
BitField Desc: Timer devide, to faster simualtion
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_upen_igrsctl_0_RsqTimeDiv_Bit_Start                                                             8
#define cAf6_upen_igrsctl_0_RsqTimeDiv_Bit_End                                                               9
#define cAf6_upen_igrsctl_0_RsqTimeDiv_Mask                                                            cBit9_8
#define cAf6_upen_igrsctl_0_RsqTimeDiv_Shift                                                                 8
#define cAf6_upen_igrsctl_0_RsqTimeDiv_MaxVal                                                              0x3
#define cAf6_upen_igrsctl_0_RsqTimeDiv_MinVal                                                              0x0
#define cAf6_upen_igrsctl_0_RsqTimeDiv_RstVal                                                              0x0

/*--------------------------------------
BitField Name: RsqIntEn
BitField Type: RW
BitField Desc: Set high to enable Interrupt
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_igrsctl_0_RsqIntEn_Bit_Start                                                               4
#define cAf6_upen_igrsctl_0_RsqIntEn_Bit_End                                                                 4
#define cAf6_upen_igrsctl_0_RsqIntEn_Mask                                                                cBit4
#define cAf6_upen_igrsctl_0_RsqIntEn_Shift                                                                   4
#define cAf6_upen_igrsctl_0_RsqIntEn_MaxVal                                                                0x1
#define cAf6_upen_igrsctl_0_RsqIntEn_MinVal                                                                0x0
#define cAf6_upen_igrsctl_0_RsqIntEn_RstVal                                                                0x0

/*--------------------------------------
BitField Name: RsqAutEn
BitField Type: RW
BitField Desc: Set high to enable auto recovery engine
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_igrsctl_0_RsqAutEn_Bit_Start                                                               2
#define cAf6_upen_igrsctl_0_RsqAutEn_Bit_End                                                                 2
#define cAf6_upen_igrsctl_0_RsqAutEn_Mask                                                                cBit2
#define cAf6_upen_igrsctl_0_RsqAutEn_Shift                                                                   2
#define cAf6_upen_igrsctl_0_RsqAutEn_MaxVal                                                                0x1
#define cAf6_upen_igrsctl_0_RsqAutEn_MinVal                                                                0x0
#define cAf6_upen_igrsctl_0_RsqAutEn_RstVal                                                                0x0

/*--------------------------------------
BitField Name: RsqEnqEn
BitField Type: RW
BitField Desc: Set high to enable receive information from data enque
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_igrsctl_0_RsqEnqEn_Bit_Start                                                               1
#define cAf6_upen_igrsctl_0_RsqEnqEn_Bit_End                                                                 1
#define cAf6_upen_igrsctl_0_RsqEnqEn_Mask                                                                cBit1
#define cAf6_upen_igrsctl_0_RsqEnqEn_Shift                                                                   1
#define cAf6_upen_igrsctl_0_RsqEnqEn_MaxVal                                                                0x1
#define cAf6_upen_igrsctl_0_RsqEnqEn_MinVal                                                                0x0
#define cAf6_upen_igrsctl_0_RsqEnqEn_RstVal                                                                0x1

/*--------------------------------------
BitField Name: RsqActEn
BitField Type: RW
BitField Desc: Active
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_igrsctl_0_RsqActEn_Bit_Start                                                               0
#define cAf6_upen_igrsctl_0_RsqActEn_Bit_End                                                                 0
#define cAf6_upen_igrsctl_0_RsqActEn_Mask                                                                cBit0
#define cAf6_upen_igrsctl_0_RsqActEn_Shift                                                                   0
#define cAf6_upen_igrsctl_0_RsqActEn_MaxVal                                                                0x1
#define cAf6_upen_igrsctl_0_RsqActEn_MinVal                                                                0x0
#define cAf6_upen_igrsctl_0_RsqActEn_RstVal                                                                0x1


/*------------------------------------------------------------------------------
Reg Name   : IGRS Control 1
Reg Addr   : 0x00_0002
Reg Formula:
    Where  :
Reg Desc   :
This register is used to control operation of RS engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_igrsctl_1_Base                                                                   0x000002
#define cAf6Reg_upen_igrsctl_1                                                                        0x000002
#define cAf6Reg_upen_igrsctl_1_WidthVal                                                                     32
#define cAf6Reg_upen_igrsctl_1_WriteMask                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : IGRS Status 0
Reg Addr   : 0x00_0003
Reg Formula:
    Where  :
Reg Desc   :
This register is used to control operation of RS engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_igrssta_0_Base                                                                   0x000003
#define cAf6Reg_upen_igrssta_0                                                                        0x000003
#define cAf6Reg_upen_igrssta_0_WidthVal                                                                     32
#define cAf6Reg_upen_igrssta_0_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: RsqInitdone
BitField Type: RO
BitField Desc: HW init done
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_igrssta_0_RsqInitdone_Bit_Start                                                            0
#define cAf6_upen_igrssta_0_RsqInitdone_Bit_End                                                              0
#define cAf6_upen_igrssta_0_RsqInitdone_Mask                                                             cBit0
#define cAf6_upen_igrssta_0_RsqInitdone_Shift                                                                0
#define cAf6_upen_igrssta_0_RsqInitdone_MaxVal                                                             0x1
#define cAf6_upen_igrssta_0_RsqInitdone_MinVal                                                             0x0
#define cAf6_upen_igrssta_0_RsqInitdone_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : IGRS param
Reg Addr   : 0x00_0004
Reg Formula:
    Where  :
Reg Desc   :
This register is used to get Param of Ingress Re-Sequence Engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_igrsparam_Base                                                                   0x000004
#define cAf6Reg_upen_igrsparam                                                                        0x000004
#define cAf6Reg_upen_igrsparam_WidthVal                                                                     32
#define cAf6Reg_upen_igrsparam_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: FBlk_bit
BitField Type: RO
BitField Desc: Fragment Block bit
BitField Bits: [31:28]
--------------------------------------*/
#define cAf6_upen_igrsparam_FBlk_bit_Bit_Start                                                              28
#define cAf6_upen_igrsparam_FBlk_bit_Bit_End                                                                31
#define cAf6_upen_igrsparam_FBlk_bit_Mask                                                            cBit31_28
#define cAf6_upen_igrsparam_FBlk_bit_Shift                                                                  28
#define cAf6_upen_igrsparam_FBlk_bit_MaxVal                                                                0xf
#define cAf6_upen_igrsparam_FBlk_bit_MinVal                                                                0x0
#define cAf6_upen_igrsparam_FBlk_bit_RstVal                                                                0xC

/*--------------------------------------
BitField Name: QBlk_bit
BitField Type: RO
BitField Desc: Oque Block  bit
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_upen_igrsparam_QBlk_bit_Bit_Start                                                              24
#define cAf6_upen_igrsparam_QBlk_bit_Bit_End                                                                27
#define cAf6_upen_igrsparam_QBlk_bit_Mask                                                            cBit27_24
#define cAf6_upen_igrsparam_QBlk_bit_Shift                                                                  24
#define cAf6_upen_igrsparam_QBlk_bit_MaxVal                                                                0xf
#define cAf6_upen_igrsparam_QBlk_bit_MinVal                                                                0x0
#define cAf6_upen_igrsparam_QBlk_bit_RstVal                                                                0xB

/*--------------------------------------
BitField Name: BBlk_bit
BitField Type: RO
BitField Desc: Bitmap Block  bit
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_upen_igrsparam_BBlk_bit_Bit_Start                                                              20
#define cAf6_upen_igrsparam_BBlk_bit_Bit_End                                                                23
#define cAf6_upen_igrsparam_BBlk_bit_Mask                                                            cBit23_20
#define cAf6_upen_igrsparam_BBlk_bit_Shift                                                                  20
#define cAf6_upen_igrsparam_BBlk_bit_MaxVal                                                                0xf
#define cAf6_upen_igrsparam_BBlk_bit_MinVal                                                                0x0
#define cAf6_upen_igrsparam_BBlk_bit_RstVal                                                                0xC

/*--------------------------------------
BitField Name: Bund_bit
BitField Type: RO
BitField Desc: Bund bit
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_upen_igrsparam_Bund_bit_Bit_Start                                                              16
#define cAf6_upen_igrsparam_Bund_bit_Bit_End                                                                19
#define cAf6_upen_igrsparam_Bund_bit_Mask                                                            cBit19_16
#define cAf6_upen_igrsparam_Bund_bit_Shift                                                                  16
#define cAf6_upen_igrsparam_Bund_bit_MaxVal                                                                0xf
#define cAf6_upen_igrsparam_Bund_bit_MinVal                                                                0x0
#define cAf6_upen_igrsparam_Bund_bit_RstVal                                                                0x9

/*--------------------------------------
BitField Name: Link_bit
BitField Type: RO
BitField Desc: Link  bit
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_upen_igrsparam_Link_bit_Bit_Start                                                              12
#define cAf6_upen_igrsparam_Link_bit_Bit_End                                                                15
#define cAf6_upen_igrsparam_Link_bit_Mask                                                            cBit15_12
#define cAf6_upen_igrsparam_Link_bit_Shift                                                                  12
#define cAf6_upen_igrsparam_Link_bit_MaxVal                                                                0xf
#define cAf6_upen_igrsparam_Link_bit_MinVal                                                                0x0
#define cAf6_upen_igrsparam_Link_bit_RstVal                                                                0xC

/*--------------------------------------
BitField Name: Space_bit
BitField Type: RO
BitField Desc: Sequence space bit
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_upen_igrsparam_Space_bit_Bit_Start                                                              8
#define cAf6_upen_igrsparam_Space_bit_Bit_End                                                               11
#define cAf6_upen_igrsparam_Space_bit_Mask                                                            cBit11_8
#define cAf6_upen_igrsparam_Space_bit_Shift                                                                  8
#define cAf6_upen_igrsparam_Space_bit_MaxVal                                                               0xf
#define cAf6_upen_igrsparam_Space_bit_MinVal                                                               0x0
#define cAf6_upen_igrsparam_Space_bit_RstVal                                                               0xC

/*--------------------------------------
BitField Name: RFlow_bit
BitField Type: RO
BitField Desc: Re-Order Engine bit
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_upen_igrsparam_RFlow_bit_Bit_Start                                                              4
#define cAf6_upen_igrsparam_RFlow_bit_Bit_End                                                                7
#define cAf6_upen_igrsparam_RFlow_bit_Mask                                                             cBit7_4
#define cAf6_upen_igrsparam_RFlow_bit_Shift                                                                  4
#define cAf6_upen_igrsparam_RFlow_bit_MaxVal                                                               0xf
#define cAf6_upen_igrsparam_RFlow_bit_MinVal                                                               0x0
#define cAf6_upen_igrsparam_RFlow_bit_RstVal                                                               0xA

/*--------------------------------------
BitField Name: EFlow_bit
BitField Type: RO
BitField Desc: Eth_Flow bit
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_igrsparam_EFlow_bit_Bit_Start                                                              0
#define cAf6_upen_igrsparam_EFlow_bit_Bit_End                                                                3
#define cAf6_upen_igrsparam_EFlow_bit_Mask                                                             cBit3_0
#define cAf6_upen_igrsparam_EFlow_bit_Shift                                                                  0
#define cAf6_upen_igrsparam_EFlow_bit_MaxVal                                                               0xf
#define cAf6_upen_igrsparam_EFlow_bit_MinVal                                                               0x0
#define cAf6_upen_igrsparam_EFlow_bit_RstVal                                                               0xC


/*------------------------------------------------------------------------------
Reg Name   : IGRS Eth_flow Control
Reg Addr   : 0x00_8000
Reg Formula: 0x00_8000 + $eflow_id
    Where  :
           + $eflow_id(0-4095) eth_flow Id
Reg Desc   :
This register is used to lookup rflow_id (re-order Id)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgrslk_Base                                                                     0x008000
#define cAf6Reg_upen_cfgrslk(eflowid)                                                     (0x008000+(eflowid))
#define cAf6Reg_upen_cfgrslk_WidthVal                                                                       32
#define cAf6Reg_upen_cfgrslk_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: IGRSRsId
BitField Type: RW
BitField Desc: Re-Order ID
BitField Bits: [17:08]
--------------------------------------*/
#define cAf6_upen_cfgrslk_IGRSRsId_Bit_Start                                                                 8
#define cAf6_upen_cfgrslk_IGRSRsId_Bit_End                                                                  17
#define cAf6_upen_cfgrslk_IGRSRsId_Mask                                                               cBit17_8
#define cAf6_upen_cfgrslk_IGRSRsId_Shift                                                                     8
#define cAf6_upen_cfgrslk_IGRSRsId_MaxVal                                                                0x3ff
#define cAf6_upen_cfgrslk_IGRSRsId_MinVal                                                                  0x0
#define cAf6_upen_cfgrslk_IGRSRsId_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: IGRSRsFlush
BitField Type: RW
BitField Desc: Re-Order Flush
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_cfgrslk_IGRSRsFlush_Bit_Start                                                              6
#define cAf6_upen_cfgrslk_IGRSRsFlush_Bit_End                                                                6
#define cAf6_upen_cfgrslk_IGRSRsFlush_Mask                                                               cBit6
#define cAf6_upen_cfgrslk_IGRSRsFlush_Shift                                                                  6
#define cAf6_upen_cfgrslk_IGRSRsFlush_MaxVal                                                               0x1
#define cAf6_upen_cfgrslk_IGRSRsFlush_MinVal                                                               0x0
#define cAf6_upen_cfgrslk_IGRSRsFlush_RstVal                                                               0x0

/*--------------------------------------
BitField Name: IGRSRsEna
BitField Type: RW
BitField Desc: Re-Order Enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_cfgrslk_IGRSRsEna_Bit_Start                                                                5
#define cAf6_upen_cfgrslk_IGRSRsEna_Bit_End                                                                  5
#define cAf6_upen_cfgrslk_IGRSRsEna_Mask                                                                 cBit5
#define cAf6_upen_cfgrslk_IGRSRsEna_Shift                                                                    5
#define cAf6_upen_cfgrslk_IGRSRsEna_MaxVal                                                                 0x1
#define cAf6_upen_cfgrslk_IGRSRsEna_MinVal                                                                 0x0
#define cAf6_upen_cfgrslk_IGRSRsEna_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: IGRSoQueEna
BitField Type: RW
BitField Desc: output queue Enable
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_cfgrslk_IGRSoQueEna_Bit_Start                                                              4
#define cAf6_upen_cfgrslk_IGRSoQueEna_Bit_End                                                                4
#define cAf6_upen_cfgrslk_IGRSoQueEna_Mask                                                               cBit4
#define cAf6_upen_cfgrslk_IGRSoQueEna_Shift                                                                  4
#define cAf6_upen_cfgrslk_IGRSoQueEna_MaxVal                                                               0x1
#define cAf6_upen_cfgrslk_IGRSoQueEna_MinVal                                                               0x0
#define cAf6_upen_cfgrslk_IGRSoQueEna_RstVal                                                               0x0

/*--------------------------------------
BitField Name: IGRSoQueId
BitField Type: RW
BitField Desc: output queue Id
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_cfgrslk_IGRSoQueId_Bit_Start                                                               0
#define cAf6_upen_cfgrslk_IGRSoQueId_Bit_End                                                                 2
#define cAf6_upen_cfgrslk_IGRSoQueId_Mask                                                              cBit2_0
#define cAf6_upen_cfgrslk_IGRSoQueId_Shift                                                                   0
#define cAf6_upen_cfgrslk_IGRSoQueId_MaxVal                                                                0x7
#define cAf6_upen_cfgrslk_IGRSoQueId_MinVal                                                                0x0
#define cAf6_upen_cfgrslk_IGRSoQueId_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : IGRS Re-Order Buffer Space
Reg Addr   : 0x00_1000
Reg Formula: 0x00_1000 + $rflow_id
    Where  :
           + $rflow_id(0-1023): Re-Order ID
Reg Desc   :
This register is used to config param of Re-Order Engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgrfsc_Base                                                                     0x001000
#define cAf6Reg_upen_cfgrfsc(rflowid)                                                     (0x001000+(rflowid))
#define cAf6Reg_upen_cfgrfsc_WidthVal                                                                       32
#define cAf6Reg_upen_cfgrfsc_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: RsqRfsId
BitField Type: RW
BitField Desc: Re-Order Space (64K) ID, (0-127), SW must cfg it when RsqRfsMd =
2
BitField Bits: [10:04]
--------------------------------------*/
#define cAf6_upen_cfgrfsc_RsqRfsId_Bit_Start                                                                 4
#define cAf6_upen_cfgrfsc_RsqRfsId_Bit_End                                                                  10
#define cAf6_upen_cfgrfsc_RsqRfsId_Mask                                                               cBit10_4
#define cAf6_upen_cfgrfsc_RsqRfsId_Shift                                                                     4
#define cAf6_upen_cfgrfsc_RsqRfsId_MaxVal                                                                 0x7f
#define cAf6_upen_cfgrfsc_RsqRfsId_MinVal                                                                  0x0
#define cAf6_upen_cfgrfsc_RsqRfsId_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: RsqRfsMd
BitField Type: RW
BitField Desc: Re-Order Sequence Buffer Space, it must be > 1.25*BT with BT is
Buffer-Occupancy Threshold
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_upen_cfgrfsc_RsqRfsMd_Bit_Start                                                                 0
#define cAf6_upen_cfgrfsc_RsqRfsMd_Bit_End                                                                   1
#define cAf6_upen_cfgrfsc_RsqRfsMd_Mask                                                                cBit1_0
#define cAf6_upen_cfgrfsc_RsqRfsMd_Shift                                                                     0
#define cAf6_upen_cfgrfsc_RsqRfsMd_MaxVal                                                                  0x3
#define cAf6_upen_cfgrfsc_RsqRfsMd_MinVal                                                                  0x0
#define cAf6_upen_cfgrfsc_RsqRfsMd_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : IGRS Re-Order Control
Reg Addr   : 0x00_2000
Reg Formula: 0x00_2000 + $rflow_id
    Where  :
           + $rflow_id(0-1023): Re-Order ID
Reg Desc   :
This register is used to config param of Re-Order Engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgrsqc_Base                                                                     0x002000
#define cAf6Reg_upen_cfgrsqc(rflowid)                                                     (0x002000+(rflowid))
#define cAf6Reg_upen_cfgrsqc_WidthVal                                                                       32
#define cAf6Reg_upen_cfgrsqc_WriteMask                                                                     0x0

#define cAf6_upen_cfgrsqc_RsqLDADis_Mask                                                             cBit31_29
#define cAf6_upen_cfgrsqc_RsqLDADis_Shift                                                                   29

#define cAf6_upen_cfgrsqc_RsqNumLink_Mask                                                            cBit28_24
#define cAf6_upen_cfgrsqc_RsqNumLink_Shift                                                                  24

/*--------------------------------------
BitField Name: RsqSeqThsh
BitField Type: RW
BitField Desc: Threshold Sequence that declaration lost sequence Buffer-
Occupancy Threshold (BT) on RFC 5236
BitField Bits: [27:12]
--------------------------------------*/
#define cAf6_upen_cfgrsqc_RsqSeqThsh_Bit_Start                                                              12
#define cAf6_upen_cfgrsqc_RsqSeqThsh_Bit_End                                                                21
#define cAf6_upen_cfgrsqc_RsqSeqThsh_Mask                                                            cBit21_12
#define cAf6_upen_cfgrsqc_RsqSeqThsh_Shift                                                                  12
#define cAf6_upen_cfgrsqc_RsqSeqThsh_MaxVal                                                             0xffff
#define cAf6_upen_cfgrsqc_RsqSeqThsh_MinVal                                                                0x0
#define cAf6_upen_cfgrsqc_RsqSeqThsh_RstVal                                                                0x0

#define cAf6_upen_cfgrsqc_RsqResThsh_Mask                                                            cBit11_10
#define cAf6_upen_cfgrsqc_RsqResThsh_Shift                                                                  10

/*--------------------------------------
BitField Name: RsqEnable
BitField Type: RW
BitField Desc:
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_cfgrsqc_RsqEnable_Bit_Start                                                                8
#define cAf6_upen_cfgrsqc_RsqEnable_Bit_End                                                                  8
#define cAf6_upen_cfgrsqc_RsqEnable_Mask                                                                 cBit8
#define cAf6_upen_cfgrsqc_RsqEnable_Shift                                                                    8
#define cAf6_upen_cfgrsqc_RsqEnable_MaxVal                                                                 0x1
#define cAf6_upen_cfgrsqc_RsqEnable_MinVal                                                                 0x0
#define cAf6_upen_cfgrsqc_RsqEnable_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: RsqTimeout
BitField Type: RW
BitField Desc: Timeout value, step 1ms
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_cfgrsqc_RsqTimeout_Bit_Start                                                               0
#define cAf6_upen_cfgrsqc_RsqTimeout_Bit_End                                                                 7
#define cAf6_upen_cfgrsqc_RsqTimeout_Mask                                                              cBit7_0
#define cAf6_upen_cfgrsqc_RsqTimeout_Shift                                                                   0
#define cAf6_upen_cfgrsqc_RsqTimeout_MaxVal                                                               0xff
#define cAf6_upen_cfgrsqc_RsqTimeout_MinVal                                                                0x0
#define cAf6_upen_cfgrsqc_RsqTimeout_RstVal                                                                0x0

/*------------------------------------------------------------------------------
Reg Name   : IGRS Re-Order FSM status
Reg Addr   : 0x00_3000
Reg Formula: 0x00_3000 + $rflow_id
    Where  :
           + $rflow_id(0-1023): Re-Order ID
Reg Desc   :
This register is used to config param of Re-Order Engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgrsqs_Base                                                                     0x003000

/*--------------------------------------
BitField Name: RsqExpt
BitField Type: RO
BitField Desc: Expected sequence
BitField Bits: [31:08]
--------------------------------------*/
#define cAf6_upen_cfgrsqs_RsqExpt_Mask                                                                cBit31_8
#define cAf6_upen_cfgrsqs_RsqExpt_Shift                                                                      8

/*--------------------------------------
BitField Name: RsqCnt
BitField Type: RO
BitField Desc: Counter good or Bad sequence, depend in RsqFsm
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_upen_cfgrsqs_RsqCnt_Mask                                                                  cBit7_4
#define cAf6_upen_cfgrsqs_RsqCnt_Shift                                                                       4

/*--------------------------------------
BitField Name: RsqFsm
BitField Type: RO
BitField Desc: FSM of engine 0x0 - FSM_IDLE    -> Idle state 0x1 - FSM_READY
-> Ready state, ready to receive new sequence stream. 0x4 - FSM_SRCH_A  ->
Search A state 0x5 - FSM_SRCH_B  -> Search B state 0x8 - FSM_SYNC_A  -> Sync A
state 0x9 - FSM_SYNC_B  -> Sync B state 0xA - FSM_EIRST   -> Engine init Reset
state (when detect long to short sequence, or disable re-order) 0xB - FSM_PIRST
-> CPU init Reset state, used to flush/clear status before use. 0xC - FSM_ERESET
-> Engine Reset state 0xD - FSM_PRESET  -> CPU reset state 0xE - FSM_EDRST   ->
Engine Reset done, auto going to FSM_READY state when <RsqEnable = 1> 0xF -
FSM_PDRST   -> CPU reset done, auto going to FSM_READY state when <RsqEnable =
1>
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_cfgrsqs_RsqFsm_Mask                                                                  cBit3_0
#define cAf6_upen_cfgrsqs_RsqFsm_Shift                                                                       0

/*------------------------------------------------------------------------------
Reg Name   : IGRS Re-Order Sticky
Reg Addr   : 0x00_C000
Reg Formula: 0x00_C000 + $r2c_en*0x1000 + $rflow_id
    Where  :
           + $r2c_en(0-1): Read to clear enable
           + $rflow_id(0-1023): Re-Order ID
Reg Desc   :
This register is used to config param of Re-Order Engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgstky_Base                                                                     0x00C000

/*--------------------------------------
BitField Name: RsqStatus
BitField Type: RO
BitField Desc: Reorder event triggers bit_00 - E_EXPT -> receive expected
sequence bit_01 - E_RCPU -> cpu read bit_02 - E_WCPU -> cpu write bit_03 -
Unused
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_upen_cfgstky_RsqStatus_Mask                                                             cBit23_20
#define cAf6_upen_cfgstky_RsqStatus_Shift                                                                   20

/*--------------------------------------
BitField Name: RsqError
BitField Type: RO
BitField Desc: Processing Error reports bit_00 - R_DISC -> receive discard
arrive sequence reset state or disable re-order bit_01 - R_MALG -> receive early
sequence + M-algoritm bit_02 - R_THSH -> receive violation window size (over
threshold sequence space). (RFC 5236) bit_03 - R_WIND -> receive unexpected
sequence bit_04 - R_LATE -> receive late sequence bit_05 - R_EOUT -> scan with
exptected timeout bit_06 - R_SOUT -> scan with exptected timeout bit_07 - R_SRST
-> Re-Order reset state bit_08 - R_DUPL -> receive duplicate sequence bit_09 -
P_ESEQ -> packet assemble sequence error bit_10 - P_EBLK -> packet block error
bit_11 - P_EMSU -> packet MSRU error bit_12 - P_EMRU -> packet MRRU error bit_13
- P_MCHK -> packet Memory check error bit_14 - A_FULL -> Arrive fifo full bit_15
- A_EMPT -> Arrive fifo with empty blocks bit_16 - P_LNUL -> Fragment have
lenght equq zero, and silence drop bit_17 - P_LDRP -> Fragment declare drop at
other blocks function bit_18 - Unused bit_19 - Unused
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_upen_cfgstky_RsqError_Mask                                                               cBit19_0
#define cAf6_upen_cfgstky_RsqError_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : IGRS Re-Order Counter
Reg Addr   : 0x02_0000
Reg Formula: 0x02_0000 + $r2c_en*0x1_0000 + $rflow_id*0x10 + $cnt_id
    Where  :
           + $r2c_en(0-1): Read to clear enable
           + $rflow_id(0-1023): Re-Order ID
           + $cnt_id(0-9): counter_id }
Reg Desc   :
This register is used to show Re-Order Counters
cnt_id = 00 : cnt_r_ftotal
cnt_id = 01 : cnt_r_fgood
cnt_id = 02 : cnt_r_timeo
cnt_id = 03 : cnt_r_overm
cnt_id = 04 : cnt_p_fgood
cnt_id = 05 : cnt_p_pgood
cnt_id = 06 : cnt_p_vmsru
cnt_id = 07 : cnt_p_vmrru
cnt_id = 08 : cnt_r_fdisc
cnt_id = 09 ... : usused

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgcntw_Base                                                                     0x020000

/*--------------------------------------
BitField Name: RsqCounter
BitField Type: RW
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cfgcntw_RsqCounter_Mask                                                             cBit31_0
#define cAf6_upen_cfgcntw_RsqCounter_Shift                                                                   0

/*------------------------------------------------------------------------------
Reg Name   : IGRS Pkt-Assemble Control
Reg Addr   : 0x00_4000
Reg Formula: 0x00_4000 + $rflow_id
    Where  :
           + $rflow_id(0-1023): Re-Order ID
Reg Desc   :
This register is used to config param of Re-Order Engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgpkac_Base                                                                     0x004000
#define cAf6Reg_upen_cfgpkac(rflowid)                                                     (0x004000+(rflowid))
#define cAf6Reg_upen_cfgpkac_WidthVal                                                                       32
#define cAf6Reg_upen_cfgpkac_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: PkaMsru
BitField Type: RW
BitField Desc: Multilink Maximum Sequence Reconstructed Unit, is maximum
fragments per packet
BitField Bits: [22:16]
--------------------------------------*/
#define cAf6_upen_cfgpkac_PkaMsru_Bit_Start                                                                 16
#define cAf6_upen_cfgpkac_PkaMsru_Bit_End                                                                   22
#define cAf6_upen_cfgpkac_PkaMsru_Mask                                                               cBit22_16
#define cAf6_upen_cfgpkac_PkaMsru_Shift                                                                     16
#define cAf6_upen_cfgpkac_PkaMsru_MaxVal                                                                  0x7f
#define cAf6_upen_cfgpkac_PkaMsru_MinVal                                                                   0x0
#define cAf6_upen_cfgpkac_PkaMsru_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: PkaMrru
BitField Type: RW
BitField Desc: Multilink Maximum Received Reconstructed Unit (RFC 1990)
BitField Bits: [13:00]
--------------------------------------*/
#define cAf6_upen_cfgpkac_PkaMrru_Bit_Start                                                                  0
#define cAf6_upen_cfgpkac_PkaMrru_Bit_End                                                                   13
#define cAf6_upen_cfgpkac_PkaMrru_Mask                                                                cBit13_0
#define cAf6_upen_cfgpkac_PkaMrru_Shift                                                                      0
#define cAf6_upen_cfgpkac_PkaMrru_MaxVal                                                                0x3fff
#define cAf6_upen_cfgpkac_PkaMrru_MinVal                                                                   0x0
#define cAf6_upen_cfgpkac_PkaMrru_RstVal                                                                   0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012RESEQUENCEREG_H_ */


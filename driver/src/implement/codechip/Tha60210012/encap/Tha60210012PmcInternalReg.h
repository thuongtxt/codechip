/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PMC
 * 
 * File        : Tha60210012PmcInternalReg.h
 * 
 * Created Date: Jul 25, 2016
 *
 * Description : Register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PMCINTERNALREG_H_
#define _THA60210012PMCINTERNALREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#define cTha60210012ModuleInternalPMCBaseAddress 0x0E40000

/*------------------------------------------------------------------------------
Reg Name   : PMC Global Counters
Reg Addr   : 0x00100-0x001FF
Reg Formula: 0x00100 + $glbcntid*16 + r2c*8 + $glbcnttyp
    Where  :
           + $glbcntid(0-15): Counter ID
           + r2c: 1 for read to clear
           + $glbcnttyp(0-7): Counter type
Reg Desc   :
Support the following Counters ID:
- glbcntid   = 4'd0 : Input CLA Counter
- glbcntid   = 4'd1 : Output CLA Counter
- glbcntid   = 4'd2 : Input Framming Counter
- glbcntid   = 4'd3 : Output Framming Counter
- glbcntid   = 4'd4 : Input PDA Counter
- glbcntid   = 4'd5 : Output PDA to Lo-ENC#0 Counter
- glbcntid   = 4'd6 : Output PDA to Lo-ENC#1 Counter
- glbcntid   = 4'd7 : Output PDA to Lo-ENC#2 Counter
- glbcntid   = 4'd8 : Output PDA to Lo-ENC#3 Counter
- glbcntid   = 4'd9 : Output PWE Counter
- glbcntid   = 4'd10: Output MUX to OOBFC Counter
- glbcntid   = 4'd11: Output MUX to PWE Counter
- glbcntid   = 4'd12: Output MUX to XFI Counter
- glbcntid(13-15)   : Reserved
Each Counter ID Support following Counter Types:
- glbcnttyp = 3'd0 : Packet Valid Counter
- glbcnttyp = 3'd1 : Packet Sop Counter
- glbcnttyp = 3'd2 : Packet Eop Counter
- glbcnttyp = 3'd3 : Packet Err Counter
- glbcnttyp = 3'd4 : Packet Nob Counter
- glbcnttyp(5-7)  : Reserved

------------------------------------------------------------------------------*/
#define cAf6Reg_pmc_glbcnt_pen_Base                                                                    0x00100
#define cAf6Reg_pmc_glbcnt_pen(glbcntid, r2c, glbcnttyp)              (0x00100+(glbcntid)*16+(r2c)*8+(glbcnttyp))
#define cAf6Reg_pmc_glbcnt_pen_WidthVal                                                                     32
#define cAf6Reg_pmc_glbcnt_pen_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: GLB_CNT
BitField Type: RO
BitField Desc: Global Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_pmc_glbcnt_pen_GLB_CNT_Bit_Start                                                                0
#define cAf6_pmc_glbcnt_pen_GLB_CNT_Bit_End                                                                 31
#define cAf6_pmc_glbcnt_pen_GLB_CNT_Mask                                                              cBit31_0
#define cAf6_pmc_glbcnt_pen_GLB_CNT_Shift                                                                    0
#define cAf6_pmc_glbcnt_pen_GLB_CNT_MaxVal                                                          0xffffffff
#define cAf6_pmc_glbcnt_pen_GLB_CNT_MinVal                                                                 0x0
#define cAf6_pmc_glbcnt_pen_GLB_CNT_RstVal                                                                0x00


/*------------------------------------------------------------------------------
Reg Name   : Link HO Counter ID Lookup Control
Reg Addr   : 0x10000-0x1107F
Reg Formula: 0x10000 + $side*4096 + $slcid*64 + $lid
    Where  :
           + $side(0-1) : 0 ENC 1 DEC
           + $slcid(0-1): Slice ID
           + $lid(0-63): LID
Reg Desc   :
This register is used to config to select Pool ID for Link counters

------------------------------------------------------------------------------*/
#define cAf6Reg_tdmhocntidlk_pen_Base                                                                  0x10000
#define cAf6Reg_tdmhocntidlk_pen(side, slcid, lid)                      (0x10000+(side)*4096+(slcid)*64+(lid))
#define cAf6Reg_tdmhocntidlk_pen_WidthVal                                                                   32
#define cAf6Reg_tdmhocntidlk_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Link_Ho_PCNT_EN
BitField Type: R/W
BitField Desc: Link Pool Count Enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_EN_Bit_Start                                                      5
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_EN_Bit_End                                                        5
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_EN_Mask                                                       cBit5
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_EN_Shift                                                          5
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_EN_MaxVal                                                       0x1
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_EN_MinVal                                                       0x0
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_EN_RstVal                                                      0x00

/*--------------------------------------
BitField Name: Link_Ho_PCNT_ID
BitField Type: R/W
BitField Desc: Link Pool ID
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_ID_Bit_Start                                                      0
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_ID_Bit_End                                                        4
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_ID_Mask                                                     cBit4_0
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_ID_Shift                                                          0
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_ID_MaxVal                                                      0x1f
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_ID_MinVal                                                       0x0
#define cAf6_tdmhocntidlk_pen_Link_Ho_PCNT_ID_RstVal                                                      0x00


/*------------------------------------------------------------------------------
Reg Name   : Link LO Counter ID Lookup Control
Reg Addr   : 0x12000-0x13FFF
Reg Formula: 0x12000 + $side*4096 + $slcid*1024 + $lid
    Where  :
           + $side(0-1)  : 0 ENC 1 DEC
           + $slcid(0-3): Slice ID
           + $lid(0-1023): LID
Reg Desc   :
This register is used to config to select Pool ID for Link counters

------------------------------------------------------------------------------*/
#define cAf6Reg_tdmlocntidlk_pen_Base                                                                  0x12000
#define cAf6Reg_tdmlocntidlk_pen(side, slcid, lid)                    (0x12000+(side)*4096+(slcid)*1024+(lid))
#define cAf6Reg_tdmlocntidlk_pen_WidthVal                                                                   32
#define cAf6Reg_tdmlocntidlk_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Link_Lo_PCNT_EN
BitField Type: R/W
BitField Desc: Link Pool Count Enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_EN_Bit_Start                                                      5
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_EN_Bit_End                                                        5
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_EN_Mask                                                       cBit5
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_EN_Shift                                                          5
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_EN_MaxVal                                                       0x1
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_EN_MinVal                                                       0x0
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_EN_RstVal                                                      0x00

/*--------------------------------------
BitField Name: Link_Lo_PCNT_ID
BitField Type: R/W
BitField Desc: Link Pool ID
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_ID_Bit_Start                                                      0
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_ID_Bit_End                                                        4
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_ID_Mask                                                     cBit4_0
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_ID_Shift                                                          0
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_ID_MaxVal                                                      0x1f
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_ID_MinVal                                                       0x0
#define cAf6_tdmlocntidlk_pen_Link_Lo_PCNT_ID_RstVal                                                      0x00


/*------------------------------------------------------------------------------
Reg Name   : Bundle Counter ID Lookup Control
Reg Addr   : 0x16000-0x165FF
Reg Formula: 0x16000 + $side*1024 + $bundid
    Where  :
           + $side(0-1): 0 Tx 1 Rx
           + $bundid(0-511): Bundle ID
Reg Desc   :
This register is used to config to select Pool ID for Bundle Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_bndcntidlk_pen_Base                                                                    0x16000
#define cAf6Reg_bndcntidlk_pen(side, bundid)                                    (0x16000+(side)*1024+(bundid))
#define cAf6Reg_bndcntidlk_pen_WidthVal                                                                     32
#define cAf6Reg_bndcntidlk_pen_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: BND_PCNT_EN
BitField Type: R/W
BitField Desc: Bundle Pool Count Enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_bndcntidlk_pen_BND_PCNT_EN_Bit_Start                                                            5
#define cAf6_bndcntidlk_pen_BND_PCNT_EN_Bit_End                                                              5
#define cAf6_bndcntidlk_pen_BND_PCNT_EN_Mask                                                             cBit5
#define cAf6_bndcntidlk_pen_BND_PCNT_EN_Shift                                                                5
#define cAf6_bndcntidlk_pen_BND_PCNT_EN_MaxVal                                                             0x1
#define cAf6_bndcntidlk_pen_BND_PCNT_EN_MinVal                                                             0x0
#define cAf6_bndcntidlk_pen_BND_PCNT_EN_RstVal                                                            0x00

/*--------------------------------------
BitField Name: BND_PCNT_ID
BitField Type: R/W
BitField Desc: Bundle Pool ID
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_bndcntidlk_pen_BND_PCNT_ID_Bit_Start                                                            0
#define cAf6_bndcntidlk_pen_BND_PCNT_ID_Bit_End                                                              4
#define cAf6_bndcntidlk_pen_BND_PCNT_ID_Mask                                                           cBit4_0
#define cAf6_bndcntidlk_pen_BND_PCNT_ID_Shift                                                                0
#define cAf6_bndcntidlk_pen_BND_PCNT_ID_MaxVal                                                            0x1f
#define cAf6_bndcntidlk_pen_BND_PCNT_ID_MinVal                                                             0x0
#define cAf6_bndcntidlk_pen_BND_PCNT_ID_RstVal                                                            0x00


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Counter ID Lookup Control
Reg Addr   : 0x14000-0x15FFF
Reg Formula: 0x14000 + $side*4096 + $efid
    Where  :
           + $side(0-1): 0 Egress Ethernet/PWE Receive
                        {1} Ingress Ethernet/PWE Transmit
           + $efid(0-4095): Ethernet flow ID}
Reg Desc   :
This register is used to config to select Pool ID for Tx Ethernet Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_ethcntidlk_pen_Base                                                                    0x14000
#define cAf6Reg_ethcntidlk_pen(side, efid)                                        (0x14000+(side)*4096+(efid))
#define cAf6Reg_ethcntidlk_pen_WidthVal                                                                     32
#define cAf6Reg_ethcntidlk_pen_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: ETH_PCNT_EN
BitField Type: R/W
BitField Desc: Ethernet Pool Count Enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ethcntidlk_pen_ETH_PCNT_EN_Bit_Start                                                            5
#define cAf6_ethcntidlk_pen_ETH_PCNT_EN_Bit_End                                                              5
#define cAf6_ethcntidlk_pen_ETH_PCNT_EN_Mask                                                             cBit5
#define cAf6_ethcntidlk_pen_ETH_PCNT_EN_Shift                                                                5
#define cAf6_ethcntidlk_pen_ETH_PCNT_EN_MaxVal                                                             0x1
#define cAf6_ethcntidlk_pen_ETH_PCNT_EN_MinVal                                                             0x0
#define cAf6_ethcntidlk_pen_ETH_PCNT_EN_RstVal                                                            0x00

/*--------------------------------------
BitField Name: ETH_PCNT_ID
BitField Type: R/W
BitField Desc: Ethernet Pool ID
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_ethcntidlk_pen_ETH_PCNT_ID_Bit_Start                                                            0
#define cAf6_ethcntidlk_pen_ETH_PCNT_ID_Bit_End                                                              4
#define cAf6_ethcntidlk_pen_ETH_PCNT_ID_Mask                                                           cBit4_0
#define cAf6_ethcntidlk_pen_ETH_PCNT_ID_Shift                                                                0
#define cAf6_ethcntidlk_pen_ETH_PCNT_ID_MaxVal                                                            0x1f
#define cAf6_ethcntidlk_pen_ETH_PCNT_ID_MinVal                                                             0x0
#define cAf6_ethcntidlk_pen_ETH_PCNT_ID_RstVal                                                            0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM Pool Encap Packet Counters
Reg Addr   : 0x01000-0x0107F
Reg Formula: 0x01000+ $r2c*64+$cnt_type*32+$cnt_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $cnt_type(0-1): counter type
           + $cnt_id(0-31): Link Pool ID
Reg Desc   :
Support the following Enc Packet counter:
- cnt_type = 0: ENC Packet Good
- cnt_type = 1: ENC Packet Error

------------------------------------------------------------------------------*/
#define cAf6Reg_encpktcnt_pen_Base                                                                     0x01000
#define cAf6Reg_encpktcnt_pen(r2c, cnttype, cntid)                     (0x01000+(r2c)*64+(cnttype)*32+(cntid))
#define cAf6Reg_encpktcnt_pen_WidthVal                                                                      32
#define cAf6Reg_encpktcnt_pen_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: ENC_PKT_CNT
BitField Type: RO
BitField Desc: Encap Packet Count
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_encpktcnt_pen_ENC_PKT_CNT_Bit_Start                                                             0
#define cAf6_encpktcnt_pen_ENC_PKT_CNT_Bit_End                                                              23
#define cAf6_encpktcnt_pen_ENC_PKT_CNT_Mask                                                           cBit23_0
#define cAf6_encpktcnt_pen_ENC_PKT_CNT_Shift                                                                 0
#define cAf6_encpktcnt_pen_ENC_PKT_CNT_MaxVal                                                         0xffffff
#define cAf6_encpktcnt_pen_ENC_PKT_CNT_MinVal                                                              0x0
#define cAf6_encpktcnt_pen_ENC_PKT_CNT_RstVal                                                             0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM Pool Encap Byte Counters
Reg Addr   : 0x01100-0x0113F
Reg Formula: 0x01100+ $r2c*64+$cnt_type*32+$cnt_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $cnt_type(0-1): counter type
           + $cnt_id(0-31): Link Pool ID
Reg Desc   :
Support the following Enc Packet counter:
- cnt_type = 0: ENC Byte Good
- cnt_type = 1: ENC Byte Error

------------------------------------------------------------------------------*/
#define cAf6Reg_encbytecnt_pen_Base                                                                    0x01100
#define cAf6Reg_encbytecnt_pen(r2c, cnttype, cntid)                    (0x01100+(r2c)*64+(cnttype)*32+(cntid))
#define cAf6Reg_encbytecnt_pen_WidthVal                                                                     32
#define cAf6Reg_encbytecnt_pen_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: ENC_Byte_CNT
BitField Type: RO
BitField Desc: Encap Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_encbytecnt_pen_ENC_Byte_CNT_Bit_Start                                                           0
#define cAf6_encbytecnt_pen_ENC_Byte_CNT_Bit_End                                                            31
#define cAf6_encbytecnt_pen_ENC_Byte_CNT_Mask                                                         cBit31_0
#define cAf6_encbytecnt_pen_ENC_Byte_CNT_Shift                                                               0
#define cAf6_encbytecnt_pen_ENC_Byte_CNT_MaxVal                                                     0xffffffff
#define cAf6_encbytecnt_pen_ENC_Byte_CNT_MinVal                                                            0x0
#define cAf6_encbytecnt_pen_ENC_Byte_CNT_RstVal                                                           0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM Pool Encap Packet Type Counters
Reg Addr   : 0x01200-0x0163F
Reg Formula: 0x01200 +$cnt_type*256+$r2c*32+$cnt_id
    Where  :
           + $cnt_type(0-7): counter type
           + $r2c(0-1): 1 for read to clear
           + $cnt_id(0-31): Link Pool ID
Reg Desc   :
Support the following Encap Packet Type Counters:
- cnt_type = 3'd0: Enc Packet fragments
- cnt_type = 3'd1: Enc Packet Plain
- cnt_type = 3'd2: Enc Packet OAM
- cnt_type = 3'd3: Enc Packet Abort
- cnt_type(5-7)  : Reserved

------------------------------------------------------------------------------*/
#define cAf6Reg_encpkttypcnt_pen_Base                                                                  0x01200
#define cAf6Reg_encpkttypcnt_pen(cnttype, r2c, cntid)                 (0x01200+(cnttype)*256+(r2c)*32+(cntid))
#define cAf6Reg_encpkttypcnt_pen_WidthVal                                                                   32
#define cAf6Reg_encpkttypcnt_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: ENC_PktTyp_CNT
BitField Type: RO
BitField Desc: Encap Packet Type Count
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_encpkttypcnt_pen_ENC_PktTyp_CNT_Bit_Start                                                       0
#define cAf6_encpkttypcnt_pen_ENC_PktTyp_CNT_Bit_End                                                        23
#define cAf6_encpkttypcnt_pen_ENC_PktTyp_CNT_Mask                                                     cBit23_0
#define cAf6_encpkttypcnt_pen_ENC_PktTyp_CNT_Shift                                                           0
#define cAf6_encpkttypcnt_pen_ENC_PktTyp_CNT_MaxVal                                                   0xffffff
#define cAf6_encpkttypcnt_pen_ENC_PktTyp_CNT_MinVal                                                        0x0
#define cAf6_encpkttypcnt_pen_ENC_PktTyp_CNT_RstVal                                                       0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM Pool Encap Idlie Byte Counter
Reg Addr   : 0x01A00-0x01A3F
Reg Formula: 0x01A00 + $r2c*32+$cnt_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $cnt_id(0-31): Link Pool ID
Reg Desc   :
Encap Idle Byte Counter:

------------------------------------------------------------------------------*/
#define cAf6Reg_encidlebytecnt_pen_Base                                                                0x01A00
#define cAf6Reg_encidlebytecnt_pen(r2c, cntid)                                      (0x01A00+(r2c)*32+(cntid))
#define cAf6Reg_encidlebytecnt_pen_WidthVal                                                                 32
#define cAf6Reg_encidlebytecnt_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: ENC_IDLE_CNT
BitField Type: RO
BitField Desc: Encap Idle Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_encidlebytecnt_pen_ENC_IDLE_CNT_Bit_Start                                                       0
#define cAf6_encidlebytecnt_pen_ENC_IDLE_CNT_Bit_End                                                        31
#define cAf6_encidlebytecnt_pen_ENC_IDLE_CNT_Mask                                                     cBit31_0
#define cAf6_encidlebytecnt_pen_ENC_IDLE_CNT_Shift                                                           0
#define cAf6_encidlebytecnt_pen_ENC_IDLE_CNT_MaxVal                                                 0xffffffff
#define cAf6_encidlebytecnt_pen_ENC_IDLE_CNT_MinVal                                                        0x0
#define cAf6_encidlebytecnt_pen_ENC_IDLE_CNT_RstVal                                                       0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM Decap Packet Counters
Reg Addr   : 0x02000-0x0207F
Reg Formula: 0x02000+ $r2c*64+$cnt_type*32+$cnt_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $cnt_type(0-1): counter type
           + $cnt_id(0-31): Link Pool ID
Reg Desc   :
Support the following Enc Packet counter:
- cnt_type = 0: DEC Packet Good
- cnt_type = 1: DEC Packet total

------------------------------------------------------------------------------*/
#define cAf6Reg_decpktcnt_pen_Base                                                                     0x02000
#define cAf6Reg_decpktcnt_pen(r2c, cnttype, cntid)                     (0x02000+(r2c)*64+(cnttype)*32+(cntid))
#define cAf6Reg_decpktcnt_pen_WidthVal                                                                      32
#define cAf6Reg_decpktcnt_pen_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: DEC_PKT_CNT
BitField Type: RO
BitField Desc: Decap Packet Count
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_decpktcnt_pen_DEC_PKT_CNT_Bit_Start                                                             0
#define cAf6_decpktcnt_pen_DEC_PKT_CNT_Bit_End                                                              23
#define cAf6_decpktcnt_pen_DEC_PKT_CNT_Mask                                                           cBit23_0
#define cAf6_decpktcnt_pen_DEC_PKT_CNT_Shift                                                                 0
#define cAf6_decpktcnt_pen_DEC_PKT_CNT_MaxVal                                                         0xffffff
#define cAf6_decpktcnt_pen_DEC_PKT_CNT_MinVal                                                              0x0
#define cAf6_decpktcnt_pen_DEC_PKT_CNT_RstVal                                                             0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM Decap Byte Counters
Reg Addr   : 0x02100-0x0217F
Reg Formula: 0x02100+ $r2c*64+$cnt_type*32+$cnt_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $cnt_type(0-1): counter type
           + $cnt_id(0-31): Link Pool ID
Reg Desc   :
Support the following Decap Byte counters:
- cnt_type = 0: DEC Byte Good
- cnt_type = 1: DEC Byte total

------------------------------------------------------------------------------*/
#define cAf6Reg_decbytecnt_pen_Base                                                                    0x02100
#define cAf6Reg_decbytecnt_pen(r2c, cnttype, cntid)                    (0x02100+(r2c)*64+(cnttype)*32+(cntid))
#define cAf6Reg_decbytecnt_pen_WidthVal                                                                     32
#define cAf6Reg_decbytecnt_pen_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: DEC_Byte_CNT
BitField Type: RO
BitField Desc: Decap Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_decbytecnt_pen_DEC_Byte_CNT_Bit_Start                                                           0
#define cAf6_decbytecnt_pen_DEC_Byte_CNT_Bit_End                                                            31
#define cAf6_decbytecnt_pen_DEC_Byte_CNT_Mask                                                         cBit31_0
#define cAf6_decbytecnt_pen_DEC_Byte_CNT_Shift                                                               0
#define cAf6_decbytecnt_pen_DEC_Byte_CNT_MaxVal                                                     0xffffffff
#define cAf6_decbytecnt_pen_DEC_Byte_CNT_MinVal                                                            0x0
#define cAf6_decbytecnt_pen_DEC_Byte_CNT_RstVal                                                           0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM Decap Packet Type Counters
Reg Addr   : 0x02200-0x0273F
Reg Formula: 0x02200 + $cnt_type*256+$r2c*32+$cnt_id
    Where  :
           + $cnt_type(0-7): counter type
           + $r2c(0-1): 1 for read to clear
           + $cnt_id(0-31): Link Pool ID
Reg Desc   :
Support the following Decap Packet Type counters:
- cnt_type = 3'd0: Dec Packet fragments
- cnt_type = 3'd1: Dec Packet Plain
- cnt_type = 3'd2: Dec Packet OAM
- cnt_type = 3'd3: Dec Packet Abort
- cnt_type = 3'd4: Dec FCS Error
- cnt_type(5-7)  : Reserved

------------------------------------------------------------------------------*/
#define cAf6Reg_decpkttypcnt_pen_Base                                                                  0x02200
#define cAf6Reg_decpkttypcnt_pen(cnttype, r2c, cntid)                 (0x02200+(cnttype)*256+(r2c)*32+(cntid))
#define cAf6Reg_decpkttypcnt_pen_WidthVal                                                                   32
#define cAf6Reg_decpkttypcnt_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: DEC_PktTyp_CNT
BitField Type: RO
BitField Desc: Decap Packet Type Counter
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_decpkttypcnt_pen_DEC_PktTyp_CNT_Bit_Start                                                       0
#define cAf6_decpkttypcnt_pen_DEC_PktTyp_CNT_Bit_End                                                        23
#define cAf6_decpkttypcnt_pen_DEC_PktTyp_CNT_Mask                                                     cBit23_0
#define cAf6_decpkttypcnt_pen_DEC_PktTyp_CNT_Shift                                                           0
#define cAf6_decpkttypcnt_pen_DEC_PktTyp_CNT_MaxVal                                                   0xffffff
#define cAf6_decpkttypcnt_pen_DEC_PktTyp_CNT_MinVal                                                        0x0
#define cAf6_decpkttypcnt_pen_DEC_PktTyp_CNT_RstVal                                                       0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM Decap Idlie Byte Counter
Reg Addr   : 0x02A00-0x02A3F
Reg Formula: 0x02A00 + $r2c*32+$cnt_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $cnt_id(0-31): Link Pool ID
Reg Desc   :
Decap Idle Byte Counter:

------------------------------------------------------------------------------*/
#define cAf6Reg_decidlebytecnt_pen_Base                                                                0x02A00
#define cAf6Reg_decidlebytecnt_pen(r2c, cntid)                                      (0x02A00+(r2c)*32+(cntid))
#define cAf6Reg_decidlebytecnt_pen_WidthVal                                                                 32
#define cAf6Reg_decidlebytecnt_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: DEC_IDLE_CNT
BitField Type: RO
BitField Desc: Decap Idle Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_decidlebytecnt_pen_DEC_IDLE_CNT_Bit_Start                                                       0
#define cAf6_decidlebytecnt_pen_DEC_IDLE_CNT_Bit_End                                                        31
#define cAf6_decidlebytecnt_pen_DEC_IDLE_CNT_Mask                                                     cBit31_0
#define cAf6_decidlebytecnt_pen_DEC_IDLE_CNT_Shift                                                           0
#define cAf6_decidlebytecnt_pen_DEC_IDLE_CNT_MaxVal                                                 0xffffffff
#define cAf6_decidlebytecnt_pen_DEC_IDLE_CNT_MinVal                                                        0x0
#define cAf6_decidlebytecnt_pen_DEC_IDLE_CNT_RstVal                                                       0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM HO Encap Packet Counters
Reg Addr   : 0x09000-0x091FF
Reg Formula: 0x09000+ $r2c*256+$cnt_type*128+$hoslc_id*64+$hoenc_id
    Where  :
           + $r2c(0-1)     : 1 for read to clear
           + $cnt_type(0-1) : counter type
           + $hoslc_id(0-1): HO Slice
           + $hoenc_id(0-47): HO ENCAP ID}
Reg Desc   :
Support the following Enc Packet counter:
- cnt_type = 0: HO ENC Packet Good
- cnt_type = 1: HO ENC Packet total

------------------------------------------------------------------------------*/
#define cAf6Reg_hoencpktcnt_pen_Base                                                                   0x09000
#define cAf6Reg_hoencpktcnt_pen(r2c, cnttype, hoslcid, hoencid)       (0x09000+(r2c)*256+(cnttype)*128+(hoslcid)*64+(hoencid))
#define cAf6Reg_hoencpktcnt_pen_WidthVal                                                                    32
#define cAf6Reg_hoencpktcnt_pen_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: HO_ENC_PKT_CNT
BitField Type: RO
BitField Desc: Encap Packet Count
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_hoencpktcnt_pen_HO_ENC_PKT_CNT_Bit_Start                                                        0
#define cAf6_hoencpktcnt_pen_HO_ENC_PKT_CNT_Bit_End                                                         23
#define cAf6_hoencpktcnt_pen_HO_ENC_PKT_CNT_Mask                                                      cBit23_0
#define cAf6_hoencpktcnt_pen_HO_ENC_PKT_CNT_Shift                                                            0
#define cAf6_hoencpktcnt_pen_HO_ENC_PKT_CNT_MaxVal                                                    0xffffff
#define cAf6_hoencpktcnt_pen_HO_ENC_PKT_CNT_MinVal                                                         0x0
#define cAf6_hoencpktcnt_pen_HO_ENC_PKT_CNT_RstVal                                                        0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM HO Encap Byte Counters
Reg Addr   : 0x09200-0x093FF
Reg Formula: 0x09200+ $r2c*256+$cnt_type*128+$hoslc_id*64+$hoenc_id
    Where  :
           + $r2c(0-1)     : 1 for read to clear
           + $cnt_type(0-1): counter type
           + $hoslc_id(0-1): HO Slice
           + $hoenc_id(0-95) : HO ENCAP ID}
Reg Desc   :
Support the following Enc Packet counter:
- cnt_type = 0: HO ENC Byte Good
- cnt_type = 1: HO ENC Byte total

------------------------------------------------------------------------------*/
#define cAf6Reg_hoencbytecnt_pen_Base                                                                  0x09200
#define cAf6Reg_hoencbytecnt_pen(r2c, cnttype, hoslcid, hoencid)      (0x09200+(r2c)*256+(cnttype)*128+(hoslcid)*64+(hoencid))
#define cAf6Reg_hoencbytecnt_pen_WidthVal                                                                   32
#define cAf6Reg_hoencbytecnt_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: HO_ENC_Byte_CNT
BitField Type: RO
BitField Desc: Encap Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_hoencbytecnt_pen_HO_ENC_Byte_CNT_Bit_Start                                                      0
#define cAf6_hoencbytecnt_pen_HO_ENC_Byte_CNT_Bit_End                                                       31
#define cAf6_hoencbytecnt_pen_HO_ENC_Byte_CNT_Mask                                                    cBit31_0
#define cAf6_hoencbytecnt_pen_HO_ENC_Byte_CNT_Shift                                                          0
#define cAf6_hoencbytecnt_pen_HO_ENC_Byte_CNT_MaxVal                                                0xffffffff
#define cAf6_hoencbytecnt_pen_HO_ENC_Byte_CNT_MinVal                                                       0x0
#define cAf6_hoencbytecnt_pen_HO_ENC_Byte_CNT_RstVal                                                      0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM HO Encap Packet Type Counters
Reg Addr   : 0x09800-0x09BFF
Reg Formula: 0x09800 +$cnt_type*256+$r2c*128+$hoslc_id*64+$hoenc_id
    Where  :
           + $cnt_type(0-3): counter type
           + $r2c(0-1): 1 for read to clear
           + $hoslc_id(0-1): HO Slice
           + $hoenc_id(0-47): HO ENCAP ID}
Reg Desc   :
Support the following Encap Packet Type Counters:
- cnt_type = 3'd0: ENC Packet fragments
- cnt_type = 3'd1: ENC Packet Plain
- cnt_type = 3'd2: ENC Packet OAM
- cnt_type = 3'd3: ENC Packet Abort

------------------------------------------------------------------------------*/
#define cAf6Reg_hoencpkttypcnt_pen_Base                                                                0x09800
#define cAf6Reg_hoencpkttypcnt_pen(cnttype, r2c, hoslcid, hoencid)    (0x09800+(cnttype)*256+(r2c)*128+(hoslcid)*64+(hoencid))
#define cAf6Reg_hoencpkttypcnt_pen_WidthVal                                                                 32
#define cAf6Reg_hoencpkttypcnt_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: HO_ENC_PktTyp_CNT
BitField Type: RO
BitField Desc: Encap Packet Type Count
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_hoencpkttypcnt_pen_HO_ENC_PktTyp_CNT_Bit_Start                                                  0
#define cAf6_hoencpkttypcnt_pen_HO_ENC_PktTyp_CNT_Bit_End                                                   23
#define cAf6_hoencpkttypcnt_pen_HO_ENC_PktTyp_CNT_Mask                                                cBit23_0
#define cAf6_hoencpkttypcnt_pen_HO_ENC_PktTyp_CNT_Shift                                                      0
#define cAf6_hoencpkttypcnt_pen_HO_ENC_PktTyp_CNT_MaxVal                                              0xffffff
#define cAf6_hoencpkttypcnt_pen_HO_ENC_PktTyp_CNT_MinVal                                                   0x0
#define cAf6_hoencpkttypcnt_pen_HO_ENC_PktTyp_CNT_RstVal                                                  0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM Encap Idlie Byte Counter
Reg Addr   : 0x09C00-0x09CFF
Reg Formula: 0x09C00 + $r2c*128+$hoslc_id*64+$hoenc_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $hoslc_id(0-1): Ho Slice
           + $hoenc_id(0-47): HO ENCAP ID
Reg Desc   :
Encap Idle Byte Counter:

------------------------------------------------------------------------------*/
#define cAf6Reg_hoencidlebytecnt_pen_Base                                                              0x09C00
#define cAf6Reg_hoencidlebytecnt_pen(r2c, hoslcid, hoencid)                  ((r2c * 128) + (hoslcId * 64) + hoencid)
#define cAf6Reg_hoencidlebytecnt_pen_WidthVal                                                               32
#define cAf6Reg_hoencidlebytecnt_pen_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: HO_ENC_IDLE_CNT
BitField Type: RO
BitField Desc: Encap Idle Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_hoencidlebytecnt_pen_HO_ENC_IDLE_CNT_Bit_Start                                                  0
#define cAf6_hoencidlebytecnt_pen_HO_ENC_IDLE_CNT_Bit_End                                                   31
#define cAf6_hoencidlebytecnt_pen_HO_ENC_IDLE_CNT_Mask                                                cBit31_0
#define cAf6_hoencidlebytecnt_pen_HO_ENC_IDLE_CNT_Shift                                                      0
#define cAf6_hoencidlebytecnt_pen_HO_ENC_IDLE_CNT_MaxVal                                            0xffffffff
#define cAf6_hoencidlebytecnt_pen_HO_ENC_IDLE_CNT_MinVal                                                   0x0
#define cAf6_hoencidlebytecnt_pen_HO_ENC_IDLE_CNT_RstVal                                                  0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM LO Encap Packet Counters
Reg Addr   : 0x40000-0x43FFF
Reg Formula: 0x40000+ $r2c*8192+$cnt_type*4096+$loslc_id*1024+$loenc_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $cnt_type(0-1): counter type
           + $loslc_id(0-3): LO Slice
           + $loenc_id(0-1023): LO ENCAP ID}
Reg Desc   :
Support the following Enc Packet counter:
- cnt_type = 0: ENC Packet Good
- cnt_type = 1: ENC Packet total

------------------------------------------------------------------------------*/
#define cAf6Reg_loencpktcnt_pen_Base                                                                   0x40000
#define cAf6Reg_loencpktcnt_pen(r2c, cnttype, loslcid, loencid)       (0x40000+(r2c)*8192+(cnttype)*4096+(loslcid)*1024+(loencid))
#define cAf6Reg_loencpktcnt_pen_WidthVal                                                                    32
#define cAf6Reg_loencpktcnt_pen_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: LO_ENC_PKT_CNT
BitField Type: RO
BitField Desc: Encap Packet Count
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_loencpktcnt_pen_LO_ENC_PKT_CNT_Bit_Start                                                        0
#define cAf6_loencpktcnt_pen_LO_ENC_PKT_CNT_Bit_End                                                         23
#define cAf6_loencpktcnt_pen_LO_ENC_PKT_CNT_Mask                                                      cBit23_0
#define cAf6_loencpktcnt_pen_LO_ENC_PKT_CNT_Shift                                                            0
#define cAf6_loencpktcnt_pen_LO_ENC_PKT_CNT_MaxVal                                                    0xffffff
#define cAf6_loencpktcnt_pen_LO_ENC_PKT_CNT_MinVal                                                         0x0
#define cAf6_loencpktcnt_pen_LO_ENC_PKT_CNT_RstVal                                                        0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM LO Encap Byte Counters
Reg Addr   : 0x44000-0x47FFF
Reg Formula: 0x44000+ $r2c*8192+$cnt_type*4096+$loslc_id*1024+$loenc_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $cnt_type(0-1): counter type
           + $loslc_id(0-3): LO Slice
           + $loenc_id(0-1023): LO ENCAP ID}
Reg Desc   :
Support the following Enc Packet counter:
- cnt_type = 0: ENC Byte Good
- cnt_type = 1: ENC Byte total

------------------------------------------------------------------------------*/
#define cAf6Reg_loencbytecnt_pen_Base                                                                  0x44000
#define cAf6Reg_loencbytecnt_pen(r2c, cnttype, loslcid, loencid)      (0x44000+(r2c)*8192+(cnttype)*4096+(loslcid)*1024+(loencid))
#define cAf6Reg_loencbytecnt_pen_WidthVal                                                                   32
#define cAf6Reg_loencbytecnt_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: LO_ENC_Byte_CNT
BitField Type: RO
BitField Desc: Encap Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_loencbytecnt_pen_LO_ENC_Byte_CNT_Bit_Start                                                      0
#define cAf6_loencbytecnt_pen_LO_ENC_Byte_CNT_Bit_End                                                       31
#define cAf6_loencbytecnt_pen_LO_ENC_Byte_CNT_Mask                                                    cBit31_0
#define cAf6_loencbytecnt_pen_LO_ENC_Byte_CNT_Shift                                                          0
#define cAf6_loencbytecnt_pen_LO_ENC_Byte_CNT_MaxVal                                                0xffffffff
#define cAf6_loencbytecnt_pen_LO_ENC_Byte_CNT_MinVal                                                       0x0
#define cAf6_loencbytecnt_pen_LO_ENC_Byte_CNT_RstVal                                                      0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM LO Encap Packet Type Counters
Reg Addr   : 0x50000-0x57FFF
Reg Formula: 0x50000 +$cnt_type*8192+$r2c*4096+$loslc_id*1024+$loenc_id
    Where  :
           + $cnt_type(0-3): counter type
           + $r2c(0-1): 1 for read to clear
           + $loslc_id(0-3): LO Slice
           + $loenc_id(0-1023): LO ENCAP ID}
Reg Desc   :
Support the following Encap Packet Type Counters:
- cnt_type = 3'd0: ENC Packet fragments
- cnt_type = 3'd1: ENC Packet Plain
- cnt_type = 3'd2: ENC Packet OAM
- cnt_type = 3'd3: ENC Packet Abort

------------------------------------------------------------------------------*/
#define cAf6Reg_loencpkttypcnt_pen_Base                                                                0x50000
#define cAf6Reg_loencpkttypcnt_pen(cnttype, r2c, loslcid, loencid)    (0x50000+(cnttype)*8192+(r2c)*4096+(loslcid)*1024+(loencid))
#define cAf6Reg_loencpkttypcnt_pen_WidthVal                                                                 32
#define cAf6Reg_loencpkttypcnt_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: LO_ENC_PktTyp_CNT
BitField Type: RO
BitField Desc: Encap Packet Type Count
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_loencpkttypcnt_pen_LO_ENC_PktTyp_CNT_Bit_Start                                                  0
#define cAf6_loencpkttypcnt_pen_LO_ENC_PktTyp_CNT_Bit_End                                                   23
#define cAf6_loencpkttypcnt_pen_LO_ENC_PktTyp_CNT_Mask                                                cBit23_0
#define cAf6_loencpkttypcnt_pen_LO_ENC_PktTyp_CNT_Shift                                                      0
#define cAf6_loencpkttypcnt_pen_LO_ENC_PktTyp_CNT_MaxVal                                              0xffffff
#define cAf6_loencpkttypcnt_pen_LO_ENC_PktTyp_CNT_MinVal                                                   0x0
#define cAf6_loencpkttypcnt_pen_LO_ENC_PktTyp_CNT_RstVal                                                  0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM LO Encap Idlie Byte Counter
Reg Addr   : 0x58000-0x59FFF
Reg Formula: 0x58000 + $r2c*4096+$loslc_id*1024+$loenc_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $loslc_id(0-3): LO Slice
           + $loenc_id(0-1023): LO ENCAP ID
Reg Desc   :
Encap Idle Byte Counter:

------------------------------------------------------------------------------*/
#define cAf6Reg_loencidlebytecnt_pen_Base                                                              0x58000
#define cAf6Reg_loencidlebytecnt_pen(r2c, loslcid, loencid)           (0x58000+(r2c)*4096+(loslcid)*1024+(loencid))
#define cAf6Reg_loencidlebytecnt_pen_WidthVal                                                               32
#define cAf6Reg_loencidlebytecnt_pen_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: LO_ENC_IDLE_CNT
BitField Type: RO
BitField Desc: Encap Idle Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_loencidlebytecnt_pen_LO_ENC_IDLE_CNT_Bit_Start                                                  0
#define cAf6_loencidlebytecnt_pen_LO_ENC_IDLE_CNT_Bit_End                                                   31
#define cAf6_loencidlebytecnt_pen_LO_ENC_IDLE_CNT_Mask                                                cBit31_0
#define cAf6_loencidlebytecnt_pen_LO_ENC_IDLE_CNT_Shift                                                      0
#define cAf6_loencidlebytecnt_pen_LO_ENC_IDLE_CNT_MaxVal                                            0xffffffff
#define cAf6_loencidlebytecnt_pen_LO_ENC_IDLE_CNT_MinVal                                                   0x0
#define cAf6_loencidlebytecnt_pen_LO_ENC_IDLE_CNT_RstVal                                                  0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM HO Decap Packet Counters
Reg Addr   : 0x0A000-0x0A1FF
Reg Formula: 0x0A000+ $r2c*256+$cnt_type*128+$hoslc_id*64+$hodec_id
    Where  :
           + $r2c(0-1)     : 1 for read to clear
           + $cnt_type(0-1): counter type
           + $hoslc_id(0-1): HO Slice
           + $hodec_id(0-47) : HO DECAP ID}
Reg Desc   :
Support the following Enc Packet counter:
- cnt_type = 0: HO DEC Packet Good
- cnt_type = 1: HO DEC Packet total

------------------------------------------------------------------------------*/
#define cAf6Reg_hodecpktcnt_pen_Base                                                                   0x0A000
#define cAf6Reg_hodecpktcnt_pen(r2c, cnttype, hoslcid, hodecid)       (0x0A000+(r2c)*256+(cnttype)*128+(hoslcid)*64+(hodecid))
#define cAf6Reg_hodecpktcnt_pen_WidthVal                                                                    32
#define cAf6Reg_hodecpktcnt_pen_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: HO_DEC_PKT_CNT
BitField Type: RO
BitField Desc: Decap Packet Count
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_hodecpktcnt_pen_HO_DEC_PKT_CNT_Bit_Start                                                        0
#define cAf6_hodecpktcnt_pen_HO_DEC_PKT_CNT_Bit_End                                                         23
#define cAf6_hodecpktcnt_pen_HO_DEC_PKT_CNT_Mask                                                      cBit23_0
#define cAf6_hodecpktcnt_pen_HO_DEC_PKT_CNT_Shift                                                            0
#define cAf6_hodecpktcnt_pen_HO_DEC_PKT_CNT_MaxVal                                                    0xffffff
#define cAf6_hodecpktcnt_pen_HO_DEC_PKT_CNT_MinVal                                                         0x0
#define cAf6_hodecpktcnt_pen_HO_DEC_PKT_CNT_RstVal                                                        0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM HO Decap Byte Counters
Reg Addr   : 0x0A200-0x0A3FF
Reg Formula: 0x0A200+ $r2c*256+$cnt_type*128+$hoslc_id*64+$hodec_id
    Where  :
           + $r2c(0-1)     : 1 for read to clear
           + $cnt_type(0-1): counter type
           + $hoslc_id(0-1): HO Slice
           + $hodec_id(0-95) : HO DECAP ID}
Reg Desc   :
Support the following Enc Packet counter:
- cnt_type = 0: HO DEC Byte Good
- cnt_type = 1: HO DEC Byte total

------------------------------------------------------------------------------*/
#define cAf6Reg_hodecbytecnt_pen_Base                                                                  0x0A200
#define cAf6Reg_hodecbytecnt_pen(r2c, cnttype, hoslcid, hodecid)      (0x0A200+(r2c)*256+(cnttype)*128+(hoslcid)*64+(hodecid))
#define cAf6Reg_hodecbytecnt_pen_WidthVal                                                                   32
#define cAf6Reg_hodecbytecnt_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: HO_DEC_Byte_CNT
BitField Type: RO
BitField Desc: Decap Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_hodecbytecnt_pen_HO_DEC_Byte_CNT_Bit_Start                                                      0
#define cAf6_hodecbytecnt_pen_HO_DEC_Byte_CNT_Bit_End                                                       31
#define cAf6_hodecbytecnt_pen_HO_DEC_Byte_CNT_Mask                                                    cBit31_0
#define cAf6_hodecbytecnt_pen_HO_DEC_Byte_CNT_Shift                                                          0
#define cAf6_hodecbytecnt_pen_HO_DEC_Byte_CNT_MaxVal                                                0xffffffff
#define cAf6_hodecbytecnt_pen_HO_DEC_Byte_CNT_MinVal                                                       0x0
#define cAf6_hodecbytecnt_pen_HO_DEC_Byte_CNT_RstVal                                                      0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM HO Decap Packet Type Counters
Reg Addr   : 0x0A800-0x0AEFF
Reg Formula: 0x0A800 +$cnt_type*256+$r2c*128+$hoslc_id*64+$hodec_id
    Where  :
           + $cnt_type(0-6): counter type
           + $r2c(0-1): 1 for read to clear
           + $hoslc_id(0-1): HO Slice
           + $hodec_id(0-47): HO DECAP ID}
Reg Desc   :
Support the following Decap Packet Type Counters:
- cnt_type = 3'd0: DEC Packet fragments
- cnt_type = 3'd1: DEC Packet Plain
- cnt_type = 3'd2: DEC Packet OAM
- cnt_type = 3'd3: DEC Packet Abort
- cnt_type = 3'd4: DEC FCS Error
- cnt_type = 3'd5: DEC Discarded Packet
- cnt_type = 3'd6: DEC MRU Packet

------------------------------------------------------------------------------*/
#define cAf6Reg_hodecpkttypcnt_pen_Base                                                                0x0A800
#define cAf6Reg_hodecpkttypcnt_pen(cnttype, r2c, hoslcid, hodecid)    (0x0A800+(cnttype)*256+(r2c)*128+(hoslcid)*64+(hodecid))
#define cAf6Reg_hodecpkttypcnt_pen_WidthVal                                                                 32
#define cAf6Reg_hodecpkttypcnt_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: HO_DEC_PktTyp_CNT
BitField Type: RO
BitField Desc: Decap Packet Type Count
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_hodecpkttypcnt_pen_HO_DEC_PktTyp_CNT_Bit_Start                                                  0
#define cAf6_hodecpkttypcnt_pen_HO_DEC_PktTyp_CNT_Bit_End                                                   23
#define cAf6_hodecpkttypcnt_pen_HO_DEC_PktTyp_CNT_Mask                                                cBit23_0
#define cAf6_hodecpkttypcnt_pen_HO_DEC_PktTyp_CNT_Shift                                                      0
#define cAf6_hodecpkttypcnt_pen_HO_DEC_PktTyp_CNT_MaxVal                                              0xffffff
#define cAf6_hodecpkttypcnt_pen_HO_DEC_PktTyp_CNT_MinVal                                                   0x0
#define cAf6_hodecpkttypcnt_pen_HO_DEC_PktTyp_CNT_RstVal                                                  0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM Decap Idlie Byte Counter
Reg Addr   : 0x0AF00-0x0AFFF
Reg Formula: 0x0AF00 + $r2c*128+$hoslc_id*64+$hodec_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $hoslc_id(0-1): Ho Slice
           + $hodec_id(0-47): HO DECAP ID
Reg Desc   :
Decap Idle Byte Counter:

------------------------------------------------------------------------------*/
#define cAf6Reg_hodecidlebytecnt_pen_Base                                                              0x0AF00
#define cAf6Reg_hodecidlebytecnt_pen(r2c, hoslcid, hodecid)                               ((r2c*128) + (hoslcid *64) + hodecid)
#define cAf6Reg_hodecidlebytecnt_pen_WidthVal                                                               32
#define cAf6Reg_hodecidlebytecnt_pen_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: HO_DEC_IDLE_CNT
BitField Type: RO
BitField Desc: Decap Idle Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_hodecidlebytecnt_pen_HO_DEC_IDLE_CNT_Bit_Start                                                  0
#define cAf6_hodecidlebytecnt_pen_HO_DEC_IDLE_CNT_Bit_End                                                   31
#define cAf6_hodecidlebytecnt_pen_HO_DEC_IDLE_CNT_Mask                                                cBit31_0
#define cAf6_hodecidlebytecnt_pen_HO_DEC_IDLE_CNT_Shift                                                      0
#define cAf6_hodecidlebytecnt_pen_HO_DEC_IDLE_CNT_MaxVal                                            0xffffffff
#define cAf6_hodecidlebytecnt_pen_HO_DEC_IDLE_CNT_MinVal                                                   0x0
#define cAf6_hodecidlebytecnt_pen_HO_DEC_IDLE_CNT_RstVal                                                  0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM LO Decap Packet Counters
Reg Addr   : 0x60000-0x63FFF
Reg Formula: 0x60000+ $r2c*8192+$cnt_type*4096+$loslc_id*1024+$lodec_id
    Where  :
           + $r2c(0-1) : 1 for read to clear
           + $cnt_type(0-1) : counter type
           + $loslc_id(0-3): LO Slice
           + $lodec_id(0-1023): LO DECAP ID}
Reg Desc   :
Support the following Enc Packet counter:
- cnt_type = 0: DEC Packet Good
- cnt_type = 1: DEC Packet total

------------------------------------------------------------------------------*/
#define cAf6Reg_lodecpktcnt_pen_Base                                                                   0x60000
#define cAf6Reg_lodecpktcnt_pen(r2c, cnttype, loslcid, lodecid)       (0x60000+(r2c)*8192+(cnttype)*4096+(loslcid)*1024+(lodecid))
#define cAf6Reg_lodecpktcnt_pen_WidthVal                                                                    32
#define cAf6Reg_lodecpktcnt_pen_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: LO_DEC_PKT_CNT
BitField Type: RO
BitField Desc: Decap Packet Count
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_lodecpktcnt_pen_LO_DEC_PKT_CNT_Bit_Start                                                        0
#define cAf6_lodecpktcnt_pen_LO_DEC_PKT_CNT_Bit_End                                                         23
#define cAf6_lodecpktcnt_pen_LO_DEC_PKT_CNT_Mask                                                      cBit23_0
#define cAf6_lodecpktcnt_pen_LO_DEC_PKT_CNT_Shift                                                            0
#define cAf6_lodecpktcnt_pen_LO_DEC_PKT_CNT_MaxVal                                                    0xffffff
#define cAf6_lodecpktcnt_pen_LO_DEC_PKT_CNT_MinVal                                                         0x0
#define cAf6_lodecpktcnt_pen_LO_DEC_PKT_CNT_RstVal                                                        0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM LO Decap Byte Counters
Reg Addr   : 0x64000-0x67FFF
Reg Formula: 0x64000+ $r2c*8192+$cnt_type*4096+$loslc_id*1024+$lodec_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $cnt_type(0-1): counter type
           + $loslc_id(0-3): LO Slice
           + $lodec_id(0-1023): LO DECAP ID}
Reg Desc   :
Support the following Decap Byte counters:
- cnt_type = 0: DEC Byte Good
- cnt_type = 1: DEC Byte total

------------------------------------------------------------------------------*/
#define cAf6Reg_lodecbytecnt_pen_Base                                                                  0x64000
#define cAf6Reg_lodecbytecnt_pen(r2c, cnttype, loslcid, lodecid)      (0x64000+(r2c)*8192+(cnttype)*4096+(loslcid)*1024+(lodecid))
#define cAf6Reg_lodecbytecnt_pen_WidthVal                                                                   32
#define cAf6Reg_lodecbytecnt_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: LO_DEC_Byte_CNT
BitField Type: RO
BitField Desc: Decap Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lodecbytecnt_pen_LO_DEC_Byte_CNT_Bit_Start                                                      0
#define cAf6_lodecbytecnt_pen_LO_DEC_Byte_CNT_Bit_End                                                       31
#define cAf6_lodecbytecnt_pen_LO_DEC_Byte_CNT_Mask                                                    cBit31_0
#define cAf6_lodecbytecnt_pen_LO_DEC_Byte_CNT_Shift                                                          0
#define cAf6_lodecbytecnt_pen_LO_DEC_Byte_CNT_MaxVal                                                0xffffffff
#define cAf6_lodecbytecnt_pen_LO_DEC_Byte_CNT_MinVal                                                       0x0
#define cAf6_lodecbytecnt_pen_LO_DEC_Byte_CNT_RstVal                                                      0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM LO Decap Packet Type Counters
Reg Addr   : 0x70000-0x7DFFF
Reg Formula: 0x70000 + $cnt_type*8192+$r2c*4096+$loslc_id*1024+$lodec_id
    Where  :
           + $cnt_type(0-6): counter type
           + $r2c(0-1): 1 for read to clear
           + $loslc_id(0-3): LO Slice
           + $lodec_id(0-1023): LO DECAP ID}
Reg Desc   :
Support the following Decap Packet Type counters:
- cnt_type = 3'd0: Dec Packet fragments
- cnt_type = 3'd1: Dec Packet Plain
- cnt_type = 3'd2: Dec Packet OAM
- cnt_type = 3'd3: Dec Packet Abort
- cnt_type = 3'd4: Dec FCS Error
- cnt_type = 3'd5: DEC Discarded Packet
- cnt_type = 3'd6: DEC MRU Packet

------------------------------------------------------------------------------*/
#define cAf6Reg_lodecpkttypcnt_pen_Base                                                                0x70000
#define cAf6Reg_lodecpkttypcnt_pen(cnttype, r2c, loslcid, lodecid)    (0x70000+(cnttype)*8192+(r2c)*4096+(loslcid)*1024+(lodecid))
#define cAf6Reg_lodecpkttypcnt_pen_WidthVal                                                                 32
#define cAf6Reg_lodecpkttypcnt_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: LO_DEC_PktTyp_CNT
BitField Type: RO
BitField Desc: Decap Packet Type Counter
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_lodecpkttypcnt_pen_LO_DEC_PktTyp_CNT_Bit_Start                                                  0
#define cAf6_lodecpkttypcnt_pen_LO_DEC_PktTyp_CNT_Bit_End                                                   23
#define cAf6_lodecpkttypcnt_pen_LO_DEC_PktTyp_CNT_Mask                                                cBit23_0
#define cAf6_lodecpkttypcnt_pen_LO_DEC_PktTyp_CNT_Shift                                                      0
#define cAf6_lodecpkttypcnt_pen_LO_DEC_PktTyp_CNT_MaxVal                                              0xffffff
#define cAf6_lodecpkttypcnt_pen_LO_DEC_PktTyp_CNT_MinVal                                                   0x0
#define cAf6_lodecpkttypcnt_pen_LO_DEC_PktTyp_CNT_RstVal                                                  0x00


/*------------------------------------------------------------------------------
Reg Name   : TDM Decap Idlie Byte Counter
Reg Addr   : 0x7E000-0x7FFFF
Reg Formula: 0x7E000 + $r2c*4096+$loslc_id*1024+$lodec_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $loslc_id(0-3): LO Slice
           + $lodec_id(0-1023): LO DECAP ID
Reg Desc   :
Decap Idle Byte Counter:

------------------------------------------------------------------------------*/
#define cAf6Reg_lodecidlebytecnt_pen_Base                                                              0x7E000
#define cAf6Reg_lodecidlebytecnt_pen(r2c, loslcid, lodecid)           (0x7E000+(r2c)*4096+(loslcid)*1024+(lodecid))
#define cAf6Reg_lodecidlebytecnt_pen_WidthVal                                                               32
#define cAf6Reg_lodecidlebytecnt_pen_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: LO_DEC_IDLE_CNT
BitField Type: RO
BitField Desc: Decap Idle Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lodecidlebytecnt_pen_LO_DEC_IDLE_CNT_Bit_Start                                                  0
#define cAf6_lodecidlebytecnt_pen_LO_DEC_IDLE_CNT_Bit_End                                                   31
#define cAf6_lodecidlebytecnt_pen_LO_DEC_IDLE_CNT_Mask                                                cBit31_0
#define cAf6_lodecidlebytecnt_pen_LO_DEC_IDLE_CNT_Shift                                                      0
#define cAf6_lodecidlebytecnt_pen_LO_DEC_IDLE_CNT_MaxVal                                            0xffffffff
#define cAf6_lodecidlebytecnt_pen_LO_DEC_IDLE_CNT_MinVal                                                   0x0
#define cAf6_lodecidlebytecnt_pen_LO_DEC_IDLE_CNT_RstVal                                                  0x00


/*------------------------------------------------------------------------------
Reg Name   : Tx Bundle Packet Counter
Reg Addr   : 0x03000-0x0307F
Reg Formula: 0x03000+ $r2c*64+$cnt_type*32+$cnt_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $cnt_type(0-1): counter type
           + $cnt_id(0-31): Bundle Pool ID
Reg Desc   :
Support the following Tx Bundle Packet counter:
- cnt_type = 0: Tx Bundle Packet Good
- cnt_type = 1: Tx Bundle Packet Discard

------------------------------------------------------------------------------*/
#define cAf6Reg_txbndpktcnt_pen_Base                                                                   0x03000
#define cAf6Reg_txbndpktcnt_pen(r2c, cnttype, cntid)                   (0x03000+(r2c)*64+(cnttype)*32+(cntid))
#define cAf6Reg_txbndpktcnt_pen_WidthVal                                                                    32
#define cAf6Reg_txbndpktcnt_pen_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: TxBND_PKT_CNT
BitField Type: RO
BitField Desc: Tx Bundle Packet Count
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_txbndpktcnt_pen_TxBND_PKT_CNT_Bit_Start                                                         0
#define cAf6_txbndpktcnt_pen_TxBND_PKT_CNT_Bit_End                                                          23
#define cAf6_txbndpktcnt_pen_TxBND_PKT_CNT_Mask                                                       cBit23_0
#define cAf6_txbndpktcnt_pen_TxBND_PKT_CNT_Shift                                                             0
#define cAf6_txbndpktcnt_pen_TxBND_PKT_CNT_MaxVal                                                     0xffffff
#define cAf6_txbndpktcnt_pen_TxBND_PKT_CNT_MinVal                                                          0x0
#define cAf6_txbndpktcnt_pen_TxBND_PKT_CNT_RstVal                                                         0x00


/*------------------------------------------------------------------------------
Reg Name   : Tx Bundle Byte Counters
Reg Addr   : 0x03100-0x0317F
Reg Formula: 0x03100+ $r2c*64+$cnt_type*32+$cnt_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $cnt_type(0-1): counter type
           + $cnt_id(0-31): Bundle Pool ID
Reg Desc   :
Support the following Tx Bundle Byte counter:
- cnt_type = 0: Tx Bundle Byte Good
- cnt_type = 1: Tx Bundle Byte Discard

------------------------------------------------------------------------------*/
#define cAf6Reg_txbndbytecnt_pen_Base                                                                  0x03100
#define cAf6Reg_txbndbytecnt_pen(r2c, cnttype, cntid)                  (0x03100+(r2c)*64+(cnttype)*32+(cntid))
#define cAf6Reg_txbndbytecnt_pen_WidthVal                                                                   32
#define cAf6Reg_txbndbytecnt_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: TxBND_Byte_CNT
BitField Type: RO
BitField Desc: Tx Bundle Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_txbndbytecnt_pen_TxBND_Byte_CNT_Bit_Start                                                       0
#define cAf6_txbndbytecnt_pen_TxBND_Byte_CNT_Bit_End                                                        31
#define cAf6_txbndbytecnt_pen_TxBND_Byte_CNT_Mask                                                     cBit31_0
#define cAf6_txbndbytecnt_pen_TxBND_Byte_CNT_Shift                                                           0
#define cAf6_txbndbytecnt_pen_TxBND_Byte_CNT_MaxVal                                                 0xffffffff
#define cAf6_txbndbytecnt_pen_TxBND_Byte_CNT_MinVal                                                        0x0
#define cAf6_txbndbytecnt_pen_TxBND_Byte_CNT_RstVal                                                       0x00


/*------------------------------------------------------------------------------
Reg Name   : Rx Bundle Packet Counter
Reg Addr   : 0x04000-0x0407F
Reg Formula: 0x04000+ $r2c*64+$cnt_type*32+$cnt_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $cnt_type(0-1): counter type
           + $cnt_id(0-31): Bundle Pool ID
Reg Desc   :
Support the following Rx Bundle Packet counter:
- cnt_type = 0: Rx Bundle Packet Good
- cnt_type = 1: Rx Bundle Packet Discard

------------------------------------------------------------------------------*/
#define cAf6Reg_rxbndpktcnt_pen_Base                                                                   0x04000
#define cAf6Reg_rxbndpktcnt_pen(r2c, cnttype, cntid)                   (0x04000+(r2c)*64+(cnttype)*32+(cntid))
#define cAf6Reg_rxbndpktcnt_pen_WidthVal                                                                    32
#define cAf6Reg_rxbndpktcnt_pen_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: RxBND_PKT_CNT
BitField Type: RO
BitField Desc: Rx Bundle Packet Count
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_rxbndpktcnt_pen_RxBND_PKT_CNT_Bit_Start                                                         0
#define cAf6_rxbndpktcnt_pen_RxBND_PKT_CNT_Bit_End                                                          23
#define cAf6_rxbndpktcnt_pen_RxBND_PKT_CNT_Mask                                                       cBit23_0
#define cAf6_rxbndpktcnt_pen_RxBND_PKT_CNT_Shift                                                             0
#define cAf6_rxbndpktcnt_pen_RxBND_PKT_CNT_MaxVal                                                     0xffffff
#define cAf6_rxbndpktcnt_pen_RxBND_PKT_CNT_MinVal                                                          0x0
#define cAf6_rxbndpktcnt_pen_RxBND_PKT_CNT_RstVal                                                         0x00


/*------------------------------------------------------------------------------
Reg Name   : Rx Bundle Byte Counter
Reg Addr   : 0x04100-0x0417F
Reg Formula: 0x04100+ $r2c*64+$cnt_type*32+$cnt_id
    Where  :
           + $r2c(0-1): 1 for read to clear
           + $cnt_type(0-1): counter type
           + $cnt_id(0-31): Bundle Pool ID
Reg Desc   :
Support the following Rx Bundle Byte counter:
- cnt_type = 0: Rx Bundle Byte Good
- cnt_type = 1: Rx Bundle Byte Discard

------------------------------------------------------------------------------*/
#define cAf6Reg_rxbndbytecnt_pen_Base                                                                  0x04100
#define cAf6Reg_rxbndbytecnt_pen(r2c, cnttype, cntid)                  (0x04100+(r2c)*64+(cnttype)*32+(cntid))
#define cAf6Reg_rxbndbytecnt_pen_WidthVal                                                                   32
#define cAf6Reg_rxbndbytecnt_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: RxBND_Byte_CNT
BitField Type: RO
BitField Desc: Rx Bundle Byte Count
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_rxbndbytecnt_pen_RxBND_Byte_CNT_Bit_Start                                                       0
#define cAf6_rxbndbytecnt_pen_RxBND_Byte_CNT_Bit_End                                                        31
#define cAf6_rxbndbytecnt_pen_RxBND_Byte_CNT_Mask                                                     cBit31_0
#define cAf6_rxbndbytecnt_pen_RxBND_Byte_CNT_Shift                                                           0
#define cAf6_rxbndbytecnt_pen_RxBND_Byte_CNT_MaxVal                                                 0xffffffff
#define cAf6_rxbndbytecnt_pen_RxBND_Byte_CNT_MinVal                                                        0x0
#define cAf6_rxbndbytecnt_pen_RxBND_Byte_CNT_RstVal                                                       0x00


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Ingress Packet Type Counters
Reg Addr   : 0x08000-0x08FFF
Reg Formula: 0x08000+$ethmod*1024+$r2c*512+$cnt_type*32+$cnt_id
    Where  :
           + $ethmod(0-1): 1 to show ETH counters
           + $r2c(0-1): 1 for read to clear
           + $cnt_type(0-15): counter type
           + $cnt_id(0-31): Ethernet Pool ID
Reg Desc   :
Support the following Ingress type counters:
With ethmod = {0}
- cnt_type = 4'd0 : PWE Transmit Packet Counter
- cnt_type = 4'd1 : PWE Transmit Byte Counter
- cnt_type = 4'd2 : PWE Transmit Packet Fragment
- cnt_type = 4'd3 : PWE Transmit Lbit Packet
- cnt_type = 4'd4 : PWE Transmit Rbit Packet
- cnt_type = 4'd5 : PWE Transmit Mbit Packet
- cnt_type = 4'd6 : PWE Transmit Pbit Packet
- cnt_type = 4'd7-4'd15: Reserved
With ethmod = {1}
- cnt_type = 4'd0 : Ethernet Ingress Packet Counter
- cnt_type = 4'd1 : Ethernet Ingress Discarded Packet Counter
- cnt_type = 4'd2 : Ethernet Ingress Byte Counter
- cnt_type = 4'd3 : Ethernet Ingress Discarded Byte Counter
- cnt_type = 4'd4 : Ethernet Ingress BECN Packet Counter
- cnt_type = 4'd5 : Ethernet Ingress FECN Packet Counter
- cnt_type = 4'd6 : Ethernet Ingress DE Packet Counter
- cnt_type = 4'd7 : Ethernet Ingress MRU Packet Counter
- cnt_type = 4'd8 : Ethernet Ingress Fragment Counter
- cnt_type = 4'd9-4'd15: Reserved

------------------------------------------------------------------------------*/
#define cAf6Reg_igethpkttypcnt_pen_Base                                                                0x08000
#define cAf6Reg_igethpkttypcnt_pen(ethmod, r2c, cnttype, cntid)       (0x08000+(ethmod)*1024+(r2c)*512+(cnttype)*32+(cntid))
#define cAf6Reg_igethpkttypcnt_pen_WidthVal                                                                 32
#define cAf6Reg_igethpkttypcnt_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: IGETH_PktTyp_CNT
BitField Type: RO
BitField Desc: Ingress Packet Type Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_igethpkttypcnt_pen_IGETH_PktTyp_CNT_Bit_Start                                                   0
#define cAf6_igethpkttypcnt_pen_IGETH_PktTyp_CNT_Bit_End                                                    31
#define cAf6_igethpkttypcnt_pen_IGETH_PktTyp_CNT_Mask                                                 cBit31_0
#define cAf6_igethpkttypcnt_pen_IGETH_PktTyp_CNT_Shift                                                       0
#define cAf6_igethpkttypcnt_pen_IGETH_PktTyp_CNT_MaxVal                                             0xffffffff
#define cAf6_igethpkttypcnt_pen_IGETH_PktTyp_CNT_MinVal                                                    0x0
#define cAf6_igethpkttypcnt_pen_IGETH_PktTyp_CNT_RstVal                                                   0x00


#define cAf6RegDebugPwCntTypeRxPackets         0x0UL
#define cAf6RegDebugPwCntTypeRxPayloadBytes    0x1UL
#define cAf6RegDebugPwCntTypeRxLofsPackets     0x2UL
#define cAf6RegDebugPwCntTypeRxLbitPackets     0x3UL
#define cAf6RegDebugPwCntTypeRxNbitPackets     0x4UL
#define cAf6RegDebugPwCntTypeRxPbitPackets     0x5UL
#define cAf6RegDebugPwCntTypeRxRbitPackets     0x6UL
#define cAf6RegDebugPwCntTypeRxLatePackets     0x7UL
#define cAf6RegDebugPwCntTypeRxEarlyPackets    0x8UL
#define cAf6RegDebugPwCntTypeRxLostPackets     0x9UL
#define cAf6RegDebugPwCntTypeRxOverrunPackets  0xAUL
#define cAf6RegDebugPwCntTypeRxUnderrunPackets 0xBUL
#define cAf6RegDebugPwCntTypeRxMalformPackets  0xCUL
#define cAf6RegDebugPwCntTypeRxStrayPackets    0xDUL
/*------------------------------------------------------------------------------
Reg Name   : Ethernet Egress Packet Type Counters
Reg Addr   : 0x06000-0x067FF
Reg Formula: 0x06000+$ethmod*1024+$cnt_type*64+$r2c*32+$cnt_id
    Where  :
           + $ethmod(0-1): 1 to show ETH counters
           + $cnt_type(0-15): counter type
           + $r2c(0-1): 1 for read to clear
           + $cnt_id(0-31): Ethernet Pool ID
Reg Desc   :
Support the following Egress Ethernet Type counters:
With ethmod = {0}
- cnt_type = 4'd0 : PWE Receive Packet Counter
- cnt_type = 4'd1 : PWE Receive Byte Counter
- cnt_type = 4'd2 : PWE Receive Lofs
- cnt_type = 4'd3 : PWE Receive Lbit Packet
- cnt_type = 4'd4 : PWE Receive Nbit Packet
- cnt_type = 4'd5 : PWE Receive Pbit Packet
- cnt_type = 4'd6 : PWE Receive Rbit Packet
- cnt_type = 4'd7 : PWE Receive Late Packet
- cnt_type = 4'd8 : PWE Receive Early Packet
- cnt_type = 4'd9 : PWE Receive Lost Packet
- cnt_type = 4'd10: PWE Receive Overrun
- cnt_type = 4'd11: PWE Receive Underrun
- cnt_type = 4'd12: PWE Receive Malform Packet
- cnt_type = 4'd13: PWE Receive Stray Packet
- cnt_type = 4'd14: Reserved
- cnt_type = 4'd15: Reserved
With ethmod = {1}
- cnt_type = 4'd0 : Ethernet Egress Packet Counter
- cnt_type = 4'd1 : Ethernet Egress Discarded Packet Counter
- cnt_type = 4'd2 : Ethernet Egress Byte Counter
- cnt_type = 4'd3 : Ethernet Egress Discarded Byte Counter
- cnt_type = 4'd4 : Ethernet Egress BECN Packet Counter
- cnt_type = 4'd5 : Ethernet Egress FECN Packet Counter
- cnt_type = 4'd6 : Ethernet Egress DE Packet Counter
- cnt_type = 4'd7 : Ethernet Egress MTU Packet Counter
- cnt_type = 4'd8 : Ethernet Egress Fragment Counter
- cnt_type = 4'd9-4'd15: Reserved

------------------------------------------------------------------------------*/
#define cAf6Reg_egethpkttypcnt_pen_Base                                                                0x06000
#define cAf6Reg_egethpkttypcnt_pen(ethmod, cnttype, r2c, cntid)       (0x06000+(ethmod)*1024+(cnttype)*64+(r2c)*32+(cntid))
#define cAf6Reg_egethpkttypcnt_pen_WidthVal                                                                 32
#define cAf6Reg_egethpkttypcnt_pen_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: EGETH_CNT
BitField Type: RO
BitField Desc: Egress Ethernet Counters
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_egethpkttypcnt_pen_EGETH_CNT_Bit_Start                                                          0
#define cAf6_egethpkttypcnt_pen_EGETH_CNT_Bit_End                                                           31
#define cAf6_egethpkttypcnt_pen_EGETH_CNT_Mask                                                        cBit31_0
#define cAf6_egethpkttypcnt_pen_EGETH_CNT_Shift                                                              0
#define cAf6_egethpkttypcnt_pen_EGETH_CNT_MaxVal                                                    0xffffffff
#define cAf6_egethpkttypcnt_pen_EGETH_CNT_MinVal                                                           0x0
#define cAf6_egethpkttypcnt_pen_EGETH_CNT_RstVal                                                          0x00

#define cAf6RegDebugPwCntTypeTxPackets         0x0UL
#define cAf6RegDebugPwCntTypeTxPayloadBytes    0x1UL
#define cAf6RegDebugPwCntTypeTxPacketFragments 0x2UL
#define cAf6RegDebugPwCntTypeTxLbitPackets     0x3UL
#define cAf6RegDebugPwCntTypeTxRbitPackets     0x4UL
#define cAf6RegDebugPwCntTypeTxMbitPackets     0x5UL
#define cAf6RegDebugPwCntTypeTxPbitPackets     0x6UL

/*------------------------------------------------------------------------------
Reg Name   : Ethernet Full Counters
Reg Addr   : 0x20000-0x3FFFF
Reg Formula: 0x20000+$cnt_type*8192+$r2c*4096+$cnt_id
    Where  :
           + $cnt_type(0-15): counter type
           + $r2c(0-1): 1 for read to clear
           + $cnt_id(0-4095): Ethernet Flow ID
Reg Desc   :
Full 4096 Ethernet Flow Counters,support the following:

------------------------------------------------------------------------------*/
#define cAf6Reg_ethfulltypcnt_pen_Base                                                                 0x20000
#define cAf6Reg_ethfulltypcnt_pen(cnttype, r2c, cntid)                (0x20000+(cnttype)*8192+(r2c)*4096+(cntid))
#define cAf6Reg_ethfulltypcnt_pen_WidthVal                                                                  32
#define cAf6Reg_ethfulltypcnt_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ETH_CNT
BitField Type: RO
BitField Desc: Ethernet Counters
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ethfulltypcnt_pen_ETH_CNT_Bit_Start                                                             0
#define cAf6_ethfulltypcnt_pen_ETH_CNT_Bit_End                                                              31
#define cAf6_ethfulltypcnt_pen_ETH_CNT_Mask                                                           cBit31_0
#define cAf6_ethfulltypcnt_pen_ETH_CNT_Shift                                                                 0
#define cAf6_ethfulltypcnt_pen_ETH_CNT_MaxVal                                                       0xffffffff
#define cAf6_ethfulltypcnt_pen_ETH_CNT_MinVal                                                              0x0
#define cAf6_ethfulltypcnt_pen_ETH_CNT_RstVal                                                             0x00


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Full Counters Mask
Reg Addr   : 0x00004
Reg Formula:
    Where  :
Reg Desc   :
This register used to enable/disable Ethernet Counters(0-15):

------------------------------------------------------------------------------*/
#define cAf6Reg_ethfullcntmsk_pen_Base                                                                 0x00004
#define cAf6Reg_ethfullcntmsk_pen                                                                      0x00004
#define cAf6Reg_ethfullcntmsk_pen_WidthVal                                                                  32
#define cAf6Reg_ethfullcntmsk_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CNT_ENA
BitField Type: R/W
BitField Desc: Enable Counter(0-15)
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_ethfullcntmsk_pen_CNT_ENA_Bit_Start                                                             0
#define cAf6_ethfullcntmsk_pen_CNT_ENA_Bit_End                                                              15
#define cAf6_ethfullcntmsk_pen_CNT_ENA_Mask                                                           cBit15_0
#define cAf6_ethfullcntmsk_pen_CNT_ENA_Shift                                                                 0
#define cAf6_ethfullcntmsk_pen_CNT_ENA_MaxVal                                                           0xffff
#define cAf6_ethfullcntmsk_pen_CNT_ENA_MinVal                                                              0x0
#define cAf6_ethfullcntmsk_pen_CNT_ENA_RstVal                                                             0x00


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Full Counters Event Group Configuration
Reg Addr   : 0x00005
Reg Formula:
    Where  :
Reg Desc   :
This register used to select event group for each couter(0-15).
{2'b00}: PWE Receive Event
{2'b01}: Egress MPEG Event
{2'b10}: PWE Transmit Event
{2'b11}: Ingress MPIG Event


------------------------------------------------------------------------------*/
#define cAf6Reg_ethfullcntsel_pen_Base                                                                 0x00005
#define cAf6Reg_ethfullcntsel_pen                                                                      0x00005
#define cAf6Reg_ethfullcntsel_pen_WidthVal                                                                  32
#define cAf6Reg_ethfullcntsel_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CNT_GRP15
BitField Type: R/W
BitField Desc: Event Group for Counter #15
BitField Bits: [31:30]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP15_Bit_Start                                                          30
#define cAf6_ethfullcntsel_pen_CNT_GRP15_Bit_End                                                            31
#define cAf6_ethfullcntsel_pen_CNT_GRP15_Mask                                                        cBit31_30
#define cAf6_ethfullcntsel_pen_CNT_GRP15_Shift                                                              30
#define cAf6_ethfullcntsel_pen_CNT_GRP15_MaxVal                                                            0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP15_MinVal                                                            0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP15_RstVal                                                           0x00

/*--------------------------------------
BitField Name: CNT_GRP14
BitField Type: R/W
BitField Desc: Event Group for Counter #14
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP14_Bit_Start                                                          28
#define cAf6_ethfullcntsel_pen_CNT_GRP14_Bit_End                                                            29
#define cAf6_ethfullcntsel_pen_CNT_GRP14_Mask                                                        cBit29_28
#define cAf6_ethfullcntsel_pen_CNT_GRP14_Shift                                                              28
#define cAf6_ethfullcntsel_pen_CNT_GRP14_MaxVal                                                            0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP14_MinVal                                                            0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP14_RstVal                                                           0x00

/*--------------------------------------
BitField Name: CNT_GRP13
BitField Type: R/W
BitField Desc: Event Group for Counter #13
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP13_Bit_Start                                                          26
#define cAf6_ethfullcntsel_pen_CNT_GRP13_Bit_End                                                            27
#define cAf6_ethfullcntsel_pen_CNT_GRP13_Mask                                                        cBit27_26
#define cAf6_ethfullcntsel_pen_CNT_GRP13_Shift                                                              26
#define cAf6_ethfullcntsel_pen_CNT_GRP13_MaxVal                                                            0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP13_MinVal                                                            0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP13_RstVal                                                           0x00

/*--------------------------------------
BitField Name: CNT_GRP12
BitField Type: R/W
BitField Desc: Event Group for Counter #12
BitField Bits: [25:24]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP12_Bit_Start                                                          24
#define cAf6_ethfullcntsel_pen_CNT_GRP12_Bit_End                                                            25
#define cAf6_ethfullcntsel_pen_CNT_GRP12_Mask                                                        cBit25_24
#define cAf6_ethfullcntsel_pen_CNT_GRP12_Shift                                                              24
#define cAf6_ethfullcntsel_pen_CNT_GRP12_MaxVal                                                            0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP12_MinVal                                                            0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP12_RstVal                                                           0x00

/*--------------------------------------
BitField Name: CNT_GRP11
BitField Type: R/W
BitField Desc: Event Group for Counter #11
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP11_Bit_Start                                                          22
#define cAf6_ethfullcntsel_pen_CNT_GRP11_Bit_End                                                            23
#define cAf6_ethfullcntsel_pen_CNT_GRP11_Mask                                                        cBit23_22
#define cAf6_ethfullcntsel_pen_CNT_GRP11_Shift                                                              22
#define cAf6_ethfullcntsel_pen_CNT_GRP11_MaxVal                                                            0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP11_MinVal                                                            0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP11_RstVal                                                           0x00

/*--------------------------------------
BitField Name: CNT_GRP10
BitField Type: R/W
BitField Desc: Event Group for Counter #10
BitField Bits: [21:20]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP10_Bit_Start                                                          20
#define cAf6_ethfullcntsel_pen_CNT_GRP10_Bit_End                                                            21
#define cAf6_ethfullcntsel_pen_CNT_GRP10_Mask                                                        cBit21_20
#define cAf6_ethfullcntsel_pen_CNT_GRP10_Shift                                                              20
#define cAf6_ethfullcntsel_pen_CNT_GRP10_MaxVal                                                            0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP10_MinVal                                                            0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP10_RstVal                                                           0x00

/*--------------------------------------
BitField Name: CNT_GRP9
BitField Type: R/W
BitField Desc: Event Group for Counter #9
BitField Bits: [19:18]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP9_Bit_Start                                                           18
#define cAf6_ethfullcntsel_pen_CNT_GRP9_Bit_End                                                             19
#define cAf6_ethfullcntsel_pen_CNT_GRP9_Mask                                                         cBit19_18
#define cAf6_ethfullcntsel_pen_CNT_GRP9_Shift                                                               18
#define cAf6_ethfullcntsel_pen_CNT_GRP9_MaxVal                                                             0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP9_MinVal                                                             0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP9_RstVal                                                            0x00

/*--------------------------------------
BitField Name: CNT_GRP8
BitField Type: R/W
BitField Desc: Event Group for Counter #8
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP8_Bit_Start                                                           16
#define cAf6_ethfullcntsel_pen_CNT_GRP8_Bit_End                                                             17
#define cAf6_ethfullcntsel_pen_CNT_GRP8_Mask                                                         cBit17_16
#define cAf6_ethfullcntsel_pen_CNT_GRP8_Shift                                                               16
#define cAf6_ethfullcntsel_pen_CNT_GRP8_MaxVal                                                             0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP8_MinVal                                                             0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP8_RstVal                                                            0x00

/*--------------------------------------
BitField Name: CNT_GRP7
BitField Type: R/W
BitField Desc: Event Group for Counter #7
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP7_Bit_Start                                                           14
#define cAf6_ethfullcntsel_pen_CNT_GRP7_Bit_End                                                             15
#define cAf6_ethfullcntsel_pen_CNT_GRP7_Mask                                                         cBit15_14
#define cAf6_ethfullcntsel_pen_CNT_GRP7_Shift                                                               14
#define cAf6_ethfullcntsel_pen_CNT_GRP7_MaxVal                                                             0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP7_MinVal                                                             0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP7_RstVal                                                            0x00

/*--------------------------------------
BitField Name: CNT_GRP6
BitField Type: R/W
BitField Desc: Event Group for Counter #6
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP6_Bit_Start                                                           12
#define cAf6_ethfullcntsel_pen_CNT_GRP6_Bit_End                                                             13
#define cAf6_ethfullcntsel_pen_CNT_GRP6_Mask                                                         cBit13_12
#define cAf6_ethfullcntsel_pen_CNT_GRP6_Shift                                                               12
#define cAf6_ethfullcntsel_pen_CNT_GRP6_MaxVal                                                             0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP6_MinVal                                                             0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP6_RstVal                                                            0x00

/*--------------------------------------
BitField Name: CNT_GRP5
BitField Type: R/W
BitField Desc: Event Group for Counter #5
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP5_Bit_Start                                                           10
#define cAf6_ethfullcntsel_pen_CNT_GRP5_Bit_End                                                             11
#define cAf6_ethfullcntsel_pen_CNT_GRP5_Mask                                                         cBit11_10
#define cAf6_ethfullcntsel_pen_CNT_GRP5_Shift                                                               10
#define cAf6_ethfullcntsel_pen_CNT_GRP5_MaxVal                                                             0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP5_MinVal                                                             0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP5_RstVal                                                            0x00

/*--------------------------------------
BitField Name: CNT_GRP4
BitField Type: R/W
BitField Desc: Event Group for Counter #4
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP4_Bit_Start                                                            8
#define cAf6_ethfullcntsel_pen_CNT_GRP4_Bit_End                                                              9
#define cAf6_ethfullcntsel_pen_CNT_GRP4_Mask                                                           cBit9_8
#define cAf6_ethfullcntsel_pen_CNT_GRP4_Shift                                                                8
#define cAf6_ethfullcntsel_pen_CNT_GRP4_MaxVal                                                             0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP4_MinVal                                                             0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP4_RstVal                                                            0x00

/*--------------------------------------
BitField Name: CNT_GRP3
BitField Type: R/W
BitField Desc: Event Group for Counter #3
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP3_Bit_Start                                                            6
#define cAf6_ethfullcntsel_pen_CNT_GRP3_Bit_End                                                              7
#define cAf6_ethfullcntsel_pen_CNT_GRP3_Mask                                                           cBit7_6
#define cAf6_ethfullcntsel_pen_CNT_GRP3_Shift                                                                6
#define cAf6_ethfullcntsel_pen_CNT_GRP3_MaxVal                                                             0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP3_MinVal                                                             0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP3_RstVal                                                            0x00

/*--------------------------------------
BitField Name: CNT_GRP2
BitField Type: R/W
BitField Desc: Event Group for Counter #2
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP2_Bit_Start                                                            4
#define cAf6_ethfullcntsel_pen_CNT_GRP2_Bit_End                                                              5
#define cAf6_ethfullcntsel_pen_CNT_GRP2_Mask                                                           cBit5_4
#define cAf6_ethfullcntsel_pen_CNT_GRP2_Shift                                                                4
#define cAf6_ethfullcntsel_pen_CNT_GRP2_MaxVal                                                             0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP2_MinVal                                                             0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP2_RstVal                                                            0x00

/*--------------------------------------
BitField Name: CNT_GRP1
BitField Type: R/W
BitField Desc: Event Group for Counter #1
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP1_Bit_Start                                                            2
#define cAf6_ethfullcntsel_pen_CNT_GRP1_Bit_End                                                              3
#define cAf6_ethfullcntsel_pen_CNT_GRP1_Mask                                                           cBit3_2
#define cAf6_ethfullcntsel_pen_CNT_GRP1_Shift                                                                2
#define cAf6_ethfullcntsel_pen_CNT_GRP1_MaxVal                                                             0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP1_MinVal                                                             0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP1_RstVal                                                            0x00

/*--------------------------------------
BitField Name: CNT_GRP0
BitField Type: R/W
BitField Desc: Event Group for Counter #0
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_ethfullcntsel_pen_CNT_GRP0_Bit_Start                                                            0
#define cAf6_ethfullcntsel_pen_CNT_GRP0_Bit_End                                                              1
#define cAf6_ethfullcntsel_pen_CNT_GRP0_Mask                                                           cBit1_0
#define cAf6_ethfullcntsel_pen_CNT_GRP0_Shift                                                                0
#define cAf6_ethfullcntsel_pen_CNT_GRP0_MaxVal                                                             0x3
#define cAf6_ethfullcntsel_pen_CNT_GRP0_MinVal                                                             0x0
#define cAf6_ethfullcntsel_pen_CNT_GRP0_RstVal                                                            0x00


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Full Counters Type Configuration
Reg Addr   : 0x00006-0x00009
Reg Formula: 0x00006+$grp_id
    Where  :
           + $grp_id(0-3): Event Group ID
Reg Desc   :
This register used to select Type for each couter.

------------------------------------------------------------------------------*/
#define cAf6Reg_ethfullcnttyp_pen_Base                                                                 0x00006
#define cAf6Reg_ethfullcnttyp_pen(grpid)                                                     (0x00006+(grpid))
#define cAf6Reg_ethfullcnttyp_pen_WidthVal                                                                  32
#define cAf6Reg_ethfullcnttyp_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: CNT_TYP
BitField Type: R/W
BitField Desc: Event Type for each Counter Group 0:
- cnt_type = 4'd0 : PWE Receive Packet Counter
- cnt_type = 4'd1 : PWE Receive Byte Counter
- cnt_type = 4'd2 : PWE Receive Lbit Packet
- cnt_type = 4'd3 : PWE Receive Nbit Packet
- cnt_type = 4'd4 : PWE Receive Pbit Packet
- cnt_type = 4'd5 : PWE Receive Rbit Packet
- cnt_type = 4'd6 : PWE Receive Late Packet
- cnt_type = 4'd7 : PWE Receive Early Packet
- cnt_type = 4'd8 : PWE Receive Lost Packet
- cnt_type = 4'd9 : PWE Receive Overrun
- cnt_type = 4'd10: PWE Receive Underrun
- cnt_type = 4'd11: PWE Receive Malform Packet
- cnt_type = 4'd12: PWE Receive Stray Packet
- cnt_type = 4'd13: PWE Receive LOPSTA
- cnt_type = 4'd14: PWE Receive LOPSYN
 Group 1:
- cnt_type = 4'd0 : Ethernet Egress Packet Counter
- cnt_type = 4'd1 : Ethernet Egress Discarded Packet Counter
- cnt_type = 4'd2 : Ethernet Egress Byte Counter
- cnt_type = 4'd3 : Ethernet Egress Discarded Byte Counter
- cnt_type = 4'd4 : Ethernet Egress BECN Packet Counter
- cnt_type = 4'd5 : Ethernet Egress FECN Packet Counter
- cnt_type = 4'd6 : Ethernet Egress DE Packet Counter
- cnt_type = 4'd7 : Ethernet Egress MTU Packet Counter
- cnt_type = 4'd8 : Ethernet Egress Fragment Counter
- cnt_type = 4'd9-4'd15: Reserved
 Group 2:
- cnt_type = 4'd0 : PWE Transmit Packet Counter
- cnt_type = 4'd1 : PWE Transmit Byte Counter
- cnt_type = 4'd2 : PWE Transmit Packet Fragment
- cnt_type = 4'd3 : PWE Transmit Lbit Packet
- cnt_type = 4'd4 : PWE Transmit Rbit Packet
- cnt_type = 4'd5 : PWE Transmit Mbit Packet
- cnt_type = 4'd6 : PWE Transmit Pbit Packet
- cnt_type = 4'd7-4'd15: Reserved
 Group 3:
- cnt_type = 4'd0 : Ethernet Ingress Packet Counter
- cnt_type = 4'd1 : Ethernet Ingress Discarded Packet Counter
- cnt_type = 4'd2 : Ethernet Ingress Byte Counter
- cnt_type = 4'd3 : Ethernet Ingress Discarded Byte Counter
- cnt_type = 4'd4 : Ethernet Ingress BECN Packet Counter
- cnt_type = 4'd5 : Ethernet Ingress FECN Packet Counter
- cnt_type = 4'd6 : Ethernet Ingress DE Packet Counter
- cnt_type = 4'd7 : Ethernet Ingress MRU Packet Counter
- cnt_type = 4'd8 : Ethernet Ingress Fragment Counter
 - cnt_type = 4'd9-4'd15: Reserved
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ethfullcnttyp_pen_CNT_TYP_Bit_Start                                                             0
#define cAf6_ethfullcnttyp_pen_CNT_TYP_Bit_End                                                               3
#define cAf6_ethfullcnttyp_pen_CNT_TYP_Mask                                                            cBit3_0
#define cAf6_ethfullcnttyp_pen_CNT_TYP_Shift                                                                 0
#define cAf6_ethfullcnttyp_pen_CNT_TYP_MaxVal                                                              0xf
#define cAf6_ethfullcnttyp_pen_CNT_TYP_MinVal                                                              0x0
#define cAf6_ethfullcnttyp_pen_CNT_TYP_RstVal                                                             0x00

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PMCINTERNALREG_H_ */


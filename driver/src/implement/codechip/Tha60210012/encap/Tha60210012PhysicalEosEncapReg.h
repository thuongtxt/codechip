/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : Tha60210012PhysicalEosEncapReg.h
 * 
 * Created Date: Mar 24, 2016
 *
 * Description : GFP encap register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PHYSICALEOSENCAPREG_H_
#define _THA60210012PHYSICALEOSENCAPREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : Hold Data Return REG 1
Reg Addr   : 0x000000
Reg Formula: 
    Where  : 
Reg Desc   : 
Return data when microprocessor accesses to the REG is more than 32 Bits on Read-Phase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_holdreg1_Base                                                                    0x000000
#define cAf6Reg_upen_holdreg1                                                                         0x000000
#define cAf6Reg_upen_holdreg1_WidthVal                                                                      32
#define cAf6Reg_upen_holdreg1_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: holdreg1
BitField Type: R/W
BitField Desc: Hold Register 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_holdreg1_holdreg1_Bit_Start                                                                0
#define cAf6_upen_holdreg1_holdreg1_Bit_End                                                                 31
#define cAf6_upen_holdreg1_holdreg1_Mask                                                              cBit31_0
#define cAf6_upen_holdreg1_holdreg1_Shift                                                                    0
#define cAf6_upen_holdreg1_holdreg1_MaxVal                                                          0xffffffff
#define cAf6_upen_holdreg1_holdreg1_MinVal                                                                 0x0
#define cAf6_upen_holdreg1_holdreg1_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : ENC Master Control
Reg Addr   : 0x000003
Reg Formula: 
    Where  : 
           + N/A
Reg Desc   : 
Use to ENC Initial

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_egrext_Base                                                                      0x000003
#define cAf6Reg_upen_egrext_WidthVal                                                                        32
#define cAf6Reg_upen_egrext_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: ocfgtocken
BitField Type: R/W
BitField Desc: Number of D-word for Buffer Requesting
BitField Bits: [7:2]
--------------------------------------*/
#define cAf6_upen_egrext_ocfgtocken_Bit_Start                                                                2
#define cAf6_upen_egrext_ocfgtocken_Bit_End                                                                  7
#define cAf6_upen_egrext_ocfgtocken_Mask                                                               cBit7_2
#define cAf6_upen_egrext_ocfgtocken_Shift                                                                    2
#define cAf6_upen_egrext_ocfgtocken_MaxVal                                                                0x3f
#define cAf6_upen_egrext_ocfgtocken_MinVal                                                                 0x0
#define cAf6_upen_egrext_ocfgtocken_RstVal                                                                 0x8

/*--------------------------------------
BitField Name: ocfgqdrini
BitField Type: R/W
BitField Desc: ENC Initial Enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_egrext_ocfgqdrini_Bit_Start                                                                1
#define cAf6_upen_egrext_ocfgqdrini_Bit_End                                                                  1
#define cAf6_upen_egrext_ocfgqdrini_Mask                                                                 cBit1
#define cAf6_upen_egrext_ocfgqdrini_Shift                                                                    1
#define cAf6_upen_egrext_ocfgqdrini_MaxVal                                                                 0x1
#define cAf6_upen_egrext_ocfgqdrini_MinVal                                                                 0x0
#define cAf6_upen_egrext_ocfgqdrini_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: encdoneini
BitField Type: RO
BitField Desc: ENC Initial Done
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_egrext_encdoneini_Bit_Start                                                                0
#define cAf6_upen_egrext_encdoneini_Bit_End                                                                  0
#define cAf6_upen_egrext_encdoneini_Mask                                                                 cBit0
#define cAf6_upen_egrext_encdoneini_Shift                                                                    0
#define cAf6_upen_egrext_encdoneini_MaxVal                                                                 0x1
#define cAf6_upen_egrext_encdoneini_MinVal                                                                 0x0
#define cAf6_upen_egrext_encdoneini_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : MAP Table
Reg Addr   : 0x010000-0x011FFF
Reg Formula: 0x010000 + $VCG
    Where  : 
           + $VCG(0-255) : VCG - ID
Reg Desc   : 
Per VCG Control

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mapf_Base                                                                        0x010000
#define cAf6Reg_upen_mapf(VCG)                                                                (0x010000+(VCG))
#define cAf6Reg_upen_mapf_WidthVal                                                                          32
#define cAf6Reg_upen_mapf_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: pgsapoct
BitField Type: R/W
BitField Desc: Second Octo of SAPI Field are Used in HDLC Byte Stuffing Mode
BitField Bits: [27:20]
--------------------------------------*/
#define cAf6_upen_mapf_pgsapoct_Bit_Start                                                                   20
#define cAf6_upen_mapf_pgsapoct_Bit_End                                                                     27
#define cAf6_upen_mapf_pgsapoct_Mask                                                                 cBit27_20
#define cAf6_upen_mapf_pgsapoct_Shift                                                                       20
#define cAf6_upen_mapf_pgsapoct_MaxVal                                                                    0xff
#define cAf6_upen_mapf_pgsapoct_MinVal                                                                     0x0
#define cAf6_upen_mapf_pgsapoct_RstVal                                                                     0x0

#define cAf6_upen_mapf_TxSAPI1_Mask                                                                 cBit27_20
#define cAf6_upen_mapf_TxSAPI1_Shift                                                                       20
#define cAf6_upen_mapf_TxSAPI0_Mask                                                                 cBit18_11
#define cAf6_upen_mapf_TxSAPI0_Shift                                                                       11

/*--------------------------------------
BitField Name: pgfcsena
BitField Type: R/W
BitField Desc: GFP - F FCS Payload Enable
BitField Bits: [19]
--------------------------------------*/
#define cAf6_upen_mapf_pgfcsena_Bit_Start                                                                   19
#define cAf6_upen_mapf_pgfcsena_Bit_End                                                                     19
#define cAf6_upen_mapf_pgfcsena_Mask                                                                    cBit19
#define cAf6_upen_mapf_pgfcsena_Shift                                                                       19
#define cAf6_upen_mapf_pgfcsena_MaxVal                                                                     0x1
#define cAf6_upen_mapf_pgfcsena_MinVal                                                                     0x0
#define cAf6_upen_mapf_pgfcsena_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: pgupigfp
BitField Type: R/W
BitField Desc: GFP - F UPI Field Control
BitField Bits: [18:11]
--------------------------------------*/
#define cAf6_upen_mapf_pgupigfp_Bit_Start                                                                   11
#define cAf6_upen_mapf_pgupigfp_Bit_End                                                                     18
#define cAf6_upen_mapf_pgupigfp_Mask                                                                 cBit18_11
#define cAf6_upen_mapf_pgupigfp_Shift                                                                       11
#define cAf6_upen_mapf_pgupigfp_MaxVal                                                                    0xff
#define cAf6_upen_mapf_pgupigfp_MinVal                                                                     0x0
#define cAf6_upen_mapf_pgupigfp_RstVal                                                                     0x0
 
/*--------------------------------------
BitField Name: HDLC_INTER_FLAG
BitField Type: R/W
BitField Desc: HDLC Minimun Control for The Number of Byte Inter Packet Flag,
Zero is Expected 1
BitField Bits: [10:9]
--------------------------------------*/
#define cAf6_upen_mapf_HDLC_INTER_FLAG_Bit_Start                                                             9
#define cAf6_upen_mapf_HDLC_INTER_FLAG_Bit_End                                                              10
#define cAf6_upen_mapf_HDLC_INTER_FLAG_Mask                                                           cBit10_9
#define cAf6_upen_mapf_HDLC_INTER_FLAG_Shift                                                                 9
#define cAf6_upen_mapf_HDLC_INTER_FLAG_MaxVal                                                              0x3
#define cAf6_upen_mapf_HDLC_INTER_FLAG_MinVal                                                              0x0
#define cAf6_upen_mapf_HDLC_INTER_FLAG_RstVal                                                              0x0

/*--------------------------------------
BitField Name: HDLCSCR
BitField Type: R/W
BitField Desc: HDLC or GFP-F Scramble Enable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_upen_mapf_HDLCSCR_Bit_Start                                                                     8
#define cAf6_upen_mapf_HDLCSCR_Bit_End                                                                       8
#define cAf6_upen_mapf_HDLCSCR_Mask                                                                      cBit8
#define cAf6_upen_mapf_HDLCSCR_Shift                                                                         8
#define cAf6_upen_mapf_HDLCSCR_MaxVal                                                                      0x1
#define cAf6_upen_mapf_HDLCSCR_MinVal                                                                      0x0
#define cAf6_upen_mapf_HDLCSCR_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: ENCMODE
BitField Type: R/W
BitField Desc: 0 is Bypass, 1 is HDLC Byte Stuffing Mode, 3 is GFP Mode
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_upen_mapf_ENCMODE_Bit_Start                                                                     6
#define cAf6_upen_mapf_ENCMODE_Bit_End                                                                       7
#define cAf6_upen_mapf_ENCMODE_Mask                                                                    cBit7_6
#define cAf6_upen_mapf_ENCMODE_Shift                                                                         6
#define cAf6_upen_mapf_ENCMODE_MaxVal                                                                      0x3
#define cAf6_upen_mapf_ENCMODE_MinVal                                                                      0x0
#define cAf6_upen_mapf_ENCMODE_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: FBIT_THRESHOLD_H
BitField Type: R/W
BitField Desc: Number of Block of 32 Bytes are Provided for ENC Cache, Total Max
Block is 4K
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_upen_mapf_FBIT_THRESHOLD_H_Bit_Start                                                            0
#define cAf6_upen_mapf_FBIT_THRESHOLD_H_Bit_End                                                              5
#define cAf6_upen_mapf_FBIT_THRESHOLD_H_Mask                                                           cBit5_0
#define cAf6_upen_mapf_FBIT_THRESHOLD_H_Shift                                                                0
#define cAf6_upen_mapf_FBIT_THRESHOLD_H_MaxVal                                                            0x3f
#define cAf6_upen_mapf_FBIT_THRESHOLD_H_MinVal                                                             0x0
#define cAf6_upen_mapf_FBIT_THRESHOLD_H_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : CSF Transmit Control
Reg Addr   : 0x010800-0x010BFF
Reg Formula: 0x010800 + $VCG
    Where  : 
           + $VCG(0-255) : VCG - ID
Reg Desc   : 
Per VCG Control

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_csfp_Base                                                                        0x010800
#define cAf6Reg_upen_csfp(VCG)                                                                (0x010800+(VCG))
#define cAf6Reg_upen_csfp_WidthVal                                                                          32
#define cAf6Reg_upen_csfp_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: decicfmena
BitField Type: R/W
BitField Desc: Enable Receive CMF
BitField Bits: [9]
--------------------------------------*/
#define cAf6_upen_csfp_decicfmena_Bit_Start                                                                  9
#define cAf6_upen_csfp_decicfmena_Bit_End                                                                    9
#define cAf6_upen_csfp_decicfmena_Mask                                                                   cBit9
#define cAf6_upen_csfp_decicfmena_Shift                                                                      9
#define cAf6_upen_csfp_decicfmena_MaxVal                                                                   0x1
#define cAf6_upen_csfp_decicfmena_MinVal                                                                   0x0
#define cAf6_upen_csfp_decicfmena_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: encicsfena
BitField Type: R/W
BitField Desc: Enable Transmit CSF
BitField Bits: [8]
--------------------------------------*/
#define cAf6_upen_csfp_encicsfena_Bit_Start                                                                  8
#define cAf6_upen_csfp_encicsfena_Bit_End                                                                    8
#define cAf6_upen_csfp_encicsfena_Mask                                                                   cBit8
#define cAf6_upen_csfp_encicsfena_Shift                                                                      8
#define cAf6_upen_csfp_encicsfena_MaxVal                                                                   0x1
#define cAf6_upen_csfp_encicsfena_MinVal                                                                   0x0
#define cAf6_upen_csfp_encicsfena_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: encicsfupi
BitField Type: R/W
BitField Desc: GFP - F UPI Field are Used for CSF Frame
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_csfp_encicsfupi_Bit_Start                                                                  0
#define cAf6_upen_csfp_encicsfupi_Bit_End                                                                    7
#define cAf6_upen_csfp_encicsfupi_Mask                                                                 cBit7_0
#define cAf6_upen_csfp_encicsfupi_Shift                                                                      0
#define cAf6_upen_csfp_encicsfupi_MaxVal                                                                  0xff
#define cAf6_upen_csfp_encicsfupi_MinVal                                                                   0x0
#define cAf6_upen_csfp_encicsfupi_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Decapsulation Table
Reg Addr   : 0x010400-0x0107FF
Reg Formula: 0x010400 + $VCG
    Where  : 
           + $VCG(0-255) : VCG - ID
Reg Desc   : 
Per VCG Control

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_decf_Base                                                                        0x010400
#define cAf6Reg_upen_decf(VCG)                                                                (0x010400+(VCG))
#define cAf6Reg_upen_decf_WidthVal                                                                          64
#define cAf6Reg_upen_decf_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: tb_drop_dismisf
BitField Type: R/W
BitField Desc: SAPI Data Frame Mismatch Drop Disable are Used in HDLC Byte
Stuffing Mode or UPI Mismatch Drop Disable are Used in GFP-F Mode
BitField Bits: [38]
--------------------------------------*/
#define cAf6_upen_decf_tb_drop_dismisf_Bit_Start                                                            38
#define cAf6_upen_decf_tb_drop_dismisf_Bit_End                                                              38
#define cAf6_upen_decf_tb_drop_dismisf_Mask                                                              cBit6
#define cAf6_upen_decf_tb_drop_dismisf_Shift                                                                 6
#define cAf6_upen_decf_tb_drop_dismisf_MaxVal                                                              0x0
#define cAf6_upen_decf_tb_drop_dismisf_MinVal                                                              0x0
#define cAf6_upen_decf_tb_drop_dismisf_RstVal                                                              0x0

/*--------------------------------------
BitField Name: tb_addr_oct
BitField Type: R/W
BitField Desc: Second Octo of SAPI Data Link are Used in HDLC Byte Stuffing Mode
BitField Bits: [37:30]
--------------------------------------*/
#define cAf6_upen_decf_tb_addr_oct_Bit_Start                                                                30
#define cAf6_upen_decf_tb_addr_oct_Bit_End                                                                  37
#define cAf6_upen_decf_tb_addr_oct_Mask_01                                                           cBit31_30
#define cAf6_upen_decf_tb_addr_oct_Shift_01                                                                 30
#define cAf6_upen_decf_tb_addr_oct_Mask_02                                                             cBit5_0
#define cAf6_upen_decf_tb_addr_oct_Shift_02                                                                  0

/*--------------------------------------
BitField Name: tb_ctrl_oct
BitField Type: R/W
BitField Desc: First Octo of SAPI Data Link are Used in HDLC Byte Stuffing Mode
or UPI Field Comparing are Used in GFP-F Mode
BitField Bits: [29:22]
--------------------------------------*/
#define cAf6_upen_decf_tb_ctrl_oct_Bit_Start                                                                22
#define cAf6_upen_decf_tb_ctrl_oct_Bit_End                                                                  29
#define cAf6_upen_decf_tb_ctrl_oct_Mask                                                              cBit29_22
#define cAf6_upen_decf_tb_ctrl_oct_Shift                                                                    22
#define cAf6_upen_decf_tb_ctrl_oct_MaxVal                                                                 0xff
#define cAf6_upen_decf_tb_ctrl_oct_MinVal                                                                  0x0
#define cAf6_upen_decf_tb_ctrl_oct_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: tb_sapi_oct
BitField Type: R/W
BitField Desc: SAPI Data Frame Comparing are Used in HDLC Byte Stuffing Mode
BitField Bits: [21:6]
--------------------------------------*/
#define cAf6_upen_decf_tb_sapi_oct_Bit_Start                                                                 6
#define cAf6_upen_decf_tb_sapi_oct_Bit_End                                                                  21
#define cAf6_upen_decf_tb_sapi_oct_Mask                                                               cBit21_6
#define cAf6_upen_decf_tb_sapi_oct_Shift                                                                     6
#define cAf6_upen_decf_tb_sapi_oct_MaxVal                                                               0xffff
#define cAf6_upen_decf_tb_sapi_oct_MinVal                                                                  0x0
#define cAf6_upen_decf_tb_sapi_oct_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: tb_hdlc_ena
BitField Type: R/W
BitField Desc: HDLC Byte Stuffing Mode Enable
BitField Bits: [5]
--------------------------------------*/
#define cAf6_upen_decf_tb_hdlc_ena_Bit_Start                                                                 5
#define cAf6_upen_decf_tb_hdlc_ena_Bit_End                                                                   5
#define cAf6_upen_decf_tb_hdlc_ena_Mask                                                                  cBit5
#define cAf6_upen_decf_tb_hdlc_ena_Shift                                                                     5
#define cAf6_upen_decf_tb_hdlc_ena_MaxVal                                                                  0x1
#define cAf6_upen_decf_tb_hdlc_ena_MinVal                                                                  0x0
#define cAf6_upen_decf_tb_hdlc_ena_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: tb_drop_discrcf
BitField Type: R/W
BitField Desc: Disable Drop Packet When CRC Payload Error
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upen_decf_tb_drop_discrcf_Bit_Start                                                             4
#define cAf6_upen_decf_tb_drop_discrcf_Bit_End                                                               4
#define cAf6_upen_decf_tb_drop_discrcf_Mask                                                              cBit4
#define cAf6_upen_decf_tb_drop_discrcf_Shift                                                                 4
#define cAf6_upen_decf_tb_drop_discrcf_MaxVal                                                              0x1
#define cAf6_upen_decf_tb_drop_discrcf_MinVal                                                              0x0
#define cAf6_upen_decf_tb_drop_discrcf_RstVal                                                              0x0

/*--------------------------------------
BitField Name: tb_drop_disthec
BitField Type: R/W
BitField Desc: Disable Drop Packet When T-HEC Error     in GFP Mode or Disable
Drop Packet When Mismatch Data Link SAPI Above in HDLC Mode
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upen_decf_tb_drop_disthec_Bit_Start                                                             3
#define cAf6_upen_decf_tb_drop_disthec_Bit_End                                                               3
#define cAf6_upen_decf_tb_drop_disthec_Mask                                                              cBit3
#define cAf6_upen_decf_tb_drop_disthec_Shift                                                                 3
#define cAf6_upen_decf_tb_drop_disthec_MaxVal                                                              0x1
#define cAf6_upen_decf_tb_drop_disthec_MinVal                                                              0x0
#define cAf6_upen_decf_tb_drop_disthec_RstVal                                                              0x0

/*--------------------------------------
BitField Name: tb_scrm_ena
BitField Type: R/W
BitField Desc: Scramble Enable on DEC
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upen_decf_tb_scrm_ena_Bit_Start                                                                 2
#define cAf6_upen_decf_tb_scrm_ena_Bit_End                                                                   2
#define cAf6_upen_decf_tb_scrm_ena_Mask                                                                  cBit2
#define cAf6_upen_decf_tb_scrm_ena_Shift                                                                     2
#define cAf6_upen_decf_tb_scrm_ena_MaxVal                                                                  0x1
#define cAf6_upen_decf_tb_scrm_ena_MinVal                                                                  0x0
#define cAf6_upen_decf_tb_scrm_ena_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: tb_thec_correct
BitField Type: R/W
BitField Desc: T-HEC Correct Enable in GFP Mode, TDM Header Remove Enable in
HDLC Mode
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_decf_tb_thec_correct_Bit_Start                                                             1
#define cAf6_upen_decf_tb_thec_correct_Bit_End                                                               1
#define cAf6_upen_decf_tb_thec_correct_Mask                                                              cBit1
#define cAf6_upen_decf_tb_thec_correct_Shift                                                                 1
#define cAf6_upen_decf_tb_thec_correct_MaxVal                                                              0x1
#define cAf6_upen_decf_tb_thec_correct_MinVal                                                              0x0
#define cAf6_upen_decf_tb_thec_correct_RstVal                                                              0x0

/*--------------------------------------
BitField Name: tb_chec_correct
BitField Type: R/W
BitField Desc: C-HEC Correct Enable on DEC
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_decf_tb_chec_correct_Bit_Start                                                             0
#define cAf6_upen_decf_tb_chec_correct_Bit_End                                                               0
#define cAf6_upen_decf_tb_chec_correct_Mask                                                              cBit0
#define cAf6_upen_decf_tb_chec_correct_Shift                                                                 0
#define cAf6_upen_decf_tb_chec_correct_MaxVal                                                              0x1
#define cAf6_upen_decf_tb_chec_correct_MinVal                                                              0x0
#define cAf6_upen_decf_tb_chec_correct_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Alarm Interrupt Enable Control
Reg Addr   : 0x001000-0x0010FF
Reg Formula: 0x001000 + $VCG
    Where  : 
           + $VCG(0-255): VCG ID
Reg Desc   : 
Registers is Used to Enable Interrupting.

------------------------------------------------------------------------------*/
#define cAf6Reg_upchcfgpen_Base                                                                       0x001000
#define cAf6Reg_upchcfgpen(VCG)                                                               (0x001000+(VCG))
#define cAf6Reg_upchcfgpen_WidthVal                                                                         32
#define cAf6Reg_upchcfgpen_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: RCP_CSF_ENB
BitField Type: R/W
BitField Desc: Received CSF Frame in GFP-F Mode or Address Mismatch in HDLC Mode
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upchcfgpen_RCP_CSF_ENB_Bit_Start                                                                2
#define cAf6_upchcfgpen_RCP_CSF_ENB_Bit_End                                                                  2
#define cAf6_upchcfgpen_RCP_CSF_ENB_Mask                                                                 cBit2
#define cAf6_upchcfgpen_RCP_CSF_ENB_Shift                                                                    2
#define cAf6_upchcfgpen_RCP_CSF_ENB_MaxVal                                                                 0x1
#define cAf6_upchcfgpen_RCP_CSF_ENB_MinVal                                                                 0x0
#define cAf6_upchcfgpen_RCP_CSF_ENB_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: FRMSTATE_ENB
BitField Type: R/W
BitField Desc: Framer State Change per Bit Control in GFP-F Mode or Control,
SAPI Mismatch in HDLC Mode
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_upchcfgpen_FRMSTATE_ENB_Bit_Start                                                               0
#define cAf6_upchcfgpen_FRMSTATE_ENB_Bit_End                                                                 1
#define cAf6_upchcfgpen_FRMSTATE_ENB_Mask                                                              cBit1_0
#define cAf6_upchcfgpen_FRMSTATE_ENB_Shift                                                                   0
#define cAf6_upchcfgpen_FRMSTATE_ENB_MaxVal                                                                0x3
#define cAf6_upchcfgpen_FRMSTATE_ENB_MinVal                                                                0x0
#define cAf6_upchcfgpen_FRMSTATE_ENB_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : Alarm Interrupt Sticky
Reg Addr   : 0x001100-0x0011FF
Reg Formula: 0x001100 + $VCG
    Where  : 
           + $VCG(0-255): VCG ID
Reg Desc   : 
Registers are Used to Latch the Interrupt bits.

------------------------------------------------------------------------------*/
#define cAf6Reg_upchstypen_Base                                                                       0x001100
#define cAf6Reg_upchstypen(VCG)                                                               (0x001100+(VCG))
#define cAf6Reg_upchstypen_WidthVal                                                                         32
#define cAf6Reg_upchstypen_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: RCP_CSF_STICKY
BitField Type: R/W/C
BitField Desc: Received CSF Frame in GFP-F Mode or Address Mismatch in HDLC Mode
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upchstypen_RCP_CSF_STICKY_Bit_Start                                                             2
#define cAf6_upchstypen_RCP_CSF_STICKY_Bit_End                                                               2
#define cAf6_upchstypen_RCP_CSF_STICKY_Mask                                                              cBit2
#define cAf6_upchstypen_RCP_CSF_STICKY_Shift                                                                 2
#define cAf6_upchstypen_RCP_CSF_STICKY_MaxVal                                                              0x1
#define cAf6_upchstypen_RCP_CSF_STICKY_MinVal                                                              0x0
#define cAf6_upchstypen_RCP_CSF_STICKY_RstVal                                                              0x0

/*--------------------------------------
BitField Name: FRMSTATE_STICKY_BIT_B
BitField Type: R/W/C
BitField Desc: The Second Bit of Frame State Transition in GFP-F Mode or Control
Mismatch in HDLC Mode
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_B_Bit_Start                                                      1
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_B_Bit_End                                                        1
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_B_Mask                                                       cBit1
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_B_Shift                                                          1
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_B_MaxVal                                                       0x1
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_B_MinVal                                                       0x0
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_B_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FRMSTATE_STICKY_BIT_A
BitField Type: R/W/C
BitField Desc: The First Bit of Frame State Transition in GFP-F Mode or SAPI
Mismatch in HDLC Mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_A_Bit_Start                                                      0
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_A_Bit_End                                                        0
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_A_Mask                                                       cBit0
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_A_Shift                                                          0
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_A_MaxVal                                                       0x1
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_A_MinVal                                                       0x0
#define cAf6_upchstypen_FRMSTATE_STICKY_BIT_A_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Alarm Interrupt Status
Reg Addr   : 0x001200-0x0012FF
Reg Formula: 0x001200 + $VCG
    Where  : 
           + $VCG(0-255): VCG ID
Reg Desc   : 
Each register is Current State.

------------------------------------------------------------------------------*/
#define cAf6Reg_upchstapen_Base                                                                       0x001200
#define cAf6Reg_upchstapen(VCG)                                                               (0x001200+(VCG))
#define cAf6Reg_upchstapen_WidthVal                                                                         32
#define cAf6Reg_upchstapen_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: RCP_CSF_STATE
BitField Type: R/W/C
BitField Desc: Received CSF Frame in GFP-F Mode or Address Mismatch in HDLC Mode
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upchstapen_RCP_CSF_STATE_Bit_Start                                                              2
#define cAf6_upchstapen_RCP_CSF_STATE_Bit_End                                                                2
#define cAf6_upchstapen_RCP_CSF_STATE_Mask                                                               cBit2
#define cAf6_upchstapen_RCP_CSF_STATE_Shift                                                                  2
#define cAf6_upchstapen_RCP_CSF_STATE_MaxVal                                                               0x1
#define cAf6_upchstapen_RCP_CSF_STATE_MinVal                                                               0x0
#define cAf6_upchstapen_RCP_CSF_STATE_RstVal                                                               0x0

/*--------------------------------------
BitField Name: FRMSTATE_STATE
BitField Type: R/W/C
BitField Desc: DEC Framer State Change : 0 is HUNT, 1 is PRESYNC, 2 is SYNC
in GFP-F Mode or Control, SAPI Mismatch in HDLC Mode
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_upchstapen_FRMSTATE_STATE_Bit_Start                                                             0
#define cAf6_upchstapen_FRMSTATE_STATE_Bit_End                                                               1
#define cAf6_upchstapen_FRMSTATE_STATE_Mask                                                            cBit1_0
#define cAf6_upchstapen_FRMSTATE_STATE_Shift                                                                 0
#define cAf6_upchstapen_FRMSTATE_STATE_MaxVal                                                              0x3
#define cAf6_upchstapen_FRMSTATE_STATE_MinVal                                                              0x0
#define cAf6_upchstapen_FRMSTATE_STATE_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : OR Interrupt Status per VCG
Reg Addr   : 0x001300-0x001307
Reg Formula: 0x001300+$addr
    Where  : 
           + $addr(0-7): Address of RAM
Reg Desc   : 
The register consists of 32 bits for 256 VCGs. Each bit is used to store Interrupt Status per VCG.

------------------------------------------------------------------------------*/
#define cAf6Reg_upsegpen_Base                                                                         0x001300
#define cAf6Reg_upsegpen(addr)                                                               (0x001300+(addr))
#define cAf6Reg_upsegpen_WidthVal                                                                           64
#define cAf6Reg_upsegpen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: intsta
BitField Type: R_O
BitField Desc: Every bit corresponding with Interrupt Status per VCG
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upsegpen_intsta_Bit_Start                                                                       0
#define cAf6_upsegpen_intsta_Bit_End                                                                        31
#define cAf6_upsegpen_intsta_Mask                                                                     cBit31_0
#define cAf6_upsegpen_intsta_Shift                                                                           0
#define cAf6_upsegpen_intsta_MaxVal                                                                 0xffffffff
#define cAf6_upsegpen_intsta_MinVal                                                                        0x0
#define cAf6_upsegpen_intsta_RstVal                                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Enable Configuration
Reg Addr   : 0x0013FE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 32 bits used to configurate the row of OR Interrupt Status per VCG (32-VCG) Interrup Enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_cfgglbintenpen_Base                                                                   0x0013FE
#define cAf6Reg_cfgglbintenpen                                                                        0x0013FE
#define cAf6Reg_cfgglbintenpen_WidthVal                                                                     32
#define cAf6Reg_cfgglbintenpen_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: intenb
BitField Type: R/W
BitField Desc: Every bit used to Enable the row of OR Interrupt Status per VCG
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_cfgglbintenpen_intenb_Bit_Start                                                                 0
#define cAf6_cfgglbintenpen_intenb_Bit_End                                                                   7
#define cAf6_cfgglbintenpen_intenb_Mask                                                                cBit7_0
#define cAf6_cfgglbintenpen_intenb_Shift                                                                     0
#define cAf6_cfgglbintenpen_intenb_MaxVal                                                                 0xff
#define cAf6_cfgglbintenpen_intenb_MinVal                                                                  0x0
#define cAf6_cfgglbintenpen_intenb_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Interrupt Status at a Row
Reg Addr   : 0x0013FF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 32 bits used to know the row of OR Interrupt Status per VCG (32-VCG) Interrup

------------------------------------------------------------------------------*/
#define cAf6Reg_upglbintpen_Base                                                                      0x0013FF
#define cAf6Reg_upglbintpen                                                                           0x0013FF
#define cAf6Reg_upglbintpen_WidthVal                                                                        32
#define cAf6Reg_upglbintpen_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: intst
BitField Type: R/W/C
BitField Desc: Every bit corresponding the row of OR Interrupt Status per VCG
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upglbintpen_intst_Bit_Start                                                                     0
#define cAf6_upglbintpen_intst_Bit_End                                                                       7
#define cAf6_upglbintpen_intst_Mask                                                                    cBit7_0
#define cAf6_upglbintpen_intst_Shift                                                                         0
#define cAf6_upglbintpen_intst_MaxVal                                                                     0xff
#define cAf6_upglbintpen_intst_MinVal                                                                      0x0
#define cAf6_upglbintpen_intst_RstVal                                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : CFM DEC Trigger Fifo Control and Status
Reg Addr   : 0x00_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
Info for Getting Packet from Buffer

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_triggf_Base                                                                      0x000004
#define cAf6Reg_upen_triggf                                                                           0x000004
#define cAf6Reg_upen_triggf_WidthVal                                                                        32
#define cAf6Reg_upen_triggf_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: cinfoval
BitField Type: R_O
BitField Desc: Info Valid on REG
BitField Bits: [21]
--------------------------------------*/
#define cAf6_upen_triggf_cinfoval_Bit_Start                                                                 21
#define cAf6_upen_triggf_cinfoval_Bit_End                                                                   21
#define cAf6_upen_triggf_cinfoval_Mask                                                                  cBit21
#define cAf6_upen_triggf_cinfoval_Shift                                                                     21
#define cAf6_upen_triggf_cinfoval_MaxVal                                                                   0x1
#define cAf6_upen_triggf_cinfoval_MinVal                                                                   0x0
#define cAf6_upen_triggf_cinfoval_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: cinfoeop
BitField Type: R_O
BitField Desc: End of Packet
BitField Bits: [20]
--------------------------------------*/
#define cAf6_upen_triggf_cinfoeop_Bit_Start                                                                 20
#define cAf6_upen_triggf_cinfoeop_Bit_End                                                                   20
#define cAf6_upen_triggf_cinfoeop_Mask                                                                  cBit20
#define cAf6_upen_triggf_cinfoeop_Shift                                                                     20
#define cAf6_upen_triggf_cinfoeop_MaxVal                                                                   0x1
#define cAf6_upen_triggf_cinfoeop_MinVal                                                                   0x0
#define cAf6_upen_triggf_cinfoeop_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: cfmvcgx
BitField Type: R_O
BitField Desc: VCG-ID
BitField Bits: [19:12]
--------------------------------------*/
#define cAf6_upen_triggf_cfmvcgx_Bit_Start                                                                  12
#define cAf6_upen_triggf_cfmvcgx_Bit_End                                                                    19
#define cAf6_upen_triggf_cfmvcgx_Mask                                                                cBit19_12
#define cAf6_upen_triggf_cfmvcgx_Shift                                                                      12
#define cAf6_upen_triggf_cfmvcgx_MaxVal                                                                   0xff
#define cAf6_upen_triggf_cfmvcgx_MinVal                                                                    0x0
#define cAf6_upen_triggf_cfmvcgx_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: blockid
BitField Type: R_O
BitField Desc: Block ID
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_upen_triggf_blockid_Bit_Start                                                                   8
#define cAf6_upen_triggf_blockid_Bit_End                                                                    11
#define cAf6_upen_triggf_blockid_Mask                                                                 cBit11_8
#define cAf6_upen_triggf_blockid_Shift                                                                       8
#define cAf6_upen_triggf_blockid_MaxVal                                                                    0xf
#define cAf6_upen_triggf_blockid_MinVal                                                                    0x0
#define cAf6_upen_triggf_blockid_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: octcntf
BitField Type: R_O
BitField Desc: Number of D-Word
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_upen_triggf_octcntf_Bit_Start                                                                   4
#define cAf6_upen_triggf_octcntf_Bit_End                                                                     7
#define cAf6_upen_triggf_octcntf_Mask                                                                  cBit7_4
#define cAf6_upen_triggf_octcntf_Shift                                                                       4
#define cAf6_upen_triggf_octcntf_MaxVal                                                                    0xf
#define cAf6_upen_triggf_octcntf_MinVal                                                                    0x0
#define cAf6_upen_triggf_octcntf_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: ff_i_nob
BitField Type: R_O
BitField Desc: Number of Byte
BitField Bits: [3:1]
--------------------------------------*/
#define cAf6_upen_triggf_ff_i_nob_Bit_Start                                                                  1
#define cAf6_upen_triggf_ff_i_nob_Bit_End                                                                    3
#define cAf6_upen_triggf_ff_i_nob_Mask                                                                 cBit3_1
#define cAf6_upen_triggf_ff_i_nob_Shift                                                                      1
#define cAf6_upen_triggf_ff_i_nob_MaxVal                                                                   0x7
#define cAf6_upen_triggf_ff_i_nob_MinVal                                                                   0x0
#define cAf6_upen_triggf_ff_i_nob_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ocfg_eve_clre
BitField Type: R/W
BitField Desc: Info Valid Clear When Changing from 0 to 1
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_triggf_ocfg_eve_clre_Bit_Start                                                             0
#define cAf6_upen_triggf_ocfg_eve_clre_Bit_End                                                               0
#define cAf6_upen_triggf_ocfg_eve_clre_Mask                                                              cBit0
#define cAf6_upen_triggf_ocfg_eve_clre_Shift                                                                 0
#define cAf6_upen_triggf_ocfg_eve_clre_MaxVal                                                              0x1
#define cAf6_upen_triggf_ocfg_eve_clre_MinVal                                                              0x0
#define cAf6_upen_triggf_ocfg_eve_clre_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : CFM DEC Trigger Data Dump
Reg Addr   : 0x001800-0x001FFF
Reg Formula: 0x001800 + 16*$BID + $CNT
    Where  : 
           + $BID(0-15) : Block ID
           + $CNT(0-15) D-Word
Reg Desc   : 
256 Location Buffer Store CFM Packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upencfm_Base                                                                          0x001800
#define cAf6Reg_upencfm(BID, CNT)                                                    (0x001800+16*(BID)+(CNT))
#define cAf6Reg_upencfm_WidthVal                                                                            64
#define cAf6Reg_upencfm_WriteMask                                                                          0x0

/*--------------------------------------
BitField Name: ff_i_dat
BitField Type: R_O
BitField Desc: Data Dump
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_upencfm_ff_i_dat_Bit_Start                                                                      0
#define cAf6_upencfm_ff_i_dat_Bit_End                                                                       63
#define cAf6_upencfm_ff_i_dat_Mask_01                                                                 cBit31_0
#define cAf6_upencfm_ff_i_dat_Shift_01                                                                       0
#define cAf6_upencfm_ff_i_dat_Mask_02                                                                 cBit31_0
#define cAf6_upencfm_ff_i_dat_Shift_02                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : CFM ENC Master Control
Reg Addr   : 0x002000-0x0027FF
Reg Formula: 0x002000 + $VCG
    Where  : 
           + $VCG(0-255): VCG ID
Reg Desc   : 
Info for Engine Read CFM Packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_tranboxf_Base                                                                    0x002000
#define cAf6Reg_upen_tranboxf(VCG)                                                            (0x002000+(VCG))
#define cAf6Reg_upen_tranboxf_WidthVal                                                                      32
#define cAf6Reg_upen_tranboxf_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: pgcfmilen
BitField Type: R/W
BitField Desc: Length of CFM Packet, Zero Expected is 1
BitField Bits: [22:9]
--------------------------------------*/
#define cAf6_upen_tranboxf_pgcfmilen_Bit_Start                                                               9
#define cAf6_upen_tranboxf_pgcfmilen_Bit_End                                                                22
#define cAf6_upen_tranboxf_pgcfmilen_Mask                                                             cBit22_9
#define cAf6_upen_tranboxf_pgcfmilen_Shift                                                                   9
#define cAf6_upen_tranboxf_pgcfmilen_MaxVal                                                             0x3fff
#define cAf6_upen_tranboxf_pgcfmilen_MinVal                                                                0x0
#define cAf6_upen_tranboxf_pgcfmilen_RstVal                                                                0x0

/*--------------------------------------
BitField Name: pgcfmiadd
BitField Type: R/W
BitField Desc: Engine Use this Field to Start Read Process at This Location,
Next Read Process is Incremented by 1 from This Field
BitField Bits: [8:1]
--------------------------------------*/
#define cAf6_upen_tranboxf_pgcfmiadd_Bit_Start                                                               1
#define cAf6_upen_tranboxf_pgcfmiadd_Bit_End                                                                 8
#define cAf6_upen_tranboxf_pgcfmiadd_Mask                                                              cBit8_1
#define cAf6_upen_tranboxf_pgcfmiadd_Shift                                                                   1
#define cAf6_upen_tranboxf_pgcfmiadd_MaxVal                                                               0xff
#define cAf6_upen_tranboxf_pgcfmiadd_MinVal                                                                0x0
#define cAf6_upen_tranboxf_pgcfmiadd_RstVal                                                                0x0

/*--------------------------------------
BitField Name: pgcfmiena
BitField Type: R/W
BitField Desc: Set 1 When CPU Want to Send a CFM Packet, Engine Will Clear This
Bit When Send Done
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_tranboxf_pgcfmiena_Bit_Start                                                               0
#define cAf6_upen_tranboxf_pgcfmiena_Bit_End                                                                 0
#define cAf6_upen_tranboxf_pgcfmiena_Mask                                                                cBit0
#define cAf6_upen_tranboxf_pgcfmiena_Shift                                                                   0
#define cAf6_upen_tranboxf_pgcfmiena_MaxVal                                                                0x1
#define cAf6_upen_tranboxf_pgcfmiena_MinVal                                                                0x0
#define cAf6_upen_tranboxf_pgcfmiena_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : CFM ENC Data Buffer
Reg Addr   : 0x010C00-0x010FFF
Reg Formula: 0x010C00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
256 Location Buffer Queuing

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfmp_Base                                                                        0x010C00
#define cAf6Reg_upen_cfmp(Location)                                                      (0x010C00+(Location))
#define cAf6Reg_upen_cfmp_WidthVal                                                                          64
#define cAf6Reg_upen_cfmp_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: cfm_o_dat
BitField Type: R/W
BitField Desc: CFM Frame
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_upen_cfmp_cfm_o_dat_Bit_Start                                                                   0
#define cAf6_upen_cfmp_cfm_o_dat_Bit_End                                                                    63
#define cAf6_upen_cfmp_cfm_o_dat_Mask_01                                                              cBit31_0
#define cAf6_upen_cfmp_cfm_o_dat_Shift_01                                                                    0
#define cAf6_upen_cfmp_cfm_o_dat_Mask_02                                                              cBit31_0
#define cAf6_upen_cfmp_cfm_o_dat_Shift_02                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : DEC Data Byte Counter
Reg Addr   : 0x020000-0x0200FF
Reg Formula: 0x020000 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
Byte Packet Count Send to TxBUF

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_clen_Base                                                                 0x020000
#define cAf6Reg_upencnt_dec_clen_ro_Base                                                              0x030000
#define cAf6Reg_upencnt_dec_clen(Location)                                               (0x020000+(Location))
#define cAf6Reg_upencnt_dec_clen_WidthVal                                                                   32
#define cAf6Reg_upencnt_dec_clen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cdeclen
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_clen_cdeclen_Bit_Start                                                              0
#define cAf6_upencnt_dec_clen_cdeclen_Bit_End                                                               31
#define cAf6_upencnt_dec_clen_cdeclen_Mask                                                            cBit31_0
#define cAf6_upencnt_dec_clen_cdeclen_Shift                                                                  0
#define cAf6_upencnt_dec_clen_cdeclen_MaxVal                                                        0xffffffff
#define cAf6_upencnt_dec_clen_cdeclen_MinVal                                                               0x0
#define cAf6_upencnt_dec_clen_cdeclen_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC Packet Counter
Reg Addr   : 0x020100-0x0201FF
Reg Formula: 0x020100 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
Packet Count Send to TxBUF

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_even1_Base                                                                0x020100
#define cAf6Reg_upencnt_dec_even1_ro_Base                                                             0x030100
#define cAf6Reg_upencnt_dec_even1(Location)                                              (0x020100+(Location))
#define cAf6Reg_upencnt_dec_even1_WidthVal                                                                  32
#define cAf6Reg_upencnt_dec_even1_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: egroeop
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_even1_egroeop_Bit_Start                                                             0
#define cAf6_upencnt_dec_even1_egroeop_Bit_End                                                              31
#define cAf6_upencnt_dec_even1_egroeop_Mask                                                           cBit31_0
#define cAf6_upencnt_dec_even1_egroeop_Shift                                                                 0
#define cAf6_upencnt_dec_even1_egroeop_MaxVal                                                       0xffffffff
#define cAf6_upencnt_dec_even1_egroeop_MinVal                                                              0x0
#define cAf6_upencnt_dec_even1_egroeop_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC Line Frame Counter
Reg Addr   : 0x02FF00-0x02FFFF(R2C)
Reg Formula: 0x02FF00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
IDLE and CHEC Correct Frame Count

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_cline_r2c_Base                                                            0x02FF00
#define cAf6Reg_upencnt_dec_cline_r2c(Location)                                          (0x02FF00+(Location))
#define cAf6Reg_upencnt_dec_cline_r2c_WidthVal                                                              64
#define cAf6Reg_upencnt_dec_cline_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: p_frmi_ochec_c
BitField Type: R2C
BitField Desc: CHEC Correct Count
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upencnt_dec_cline_r2c_p_frmi_ochec_c_Bit_Start                                                 32
#define cAf6_upencnt_dec_cline_r2c_p_frmi_ochec_c_Bit_End                                                   63
#define cAf6_upencnt_dec_cline_r2c_p_frmi_ochec_c_Mask                                                cBit31_0
#define cAf6_upencnt_dec_cline_r2c_p_frmi_ochec_c_Shift                                                      0
#define cAf6_upencnt_dec_cline_r2c_p_frmi_ochec_c_MaxVal                                                   0x0
#define cAf6_upencnt_dec_cline_r2c_p_frmi_ochec_c_MinVal                                                   0x0
#define cAf6_upencnt_dec_cline_r2c_p_frmi_ochec_c_RstVal                                                   0x0

/*--------------------------------------
BitField Name: p_frmi_oidle_f
BitField Type: R2C
BitField Desc: IDLE Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_cline_r2c_p_frmi_oidle_f_Bit_Start                                                  0
#define cAf6_upencnt_dec_cline_r2c_p_frmi_oidle_f_Bit_End                                                   31
#define cAf6_upencnt_dec_cline_r2c_p_frmi_oidle_f_Mask                                                cBit31_0
#define cAf6_upencnt_dec_cline_r2c_p_frmi_oidle_f_Shift                                                      0
#define cAf6_upencnt_dec_cline_r2c_p_frmi_oidle_f_MaxVal                                            0xffffffff
#define cAf6_upencnt_dec_cline_r2c_p_frmi_oidle_f_MinVal                                                   0x0
#define cAf6_upencnt_dec_cline_r2c_p_frmi_oidle_f_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC Line Frame Counter
Reg Addr   : 0x03FF00-0x03FFFF(RO)
Reg Formula: 0x03FF00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
IDLE and CHEC Correct Frame Count

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_cline_ro_Base                                                             0x03FF00
#define cAf6Reg_upencnt_dec_cline_ro(Location)                                           (0x03FF00+(Location))
#define cAf6Reg_upencnt_dec_cline_ro_WidthVal                                                               64
#define cAf6Reg_upencnt_dec_cline_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: p_frmi_ochec_c
BitField Type: R2C
BitField Desc: CHEC Correct Count
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upencnt_dec_cline_ro_p_frmi_ochec_c_Bit_Start                                                  32
#define cAf6_upencnt_dec_cline_ro_p_frmi_ochec_c_Bit_End                                                    63
#define cAf6_upencnt_dec_cline_ro_p_frmi_ochec_c_Mask                                                 cBit31_0
#define cAf6_upencnt_dec_cline_ro_p_frmi_ochec_c_Shift                                                       0
#define cAf6_upencnt_dec_cline_ro_p_frmi_ochec_c_MaxVal                                                    0x0
#define cAf6_upencnt_dec_cline_ro_p_frmi_ochec_c_MinVal                                                    0x0
#define cAf6_upencnt_dec_cline_ro_p_frmi_ochec_c_RstVal                                                    0x0

/*--------------------------------------
BitField Name: p_frmi_oidle_f
BitField Type: R2C
BitField Desc: IDLE Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_cline_ro_p_frmi_oidle_f_Bit_Start                                                   0
#define cAf6_upencnt_dec_cline_ro_p_frmi_oidle_f_Bit_End                                                    31
#define cAf6_upencnt_dec_cline_ro_p_frmi_oidle_f_Mask                                                 cBit31_0
#define cAf6_upencnt_dec_cline_ro_p_frmi_oidle_f_Shift                                                       0
#define cAf6_upencnt_dec_cline_ro_p_frmi_oidle_f_MaxVal                                             0xffffffff
#define cAf6_upencnt_dec_cline_ro_p_frmi_oidle_f_MinVal                                                    0x0
#define cAf6_upencnt_dec_cline_ro_p_frmi_oidle_f_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC C-HEC Error Counter
Reg Addr   : 0x020200-0x0202FF(R2C)
Reg Formula: 0x020200 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
DEC C-HEC Error Count

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_even4_r2c_Base                                                            0x020200
#define cAf6Reg_upencnt_dec_even4_r2c(Location)                                          (0x020200+(Location))
#define cAf6Reg_upencnt_dec_even4_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_dec_even4_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: p_frmi_ochec_e
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_even4_r2c_p_frmi_ochec_e_Bit_Start                                                  0
#define cAf6_upencnt_dec_even4_r2c_p_frmi_ochec_e_Bit_End                                                   31
#define cAf6_upencnt_dec_even4_r2c_p_frmi_ochec_e_Mask                                                cBit31_0
#define cAf6_upencnt_dec_even4_r2c_p_frmi_ochec_e_Shift                                                      0
#define cAf6_upencnt_dec_even4_r2c_p_frmi_ochec_e_MaxVal                                            0xffffffff
#define cAf6_upencnt_dec_even4_r2c_p_frmi_ochec_e_MinVal                                                   0x0
#define cAf6_upencnt_dec_even4_r2c_p_frmi_ochec_e_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC C-HEC Error Counter
Reg Addr   : 0x030200-0x0302FF(RO)
Reg Formula: 0x030200 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
DEC C-HEC Error Count

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_even4_ro_Base                                                             0x030200
#define cAf6Reg_upencnt_dec_even4_ro(Location)                                           (0x030200+(Location))
#define cAf6Reg_upencnt_dec_even4_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_dec_even4_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: p_frmi_ochec_e
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_even4_ro_p_frmi_ochec_e_Bit_Start                                                   0
#define cAf6_upencnt_dec_even4_ro_p_frmi_ochec_e_Bit_End                                                    31
#define cAf6_upencnt_dec_even4_ro_p_frmi_ochec_e_Mask                                                 cBit31_0
#define cAf6_upencnt_dec_even4_ro_p_frmi_ochec_e_Shift                                                       0
#define cAf6_upencnt_dec_even4_ro_p_frmi_ochec_e_MaxVal                                             0xffffffff
#define cAf6_upencnt_dec_even4_ro_p_frmi_ochec_e_MinVal                                                    0x0
#define cAf6_upencnt_dec_even4_ro_p_frmi_ochec_e_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC UPI Mismatch Counter
Reg Addr   : 0x020300-0x0203FF(R2C)
Reg Formula: 0x020300 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
UPI Mismatch Count in GFP Mode or SAPI Mismatch Count in HDLC Mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_even5_r2c_Base                                                            0x020300
#define cAf6Reg_upencnt_dec_even5_r2c(Location)                                          (0x020300+(Location))
#define cAf6Reg_upencnt_dec_even5_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_dec_even5_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: o_gfpf_omisupi
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_even5_r2c_o_gfpf_omisupi_Bit_Start                                                  0
#define cAf6_upencnt_dec_even5_r2c_o_gfpf_omisupi_Bit_End                                                   31
#define cAf6_upencnt_dec_even5_r2c_o_gfpf_omisupi_Mask                                                cBit31_0
#define cAf6_upencnt_dec_even5_r2c_o_gfpf_omisupi_Shift                                                      0
#define cAf6_upencnt_dec_even5_r2c_o_gfpf_omisupi_MaxVal                                            0xffffffff
#define cAf6_upencnt_dec_even5_r2c_o_gfpf_omisupi_MinVal                                                   0x0
#define cAf6_upencnt_dec_even5_r2c_o_gfpf_omisupi_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC UPI Mismatch Counter
Reg Addr   : 0x030300-0x0303FF(RO)
Reg Formula: 0x030300 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
UPI Mismatch Count in GFP Mode or SAPI Mismatch Count in HDLC Mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_even5_ro_Base                                                             0x030300
#define cAf6Reg_upencnt_dec_even5_ro(Location)                                           (0x030300+(Location))
#define cAf6Reg_upencnt_dec_even5_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_dec_even5_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: o_gfpf_omisupi
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_even5_ro_o_gfpf_omisupi_Bit_Start                                                   0
#define cAf6_upencnt_dec_even5_ro_o_gfpf_omisupi_Bit_End                                                    31
#define cAf6_upencnt_dec_even5_ro_o_gfpf_omisupi_Mask                                                 cBit31_0
#define cAf6_upencnt_dec_even5_ro_o_gfpf_omisupi_Shift                                                       0
#define cAf6_upencnt_dec_even5_ro_o_gfpf_omisupi_MaxVal                                             0xffffffff
#define cAf6_upencnt_dec_even5_ro_o_gfpf_omisupi_MinVal                                                    0x0
#define cAf6_upencnt_dec_even5_ro_o_gfpf_omisupi_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CRC Error Counter
Reg Addr   : 0x020400-0x0204FF(R2C)
Reg Formula: 0x020400 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
DEC CRC Error Count

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_even6_r2c_Base                                                            0x020400
#define cAf6Reg_upencnt_dec_even6_r2c(Location)                                          (0x020400+(Location))
#define cAf6Reg_upencnt_dec_even6_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_dec_even6_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: o_gfpf_ocrcerr
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_even6_r2c_o_gfpf_ocrcerr_Bit_Start                                                  0
#define cAf6_upencnt_dec_even6_r2c_o_gfpf_ocrcerr_Bit_End                                                   31
#define cAf6_upencnt_dec_even6_r2c_o_gfpf_ocrcerr_Mask                                                cBit31_0
#define cAf6_upencnt_dec_even6_r2c_o_gfpf_ocrcerr_Shift                                                      0
#define cAf6_upencnt_dec_even6_r2c_o_gfpf_ocrcerr_MaxVal                                            0xffffffff
#define cAf6_upencnt_dec_even6_r2c_o_gfpf_ocrcerr_MinVal                                                   0x0
#define cAf6_upencnt_dec_even6_r2c_o_gfpf_ocrcerr_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CRC Error Counter
Reg Addr   : 0x030400-0x0304FF(RO)
Reg Formula: 0x030400 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
DEC CRC Error Count

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_even6_ro_Base                                                             0x030400
#define cAf6Reg_upencnt_dec_even6_ro(Location)                                           (0x030400+(Location))
#define cAf6Reg_upencnt_dec_even6_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_dec_even6_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: o_gfpf_ocrcerr
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_even6_ro_o_gfpf_ocrcerr_Bit_Start                                                   0
#define cAf6_upencnt_dec_even6_ro_o_gfpf_ocrcerr_Bit_End                                                    31
#define cAf6_upencnt_dec_even6_ro_o_gfpf_ocrcerr_Mask                                                 cBit31_0
#define cAf6_upencnt_dec_even6_ro_o_gfpf_ocrcerr_Shift                                                       0
#define cAf6_upencnt_dec_even6_ro_o_gfpf_ocrcerr_MaxVal                                             0xffffffff
#define cAf6_upencnt_dec_even6_ro_o_gfpf_ocrcerr_MinVal                                                    0x0
#define cAf6_upencnt_dec_even6_ro_o_gfpf_ocrcerr_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC Packet Drop Counter
Reg Addr   : 0x020500-0x0205FF(R2C)
Reg Formula: 0x020500 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
DEC Packet Drop Count

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_even7_r2c_Base                                                            0x020500
#define cAf6Reg_upencnt_dec_even7_r2c(Location)                                          (0x020500+(Location))
#define cAf6Reg_upencnt_dec_even7_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_dec_even7_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: o_gfpf_odrop_e
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_even7_r2c_o_gfpf_odrop_e_Bit_Start                                                  0
#define cAf6_upencnt_dec_even7_r2c_o_gfpf_odrop_e_Bit_End                                                   31
#define cAf6_upencnt_dec_even7_r2c_o_gfpf_odrop_e_Mask                                                cBit31_0
#define cAf6_upencnt_dec_even7_r2c_o_gfpf_odrop_e_Shift                                                      0
#define cAf6_upencnt_dec_even7_r2c_o_gfpf_odrop_e_MaxVal                                            0xffffffff
#define cAf6_upencnt_dec_even7_r2c_o_gfpf_odrop_e_MinVal                                                   0x0
#define cAf6_upencnt_dec_even7_r2c_o_gfpf_odrop_e_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC Packet Drop Counter
Reg Addr   : 0x030500-0x0305FF(RO)
Reg Formula: 0x030500 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
DEC Packet Drop Count

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_even7_ro_Base                                                             0x030500
#define cAf6Reg_upencnt_dec_even7_ro(Location)                                           (0x030500+(Location))
#define cAf6Reg_upencnt_dec_even7_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_dec_even7_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: o_gfpf_odrop_e
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_even7_ro_o_gfpf_odrop_e_Bit_Start                                                   0
#define cAf6_upencnt_dec_even7_ro_o_gfpf_odrop_e_Bit_End                                                    31
#define cAf6_upencnt_dec_even7_ro_o_gfpf_odrop_e_Mask                                                 cBit31_0
#define cAf6_upencnt_dec_even7_ro_o_gfpf_odrop_e_Shift                                                       0
#define cAf6_upencnt_dec_even7_ro_o_gfpf_odrop_e_MaxVal                                             0xffffffff
#define cAf6_upencnt_dec_even7_ro_o_gfpf_odrop_e_MinVal                                                    0x0
#define cAf6_upencnt_dec_even7_ro_o_gfpf_odrop_e_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC T-HEC Correct Counter
Reg Addr   : 0x020600-0x0206FF(R2C)
Reg Formula: 0x020600 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
DEC T-HEC Correct Count

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_even8_r2c_Base                                                            0x020600
#define cAf6Reg_upencnt_dec_even8_r2c(Location)                                          (0x020600+(Location))
#define cAf6Reg_upencnt_dec_even8_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_dec_even8_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: o_gfpf_othec_c
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_even8_r2c_o_gfpf_othec_c_Bit_Start                                                  0
#define cAf6_upencnt_dec_even8_r2c_o_gfpf_othec_c_Bit_End                                                   31
#define cAf6_upencnt_dec_even8_r2c_o_gfpf_othec_c_Mask                                                cBit31_0
#define cAf6_upencnt_dec_even8_r2c_o_gfpf_othec_c_Shift                                                      0
#define cAf6_upencnt_dec_even8_r2c_o_gfpf_othec_c_MaxVal                                            0xffffffff
#define cAf6_upencnt_dec_even8_r2c_o_gfpf_othec_c_MinVal                                                   0x0
#define cAf6_upencnt_dec_even8_r2c_o_gfpf_othec_c_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC T-HEC Correct Counter
Reg Addr   : 0x030600-0x0306FF(RO)
Reg Formula: 0x030600 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
DEC T-HEC Correct Count

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_even8_ro_Base                                                             0x030600
#define cAf6Reg_upencnt_dec_even8_ro(Location)                                           (0x030600+(Location))
#define cAf6Reg_upencnt_dec_even8_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_dec_even8_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: o_gfpf_othec_c
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_even8_ro_o_gfpf_othec_c_Bit_Start                                                   0
#define cAf6_upencnt_dec_even8_ro_o_gfpf_othec_c_Bit_End                                                    31
#define cAf6_upencnt_dec_even8_ro_o_gfpf_othec_c_Mask                                                 cBit31_0
#define cAf6_upencnt_dec_even8_ro_o_gfpf_othec_c_Shift                                                       0
#define cAf6_upencnt_dec_even8_ro_o_gfpf_othec_c_MaxVal                                             0xffffffff
#define cAf6_upencnt_dec_even8_ro_o_gfpf_othec_c_MinVal                                                    0x0
#define cAf6_upencnt_dec_even8_ro_o_gfpf_othec_c_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC T-HEC Error Counter
Reg Addr   : 0x020700-0x0207FF(R2C)
Reg Formula: 0x020700 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
DEC T-HEC Error Count in GFP Mode or DEC Abort Count in HDLC Mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_even9_r2c_Base                                                            0x020700
#define cAf6Reg_upencnt_dec_even9_r2c(Location)                                          (0x020700+(Location))
#define cAf6Reg_upencnt_dec_even9_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_dec_even9_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: o_gfpf_othec_e
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_even9_r2c_o_gfpf_othec_e_Bit_Start                                                  0
#define cAf6_upencnt_dec_even9_r2c_o_gfpf_othec_e_Bit_End                                                   31
#define cAf6_upencnt_dec_even9_r2c_o_gfpf_othec_e_Mask                                                cBit31_0
#define cAf6_upencnt_dec_even9_r2c_o_gfpf_othec_e_Shift                                                      0
#define cAf6_upencnt_dec_even9_r2c_o_gfpf_othec_e_MaxVal                                            0xffffffff
#define cAf6_upencnt_dec_even9_r2c_o_gfpf_othec_e_MinVal                                                   0x0
#define cAf6_upencnt_dec_even9_r2c_o_gfpf_othec_e_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC T-HEC Error Counter
Reg Addr   : 0x030700-0x0307FF(RO)
Reg Formula: 0x030700 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
DEC T-HEC Error Count in GFP Mode or DEC Abort Count in HDLC Mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_dec_even9_ro_Base                                                             0x030700
#define cAf6Reg_upencnt_dec_even9_ro(Location)                                           (0x030700+(Location))
#define cAf6Reg_upencnt_dec_even9_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_dec_even9_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: o_gfpf_othec_e
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_dec_even9_ro_o_gfpf_othec_e_Bit_Start                                                   0
#define cAf6_upencnt_dec_even9_ro_o_gfpf_othec_e_Bit_End                                                    31
#define cAf6_upencnt_dec_even9_ro_o_gfpf_othec_e_Mask                                                 cBit31_0
#define cAf6_upencnt_dec_even9_ro_o_gfpf_othec_e_Shift                                                       0
#define cAf6_upencnt_dec_even9_ro_o_gfpf_othec_e_MaxVal                                             0xffffffff
#define cAf6_upencnt_dec_even9_ro_o_gfpf_othec_e_MinVal                                                    0x0
#define cAf6_upencnt_dec_even9_ro_o_gfpf_othec_e_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CFM loss of client signal frame counter
Reg Addr   : 0x020800-0x0208FF(R2C)
Reg Formula: 0x020800 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet With PTI is 4 and UPI is 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_oam_even1_r2c_Base                                                            0x020800
#define cAf6Reg_upencnt_oam_even1_r2c(Location)                                          (0x020800+(Location))
#define cAf6Reg_upencnt_oam_even1_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_oam_even1_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: cntotyp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_oam_even1_r2c_cntotyp_Bit_Start                                                         0
#define cAf6_upencnt_oam_even1_r2c_cntotyp_Bit_End                                                          31
#define cAf6_upencnt_oam_even1_r2c_cntotyp_Mask                                                       cBit31_0
#define cAf6_upencnt_oam_even1_r2c_cntotyp_Shift                                                             0
#define cAf6_upencnt_oam_even1_r2c_cntotyp_MaxVal                                                   0xffffffff
#define cAf6_upencnt_oam_even1_r2c_cntotyp_MinVal                                                          0x0
#define cAf6_upencnt_oam_even1_r2c_cntotyp_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CFM loss of client signal frame counter
Reg Addr   : 0x030800-0x0308FF(RO)
Reg Formula: 0x030800 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet With PTI is 4 and UPI is 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_oam_even1_ro_Base                                                             0x030800
#define cAf6Reg_upencnt_oam_even1_ro(Location)                                           (0x030800+(Location))
#define cAf6Reg_upencnt_oam_even1_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_oam_even1_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: cntotyp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_oam_even1_ro_cntotyp_Bit_Start                                                          0
#define cAf6_upencnt_oam_even1_ro_cntotyp_Bit_End                                                           31
#define cAf6_upencnt_oam_even1_ro_cntotyp_Mask                                                        cBit31_0
#define cAf6_upencnt_oam_even1_ro_cntotyp_Shift                                                              0
#define cAf6_upencnt_oam_even1_ro_cntotyp_MaxVal                                                    0xffffffff
#define cAf6_upencnt_oam_even1_ro_cntotyp_MinVal                                                           0x0
#define cAf6_upencnt_oam_even1_ro_cntotyp_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CFM loss of character synchronization counter
Reg Addr   : 0x020900-0x0209FF(R2C)
Reg Formula: 0x020900 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet With PTI is 4 and UPI is 2

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_oam_even2_r2c_Base                                                            0x020900
#define cAf6Reg_upencnt_oam_even2_r2c(Location)                                          (0x020900+(Location))
#define cAf6Reg_upencnt_oam_even2_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_oam_even2_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: cntotyp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_oam_even2_r2c_cntotyp_Bit_Start                                                         0
#define cAf6_upencnt_oam_even2_r2c_cntotyp_Bit_End                                                          31
#define cAf6_upencnt_oam_even2_r2c_cntotyp_Mask                                                       cBit31_0
#define cAf6_upencnt_oam_even2_r2c_cntotyp_Shift                                                             0
#define cAf6_upencnt_oam_even2_r2c_cntotyp_MaxVal                                                   0xffffffff
#define cAf6_upencnt_oam_even2_r2c_cntotyp_MinVal                                                          0x0
#define cAf6_upencnt_oam_even2_r2c_cntotyp_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CFM loss of character synchronization counter
Reg Addr   : 0x030900-0x0309FF(RO)
Reg Formula: 0x030900 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet With PTI is 4 and UPI is 2

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_oam_even2_ro_Base                                                             0x030900
#define cAf6Reg_upencnt_oam_even2_ro(Location)                                           (0x030900+(Location))
#define cAf6Reg_upencnt_oam_even2_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_oam_even2_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: cntotyp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_oam_even2_ro_cntotyp_Bit_Start                                                          0
#define cAf6_upencnt_oam_even2_ro_cntotyp_Bit_End                                                           31
#define cAf6_upencnt_oam_even2_ro_cntotyp_Mask                                                        cBit31_0
#define cAf6_upencnt_oam_even2_ro_cntotyp_Shift                                                              0
#define cAf6_upencnt_oam_even2_ro_cntotyp_MaxVal                                                    0xffffffff
#define cAf6_upencnt_oam_even2_ro_cntotyp_MinVal                                                           0x0
#define cAf6_upencnt_oam_even2_ro_cntotyp_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CFM Client defect clear indication counter
Reg Addr   : 0x020A00-0x020AFF(R2C)
Reg Formula: 0x020A00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet With PTI is 4 and UPI is 3

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_oam_even3_r2c_Base                                                            0x020A00
#define cAf6Reg_upencnt_oam_even3_r2c(Location)                                          (0x020A00+(Location))
#define cAf6Reg_upencnt_oam_even3_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_oam_even3_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: cntotyp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_oam_even3_r2c_cntotyp_Bit_Start                                                         0
#define cAf6_upencnt_oam_even3_r2c_cntotyp_Bit_End                                                          31
#define cAf6_upencnt_oam_even3_r2c_cntotyp_Mask                                                       cBit31_0
#define cAf6_upencnt_oam_even3_r2c_cntotyp_Shift                                                             0
#define cAf6_upencnt_oam_even3_r2c_cntotyp_MaxVal                                                   0xffffffff
#define cAf6_upencnt_oam_even3_r2c_cntotyp_MinVal                                                          0x0
#define cAf6_upencnt_oam_even3_r2c_cntotyp_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CFM Client defect clear indication counter
Reg Addr   : 0x030A00-0x030AFF(RO)
Reg Formula: 0x030A00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet With PTI is 4 and UPI is 3

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_oam_even3_ro_Base                                                             0x030A00
#define cAf6Reg_upencnt_oam_even3_ro(Location)                                           (0x030A00+(Location))
#define cAf6Reg_upencnt_oam_even3_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_oam_even3_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: cntotyp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_oam_even3_ro_cntotyp_Bit_Start                                                          0
#define cAf6_upencnt_oam_even3_ro_cntotyp_Bit_End                                                           31
#define cAf6_upencnt_oam_even3_ro_cntotyp_Mask                                                        cBit31_0
#define cAf6_upencnt_oam_even3_ro_cntotyp_Shift                                                              0
#define cAf6_upencnt_oam_even3_ro_cntotyp_MaxVal                                                    0xffffffff
#define cAf6_upencnt_oam_even3_ro_cntotyp_MinVal                                                           0x0
#define cAf6_upencnt_oam_even3_ro_cntotyp_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CFM Client forward defect indication counter
Reg Addr   : 0x020B00-0x020BFF(R2C)
Reg Formula: 0x020B00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet With PTI is 4 and UPI is 4

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_oam_even4_r2c_Base                                                            0x020B00
#define cAf6Reg_upencnt_oam_even4_r2c(Location)                                          (0x020B00+(Location))
#define cAf6Reg_upencnt_oam_even4_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_oam_even4_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: cntotyp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_oam_even4_r2c_cntotyp_Bit_Start                                                         0
#define cAf6_upencnt_oam_even4_r2c_cntotyp_Bit_End                                                          31
#define cAf6_upencnt_oam_even4_r2c_cntotyp_Mask                                                       cBit31_0
#define cAf6_upencnt_oam_even4_r2c_cntotyp_Shift                                                             0
#define cAf6_upencnt_oam_even4_r2c_cntotyp_MaxVal                                                   0xffffffff
#define cAf6_upencnt_oam_even4_r2c_cntotyp_MinVal                                                          0x0
#define cAf6_upencnt_oam_even4_r2c_cntotyp_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CFM Client forward defect indication counter
Reg Addr   : 0x030B00-0x030BFF(RO)
Reg Formula: 0x030B00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet With PTI is 4 and UPI is 4

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_oam_even4_ro_Base                                                             0x030B00
#define cAf6Reg_upencnt_oam_even4_ro(Location)                                           (0x030B00+(Location))
#define cAf6Reg_upencnt_oam_even4_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_oam_even4_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: cntotyp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_oam_even4_ro_cntotyp_Bit_Start                                                          0
#define cAf6_upencnt_oam_even4_ro_cntotyp_Bit_End                                                           31
#define cAf6_upencnt_oam_even4_ro_cntotyp_Mask                                                        cBit31_0
#define cAf6_upencnt_oam_even4_ro_cntotyp_Shift                                                              0
#define cAf6_upencnt_oam_even4_ro_cntotyp_MaxVal                                                    0xffffffff
#define cAf6_upencnt_oam_even4_ro_cntotyp_MinVal                                                           0x0
#define cAf6_upencnt_oam_even4_ro_cntotyp_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CFM Client reverse defect indication counter
Reg Addr   : 0x020C00-0x020CFF(R2C)
Reg Formula: 0x020C00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet With PTI is 4 and UPI is 5

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_oam_even5_r2c_Base                                                            0x020C00
#define cAf6Reg_upencnt_oam_even5_r2c(Location)                                          (0x020C00+(Location))
#define cAf6Reg_upencnt_oam_even5_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_oam_even5_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: cntotyp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_oam_even5_r2c_cntotyp_Bit_Start                                                         0
#define cAf6_upencnt_oam_even5_r2c_cntotyp_Bit_End                                                          31
#define cAf6_upencnt_oam_even5_r2c_cntotyp_Mask                                                       cBit31_0
#define cAf6_upencnt_oam_even5_r2c_cntotyp_Shift                                                             0
#define cAf6_upencnt_oam_even5_r2c_cntotyp_MaxVal                                                   0xffffffff
#define cAf6_upencnt_oam_even5_r2c_cntotyp_MinVal                                                          0x0
#define cAf6_upencnt_oam_even5_r2c_cntotyp_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CFM Client reverse defect indication counter
Reg Addr   : 0x030C00-0x030CFF(RO)
Reg Formula: 0x030C00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet With PTI is 4 and UPI is 5

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_oam_even5_ro_Base                                                             0x030C00
#define cAf6Reg_upencnt_oam_even5_ro(Location)                                           (0x030C00+(Location))
#define cAf6Reg_upencnt_oam_even5_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_oam_even5_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: cntotyp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_oam_even5_ro_cntotyp_Bit_Start                                                          0
#define cAf6_upencnt_oam_even5_ro_cntotyp_Bit_End                                                           31
#define cAf6_upencnt_oam_even5_ro_cntotyp_Mask                                                        cBit31_0
#define cAf6_upencnt_oam_even5_ro_cntotyp_Shift                                                              0
#define cAf6_upencnt_oam_even5_ro_cntotyp_MaxVal                                                    0xffffffff
#define cAf6_upencnt_oam_even5_ro_cntotyp_MinVal                                                           0x0
#define cAf6_upencnt_oam_even5_ro_cntotyp_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CFM Reserved counter
Reg Addr   : 0x020D00-0x020DFF(R2C)
Reg Formula: 0x020D00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet With PTI is 4 and UPI are 0x0, 0xFF or 0xE0 to 0xFE

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_oam_even6_r2c_Base                                                            0x020D00
#define cAf6Reg_upencnt_oam_even6_r2c(Location)                                          (0x020D00+(Location))
#define cAf6Reg_upencnt_oam_even6_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_oam_even6_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: cntotyp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_oam_even6_r2c_cntotyp_Bit_Start                                                         0
#define cAf6_upencnt_oam_even6_r2c_cntotyp_Bit_End                                                          31
#define cAf6_upencnt_oam_even6_r2c_cntotyp_Mask                                                       cBit31_0
#define cAf6_upencnt_oam_even6_r2c_cntotyp_Shift                                                             0
#define cAf6_upencnt_oam_even6_r2c_cntotyp_MaxVal                                                   0xffffffff
#define cAf6_upencnt_oam_even6_r2c_cntotyp_MinVal                                                          0x0
#define cAf6_upencnt_oam_even6_r2c_cntotyp_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : DEC CFM Reserved counter
Reg Addr   : 0x030D00-0x030DFF(RO)
Reg Formula: 0x030D00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet With PTI is 4 and UPI are 0x0, 0xFF or 0xE0 to 0xFE

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_oam_even6_ro_Base                                                             0x030D00
#define cAf6Reg_upencnt_oam_even6_ro(Location)                                           (0x030D00+(Location))
#define cAf6Reg_upencnt_oam_even6_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_oam_even6_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: cntotyp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_oam_even6_ro_cntotyp_Bit_Start                                                          0
#define cAf6_upencnt_oam_even6_ro_cntotyp_Bit_End                                                           31
#define cAf6_upencnt_oam_even6_ro_cntotyp_Mask                                                        cBit31_0
#define cAf6_upencnt_oam_even6_ro_cntotyp_Shift                                                              0
#define cAf6_upencnt_oam_even6_ro_cntotyp_MaxVal                                                    0xffffffff
#define cAf6_upencnt_oam_even6_ro_cntotyp_MinVal                                                           0x0
#define cAf6_upencnt_oam_even6_ro_cntotyp_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : ENC Data Byte Counter
Reg Addr   : 0x020E00-0x020EFF(R2C)
Reg Formula: 0x020E00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
Byte Packet Count Send to V-CAT

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_enc_clen_r2c_Base                                                             0x020E00
#define cAf6Reg_upencnt_enc_clen_r2c(Location)                                           (0x020E00+(Location))
#define cAf6Reg_upencnt_enc_clen_r2c_WidthVal                                                               32
#define cAf6Reg_upencnt_enc_clen_r2c_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: cenclen
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_enc_clen_r2c_cenclen_Bit_Start                                                          0
#define cAf6_upencnt_enc_clen_r2c_cenclen_Bit_End                                                           31
#define cAf6_upencnt_enc_clen_r2c_cenclen_Mask                                                        cBit31_0
#define cAf6_upencnt_enc_clen_r2c_cenclen_Shift                                                              0
#define cAf6_upencnt_enc_clen_r2c_cenclen_MaxVal                                                    0xffffffff
#define cAf6_upencnt_enc_clen_r2c_cenclen_MinVal                                                           0x0
#define cAf6_upencnt_enc_clen_r2c_cenclen_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : ENC Data Byte Counter
Reg Addr   : 0x030E00-0x030EFF(RO)
Reg Formula: 0x030E00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
Byte Packet Count Send to V-CAT

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_enc_clen_ro_Base                                                              0x030E00
#define cAf6Reg_upencnt_enc_clen_ro(Location)                                            (0x030E00+(Location))
#define cAf6Reg_upencnt_enc_clen_ro_WidthVal                                                                32
#define cAf6Reg_upencnt_enc_clen_ro_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cenclen
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_enc_clen_ro_cenclen_Bit_Start                                                           0
#define cAf6_upencnt_enc_clen_ro_cenclen_Bit_End                                                            31
#define cAf6_upencnt_enc_clen_ro_cenclen_Mask                                                         cBit31_0
#define cAf6_upencnt_enc_clen_ro_cenclen_Shift                                                               0
#define cAf6_upencnt_enc_clen_ro_cenclen_MaxVal                                                     0xffffffff
#define cAf6_upencnt_enc_clen_ro_cenclen_MinVal                                                            0x0
#define cAf6_upencnt_enc_clen_ro_cenclen_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : ENC Packet Counter
Reg Addr   : 0x020F00-0x020FFF(R2C)
Reg Formula: 0x020F00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
Packet Count Send to V-CAT

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_enc_even1_r2c_Base                                                            0x020F00
#define cAf6Reg_upencnt_enc_even1_r2c(Location)                                          (0x020F00+(Location))
#define cAf6Reg_upencnt_enc_even1_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_enc_even1_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: encieop
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_enc_even1_r2c_encieop_Bit_Start                                                         0
#define cAf6_upencnt_enc_even1_r2c_encieop_Bit_End                                                          31
#define cAf6_upencnt_enc_even1_r2c_encieop_Mask                                                       cBit31_0
#define cAf6_upencnt_enc_even1_r2c_encieop_Shift                                                             0
#define cAf6_upencnt_enc_even1_r2c_encieop_MaxVal                                                   0xffffffff
#define cAf6_upencnt_enc_even1_r2c_encieop_MinVal                                                          0x0
#define cAf6_upencnt_enc_even1_r2c_encieop_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : ENC Packet Counter
Reg Addr   : 0x030F00-0x030FFF(RO)
Reg Formula: 0x030F00 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
Packet Count Send to V-CAT

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_enc_even1_ro_Base                                                             0x030F00
#define cAf6Reg_upencnt_enc_even1_ro(Location)                                           (0x030F00+(Location))
#define cAf6Reg_upencnt_enc_even1_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_enc_even1_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: encieop
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_enc_even1_ro_encieop_Bit_Start                                                          0
#define cAf6_upencnt_enc_even1_ro_encieop_Bit_End                                                           31
#define cAf6_upencnt_enc_even1_ro_encieop_Mask                                                        cBit31_0
#define cAf6_upencnt_enc_even1_ro_encieop_Shift                                                              0
#define cAf6_upencnt_enc_even1_ro_encieop_MaxVal                                                    0xffffffff
#define cAf6_upencnt_enc_even1_ro_encieop_MinVal                                                           0x0
#define cAf6_upencnt_enc_even1_ro_encieop_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : ENC IDLE Frame Counter
Reg Addr   : 0x021000-0x0210FF(R2C)
Reg Formula: 0x021000 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
IDLE Frame Count

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_enc_even2_r2c_Base                                                            0x021000
#define cAf6Reg_upencnt_enc_even2_r2c(Location)                                          (0x021000+(Location))
#define cAf6Reg_upencnt_enc_even2_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_enc_even2_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: idlegfp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_enc_even2_r2c_idlegfp_Bit_Start                                                         0
#define cAf6_upencnt_enc_even2_r2c_idlegfp_Bit_End                                                          31
#define cAf6_upencnt_enc_even2_r2c_idlegfp_Mask                                                       cBit31_0
#define cAf6_upencnt_enc_even2_r2c_idlegfp_Shift                                                             0
#define cAf6_upencnt_enc_even2_r2c_idlegfp_MaxVal                                                   0xffffffff
#define cAf6_upencnt_enc_even2_r2c_idlegfp_MinVal                                                          0x0
#define cAf6_upencnt_enc_even2_r2c_idlegfp_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : ENC IDLE Frame Counter
Reg Addr   : 0x031000-0x0310FF(RO)
Reg Formula: 0x031000 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
IDLE Frame Count

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_enc_even2_ro_Base                                                             0x031000
#define cAf6Reg_upencnt_enc_even2_ro(Location)                                           (0x031000+(Location))
#define cAf6Reg_upencnt_enc_even2_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_enc_even2_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: idlegfp
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_enc_even2_ro_idlegfp_Bit_Start                                                          0
#define cAf6_upencnt_enc_even2_ro_idlegfp_Bit_End                                                           31
#define cAf6_upencnt_enc_even2_ro_idlegfp_Mask                                                        cBit31_0
#define cAf6_upencnt_enc_even2_ro_idlegfp_Shift                                                              0
#define cAf6_upencnt_enc_even2_ro_idlegfp_MaxVal                                                    0xffffffff
#define cAf6_upencnt_enc_even2_ro_idlegfp_MinVal                                                           0x0
#define cAf6_upencnt_enc_even2_ro_idlegfp_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : ENC Abort Packet Counter
Reg Addr   : 0x021100-0x0211FF(R2C)
Reg Formula: 0x021100 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
Abort Frame Count on ENC

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_enc_even3_r2c_Base                                                            0x021100
#define cAf6Reg_upencnt_enc_even3_r2c(Location)                                          (0x021100+(Location))
#define cAf6Reg_upencnt_enc_even3_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_enc_even3_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: iabort
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_enc_even3_r2c_iabort_Bit_Start                                                          0
#define cAf6_upencnt_enc_even3_r2c_iabort_Bit_End                                                           31
#define cAf6_upencnt_enc_even3_r2c_iabort_Mask                                                        cBit31_0
#define cAf6_upencnt_enc_even3_r2c_iabort_Shift                                                              0
#define cAf6_upencnt_enc_even3_r2c_iabort_MaxVal                                                    0xffffffff
#define cAf6_upencnt_enc_even3_r2c_iabort_MinVal                                                           0x0
#define cAf6_upencnt_enc_even3_r2c_iabort_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : ENC Abort Packet Counter
Reg Addr   : 0x031100-0x0311FF(RO)
Reg Formula: 0x031100 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
Abort Frame Count on ENC

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_enc_even3_ro_Base                                                             0x031100
#define cAf6Reg_upencnt_enc_even3_ro(Location)                                           (0x031100+(Location))
#define cAf6Reg_upencnt_enc_even3_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_enc_even3_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: iabort
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_enc_even3_ro_iabort_Bit_Start                                                           0
#define cAf6_upencnt_enc_even3_ro_iabort_Bit_End                                                            31
#define cAf6_upencnt_enc_even3_ro_iabort_Mask                                                         cBit31_0
#define cAf6_upencnt_enc_even3_ro_iabort_Shift                                                               0
#define cAf6_upencnt_enc_even3_ro_iabort_MaxVal                                                     0xffffffff
#define cAf6_upencnt_enc_even3_ro_iabort_MinVal                                                            0x0
#define cAf6_upencnt_enc_even3_ro_iabort_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : ENC CSF Frame Counter
Reg Addr   : 0x021200-0x0212FF(R2C)
Reg Formula: 0x021200 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CSF Packet Send to V-CAT

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_enc_oamp1_r2c_Base                                                            0x021200
#define cAf6Reg_upencnt_enc_oamp1_r2c(Location)                                          (0x021200+(Location))
#define cAf6Reg_upencnt_enc_oamp1_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_enc_oamp1_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: cntoice
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_enc_oamp1_r2c_cntoice_Bit_Start                                                         0
#define cAf6_upencnt_enc_oamp1_r2c_cntoice_Bit_End                                                          31
#define cAf6_upencnt_enc_oamp1_r2c_cntoice_Mask                                                       cBit31_0
#define cAf6_upencnt_enc_oamp1_r2c_cntoice_Shift                                                             0
#define cAf6_upencnt_enc_oamp1_r2c_cntoice_MaxVal                                                   0xffffffff
#define cAf6_upencnt_enc_oamp1_r2c_cntoice_MinVal                                                          0x0
#define cAf6_upencnt_enc_oamp1_r2c_cntoice_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : ENC CSF Frame Counter
Reg Addr   : 0x031200-0x0312FF(RO)
Reg Formula: 0x031200 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CSF Packet Send to V-CAT

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_enc_oamp1_ro_Base                                                             0x031200
#define cAf6Reg_upencnt_enc_oamp1_ro(Location)                                           (0x031200+(Location))
#define cAf6Reg_upencnt_enc_oamp1_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_enc_oamp1_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: cntoice
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_enc_oamp1_ro_cntoice_Bit_Start                                                          0
#define cAf6_upencnt_enc_oamp1_ro_cntoice_Bit_End                                                           31
#define cAf6_upencnt_enc_oamp1_ro_cntoice_Mask                                                        cBit31_0
#define cAf6_upencnt_enc_oamp1_ro_cntoice_Shift                                                              0
#define cAf6_upencnt_enc_oamp1_ro_cntoice_MaxVal                                                    0xffffffff
#define cAf6_upencnt_enc_oamp1_ro_cntoice_MinVal                                                           0x0
#define cAf6_upencnt_enc_oamp1_ro_cntoice_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : ENC CFM Frame Counter
Reg Addr   : 0x021300-0x0213FF(R2C)
Reg Formula: 0x021300 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet Send to V-CAT

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_enc_oamp2_r2c_Base                                                            0x021300
#define cAf6Reg_upencnt_enc_oamp2_r2c(Location)                                          (0x021300+(Location))
#define cAf6Reg_upencnt_enc_oamp2_r2c_WidthVal                                                              32
#define cAf6Reg_upencnt_enc_oamp2_r2c_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: cntoice
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_enc_oamp2_r2c_cntoice_Bit_Start                                                         0
#define cAf6_upencnt_enc_oamp2_r2c_cntoice_Bit_End                                                          31
#define cAf6_upencnt_enc_oamp2_r2c_cntoice_Mask                                                       cBit31_0
#define cAf6_upencnt_enc_oamp2_r2c_cntoice_Shift                                                             0
#define cAf6_upencnt_enc_oamp2_r2c_cntoice_MaxVal                                                   0xffffffff
#define cAf6_upencnt_enc_oamp2_r2c_cntoice_MinVal                                                          0x0
#define cAf6_upencnt_enc_oamp2_r2c_cntoice_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : ENC CFM Frame Counter
Reg Addr   : 0x031300-0x0313FF(RO)
Reg Formula: 0x031300 + $Location
    Where  : 
           + $Location(0-255)
Reg Desc   : 
CFM Packet Send to V-CAT

------------------------------------------------------------------------------*/
#define cAf6Reg_upencnt_enc_oamp2_ro_Base                                                             0x031300
#define cAf6Reg_upencnt_enc_oamp2_ro(Location)                                           (0x031300+(Location))
#define cAf6Reg_upencnt_enc_oamp2_ro_WidthVal                                                               32
#define cAf6Reg_upencnt_enc_oamp2_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: cntoice
BitField Type: R2C
BitField Desc: Count
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upencnt_enc_oamp2_ro_cntoice_Bit_Start                                                          0
#define cAf6_upencnt_enc_oamp2_ro_cntoice_Bit_End                                                           31
#define cAf6_upencnt_enc_oamp2_ro_cntoice_Mask                                                        cBit31_0
#define cAf6_upencnt_enc_oamp2_ro_cntoice_Shift                                                              0
#define cAf6_upencnt_enc_oamp2_ro_cntoice_MaxVal                                                    0xffffffff
#define cAf6_upencnt_enc_oamp2_ro_cntoice_MinVal                                                           0x0
#define cAf6_upencnt_enc_oamp2_ro_cntoice_RstVal                                                           0x0


#endif /* _THA60210012PHYSICALEOSENCAPREG_H_ */

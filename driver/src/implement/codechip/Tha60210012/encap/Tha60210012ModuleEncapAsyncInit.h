/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60210012DeviceAsyncInit.h
 * 
 * Created Date: Aug 23, 2016
 *
 * Description : AsyncInit
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012DEVICEASYNCINIT_H_
#define _THA60210012DEVICEASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210012ModuleEncapAsyncInit(AtObject self);
eAtRet Tha60210012ModuleEncapSuperAsyncInit(AtObject self);
eAtRet Tha60210012ModuleEncapActivate(AtObject self);
eAtRet Tha60210012ModuleEncapDebugLoopbackSet(AtObject self);
eAtRet Th60210012ModuleEncapEosInit(AtObject self);


#ifdef __cplusplus
}
#endif
#endif /* _THA60210012DEVICEASYNCINIT_H_ */


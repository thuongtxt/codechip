/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60210012GfpChannel.c
 *
 * Created Date: Apr 13, 2016
 *
 * Description : 60210012 physical GFP channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleConcate.h"
#include "AtConcateGroup.h"
#include "../../../default/encap/gfp/ThaGfpChannelInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pwe/ThaModulePwe.h"
#include "../../../default/pda/ThaModulePda.h"
#include "../pwe/Tha60210012ModulePwe.h"
#include "../pda/Tha60210012ModulePda.h"
#include "../cla/Tha60210012ModuleCla.h"
#include "../mpeg/Tha60210012ModuleMpeg.h"
#include "Tha60210012PhysicalEosEncapReg.h"
#include "Tha60210012ModuleEncap.h"
#include "Tha60210012VcgEncapChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210012VcgGfpChannel*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012VcgGfpChannel
    {
    tThaGfpChannelV2 super;
    }tTha60210012VcgGfpChannel;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods      m_AtChannelOverride;
static tAtGfpChannelMethods   m_AtGfpChannelOverride;
static tAtEncapChannelMethods m_AtEncapChannelOverride;

/* Save super implementation */
static const tAtChannelMethods      *m_AtChannelMethods      = NULL;
static const tAtGfpChannelMethods   *m_AtGfpChannelMethods   = NULL;
static const tAtEncapChannelMethods *m_AtEncapChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePwe ModulePwe(AtEncapChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static ThaModulePda ModulePda(AtEncapChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
    }

static ThaModuleCla ModuleCla(AtEncapChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static AtModuleEncap ModuleEncap(AtChannel self)
	{
	return (AtModuleEncap)AtChannelModuleGet(self);
	}

static uint32 BaseAddress(void)
    {
    return Tha60210012ModuleEncapEosBaseAddress();
    }

static uint32 ChannelOffset(AtEncapChannel self)
    {
    return AtChannelHwIdGet((AtChannel)self);
    }

static eAtModuleEncapRet TxUpiSet(AtGfpChannel self, uint8 txUpi)
    {
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_mapf_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_mapf_pgupigfp_, txUpi);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static uint8 TxUpiGet(AtGfpChannel self)
    {
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_mapf_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    return (uint8)mRegField(regVal, cAf6_upen_mapf_pgupigfp_);
    }

static eAtModuleEncapRet TxFcsEnable(AtGfpChannel self, eBool enable)
	{
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_mapf_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_mapf_pgfcsena_, enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
	}

static eBool TxFcsIsEnabled(AtGfpChannel self)
	{
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_mapf_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    return (eBool)mRegField(regVal, cAf6_upen_mapf_pgfcsena_);
	}

static eAtModuleEncapRet TxCsfEnable(AtGfpChannel self, eBool enable)
    {
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_csfp_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_csfp_encicsfena_, enable);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eBool TxCsfIsEnabled(AtGfpChannel self)
    {
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_csfp_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    return (eBool)mRegField(regVal, cAf6_upen_csfp_encicsfena_);
    }

static eAtModuleEncapRet TxCsfUpiSet(AtGfpChannel self, uint8 txUpi)
    {
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_csfp_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_csfp_encicsfupi_, txUpi);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static uint8 TxCsfUpiGet(AtGfpChannel self)
    {
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_csfp_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    return (uint8)mRegField(regVal, cAf6_upen_csfp_encicsfupi_);
    }

static eAtModuleEncapRet ExpectedUpiSet(AtGfpChannel self, uint8 expectedUpi)
    {
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_decf_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_decf_tb_ctrl_oct_, expectedUpi);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
    return cAtOk;
    }

static uint8 ExpectedUpiGet(AtGfpChannel self)
    {
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_decf_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    return (uint8)mRegField(regVal, cAf6_upen_decf_tb_ctrl_oct_);
    }

static eAtModuleEncapRet UpiMonitorEnable(AtGfpChannel self, eBool enable)
    {
    uint32 regVal[2];
    uint32 regAddr = cAf6Reg_upen_decf_Base + ChannelOffset((AtEncapChannel)self);
    eAtRet ret = Tha60210012ModuleEncapEosLongRead(ModuleEncap((AtChannel) self), regAddr, regVal);
    if (ret != cAtOk)
        return ret;

    mRegFieldSet(regVal[1], cAf6_upen_decf_tb_drop_dismisf_, enable ? 1 : 0);
    return Tha60210012ModuleEncapEosLongWrite(ModuleEncap((AtChannel) self), regAddr, regVal);
    }


static eBool UpiMonitorIsEnabled(AtGfpChannel self)
    {
    uint32 regVal[2];
    uint32 regAddr = cAf6Reg_upen_decf_Base + ChannelOffset((AtEncapChannel)self);
    eAtRet ret = Tha60210012ModuleEncapEosLongRead(ModuleEncap((AtChannel)self), regAddr, regVal);
    if (ret != cAtOk)
        return cAtFalse;

    return (mRegField(regVal[1], cAf6_upen_decf_tb_drop_dismisf_) != 0) ? cAtTrue : cAtFalse;
    }

static eAtModuleEncapRet FcsMonitorEnable(AtGfpChannel self, eBool fcsCheck)
    {
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_decf_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_decf_tb_drop_discrcf_, fcsCheck ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eBool FcsMonitorIsEnabled(AtGfpChannel self)
    {
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_decf_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

	return (mRegField(regVal, cAf6_upen_decf_tb_drop_discrcf_) != 0) ? cAtTrue : cAtFalse;
    }

static eAtModuleEncapRet PayloadScrambleEnable(AtGfpChannel self, eBool enable)
    {
    Tha60210012EncapChannelScrambleEnable((AtEncapChannel)self, enable);
    Tha60210012DecapChannelScrambleEnable((AtEncapChannel)self, enable);
    return cAtOk;
    }

static eBool PayloadScrambleIsEnabled(AtGfpChannel self)
    {
    return Tha60210012EncapChannelScrambleIsEnabled((AtEncapChannel)self);
    }

static eAtModuleEncapRet CoreHeaderScrambleEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    if(enable)
        return cAtOk;
    else
        return cAtErrorModeNotSupport;
    }

static eBool CoreHeaderScrambleIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet Default(AtChannel self)
    {
    eAtRet ret;
	AtGfpChannel gfpChannel = (AtGfpChannel)self;
	uint32 channelId = AtChannelIdGet(self);
	AtModuleEncap encapModule = ModuleEncap(self);

	ret = Tha60210012ModuleEncapChannelRegisterReset(encapModule, channelId);
	ret |= Tha60210012ModuleEncapChannelEncapModeSet(encapModule, channelId, cAtEncapGfp);
	ret |= Tha60210012ModuleEncapChannelThresholdSet(encapModule, channelId, 3);

	ret |= AtGfpChannelTxPtiSet(gfpChannel, 0x0);
	ret |= AtGfpChannelExpectedPtiSet(gfpChannel, 0x0);
	ret |= AtGfpChannelPtiMonitorEnable(gfpChannel, cAtTrue);

	ret |= AtGfpChannelTxUpiSet(gfpChannel, 0x1);
	ret |= AtGfpChannelExpectedUpiSet(gfpChannel, 0x1);
	ret |= AtGfpChannelUpiMonitorEnable(gfpChannel, cAtTrue);

	ret |= AtGfpChannelTxExiSet(gfpChannel, 0x0);
	ret |= AtGfpChannelExpectedExiSet(gfpChannel, 0x0);

	ret |= AtGfpChannelTxPfiSet(gfpChannel, 0x0);
	ret |= AtGfpChannelTxFcsEnable(gfpChannel, cAtTrue);
	ret |= AtGfpChannelFcsMonitorEnable(gfpChannel, cAtTrue);
	ret |= AtGfpChannelFrameModeSet(gfpChannel, cAtGfpFrameModeGfpF);

	ret |= AtGfpChannelPayloadScrambleEnable(gfpChannel, cAtTrue);
	ret |= AtGfpChannelCoreHeaderScrambleEnable(gfpChannel, cAtTrue);

	return ret;
    }

static void EncapChannelScrambleEnable(AtEncapChannel self, eBool enable)
    {
    uint32 regAddr = Tha60210012ModuleEncapEosBaseAddress() + cAf6Reg_upen_mapf_Base + ChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_mapf_HDLCSCR_, enable);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
    }

static eBool EncapChannelScrambleIsEnabled(AtEncapChannel self)
    {
    uint32 regAddr = Tha60210012ModuleEncapEosBaseAddress() + cAf6Reg_upen_mapf_Base + ChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    return mRegField(regVal, cAf6_upen_mapf_HDLCSCR_) != 0 ? cAtTrue : cAtFalse;
    }

static void DecapChannelScrambleEnable(AtEncapChannel self, eBool enable)
    {
    uint32 regAddr = Tha60210012ModuleEncapEosBaseAddress() + cAf6Reg_upen_decf_Base + ChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_decf_tb_scrm_ena_, enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
    }

static eBool DecapChannelScrambleIsEnabled(AtEncapChannel self)
    {
    uint32 regAddr = Tha60210012ModuleEncapEosBaseAddress() + cAf6Reg_upen_decf_Base + ChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    return mRegField(regVal, cAf6_upen_decf_tb_scrm_ena_) != 0 ? cAtTrue : cAtFalse;
    }

static eAtRet UnBindFlow(AtEncapChannel self)
    {
    eAtRet ret = cAtOk;
    AtEthFlow boundFlow = AtEncapChannelBoundFlowGet(self);
    uint32 vcgId = ChannelOffset(self);

    if (boundFlow == NULL)
        return cAtOk;

    ret |= Tha60210012ModuleClaVcgServiceEnable(ModuleCla(self), vcgId, boundFlow, cAtFalse);
    ret |= Tha60210012ModulePdaVcgLookUpControl(ModulePda(self), vcgId, boundFlow, cAtFalse);
    ret |= AtChannelQueueEnable((AtChannel) self, cAtFalse);

    ret |= m_AtEncapChannelMethods->FlowBind(self, NULL);

    return ret;
    }

static eAtRet BindFlow(AtEncapChannel self, AtEthFlow flow)
    {
    uint32 vcgId = ChannelOffset(self);
    eAtRet ret = m_AtEncapChannelMethods->FlowBind(self, flow);

    if (ret != cAtOk)
        return ret;

    ret |= AtChannelQueueEnable((AtChannel) self, cAtTrue);
    ret |= Tha60210012ModulePweEthFlowVcgLookup(ModulePwe(self), vcgId, flow);
    ret |= Tha60210012ModulePdaVcgLookUpControl(ModulePda(self), vcgId, flow, cAtTrue);
    ret |= Tha60210012ModuleClaVcgServiceEnable(ModuleCla(self), vcgId, flow, cAtTrue);

    return ret;
    }

static eAtRet EopEncapChannelFlowBind(AtEncapChannel self, AtEthFlow flow)
    {
    return (flow) ? BindFlow(self, flow) : UnBindFlow(self);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return Default(self);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtGfpChannelCounterTypeTxBytes            : return cAtTrue;
        case cAtGfpChannelCounterTypeRxBytes            : return cAtTrue;
		case cAtGfpChannelCounterTypeTxGoodFrames       : return cAtTrue;
		case cAtGfpChannelCounterTypeTxIdleFrames       : return cAtTrue;
		case cAtGfpChannelCounterTypeRxGoodFrames       : return cAtTrue;
		case cAtGfpChannelCounterTypeRxIdleFrames       : return cAtTrue;
		case cAtGfpChannelCounterTypeRxUpm              : return cAtTrue;
		case cAtGfpChannelCounterTypeRxFcsError         : return cAtTrue;
		case cAtGfpChannelCounterTypeRxCHecCorrected    : return cAtTrue;
		case cAtGfpChannelCounterTypeRxTHecCorrected    : return cAtTrue;
        case cAtGfpChannelCounterTypeTxCsf              : return cAtTrue;
        case cAtGfpChannelCounterTypeRxTHecError        : return cAtTrue;
        case cAtGfpChannelCounterTypeRxDrops            : return cAtTrue;

        default:return cAtFalse;
        }
    }

static uint32 CounterBaseAddress(AtChannel self, uint16 counterType, eBool r2c)
    {
    AtUnused(self);
    AtUnused(r2c);

    switch (counterType)
        {
        case cAtGfpChannelCounterTypeTxBytes            : return r2c ? cAf6Reg_upencnt_enc_clen_r2c_Base    : cAf6Reg_upencnt_enc_clen_ro_Base;
        case cAtGfpChannelCounterTypeRxBytes            : return r2c ? cAf6Reg_upencnt_dec_clen_Base        : cAf6Reg_upencnt_dec_clen_ro_Base;
        case cAtGfpChannelCounterTypeTxGoodFrames       : return r2c ? cAf6Reg_upencnt_enc_even1_r2c_Base   : cAf6Reg_upencnt_enc_even1_ro_Base;
        case cAtGfpChannelCounterTypeTxIdleFrames       : return r2c ? cAf6Reg_upencnt_enc_even2_r2c_Base   : cAf6Reg_upencnt_enc_even2_ro_Base;
        case cAtGfpChannelCounterTypeRxGoodFrames       : return r2c ? cAf6Reg_upencnt_dec_even1_Base       : cAf6Reg_upencnt_dec_even1_ro_Base;
        case cAtGfpChannelCounterTypeRxIdleFrames       : return r2c ? cAf6Reg_upencnt_dec_cline_r2c_Base   : cAf6Reg_upencnt_dec_cline_ro_Base;
        case cAtGfpChannelCounterTypeRxUpm              : return r2c ? cAf6Reg_upencnt_dec_even5_r2c_Base   : cAf6Reg_upencnt_dec_even5_ro_Base;
        case cAtGfpChannelCounterTypeRxFcsError         : return r2c ? cAf6Reg_upencnt_dec_even6_r2c_Base   : cAf6Reg_upencnt_dec_even6_ro_Base;
        case cAtGfpChannelCounterTypeRxCHecCorrected    : return r2c ? cAf6Reg_upencnt_dec_even4_r2c_Base   : cAf6Reg_upencnt_dec_even4_ro_Base;
        case cAtGfpChannelCounterTypeRxTHecCorrected    : return r2c ? cAf6Reg_upencnt_dec_even8_r2c_Base   : cAf6Reg_upencnt_dec_even8_ro_Base;
        case cAtGfpChannelCounterTypeTxCsf              : return r2c ? cAf6Reg_upencnt_enc_oamp1_r2c_Base   : cAf6Reg_upencnt_enc_oamp1_ro_Base;
        case cAtGfpChannelCounterTypeRxTHecError        : return r2c ? cAf6Reg_upencnt_dec_even9_r2c_Base   : cAf6Reg_upencnt_dec_even9_ro_Base;
        case cAtGfpChannelCounterTypeRxDrops            : return r2c ? cAf6Reg_upencnt_dec_even7_r2c_Base   : cAf6Reg_upencnt_dec_even7_ro_Base;
        default:return cBit31_0;
        }
    }

static uint32 HwCounterGet(AtChannel self, uint16 counterType, eBool R2C)
    {
    uint32 regAddr = BaseAddress() + CounterBaseAddress(self, counterType, R2C) + ChannelOffset((AtEncapChannel)self);
    return mChannelHwRead(self, regAddr, cAtModuleEncap);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (AtChannelCounterIsSupported(self, counterType))
        return HwCounterGet(self, counterType, cAtFalse);

    return 0x0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (AtChannelCounterIsSupported(self, counterType))
        return HwCounterGet(self, counterType, cAtTrue);
    return 0x0;
    }

static uint32 HwAlarmHistory(AtChannel self, eBool clear)
    {
    uint32 alarm = 0x0;
    uint32 regAddr = BaseAddress() + cAf6Reg_upchstypen_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);
    if (clear)
        mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    if (regVal & cAf6_upchstypen_RCP_CSF_STICKY_Mask)
        alarm |= cAtGfpChannelAlarmCsf;

    if (regVal & cAf6_upchstypen_FRMSTATE_STICKY_BIT_A_Mask)
        alarm |= cAtGfpChannelAlarmLfd;

    return alarm;
    }

static uint32 AlarmHistoryGet(AtChannel self)
    {
    return HwAlarmHistory(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtChannel self)
    {
    return HwAlarmHistory(self, cAtTrue);
    }

static uint32 AlarmGet(AtChannel self)
    {
    uint32 alarm = 0x0;
    uint32 regAddr = BaseAddress() + cAf6Reg_upchstapen_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);

    if (regVal & cAf6_upchstapen_RCP_CSF_STATE_Mask)
        alarm |= cAtGfpChannelAlarmCsf;

    if (mRegField(regVal, cAf6_upchstapen_FRMSTATE_STATE_) != 2)
        alarm |= cAtGfpChannelAlarmLfd;

    return alarm;
    }

static eAtEncapType EncapTypeGet(AtEncapChannel self)
	{
	AtUnused(self);
	return cAtEncapGfp;
	}

static uint32 HwThresholdVal(eAtConcateMemberType memberType, uint32 numberMember)
    {
    switch ((uint32)memberType)
        {
        case cAtConcateMemberTypeDs3:
        case cAtConcateMemberTypeE3:
            return (numberMember < 7) ? 4 : 5;

        default:
            return 3;
        }
    }

static eAtRet BandwidthUpdate(AtEncapChannel self)
    {
    AtModuleEncap encapModule = ModuleEncap((AtChannel)self);
    AtDevice device = AtModuleDeviceGet((AtModule)encapModule);
    AtModuleConcate concateModule = (AtModuleConcate)AtDeviceModuleGet(device, cAtModuleConcate);
    uint32 channelId = AtChannelIdGet((AtChannel)self);
    AtConcateGroup group = AtModuleConcateGroupGet(concateModule, channelId);
    eAtConcateMemberType memberType = AtConcateGroupMemberTypeGet(group);
    uint32 numberMember = AtConcateGroupNumSourceMembersGet(group);

    return Tha60210012ModuleEncapChannelThresholdSet(encapModule, channelId, HwThresholdVal(memberType, numberMember));
    }

static eAtModuleEncapRet FlowBind(AtEncapChannel self, AtEthFlow flow)
	{
    eAtRet ret = cAtOk;
    if (flow)
        ret |= Tha60210012EncapChannelBandwidthUpdate(self);

    ret |= Tha60210012EopEncapChannelFlowBind(self, flow);
    return ret;
	}

static const char* State2String(uint32 state)
    {
    switch(state)
        {
        case 0:     return "hunt";
        case 1:     return "pre-sync";
        case 2:     return "sync";
        default:    return "unknown";
        }
    }

static uint32 State2Color(uint32 state)
    {
    switch(state)
        {
        case 0:     return cSevCritical;
        case 1:     return cSevWarning;
        case 2:     return cSevInfo;
        default:    return cSevCritical;
        }
    }

static eAtRet Debug(AtChannel self)
    {
    uint32 regAddr = BaseAddress() + cAf6Reg_upchstapen_Base + ChannelOffset((AtEncapChannel)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);
    uint32 currentState = mRegField(regVal, cAf6_upchstapen_FRMSTATE_STATE_);

    AtPrintc(cSevNormal, "\r\n - Current state:");
    AtPrintc(State2Color(currentState), "    %s \r\n", State2String(currentState));

    AtPrintc(cSevNormal, "\r\n - Register Configure:");
    Tha60210012ModuleEncapEosChannelRegisterShow(ModuleEncap(self), AtChannelIdGet(self));

    AtPrintc(cSevNormal, "\r\n -------------------------------------------- \r\n ");
    regAddr = BaseAddress() + cAf6Reg_upencnt_dec_even5_r2c_Base + ChannelOffset((AtEncapChannel)self);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);
    AtPrintc((regVal != 0) ? cSevCritical : cSevNormal, "\r\n - Buffer Empty Counters: %u \r\n", regVal);
    AtPrintc(cSevNormal, "\r\n -------------------------------------------- \r\n ");

    return cAtOk;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return enable;
    }

static eBool FrameModeIsSupported(AtGfpChannel self, eAtGfpFrameMode frameMappingMode)
    {
    AtUnused(self);
    if (frameMappingMode == cAtGfpFrameModeGfpF)
        return cAtTrue;
    return cAtFalse;
    }

static eAtModuleEncapRet FrameModeSet(AtGfpChannel self, eAtGfpFrameMode frameMappingMode)
    {
    AtUnused(self);
    return (frameMappingMode == cAtGfpFrameModeGfpF) ? cAtOk: cAtErrorModeNotSupport;
    }

static eAtGfpFrameMode FrameModeGet(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtGfpFrameModeGfpF;
    }

static Tha60210012ModuleMpeg ModuleMpeg(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    return (Tha60210012ModuleMpeg)AtDeviceModuleGet(device, cThaModuleMpeg);
    }

static eAtRet EopEncapChannelQueueEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    uint32 vcgId = ChannelOffset((AtEncapChannel) self);
    Tha60210012ModuleMpeg moduleMpeg = ModuleMpeg(self);

    if (enable)
        {
        ret |= Tha60210012ModuleMpegVcgQueueEnable(moduleMpeg, vcgId, cAtFalse);
        ret |= Tha60210012ModuleMpegQueueVcgPrioritySet(moduleMpeg, vcgId, cThaMpegQueuePriorityLowest);
        ret |= Tha60210012ModuleMpegVcgQueueEnable(moduleMpeg, vcgId, cAtTrue);
        }
    else
        {
        ret |= Tha60210012ModuleMpegVcgQueueEnable(moduleMpeg, vcgId, cAtFalse);
        Tha60210012ModuleMpegQueueWaitBufferIsEmpty(moduleMpeg);
        ret |= Tha60210012ModuleMpegQueueVcgPrioritySet(moduleMpeg, vcgId, cThaMpegQueuePriorityUnknown);
        }

    return ret;
    }

static eAtRet QueueEnable(AtChannel self, eBool enable)
    {
    return Tha60210012EopEncapChannelQueueEnable(self, enable);
    }

static void OverrideAtEncapChannel(AtGfpChannel self)
    {
	AtEncapChannel channel = (AtEncapChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEncapChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapChannelOverride, m_AtEncapChannelMethods, sizeof(tAtEncapChannelMethods));

        mMethodOverride(m_AtEncapChannelOverride, EncapTypeGet);
        mMethodOverride(m_AtEncapChannelOverride, FlowBind);
        }

    mMethodsSet(channel, &m_AtEncapChannelOverride);
    }

static void OverrideAtChannel(AtGfpChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryClear);
        mMethodOverride(m_AtChannelOverride, AlarmGet);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, CanEnable);
        mMethodOverride(m_AtChannelOverride, QueueEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtGfpChannel(AtGfpChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtGfpChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtGfpChannelOverride, m_AtGfpChannelMethods, sizeof(m_AtGfpChannelOverride));

        mMethodOverride(m_AtGfpChannelOverride, TxUpiSet);
        mMethodOverride(m_AtGfpChannelOverride, TxUpiGet);
        mMethodOverride(m_AtGfpChannelOverride, TxCsfEnable);
        mMethodOverride(m_AtGfpChannelOverride, TxCsfIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, TxCsfUpiSet);
        mMethodOverride(m_AtGfpChannelOverride, TxCsfUpiGet);
        mMethodOverride(m_AtGfpChannelOverride, TxFcsEnable);
        mMethodOverride(m_AtGfpChannelOverride, TxFcsIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, FrameModeSet);
        mMethodOverride(m_AtGfpChannelOverride, FrameModeGet);
        mMethodOverride(m_AtGfpChannelOverride, FrameModeIsSupported);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedUpiSet);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedUpiGet);
        mMethodOverride(m_AtGfpChannelOverride, UpiMonitorEnable);
        mMethodOverride(m_AtGfpChannelOverride, UpiMonitorIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, FcsMonitorEnable);
        mMethodOverride(m_AtGfpChannelOverride, FcsMonitorIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, PayloadScrambleEnable);
        mMethodOverride(m_AtGfpChannelOverride, PayloadScrambleIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, CoreHeaderScrambleEnable);
        mMethodOverride(m_AtGfpChannelOverride, CoreHeaderScrambleIsEnabled);
        }

    mMethodsSet(self, &m_AtGfpChannelOverride);
    }

static void Override(AtGfpChannel self)
    {
	OverrideAtEncapChannel(self);
    OverrideAtGfpChannel(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012VcgGfpChannel);
    }

static AtGfpChannel ObjectInit(AtGfpChannel self, uint32 channelId, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaGfpChannelV2ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtGfpChannel Tha60210012VcgGfpChannelNew(uint32 channelId, AtModuleEncap module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtGfpChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChannel, channelId, module);
    }

void Tha60210012EncapChannelScrambleEnable(AtEncapChannel self, eBool enable)
    {
    if(self)
        EncapChannelScrambleEnable(self, enable);
    }

eBool Tha60210012EncapChannelScrambleIsEnabled(AtEncapChannel self)
    {
    if(self)
        return EncapChannelScrambleIsEnabled(self);

    return cAtFalse;
    }

void Tha60210012DecapChannelScrambleEnable(AtEncapChannel self, eBool enable)
    {
    if(self)
        DecapChannelScrambleEnable(self, enable);
    }

eBool Tha60210012DecapChannelScrambleIsEnabled(AtEncapChannel self)
    {
    if(self)
        return DecapChannelScrambleIsEnabled(self);

    return cAtFalse;
    }

eAtRet Tha60210012EopEncapChannelFlowBind(AtEncapChannel self, AtEthFlow flow)
    {
    if(self)
        return EopEncapChannelFlowBind(self, flow);

    return cAtErrorNotExist;
    }

eAtRet Tha60210012EopEncapChannelQueueEnable(AtChannel self, eBool enable)
    {
    if (self == NULL)
        return cAtErrorNotExist;

    return EopEncapChannelQueueEnable(self, enable);
    }

eAtRet Tha60210012EncapChannelBandwidthUpdate(AtEncapChannel self)
	{
	if (self)
		return BandwidthUpdate(self);
	return cAtErrorNotExist;
	}

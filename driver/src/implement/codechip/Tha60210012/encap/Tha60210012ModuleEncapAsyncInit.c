/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Tha60210012DeviceAsyncInit.c
 *
 * Created Date: Aug 23, 2016
 *
 * Description : Asyncinit
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/encap/dynamic/ThaModuleDynamicEncapInternal.h"
#include "../../../default/encap/dynamic/ThaPhysicalHdlcChannel.h"
#include "../../../default/encap/dynamic/ThaEncapDynamicBinder.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/man/ThaDevice.h"
#include "../pw/activator/Tha60210012PwActivator.h"
#include "../pw/Tha60210012ModulePw.h"
#include "../man/Tha60210012Device.h"
#include "../eth/Tha60210012ModuleEthFlowLookupReg.h"
#include "../ppp/Tha60210012MpBundle.h"
#include "Tha60210012PhysicalHdlcChannel.h"
#include "Tha60210012ModuleDecapParserReg.h"
#include "Tha60210012PhysicalEosEncapReg.h"
#include "Tha60210012PhysicalEncapReg.h"
#include "Tha60210012VcgEncapChannel.h"
#include "Tha60210012ModuleEncapInternal.h"
#include "Tha60210012ModuleEncapAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef eAtRet (*tAtAsycnOperationFunc)(AtObject self);


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtAsycnOperationFunc functions[] = {Tha60210012ModuleEncapSuperAsyncInit, Tha60210012ModuleEncapActivate,
                                            Tha60210012ModuleEncapDebugLoopbackSet, Th60210012ModuleEncapEosInit};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 AsyncMaxState(AtObject self)
    {
    AtUnused(self);
    return mCount(functions);
    }

static uint32 AsyncStateGet(AtObject self)
    {
    return ((Tha60210012ModuleEncap)self)->asyncInitState;
    }

static void AsyncStateIncrease(AtObject self)
    {
    ((Tha60210012ModuleEncap)self)->asyncInitState += 1;
    }

static void AsyncStateReset(AtObject self)
    {
    ((Tha60210012ModuleEncap)self)->asyncInitState = 0;
    }

static tAtAsycnOperationFunc AsyncOperationGet(AtObject self, uint32 state)
    {
    if (state >= AsyncMaxState(self))
        return NULL;

    return functions[state];
    }

static eAtRet AsyncInitMain(AtObject self)
    {
    uint32 state = AsyncStateGet(self);
    tAtAsycnOperationFunc func = AsyncOperationGet(self, state);
    eAtRet ret = cAtOk;
    tAtOsalCurTime profileTime;

    char stateString[32];
    AtSprintf(stateString, "Async state: %d", state);
    AtOsalCurTimeGet(&profileTime);

    if (func == NULL)
        {
        AsyncStateReset(self);
        return cAtErrorNullPointer;
        }

    ret = func(self);
    AtDeviceAccessTimeInMsSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, &profileTime, __FILE__, __LINE__, stateString);
    AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, __FILE__, __LINE__, stateString);
    if (ret == cAtOk)
        {
        AsyncStateIncrease(self);
        ret = cAtErrorAgain;
        }

    else if (!AtDeviceAsyncRetValIsInState(ret))
        {
        AsyncStateReset(self);
        return ret;
        }

    if (AsyncStateGet(self) == AsyncMaxState(self))
        {
        ret = cAtOk;
        AsyncStateReset(self);
        }

    return ret;
    }

eAtRet Tha60210012ModuleEncapAsyncInit(AtObject self)
    {
    return AsyncInitMain(self);
    }

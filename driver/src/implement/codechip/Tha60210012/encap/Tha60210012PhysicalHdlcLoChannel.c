/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60210012PhysicalHdlcLoChannel.c
 *
 * Created Date: Apr 7, 2016
 *
 * Description : 60210012 physical HDLC (Low path) channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../eth/Tha60210012ModuleEth.h"
#include "../eth/Tha60210012ModuleEthFlowLookupReg.h"
#include "Tha60210012ModuleEncapInternal.h"
#include "Tha60210012PhysicalHdlcChannel.h"
#include "Tha60210012PhysicalHdlcChannelInternal.h"
#include "Tha60210012ModuleDecapParserReg.h"
#include "Tha60210012PhysicalEncapReg.h"
#include "Tha60210012PmcInternalReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mDefaultOffset(self) Tha60210012PhysicalHdlcChannelDefaultOffset((Tha60210012PhysicalHdlcChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods              m_AtChannelOverride;
static tAtHdlcChannelMethods          m_AtHdlcChannelOverride;
static tAtEncapChannelMethods         m_AtEncapChannelOverride;
static tTha60210012PhysicalHdlcChannelMethods m_Tha60210012PhysicalHdlcChannelOverride;

/* Save super implementation */
static const tAtChannelMethods      *m_AtChannelMethods = NULL;
static const tAtHdlcChannelMethods  *m_AtHdlcChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210012ModuleEncap ModuleEncap(Tha60210012PhysicalHdlcChannel physicalChannel)
    {
    return (Tha60210012ModuleEncap)AtChannelModuleGet((AtChannel)physicalChannel);
    }

static uint32 LinkLookupOffset(Tha60210012PhysicalHdlcChannel physicalChannel)
    {
    uint32 baseAddress = cThaModuleEthFlowLookupBaseAddress;
    uint8 sliceId = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)physicalChannel);
    uint32 offset = (Tha60210012ModuleEncapRegistersNeedShiftUp(ModuleEncap(physicalChannel))) ? 2048 : 1024;
    return (baseAddress + AtChannelHwIdGet((AtChannel)physicalChannel) + (uint32)(sliceId * offset));
    }

static uint32 SliceOffset(Tha60210012PhysicalHdlcChannel self)
    {
    uint32 slice = (uint32)ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)self);

    return (uint32)(0x4000 * slice);
    }

static eAtRet EncapTypeSet(Tha60210012PhysicalHdlcChannel self, uint8 encapType)
    {
    uint32 regAddress = Tha60210012PhysicalHdlcChannelDecapBaseAddress(self) +
                        cAf6Reg_upen_cidcfg_locfg_Base + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_upen_cidlocfg_enc_typ_, encapType);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);
    return cAtOk;
    }

static uint32 EncapBaseAddress(Tha60210012PhysicalHdlcChannel self)
    {
    AtUnused(self);
    return Tha60210012ModuleEncapLoBaseAddress();
    }

static uint32 DecapBaseAddress(Tha60210012PhysicalHdlcChannel self)
    {
    AtUnused(self);
    return Tha60210012ModuleDecapLoBaseAddress();
    }

static eAtRet LinkLookupSet(Tha60210012PhysicalHdlcChannel self, AtHdlcLink physicalLink)
    {
    uint32 regAddress = cAf6Reg_lolink_lup_table_Base + LinkLookupOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_lolink_lup_table_HDLC_Valid_, 0);/* Disable lookup */
    mRegFieldSet(regValue, cAf6_lolink_lup_table_HDLC_LinkID_, AtChannelHwIdGet((AtChannel)physicalLink));
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

static eAtRet LinkLookupEnable(Tha60210012PhysicalHdlcChannel self, eBool enable)
    {
    uint32 regAddress = cAf6Reg_lolink_lup_table_Base + LinkLookupOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_lolink_lup_table_HDLC_Valid_, mBoolToBin(enable));/* Enable lookup */
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

static eAtRet LinkLookupReset(Tha60210012PhysicalHdlcChannel self)
    {
    uint32 regAddress = cAf6Reg_lolink_lup_table_Base + LinkLookupOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_lolink_lup_table_HDLC_Valid_, 0);/* Disable lookup */
    mRegFieldSet(regValue, cAf6_lolink_lup_table_HDLC_LinkID_, 0xFFF); /* Reset ID */
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

static void DebugPrintErrorBit(const char* title, uint32 hwValue, uint32 mask, const char *raiseString, const char *clearString)
    {
    AtPrintc(cSevNormal, "    %-40s: ", title);
    if (hwValue & mask)
        AtPrintc(cSevCritical, "%-10s\r\n", raiseString);
    else
        AtPrintc(cSevInfo, "%-10s\r\n", clearString);
    }

static void FcsStatePrint(uint32 regValue)
    {
    uint8 hwFcsState = (uint8)(regValue & cAf6_upen_sticky0_fcs_state_check_Mask);

    AtPrintc(cSevNormal, "    %-40s: ", "FCS State:");
    if (hwFcsState == 0)
        AtPrintc(cSevCritical, "%-10s\r\n", "Disable");
    else if (hwFcsState == 0xF)
        AtPrintc(cSevInfo, "%-10s\r\n", "FCS32");
    else if (hwFcsState == 0xC)
        AtPrintc(cSevInfo, "%-10s\r\n", "FCS16");
    else
        AtPrintc(cSevCritical, "%-10s\r\n", "FCS-Error");
    }

static void Sticky0Get(AtChannel phyHdlcChannel)
    {
    uint32 regAddr, regValue;
    AtModule module = AtChannelModuleGet((AtChannel)phyHdlcChannel);
    uint32 sliceOffset = SliceOffset((Tha60210012PhysicalHdlcChannel)phyHdlcChannel);

    /* select channel that need to get sticky */
    regAddr  = cAf6Reg_pencfg_lshift8_Base + Tha60210012ModuleEncapLoBaseAddress() + sliceOffset;
    regValue = mModuleHwRead(module, regAddr);
    mRegFieldSet(regValue, cAf6_pencfg_lshift8_pwchid_db_, AtChannelHwIdGet(phyHdlcChannel));
    mModuleHwWrite(module, regAddr, regValue);

    /* Get sticky */
    regAddr = cAf6Reg_upen_sticky0_Base + Tha60210012ModuleEncapLoBaseAddress() + sliceOffset;
    regValue = mModuleHwRead(module, regAddr);
    AtPrintc(cSevInfo, "\r\n* Lo Encap debug sticky: : (0x%08x = 0x%08x)\r\n", regAddr, regValue);
    DebugPrintErrorBit("Map Request :", regValue, cAf6_upen_sticky0_map_req_Mask, "Set", "Clear");
    DebugPrintErrorBit("Encap Bypass :", regValue, cAf6_upen_sticky0_enc_bypass_Mask, "Bypass", "Normal");
    DebugPrintErrorBit("Good Valid To Map :", regValue, cAf6_upen_sticky0_map_ovld_Mask, "Set", "Clear");
    DebugPrintErrorBit("Idle Good same PMC :", regValue, cAf6_upen_sticky0_idle_good_same_pmc_Mask, "Same", "Not-Same");
    DebugPrintErrorBit("PDA Request :", regValue, cAf6_upen_sticky0_pda_req_Mask, "Set", "Clear");
    DebugPrintErrorBit("MAP Request other ID :", regValue, cAf6_upen_sticky0_map_req_other_id_Mask, "Set", "Clear");
    DebugPrintErrorBit("Other Id Valid From PDA :", regValue, cAf6_upen_sticky0_pda_vld_other_id_Mask, "Set", "Clear");
    DebugPrintErrorBit("PDA Buffer Not Empty:", regValue, cAf6_upen_sticky0_pda_emp_Mask, "Set", "N/A");
    DebugPrintErrorBit("PDA Buffer Empty:", regValue, cAf6_upen_sticky0_pda_nemp_Mask, "Set", "N/A");
    FcsStatePrint(regValue);
    DebugPrintErrorBit("Process Data ENC:", regValue, cAf6_upen_sticky0_trans_state_check_Mask, "Good", "N/A");
    DebugPrintErrorBit("Map Request Error:", regValue, cAf6_upen_sticky0_map_req_err_Mask, "Set", "Clear");

    mModuleHwWrite(module, regAddr, regValue);
    }

static eAtRet Debug(AtChannel self)
    {
    uint32 address = cAf6Reg_lolink_lup_table_Base + LinkLookupOffset((Tha60210012PhysicalHdlcChannel)self);

    m_AtChannelMethods->Debug(self);

    ThaDeviceRegNameDisplay("Lo-order HDLC Link Lookup");
    ThaDeviceChannelRegValueDisplay(self, address, cAtModuleEncap);

    Sticky0Get(self);
    return cAtOk;
    }

static uint32 EncapControlAddress(Tha60210012PhysicalHdlcChannel self)
    {
    return Tha60210012PhysicalHdlcChannelEncapBaseAddress(self) + cAf6Reg_upenctr1_Lo_Base;
    }

static eAtRet ScrambleEnable(AtEncapChannel self, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 baseAddr;

    /* Decap configuration */
    baseAddr = Tha60210012PhysicalHdlcChannelDecapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + cAf6Reg_upen_hdlc_locfg_Base;
    regAddr = baseAddr + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cAf6_upen_hdlc_locfg_cfg_scren_Mask, cAf6_upen_hdlc_locfg_cfg_scren_Shift, enable);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    /* Encap configuration */
    regAddr = EncapControlAddress((Tha60210012PhysicalHdlcChannel)self) + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mRegFieldSet(regVal, cAf6_upenctr1_scrable_en_, enable);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eBool ScrambleIsEnabled(AtEncapChannel self)
    {
    uint8 hwEnable;
    uint32 baseAddr = Tha60210012PhysicalHdlcChannelDecapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + cAf6Reg_upen_hdlc_locfg_Base;
    uint32 regAddr = baseAddr + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mFieldGet(regVal, cAf6_upen_hdlc_locfg_cfg_scren_Mask, cAf6_upen_hdlc_locfg_cfg_scren_Shift, uint8, &hwEnable);
    return hwEnable ? cAtTrue : cAtFalse;
    }

static eAtRet FcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    uint32 baseAddr, regAddr, regVal;

    if ((fcsMode != cAtHdlcFcsModeNoFcs) &&
        (fcsMode != cAtHdlcFcsModeFcs32) &&
        (fcsMode != cAtHdlcFcsModeFcs16))
        return cAtErrorModeNotSupport;

    /* Decap configuration */
    baseAddr = Tha60210012PhysicalHdlcChannelDecapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + cAf6Reg_upen_hdlc_locfg_Base;
    regAddr = baseAddr + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cAf6_upen_hdlc_locfg_cfg_fcsmode_Mask, cAf6_upen_hdlc_locfg_cfg_fcsmode_Shift, (fcsMode == cAtHdlcFcsModeFcs32) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return m_AtHdlcChannelMethods->FcsModeSet(self, fcsMode);
    }

static eAtHdlcFcsMode FcsModeGet(AtHdlcChannel self)
    {
    uint8 hwFcsMode;
    uint32 regAddr, regVal, baseAddr;

    baseAddr = Tha60210012PhysicalHdlcChannelDecapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + cAf6Reg_upen_hdlc_locfg_Base;
    regAddr = baseAddr + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    hwFcsMode = (uint8)mRegField(regVal, cAf6_upen_hdlc_locfg_cfg_fcsmode_);

    return (hwFcsMode == 1) ? cAtHdlcFcsModeFcs32 : cAtHdlcFcsModeFcs16;
    }

static eAtRet StuffModeSet(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    uint32 baseAddr, regAddr, regVal;

    if ((stuffMode != cAtHdlcStuffBit) &&
        (stuffMode != cAtHdlcStuffByte))
        return cAtErrorModeNotSupport;

    /* Decap configuration */
    baseAddr = Tha60210012PhysicalHdlcChannelDecapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + cAf6Reg_upen_hdlc_locfg_Base;
    regAddr = baseAddr + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_hdlc_locfg_cfg_bitstuff_, (stuffMode == cAtHdlcStuffBit) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    regAddr = EncapControlAddress((Tha60210012PhysicalHdlcChannel)self) + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    /* As hardware recommend default always run with calculate LSB mode */
    mRegFieldSet(regVal, cAf6_upenctr1_cfg_lsbfirst_, 1);

    mRegFieldSet(regVal, cAf6_upenctr1_bit_stuff_, (stuffMode == cAtHdlcStuffBit) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static uint32 PacketCounterOffset(Tha60210012PhysicalHdlcChannel self, eBool r2c)
    {
    uint8 slice = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)self);

    return ((uint32)(r2c * 8192) + (uint32)(slice * 1024) + AtChannelHwIdGet((AtChannel)self));
    }

static uint32 CounterBaseAddress(Tha60210012PhysicalHdlcChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return Tha60210012ModuleEthPMCInternalBaseAddress(ethModule);
    }

static uint32 TxCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 type)
    {
    uint32 offset = PacketCounterOffset(self, r2c) + (cTotalCounter * 4096) + CounterBaseAddress(self);
    uint32 regAddress = (type == cPktCounter) ? cAf6Reg_loencpktcnt_pen_Base : cAf6Reg_loencbytecnt_pen_Base;
    return mChannelHwRead(self, regAddress + offset, cAtModuleEncap);
    }

static uint32 RxCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 type)
    {
    uint32 offset     = PacketCounterOffset(self, r2c) + (cTotalCounter * 4096) + CounterBaseAddress(self);
    uint32 regAddress = (type == cPktCounter) ? cAf6Reg_lodecpktcnt_pen_Base : cAf6Reg_lodecbytecnt_pen_Base;
    return mChannelHwRead(self, regAddress + offset, cAtModuleEncap);
    }

static uint32 LinkCounterOffset(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset)
    {
    uint8 slice = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)self);
    return ((uint32)(r2c * 4096) + (uint32)(counterOffset * 8192) + (uint32)(slice * 1024) + AtChannelHwIdGet((AtChannel)self));
    }

static uint32 TxLinkCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset)
    {
    uint32 offset = LinkCounterOffset(self, r2c, counterOffset) + CounterBaseAddress(self);
    return mChannelHwRead(self, cAf6Reg_loencpkttypcnt_pen_Base + offset, cAtModuleEncap);
    }

static uint32 RxLinkCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset)
    {
    uint32 offset = LinkCounterOffset(self, r2c, counterOffset) + CounterBaseAddress(self);
    return mChannelHwRead(self, cAf6Reg_lodecpkttypcnt_pen_Base + offset, cAtModuleEncap);
    }

static uint32 ByteIdleCounterOffset(Tha60210012PhysicalHdlcChannel self, eBool r2c)
    {
    uint8 slice = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)self);

    return ((uint32)(r2c * 4096) + (uint32)(slice * 1024) + AtChannelHwIdGet((AtChannel)self));
    }

static uint32 TxByteIdleCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c)
    {
    uint32 offset = ByteIdleCounterOffset(self, r2c) + CounterBaseAddress(self);
    return mChannelHwRead(self, cAf6Reg_loencidlebytecnt_pen_Base + offset, cAtModuleEncap);
    }

static uint32 RxByteIdleCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c)
    {
    uint32 offset = ByteIdleCounterOffset(self, r2c) + CounterBaseAddress(self);
    return mChannelHwRead(self, cAf6Reg_lodecidlebytecnt_pen_Base + offset, cAtModuleEncap);
    }

static eAtRet FrameTypeSet(AtHdlcChannel self, uint8 frameType)
    {
    Tha60210012PhysicalHdlcChannel hdlcChannel = (Tha60210012PhysicalHdlcChannel)self;
    uint32 regAddress = Tha60210012PhysicalHdlcChannelDecapBaseAddress(hdlcChannel) +
                        cAf6Reg_upen_cidcfg_locfg_Base + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_upen_cidlocfg_frm_typ_, Tha60210012PhysicalHdlcChannelSw2HwFrameType(frameType));
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);
    return cAtOk;
    }

static uint32 TxGoodCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 type)
    {
    uint32 offset = PacketCounterOffset(self, r2c) + CounterBaseAddress(self);
    uint32 regAddress = (type == cPktCounter) ? cAf6Reg_loencpktcnt_pen_Base : cAf6Reg_loencbytecnt_pen_Base;
    return mChannelHwRead(self, regAddress + offset, cAtModuleEncap);
    }

static uint32 RxGoodCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 type)
    {
    uint32 offset     = PacketCounterOffset(self, r2c) + CounterBaseAddress(self);
    uint32 regAddress = (type == cPktCounter) ? cAf6Reg_lodecpktcnt_pen_Base : cAf6Reg_lodecbytecnt_pen_Base;
    return mChannelHwRead(self, regAddress + offset, cAtModuleEncap);
    }

static eAtModuleEncapRet IdlePatternSet(AtHdlcChannel self, uint8 idlePattern)
    {
    uint32 offset, regAddress, regValue;
    uint8 hwMode = Tha60210012PhysicalHdlcChannelIdlePattern2HwMode(idlePattern);

    if (hwMode == cBit7_0)
        return cAtErrorModeNotSupport;

    offset = Tha60210012PhysicalHdlcChannelEncapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + mDefaultOffset(self);
    regAddress = cAf6Reg_upenctr1_Lo_Base + offset;
    regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_upenctr1_idle_mode_, hwMode);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

static uint8 IdlePatternGet(AtHdlcChannel self)
    {
    uint32 offset = Tha60210012PhysicalHdlcChannelEncapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + mDefaultOffset(self);
    uint32 regAddress = cAf6Reg_upenctr1_Lo_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    return (mRegField(regValue, cAf6_upenctr1_idle_mode_) == 1) ? 0xFF : 0x7E;
    }

static eAtRet RxAddressControlCompress(AtHdlcChannel self, eBool rxAddrCtrlCompress)
    {
    uint32 offset = mDefaultOffset(self) + Tha60210012ModuleDecapLoBaseAddress();
    uint32 regAddress = cAf6Reg_upen_cidcfg_locfg_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_upen_cidlocfg_addcfg_, mBoolToBin(rxAddrCtrlCompress));
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

static eBool RxAddressControlIsCompressed(AtHdlcChannel self)
    {
    uint32 offset = mDefaultOffset(self) + Tha60210012ModuleDecapLoBaseAddress();
    uint32 regAddress = cAf6Reg_upen_cidcfg_locfg_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    return mRegField(regValue, cAf6_upen_cidlocfg_addcfg_) ? cAtTrue : cAtFalse;
    }

static eBool FcsCalculationModeIsSupported(eAtHdlcFcsCalculationMode mode)
    {
    if ((mode == cAtHdlcFcsCalculationModeLsb) ||
        (mode == cAtHdlcFcsCalculationModeMsb))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet FcsCalculationModeSet(AtHdlcChannel self, eAtHdlcFcsCalculationMode mode)
    {
    uint32 baseAddr, regAddr, regVal;

    if (!FcsCalculationModeIsSupported(mode))
        return cAtErrorModeNotSupport;

    /* Decap configuration */
    baseAddr = Tha60210012PhysicalHdlcChannelDecapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + cAf6Reg_upen_hdlc_locfg_Base;
    regAddr = baseAddr + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mRegFieldSet(regVal, cAf6_upen_hdlc_locfg_cfg_fcsmsb_, (mode == cAtHdlcFcsCalculationModeMsb) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eAtHdlcFcsCalculationMode FcsCalculationModeGet(AtHdlcChannel self)
    {
    uint32 baseAddr = Tha60210012PhysicalHdlcChannelDecapBaseAddress((Tha60210012PhysicalHdlcChannel)self) + cAf6Reg_upen_hdlc_locfg_Base;
    uint32 regAddr = baseAddr + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    uint8 hwFcsMode = (uint8)mRegField(regVal, cAf6_upen_hdlc_locfg_cfg_fcsmsb_);

    return (hwFcsMode == 1) ? cAtHdlcFcsCalculationModeMsb : cAtHdlcFcsCalculationModeLsb;
    }

static void OverrideAtChannel(AtHdlcChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, m_AtHdlcChannelMethods, sizeof(m_AtHdlcChannelOverride));

        mMethodOverride(m_AtHdlcChannelOverride, FcsModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, IdlePatternSet);
        mMethodOverride(m_AtHdlcChannelOverride, IdlePatternGet);
        mMethodOverride(m_AtHdlcChannelOverride, RxAddressControlCompress);
        mMethodOverride(m_AtHdlcChannelOverride, RxAddressControlIsCompressed);
        mMethodOverride(m_AtHdlcChannelOverride, FcsCalculationModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsCalculationModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, FrameTypeSet);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void OverrideAtEncapChannel(AtHdlcChannel self)
    {
    AtEncapChannel encap = (AtEncapChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapChannelOverride, mMethodsGet(encap), sizeof(m_AtEncapChannelOverride));

        mMethodOverride(m_AtEncapChannelOverride, ScrambleIsEnabled);
        mMethodOverride(m_AtEncapChannelOverride, ScrambleEnable);
        }

    mMethodsSet(encap, &m_AtEncapChannelOverride);
    }

static void OverrideTha60210012PhysicalHdlcChannel(AtHdlcChannel self)
    {
    Tha60210012PhysicalHdlcChannel channel = (Tha60210012PhysicalHdlcChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012PhysicalHdlcChannelOverride, mMethodsGet(channel), sizeof(m_Tha60210012PhysicalHdlcChannelOverride));

        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, EncapTypeSet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, EncapBaseAddress);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, DecapBaseAddress);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, LinkLookupSet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, LinkLookupEnable);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, LinkLookupReset);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, EncapControlAddress);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, SliceOffset);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, TxLinkCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, RxLinkCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, TxCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, RxCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, TxByteIdleCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, RxByteIdleCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, TxGoodCounterGet);
        mMethodOverride(m_Tha60210012PhysicalHdlcChannelOverride, RxGoodCounterGet);
        }

    mMethodsSet(channel, &m_Tha60210012PhysicalHdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtChannel(self);
    OverrideAtHdlcChannel(self);
    OverrideAtEncapChannel(self);
    OverrideTha60210012PhysicalHdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012PhysicalHdlcLoChannel);
    }

AtHdlcChannel Tha60210012PhysicalHdlcLoChannelObjectInit(AtHdlcChannel self, uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012PhysicalHdlcChannelObjectInit(self, channelId, logicalChannel, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel Tha60210012PhysicalHdlcLoChannelNew(uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012PhysicalHdlcLoChannelObjectInit(newChannel, channelId, logicalChannel, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60210012PhysicalHdlcChannelInternal.h
 *
 * Created Date: Jul 28, 2016 
 *
 * Description : 60210012 physical HDLC channel internal definition
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PHYSICALHDLCCHANNELINTERNAL_H_
#define _THA60210012PHYSICALHDLCCHANNELINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/encap/dynamic/ThaPhysicalHdlcChannelInternal.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "Tha60210012PhysicalHdlcChannel.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/
#define cTotalCounter       (1)
#define cGoodCounter        (0)
#define cPktCounter         (0)
#define cByteCounter        (1)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012PhysicalHdlcChannelMethods
    {
    eAtRet (*EncapTypeSet)(Tha60210012PhysicalHdlcChannel self, uint8 encapType);
    uint32 (*EncapBaseAddress)(Tha60210012PhysicalHdlcChannel self);
    uint32 (*DecapBaseAddress)(Tha60210012PhysicalHdlcChannel self);
    eAtRet (*LinkLookupSet)(Tha60210012PhysicalHdlcChannel self, AtHdlcLink physicalLink);
    eAtRet (*LinkLookupEnable)(Tha60210012PhysicalHdlcChannel self, eBool enable);
    eAtRet (*LinkLookupReset)(Tha60210012PhysicalHdlcChannel self);
    uint32 (*EncapControlAddress)(Tha60210012PhysicalHdlcChannel self);
    uint32 (*SliceOffset)(Tha60210012PhysicalHdlcChannel self);
    uint32 (*TxLinkCounterGet)(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset);
    uint32 (*RxLinkCounterGet)(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset);
    uint32 (*TxCounterGet)(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset);
    uint32 (*RxCounterGet)(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset);
    uint32 (*TxByteIdleCounterGet)(Tha60210012PhysicalHdlcChannel self, eBool r2c);
    uint32 (*RxByteIdleCounterGet)(Tha60210012PhysicalHdlcChannel self, eBool r2c);
    uint32 (*TxGoodCounterGet)(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset);
    uint32 (*RxGoodCounterGet)(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset);
    }tTha60210012PhysicalHdlcChannelMethods;

typedef struct tTha60210012PhysicalHdlcChannel
    {
    tThaPhysicalHdlcChannel super;
    const tTha60210012PhysicalHdlcChannelMethods * methods;

    }tTha60210012PhysicalHdlcChannel;

typedef struct tTha60210012PhysicalHdlcHoChannel
    {
    tTha60210012PhysicalHdlcChannel super;
    }tTha60210012PhysicalHdlcHoChannel;

typedef struct tTha60210012PhysicalHdlcLoChannel
    {
    tTha60210012PhysicalHdlcChannel super;
    }tTha60210012PhysicalHdlcLoChannel;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcChannel Tha60210012PhysicalHdlcChannelObjectInit(AtHdlcChannel self, uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module);
AtHdlcChannel Tha60210012PhysicalHdlcHoChannelObjectInit(AtHdlcChannel self, uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module);
AtHdlcChannel Tha60210012PhysicalHdlcLoChannelObjectInit(AtHdlcChannel self, uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module);
uint8 Tha60210012PhysicalHdlcChannelIdlePattern2HwMode(uint8 flag);
eAtRet Tha60210012PhysicalHdlcChannelTxServiceEnable(AtHdlcChannel self, uint8 payloadType);
eAtRet Tha60210012PhysicalHdlcChannelTxServiceDisable(AtHdlcChannel self);
uint8 Tha60210012PhysicalHdlcChannelEncapType2Hw(AtHdlcChannel self, uint8 frameType);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210012PHYSICALHDLCCHANNELINTERNAL_H_ */

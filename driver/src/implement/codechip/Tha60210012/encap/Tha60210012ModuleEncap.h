/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60210012ModuleEncap.h
 * 
 * Created Date: Sep 23, 2015
 *
 * Description : ENCAP of Tha60210012 product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEENCAP_H_
#define _THA60210012MODULEENCAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthFlow.h"
#include "AtSdhChannel.h"
#include "AtPw.h"
#include "AtEncapChannel.h"
#include "../../../default/encap/ThaModuleEncap.h"
#include "../../../default/encap/profile/ThaProfileManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cHwEncapGfpMode  3
#define cHwEncapHdlcMode 1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleEncap * Tha60210012ModuleEncap;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60210012ModuleEncapHoBaseAddress(void);
uint32 Tha60210012ModuleEncapLoBaseAddress(void);
uint32 Tha60210012ModuleDecapHoBaseAddress(void);
uint32 Tha60210012ModuleDecapLoBaseAddress(void);

uint32 Tha60210012ModuleEncapEosBaseAddress(void);
eAtRet Tha60210012ModuleEncapChannelRegisterReset(AtModuleEncap self, uint32 channelId);
eAtRet Tha60210012ModuleEncapHdlcLinkFlowLookupEnable(Tha60210012ModuleEncap self, AtHdlcLink link, eBool enable);
eBool Tha60210012ModuleEncapHdlcLinkFlowLookupIsEnabled(Tha60210012ModuleEncap self, AtHdlcLink link);
eAtRet Tha60210012ModuleEncapHdlcLinkHwFlowSet(Tha60210012ModuleEncap self, AtHdlcLink link, AtEthFlow flow);
eAtRet Tha60210012ModuleEncapHdlcLinkHwFlowReset(Tha60210012ModuleEncap self, AtHdlcLink link);
eAtRet Tha60210012ModuleEncapMpBundleFlowLookupEnable(Tha60210012ModuleEncap self, AtMpBundle bundle, eBool enable);
eBool Tha60210012ModuleEncapMpBundleFlowLookupIsEnabled(Tha60210012ModuleEncap self, AtMpBundle bundle);
eAtRet Tha60210012ModuleEncapFlowMpBundleSet(Tha60210012ModuleEncap self, AtMpBundle bundle, AtEthFlow flow);
eAtRet Tha60210012ModuleEncapMpBundleFlowReset(Tha60210012ModuleEncap self, AtMpBundle bundle);
eAtRet Tha60210012ModuleEncapLinkBundleSet(AtHdlcLink physicalLink, AtHdlcBundle bundle);
eAtRet Tha60210012ModuleEncapLinkBundleReSet(AtHdlcLink physicalLink);
eAtRet Tha60210012ModuleEncapLinkBundleLookupEnable(AtHdlcLink logicLink, eBool enable);
eAtRet Tha60210012ModuleEncapHdlcChannelFlowConnect(Tha60210012ModuleEncap self, AtPw pw, AtChannel circuit);
eAtRet Tha60210012ModuleEncapHdlcLinkOamCircuitVlanInsert(AtHdlcLink self, uint16 vlanId, eBool enable);
eAtRet Tha60210012ModuleEncapHdlcBundleOamCircuitVlanInsert(AtHdlcBundle self, uint16 vlanId, eBool enable);
eAtRet Tha60210012ModuleEncapStsNcBoundHwChannelTypeSet(AtSdhChannel auVc, uint32 startOffset, uint8 hwChannelType);
eAtRet Tha60210012ModuleEncapBundlePwFlowConnect(Tha60210012ModuleEncap self, AtMpBundle bundle, AtPw pw);

eAtRet Tha60210012ModuleEncapSts3cEnable(AtModuleEncap self, AtSdhChannel vc, eBool enable);
eBool Tha60210012ModuleEncapStsNcNeedSelect(AtModuleEncap self, AtSdhChannel sdhChannel);
eBool Tha60210012ModuleEncapRegistersNeedShiftUp(Tha60210012ModuleEncap self);

/* To control Encap mode per VCG */
uint32 Tha60210012ModuleEncapChannelEncapModeGet(AtModuleEncap self, uint32 channelId);
eAtRet Tha60210012ModuleEncapChannelEncapModeSet(AtModuleEncap self, uint32 channelId, eAtEncapType encapType);
eAtRet Tha60210012ModuleEncapChannelThresholdSet(AtModuleEncap self, uint32 channelId, uint32 thres);
eAtRet Tha60210012ModuleHdlcTdmHeaderRemove(AtModuleEncap self, uint32 channelId);

AtGfpChannel Tha60210012GfpChannelNew(uint32 channelId, AtModuleEncap module);
ThaProfileManager Tha60210012ProfileManagerNew(AtModuleEncap module);

/* For debugging */
eAtRet ThaModuleEncapDebugCountersModuleSet(ThaModuleEncap self, uint32 module);
uint32 ThaModuleEncapDebugCountersModuleGet(ThaModuleEncap self);
eBool ThaModuleEncapShouldReadCountersFromSurModule(ThaModuleEncap self);

/* ENCAP Long read/write */
eAtRet Tha60210012ModuleEncapEosLongRead(AtModuleEncap self, uint32 address, uint32 *regValue);
eAtRet Tha60210012ModuleEncapEosLongWrite(AtModuleEncap self, uint32 address, uint32 *regValue);
void Tha60210012ModuleEncapEosChannelRegisterShow(AtModuleEncap self, uint32 channelId);

uint32 Tha60210012ModuleEncapNumSlice48(AtModuleEncap self);
uint32 Tha60210012ModuleEncapNumSlice24(AtModuleEncap self);
eBool Tha60210012ModuleEncapResequenceManagerIsSupported(ThaModuleEncap self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEENCAP_H_ */

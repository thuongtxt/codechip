/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : Tha60210012PhysicalHdlcChannel.h
 * 
 * Created Date: Apr 14, 2016
 *
 * Description : 60210012 physical HDLC channel interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PHYSICALHDLCCHANNEL_H_
#define _THA60210012PHYSICALHDLCCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012PhysicalHdlcChannel * Tha60210012PhysicalHdlcChannel;
typedef struct tTha60210012PhysicalHdlcHoChannel * Tha60210012PhysicalHdlcHoChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcChannel Tha60210012PhysicalHdlcHoChannelNew(uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module);
AtHdlcChannel Tha60210012PhysicalHdlcLoChannelNew(uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module);

uint32 Tha60210012PhysicalHdlcChannelEncapBaseAddress(Tha60210012PhysicalHdlcChannel self);
uint32 Tha60210012PhysicalHdlcChannelDecapBaseAddress(Tha60210012PhysicalHdlcChannel self);

eAtRet Tha60210012PhysicalHdlcChannelEncapTypeSet(Tha60210012PhysicalHdlcChannel self, uint8 encapType);
uint8 Tha60210012PhysicalHdlcChannelSw2HwFrameType(uint8 frameType);

eAtRet Tha60210012PhysicalHdlcChannelLinkLookupSet(Tha60210012PhysicalHdlcChannel self, AtHdlcLink physicalLink);
eAtRet Tha60210012PhysicalHdlcChannelLinkLookupReset(Tha60210012PhysicalHdlcChannel self);
eAtRet Tha60210012PhysicalHdlcChannelLinkLookupEnable(Tha60210012PhysicalHdlcChannel self, eBool enable);
eAtRet Tha60210012TxHdlcServiceSetup(AtHdlcChannel logicalChannel, eBool enable, uint8 payloadType);

eAtRet Tha60210012PhysicalHdlcChannelStsNcSelect(Tha60210012PhysicalHdlcChannel self, AtSdhChannel auVc);
uint32 Tha60210012PhysicalHdlcChannelDefaultOffset(Tha60210012PhysicalHdlcChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PHYSICALHDLCCHANNEL_H_ */


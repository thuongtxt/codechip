/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60210012PhysicalHdlcChannel.c
 *
 * Created Date: Apr 7, 2016
 *
 * Description : 60210012 physical HDLC channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/encap/dynamic/ThaDynamicHdlcChannel.h"
#include "../../../default/encap/dynamic/ThaModuleDynamicEncap.h"
#include "../../../default/ppp/dynamic/ThaPhysicalPppLink.h"
#include "../../../default/sur/hard/ThaModuleHardSurInternal.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../pda/Tha60210012ModulePda.h"
#include "../ppp/Tha60210012PhysicalPppLink.h"
#include "../cla/Tha60210012ModuleCla.h"
#include "../eth/Tha60210012ModuleEthFlowLookupReg.h"
#include "Tha60210012ModuleEncap.h"
#include "Tha60210012ModuleDecapParserReg.h"
#include "Tha60210012PhysicalEncapReg.h"
#include "Tha60210012PhysicalHdlcChannel.h"
#include "Tha60210012PhysicalCiscoHdlcLink.h"
#include "Tha60210012PmcInternalReg.h"
#include "Tha60210012PhysicalHdlcChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cFragmentCounter 0
#define cPlainCounter    1
#define cOamCounter      2
#define cAbortCounter    3
#define cFcsErrorCounter 4
#define cDiscardCounter  5
#define cMRUCounter      6

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210012PhysicalHdlcChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012PhysicalHdlcChannelMethods m_methods;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tAtHdlcChannelMethods m_AtHdlcChannelOverride;
static tThaHdlcChannelMethods m_ThaHdlcChannelOverride;
static tThaPhysicalHdlcChannelMethods m_ThaPhysicalHdlcChannelOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtHdlcChannelMethods *m_AtHdlcChannelMethods = NULL;
static const tThaPhysicalHdlcChannelMethods *m_ThaPhysicalHdlcChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHdlcLink LinkCreate(ThaPhysicalHdlcChannel self)
    {
    eAtHdlcFrameType frameType = AtHdlcChannelFrameTypeGet((AtHdlcChannel)self);

    if (frameType == cAtHdlcFrmCiscoHdlc)
        return (AtHdlcLink)Tha60210012PhysicalCiscoHdlcLinkNew((AtHdlcChannel)self);

    if (frameType == cAtHdlcFrmPpp)
        return (AtHdlcLink)Tha60210012PhysicalPppLinkNew((AtHdlcChannel)self);

    return m_ThaPhysicalHdlcChannelMethods->LinkCreate(self);
    }

static void LinkFlowLookupPrint(AtChannel link)
    {
    uint32 offset = cThaModuleEthFlowLookupBaseAddress + AtChannelHwIdGet(link);
    uint32 regAddress = cAf6Reg_pppdat_ethflow_table_Base + offset;

    ThaDeviceRegNameDisplay("Link (Data) to Ethernet Flow Lookup Table");
    ThaDeviceChannelRegValueDisplay(link, regAddress, cAtModuleEncap);
    }

static eAtRet Debug(AtChannel self)
    {
    AtHdlcChannel hdlcChannel = (AtHdlcChannel)self;
    AtHdlcLink link = AtHdlcChannelHdlcLinkGet(hdlcChannel);

    AtPrintc(cSevNormal, "* Channel: \r\n");
    AtPrintc(cSevInfo, "   - Channel HW ID: %u\r\n", AtChannelHwIdGet(self));
    AtPrintc(cSevInfo, "   - Channel In Slice: %u\r\n", ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)self));
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "* Link mapping: \r\n");
    AtPrintc(cSevInfo, "   - Link HW ID: %u\r\n", AtChannelHwIdGet((AtChannel)link));

    LinkFlowLookupPrint((AtChannel)link);
    return cAtOk;
    }

static uint32 EncapControlAddress(Tha60210012PhysicalHdlcChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 SliceOffset(Tha60210012PhysicalHdlcChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    Tha60210012ModuleEncap encapModule;
    AtChannel physicalHdlcLink;
    if (pseudowire == NULL)
        {
        AtHdlcChannel hdlcChannel = (AtHdlcChannel)self;
        return AtHdlcChannelFrameTypeSet(hdlcChannel, AtHdlcChannelFrameTypeGet(hdlcChannel));
        }

    /* Frame type will be actually configured to HW when PW payload type is set */
    encapModule = (Tha60210012ModuleEncap)AtChannelModuleGet(self);
    physicalHdlcLink = (AtChannel)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self);
    return Tha60210012ModuleEncapHdlcChannelFlowConnect(encapModule, (AtPw)pseudowire, physicalHdlcLink);
    }

static Tha60210012ModuleCla ModuleCla(ThaPhysicalHdlcChannel self)
    {
    return (Tha60210012ModuleCla)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleCla);
    }

static uint32 HaRestore(ThaPhysicalHdlcChannel self)
    {
    uint32 remain = m_ThaPhysicalHdlcChannelMethods->HaRestore(self);
    Tha60210012ModuleCla moduleCla = ModuleCla(self);

    /* Channel pw */
    AtPw pw = AtChannelBoundPwGet((AtChannel)self);
    if (pw)
        remain = remain + Tha60210012ModuleClaPwEthFlowHaRestore(moduleCla, pw);

    /* Link pw */
    pw = AtChannelBoundPwGet((AtChannel)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self));
    if (pw)
        remain = remain + Tha60210012ModuleClaPwEthFlowHaRestore(moduleCla, pw);

    return remain;
    }

static eBool ThaHdlcChannelShouldReadCounterFromSurModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleEncap encapModule = (ThaModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    return ThaModuleEncapShouldReadCountersFromSurModule(encapModule);
    }

static eAtRet QueueEnable(AtChannel self, eBool enable)
    {
    AtChannel physicalLink = (AtChannel)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self);
    return AtChannelQueueEnable(physicalLink, enable);
    }

static eAtRet TxServiceEnable(AtHdlcChannel phyHdlcChannel, uint8 payloadType)
    {
    eAtRet ret;

    /* Enable Mpeg queue */
    ret = AtChannelQueueEnable((AtChannel)phyHdlcChannel, cAtTrue);
    if (ret != cAtOk)
        return ret;

    ret = Tha60210012ModulePdaHdlcServiceLoopkupSet(phyHdlcChannel);
    if (ret != cAtOk)
        return ret;

    ret = Tha60210012ModulePdaHdlcTdmPayloadMode(phyHdlcChannel, payloadType);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static eAtRet TxServiceDisable(AtHdlcChannel phyHdlcChannel)
    {
    eAtRet ret;

    /* Enable Mpeg queue */
    ret = AtChannelQueueEnable((AtChannel)phyHdlcChannel, cAtFalse);
    if (ret != cAtOk)
        return ret;

    ret = Tha60210012ModulePdaHdlcServiceLoopkupReset(phyHdlcChannel);
    if (ret != cAtOk)
        return ret;

    ret = Tha60210012ModulePdaHdlcTdmPayloadModeReset(phyHdlcChannel);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool rxTrafficEnable)
    {
    eAtRet ret = cAtOk;
    AtHdlcChannel physicalChannel = (AtHdlcChannel)self;
    AtEncapChannel logicChannel = (AtEncapChannel)ThaPhysicalHdlcChannelLogicalChannelGet(physicalChannel);
    AtChannel tdmChannel = AtEncapChannelBoundPhyGet(logicChannel);

    if (rxTrafficEnable)
        {
        ret |= Tha60210012PhysicalHdlcChannelLinkLookupEnable(mThis(physicalChannel), rxTrafficEnable);
        ret |= mMethodsGet(tdmChannel)->RxEncapConnectionEnable(tdmChannel, rxTrafficEnable);

        return ret;
        }

    ret |= mMethodsGet(tdmChannel)->RxEncapConnectionEnable(tdmChannel, rxTrafficEnable);
    ret |= Tha60210012PhysicalHdlcChannelLinkLookupEnable(mThis(physicalChannel), rxTrafficEnable);

    return ret;
    }

static uint8 EncapTypeSw2Hw(AtHdlcChannel self, uint8 frameType)
    {
    AtUnused(self);
    if (frameType == cAtHdlcFrmCiscoHdlc)
        return 0;
    if (frameType == cAtHdlcFrmPpp)
        return 1;
    if (frameType == cAtHdlcFrmFr)
        return 2;
    if (frameType == cAtHdlcFrmLapd)
        return 0; /* It should be 3, but there's an issue with parser engine
                     that it things LAPD packets are MLPPP error packets
                     so all packets are dropped.
                     To workaround, configure mode 0 (pw mode) so that parser engine
                     will stop examine packets header */

    /* Invalid */
    return 4;
    }

static eAtRet HdlcLinkSet(AtHdlcChannel self, AtHdlcLink link)
    {
    eAtRet ret;

    if (link)
        {
        uint8 frameType = AtHdlcChannelFrameTypeGet(self);
        AtHdlcChannelFrameTypeSet(self, frameType);
        Tha60210012PhysicalHdlcChannelEncapTypeSet(mThis(self), EncapTypeSw2Hw(self, frameType));
        ret = Tha60210012PhysicalHdlcChannelLinkLookupSet(mThis(self), link);
        }
    else
        ret = Tha60210012PhysicalHdlcChannelLinkLookupReset(mThis(self));

    if (ret != cAtOk)
        return ret;

    return m_AtHdlcChannelMethods->HdlcLinkSet(self, link);
    }

static eAtHdlcStuffMode StuffModeGet(AtHdlcChannel self)
    {
    uint32 regAddr, regVal;
    uint8 isBitStuff;

    regAddr = mMethodsGet(mThis(self))->EncapControlAddress(mThis(self)) + Tha60210012PhysicalHdlcChannelDefaultOffset(mThis(self));
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    isBitStuff = (uint8)mRegField(regVal, cAf6_upenctr1_bit_stuff_);

    return (isBitStuff == 1) ? cAtHdlcStuffBit : cAtHdlcStuffByte;
    }

static eAtRet FcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    uint8 hwFcsMode;
    uint8 fcsEnabled;
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(mThis(self))->EncapControlAddress(mThis(self)) + Tha60210012PhysicalHdlcChannelDefaultOffset(mThis(self));
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    fcsEnabled = (fcsMode == cAtHdlcFcsModeNoFcs) ? 0 : 1;
    mRegFieldSet(regVal, cAf6_upenctr1_fcsinscfg_, fcsEnabled);
    hwFcsMode = (fcsMode == cAtHdlcFcsModeFcs32) ? 1 : 0;
    mRegFieldSet(regVal, cAf6_upenctr1_fcsmod32_, hwFcsMode);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eAtHdlcFcsMode FcsModeGet(AtHdlcChannel self)
    {
    uint8 hwFcsMode;
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(mThis(self))->EncapControlAddress(mThis(self)) + Tha60210012PhysicalHdlcChannelDefaultOffset(mThis(self));
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    if (mRegField(regVal, cAf6_upenctr1_fcsinscfg_) == 0)
        return cAtHdlcFcsModeNoFcs;

    hwFcsMode = (uint8)mRegField(regVal, cAf6_upenctr1_fcsmod32_);
    return (hwFcsMode == 1) ? cAtHdlcFcsModeFcs32 : cAtHdlcFcsModeFcs16;
    }

static uint8 SdhChannelType2HwType(AtSdhChannel auVc)
    {
    uint8 channelType = AtSdhChannelTypeGet(auVc);

    if (channelType == cAtSdhChannelTypeVc3)
        return 1;
    if (channelType == cAtSdhChannelTypeVc4)
        return 2;
    if (channelType == cAtSdhChannelTypeVc4_4c)
        return 3;
    if (channelType == cAtSdhChannelTypeVc4_16c)
        return 5;

    /* Otherwise invalid values */
    return cBit7_0;
    }

static uint32 TxCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset)
    {
    AtUnused(self);
    AtUnused(r2c);
    AtUnused(counterOffset);
    return 0;
    }

static uint32 RxCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset)
    {
    AtUnused(self);
    AtUnused(r2c);
    AtUnused(counterOffset);
    return 0;
    }

static uint32 TxLinkCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset)
    {
    AtUnused(self);
    AtUnused(r2c);
    AtUnused(counterOffset);
    return 0;
    }

static uint32 RxLinkCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset)
    {
    AtUnused(self);
    AtUnused(r2c);
    AtUnused(counterOffset);
    return 0;
    }

static uint32 TxByteIdleCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c)
    {
    AtUnused(self);
    AtUnused(r2c);
    return 0;
    }

static uint32 RxByteIdleCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c)
    {
    AtUnused(self);
    AtUnused(r2c);
    return 0;
    }

static ThaModuleHardSur ModuleSur(AtChannel self)
    {
    return (ThaModuleHardSur)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleSur);
    }

static eAtRet SurAllCountersRead(AtChannel self, tAtHdlcChannelCounters* allCounters, eBool r2c)
    {
    eAtRet ret = ThaModuleHardSurHdlcChannelCounterLatch(ModuleSur(self), self, r2c);
    if (ret != cAtOk)
        {
        AtChannelLog(self, cAtLogLevelCritical, AtSourceLocation, "SUR HDLC channel counter latching fail with ret = %s\r\n", AtRet2String(ret));
        return ret;
        }

    allCounters->txGoodPkt    = ThaModuleHardSurHdlcChannelTxGoodPacketsGet(ModuleSur(self), self, r2c);
    allCounters->txGoodByte   = ThaModuleHardSurHdlcChannelTxGoodBytesGet(ModuleSur(self), self, r2c);
    allCounters->txFragment   = ThaModuleHardSurHdlcChannelTxFragmentsGet(ModuleSur(self), self, r2c);
    allCounters->txPlain      = ThaModuleHardSurHdlcChannelTxPlainGet(ModuleSur(self), self, r2c);
    allCounters->txOam        = ThaModuleHardSurHdlcChannelTxOamGet(ModuleSur(self), self, r2c);
    allCounters->txIdleByte   = ThaModuleHardSurHdlcChannelTxIdleBytesGet(ModuleSur(self), self, r2c);
    allCounters->txAbortPkt   = ThaModuleHardSurHdlcChannelTxAbortsGet(ModuleSur(self), self, r2c);
    allCounters->rxGoodPkt    = ThaModuleHardSurHdlcChannelRxGoodPacketsGet(ModuleSur(self), self, r2c);
    allCounters->rxByte       = ThaModuleHardSurHdlcChannelRxBytesGet(ModuleSur(self), self, r2c);
    allCounters->rxGoodByte   = ThaModuleHardSurHdlcChannelRxGoodBytesGet(ModuleSur(self), self, r2c);
    allCounters->rxFragment   = ThaModuleHardSurHdlcChannelRxFragmentsGet(ModuleSur(self), self, r2c);
    allCounters->rxPlain      = ThaModuleHardSurHdlcChannelRxPlainGet(ModuleSur(self), self, r2c);
    allCounters->rxOam        = ThaModuleHardSurHdlcChannelRxOamGet(ModuleSur(self), self, r2c);
    allCounters->rxAbortPkt   = ThaModuleHardSurHdlcChannelRxAbortsGet(ModuleSur(self), self, r2c);
    allCounters->rxIdleByte   = ThaModuleHardSurHdlcChannelRxIdleBytesGet(ModuleSur(self), self, r2c);
    allCounters->rxMRUPkts    = ThaModuleHardSurHdlcChannelRxMRUPacketsGet(ModuleSur(self), self, r2c);
    allCounters->rxFcsErrPkt  = ThaModuleHardSurHdlcChannelRxFcsErrorGet(ModuleSur(self), self, r2c);
    allCounters->rxDiscardPkt = ThaModuleHardSurHdlcChannelRxDiscardFramesGet(ModuleSur(self), self, r2c);

    return cAtOk;
    }

static uint32 PmcCounterRead(AtChannel self, uint16 counterType, eBool r2c)
    {
    switch (counterType)
        {
        case cAtHdlcChannelCounterTypeTxPackets:
            return mMethodsGet(mThis(self))->TxCounterGet(mThis(self), r2c, cPktCounter);

        case cAtHdlcChannelCounterTypeTxBytes:
            return mMethodsGet(mThis(self))->TxCounterGet(mThis(self), r2c, cByteCounter);

        case cAtHdlcChannelCounterTypeTxGoodPackets:
            return mMethodsGet(mThis(self))->TxGoodCounterGet(mThis(self), r2c, cPktCounter);

        case cAtHdlcChannelCounterTypeTxGoodBytes:
            return mMethodsGet(mThis(self))->TxGoodCounterGet(mThis(self), r2c, cByteCounter);

        case cAtHdlcChannelCounterTypeTxFragments:
            return mMethodsGet(mThis(self))->TxLinkCounterGet(mThis(self), r2c, cFragmentCounter);

        case cAtHdlcChannelCounterTypeTxPlain:
            return mMethodsGet(mThis(self))->TxLinkCounterGet(mThis(self), r2c, cPlainCounter);

        case cAtHdlcChannelCounterTypeTxOam:
            return mMethodsGet(mThis(self))->TxLinkCounterGet(mThis(self), r2c, cOamCounter);

        case cAtHdlcChannelCounterTypeTxIdleBytes:
            return mMethodsGet(mThis(self))->TxByteIdleCounterGet(mThis(self), r2c);

        case cAtHdlcChannelCounterTypeTxAborts:
            return mMethodsGet(mThis(self))->TxLinkCounterGet(mThis(self), r2c, cAbortCounter);

        case cAtHdlcChannelCounterTypeRxPackets:
            return mMethodsGet(mThis(self))->RxCounterGet(mThis(self), r2c, cPktCounter);

        case cAtHdlcChannelCounterTypeRxBytes:
            return mMethodsGet(mThis(self))->RxCounterGet(mThis(self), r2c, cByteCounter);

        case cAtHdlcChannelCounterTypeRxGoodPackets:
            return mMethodsGet(mThis(self))->RxGoodCounterGet(mThis(self), r2c, cPktCounter);

        case cAtHdlcChannelCounterTypeRxGoodBytes:
            return mMethodsGet(mThis(self))->RxGoodCounterGet(mThis(self), r2c, cByteCounter);

        case cAtHdlcChannelCounterTypeRxFragments:
            return mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cFragmentCounter);

        case cAtHdlcChannelCounterTypeRxPlains:
            return mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cPlainCounter);

        case cAtHdlcChannelCounterTypeRxOams:
            return mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cOamCounter);

        case cAtHdlcChannelCounterTypeRxIdleBytes:
            return mMethodsGet(mThis(self))->RxByteIdleCounterGet(mThis(self), r2c);

        case cAtHdlcChannelCounterTypeRxAborts:
            return mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cAbortCounter);

        case cAtHdlcChannelCounterTypeRxFcsErrors:
            return mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cFcsErrorCounter);

        case cAtHdlcChannelCounterTypeRxMRUPackets:
            return mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cMRUCounter);

        case cAtHdlcChannelCounterTypeRxDiscardFrames:
            return mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cDiscardCounter);

        case cAtHdlcChannelCounterTypeTxMTUPackets:
        case cAtHdlcChannelCounterTypeRxUndersizePackets:
        case cAtHdlcChannelCounterTypeRxUndersizeBytes:
        case cAtHdlcChannelCounterTypeRxOversizePackets:
        case cAtHdlcChannelCounterTypeRxOversizeBytes:
        case cAtHdlcChannelCounterTypeRxAddrCtrlErrors:
        default:
            return 0;
        }
    }

static uint32 CounterGetClear(AtChannel self, uint16 counterType, eBool r2c)
    {
    if (ThaHdlcChannelShouldReadCounterFromSurModule(self))
        return 0; /* SUR module does not support get counter per type but all counters at a time */

    return PmcCounterRead(self, counterType, r2c);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return CounterGetClear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return CounterGetClear(self, counterType, cAtTrue);
    }

static eAtRet PmcAllCountersRead(AtChannel self, tAtHdlcChannelCounters* allCounters, eBool r2c)
    {
    allCounters->txPkt        = mMethodsGet(mThis(self))->TxCounterGet(mThis(self), r2c, cPktCounter);
    allCounters->txByte       = mMethodsGet(mThis(self))->TxCounterGet(mThis(self), r2c, cByteCounter);
    allCounters->txGoodPkt    = mMethodsGet(mThis(self))->TxGoodCounterGet(mThis(self), r2c, cPktCounter);
    allCounters->txGoodByte   = mMethodsGet(mThis(self))->TxGoodCounterGet(mThis(self), r2c, cByteCounter);
    allCounters->txFragment   = mMethodsGet(mThis(self))->TxLinkCounterGet(mThis(self), r2c, cFragmentCounter);
    allCounters->txPlain      = mMethodsGet(mThis(self))->TxLinkCounterGet(mThis(self), r2c, cPlainCounter);
    allCounters->txOam        = mMethodsGet(mThis(self))->TxLinkCounterGet(mThis(self), r2c, cOamCounter);
    allCounters->txIdleByte   = mMethodsGet(mThis(self))->TxByteIdleCounterGet(mThis(self), r2c);
    allCounters->txAbortPkt   = mMethodsGet(mThis(self))->TxLinkCounterGet(mThis(self), r2c, cAbortCounter);
    allCounters->rxPkt        = mMethodsGet(mThis(self))->RxCounterGet(mThis(self), r2c, cPktCounter);
    allCounters->rxByte       = mMethodsGet(mThis(self))->RxCounterGet(mThis(self), r2c, cByteCounter);
    allCounters->rxGoodPkt    = mMethodsGet(mThis(self))->RxGoodCounterGet(mThis(self), r2c, cPktCounter);
    allCounters->rxGoodByte   = mMethodsGet(mThis(self))->RxGoodCounterGet(mThis(self), r2c, cByteCounter);
    allCounters->rxFragment   = mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cFragmentCounter);
    allCounters->rxPlain      = mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cPlainCounter);
    allCounters->rxOam        = mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cOamCounter);
    allCounters->rxIdleByte   = mMethodsGet(mThis(self))->RxByteIdleCounterGet(mThis(self), r2c);
    allCounters->rxAbortPkt   = mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cAbortCounter);
    allCounters->rxFcsErrPkt  = mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cFcsErrorCounter);
    allCounters->rxMRUPkts    = mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cMRUCounter);
    allCounters->rxDiscardPkt = mMethodsGet(mThis(self))->RxLinkCounterGet(mThis(self), r2c, cDiscardCounter);

    return cAtOk;
    }

static eAtRet AllCountersGetClear(AtChannel self, void *pAllCounters, eBool r2c)
    {
    if (pAllCounters == NULL)
        return cAtErrorNullPointer;

    AtOsalMemInit(pAllCounters, 0, sizeof(tAtHdlcChannelCounters));

    if (ThaHdlcChannelShouldReadCounterFromSurModule(self))
        return SurAllCountersRead(self, pAllCounters, r2c);

    return PmcAllCountersRead(self, pAllCounters, r2c);
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersGetClear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersGetClear(self, pAllCounters, cAtTrue);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtHdlcChannelCounterTypeTxPackets:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeTxBytes:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeTxGoodPackets:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeTxGoodBytes:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeTxFragments:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeTxPlain:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeTxOam:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeTxIdleBytes:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeTxAborts:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeRxPackets:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeRxBytes:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeRxGoodPackets:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeRxGoodBytes:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeRxFragments:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeRxPlains:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeRxOams:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeRxIdleBytes:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeRxAborts:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeRxFcsErrors:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeRxMRUPackets:
            return cAtTrue;
        case cAtHdlcChannelCounterTypeRxDiscardFrames:
            return cAtTrue;

        default:
            return cAtFalse;
        }
    }

static eAtRet EncapTypeSet(Tha60210012PhysicalHdlcChannel self, uint8 encapType)
    {
    AtUnused(self);
    AtUnused(encapType);
    return cAtOk;
    }

static uint32 EncapBaseAddress(Tha60210012PhysicalHdlcChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DecapBaseAddress(Tha60210012PhysicalHdlcChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet LinkLookupSet(Tha60210012PhysicalHdlcChannel self, AtHdlcLink physicalLink)
    {
    AtUnused(self);
    AtUnused(physicalLink);
    return cAtOk;
    }

static eAtRet LinkLookupEnable(Tha60210012PhysicalHdlcChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eAtRet LinkLookupReset(Tha60210012PhysicalHdlcChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static uint32 TxGoodCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset)
    {
    AtUnused(self);
    AtUnused(r2c);
    AtUnused(counterOffset);
    return 0;
    }

static uint32 RxGoodCounterGet(Tha60210012PhysicalHdlcChannel self, eBool r2c, uint8 counterOffset)
    {
    AtUnused(self);
    AtUnused(r2c);
    AtUnused(counterOffset);
    return 0;
    }

static eAtRet HdlcPwPayloadTypeSet(ThaHdlcChannel self, eAtPwHdlcPayloadType payloadType)
    {
    AtHdlcChannel phyChannel = (AtHdlcChannel)self;
    uint8 frameType = (uint8)((payloadType == cAtPwHdlcPayloadTypePdu) ? AtHdlcChannelFrameTypeGet(phyChannel) : cAtHdlcFrmUnknown);

    return AtHdlcChannelFrameTypeSet(phyChannel, frameType);
    }

static void MethodsInit(Tha60210012PhysicalHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, EncapTypeSet);
        mMethodOverride(m_methods, EncapBaseAddress);
        mMethodOverride(m_methods, DecapBaseAddress);
        mMethodOverride(m_methods, LinkLookupSet);
        mMethodOverride(m_methods, LinkLookupEnable);
        mMethodOverride(m_methods, LinkLookupReset);
        mMethodOverride(m_methods, EncapControlAddress);
        mMethodOverride(m_methods, SliceOffset);
        mMethodOverride(m_methods, TxLinkCounterGet);
        mMethodOverride(m_methods, RxLinkCounterGet);
        mMethodOverride(m_methods, TxCounterGet);
        mMethodOverride(m_methods, RxCounterGet);
        mMethodOverride(m_methods, TxByteIdleCounterGet);
        mMethodOverride(m_methods, RxByteIdleCounterGet);
        mMethodOverride(m_methods, TxGoodCounterGet);
        mMethodOverride(m_methods, RxGoodCounterGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtChannel(AtHdlcChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, QueueEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, m_AtHdlcChannelMethods, sizeof(m_AtHdlcChannelOverride));

        mMethodOverride(m_AtHdlcChannelOverride, HdlcLinkSet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeSet);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void OverrideThaHdlcChannel(AtHdlcChannel self)
    {
    ThaHdlcChannel hdlcChannel = (ThaHdlcChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHdlcChannelOverride, mMethodsGet(hdlcChannel), sizeof(m_ThaHdlcChannelOverride));

        mMethodOverride(m_ThaHdlcChannelOverride, HdlcPwPayloadTypeSet);
        }

    mMethodsSet(hdlcChannel, &m_ThaHdlcChannelOverride);
    }

static void OverrideThaPhysicalHdlcChannel(AtHdlcChannel self)
    {
    ThaPhysicalHdlcChannel channel = (ThaPhysicalHdlcChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPhysicalHdlcChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPhysicalHdlcChannelOverride, m_ThaPhysicalHdlcChannelMethods, sizeof(m_ThaPhysicalHdlcChannelOverride));

        mMethodOverride(m_ThaPhysicalHdlcChannelOverride, LinkCreate);
        mMethodOverride(m_ThaPhysicalHdlcChannelOverride, HaRestore);
        }

    mMethodsSet(channel, &m_ThaPhysicalHdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtChannel(self);
    OverrideAtHdlcChannel(self);
    OverrideThaHdlcChannel(self);
    OverrideThaPhysicalHdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012PhysicalHdlcChannel);
    }

AtHdlcChannel Tha60210012PhysicalHdlcChannelObjectInit(AtHdlcChannel self, uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPhysicalHdlcChannelObjectInit(self, channelId, logicalChannel, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((Tha60210012PhysicalHdlcChannel)self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

eAtRet Tha60210012PhysicalHdlcChannelEncapTypeSet(Tha60210012PhysicalHdlcChannel self, uint8 encapType)
    {
    if (self)
        return mMethodsGet(self)->EncapTypeSet(self, encapType);

    return cAtErrorNullPointer;
    }

uint32 Tha60210012PhysicalHdlcChannelEncapBaseAddress(Tha60210012PhysicalHdlcChannel self)
    {
    if (self)
        return mMethodsGet(self)->EncapBaseAddress(self);

    return cInvalidUint32;
    }

uint32 Tha60210012PhysicalHdlcChannelDecapBaseAddress(Tha60210012PhysicalHdlcChannel self)
    {
    if (self)
        return mMethodsGet(self)->DecapBaseAddress(self);

    return cInvalidUint32;
    }

eAtRet Tha60210012TxHdlcServiceSetup(AtHdlcChannel logicalChannel, eBool enable, uint8 payloadType)
    {
    AtHdlcChannel phyHdlcChannel = ThaHdlcChannelPhysicalChannelGet(logicalChannel);
    if (enable)
        return TxServiceEnable(phyHdlcChannel, payloadType);

    return TxServiceDisable(phyHdlcChannel);
    }

eAtRet Tha60210012PhysicalHdlcChannelLinkLookupSet(Tha60210012PhysicalHdlcChannel self, AtHdlcLink physicalLink)
    {
    if (self)
        return mMethodsGet(self)->LinkLookupSet(self, physicalLink);

    return cAtErrorNullPointer;
    }

eAtRet Tha60210012PhysicalHdlcChannelLinkLookupEnable(Tha60210012PhysicalHdlcChannel self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->LinkLookupEnable(self, enable);

    return cAtErrorNullPointer;
    }

eAtRet Tha60210012PhysicalHdlcChannelLinkLookupReset(Tha60210012PhysicalHdlcChannel self)
    {
    if (self)
        return mMethodsGet(self)->LinkLookupReset(self);

    return cAtErrorNullPointer;
    }

eAtRet Tha60210012PhysicalHdlcChannelStsNcSelect(Tha60210012PhysicalHdlcChannel self, AtSdhChannel auVc)
    {
    if (self)
        {
        uint32 startOffset = mMethodsGet(self)->SliceOffset(self) + Tha60210012PhysicalHdlcChannelEncapBaseAddress(self);
        return Tha60210012ModuleEncapStsNcBoundHwChannelTypeSet(auVc, startOffset, SdhChannelType2HwType(auVc));
        }

    return cAtErrorNullPointer;
    }

uint8 Tha60210012PhysicalHdlcChannelSw2HwFrameType(uint8 frameType)
    {
    if (frameType == cAtHdlcFrmUnknown)   return 0;
    if (frameType == cAtHdlcFrmCiscoHdlc) return 2;
    if (frameType == cAtHdlcFrmPpp)       return 1;
    if (frameType == cAtHdlcFrmLapd)      return 0; /* It should be 1, but there's an issue with parser engine
                                                       that it things LAPD packets are MLPPP error packets
                                                       so all packets are dropped.
                                                       To workaround, configure mode 0 (pw mode) so that parser engine
                                                       will stop examine packets header */

    /* For frame relay mode will updated when start code */
    return cBit7_0;
    }

uint32 Tha60210012PhysicalHdlcChannelDefaultOffset(Tha60210012PhysicalHdlcChannel self)
    {
    if (self)
        return mMethodsGet(mThis(self))->SliceOffset(mThis(self)) + AtChannelHwIdGet((AtChannel)self);

    return cInvalidUint32;
    }

uint8 Tha60210012PhysicalHdlcChannelIdlePattern2HwMode(uint8 flag)
    {
    if (flag == 0xFF)
        return 1;

    if (flag == 0x7E)
        return 0;

    return cBit7_0;
    }

eAtRet Tha60210012PhysicalHdlcChannelTxServiceEnable(AtHdlcChannel self, uint8 payloadType)
    {
    if (self)
        return TxServiceEnable(self, payloadType);
    return cAtErrorNullPointer;
    }

eAtRet Tha60210012PhysicalHdlcChannelTxServiceDisable(AtHdlcChannel self)
    {
    if (self)
        return TxServiceDisable(self);
    return cAtErrorNullPointer;
    }

uint8 Tha60210012PhysicalHdlcChannelEncapType2Hw(AtHdlcChannel self, uint8 frameType)
    {
    if (self)
        return EncapTypeSw2Hw(self, frameType);
    return 0;
    }

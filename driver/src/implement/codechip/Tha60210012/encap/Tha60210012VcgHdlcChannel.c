/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60210012HdlcChannel.c
 *
 * Created Date: Apr 7, 2016
 *
 * Description : 60210012 HDLC channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/encap/AtHdlcChannelInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "Tha60210012ModuleEncap.h"
#include "Tha60210012PhysicalEosEncapReg.h"
#include "Tha60210012VcgEncapChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012VcgHdlcChannel
    {
    tAtHdlcChannel super;
    }tTha60210012VcgHdlcChannel;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods              m_AtChannelOverride;
static tAtEncapChannelMethods         m_AtEncapChannelOverride;
static tAtHdlcChannelMethods          m_AtHdlcChannelOverride;

/* Save super implementation */
static const tAtChannelMethods      *m_AtChannelMethods      = NULL;
static const tAtEncapChannelMethods *m_AtEncapChannelMethods = NULL;
static const tAtHdlcChannelMethods  *m_AtHdlcChannelMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(void)
    {
    return Tha60210012ModuleEncapEosBaseAddress();
    }

static AtModuleEncap EncapModule(AtChannel self)
    {
    return (AtModuleEncap)AtChannelModuleGet(self);
    }

static uint32 ChannelOffset(AtEncapChannel self)
    {
    return AtChannelHwIdGet((AtChannel)self);
    }

static eAtRet FlagSet(AtHdlcChannel self, uint8 flag)
    {
    AtUnused(self);
    return (flag == 0x7E) ? cAtOk  : cAtErrorModeNotSupport;
    }

static uint8 FlagGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return 0x7E;
    }

static eAtModuleEncapRet IdlePatternSet(AtHdlcChannel self, uint8 idle)
    {
    AtUnused(self);
    return (idle == 0x7E) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 IdlePatternGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return 0x7E;
    }

static eAtRet ScrambleEnable(AtEncapChannel self, eBool enable)
    {
    Tha60210012EncapChannelScrambleEnable(self, enable);
    Tha60210012DecapChannelScrambleEnable(self, enable);

    return cAtOk;
    }

static eBool ScrambleIsEnabled(AtEncapChannel self)
    {
    return Tha60210012EncapChannelScrambleIsEnabled(self);
    }

static eBool FcsModeIsSupported(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    AtUnused(self);
    return (fcsMode == cAtHdlcFcsModeFcs32) ? cAtTrue : cAtFalse;
    }

static eAtRet FcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    AtUnused(self);
    return (fcsMode == cAtHdlcFcsModeFcs32) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtHdlcFcsMode FcsModeGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtHdlcFcsModeFcs32;
    }

static eAtHdlcFcsMode FcsModeDefault(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtHdlcFcsModeFcs32;
    }

static eBool StuffModeIsSupported(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    AtUnused(self);
    return (stuffMode == cAtHdlcStuffByte) ? cAtTrue : cAtFalse;
    }

static eAtHdlcStuffMode StuffModeDefault(AtHdlcChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtHdlcStuffByte;
    }

static eAtRet StuffModeSet(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    AtUnused(self);
    return (stuffMode == cAtHdlcStuffByte) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtHdlcStuffMode StuffModeGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtHdlcStuffByte;
    }

static eBool MtuIsSupported(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtEncapType EncapTypeGet(AtEncapChannel self)
    {
    AtUnused(self);
    return cAtEncapHdlc;
    }

static eAtHdlcFrameType FrameTypeGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtHdlcFrmLaps;
    }

static eAtRet FrameTypeSet(AtHdlcChannel self, uint8 frameType)
    {
    AtUnused(self);
    return (frameType == cAtHdlcFrmLaps) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtRet HwInit(AtChannel self)
    {
    eAtRet ret = cAtOk;
    AtModuleEncap moduleEncap = EncapModule(self);

    ret |= Tha60210012ModuleHdlcTdmHeaderRemove(moduleEncap, AtChannelIdGet(self));
    ret |= Tha60210012ModuleEncapChannelEncapModeSet(moduleEncap, AtChannelIdGet(self), cAtEncapHdlc);
    ret |= Tha60210012ModuleEncapChannelThresholdSet(moduleEncap, AtChannelIdGet(self), 3);

    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    uint8 expectedAddress = 0x4;
    uint8 expectedControl = 0x3;

    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret = Tha60210012ModuleEncapChannelRegisterReset(EncapModule(self), AtChannelIdGet(self));
    ret |= AtHdlcChannelExpectedAddressSet((AtHdlcChannel)self, &expectedAddress);
    ret |= AtHdlcChannelAddressMonitorEnable((AtHdlcChannel)self, cAtTrue);
    ret |= AtHdlcChannelExpectedControlSet((AtHdlcChannel)self, &expectedControl);
    ret |= AtHdlcChannelControlMonitorEnable((AtHdlcChannel)self, cAtTrue);
    ret |= HwInit(self);
    ret |= AtChannelEnable(self, cAtTrue);
    ret |= AtEncapChannelScrambleEnable((AtEncapChannel)self, cAtTrue);

    return ret;
    }

static eAtRet Debug(AtChannel self)
    {
    Tha60210012ModuleEncapEosChannelRegisterShow(EncapModule(self), AtChannelIdGet(self));
    return cAtOk;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtHdlcChannelCounterTypeTxBytes         : return cAtTrue;
        case cAtHdlcChannelCounterTypeTxIdleBytes     : return cAtTrue;
        case cAtHdlcChannelCounterTypeTxPackets       : return cAtTrue;
        case cAtHdlcChannelCounterTypeRxBytes         : return cAtTrue;
        case cAtHdlcChannelCounterTypeRxIdleBytes     : return cAtTrue;
        case cAtHdlcChannelCounterTypeRxGoodPackets   : return cAtTrue;
        case cAtHdlcChannelCounterTypeRxFcsErrors     : return cAtTrue;
        case cAtHdlcChannelCounterTypeRxAddrCtrlErrors: return cAtTrue;
        case cAtHdlcChannelCounterTypeRxAborts        : return cAtTrue;

        default:return cAtFalse;
        }
    }

static uint32 CounterBaseAddress(AtChannel self, uint16 counterType, eBool r2c)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cAtHdlcChannelCounterTypeTxBytes         : return r2c ? cAf6Reg_upencnt_enc_clen_r2c_Base  : cAf6Reg_upencnt_enc_clen_ro_Base;
        case cAtHdlcChannelCounterTypeTxPackets       : return r2c ? cAf6Reg_upencnt_enc_even1_r2c_Base : cAf6Reg_upencnt_enc_even1_ro_Base;
        case cAtHdlcChannelCounterTypeTxIdleBytes     : return r2c ? cAf6Reg_upencnt_enc_even2_r2c_Base : cAf6Reg_upencnt_enc_even2_ro_Base;
        case cAtHdlcChannelCounterTypeRxBytes         : return r2c ? cAf6Reg_upencnt_dec_clen_Base      : cAf6Reg_upencnt_dec_clen_ro_Base;
        case cAtHdlcChannelCounterTypeRxIdleBytes     : return r2c ? cAf6Reg_upencnt_dec_cline_r2c_Base : cAf6Reg_upencnt_dec_cline_ro_Base;
        case cAtHdlcChannelCounterTypeRxGoodPackets   : return r2c ? cAf6Reg_upencnt_dec_even1_Base     : cAf6Reg_upencnt_dec_even1_ro_Base;
        case cAtHdlcChannelCounterTypeRxFcsErrors     : return r2c ? cAf6Reg_upencnt_dec_even6_r2c_Base : cAf6Reg_upencnt_dec_even6_ro_Base;
        case cAtHdlcChannelCounterTypeRxAddrCtrlErrors: return r2c ? cAf6Reg_upencnt_dec_even4_r2c_Base : cAf6Reg_upencnt_dec_even4_ro_Base;
        case cAtHdlcChannelCounterTypeRxAborts        : return r2c ? cAf6Reg_upencnt_dec_even5_r2c_Base : cAf6Reg_upencnt_dec_even5_ro_Base;

        default:return cBit31_0;
        }
    }

static uint32 HwCounterGet(AtChannel self, uint16 counterType, eBool r2c)
    {
    uint32 regAddr = BaseAddress() + CounterBaseAddress(self, counterType, r2c) + ChannelOffset((AtEncapChannel)self);
    return mChannelHwRead(self, regAddr, cAtModuleEncap);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (AtChannelCounterIsSupported(self, counterType))
        return HwCounterGet(self, counterType, cAtFalse);
    return 0x0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (AtChannelCounterIsSupported(self, counterType))
        return HwCounterGet(self, counterType, cAtTrue);
    return 0x0;
    }

static eAtRet AddressMonitorEnable(AtHdlcChannel self, eBool compareEnable)
    {
    AtUnused(self);
    return (compareEnable) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtRet ExpectedAddressSet(AtHdlcChannel self, uint8 *expectedAddress)
    {
    eAtRet ret;
    uint32 regVal[2];
    uint32 regAddr = cAf6Reg_upen_decf_Base + ChannelOffset((AtEncapChannel)self);
   
    if (expectedAddress == NULL)
        return cAtErrorNullPointer;

    ret = Tha60210012ModuleEncapEosLongRead(EncapModule((AtChannel)self), regAddr, regVal);
    if (ret != cAtOk)
        return ret;

    mRegFieldSet(regVal[0], cAf6_upen_decf_tb_ctrl_oct_, *expectedAddress);
    return Tha60210012ModuleEncapEosLongWrite(EncapModule((AtChannel)self), regAddr, regVal);
    }

static eBool AddressMonitorIsEnabled(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet ExpectedAddressGet(AtHdlcChannel self, uint8 *address)
    {
    eAtRet ret;
    uint32 regVal[2];
    uint32 regAddr;

    regAddr = cAf6Reg_upen_decf_Base + ChannelOffset((AtEncapChannel)self);
    ret = Tha60210012ModuleEncapEosLongRead(EncapModule((AtChannel)self), regAddr, regVal);
    if (ret != cAtOk)
        {
        *address = cInvalidUint8;
        return ret;
        }

    *address = (uint8)mRegField(regVal[0], cAf6_upen_decf_tb_ctrl_oct_);
    return cAtOk;
    }

static eAtRet AddressInsertionEnable(AtHdlcChannel self, eBool insertEnable)
    {
    AtUnused(self);
    
    if (insertEnable == cAtFalse)
        return cAtErrorModeNotSupport;

    return cAtOk;
    }

static eAtRet TxAddressSet(AtHdlcChannel self, uint8 *address)
    {
    AtUnused(self);

    if (*address != 0x04)
        return cAtErrorModeNotSupport;

    return cAtOk;
    }

static eBool AddressInsertionIsEnabled(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet TxAddressGet(AtHdlcChannel self, uint8 *address)
    {
    AtUnused(self);

    if (address)
        *address = 0x04;

    return cAtOk;
    }

static eAtRet ControlMonitorEnable(AtHdlcChannel self, eBool compareEnable)
    {
    AtUnused(self);
    return compareEnable ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtRet ExpectedControlSet(AtHdlcChannel self, uint8 *expectedControl)
    {
    eAtRet ret;
    uint32 regVal[2];
    uint8  control_Bit1_0_Val;
    uint8  control_Bit7_2_Val;
    uint32 regAddr = cAf6Reg_upen_decf_Base + ChannelOffset((AtEncapChannel)self);

    if (expectedControl == NULL)
        return cAtErrorNullPointer;

    ret = Tha60210012ModuleEncapEosLongRead(EncapModule((AtChannel)self), regAddr, regVal);
    if (ret != cAtOk)
        return ret;

    control_Bit1_0_Val = (uint8) ((*expectedControl) & cBit1_0);
    control_Bit7_2_Val = (uint8) (((*expectedControl) & cBit7_2) >> 2);
    mFieldIns(&regVal[0],
              cAf6_upen_decf_tb_addr_oct_Mask_01,
              cAf6_upen_decf_tb_addr_oct_Shift_01,
              control_Bit1_0_Val);

    mFieldIns(&regVal[1],
              cAf6_upen_decf_tb_addr_oct_Mask_02,
              cAf6_upen_decf_tb_addr_oct_Shift_02,
              control_Bit7_2_Val);

    return Tha60210012ModuleEncapEosLongWrite(EncapModule((AtChannel)self), regAddr, regVal);
    }

static eBool ControlMonitorIsEnabled(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet ExpectedControlGet(AtHdlcChannel self, uint8 *control)
    {
    eAtRet ret;
    uint32 regVal[2];
    uint8  control_Bit1_0_Val;
    uint8  control_Bit7_2_Val;
    uint32 regAddr;

    if (control == NULL)
        return cAtTrue;

    regAddr = cAf6Reg_upen_decf_Base + ChannelOffset((AtEncapChannel)self);
    ret = Tha60210012ModuleEncapEosLongRead(EncapModule((AtChannel)self), regAddr, regVal);
    if (ret != cAtOk)
        {
        *control = cInvalidUint8;
        return cAtTrue;
        }

    mFieldGet(regVal[0],
              cAf6_upen_decf_tb_addr_oct_Mask_01,
              cAf6_upen_decf_tb_addr_oct_Shift_01,
              uint8,
              &control_Bit1_0_Val);

    mFieldGet(regVal[1],
              cAf6_upen_decf_tb_addr_oct_Mask_02,
              cAf6_upen_decf_tb_addr_oct_Shift_02,
              uint8,
              &control_Bit7_2_Val);

    *control = (uint8) (control_Bit1_0_Val | (control_Bit7_2_Val << 2));
    return cAtTrue;
    }

static eAtRet ControlInsertionEnable(AtHdlcChannel self, eBool insertEnable)
    {
    AtUnused(self);
    if (insertEnable == cAtFalse)
        return cAtErrorModeNotSupport;

    return cAtOk;
    }

static eAtRet TxControlSet(AtHdlcChannel self, uint8 *control)
    {
    AtUnused(self);

    if (control == NULL)
        return cAtErrorNullPointer;

    if (*control != 0x03)
        return cAtErrorModeNotSupport;

    return cAtOk;
    }

static eAtRet TxControlGet(AtHdlcChannel self, uint8 *control)
    {
    AtUnused(self);
    if (control)
        *control = 0x03;
    return cAtTrue;
    }

static eBool ControlInsertionIsEnabled(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet AddressSizeSet(AtHdlcChannel self, uint8 numberOfAddressByte)
    {
    AtUnused(self);
    return (numberOfAddressByte == 1) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 AddressSizeGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return 1;
    }

static eAtRet ControlSizeSet(AtHdlcChannel self, uint8 numberOfByte)
    {
    AtUnused(self);
    return (numberOfByte == 1) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 ControlSizeGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return 1;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    uint32 regVal[2];
    AtModuleEncap moduleEncap = EncapModule((AtChannel)self);
    uint32 regAddr = cAf6Reg_upen_decf_Base + ChannelOffset((AtEncapChannel)self);
    eAtRet ret = Tha60210012ModuleEncapEosLongRead(moduleEncap, regAddr, regVal);
    if (ret != cAtOk)
        return ret;

    mRegFieldSet(regVal[0], cAf6_upen_decf_tb_hdlc_ena_, enable ? 1 : 0);
    return Tha60210012ModuleEncapEosLongWrite(moduleEncap, regAddr, regVal);
    }

static eBool IsEnabled(AtChannel self)
    {
    uint32 regVal[2];
    uint32 regAddr = cAf6Reg_upen_decf_Base + ChannelOffset((AtEncapChannel)self);
    eAtRet ret = Tha60210012ModuleEncapEosLongRead(EncapModule((AtChannel)self), regAddr, regVal);
    if (ret != cAtOk)
        return cAtFalse;

    return (mRegField(regVal[0], cAf6_upen_decf_tb_hdlc_ena_) != 0) ? cAtTrue : cAtFalse;
    }

static eBool CanEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static eBool ControlCanCompare(AtHdlcChannel self, eBool compareEnable)
    {
    AtUnused(self);
    return compareEnable;
    }

static eBool AddressCanCompare(AtHdlcChannel self, eBool compareEnable)
    {
    AtUnused(self);
    return compareEnable;
    }

static eAtRet QueueEnable(AtChannel self, eBool enable)
    {
    return Tha60210012EopEncapChannelQueueEnable(self, enable);
    }

static void OverrideAtChannel(AtHdlcChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, CanEnable);
        mMethodOverride(m_AtChannelOverride, QueueEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEncapChannel(AtHdlcChannel self)
    {
    AtEncapChannel encap = (AtEncapChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEncapChannelMethods = mMethodsGet(encap);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapChannelOverride, m_AtEncapChannelMethods, sizeof(m_AtEncapChannelOverride));

        mMethodOverride(m_AtEncapChannelOverride, ScrambleEnable);
        mMethodOverride(m_AtEncapChannelOverride, ScrambleIsEnabled);
        mMethodOverride(m_AtEncapChannelOverride, EncapTypeGet);
        }

    mMethodsSet(encap, &m_AtEncapChannelOverride);
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, m_AtHdlcChannelMethods, sizeof(m_AtHdlcChannelOverride));

        mMethodOverride(m_AtHdlcChannelOverride, FcsModeIsSupported);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeDefault);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeDefault);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeIsSupported);
        mMethodOverride(m_AtHdlcChannelOverride, FlagSet);
        mMethodOverride(m_AtHdlcChannelOverride, FlagGet);
        mMethodOverride(m_AtHdlcChannelOverride, FrameTypeGet);
        mMethodOverride(m_AtHdlcChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtHdlcChannelOverride, AddressCanCompare);
        mMethodOverride(m_AtHdlcChannelOverride, AddressMonitorEnable);
        mMethodOverride(m_AtHdlcChannelOverride, AddressMonitorIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, AddressInsertionEnable);
        mMethodOverride(m_AtHdlcChannelOverride, AddressInsertionIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, ControlCanCompare);
        mMethodOverride(m_AtHdlcChannelOverride, ControlMonitorEnable);
        mMethodOverride(m_AtHdlcChannelOverride, ControlMonitorIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, ControlInsertionEnable);
        mMethodOverride(m_AtHdlcChannelOverride, ControlInsertionIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, AddressSizeSet);
        mMethodOverride(m_AtHdlcChannelOverride, AddressSizeGet);
        mMethodOverride(m_AtHdlcChannelOverride, ControlSizeSet);
        mMethodOverride(m_AtHdlcChannelOverride, ControlSizeGet);
        mMethodOverride(m_AtHdlcChannelOverride, MtuIsSupported);
        mMethodOverride(m_AtHdlcChannelOverride, IdlePatternSet);
        mMethodOverride(m_AtHdlcChannelOverride, IdlePatternGet);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedAddressSet);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedAddressGet);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedControlSet);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedControlGet);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressSet);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressGet);
        mMethodOverride(m_AtHdlcChannelOverride, TxControlSet);
        mMethodOverride(m_AtHdlcChannelOverride, TxControlGet);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtChannel(self);
    OverrideAtEncapChannel(self);
    OverrideAtHdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012VcgHdlcChannel);
    }

static AtHdlcChannel ObjectInit(AtHdlcChannel self, uint32 channelId, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtHdlcChannelObjectInit(self, channelId, cAtHdlcFrmLaps, (AtModule)module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel Tha60210012VcgHdlcChannelNew(uint32 channelId, AtModuleEncap module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChannel, channelId, module);
    }

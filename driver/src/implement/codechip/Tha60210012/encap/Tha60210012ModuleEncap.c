/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60210012ModuleEncap.c
 *
 * Created Date: Sep 9, 2015
 *
 * Description : Encapsulation module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/encap/dynamic/ThaModuleDynamicEncapInternal.h"
#include "../../../default/encap/dynamic/ThaPhysicalHdlcChannel.h"
#include "../../../default/encap/dynamic/ThaEncapDynamicBinder.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/encap/dynamic/ThaDynamicHdlcChannel.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../../Tha60210011/man/Tha60210011Device.h"
#include "../pw/activator/Tha60210012PwActivator.h"
#include "../pw/Tha60210012ModulePw.h"
#include "../man/Tha60210012Device.h"
#include "../eth/Tha60210012ModuleEthFlowLookupReg.h"
#include "../ppp/Tha60210012MpBundle.h"
#include "../mpeg/Tha60210012MpegReg.h"
#include "../mpeg/Tha60210012ModuleMpeg.h"
#include "resequence/Tha60210012ResequenceReg.h"
#include "Tha60210012PhysicalHdlcChannel.h"
#include "Tha60210012ModuleDecapParserReg.h"
#include "Tha60210012PhysicalEosEncapReg.h"
#include "Tha60210012PhysicalEncapReg.h"
#include "Tha60210012VcgEncapChannel.h"
#include "Tha60210012ModuleEncapInternal.h"
#include "Tha60210012ModuleEncapAsyncInit.h"
#include "../../../default/encap/dynamic/ThaDynamicHdlcChannel.h"
#include "../mpeg/Tha60210012MpegReg.h"
#include "../mpeg/Tha60210012ModuleMpeg.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaBaseAddressHoEncapReg   0x0E30000
#define cThaBaseAddressLoEncapReg   0x0E00000

#define cThaBaseAddressHoDecapReg   0x0E34000
#define cThaBaseAddressLoDecapReg   0x0E20000

/* Bundle */
#define caf6_cfg0_qthsh_pen           0x20000

/*--------------------------- Macros -----------------------------------------*/
#define mDefaultOffset(channel) Tha60210012PhysicalHdlcChannelDefaultOffset((Tha60210012PhysicalHdlcChannel)channel)
#define mThis(self) ((Tha60210012ModuleEncap)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012ModuleEncapMethods m_methods;

/* Override */
static tAtObjectMethods              m_AtObjectOverride;
static tAtModuleMethods              m_AtModuleOverride;
static tAtModuleEncapMethods         m_AtModuleEncapOverride;
static tThaModuleDynamicEncapMethods m_ThaModuleDynamicEncapOverride;

/* Save super implementation */
static const tAtObjectMethods              *m_AtObjectMethods              = NULL;
static const tAtModuleMethods              *m_AtModuleMethods              = NULL;
static const tThaModuleDynamicEncapMethods *m_ThaModuleDynamicEncapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *CapacityDescription(AtModule self)
    {
    AtModuleEncap moduleEncap = (AtModuleEncap)self;
    static char string[64];

    AtSprintf(string, "channel: %hu, bundle: %u(ppp/fr), resequence engine: %u",
              AtModuleEncapMaxChannelsGet(moduleEncap),
              AtModuleEncapMaxBundlesGet(moduleEncap),
              AtResequenceManagerNumEngines(ThaDeviceResequenceManagerGet((ThaDevice)AtModuleDeviceGet(self))));
    return string;
    }

static uint16 MaxChannelsGet(AtModuleEncap self)
    {
    AtUnused(self);
    return 3072;
    }

static uint32 MaxBundlesGet(AtModuleEncap self)
    {
    AtUnused(self);
    return 512;
    }

static AtGfpChannel GfpChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    return (AtGfpChannel)Tha60210012VcgGfpChannelNew(channelId, self);
    }

static AtLapsLink LapsLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
    AtUnused(self);
    return Tha60210012VcgLapsLinkNew(hdlcChannel);
    }

static AtHdlcChannel HdlcLapsChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    return Tha60210012VcgHdlcChannelNew(channelId, self);
    }

static AtEncapChannel PhysicalEncapChannelCreate(ThaModuleDynamicEncap self, uint32 channelId, AtEncapChannel logicalChannel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)logicalChannel);
    AtEncapBinder binder = AtDeviceEncapBinder(device);

    if (AtEncapChannelEncapTypeGet(logicalChannel) == cAtEncapHdlc)
        {
        if (ThaEncapDynamicBinderHwIsHo((ThaEncapDynamicBinder)binder, channelId))
            return (AtEncapChannel)Tha60210012PhysicalHdlcHoChannelNew(channelId, (AtHdlcChannel)logicalChannel, (AtModuleEncap)self);

        return (AtEncapChannel)Tha60210012PhysicalHdlcLoChannelNew(channelId, (AtHdlcChannel)logicalChannel, (AtModuleEncap)self);
        }

    return m_ThaModuleDynamicEncapMethods->PhysicalEncapChannelCreate(self, channelId, logicalChannel);
    }

static uint32 LoSliceOffset(uint32 slice)
    {
    return (uint32)(0x4000 * slice);
    }

static uint32 HoSliceOffset(uint32 slice)
    {
    return (uint32)(0x1000 * slice);
    }

static uint32 NumSlice48(AtModule self)
    {
    return Tha60210011DeviceNumUsedOc48Slices((Tha60210011Device)AtModuleDeviceGet(self));
    }

static uint32 NumSlice24(AtModule self)
    {
    return NumSlice48(self) * 2;
    }

static Tha60210011ModulePw PwModule(Tha60210012ModuleEncap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (Tha60210011ModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static uint32 LoDecapGlbReg(Tha60210012ModuleEncap self)
    {
    if (Tha60210012ModuleEncapRegistersNeedShiftUp(self))
        return 0x2804;

    return cAf6Reg_upen_hdlc_loglbcfg_Base;
    }

static eAtRet LoActivate(AtModule self, eBool activate)
    {
    uint8 slice;

    for (slice = 0; slice < NumSlice24(self); slice++)
        {
        uint32 regAddr, regVal, offset;

        /* Encap */
        offset = LoSliceOffset(slice) + Tha60210012ModuleEncapLoBaseAddress();
        mModuleHwWrite(self, cAf6Reg_pencfg_lshift8_Base + offset, mBoolToBin(activate));

        /* Decap */
        offset = LoSliceOffset(slice) + Tha60210012ModuleDecapLoBaseAddress();
        regAddr = LoDecapGlbReg(mThis(self)) + offset;
        regVal = mModuleHwRead(self, regAddr);
        mRegFieldSet(regVal, cAf6_upen_hdlc_loglbcfg_cfg_upactive_, mBoolToBin(activate));
        
        /* As hardware recommend default always run with calculate LSB mode */
        mRegFieldSet(regVal, cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_, 1);
        mModuleHwWrite(self, regAddr, regVal);
        }

    return cAtOk;
    }

static eAtRet HoActivate(AtModule self, eBool activate)
    {
    uint8 slice;

    for (slice = 0; slice < NumSlice48(self); slice++)
        {
        uint32 offset = HoSliceOffset(slice) + Tha60210012ModuleDecapHoBaseAddress();
        uint32 regAdd = cAf6Reg_upen_hdlc_hoglbcfg_Base + offset;
        uint32 regVal = mModuleHwRead(self, regAdd);
        mRegFieldSet(regVal, cAf6_upen_hdlc_hoglbcfg_cfg_upactive_, mBoolToBin(activate));
        mModuleHwWrite(self, regAdd, regVal);
        }

    return cAtOk;
    }

static eAtRet Activate(AtModule self, eBool activate)
    {
    eAtRet ret = cAtOk;

    ret |= HoActivate(self, activate);
    ret |= LoActivate(self, activate);

    return ret;
    }

static eAtRet EosInit(AtModule self)
    {
    uint32 regAddr = Tha60210012ModuleEncapEosBaseAddress() + cAf6Reg_upen_egrext_Base;
    mModuleHwWrite(self, regAddr, 0);
    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret;

    /* Let super deal with database */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= Activate(self, cAtTrue);
    ret |= AtModuleDebugLoopbackSet(self, cAtLoopbackModeRelease);
    ret |= EosInit(self);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Tha60210012ModuleEncapAsyncInit((AtObject)self);
    }

static void HoLoopbackSet(Tha60210012ModuleEncap self, uint8 hwLoopbackMode, uint8 slice)
    {
    uint32 offset = HoSliceOffset(slice) + Tha60210012ModuleDecapHoBaseAddress();
    uint32 regAddress = cAf6Reg_upen_hdlc_hoglbcfg_Base + offset;
    uint32 regValue = mModuleHwRead(self, regAddress);

    mRegFieldSet(regValue, cAf6_upen_hdlc_hoglbcfg_cfg_loop_, hwLoopbackMode);
    mModuleHwWrite(self, regAddress, regValue);
    }

static void LoLoopbackSet(Tha60210012ModuleEncap self, uint8 hwLoopbackMode, uint8 slice)
    {
    uint32 offset = LoSliceOffset(slice) + Tha60210012ModuleDecapLoBaseAddress();
    uint32 regAddress = LoDecapGlbReg(self) + offset;
    uint32 regValue = mModuleHwRead(self, regAddress);

    mRegFieldSet(regValue, cAf6_upen_hdlc_loglbcfg_cfg_loop_, hwLoopbackMode);
    mModuleHwWrite(self, regAddress, regValue);
    }

static uint8 HoLoopbackGet(Tha60210012ModuleEncap self, uint8 slice)
    {
    uint32 offset = HoSliceOffset(slice) + Tha60210012ModuleDecapHoBaseAddress();
    uint32 regAddress = cAf6Reg_upen_hdlc_hoglbcfg_Base + offset;
    uint32 regValue = mModuleHwRead(self, regAddress);
    return (uint8)mRegField(regValue, cAf6_upen_hdlc_hoglbcfg_cfg_loop_);
    }

static eAtRet LoopbackSet(AtModule self, uint8 loopbackMode)
    {
    uint8 hwLoopbackMode;
    uint8 slice_i;

    if (!mMethodsGet(self)->LoopbackIsSupported(self, loopbackMode))
        return cAtErrorModeNotSupport;

    hwLoopbackMode = (loopbackMode == cAtLoopbackModeLocal) ? 1 : 0;

    for (slice_i = 0; slice_i < NumSlice48(self); slice_i++)
        HoLoopbackSet(mThis(self), hwLoopbackMode, slice_i);

    for (slice_i = 0; slice_i < NumSlice24(self); slice_i++)
        LoLoopbackSet(mThis(self), hwLoopbackMode, slice_i);

    return cAtOk;
    }

static uint8 LoopbackGet(AtModule self)
    {
    return HoLoopbackGet((Tha60210012ModuleEncap)self, 0) ? cAtLoopbackModeLocal : cAtLoopbackModeRelease;
    }

static eBool LoopbackIsSupported(AtModule self, uint8 loopbackMode)
    {
    AtUnused(self);

    /* Only local loopback and release can be supported */
    if (loopbackMode == cAtLoopbackModeRemote)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 EncapTypeSw2Hw(eAtEncapType swMode)
    {
    switch ((uint32)swMode)
        {
        case cAtEncapGfp:  return cHwEncapGfpMode;
        case cAtEncapHdlc: return cHwEncapHdlcMode;
        default: return 0;
        }
    }

static eAtEncapType EncapTypeHw2Sw(uint32 hwMode)
    {
    switch ((uint32)hwMode)
        {
        case cHwEncapGfpMode:  return cAtEncapGfp;
        case cHwEncapHdlcMode: return cAtEncapHdlc;
        default: return cAtEncapUnknown;
        }
    }

static eAtRet ChannelEncapModeSet(AtModuleEncap self, uint32 channelId, eAtEncapType encapType)
    {
    uint32 regAddr = Tha60210012ModuleEncapEosBaseAddress() + cAf6Reg_upen_mapf_Base + channelId;
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_mapf_ENCMODE_, EncapTypeSw2Hw(encapType));
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 ChannelEncapModeGet(AtModuleEncap self, uint32 channelId)
    {
    uint32 regAddr = Tha60210012ModuleEncapEosBaseAddress() + cAf6Reg_upen_mapf_Base + channelId;
    uint32 regVal = mModuleHwRead(self, regAddr);
    return EncapTypeHw2Sw(mRegField(regVal, cAf6_upen_mapf_ENCMODE_));
    }

static eAtRet ChannelThresholdSet(AtModuleEncap self, uint32 channelId, uint32 thres)
    {
    uint32 regAddr = Tha60210012ModuleEncapEosBaseAddress() + cAf6Reg_upen_mapf_Base + channelId;
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_mapf_FBIT_THRESHOLD_H_, thres);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet ChannelEncapRegisterReset(AtModuleEncap self, uint32 channelId)
    {
    uint32 regVal[2];
    uint32 regAddr = Tha60210012ModuleEncapEosBaseAddress() + cAf6Reg_upen_mapf_Base + channelId;
    
    mModuleHwWrite(self, regAddr, 0x0);
    regAddr = cAf6Reg_upen_decf_Base + channelId;
    regVal[0] = 0;
    regVal[1] = 0;
    return Tha60210012ModuleEncapEosLongWrite(self, regAddr, regVal);
    }

static eAtRet HdlcTdmHeaderRemove(AtModuleEncap self, uint32 channelId)
    {
    uint32 regVal[2];
    uint32 regAddr = cAf6Reg_upen_decf_Base + channelId;
    eAtRet ret = Tha60210012ModuleEncapEosLongRead(self, regAddr, regVal);
    if (ret != cAtOk)
        return ret;

    mRegFieldSet(regVal[0], cAf6_upen_decf_tb_thec_correct_, 1);
    return Tha60210012ModuleEncapEosLongWrite(self, regAddr, regVal);
    }

static ThaProfileManager ProfileManagerObjectCreate(ThaModuleDynamicEncap self)
    {
    return Tha60210012ProfileManagerNew((AtModuleEncap)self);
    }

static uint32 StartVersionSupportProfileManager(Tha60210012ModuleEncap self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x0, 0x0);
    }

static eBool ProfileManagerIsSupported(ThaModuleDynamicEncap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(device));

    if (version >= mMethodsGet(mThis(self))->StartVersionSupportProfileManager(mThis(self)))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 StartVersionNeedSelectStsNc(Tha60210012ModuleEncap self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x0, 0x0);
    }

static eAtRet HdlcLinkOamCircuitVlanInsert(AtHdlcLink self, uint16 vlanId, eBool enable)
    {
    uint32 offset = cThaModuleEthFlowLookupBaseAddress + AtChannelIdGet((AtChannel)self);
    uint32 regAddress = cAf6Reg_pppdat_ethflow_table_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_linkdat_ethflow_table_CtrlVlan_Enable_, mBoolToBin(enable));
    mRegFieldSet(regValue, cAf6_linkdat_ethflow_table_CtrlVlan_ID_, vlanId);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

static eAtRet HdlcBundleOamCircuitVlanInsert(AtHdlcBundle self, uint16 vlanId, eBool enable)
    {
    uint32 offset = cThaModuleEthFlowLookupBaseAddress + AtChannelIdGet((AtChannel)self);
    uint32 regAddress = cAf6Reg_bundledat_ethflow_table_Base + offset;
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_bundledat_ethflow_table_CtrlVlan_Enable_, mBoolToBin(enable));
    mRegFieldSet(regValue, cAf6_bundledat_ethflow_table_CtrlVlan_ID_, vlanId);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsEncapRegister(AtModuleDeviceGet(self), address);
	}

static void EncapRegShow(AtEncapChannel channel, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay((AtChannel)channel, address, cAtModuleEncap);
    }

static uint32 LinkLookupOffset(AtEncapChannel physicalChannel, eBool circuitIsHo)
    {
    uint32 baseAddress = cThaModuleEthFlowLookupBaseAddress;
    uint8 sliceId = ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)physicalChannel);

    return (baseAddress + AtChannelHwIdGet((AtChannel)physicalChannel) + (uint32)(sliceId * (circuitIsHo ? 64 : 1024)));
    }

static void EncapChannelRegsShow(AtModuleEncap self, AtEncapChannel encapChannel)
    {
    uint32 linkId, offset, encapOffset;
    AtHdlcLink hdlcLink;
    AtHdlcChannel physicalChannel;
    AtUnused(self);

    if (AtEncapChannelEncapTypeGet(encapChannel) != cAtEncapHdlc)
        {
        AtPrintc(cSevCritical, "ERROR: Not HDLC encapsulation\r\n");
        return;
        }

    hdlcLink = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)encapChannel);
    linkId = AtChannelIdGet((AtChannel)hdlcLink);
    offset = cThaModuleEthFlowLookupBaseAddress + linkId;

    EncapRegShow(encapChannel, "Link to Ethernet Flow LookUp Table", cAf6Reg_pppdat_ethflow_table_Base + offset);
    EncapRegShow(encapChannel, "Bundle LookUp", cAf6Reg_bundle_lup_table_Base + offset);

    physicalChannel = ThaHdlcChannelPhysicalChannelGet((AtHdlcChannel)encapChannel);
    offset = Tha60210012PhysicalHdlcChannelDecapBaseAddress((Tha60210012PhysicalHdlcChannel)physicalChannel) + mDefaultOffset(physicalChannel);
    encapOffset = Tha60210012PhysicalHdlcChannelEncapBaseAddress((Tha60210012PhysicalHdlcChannel)physicalChannel) + mDefaultOffset(physicalChannel);

    if (ThaHdlcChannelCircuitIsHo((ThaHdlcChannel)encapChannel))
        {
        EncapRegShow(encapChannel, "CONFIG HDLC HO DEC", cAf6Reg_upen_hdlc_Hocfg_Base + offset);
        EncapRegShow(encapChannel, "CONFIG HDLC HO DEC", cAf6Reg_upen_hdlc_hoglbcfg_Base + offset);
        EncapRegShow(encapChannel, "Channel id config for parser", cAf6Reg_upen_cidcfg_HoCfg_Base + offset);
        EncapRegShow(encapChannel, "Ho-order Link LookUp", cAf6Reg_holink_lup_table_Base + LinkLookupOffset(encapChannel, cAtTrue));
        EncapRegShow(encapChannel, "Ho-order encap channel control", cAf6Reg_upenctr1_Ho_Base + encapOffset);
        }
    else
        {
        EncapRegShow(encapChannel, "CONFIG HDLC LO DEC", cAf6Reg_upen_hdlc_locfg_Base + offset);
        EncapRegShow(encapChannel, "CONFIG HDLC LO DEC", cAf6Reg_upen_hdlc_loglbcfg_Base + offset);
        EncapRegShow(encapChannel, "Channel id config for parser", cAf6Reg_upen_cidcfg_locfg_Base + offset);
        EncapRegShow(encapChannel, "Lo-order LinkLookUp", cAf6Reg_lolink_lup_table_Base + LinkLookupOffset(encapChannel, cAtFalse));
        EncapRegShow(encapChannel, "Lo-order encap channel control", cAf6Reg_upenctr1_Lo_Base + encapOffset);
        }
    }

static AtMpBundle MpBundleObjectCreate(AtModuleEncap self, uint32 bundleId)
    {
    return Tha60210012MpBundleNew(bundleId, (AtModule)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210012ModuleEncap object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(counterModule);
    mEncodeUInt(asyncInitState);
    }

static eBool StsNcNeedSelect(Tha60210012ModuleEncap self, AtSdhChannel sdhChannel)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    AtUnused(sdhChannel);

    return (hwVersion >= StartVersionNeedSelectStsNc(mThis(self))) ? cAtTrue : cAtFalse;
    }

static eAtRet Debug(AtModule self)
    {
    m_AtModuleMethods->Debug(self);
    AtResequenceManagerDebug(ThaDeviceResequenceManagerGet((ThaDevice)AtModuleDeviceGet(self)));
    return cAtOk;
    }

static void HdlcBundleRegsShow(AtModuleEncap self, AtHdlcBundle bundle)
    {
    uint32 addr = cAf6Reg_bundledat_ethflow_table_Base + cThaModuleEthFlowLookupBaseAddress + AtChannelIdGet((AtChannel)bundle);
    uint32 regVal = mModuleHwRead(self, addr);
    
    Tha6021DebugPrintRegName(NULL, "Bundle to Ethernet Flow LookUp Table", addr, regVal);
    }

static uint32 StartVersionSupportResequenceManager(Tha60210012ModuleEncap self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x0, 0x0);
    }

static void MethodsInit(Tha60210012ModuleEncap self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StsNcNeedSelect);
        mMethodOverride(m_methods, StartVersionSupportProfileManager);
        mMethodOverride(m_methods, StartVersionSupportResequenceManager);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtModuleEncap self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleEncap self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, LoopbackSet);
        mMethodOverride(m_AtModuleOverride, LoopbackGet);
        mMethodOverride(m_AtModuleOverride, LoopbackIsSupported);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideThaModuleDynamicEncap(AtModuleEncap self)
    {
    ThaModuleDynamicEncap encapModule = (ThaModuleDynamicEncap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleDynamicEncapMethods = mMethodsGet(encapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDynamicEncapOverride, m_ThaModuleDynamicEncapMethods, sizeof(m_ThaModuleDynamicEncapOverride));

        mMethodOverride(m_ThaModuleDynamicEncapOverride, PhysicalEncapChannelCreate);
        mMethodOverride(m_ThaModuleDynamicEncapOverride, ProfileManagerObjectCreate);
        mMethodOverride(m_ThaModuleDynamicEncapOverride, ProfileManagerIsSupported);
        }

    mMethodsSet(encapModule, &m_ThaModuleDynamicEncapOverride);
    }

static void OverrideAtModuleEncap(AtModuleEncap self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEncapOverride, mMethodsGet(self), sizeof(m_AtModuleEncapOverride));

        mMethodOverride(m_AtModuleEncapOverride, MaxChannelsGet);
        mMethodOverride(m_AtModuleEncapOverride, GfpChannelObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, HdlcLapsChannelObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, LapsLinkObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, MpBundleObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, MaxBundlesGet);
        mMethodOverride(m_AtModuleEncapOverride, EncapChannelRegsShow);
        mMethodOverride(m_AtModuleEncapOverride, HdlcBundleRegsShow);
        }

    mMethodsSet(self, &m_AtModuleEncapOverride);
    }

static void Override(AtModuleEncap self)
    {
    OverrideAtObject(self);
    OverrideAtModuleEncap(self);
    OverrideThaModuleDynamicEncap(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleEncap);
    }

AtModuleEncap Tha60210012ModuleEncapObjectInit(AtModuleEncap self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleDynamicEncapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;
    (mThis(self))->counterModule = cAtModuleEncap;

    return self;
    }

AtModuleEncap Tha60210012ModuleEncapNew(AtDevice device)
    {
    /* Allocate memory for this module */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEncap newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Constructor */
    return Tha60210012ModuleEncapObjectInit(newModule, device);
    }

eAtRet Tha60210012ModuleEncapHdlcChannelFlowConnect(Tha60210012ModuleEncap self, AtPw pw, AtChannel circuit)
    {
    uint32 offset, regAddress, regValue;
    uint32 hwFlowId = Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pw);

    AtUnused(self);

    /* PPP data packet lookup */
    offset = cThaModuleEthFlowLookupBaseAddress + AtChannelHwIdGet(circuit); /* Get logic channel Id (0-3071) */
    regAddress = cAf6Reg_pppdat_ethflow_table_Base + offset;
    regValue = mChannelHwRead(pw, regAddress, cAtModuleEncap);
    mRegFieldSet(regValue, cAf6_pppdat_ethflow_table_PPP_Data_Enable_, 1);/* Enable lookup */
    mRegFieldSet(regValue, cAf6_pppdat_ethflow_table_PPP_Data_EthFlowID_, hwFlowId);
    mChannelHwWrite(pw, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

uint32 Tha60210012ModuleEncapHoBaseAddress(void)
    {
    return cThaBaseAddressHoEncapReg;
    }

uint32 Tha60210012ModuleEncapLoBaseAddress(void)
    {
    return cThaBaseAddressLoEncapReg;
    }

uint32 Tha60210012ModuleDecapHoBaseAddress(void)
    {
    return cThaBaseAddressHoDecapReg;
    }

uint32 Tha60210012ModuleDecapLoBaseAddress(void)
    {
    return cThaBaseAddressLoDecapReg;
    }

eAtRet Tha60210012ModuleEncapHdlcLinkFlowLookupEnable(Tha60210012ModuleEncap self, AtHdlcLink link, eBool enable)
    {
    uint32 offset, regAddress, regValue;
    AtUnused(self);

    offset = cThaModuleEthFlowLookupBaseAddress + AtChannelIdGet((AtChannel)link);
    regAddress = cAf6Reg_pppdat_ethflow_table_Base + offset;
    regValue = mChannelHwRead(link, regAddress, cAtModuleEncap);
    mRegFieldSet(regValue, cAf6_pppdat_ethflow_table_PPP_Data_Enable_, mBoolToBin(enable));/* Enable lookup */
    mChannelHwWrite(link, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

eBool Tha60210012ModuleEncapHdlcLinkFlowLookupIsEnabled(Tha60210012ModuleEncap self, AtHdlcLink link)
    {
    uint32 offset, regAddress, regValue;
    AtUnused(self);

    offset = cThaModuleEthFlowLookupBaseAddress + AtChannelIdGet((AtChannel)link);
    regAddress = cAf6Reg_pppdat_ethflow_table_Base + offset;
    regValue = mChannelHwRead(link, regAddress, cAtModuleEncap);

    return mRegField(regValue, cAf6_pppdat_ethflow_table_PPP_Data_Enable_) ? cAtTrue : cAtFalse;
    }

eAtRet Tha60210012ModuleEncapHdlcLinkHwFlowSet(Tha60210012ModuleEncap self, AtHdlcLink link, AtEthFlow flow)
    {
    uint32 offset, regAddress, regValue;
    AtUnused(self);

    offset = cThaModuleEthFlowLookupBaseAddress + AtChannelIdGet((AtChannel)link);
    regAddress = cAf6Reg_pppdat_ethflow_table_Base + offset;
    regValue = mChannelHwRead(link, regAddress, cAtModuleEncap);
    mRegFieldSet(regValue, cAf6_pppdat_ethflow_table_PPP_Data_EthFlowID_, AtChannelHwIdGet((AtChannel)flow));
    mChannelHwWrite(link, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEncapHdlcLinkHwFlowReset(Tha60210012ModuleEncap self, AtHdlcLink link)
    {
    const uint16 cFlowIdReset = cBit11_0;
    uint32 offset, regAddress, regValue;

    AtUnused(self);
    offset = cThaModuleEthFlowLookupBaseAddress + AtChannelIdGet((AtChannel)link);
    regAddress = cAf6Reg_pppdat_ethflow_table_Base + offset;
    regValue = mChannelHwRead(link, regAddress, cAtModuleEncap);
    mRegFieldSet(regValue, cAf6_pppdat_ethflow_table_PPP_Data_EthFlowID_, cFlowIdReset);
    mRegFieldSet(regValue, cAf6_pppdat_ethflow_table_PPP_Data_Enable_, 0); /* Disable */
    mChannelHwWrite(link, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEncapMpBundleFlowLookupEnable(Tha60210012ModuleEncap self, AtMpBundle bundle, eBool enable)
    {
    uint32 offset, regAddress, regValue;
    AtUnused(self);

    offset = cThaModuleEthFlowLookupBaseAddress + AtChannelIdGet((AtChannel)bundle);
    regAddress = cAf6Reg_bundledat_ethflow_table_Base + offset;
    regValue = mChannelHwRead(bundle, regAddress, cAtModuleEncap);
    mRegFieldSet(regValue, cAf6_bundledat_ethflow_table_Bundle_Data_Enable_, mBoolToBin(enable));/* Enable lookup */
    mChannelHwWrite(bundle, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

eBool Tha60210012ModuleEncapMpBundleFlowLookupIsEnabled(Tha60210012ModuleEncap self, AtMpBundle bundle)
    {
    uint32 offset, regAddress, regValue;
    AtUnused(self);

    offset = cThaModuleEthFlowLookupBaseAddress + AtChannelIdGet((AtChannel)bundle);
    regAddress = cAf6Reg_bundledat_ethflow_table_Base + offset;
    regValue = mChannelHwRead(bundle, regAddress, cAtModuleEncap);

    return mRegField(regValue, cAf6_bundledat_ethflow_table_Bundle_Data_Enable_) ? cAtTrue : cAtFalse;
    }

eAtRet Tha60210012ModuleEncapFlowMpBundleSet(Tha60210012ModuleEncap self, AtMpBundle bundle, AtEthFlow flow)
    {
    uint32 offset, regAddress, regValue;
    uint32 hwFlowId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
    AtUnused(self);

    offset = cThaModuleEthFlowLookupBaseAddress + AtChannelIdGet((AtChannel)bundle);
    regAddress = cAf6Reg_bundledat_ethflow_table_Base + offset;
    regValue = mChannelHwRead(bundle, regAddress, cAtModuleEncap);
    mRegFieldSet(regValue, cAf6_bundledat_ethflow_table_Bundle_Data_EthFlowID_, hwFlowId);
    mChannelHwWrite(bundle, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEncapMpBundleFlowReset(Tha60210012ModuleEncap self, AtMpBundle bundle)
    {
    const uint16 cFlowIdReset = cBit11_0;
    uint32 offset, regAddress, regValue;
    AtUnused(self);

    offset = cThaModuleEthFlowLookupBaseAddress + AtChannelIdGet((AtChannel)bundle);
    regAddress = cAf6Reg_bundledat_ethflow_table_Base + offset;
    regValue = mChannelHwRead(bundle, regAddress, cAtModuleEncap);
    mRegFieldSet(regValue, cAf6_bundledat_ethflow_table_Bundle_Data_EthFlowID_, cFlowIdReset);
    mRegFieldSet(regValue, cAf6_bundledat_ethflow_table_Bundle_Data_Enable_, 0); /* Disable */
    mChannelHwWrite(bundle, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEncapLinkBundleSet(AtHdlcLink link, AtHdlcBundle bundle)
    {
    uint32 channelId  = AtChannelHwIdGet((AtChannel)link);
    uint32 offset     = cThaModuleEthFlowLookupBaseAddress + channelId;
    uint32 regAddress = cAf6Reg_bundle_lup_table_Base + offset;
    uint32 regValue   = mChannelHwRead(link, regAddress, cAtModuleEncap);

    mRegFieldSet(regValue, cAf6_bundle_lup_table_HDLC_BundleID_, AtChannelIdGet((AtChannel)bundle));
    mChannelHwWrite(link, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEncapLinkBundleReSet(AtHdlcLink link)
    {
    uint32 offset, regAddress, regValue;
    uint32 channelId = AtChannelHwIdGet((AtChannel)link);
    static const uint16 cResetChannelValue = 0x1FF;

    offset = cThaModuleEthFlowLookupBaseAddress + channelId;
    regAddress = cAf6Reg_bundle_lup_table_Base + offset;
    regValue = mChannelHwRead(link, regAddress, cAtModuleEncap);
    mRegFieldSet(regValue, cAf6_bundle_lup_table_HDLC_BundleID_, cResetChannelValue);
    mChannelHwWrite(link, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEncapLinkBundleLookupEnable(AtHdlcLink link, eBool enable)
    {
    uint32 offset, regAddress, regValue;
    uint32 channelId = AtChannelHwIdGet((AtChannel)link);

    offset = cThaModuleEthFlowLookupBaseAddress + channelId;
    regAddress = cAf6Reg_bundle_lup_table_Base + offset;
    regValue = mChannelHwRead(link, regAddress, cAtModuleEncap);
    mRegFieldSet(regValue, cAf6_bundle_lup_table_HDLC_BundleVld_, mBoolToBin(enable)); /* Enable */
    mChannelHwWrite(link, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

uint32 Tha60210012ModuleEncapEosBaseAddress(void)
    {
    return 0x0A40000;
    }

eAtRet Tha60210012ModuleEncapChannelRegisterReset(AtModuleEncap self, uint32 channelId)
    {
    if (self)
        return ChannelEncapRegisterReset(self, channelId);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ModuleEncapChannelEncapModeSet(AtModuleEncap self, uint32 channelId, eAtEncapType encapType)
    {
    if (self)
        return ChannelEncapModeSet(self, channelId, encapType);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ModuleEncapChannelThresholdSet(AtModuleEncap self, uint32 channelId, uint32 thres)
    {
    if (self)
        return ChannelThresholdSet(self, channelId, thres);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ModuleHdlcTdmHeaderRemove(AtModuleEncap self, uint32 channelId)
    {
    if (self)
        return HdlcTdmHeaderRemove(self, channelId);
    return cAtErrorObjectNotExist;
    }

uint32 Tha60210012ModuleEncapChannelEncapModeGet(AtModuleEncap self, uint32 channelId)
    {
    if (self)
        return ChannelEncapModeGet(self, channelId);
    return cInvalidUint32;
    }

eAtRet Tha60210012ModuleEncapSts3cEnable(AtModuleEncap self, AtSdhChannel vc, eBool enable)
    {
    uint32 regAddress, regValue;
    uint8 hwSts, hwSlice;

    if (vc == NULL)
        return cAtOk;

    ThaSdhChannel2HwMasterStsId(vc, cAtModuleEncap, &hwSlice, &hwSts);
    regAddress = cAf6Reg_pencfg_sts3c_Base + Tha60210012ModuleEncapHoBaseAddress();
    regValue = mModuleHwRead(self, regAddress);

    mFieldIns(&regValue,
              cAf6_pencfg_sts3c_sts3c_en_Mask(hwSts),
              cAf6_pencfg_sts3c_sts3c_en_Shift(hwSts),
              enable ? 1 : 0);

    mModuleHwWrite(self, regAddress, regValue);

    return cAtOk;
    }

eBool Tha60210012ModuleEncapStsNcNeedSelect(AtModuleEncap self, AtSdhChannel sdhChannel)
    {
    if (self)
        return mMethodsGet(mThis(self))->StsNcNeedSelect(mThis(self), sdhChannel);

    return cAtFalse;
    }

eAtRet Tha60210012ModuleEncapHdlcLinkOamCircuitVlanInsert(AtHdlcLink self, uint16 vlanId, eBool enable)
    {
    if (self)
        return HdlcLinkOamCircuitVlanInsert(self, vlanId, enable);
    return cAtErrorNullPointer;
    }

eAtRet Tha60210012ModuleEncapHdlcBundleOamCircuitVlanInsert(AtHdlcBundle self, uint16 vlanId, eBool enable)
    {
    if (self)
        return HdlcBundleOamCircuitVlanInsert(self, vlanId, enable);
    return cAtErrorNullPointer;
    }

eAtRet Tha60210012ModuleEncapStsNcBoundHwChannelTypeSet(AtSdhChannel auVc, uint32 startOffset, uint8 hwChannelType)
    {
    uint32 regAddr, regValue, offset;
    uint8 hwSlice, hwSts;
    uint8 numSts   = AtSdhChannelNumSts(auVc);
    uint8 startSts = AtSdhChannelSts1Get(auVc);
    uint8 sts_i, hwMasterStsId;
    static const uint8 cNumSwStsInVc4_4C = 12;

    if (auVc == NULL)
        return cAtErrorNullPointer;

    ThaSdhChannel2HwMasterStsId(auVc, cAtModuleEncap, &hwSlice, &hwMasterStsId);
    if (AtSdhChannelTypeGet(auVc) == cAtSdhChannelTypeVc4_4c)
        hwMasterStsId = AtSdhChannelSts1Get(auVc) / cNumSwStsInVc4_4C;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        ThaSdhChannelHwStsGet(auVc, cAtModuleEncap, (uint8)(startSts + sts_i), &hwSlice, &hwSts);

        offset   = startOffset + hwSts;
        regAddr  = cAf6Reg_upen_map_sts_set_Base + offset;
        regValue = mChannelHwRead(auVc, regAddr, cAtModuleEncap);
        mRegFieldSet(regValue, cAf6_upen_mapf_encicfg_, hwChannelType);
        mRegFieldSet(regValue, cAf6_upen_mapf_encivcx_, hwMasterStsId);
        mChannelHwWrite(auVc, regAddr, regValue, cAtModuleEncap);
        }

    return cAtOk;
    }

eAtRet ThaModuleEncapDebugCountersModuleSet(ThaModuleEncap self, uint32 module)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if ((module == cAtModuleEncap) || (module == cAtModuleSur))
        {
        mThis(self)->counterModule = module;
        return cAtOk;
        }

    return cAtErrorModeNotSupport;
    }

uint32 ThaModuleEncapDebugCountersModuleGet(ThaModuleEncap self)
    {
    return self ? mThis(self)->counterModule : cAtModuleStart;
    }

eBool ThaModuleEncapShouldReadCountersFromSurModule(ThaModuleEncap self)
    {
    return ((ThaModuleEncapDebugCountersModuleGet(self) == cAtModuleSur) ? cAtTrue : cAtFalse);
    }

eAtRet Tha60210012ModuleEncapEosLongRead(AtModuleEncap self, uint32 address, uint32 *regValue)
    {
    uint32 baseAddress;
    if ((self == NULL) || (regValue == NULL))
        return cAtErrorNullPointer;

    baseAddress = Tha60210012ModuleEncapEosBaseAddress();
    regValue[0] = mModuleHwRead(self, baseAddress + address);
    regValue[1] = mModuleHwRead(self, baseAddress + 0x0);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEncapEosLongWrite(AtModuleEncap self, uint32 address, uint32 *regValue)
    {
    uint32 baseAddress;
    if ((self == NULL) || (regValue == NULL))
        return cAtErrorNullPointer;

    baseAddress = Tha60210012ModuleEncapEosBaseAddress();
    mModuleHwWrite(self, baseAddress + 0x0, regValue[1]);
    mModuleHwWrite(self, baseAddress + address, regValue[0]);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEncapSuperAsyncInit(AtObject self)
    {
    return m_AtModuleMethods->AsyncInit((AtModule)self);
    }

eAtRet Tha60210012ModuleEncapActivate(AtObject self)
    {
    return Activate((AtModule)self, cAtTrue);
    }

eAtRet Tha60210012ModuleEncapDebugLoopbackSet(AtObject self)
    {
    return AtModuleDebugLoopbackSet((AtModule)self, cAtLoopbackModeRelease);
    }

eAtRet Th60210012ModuleEncapEosInit(AtObject self)
    {
    return EosInit((AtModule)self);
    }

void Tha60210012ModuleEncapEosChannelRegisterShow(AtModuleEncap self, uint32 channelId)
    {
    uint32 regValue[2];
    uint32 regAddress = Tha60210012ModuleEncapEosBaseAddress() + cAf6Reg_upen_mapf_Base;
    AtDebugger debugger = NULL;
    AtOsalMemInit(regValue, 0, sizeof(regValue));

    regValue[0] = mModuleHwRead(self, regAddress);
    Tha6021DebugPrintRegName (debugger, "* ENCAP Register", regAddress, regValue[0]);
    Tha6021DebugPrintBitFieldVal(debugger, " First SAPI Field are Used in HDLC               ", regValue[0], cAf6_upen_mapf_TxSAPI0_Mask, cAf6_upen_mapf_TxSAPI0_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " Second SAPI Field are Used in HDLC              ", regValue[0], cAf6_upen_mapf_TxSAPI1_Mask, cAf6_upen_mapf_TxSAPI1_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " GFP-F FCS Payload Enable                        ", regValue[0], cAf6_upen_mapf_pgfcsena_Mask, cAf6_upen_mapf_pgfcsena_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " HDLC The Number of Byte Inter Packet Flag       ", regValue[0], cAf6_upen_mapf_HDLC_INTER_FLAG_Mask, cAf6_upen_mapf_HDLC_INTER_FLAG_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " HDLC or GFP-F Scramble Enable                   ", regValue[0], cAf6_upen_mapf_HDLCSCR_Mask, cAf6_upen_mapf_HDLCSCR_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " ENCMODE: 0 is Bypass, 1 is HDLC, 3 is GFP       ", regValue[0], cAf6_upen_mapf_ENCMODE_Mask, cAf6_upen_mapf_ENCMODE_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " Number of Block of 32 Bytes are Provided        ", regValue[0], cAf6_upen_mapf_FBIT_THRESHOLD_H_Mask, cAf6_upen_mapf_FBIT_THRESHOLD_H_Shift);

    Tha60210012ModuleEncapEosLongRead(self, cAf6Reg_upen_decf_Base + channelId, regValue);
    Tha6021DebugPrintRegName (debugger, "* DECAP Register Bit31_0", regAddress, regValue[0]);
    Tha6021DebugPrintRegName (debugger, "* DECAP Register Bit63_32", regAddress, regValue[1]);
    Tha6021DebugPrintBitFieldVal(debugger, " SAPI Data Frame Mismatch Drop Disable           ", regValue[1], cAf6_upen_decf_tb_drop_dismisf_Mask, cAf6_upen_decf_tb_drop_dismisf_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " UPI Mismatch Drop Disable are Used in GFP-F     ", regValue[1], cAf6_upen_decf_tb_drop_dismisf_Mask, cAf6_upen_decf_tb_drop_dismisf_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " Expected Address Data Link are Used in HDLC[7:2]", regValue[1], cAf6_upen_decf_tb_addr_oct_Mask_02, cAf6_upen_decf_tb_addr_oct_Shift_02);
    Tha6021DebugPrintBitFieldVal(debugger, " Expected Address Data Link are Used in HDLC[1:0]", regValue[0], cAf6_upen_decf_tb_addr_oct_Mask_01, cAf6_upen_decf_tb_addr_oct_Shift_01);
    Tha6021DebugPrintBitFieldVal(debugger, " Expected Control Data Link are Used in HDLC     ", regValue[0], cAf6_upen_decf_tb_ctrl_oct_Mask, cAf6_upen_decf_tb_ctrl_oct_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " Expected SAPI Data Link are Used in HDLC        ", regValue[0], cAf6_upen_decf_tb_sapi_oct_Mask, cAf6_upen_decf_tb_sapi_oct_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " SAPI Data Link are Used in HDLC                 ", regValue[0], cAf6_upen_decf_tb_sapi_oct_Mask, cAf6_upen_decf_tb_sapi_oct_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " HDLC Byte Stuffing Mode Enable                  ", regValue[0], cAf6_upen_decf_tb_hdlc_ena_Mask, cAf6_upen_decf_tb_hdlc_ena_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " Disable Drop Packet When CRC Payload Error      ", regValue[0], cAf6_upen_decf_tb_drop_discrcf_Mask, cAf6_upen_decf_tb_drop_discrcf_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " Disable Drop Packet When T-HEC Error in GFP Mode", regValue[0], cAf6_upen_decf_tb_drop_disthec_Mask, cAf6_upen_decf_tb_drop_disthec_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " Disable Drop Packet When Mismatch Data Link SAPI", regValue[0], cAf6_upen_decf_tb_drop_disthec_Mask, cAf6_upen_decf_tb_drop_disthec_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " Scramble Enable on DEC                          ", regValue[0], cAf6_upen_decf_tb_scrm_ena_Mask, cAf6_upen_decf_tb_scrm_ena_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " T-HEC Correct Enable in GFP Mode                ", regValue[0], cAf6_upen_decf_tb_thec_correct_Mask, cAf6_upen_decf_tb_thec_correct_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " TDM Header Remove Enable in HDLC Mode           ", regValue[0], cAf6_upen_decf_tb_thec_correct_Mask, cAf6_upen_decf_tb_thec_correct_Shift);
    Tha6021DebugPrintBitFieldVal(debugger, " C-HEC Correct Enable on DEC                     ", regValue[0], cAf6_upen_decf_tb_chec_correct_Mask, cAf6_upen_decf_tb_chec_correct_Shift);
    }

uint32 Tha60210012ModuleEncapNumSlice48(AtModuleEncap self)
    {
    if (self)
        return NumSlice48((AtModule)self);

    return 0;
    }

uint32 Tha60210012ModuleEncapNumSlice24(AtModuleEncap self)
    {
    if (self)
        return NumSlice24((AtModule)self);

    return 0;
    }

eAtRet Tha60210012ModuleEncapBundlePwFlowConnect(Tha60210012ModuleEncap self, AtMpBundle bundle, AtPw pw)
    {
    uint32 offset, regAddress, regValue;
    uint32 hwFlowId = Tha60210012PwAdapterHwFlowIdGet((ThaPwAdapter)pw);
    AtUnused(self);

    /* Get logic channel Id 0-511 */
    offset = cThaModuleEthFlowLookupBaseAddress + AtChannelIdGet((AtChannel)bundle);
    regAddress = cAf6Reg_bundledat_ethflow_table_Base + offset;
    regValue = mChannelHwRead(bundle, regAddress, cAtModuleEncap);

    /* Enable lookup */
    mRegFieldSet(regValue, cAf6_bundledat_ethflow_table_Bundle_Data_Enable_, 1);
    mRegFieldSet(regValue, cAf6_bundledat_ethflow_table_Bundle_Data_EthFlowID_, hwFlowId);
    mChannelHwWrite(bundle, regAddress, regValue, cAtModuleEncap);

    return cAtOk;
    }

eBool Tha60210012ModuleEncapResequenceManagerIsSupported(ThaModuleEncap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion >= mMethodsGet(mThis(self))->StartVersionSupportResequenceManager(mThis(self)))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60210012ModuleEncapRegistersNeedShiftUp(Tha60210012ModuleEncap self)
    {
    Tha60210011ModulePw pwModule = PwModule(self);
    uint32 maxPws = Tha60210011ModulePwMaxNumPwsPerSts24Slice(pwModule);
    uint32 originalMaxPws = Tha60210011ModulePwOriginalNumPwsPerSts24Slice(pwModule);
    return (maxPws > originalMaxPws) ? cAtTrue : cAtFalse;
    }

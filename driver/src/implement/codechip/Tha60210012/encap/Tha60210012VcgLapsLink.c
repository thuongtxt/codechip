/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60210012LapsLink.c
 *
 * Created Date: May 25, 2016
 *
 * Description : Tha60210012, LAPS Link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/encap/AtEncapChannelInternal.h"
#include "../../../../generic/encap/AtLapsLinkInternal.h"
#include "../../../../generic/eth/AtEthFlowInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pwe/ThaModulePwe.h"
#include "../../../default/pda/ThaModulePda.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../../../default/eth/controller/ThaEthFlowController.h"
#include "../pwe/Tha60210012ModulePwe.h"
#include "../pda/Tha60210012ModulePda.h"
#include "../cla/Tha60210012ModuleCla.h"
#include "Tha60210012ModuleEncapInternal.h"
#include "Tha60210012PhysicalEosEncapReg.h"
#include "Tha60210012VcgEncapChannel.h"

/*--------------------------- Define -----------------------------------------*/
/* LAPS link class */
typedef struct tTha60210012VcgLapsLink
    {
    tAtLapsLink super;
    }tTha60210012VcgLapsLink;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods                m_AtChannelOverride;
static tAtHdlcLinkMethods               m_AtHdlcLinkOverride;
static tAtLapsLinkMethods               m_AtLapsLinkOverride;

/* Save super implementation */
static const tAtChannelMethods          *m_AtChannelMethods = NULL;
static const tAtHdlcLinkMethods         *m_AtHdlcLinkMethods = NULL;
static const tAtLapsLinkMethods         *m_AtLapsLinkMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePwe ModulePwe(AtHdlcLink self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static ThaModulePda ModulePda(AtHdlcLink self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePda)AtDeviceModuleGet(device, cThaModulePda);
    }

static ThaModuleCla ModuleCla(AtHdlcLink self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static AtModuleEncap ModuleEncap(AtLapsLink self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static uint32 BaseAddress(void)
    {
    return Tha60210012ModuleEncapEosBaseAddress();
    }

static uint32 ChannelOffset(AtLapsLink self)
    {
    return AtChannelHwIdGet((AtChannel) AtHdlcLinkHdlcChannelGet((AtHdlcLink) self));
    }

static eBool TxSapiCanModify(AtLapsLink self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet TxSapiSet(AtLapsLink self, uint16 value)
    {
    uint32 sapiMsb = (uint32) ((value & 0xFF00) >> 8);
    uint32 sapiLsb = (uint32) (value & 0x00FF);
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_mapf_Base + ChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mRegFieldSet(regVal, cAf6_upen_mapf_TxSAPI1_, sapiMsb);
    mRegFieldSet(regVal, cAf6_upen_mapf_TxSAPI0_, sapiLsb);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
    
    return cAtOk;
    }

static uint16 TxSapiGet(AtLapsLink self)
    {
    uint32 sapiMsb;
    uint32 sapiLsb;
    uint32 regAddr = BaseAddress() + cAf6Reg_upen_mapf_Base + ChannelOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    sapiMsb = mRegField(regVal, cAf6_upen_mapf_TxSAPI1_);
    sapiLsb = mRegField(regVal, cAf6_upen_mapf_TxSAPI0_);

    return (uint16) ((sapiMsb << 8) | sapiLsb);
    }

static eAtRet SapiMonitorEnable(AtLapsLink self, eBool enable)
    {
    uint32 regVal[2];
    uint32 regAddr = cAf6Reg_upen_decf_Base + ChannelOffset(self);
    eAtRet ret = Tha60210012ModuleEncapEosLongRead(ModuleEncap(self), regAddr, regVal);
    if (ret != cAtOk)
        return ret;

    mRegFieldSet(regVal[1], cAf6_upen_decf_tb_drop_dismisf_, enable ? 1 : 0);
    return Tha60210012ModuleEncapEosLongWrite(ModuleEncap(self), regAddr, regVal);
    }

static eBool SapiMonitorIsEnabled(AtLapsLink self)
    {
    uint32 regVal[2];
    uint32 regAddr = cAf6Reg_upen_decf_Base + ChannelOffset(self);
    eAtRet ret = Tha60210012ModuleEncapEosLongRead(ModuleEncap(self), regAddr, regVal);
    if (ret != cAtOk)
        return cAtFalse;

    return (mRegField(regVal[1], cAf6_upen_decf_tb_drop_dismisf_) != 0) ? cAtTrue : cAtFalse;
    }

static eAtRet ExpectedSapiSet(AtLapsLink self, uint16 value)
    {
    uint32 regVal[2];
    uint32 regAddr = cAf6Reg_upen_decf_Base + ChannelOffset(self);
    eAtRet ret = Tha60210012ModuleEncapEosLongRead(ModuleEncap(self), regAddr, regVal);
    if (ret != cAtOk)
        return ret;

    mRegFieldSet(regVal[0], cAf6_upen_decf_tb_sapi_oct_, value);
    return Tha60210012ModuleEncapEosLongWrite(ModuleEncap(self), regAddr, regVal);
    }

static uint16 ExpectedSapiGet(AtLapsLink self)
    {
    uint32 regVal[2];
    uint32 regAddr = cAf6Reg_upen_decf_Base + ChannelOffset(self);
    eAtRet ret = Tha60210012ModuleEncapEosLongRead(ModuleEncap(self), regAddr, regVal);
    if (ret != cAtOk)
        return ret;

    return (uint16)mRegField(regVal[0], cAf6_upen_decf_tb_sapi_oct_);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtLapsLinkCounterTypeRxSapiError : return cAtTrue;
        default:return cAtFalse;
        }
    }

static uint32 CounterBaseAddress(AtChannel self, uint16 counterType, eBool r2c)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtLapsLinkCounterTypeRxSapiError:
            return r2c ? cAf6Reg_upencnt_dec_even5_r2c_Base : cAf6Reg_upencnt_dec_even5_ro_Base;
        default:
            return cBit31_0;
        }
    }

static uint32 HwCounterGet(AtChannel self, uint16 counterType, eBool r2c)
    {
    uint32 regAddr = BaseAddress() + CounterBaseAddress(self, counterType, r2c) + ChannelOffset((AtLapsLink)self);
    return mChannelHwRead(self, regAddr, cAtModuleEncap);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (!AtChannelCounterIsSupported(self, counterType))
        return 0x0;

    return HwCounterGet(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (!AtChannelCounterIsSupported(self, counterType))
        return 0x0;

    return HwCounterGet(self, counterType, cAtTrue);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);

    ret |= AtLapsLinkTxSapiSet((AtLapsLink)self, 0xfe01);
    ret |= AtLapsLinkExpectedSapiSet((AtLapsLink)self, 0xfe01);
    ret |= AtLapsLinkSapiMonitorEnable((AtLapsLink)self, cAtTrue);
    return ret;
    }

static eAtRet FlowActivate(AtHdlcLink self, AtEthFlow flow)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)flow);
    ThaEthFlowController flowController = ThaModuleEthEncapFlowControllerGet(ethModule);
    return ThaEthFlowControllerFlowActivate(flowController, flow, (AtChannel)self, 0);
    }

static eAtRet FlowBinding(AtHdlcLink self, AtEthFlow flow)
    {
    eAtRet ret;
    AtEthFlow currentFlow = AtHdlcLinkBoundFlowGet(self);
    uint32 vcgId = ChannelOffset((AtLapsLink)self);

    if (currentFlow == flow)
        return cAtOk;

    /* Do not allow to bind to another flow if this channel is busy */
    if ((currentFlow != NULL) || AtEthFlowIsBusy(flow))
        return cAtErrorChannelBusy;

    ret = AtEthFlowHdlcLinkSet(flow, self);
    ret |= FlowActivate(self, flow);
    ret |= m_AtHdlcLinkMethods->FlowBind(self, flow);
    if (ret != cAtOk)
        return ret;

    ret |= AtEthFlowHdlcLinkSet(flow, self);
    ret |= AtChannelQueueEnable((AtChannel) self, cAtTrue);
    ret |= Tha60210012ModulePweEthFlowVcgLookup(ModulePwe(self), vcgId, flow);
    ret |= Tha60210012ModulePdaVcgLookUpControl(ModulePda(self), vcgId, flow, cAtTrue);
    ret |= Tha60210012ModuleClaVcgServiceEnable(ModuleCla(self), vcgId, flow, cAtTrue);
    ret |= Tha60210012EncapChannelBandwidthUpdate((AtEncapChannel)AtHdlcLinkHdlcChannelGet(self));

    return ret;
    }

static eAtRet FlowDeActivate(AtHdlcLink self, AtEthFlow flow)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)flow);
    ThaEthFlowController flowController = ThaModuleEthEncapFlowControllerGet(ethModule);
    AtUnused(self);
    return ThaEthFlowControllerFlowDeActivate(flowController, flow);
    }

static eAtRet FlowUnBind(AtHdlcLink self)
    {
    eAtRet ret;
    AtEthFlow currentFlow = AtHdlcLinkBoundFlowGet(self);
    uint32 vcgId = ChannelOffset((AtLapsLink)self);

    if (currentFlow == NULL)
        return cAtOk;

    ret  = Tha60210012ModuleClaVcgServiceEnable(ModuleCla(self), vcgId, currentFlow, cAtFalse);
    ret |= Tha60210012ModulePdaVcgLookUpControl(ModulePda(self), vcgId, currentFlow, cAtFalse);
    ret |= AtChannelQueueEnable((AtChannel) self, cAtFalse);
    ret |= AtEthFlowHdlcLinkSet(currentFlow, NULL);
    ret |= FlowDeActivate(self, currentFlow);

    if (ret != cAtOk)
        return ret;

    AtEthFlowHdlcLinkSet(currentFlow, NULL);

    return m_AtHdlcLinkMethods->FlowBind(self, NULL);
    }

static eAtRet FlowBind(AtHdlcLink self, AtEthFlow flow)
    {
    return (flow) ? FlowBinding(self, flow) : FlowUnBind(self);
    }

static eAtRet QueueEnable(AtChannel self, eBool enable)
    {
    return Tha60210012EopEncapChannelQueueEnable(self, enable);
    }

static void OverrideAtChannel(AtLapsLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, QueueEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtHdlcLink(AtLapsLink self)
    {
    AtHdlcLink link = (AtHdlcLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcLinkMethods = mMethodsGet(link);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcLinkOverride, mMethodsGet(link), sizeof(m_AtHdlcLinkOverride));
        mMethodOverride(m_AtHdlcLinkOverride, FlowBind);
        }

    mMethodsSet(link, &m_AtHdlcLinkOverride);
    }

static void OverrideAtLapsLink(AtLapsLink self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtLapsLinkMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtLapsLinkOverride, m_AtLapsLinkMethods, sizeof(m_AtLapsLinkOverride));

        mMethodOverride(m_AtLapsLinkOverride, TxSapiCanModify);
        mMethodOverride(m_AtLapsLinkOverride, TxSapiSet);
        mMethodOverride(m_AtLapsLinkOverride, TxSapiGet);
        mMethodOverride(m_AtLapsLinkOverride, SapiMonitorEnable);
        mMethodOverride(m_AtLapsLinkOverride, SapiMonitorIsEnabled);
        mMethodOverride(m_AtLapsLinkOverride, ExpectedSapiSet);
        mMethodOverride(m_AtLapsLinkOverride, ExpectedSapiGet);
        }

    mMethodsSet(self, &m_AtLapsLinkOverride);
    }

static void Override(AtLapsLink self)
    {
    OverrideAtChannel(self);
    OverrideAtHdlcLink(self);
    OverrideAtLapsLink(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012VcgLapsLink);
    }

static AtLapsLink ObjectInit(AtLapsLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Call super constructor */
    if (AtLapsLinkObjectInit(self, hdlcChannel) == NULL)
        return NULL;

    /* Initialize implementation */
    Override(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

AtLapsLink Tha60210012VcgLapsLinkNew(AtHdlcChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtLapsLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLink, channel);
    }

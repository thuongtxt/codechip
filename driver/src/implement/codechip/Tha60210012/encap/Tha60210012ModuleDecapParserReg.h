/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60210012ModuleEncapParserReg.h
 * 
 * Created Date: Apr 7, 2016
 *
 * Description : Encap Parser Register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEENCAPPARSERREG_H_
#define _THA60210012MODULEENCAPPARSERREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : CONFIG HDLC LO DEC
Reg Addr   : 0x2000 - 0x23FF
Reg Formula: 0x2000+ $CID
    Where  :
           + $CID (0-1023) : Channel ID
Reg Desc   :
config HDLC ID 0-1023

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_locfg_Base                                                                    0x2000UL
#define cAf6Reg_upen_hdlc_locfg(CID)                                                            (0x2000+(CID))
#define cAf6Reg_upen_hdlc_locfg_WidthVal                                                                    32
#define cAf6Reg_upen_hdlc_locfg_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: cfg_scren
BitField Type: R/W
BitField Desc: config to enable scramble, (1) is enable, (0) is disable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_scren_Bit_Start                                                             4
#define cAf6_upen_hdlc_locfg_cfg_scren_Bit_End                                                               4
#define cAf6_upen_hdlc_locfg_cfg_scren_Mask                                                              cBit4
#define cAf6_upen_hdlc_locfg_cfg_scren_Shift                                                                 4
#define cAf6_upen_hdlc_locfg_cfg_scren_MaxVal                                                              0x1
#define cAf6_upen_hdlc_locfg_cfg_scren_MinVal                                                              0x0
#define cAf6_upen_hdlc_locfg_cfg_scren_RstVal                                                              0x0

/*--------------------------------------
BitField Name: cfg_lsbfirst
BitField Type: R/W
BitField Desc: config to receive LSB first, (1) is LSB first, (0) is default MSB
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_lsbfirst_Bit_Start                                                          3
#define cAf6_upen_hdlc_locfg_cfg_lsbfirst_Bit_End                                                            3
#define cAf6_upen_hdlc_locfg_cfg_lsbfirst_Mask                                                           cBit3
#define cAf6_upen_hdlc_locfg_cfg_lsbfirst_Shift                                                              3
#define cAf6_upen_hdlc_locfg_cfg_lsbfirst_MaxVal                                                           0x1
#define cAf6_upen_hdlc_locfg_cfg_lsbfirst_MinVal                                                           0x0
#define cAf6_upen_hdlc_locfg_cfg_lsbfirst_RstVal                                                           0x0

/*--------------------------------------
BitField Name: cfg_bitstuff
BitField Type: R/W
BitField Desc: config to select bit stuff or byte sutff, (1) is bit stuff, (0)
is byte stuff
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_Bit_Start                                                          2
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_Bit_End                                                            2
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_Mask                                                           cBit2
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_Shift                                                              2
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_MaxVal                                                           0x1
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_MinVal                                                           0x0
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_RstVal                                                           0x0

/*--------------------------------------
BitField Name: cfg_fcsmsb
BitField Type: R/W
BitField Desc: config to calculate FCS from MSB or LSB, (1) is MSB, (0) is LSB
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_Bit_Start                                                            1
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_Bit_End                                                              1
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_Mask                                                             cBit1
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_Shift                                                                1
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_MaxVal                                                             0x1
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_MinVal                                                             0x0
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_RstVal                                                             0x0

/*--------------------------------------
BitField Name: cfg_fcsmode
BitField Type: R/W
BitField Desc: config to calculate FCS 32 or FCS 16, (1) is FCS 32, (0) is FCS
16
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_Bit_Start                                                           0
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_Bit_End                                                             0
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_Mask                                                            cBit0
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_Shift                                                               0
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_MaxVal                                                            0x1
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_MinVal                                                            0x0
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : CONFIG HDLC LO DEC
Reg Addr   : 0x2404
Reg Formula: 0x2404+ $CID
    Where  :
           + $CID (0-1023) : Channel ID
Reg Desc   :
config HDLC ID 0-1023

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_loglbcfg_Base                                                                 0x2404
#define cAf6Reg_upen_hdlc_loglbcfg(CID)                                                         (0x2404+(CID))
#define cAf6Reg_upen_hdlc_loglbcfg_WidthVal                                                                 32
#define cAf6Reg_upen_hdlc_loglbcfg_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: cfg_lsbfirst
BitField Type: R/W
BitField Desc: config to receive LSB first, (1) is LSB first, (0) is default MSB
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_Bit_Start                                                       3
#define cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_Bit_End                                                         3
#define cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_Mask                                                        cBit3
#define cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_Shift                                                           3
#define cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_MaxVal                                                        0x1
#define cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_MinVal                                                        0x0
#define cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_RstVal                                                        0x0

/*--------------------------------------
BitField Name: cfg_loop
BitField Type: R/W
BitField Desc: config loopback ENC -DEC, receive directly data from ENC
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upen_hdlc_loglbcfg_cfg_loop_Bit_Start                                                           2
#define cAf6_upen_hdlc_loglbcfg_cfg_loop_Bit_End                                                             2
#define cAf6_upen_hdlc_loglbcfg_cfg_loop_Mask                                                            cBit2
#define cAf6_upen_hdlc_loglbcfg_cfg_loop_Shift                                                               2
#define cAf6_upen_hdlc_loglbcfg_cfg_loop_MaxVal                                                            0x1
#define cAf6_upen_hdlc_loglbcfg_cfg_loop_MinVal                                                            0x0
#define cAf6_upen_hdlc_loglbcfg_cfg_loop_RstVal                                                            0x0

/*--------------------------------------
BitField Name: cfg_alrmrstint
BitField Type: R/W
BitField Desc: config reset sticky, (1) is reset, (0) is normal
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_hdlc_loglbcfg_cfg_alrmrstint_Bit_Start                                                     1
#define cAf6_upen_hdlc_loglbcfg_cfg_alrmrstint_Bit_End                                                       1
#define cAf6_upen_hdlc_loglbcfg_cfg_alrmrstint_Mask                                                      cBit1
#define cAf6_upen_hdlc_loglbcfg_cfg_alrmrstint_Shift                                                         1
#define cAf6_upen_hdlc_loglbcfg_cfg_alrmrstint_MaxVal                                                      0x1
#define cAf6_upen_hdlc_loglbcfg_cfg_alrmrstint_MinVal                                                      0x0
#define cAf6_upen_hdlc_loglbcfg_cfg_alrmrstint_RstVal                                                      0x0

/*--------------------------------------
BitField Name: cfg_upactive
BitField Type: R/W
BitField Desc: config active from CPU (1) is active, (0) is disable active
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_hdlc_loglbcfg_cfg_upactive_Bit_Start                                                       0
#define cAf6_upen_hdlc_loglbcfg_cfg_upactive_Bit_End                                                         0
#define cAf6_upen_hdlc_loglbcfg_cfg_upactive_Mask                                                        cBit0
#define cAf6_upen_hdlc_loglbcfg_cfg_upactive_Shift                                                           0
#define cAf6_upen_hdlc_loglbcfg_cfg_upactive_MaxVal                                                        0x1
#define cAf6_upen_hdlc_loglbcfg_cfg_upactive_MinVal                                                        0x0
#define cAf6_upen_hdlc_loglbcfg_cfg_upactive_RstVal                                                        0x1

/*------------------------------------------------------------------------------
Reg Name   : PPP PID  global config
Reg Addr   : 0x0_0000-0x0_000E
Reg Formula: 0x0_0000 + $ppp_type
    Where  :
           + $ppp_type(0-14) : PPP_TYPE
Reg Desc   :
Use to configure protocol id for PPP_TYPE
Reset_value of ppp_type
ppp_type == 0: 0x0021, is Ipv4 packets
ppp_type == 1: 0x0057, is Ipv6 packets
ppp_type == 2: 0x0281, is MPLS unicast packets
ppp_type == 3: 0x0283, is MPLS multicast packets
ppp_type == 4: 0x0023, is IS-IS packets
ppp_type == 5: 0x0031, is BCP packets
ppp_type == 6: 0x0021, is reserve#6
ppp_type == 7: 0x0021, is reserve#7
ppp_type == 8: 0x0021, is reserve#8
ppp_type == 9: 0x0021, is reserve#9
ppp_type == 10: 0x0021, is reserve#10
ppp_type == 11: 0x0021, is reserve#11
ppp_type == 12: 0x0021, is reserve#12
ppp_type == 13: 0x0021, is reserve#13
ppp_type == 14: 0x0021, is reserve#14

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ppppid_locfg_Base                                                                 0x00000
#define cAf6Reg_upen_ppppid_locfg(ppptype)                                                 (0x00000+(ppptype))
#define cAf6Reg_upen_ppppid_locfg_WidthVal                                                                  32
#define cAf6Reg_upen_ppppid_locfg_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ppppid_mask
BitField Type: R/W
BitField Desc: PPP PID mask
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_ppppid_locfg_ppppid_mask_Bit_Start                                                        16
#define cAf6_upen_ppppid_locfg_ppppid_mask_Bit_End                                                          31
#define cAf6_upen_ppppid_locfg_ppppid_mask_Mask                                                      cBit31_16
#define cAf6_upen_ppppid_locfg_ppppid_mask_Shift                                                            16
#define cAf6_upen_ppppid_locfg_ppppid_mask_MaxVal                                                       0xffff
#define cAf6_upen_ppppid_locfg_ppppid_mask_MinVal                                                          0x0
#define cAf6_upen_ppppid_locfg_ppppid_mask_RstVal                                                       0xFFFF

/*--------------------------------------
BitField Name: ppppid_val
BitField Type: R/W
BitField Desc: PPP PIO value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_ppppid_locfg_ppppid_val_Bit_Start                                                          0
#define cAf6_upen_ppppid_locfg_ppppid_val_Bit_End                                                           15
#define cAf6_upen_ppppid_locfg_ppppid_val_Mask                                                        cBit15_0
#define cAf6_upen_ppppid_locfg_ppppid_val_Shift                                                              0
#define cAf6_upen_ppppid_locfg_ppppid_val_MaxVal                                                        0xffff
#define cAf6_upen_ppppid_locfg_ppppid_val_MinVal                                                           0x0
#define cAf6_upen_ppppid_locfg_ppppid_val_RstVal                                                   Reset_value


/*------------------------------------------------------------------------------
Reg Name   : ETH Type  global config
Reg Addr   : 0x0_0010-0015
Reg Formula: 0x0_0010 + $ppp_type
    Where  :
           + $ppp_type(0-5) : PPP_TYPE
Reg Desc   :
Use to configure ethernet type for ppp_type
Reset_value of ppp_type
ppp_type == 0: 0x0800, is Ipv4 packets
ppp_type == 1: 0x86DD, is Ipv6 packets
ppp_type == 2: 0x8847, is MPLS unicast packets
ppp_type == 3: 0x8848, is MPLS multicast packets
ppp_type == 4: 0xFEFE, is IS-IS packets
ppp_type == 5: 0x1234, is Ethernet transparent packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ethtyp_locfg_Base                                                                 0x00010
#define cAf6Reg_upen_ethtyp_locfg(ppptype)                                                 (0x00010+(ppptype))
#define cAf6Reg_upen_ethtyp_locfg_WidthVal                                                                  32
#define cAf6Reg_upen_ethtyp_locfg_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ethtyp_mask
BitField Type: R/W
BitField Desc: PPP PID mask
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_ethtyp_locfg_ethtyp_mask_Bit_Start                                                        16
#define cAf6_upen_ethtyp_locfg_ethtyp_mask_Bit_End                                                          31
#define cAf6_upen_ethtyp_locfg_ethtyp_mask_Mask                                                      cBit31_16
#define cAf6_upen_ethtyp_locfg_ethtyp_mask_Shift                                                            16
#define cAf6_upen_ethtyp_locfg_ethtyp_mask_MaxVal                                                       0xffff
#define cAf6_upen_ethtyp_locfg_ethtyp_mask_MinVal                                                          0x0
#define cAf6_upen_ethtyp_locfg_ethtyp_mask_RstVal                                                       0xFFFF

/*--------------------------------------
BitField Name: ethtyp_val
BitField Type: R/W
BitField Desc: ETH Type value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_ethtyp_locfg_ethtyp_val_Bit_Start                                                          0
#define cAf6_upen_ethtyp_locfg_ethtyp_val_Bit_End                                                           15
#define cAf6_upen_ethtyp_locfg_ethtyp_val_Mask                                                        cBit15_0
#define cAf6_upen_ethtyp_locfg_ethtyp_val_Shift                                                              0
#define cAf6_upen_ethtyp_locfg_ethtyp_val_MaxVal                                                        0xffff
#define cAf6_upen_ethtyp_locfg_ethtyp_val_MinVal                                                           0x0
#define cAf6_upen_ethtyp_locfg_ethtyp_val_RstVal                                                        0x0800

/*------------------------------------------------------------------------------
Reg Name   : Channel id config for parser
Reg Addr   : 0x0_0400-0x0_07FF
Reg Formula: 0x0_0400 + $cid
    Where  :
           + $cid(0-1023) : Channel - ID
Reg Desc   :
Use to configure for channel id
HDL_PATH: iloparser_core.mem_lklinkcfg.ram.ram[$cid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cidcfg_locfg_Base                                                                 0x00400
#define cAf6Reg_upen_cidlocfg(cid)                                                               (0x00400+(cid))
#define cAf6Reg_upen_cidlocfg_WidthVal                                                                        32
#define cAf6Reg_upen_cidlocfg_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: ml_typ
BitField Type: R/W
BitField Desc: Multilink type MLPPP : 0 : MLPPP short sequence , 1 : MLPPP long
sequence
BitField Bits: [9]
--------------------------------------*/
#define cAf6_upen_cidlocfg_ml_typ_Bit_Start                                                                    9
#define cAf6_upen_cidlocfg_ml_typ_Bit_End                                                                      9
#define cAf6_upen_cidlocfg_ml_typ_Mask                                                                     cBit9
#define cAf6_upen_cidlocfg_ml_typ_Shift                                                                        9
#define cAf6_upen_cidlocfg_ml_typ_MaxVal                                                                     0x1
#define cAf6_upen_cidlocfg_ml_typ_MinVal                                                                     0x0
#define cAf6_upen_cidlocfg_ml_typ_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: ml_enb
BitField Type: R/W
BitField Desc: Multilink enable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_upen_cidlocfg_ml_enb_Bit_Start                                                                    8
#define cAf6_upen_cidlocfg_ml_enb_Bit_End                                                                      8
#define cAf6_upen_cidlocfg_ml_enb_Mask                                                                     cBit8
#define cAf6_upen_cidlocfg_ml_enb_Shift                                                                        8
#define cAf6_upen_cidlocfg_ml_enb_MaxVal                                                                     0x1
#define cAf6_upen_cidlocfg_ml_enb_MinVal                                                                     0x0
#define cAf6_upen_cidlocfg_ml_enb_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: opt_typ
BitField Type: R/W
BitField Desc: Option type PPP          : 0 : PPP no class , 1 : PPP class PW FR
: 0 : FR port      , 1 : FR One2One Others FR   : 0 : MLFR End2End , 1 : MLFR
UNI-NNi
BitField Bits: [7]
--------------------------------------*/
#define cAf6_upen_cidlocfg_opt_typ_Bit_Start                                                                   7
#define cAf6_upen_cidlocfg_opt_typ_Bit_End                                                                     7
#define cAf6_upen_cidlocfg_opt_typ_Mask                                                                    cBit7
#define cAf6_upen_cidlocfg_opt_typ_Shift                                                                       7
#define cAf6_upen_cidlocfg_opt_typ_MaxVal                                                                    0x1
#define cAf6_upen_cidlocfg_opt_typ_MinVal                                                                    0x0
#define cAf6_upen_cidlocfg_opt_typ_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: frm_typ
BitField Type: R/W
BitField Desc: Encap type 0             : PW 1          : PPP TER , IETF FR 2
: Cisco HDLC , Cisco FR 3               : Both Cisco FR and IETF FR
BitField Bits: [6:5]
--------------------------------------*/
#define cAf6_upen_cidlocfg_frm_typ_Bit_Start                                                                   5
#define cAf6_upen_cidlocfg_frm_typ_Bit_End                                                                     6
#define cAf6_upen_cidlocfg_frm_typ_Mask                                                                  cBit6_5
#define cAf6_upen_cidlocfg_frm_typ_Shift                                                                       5
#define cAf6_upen_cidlocfg_frm_typ_MaxVal                                                                    0x3
#define cAf6_upen_cidlocfg_frm_typ_MinVal                                                                    0x0
#define cAf6_upen_cidlocfg_frm_typ_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: enc_typ
BitField Type: R/W
BitField Desc: Encap type 0             : HDLC / Cisco HDLC 1           : PPP 2
: FR Others     : unused
BitField Bits: [4:2]
--------------------------------------*/
#define cAf6_upen_cidlocfg_enc_typ_Bit_Start                                                                   2
#define cAf6_upen_cidlocfg_enc_typ_Bit_End                                                                     4
#define cAf6_upen_cidlocfg_enc_typ_Mask                                                                  cBit4_2
#define cAf6_upen_cidlocfg_enc_typ_Shift                                                                       2
#define cAf6_upen_cidlocfg_enc_typ_MaxVal                                                                    0x7
#define cAf6_upen_cidlocfg_enc_typ_MinVal                                                                    0x0
#define cAf6_upen_cidlocfg_enc_typ_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: addcfg
BitField Type: R/W
BitField Desc: Address configure PPP:  addcfg [1]  Address compress PPP mode
enable addcfg [0]  Protocol compress PPP mode enable FR : addcfg = 00 : 2byte
Q922 address addcfg = 01 : 3byte Q922 address addcfg = 10 : 4byte Q922 address
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_upen_cidlocfg_addcfg_Bit_Start                                                                    0
#define cAf6_upen_cidlocfg_addcfg_Bit_End                                                                      1
#define cAf6_upen_cidlocfg_Q922Addr_Mask                                                                 cBit1_0
#define cAf6_upen_cidlocfg_Q922Addr_Shift                                                                      0
#define cAf6_upen_cidlocfg_addcfg_Mask                                                                     cBit1
#define cAf6_upen_cidlocfg_addcfg_Shift                                                                        1
#define cAf6_upen_cidlocfg_pfccfg_Mask                                                                     cBit0
#define cAf6_upen_cidlocfg_pfccfg_Shift                                                                        0
#define cAf6_upen_cidlocfg_addcfg_MaxVal                                                                     0x3
#define cAf6_upen_cidlocfg_addcfg_MinVal                                                                     0x0
#define cAf6_upen_cidlocfg_addcfg_RstVal                                                                     0x0

/*------------------------------------------------------------------------------
Reg Name   : Channel id config for parser
Reg Addr   : 0x0_0100-0x0_01FF
Reg Formula: 0x0_0100 + $cid
    Where  :
           + $cid(0-47) : Channel - ID
Reg Desc   :
Use to configure for channel id
HDL_PATH: ihoparser_core.mem_lklinkcfg.ram.ram[$cid]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cidcfg_HoCfg_Base                                                                   0x00100
#define cAf6Reg_upen_cidhocfg(cid)                                                               (0x00100+(cid))
#define cAf6Reg_upen_cidhocfg_WidthVal                                                                        32
#define cAf6Reg_upen_cidhocfg_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: ml_typ
BitField Type: R/W
BitField Desc: Multilink type MLPPP : 0 : MLPPP short sequence , 1 : MLPPP long
sequence
BitField Bits: [9]
--------------------------------------*/
#define cAf6_upen_cidhocfg_ml_typ_Bit_Start                                                                    9
#define cAf6_upen_cidhocfg_ml_typ_Bit_End                                                                      9
#define cAf6_upen_cidhocfg_ml_typ_Mask                                                                     cBit9
#define cAf6_upen_cidhocfg_ml_typ_Shift                                                                        9
#define cAf6_upen_cidhocfg_ml_typ_MaxVal                                                                     0x1
#define cAf6_upen_cidhocfg_ml_typ_MinVal                                                                     0x0
#define cAf6_upen_cidhocfg_ml_typ_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: ml_enb
BitField Type: R/W
BitField Desc: Multilink enable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_upen_cidhocfg_ml_enb_Bit_Start                                                                    8
#define cAf6_upen_cidhocfg_ml_enb_Bit_End                                                                      8
#define cAf6_upen_cidhocfg_ml_enb_Mask                                                                     cBit8
#define cAf6_upen_cidhocfg_ml_enb_Shift                                                                        8
#define cAf6_upen_cidhocfg_ml_enb_MaxVal                                                                     0x1
#define cAf6_upen_cidhocfg_ml_enb_MinVal                                                                     0x0
#define cAf6_upen_cidhocfg_ml_enb_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: opt_typ
BitField Type: R/W
BitField Desc: Option type PPP : 0 : PPP no class , 1 : PPP class PW FR
: 0 : FR port      , 1 : FR One2One Others FR   : 0 : MLFR End2End , 1 : MLFR
UNI-NNi
BitField Bits: [7]
--------------------------------------*/
#define cAf6_upen_cidhocfg_opt_typ_Bit_Start                                                                   7
#define cAf6_upen_cidhocfg_opt_typ_Bit_End                                                                     7
#define cAf6_upen_cidhocfg_opt_typ_Mask                                                                    cBit7
#define cAf6_upen_cidhocfg_opt_typ_Shift                                                                       7
#define cAf6_upen_cidhocfg_opt_typ_MaxVal                                                                    0x1
#define cAf6_upen_cidhocfg_opt_typ_MinVal                                                                    0x0
#define cAf6_upen_cidhocfg_opt_typ_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: frm_typ
BitField Type: R/W
BitField Desc: Encap type 0             : PW 1          : PPP TER , IETF FR 2
: Cisco HDLC , Cisco FR 3               : Both Cisco FR and IETF FR
BitField Bits: [6:5]
--------------------------------------*/
#define cAf6_upen_cidhocfg_frm_typ_Bit_Start                                                                   5
#define cAf6_upen_cidhocfg_frm_typ_Bit_End                                                                     6
#define cAf6_upen_cidhocfg_frm_typ_Mask                                                                  cBit6_5
#define cAf6_upen_cidhocfg_frm_typ_Shift                                                                       5
#define cAf6_upen_cidhocfg_frm_typ_MaxVal                                                                    0x3
#define cAf6_upen_cidhocfg_frm_typ_MinVal                                                                    0x0
#define cAf6_upen_cidhocfg_frm_typ_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: enc_typ
BitField Type: R/W
BitField Desc: Encap type 0             : HDLC / Cisco HDLC 1           : PPP 2
: FR Others     : unused
BitField Bits: [4:2]
--------------------------------------*/
#define cAf6_upen_cidhocfg_enc_typ_Bit_Start                                                                   2
#define cAf6_upen_cidhocfg_enc_typ_Bit_End                                                                     4
#define cAf6_upen_cidhocfg_enc_typ_Mask                                                                  cBit4_2
#define cAf6_upen_cidhocfg_enc_typ_Shift                                                                       2
#define cAf6_upen_cidhocfg_enc_typ_MaxVal                                                                    0x7
#define cAf6_upen_cidhocfg_enc_typ_MinVal                                                                    0x0
#define cAf6_upen_cidhocfg_enc_typ_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: addcfg
BitField Type: R/W
BitField Desc: Address configure PPP:  addcfg [1]  Address compress PPP mode
enable addcfg [0]  Protocol compress PPP mode enable FR : addcfg = 00 : 2byte
Q922 address addcfg = 01 : 3byte Q922 address addcfg = 10 : 4byte Q922 address
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_upen_cidhocfg_addcfg_Bit_Start                                                                    0
#define cAf6_upen_cidhocfg_addcfg_Bit_End                                                                      1
#define cAf6_upen_cidhocfg_Q922Addr_Mask                                                                 cBit1_0
#define cAf6_upen_cidhocfg_Q922Addr_Shift                                                                      0
#define cAf6_upen_cidhocfg_addcfg_Mask                                                                     cBit1
#define cAf6_upen_cidhocfg_addcfg_Shift                                                                        1
#define cAf6_upen_cidhocfg_afccfg_Mask                                                                     cBit0
#define cAf6_upen_cidhocfg_afccfg_Shift                                                                        0
#define cAf6_upen_cidhocfg_addcfg_MaxVal                                                                     0x3
#define cAf6_upen_cidhocfg_addcfg_MinVal                                                                     0x0
#define cAf6_upen_cidhocfg_addcfg_RstVal                                                                     0x0

/*------------------------------------------------------------------------------
Reg Name   : Channel ID  Counters for parser
Reg Addr   : 0x0_1000 - 0x0_1FFF (R2C)
Reg Formula: 0x0_1000 + $cid*4  + $typ
    Where  :
           + $cid(0-1023) : CID - ID
           + $typ(0-3) : Type counter
Reg Desc   :
Use to read/read to clear some counters at each channel id
typ = 0 : SOP error counter
typ = 1 : Address error counter
typ = 2 : Control error counter
typ = 3 : Protocol error counter

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cidcnt_locfg_Base                                                                 0x01000
#define cAf6Reg_upen_cidcnt_locfg(cid, typ)                                         (0x01000+(typ)*4+(cid))
#define cAf6Reg_upen_cidcnt_locfg_WidthVal                                                                  32
#define cAf6Reg_upen_cidcnt_locfg_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: cnt_value
BitField Type: R2C
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cidcnt_locfg_cnt_value_Bit_Start                                                           0
#define cAf6_upen_cidcnt_locfg_cnt_value_Bit_End                                                            31
#define cAf6_upen_cidcnt_locfg_cnt_value_Mask                                                         cBit31_0
#define cAf6_upen_cidcnt_locfg_cnt_value_Shift                                                               0
#define cAf6_upen_cidcnt_locfg_cnt_value_MaxVal                                                     0xffffffff
#define cAf6_upen_cidcnt_locfg_cnt_value_MinVal                                                            0x0
#define cAf6_upen_cidcnt_locfg_cnt_value_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG HDLC HO DEC
Reg Addr   : 0x800 - 0x87F
Reg Formula: 0x800+ $STSID
    Where  :
           + $STSID (0-47) : STS ID
Reg Desc   :
config message MDL BUFFER Channel ID 0-15

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_Hocfg_Base                                                                     0x800UL
#define cAf6Reg_upen_hdlc_Hocfg(STSID)                                                         (0x800+(STSID))
#define cAf6Reg_upen_hdlc_Hocfg_WidthVal                                                                    32
#define cAf6Reg_upen_hdlc_Hocfg_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: cfg_scren
BitField Type: R/W
BitField Desc: config to enable scramble, (1) is enable, (0) is disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upen_hdlc_Hocfg_cfg_scren_Bit_Start                                                             2
#define cAf6_upen_hdlc_Hocfg_cfg_scren_Bit_End                                                               2
#define cAf6_upen_hdlc_Hocfg_cfg_scren_Mask                                                              cBit2
#define cAf6_upen_hdlc_Hocfg_cfg_scren_Shift                                                                 2
#define cAf6_upen_hdlc_Hocfg_cfg_scren_MaxVal                                                              0x1
#define cAf6_upen_hdlc_Hocfg_cfg_scren_MinVal                                                              0x0
#define cAf6_upen_hdlc_Hocfg_cfg_scren_RstVal                                                              0x0

/*--------------------------------------
BitField Name: cfg_fcsmsb
BitField Type: R/W
BitField Desc: config to calculate FCS from MSB or LSB, (1) is MSB, (0) is LSB
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmsb_Bit_Start                                                            1
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmsb_Bit_End                                                              1
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmsb_Mask                                                             cBit1
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmsb_Shift                                                                1
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmsb_MaxVal                                                             0x1
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmsb_MinVal                                                             0x0
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmsb_RstVal                                                             0x0

/*--------------------------------------
BitField Name: cfg_fcsmode
BitField Type: R/W
BitField Desc: config to calculate FCS 32 or FCS 16, (1) is FCS 32, (0) is FCS
16
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmode_Bit_Start                                                           0
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmode_Bit_End                                                             0
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmode_Mask                                                            cBit0
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmode_Shift                                                               0
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmode_MaxVal                                                            0x1
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmode_MinVal                                                            0x0
#define cAf6_upen_hdlc_Hocfg_cfg_fcsmode_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : CONFIG HDLC HO DEC
Reg Addr   : 0x884
Reg Formula: 0x884+ $CID
    Where  :
           + $CID (0-1023) : Channel ID
Reg Desc   :
config HDLC ID 0-1023

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_hoglbcfg_Base                                                                  0x884
#define cAf6Reg_upen_hdlc_hoglbcfg(CID)                                                          (0x884+(CID))
#define cAf6Reg_upen_hdlc_hoglbcfg_WidthVal                                                                 32
#define cAf6Reg_upen_hdlc_hoglbcfg_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: cfg_loop
BitField Type: R/W
BitField Desc: config loopback ENC -DEC, receive directly data from ENC
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upen_hdlc_hoglbcfg_cfg_loop_Bit_Start                                                           2
#define cAf6_upen_hdlc_hoglbcfg_cfg_loop_Bit_End                                                             2
#define cAf6_upen_hdlc_hoglbcfg_cfg_loop_Mask                                                            cBit2
#define cAf6_upen_hdlc_hoglbcfg_cfg_loop_Shift                                                               2
#define cAf6_upen_hdlc_hoglbcfg_cfg_loop_MaxVal                                                            0x1
#define cAf6_upen_hdlc_hoglbcfg_cfg_loop_MinVal                                                            0x0
#define cAf6_upen_hdlc_hoglbcfg_cfg_loop_RstVal                                                            0x0

/*--------------------------------------
BitField Name: cfg_alrmrstint
BitField Type: R/W
BitField Desc: config reset sticky, (1) is reset, (0) is normal
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_hdlc_hoglbcfg_cfg_alrmrstint_Bit_Start                                                     1
#define cAf6_upen_hdlc_hoglbcfg_cfg_alrmrstint_Bit_End                                                       1
#define cAf6_upen_hdlc_hoglbcfg_cfg_alrmrstint_Mask                                                      cBit1
#define cAf6_upen_hdlc_hoglbcfg_cfg_alrmrstint_Shift                                                         1
#define cAf6_upen_hdlc_hoglbcfg_cfg_alrmrstint_MaxVal                                                      0x1
#define cAf6_upen_hdlc_hoglbcfg_cfg_alrmrstint_MinVal                                                      0x0
#define cAf6_upen_hdlc_hoglbcfg_cfg_alrmrstint_RstVal                                                      0x0

/*--------------------------------------
BitField Name: cfg_upactive
BitField Type: R/W
BitField Desc: config active from CPU (1) is active, (0) is disable active
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_hdlc_hoglbcfg_cfg_upactive_Bit_Start                                                       0
#define cAf6_upen_hdlc_hoglbcfg_cfg_upactive_Bit_End                                                         0
#define cAf6_upen_hdlc_hoglbcfg_cfg_upactive_Mask                                                        cBit0
#define cAf6_upen_hdlc_hoglbcfg_cfg_upactive_Shift                                                           0
#define cAf6_upen_hdlc_hoglbcfg_cfg_upactive_MaxVal                                                        0x1
#define cAf6_upen_hdlc_hoglbcfg_cfg_upactive_MinVal                                                        0x0
#define cAf6_upen_hdlc_hoglbcfg_cfg_upactive_RstVal                                                        0x1

/*------------------------------------------------------------------------------
Reg Name   : PPP PID  global config
Reg Addr   : 0x0_0000-0x0_000E
Reg Formula: 0x0_0000 + $ppp_type
    Where  :
           + $ppp_type(0-14) : PPP_TYPE
Reg Desc   :
Use to configure protocol id for PPP_TYPE
Reset_value of ppp_type
ppp_type == 0: 0x0021, is Ipv4 packets
ppp_type == 1: 0x0057, is Ipv6 packets
ppp_type == 2: 0x0281, is MPLS unicast packets
ppp_type == 3: 0x0283, is MPLS multicast packets
ppp_type == 4: 0x0023, is IS-IS packets
ppp_type == 5: 0x0031, is BCP packets
ppp_type == 6: 0x0021, is reserve#6
ppp_type == 7: 0x0021, is reserve#7
ppp_type == 8: 0x0021, is reserve#8
ppp_type == 9: 0x0021, is reserve#9
ppp_type == 10: 0x0021, is reserve#10
ppp_type == 11: 0x0021, is reserve#11
ppp_type == 12: 0x0021, is reserve#12
ppp_type == 13: 0x0021, is reserve#13
ppp_type == 14: 0x0021, is reserve#14

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ppppid_HoCfg_Base                                                                 0x00000
#define cAf6Reg_upen_ppppid_HoCfg(ppptype)                                                 (0x00000+(ppptype))
#define cAf6Reg_upen_ppppid_HoCfg_WidthVal                                                                  32
#define cAf6Reg_upen_ppppid_HoCfg_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ppppid_mask
BitField Type: R/W
BitField Desc: PPP PID mask
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_ppppid_HoCfg_ppppid_mask_Bit_Start                                                        16
#define cAf6_upen_ppppid_HoCfg_ppppid_mask_Bit_End                                                          31
#define cAf6_upen_ppppid_HoCfg_ppppid_mask_Mask                                                      cBit31_16
#define cAf6_upen_ppppid_HoCfg_ppppid_mask_Shift                                                            16
#define cAf6_upen_ppppid_HoCfg_ppppid_mask_MaxVal                                                       0xffff
#define cAf6_upen_ppppid_HoCfg_ppppid_mask_MinVal                                                          0x0
#define cAf6_upen_ppppid_HoCfg_ppppid_mask_RstVal                                                       0xFFFF

/*--------------------------------------
BitField Name: ppppid_val
BitField Type: R/W
BitField Desc: PPP PIO value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_ppppid_HoCfg_ppppid_val_Bit_Start                                                          0
#define cAf6_upen_ppppid_HoCfg_ppppid_val_Bit_End                                                           15
#define cAf6_upen_ppppid_HoCfg_ppppid_val_Mask                                                        cBit15_0
#define cAf6_upen_ppppid_HoCfg_ppppid_val_Shift                                                              0
#define cAf6_upen_ppppid_HoCfg_ppppid_val_MaxVal                                                        0xffff
#define cAf6_upen_ppppid_HoCfg_ppppid_val_MinVal                                                           0x0
#define cAf6_upen_ppppid_HoCfg_ppppid_val_RstVal                                                   Reset_value


/*------------------------------------------------------------------------------
Reg Name   : ETH Type  global config
Reg Addr   : 0x0_0010-0015
Reg Formula: 0x0_0010 + $ppp_type
    Where  :
           + $ppp_type(0-5) : PPP_TYPE
Reg Desc   :
Use to configure ethernet type for ppp_type
Reset_value of ppp_type
ppp_type == 0: 0x0800, is Ipv4 packets
ppp_type == 1: 0x86DD, is Ipv6 packets
ppp_type == 2: 0x8847, is MPLS unicast packets
ppp_type == 3: 0x8848, is MPLS multicast packets
ppp_type == 4: 0xFEFE, is IS-IS packets
ppp_type == 5: 0x1234, is Ethernet transparent packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ethtyp_HoCfg_Base                                                                 0x00010
#define cAf6Reg_upen_ethtyp_HoCfg(ppptype)                                                 (0x00010+(ppptype))
#define cAf6Reg_upen_ethtyp_HoCfg_WidthVal                                                                  32
#define cAf6Reg_upen_ethtyp_HoCfg_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ethtyp_mask
BitField Type: R/W
BitField Desc: Eth Type  mask
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_ethtyp_HoCfg_ethtyp_mask_Bit_Start                                                        16
#define cAf6_upen_ethtyp_HoCfg_ethtyp_mask_Bit_End                                                          31
#define cAf6_upen_ethtyp_HoCfg_ethtyp_mask_Mask                                                      cBit31_16
#define cAf6_upen_ethtyp_HoCfg_ethtyp_mask_Shift                                                            16
#define cAf6_upen_ethtyp_HoCfg_ethtyp_mask_MaxVal                                                       0xffff
#define cAf6_upen_ethtyp_HoCfg_ethtyp_mask_MinVal                                                          0x0
#define cAf6_upen_ethtyp_HoCfg_ethtyp_mask_RstVal                                                       0xFFFF

/*--------------------------------------
BitField Name: ethtyp_val
BitField Type: R/W
BitField Desc: ETH Type value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_ethtyp_HoCfg_ethtyp_val_Bit_Start                                                          0
#define cAf6_upen_ethtyp_HoCfg_ethtyp_val_Bit_End                                                           15
#define cAf6_upen_ethtyp_HoCfg_ethtyp_val_Mask                                                        cBit15_0
#define cAf6_upen_ethtyp_HoCfg_ethtyp_val_Shift                                                              0
#define cAf6_upen_ethtyp_HoCfg_ethtyp_val_MaxVal                                                        0xffff
#define cAf6_upen_ethtyp_HoCfg_ethtyp_val_MinVal                                                           0x0
#define cAf6_upen_ethtyp_HoCfg_ethtyp_val_RstVal                                                        0x0800

/*------------------------------------------------------------------------------
Reg Name   : COUNTER HDLC GOOD PACKET HO DEC
Reg Addr   : 0x900 - 0x9FF
Reg Formula: 0x900+ $CID + $RO * 0x30
    Where  :
           + $CID (0-47) : Channel ID
           + $RO (0-1) : READ ONLY
Reg Desc   :
counter good packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ho_gpkt_cnt_Base                                                                    0x900
#define cAf6Reg_upen_ho_gpkt_cnt(CID, RO)                                              (0x900+(CID)+(RO)*0x30)
#define cAf6Reg_upen_ho_gpkt_cnt_WidthVal                                                                   32
#define cAf6Reg_upen_ho_gpkt_cnt_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cnt_gpkt
BitField Type: R/W
BitField Desc: vlaue of counter good packet
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_ho_gpkt_cnt_cnt_gpkt_Bit_Start                                                             0
#define cAf6_upen_ho_gpkt_cnt_cnt_gpkt_Bit_End                                                              15
#define cAf6_upen_ho_gpkt_cnt_cnt_gpkt_Mask                                                           cBit15_0
#define cAf6_upen_ho_gpkt_cnt_cnt_gpkt_Shift                                                                 0
#define cAf6_upen_ho_gpkt_cnt_cnt_gpkt_MaxVal                                                           0xffff
#define cAf6_upen_ho_gpkt_cnt_cnt_gpkt_MinVal                                                              0x0
#define cAf6_upen_ho_gpkt_cnt_cnt_gpkt_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER HDLC DROP PACKET HO DEC
Reg Addr   : 0xA00 - 0xAFF
Reg Formula: 0xA00+ $CID + $RO * 0x30
    Where  :
           + $CID (0-47) : Channel ID
           + $RO (0-1) : READ ONLY
Reg Desc   :
counter drop packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ho_drpkt_cnt_Base                                                                   0xA00
#define cAf6Reg_upen_ho_drpkt_cnt(CID, RO)                                             (0xA00+(CID)+(RO)*0x30)
#define cAf6Reg_upen_ho_drpkt_cnt_WidthVal                                                                  32
#define cAf6Reg_upen_ho_drpkt_cnt_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: cnt_drpkt
BitField Type: R/W
BitField Desc: vlaue of counter drop packet
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_ho_drpkt_cnt_cnt_drpkt_Bit_Start                                                           0
#define cAf6_upen_ho_drpkt_cnt_cnt_drpkt_Bit_End                                                            15
#define cAf6_upen_ho_drpkt_cnt_cnt_drpkt_Mask                                                         cBit15_0
#define cAf6_upen_ho_drpkt_cnt_cnt_drpkt_Shift                                                               0
#define cAf6_upen_ho_drpkt_cnt_cnt_drpkt_MaxVal                                                         0xffff
#define cAf6_upen_ho_drpkt_cnt_cnt_drpkt_MinVal                                                            0x0
#define cAf6_upen_ho_drpkt_cnt_cnt_drpkt_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : COUNTER HDLC ABORT PACKET HO DEC
Reg Addr   : 0xB00 - 0xBFF
Reg Formula: 0xB00+ $CID + $RO * 0x30
    Where  :
           + $CID (0-47) : Channel ID
           + $RO (0-1) : READ ONLY
Reg Desc   :
counter abort packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ho_abpkt_cnt_Base                                                                   0xB00
#define cAf6Reg_upen_ho_abpkt_cnt(CID, RO)                                             (0xB00+(CID)+(RO)*0x30)
#define cAf6Reg_upen_ho_abpkt_cnt_WidthVal                                                                  32
#define cAf6Reg_upen_ho_abpkt_cnt_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: cnt_abpkt
BitField Type: R/W
BitField Desc: vlaue of counter abort packet
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_ho_abpkt_cnt_cnt_abpkt_Bit_Start                                                           0
#define cAf6_upen_ho_abpkt_cnt_cnt_abpkt_Bit_End                                                            15
#define cAf6_upen_ho_abpkt_cnt_cnt_abpkt_Mask                                                         cBit15_0
#define cAf6_upen_ho_abpkt_cnt_cnt_abpkt_Shift                                                               0
#define cAf6_upen_ho_abpkt_cnt_cnt_abpkt_MaxVal                                                         0xffff
#define cAf6_upen_ho_abpkt_cnt_cnt_abpkt_MinVal                                                            0x0
#define cAf6_upen_ho_abpkt_cnt_cnt_abpkt_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : COUNTER HDLC GOOD PACKET LO DEC
Reg Addr   : 0x2800 - 0x2FFF
Reg Formula: 0x2800+ $CID + $RO * 0x400
    Where  :
           + $CID (0-1023) : Channel ID
           + $RO (0-1) : READ ONLY
Reg Desc   :
counter good packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_lo_gpkt_cnt_Base                                                                   0x2800
#define cAf6Reg_upen_lo_gpkt_cnt(CID, RO)                                            (0x2800+(CID)+(RO)*0x400)
#define cAf6Reg_upen_lo_gpkt_cnt_WidthVal                                                                   32
#define cAf6Reg_upen_lo_gpkt_cnt_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cnt_gpkt
BitField Type: R/W
BitField Desc: vlaue of counter good packet
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_lo_gpkt_cnt_cnt_gpkt_Bit_Start                                                             0
#define cAf6_upen_lo_gpkt_cnt_cnt_gpkt_Bit_End                                                              15
#define cAf6_upen_lo_gpkt_cnt_cnt_gpkt_Mask                                                           cBit15_0
#define cAf6_upen_lo_gpkt_cnt_cnt_gpkt_Shift                                                                 0
#define cAf6_upen_lo_gpkt_cnt_cnt_gpkt_MaxVal                                                           0xffff
#define cAf6_upen_lo_gpkt_cnt_cnt_gpkt_MinVal                                                              0x0
#define cAf6_upen_lo_gpkt_cnt_cnt_gpkt_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER HDLC DROP PACKET LO DEC
Reg Addr   : 0x3000 - 0x37FF
Reg Formula: 0x3000+ $CID + $RO * 0x400
    Where  :
           + $CID (0-1023) : Channel ID
           + $RO (0-1) : READ ONLY
Reg Desc   :
counter drop packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_lo_drpkt_cnt_Base                                                                  0x3000
#define cAf6Reg_upen_lo_drpkt_cnt(CID, RO)                                           (0x3000+(CID)+(RO)*0x400)
#define cAf6Reg_upen_lo_drpkt_cnt_WidthVal                                                                  32
#define cAf6Reg_upen_lo_drpkt_cnt_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: cnt_drpkt
BitField Type: R/W
BitField Desc: vlaue of counter drop packet
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_lo_drpkt_cnt_cnt_drpkt_Bit_Start                                                           0
#define cAf6_upen_lo_drpkt_cnt_cnt_drpkt_Bit_End                                                            15
#define cAf6_upen_lo_drpkt_cnt_cnt_drpkt_Mask                                                         cBit15_0
#define cAf6_upen_lo_drpkt_cnt_cnt_drpkt_Shift                                                               0
#define cAf6_upen_lo_drpkt_cnt_cnt_drpkt_MaxVal                                                         0xffff
#define cAf6_upen_lo_drpkt_cnt_cnt_drpkt_MinVal                                                            0x0
#define cAf6_upen_lo_drpkt_cnt_cnt_drpkt_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : COUNTER HDLC ABORT PACKET LO DEC
Reg Addr   : 0x3800 - 0x3FFF
Reg Formula: 0x3800+ $CID + $RO * 0x400
    Where  :
           + $CID (0-1023) : Channel ID
           + $RO (0-1) : READ ONLY
Reg Desc   :
counter abort packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_lo_abpkt_cnt_Base                                                                  0x3800
#define cAf6Reg_upen_lo_abpkt_cnt(CID, RO)                                           (0x3800+(CID)+(RO)*0x400)
#define cAf6Reg_upen_lo_abpkt_cnt_WidthVal                                                                  32
#define cAf6Reg_upen_lo_abpkt_cnt_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: cnt_abpkt
BitField Type: R/W
BitField Desc: vlaue of counter abort packet
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_lo_abpkt_cnt_cnt_abpkt_Bit_Start                                                           0
#define cAf6_upen_lo_abpkt_cnt_cnt_abpkt_Bit_End                                                            15
#define cAf6_upen_lo_abpkt_cnt_cnt_abpkt_Mask                                                         cBit15_0
#define cAf6_upen_lo_abpkt_cnt_cnt_abpkt_Shift                                                               0
#define cAf6_upen_lo_abpkt_cnt_cnt_abpkt_MaxVal                                                         0xffff
#define cAf6_upen_lo_abpkt_cnt_cnt_abpkt_MinVal                                                            0x0
#define cAf6_upen_lo_abpkt_cnt_cnt_abpkt_RstVal                                                            0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEENCAPPARSERREG_H_ */


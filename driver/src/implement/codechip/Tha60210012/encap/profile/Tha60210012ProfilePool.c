/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60210012ProfilePool.c
 *
 * Created Date: Jun 16, 2016
 *
 * Description : Profile pool
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/encap/profile/ThaProfileManager.h"
#include "../../../../default/encap/profile/profilepool/ThaProfilePoolInternal.h"
#include "Tha60210012ProfileManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012ProfilePool
    {
    tThaProfilePool super;
    }tTha60210012ProfilePool;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaProfilePoolMethods m_ThaProfilePoolOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaProfile ErrorProfileObjectCreate(ThaProfilePool self, uint32 profileId)
    {
    return Tha60210012ErrorProfileNew(self, profileId);
    }

static ThaProfile OamProfileObjectCreate(ThaProfilePool self, uint32 profileId)
    {
    return Tha60210012OamProfileNew(self, profileId);
    }

static ThaProfile NetworkProfileObjectCreate(ThaProfilePool self, uint32 profileId)
    {
    return Tha60210012NetworkProfileNew(self, profileId);
    }

static ThaProfile BcpProfileObjectCreate(ThaProfilePool self, uint32 profileId)
    {
    return Tha60210012BcpProfileNew(self, profileId);
    }

static ThaProfile FcnProfileObjectCreate(ThaProfilePool self, uint32 profileId)
    {
    return Tha60210012FcnProfileNew(self, profileId);
    }

static uint8 MaxNumNetworkProfile(ThaProfilePool self)
    {
    AtUnused(self);
    return 32;
    }

static uint8 MaxNumErrorProfile(ThaProfilePool self)
    {
    AtUnused(self);
    return 8;
    }

static uint8 MaxNumOamProfile(ThaProfilePool self)
    {
    AtUnused(self);
    return 16;
    }

static uint8 MaxNumBcpProfile(ThaProfilePool self)
    {
    AtUnused(self);
    return 8;
    }

static uint8 MaxNumFcnProfile(ThaProfilePool self)
    {
    AtUnused(self);
    return 8;
    }

static void OverrideThaProfilePool(ThaProfilePool self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaProfilePoolOverride, mMethodsGet(self), sizeof(m_ThaProfilePoolOverride));

        mMethodOverride(m_ThaProfilePoolOverride, ErrorProfileObjectCreate);
        mMethodOverride(m_ThaProfilePoolOverride, OamProfileObjectCreate);
        mMethodOverride(m_ThaProfilePoolOverride, NetworkProfileObjectCreate);
        mMethodOverride(m_ThaProfilePoolOverride, BcpProfileObjectCreate);
        mMethodOverride(m_ThaProfilePoolOverride, FcnProfileObjectCreate);
        mMethodOverride(m_ThaProfilePoolOverride, MaxNumFcnProfile);
        mMethodOverride(m_ThaProfilePoolOverride, MaxNumBcpProfile);
        mMethodOverride(m_ThaProfilePoolOverride, MaxNumOamProfile);
        mMethodOverride(m_ThaProfilePoolOverride, MaxNumErrorProfile);
        mMethodOverride(m_ThaProfilePoolOverride, MaxNumNetworkProfile);
        }

    mMethodsSet(self, &m_ThaProfilePoolOverride);
    }

static void Override(ThaProfilePool self)
    {
    OverrideThaProfilePool(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ProfilePool);
    }

static ThaProfilePool ObjectInit(ThaProfilePool self, ThaProfileManager manager)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaProfilePoolObjectInit(self, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaProfilePool Tha60210012ProfilePoolNew(ThaProfileManager manager)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaProfilePool newPool = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPool == NULL)
        return NULL;

    return ObjectInit(newPool, manager);
    }

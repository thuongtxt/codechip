/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60210012ProfileManager.c
 *
 * Created Date: Jun 16, 2016
 *
 * Description : Profile manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/encap/profile/ThaProfileManagerInternal.h"
#include "Tha60210012ProfileManager.h"
#include "../Tha60210012ModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012ProfileManager
    {
    tThaProfileManager super;
    }tTha60210012ProfileManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaProfileManagerMethods m_ThaProfileManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaProfilePool ProfilePoolObjectCreate(ThaProfileManager self)
    {
    return Tha60210012ProfilePoolNew(self);
    }

static uint8 RuleToHw(ThaProfileManager self, eThaProfileRule rule)
    {
    AtUnused(self);
    if (rule == cThaProfileRuleDrop)    return 0;
    if (rule == cThaProfileRuleData)    return 1;
    if (rule == cThaProfileRuleControl) return 2;
    if (rule == cThaProfileRuleCorrupt) return 3;

    return 0;
    }

static eThaProfileRule HwToRule(ThaProfileManager self, uint8 hwValue)
    {
    AtUnused(self);
    if (hwValue == 0) return cThaProfileRuleDrop;
    if (hwValue == 1) return cThaProfileRuleData;
    if (hwValue == 2) return cThaProfileRuleControl;
    if (hwValue == 3) return cThaProfileRuleCorrupt;
    return cThaProfileRuleUnknown;
    }

static void OverrideThaProfileManager(ThaProfileManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaProfileManagerOverride, mMethodsGet(self), sizeof(m_ThaProfileManagerOverride));

        mMethodOverride(m_ThaProfileManagerOverride, ProfilePoolObjectCreate);
        mMethodOverride(m_ThaProfileManagerOverride, RuleToHw);
        mMethodOverride(m_ThaProfileManagerOverride, HwToRule);
        }

    mMethodsSet(self, &m_ThaProfileManagerOverride);
    }

static void Override(ThaProfileManager self)
    {
    OverrideThaProfileManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ProfileManager);
    }

static ThaProfileManager ObjectInit(ThaProfileManager self, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaProfileManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaProfileManager Tha60210012ProfileManagerNew(AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaProfileManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    return ObjectInit(newManager, module);
    }

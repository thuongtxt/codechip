/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60210012OamProfile.c
 *
 * Created Date: Jun 27, 2016
 *
 * Description : 60210012 OAM profile
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/encap/profile/profilepool/ThaProfileInternal.h"
#include "../../eth/Tha60210012ModuleEthFlowLookupReg.h"
#include "Tha60210012ProfileReg.h"
#include "Tha60210012ProfileManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012OamProfile
    {
    tThaOamProfile super;
    }tTha60210012OamProfile;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaProfileMethods m_ThaProfileOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(void)
    {
    return cThaModuleEthFlowLookupBaseAddress;
    }

static uint32 Offset(ThaProfile self)
    {
    return (ThaProfileProfileIdGet(self) + BaseAddress());
    }

static eAtRet KeyValueSet(ThaProfile self, uint32 hwValue)
    {
    ThaProfileHwWrite(self, cAf6Reg_upen_oam_profiles_Base + Offset(self), hwValue);
    return cAtOk;
    }

static uint32 KeyValueGet(ThaProfile self)
    {
    return ThaProfileHwRead(self, cAf6Reg_upen_oam_profiles_Base + Offset(self));
    }

static uint8 TypeBitShift(ThaProfile self, uint16 type)
    {
    AtUnused(self);
    switch (type)
        {
        case cThaProfileOamTypeLCP:     return 0;
        case cThaProfileOamTypeNCP:     return 2;
        case cThaProfileOamTypeQ933:    return 4;
        case cThaProfileOamTypeCiscoLMI:return 6;
        case cThaProfileOamTypeCLNP:    return 8;
        case cThaProfileOamTypeESIS:    return 10;
        case cThaProfileOamTypeISIS:    return 12;
        case cThaProfileOamTypeLIPCM:   return 14;
        case cThaProfileOamTypeAll:
        default:                        return 0;
        }
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012OamProfile);
    }

static void OverrideThaProfile(ThaProfile self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaProfileOverride, mMethodsGet(self), sizeof(m_ThaProfileOverride));

        mMethodOverride(m_ThaProfileOverride, KeyValueSet);
        mMethodOverride(m_ThaProfileOverride, KeyValueGet);
        mMethodOverride(m_ThaProfileOverride, TypeBitShift);
        }

    mMethodsSet(self, &m_ThaProfileOverride);
    }

static void Override(ThaProfile self)
    {
    OverrideThaProfile(self);
    }

static ThaProfile ObjectInit(ThaProfile self, ThaProfilePool pool, uint32 profileId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaOamProfileObjectInit(self, pool, profileId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaProfile Tha60210012OamProfileNew(ThaProfilePool pool, uint32 profileId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaProfile newProfile = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    return ObjectInit(newProfile, pool, profileId);
    }

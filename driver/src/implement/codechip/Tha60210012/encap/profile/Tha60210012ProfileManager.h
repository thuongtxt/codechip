/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60210012ProfileManager.h
 * 
 * Created Date: Jun 17, 2016
 *
 * Description : Profile
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PROFILEMANAGER_H_
#define _THA60210012PROFILEMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaProfilePool Tha60210012ProfilePoolNew(ThaProfileManager manager);
ThaProfile Tha60210012ErrorProfileNew(ThaProfilePool pool, uint32 profileId);
ThaProfile Tha60210012OamProfileNew(ThaProfilePool pool, uint32 profileId);
ThaProfile Tha60210012NetworkProfileNew(ThaProfilePool pool, uint32 profileId);
ThaProfile Tha60210012BcpProfileNew(ThaProfilePool pool, uint32 profileId);
ThaProfile Tha60210012FcnProfileNew(ThaProfilePool pool, uint32 profileId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PROFILEMANAGER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60210012ProfileReg.h
 * 
 * Created Date: Jun 16, 2016
 *
 * Description : Register of profile
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PROFILEREG_H_
#define _THA60210012PROFILEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : IgRule Params
Reg Addr   : 0x00_C000
Reg Formula:
    Where  :
Reg Desc   :
This register is used to get params of Ingress Rule Check Engine
We have 5 type profile correspond for type = {FCN,BCP,ERR,OAM,NLP}.
Number of profiles eache type is 2^type_bit.
Exam NLP_bit = 5 -> NLP have 2^32 = 32 profiles.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_params_Base                                                                      0x00C000
#define cAf6Reg_upen_params                                                                           0x00C000
#define cAf6Reg_upen_params_WidthVal                                                                        32
#define cAf6Reg_upen_params_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: Eng_Id
BitField Type: RO
BitField Desc: Engine code
BitField Bits: [31:20]
--------------------------------------*/
#define cAf6_upen_params_Eng_Id_Bit_Start                                                                   20
#define cAf6_upen_params_Eng_Id_Bit_End                                                                     31
#define cAf6_upen_params_Eng_Id_Mask                                                                 cBit31_20
#define cAf6_upen_params_Eng_Id_Shift                                                                       20
#define cAf6_upen_params_Eng_Id_MaxVal                                                                   0xfff
#define cAf6_upen_params_Eng_Id_MinVal                                                                     0x0
#define cAf6_upen_params_Eng_Id_RstVal                                                                   0xA43

/*--------------------------------------
BitField Name: FCN_bit
BitField Type: RO
BitField Desc: FCN_bit
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_upen_params_FCN_bit_Bit_Start                                                                  16
#define cAf6_upen_params_FCN_bit_Bit_End                                                                    19
#define cAf6_upen_params_FCN_bit_Mask                                                                cBit19_16
#define cAf6_upen_params_FCN_bit_Shift                                                                      16
#define cAf6_upen_params_FCN_bit_MaxVal                                                                    0xf
#define cAf6_upen_params_FCN_bit_MinVal                                                                    0x0
#define cAf6_upen_params_FCN_bit_RstVal                                                                    0x3

/*--------------------------------------
BitField Name: BCP_bit
BitField Type: RO
BitField Desc: BCP_bit
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_upen_params_BCP_bit_Bit_Start                                                                  12
#define cAf6_upen_params_BCP_bit_Bit_End                                                                    15
#define cAf6_upen_params_BCP_bit_Mask                                                                cBit15_12
#define cAf6_upen_params_BCP_bit_Shift                                                                      12
#define cAf6_upen_params_BCP_bit_MaxVal                                                                    0xf
#define cAf6_upen_params_BCP_bit_MinVal                                                                    0x0
#define cAf6_upen_params_BCP_bit_RstVal                                                                    0x3

/*--------------------------------------
BitField Name: ERR_bit
BitField Type: RO
BitField Desc: ERR_bit
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_upen_params_ERR_bit_Bit_Start                                                                   8
#define cAf6_upen_params_ERR_bit_Bit_End                                                                    11
#define cAf6_upen_params_ERR_bit_Mask                                                                 cBit11_8
#define cAf6_upen_params_ERR_bit_Shift                                                                       8
#define cAf6_upen_params_ERR_bit_MaxVal                                                                    0xf
#define cAf6_upen_params_ERR_bit_MinVal                                                                    0x0
#define cAf6_upen_params_ERR_bit_RstVal                                                                    0x3

/*--------------------------------------
BitField Name: OAM_bit
BitField Type: RO
BitField Desc: OAM_bit
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_upen_params_OAM_bit_Bit_Start                                                                   4
#define cAf6_upen_params_OAM_bit_Bit_End                                                                     7
#define cAf6_upen_params_OAM_bit_Mask                                                                  cBit7_4
#define cAf6_upen_params_OAM_bit_Shift                                                                       4
#define cAf6_upen_params_OAM_bit_MaxVal                                                                    0xf
#define cAf6_upen_params_OAM_bit_MinVal                                                                    0x0
#define cAf6_upen_params_OAM_bit_RstVal                                                                    0x4

/*--------------------------------------
BitField Name: NLP_bit
BitField Type: RO
BitField Desc: NLP_bit
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_params_NLP_bit_Bit_Start                                                                   0
#define cAf6_upen_params_NLP_bit_Bit_End                                                                     3
#define cAf6_upen_params_NLP_bit_Mask                                                                  cBit3_0
#define cAf6_upen_params_NLP_bit_Shift                                                                       0
#define cAf6_upen_params_NLP_bit_MaxVal                                                                    0xf
#define cAf6_upen_params_NLP_bit_MinVal                                                                    0x0
#define cAf6_upen_params_NLP_bit_RstVal                                                                    0x5


/*------------------------------------------------------------------------------
Reg Name   : IgRule NLP Profiles
Reg Addr   : 0x00_C100
Reg Formula: 0x00_C100 + $nlp_profiles
    Where  :
           + $nlp_profiles(0-31) nlp_profile_id
Reg Desc   :
This register is used to config profile forward (fwd) rule for each network layer protocol (NLP)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_nlp_profiles_Base                                                                0x00C100
#define cAf6Reg_upen_nlp_profiles(nlpprofiles)                                        (0x00C100+(nlpprofiles))
#define cAf6Reg_upen_nlp_profiles_WidthVal                                                                  32
#define cAf6Reg_upen_nlp_profiles_WriteMask                                                                0x0

#define cAf6_upen_nlp_profiles_FwdRule_NLP_Mask(profileId)                            (cBit1_0 << (profileId))
#define cAf6_upen_nlp_profiles_FwdRule_NLP_Shift(profileId)                           (0 + (profileId))

/*--------------------------------------
BitField Name: FwdRule_NLP_15
BitField Type: RW
BitField Desc: FwdRule for NLP#15
BitField Bits: [31:30]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_15_Bit_Start                                                     30
#define cAf6_upen_nlp_profiles_FwdRule_NLP_15_Bit_End                                                       31
#define cAf6_upen_nlp_profiles_FwdRule_NLP_15_Mask                                                   cBit31_30
#define cAf6_upen_nlp_profiles_FwdRule_NLP_15_Shift                                                         30
#define cAf6_upen_nlp_profiles_FwdRule_NLP_15_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_15_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_15_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_14
BitField Type: RW
BitField Desc: FwdRule for NLP#14
BitField Bits: [27:28]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_14_Bit_Start                                                     28
#define cAf6_upen_nlp_profiles_FwdRule_NLP_14_Bit_End                                                       27
#define cAf6_upen_nlp_profiles_FwdRule_NLP_14_Mask                                                   cBit27_28
#define cAf6_upen_nlp_profiles_FwdRule_NLP_14_Shift                                                         28
#define cAf6_upen_nlp_profiles_FwdRule_NLP_14_MaxVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_14_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_14_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_13
BitField Type: RW
BitField Desc: FwdRule for NLP#13
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_13_Bit_Start                                                     26
#define cAf6_upen_nlp_profiles_FwdRule_NLP_13_Bit_End                                                       27
#define cAf6_upen_nlp_profiles_FwdRule_NLP_13_Mask                                                   cBit27_26
#define cAf6_upen_nlp_profiles_FwdRule_NLP_13_Shift                                                         26
#define cAf6_upen_nlp_profiles_FwdRule_NLP_13_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_13_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_13_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_12
BitField Type: RW
BitField Desc: FwdRule for NLP#12
BitField Bits: [25:24]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_12_Bit_Start                                                     24
#define cAf6_upen_nlp_profiles_FwdRule_NLP_12_Bit_End                                                       25
#define cAf6_upen_nlp_profiles_FwdRule_NLP_12_Mask                                                   cBit25_24
#define cAf6_upen_nlp_profiles_FwdRule_NLP_12_Shift                                                         24
#define cAf6_upen_nlp_profiles_FwdRule_NLP_12_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_12_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_12_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_11
BitField Type: RW
BitField Desc: FwdRule for NLP#11
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_11_Bit_Start                                                     22
#define cAf6_upen_nlp_profiles_FwdRule_NLP_11_Bit_End                                                       23
#define cAf6_upen_nlp_profiles_FwdRule_NLP_11_Mask                                                   cBit23_22
#define cAf6_upen_nlp_profiles_FwdRule_NLP_11_Shift                                                         22
#define cAf6_upen_nlp_profiles_FwdRule_NLP_11_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_11_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_11_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_10
BitField Type: RW
BitField Desc: FwdRule for NLP#10
BitField Bits: [21:20]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_10_Bit_Start                                                     20
#define cAf6_upen_nlp_profiles_FwdRule_NLP_10_Bit_End                                                       21
#define cAf6_upen_nlp_profiles_FwdRule_NLP_10_Mask                                                   cBit21_20
#define cAf6_upen_nlp_profiles_FwdRule_NLP_10_Shift                                                         20
#define cAf6_upen_nlp_profiles_FwdRule_NLP_10_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_10_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_10_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_09
BitField Type: RW
BitField Desc: FwdRule for NLP#09
BitField Bits: [19:18]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_09_Bit_Start                                                     18
#define cAf6_upen_nlp_profiles_FwdRule_NLP_09_Bit_End                                                       19
#define cAf6_upen_nlp_profiles_FwdRule_NLP_09_Mask                                                   cBit19_18
#define cAf6_upen_nlp_profiles_FwdRule_NLP_09_Shift                                                         18
#define cAf6_upen_nlp_profiles_FwdRule_NLP_09_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_09_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_09_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_08
BitField Type: RW
BitField Desc: FwdRule for NLP#08
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_08_Bit_Start                                                     16
#define cAf6_upen_nlp_profiles_FwdRule_NLP_08_Bit_End                                                       17
#define cAf6_upen_nlp_profiles_FwdRule_NLP_08_Mask                                                   cBit17_16
#define cAf6_upen_nlp_profiles_FwdRule_NLP_08_Shift                                                         16
#define cAf6_upen_nlp_profiles_FwdRule_NLP_08_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_08_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_08_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_07
BitField Type: RW
BitField Desc: FwdRule for NLP#07
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_07_Bit_Start                                                     14
#define cAf6_upen_nlp_profiles_FwdRule_NLP_07_Bit_End                                                       15
#define cAf6_upen_nlp_profiles_FwdRule_NLP_07_Mask                                                   cBit15_14
#define cAf6_upen_nlp_profiles_FwdRule_NLP_07_Shift                                                         14
#define cAf6_upen_nlp_profiles_FwdRule_NLP_07_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_07_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_07_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_06
BitField Type: RW
BitField Desc: FwdRule for NLP#06
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_06_Bit_Start                                                     12
#define cAf6_upen_nlp_profiles_FwdRule_NLP_06_Bit_End                                                       13
#define cAf6_upen_nlp_profiles_FwdRule_NLP_06_Mask                                                   cBit13_12
#define cAf6_upen_nlp_profiles_FwdRule_NLP_06_Shift                                                         12
#define cAf6_upen_nlp_profiles_FwdRule_NLP_06_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_06_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_06_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_05
BitField Type: RW
BitField Desc: FwdRule for NLP#05
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_05_Bit_Start                                                     10
#define cAf6_upen_nlp_profiles_FwdRule_NLP_05_Bit_End                                                       11
#define cAf6_upen_nlp_profiles_FwdRule_NLP_05_Mask                                                   cBit11_10
#define cAf6_upen_nlp_profiles_FwdRule_NLP_05_Shift                                                         10
#define cAf6_upen_nlp_profiles_FwdRule_NLP_05_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_05_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_05_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_04
BitField Type: RW
BitField Desc: FwdRule for NLP#04
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_04_Bit_Start                                                      8
#define cAf6_upen_nlp_profiles_FwdRule_NLP_04_Bit_End                                                        9
#define cAf6_upen_nlp_profiles_FwdRule_NLP_04_Mask                                                     cBit9_8
#define cAf6_upen_nlp_profiles_FwdRule_NLP_04_Shift                                                          8
#define cAf6_upen_nlp_profiles_FwdRule_NLP_04_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_04_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_04_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_03
BitField Type: RW
BitField Desc: FwdRule for NLP#03
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_03_Bit_Start                                                      6
#define cAf6_upen_nlp_profiles_FwdRule_NLP_03_Bit_End                                                        7
#define cAf6_upen_nlp_profiles_FwdRule_NLP_03_Mask                                                     cBit7_6
#define cAf6_upen_nlp_profiles_FwdRule_NLP_03_Shift                                                          6
#define cAf6_upen_nlp_profiles_FwdRule_NLP_03_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_03_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_03_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_02
BitField Type: RW
BitField Desc: FwdRule for NLP#02
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_02_Bit_Start                                                      4
#define cAf6_upen_nlp_profiles_FwdRule_NLP_02_Bit_End                                                        5
#define cAf6_upen_nlp_profiles_FwdRule_NLP_02_Mask                                                     cBit5_4
#define cAf6_upen_nlp_profiles_FwdRule_NLP_02_Shift                                                          4
#define cAf6_upen_nlp_profiles_FwdRule_NLP_02_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_02_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_02_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_01
BitField Type: RW
BitField Desc: FwdRule for NLP#01
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_01_Bit_Start                                                      2
#define cAf6_upen_nlp_profiles_FwdRule_NLP_01_Bit_End                                                        3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_01_Mask                                                     cBit3_2
#define cAf6_upen_nlp_profiles_FwdRule_NLP_01_Shift                                                          2
#define cAf6_upen_nlp_profiles_FwdRule_NLP_01_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_01_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_01_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_NLP_00
BitField Type: RW
BitField Desc: FwdRule for NLP#00
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_upen_nlp_profiles_FwdRule_NLP_00_Bit_Start                                                      0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_00_Bit_End                                                        1
#define cAf6_upen_nlp_profiles_FwdRule_NLP_00_Mask                                                     cBit1_0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_00_Shift                                                          0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_00_MaxVal                                                       0x3
#define cAf6_upen_nlp_profiles_FwdRule_NLP_00_MinVal                                                       0x0
#define cAf6_upen_nlp_profiles_FwdRule_NLP_00_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : IgRule OAM Profiles
Reg Addr   : 0x00_C200
Reg Formula: 0x00_C200 + $oam_profiles
    Where  :
           + $oam_profiles(0-15) oam_profile_id
Reg Desc   :
This register is used to config profile forward (fwd) rule for OAM layer protocol (OAM)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_oam_profiles_Base                                                                0x00C200
#define cAf6Reg_upen_oam_profiles(oamprofiles)                                        (0x00C200+(oamprofiles))
#define cAf6Reg_upen_oam_profiles_WidthVal                                                                  32
#define cAf6Reg_upen_oam_profiles_WriteMask                                                                0x0

#define cAf6_upen_oam_profiles_FwdRule_OAM_Mask(profileId)                            (cBit1_0 << (profileId))
#define cAf6_upen_oam_profiles_FwdRule_OAM_Shift(profileId)                           (0 + (profileId))

/*--------------------------------------
BitField Name: FwdRule_OAM_15
BitField Type: RW
BitField Desc: FwdRule for OAM#15
BitField Bits: [31:30]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_15_Bit_Start                                                     30
#define cAf6_upen_oam_profiles_FwdRule_OAM_15_Bit_End                                                       31
#define cAf6_upen_oam_profiles_FwdRule_OAM_15_Mask                                                   cBit31_30
#define cAf6_upen_oam_profiles_FwdRule_OAM_15_Shift                                                         30
#define cAf6_upen_oam_profiles_FwdRule_OAM_15_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_15_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_15_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_14
BitField Type: RW
BitField Desc: FwdRule for OAM#14
BitField Bits: [27:28]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_14_Bit_Start                                                     28
#define cAf6_upen_oam_profiles_FwdRule_OAM_14_Bit_End                                                       27
#define cAf6_upen_oam_profiles_FwdRule_OAM_14_Mask                                                   cBit27_28
#define cAf6_upen_oam_profiles_FwdRule_OAM_14_Shift                                                         28
#define cAf6_upen_oam_profiles_FwdRule_OAM_14_MaxVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_14_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_14_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_13
BitField Type: RW
BitField Desc: FwdRule for OAM#13
BitField Bits: [27:26]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_13_Bit_Start                                                     26
#define cAf6_upen_oam_profiles_FwdRule_OAM_13_Bit_End                                                       27
#define cAf6_upen_oam_profiles_FwdRule_OAM_13_Mask                                                   cBit27_26
#define cAf6_upen_oam_profiles_FwdRule_OAM_13_Shift                                                         26
#define cAf6_upen_oam_profiles_FwdRule_OAM_13_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_13_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_13_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_12
BitField Type: RW
BitField Desc: FwdRule for OAM#12
BitField Bits: [25:24]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_12_Bit_Start                                                     24
#define cAf6_upen_oam_profiles_FwdRule_OAM_12_Bit_End                                                       25
#define cAf6_upen_oam_profiles_FwdRule_OAM_12_Mask                                                   cBit25_24
#define cAf6_upen_oam_profiles_FwdRule_OAM_12_Shift                                                         24
#define cAf6_upen_oam_profiles_FwdRule_OAM_12_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_12_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_12_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_11
BitField Type: RW
BitField Desc: FwdRule for OAM#11
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_11_Bit_Start                                                     22
#define cAf6_upen_oam_profiles_FwdRule_OAM_11_Bit_End                                                       23
#define cAf6_upen_oam_profiles_FwdRule_OAM_11_Mask                                                   cBit23_22
#define cAf6_upen_oam_profiles_FwdRule_OAM_11_Shift                                                         22
#define cAf6_upen_oam_profiles_FwdRule_OAM_11_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_11_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_11_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_10
BitField Type: RW
BitField Desc: FwdRule for OAM#10
BitField Bits: [21:20]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_10_Bit_Start                                                     20
#define cAf6_upen_oam_profiles_FwdRule_OAM_10_Bit_End                                                       21
#define cAf6_upen_oam_profiles_FwdRule_OAM_10_Mask                                                   cBit21_20
#define cAf6_upen_oam_profiles_FwdRule_OAM_10_Shift                                                         20
#define cAf6_upen_oam_profiles_FwdRule_OAM_10_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_10_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_10_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_09
BitField Type: RW
BitField Desc: FwdRule for OAM#09
BitField Bits: [19:18]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_09_Bit_Start                                                     18
#define cAf6_upen_oam_profiles_FwdRule_OAM_09_Bit_End                                                       19
#define cAf6_upen_oam_profiles_FwdRule_OAM_09_Mask                                                   cBit19_18
#define cAf6_upen_oam_profiles_FwdRule_OAM_09_Shift                                                         18
#define cAf6_upen_oam_profiles_FwdRule_OAM_09_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_09_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_09_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_08
BitField Type: RW
BitField Desc: FwdRule for OAM#08
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_08_Bit_Start                                                     16
#define cAf6_upen_oam_profiles_FwdRule_OAM_08_Bit_End                                                       17
#define cAf6_upen_oam_profiles_FwdRule_OAM_08_Mask                                                   cBit17_16
#define cAf6_upen_oam_profiles_FwdRule_OAM_08_Shift                                                         16
#define cAf6_upen_oam_profiles_FwdRule_OAM_08_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_08_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_08_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_07
BitField Type: RW
BitField Desc: FwdRule for OAM#07
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_07_Bit_Start                                                     14
#define cAf6_upen_oam_profiles_FwdRule_OAM_07_Bit_End                                                       15
#define cAf6_upen_oam_profiles_FwdRule_OAM_07_Mask                                                   cBit15_14
#define cAf6_upen_oam_profiles_FwdRule_OAM_07_Shift                                                         14
#define cAf6_upen_oam_profiles_FwdRule_OAM_07_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_07_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_07_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_06
BitField Type: RW
BitField Desc: FwdRule for OAM#06
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_06_Bit_Start                                                     12
#define cAf6_upen_oam_profiles_FwdRule_OAM_06_Bit_End                                                       13
#define cAf6_upen_oam_profiles_FwdRule_OAM_06_Mask                                                   cBit13_12
#define cAf6_upen_oam_profiles_FwdRule_OAM_06_Shift                                                         12
#define cAf6_upen_oam_profiles_FwdRule_OAM_06_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_06_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_06_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_05
BitField Type: RW
BitField Desc: FwdRule for OAM#05
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_05_Bit_Start                                                     10
#define cAf6_upen_oam_profiles_FwdRule_OAM_05_Bit_End                                                       11
#define cAf6_upen_oam_profiles_FwdRule_OAM_05_Mask                                                   cBit11_10
#define cAf6_upen_oam_profiles_FwdRule_OAM_05_Shift                                                         10
#define cAf6_upen_oam_profiles_FwdRule_OAM_05_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_05_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_05_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_04
BitField Type: RW
BitField Desc: FwdRule for OAM#04
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_04_Bit_Start                                                      8
#define cAf6_upen_oam_profiles_FwdRule_OAM_04_Bit_End                                                        9
#define cAf6_upen_oam_profiles_FwdRule_OAM_04_Mask                                                     cBit9_8
#define cAf6_upen_oam_profiles_FwdRule_OAM_04_Shift                                                          8
#define cAf6_upen_oam_profiles_FwdRule_OAM_04_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_04_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_04_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_03
BitField Type: RW
BitField Desc: FwdRule for OAM#03
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_03_Bit_Start                                                      6
#define cAf6_upen_oam_profiles_FwdRule_OAM_03_Bit_End                                                        7
#define cAf6_upen_oam_profiles_FwdRule_OAM_03_Mask                                                     cBit7_6
#define cAf6_upen_oam_profiles_FwdRule_OAM_03_Shift                                                          6
#define cAf6_upen_oam_profiles_FwdRule_OAM_03_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_03_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_03_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_02
BitField Type: RW
BitField Desc: FwdRule for OAM#02
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_02_Bit_Start                                                      4
#define cAf6_upen_oam_profiles_FwdRule_OAM_02_Bit_End                                                        5
#define cAf6_upen_oam_profiles_FwdRule_OAM_02_Mask                                                     cBit5_4
#define cAf6_upen_oam_profiles_FwdRule_OAM_02_Shift                                                          4
#define cAf6_upen_oam_profiles_FwdRule_OAM_02_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_02_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_02_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_01
BitField Type: RW
BitField Desc: FwdRule for OAM#01
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_01_Bit_Start                                                      2
#define cAf6_upen_oam_profiles_FwdRule_OAM_01_Bit_End                                                        3
#define cAf6_upen_oam_profiles_FwdRule_OAM_01_Mask                                                     cBit3_2
#define cAf6_upen_oam_profiles_FwdRule_OAM_01_Shift                                                          2
#define cAf6_upen_oam_profiles_FwdRule_OAM_01_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_01_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_01_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_OAM_00
BitField Type: RW
BitField Desc: FwdRule for OAM#00
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_upen_oam_profiles_FwdRule_OAM_00_Bit_Start                                                      0
#define cAf6_upen_oam_profiles_FwdRule_OAM_00_Bit_End                                                        1
#define cAf6_upen_oam_profiles_FwdRule_OAM_00_Mask                                                     cBit1_0
#define cAf6_upen_oam_profiles_FwdRule_OAM_00_Shift                                                          0
#define cAf6_upen_oam_profiles_FwdRule_OAM_00_MaxVal                                                       0x3
#define cAf6_upen_oam_profiles_FwdRule_OAM_00_MinVal                                                       0x0
#define cAf6_upen_oam_profiles_FwdRule_OAM_00_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : IgRule ERR Profiles
Reg Addr   : 0x00_C300
Reg Formula: 0x00_C300 + $err_profiles
    Where  :
           + $err_profiles(0-7) err_profile_id
Reg Desc   :
This register is used to config profile forward (fwd) rule for ERR status (ERR)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_err_profiles_Base                                                                0x00C300
#define cAf6Reg_upen_err_profiles(errprofiles)                                        (0x00C300+(errprofiles))
#define cAf6Reg_upen_err_profiles_WidthVal                                                                  32
#define cAf6Reg_upen_err_profiles_WriteMask                                                                0x0

#define cAf6_upen_err_profiles_FwdRule_ER_Mask(profileId)                             (cBit1_0 << (profileId))
#define cAf6_upen_err_profiles_FwdRule_ER_Shift(profileId)                            (0 + (profileId))

/*--------------------------------------
BitField Name: FwdRule_ERR_07
BitField Type: RW
BitField Desc: FwdRule for ERR#07
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_upen_err_profiles_FwdRule_ERR_07_Bit_Start                                                     14
#define cAf6_upen_err_profiles_FwdRule_ERR_07_Bit_End                                                       15
#define cAf6_upen_err_profiles_FwdRule_ERR_07_Mask                                                   cBit15_14
#define cAf6_upen_err_profiles_FwdRule_ERR_07_Shift                                                         14
#define cAf6_upen_err_profiles_FwdRule_ERR_07_MaxVal                                                       0x3
#define cAf6_upen_err_profiles_FwdRule_ERR_07_MinVal                                                       0x0
#define cAf6_upen_err_profiles_FwdRule_ERR_07_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_ERR_06
BitField Type: RW
BitField Desc: FwdRule for ERR#06
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_upen_err_profiles_FwdRule_ERR_06_Bit_Start                                                     12
#define cAf6_upen_err_profiles_FwdRule_ERR_06_Bit_End                                                       13
#define cAf6_upen_err_profiles_FwdRule_ERR_06_Mask                                                   cBit13_12
#define cAf6_upen_err_profiles_FwdRule_ERR_06_Shift                                                         12
#define cAf6_upen_err_profiles_FwdRule_ERR_06_MaxVal                                                       0x3
#define cAf6_upen_err_profiles_FwdRule_ERR_06_MinVal                                                       0x0
#define cAf6_upen_err_profiles_FwdRule_ERR_06_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_ERR_05
BitField Type: RW
BitField Desc: FwdRule for ERR#05
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_upen_err_profiles_FwdRule_ERR_05_Bit_Start                                                     10
#define cAf6_upen_err_profiles_FwdRule_ERR_05_Bit_End                                                       11
#define cAf6_upen_err_profiles_FwdRule_ERR_05_Mask                                                   cBit11_10
#define cAf6_upen_err_profiles_FwdRule_ERR_05_Shift                                                         10
#define cAf6_upen_err_profiles_FwdRule_ERR_05_MaxVal                                                       0x3
#define cAf6_upen_err_profiles_FwdRule_ERR_05_MinVal                                                       0x0
#define cAf6_upen_err_profiles_FwdRule_ERR_05_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_ERR_04
BitField Type: RW
BitField Desc: FwdRule for ERR#04
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_upen_err_profiles_FwdRule_ERR_04_Bit_Start                                                      8
#define cAf6_upen_err_profiles_FwdRule_ERR_04_Bit_End                                                        9
#define cAf6_upen_err_profiles_FwdRule_ERR_04_Mask                                                     cBit9_8
#define cAf6_upen_err_profiles_FwdRule_ERR_04_Shift                                                          8
#define cAf6_upen_err_profiles_FwdRule_ERR_04_MaxVal                                                       0x3
#define cAf6_upen_err_profiles_FwdRule_ERR_04_MinVal                                                       0x0
#define cAf6_upen_err_profiles_FwdRule_ERR_04_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_ERR_03
BitField Type: RW
BitField Desc: FwdRule for ERR#03
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_upen_err_profiles_FwdRule_ERR_03_Bit_Start                                                      6
#define cAf6_upen_err_profiles_FwdRule_ERR_03_Bit_End                                                        7
#define cAf6_upen_err_profiles_FwdRule_ERR_03_Mask                                                     cBit7_6
#define cAf6_upen_err_profiles_FwdRule_ERR_03_Shift                                                          6
#define cAf6_upen_err_profiles_FwdRule_ERR_03_MaxVal                                                       0x3
#define cAf6_upen_err_profiles_FwdRule_ERR_03_MinVal                                                       0x0
#define cAf6_upen_err_profiles_FwdRule_ERR_03_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_ERR_02
BitField Type: RW
BitField Desc: FwdRule for ERR#02
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_upen_err_profiles_FwdRule_ERR_02_Bit_Start                                                      4
#define cAf6_upen_err_profiles_FwdRule_ERR_02_Bit_End                                                        5
#define cAf6_upen_err_profiles_FwdRule_ERR_02_Mask                                                     cBit5_4
#define cAf6_upen_err_profiles_FwdRule_ERR_02_Shift                                                          4
#define cAf6_upen_err_profiles_FwdRule_ERR_02_MaxVal                                                       0x3
#define cAf6_upen_err_profiles_FwdRule_ERR_02_MinVal                                                       0x0
#define cAf6_upen_err_profiles_FwdRule_ERR_02_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_ERR_01
BitField Type: RW
BitField Desc: FwdRule for ERR#01
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_upen_err_profiles_FwdRule_ERR_01_Bit_Start                                                      2
#define cAf6_upen_err_profiles_FwdRule_ERR_01_Bit_End                                                        3
#define cAf6_upen_err_profiles_FwdRule_ERR_01_Mask                                                     cBit3_2
#define cAf6_upen_err_profiles_FwdRule_ERR_01_Shift                                                          2
#define cAf6_upen_err_profiles_FwdRule_ERR_01_MaxVal                                                       0x3
#define cAf6_upen_err_profiles_FwdRule_ERR_01_MinVal                                                       0x0
#define cAf6_upen_err_profiles_FwdRule_ERR_01_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_ERR_00
BitField Type: RW
BitField Desc: FwdRule for ERR#00
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_upen_err_profiles_FwdRule_ERR_00_Bit_Start                                                      0
#define cAf6_upen_err_profiles_FwdRule_ERR_00_Bit_End                                                        1
#define cAf6_upen_err_profiles_FwdRule_ERR_00_Mask                                                     cBit1_0
#define cAf6_upen_err_profiles_FwdRule_ERR_00_Shift                                                          0
#define cAf6_upen_err_profiles_FwdRule_ERR_00_MaxVal                                                       0x3
#define cAf6_upen_err_profiles_FwdRule_ERR_00_MinVal                                                       0x0
#define cAf6_upen_err_profiles_FwdRule_ERR_00_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : IgRule BCP Profiles
Reg Addr   : 0x00_C400
Reg Formula: 0x00_C400 + $bcp_profiles
    Where  :
           + $bcp_profiles(0-7) bcp_profile_id
Reg Desc   :
This register is used to config profile forward (fwd) rule for BCP status (BCP)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_bcp_profiles_Base                                                                0x00C400
#define cAf6Reg_upen_bcp_profiles(bcpprofiles)                                        (0x00C400+(bcpprofiles))
#define cAf6Reg_upen_bcp_profiles_WidthVal                                                                  32
#define cAf6Reg_upen_bcp_profiles_WriteMask                                                                0x0

#define cAf6_upen_bcp_profiles_FwdRule_BCP_Mask(profileId)                             (cBit1_0 << (profileId))
#define cAf6_upen_bcp_profiles_FwdRule_BCP_Shift(profileId)                            (0 + (profileId))

/*--------------------------------------
BitField Name: FwdRule_BCP_03
BitField Type: RW
BitField Desc: FwdRule for BCP#03
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_upen_bcp_profiles_FwdRule_BCP_03_Bit_Start                                                      6
#define cAf6_upen_bcp_profiles_FwdRule_BCP_03_Bit_End                                                        7
#define cAf6_upen_bcp_profiles_FwdRule_BCP_03_Mask                                                     cBit7_6
#define cAf6_upen_bcp_profiles_FwdRule_BCP_03_Shift                                                          6
#define cAf6_upen_bcp_profiles_FwdRule_BCP_03_MaxVal                                                       0x3
#define cAf6_upen_bcp_profiles_FwdRule_BCP_03_MinVal                                                       0x0
#define cAf6_upen_bcp_profiles_FwdRule_BCP_03_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_BCP_02
BitField Type: RW
BitField Desc: FwdRule for BCP#02
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_upen_bcp_profiles_FwdRule_BCP_02_Bit_Start                                                      4
#define cAf6_upen_bcp_profiles_FwdRule_BCP_02_Bit_End                                                        5
#define cAf6_upen_bcp_profiles_FwdRule_BCP_02_Mask                                                     cBit5_4
#define cAf6_upen_bcp_profiles_FwdRule_BCP_02_Shift                                                          4
#define cAf6_upen_bcp_profiles_FwdRule_BCP_02_MaxVal                                                       0x3
#define cAf6_upen_bcp_profiles_FwdRule_BCP_02_MinVal                                                       0x0
#define cAf6_upen_bcp_profiles_FwdRule_BCP_02_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_BCP_01
BitField Type: RW
BitField Desc: FwdRule for BCP#01
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_upen_bcp_profiles_FwdRule_BCP_01_Bit_Start                                                      2
#define cAf6_upen_bcp_profiles_FwdRule_BCP_01_Bit_End                                                        3
#define cAf6_upen_bcp_profiles_FwdRule_BCP_01_Mask                                                     cBit3_2
#define cAf6_upen_bcp_profiles_FwdRule_BCP_01_Shift                                                          2
#define cAf6_upen_bcp_profiles_FwdRule_BCP_01_MaxVal                                                       0x3
#define cAf6_upen_bcp_profiles_FwdRule_BCP_01_MinVal                                                       0x0
#define cAf6_upen_bcp_profiles_FwdRule_BCP_01_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_BCP_00
BitField Type: RW
BitField Desc: FwdRule for BCP#00
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_upen_bcp_profiles_FwdRule_BCP_00_Bit_Start                                                      0
#define cAf6_upen_bcp_profiles_FwdRule_BCP_00_Bit_End                                                        1
#define cAf6_upen_bcp_profiles_FwdRule_BCP_00_Mask                                                     cBit1_0
#define cAf6_upen_bcp_profiles_FwdRule_BCP_00_Shift                                                          0
#define cAf6_upen_bcp_profiles_FwdRule_BCP_00_MaxVal                                                       0x3
#define cAf6_upen_bcp_profiles_FwdRule_BCP_00_MinVal                                                       0x0
#define cAf6_upen_bcp_profiles_FwdRule_BCP_00_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : IgRule FCN Profiles
Reg Addr   : 0x00_C500
Reg Formula: 0x00_C500 + $fcn_profiles
    Where  :
           + $fcn_profiles(0-7) fcn_profile_id
Reg Desc   :
This register is used to config profile forward (fwd) rule for FCN status (FCN)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_fcn_profiles_Base                                                                0x00C500
#define cAf6Reg_upen_fcn_profiles(fcnprofiles)                                        (0x00C500+(fcnprofiles))
#define cAf6Reg_upen_fcn_profiles_WidthVal                                                                  32
#define cAf6Reg_upen_fcn_profiles_WriteMask                                                                0x0

#define cAf6_upen_fcn_profiles_FwdRule_FCN_Mask(profileId)                             (cBit1_0 << (profileId))
#define cAf6_upen_fcn_profiles_FwdRule_FCN_Shift(profileId)                            (0 + (profileId))

/*--------------------------------------
BitField Name: FwdRule_FCN_03
BitField Type: RW
BitField Desc: FwdRule for FCN#03
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_upen_fcn_profiles_FwdRule_FCN_03_Bit_Start                                                      6
#define cAf6_upen_fcn_profiles_FwdRule_FCN_03_Bit_End                                                        7
#define cAf6_upen_fcn_profiles_FwdRule_FCN_03_Mask                                                     cBit7_6
#define cAf6_upen_fcn_profiles_FwdRule_FCN_03_Shift                                                          6
#define cAf6_upen_fcn_profiles_FwdRule_FCN_03_MaxVal                                                       0x3
#define cAf6_upen_fcn_profiles_FwdRule_FCN_03_MinVal                                                       0x0
#define cAf6_upen_fcn_profiles_FwdRule_FCN_03_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_FCN_02
BitField Type: RW
BitField Desc: FwdRule for FCN#02
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_upen_fcn_profiles_FwdRule_FCN_02_Bit_Start                                                      4
#define cAf6_upen_fcn_profiles_FwdRule_FCN_02_Bit_End                                                        5
#define cAf6_upen_fcn_profiles_FwdRule_FCN_02_Mask                                                     cBit5_4
#define cAf6_upen_fcn_profiles_FwdRule_FCN_02_Shift                                                          4
#define cAf6_upen_fcn_profiles_FwdRule_FCN_02_MaxVal                                                       0x3
#define cAf6_upen_fcn_profiles_FwdRule_FCN_02_MinVal                                                       0x0
#define cAf6_upen_fcn_profiles_FwdRule_FCN_02_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_FCN_01
BitField Type: RW
BitField Desc: FwdRule for FCN#01
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_upen_fcn_profiles_FwdRule_FCN_01_Bit_Start                                                      2
#define cAf6_upen_fcn_profiles_FwdRule_FCN_01_Bit_End                                                        3
#define cAf6_upen_fcn_profiles_FwdRule_FCN_01_Mask                                                     cBit3_2
#define cAf6_upen_fcn_profiles_FwdRule_FCN_01_Shift                                                          2
#define cAf6_upen_fcn_profiles_FwdRule_FCN_01_MaxVal                                                       0x3
#define cAf6_upen_fcn_profiles_FwdRule_FCN_01_MinVal                                                       0x0
#define cAf6_upen_fcn_profiles_FwdRule_FCN_01_RstVal                                                       0x0

/*--------------------------------------
BitField Name: FwdRule_FCN_00
BitField Type: RW
BitField Desc: FwdRule for FCN#00
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_upen_fcn_profiles_FwdRule_FCN_00_Bit_Start                                                      0
#define cAf6_upen_fcn_profiles_FwdRule_FCN_00_Bit_End                                                        1
#define cAf6_upen_fcn_profiles_FwdRule_FCN_00_Mask                                                     cBit1_0
#define cAf6_upen_fcn_profiles_FwdRule_FCN_00_Shift                                                          0
#define cAf6_upen_fcn_profiles_FwdRule_FCN_00_MaxVal                                                       0x3
#define cAf6_upen_fcn_profiles_FwdRule_FCN_00_MinVal                                                       0x0
#define cAf6_upen_fcn_profiles_FwdRule_FCN_00_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : IgRule NLP FwdTable
Reg Addr   : 0x00_D000
Reg Formula: 0x00_D000 + $link_id
    Where  :
           + $link_id(0-3072) link_id
Reg Desc   :
This register is used to config profile forward (fwd) for eache link

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_link_id_Base                                                                     0x00D000
#define cAf6Reg_upen_link_id(linkid)                                                       (0x00D000+(linkid))
#define cAf6Reg_upen_link_id_WidthVal                                                                       32
#define cAf6Reg_upen_link_id_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: FCN_Profile_Id
BitField Type: RW
BitField Desc: FCN_Profiles_Id
BitField Bits: [30:28]
--------------------------------------*/
#define cAf6_upen_link_id_FCN_Profile_Id_Bit_Start                                                          28
#define cAf6_upen_link_id_FCN_Profile_Id_Bit_End                                                            30
#define cAf6_upen_link_id_FCN_Profile_Id_Mask                                                        cBit30_28
#define cAf6_upen_link_id_FCN_Profile_Id_Shift                                                              28
#define cAf6_upen_link_id_FCN_Profile_Id_MaxVal                                                            0x7
#define cAf6_upen_link_id_FCN_Profile_Id_MinVal                                                            0x0
#define cAf6_upen_link_id_FCN_Profile_Id_RstVal                                                            0x0

/*--------------------------------------
BitField Name: BCP_Profile_Id
BitField Type: RW
BitField Desc: BCP_Profiles_Id
BitField Bits: [26:24]
--------------------------------------*/
#define cAf6_upen_link_id_BCP_Profile_Id_Bit_Start                                                          24
#define cAf6_upen_link_id_BCP_Profile_Id_Bit_End                                                            26
#define cAf6_upen_link_id_BCP_Profile_Id_Mask                                                        cBit26_24
#define cAf6_upen_link_id_BCP_Profile_Id_Shift                                                              24
#define cAf6_upen_link_id_BCP_Profile_Id_MaxVal                                                            0x7
#define cAf6_upen_link_id_BCP_Profile_Id_MinVal                                                            0x0
#define cAf6_upen_link_id_BCP_Profile_Id_RstVal                                                            0x0

/*--------------------------------------
BitField Name: ERR_Profile_Id
BitField Type: RW
BitField Desc: ERR_Profiles_Id
BitField Bits: [22:20]
--------------------------------------*/
#define cAf6_upen_link_id_ERR_Profile_Id_Bit_Start                                                          20
#define cAf6_upen_link_id_ERR_Profile_Id_Bit_End                                                            22
#define cAf6_upen_link_id_ERR_Profile_Id_Mask                                                        cBit22_20
#define cAf6_upen_link_id_ERR_Profile_Id_Shift                                                              20
#define cAf6_upen_link_id_ERR_Profile_Id_MaxVal                                                            0x7
#define cAf6_upen_link_id_ERR_Profile_Id_MinVal                                                            0x0
#define cAf6_upen_link_id_ERR_Profile_Id_RstVal                                                            0x0

/*--------------------------------------
BitField Name: OAM_Profile_Id
BitField Type: RW
BitField Desc: OAM_Profiles_Id
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_upen_link_id_OAM_Profile_Id_Bit_Start                                                          16
#define cAf6_upen_link_id_OAM_Profile_Id_Bit_End                                                            19
#define cAf6_upen_link_id_OAM_Profile_Id_Mask                                                        cBit19_16
#define cAf6_upen_link_id_OAM_Profile_Id_Shift                                                              16
#define cAf6_upen_link_id_OAM_Profile_Id_MaxVal                                                            0xf
#define cAf6_upen_link_id_OAM_Profile_Id_MinVal                                                            0x0
#define cAf6_upen_link_id_OAM_Profile_Id_RstVal                                                            0x0

/*--------------------------------------
BitField Name: NLP_Profile_Id
BitField Type: RW
BitField Desc: NLP_Profiles_Id
BitField Bits: [12:08]
--------------------------------------*/
#define cAf6_upen_link_id_NLP_Profile_Id_Bit_Start                                                           8
#define cAf6_upen_link_id_NLP_Profile_Id_Bit_End                                                            12
#define cAf6_upen_link_id_NLP_Profile_Id_Mask                                                         cBit12_8
#define cAf6_upen_link_id_NLP_Profile_Id_Shift                                                               8
#define cAf6_upen_link_id_NLP_Profile_Id_MaxVal                                                           0x1f
#define cAf6_upen_link_id_NLP_Profile_Id_MinVal                                                            0x0
#define cAf6_upen_link_id_NLP_Profile_Id_RstVal                                                            0x0

/*--------------------------------------
BitField Name: FCN_FwdChk_En
BitField Type: RW
BitField Desc: FwdFCN check Enable
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_link_id_FCN_FwdChk_En_Bit_Start                                                            4
#define cAf6_upen_link_id_FCN_FwdChk_En_Bit_End                                                              4
#define cAf6_upen_link_id_FCN_FwdChk_En_Mask                                                             cBit4
#define cAf6_upen_link_id_FCN_FwdChk_En_Shift                                                                4
#define cAf6_upen_link_id_FCN_FwdChk_En_MaxVal                                                             0x1
#define cAf6_upen_link_id_FCN_FwdChk_En_MinVal                                                             0x0
#define cAf6_upen_link_id_FCN_FwdChk_En_RstVal                                                             0x0

/*--------------------------------------
BitField Name: BCP_FwdChk_En
BitField Type: RW
BitField Desc: FwdBCP check Enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_link_id_BCP_FwdChk_En_Bit_Start                                                            3
#define cAf6_upen_link_id_BCP_FwdChk_En_Bit_End                                                              3
#define cAf6_upen_link_id_BCP_FwdChk_En_Mask                                                             cBit3
#define cAf6_upen_link_id_BCP_FwdChk_En_Shift                                                                3
#define cAf6_upen_link_id_BCP_FwdChk_En_MaxVal                                                             0x1
#define cAf6_upen_link_id_BCP_FwdChk_En_MinVal                                                             0x0
#define cAf6_upen_link_id_BCP_FwdChk_En_RstVal                                                             0x0

/*--------------------------------------
BitField Name: ERR_FwdChk_En
BitField Type: RW
BitField Desc: FwdERR check Enable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_link_id_ERR_FwdChk_En_Bit_Start                                                            2
#define cAf6_upen_link_id_ERR_FwdChk_En_Bit_End                                                              2
#define cAf6_upen_link_id_ERR_FwdChk_En_Mask                                                             cBit2
#define cAf6_upen_link_id_ERR_FwdChk_En_Shift                                                                2
#define cAf6_upen_link_id_ERR_FwdChk_En_MaxVal                                                             0x1
#define cAf6_upen_link_id_ERR_FwdChk_En_MinVal                                                             0x0
#define cAf6_upen_link_id_ERR_FwdChk_En_RstVal                                                             0x0

/*--------------------------------------
BitField Name: OAM_FwdChk_En
BitField Type: RW
BitField Desc: FwdOAM check Enable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_link_id_OAM_FwdChk_En_Bit_Start                                                            1
#define cAf6_upen_link_id_OAM_FwdChk_En_Bit_End                                                              1
#define cAf6_upen_link_id_OAM_FwdChk_En_Mask                                                             cBit1
#define cAf6_upen_link_id_OAM_FwdChk_En_Shift                                                                1
#define cAf6_upen_link_id_OAM_FwdChk_En_MaxVal                                                             0x1
#define cAf6_upen_link_id_OAM_FwdChk_En_MinVal                                                             0x0
#define cAf6_upen_link_id_OAM_FwdChk_En_RstVal                                                             0x0

/*--------------------------------------
BitField Name: NLP_FwdChk_En
BitField Type: RW
BitField Desc: FwdNLP check Enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_link_id_NLP_FwdChk_En_Bit_Start                                                            0
#define cAf6_upen_link_id_NLP_FwdChk_En_Bit_End                                                              0
#define cAf6_upen_link_id_NLP_FwdChk_En_Mask                                                             cBit0
#define cAf6_upen_link_id_NLP_FwdChk_En_Shift                                                                0
#define cAf6_upen_link_id_NLP_FwdChk_En_MaxVal                                                             0x1
#define cAf6_upen_link_id_NLP_FwdChk_En_MinVal                                                             0x0
#define cAf6_upen_link_id_NLP_FwdChk_En_RstVal                                                             0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PROFILEREG_H_ */


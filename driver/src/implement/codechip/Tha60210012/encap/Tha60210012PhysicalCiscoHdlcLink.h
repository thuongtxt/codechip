/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60210012PhysicalCiscoHdlcLink.h
 * 
 * Created Date: Apr 20, 2016
 *
 * Description : Cisco-Hdlc Link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012PHYSICALCISCOHDLCLINK_H_
#define _THA60210012PHYSICALCISCOHDLCLINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/encap/dynamic/ThaPhysicalCiscoHdlcLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012PhysicalCiscoHdlcLink
    {
    tThaPhysicalCiscoHdlcLink super;
    }tTha60210012PhysicalCiscoHdlcLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210012HdlcLinkQueueEnable(AtChannel self, eBool enable);
AtPppLink Tha60210012PhysicalCiscoHdlcLinkObjectInit(AtPppLink self, AtHdlcChannel physicalChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012PHYSICALCISCOHDLCLINK_H_ */


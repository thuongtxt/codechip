/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210012EXaui.h
 * 
 * Created Date: May 10, 2016
 *
 * Description : eXAUI
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012EXAUI_H_
#define _THA60210012EXAUI_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eTha60210012Source
    {
    cTha60210012SourceInvalid,
    cTha60210012SourceXFI,
    cTha60210012SourceEXaui
    }eTha60210012Source;

typedef enum eTha60210012EthEXauiPrbsCounterType
    {
    cTha60210012EthEXauiPrbsCounterRxPackets,
    cTha60210012EthEXauiPrbsCounterRxBytes
    }eTha60210012EthEXauiPrbsCounterType;

typedef enum eTha60210012EthExauiPrbsLenMode
    {
    cTha60210012EthEXauiPrbsLenModeFix,
    cTha60210012EthEXauiPrbsLenModeIncrease
    }eTha60210012EthExauiPrbsLenMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210012EXauiIngressVlanSet(AtEthPort self, uint32 channelId, const tAtVlan *vlan);
eAtRet Tha60210012EXauiIngressVlanGet(AtEthPort self, uint32 channelId, tAtVlan *vlan);
eAtRet Tha60210012EXauiEgressExpectedVlanIdSet(AtEthPort self, uint32 channelId, uint16 expectedVlanId);
uint16 Tha60210012EXauiEgressExpectedVlanIdGet(AtEthPort self, uint32 channelId);
uint32 Tha60210012EthEXauiPortBaseAddress(AtEthPort self);


eAtRet Tha60210012EthEXauiPrbsMonitorSourceSelect(AtEthPort self, eTha60210012Source source);
eTha60210012Source Tha60210012EthEXauiPrbsMonitorSourceSelectGet(AtEthPort self);
eAtRet Tha60210012EthEXauiPrbsSingleGenerate(AtEthPort self, uint32 numPacket);
uint32 Tha60210012EthEXauiPrbsSingleGenerateGet(AtEthPort self);
eAtRet Tha60210012EthEXauiPrbsContinousGenerate(AtEthPort self);
eAtRet Tha60210012EthEXauiPrbsEnable(AtEthPort self, eBool enable);
eBool Tha60210012EthEXauiPrbsIsEnabled(AtEthPort self);
eAtRet Tha60210012EthEXauiPrbsForceError(AtEthPort self, eBool force);
eBool Tha60210012EthEXauiPrbsIsForceError(AtEthPort self);
uint32 Tha60210012EthEXauiPrbsErrorGet(AtEthPort self);
eAtRet Tha60210012EthExauiPrbsLenModeSet(AtEthPort self, uint32 lenMode);
uint32 Tha60210012EthExauiPrbsLenModeGet(AtEthPort self);
eAtRet Tha60210012EthExauiPrbsMinLenSet(AtEthPort self, uint32 pktLen);
uint32 Tha60210012EthExauiPrbsMinLenGet(AtEthPort self);
eAtRet Tha60210012EthExauiPrbsMaxLenSet(AtEthPort self, uint32 pktLen);
uint32 Tha60210012EthExauiPrbsMaxLenGet(AtEthPort self);
eAtRet Tha60210012EthExauiPrbsBandwidthSet(AtEthPort self, uint32 bandwidth);
uint32 Tha60210012EthExauiPrbsBandwidthGet(AtEthPort self);
uint32 Tha60210012EthEXauiPrbsCounterGet(AtEthPort self, uint32 counterType);
uint32 Tha60210012EthEXauiPrbsCounterClear(AtEthPort self, uint32 counterType);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012EXAUI_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210012MpEthFlowController.c
 *
 * Created Date: Apr 28, 2016
 *
 * Description : Ethernet MLPPP flow controller of 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210012EthFlowControllerInternal.h"
#include "../../../../default/eth/ThaEthFlowInternal.h"
#include "../../encap/Tha60210012ModuleEncap.h"
#include "../../cla/Tha60210012ModuleCla.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012MpEthFlowController
    {
    tTha60210012EthFlowController super;
    }tTha60210012MpEthFlowController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/* Supper */
static const tThaEthFlowControllerMethods *m_ThaEthFlowControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtMpBundle BundleOfFlow(AtEthFlow self)
    {
    return AtEthFlowMpBundleGet(self);
    }

static Tha60210012ModuleEncap EncapModule(AtEthFlow flow)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)flow);
    return (Tha60210012ModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static eAtRet TxTrafficEnable(ThaEthFlowController self, AtEthFlow flow, eBool enable)
    {
    AtUnused(self);
    return Tha60210012ModuleEncapMpBundleFlowLookupEnable(EncapModule(flow), BundleOfFlow(flow), enable);
    }

static eBool TxTrafficIsEnabled(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    return Tha60210012ModuleEncapMpBundleFlowLookupIsEnabled(EncapModule(flow), BundleOfFlow(flow));
    }

static ThaModuleCla ClaModule(AtEthFlow flow)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)flow);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static eAtRet Activate(ThaEthFlowController self, AtEthFlow flow, AtChannel channel, uint8 classNumber)
    {
    AtMpBundle bundle;
    eAtRet ret;

    /* Updated database */
    AtEthFlowHdlcBundleSet(flow, (AtHdlcBundle)channel);

    bundle = BundleOfFlow(flow);
    ret = m_ThaEthFlowControllerMethods->Activate(self, flow, channel, classNumber);
    if (ret != cAtOk)
        return ret;

    /* Set lookup TDM to PSN direction */
    ret = Tha60210012ModuleEncapFlowMpBundleSet(EncapModule(flow), bundle, flow);

    /* Set lookup PSN to TDM direction */
    ret |= Tha60210012ModuleClaFlowMpBundleSet(ClaModule(flow), flow, bundle);

    return ret;
    }

static eAtRet DeActivate(ThaEthFlowController self, AtEthFlow flow)
    {
    ThaEthFlow thaFlow = (ThaEthFlow)flow;
    eAtRet ret = m_ThaEthFlowControllerMethods->DeActivate(self, flow);

    if (ret != cAtOk)
        return ret;

    ret = Tha60210012ModuleEncapMpBundleFlowReset(EncapModule(flow), BundleOfFlow(flow));
    ret |= Tha60210012ModuleClaFlowReset(ClaModule(flow), flow);

    AtEthFlowHdlcBundleSet(flow, NULL);
    thaFlow->classId  = 0xFF;

    return ret;
    }

static void OverrideThaEthFlowController(ThaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, mMethodsGet(self), sizeof(m_ThaEthFlowControllerOverride));
        mMethodOverride(m_ThaEthFlowControllerOverride, TxTrafficEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, TxTrafficIsEnabled);
        mMethodOverride(m_ThaEthFlowControllerOverride, Activate);
        mMethodOverride(m_ThaEthFlowControllerOverride, DeActivate);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012MpEthFlowController);
    }

static void Override(ThaEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

static ThaEthFlowController ObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012EthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController Tha60210012MpEthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlow, module);
    }

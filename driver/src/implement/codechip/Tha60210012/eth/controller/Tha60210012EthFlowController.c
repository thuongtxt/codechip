/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210012EthFlowController.c
 *
 * Created Date: Mar 28, 2016
 *
 * Description : Ethernet flow controller of 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/eth/ThaEthFlowInternal.h"
#include "../../../../default/encap/dynamic/ThaDynamicHdlcLink.h"
#include "../../../../default/sur/hard/ThaModuleHardSur.h"
#include "../../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "../../pwe/Tha60210012ModulePwe.h"
#include "../../cla/Tha60210012ModuleCla.h"
#include "../../pda/Tha60210012ModulePda.h"
#include "../Tha60210012EthFlow.h"
#include "../Tha60210012ModuleEthPmcInternalReg.h"
#include "../Tha60210012ModuleEth.h"
#include "Tha60210012EthFlowControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha60210012EthFlowController *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/* Supper */
static const tThaEthFlowControllerMethods *m_ThaEthFlowControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePwe ModulePwe(AtEthFlow flow)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)flow);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static ThaModuleCla ModuleCla(AtEthFlow flow)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)flow);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static eAtRet PsnBufferGet(ThaEthFlowController self, AtEthFlow flow, uint8* pBuffer, uint32* psnLen)
    {
    ThaModulePwe modulePwe = ModulePwe(flow);
    uint8 psnLength = Tha60210012ModulePweFlowHeaderLengthGet(modulePwe, flow);
    AtUnused(self);

    if (psnLen)
        *psnLen = (psnLength == 0) ? (uint32)mMethodsGet(self)->EthHeaderNoVlanLengthGet(self, flow) : psnLength;
 
    if (psnLength == 0)
    	return cAtOk;

    return Tha60210012ModulePweFlowHeaderRead(modulePwe, flow, cWorkingPsnPage, pBuffer, psnLength);
    }

static eAtRet PsnBufferSet(ThaEthFlowController self, AtEthFlow flow, uint8* pBuffer, uint32 psnLen)
    {
    ThaModulePwe modulePwe = ModulePwe(flow);
    eAtRet ret = cAtOk;
    AtUnused(self);

    ret |= Tha60210012ModulePweFlowHeaderWrite(modulePwe, flow, cWorkingPsnPage, pBuffer, (uint8)psnLen);
    ret |= Tha60210012ModulePweFlowHeaderLengthSet(modulePwe, flow, (uint8)psnLen);

    return ret;
    }

static eAtRet Activate(ThaEthFlowController self, AtEthFlow flow, AtChannel channel, uint8 classNumber)
    {
    eAtRet ret;
    uint32 hwFlowId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);

    if (hwFlowId == cInvalidUint32)
        {
        hwFlowId = Tha60210012ModuleClaHwFlowIdAllocate(ModuleCla(flow), AtChannelIdGet(channel));
        if (hwFlowId == cInvalidUint32)
            return cAtErrorRsrcNoAvail;

        ThaEthFlowHwFlowIdSet((ThaEthFlow)flow, hwFlowId);
        }

    ret = m_ThaEthFlowControllerMethods->Activate(self, flow, channel, classNumber);
    if (ret != cAtOk)
        return ret;

    /* Set default PTCH mode 2 bytes */
    if (Tha60210012ModulePwePtchEnablePerChannelIsSupported(ModulePwe(flow)))
        {
        ret = AtEthFlowPtchInsertionModeSet(flow, Tha60210012ModulePweDefaultPtchMode());
        ret |= AtEthFlowPtchServiceEnable(flow, cAtTrue);

        return ret;
        }

    /* HW recommend, need to enable removing by default */
    return Tha60210012ModuleClaEthFlowLanFcsRemoveEnable(ModuleCla(flow), flow, cAtTrue);
    }

static eAtRet DeActivate(ThaEthFlowController self, AtEthFlow flow)
    {
    eAtRet ret = m_ThaEthFlowControllerMethods->DeActivate(self, flow);
    if (ret != cAtOk)
        return ret;

    Tha60210012ModuleClaHwFlowIdDeallocate(ModuleCla(flow), ThaEthFlowHwFlowIdGet((ThaEthFlow)flow));
    ThaEthFlowHwFlowIdSet((ThaEthFlow)flow, cInvalidUint32);

    if (Tha60210012ModulePwePtchEnablePerChannelIsSupported(ModulePwe(flow)))
        AtEthFlowPtchServiceEnable(flow, cAtFalse);

    return cAtOk;
    }

static eAtRet NumberEgVlanSet(ThaEthFlowController self, AtEthFlow flow, uint8 vlanTagNum)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(vlanTagNum);
    return cAtOk;
    }

static void PsnLengthReset(ThaEthFlowController self, AtEthFlow flow)
    {
    /* TODO: Need to handle this code */
    AtUnused(self);
    AtUnused(flow);
    }

static eAtRet IngressVlanDisable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    tAtEthVlanTag cVlan;
    AtUnused(self);

    if (desc == NULL)
        return cAtErrorNullPointer;

    if (desc->numberOfVlans == 0)
        return cAtErrorRsrcNoAvail;

    cVlan = desc->vlans[0];
    return Tha60210012ModuleClaEthFlowExpectedCVlanSet(ModuleCla(flow), flow, &cVlan, cAtFalse);
    }

static eAtRet IngressVlanEnable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    tAtEthVlanTag cVlan;
    AtUnused(self);

    if (desc == NULL)
        return cAtErrorNullPointer;

    if (desc->numberOfVlans == 0)
        return cAtErrorRsrcNoAvail;

    cVlan = desc->vlans[0];
    return Tha60210012ModuleClaEthFlowExpectedCVlanSet(ModuleCla(flow), flow, &cVlan, cAtTrue);
    }

static eAtRet RxTrafficEnable(ThaEthFlowController self, AtEthFlow flow, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)flow);
    ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    uint8 serviceType = (uint8)((enable) ? Tha60210012EthFlowCachedServiceType(flow) : 0);
    AtUnused(self);

    return Tha60210012ModuleClaEthFlowServiceTypeSet(claModule, flow, serviceType);
    }

static uint8 PartId(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(flow);
    AtUnused(self);
    return 0;
    }

static void FlowControlThresholdInit(ThaEthFlowController self, AtEthFlow ethFlow, AtChannel channel)
    {
    AtUnused(channel);
    AtUnused(ethFlow);
    AtUnused(self);
    }

static uint8 FlowCounterTypeSw2Hw(uint16 swCounterType)
    {
    if (swCounterType == cAtEthFlowCounterTxPackets)         return 8;
    if (swCounterType == cAtEthFlowCounterTxBytes)           return 9;
    if (swCounterType == cAtEthFlowCounterTxFragmentPackets) return 10;
    if (swCounterType == cAtEthFlowCounterTxDiscardPackets)  return 11;
    if (swCounterType == cAtEthFlowCounterTxDiscardBytes)    return 12;
    if (swCounterType == cAtEthFlowCounterTxBECNPackets)     return 13;
    if (swCounterType == cAtEthFlowCounterTxFECNPackets)     return 14;
    if (swCounterType == cAtEthFlowCounterTxDEPackets)       return 15;
    if (swCounterType == cAtEthFlowCounterRxPackets)         return 0;
    if (swCounterType == cAtEthFlowCounterRxBytes)           return 1;
    if (swCounterType == cAtEthFlowCounterRxFragmentPackets) return 2;
    if (swCounterType == cAtEthFlowCounterRxDiscardPackets)  return 3;
    if (swCounterType == cAtEthFlowCounterRxDiscardBytes)    return 4;
    if (swCounterType == cAtEthFlowCounterRxBECNPackets)     return 5;
    if (swCounterType == cAtEthFlowCounterRxFECNPackets)     return 6;
    if (swCounterType == cAtEthFlowCounterRxDEPackets)       return 7;

    return cBit7_0;
    }

static ThaModuleEth ModuleEth(AtEthFlow flow)
    {
    return (ThaModuleEth)AtChannelModuleGet((AtChannel)flow);
    }

static ThaModuleHardSur ModuleSur(AtEthFlow flow)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)flow);
    return (ThaModuleHardSur)AtDeviceModuleGet(device, cAtModuleSur);
    }

static uint32 ExternalCounterGet(AtEthFlow flow, uint16 counterType, eBool r2c)
    {
    ThaModuleHardSur surModule = ModuleSur(flow);
    eAtRet ret = ThaModuleHardSurEthFlowCounterLatch(surModule, flow, r2c);

    if (ret != cAtOk)
        return 0;

    switch (counterType)
        {
        case cAtEthFlowCounterTxBytes:          return ThaModuleHardSurEthFlowTxTotalByteGet(surModule, flow);
        case cAtEthFlowCounterTxPackets:        return ThaModuleHardSurEthFlowTxGoodPacketGet(surModule, flow);
        case cAtEthFlowCounterRxBytes:          return ThaModuleHardSurEthFlowRxTotalByteGet(surModule, flow);
        case cAtEthFlowCounterRxPackets:        return ThaModuleHardSurEthFlowRxGoodPacketGet(surModule, flow);
        case cAtEthFlowCounterRxDiscardPackets: return ThaModuleHardSurEthFlowRxDiscardPktGet(surModule, flow);
        default: return 0;
        }
    }

static uint32 CounterGet(ThaEthFlowController self, AtEthFlow flow, uint16 counterType)
    {
    ThaModuleEth moduleEth = ModuleEth(flow);
    AtUnused(self);

    if (Tha60210012ModuleEthExternalPmcCounterIsSupported((Tha60210012ModuleEth)moduleEth))
        return ExternalCounterGet(flow, counterType, cAtFalse);

    return Tha60210012ModuleEthHwFlowCounterGet(moduleEth, AtChannelHwIdGet((AtChannel)flow), FlowCounterTypeSw2Hw(counterType), cAtFalse);
    }

static uint32 CounterClear(ThaEthFlowController self, AtEthFlow flow, uint16 counterType)
    {
    ThaModuleEth moduleEth = ModuleEth(flow);
    AtUnused(self);

    if (Tha60210012ModuleEthExternalPmcCounterIsSupported((Tha60210012ModuleEth)moduleEth))
        return ExternalCounterGet(flow, counterType, cAtTrue);

    return Tha60210012ModuleEthHwFlowCounterGet(moduleEth, AtChannelHwIdGet((AtChannel)flow), FlowCounterTypeSw2Hw(counterType), cAtTrue);
    }

static void OverrideThaEthFlowController(ThaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, m_ThaEthFlowControllerMethods, sizeof(m_ThaEthFlowControllerOverride));
        mMethodOverride(m_ThaEthFlowControllerOverride, PsnBufferGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, PsnBufferSet);
        mMethodOverride(m_ThaEthFlowControllerOverride, Activate);
        mMethodOverride(m_ThaEthFlowControllerOverride, DeActivate);
        mMethodOverride(m_ThaEthFlowControllerOverride, NumberEgVlanSet);
        mMethodOverride(m_ThaEthFlowControllerOverride, PsnLengthReset);
        mMethodOverride(m_ThaEthFlowControllerOverride, IngressVlanDisable);
        mMethodOverride(m_ThaEthFlowControllerOverride, IngressVlanEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, RxTrafficEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, PartId);
        mMethodOverride(m_ThaEthFlowControllerOverride, FlowControlThresholdInit);
        mMethodOverride(m_ThaEthFlowControllerOverride, CounterGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, CounterClear);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012EthFlowController);
    }

static void Override(ThaEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

ThaEthFlowController Tha60210012EthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController Tha60210012EthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012EthFlowControllerObjectInit(newFlow, module);
    }

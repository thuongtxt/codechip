/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210012PppEthFlowController.c
 *
 * Created Date: Apr 28, 2016
 *
 * Description : Ethernet PPP flow controller of 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/eth/ThaEthFlowInternal.h"
#include "../../../../default/encap/dynamic/ThaDynamicHdlcLink.h"
#include "../../encap/Tha60210012ModuleEncap.h"
#include "../../encap/Tha60210012PhysicalHdlcChannel.h"
#include "../../ppp/Tha60210012PhysicalPppLink.h"
#include "../../cla/Tha60210012ModuleCla.h"
#include "Tha60210012EthFlowControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/* Supper */
static const tThaEthFlowControllerMethods *m_ThaEthFlowControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHdlcLink LinkBoundFlow(AtEthFlow flow)
    {
    return AtEthFlowHdlcLinkGet(flow);
    }

static Tha60210012ModuleEncap EncapModule(AtEthFlow flow)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)flow);
    return (Tha60210012ModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static eAtRet TxTrafficEnable(ThaEthFlowController self, AtEthFlow flow, eBool enable)
    {
    AtUnused(self);
    return Tha60210012ModuleEncapHdlcLinkFlowLookupEnable(EncapModule(flow), LinkBoundFlow(flow), enable);
    }

static eBool TxTrafficIsEnabled(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    return Tha60210012ModuleEncapHdlcLinkFlowLookupIsEnabled(EncapModule(flow), LinkBoundFlow(flow));
    }

static ThaModuleCla ClaModule(AtEthFlow flow)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)flow);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static eAtRet Activate(ThaEthFlowController self, AtEthFlow flow, AtChannel channel, uint8 classNumber)
    {
    AtHdlcLink link, currentLink;
    eAtRet ret;

    link = (AtHdlcLink)channel;
    currentLink = AtEthFlowHdlcLinkGet(flow);

    if (currentLink == link)
        return cAtOk;

    AtEthFlowHdlcLinkSet(flow, link);
    ret = m_ThaEthFlowControllerMethods->Activate(self, flow, channel, classNumber);
    if (ret != cAtOk)
        return ret;

    /* Set lookup TDM to PSN direction */
    ret = Tha60210012ModuleEncapHdlcLinkHwFlowSet(EncapModule(flow), (AtHdlcLink)channel, flow);

    /* Set lookup PSN to TDM direction */
    ret |= Tha60210012ModuleClaFlowPppLinkSet(ClaModule(flow), flow, (AtHdlcLink)channel);
    if (ret != cAtOk)
        return ret;

    if (AtHdlcLinkPduTypeGet((AtHdlcLink)channel) == cAtHdlcPduTypeEthernet)
        return AtPppLinkBcpLanFcsInsertionEnable((AtPppLink)channel, ThaDynamicPppLinkLanFcsCacheGet((AtHdlcLink)channel));

    return cAtOk;
    }

static eAtRet DeActivate(ThaEthFlowController self, AtEthFlow flow)
    {
    eAtRet ret = m_ThaEthFlowControllerMethods->DeActivate(self, flow);

    if (ret != cAtOk)
        return ret;

    ret = Tha60210012ModuleEncapHdlcLinkHwFlowReset(EncapModule(flow), AtEthFlowHdlcLinkGet(flow));
    ret |= Tha60210012ModuleClaFlowReset(ClaModule(flow), flow);

    return ret;
    }

static eBool IsBcp(AtEthFlow flow)
    {
    if (AtHdlcLinkPduTypeGet(AtEthFlowHdlcLinkGet(flow)) == cAtHdlcPduTypeEthernet)
        return cAtTrue;

    return cAtFalse;
    }

static eBool HasEthernetType(ThaEthFlowController self, AtEthFlow flow)
    {
    /* Bridge path do not support programmable DA/SA and ethernet type */
    if (IsBcp(flow))
        return cAtFalse;

    return m_ThaEthFlowControllerMethods->HasEthernetType(self, flow);
    }

static eBool MacIsProgrammable(ThaEthFlowController self, AtEthFlow flow)
    {
    /* Bridge path do not support programmable DA/SA */
    if (IsBcp(flow))
        return cAtFalse;

    return m_ThaEthFlowControllerMethods->MacIsProgrammable(self, flow);
    }

static uint16 EthHeaderNoVlanLengthGet(ThaEthFlowController self, AtEthFlow flow)
    {
    if (IsBcp(flow))
        return 0;

    return m_ThaEthFlowControllerMethods->EthHeaderNoVlanLengthGet(self, flow);
    }

static void OverrideThaEthFlowController(ThaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, m_ThaEthFlowControllerMethods, sizeof(m_ThaEthFlowControllerOverride));
        mMethodOverride(m_ThaEthFlowControllerOverride, TxTrafficEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, TxTrafficIsEnabled);
        mMethodOverride(m_ThaEthFlowControllerOverride, Activate);
        mMethodOverride(m_ThaEthFlowControllerOverride, DeActivate);
        mMethodOverride(m_ThaEthFlowControllerOverride, MacIsProgrammable);
        mMethodOverride(m_ThaEthFlowControllerOverride, HasEthernetType);
        mMethodOverride(m_ThaEthFlowControllerOverride, EthHeaderNoVlanLengthGet);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012PppEthFlowController);
    }

static void Override(ThaEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

ThaEthFlowController Tha60210012PppEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012EthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController Tha60210012PppEthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012PppEthFlowControllerObjectInit(newFlow, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : Tha60210012EthFlowController.h
 * 
 * Created Date: Mar 28, 2016
 *
 * Description : 60210012 Ethernet flow controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012ETHFLOWCONTROLLER_H_
#define _THA60210012ETHFLOWCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthFlowController Tha60210012EthFlowControllerNew(AtModuleEth module);
ThaEthFlowController Tha60210012EopEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController Tha60210012PppEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController Tha60210012MpEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController Tha60210012OamEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController Tha60210012CiscoHdlcEthFlowControllerNew(AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012ETHFLOWCONTROLLER_H_ */


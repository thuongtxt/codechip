/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210012OamEthFlowController.c
 *
 * Created Date: Jul 18, 2016
 *
 * Description : OAM flow controller for 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210012EthFlowControllerInternal.h"
#include "../../encap/Tha60210012ModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012OamEthFlowController
    {
    tTha60210012EthFlowController super;
    }tTha60210012OamEthFlowController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/* Supper */
static const tThaEthFlowControllerMethods *m_ThaEthFlowControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HwFlowId(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cInvalidUint32;
    }

static uint8 *PsnArrayTypes(ThaEthFlowController self, uint8 *numPsnType)
    {
    AtUnused(self);
    AtUnused(numPsnType);
    return NULL;
    }

static uint16 EthVlanLookupTypeGet(ThaEthFlowController self)
    {
    AtUnused(self);
    return cInvalidUint16;
    }

static uint32 MpigDatDeQueRegisterAddress(ThaEthFlowController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint16 EthHeaderNoVlanLengthGet(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cInvalidUint16;
    }

static uint8  PsnMode(ThaEthFlowController self)
    {
    AtUnused(self);
    return cBit7_0;
    }

static uint32 VlanLookupId(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cInvalidUint32;
    }

static uint32 VlanIdSw2Hw(ThaEthFlowController self, AtEthFlow flow, uint32 vlanId)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(vlanId);
    return cInvalidUint32;
    }

static eAtRet EthHdrSet(ThaEthFlowController self, AtEthFlow flow, uint8* destMacAddr, uint8* srcMacAddr, tAtEthVlanDesc* egressVlan)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(destMacAddr);
    AtUnused(srcMacAddr);
    AtUnused(egressVlan);
    return cAtOk;
    }

static eBool HasEthernetType(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cAtFalse;
    }

static eAtRet EthernetTypeSet(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cAtOk;
    }

static eAtRet TxTrafficEnable(ThaEthFlowController self, AtEthFlow flow, eBool enable)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(enable);
    return cAtOk;
    }

static eBool TxTrafficIsEnabled(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cAtOk;
    }

static eAtRet RxTrafficEnable(ThaEthFlowController self, AtEthFlow flow, eBool enable)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(enable);
    return cAtOk;
    }

static eAtRet Debug(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cAtOk;
    }

static eAtRet IngressVlanDisable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(desc);
    return cAtOk;
    }

static eAtRet IngressVlanEnable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(desc);
    return cAtOk;
    }

static eAtRet PsnBufferGet(ThaEthFlowController self, AtEthFlow flow, uint8* pBuffer, uint32* psnLen)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(pBuffer);
    AtUnused(psnLen);
    return cAtOk;
    }

static eAtRet PsnBufferSet(ThaEthFlowController self, AtEthFlow flow, uint8* pBuffer, uint32 psnLen)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(pBuffer);
    AtUnused(psnLen);
    return cAtOk;
    }

static eAtRet NumberEgVlanSet(ThaEthFlowController self, AtEthFlow flow, uint8 vlanTagNum)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(vlanTagNum);
    return cAtOk;
    }

static uint32 MpigFlowOffset(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cInvalidUint32;
    }

static uint32 InsertVlanToBuffer(ThaEthFlowController self, AtEthFlow ethFlow, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen)
    {
    AtUnused(self);
    AtUnused(egressVlan);
    AtUnused(buffer);
    AtUnused(psnLen);
    AtUnused(ethFlow);
    return cInvalidUint32;
    }

static uint32 RemoveVlanFromBuffer(ThaEthFlowController self, AtEthFlow ethFlow, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen)
    {
    AtUnused(self);
    AtUnused(egressVlan);
    AtUnused(buffer);
    AtUnused(psnLen);
    AtUnused(ethFlow);
    return cInvalidUint32;
    }

static eAtRet Activate(ThaEthFlowController self, AtEthFlow ethFlow, AtChannel channel, uint8 classNumber)
    {
    AtUnused(channel);
    AtUnused(classNumber);

    ThaEthFlowControllerSet((ThaEthFlow)ethFlow, self);
    return cAtOk;
    }

static eAtRet DeActivate(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    ThaEthFlowControllerSet((ThaEthFlow) flow, NULL);
    return cAtOk;
    }

static void OverrideThaEthFlowController(ThaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, mMethodsGet(self), sizeof(m_ThaEthFlowControllerOverride));

        mMethodOverride(m_ThaEthFlowControllerOverride, RemoveVlanFromBuffer);
        mMethodOverride(m_ThaEthFlowControllerOverride, InsertVlanToBuffer);
        mMethodOverride(m_ThaEthFlowControllerOverride, MpigFlowOffset);
        mMethodOverride(m_ThaEthFlowControllerOverride, NumberEgVlanSet);
        mMethodOverride(m_ThaEthFlowControllerOverride, PsnBufferSet);
        mMethodOverride(m_ThaEthFlowControllerOverride, PsnBufferGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, IngressVlanDisable);
        mMethodOverride(m_ThaEthFlowControllerOverride, IngressVlanEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, Debug);
        mMethodOverride(m_ThaEthFlowControllerOverride, RxTrafficEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, TxTrafficIsEnabled);
        mMethodOverride(m_ThaEthFlowControllerOverride, TxTrafficEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, EthernetTypeSet);
        mMethodOverride(m_ThaEthFlowControllerOverride, HasEthernetType);
        mMethodOverride(m_ThaEthFlowControllerOverride, EthHdrSet);
        mMethodOverride(m_ThaEthFlowControllerOverride, VlanIdSw2Hw);
        mMethodOverride(m_ThaEthFlowControllerOverride, VlanLookupId);
        mMethodOverride(m_ThaEthFlowControllerOverride, PsnMode);
        mMethodOverride(m_ThaEthFlowControllerOverride, EthHeaderNoVlanLengthGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, MpigDatDeQueRegisterAddress);
        mMethodOverride(m_ThaEthFlowControllerOverride, EthVlanLookupTypeGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, PsnArrayTypes);
        mMethodOverride(m_ThaEthFlowControllerOverride, HwFlowId);
        mMethodOverride(m_ThaEthFlowControllerOverride, Activate);
        mMethodOverride(m_ThaEthFlowControllerOverride, DeActivate);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012OamEthFlowController);
    }

static void Override(ThaEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

static ThaEthFlowController ObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012EthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController Tha60210012OamEthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlow, module);
    }

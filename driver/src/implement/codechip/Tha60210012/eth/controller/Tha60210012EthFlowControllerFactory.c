/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210012EthFlowControllerFactory.c
 *
 * Created Date: Mar 28, 2016
 *
 * Description : Flow controller factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/eth/controller/ThaEthFlowControllerFactoryInternal.h"
#include "../Tha60210012ModuleEthInternal.h"
#include "Tha60210012EthFlowController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012EthFlowControllerFactory
    {
    tThaEthFlowControllerFactory super;
    }tTha60210012EthFlowControllerFactory;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerFactoryMethods m_ThaEthFlowControllerFactoryOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaEthFlowController PppFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return Tha60210012PppEthFlowControllerNew(ethModule);
    }

static ThaEthFlowController OamFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return Tha60210012OamEthFlowControllerNew(ethModule);
    }

static ThaEthFlowController MpFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return Tha60210012MpEthFlowControllerNew(ethModule);
    }

static ThaEthFlowController CiscoHdlcFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return Tha60210012CiscoHdlcEthFlowControllerNew(ethModule);
    }

static ThaEthFlowController FrFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    AtUnused(ethModule);
    return NULL;
    }

static ThaEthFlowController EncapFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return Tha60210012EopEthFlowControllerNew(ethModule);
    }

static void OverrideThaEthFlowControllerFactory(ThaEthFlowControllerFactory self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerFactoryOverride, mMethodsGet(self), sizeof(m_ThaEthFlowControllerFactoryOverride));

        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, PppFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, MpFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, CiscoHdlcFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, FrFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, OamFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, EncapFlowControllerCreate);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerFactoryOverride);
    }

static void Override(ThaEthFlowControllerFactory self)
    {
    OverrideThaEthFlowControllerFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012EthFlowControllerFactory);
    }

static ThaEthFlowControllerFactory ObjectInit(ThaEthFlowControllerFactory self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Constructor */
    if (ThaEthFlowControllerFactoryObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowControllerFactory Tha60210012EthFlowControllerFactory(void)
    {
    static ThaEthFlowControllerFactory m_factory = NULL;
    static tTha60210012EthFlowControllerFactory factory;

    if (m_factory == NULL)
        m_factory = ObjectInit((ThaEthFlowControllerFactory)&factory);

    return m_factory;
    }

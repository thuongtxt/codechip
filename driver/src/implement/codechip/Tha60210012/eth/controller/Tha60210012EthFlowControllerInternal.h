/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210012EthFlowControllerInternal.h
 * 
 * Created Date: Apr 28, 2016
 *
 * Description : Ethernet flow controller of 60210012
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012ETHFLOWCONTROLLERINTERNAL_H_
#define _THA60210012ETHFLOWCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/eth/controller/ThaEthFlowControllerInternal.h"
#include "Tha60210012EthFlowController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012EthFlowController
    {
    tThaEthFlowController super;
    }tTha60210012EthFlowController;

typedef struct tTha60210012PppEthFlowController
    {
    tTha60210012EthFlowController super;
    }tTha60210012PppEthFlowController;

typedef struct tTha60210012EopEthFlowController
    {
    tTha60210012EthFlowController super;
    }tTha60210012EopEthFlowController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthFlowController Tha60210012EthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module);
ThaEthFlowController Tha60210012PppEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012ETHFLOWCONTROLLERINTERNAL_H_ */


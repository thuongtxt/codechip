/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210012CiscoHdlcEthFlowController.c
 *
 * Created Date: Jul 25, 2016
 *
 * Description : CISCO-HDLC flow controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/encap/dynamic/ThaDynamicHdlcLink.h"
#include "../../../../default/encap/dynamic/ThaDynamicHdlcChannel.h"
#include "../../ppp/Tha60210012PhysicalPppLink.h"
#include "../../encap/Tha60210012PhysicalHdlcChannel.h"
#include "../Tha60210012EthFlow.h"
#include "Tha60210012EthFlowControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012CiscoHdlcEthFlowController
    {
    tTha60210012PppEthFlowController super;
    }tTha60210012CiscoHdlcEthFlowController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/* Supper */
static const tThaEthFlowControllerMethods *m_ThaEthFlowControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 EthHeaderNoVlanLengthGet(ThaEthFlowController self, AtEthFlow flow)
    {
    if (AtHdlcLinkPduTypeGet(AtEthFlowHdlcLinkGet(flow)) == cAtHdlcPduTypeEthernet)
        return 0;

    return ThaEthFlowHeaderProviderEthHeaderNoVlanLengthGet(ThaEthFlowControllerHeaderProvider(self));
    }

static eAtRet Activate(ThaEthFlowController self, AtEthFlow flow, AtChannel channel, uint8 classNumber)
    {
    static const uint8 cCHdlcService = 6;
    eAtRet ret = m_ThaEthFlowControllerMethods->Activate(self, flow, channel, classNumber);
    if (ret != cAtOk)
        return ret;

    Tha60210012EthFlowServiceTypeCache(flow, cCHdlcService);
    return cAtOk;
    }

static void OverrideThaEthFlowController(ThaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, mMethodsGet(self), sizeof(m_ThaEthFlowControllerOverride));

        mMethodOverride(m_ThaEthFlowControllerOverride, EthHeaderNoVlanLengthGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, Activate);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012CiscoHdlcEthFlowController);
    }

static void Override(ThaEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

static ThaEthFlowController ObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012PppEthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController Tha60210012CiscoHdlcEthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlow, module);
    }

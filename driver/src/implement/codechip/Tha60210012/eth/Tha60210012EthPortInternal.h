/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : Tha60210012EthPortInternal.h
 * 
 * Created Date: Jul 4, 2016
 *
 * Description : 60210012 ethernet port internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012ETHPORTINTERNAL_H_
#define _THA60210012ETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/eth/Tha60210051ModuleEthInternal.h"
#include "Tha60210012EthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012EthPort
    {
    tTha60210051EthPort super;
    }tTha60210012EthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60210012EthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012ETHPORTINTERNAL_H_ */


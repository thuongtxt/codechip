/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : Tha60210012EthFlow.h
 * 
 * Created Date: Jun 13, 2016
 *
 * Description : 60210012 Ethernet flow utilities
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012ETHFLOW_H_
#define _THA60210012ETHFLOW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/encap/resequence/AtResequenceEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthFlow Tha60210012EthFlowNew(uint32 flowId, AtModuleEth module);
void Tha60210012EthFlowServiceTypeCache(AtEthFlow self, uint8 serviceType);
uint8 Tha60210012EthFlowCachedServiceType(AtEthFlow self);

/* Associated resequence engine when flow is added to bundle */
void Tha60210012EthFlowResequenceEngineSet(AtEthFlow self, AtResequenceEngine engine);
AtResequenceEngine Tha60210012EthFlowResequenceEngineGet(AtEthFlow self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012ETHFLOW_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210012EthPort.h
 * 
 * Created Date: May 14, 2016
 *
 * Description : XFI Port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012ETHPORT_H_
#define _THA60210012ETHPORT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60210012EthPortIsEXauiPort(AtEthPort self);
eBool Tha60210012EthPortPtchIsEnabled(AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012ETHPORT_H_ */


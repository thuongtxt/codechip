/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet module
 * 
 * File        : Tha60210012EXauiReg.h
 * 
 * Created Date: May 5, 2016
 *
 * Description : eXAUI registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012EXAUIREG_H_
#define _THA60210012EXAUIREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : Global Configure 0
Reg Addr   : 0x000
Reg Formula: 0x000
    Where  :
Reg Desc   :
Used to configure:
- Active MUX and eXAUI#0 Buffer
- MTU check

------------------------------------------------------------------------------*/
#define cAf6Reg_cfg0_pen_Base                                                                            0x000
#define cAf6Reg_cfg0_pen                                                                               0x000UL
#define cAf6Reg_cfg0_pen_WidthVal                                                                           32
#define cAf6Reg_cfg0_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: MTU
BitField Type: R/W
BitField Desc: Maximum Transmit Unit for eXAUI#0 Buffer
BitField Bits: [31:04]
--------------------------------------*/
#define cAf6_cfg0_pen_MTU_Bit_Start                                                                          4
#define cAf6_cfg0_pen_MTU_Bit_End                                                                           31
#define cAf6_cfg0_pen_MTU_Mask                                                                        cBit31_4
#define cAf6_cfg0_pen_MTU_Shift                                                                              4
#define cAf6_cfg0_pen_MTU_MaxVal                                                                     0xfffffff
#define cAf6_cfg0_pen_MTU_MinVal                                                                           0x0
#define cAf6_cfg0_pen_MTU_RstVal                                                                        0x3FFF

/*--------------------------------------
BitField Name: Active
BitField Type: R/W
BitField Desc: Active Engine
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_cfg0_pen_Active_Bit_Start                                                                       0
#define cAf6_cfg0_pen_Active_Bit_End                                                                         0
#define cAf6_cfg0_pen_Active_Mask                                                                        cBit0
#define cAf6_cfg0_pen_Active_Shift                                                                           0
#define cAf6_cfg0_pen_Active_MaxVal                                                                        0x1
#define cAf6_cfg0_pen_Active_MinVal                                                                        0x0
#define cAf6_cfg0_pen_Active_RstVal                                                                        0x1


/*------------------------------------------------------------------------------
Reg Name   : Global Configure 0
Reg Addr   : 0x001
Reg Formula: 0x001
    Where  :
Reg Desc   :
Used to configure:
- Force status MUX Engine
- Flush MUX Engine

------------------------------------------------------------------------------*/
#define cAf6Reg_cfg1_pen_Base                                                                            0x001
#define cAf6Reg_cfg1_pen                                                                               0x001UL
#define cAf6Reg_cfg1_pen_WidthVal                                                                           32
#define cAf6Reg_cfg1_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: CFG_Force_MUX_Engine_En
BitField Type: R/W
BitField Desc: CFG_Force_MUX_Engine_En
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_cfg1_pen_CFG_Force_MUX_Engine_En_Bit_Start                                                      4
#define cAf6_cfg1_pen_CFG_Force_MUX_Engine_En_Bit_End                                                        4
#define cAf6_cfg1_pen_CFG_Force_MUX_Engine_En_Mask                                                       cBit4
#define cAf6_cfg1_pen_CFG_Force_MUX_Engine_En_Shift                                                          4
#define cAf6_cfg1_pen_CFG_Force_MUX_Engine_En_MaxVal                                                       0x1
#define cAf6_cfg1_pen_CFG_Force_MUX_Engine_En_MinVal                                                       0x0
#define cAf6_cfg1_pen_CFG_Force_MUX_Engine_En_RstVal                                                       0x0

/*--------------------------------------
BitField Name: CFG_MUX_Engine_Read_Source_ID
BitField Type: R/W
BitField Desc: CFG_MUX_Engine_Read_Source_ID
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_cfg1_pen_CFG_MUX_Engine_Read_Source_ID_Bit_Start                                                2
#define cAf6_cfg1_pen_CFG_MUX_Engine_Read_Source_ID_Bit_End                                                  3
#define cAf6_cfg1_pen_CFG_MUX_Engine_Read_Source_ID_Mask                                               cBit3_2
#define cAf6_cfg1_pen_CFG_MUX_Engine_Read_Source_ID_Shift                                                    2
#define cAf6_cfg1_pen_CFG_MUX_Engine_Read_Source_ID_MaxVal                                                 0x3
#define cAf6_cfg1_pen_CFG_MUX_Engine_Read_Source_ID_MinVal                                                 0x0
#define cAf6_cfg1_pen_CFG_MUX_Engine_Read_Source_ID_RstVal                                                 0x0

/*--------------------------------------
BitField Name: CFG_MUX_Engine_State
BitField Type: R/W
BitField Desc: CFG_MUX_Engine_State
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_cfg1_pen_CFG_MUX_Engine_State_Bit_Start                                                         0
#define cAf6_cfg1_pen_CFG_MUX_Engine_State_Bit_End                                                           1
#define cAf6_cfg1_pen_CFG_MUX_Engine_State_Mask                                                        cBit1_0
#define cAf6_cfg1_pen_CFG_MUX_Engine_State_Shift                                                             0
#define cAf6_cfg1_pen_CFG_MUX_Engine_State_MaxVal                                                          0x3
#define cAf6_cfg1_pen_CFG_MUX_Engine_State_MinVal                                                          0x0
#define cAf6_cfg1_pen_CFG_MUX_Engine_State_RstVal                                                          0x0

/*------------------------------------------------------------------------------
Reg Name   : PTCH Number for eXAUI to XFI Direction
Reg Addr   : 0x002
Reg Formula: 0x002
    Where  :
Reg Desc   :
Used to configure Offset for eXAUI#0 to XFI
- eXAUI format: Channel_ID[1 Byte] + Type[1 Byte] + Payload + BIP8[1 Byte]
- XFI for format for eXAUI#0: 80 + [Channel+Offset][1 Byte] + Payload + FCS[4 Byte]
- XFI for format for eXAUI#1(EOS): 80 + PTCH_ID[1 Byte] + Payload + FCS[4 Byte]

------------------------------------------------------------------------------*/
#define cAf6Reg_cfg2_pen_Base                                                                            0x002
#define cAf6Reg_cfg2_pen                                                                               0x002UL
#define cAf6Reg_cfg2_pen_WidthVal                                                                           32
#define cAf6Reg_cfg2_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: Channel_ID
BitField Type: R/W
BitField Desc: Channel_ID
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_cfg2_pen_Channel_ID_Bit_Start                                                                  16
#define cAf6_cfg2_pen_Channel_ID_Bit_End                                                                    23
#define cAf6_cfg2_pen_Channel_ID_Mask                                                                cBit23_16
#define cAf6_cfg2_pen_Channel_ID_Shift                                                                      16
#define cAf6_cfg2_pen_Channel_ID_MaxVal                                                                   0xff
#define cAf6_cfg2_pen_Channel_ID_MinVal                                                                    0x0
#define cAf6_cfg2_pen_Channel_ID_RstVal                                                                   0x80

/*--------------------------------------
BitField Name: PTCHID_eXAUI1
BitField Type: R/W
BitField Desc: PTCH number Value
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_cfg2_pen_PTCHID_eXAUI1_Bit_Start                                                                8
#define cAf6_cfg2_pen_PTCHID_eXAUI1_Bit_End                                                                 15
#define cAf6_cfg2_pen_PTCHID_eXAUI1_Mask                                                              cBit15_8
#define cAf6_cfg2_pen_PTCHID_eXAUI1_Shift                                                                    8
#define cAf6_cfg2_pen_PTCHID_eXAUI1_MaxVal                                                                0xff
#define cAf6_cfg2_pen_PTCHID_eXAUI1_MinVal                                                                 0x0
#define cAf6_cfg2_pen_PTCHID_eXAUI1_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: OFFSET_eXAUI0
BitField Type: R/W
BitField Desc: Offset Value
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_cfg2_pen_OFFSET_eXAUI0_Bit_Start                                                                0
#define cAf6_cfg2_pen_OFFSET_eXAUI0_Bit_End                                                                  7
#define cAf6_cfg2_pen_OFFSET_eXAUI0_Mask                                                               cBit7_0
#define cAf6_cfg2_pen_OFFSET_eXAUI0_Shift                                                                    0
#define cAf6_cfg2_pen_OFFSET_eXAUI0_MaxVal                                                                0xff
#define cAf6_cfg2_pen_OFFSET_eXAUI0_MinVal                                                                 0x0
#define cAf6_cfg2_pen_OFFSET_eXAUI0_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : VLAN LUT for 64 VCG of eXAUI#1 to XFI
Reg Addr   : 0x100-0x1FF
Reg Formula: 0x100+$chid
    Where  :
           + $chid(0-255): Channel ID
Reg Desc   :
VLAN LUT from channel ID.
- eXAUI format: Channel_ID[1 Byte] + Type[1 Byte] + Payload + BIP8[1 Byte]
-  XFI for format for eXAUI#0: 80 + [Channel+Offset][1 Byte] + Payload + FCS[4 Byte]
-  XFI for format for eXAUI#1(EOS): 80 + PTCH_ID[1 Byte] + Payload[DA+SA+VLAN(insert)+....] + FCS[4 Byte]

------------------------------------------------------------------------------*/
#define cAf6Reg_cfg3_pen_Base                                                                            0x100
#define cAf6Reg_cfg3_pen(chid)                                                                (0x100UL+(chid))
#define cAf6Reg_cfg3_pen_WidthVal                                                                           32
#define cAf6Reg_cfg3_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: EtheType
BitField Type: R/W
BitField Desc: Ethernet type
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_cfg3_pen_EtheType_Bit_Start                                                                    16
#define cAf6_cfg3_pen_EtheType_Bit_End                                                                      31
#define cAf6_cfg3_pen_EtheType_Mask                                                                  cBit31_16
#define cAf6_cfg3_pen_EtheType_Shift                                                                        16
#define cAf6_cfg3_pen_EtheType_MaxVal                                                                   0xffff
#define cAf6_cfg3_pen_EtheType_MinVal                                                                      0x0
#define cAf6_cfg3_pen_EtheType_RstVal                                                                   0x8100

/*--------------------------------------
BitField Name: EtherPri
BitField Type: R/W
BitField Desc: Priority
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_cfg3_pen_EtherPri_Bit_Start                                                                    13
#define cAf6_cfg3_pen_EtherPri_Bit_End                                                                      15
#define cAf6_cfg3_pen_EtherPri_Mask                                                                  cBit15_13
#define cAf6_cfg3_pen_EtherPri_Shift                                                                        13
#define cAf6_cfg3_pen_EtherPri_MaxVal                                                                      0x7
#define cAf6_cfg3_pen_EtherPri_MinVal                                                                      0x0
#define cAf6_cfg3_pen_EtherPri_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: EtherCFI
BitField Type: R/W
BitField Desc: CFI
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_cfg3_pen_EtherCFI_Bit_Start                                                                    12
#define cAf6_cfg3_pen_EtherCFI_Bit_End                                                                      12
#define cAf6_cfg3_pen_EtherCFI_Mask                                                                     cBit12
#define cAf6_cfg3_pen_EtherCFI_Shift                                                                        12
#define cAf6_cfg3_pen_EtherCFI_MaxVal                                                                      0x1
#define cAf6_cfg3_pen_EtherCFI_MinVal                                                                      0x0
#define cAf6_cfg3_pen_EtherCFI_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: EtherVLANID
BitField Type: R/W
BitField Desc: Vlan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_cfg3_pen_EtherVLANID_Bit_Start                                                                  0
#define cAf6_cfg3_pen_EtherVLANID_Bit_End                                                                   11
#define cAf6_cfg3_pen_EtherVLANID_Mask                                                                cBit11_0
#define cAf6_cfg3_pen_EtherVLANID_Shift                                                                      0
#define cAf6_cfg3_pen_EtherVLANID_MaxVal                                                                 0xfff
#define cAf6_cfg3_pen_EtherVLANID_MinVal                                                                   0x0
#define cAf6_cfg3_pen_EtherVLANID_RstVal                                                                   0x0

/*------------------------------------------------------------------------------
Reg Name   : Global Counter for MUX and eXAUI#0 Buffer
Reg Addr   : 0x800-0x81F
Reg Formula: 0x800+ $cntid
    Where  :
           + $cntid(0-31) : counter id
Reg Desc   :
Support Global Counter:
- cntid = 00 : Packet Input MUX Source0 for PW
- cntid = 01 : Packet Input MUX Source1 for eXAUI#0
- cntid = 02 : Packet Input MUX Source2 for iMSG/EOP
- cntid = 03 : Packet Input MUX Source3 for eXAUI#1
- cntid = 04 : Byte Input MUX Source0 for PW
- cntid = 05 : Byte Input MUX Source1 for eXAUI#0
- cntid = 06 : Byte Input MUX Source2 for iMSG/EOP
- cntid = 07 : Byte Input MUX Source3 for eXAUI#1
- cntid = 08 : Packet Output MUX Source0 for PW
- cntid = 09 : Packet Output MUX Source1 for eXAUI#0
- cntid = 10 : Packet Output MUX Source2 for iMSG/EOP
- cntid = 11 : Packet Output MUX Source3 for eXAUI#1
- cntid = 12 : Byte Output MUX Source0 for PW
- cntid = 13 : Byte Output MUX Source1 for eXAUI#0
- cntid = 14 : Byte Output MUX Source2 for iMSG/EOP
- cntid = 15 : Byte Output MUX Source3 for eXAUI#1
- cntid = 16 : Packet Output to TX MAC Source0 for PW
- cntid = 17 : Packet Output to TX MAC Source1 for eXAUI#0
- cntid = 18 : Packet Output to TX MAC Source2 for iMSG/EOP
- cntid = 19 : Packet Output to TX MAC Source3 for eXAUI#1
- cntid = 20 : Byte Output to TX MAC Source0 for PW
- cntid = 21 : Byte Output to TX MAC Source1 for eXAUI#0
- cntid = 22 : Byte Output to TX MAC Source2 for iMSG/EOP
- cntid = 23 : Byte Output to TX MAC Source3 for eXAUI#1
- cntid = 24 : Input eXAUI#0 Buffer Error(FCS or MTU over)
- cntid = 25 : Input eXAUI#0 Buffer Monitor MTU over
- cntid = 26 : eXAUI#0 Buffer Full
- cntid = 27 : Reserved
- cntid = 28 : Reserved
- cntid = 29 : Reserved
- cntid = 30 : Reserved
- cntid = 31 : Reserved

------------------------------------------------------------------------------*/
#define cAf6Reg_pmcnt_pen_Base                                                                           0x800
#define cAf6Reg_pmcnt_pen(cntid)                                                             (0x800UL+(cntid))
#define cAf6Reg_pmcnt_pen_WidthVal                                                                          32
#define cAf6Reg_pmcnt_pen_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: mux_cnt
BitField Type: R/W
BitField Desc: Value of counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_pmcnt_pen_mux_cnt_Bit_Start                                                                     0
#define cAf6_pmcnt_pen_mux_cnt_Bit_End                                                                      31
#define cAf6_pmcnt_pen_mux_cnt_Mask                                                                   cBit31_0
#define cAf6_pmcnt_pen_mux_cnt_Shift                                                                         0
#define cAf6_pmcnt_pen_mux_cnt_MaxVal                                                               0xffffffff
#define cAf6_pmcnt_pen_mux_cnt_MinVal                                                                      0x0
#define cAf6_pmcnt_pen_mux_cnt_RstVal                                                                      0x0

/*------------------------------------------------------------------------------
Reg Name   : Stick 0
Reg Addr   : 0x400
Reg Formula:
    Where  :
Reg Desc   :
Used to debug MPEG

------------------------------------------------------------------------------*/
#define cAf6Reg_stk0_pen_Base                                                                            0x400
#define cAf6Reg_stk0_pen                                                                               0x400UL
#define cAf6Reg_stk0_pen_WidthVal                                                                           32
#define cAf6Reg_stk0_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: Input_From_PW_path
BitField Type: R/W
BitField Desc: Input From PW Path
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_stk0_pen_Input_From_PW_path_Bit_Start                                                          31
#define cAf6_stk0_pen_Input_From_PW_path_Bit_End                                                            31
#define cAf6_stk0_pen_Input_From_PW_path_Mask                                                           cBit31
#define cAf6_stk0_pen_Input_From_PW_path_Shift                                                              31
#define cAf6_stk0_pen_Input_From_PW_path_MaxVal                                                            0x1
#define cAf6_stk0_pen_Input_From_PW_path_MinVal                                                            0x0
#define cAf6_stk0_pen_Input_From_PW_path_RstVal                                                            0x0

/*--------------------------------------
BitField Name: Input_From_eXAUI0_path
BitField Type: R/W
BitField Desc: Input From eXAUI0 Path
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_stk0_pen_Input_From_eXAUI0_path_Bit_Start                                                      30
#define cAf6_stk0_pen_Input_From_eXAUI0_path_Bit_End                                                        30
#define cAf6_stk0_pen_Input_From_eXAUI0_path_Mask                                                       cBit30
#define cAf6_stk0_pen_Input_From_eXAUI0_path_Shift                                                          30
#define cAf6_stk0_pen_Input_From_eXAUI0_path_MaxVal                                                        0x1
#define cAf6_stk0_pen_Input_From_eXAUI0_path_MinVal                                                        0x0
#define cAf6_stk0_pen_Input_From_eXAUI0_path_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Input_From_iMSG_EOP_path
BitField Type: R/W
BitField Desc: Input From iMSG EOP Path
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_stk0_pen_Input_From_iMSG_EOP_path_Bit_Start                                                    29
#define cAf6_stk0_pen_Input_From_iMSG_EOP_path_Bit_End                                                      29
#define cAf6_stk0_pen_Input_From_iMSG_EOP_path_Mask                                                     cBit29
#define cAf6_stk0_pen_Input_From_iMSG_EOP_path_Shift                                                        29
#define cAf6_stk0_pen_Input_From_iMSG_EOP_path_MaxVal                                                      0x1
#define cAf6_stk0_pen_Input_From_iMSG_EOP_path_MinVal                                                      0x0
#define cAf6_stk0_pen_Input_From_iMSG_EOP_path_RstVal                                                      0x0

/*--------------------------------------
BitField Name: Input_From_eXAUI1_path
BitField Type: R/W
BitField Desc: Input From eXAUI1 Path
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_stk0_pen_Input_From_eXAUI1_path_Bit_Start                                                      28
#define cAf6_stk0_pen_Input_From_eXAUI1_path_Bit_End                                                        28
#define cAf6_stk0_pen_Input_From_eXAUI1_path_Mask                                                       cBit28
#define cAf6_stk0_pen_Input_From_eXAUI1_path_Shift                                                          28
#define cAf6_stk0_pen_Input_From_eXAUI1_path_MaxVal                                                        0x1
#define cAf6_stk0_pen_Input_From_eXAUI1_path_MinVal                                                        0x0
#define cAf6_stk0_pen_Input_From_eXAUI1_path_RstVal                                                        0x0

/*--------------------------------------
BitField Name: Output_Request_To_PW_path
BitField Type: R/W
BitField Desc: Output Request to PW Path
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_stk0_pen_Output_Request_To_PW_path_Bit_Start                                                   27
#define cAf6_stk0_pen_Output_Request_To_PW_path_Bit_End                                                     27
#define cAf6_stk0_pen_Output_Request_To_PW_path_Mask                                                    cBit27
#define cAf6_stk0_pen_Output_Request_To_PW_path_Shift                                                       27
#define cAf6_stk0_pen_Output_Request_To_PW_path_MaxVal                                                     0x1
#define cAf6_stk0_pen_Output_Request_To_PW_path_MinVal                                                     0x0
#define cAf6_stk0_pen_Output_Request_To_PW_path_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Output_Request_To_eXAUI0_path
BitField Type: R/W
BitField Desc: Output Request to eXAUI0 Path
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_stk0_pen_Output_Request_To_eXAUI0_path_Bit_Start                                               26
#define cAf6_stk0_pen_Output_Request_To_eXAUI0_path_Bit_End                                                 26
#define cAf6_stk0_pen_Output_Request_To_eXAUI0_path_Mask                                                cBit26
#define cAf6_stk0_pen_Output_Request_To_eXAUI0_path_Shift                                                   26
#define cAf6_stk0_pen_Output_Request_To_eXAUI0_path_MaxVal                                                 0x1
#define cAf6_stk0_pen_Output_Request_To_eXAUI0_path_MinVal                                                 0x0
#define cAf6_stk0_pen_Output_Request_To_eXAUI0_path_RstVal                                                 0x0

/*--------------------------------------
BitField Name: Output_Request_To_iMSG_EOP_path
BitField Type: R/W
BitField Desc: Output Request to iMSG/EOP Path
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_stk0_pen_Output_Request_To_iMSG_EOP_path_Bit_Start                                             25
#define cAf6_stk0_pen_Output_Request_To_iMSG_EOP_path_Bit_End                                               25
#define cAf6_stk0_pen_Output_Request_To_iMSG_EOP_path_Mask                                              cBit25
#define cAf6_stk0_pen_Output_Request_To_iMSG_EOP_path_Shift                                                 25
#define cAf6_stk0_pen_Output_Request_To_iMSG_EOP_path_MaxVal                                               0x1
#define cAf6_stk0_pen_Output_Request_To_iMSG_EOP_path_MinVal                                               0x0
#define cAf6_stk0_pen_Output_Request_To_iMSG_EOP_path_RstVal                                               0x0

/*--------------------------------------
BitField Name: Output_Request_To_eXAUI1_path
BitField Type: R/W
BitField Desc: Output Request to eXAUI1 Path
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_stk0_pen_Output_Request_To_eXAUI1_path_Bit_Start                                               24
#define cAf6_stk0_pen_Output_Request_To_eXAUI1_path_Bit_End                                                 24
#define cAf6_stk0_pen_Output_Request_To_eXAUI1_path_Mask                                                cBit24
#define cAf6_stk0_pen_Output_Request_To_eXAUI1_path_Shift                                                   24
#define cAf6_stk0_pen_Output_Request_To_eXAUI1_path_MaxVal                                                 0x1
#define cAf6_stk0_pen_Output_Request_To_eXAUI1_path_MinVal                                                 0x0
#define cAf6_stk0_pen_Output_Request_To_eXAUI1_path_RstVal                                                 0x0

/*--------------------------------------
BitField Name: Output_MUX_Source0_for_PW_En
BitField Type: R/W
BitField Desc: Output MUX Source0 for PW Enable
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_stk0_pen_Output_MUX_Source0_for_PW_En_Bit_Start                                                23
#define cAf6_stk0_pen_Output_MUX_Source0_for_PW_En_Bit_End                                                  23
#define cAf6_stk0_pen_Output_MUX_Source0_for_PW_En_Mask                                                 cBit23
#define cAf6_stk0_pen_Output_MUX_Source0_for_PW_En_Shift                                                    23
#define cAf6_stk0_pen_Output_MUX_Source0_for_PW_En_MaxVal                                                  0x1
#define cAf6_stk0_pen_Output_MUX_Source0_for_PW_En_MinVal                                                  0x0
#define cAf6_stk0_pen_Output_MUX_Source0_for_PW_En_RstVal                                                  0x0

/*--------------------------------------
BitField Name: Output_MUX_Source1_for_eXAUI0_En
BitField Type: R/W
BitField Desc: Output MUX Source1 for eXAUI0 Enable
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_stk0_pen_Output_MUX_Source1_for_eXAUI0_En_Bit_Start                                            22
#define cAf6_stk0_pen_Output_MUX_Source1_for_eXAUI0_En_Bit_End                                              22
#define cAf6_stk0_pen_Output_MUX_Source1_for_eXAUI0_En_Mask                                             cBit22
#define cAf6_stk0_pen_Output_MUX_Source1_for_eXAUI0_En_Shift                                                22
#define cAf6_stk0_pen_Output_MUX_Source1_for_eXAUI0_En_MaxVal                                              0x1
#define cAf6_stk0_pen_Output_MUX_Source1_for_eXAUI0_En_MinVal                                              0x0
#define cAf6_stk0_pen_Output_MUX_Source1_for_eXAUI0_En_RstVal                                              0x0

/*--------------------------------------
BitField Name: Output_MUX_Source2_for_iMSG_EOP_En
BitField Type: R/W
BitField Desc: Output MUX Source2 for iMSG/EOP Enable
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_stk0_pen_Output_MUX_Source2_for_iMSG_EOP_En_Bit_Start                                          21
#define cAf6_stk0_pen_Output_MUX_Source2_for_iMSG_EOP_En_Bit_End                                            21
#define cAf6_stk0_pen_Output_MUX_Source2_for_iMSG_EOP_En_Mask                                           cBit21
#define cAf6_stk0_pen_Output_MUX_Source2_for_iMSG_EOP_En_Shift                                              21
#define cAf6_stk0_pen_Output_MUX_Source2_for_iMSG_EOP_En_MaxVal                                            0x1
#define cAf6_stk0_pen_Output_MUX_Source2_for_iMSG_EOP_En_MinVal                                            0x0
#define cAf6_stk0_pen_Output_MUX_Source2_for_iMSG_EOP_En_RstVal                                            0x0

/*--------------------------------------
BitField Name: Output_MUX_Source3_for_eXAUI1_En
BitField Type: R/W
BitField Desc: Output MUX Source3 for eXAUI1 Enable
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_stk0_pen_Output_MUX_Source3_for_eXAUI1_En_Bit_Start                                            20
#define cAf6_stk0_pen_Output_MUX_Source3_for_eXAUI1_En_Bit_End                                              20
#define cAf6_stk0_pen_Output_MUX_Source3_for_eXAUI1_En_Mask                                             cBit20
#define cAf6_stk0_pen_Output_MUX_Source3_for_eXAUI1_En_Shift                                                20
#define cAf6_stk0_pen_Output_MUX_Source3_for_eXAUI1_En_MaxVal                                              0x1
#define cAf6_stk0_pen_Output_MUX_Source3_for_eXAUI1_En_MinVal                                              0x0
#define cAf6_stk0_pen_Output_MUX_Source3_for_eXAUI1_En_RstVal                                              0x0

/*--------------------------------------
BitField Name: TX_MAC_Request_to_MUX
BitField Type: R/W
BitField Desc: TX MAC Request to MUX
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_stk0_pen_TX_MAC_Request_to_MUX_Bit_Start                                                       19
#define cAf6_stk0_pen_TX_MAC_Request_to_MUX_Bit_End                                                         19
#define cAf6_stk0_pen_TX_MAC_Request_to_MUX_Mask                                                        cBit19
#define cAf6_stk0_pen_TX_MAC_Request_to_MUX_Shift                                                           19
#define cAf6_stk0_pen_TX_MAC_Request_to_MUX_MaxVal                                                         0x1
#define cAf6_stk0_pen_TX_MAC_Request_to_MUX_MinVal                                                         0x0
#define cAf6_stk0_pen_TX_MAC_Request_to_MUX_RstVal                                                         0x0

/*--------------------------------------
BitField Name: MUX_Output_Valid_to_tx_MAC
BitField Type: R/W
BitField Desc: MUX Output Valid to tx MAC
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_stk0_pen_MUX_Output_Valid_to_tx_MAC_Bit_Start                                                  18
#define cAf6_stk0_pen_MUX_Output_Valid_to_tx_MAC_Bit_End                                                    18
#define cAf6_stk0_pen_MUX_Output_Valid_to_tx_MAC_Mask                                                   cBit18
#define cAf6_stk0_pen_MUX_Output_Valid_to_tx_MAC_Shift                                                      18
#define cAf6_stk0_pen_MUX_Output_Valid_to_tx_MAC_MaxVal                                                    0x1
#define cAf6_stk0_pen_MUX_Output_Valid_to_tx_MAC_MinVal                                                    0x0
#define cAf6_stk0_pen_MUX_Output_Valid_to_tx_MAC_RstVal                                                    0x0

/*--------------------------------------
BitField Name: MUX_Request_to_all_Source
BitField Type: R/W
BitField Desc: MUX Request to all Source
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_stk0_pen_MUX_Request_to_all_Source_Bit_Start                                                   17
#define cAf6_stk0_pen_MUX_Request_to_all_Source_Bit_End                                                     17
#define cAf6_stk0_pen_MUX_Request_to_all_Source_Mask                                                    cBit17
#define cAf6_stk0_pen_MUX_Request_to_all_Source_Shift                                                       17
#define cAf6_stk0_pen_MUX_Request_to_all_Source_MaxVal                                                     0x1
#define cAf6_stk0_pen_MUX_Request_to_all_Source_MinVal                                                     0x0
#define cAf6_stk0_pen_MUX_Request_to_all_Source_RstVal                                                     0x0

/*--------------------------------------
BitField Name: MUX_Output_Valid_to_PTCH_Engine
BitField Type: R/W
BitField Desc: MUX Output Valid to PTCH Engine
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_stk0_pen_MUX_Output_Valid_to_PTCH_Engine_Bit_Start                                             16
#define cAf6_stk0_pen_MUX_Output_Valid_to_PTCH_Engine_Bit_End                                               16
#define cAf6_stk0_pen_MUX_Output_Valid_to_PTCH_Engine_Mask                                              cBit16
#define cAf6_stk0_pen_MUX_Output_Valid_to_PTCH_Engine_Shift                                                 16
#define cAf6_stk0_pen_MUX_Output_Valid_to_PTCH_Engine_MaxVal                                               0x1
#define cAf6_stk0_pen_MUX_Output_Valid_to_PTCH_Engine_MinVal                                               0x0
#define cAf6_stk0_pen_MUX_Output_Valid_to_PTCH_Engine_RstVal                                               0x0

/*--------------------------------------
BitField Name: Input_Valid_To_MUX_Source0_for_PW_En
BitField Type: R/W
BitField Desc: Input Valid To MUX Source0 for PW Enable
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source0_for_PW_En_Bit_Start                                        15
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source0_for_PW_En_Bit_End                                          15
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source0_for_PW_En_Mask                                         cBit15
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source0_for_PW_En_Shift                                            15
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source0_for_PW_En_MaxVal                                          0x1
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source0_for_PW_En_MinVal                                          0x0
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source0_for_PW_En_RstVal                                          0x0

/*--------------------------------------
BitField Name: Input_Valid_To_MUX_Source1_for_eXAUI0_En
BitField Type: R/W
BitField Desc: Input Valid To MUX Source1 for eXAUI0 Enable
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source1_for_eXAUI0_En_Bit_Start                                      14
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source1_for_eXAUI0_En_Bit_End                                      14
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source1_for_eXAUI0_En_Mask                                     cBit14
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source1_for_eXAUI0_En_Shift                                        14
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source1_for_eXAUI0_En_MaxVal                                      0x1
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source1_for_eXAUI0_En_MinVal                                      0x0
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source1_for_eXAUI0_En_RstVal                                      0x0

/*--------------------------------------
BitField Name: Input_Valid_To_MUX_Source2_for_iMSG_EOP_En
BitField Type: R/W
BitField Desc: Input Valid To MUX Source2 for iMSG/EOP Enable
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source2_for_iMSG_EOP_En_Bit_Start                                      13
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source2_for_iMSG_EOP_En_Bit_End                                      13
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source2_for_iMSG_EOP_En_Mask                                   cBit13
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source2_for_iMSG_EOP_En_Shift                                      13
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source2_for_iMSG_EOP_En_MaxVal                                     0x1
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source2_for_iMSG_EOP_En_MinVal                                     0x0
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source2_for_iMSG_EOP_En_RstVal                                     0x0

/*--------------------------------------
BitField Name: Input_Valid_To_MUX_Source3_for_eXAUI1_En
BitField Type: R/W
BitField Desc: Input Valid To MUX Source3 for eXAUI1 Enable
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source3_for_eXAUI1_En_Bit_Start                                      12
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source3_for_eXAUI1_En_Bit_End                                      12
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source3_for_eXAUI1_En_Mask                                     cBit12
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source3_for_eXAUI1_En_Shift                                        12
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source3_for_eXAUI1_En_MaxVal                                      0x1
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source3_for_eXAUI1_En_MinVal                                      0x0
#define cAf6_stk0_pen_Input_Valid_To_MUX_Source3_for_eXAUI1_En_RstVal                                      0x0

/*--------------------------------------
BitField Name: Output_Request_To_FIFO_Source0_for_PW_En
BitField Type: R/W
BitField Desc: Output Request To FIFO Source0 for PW Enable
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source0_for_PW_En_Bit_Start                                      11
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source0_for_PW_En_Bit_End                                      11
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source0_for_PW_En_Mask                                     cBit11
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source0_for_PW_En_Shift                                        11
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source0_for_PW_En_MaxVal                                      0x1
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source0_for_PW_En_MinVal                                      0x0
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source0_for_PW_En_RstVal                                      0x0

/*--------------------------------------
BitField Name: Output_Request_To_FIFO_Source1_for_eXAUI0_En
BitField Type: R/W
BitField Desc: Output Request To FIFO Source1 for eXAUI0 Enable
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source1_for_eXAUI0_En_Bit_Start                                      10
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source1_for_eXAUI0_En_Bit_End                                      10
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source1_for_eXAUI0_En_Mask                                  cBit10
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source1_for_eXAUI0_En_Shift                                      10
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source1_for_eXAUI0_En_MaxVal                                     0x1
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source1_for_eXAUI0_En_MinVal                                     0x0
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source1_for_eXAUI0_En_RstVal                                     0x0

/*--------------------------------------
BitField Name: Output_Request_To_FIFO_Source2_for_iMSG_EOP_En
BitField Type: R/W
BitField Desc: Output Request To FIFO Source2 for iMSG/EOP Enable
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source2_for_iMSG_EOP_En_Bit_Start                                       9
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source2_for_iMSG_EOP_En_Bit_End                                       9
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source2_for_iMSG_EOP_En_Mask                                   cBit9
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source2_for_iMSG_EOP_En_Shift                                       9
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source2_for_iMSG_EOP_En_MaxVal                                     0x1
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source2_for_iMSG_EOP_En_MinVal                                     0x0
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source2_for_iMSG_EOP_En_RstVal                                     0x0

/*--------------------------------------
BitField Name: Output_Request_To_FIFO_Source3_for_eXAUI1_En
BitField Type: R/W
BitField Desc: Output Request To FIFO Source3 for eXAUI1 Enable
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source3_for_eXAUI1_En_Bit_Start                                       8
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source3_for_eXAUI1_En_Bit_End                                       8
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source3_for_eXAUI1_En_Mask                                   cBit8
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source3_for_eXAUI1_En_Shift                                       8
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source3_for_eXAUI1_En_MaxVal                                     0x1
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source3_for_eXAUI1_En_MinVal                                     0x0
#define cAf6_stk0_pen_Output_Request_To_FIFO_Source3_for_eXAUI1_En_RstVal                                     0x0

/*--------------------------------------
BitField Name: FiFO_Source3_Write_Err
BitField Type: R/W
BitField Desc: FiFO Source3 Write Err
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_stk0_pen_FiFO_Source3_Write_Err_Bit_Start                                                       7
#define cAf6_stk0_pen_FiFO_Source3_Write_Err_Bit_End                                                         7
#define cAf6_stk0_pen_FiFO_Source3_Write_Err_Mask                                                        cBit7
#define cAf6_stk0_pen_FiFO_Source3_Write_Err_Shift                                                           7
#define cAf6_stk0_pen_FiFO_Source3_Write_Err_MaxVal                                                        0x1
#define cAf6_stk0_pen_FiFO_Source3_Write_Err_MinVal                                                        0x0
#define cAf6_stk0_pen_FiFO_Source3_Write_Err_RstVal                                                        0x0

/*--------------------------------------
BitField Name: FiFO_Source2_Write_Err
BitField Type: R/W
BitField Desc: FiFO Source2 Write Err
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_stk0_pen_FiFO_Source2_Write_Err_Bit_Start                                                       6
#define cAf6_stk0_pen_FiFO_Source2_Write_Err_Bit_End                                                         6
#define cAf6_stk0_pen_FiFO_Source2_Write_Err_Mask                                                        cBit6
#define cAf6_stk0_pen_FiFO_Source2_Write_Err_Shift                                                           6
#define cAf6_stk0_pen_FiFO_Source2_Write_Err_MaxVal                                                        0x1
#define cAf6_stk0_pen_FiFO_Source2_Write_Err_MinVal                                                        0x0
#define cAf6_stk0_pen_FiFO_Source2_Write_Err_RstVal                                                        0x0

/*--------------------------------------
BitField Name: FiFO_Source1_Write_Err
BitField Type: R/W
BitField Desc: FiFO Source1 Write Err
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_stk0_pen_FiFO_Source1_Write_Err_Bit_Start                                                       5
#define cAf6_stk0_pen_FiFO_Source1_Write_Err_Bit_End                                                         5
#define cAf6_stk0_pen_FiFO_Source1_Write_Err_Mask                                                        cBit5
#define cAf6_stk0_pen_FiFO_Source1_Write_Err_Shift                                                           5
#define cAf6_stk0_pen_FiFO_Source1_Write_Err_MaxVal                                                        0x1
#define cAf6_stk0_pen_FiFO_Source1_Write_Err_MinVal                                                        0x0
#define cAf6_stk0_pen_FiFO_Source1_Write_Err_RstVal                                                        0x0

/*--------------------------------------
BitField Name: FiFO_Source0_Write_Err
BitField Type: R/W
BitField Desc: FiFO Source0 Write Err
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_stk0_pen_FiFO_Source0_Write_Err_Bit_Start                                                       4
#define cAf6_stk0_pen_FiFO_Source0_Write_Err_Bit_End                                                         4
#define cAf6_stk0_pen_FiFO_Source0_Write_Err_Mask                                                        cBit4
#define cAf6_stk0_pen_FiFO_Source0_Write_Err_Shift                                                           4
#define cAf6_stk0_pen_FiFO_Source0_Write_Err_MaxVal                                                        0x1
#define cAf6_stk0_pen_FiFO_Source0_Write_Err_MinVal                                                        0x0
#define cAf6_stk0_pen_FiFO_Source0_Write_Err_RstVal                                                        0x0

/*--------------------------------------
BitField Name: FiFO_Source3_Read_Err
BitField Type: R/W
BitField Desc: FiFO Source3 Read Err
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_stk0_pen_FiFO_Source3_Read_Err_Bit_Start                                                        3
#define cAf6_stk0_pen_FiFO_Source3_Read_Err_Bit_End                                                          3
#define cAf6_stk0_pen_FiFO_Source3_Read_Err_Mask                                                         cBit3
#define cAf6_stk0_pen_FiFO_Source3_Read_Err_Shift                                                            3
#define cAf6_stk0_pen_FiFO_Source3_Read_Err_MaxVal                                                         0x1
#define cAf6_stk0_pen_FiFO_Source3_Read_Err_MinVal                                                         0x0
#define cAf6_stk0_pen_FiFO_Source3_Read_Err_RstVal                                                         0x0

/*--------------------------------------
BitField Name: FiFO_Source2_Read_Err
BitField Type: R/W
BitField Desc: FiFO Source2 Read Err
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_stk0_pen_FiFO_Source2_Read_Err_Bit_Start                                                        2
#define cAf6_stk0_pen_FiFO_Source2_Read_Err_Bit_End                                                          2
#define cAf6_stk0_pen_FiFO_Source2_Read_Err_Mask                                                         cBit2
#define cAf6_stk0_pen_FiFO_Source2_Read_Err_Shift                                                            2
#define cAf6_stk0_pen_FiFO_Source2_Read_Err_MaxVal                                                         0x1
#define cAf6_stk0_pen_FiFO_Source2_Read_Err_MinVal                                                         0x0
#define cAf6_stk0_pen_FiFO_Source2_Read_Err_RstVal                                                         0x0

/*--------------------------------------
BitField Name: FiFO_Source1_Read_Err
BitField Type: R/W
BitField Desc: FiFO Source1 Read Err
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_stk0_pen_FiFO_Source1_Read_Err_Bit_Start                                                        1
#define cAf6_stk0_pen_FiFO_Source1_Read_Err_Bit_End                                                          1
#define cAf6_stk0_pen_FiFO_Source1_Read_Err_Mask                                                         cBit1
#define cAf6_stk0_pen_FiFO_Source1_Read_Err_Shift                                                            1
#define cAf6_stk0_pen_FiFO_Source1_Read_Err_MaxVal                                                         0x1
#define cAf6_stk0_pen_FiFO_Source1_Read_Err_MinVal                                                         0x0
#define cAf6_stk0_pen_FiFO_Source1_Read_Err_RstVal                                                         0x0

/*--------------------------------------
BitField Name: FiFO_Source0_Read_Err
BitField Type: R/W
BitField Desc: FiFO Source0 Read Err
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_stk0_pen_FiFO_Source0_Read_Err_Bit_Start                                                        0
#define cAf6_stk0_pen_FiFO_Source0_Read_Err_Bit_End                                                          0
#define cAf6_stk0_pen_FiFO_Source0_Read_Err_Mask                                                         cBit0
#define cAf6_stk0_pen_FiFO_Source0_Read_Err_Shift                                                            0
#define cAf6_stk0_pen_FiFO_Source0_Read_Err_MaxVal                                                         0x1
#define cAf6_stk0_pen_FiFO_Source0_Read_Err_MinVal                                                         0x0
#define cAf6_stk0_pen_FiFO_Source0_Read_Err_RstVal                                                         0x0

/*------------------------------------------------------------------------------
Reg Name   : Stick 1
Reg Addr   : 0x401
Reg Formula:
    Where  :
Reg Desc   :
Used to debug MPEG

------------------------------------------------------------------------------*/
#define cAf6Reg_stk1_pen_Base                                                                            0x401
#define cAf6Reg_stk1_pen                                                                               0x401UL
#define cAf6Reg_stk1_pen_WidthVal                                                                           32
#define cAf6Reg_stk1_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: MUX_Engine_State_SOP
BitField Type: R/W
BitField Desc: MUX Engine State SOP
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_State_SOP_Bit_Start                                                        31
#define cAf6_stk1_pen_MUX_Engine_State_SOP_Bit_End                                                          31
#define cAf6_stk1_pen_MUX_Engine_State_SOP_Mask                                                         cBit31
#define cAf6_stk1_pen_MUX_Engine_State_SOP_Shift                                                            31
#define cAf6_stk1_pen_MUX_Engine_State_SOP_MaxVal                                                          0x1
#define cAf6_stk1_pen_MUX_Engine_State_SOP_MinVal                                                          0x0
#define cAf6_stk1_pen_MUX_Engine_State_SOP_RstVal                                                          0x0

/*--------------------------------------
BitField Name: MUX_Engine_State_EOP
BitField Type: R/W
BitField Desc: MUX Engine State EOP
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_State_EOP_Bit_Start                                                        30
#define cAf6_stk1_pen_MUX_Engine_State_EOP_Bit_End                                                          30
#define cAf6_stk1_pen_MUX_Engine_State_EOP_Mask                                                         cBit30
#define cAf6_stk1_pen_MUX_Engine_State_EOP_Shift                                                            30
#define cAf6_stk1_pen_MUX_Engine_State_EOP_MaxVal                                                          0x1
#define cAf6_stk1_pen_MUX_Engine_State_EOP_MinVal                                                          0x0
#define cAf6_stk1_pen_MUX_Engine_State_EOP_RstVal                                                          0x0

/*--------------------------------------
BitField Name: MUX_Engine_State_ERR
BitField Type: R/W
BitField Desc: MUX Engine State ERR
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_State_ERR_Bit_Start                                                        29
#define cAf6_stk1_pen_MUX_Engine_State_ERR_Bit_End                                                          29
#define cAf6_stk1_pen_MUX_Engine_State_ERR_Mask                                                         cBit29
#define cAf6_stk1_pen_MUX_Engine_State_ERR_Shift                                                            29
#define cAf6_stk1_pen_MUX_Engine_State_ERR_MaxVal                                                          0x1
#define cAf6_stk1_pen_MUX_Engine_State_ERR_MinVal                                                          0x0
#define cAf6_stk1_pen_MUX_Engine_State_ERR_RstVal                                                          0x0

/*--------------------------------------
BitField Name: MUX_Engine_State_IDLE
BitField Type: R/W
BitField Desc: MUX Engine State IDLE
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_State_IDLE_Bit_Start                                                       28
#define cAf6_stk1_pen_MUX_Engine_State_IDLE_Bit_End                                                         28
#define cAf6_stk1_pen_MUX_Engine_State_IDLE_Mask                                                        cBit28
#define cAf6_stk1_pen_MUX_Engine_State_IDLE_Shift                                                           28
#define cAf6_stk1_pen_MUX_Engine_State_IDLE_MaxVal                                                         0x1
#define cAf6_stk1_pen_MUX_Engine_State_IDLE_MinVal                                                         0x0
#define cAf6_stk1_pen_MUX_Engine_State_IDLE_RstVal                                                         0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source3_State_IDLE
BitField Type: R/W
BitField Desc: MUX Engine Source3 State IDLE
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source3_State_IDLE_Bit_Start                                               27
#define cAf6_stk1_pen_MUX_Engine_Source3_State_IDLE_Bit_End                                                 27
#define cAf6_stk1_pen_MUX_Engine_Source3_State_IDLE_Mask                                                cBit27
#define cAf6_stk1_pen_MUX_Engine_Source3_State_IDLE_Shift                                                   27
#define cAf6_stk1_pen_MUX_Engine_Source3_State_IDLE_MaxVal                                                 0x1
#define cAf6_stk1_pen_MUX_Engine_Source3_State_IDLE_MinVal                                                 0x0
#define cAf6_stk1_pen_MUX_Engine_Source3_State_IDLE_RstVal                                                 0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source2_State_IDLE
BitField Type: R/W
BitField Desc: MUX Engine Source2 State IDLE
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source2_State_IDLE_Bit_Start                                               26
#define cAf6_stk1_pen_MUX_Engine_Source2_State_IDLE_Bit_End                                                 26
#define cAf6_stk1_pen_MUX_Engine_Source2_State_IDLE_Mask                                                cBit26
#define cAf6_stk1_pen_MUX_Engine_Source2_State_IDLE_Shift                                                   26
#define cAf6_stk1_pen_MUX_Engine_Source2_State_IDLE_MaxVal                                                 0x1
#define cAf6_stk1_pen_MUX_Engine_Source2_State_IDLE_MinVal                                                 0x0
#define cAf6_stk1_pen_MUX_Engine_Source2_State_IDLE_RstVal                                                 0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source1_State_IDLE
BitField Type: R/W
BitField Desc: MUX Engine Source1 State IDLE
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source1_State_IDLE_Bit_Start                                               25
#define cAf6_stk1_pen_MUX_Engine_Source1_State_IDLE_Bit_End                                                 25
#define cAf6_stk1_pen_MUX_Engine_Source1_State_IDLE_Mask                                                cBit25
#define cAf6_stk1_pen_MUX_Engine_Source1_State_IDLE_Shift                                                   25
#define cAf6_stk1_pen_MUX_Engine_Source1_State_IDLE_MaxVal                                                 0x1
#define cAf6_stk1_pen_MUX_Engine_Source1_State_IDLE_MinVal                                                 0x0
#define cAf6_stk1_pen_MUX_Engine_Source1_State_IDLE_RstVal                                                 0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source0_State_IDLE
BitField Type: R/W
BitField Desc: MUX Engine Source0 State IDLE
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source0_State_IDLE_Bit_Start                                               24
#define cAf6_stk1_pen_MUX_Engine_Source0_State_IDLE_Bit_End                                                 24
#define cAf6_stk1_pen_MUX_Engine_Source0_State_IDLE_Mask                                                cBit24
#define cAf6_stk1_pen_MUX_Engine_Source0_State_IDLE_Shift                                                   24
#define cAf6_stk1_pen_MUX_Engine_Source0_State_IDLE_MaxVal                                                 0x1
#define cAf6_stk1_pen_MUX_Engine_Source0_State_IDLE_MinVal                                                 0x0
#define cAf6_stk1_pen_MUX_Engine_Source0_State_IDLE_RstVal                                                 0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source3_State_EOP
BitField Type: R/W
BitField Desc: MUX Engine Source3 State EOP
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source3_State_EOP_Bit_Start                                                23
#define cAf6_stk1_pen_MUX_Engine_Source3_State_EOP_Bit_End                                                  23
#define cAf6_stk1_pen_MUX_Engine_Source3_State_EOP_Mask                                                 cBit23
#define cAf6_stk1_pen_MUX_Engine_Source3_State_EOP_Shift                                                    23
#define cAf6_stk1_pen_MUX_Engine_Source3_State_EOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source3_State_EOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source3_State_EOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source2_State_EOP
BitField Type: R/W
BitField Desc: MUX Engine Source2 State EOP
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source2_State_EOP_Bit_Start                                                22
#define cAf6_stk1_pen_MUX_Engine_Source2_State_EOP_Bit_End                                                  22
#define cAf6_stk1_pen_MUX_Engine_Source2_State_EOP_Mask                                                 cBit22
#define cAf6_stk1_pen_MUX_Engine_Source2_State_EOP_Shift                                                    22
#define cAf6_stk1_pen_MUX_Engine_Source2_State_EOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source2_State_EOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source2_State_EOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source1_State_EOP
BitField Type: R/W
BitField Desc: MUX Engine Source1 State EOP
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source1_State_EOP_Bit_Start                                                21
#define cAf6_stk1_pen_MUX_Engine_Source1_State_EOP_Bit_End                                                  21
#define cAf6_stk1_pen_MUX_Engine_Source1_State_EOP_Mask                                                 cBit21
#define cAf6_stk1_pen_MUX_Engine_Source1_State_EOP_Shift                                                    21
#define cAf6_stk1_pen_MUX_Engine_Source1_State_EOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source1_State_EOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source1_State_EOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source0_State_EOP
BitField Type: R/W
BitField Desc: MUX Engine Source0 State EOP
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source0_State_EOP_Bit_Start                                                20
#define cAf6_stk1_pen_MUX_Engine_Source0_State_EOP_Bit_End                                                  20
#define cAf6_stk1_pen_MUX_Engine_Source0_State_EOP_Mask                                                 cBit20
#define cAf6_stk1_pen_MUX_Engine_Source0_State_EOP_Shift                                                    20
#define cAf6_stk1_pen_MUX_Engine_Source0_State_EOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source0_State_EOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source0_State_EOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source3_State_SOP
BitField Type: R/W
BitField Desc: MUX Engine Source3 State SOP
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source3_State_SOP_Bit_Start                                                19
#define cAf6_stk1_pen_MUX_Engine_Source3_State_SOP_Bit_End                                                  19
#define cAf6_stk1_pen_MUX_Engine_Source3_State_SOP_Mask                                                 cBit19
#define cAf6_stk1_pen_MUX_Engine_Source3_State_SOP_Shift                                                    19
#define cAf6_stk1_pen_MUX_Engine_Source3_State_SOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source3_State_SOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source3_State_SOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source2_State_SOP
BitField Type: R/W
BitField Desc: MUX Engine Source2 State SOP
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source2_State_SOP_Bit_Start                                                18
#define cAf6_stk1_pen_MUX_Engine_Source2_State_SOP_Bit_End                                                  18
#define cAf6_stk1_pen_MUX_Engine_Source2_State_SOP_Mask                                                 cBit18
#define cAf6_stk1_pen_MUX_Engine_Source2_State_SOP_Shift                                                    18
#define cAf6_stk1_pen_MUX_Engine_Source2_State_SOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source2_State_SOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source2_State_SOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source1_State_SOP
BitField Type: R/W
BitField Desc: MUX Engine Source1 State SOP
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source1_State_SOP_Bit_Start                                                17
#define cAf6_stk1_pen_MUX_Engine_Source1_State_SOP_Bit_End                                                  17
#define cAf6_stk1_pen_MUX_Engine_Source1_State_SOP_Mask                                                 cBit17
#define cAf6_stk1_pen_MUX_Engine_Source1_State_SOP_Shift                                                    17
#define cAf6_stk1_pen_MUX_Engine_Source1_State_SOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source1_State_SOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source1_State_SOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source0_State_SOP
BitField Type: R/W
BitField Desc: MUX Engine Source0 State SOP
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source0_State_SOP_Bit_Start                                                16
#define cAf6_stk1_pen_MUX_Engine_Source0_State_SOP_Bit_End                                                  16
#define cAf6_stk1_pen_MUX_Engine_Source0_State_SOP_Mask                                                 cBit16
#define cAf6_stk1_pen_MUX_Engine_Source0_State_SOP_Shift                                                    16
#define cAf6_stk1_pen_MUX_Engine_Source0_State_SOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source0_State_SOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source0_State_SOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source3_Error_IDLE
BitField Type: R/W
BitField Desc: MUX Engine Source3 Error IDLE
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_IDLE_Bit_Start                                               15
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_IDLE_Bit_End                                                 15
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_IDLE_Mask                                                cBit15
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_IDLE_Shift                                                   15
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_IDLE_MaxVal                                                 0x1
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_IDLE_MinVal                                                 0x0
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_IDLE_RstVal                                                 0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source2_Error_IDLE
BitField Type: R/W
BitField Desc: MUX Engine Source2 Error IDLE
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_IDLE_Bit_Start                                               14
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_IDLE_Bit_End                                                 14
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_IDLE_Mask                                                cBit14
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_IDLE_Shift                                                   14
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_IDLE_MaxVal                                                 0x1
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_IDLE_MinVal                                                 0x0
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_IDLE_RstVal                                                 0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source1_Error_IDLE
BitField Type: R/W
BitField Desc: MUX Engine Source1 Error IDLE
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_IDLE_Bit_Start                                               13
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_IDLE_Bit_End                                                 13
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_IDLE_Mask                                                cBit13
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_IDLE_Shift                                                   13
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_IDLE_MaxVal                                                 0x1
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_IDLE_MinVal                                                 0x0
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_IDLE_RstVal                                                 0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source0_Error_IDLE
BitField Type: R/W
BitField Desc: MUX Engine Source0 Error IDLE
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_IDLE_Bit_Start                                               12
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_IDLE_Bit_End                                                 12
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_IDLE_Mask                                                cBit12
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_IDLE_Shift                                                   12
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_IDLE_MaxVal                                                 0x1
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_IDLE_MinVal                                                 0x0
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_IDLE_RstVal                                                 0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source3_Error_EOP
BitField Type: R/W
BitField Desc: MUX Engine Source3 Error EOP
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_EOP_Bit_Start                                                11
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_EOP_Bit_End                                                  11
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_EOP_Mask                                                 cBit11
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_EOP_Shift                                                    11
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_EOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_EOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_EOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source2_Error_EOP
BitField Type: R/W
BitField Desc: MUX Engine Source2 Error EOP
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_EOP_Bit_Start                                                10
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_EOP_Bit_End                                                  10
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_EOP_Mask                                                 cBit10
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_EOP_Shift                                                    10
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_EOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_EOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_EOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source1_Error_EOP
BitField Type: R/W
BitField Desc: MUX Engine Source1 Error EOP
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_EOP_Bit_Start                                                 9
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_EOP_Bit_End                                                   9
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_EOP_Mask                                                  cBit9
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_EOP_Shift                                                     9
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_EOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_EOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_EOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source0_Error_EOP
BitField Type: R/W
BitField Desc: MUX Engine Source0 Error EOP
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_EOP_Bit_Start                                                 8
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_EOP_Bit_End                                                   8
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_EOP_Mask                                                  cBit8
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_EOP_Shift                                                     8
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_EOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_EOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_EOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source3_Error_SOP
BitField Type: R/W
BitField Desc: MUX Engine Source3 Error SOP
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_SOP_Bit_Start                                                 7
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_SOP_Bit_End                                                   7
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_SOP_Mask                                                  cBit7
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_SOP_Shift                                                     7
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_SOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_SOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source3_Error_SOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source2_Error_SOP
BitField Type: R/W
BitField Desc: MUX Engine Source2 Error SOP
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_SOP_Bit_Start                                                 6
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_SOP_Bit_End                                                   6
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_SOP_Mask                                                  cBit6
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_SOP_Shift                                                     6
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_SOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_SOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source2_Error_SOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source1_Error_SOP
BitField Type: R/W
BitField Desc: MUX Engine Source1 Error SOP
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_SOP_Bit_Start                                                 5
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_SOP_Bit_End                                                   5
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_SOP_Mask                                                  cBit5
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_SOP_Shift                                                     5
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_SOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_SOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source1_Error_SOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Source0_Error_SOP
BitField Type: R/W
BitField Desc: MUX Engine Source0 Error SOP
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_SOP_Bit_Start                                                 4
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_SOP_Bit_End                                                   4
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_SOP_Mask                                                  cBit4
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_SOP_Shift                                                     4
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_SOP_MaxVal                                                  0x1
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_SOP_MinVal                                                  0x0
#define cAf6_stk1_pen_MUX_Engine_Source0_Error_SOP_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MUX_Engine_Conflict_0
BitField Type: R/W
BitField Desc: MUX Engine Conflict 0
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Conflict_0_Bit_Start                                                        1
#define cAf6_stk1_pen_MUX_Engine_Conflict_0_Bit_End                                                          1
#define cAf6_stk1_pen_MUX_Engine_Conflict_0_Mask                                                         cBit1
#define cAf6_stk1_pen_MUX_Engine_Conflict_0_Shift                                                            1
#define cAf6_stk1_pen_MUX_Engine_Conflict_0_MaxVal                                                         0x1
#define cAf6_stk1_pen_MUX_Engine_Conflict_0_MinVal                                                         0x0
#define cAf6_stk1_pen_MUX_Engine_Conflict_0_RstVal                                                         0x0

/*--------------------------------------
BitField Name: MUX_Engine_Conflict_1
BitField Type: R/W
BitField Desc: MUX Engine Conflict 1
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_stk1_pen_MUX_Engine_Conflict_1_Bit_Start                                                        0
#define cAf6_stk1_pen_MUX_Engine_Conflict_1_Bit_End                                                          0
#define cAf6_stk1_pen_MUX_Engine_Conflict_1_Mask                                                         cBit0
#define cAf6_stk1_pen_MUX_Engine_Conflict_1_Shift                                                            0
#define cAf6_stk1_pen_MUX_Engine_Conflict_1_MaxVal                                                         0x1
#define cAf6_stk1_pen_MUX_Engine_Conflict_1_MinVal                                                         0x0
#define cAf6_stk1_pen_MUX_Engine_Conflict_1_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Stick 2
Reg Addr   : 0x402
Reg Formula:
    Where  :
Reg Desc   :
Used to debug MPEG

------------------------------------------------------------------------------*/
#define cAf6Reg_stk2_pen_Base                                                                            0x402
#define cAf6Reg_stk2_pen                                                                               0x402UL
#define cAf6Reg_stk2_pen_WidthVal                                                                           32
#define cAf6Reg_stk2_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: eXAUI#0_Buffer_Full
BitField Type: R/W
BitField Desc: eXAUI#0 Buffer Full
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_Bit_Start                                                          7
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_Bit_End                                                            7
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_Mask                                                           cBit7
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_Shift                                                              7
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_MaxVal                                                           0x1
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_MinVal                                                           0x0
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_RstVal                                                           0x0

/*--------------------------------------
BitField Name: Input_eXAUI#0_Err
BitField Type: R/W
BitField Desc: Input eXAUI#0 Err
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_stk2_pen_Input_eXAUI0_Err_Bit_Start                                                            6
#define cAf6_stk2_pen_Input_eXAUI0_Err_Bit_End                                                              6
#define cAf6_stk2_pen_Input_eXAUI0_Err_Mask                                                             cBit6
#define cAf6_stk2_pen_Input_eXAUI0_Err_Shift                                                                6
#define cAf6_stk2_pen_Input_eXAUI0_Err_MaxVal                                                             0x1
#define cAf6_stk2_pen_Input_eXAUI0_Err_MinVal                                                             0x0
#define cAf6_stk2_pen_Input_eXAUI0_Err_RstVal                                                             0x0

/*--------------------------------------
BitField Name: eXAUI#0_Detect_over_MTU
BitField Type: R/W
BitField Desc: eXAUI#0 Detect over MTU
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_stk2_pen_eXAUI0_Detect_over_MTU_Bit_Start                                                      5
#define cAf6_stk2_pen_eXAUI0_Detect_over_MTU_Bit_End                                                        5
#define cAf6_stk2_pen_eXAUI0_Detect_over_MTU_Mask                                                       cBit5
#define cAf6_stk2_pen_eXAUI0_Detect_over_MTU_Shift                                                          5
#define cAf6_stk2_pen_eXAUI0_Detect_over_MTU_MaxVal                                                       0x1
#define cAf6_stk2_pen_eXAUI0_Detect_over_MTU_MinVal                                                       0x0
#define cAf6_stk2_pen_eXAUI0_Detect_over_MTU_RstVal                                                       0x0

/*--------------------------------------
BitField Name: eXAUI#0_Buffer_Full_State
BitField Type: R/W
BitField Desc: eXAUI#0 Buffer State
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_State_Bit_Start                                                    4
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_State_Bit_End                                                      4
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_State_Mask                                                     cBit4
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_State_Shift                                                        4
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_State_MaxVal                                                     0x1
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_State_MinVal                                                     0x0
#define cAf6_stk2_pen_eXAUI0_Buffer_Full_State_RstVal                                                     0x0

/*--------------------------------------
BitField Name: FF_Packet_Descriptor_eXAUI0_Write_Error
BitField Type: R/W
BitField Desc: FF_Packet_Descriptor_eXAUI0_Write_Error
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Write_Error_Bit_Start                                       1
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Write_Error_Bit_End                                        1
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Write_Error_Mask                                       cBit1
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Write_Error_Shift                                          1
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Write_Error_MaxVal                                       0x1
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Write_Error_MinVal                                       0x0
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Write_Error_RstVal                                       0x0

/*--------------------------------------
BitField Name: FF_Packet_Descriptor_eXAUI0_Read_Error
BitField Type: R/W
BitField Desc: FF_Packet_Descriptor_eXAUI0_Read_Error
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Read_Error_Bit_Start                                       0
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Read_Error_Bit_End                                         0
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Read_Error_Mask                                        cBit0
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Read_Error_Shift                                           0
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Read_Error_MaxVal                                        0x1
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Read_Error_MinVal                                        0x0
#define cAf6_stk2_pen_FF_Packet_Descriptor_eXAUI0_Read_Error_RstVal                                        0x0

/*------------------------------------------------------------------------------
Reg Name   : ETH 10G Dignostic Control
Reg Addr   : 0x60
Reg Formula:
    Where  :
Reg Desc   :
This register is used to config dignostic

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_diag_ctr_Base                                                                      0x1060
#define cAf6Reg_eth10g_diag_ctr                                                                           0x1060
#define cAf6Reg_eth10g_diag_ctr_WidthVal                                                                    32
#define cAf6Reg_eth10g_diag_ctr_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: Eth10GDiagForceErr
BitField Type: RO
BitField Desc: Dignostic force error
BitField Bits: [12]
--------------------------------------*/
#define cAf6_eth10g_diag_ctr_Eth10GDiagForceErr_Bit_Start                                                   12
#define cAf6_eth10g_diag_ctr_Eth10GDiagForceErr_Bit_End                                                     12
#define cAf6_eth10g_diag_ctr_Eth10GDiagForceErr_Mask                                                    cBit12
#define cAf6_eth10g_diag_ctr_Eth10GDiagForceErr_Shift                                                       12
#define cAf6_eth10g_diag_ctr_Eth10GDiagForceErr_MaxVal                                                     0x1
#define cAf6_eth10g_diag_ctr_Eth10GDiagForceErr_MinVal                                                     0x0
#define cAf6_eth10g_diag_ctr_Eth10GDiagForceErr_RstVal                                                     0x0

/*--------------------------------------
BitField Name: Eth10GDiagGenSrc
BitField Type: RO
BitField Desc: Dignostic select source to monitor 1: TX Diag 0: Tx user
BitField Bits: [8]
--------------------------------------*/
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenSrc_Bit_Start                                                      8
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenSrc_Bit_End                                                        8
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenSrc_Mask                                                       cBit8
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenSrc_Shift                                                          8
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenSrc_MaxVal                                                       0x1
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenSrc_MinVal                                                       0x0
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenSrc_RstVal                                                       0x0

/*--------------------------------------
BitField Name: Eth10GDiagPktLenMode
BitField Type: RW
BitField Desc: packet length mode 0: fix length (use configured min length) 1:
increase 1
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_eth10g_diag_ctr_Eth10GDiagPktLenMode_Bit_Start                                                  4
#define cAf6_eth10g_diag_ctr_Eth10GDiagPktLenMode_Bit_End                                                    5
#define cAf6_eth10g_diag_ctr_Eth10GDiagPktLenMode_Mask                                                 cBit5_4
#define cAf6_eth10g_diag_ctr_Eth10GDiagPktLenMode_Shift                                                      4
#define cAf6_eth10g_diag_ctr_Eth10GDiagPktLenMode_MaxVal                                                   0x3
#define cAf6_eth10g_diag_ctr_Eth10GDiagPktLenMode_MinVal                                                   0x0
#define cAf6_eth10g_diag_ctr_Eth10GDiagPktLenMode_RstVal                                                   0x0

/*--------------------------------------
BitField Name: Eth10GDiagMonSrc
BitField Type: RO
BitField Desc: Dignostic select source to monitor 1: TX user 0: RX MAC
BitField Bits: [1]
--------------------------------------*/
#define cAf6_eth10g_diag_ctr_Eth10GDiagMonSrc_Bit_Start                                                      1
#define cAf6_eth10g_diag_ctr_Eth10GDiagMonSrc_Bit_End                                                        1
#define cAf6_eth10g_diag_ctr_Eth10GDiagMonSrc_Mask                                                       cBit1
#define cAf6_eth10g_diag_ctr_Eth10GDiagMonSrc_Shift                                                          1
#define cAf6_eth10g_diag_ctr_Eth10GDiagMonSrc_MaxVal                                                       0x1
#define cAf6_eth10g_diag_ctr_Eth10GDiagMonSrc_MinVal                                                       0x0
#define cAf6_eth10g_diag_ctr_Eth10GDiagMonSrc_RstVal                                                       0x0

/*--------------------------------------
BitField Name: Eth10GDiagGenEn
BitField Type: RO
BitField Desc: Dignostic enable to generate packets 1: enable 0: disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenEn_Bit_Start                                                       0
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenEn_Bit_End                                                         0
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenEn_Mask                                                        cBit0
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenEn_Shift                                                           0
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenEn_MaxVal                                                        0x1
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenEn_MinVal                                                        0x0
#define cAf6_eth10g_diag_ctr_Eth10GDiagGenEn_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G Dignostic Max Length Control
Reg Addr   : 0x61
Reg Formula:
    Where  :
Reg Desc   :
This register is used to config max packet length in case of increased length dignostic packet

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_diag_max_len_ctr_Base                                                              0x1061
#define cAf6Reg_eth10g_diag_max_len_ctr                                                                   0x1061
#define cAf6Reg_eth10g_diag_max_len_ctr_WidthVal                                                            32
#define cAf6Reg_eth10g_diag_max_len_ctr_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: Eth10GDiagMaxLen
BitField Type: RO
BitField Desc: Dignostic max length of packets
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth10g_diag_max_len_ctr_Eth10GDiagMaxLen_Bit_Start                                              0
#define cAf6_eth10g_diag_max_len_ctr_Eth10GDiagMaxLen_Bit_End                                               15
#define cAf6_eth10g_diag_max_len_ctr_Eth10GDiagMaxLen_Mask                                            cBit15_0
#define cAf6_eth10g_diag_max_len_ctr_Eth10GDiagMaxLen_Shift                                                  0
#define cAf6_eth10g_diag_max_len_ctr_Eth10GDiagMaxLen_MaxVal                                            0xffff
#define cAf6_eth10g_diag_max_len_ctr_Eth10GDiagMaxLen_MinVal                                               0x0
#define cAf6_eth10g_diag_max_len_ctr_Eth10GDiagMaxLen_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G Dignostic Min Length Control
Reg Addr   : 0x62
Reg Formula:
    Where  :
Reg Desc   :
This register is used to config min packet length in case of increased length dignostic packet or length packet in fix length mode

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_diag_min_len_ctr_Base                                                              0x1062
#define cAf6Reg_eth10g_diag_min_len_ctr                                                                   0x62
#define cAf6Reg_eth10g_diag_min_len_ctr_WidthVal                                                            32
#define cAf6Reg_eth10g_diag_min_len_ctr_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: Eth10GDiagMinLen
BitField Type: RO
BitField Desc: Dignostic min length of packets
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth10g_diag_min_len_ctr_Eth10GDiagMinLen_Bit_Start                                              0
#define cAf6_eth10g_diag_min_len_ctr_Eth10GDiagMinLen_Bit_End                                               15
#define cAf6_eth10g_diag_min_len_ctr_Eth10GDiagMinLen_Mask                                            cBit15_0
#define cAf6_eth10g_diag_min_len_ctr_Eth10GDiagMinLen_Shift                                                  0
#define cAf6_eth10g_diag_min_len_ctr_Eth10GDiagMinLen_MaxVal                                            0xffff
#define cAf6_eth10g_diag_min_len_ctr_Eth10GDiagMinLen_MinVal                                               0x0
#define cAf6_eth10g_diag_min_len_ctr_Eth10GDiagMinLen_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G Dignostic number packet Control
Reg Addr   : 0x64
Reg Formula:
    Where  :
Reg Desc   :
This register is used to config number of packet generated

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_diag_num_pkt_ctr_Base                                                              0x1064
#define cAf6Reg_eth10g_diag_num_pkt_ctr                                                                   0x64
#define cAf6Reg_eth10g_diag_num_pkt_ctr_WidthVal                                                            32
#define cAf6Reg_eth10g_diag_num_pkt_ctr_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: Eth10GDiagNumPkt
BitField Type: RO
BitField Desc: Dignostic number of generated packet, value of 16'hFFFF for
continuous generating
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_eth10g_diag_num_pkt_ctr_Eth10GDiagNumPkt_Bit_Start                                              0
#define cAf6_eth10g_diag_num_pkt_ctr_Eth10GDiagNumPkt_Bit_End                                               15
#define cAf6_eth10g_diag_num_pkt_ctr_Eth10GDiagNumPkt_Mask                                            cBit15_0
#define cAf6_eth10g_diag_num_pkt_ctr_Eth10GDiagNumPkt_Shift                                                  0
#define cAf6_eth10g_diag_num_pkt_ctr_Eth10GDiagNumPkt_MaxVal                                            0xffff
#define cAf6_eth10g_diag_num_pkt_ctr_Eth10GDiagNumPkt_MinVal                                               0x0
#define cAf6_eth10g_diag_num_pkt_ctr_Eth10GDiagNumPkt_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G Dignostic Error Sticky
Reg Addr   : 0x48
Reg Formula:
    Where  :
Reg Desc   :
This register is used to sticky error for dignostic

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_diag_err_stk_Base                                                                  0x1048
#define cAf6Reg_eth10g_diag_err_stk                                                                       0x48
#define cAf6Reg_eth10g_diag_err_stk_WidthVal                                                                32
#define cAf6Reg_eth10g_diag_err_stk_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: Eth10GDiagErrStk
BitField Type: RO
BitField Desc: Dignostic error sticky
BitField Bits: [10]
--------------------------------------*/
#define cAf6_eth10g_diag_err_stk_Eth10GDiagErrStk_Bit_Start                                                  10
#define cAf6_eth10g_diag_err_stk_Eth10GDiagErrStk_Bit_End                                                    10
#define cAf6_eth10g_diag_err_stk_Eth10GDiagErrStk_Mask                                                   cBit10
#define cAf6_eth10g_diag_err_stk_Eth10GDiagErrStk_Shift                                                      10
#define cAf6_eth10g_diag_err_stk_Eth10GDiagErrStk_MaxVal                                                   0x1
#define cAf6_eth10g_diag_err_stk_Eth10GDiagErrStk_MinVal                                                   0x0
#define cAf6_eth10g_diag_err_stk_Eth10GDiagErrStk_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Dignostic Receive Ethernet port Count Total packet
Reg Addr   : 0x00004024(RO)
Reg Formula: 0x00004024 + eth_port
    Where  :
           + $eth_port(0-0): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the total packet at receive dignostic

------------------------------------------------------------------------------*/
#define cAf6Reg_diag_rx_pkt_cnt_ro_Base                                                             0x00004024
#define cAf6Reg_diag_rx_pkt_cnt_ro(ethport)                                             (0x00004024+(ethport))
#define cAf6Reg_diag_rx_pkt_cnt_ro_WidthVal                                                                 32
#define cAf6Reg_diag_rx_pkt_cnt_ro_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: DiagRxPktCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total rx packet for diagnotic
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_diag_rx_pkt_cnt_ro_DiagRxPktCntTotal_Bit_Start                                                  0
#define cAf6_diag_rx_pkt_cnt_ro_DiagRxPktCntTotal_Bit_End                                                   31
#define cAf6_diag_rx_pkt_cnt_ro_DiagRxPktCntTotal_Mask                                                cBit31_0
#define cAf6_diag_rx_pkt_cnt_ro_DiagRxPktCntTotal_Shift                                                      0
#define cAf6_diag_rx_pkt_cnt_ro_DiagRxPktCntTotal_MaxVal                                            0xffffffff
#define cAf6_diag_rx_pkt_cnt_ro_DiagRxPktCntTotal_MinVal                                                   0x0
#define cAf6_diag_rx_pkt_cnt_ro_DiagRxPktCntTotal_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Dignostic Receive Ethernet port Count Total packet
Reg Addr   : 0x00004824(RC)
Reg Formula: 0x00004824 + eth_port
    Where  :
           + $eth_port(0-0): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the total packet at receive dignostic

------------------------------------------------------------------------------*/
#define cAf6Reg_diag_rx_pkt_cnt_rc_Base                                                             0x00004824
#define cAf6Reg_diag_rx_pkt_cnt_rc(ethport)                                             (0x00004824+(ethport))
#define cAf6Reg_diag_rx_pkt_cnt_rc_WidthVal                                                                 32
#define cAf6Reg_diag_rx_pkt_cnt_rc_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: DiagRxPktCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total rx packet for diagnotic
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_diag_rx_pkt_cnt_rc_DiagRxPktCntTotal_Bit_Start                                                  0
#define cAf6_diag_rx_pkt_cnt_rc_DiagRxPktCntTotal_Bit_End                                                   31
#define cAf6_diag_rx_pkt_cnt_rc_DiagRxPktCntTotal_Mask                                                cBit31_0
#define cAf6_diag_rx_pkt_cnt_rc_DiagRxPktCntTotal_Shift                                                      0
#define cAf6_diag_rx_pkt_cnt_rc_DiagRxPktCntTotal_MaxVal                                            0xffffffff
#define cAf6_diag_rx_pkt_cnt_rc_DiagRxPktCntTotal_MinVal                                                   0x0
#define cAf6_diag_rx_pkt_cnt_rc_DiagRxPktCntTotal_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : Dignotic Receive Ethernet port Count number of bytes of packet
Reg Addr   : 0x0000403C(RO)
Reg Formula: 0x0000403C + eth_port
    Where  :
           + $eth_port(0-0): Receive Ethernet port ID
Reg Desc   :
This register is statistic count number of bytes of rx packet for dignostic

------------------------------------------------------------------------------*/
#define cAf6Reg_diag_rx_cnt_byte_ro_Base                                                            0x0000403C
#define cAf6Reg_diag_rx_cnt_byte_ro(ethport)                                            (0x0000403C+(ethport))
#define cAf6Reg_diag_rx_cnt_byte_ro_WidthVal                                                                32
#define cAf6Reg_diag_rx_cnt_byte_ro_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: DiagRxRxCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of rx packet for
diagnostic
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_diag_rx_cnt_byte_ro_DiagRxRxCntByte_Bit_Start                                                   0
#define cAf6_diag_rx_cnt_byte_ro_DiagRxRxCntByte_Bit_End                                                    31
#define cAf6_diag_rx_cnt_byte_ro_DiagRxRxCntByte_Mask                                                 cBit31_0
#define cAf6_diag_rx_cnt_byte_ro_DiagRxRxCntByte_Shift                                                       0
#define cAf6_diag_rx_cnt_byte_ro_DiagRxRxCntByte_MaxVal                                             0xffffffff
#define cAf6_diag_rx_cnt_byte_ro_DiagRxRxCntByte_MinVal                                                    0x0
#define cAf6_diag_rx_cnt_byte_ro_DiagRxRxCntByte_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Dignotic Receive Ethernet port Count number of bytes of packet
Reg Addr   : 0x0000483C(RC)
Reg Formula: 0x0000483C + eth_port
    Where  :
           + $eth_port(0-0): Receive Ethernet port ID
Reg Desc   :
This register is statistic count number of bytes of rx packet for dignostic

------------------------------------------------------------------------------*/
#define cAf6Reg_diag_rx_cnt_byte_rc_Base                                                            0x0000483C
#define cAf6Reg_diag_rx_cnt_byte_rc(ethport)                                            (0x0000483C+(ethport))
#define cAf6Reg_diag_rx_cnt_byte_rc_WidthVal                                                                32
#define cAf6Reg_diag_rx_cnt_byte_rc_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: DiagRxRxCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of rx packet for
diagnostic
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_diag_rx_cnt_byte_rc_DiagRxRxCntByte_Bit_Start                                                   0
#define cAf6_diag_rx_cnt_byte_rc_DiagRxRxCntByte_Bit_End                                                    31
#define cAf6_diag_rx_cnt_byte_rc_DiagRxRxCntByte_Mask                                                 cBit31_0
#define cAf6_diag_rx_cnt_byte_rc_DiagRxRxCntByte_Shift                                                       0
#define cAf6_diag_rx_cnt_byte_rc_DiagRxRxCntByte_MaxVal                                             0xffffffff
#define cAf6_diag_rx_cnt_byte_rc_DiagRxRxCntByte_MinVal                                                    0x0
#define cAf6_diag_rx_cnt_byte_rc_DiagRxRxCntByte_RstVal                                                    0x0

/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count FCS error packet
Reg Addr   : 0x00003038(RO)
Reg Formula: 0x00003038 + eth_port
    Where  :
           + $eth_port(0-0): Receive Ethernet port ID
Reg Desc   :
This register is statistic count FCS error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_RxEth_cnt_fcs_err_ro_Base                                                           0x00003038
#define cAf6Reg_RxEth_cnt_fcs_err_ro(ethport)                                           (0x00003038+(ethport))
#define cAf6Reg_RxEth_cnt_fcs_err_ro_WidthVal                                                               32
#define cAf6Reg_RxEth_cnt_fcs_err_ro_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: RxEthCntPhyErr
BitField Type: RO
BitField Desc: This is statistic counter FCS error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_RxEth_cnt_phy_err_ro_RxEthCntPhyErr_Bit_Start                                                   0
#define cAf6_RxEth_cnt_phy_err_ro_RxEthCntPhyErr_Bit_End                                                    31
#define cAf6_RxEth_cnt_phy_err_ro_RxEthCntPhyErr_Mask                                                 cBit31_0
#define cAf6_RxEth_cnt_phy_err_ro_RxEthCntPhyErr_Shift                                                       0
#define cAf6_RxEth_cnt_phy_err_ro_RxEthCntPhyErr_MaxVal                                             0xffffffff
#define cAf6_RxEth_cnt_phy_err_ro_RxEthCntPhyErr_MinVal                                                    0x0
#define cAf6_RxEth_cnt_phy_err_ro_RxEthCntPhyErr_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count FCS error packet
Reg Addr   : 0x00003838(RC)
Reg Formula: 0x00003838 + eth_port
    Where  :
           + $eth_port(0-0): Receive Ethernet port ID
Reg Desc   :
This register is statistic count FCS error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_RxEth_cnt_phy_err_rc_Base                                                           0x00003838
#define cAf6Reg_RxEth_cnt_phy_err_rc(ethport)                                           (0x00003838+(ethport))
#define cAf6Reg_RxEth_cnt_phy_err_rc_WidthVal                                                               32
#define cAf6Reg_RxEth_cnt_phy_err_rc_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: RxEthCntPhyErr
BitField Type: RO
BitField Desc: This is statistic counter FCS error packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_RxEth_cnt_phy_err_rc_RxEthCntPhyErr_Bit_Start                                                   0
#define cAf6_RxEth_cnt_phy_err_rc_RxEthCntPhyErr_Bit_End                                                    31
#define cAf6_RxEth_cnt_phy_err_rc_RxEthCntPhyErr_Mask                                                 cBit31_0
#define cAf6_RxEth_cnt_phy_err_rc_RxEthCntPhyErr_Shift                                                       0
#define cAf6_RxEth_cnt_phy_err_rc_RxEthCntPhyErr_MaxVal                                             0xffffffff
#define cAf6_RxEth_cnt_phy_err_rc_RxEthCntPhyErr_MinVal                                                    0x0
#define cAf6_RxEth_cnt_phy_err_rc_RxEthCntPhyErr_RstVal                                                    0x0

/*------------------------------------------------------------------------------
Reg Name   : Dignotic genereate bandwidth control
Reg Addr   : 0x1002
Reg Formula: 0x1002 + eth_port
    Where  :
           + $eth_port(0-0): Ethernet port ID
Reg Desc   :
This register is used to create bandwidth for generating diag packets
Note:
    cfg EXAUI#0 : 0xA81002
    cfg EXAUI#1 : 0xA91002
------------------------------------------------------------------------------*/
#define cAf6Reg_diag_gen_bw_ctrl_Base                                                                   0x1002
#define cAf6Reg_diag_gen_bw_ctrl(ethport)                                                 (0x1002UL+(ethport))
#define cAf6Reg_diag_gen_bw_ctrl_WidthVal                                                                   32
#define cAf6Reg_diag_gen_bw_ctrl_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: DiagGenBwCtrl
BitField Type: RO
BitField Desc: bandwidth for generating diag packet, unit is 1.2G
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_diag_gen_bw_ctrl_DiagGenBwCtrl_Bit_Start                                                        0
#define cAf6_diag_gen_bw_ctrl_DiagGenBwCtrl_Bit_End                                                          2
#define cAf6_diag_gen_bw_ctrl_DiagGenBwCtrl_Mask                                                       cBit2_0
#define cAf6_diag_gen_bw_ctrl_DiagGenBwCtrl_Shift                                                            0
#define cAf6_diag_gen_bw_ctrl_DiagGenBwCtrl_MaxVal                                                         0x7
#define cAf6_diag_gen_bw_ctrl_DiagGenBwCtrl_MinVal                                                         0x0
#define cAf6_diag_gen_bw_ctrl_DiagGenBwCtrl_RstVal                                                         0x0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012EXAUIREG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210012ModuleEthInternal.h
 * 
 * Created Date: Sep 22, 2015
 *
 * Description : ETH module of Tha60210012 product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEETHINTERNAL_H_
#define _THA60210012MODULEETHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/util/ThaBitMask.h"
#include "../../Tha60210051/eth/Tha60210051ModuleEthInternal.h"
#include "../man/Tha60210012Device.h"
#include "Tha60210012ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleEthMethods
    {
    eAtRet (*SerdesDefaultSetup)(Tha60210012ModuleEth self);
    void (*EXauInit)(Tha60210012ModuleEth self);
    eAtRet (*AllPtchServiceCreate)(Tha60210012ModuleEth self);
    eAtRet (*AllPtchServiceDelete)(Tha60210012ModuleEth self);
    uint32 (*StartVersionSupportNewMaxMinPacketSize)(Tha60210012ModuleEth self);
    uint32 (*StartVersionSupportNewPMCCounterOffset)(Tha60210012ModuleEth self);
    eAtRet (*PtchLookupEnable)(Tha60210012ModuleEth self, eBool enabled);
    tAtAsyncOperationFunc (*AsyncOperationGet)(Tha60210012ModuleEth self, uint32 state);
    uint32 (*NumAsyncOperation)(Tha60210012ModuleEth self);
    eBool  (*ExternalPmcCounterIsSupported)(Tha60210012ModuleEth self);
    }tTha60210012ModuleEthMethods;

typedef struct tTha60210012ModuleEth
    {
    tTha60210051ModuleEth super;
    const tTha60210012ModuleEthMethods * methods;

    /* PTCH */
    AtPtchService cemPtch;
    AtPtchService imsgEoPPtch;
    AtPtchService exaui0Ptch;
    AtPtchService exaui1Ptch;

    /* HW only lookup base on VLAN ID, so only VLAN ID is configured to HW
     * Need to cache whole VLAN information to retrieve when needed
     */
    tAtVlan controlFlowIgVlan;

    /* async init */
    uint32 asyncInitState;
    ThaBitMask vlanMask;
    }tTha60210012ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha60210012ModuleEthObjectInit(AtModuleEth self, AtDevice device);
ThaEthFlowControllerFactory Tha60210012EthFlowControllerFactory(void);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEETHINTERNAL_H_ */


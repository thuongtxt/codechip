/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210012ModuleEth.c
 *
 * Created Date: Sep 9, 2015
 *
 * Description : ETH Module of product 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPtchService.h"
#include "AtMpBundle.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/eth/ThaEthFlow.h"
#include "../../../default/cla/ThaModuleCla.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../physical/Tha60210012PtchService.h"
#include "../cla/Tha60210012ModuleClaReg.h"
#include "../cla/Tha60210012ModuleCla.h"
#include "../cla/controller/Tha60210012ClaPwEthPortController.h"
#include "../man/Tha60210012Device.h"
#include "../pwe/Tha60210012ModulePwe.h"
#include "../pwe/Tha60210012ModulePweReg.h"
#include "Tha60210012ModuleEthInternal.h"
#include "Tha60210012EXauiReg.h"
#include "Tha60210012EXaui.h"
#include "Tha60210012EthFlow.h"
#include "Tha60210012EthPortReg.h"
#include "Tha60210012ModuleEthFlowLookupReg.h"
#include "Tha60210012ModuleEthPmcInternalReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cTha60210012ModuleEthNumOfXauiSupport       2
#define cTha60210012ModulePdaBaseAddress            0x500000
#define cTha60210012MaxNumOffsetControlHdr          6
#define cThaNumBitInDword                           32

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)  ((Tha60210012ModuleEth)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012ModuleEthBitFieldInfo
    {
    const char *name;
    uint32 mask;
    uint32 shift;
    }tTha60210012ModuleEthBitFieldInfo;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012ModuleEthMethods m_methods;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tAtModuleEthMethods          m_AtModuleEthOverride;
static tThaModuleEthMethods         m_ThaModuleEthOverride;
static tTha60210051ModuleEthMethods m_Tha60210051ModuleEthOverride;

/* Save super implementation */
static const tAtModuleEthMethods  *m_AtModuleEthMethods                  = NULL;
static const tAtObjectMethods     *m_AtObjectMethods                     = NULL;
static const tAtModuleMethods     *m_AtModuleMethods                     = NULL;
static const tTha60210051ModuleEthMethods *m_Tha60210051ModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool EXauiIsReady(AtModuleEth self)
    {
    return Tha60210012DeviceEXauiIsReady(AtModuleDeviceGet((AtModule)self));
    }

static eBool PtchServiceIsReady(AtModuleEth self)
    {
    return Tha60210012DevicePtchServiceIsReady(AtModuleDeviceGet((AtModule)self));
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
    if (EXauiIsReady(self))
        return 3;
    return 1;
    }

static uint8 EXaui0PortIdGet(AtModuleEth self)
    {
    if (EXauiIsReady(self))
        return 1;

    return cInvalidUint8;
    }

static uint8 EXaui1PortIdGet(AtModuleEth self)
    {
    if (EXauiIsReady(self))
        return 2;

    return cInvalidUint8;
    }

static uint16 MaxFlowsGet(AtModuleEth self)
    {
    AtUnused(self);
    return 4096;
    }

static uint32 MacBaseAddress(ThaModuleEth self, AtEthPort port)
    {
    uint32 portId = AtChannelIdGet((AtChannel)port);
    AtUnused(self);

    /* XFI */
    if (portId == 0)
        return (portId * 0x10000) + 0x21000;

    return (Tha60210012EthEXauiPortBaseAddress(port) + 0x1000);
    }

static uint32 SerdesIdToEthPortId(ThaModuleEth self, uint32 serdesId)
    {
    AtUnused(self);
    if (serdesId < 8)
    	return 0;
    return serdesId;
    }

static uint32 StartVersionSupportsSerdesAps(ThaModuleEth self)
    {
    AtUnused(self);
    return 0x0;
    }

static AtEthFlow FlowObjectCreate(AtModuleEth self, uint16 flowId)
    {
    return Tha60210012EthFlowNew(flowId, self);
    }

static ThaEthFlowControllerFactory FlowControllerFactory(ThaModuleEth self)
    {
    AtUnused(self);
    return Tha60210012EthFlowControllerFactory();
    }

static uint32 MaxNumMpFlows(ThaModuleEth self)
    {
    AtUnused(self);
    /* This product do not support it */
    return 0;
    }

static ThaEthFlowHeaderProvider FlowHeaderProviderCreate(ThaModuleEth self)
    {
    return ThaEthFlowHeaderProviderNew((AtModuleEth)self);
    }

static eAtRet AllPtchServiceCreate(Tha60210012ModuleEth self)
    {
    AtModuleEth moduleEth = (AtModuleEth)self;
    self->cemPtch     = Tha60210012CemPtchServiceNew(moduleEth);
    self->imsgEoPPtch = Tha60210012ImsgEopPtchServiceNew(moduleEth);
    self->exaui0Ptch  = Tha60210012EXauiPtchServiceNew(moduleEth, cAtPtchServiceTypeEXAUI_0);
    self->exaui1Ptch  = Tha60210012EXauiPtchServiceNew(moduleEth, cAtPtchServiceTypeEXAUI_1);
    return cAtOk;
    }

static void PtchServiceDelete(AtPtchService *ptch)
    {
    AtObjectDelete((AtObject)*ptch);
    *ptch = NULL;
    }

static eAtRet AllPtchServiceDelete(Tha60210012ModuleEth self)
    {
    PtchServiceDelete(&self->cemPtch);
    PtchServiceDelete(&self->imsgEoPPtch);
    PtchServiceDelete(&self->exaui0Ptch);
    PtchServiceDelete(&self->exaui1Ptch);

    return cAtOk;
    }

static eAtRet Setup(AtModule self)
    {
    AtModuleEth ethModule = (AtModuleEth)self;

    /* Super */
    eAtRet ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Create all PTCH services */
    if (PtchServiceIsReady(ethModule))
        {
        (void)mMethodsGet(mThis(self))->AllPtchServiceDelete(mThis(self));
        return mMethodsGet(mThis(self))->AllPtchServiceCreate(mThis(self));
        }

    return cAtOk;
    }

static void EXauiActive(Tha60210012ModuleEth self, eBool enable)
    {
    uint32 regVal, regAddr;

    if (!EXauiIsReady((AtModuleEth)self))
        return;

    /* Active/Deactive MUX and eXAUI#0 Buffer */
    regAddr = cAf6Reg_cfg0_pen_Base + Tha60210012ModuleEthEXaui1BaseAddress((AtModuleEth)self);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_cfg0_pen_Active_, enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);
    }

static void EXauInit(Tha60210012ModuleEth self)
    {
    if (!EXauiIsReady((AtModuleEth)self))
        return;

    /* Initialize default value (0x80) for channel ID (eXAUI --> XFI) */
    Tha60210012ModuleEthIngressChannelIdSet((AtModuleEth)self, 0x80);
    }

static AtSerdesController DefaultBridgeSerdesOfPort(AtModuleEth self, AtEthPort port)
    {
    return AtModuleEthSerdesController(self, AtChannelIdGet((AtChannel)port) + 2);
    }

static eAtRet SerdesDefaultSetup(Tha60210012ModuleEth self)
    {
    eAtRet ret = cAtOk;
    const uint8 cXfiPortId = 0;
    AtModuleEth moduleEth = (AtModuleEth)self;

    if (Tha60210012ModuleEthReduceTo4Serdes(moduleEth))
        {
        AtEthPort port = AtModuleEthPortGet(moduleEth, cXfiPortId);
        AtSerdesController serdes = AtEthPortSerdesController(port);

        ret |= AtEthPortTxSerdesSet(port, serdes);
        ret |= AtSerdesControllerPowerDown(serdes, cAtFalse);

        serdes = DefaultBridgeSerdesOfPort(moduleEth, port);
        ret |= AtEthPortTxSerdesBridge(port, serdes);
        ret |= AtSerdesControllerPowerDown(serdes, cAtFalse);
        }

    return ret;
    }

static uint16 PtchDefaultValueByType(eAtPtchServiceType type)
    {
    return (uint16)type;
    }

static ThaModulePwe ModulePwe(AtModule self)
    {
    return (ThaModulePwe)AtDeviceModuleGet(AtModuleDeviceGet(self), cThaModulePwe);
    }

static uint16 IngressPtchValueCorrect(uint16 value)
    {
    return (uint16)(value << 8);
    }

static eAtRet CemPtchDefaultSet(AtModule self, AtPtchService cemPtch)
    {
    uint16 value = PtchDefaultValueByType(cAtPtchServiceTypeCEM);
    eAtRet ret = AtPtchServiceIngressPtchSet(cemPtch, IngressPtchValueCorrect(value));
    ret |= AtPtchServiceEgressPtchSet(cemPtch, value);

    if (Tha60210012ModulePwePtchEnableOnModuleIsSupported(ModulePwe(self)))
        ret |= AtPtchServiceEnable(cemPtch, cAtTrue);

    return ret;
    }

static eAtRet Exaui1PtchDefaultSet(AtModule self, AtPtchService exaui1Ptch)
    {
    AtUnused(self);
    if (exaui1Ptch)
        {
        uint16 value = PtchDefaultValueByType(cAtPtchServiceTypeEXAUI_1);
        eAtRet ret = AtPtchServiceIngressPtchSet(exaui1Ptch, IngressPtchValueCorrect(value));
        ret |= AtPtchServiceEgressPtchSet(exaui1Ptch, value);
        /* Will be enabled when require */
        return ret;
        }

    return cAtOk;
    }

static eAtRet iMsgEopPtchDefaultSet(AtModule self, AtPtchService imsgEoPPtch)
    {
    uint16 value = PtchDefaultValueByType(cAtPtchServiceTypeIMSG_EoP);
    eAtRet ret = AtPtchServiceIngressPtchSet(imsgEoPPtch, IngressPtchValueCorrect(value));
    ret |= AtPtchServiceEgressPtchSet(imsgEoPPtch, value);

    if (Tha60210012ModulePwePtchEnableOnModuleIsSupported(ModulePwe(self)))
        ret |= AtPtchServiceEnable(imsgEoPPtch, cAtTrue);

    return ret;
    }

static eAtRet PtchDefaultSet(AtModule self)
    {
    eAtRet ret = cAtOk;

    ret |= CemPtchDefaultSet(self, mThis(self)->cemPtch);
    ret |= Exaui1PtchDefaultSet(self, mThis(self)->exaui1Ptch);
    ret |= iMsgEopPtchDefaultSet(self, mThis(self)->imsgEoPPtch);
    ret |= Tha60210012ModuleEthPtchLookupEnable((AtModuleEth)self, cAtTrue);

    return ret;
    }

static uint32 ControlHeaderBaseAddress(void)
    {
    return 0x70000;
    }

static void HeaderRead(AtModuleEth ethModule, uint32 *buffer)
    {
    uint32 regAddress;
    uint8 location_i;

    for (location_i = 0; location_i < cTha60210012MaxNumOffsetControlHdr; location_i++)
        {
        regAddress = cAf6Reg_TxEth_PSNOAM_Base + ControlHeaderBaseAddress() + location_i;
        buffer[location_i] = mModuleHwRead(ethModule, regAddress);
        }
    }

static eAtRet HeaderWrite(AtModuleEth ethModule, uint32 *buffer)
    {
    uint32 regAddress, regValue;
    uint8 location_i;

    for (location_i = 0; location_i < cTha60210012MaxNumOffsetControlHdr; location_i++)
        {
        regAddress = cAf6Reg_TxEth_PSNOAM_Base + ControlHeaderBaseAddress() + location_i;
        regValue = mModuleHwRead(ethModule, regAddress);
        mRegFieldSet(regValue, cAf6_TxEth_PSNOAM_TxEthPSNOAM_, buffer[location_i]);
        mModuleHwWrite(ethModule, regAddress, regValue);

        /* Make corrupt header identical with control header  */
        regAddress = cAf6Reg_TxEth_PSNOAMErr_Base + ControlHeaderBaseAddress() + location_i;
        regValue = mModuleHwRead(ethModule, regAddress);
        mRegFieldSet(regValue, cAf6_TxEth_PSNOAMErr_TxEthPSNOAMErr_, buffer[location_i]);
        mModuleHwWrite(ethModule, regAddress, regValue);
        }

    return cAtOk;
    }

static void DefaultCircuitVlanFields(AtModuleEth self)
    {
    uint32 buffer[cTha60210012MaxNumOffsetControlHdr];
    const uint8 cDefaultCircuitVlanPriority = 1;
    const uint8 cDefaultCircuitVlanCfi = 1;
    const uint16 cDefaultCircuitVlanTpid = 0x8100;

    /* Build VLAN for Control flow */
    HeaderRead(self, buffer);
    buffer[4]  = (uint32)((cDefaultCircuitVlanTpid & cBit15_0) << 16);
    buffer[4] |= (uint32)((cDefaultCircuitVlanPriority & cBit2_0) << 13);
    buffer[4] |= (uint32)((cDefaultCircuitVlanCfi & cBit0)        << 12);
    HeaderWrite(self, buffer);
    }

static void OamBufferReset(AtModule self)
    {
    uint32 regAddress;
    uint8 location_i;
    const uint16 cDefaultEthTypeForOam = 0x8857;

    /* Clear current header */
    for (location_i = 0; location_i < cTha60210012MaxNumOffsetControlHdr; location_i++)
        {
        regAddress = cAf6Reg_TxEth_PSNOAM_Base + ControlHeaderBaseAddress() + location_i;
        mModuleHwWrite(self, regAddress, 0);
        }

    /* Default ethernet type and some fields of circuit VLAN */
    Tha60210012ModuleEthOamFlowEgressEthTypeSet((AtModuleEth)self, cDefaultEthTypeForOam);
    DefaultCircuitVlanFields((AtModuleEth)self);
    }

static eAtRet EthSerdesDefaultSetup(AtModule self)
    {
    return mMethodsGet(mThis(self))->SerdesDefaultSetup(mThis(self));
    }

static eAtRet EthEXauiInit(AtModule self)
    {
    mMethodsGet(mThis(self))->EXauInit(mThis(self));
    return cAtOk;
    }

static eAtRet VlanMaskBitInit(AtModule self)
    {
    if (mThis(self)->vlanMask)
        ThaBitMaskInit(mThis(self)->vlanMask);
    return cAtOk;
    }

static eAtRet EthOamBufferReset(AtModule self)
    {
    OamBufferReset(self);
    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    /* Super */
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret = EthSerdesDefaultSetup(self);
    if (ret != cAtOk)
        return ret;

    /* Initialize for eXAUI */
    EthEXauiInit(self);
    VlanMaskBitInit(self);

    EthOamBufferReset(self);

    /* Default PTCH service */
    return PtchDefaultSet(self);
    }

static eAtRet SuperAsyncInit(AtModule self)
    {
    return m_AtModuleMethods->AsyncInit(self);
    }

static tAtAsyncOperationFunc* AllAsyncOperation(uint32 *numOps)
    {
    static tAtAsyncOperationFunc functions[] = {(tAtAsyncOperationFunc)SuperAsyncInit,
                                                (tAtAsyncOperationFunc)EthSerdesDefaultSetup,
                                                (tAtAsyncOperationFunc)EthEXauiInit,
                                                (tAtAsyncOperationFunc)VlanMaskBitInit,
                                                (tAtAsyncOperationFunc)EthOamBufferReset,
                                                (tAtAsyncOperationFunc)PtchDefaultSet};
    *numOps = mCount(functions);
    return functions;
    }

static uint32 NumAsyncOperation(Tha60210012ModuleEth self)
    {
    uint32 numOp;
    AtUnused(self);

    AllAsyncOperation(&numOp);
    return numOp;
    }

static tAtAsyncOperationFunc AsyncOperationGet(Tha60210012ModuleEth self, uint32 state)
    {
    uint32 numOp;
    tAtAsyncOperationFunc* allOp = AllAsyncOperation(&numOp);
    AtUnused(self);

    if (state >= numOp)
        return NULL;

    return allOp[state];
    }

static eAtRet AsyncInit(AtModule self)
    {
    uint32 state = mThis(self)->asyncInitState;
    eAtRet ret = cAtOk;
    tAtOsalCurTime profileTime;
    tAtAsyncOperationFunc func;
    char stateString[32];

    func = mMethodsGet(mThis(self))->AsyncOperationGet(mThis(self), state);
    if (func == NULL)
        {
        mThis(self)->asyncInitState = 0;
        return cAtOk;
        }

    AtSprintf(stateString, "Async state: %d", state);
    AtOsalCurTimeGet(&profileTime);
    ret = func((AtObject)self);

    AtDeviceAccessTimeInMsSinceLastProfileCheck(AtModuleDeviceGet(self), cAtTrue, &profileTime, AtSourceLocation, stateString);
    AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet(self), cAtTrue, AtSourceLocation, stateString);
    if (AtDeviceAsyncRetValIsInState(ret))
        return ret;

    if (ret != cAtOk)
        {
        mThis(self)->asyncInitState = 0;
        return ret;
        }

    mThis(self)->asyncInitState++;
    return cAtErrorAgain;
    }

static void DebugStickyPrint(const char* str, eBool sticky)
    {
    AtPrintc(cSevNormal, "%-50s: ", str);
    AtPrintc((sticky) ? cSevCritical : cSevInfo, "%s\r\n", (sticky) ? "RAISE" : "CLEAR");
    }

static void DebugSticky(AtModule self, uint32 address, tTha60210012ModuleEthBitFieldInfo* bitInfo, uint8 numOfStk)
    {
    uint32 regAddr = Tha60210012ModuleEthEXaui1BaseAddress((AtModuleEth)self) + address;
    uint32 regVal = mModuleHwRead(self, regAddr);
    eBool sticky;
    uint8 i;

    if(bitInfo == NULL)
        return;

    AtPrintc(cSevNormal, "0x%08X: 0x%08x\r\n\r\n", regAddr, regVal);

    for (i = 0; i < numOfStk; i++)
        {
        mFieldGet(regVal, bitInfo[i].mask, bitInfo[i].shift, eBool, &sticky);
        DebugStickyPrint(bitInfo[i].name, sticky);
        }

    AtPrintc(cSevNormal, "----------------------------------------\r\n");

    /* Clear sticky */
    mModuleHwWrite(self, regAddr, regVal);
    }

static void ShowStick0(AtModule self)
    {
    tTha60210012ModuleEthBitFieldInfo stickies[] = {
                         {"Input_From_PW_path"                              , cBit31 , 31},
                         {"Input_From_eXAUI0_path"                          , cBit30 , 30},
                         {"Input_From_iMSG_EOP_path"                        , cBit29 , 29},
                         {"Input_From_eXAUI1_path"                          , cBit28 , 28},
                         {"Output_Request_To_PW_path"                       , cBit27 , 27},
                         {"Output_Request_To_eXAUI0_path"                   , cBit26 , 26},
                         {"Output_Request_To_iMSG_EOP_path"                 , cBit25 , 25},
                         {"Output_Request_To_eXAUI1_path"                   , cBit24 , 24},
                         {"Output_MUX_Source0_for_PW_En"                    , cBit23 , 23},
                         {"Output_MUX_Source1_for_eXAUI0_En"                , cBit22 , 22},
                         {"Output_MUX_Source2_for_iMSG_EOP_En"              , cBit21 , 21},
                         {"Output_MUX_Source3_for_eXAUI1_En"                , cBit20 , 20},
                         {"TX_MAC_Request_to_MUX"                           , cBit19 , 19},
                         {"MUX_Output_Valid_to_tx_MAC"                      , cBit18 , 18},
                         {"MUX_Request_to_all_Source"                       , cBit17 , 17},
                         {"MUX_Output_Valid_to_PTCH_Engine"                 , cBit16 , 16},
                         {"Input_Valid_To_MUX_Source0_for_PW_En"            , cBit15 , 15},
                         {"Input_Valid_To_MUX_Source1_for_eXAUI0_En"        , cBit14 , 14},
                         {"Input_Valid_To_MUX_Source2_for_iMSG_EOP_En"      , cBit13 , 13},
                         {"Input_Valid_To_MUX_Source3_for_eXAUI1_En"        , cBit12 , 12},
                         {"Output_Request_To_FIFO_Source0_for_PW_En"        , cBit11 , 11},
                         {"Output_Request_To_FIFO_Source1_for_eXAUI0_En"    , cBit10 , 10},
                         {"Output_Request_To_FIFO_Source2_for_iMSG_EOP_En"  , cBit9  , 9 },
                         {"Output_Request_To_FIFO_Source3_for_eXAUI1_En"    , cBit8  , 8 },
                         {"FiFO_Source3_Write_Err"                          , cBit7  , 7 },
                         {"FiFO_Source2_Write_Err"                          , cBit6  , 6 },
                         {"FiFO_Source1_Write_Err"                          , cBit5  , 5 },
                         {"FiFO_Source0_Write_Err"                          , cBit4  , 4 },
                         {"FiFO_Source3_Read_Err"                           , cBit3  , 3 },
                         {"FiFO_Source2_Read_Err"                           , cBit2  , 2 },
                         {"FiFO_Source1_Read_Err"                           , cBit1  , 1 },
                         {"FiFO_Source0_Read_Err"                           , cBit0  , 0 }
    };

    DebugSticky(self, cAf6Reg_stk0_pen_Base, stickies, mCount(stickies));
    }

static void ShowStick1(AtModule self)
    {
    tTha60210012ModuleEthBitFieldInfo stickies[] = {
                         {"MUX_Engine_State_SOP"           , cBit31 , 31},
                         {"MUX_Engine_State_EOP"           , cBit30 , 30},
                         {"MUX_Engine_State_ERR"           , cBit29 , 29},
                         {"MUX_Engine_State_IDLE"          , cBit28 , 28},
                         {"MUX_Engine_Source3_State_IDLE"  , cBit27 , 27},
                         {"MUX_Engine_Source2_State_IDLE"  , cBit26 , 26},
                         {"MUX_Engine_Source1_State_IDLE"  , cBit25 , 25},
                         {"MUX_Engine_Source0_State_IDLE"  , cBit24 , 24},
                         {"MUX_Engine_Source3_State_EOP"   , cBit23 , 23},
                         {"MUX_Engine_Source2_State_EOP"   , cBit22 , 22},
                         {"MUX_Engine_Source1_State_EOP"   , cBit21 , 21},
                         {"MUX_Engine_Source0_State_EOP"   , cBit20 , 20},
                         {"MUX_Engine_Source3_State_SOP"   , cBit19 , 19},
                         {"MUX_Engine_Source2_State_SOP"   , cBit18 , 18},
                         {"MUX_Engine_Source1_State_SOP"   , cBit17 , 17},
                         {"MUX_Engine_Source0_State_SOP"   , cBit16 , 16},
                         {"MUX_Engine_Source3_Error_IDLE"  , cBit15 , 15},
                         {"MUX_Engine_Source2_Error_IDLE"  , cBit14 , 14},
                         {"MUX_Engine_Source1_Error_IDLE"  , cBit13 , 13},
                         {"MUX_Engine_Source0_Error_IDLE"  , cBit12 , 12},
                         {"MUX_Engine_Source3_Error_EOP"   , cBit11 , 11},
                         {"MUX_Engine_Source2_Error_EOP"   , cBit10 , 10},
                         {"MUX_Engine_Source1_Error_EOP"   , cBit9  , 9 },
                         {"MUX_Engine_Source0_Error_EOP"   , cBit8  , 8 },
                         {"MUX_Engine_Source3_Error_SOP"   , cBit7  , 7 },
                         {"MUX_Engine_Source2_Error_SOP"   , cBit6  , 6 },
                         {"MUX_Engine_Source1_Error_SOP"   , cBit5  , 5 },
                         {"MUX_Engine_Source0_Error_SOP"   , cBit4  , 4 },
                         {"MUX_Engine_Conflict_0"          , cBit1  , 1 },
                         {"MUX_Engine_Conflict_1"          , cBit0  , 0 },
    };

    DebugSticky(self, cAf6Reg_stk1_pen_Base, stickies, mCount(stickies));
    }

static void ShowStick2(AtModule self)
    {
    tTha60210012ModuleEthBitFieldInfo stickies[] = {
                         {"eXAUI0_Buffer_Full"                      , cBit7  , 7 },
                         {"Input_eXAUI0_Err"                        , cBit6  , 6 },
                         {"eXAUI0_Detect_over_MTU"                  , cBit5  , 5 },
                         {"eXAUI0_Buffer_Full_State"                , cBit4  , 4 },
                         {"FF_Packet_Descriptor_eXAUI0_Write_Error" , cBit1  , 1 },
                         {"FF_Packet_Descriptor_eXAUI0_Read_Error"  , cBit0  , 0 },
    };
    DebugSticky(self, cAf6Reg_stk2_pen_Base, stickies, mCount(stickies));
    }

static eAtRet Debug(AtModule self)
    {
    eBool enabled;

    /* Super */
    eAtRet ret = m_AtModuleMethods->Debug(self);
    if (ret != cAtOk)
        return ret;

    enabled = Tha60210012ModuleEthPtchLookupIsEnabled((AtModuleEth)self);
    AtPrintc(cSevNormal, "-------------------------------\r\n");
    AtPrintc(cSevNormal, "PTCH Lookup: ");
    AtPrintc((enabled)?cSevInfo:cSevCritical, "%s\r\n", (enabled)? "Enable": "Disable");
    AtPrintc(cSevNormal, "-------------------------------\r\n");

    AtPrintc(cSevDebug, "\r\n EXAUI DEBUG information: \r\n");
    AtPrintc(cSevNormal, "=========================================================================== \r\n");
    AtPrintc(cSevNormal, "  + Sticky 0 - "); ShowStick0(self);
    AtPrintc(cSevNormal, "  + Sticky 1 - "); ShowStick1(self);
    AtPrintc(cSevNormal, "  + Sticky 2 - "); ShowStick2(self);
    AtPrintc(cSevNormal, "=========================================================================== \r\n");

    return cAtOk;
    }

static uint32 NumExauiGet(AtModuleEth self)
    {
    return EXauiIsReady(self) ? 2 : 0;
    }

static AtEthPort ExauiGet(AtModuleEth self, uint32 exauiId)
    {
    uint32 portId;

    if (!EXauiIsReady(self))
        return NULL;

    portId = exauiId + 1; /* The first port is used for 10G MAC */
    return AtModuleEthPortGet(self, (uint8)portId);
    }

static AtPtchService PtchServiceGet(AtModuleEth self, eAtPtchServiceType serviceType)
    {
    if (!PtchServiceIsReady(self))
        return NULL;

    switch (serviceType)
        {
        case cAtPtchServiceTypeCEM      : return mThis(self)->cemPtch;
        case cAtPtchServiceTypeIMSG_EoP : return mThis(self)->imsgEoPPtch;
        case cAtPtchServiceTypeEXAUI_0  : return mThis(self)->exaui0Ptch;
        case cAtPtchServiceTypeEXAUI_1  : return mThis(self)->exaui1Ptch;

        case cAtPtchServiceTypeUnknown:
        case cAtPtchServiceTypeGeBypass:
        default: return NULL;
        }
    }

static void Delete(AtObject self)
    {
    mMethodsGet(mThis(self))->AllPtchServiceDelete(mThis(self));
    AtObjectDelete((AtObject)mThis(self)->vlanMask);
    mThis(self)->vlanMask = NULL;

    m_AtObjectMethods->Delete(self);
    }

static void SerializeVlan(AtObject self, AtCoder encoder)
    {
    tAtVlan *object = (tAtVlan *)self;
    mEncodeUInt(tpid);
    mEncodeUInt(priority);
    mEncodeUInt(cfi);
    mEncodeUInt(vlanId);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210012ModuleEth object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(cemPtch);
    mEncodeObject(imsgEoPPtch);
    mEncodeObject(exaui0Ptch);
    mEncodeObject(exaui1Ptch);
    AtCoderEncodeObjectWithHandler(encoder, (AtObject)(AtObjectAny)&(object->controlFlowIgVlan), "controlFlowIgVlan", SerializeVlan);
    mEncodeUInt(asyncInitState);
    mEncodeObject(vlanMask);
    }

static ThaEthSerdesManager SerdesManagerCreate(ThaModuleEth self)
    {
    return Tha60210012EthSerdesManagerNew((AtModuleEth)self);
    }

static eAtRet AllFlowsServiceRemove(ThaModuleEth self)
    {
    eAtRet ret = cAtOk;
    uint16 flow_i;
    uint16 numFlows = AtModuleEthMaxFlowsGet((AtModuleEth)self);
    for (flow_i = 0; flow_i < numFlows; flow_i++)
        {
        AtMpBundle bundle;
        AtHdlcLink hdlcLink;
        AtEncapChannel encapChannel;
        AtEthFlow flow = AtModuleEthFlowGet((AtModuleEth) self, flow_i);

        if (flow == NULL)
            continue;

        /* Unbind Encap service */
        encapChannel = AtEthFlowBoundEncapChannelGet(flow);
        if (encapChannel)
            ret |= AtEncapChannelFlowBind(encapChannel, NULL);

        /* Unbind HDLC service */
        hdlcLink = AtEthFlowHdlcLinkGet(flow);
        if (hdlcLink)
            ret |= AtHdlcLinkFlowBind(hdlcLink, NULL);

        /* Unbind bundle MLPP service */
        bundle = AtEthFlowMpBundleGet(flow);
        if (bundle)
            ret |= AtMpBundleFlowRemove(bundle, flow);
        }
        
	return ret;
    }

static uint32 EthPortIdToSerdesId(ThaModuleEth self, uint32 ethPortId)
    {
    uint32 xfiGroup = Tha60210051ModuleEthXfiGroupGet((AtModuleEth)self);
    AtUnused(ethPortId);

    return xfiGroup;
    }

static uint32 StartVersionSupportNewPMCCounterOffset(Tha60210012ModuleEth self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x1, 0x1016);
    }

static eBool NewOffsetPMCInternalIsSupported(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion >= mMethodsGet(mThis(self))->StartVersionSupportNewPMCCounterOffset(mThis(self)))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PmcInternalBaseAddress(AtModuleEth self)
    {
    if (NewOffsetPMCInternalIsSupported(self))
        return 0x1C00000;
        
    return 0xE40000;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsEthRegister(AtModuleDeviceGet(self), address);
    }

static void OverrideAtObject(AtModuleEth self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    if (portId == 0)
        return Tha60210012EthPortNew(portId, self);

    return Tha60210012EXauiNew(portId, self);
    }

static ThaClaEthPortController ClaEthPortController(AtModuleEth self)
    {
    AtModule cla = AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleCla);
    return ThaModuleClaEthPortControllerGet((ThaModuleCla)cla);
    }

static eAtRet PtchLookupEnable(Tha60210012ModuleEth self, eBool enabled)
    {
    AtEthPort port = AtModuleEthPortGet((AtModuleEth)self, 0);
    return Tha60210012ClaPwEthPortControllerPtchEnable(ClaEthPortController((AtModuleEth)self), port, enabled);
    }

static eBool PtchLookupIsEnabled(AtModuleEth self)
    {
    AtEthPort port = AtModuleEthPortGet(self, 0);
    return Tha60210012ClaPwEthPortControllerPtchIsEnabled(ClaEthPortController(self), port);
    }

static eAtRet IngressChannelIdSet(AtModuleEth self, uint8 channelId)
    {
    uint32 regAddr = Tha60210012ModuleEthEXaui1BaseAddress(self) + cAf6Reg_cfg2_pen_Base;
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_cfg2_pen_Channel_ID_, (uint8)channelId);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 StartHwVersionSupportCrossPoint(Tha60210051ModuleEth self)
    {
    AtUnused(self);
    /* This product does not support */
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(cBit31_0, cBit31_0, cBit31_0);
    }

static uint32 BaseAddressEXaui1(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    ThaModulePwe pweModule = (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);

    if (hwVersion >= Tha60210012ModulePweStartVersionSupportInsertEXaui(pweModule))
        return 0x0E80000;

    return 0x0E02000;
    }

static uint32 BaseAddressEXaui2(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    ThaModulePwe pweModule = (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);

    if (hwVersion >= Tha60210012ModulePweStartVersionSupportInsertEXaui(pweModule))
        return 0x0E81000;

    return 0x0E03000;
    }

static void IngressVlanCache(AtModuleEth module, const tAtVlan* vlan)
    {
    AtOsalMemCpy(&mThis(module)->controlFlowIgVlan, vlan, sizeof(tAtVlan));
    }

static void IngressVlanFromCache(AtModuleEth module, tAtVlan *vlan)
    {
    AtOsalMemCpy(vlan, &mThis(module)->controlFlowIgVlan, sizeof(tAtVlan));
    }

static ThaOamFlowManager OamFlowManagerObjectCreate(ThaModuleEth self)
    {
    return Tha60210012OamFlowManagerNew((AtModuleEth)self);
    }

static uint32 StartHwVersionReduceTo4Serdes(AtModuleEth self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x0);
    }

static void XfiGroupSelect(Tha60210051ModuleEth self, uint32 xfiGroupIdx)
    {
    uint32 regValue;
    uint8 firstXfiInGroupSelected = (xfiGroupIdx == 1) ? 1 : 0; /* Group#0: XFI:0,2
                                                                   Group#1: XFI:1,3 */
    regValue = mModuleHwRead(self, cEthPortSerdesSelectReg);
    mRegFieldSet(regValue, cThaReg_XFI_Mac0_Sel_, firstXfiInGroupSelected);
    mModuleHwWrite(self, cEthPortSerdesSelectReg, regValue);
    }

static uint32 XfiGroupGet(Tha60210051ModuleEth self)
    {
    uint32 regValue = mModuleHwRead(self, cEthPortSerdesSelectReg);
    uint32 selectedSerdes = mRegField(regValue, cThaReg_XFI_Mac0_Sel_);

    return ((selectedSerdes == 0) || (selectedSerdes == 2)) ? 0 : 1;
    }

static void ControlVlanUse(ThaModuleEth self, uint16 vlanId)
    {
    uint16 currentVlanId;
    tAtVlan vlans;
    AtEthFlow flow = NULL;

    /* Clear current vlanId */
    Tha60210012ModuleEthOamFlowIngressControlVlanGet((AtModuleEth)self, &vlans);
    currentVlanId = vlans.vlanId;
    ThaModuleEthIngressVlanUnUse(self, flow, currentVlanId);

    /* Apply new VlanId */
    ThaModuleEthIngressVlanUse(self, flow, vlanId);
    }

static uint16 MaxNumVlanId(void)
    {
    return 4096;
    }

static ThaBitMask VlanMask(ThaModuleEth self)
    {
    if (mThis(self)->vlanMask == NULL)
        mThis(self)->vlanMask = ThaBitMaskNew(MaxNumVlanId());
    return mThis(self)->vlanMask;
    }

static eBool VlanIsValid(ThaModuleEth self, uint32 vlanId)
    {
    AtUnused(self);
    return (vlanId < MaxNumVlanId()) ? cAtTrue : cAtFalse;
    }

static ThaModuleCla ModuleCla(AtModuleEth self)
    {
    return (ThaModuleCla)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleCla);
    }

static eAtRet IngressControlVlanRemove(AtModuleEth module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    static const uint16 cInvalidVlan = 0xFFF;
    uint32 regAddress;
    AtEthFlow flow = NULL;
    tAtVlan curVlan;
    uint32 longRegVal[cThaLongRegMaxSize];
    AtIpCore core;

    /* Reset hardware */
    regAddress = cAf6Reg_cla_glb_psn_Base + Tha60210011ModuleClaBaseAddress(ModuleCla(module));
    core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)module), AtModuleDefaultCoreGet((AtModule)module));
    mModuleHwLongRead(module, regAddress, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[0], cAf6_cla_glb_psn_RxVlanCtrl_, cInvalidVlan);
    mRegFieldSet(longRegVal[2], cAf6_cla_glb_psn_VlanCtrlEnable_, 0);
    mModuleHwLongWrite(module, regAddress, longRegVal, cThaLongRegMaxSize, core);

    /* Clear database */
    Tha60210012ModuleEthOamFlowIngressControlVlanGet(module, &curVlan);
    ThaModuleEthIngressVlanUnUse((ThaModuleEth)module, flow, curVlan.vlanId);

    mMethodsGet(osal)->MemInit(osal, &curVlan, 0, sizeof(tAtVlan));
    IngressVlanCache(module, &curVlan);

    return cAtOk;
    }

static eAtRet IngressControlVlanAdd(AtModuleEth module, const tAtVlan* vlan)
    {
    ThaModuleCla claModule;
    uint32 regAddress;
    ThaModuleEth thaEthModule = (ThaModuleEth)module;
    uint32 longRegVal[cThaLongRegMaxSize];
    AtIpCore core;

    if (ThaModuleEthIngressVlanIsInused(thaEthModule, NULL, vlan->vlanId))
        {
        AtModuleLog((AtModule)module, cAtLogLevelCritical, AtSourceLocation, "VlanId %d is used by other control or data flow\r\n", vlan->vlanId);
        return cAtErrorChannelBusy;
        }

    claModule = (ThaModuleCla)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)module), cThaModuleCla);
    core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)module), AtModuleDefaultCoreGet((AtModule)module));
    regAddress = cAf6Reg_cla_glb_psn_Base + Tha60210011ModuleClaBaseAddress(claModule);
    mModuleHwLongRead(module, regAddress, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[0], cAf6_cla_glb_psn_RxVlanCtrl_, vlan->vlanId);
    mRegFieldSet(longRegVal[2], cAf6_cla_glb_psn_VlanCtrlEnable_, 1);
    mModuleHwLongWrite(module, regAddress, longRegVal, cThaLongRegMaxSize, core);

    /* Updated database */
    ControlVlanUse(thaEthModule, vlan->vlanId);
    IngressVlanCache(module, vlan);

    return cAtOk;
    }

static uint32 StartVersionSupportNewMaxMinPacketSize(Tha60210012ModuleEth self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x0, 0x13);
    }

static eBool ExternalPmcCounterIsSupported(Tha60210012ModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool OamMACHeaderNeedToBeUpdated(AtModuleEth module, const uint8* newSrcMac, eAtRet (*macGetFunc)(AtModuleEth, uint8 *))
    {
    eAtRet ret;
    uint8 currMacs[cTha60210012MaxNumOffsetControlHdr];
    uint8 byte_i;

    ret = macGetFunc(module, currMacs);
    if (ret != cAtOk)
        return cAtTrue;

    for (byte_i = 0; byte_i < cTha60210012MaxNumOffsetControlHdr; byte_i++)
        {
        if (newSrcMac[byte_i] != currMacs[byte_i])
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool OamVlanHeaderNeedTobeUpdated(AtModuleEth module, const tAtVlan* newVlan, eAtRet (*vlanGetFunc)(AtModuleEth, tAtVlan*))
    {
    tAtVlan curVlan;

    vlanGetFunc(module, &curVlan);
    if ((curVlan.cfi      != newVlan->cfi)      ||
        (curVlan.priority != newVlan->priority) ||
        (curVlan.tpid     != newVlan->tpid)     ||
        (curVlan.vlanId   != newVlan->vlanId))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 XauiSerdesTuningIsSupported(AtModuleEth self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xf, 0xf, 0xffff);
    }

static void MethodsInit(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, EXauInit);
        mMethodOverride(m_methods, SerdesDefaultSetup);
        mMethodOverride(m_methods, AllPtchServiceCreate);
        mMethodOverride(m_methods, AllPtchServiceDelete);
        mMethodOverride(m_methods, StartVersionSupportNewMaxMinPacketSize);
        mMethodOverride(m_methods, StartVersionSupportNewPMCCounterOffset);
        mMethodOverride(m_methods, AsyncOperationGet);
        mMethodOverride(m_methods, NumAsyncOperation);
        mMethodOverride(m_methods, PtchLookupEnable);
        mMethodOverride(m_methods, ExternalPmcCounterIsSupported);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideTha60210051ModuleEth(AtModuleEth self)
    {
    Tha60210051ModuleEth module = (Tha60210051ModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210051ModuleEthMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051ModuleEthOverride, mMethodsGet(module), sizeof(m_Tha60210051ModuleEthOverride));

        mMethodOverride(m_Tha60210051ModuleEthOverride, XfiGroupSelect);
        mMethodOverride(m_Tha60210051ModuleEthOverride, XfiGroupGet);
        mMethodOverride(m_Tha60210051ModuleEthOverride, StartHwVersionSupportCrossPoint);
        }

    mMethodsSet(module, &m_Tha60210051ModuleEthOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, MaxFlowsGet);
        mMethodOverride(m_AtModuleEthOverride, FlowObjectCreate);
        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, MacBaseAddress);
        mMethodOverride(m_ThaModuleEthOverride, SerdesIdToEthPortId);
        mMethodOverride(m_ThaModuleEthOverride, EthPortIdToSerdesId);
        mMethodOverride(m_ThaModuleEthOverride, StartVersionSupportsSerdesAps);
        mMethodOverride(m_ThaModuleEthOverride, FlowControllerFactory);
        mMethodOverride(m_ThaModuleEthOverride, MaxNumMpFlows);
        mMethodOverride(m_ThaModuleEthOverride, FlowHeaderProviderCreate);
        mMethodOverride(m_ThaModuleEthOverride, SerdesManagerCreate);
        mMethodOverride(m_ThaModuleEthOverride, OamFlowManagerObjectCreate);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    OverrideTha60210051ModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleEth);
    }

AtModuleEth Tha60210012ModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60210012ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModuleEthObjectInit(newModule, device);
    }

eAtRet Tha60210012ModuleEthPtchLookupEnable(AtModuleEth self, eBool enabled)
    {
    if (self)
        return mMethodsGet(mThis(self))->PtchLookupEnable(mThis(self), enabled);
    return cAtErrorNullPointer;
    }

eBool Tha60210012ModuleEthPtchLookupIsEnabled(AtModuleEth self)
    {
    if (self)
        return PtchLookupIsEnabled(self);
    return cAtFalse;
    }

uint32 Tha60210012ModuleEthNumExauiGet(AtModuleEth self)
    {
    if (self)
        return NumExauiGet(self);
    return 0;
    }

AtEthPort Tha60210012ModuleEthExauiGet(AtModuleEth self, uint32 exauiId)
    {
    if (self)
        return ExauiGet(self, exauiId);
    return NULL;
    }

AtPtchService Tha60210012ModuleEthPtchServiceGet(AtModuleEth self, eAtPtchServiceType serviceType)
    {
    if (self)
        return PtchServiceGet(self, serviceType);
    return NULL;
    }

eAtRet Tha60210012ModuleEthIngressChannelIdSet(AtModuleEth self, uint8 channelId)
    {
    if (self)
        return IngressChannelIdSet(self, channelId);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012ModuleEthExauiGlobalCountersDebug(AtModuleEth self, eBool r2c)
    {
    const char* mGlobalCountersForMuxExaui0Str[] = {
                                                    "Packet Input MUX Source0 for PW",
                                                    "Packet Input MUX Source1 for eXAUI#0",
                                                    "Packet Input MUX Source2 for iMSG/EOP",
                                                    "Packet Input MUX Source3 for eXAUI#1",
                                                    "Byte Input MUX Source0 for PW",
                                                    "Byte Input MUX Source1 for eXAUI#0",
                                                    "Byte Input MUX Source2 for iMSG/EOP",
                                                    "Byte Input MUX Source3 for eXAUI#1",
                                                    "Packet Output MUX Source0 for PW",
                                                    "Packet Output MUX Source1 for eXAUI#0",
                                                    "Packet Output MUX Source2 for iMSG/EOP",
                                                    "Packet Output MUX Source3 for eXAUI#1",
                                                    "Byte Output MUX Source0 for PW",
                                                    "Byte Output MUX Source1 for eXAUI#0",
                                                    "Byte Output MUX Source2 for iMSG/EOP",
                                                    "Byte Output MUX Source3 for eXAUI#1",
                                                    "Packet Output to TX MAC Source0 for PW",
                                                    "Packet Output to TX MAC Source1 for eXAUI#0",
                                                    "Packet Output to TX MAC Source2 for iMSG/EOP",
                                                    "Packet Output to TX MAC Source3 for eXAUI#1",
                                                    "Byte Output to TX MAC Source0 for PW",
                                                    "Byte Output to TX MAC Source1 for eXAUI#0",
                                                    "Byte Output to TX MAC Source2 for iMSG/EOP",
                                                    "Byte Output to TX MAC Source3 for eXAUI#1",
                                                    "Input eXAUI#0 Buffer Error(FCS or MTU over)",
                                                    "Input eXAUI#0 Buffer Monitor MTU over",
                                                    "eXAUI#0 Buffer Full"

    };
    uint32 counter_i;

    AtPrintc(cSevDebug, "Global Counter for MUX and eXAUI#0 Buffer\r\n");
    AtPrintc(cSevDebug, "%-80s\r\n", "-");

    for(counter_i = 0; counter_i < mCount(mGlobalCountersForMuxExaui0Str); counter_i++)
        {
        uint32 regAddr = Tha60210012ModuleEthEXaui1BaseAddress(self) + cAf6Reg_pmcnt_pen(counter_i);
        uint32 counter = mModuleHwRead(self, regAddr);
        AtPrintc(cSevNormal, "%-50s", mGlobalCountersForMuxExaui0Str[counter_i]);
        AtPrintc(cSevInfo, ": %u\r\n", counter);

        if(r2c)
            mModuleHwWrite(self, regAddr, 0x0);
        }
    AtPrintc(cSevDebug, "%-80s\r\n", "-");

    return cAtOk;
    }

eBool Tha60210012ModuleEthReduceTo4Serdes(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion >= StartHwVersionReduceTo4Serdes(self))
        return cAtTrue;

    return cAtFalse;
    }

uint32 Tha60210012ModuleEthEXaui1BaseAddress(AtModuleEth self)
    {
    if(self)
        return BaseAddressEXaui1(self);
    return 0;
    }

uint32 Tha60210012ModuleEthEXaui2BaseAddress(AtModuleEth self)
    {
    if(self)
        return BaseAddressEXaui2(self);
    return 0;
    }

uint8 Tha60210012ModuleEthEXaui0PortIdGet(AtModuleEth self)
    {
    if (self)
        return EXaui0PortIdGet(self);

    return cInvalidUint8;
    }

uint8 Tha60210012ModuleEthEXaui1PortIdGet(AtModuleEth self)
    {
    if (self)
        return EXaui1PortIdGet(self);

    return cInvalidUint8;
    }

eAtRet Tha60210012ModuleEthAllServicesRemove(ThaModuleEth self)
    {
    /* Stop all PTCH service */
    AtPtchServiceEnable(mThis(self)->cemPtch, cAtFalse);
    AtPtchServiceEnable(mThis(self)->imsgEoPPtch, cAtFalse);

    if (mThis(self)->exaui0Ptch)
        AtPtchServiceEnable(mThis(self)->exaui0Ptch, cAtFalse);

    if (mThis(self)->exaui1Ptch)
        AtPtchServiceEnable(mThis(self)->exaui1Ptch, cAtFalse);

    /* Remove all Flows*/
    return AllFlowsServiceRemove(self);
    }

uint32 Tha60210012ModuleEthFlowLookupBaseAddress(void)
    {
    return cThaModuleEthFlowLookupBaseAddress;
    }

eAtRet Tha60210012ModuleEthOamFlowEgressDestMacSet(AtModuleEth self, const uint8 *destMac)
    {
    uint32 buffer[cTha60210012MaxNumOffsetControlHdr];

    if ((self == NULL) || (destMac == NULL))
        return cAtErrorNullPointer;

    /* Nothing to do when the oam not update */
    if (!OamMACHeaderNeedToBeUpdated(self, destMac, Tha60210012ModuleEthOamFlowEgressDestMacGet))
        return cAtOk;

    HeaderRead(self, buffer);
    buffer[0]  = (uint32)(destMac[0] << 24);
    buffer[0] |= (uint32)(destMac[1] << 16);
    buffer[0] |= (uint32)(destMac[2] << 8);
    buffer[0] |= (uint32)(destMac[3]);

    buffer[1] = buffer[1] & cBit15_0;
    buffer[1] |= (uint32)(destMac[4] << 24);
    buffer[1] |= (uint32)(destMac[5] << 16);
    HeaderWrite(self, buffer);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEthOamFlowEgressDestMacGet(AtModuleEth module, uint8* destMac)
    {
    uint32 buffer[cTha60210012MaxNumOffsetControlHdr];

    if (module == NULL)
        return cAtErrorNullPointer;

    HeaderRead(module, buffer);
    destMac[0] = (uint8)((buffer[0] >> 24) & cBit7_0);
    destMac[1] = (uint8)((buffer[0] >> 16) & cBit7_0);
    destMac[2] = (uint8)((buffer[0] >> 8)  & cBit7_0);
    destMac[3] = (uint8)((buffer[0] & cBit7_0));
    destMac[4] = (uint8)((buffer[1] >> 24) & cBit7_0);
    destMac[5] = (uint8)((buffer[1] >> 16) & cBit7_0);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEthOamFlowEgressSrcMacSet(AtModuleEth module, const uint8* srcMac)
    {
    uint32 buffer[cTha60210012MaxNumOffsetControlHdr];

    if ((module == NULL) || (srcMac == NULL))
        return cAtErrorNullPointer;

    /* Nothing to do when the oam not update */
    if (!OamMACHeaderNeedToBeUpdated(module, srcMac, Tha60210012ModuleEthOamFlowEgressSrcMacGet))
        return cAtOk;

    HeaderRead(module, buffer);
    buffer[1] = buffer[1] & cBit31_16;
    buffer[1] |= (uint32)(srcMac[0] << 8);
    buffer[1] |= (uint32)(srcMac[1]);

    buffer[2] = (uint32)(srcMac[2] << 24);
    buffer[2] |= (uint32)(srcMac[3] << 16);
    buffer[2] |= (uint32)(srcMac[4] << 8);
    buffer[2] |= (uint32)(srcMac[5]);
    HeaderWrite(module, buffer);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEthOamFlowEgressSrcMacGet(AtModuleEth module, uint8* srcMac)
    {
    uint32 buffer[cTha60210012MaxNumOffsetControlHdr];

    if (module == NULL)
        return cAtErrorNullPointer;

    HeaderRead(module, buffer);
    srcMac[0] = (uint8)((buffer[1] >> 8) & cBit7_0);
    srcMac[1] = (uint8)((buffer[1] & cBit7_0));
    srcMac[2] = (uint8)((buffer[2] >> 24)& cBit7_0);
    srcMac[3] = (uint8)((buffer[2] >> 16)& cBit7_0);
    srcMac[4] = (uint8)((buffer[2] >> 8) & cBit7_0);
    srcMac[5] = (uint8)((buffer[2])      & cBit7_0);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEthOamFlowEgressEthTypeSet(AtModuleEth module, uint16 ethType)
    {
    uint32 buffer[cTha60210012MaxNumOffsetControlHdr];

    if (module == NULL)
        return cAtErrorNullPointer;

    if (Tha60210012ModuleEthOamFlowEgressEthTypeGet(module) == ethType)
        return cAtOk;

    HeaderRead(module, buffer);
    buffer[5] = (uint32)(ethType << 16);
    HeaderWrite(module, buffer);

    return cAtOk;
    }

uint16 Tha60210012ModuleEthOamFlowEgressEthTypeGet(AtModuleEth module)
    {
    uint32 buffer[cTha60210012MaxNumOffsetControlHdr];

    if (module == NULL)
        return 0;

    HeaderRead(module, buffer);
    return (uint16)(((buffer[5] >> 16) & cBit15_0));
    }

eAtRet Tha60210012ModuleEthOamFlowEgressControlVlanSet(AtModuleEth module, const tAtVlan* vlan)
    {
    uint32 buffer[cTha60210012MaxNumOffsetControlHdr];

    if ((module == NULL) || (vlan == NULL))
        return cAtErrorNullPointer;

    if (!OamVlanHeaderNeedTobeUpdated(module, vlan, Tha60210012ModuleEthOamFlowEgressControlVlanGet))
        return cAtOk;

    /* Build VLAN for Control flow */
    HeaderRead(module, buffer);
    buffer[3]  = (uint32)((vlan->tpid & cBit15_0)    << 16);
    buffer[3] |= (uint32)((vlan->priority & cBit2_0) << 13);
    buffer[3] |= (uint32)((vlan->cfi & cBit0)        << 12);
    buffer[3] |= (uint32) (vlan->vlanId & cBit11_0);
    HeaderWrite(module, buffer);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEthOamFlowEgressControlVlanGet(AtModuleEth module, tAtVlan* vlan)
    {
    uint32 buffer[cTha60210012MaxNumOffsetControlHdr];

    if ((module == NULL) || (vlan == NULL))
        return cAtErrorNullPointer;

    HeaderRead(module, buffer);
    vlan->tpid     = (uint16)((buffer[3] >> 16) & cBit15_0);
    vlan->priority = (uint8)((buffer[3]  >> 13) & cBit2_0);
    vlan->cfi      = (uint8)((buffer[3]  >> 12) & cBit0);
    vlan->vlanId   = (uint16)(buffer[3] & cBit11_0);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEthOamFlowIngressControlVlanSet(AtModuleEth module, const tAtVlan* vlan)
    {
    if (module == NULL)
        return cAtErrorNullPointer;

    /* Remove vlan */
    if (vlan == NULL)
        return IngressControlVlanRemove(module);

    return IngressControlVlanAdd(module, vlan);
    }

eAtRet Tha60210012ModuleEthOamFlowIngressControlVlanGet(AtModuleEth module, tAtVlan* vlan)
    {
    if ((module == NULL) || (vlan == NULL))
        return cAtErrorNullPointer;

    IngressVlanFromCache(module, vlan);
    return cAtOk;
    }

uint32 Tha60210012ModuleEthHwFlowCounterGet(ThaModuleEth self, uint32 hwFlowId, uint16 hwCounterType, eBool r2c)
    {
    uint32 baseAddress, regAddress;

    /* Invalid counter type */
    if ((self == NULL) || (hwCounterType == cBit7_0))
        return 0;

    /* Address + $cnt_type * 8192 + $r2c * 4096 + $flowId */
    baseAddress = Tha60210012ModuleEthPMCInternalBaseAddress((AtModuleEth)self) + (uint32)(r2c * 4096) + (uint32)(hwCounterType * 8192) + hwFlowId;
    regAddress = baseAddress + cAf6Reg_ethfulltypcnt_pen_Base;

    return mModuleHwRead(self, regAddress);
    }

uint32 Tha60210012ModuleEthPMCInternalBaseAddress(AtModuleEth self)
    {
    if (self)
        return PmcInternalBaseAddress(self);

    return cBit31_0;
    }

eAtRet Tha60210012ModuleEthOamFlowEgressCircuitVlanTPIDSet(AtModuleEth module, uint16 tpid)
    {
    uint32 buffer[cTha60210012MaxNumOffsetControlHdr];

    if (Tha60210012ModuleEthOamFlowEgressCircuitVlanTPIDGet(module) == tpid)
        return cAtOk;

    HeaderRead(module, buffer);
    buffer[4]  = (uint32)((tpid & cBit15_0) << 16);
    HeaderWrite(module, buffer);

    return cAtOk;
    }

uint16 Tha60210012ModuleEthOamFlowEgressCircuitVlanTPIDGet(AtModuleEth module)
    {
    uint32 buffer[cTha60210012MaxNumOffsetControlHdr];

    HeaderRead(module, buffer);
    return (uint16)((buffer[4] >> 16) & cBit15_0);
    }

eAtRet Tha60210012ModuleEthIngressVlanCircuitUse(ThaModuleEth self, uint32 vlanId)
    {
    ThaBitMask vlanMask;

    if (!VlanIsValid(self, vlanId))
        return cAtErrorInvlParm;

    vlanMask = VlanMask(self);
    if (ThaBitMaskBitVal(vlanMask, vlanId))
        return cAtErrorChannelBusy;

    ThaBitMaskSetBit(vlanMask, vlanId);

    return cAtOk;
    }

eAtRet Tha60210012ModuleEthIngressVlanCircuitUnUse(ThaModuleEth self, uint32 vlanId)
    {
    ThaBitMask vlanMask;

    if (!VlanIsValid(self, vlanId))
        return cAtErrorInvlParm;

    vlanMask = VlanMask(self);
    ThaBitMaskClearBit(vlanMask, vlanId);

    return cAtOk;
    }

eBool Tha60210012ModuleEthIngressVlanCircuitIsInused(ThaModuleEth self, uint32 vlanId)
    {
    ThaBitMask vlanMask;

    if (!VlanIsValid(self, vlanId))
        return cAtFalse;

    vlanMask = VlanMask(self);
    return (ThaBitMaskBitVal(vlanMask, vlanId) == 1) ? cAtTrue : cAtFalse;
    }

void Tha60210012ModuleEthEXauiActive(ThaModuleEth self, eBool enable)
    {
    if (self)
        EXauiActive((Tha60210012ModuleEth)self, enable);
    }

eBool Tha60210012NewMaxMinPacketSizeSupported(Tha60210012ModuleEth self)
    {
    AtDevice device;
    ThaVersionReader versionReader;
    uint32 hwVersion, startSupportedVersion;

    if (self == NULL)
        return cAtFalse;

    device = AtModuleDeviceGet((AtModule)self);
    versionReader = ThaDeviceVersionReader(device);
    hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    startSupportedVersion = mMethodsGet(self)->StartVersionSupportNewMaxMinPacketSize(self) ;

    return (hwVersion >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

eBool Tha60210012ModuleEthExternalPmcCounterIsSupported(Tha60210012ModuleEth self)
    {
    if (self == NULL)
        return cAtFalse;

    return mMethodsGet(self)->ExternalPmcCounterIsSupported(self) ;
    }

eBool Tha60210012ModuleEthXauiSerdesTuningIsSupported(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion >= XauiSerdesTuningIsSupported(self))
        return cAtTrue;

    return cAtFalse;
    }


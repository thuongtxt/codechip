/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210051EthPort.c
 *
 * Created Date: Oct 23, 2015
 *
 * Description : Ethernet port of 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../Tha60210051/eth/Tha60210051ModuleEthInternal.h"
#include "../../../default/cla/controllers/ThaClaController.h"
#include "../man/Tha60210012Device.h"
#include "../cla/Tha60210012ModuleCla.h"
#include "Tha60210012ModuleEth.h"
#include "Tha60210012EthPortInternal.h"
#include "Tha60210012EthPortReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods      m_AtChannelOverride;
static tAtEthPortMethods      m_AtEthPortOverride;
static tThaEthPortMethods     m_ThaEthPortOverride;

/* Save super implementation */
static const tAtChannelMethods  *m_AtChannelMethods = NULL;
static const tAtEthPortMethods  *m_AtEthPortMethods = NULL;
static const tThaEthPortMethods *m_ThaEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (counterType == cAtEthPortCounterRxErrPsnPackets)
        return cAtTrue;

    return m_AtChannelMethods->CounterIsSupported(self, counterType);
    }

static AtModuleEth ModuleEth(AtEthPort self)
    {
    return (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static eBool SerdesIsInGroup(uint32 groupId, AtSerdesController serdes)
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);

    if (groupId == 0 && (serdesId == 0 || serdesId == 2))
        return cAtTrue;

    if (groupId == 1 && (serdesId == 1 || serdesId == 3))
        return cAtTrue;

    return cAtFalse;
    }

static eBool SerdesOperationIsValid(AtEthPort self, AtSerdesController serdes)
    {
    AtModuleEth module = ModuleEth(self);
    uint32 xfiGroup = Tha60210051ModuleEthXfiGroupGet(module);

    if (!SerdesIsInGroup(xfiGroup, serdes))
        return cAtFalse;

    return cAtTrue;
    }

static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    uint32 regVal, serdesHwId;

    if (!Tha60210012ModuleEthReduceTo4Serdes(ModuleEth(self)))
        return m_AtEthPortMethods->RxSerdesSelect(self, serdes);

    if (!SerdesOperationIsValid(self, serdes))
        return cAtErrorInvlParm;

    regVal = mChannelHwRead(self, cEthPortSerdesSelectReg, cAtModuleEth);
    serdesHwId = mMethodsGet(serdes)->HwIdGet(serdes);
    mRegFieldSet(regVal, cThaReg_XFI_Mac0_Sel_, serdesHwId);
    mChannelHwWrite(self, cEthPortSerdesSelectReg, regVal, cAtModuleEth);

    return cAtOk;
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    ThaModuleEth module = (ThaModuleEth)ModuleEth(self);
    uint32 regVal, serdesHwId, serdesSwId;

    if (!Tha60210012ModuleEthReduceTo4Serdes((AtModuleEth)module))
        return m_AtEthPortMethods->RxSelectedSerdes(self);

    regVal = mChannelHwRead(self, cEthPortSerdesSelectReg, cAtModuleEth);
    serdesHwId = mRegField(regVal, cThaReg_XFI_Mac0_Sel_);
    serdesSwId = mMethodsGet(module)->SerdesIdHwToSw(module, serdesHwId);

    return AtModuleEthSerdesController((AtModuleEth)module, serdesSwId);
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    if (!Tha60210012ModuleEthReduceTo4Serdes(ModuleEth(self)))
        return m_AtEthPortMethods->TxSerdesBridge(self, serdes);

    if (!SerdesOperationIsValid(self, serdes))
        return cAtErrorModeNotSupport;

    /* HW bridge as default */
    return cAtOk;
    }

static eBool TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    if (!Tha60210012ModuleEthReduceTo4Serdes(ModuleEth(self)))
        return m_AtEthPortMethods->TxSerdesCanBridge(self, serdes);

    if (!SerdesOperationIsValid(self, serdes))
        return cAtFalse;

    return cAtTrue;
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    uint32 brigdedSerdesId, serdesId;
    AtSerdesController controller;
    AtModuleEth module = ModuleEth(self);

    if (!Tha60210012ModuleEthReduceTo4Serdes(module))
        return m_AtEthPortMethods->TxBridgedSerdes(self);

    controller = AtEthPortSerdesController(self);
    serdesId = AtSerdesControllerIdGet((controller));
    brigdedSerdesId = serdesId + 2;

    return AtModuleEthSerdesController(module, brigdedSerdesId);
    }

static eAtRet MacActivate(ThaEthPort self, eBool activate)
    {
    AtChannel channel = (AtChannel)self;
    ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(AtChannelDeviceGet(channel), cThaModuleCla);
    ThaClaController claController = (ThaClaController)ThaModuleClaEthPortControllerGet(claModule);

    ThaClaControllerChannelEnable(claController, channel, activate);
    return cAtOk;
    }

static eAtRet DefaultMaxMinPacketSizeSet(AtChannel self)
    {
    eAtRet ret;
    ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleCla);
    ThaClaEthPortController controller = ThaModuleClaEthPortControllerGet(claModule);

    /* HW recommend set max packet size = 12000 and min = 64 */
    ret = Tha60210012ClaPwEthPortControllerMaxPacketSizeSet(controller, (AtEthPort)self, 12000);
    ret |= Tha60210012ClaPwEthPortControllerMinPacketSizeSet(controller, (AtEthPort)self, 64);

    return ret;
    }

static eBool NewMaxMinPacketSizeSupported(AtChannel self)
    {
    return Tha60210012NewMaxMinPacketSizeSupported((Tha60210012ModuleEth)AtChannelModuleGet(self));
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (NewMaxMinPacketSizeSupported(self))
        return DefaultMaxMinPacketSizeSet(self);

    return cAtOk;
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, RxSerdesSelect);
        mMethodOverride(m_AtEthPortOverride, RxSelectedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesBridge);
        mMethodOverride(m_AtEthPortOverride, TxBridgedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesCanBridge);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort channel = (ThaEthPort)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPortMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, mMethodsGet(channel), sizeof(m_ThaEthPortOverride));

        mMethodOverride(m_ThaEthPortOverride, MacActivate);
        }

    mMethodsSet(channel, &m_ThaEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012EthPort);
    }

AtEthPort Tha60210012EthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051EthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Initialize method */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60210012EthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012EthPortObjectInit(newPort, portId, module);
    }

eBool Tha60210012EthPortIsEXauiPort(AtEthPort self)
    {
    if (AtChannelIdGet((AtChannel)self) == 0x0)
        return cAtFalse;
    return cAtTrue;
    }

eBool Tha60210012EthPortPtchIsEnabled(AtEthPort port)
    {
    AtModuleEth moduleEth = (AtModuleEth)AtChannelModuleGet((AtChannel)port);
    AtPtchService ptchService = Tha60210012ModuleEthPtchServiceGet(moduleEth, cAtPtchServiceTypeCEM);

    if (AtPtchServiceIsEnabled(ptchService))
        return cAtTrue;

    ptchService = Tha60210012ModuleEthPtchServiceGet(moduleEth, cAtPtchServiceTypeIMSG_EoP);
    if (AtPtchServiceIsEnabled(ptchService))
        return cAtTrue;

    return cAtFalse;
    }

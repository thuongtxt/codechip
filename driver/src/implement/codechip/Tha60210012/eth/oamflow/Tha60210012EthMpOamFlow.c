/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210012EthMpOamFlow.c
 *
 * Created Date: Aug 23, 2016
 *
 * Description : MP Oam Flow
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/eth/oamflow/ThaOamFlowInternal.h"
#include "../../encap/Tha60210012ModuleEncap.h"
#include "../../cla/Tha60210012ModuleCla.h"
#include "../Tha60210012ModuleEth.h"
#include "Tha60210012OamFlowManager.h"
#include "Tha60210012EthOamFlow.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210012MpOamFlow *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012EthMpOamFlow
    {
    tTha60210012EthPppOamFlow super;
    }tTha60210012EthMpOamFlow;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210012PppOamFlowMethods m_Tha60210012PppOamFlowOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet OamServiceEnable(Tha60210012PppOamFlow self, uint16 vlanId)
    {
    return Tha60210012ModuleClaMlpppOamServiceEnable(AtEthFlowHdlcBundleGet((AtEthFlow)self), vlanId);
    }

static eAtRet OamServiceDisable(Tha60210012PppOamFlow self, uint16 vlanId)
    {
    return Tha60210012ModuleClaMlpppOamServiceDisable(AtEthFlowHdlcBundleGet((AtEthFlow)self), vlanId);
    }

static eAtRet OamCircuitVlanInsert(Tha60210012PppOamFlow self, uint16 vlanId, eBool enable)
    {
    return Tha60210012ModuleEncapHdlcBundleOamCircuitVlanInsert(AtEthFlowHdlcBundleGet((AtEthFlow)self), vlanId, enable);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012EthMpOamFlow);
    }

static void OverrideTha60210012PppOamFlow(AtEthFlow self)
    {
    Tha60210012PppOamFlow oamFlow = (Tha60210012PppOamFlow)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210012PppOamFlowOverride, mMethodsGet(oamFlow), sizeof(m_Tha60210012PppOamFlowOverride));

        mMethodOverride(m_Tha60210012PppOamFlowOverride, OamServiceEnable);
        mMethodOverride(m_Tha60210012PppOamFlowOverride, OamServiceDisable);
        mMethodOverride(m_Tha60210012PppOamFlowOverride, OamCircuitVlanInsert);
        }

    mMethodsSet(oamFlow, &m_Tha60210012PppOamFlowOverride);
    }

static void Override(AtEthFlow self)
    {
    OverrideTha60210012PppOamFlow(self);
    }

static AtEthFlow ObjectInit(AtEthFlow self, uint32 flowId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210012PppOamFlowObjectInit(self, flowId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthFlow Tha60210012MpOamFlowNew(uint32 flowId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthFlow newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlow, flowId, module);
    }

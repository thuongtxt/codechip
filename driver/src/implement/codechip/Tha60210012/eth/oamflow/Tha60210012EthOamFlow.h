/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : Tha60210012EthOamFlow.h
 * 
 * Created Date: Aug 30, 2016
 *
 * Description : OAM flow interfaces
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012ETHOAMFLOW_H_
#define _THA60210012ETHOAMFLOW_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012EthPppOamFlow * Tha60210012PppOamFlow;
typedef struct tTha60210012PppOamFlowMethods
    {
    eAtRet (*OamServiceEnable)(Tha60210012PppOamFlow self, uint16 vlanId);
    eAtRet (*OamServiceDisable)(Tha60210012PppOamFlow self, uint16 vlanId);
    eAtRet (*OamCircuitVlanInsert)(Tha60210012PppOamFlow self, uint16 vlanId, eBool enable);

    }tTha60210012PppOamFlowMethods;

typedef struct tTha60210012EthPppOamFlow
    {
    tThaOamFlow super;
    const tTha60210012PppOamFlowMethods * methods;

    /* Private database */
    tAtEthVlanDesc descs;
    }tTha60210012EthPppOamFlow;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthFlow Tha60210012PppOamFlowObjectInit(AtEthFlow self, uint32 flowId, AtModuleEth module);
uint32 Tha60210012EthFlowOamIngressAllVlansGet(AtEthFlow self, tAtEthVlanDesc *descs);
eBool Tha60210012EthFlowOamIngressVlanIsValid(const tAtEthVlanDesc *vlan);
eBool Tha60210012EthFlowOamVlanIsSame(const tAtEthVlanDesc *desc1, const tAtEthVlanDesc *desc2);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012ETHOAMFLOW_H_ */


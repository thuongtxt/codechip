/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210012EthPppOamFlow.c
 *
 * Created Date: Aug 23, 2016
 *
 * Description : PPP Oam Flow
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../util/coder/AtCoderUtil.h"
#include "../../../../default/eth/oamflow/ThaOamFlowInternal.h"
#include "../../encap/Tha60210012ModuleEncap.h"
#include "../../cla/Tha60210012ModuleCla.h"
#include "../Tha60210012ModuleEth.h"
#include "Tha60210012OamFlowManager.h"
#include "Tha60210012EthOamFlow.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210012PppOamFlow)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210012PppOamFlowMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtEthFlowMethods m_AtEthFlowOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtEthFlowMethods *m_AtEthFlowMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleEth EthModule(AtEthFlow self)
    {
    return (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static eBool HasVlan(AtEthFlow self)
    {
    return ((mThis(self))->descs.numberOfVlans > 0) ? cAtTrue : cAtFalse;
    }

static eAtRet OamServiceEnable(Tha60210012PppOamFlow self, uint16 vlanId)
    {
    return Tha60210012ModuleClaPppOamServiceEnable(AtEthFlowHdlcLinkGet((AtEthFlow)self), vlanId);
    }

static eAtRet OamServiceDisable(Tha60210012PppOamFlow self, uint16 vlanId)
    {
    return Tha60210012ModuleClaPppOamServiceDisable(AtEthFlowHdlcLinkGet((AtEthFlow)self), vlanId);
    }

static eAtModuleEthRet IngressVlanAdd(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    uint16 vlanId = desc->vlans[0].vlanId;
    ThaModuleEth ethModule = EthModule(self);
    AtOsal osal = AtSharedDriverOsalGet();

    if (!Tha60210012EthFlowOamIngressVlanIsValid(desc))
        return cAtErrorInvlParm;

    if (HasVlan(self))
        {
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, AtSourceLocation, "Could not add VLAN, only support 1 VLAN\r\n");
        return cAtErrorNotApplicable;
        }

    if (Tha60210012ModuleEthIngressVlanCircuitIsInused(ethModule, vlanId))
        {
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, AtSourceLocation, "VLAN is using for other purpose\r\n");
        return cAtErrorChannelBusy;
        }

    Tha60210012ModuleEthIngressVlanCircuitUse(ethModule, vlanId);
    mMethodsGet(osal)->MemCpy(osal, &mThis(self)->descs, desc, sizeof(tAtEthVlanDesc));

    return mMethodsGet(mThis(self))->OamServiceEnable(mThis(self), vlanId);
    }

static eAtModuleEthRet IngressVlanRemove(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    uint16 vlanId = desc->vlans[0].vlanId;
    ThaModuleEth ethModule = EthModule(self);
    AtOsal osal = AtSharedDriverOsalGet();

    if (!Tha60210012EthFlowOamIngressVlanIsValid(desc))
        return cAtErrorInvlParm;

    if (!Tha60210012EthFlowOamVlanIsSame(&mThis(self)->descs, desc))
        return cAtErrorObjectNotExist;

    Tha60210012ModuleEthIngressVlanCircuitUnUse(ethModule, vlanId);
    mMethodsGet(osal)->MemInit(osal, &mThis(self)->descs, 0, sizeof(tAtEthVlanDesc));

    return mMethodsGet(mThis(self))->OamServiceDisable(mThis(self), vlanId);
    }

static eAtRet OamCircuitVlanInsert(Tha60210012PppOamFlow self, uint16 vlanId, eBool enable)
    {
    return Tha60210012ModuleEncapHdlcLinkOamCircuitVlanInsert(AtEthFlowHdlcLinkGet((AtEthFlow)self), vlanId, enable);
    }

static eAtModuleEthRet EgressVlanAdd(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    eAtRet ret;
    tAtEthVlanTag cVlan;

    ret = m_AtEthFlowMethods->EgressVlanAdd(self, desc);
    if (ret != cAtOk)
        return ret;

    cVlan = desc->vlans[0];
    return mMethodsGet(mThis(self))->OamCircuitVlanInsert(mThis(self), cVlan.vlanId, cAtTrue);
    }

static eAtModuleEthRet EgressVlanRemove(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    eAtRet ret;
    tAtEthVlanTag cVlan;

    ret = m_AtEthFlowMethods->EgressVlanRemove(self, desc);
    if (ret != cAtOk)
        return ret;

    cVlan = desc->vlans[0];
    return mMethodsGet(mThis(self))->OamCircuitVlanInsert(mThis(self), cVlan.vlanId, cAtFalse);
    }

static eAtModuleEthRet EgressDestMacSet(AtEthFlow self, uint8 *egressDestMac)
    {
    AtUnused(self);
    AtUnused(egressDestMac);
    return cAtErrorNotImplemented;
    }

static eAtModuleEthRet EgressDestMacGet(AtEthFlow self, uint8 *egressDestMac)
    {
    AtUnused(self);
    AtUnused(egressDestMac);
    return cAtErrorNotImplemented;
    }

static eAtModuleEthRet EgressSourceMacSet(AtEthFlow self, uint8 *egressSourceMac)
    {
    AtUnused(self);
    AtUnused(egressSourceMac);
    return cAtErrorNotImplemented;
    }

static eAtModuleEthRet EgressSourceMacGet(AtEthFlow self, uint8 *egressSourceMac)
    {
    AtUnused(self);
    AtUnused(egressSourceMac);
    return cAtErrorNotImplemented;
    }

static uint32 IngressAllVlansGet(AtEthFlow self, tAtEthVlanDesc *descs)
    {
    return Tha60210012EthFlowOamIngressAllVlansGet(self, descs);
    }

static uint32 IngressNumVlansGet(AtEthFlow self)
    {
	return mThis(self)->descs.numberOfVlans;
    }

static tAtEthVlanDesc *IngressVlanAtIndex(AtEthFlow self, uint32 vlanIndex, tAtEthVlanDesc *descs)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (vlanIndex > 0)
        return NULL;

    mMethodsGet(osal)->MemCpy(osal, descs, &mThis(self)->descs, sizeof(tAtEthVlanDesc));
    return descs;
    }

static eBool EgressMacIsProgrammable(AtEthFlow self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(AtEthFlow self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, OamServiceEnable);
        mMethodOverride(m_methods, OamServiceDisable);
        mMethodOverride(m_methods, OamCircuitVlanInsert);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210012PppOamFlow object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(descs.ethPortId);
    mEncodeUInt(descs.numberOfVlans);
    AtCoderEncodeUInt8Array(encoder, (uint8*)object->descs.vlans,
                            sizeof(object->descs.vlans)*cAtMaxNumVlanTag, "descs.vlans");
    }

static void OverrideAtObject(AtEthFlow self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtEthFlow(AtEthFlow self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthFlowMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthFlowOverride, mMethodsGet(self), sizeof(m_AtEthFlowOverride));

        mMethodOverride(m_AtEthFlowOverride, IngressVlanAdd);
        mMethodOverride(m_AtEthFlowOverride, IngressVlanRemove);
        mMethodOverride(m_AtEthFlowOverride, EgressVlanAdd);
        mMethodOverride(m_AtEthFlowOverride, EgressVlanRemove);
        mMethodOverride(m_AtEthFlowOverride, EgressDestMacSet);
        mMethodOverride(m_AtEthFlowOverride, EgressDestMacGet);
        mMethodOverride(m_AtEthFlowOverride, EgressSourceMacSet);
        mMethodOverride(m_AtEthFlowOverride, EgressSourceMacGet);
        mMethodOverride(m_AtEthFlowOverride, IngressAllVlansGet);
        mMethodOverride(m_AtEthFlowOverride, IngressNumVlansGet);
        mMethodOverride(m_AtEthFlowOverride, IngressVlanAtIndex);
        mMethodOverride(m_AtEthFlowOverride, EgressMacIsProgrammable);
        }

    mMethodsSet(self, &m_AtEthFlowOverride);
    }

static void Override(AtEthFlow self)
    {
    OverrideAtObject(self);
    OverrideAtEthFlow(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012EthPppOamFlow);
    }

AtEthFlow Tha60210012PppOamFlowObjectInit(AtEthFlow self, uint32 flowId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaOamFlowObjectInit(self, flowId, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthFlow Tha60210012PppOamFlowNew(uint32 flowId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthFlow newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012PppOamFlowObjectInit(newFlow, flowId, module);
    }

uint32 Tha60210012EthFlowOamIngressAllVlansGet(AtEthFlow self, tAtEthVlanDesc *descs)
    {
    uint16 idx;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 igVlanNum = AtEthFlowIngressNumVlansGet(self);

    if (descs == NULL)
        return igVlanNum;

    for (idx = 0; idx < igVlanNum; idx++)
        {
        tAtEthVlanDesc vlanDesc;

        AtEthFlowIngressVlanAtIndex(self, idx, &vlanDesc);
        mMethodsGet(osal)->MemCpy(osal, &descs[idx], &vlanDesc, sizeof(tAtEthVlanDesc));
        }

    return igVlanNum;
    }

eBool Tha60210012EthFlowOamIngressVlanIsValid(const tAtEthVlanDesc *vlan)
    {
    if (AtEthVlanDescIsValid(vlan) == cAtFalse)
        return cAtFalse;

    if (vlan->numberOfVlans != 1)
        return cAtFalse;

    return cAtTrue;
    }

eBool Tha60210012EthFlowOamVlanIsSame(const tAtEthVlanDesc *desc1, const tAtEthVlanDesc *desc2)
    {
    uint8 i = 0;

    if (desc1->ethPortId != desc2->ethPortId)
        return cAtFalse;
    if (desc1->numberOfVlans != desc2->numberOfVlans)
        return cAtFalse;

    for (i = 0; i < cAtMaxNumVlanTag; i++)
        {
        if (desc1->vlans[i].cfi != desc2->vlans[i].cfi)
            return cAtFalse;
        if (desc1->vlans[i].priority != desc2->vlans[i].priority)
            return cAtFalse;
        if (desc1->vlans[i].vlanId != desc2->vlans[i].vlanId)
            return cAtFalse;
        }

    return cAtTrue;
    }

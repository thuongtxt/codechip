/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210012OamFlowManager.h
 * 
 * Created Date: Aug 24, 2016
 *
 * Description : Oam Flow manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012OAMFLOWMANAGER_H_
#define _THA60210012OAMFLOWMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthFlow Tha60210012PppOamFlowNew(uint32 flowId, AtModuleEth module);
AtEthFlow Tha60210012MpOamFlowNew(uint32 flowId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012OAMFLOWMANAGER_H_ */


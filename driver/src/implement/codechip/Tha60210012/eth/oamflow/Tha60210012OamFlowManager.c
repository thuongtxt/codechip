/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210012OamFlowManager.c
 *
 * Created Date: Aug 24, 2016
 *
 * Description : OAM Manger
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "../../../../default/eth/oamflow/ThaOamFlowManagerInternal.h"
#include "Tha60210012OamFlowManager.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012OamFlowManager
    {
    tThaOamFlowManagerDefault super;
    }tTha60210012OamFlowManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaOamFlowManagerMethods m_ThaOamFlowManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthFlow OamFlowObjectCreate(ThaOamFlowManager self, AtHdlcLink link)
    {
    return Tha60210012PppOamFlowNew(AtChannelIdGet((AtChannel)link), ThaOamFlowManagerModuleGet(self));
    }

static AtEthFlow OamMpFlowObjectCreate(ThaOamFlowManager self, AtHdlcBundle bundle)
    {
    return Tha60210012MpOamFlowNew(AtChannelIdGet((AtChannel)bundle), ThaOamFlowManagerModuleGet(self));
    }

static void OverrideThaOamFlowManager(ThaOamFlowManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaOamFlowManagerOverride, mMethodsGet(self), sizeof(m_ThaOamFlowManagerOverride));

        mMethodOverride(m_ThaOamFlowManagerOverride, OamFlowObjectCreate);
        mMethodOverride(m_ThaOamFlowManagerOverride, OamMpFlowObjectCreate);
        }

    mMethodsSet(self, &m_ThaOamFlowManagerOverride);
    }

static void Override(ThaOamFlowManager self)
    {
    OverrideThaOamFlowManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012OamFlowManager);
    }

static ThaOamFlowManager ObjectInit(ThaOamFlowManager self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaOamFlowManagerDefaultObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaOamFlowManager Tha60210012OamFlowManagerNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaOamFlowManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newManager, ethModule);
    }

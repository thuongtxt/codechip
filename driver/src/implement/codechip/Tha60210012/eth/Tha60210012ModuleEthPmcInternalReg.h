/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210012ModuleEthPmcInternalReg.h
 *
 * Created Date: Jul 22, 2016
 *
 * Description : PMC Internal Counters
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : Ethernet Full Counters
Reg Addr   : 0x20000-0x3FFFF
Reg Formula: 0x20000+$cnt_type*8192+$r2c*4096+$cnt_id
    Where  :
           + $cnt_type(0-15): counter type
           + $r2c(0-1): 1 for read to clear
           + $cnt_id(0-4095): Ethernet Flow ID
Reg Desc   :
Full 4096 Ethernet Flow Counters,support the following:
- cnt_type = 4'd0 : Ethernet Egress Packet Counter
- cnt_type = 4'd1 : Ethernet Egress Byte Counter
- cnt_type = 4'd2 : Ethernet Egress Fragment Counter
- cnt_type = 4'd3 : Ethernet Egress Discarded Packet Counter
- cnt_type = 4'd4 : Ethernet Egress Discarded Byte Counter
- cnt_type = 4'd5 : Ethernet Egress BECN Packet Counter
- cnt_type = 4'd6 : Ethernet Egress FECN Packet Counter
- cnt_type = 4'd7 : Ethernet Egress DE Packet Counter
- cnt_type = 4'd8 : Ethernet Igress Packet Counter
- cnt_type = 4'd9 : Ethernet Igress Byte Counter
- cnt_type = 4'd10: Ethernet Igress Fragment Counter
- cnt_type = 4'd11: Ethernet Igress Discarded Packet Counter
- cnt_type = 4'd12: Ethernet Igress Discarded Byte Counter
- cnt_type = 4'd13: Ethernet Igress BECN Packet Counter
- cnt_type = 4'd14: Ethernet Igress FECN Packet Counter
- cnt_type = 4'd15: Ethernet Igress DE Packet Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_ethfulltypcnt_pen_Base                                                                 0x20000
#define cAf6Reg_ethfulltypcnt_pen(cnttype, r2c, cntid)                (0x20000+(cnttype)*8192+(r2c)*4096+(cntid))
#define cAf6Reg_ethfulltypcnt_pen_WidthVal                                                                  32
#define cAf6Reg_ethfulltypcnt_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: ETH_CNT
BitField Type: RO
BitField Desc: Ethernet Counters
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ethfulltypcnt_pen_ETH_CNT_Bit_Start                                                             0
#define cAf6_ethfulltypcnt_pen_ETH_CNT_Bit_End                                                              31
#define cAf6_ethfulltypcnt_pen_ETH_CNT_Mask                                                           cBit31_0
#define cAf6_ethfulltypcnt_pen_ETH_CNT_Shift                                                                 0
#define cAf6_ethfulltypcnt_pen_ETH_CNT_MaxVal                                                       0xffffffff
#define cAf6_ethfulltypcnt_pen_ETH_CNT_MinVal                                                              0x0
#define cAf6_ethfulltypcnt_pen_ETH_CNT_RstVal                                                             0x00

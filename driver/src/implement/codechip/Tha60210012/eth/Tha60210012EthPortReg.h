/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210012EthPortReg.h
 *
 * Created Date: Jul 21, 2016
 *
 * Description : Register XFI serdes
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#define cEthPortSerdesSelectReg          0xF00040
#define cThaReg_XFI_Mac0_Sel_Mask        cBit5_4
#define cThaReg_XFI_Mac0_Sel_Shift       4

#define cEthPortSerdesEnableReg                    0xF00041
#define cThaReg_XFI_Mac0_Tx_Enable_Mask(serdesId)  (cBit0 << (serdesId))
#define cThaReg_XFI_Mac0_Tx_Enable_Shift(serdesId) (0 + (serdesId))

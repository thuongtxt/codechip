/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210012EthFlow.c
 *
 * Created Date: Jun 13, 2016
 *
 * Description : 60210012 Ethernet flow
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/eth/ThaEthFlowInternal.h"
#include "../../../default/pwe/ThaModulePweInternal.h"
#include "Tha60210012EthFlow.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210012EthFlow*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012EthFlow
    {
    tThaEthFlow super;
    uint8 hwServiceType;
    AtResequenceEngine engine;
    }tTha60210012EthFlow;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtEthFlowMethods m_AtEthFlowOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtEthFlowMethods *m_AtEthFlowMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePwe ModulePwe(AtEthFlow self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static eAtRet PtchInsertionModeSet(AtEthFlow self, eAtPtchMode insertMode)
    {
    return ThaModulePweEthFlowPtchInsertionModeSet(ModulePwe(self), self, insertMode);
    }

static eAtPtchMode PtchInsertionModeGet(AtEthFlow self)
    {
    return ThaModulePweEthFlowPtchInsertionModeGet(ModulePwe(self), self);
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    AtUnused(self);
    AtUnused(pAllCounters);
    return cAtErrorNotImplemented;
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    AtUnused(self);
    AtUnused(pAllCounters);
    return cAtErrorNotImplemented;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    ThaEthFlowController controller = ThaEthFlowControllerGet((ThaEthFlow)self);
    if (controller)
        return mMethodsGet(controller)->CounterGet(controller, (AtEthFlow)self, counterType);

    return 0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    ThaEthFlowController controller = ThaEthFlowControllerGet((ThaEthFlow)self);
    if (controller)
        return mMethodsGet(controller)->CounterClear(controller, (AtEthFlow)self, counterType);

    return 0;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cAtEthFlowCounterTxBytes                   :return cAtTrue;
        case cAtEthFlowCounterTxPackets                 :return cAtTrue;
        case cAtEthFlowCounterTxFragmentPackets         :return cAtTrue;
        case cAtEthFlowCounterTxBECNPackets             :return cAtTrue;
        case cAtEthFlowCounterTxFECNPackets             :return cAtTrue;
        case cAtEthFlowCounterTxDEPackets               :return cAtTrue;
        case cAtEthFlowCounterTxWindowViolationPackets  :return cAtTrue;
        case cAtEthFlowCounterTxNullFragmentPackets     :return cAtTrue;
        case cAtEthFlowCounterTxDiscardPackets          :return cAtTrue;
        case cAtEthFlowCounterTxDiscardBytes            :return cAtTrue;
        case cAtEthFlowCounterTxTimeOuts                :return cAtTrue;
        case cAtEthFlowCounterRxPackets                 :return cAtTrue;
        case cAtEthFlowCounterRxBytes                   :return cAtTrue;
        case cAtEthFlowCounterRxFragmentPackets         :return cAtTrue;
        case cAtEthFlowCounterRxBECNPackets             :return cAtTrue;
        case cAtEthFlowCounterRxFECNPackets             :return cAtTrue;
        case cAtEthFlowCounterRxDEPackets               :return cAtTrue;
        case cAtEthFlowCounterRxNullFragmentPackets     :return cAtTrue;
        case cAtEthFlowCounterRxDiscardPackets          :return cAtTrue;
        case cAtEthFlowCounterRxDiscardBytes            :return cAtTrue;

        case cAtEthFlowCounterTxGoodPackets             :return cAtFalse;
        case cAtEthFlowCounterTxGoodBytes               :return cAtFalse;
        case cAtEthFlowCounterTxGoodFragmentPackets     :return cAtFalse;
        case cAtEthFlowCounterRxGoodFragmentPackets     :return cAtFalse;
        case cAtEthFlowCounterTxMRRUExceedPackets       :return cAtFalse;
        case cAtEthFlowCounterTxDiscardFragmentPackets  :return cAtFalse;
        case cAtEthFlowCounterRxGoodPackets             :return cAtFalse;
        case cAtEthFlowCounterRxGoodBytes               :return cAtFalse;
        default                                         :return cAtFalse;
        }
    }

static eAtRet PtchServiceEnable(AtEthFlow self, eBool enable)
    {
    return ThaModulePweEthFlowPtchServiceEnable(ModulePwe(self), self, enable);
    }

static eBool PtchServiceIsEnabled(AtEthFlow self)
    {
    return ThaModulePweEthFlowPtchServiceIsEnabled(ModulePwe(self), self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210012EthFlow * object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(hwServiceType);
    mEncodeObjectDescription(engine);
    }

static uint32 HwIdGet(AtChannel self)
    {
    ThaEthFlowController controller = ThaEthFlowControllerGet((ThaEthFlow)self);
    if (controller)
        return mMethodsGet(controller)->HwFlowId(controller, (AtEthFlow)self);

    return ((ThaEthFlow)self)->hwFlowId;
    }

static eAtModuleEthRet EgressVlanAdd(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    if (AtEthFlowEgressNumVlansGet(self) > 0)
        return cAtErrorInvlParm;

    return m_AtEthFlowMethods->EgressVlanAdd(self, desc);
    }

static void OverrideAtObject(AtEthFlow self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtEthFlow(AtEthFlow self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthFlowMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthFlowOverride, mMethodsGet(self), sizeof(m_AtEthFlowOverride));

        mMethodOverride(m_AtEthFlowOverride, PtchInsertionModeSet);
        mMethodOverride(m_AtEthFlowOverride, PtchInsertionModeGet);
        mMethodOverride(m_AtEthFlowOverride, PtchServiceEnable);
        mMethodOverride(m_AtEthFlowOverride, PtchServiceIsEnabled);
        mMethodOverride(m_AtEthFlowOverride, EgressVlanAdd);
        }

    mMethodsSet(self, &m_AtEthFlowOverride);
    }

static void OverrideAtChannel(AtEthFlow self)
    {
    AtChannel channel = (AtChannel)self;
    
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthFlow self)
    {
    OverrideAtObject(self);
    OverrideAtEthFlow(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012EthFlow);
    }

static AtEthFlow ObjectInit(AtEthFlow self, uint32 flowId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthFlowObjectInit(self, flowId, module) == NULL)
        return NULL;

    /* Setup class */
     Override(self);
     m_methodsInit = 1;

    return self;
    }

AtEthFlow Tha60210012EthFlowNew(uint32 flowId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthFlow newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlow, flowId, module);
    }

void Tha60210012EthFlowServiceTypeCache(AtEthFlow self, uint8 serviceType)
    {
    if (self)
        mThis(self)->hwServiceType = serviceType;
    }

uint8 Tha60210012EthFlowCachedServiceType(AtEthFlow self)
    {
    if (self)
        return mThis(self)->hwServiceType;

    return 0;
    }

void Tha60210012EthFlowResequenceEngineSet(AtEthFlow self, AtResequenceEngine engine)
    {
    if (self)
        mThis(self)->engine = engine;
    }

AtResequenceEngine Tha60210012EthFlowResequenceEngineGet(AtEthFlow self)
    {
    if (self)
        return mThis(self)->engine;
    return NULL;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210012ModuleEth.h
 * 
 * Created Date: Sep 22, 2015
 *
 * Description : ETH module of Tha60210012 product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEETH_H_
#define _THA60210012MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h"
#include "AtPtchService.h"
#include "../../../default/eth/ThaModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cStartXauiSerdesId 4
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleEth * Tha60210012ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60210012ModuleEthBaseAddress(Tha60210012ModuleEth self);
uint32 Tha60210012ModuleEthBaseAddressXGbe(Tha60210012ModuleEth self);
uint32 Tha60210012ModuleEthBaseAddressTxBuf(Tha60210012ModuleEth self);
uint32 Tha60210012ModuleEthBaseAddressRxBuf(Tha60210012ModuleEth self);
uint16 Tha60210012ModuleEthLongRegisterTxBufRead(Tha60210012ModuleEth self, uint32 address, uint32 *dataBuffer, uint16 bufferLen);
uint16 Tha60210012ModuleEthLongRegisterTxBufWrite(Tha60210012ModuleEth self, uint32 address, uint32 *dataBuffer, uint16 bufferLen);

AtEthPort Tha60210012EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60210012EXauiNew(uint8 portId, AtModuleEth module);
eBool Tha60210012ModuleEthReduceTo4Serdes(AtModuleEth self);

eAtRet Tha60210012ModuleEthPtchLookupEnable(AtModuleEth self, eBool enabled);
eBool Tha60210012ModuleEthPtchLookupIsEnabled(AtModuleEth self);
uint32 Tha60210012ModuleEthNumExauiGet(AtModuleEth self);
AtEthPort Tha60210012ModuleEthExauiGet(AtModuleEth self, uint32 exauiId);
AtPtchService Tha60210012ModuleEthPtchServiceGet(AtModuleEth self, eAtPtchServiceType serviceType);

eAtRet Tha60210012ModuleEthIngressChannelIdSet(AtModuleEth self, uint8 channelId);
eAtRet Tha60210012ModuleEthExauiGlobalCountersDebug(AtModuleEth self, eBool r2c);
eAtRet Tha60210012ModuleEthAllServicesRemove(ThaModuleEth self);

uint8 Tha60210012ModuleEthEXaui0PortIdGet(AtModuleEth self);
uint8 Tha60210012ModuleEthEXaui1PortIdGet(AtModuleEth self);

uint32 Tha60210012ModuleEthEXaui1BaseAddress(AtModuleEth self);
uint32 Tha60210012ModuleEthEXaui2BaseAddress(AtModuleEth self);
uint32 Tha60210012ModuleEthHwFlowCounterGet(ThaModuleEth self, uint32 hwFlowId, uint16 hwCounterType, eBool r2c);

eAtRet Tha60210012ModuleEthOamFlowEgressDestMacSet(AtModuleEth module, const uint8 *destMac);
eAtRet Tha60210012ModuleEthOamFlowEgressDestMacGet(AtModuleEth module, uint8* destMac);
eAtRet Tha60210012ModuleEthOamFlowEgressSrcMacSet(AtModuleEth module, const uint8* srcMac);
eAtRet Tha60210012ModuleEthOamFlowEgressSrcMacGet(AtModuleEth module, uint8* srcMac);
eAtRet Tha60210012ModuleEthOamFlowEgressEthTypeSet(AtModuleEth module, uint16 ethType);
uint16 Tha60210012ModuleEthOamFlowEgressEthTypeGet(AtModuleEth module);
eAtRet Tha60210012ModuleEthOamFlowEgressControlVlanSet(AtModuleEth module, const tAtVlan* vlan);
eAtRet Tha60210012ModuleEthOamFlowEgressControlVlanGet(AtModuleEth module, tAtVlan* vlan);
eAtRet Tha60210012ModuleEthOamFlowIngressControlVlanSet(AtModuleEth module, const tAtVlan* vlan);
eAtRet Tha60210012ModuleEthOamFlowIngressControlVlanGet(AtModuleEth module, tAtVlan* vlan);
eAtRet Tha60210012ModuleEthOamFlowEgressCircuitVlanTPIDSet(AtModuleEth module, uint16 tpid);
uint16 Tha60210012ModuleEthOamFlowEgressCircuitVlanTPIDGet(AtModuleEth module);

uint32 Tha60210012ModuleEthFlowLookupBaseAddress(void);
uint32 Tha60210012ModuleEthPMCInternalBaseAddress(AtModuleEth self);

eAtRet Tha60210012ModuleEthIngressVlanCircuitUse(ThaModuleEth self, uint32 vlanId);
eAtRet Tha60210012ModuleEthIngressVlanCircuitUnUse(ThaModuleEth self, uint32 vlanId);
eBool  Tha60210012ModuleEthIngressVlanCircuitIsInused(ThaModuleEth self, uint32 vlanId);
void Tha60210012ModuleEthEXauiActive(ThaModuleEth self, eBool enable);

eBool Tha60210012NewMaxMinPacketSizeSupported(Tha60210012ModuleEth self);
eBool Tha60210012ModuleEthExternalPmcCounterIsSupported(Tha60210012ModuleEth self);
eBool Tha60210012ModuleEthXauiSerdesTuningIsSupported(AtModuleEth self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEETH_H_ */


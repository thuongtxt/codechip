/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210012EXaui.c
 *
 * Created Date: May 10, 2016
 *
 * Description : eXAUI
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210051/eth/Tha60210051ModuleEthInternal.h"
#include "../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "../pwe/Tha60210012ModulePwe.h"
#include "../cla/Tha60210012ModuleClaReg.h"
#include "../pwe/Tha60210012ModulePweReg.h"
#include "../cla/Tha60210012ModuleCla.h"
#include "commacro.h"
#include "Tha60210012ModuleEth.h"
#include "Tha60210012EXaui.h"
#include "Tha60210012EXauiReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cTha60210012ExauiChnMax 256
#define cTha60210012EthEXaui0BaseAddress 0xA80000
#define cTha60210012EthEXaui1BaseAddress 0xA90000

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210012EXaui *)self)
#define mPrbsBandwidthStepIdxValidCheck(idx) ((idx > 7)? 0:1)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012EXaui
    {
    tTha60210051EthPort super;

    uint16 expectedVlanCache[cTha60210012ExauiChnMax];
    }tTha60210012EXaui;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtEthPortMethods m_AtEthPortOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods  = NULL;
static const tAtChannelMethods      *m_AtChannelMethods = NULL;
static const tAtEthPortMethods      *m_AtEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ChannelIsValid(uint32 channelId)
    {
    if (channelId < cTha60210012ExauiChnMax)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 PweBaseAddress(void)
    {
    return Tha60210011ModulePweBaseAddress();
    }

static uint32 EXaui1BaseAddress(AtEthPort self)
    {
    AtModule module = AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleEth);
    return Tha60210012ModuleEthEXaui1BaseAddress((AtModuleEth)module);
    }

static eBool PweInsertEXauiIsSupported(AtEthPort self)
    {
    AtModule module = AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePwe);
    return Tha60210012ModulePweInsertEXauiIsSupported(module);
    }

static eAtRet EXauiIngressVlanSet(AtEthPort self, uint32 channelId, const tAtVlan *vlan)
    {
    uint32 regVal, regAddr;
    AtModuleEth module = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint32 portId = AtChannelIdGet((AtChannel)self);

    if (portId != Tha60210012ModuleEthEXaui1PortIdGet(module))
        return cAtErrorModeNotSupport;

    if (PweInsertEXauiIsSupported(self))
        regAddr = cAf6Reg_TxEth_VlanIns(channelId) + PweBaseAddress();
    else
        regAddr = cAf6Reg_cfg3_pen(channelId) + EXaui1BaseAddress(self);

    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
	mRegFieldSet(regVal, cAf6_cfg3_pen_EtheType_, vlan->tpid);
    mRegFieldSet(regVal, cAf6_cfg3_pen_EtherPri_, vlan->priority);
    mRegFieldSet(regVal, cAf6_cfg3_pen_EtherCFI_, vlan->cfi);
    mRegFieldSet(regVal, cAf6_cfg3_pen_EtherVLANID_, vlan->vlanId);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet EXauiIngressVlanGet(AtEthPort self, uint32 channelId, tAtVlan *vlan)
    {
    uint32 regVal, regAddr;
    AtModule module = AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleEth);
    uint32 portId = AtChannelIdGet((AtChannel)self);

    if (portId != Tha60210012ModuleEthEXaui1PortIdGet((AtModuleEth)module))
        return cAtErrorModeNotSupport;

    if (PweInsertEXauiIsSupported(self))
        regAddr = cAf6Reg_TxEth_VlanIns(channelId) + PweBaseAddress();
    else
        regAddr = cAf6Reg_cfg3_pen(channelId) + EXaui1BaseAddress(self);

    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    vlan->tpid     = (uint16)mRegField(regVal, cAf6_cfg3_pen_EtheType_);
    vlan->priority = (uint8)mRegField(regVal, cAf6_cfg3_pen_EtherPri_);
    vlan->cfi      = (uint8)mRegField(regVal, cAf6_cfg3_pen_EtherCFI_);
    vlan->vlanId   = (uint16)mRegField(regVal, cAf6_cfg3_pen_EtherVLANID_);

    return cAtOk;
    }

static uint32 ClaBaseAddress(AtEthPort self)
    {
    AtModule cla = AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleCla);
    return Tha60210011ModuleClaBaseAddress((ThaModuleCla)cla);
    }

static eAtRet EgressExpectedVlanIdSet(AtEthPort self, uint32 channelId, uint16 expectedVlanId)
    {
    uint32 regAddr = cAf6Reg_cla_tpch_eos_channel((uint32)expectedVlanId) + ClaBaseAddress(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    AtModule module = AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleEth);
    uint32 portId = AtChannelIdGet((AtChannel)self);

    if (portId != Tha60210012ModuleEthEXaui1PortIdGet((AtModuleEth)module))
        return cAtErrorModeNotSupport;

    mRegFieldSet(regVal, cAf6_cla_tpch_eos_channel_CLAPTCHChlEoS_, channelId);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    /* Cache this to avoid scanning when get */
    mThis(self)->expectedVlanCache[channelId] = expectedVlanId;

    return cAtOk;
    }

static uint16 EgressExpectedVlanIdGet(AtEthPort self, uint32 channelId)
    {
    uint16 vlanId;
    AtModule module = AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleEth);
    uint32 portId = AtChannelIdGet((AtChannel)self);

    if (portId != Tha60210012ModuleEthEXaui1PortIdGet((AtModuleEth)module))
        return cInvalidUint16;

    if(mOutOfRange(channelId, 0, cTha60210012ExauiChnMax-1))
        return 0;

    vlanId = mThis(self)->expectedVlanCache[channelId];

    return vlanId;
    }

static eAtRet ChannelIdCheck(AtEthPort self, uint32 channelId)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (!ChannelIsValid(channelId))
        return cAtErrorOutOfRangParm;

    return cAtOk;
    }

static eAtModuleEthRet SourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet SourceMacAddressGet(AtEthPort port, uint8 *address)
    {
    AtUnused(address);
    AtUnused(port);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet MacCheckingEnable(AtEthPort port, eBool enable)
    {
    AtUnused(port);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool MacCheckingIsEnabled(AtEthPort port)
    {
    AtUnused(port);
    return cAtFalse;
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed10G) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed10G) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortSpeed10G;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    AtUnused(self);
    return (interface == cAtEthPortInterfaceXAUI) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortInterfaceXAUI;
    }

static eAtEthPortInterface DefaultInterface(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortInterfaceXAUI;
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtModuleEthRet DuplexModeSet(AtEthPort self, eAtEthPortDuplexMode duplexMode)
    {
    AtUnused(self);
    return (duplexMode == cAtEthPortWorkingModeFullDuplex) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortDuplexMode DuplexModeGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortWorkingModeFullDuplex;
    }

static eAtModuleEthRet TxIpgSet(AtEthPort self, uint8 txIpg)
    {
    AtUnused(self);
    return (txIpg == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 TxIpgGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEthRet RxIpgSet(AtEthPort self, uint8 rxIpg)
    {
    AtUnused(self);
    return (rxIpg == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 RxIpgGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEthRet AutoNegEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool AutoNegIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool AutoNegIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtEthPortAutoNegState AutoNegStateGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEthRet Switch(AtEthPort self, AtEthPort toPort)
    {
    AtUnused(toPort);
    AtUnused(self);
    return cAtError;
    }

static AtEthPort SwitchedPortGet(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtModuleEthRet SwitchRelease(AtEthPort self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet Bridge(AtEthPort self, AtEthPort toPort)
    {
    AtUnused(toPort);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static AtEthPort BridgedPortGet(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtModuleEthRet BridgeRelease(AtEthPort self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }


static uint32 ProvisionedBandwidthInKbpsGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 RunningBandwidthInKbpsGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEthRet IpV4AddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV4AddressGet(AtEthPort self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV6AddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV6AddressGet(AtEthPort self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return serdes ? cAtErrorModeNotSupport : cAtOk;
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return cAtErrorModeNotSupport;
    }

static eBool TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return cAtFalse;
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static AtErrorGenerator TxErrorGeneratorGet(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static AtErrorGenerator RxErrorGeneratorGet(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtRet Init(AtChannel self)
    {
    return Tha60210012EthEXauiPrbsEnable((AtEthPort)self, cAtFalse);
    }

static uint8 SourceMonitorSw2Hw(eTha60210012Source source)
    {
    if (source == cTha60210012SourceEXaui) return 0x1;
    if (source == cTha60210012SourceXFI)   return 0x0;

    /* Invalid */
    return 0xFF;
    }

static eTha60210012Source SourceMonitorHw2Sw(uint8 source)
    {
    if (source == 0x1) return cTha60210012SourceEXaui;
    if (source == 0x0) return cTha60210012SourceXFI;

    /* Invalid */
    return cTha60210012SourceInvalid;
    }

static uint32 PrbsCounterGet(AtEthPort self, uint32 counterType, eBool r2c)
    {
    uint32 offset = ((r2c) ? 0x800 : 0x0) + Tha60210012EthEXauiPortBaseAddress(self);

    if (counterType == cTha60210012EthEXauiPrbsCounterRxPackets)
        return mChannelHwRead(self, cAf6Reg_diag_rx_pkt_cnt_ro_Base + offset, cAtModuleEth);

    if (counterType == cTha60210012EthEXauiPrbsCounterRxBytes)
        return mChannelHwRead(self, cAf6Reg_diag_rx_cnt_byte_ro_Base + offset, cAtModuleEth);

    return 0;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (counterType == cAtEthPortCounterRxErrFcsPackets)
        return cAtTrue;

    if ((counterType == cAtEthPortCounterTxBrdCastPackets) ||
        (counterType == cAtEthPortCounterTxMultCastPackets)||
        (counterType == cAtEthPortCounterTxUniCastPackets) ||
        (counterType == cAtEthPortCounterRxUniCastPackets) ||
        (counterType == cAtEthPortCounterRxBrdCastPackets) ||
        (counterType == cAtEthPortCounterRxMultCastPackets)||
        (counterType == cAtEthPortCounterRxMultCastPackets))
        return cAtFalse;

    return m_AtChannelMethods->CounterIsSupported(self, counterType);
    }

static eAtRet EXauiPrbsMonitorSourceSelect(AtEthPort self, eTha60210012Source source)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_ctr_Base + Tha60210012EthEXauiPortBaseAddress(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);

    mRegFieldSet(regValue, cAf6_eth10g_diag_ctr_Eth10GDiagGenSrc_, SourceMonitorSw2Hw(source));
    mRegFieldSet(regValue, cAf6_eth10g_diag_ctr_Eth10GDiagMonSrc_, SourceMonitorSw2Hw(source));
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEth);

    return cAtOk;
    }

static eTha60210012Source EXauiPrbsMonitorSourceSelectGet(AtEthPort self)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_ctr_Base + Tha60210012EthEXauiPortBaseAddress(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);
    return SourceMonitorHw2Sw((uint8)mRegField(regValue, cAf6_eth10g_diag_ctr_Eth10GDiagMonSrc_));
    }

static eAtRet EXauiPrbsSingleGenerate(AtEthPort self, uint32 numPacket)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_num_pkt_ctr_Base + Tha60210012EthEXauiPortBaseAddress(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);

    mRegFieldSet(regValue, cAf6_eth10g_diag_num_pkt_ctr_Eth10GDiagNumPkt_, numPacket);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEth);

    return cAtOk;
    }

static uint32 EXauiPrbsSingleGenerateGet(AtEthPort self)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_num_pkt_ctr_Base + Tha60210012EthEXauiPortBaseAddress(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);
    return mRegField(regValue, cAf6_eth10g_diag_num_pkt_ctr_Eth10GDiagNumPkt_);
    }

static eAtRet EXauiPrbsContinousGenerate(AtEthPort self)
    {
    static const uint32 cNumPktContinuousGenerate = 0xFFFF;
    uint32 regAddress = cAf6Reg_eth10g_diag_num_pkt_ctr_Base + Tha60210012EthEXauiPortBaseAddress(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);

    mRegFieldSet(regValue, cAf6_eth10g_diag_num_pkt_ctr_Eth10GDiagNumPkt_, cNumPktContinuousGenerate);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEth);

    return cAtOk;
    }

static eAtRet EXauiPrbsEnable(AtEthPort self, eBool enable)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_ctr_Base + Tha60210012EthEXauiPortBaseAddress(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);

    mRegFieldSet(regValue, cAf6_eth10g_diag_ctr_Eth10GDiagGenEn_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEth);

    return cAtOk;
    }

static eBool EXauiPrbsIsEnabled(AtEthPort self)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_ctr_Base + Tha60210012EthEXauiPortBaseAddress(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);
    return mRegField(regValue, cAf6_eth10g_diag_ctr_Eth10GDiagGenEn_) ? cAtTrue : cAtFalse;
    }

static eAtRet EXauiPrbsForceError(AtEthPort self, eBool force)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_ctr_Base + Tha60210012EthEXauiPortBaseAddress(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);

    mRegFieldSet(regValue, cAf6_eth10g_diag_ctr_Eth10GDiagForceErr_, mBoolToBin(force));
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEth);

    return cAtOk;
    }

static eBool EXauiPrbsIsForceError(AtEthPort self)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_ctr_Base + Tha60210012EthEXauiPortBaseAddress((AtEthPort)self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);
    return mRegField(regValue, cAf6_eth10g_diag_ctr_Eth10GDiagForceErr_) ? cAtTrue : cAtFalse;
    }

static uint32 EXauiPrbsErrorGet(AtEthPort self)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_err_stk_Base + Tha60210012EthEXauiPortBaseAddress(self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);

    /* Clear sticky */
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEth);
    if (mRegField(regValue, cAf6_eth10g_diag_err_stk_Eth10GDiagErrStk_))
        return cAtPrbsEngineAlarmTypeError;

    return 0;
    }

static eAtRet ExauiPrbsLenModeSet(AtEthPort self, uint32 lenMode)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_ctr_Base + Tha60210012EthEXauiPortBaseAddress((AtEthPort)self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);
    mRegFieldSet(regValue, cAf6_eth10g_diag_ctr_Eth10GDiagPktLenMode_, lenMode);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEth);
    return cAtOk;
    }

static uint32 ExauiPrbsLenModeGet(AtEthPort self)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_ctr_Base + Tha60210012EthEXauiPortBaseAddress((AtEthPort)self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);
    return mRegField(regValue, cAf6_eth10g_diag_ctr_Eth10GDiagPktLenMode_);
    }

static eAtRet ExauiPrbsMinLenSet(AtEthPort self, uint32 pktLen)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_min_len_ctr_Base + Tha60210012EthEXauiPortBaseAddress((AtEthPort)self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);
    mRegFieldSet(regValue, cAf6_eth10g_diag_min_len_ctr_Eth10GDiagMinLen_, pktLen);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEth);
    return cAtOk;
    }

static uint32 ExauiPrbsMinLenGet(AtEthPort self)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_min_len_ctr_Base + Tha60210012EthEXauiPortBaseAddress((AtEthPort)self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);
    return mRegField(regValue, cAf6_eth10g_diag_min_len_ctr_Eth10GDiagMinLen_);
    }

static eAtRet ExauiPrbsMaxLenSet(AtEthPort self, uint32 pktLen)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_max_len_ctr_Base + Tha60210012EthEXauiPortBaseAddress((AtEthPort)self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);
    mRegFieldSet(regValue, cAf6_eth10g_diag_max_len_ctr_Eth10GDiagMaxLen_, pktLen);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEth);
    return cAtOk;
    }

static uint32 ExauiPrbsMaxLenGet(AtEthPort self)
    {
    uint32 regAddress = cAf6Reg_eth10g_diag_max_len_ctr_Base + Tha60210012EthEXauiPortBaseAddress((AtEthPort)self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);
    return mRegField(regValue, cAf6_eth10g_diag_max_len_ctr_Eth10GDiagMaxLen_);
    }

static eAtRet ExauiPrbsBandwidthSet(AtEthPort self, uint32 banwidth)
    {
    uint32 regAddress, regValue;

    if (!mPrbsBandwidthStepIdxValidCheck(banwidth))
        return cAtErrorOutOfRangParm;

    regAddress = cAf6Reg_diag_gen_bw_ctrl_Base + Tha60210012EthEXauiPortBaseAddress((AtEthPort)self);
    regValue = mChannelHwRead(self, regAddress, cAtModuleEth);
    mRegFieldSet(regValue, cAf6_diag_gen_bw_ctrl_DiagGenBwCtrl_, banwidth);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleEth);

    return cAtOk;
    }

static uint32 ExauiPrbsBandwidthGet(AtEthPort self)
    {
    uint32 regAddress = cAf6Reg_diag_gen_bw_ctrl_Base + Tha60210012EthEXauiPortBaseAddress((AtEthPort)self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleEth);
    return mRegField(regValue, cAf6_diag_gen_bw_ctrl_DiagGenBwCtrl_);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210012EXaui* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt16Array(expectedVlanCache, cTha60210012ExauiChnMax);
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, SourceMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, MacCheckingEnable);
        mMethodOverride(m_AtEthPortOverride, MacCheckingIsEnabled);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, DuplexModeSet);
        mMethodOverride(m_AtEthPortOverride, DuplexModeGet);
        mMethodOverride(m_AtEthPortOverride, TxIpgSet);
        mMethodOverride(m_AtEthPortOverride, TxIpgGet);
        mMethodOverride(m_AtEthPortOverride, RxIpgSet);
        mMethodOverride(m_AtEthPortOverride, RxIpgGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegEnable);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsEnabled);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsSupported);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, AutoNegStateGet);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, DefaultInterface);
        mMethodOverride(m_AtEthPortOverride, ProvisionedBandwidthInKbpsGet);
        mMethodOverride(m_AtEthPortOverride, RunningBandwidthInKbpsGet);
        mMethodOverride(m_AtEthPortOverride, Switch);
        mMethodOverride(m_AtEthPortOverride, SwitchedPortGet);
        mMethodOverride(m_AtEthPortOverride, SwitchRelease);
        mMethodOverride(m_AtEthPortOverride, Bridge);
        mMethodOverride(m_AtEthPortOverride, BridgedPortGet);
        mMethodOverride(m_AtEthPortOverride, BridgeRelease);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressGet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressGet);
        mMethodOverride(m_AtEthPortOverride, RxSerdesSelect);
        mMethodOverride(m_AtEthPortOverride, RxSelectedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesBridge);
        mMethodOverride(m_AtEthPortOverride, TxSerdesCanBridge);
        mMethodOverride(m_AtEthPortOverride, TxBridgedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxErrorGeneratorGet);
        mMethodOverride(m_AtEthPortOverride, RxErrorGeneratorGet);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012EXaui);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051EthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60210012EXauiNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

static eBool VlanIsValid( const tAtVlan *vlan)
    {
    /*
     * TPID     : 16 bits
     * Priority : 3 bits
     * CFI      : 1 bit
     * VlanId   : 12 bits
     */
    if (vlan->priority > cBit2_0)
        return cAtFalse;

    if (vlan->cfi > cBit0)
        return cAtFalse;

    if (vlan->vlanId > cBit11_0)
        return cAtFalse;

    return cAtTrue;
    }

eAtRet Tha60210012EXauiIngressVlanSet(AtEthPort self, uint32 channelId, const tAtVlan *vlan)
    {
    eAtRet ret = ChannelIdCheck(self, channelId);

    if (ret != cAtOk)
        return ret;

    if (vlan == NULL)
        return cAtErrorNullPointer;

    if(!VlanIsValid(vlan))
        return cAtErrorInvlParm;

    return EXauiIngressVlanSet(self, channelId, vlan);
    }

eAtRet Tha60210012EXauiIngressVlanGet(AtEthPort self, uint32 channelId, tAtVlan *vlan)
    {
    eAtRet ret = ChannelIdCheck(self, channelId);
    if (ret != cAtOk)
        return ret;

    if (vlan == NULL)
        return cAtErrorNullPointer;

    return EXauiIngressVlanGet(self, channelId, vlan);
    }

eAtRet Tha60210012EXauiEgressExpectedVlanIdSet(AtEthPort self, uint32 channelId, uint16 expectedVlanId)
    {
    eAtRet ret = ChannelIdCheck(self, channelId);
    if (ret != cAtOk)
        return ret;

    if (expectedVlanId > cBit11_0)
        return cAtErrorInvlParm;

    return EgressExpectedVlanIdSet(self, channelId, expectedVlanId);
    }

uint16 Tha60210012EXauiEgressExpectedVlanIdGet(AtEthPort self, uint32 channelId)
    {
    eAtRet ret = ChannelIdCheck(self, channelId);
    if (ret != cAtOk)
        return cInvalidUint16;
    return EgressExpectedVlanIdGet(self, channelId);
    }

uint32 Tha60210012EthEXauiPortBaseAddress(AtEthPort self)
    {
    if (AtChannelIdGet((AtChannel)self) == 1)
        return cTha60210012EthEXaui0BaseAddress;
    return cTha60210012EthEXaui1BaseAddress;
    }

eAtRet Tha60210012EthEXauiPrbsMonitorSourceSelect(AtEthPort self, eTha60210012Source source)
    {
    if (self)
        return EXauiPrbsMonitorSourceSelect(self, source);
    return cAtOk;
    }

eTha60210012Source Tha60210012EthEXauiPrbsMonitorSourceSelectGet(AtEthPort self)
    {
    if (self)
        return EXauiPrbsMonitorSourceSelectGet(self);
    return cTha60210012SourceInvalid;
    }

eAtRet Tha60210012EthEXauiPrbsSingleGenerate(AtEthPort self, uint32 numPacket)
    {
    if (self)
        return EXauiPrbsSingleGenerate(self, numPacket);
    return cAtErrorObjectNotExist;
    }

uint32 Tha60210012EthEXauiPrbsSingleGenerateGet(AtEthPort self)
    {
    if (self)
        return EXauiPrbsSingleGenerateGet(self);
    return 0;
    }

eAtRet Tha60210012EthEXauiPrbsContinousGenerate(AtEthPort self)
    {
    if (self)
        return EXauiPrbsContinousGenerate(self);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha60210012EthEXauiPrbsEnable(AtEthPort self, eBool enable)
    {
    if (self)
        return EXauiPrbsEnable(self, enable);
    return cAtErrorObjectNotExist;
    }

eBool Tha60210012EthEXauiPrbsIsEnabled(AtEthPort self)
    {
    if (self)
        return EXauiPrbsIsEnabled(self);
    return cAtFalse;
    }

eAtRet Tha60210012EthEXauiPrbsForceError(AtEthPort self, eBool force)
    {
    if (self)
        return EXauiPrbsForceError(self, force);
    return cAtErrorObjectNotExist;
    }

eBool Tha60210012EthEXauiPrbsIsForceError(AtEthPort self)
    {
    if (self)
        return EXauiPrbsIsForceError(self);
    return cAtFalse;
    }

uint32 Tha60210012EthEXauiPrbsErrorGet(AtEthPort self)
    {
    if (self)
        return EXauiPrbsErrorGet(self);
    return 0;
    }

eAtRet Tha60210012EthExauiPrbsLenModeSet(AtEthPort self, uint32 lenMode)
    {
    if (self)
        return ExauiPrbsLenModeSet(self, lenMode);
    return cAtErrorObjectNotExist;
    }

uint32 Tha60210012EthExauiPrbsLenModeGet(AtEthPort self)
    {
    if (self)
        return ExauiPrbsLenModeGet(self);
    return 0;
    }

eAtRet Tha60210012EthExauiPrbsMinLenSet(AtEthPort self, uint32 pktLen)
    {
    if (self)
        return ExauiPrbsMinLenSet(self, pktLen);
    return cAtErrorObjectNotExist;
    }
    
uint32 Tha60210012EthExauiPrbsMinLenGet(AtEthPort self)
    {
    if (self)
        return ExauiPrbsMinLenGet(self);
    return 0;
    }
    
eAtRet Tha60210012EthExauiPrbsMaxLenSet(AtEthPort self, uint32 pktLen)
    {
    if (self)
        return ExauiPrbsMaxLenSet(self, pktLen);
    return cAtErrorObjectNotExist;
    }

uint32 Tha60210012EthExauiPrbsMaxLenGet(AtEthPort self)
    {
    if (self)
        return ExauiPrbsMaxLenGet(self);
    return 0;
    }

eAtRet Tha60210012EthExauiPrbsBandwidthSet(AtEthPort self, uint32 bandwidth)
    {
    if (self)
        return ExauiPrbsBandwidthSet(self, bandwidth);
    return cAtErrorObjectNotExist;
    }

uint32 Tha60210012EthExauiPrbsBandwidthGet(AtEthPort self)
    {
    if (self)
        return ExauiPrbsBandwidthGet(self);
    return 0;
    }

uint32 Tha60210012EthEXauiPrbsCounterGet(AtEthPort self, uint32 counterType)
    {
    if (self)
        return PrbsCounterGet(self, counterType, cAtFalse);
    return 0;
    }

uint32 Tha60210012EthEXauiPrbsCounterClear(AtEthPort self, uint32 counterType)
    {
    if (self)
        return PrbsCounterGet(self, counterType, cAtTrue);
    return 0;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210021ModuleEthFlowLookupReg.h
 * 
 * Created Date: Apr 6, 2016
 *
 * Description : Register for flow lookup
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEETHFLOWLOOKUPREG_H_
#define _THA60210012MODULEETHFLOWLOOKUPREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#define cThaModuleEthFlowLookupBaseAddress 0x0A10000

/*------------------------------------------------------------------------------
Reg Name   : Access Data Holding
Reg Addr   : 0x00000
Reg Formula:
    Where  :
Reg Desc   :
Data Hold for configuration or status that is greater than 32-bit wide.

------------------------------------------------------------------------------*/
#define cAf6Reg_hold_pen_Base                                                                          0x00000
#define cAf6Reg_hold_pen                                                                               0x00000
#define cAf6Reg_hold_pen_WidthVal                                                                           32
#define cAf6Reg_hold_pen_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: Dat_Hold
BitField Type: R/W
BitField Desc:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_hold_pen_Dat_Hold_Bit_Start                                                                     0
#define cAf6_hold_pen_Dat_Hold_Bit_End                                                                      31
#define cAf6_hold_pen_Dat_Hold_Mask                                                                   cBit31_0
#define cAf6_hold_pen_Dat_Hold_Shift                                                                         0
#define cAf6_hold_pen_Dat_Hold_MaxVal                                                               0xffffffff
#define cAf6_hold_pen_Dat_Hold_MinVal                                                                      0x0
#define cAf6_hold_pen_Dat_Hold_RstVal                                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Lo-order HDLC LinkLookUp
Reg Addr   : 0x01000-0x01FFF
Reg Formula: 0x01000 + $dec_slice_id*1024 + $dec_channel_id
    Where  :
           + $dec_slice_id(0-3)
           + $dec_channel_id(0-1023)
Reg Desc   :
LookUp table: {dec_slice_id[1:0],dec_channel_id[9:0]} ---> link_id[11:0]

------------------------------------------------------------------------------*/
#define cAf6Reg_lolink_lup_table_Base                                                                  0x01000
#define cAf6Reg_lolink_lup_table(decsliceid, decchannelid)            (0x01000+(decsliceid)*1024+(decchannelid))
#define cAf6Reg_lolink_lup_table_WidthVal                                                                   32
#define cAf6Reg_lolink_lup_table_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: HDLC_Valid
BitField Type: R/W
BitField Desc: 0: LookUp Fail
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_lolink_lup_table_HDLC_Valid_Bit_Start                                                          12
#define cAf6_lolink_lup_table_HDLC_Valid_Bit_End                                                            12
#define cAf6_lolink_lup_table_HDLC_Valid_Mask                                                           cBit12
#define cAf6_lolink_lup_table_HDLC_Valid_Shift                                                              12
#define cAf6_lolink_lup_table_HDLC_Valid_MaxVal                                                            0x1
#define cAf6_lolink_lup_table_HDLC_Valid_MinVal                                                            0x0
#define cAf6_lolink_lup_table_HDLC_Valid_RstVal                                                            0x0

/*--------------------------------------
BitField Name: HDLC_LinkID
BitField Type: R/W
BitField Desc: Link 0-3071
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_lolink_lup_table_HDLC_LinkID_Bit_Start                                                          0
#define cAf6_lolink_lup_table_HDLC_LinkID_Bit_End                                                           11
#define cAf6_lolink_lup_table_HDLC_LinkID_Mask                                                        cBit11_0
#define cAf6_lolink_lup_table_HDLC_LinkID_Shift                                                              0
#define cAf6_lolink_lup_table_HDLC_LinkID_MaxVal                                                         0xfff
#define cAf6_lolink_lup_table_HDLC_LinkID_MinVal                                                           0x0
#define cAf6_lolink_lup_table_HDLC_LinkID_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Ho-order HDLC Link LookUp
Reg Addr   : 0x02000-0x0205F
Reg Formula: 0x02000 + $dec_slice_id*48 + $dec_channel_id
    Where  :
           + $dec_slice_id(0-1)
           + $dec_channel_id(0-47)
Reg Desc   :
LookUp table: {dec_slice_id[0],dec_channel_id[5:0]} ---> link_id[11:0]

------------------------------------------------------------------------------*/
#define cAf6Reg_holink_lup_table_Base                                                                  0x02000
#define cAf6Reg_holink_lup_table(decsliceid, decchannelid)            (0x02000+(decsliceid)*48+(decchannelid))
#define cAf6Reg_holink_lup_table_WidthVal                                                                   32
#define cAf6Reg_holink_lup_table_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: HDLC_Valid
BitField Type: R/W
BitField Desc: 0: LookUp Fail
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_holink_lup_table_HDLC_Valid_Bit_Start                                                          12
#define cAf6_holink_lup_table_HDLC_Valid_Bit_End                                                            12
#define cAf6_holink_lup_table_HDLC_Valid_Mask                                                           cBit12
#define cAf6_holink_lup_table_HDLC_Valid_Shift                                                              12
#define cAf6_holink_lup_table_HDLC_Valid_MaxVal                                                            0x1
#define cAf6_holink_lup_table_HDLC_Valid_MinVal                                                            0x0
#define cAf6_holink_lup_table_HDLC_Valid_RstVal                                                            0x0

/*--------------------------------------
BitField Name: HDLC_LinkID
BitField Type: R/W
BitField Desc: Link 0-3071
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_holink_lup_table_HDLC_LinkID_Bit_Start                                                          0
#define cAf6_holink_lup_table_HDLC_LinkID_Bit_End                                                           11
#define cAf6_holink_lup_table_HDLC_LinkID_Mask                                                        cBit11_0
#define cAf6_holink_lup_table_HDLC_LinkID_Shift                                                              0
#define cAf6_holink_lup_table_HDLC_LinkID_MaxVal                                                         0xfff
#define cAf6_holink_lup_table_HDLC_LinkID_MinVal                                                           0x0
#define cAf6_holink_lup_table_HDLC_LinkID_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : HDLC Bundle LookUp
Reg Addr   : 0x03000-0x03FFF
Reg Formula: 0x03000 + $link_id
    Where  :
           + $link_id(0-3071)
Reg Desc   :
LookUp table: link_id[11:0] -> bundle_id[8:0]

------------------------------------------------------------------------------*/
#define cAf6Reg_bundle_lup_table_Base                                                                  0x03000
#define cAf6Reg_bundle_lup_table(linkid)                                                    (0x03000+(linkid))
#define cAf6Reg_bundle_lup_table_WidthVal                                                                   32
#define cAf6Reg_bundle_lup_table_WriteMask                                                                 0x0

#define cAf6_bundle_lup_table_HDLC_Member_Id_Mask                                                        cBit20_16
#define cAf6_bundle_lup_table_HDLC_Member_Id_Shift                                                       16

/*--------------------------------------
BitField Name: HDLC_BundleVld
BitField Type: R/W
BitField Desc: 0: LookUp Fail
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_bundle_lup_table_HDLC_BundleVld_Bit_Start                                                       12
#define cAf6_bundle_lup_table_HDLC_BundleVld_Bit_End                                                         12
#define cAf6_bundle_lup_table_HDLC_BundleVld_Mask                                                        cBit12
#define cAf6_bundle_lup_table_HDLC_BundleVld_Shift                                                           12
#define cAf6_bundle_lup_table_HDLC_BundleVld_MaxVal                                                        0x1
#define cAf6_bundle_lup_table_HDLC_BundleVld_MinVal                                                        0x0
#define cAf6_bundle_lup_table_HDLC_BundleVld_RstVal                                                        0x0

/*--------------------------------------
BitField Name: HDLC_BundleID
BitField Type: R/W
BitField Desc: Bundle 0-511
BitField Bits: [08:00]
--------------------------------------*/
#define cAf6_bundle_lup_table_HDLC_BundleID_Bit_Start                                                        0
#define cAf6_bundle_lup_table_HDLC_BundleID_Bit_End                                                          8
#define cAf6_bundle_lup_table_HDLC_BundleID_Mask                                                       cBit8_0
#define cAf6_bundle_lup_table_HDLC_BundleID_Shift                                                            0
#define cAf6_bundle_lup_table_HDLC_BundleID_MaxVal                                                       0x1ff
#define cAf6_bundle_lup_table_HDLC_BundleID_MinVal                                                         0x0
#define cAf6_bundle_lup_table_HDLC_BundleID_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : PPP Link (Data) to Ethernet Flow LookUp Table
Reg Addr   : 0x04000-0x04BFF
Reg Formula: 0x04000 + $ppp_link_id
    Where  :
           + $ppp_link_id(0-3071)
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_pppdat_ethflow_table_Base                                                              0x04000
#define cAf6Reg_pppdat_ethflow_table(ppplinkid)                                          (0x04000+(ppplinkid))
#define cAf6Reg_pppdat_ethflow_table_WidthVal                                                               32
#define cAf6Reg_pppdat_ethflow_table_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: CtrlVlan_Enable
BitField Type: R/W
BitField Desc: 0: LookUp Fail
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_linkdat_ethflow_table_CtrlVlan_Enable_Bit_Start                                                28
#define cAf6_linkdat_ethflow_table_CtrlVlan_Enable_Bit_End                                                  28
#define cAf6_linkdat_ethflow_table_CtrlVlan_Enable_Mask                                                 cBit28
#define cAf6_linkdat_ethflow_table_CtrlVlan_Enable_Shift                                                    28
#define cAf6_linkdat_ethflow_table_CtrlVlan_Enable_MaxVal                                                  0x1
#define cAf6_linkdat_ethflow_table_CtrlVlan_Enable_MinVal                                                  0x0
#define cAf6_linkdat_ethflow_table_CtrlVlan_Enable_RstVal                                                  0x0

/*--------------------------------------
BitField Name: CtrlVlan_ID
BitField Type: R/W
BitField Desc: VLAN Control: 0-4095
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_linkdat_ethflow_table_CtrlVlan_ID_Bit_Start                                                    16
#define cAf6_linkdat_ethflow_table_CtrlVlan_ID_Bit_End                                                      27
#define cAf6_linkdat_ethflow_table_CtrlVlan_ID_Mask                                                  cBit27_16
#define cAf6_linkdat_ethflow_table_CtrlVlan_ID_Shift                                                        16
#define cAf6_linkdat_ethflow_table_CtrlVlan_ID_MaxVal                                                    0xfff
#define cAf6_linkdat_ethflow_table_CtrlVlan_ID_MinVal                                                      0x0
#define cAf6_linkdat_ethflow_table_CtrlVlan_ID_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PPP_Data_Enable
BitField Type: R/W
BitField Desc: 0: LookUp Fail
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_pppdat_ethflow_table_PPP_Data_Enable_Bit_Start                                                 12
#define cAf6_pppdat_ethflow_table_PPP_Data_Enable_Bit_End                                                   12
#define cAf6_pppdat_ethflow_table_PPP_Data_Enable_Mask                                                  cBit12
#define cAf6_pppdat_ethflow_table_PPP_Data_Enable_Shift                                                     12
#define cAf6_pppdat_ethflow_table_PPP_Data_Enable_MaxVal                                                   0x1
#define cAf6_pppdat_ethflow_table_PPP_Data_Enable_MinVal                                                   0x0
#define cAf6_pppdat_ethflow_table_PPP_Data_Enable_RstVal                                                   0x0

/*--------------------------------------
BitField Name: PPP_Data_EthFlowID
BitField Type: R/W
BitField Desc: Ethernet FLow: 0-4095
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_pppdat_ethflow_table_PPP_Data_EthFlowID_Bit_Start                                               0
#define cAf6_pppdat_ethflow_table_PPP_Data_EthFlowID_Bit_End                                                11
#define cAf6_pppdat_ethflow_table_PPP_Data_EthFlowID_Mask                                             cBit11_0
#define cAf6_pppdat_ethflow_table_PPP_Data_EthFlowID_Shift                                                   0
#define cAf6_pppdat_ethflow_table_PPP_Data_EthFlowID_MaxVal                                              0xfff
#define cAf6_pppdat_ethflow_table_PPP_Data_EthFlowID_MinVal                                                0x0
#define cAf6_pppdat_ethflow_table_PPP_Data_EthFlowID_RstVal                                                0x0

/*------------------------------------------------------------------------------
Reg Name   : Bundle Link (Data) to Ethernet Flow LookUp Table
Reg Addr   : 0x06000-0x061FF
Reg Formula: 0x06000 + $bundle_id
    Where  :
           + $bundle_id(0-511)
Reg Desc   :

------------------------------------------------------------------------------*/
#define cAf6Reg_bundledat_ethflow_table_Base                                                           0x06000
#define cAf6Reg_bundledat_ethflow_table(bundleid)                                         (0x06000+(bundleid))
#define cAf6Reg_bundledat_ethflow_table_WidthVal                                                            32
#define cAf6Reg_bundledat_ethflow_table_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: CtrlVlan_Enable
BitField Type: R/W
BitField Desc: 0: LookUp Fail
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_bundledat_ethflow_table_CtrlVlan_Enable_Bit_Start                                              28
#define cAf6_bundledat_ethflow_table_CtrlVlan_Enable_Bit_End                                                28
#define cAf6_bundledat_ethflow_table_CtrlVlan_Enable_Mask                                               cBit28
#define cAf6_bundledat_ethflow_table_CtrlVlan_Enable_Shift                                                  28
#define cAf6_bundledat_ethflow_table_CtrlVlan_Enable_MaxVal                                                0x1
#define cAf6_bundledat_ethflow_table_CtrlVlan_Enable_MinVal                                                0x0
#define cAf6_bundledat_ethflow_table_CtrlVlan_Enable_RstVal                                                0x0

/*--------------------------------------
BitField Name: CtrlVlan_ID
BitField Type: R/W
BitField Desc: VLAN Control: 0-4095
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_bundledat_ethflow_table_CtrlVlan_ID_Bit_Start                                                  16
#define cAf6_bundledat_ethflow_table_CtrlVlan_ID_Bit_End                                                    27
#define cAf6_bundledat_ethflow_table_CtrlVlan_ID_Mask                                                cBit27_16
#define cAf6_bundledat_ethflow_table_CtrlVlan_ID_Shift                                                      16
#define cAf6_bundledat_ethflow_table_CtrlVlan_ID_MaxVal                                                  0xfff
#define cAf6_bundledat_ethflow_table_CtrlVlan_ID_MinVal                                                    0x0
#define cAf6_bundledat_ethflow_table_CtrlVlan_ID_RstVal                                                    0x0

/*--------------------------------------
BitField Name: Bundle_Data_Enable
BitField Type: R/W
BitField Desc: 0: LookUp Fail
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_bundledat_ethflow_table_Bundle_Data_Enable_Bit_Start                                           12
#define cAf6_bundledat_ethflow_table_Bundle_Data_Enable_Bit_End                                             12
#define cAf6_bundledat_ethflow_table_Bundle_Data_Enable_Mask                                            cBit12
#define cAf6_bundledat_ethflow_table_Bundle_Data_Enable_Shift                                               12
#define cAf6_bundledat_ethflow_table_Bundle_Data_Enable_MaxVal                                             0x1
#define cAf6_bundledat_ethflow_table_Bundle_Data_Enable_MinVal                                             0x0
#define cAf6_bundledat_ethflow_table_Bundle_Data_Enable_RstVal                                             0x0

/*--------------------------------------
BitField Name: Bundle_Data_EthFlowID
BitField Type: R/W
BitField Desc: Ethernet FLow: 0-4095
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_bundledat_ethflow_table_Bundle_Data_EthFlowID_Bit_Start                                         0
#define cAf6_bundledat_ethflow_table_Bundle_Data_EthFlowID_Bit_End                                          11
#define cAf6_bundledat_ethflow_table_Bundle_Data_EthFlowID_Mask                                       cBit11_0
#define cAf6_bundledat_ethflow_table_Bundle_Data_EthFlowID_Shift                                             0
#define cAf6_bundledat_ethflow_table_Bundle_Data_EthFlowID_MaxVal                                        0xfff
#define cAf6_bundledat_ethflow_table_Bundle_Data_EthFlowID_MinVal                                          0x0
#define cAf6_bundledat_ethflow_table_Bundle_Data_EthFlowID_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : PPP Bundle Control Packet to Ethernet Flow LookUp Table
Reg Addr   : 0x07000-0x071FF
Reg Formula: 0x07000 + $bundle_id
    Where  :
           + $bundle_id(0-511)
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_bundlectrl_ethflow_table_Base                                                          0x07000
#define cAf6Reg_bundlectrl_ethflow_table(bundleid)                                        (0x07000+(bundleid))
#define cAf6Reg_bundlectrl_ethflow_table_WidthVal                                                           32
#define cAf6Reg_bundlectrl_ethflow_table_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: Bundle_Ctrl_Enable
BitField Type: R/W
BitField Desc: 0: LookUp Fail
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_Enable_Bit_Start                                          12
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_Enable_Bit_End                                            12
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_Enable_Mask                                           cBit12
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_Enable_Shift                                              12
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_Enable_MaxVal                                            0x1
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_Enable_MinVal                                            0x0
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_Enable_RstVal                                            0x0

/*--------------------------------------
BitField Name: Bundle_Ctrl_EthFlowID
BitField Type: R/W
BitField Desc: Ethernet FLow: 0-4095
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_EthFlowID_Bit_Start                                        0
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_EthFlowID_Bit_End                                         11
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_EthFlowID_Mask                                      cBit11_0
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_EthFlowID_Shift                                            0
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_EthFlowID_MaxVal                                       0xfff
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_EthFlowID_MinVal                                         0x0
#define cAf6_bundlectrl_ethflow_table_Bundle_Ctrl_EthFlowID_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Frame Relay Hash Table #0
Reg Addr   : 0x08000-0x08FFF
Reg Formula: 0x08000 + $hash_id
    Where  :
           + $hash_id(0-4095)
Reg Desc   :
Dual hash with:
- CRC1: x^12+x^6+x^4+x^0
- CRC2: x^12+x^8+x^4+x^2+x^0
Input key: {FR_Link[11:0],FR_DLCI[22:0]}

------------------------------------------------------------------------------*/
#define cAf6Reg_fr_hash_table0_Base                                                                    0x08000
#define cAf6Reg_fr_hash_table0(hashid)                                                      (0x08000+(hashid))
#define cAf6Reg_fr_hash_table0_WidthVal                                                                     64
#define cAf6Reg_fr_hash_table0_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: FR_Hash_Valid
BitField Type: R/W
BitField Desc: 0: LookUp Fail
BitField Bits: [47:47]
--------------------------------------*/
#define cAf6_fr_hash_table0_FR_Hash_Valid_Bit_Start                                                         47
#define cAf6_fr_hash_table0_FR_Hash_Valid_Bit_End                                                           47
#define cAf6_fr_hash_table0_FR_Hash_Valid_Mask                                                          cBit15
#define cAf6_fr_hash_table0_FR_Hash_Valid_Shift                                                             15
#define cAf6_fr_hash_table0_FR_Hash_Valid_MaxVal                                                           0x0
#define cAf6_fr_hash_table0_FR_Hash_Valid_MinVal                                                           0x0
#define cAf6_fr_hash_table0_FR_Hash_Valid_RstVal                                                           0x0

/*--------------------------------------
BitField Name: FR_Link
BitField Type: R/W
BitField Desc: Link (0-3071)
BitField Bits: [46:35]
--------------------------------------*/
#define cAf6_fr_hash_table0_FR_Link_Bit_Start                                                               35
#define cAf6_fr_hash_table0_FR_Link_Bit_End                                                                 46
#define cAf6_fr_hash_table0_FR_Link_Mask                                                              cBit14_3
#define cAf6_fr_hash_table0_FR_Link_Shift                                                                    3
#define cAf6_fr_hash_table0_FR_Link_MaxVal                                                                 0x0
#define cAf6_fr_hash_table0_FR_Link_MinVal                                                                 0x0
#define cAf6_fr_hash_table0_FR_Link_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: FR_DLCI
BitField Type: R/W
BitField Desc: Frame Relay DLCI
BitField Bits: [34:12]
--------------------------------------*/
#define cAf6_fr_hash_table0_FR_DLCI_Bit_Start                                                               12
#define cAf6_fr_hash_table0_FR_DLCI_Bit_End                                                                 34
#define cAf6_fr_hash_table0_FR_DLCI_Mask_01                                                          cBit31_12
#define cAf6_fr_hash_table0_FR_DLCI_Shift_01                                                                12
#define cAf6_fr_hash_table0_FR_DLCI_Mask_02                                                            cBit2_0
#define cAf6_fr_hash_table0_FR_DLCI_Shift_02                                                                 0

/*--------------------------------------
BitField Name: FR_EthFlowID
BitField Type: R/W
BitField Desc: Ethernet FLow: 0-4095
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_fr_hash_table0_FR_EthFlowID_Bit_Start                                                           0
#define cAf6_fr_hash_table0_FR_EthFlowID_Bit_End                                                            11
#define cAf6_fr_hash_table0_FR_EthFlowID_Mask                                                         cBit11_0
#define cAf6_fr_hash_table0_FR_EthFlowID_Shift                                                               0
#define cAf6_fr_hash_table0_FR_EthFlowID_MaxVal                                                          0xfff
#define cAf6_fr_hash_table0_FR_EthFlowID_MinVal                                                            0x0
#define cAf6_fr_hash_table0_FR_EthFlowID_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Frame Relay Hash Table #1
Reg Addr   : 0x09000-0x09FFF
Reg Formula: 0x09000 + $hash_id
    Where  :
           + $hash_id(0-4095)
Reg Desc   :
Dual hash with:
- CRC1: x^12+x^7+x^2+x^0
- CRC2: x^12+x^8+x^5+x^3+x^2+x^0
Input key: {FR_Link[11:0],FR_DLCI[22:0]}

------------------------------------------------------------------------------*/
#define cAf6Reg_fr_hash_table1_Base                                                                    0x09000
#define cAf6Reg_fr_hash_table1(hashid)                                                      (0x09000+(hashid))
#define cAf6Reg_fr_hash_table1_WidthVal                                                                     64
#define cAf6Reg_fr_hash_table1_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: FR_Hash_Valid
BitField Type: R/W
BitField Desc: 0: LookUp Fail
BitField Bits: [47:47]
--------------------------------------*/
#define cAf6_fr_hash_table1_FR_Hash_Valid_Bit_Start                                                         47
#define cAf6_fr_hash_table1_FR_Hash_Valid_Bit_End                                                           47
#define cAf6_fr_hash_table1_FR_Hash_Valid_Mask                                                          cBit15
#define cAf6_fr_hash_table1_FR_Hash_Valid_Shift                                                             15
#define cAf6_fr_hash_table1_FR_Hash_Valid_MaxVal                                                           0x0
#define cAf6_fr_hash_table1_FR_Hash_Valid_MinVal                                                           0x0
#define cAf6_fr_hash_table1_FR_Hash_Valid_RstVal                                                           0x0

/*--------------------------------------
BitField Name: FR_Link
BitField Type: R/W
BitField Desc: Link (0-3071)
BitField Bits: [46:35]
--------------------------------------*/
#define cAf6_fr_hash_table1_FR_Link_Bit_Start                                                               35
#define cAf6_fr_hash_table1_FR_Link_Bit_End                                                                 46
#define cAf6_fr_hash_table1_FR_Link_Mask                                                              cBit14_3
#define cAf6_fr_hash_table1_FR_Link_Shift                                                                    3
#define cAf6_fr_hash_table1_FR_Link_MaxVal                                                                 0x0
#define cAf6_fr_hash_table1_FR_Link_MinVal                                                                 0x0
#define cAf6_fr_hash_table1_FR_Link_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: FR_DLCI
BitField Type: R/W
BitField Desc: Frame Relay DLCI
BitField Bits: [34:12]
--------------------------------------*/
#define cAf6_fr_hash_table1_FR_DLCI_Bit_Start                                                               12
#define cAf6_fr_hash_table1_FR_DLCI_Bit_End                                                                 34
#define cAf6_fr_hash_table1_FR_DLCI_Mask_01                                                          cBit31_12
#define cAf6_fr_hash_table1_FR_DLCI_Shift_01                                                                12
#define cAf6_fr_hash_table1_FR_DLCI_Mask_02                                                            cBit2_0
#define cAf6_fr_hash_table1_FR_DLCI_Shift_02                                                                 0

/*--------------------------------------
BitField Name: FR_EthFlowID
BitField Type: R/W
BitField Desc: Ethernet FLow: 0-4095
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_fr_hash_table1_FR_EthFlowID_Bit_Start                                                           0
#define cAf6_fr_hash_table1_FR_EthFlowID_Bit_End                                                            11
#define cAf6_fr_hash_table1_FR_EthFlowID_Mask                                                         cBit11_0
#define cAf6_fr_hash_table1_FR_EthFlowID_Shift                                                               0
#define cAf6_fr_hash_table1_FR_EthFlowID_MaxVal                                                          0xfff
#define cAf6_fr_hash_table1_FR_EthFlowID_MinVal                                                            0x0
#define cAf6_fr_hash_table1_FR_EthFlowID_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Frame Relay Hash Table #2
Reg Addr   : 0x0A000-0x0AFFF
Reg Formula: 0x0A000 + $hash_id
    Where  :
           + $hash_id(0-4095)
Reg Desc   :
Dual hash with:
- CRC1: x^12+x^7+x^4+x^3+x^2+x^0
- CRC2: x^12+x^9+x^5+x^3+x^0
Input key: {FR_Link[11:0],FR_DLCI[22:0]}

------------------------------------------------------------------------------*/
#define cAf6Reg_fr_hash_table2_Base                                                                    0x0A000
#define cAf6Reg_fr_hash_table2(hashid)                                                      (0x0A000+(hashid))
#define cAf6Reg_fr_hash_table2_WidthVal                                                                     64
#define cAf6Reg_fr_hash_table2_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: FR_Hash_Valid
BitField Type: R/W
BitField Desc: 0: LookUp Fail
BitField Bits: [47:47]
--------------------------------------*/
#define cAf6_fr_hash_table2_FR_Hash_Valid_Bit_Start                                                         47
#define cAf6_fr_hash_table2_FR_Hash_Valid_Bit_End                                                           47
#define cAf6_fr_hash_table2_FR_Hash_Valid_Mask                                                          cBit15
#define cAf6_fr_hash_table2_FR_Hash_Valid_Shift                                                             15
#define cAf6_fr_hash_table2_FR_Hash_Valid_MaxVal                                                           0x0
#define cAf6_fr_hash_table2_FR_Hash_Valid_MinVal                                                           0x0
#define cAf6_fr_hash_table2_FR_Hash_Valid_RstVal                                                           0x0

/*--------------------------------------
BitField Name: FR_Link
BitField Type: R/W
BitField Desc: Link (0-3071)
BitField Bits: [46:35]
--------------------------------------*/
#define cAf6_fr_hash_table2_FR_Link_Bit_Start                                                               35
#define cAf6_fr_hash_table2_FR_Link_Bit_End                                                                 46
#define cAf6_fr_hash_table2_FR_Link_Mask                                                              cBit14_3
#define cAf6_fr_hash_table2_FR_Link_Shift                                                                    3
#define cAf6_fr_hash_table2_FR_Link_MaxVal                                                                 0x0
#define cAf6_fr_hash_table2_FR_Link_MinVal                                                                 0x0
#define cAf6_fr_hash_table2_FR_Link_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: FR_DLCI
BitField Type: R/W
BitField Desc: Frame Relay DLCI
BitField Bits: [34:12]
--------------------------------------*/
#define cAf6_fr_hash_table2_FR_DLCI_Bit_Start                                                               12
#define cAf6_fr_hash_table2_FR_DLCI_Bit_End                                                                 34
#define cAf6_fr_hash_table2_FR_DLCI_Mask_01                                                          cBit31_12
#define cAf6_fr_hash_table2_FR_DLCI_Shift_01                                                                12
#define cAf6_fr_hash_table2_FR_DLCI_Mask_02                                                            cBit2_0
#define cAf6_fr_hash_table2_FR_DLCI_Shift_02                                                                 0

/*--------------------------------------
BitField Name: FR_EthFlowID
BitField Type: R/W
BitField Desc: Ethernet FLow: 0-4095
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_fr_hash_table2_FR_EthFlowID_Bit_Start                                                           0
#define cAf6_fr_hash_table2_FR_EthFlowID_Bit_End                                                            11
#define cAf6_fr_hash_table2_FR_EthFlowID_Mask                                                         cBit11_0
#define cAf6_fr_hash_table2_FR_EthFlowID_Shift                                                               0
#define cAf6_fr_hash_table2_FR_EthFlowID_MaxVal                                                          0xfff
#define cAf6_fr_hash_table2_FR_EthFlowID_MinVal                                                            0x0
#define cAf6_fr_hash_table2_FR_EthFlowID_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Frame Relay Hash Table #3
Reg Addr   : 0x0B000-0x0BFFF
Reg Formula: 0x0B000 + $hash_id
    Where  :
           + $hash_id(0-4095)
Reg Desc   :
Dual hash with:
- CRC1: x^12+x^7+x^5+x^2+x^0
- CRC2: x^12+x^9+x^6+x^4+x^1+x^0
Input key: {FR_Link[11:0],FR_DLCI[22:0]}

------------------------------------------------------------------------------*/
#define cAf6Reg_fr_hash_table3_Base                                                                    0x0B000
#define cAf6Reg_fr_hash_table3(hashid)                                                      (0x0B000+(hashid))
#define cAf6Reg_fr_hash_table3_WidthVal                                                                     64
#define cAf6Reg_fr_hash_table3_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: FR_Hash_Valid
BitField Type: R/W
BitField Desc: 0: LookUp Fail
BitField Bits: [47:47]
--------------------------------------*/
#define cAf6_fr_hash_table3_FR_Hash_Valid_Bit_Start                                                         47
#define cAf6_fr_hash_table3_FR_Hash_Valid_Bit_End                                                           47
#define cAf6_fr_hash_table3_FR_Hash_Valid_Mask                                                          cBit15
#define cAf6_fr_hash_table3_FR_Hash_Valid_Shift                                                             15
#define cAf6_fr_hash_table3_FR_Hash_Valid_MaxVal                                                           0x0
#define cAf6_fr_hash_table3_FR_Hash_Valid_MinVal                                                           0x0
#define cAf6_fr_hash_table3_FR_Hash_Valid_RstVal                                                           0x0

/*--------------------------------------
BitField Name: FR_Link
BitField Type: R/W
BitField Desc: Link (0-3071)
BitField Bits: [46:35]
--------------------------------------*/
#define cAf6_fr_hash_table3_FR_Link_Bit_Start                                                               35
#define cAf6_fr_hash_table3_FR_Link_Bit_End                                                                 46
#define cAf6_fr_hash_table3_FR_Link_Mask                                                              cBit14_3
#define cAf6_fr_hash_table3_FR_Link_Shift                                                                    3
#define cAf6_fr_hash_table3_FR_Link_MaxVal                                                                 0x0
#define cAf6_fr_hash_table3_FR_Link_MinVal                                                                 0x0
#define cAf6_fr_hash_table3_FR_Link_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: FR_DLCI
BitField Type: R/W
BitField Desc: Frame Relay DLCI
BitField Bits: [34:12]
--------------------------------------*/
#define cAf6_fr_hash_table3_FR_DLCI_Bit_Start                                                               12
#define cAf6_fr_hash_table3_FR_DLCI_Bit_End                                                                 34
#define cAf6_fr_hash_table3_FR_DLCI_Mask_01                                                          cBit31_12
#define cAf6_fr_hash_table3_FR_DLCI_Shift_01                                                                12
#define cAf6_fr_hash_table3_FR_DLCI_Mask_02                                                            cBit2_0
#define cAf6_fr_hash_table3_FR_DLCI_Shift_02                                                                 0

/*--------------------------------------
BitField Name: FR_EthFlowID
BitField Type: R/W
BitField Desc: Ethernet FLow: 0-4095
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_fr_hash_table3_FR_EthFlowID_Bit_Start                                                           0
#define cAf6_fr_hash_table3_FR_EthFlowID_Bit_End                                                            11
#define cAf6_fr_hash_table3_FR_EthFlowID_Mask                                                         cBit11_0
#define cAf6_fr_hash_table3_FR_EthFlowID_Shift                                                               0
#define cAf6_fr_hash_table3_FR_EthFlowID_MaxVal                                                          0xfff
#define cAf6_fr_hash_table3_FR_EthFlowID_MinVal                                                            0x0
#define cAf6_fr_hash_table3_FR_EthFlowID_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : Ethernet Flow Look up Link Stiky
Reg Addr   : 0x0E000- 0x0FBFF
Reg Formula: 0x0E000 + $r2c*4096 + $link_id
    Where  :
           + $r2c(0-1) read to clear 0-RO, 1-R2C
           + $link_id(0-3071) link_id
Reg Desc   :
Sticky for each link id , support both mode  read only and read to clear
- Read only ,need write 1 to clear sticky
- Read to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_link_stk_Base                                                                          0x0E000
#define cAf6Reg_link_stk(r2c, linkid)                                            (0x0E000+(r2c)*4096+(linkid))
#define cAf6Reg_link_stk_WidthVal                                                                           32
#define cAf6Reg_link_stk_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: rule_outdrop_stk
BitField Type: R2C
BitField Desc: RULE_OUT_DROP sticky
BitField Bits: [16]
--------------------------------------*/
#define cAf6_link_stk_rule_outdrop_stk_Bit_Start                                                            16
#define cAf6_link_stk_rule_outdrop_stk_Bit_End                                                              16
#define cAf6_link_stk_rule_outdrop_stk_Mask                                                             cBit16
#define cAf6_link_stk_rule_outdrop_stk_Shift                                                                16
#define cAf6_link_stk_rule_outdrop_stk_MaxVal                                                              0x1
#define cAf6_link_stk_rule_outdrop_stk_MinVal                                                              0x0
#define cAf6_link_stk_rule_outdrop_stk_RstVal                                                              0x0

/*--------------------------------------
BitField Name: lpsr_lkerr_stk
BitField Type: R2C
BitField Desc: LPSR_LK_ERR sticky
BitField Bits: [15]
--------------------------------------*/
#define cAf6_link_stk_lpsr_lkerr_stk_Bit_Start                                                              15
#define cAf6_link_stk_lpsr_lkerr_stk_Bit_End                                                                15
#define cAf6_link_stk_lpsr_lkerr_stk_Mask                                                               cBit15
#define cAf6_link_stk_lpsr_lkerr_stk_Shift                                                                  15
#define cAf6_link_stk_lpsr_lkerr_stk_MaxVal                                                                0x1
#define cAf6_link_stk_lpsr_lkerr_stk_MinVal                                                                0x0
#define cAf6_link_stk_lpsr_lkerr_stk_RstVal                                                                0x0

/*--------------------------------------
BitField Name: lpsr_macerr_stk
BitField Type: R2C
BitField Desc: LPSR_MAC_ERR sticky
BitField Bits: [14]
--------------------------------------*/
#define cAf6_link_stk_lpsr_macerr_stk_Bit_Start                                                             14
#define cAf6_link_stk_lpsr_macerr_stk_Bit_End                                                               14
#define cAf6_link_stk_lpsr_macerr_stk_Mask                                                              cBit14
#define cAf6_link_stk_lpsr_macerr_stk_Shift                                                                 14
#define cAf6_link_stk_lpsr_macerr_stk_MaxVal                                                               0x1
#define cAf6_link_stk_lpsr_macerr_stk_MinVal                                                               0x0
#define cAf6_link_stk_lpsr_macerr_stk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: lpsr_mlerr_stk
BitField Type: R2C
BitField Desc: LPSR_MULTILINK_ERR sticky
BitField Bits: [13]
--------------------------------------*/
#define cAf6_link_stk_lpsr_mlerr_stk_Bit_Start                                                              13
#define cAf6_link_stk_lpsr_mlerr_stk_Bit_End                                                                13
#define cAf6_link_stk_lpsr_mlerr_stk_Mask                                                               cBit13
#define cAf6_link_stk_lpsr_mlerr_stk_Shift                                                                  13
#define cAf6_link_stk_lpsr_mlerr_stk_MaxVal                                                                0x1
#define cAf6_link_stk_lpsr_mlerr_stk_MinVal                                                                0x0
#define cAf6_link_stk_lpsr_mlerr_stk_RstVal                                                                0x0

/*--------------------------------------
BitField Name: lpsr_piderr_stk
BitField Type: R2C
BitField Desc: LPSR_PROTOCOL_ERR sticky
BitField Bits: [12]
--------------------------------------*/
#define cAf6_link_stk_lpsr_piderr_stk_Bit_Start                                                             12
#define cAf6_link_stk_lpsr_piderr_stk_Bit_End                                                               12
#define cAf6_link_stk_lpsr_piderr_stk_Mask                                                              cBit12
#define cAf6_link_stk_lpsr_piderr_stk_Shift                                                                 12
#define cAf6_link_stk_lpsr_piderr_stk_MaxVal                                                               0x1
#define cAf6_link_stk_lpsr_piderr_stk_MinVal                                                               0x0
#define cAf6_link_stk_lpsr_piderr_stk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: lpsr_ctrlerr_stk
BitField Type: R2C
BitField Desc: LPSR_CONTROL_ERR sticky
BitField Bits: [11]
--------------------------------------*/
#define cAf6_link_stk_lpsr_ctrlerr_stk_Bit_Start                                                            11
#define cAf6_link_stk_lpsr_ctrlerr_stk_Bit_End                                                              11
#define cAf6_link_stk_lpsr_ctrlerr_stk_Mask                                                             cBit11
#define cAf6_link_stk_lpsr_ctrlerr_stk_Shift                                                                11
#define cAf6_link_stk_lpsr_ctrlerr_stk_MaxVal                                                              0x1
#define cAf6_link_stk_lpsr_ctrlerr_stk_MinVal                                                              0x0
#define cAf6_link_stk_lpsr_ctrlerr_stk_RstVal                                                              0x0

/*--------------------------------------
BitField Name: lpsr_adrerr_stk
BitField Type: R2C
BitField Desc: LPSR_ADDRESS_ERR sticky
BitField Bits: [10]
--------------------------------------*/
#define cAf6_link_stk_lpsr_adrerr_stk_Bit_Start                                                             10
#define cAf6_link_stk_lpsr_adrerr_stk_Bit_End                                                               10
#define cAf6_link_stk_lpsr_adrerr_stk_Mask                                                              cBit10
#define cAf6_link_stk_lpsr_adrerr_stk_Shift                                                                 10
#define cAf6_link_stk_lpsr_adrerr_stk_MaxVal                                                               0x1
#define cAf6_link_stk_lpsr_adrerr_stk_MinVal                                                               0x0
#define cAf6_link_stk_lpsr_adrerr_stk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: lpsr_abort_stk
BitField Type: R2C
BitField Desc: LPSR_ABORT sticky
BitField Bits: [9]
--------------------------------------*/
#define cAf6_link_stk_lpsr_abort_stk_Bit_Start                                                               9
#define cAf6_link_stk_lpsr_abort_stk_Bit_End                                                                 9
#define cAf6_link_stk_lpsr_abort_stk_Mask                                                                cBit9
#define cAf6_link_stk_lpsr_abort_stk_Shift                                                                   9
#define cAf6_link_stk_lpsr_abort_stk_MaxVal                                                                0x1
#define cAf6_link_stk_lpsr_abort_stk_MinVal                                                                0x0
#define cAf6_link_stk_lpsr_abort_stk_RstVal                                                                0x0

/*--------------------------------------
BitField Name: lpsr_fcserr_stk
BitField Type: R2C
BitField Desc: LPSR_FCS_ERR sticky
BitField Bits: [8]
--------------------------------------*/
#define cAf6_link_stk_lpsr_fcserr_stk_Bit_Start                                                              8
#define cAf6_link_stk_lpsr_fcserr_stk_Bit_End                                                                8
#define cAf6_link_stk_lpsr_fcserr_stk_Mask                                                               cBit8
#define cAf6_link_stk_lpsr_fcserr_stk_Shift                                                                  8
#define cAf6_link_stk_lpsr_fcserr_stk_MaxVal                                                               0x1
#define cAf6_link_stk_lpsr_fcserr_stk_MinVal                                                               0x0
#define cAf6_link_stk_lpsr_fcserr_stk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: lfwd_drop_stk
BitField Type: R2C
BitField Desc: LFWD_DROP sticky
BitField Bits: [7]
--------------------------------------*/
#define cAf6_link_stk_lfwd_drop_stk_Bit_Start                                                                7
#define cAf6_link_stk_lfwd_drop_stk_Bit_End                                                                  7
#define cAf6_link_stk_lfwd_drop_stk_Mask                                                                 cBit7
#define cAf6_link_stk_lfwd_drop_stk_Shift                                                                    7
#define cAf6_link_stk_lfwd_drop_stk_MaxVal                                                                 0x1
#define cAf6_link_stk_lfwd_drop_stk_MinVal                                                                 0x0
#define cAf6_link_stk_lfwd_drop_stk_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: lfwd_mldata_stk
BitField Type: R2C
BitField Desc: LFWD_ML_DATA sticky
BitField Bits: [6]
--------------------------------------*/
#define cAf6_link_stk_lfwd_mldata_stk_Bit_Start                                                              6
#define cAf6_link_stk_lfwd_mldata_stk_Bit_End                                                                6
#define cAf6_link_stk_lfwd_mldata_stk_Mask                                                               cBit6
#define cAf6_link_stk_lfwd_mldata_stk_Shift                                                                  6
#define cAf6_link_stk_lfwd_mldata_stk_MaxVal                                                               0x1
#define cAf6_link_stk_lfwd_mldata_stk_MinVal                                                               0x0
#define cAf6_link_stk_lfwd_mldata_stk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: lfwd_mlctrl_stk
BitField Type: R2C
BitField Desc: LFWD_ML_CONTROL sticky
BitField Bits: [5]
--------------------------------------*/
#define cAf6_link_stk_lfwd_mlctrl_stk_Bit_Start                                                              5
#define cAf6_link_stk_lfwd_mlctrl_stk_Bit_End                                                                5
#define cAf6_link_stk_lfwd_mlctrl_stk_Mask                                                               cBit5
#define cAf6_link_stk_lfwd_mlctrl_stk_Shift                                                                  5
#define cAf6_link_stk_lfwd_mlctrl_stk_MaxVal                                                               0x1
#define cAf6_link_stk_lfwd_mlctrl_stk_MinVal                                                               0x0
#define cAf6_link_stk_lfwd_mlctrl_stk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: lfwd_dpctrl_stk
BitField Type: R2C
BitField Desc: LFWD_DP_CONTROL sticky
BitField Bits: [4]
--------------------------------------*/
#define cAf6_link_stk_lfwd_dpctrl_stk_Bit_Start                                                              4
#define cAf6_link_stk_lfwd_dpctrl_stk_Bit_End                                                                4
#define cAf6_link_stk_lfwd_dpctrl_stk_Mask                                                               cBit4
#define cAf6_link_stk_lfwd_dpctrl_stk_Shift                                                                  4
#define cAf6_link_stk_lfwd_dpctrl_stk_MaxVal                                                               0x1
#define cAf6_link_stk_lfwd_dpctrl_stk_MinVal                                                               0x0
#define cAf6_link_stk_lfwd_dpctrl_stk_RstVal                                                               0x0

/*--------------------------------------
BitField Name: lfwd_corrupt_stk
BitField Type: R2C
BitField Desc: LFWD_CORRUPT sticky
BitField Bits: [3]
--------------------------------------*/
#define cAf6_link_stk_lfwd_corrupt_stk_Bit_Start                                                             3
#define cAf6_link_stk_lfwd_corrupt_stk_Bit_End                                                               3
#define cAf6_link_stk_lfwd_corrupt_stk_Mask                                                              cBit3
#define cAf6_link_stk_lfwd_corrupt_stk_Shift                                                                 3
#define cAf6_link_stk_lfwd_corrupt_stk_MaxVal                                                              0x1
#define cAf6_link_stk_lfwd_corrupt_stk_MinVal                                                              0x0
#define cAf6_link_stk_lfwd_corrupt_stk_RstVal                                                              0x0

/*--------------------------------------
BitField Name: lfwd_data_stk
BitField Type: R2C
BitField Desc: LFWD_DATA sticky
BitField Bits: [2]
--------------------------------------*/
#define cAf6_link_stk_lfwd_data_stk_Bit_Start                                                                2
#define cAf6_link_stk_lfwd_data_stk_Bit_End                                                                  2
#define cAf6_link_stk_lfwd_data_stk_Mask                                                                 cBit2
#define cAf6_link_stk_lfwd_data_stk_Shift                                                                    2
#define cAf6_link_stk_lfwd_data_stk_MaxVal                                                                 0x1
#define cAf6_link_stk_lfwd_data_stk_MinVal                                                                 0x0
#define cAf6_link_stk_lfwd_data_stk_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: lfwd_pw_stk
BitField Type: R2C
BitField Desc: LFWD_PW sticky
BitField Bits: [1]
--------------------------------------*/
#define cAf6_link_stk_lfwd_pw_stk_Bit_Start                                                                  1
#define cAf6_link_stk_lfwd_pw_stk_Bit_End                                                                    1
#define cAf6_link_stk_lfwd_pw_stk_Mask                                                                   cBit1
#define cAf6_link_stk_lfwd_pw_stk_Shift                                                                      1
#define cAf6_link_stk_lfwd_pw_stk_MaxVal                                                                   0x1
#define cAf6_link_stk_lfwd_pw_stk_MinVal                                                                   0x0
#define cAf6_link_stk_lfwd_pw_stk_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: lfwd_ctrl_stk
BitField Type: R2C
BitField Desc: LFWD_CONTROL sticky
BitField Bits: [0]
--------------------------------------*/
#define cAf6_link_stk_lfwd_ctrl_stk_Bit_Start                                                                0
#define cAf6_link_stk_lfwd_ctrl_stk_Bit_End                                                                  0
#define cAf6_link_stk_lfwd_ctrl_stk_Mask                                                                 cBit0
#define cAf6_link_stk_lfwd_ctrl_stk_Shift                                                                    0
#define cAf6_link_stk_lfwd_ctrl_stk_MaxVal                                                                 0x1
#define cAf6_link_stk_lfwd_ctrl_stk_MinVal                                                                 0x0
#define cAf6_link_stk_lfwd_ctrl_stk_RstVal                                                                 0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEETHFLOWLOOKUPREG_H_ */


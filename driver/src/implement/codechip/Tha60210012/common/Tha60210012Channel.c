/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : Tha60210012Channel.c
 *
 * Created Date: May 24, 2016
 *
 * Description : Channel common functions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "Tha60210012Channel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210012ChannelHwResource
    {
    uint32 plaBlockId;

    /* More hardware resources will be put */
    }tTha60210012ChannelHwResource;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tTha60210012ChannelHwResource* HwResource(AtChannel self)
    {
    void* hwResource = AtChannelHwResourceGet(self);
    if (hwResource == NULL)
        {
        hwResource = AtOsalMemAlloc(sizeof(tTha60210012ChannelHwResource));
        AtOsalMemInit(hwResource, 0, sizeof(tTha60210012ChannelHwResource));
        AtChannelHwResourceCache(self, hwResource);
        }

    return (tTha60210012ChannelHwResource*)hwResource;
    }

static eAtRet PlaBlockIdSet(AtChannel self, uint32 blockId)
    {
    tTha60210012ChannelHwResource* hwResource = HwResource(self);
    if (hwResource == NULL)
        return cAtErrorRsrcNoAvail;

    hwResource->plaBlockId = blockId;
    return cAtOk;
    }

static uint32 PlaBlockIdGet(AtChannel self)
    {
    tTha60210012ChannelHwResource* hwResource = HwResource(self);
    if (hwResource == NULL)
        return cInvalidUint32;

    return HwResource(self)->plaBlockId;
    }

static void SerializeHwResource(AtObject hwResource, AtCoder encoder)
    {
    tTha60210012ChannelHwResource *object = (tTha60210012ChannelHwResource *)hwResource;
    mEncodeUInt(plaBlockId);
    }

eAtRet Tha60210012ChannelPlaBlockIdSet(AtChannel self, uint32 blockId)
    {
    if (self)
        return PlaBlockIdSet(self, blockId);
    return cAtErrorObjectNotExist;
    }

uint32 Tha60210012ChannelPlaBlockIdGet(AtChannel self)
    {
    if (self)
        return PlaBlockIdGet(self);
    return cInvalidUint32;
    }

void Tha60210012ChannelSerializeHwResource(AtChannel self, AtCoder encoder)
    {
    tTha60210012ChannelHwResource* hwResource = HwResource(self);
    if (hwResource && encoder)
        AtCoderEncodeObjectWithHandler(encoder, (AtObject)hwResource, "hwResource", SerializeHwResource);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : Tha60210012Channel.h
 * 
 * Created Date: May 24, 2016
 *
 * Description : Common declarations for channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012CHANNEL_H_
#define _THA60210012CHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210012ChannelPlaBlockIdSet(AtChannel self, uint32 blockId);
uint32 Tha60210012ChannelPlaBlockIdGet(AtChannel self);
void Tha60210012ChannelSerializeHwResource(AtChannel self, AtCoder encoder);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012CHANNEL_H_ */


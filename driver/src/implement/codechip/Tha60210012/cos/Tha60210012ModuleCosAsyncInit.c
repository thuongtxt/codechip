/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : Tha60210012ModuleCosAsyncInit.c
 *
 * Created Date: Aug 24, 2016
 *
 * Description : AsyncInit
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleEth.h"
#include "../../../default/pwe/ThaModulePweInternal.h"
#include "../../Tha60210011/cos/Tha60210011ModuleCosInternal.h"
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "../pwe/Tha60210012ModulePlaReg.h"
#include "../pw/activator/Tha60210012PwActivator.h"
#include "../pw/Tha60210012ModulePw.h"
#include "Tha60210012ModuleCosInternal.h"
#include "Tha60210012ModuleCosAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxEntry(Tha60210012ModuleCos self)
    {
    AtModuleEth moduleEth = (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEth);
    return (uint32)AtModuleEthMaxFlowsGet(moduleEth);
    }

static void AsyncHwDefaultStateSet(Tha60210012ModuleCos self, uint32 state)
    {
    self->asyncInitHwDefaultState = state;
    }

static uint32 AsyncHwDefaultStateGet(Tha60210012ModuleCos self)
    {
    return self->asyncInitHwDefaultState;
    }

static uint32 MaxEntryPerAsyncInitGet(Tha60210012ModuleCos self, uint32 state)
    {
    uint32 max0 = (state + 1) * 1000;
    uint32 max1 = MaxEntry(self);

    if (max0 < max1)
        return max0;
    return max1;
    }

static uint32 StartEntryPerAsyncInitGet(Tha60210012ModuleCos self, uint32 state)
    {
    AtUnused(self);
    return state * 1000;
    }

static eAtRet FlowDefaultAsyncReset(Tha60210012ModuleCos self)
    {
    eAtRet ret = cAtOk;
    uint32 i;
    uint32 state = AsyncHwDefaultStateGet(self);
    uint32 maxEntry = MaxEntryPerAsyncInitGet(self, state);
    tAtOsalCurTime profileTime;

    char stateString[32];
    AtSprintf(stateString, "Async state: %d", state);
    AtOsalCurTimeGet(&profileTime);

    for (i = StartEntryPerAsyncInitGet(self, state); i < maxEntry; i++)
        Tha60210012ModuleCosFlowHeaderLengthReset(self, i);
    AtDeviceAccessTimeInMsSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, &profileTime, __FILE__, __LINE__, stateString);
    AtDeviceAccessTimeCountSinceLastProfileCheck(AtModuleDeviceGet((AtModule)self), cAtTrue, __FILE__, __LINE__, stateString);

    state += 1;
    if (maxEntry == MaxEntry(self))
        {
        ret = cAtOk;
        state = 0;
        }
    else
        ret = cAtErrorAgain;

    AsyncHwDefaultStateSet(self, state);
    return ret;
    }

eAtRet Tha60210012ModuleCosDefaultAsyncReset(Tha60210012ModuleCos self)
    {
    return FlowDefaultAsyncReset(self);
    }

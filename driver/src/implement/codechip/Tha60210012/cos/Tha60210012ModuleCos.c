/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : Tha60210012ModuleCos.c
 *
 * Created Date: Mar 2, 2016
 *
 * Description : Module COS of 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleEth.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/pwe/ThaModulePweInternal.h"
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "../pwe/Tha60210012ModulePlaReg.h"
#include "../pw/activator/Tha60210012PwActivator.h"
#include "../pw/Tha60210012ModulePw.h"
#include "Tha60210012ModuleCosInternal.h"
#include "Tha60210012ModuleCosAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tThaModuleCosMethods m_ThaModuleCosOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePwe PweModule(ThaModuleCos self)
    {
    return (ThaModulePwe)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePwe);
    }

static uint8 HeaderLengthWriteToHw(ThaModuleCos self, AtPw pw, uint8 hdrLenInByte)
    {
    Tha60210011ModulePwe modulePwe = (Tha60210011ModulePwe)PweModule(self);
    return (uint8)(hdrLenInByte + Tha60210011ModulePweNumberOfAddtionalByte(modulePwe, pw));
    }

static uint32 PwToFlowId(ThaPwAdapter pwAdapter)
    {
    return Tha60210012PwAdapterHwFlowIdGet(pwAdapter);
    }

static uint32 RegAddress_pla_out_flow_ctrl(ThaModulePwe self, uint32 flowId)
    {
    AtUnused(self);
    return cAf6Reg_pla_out_flow_ctrl_Base + flowId + Tha60210011ModulePlaBaseAddress();
    }

static eAtRet PwHeaderLengthSet(ThaModuleCos self, AtPw pw, uint8 hdrLenInByte)
    {
    uint32 regVal;
    uint32 flowId = PwToFlowId((ThaPwAdapter)pw);
    uint32 regAddr = RegAddress_pla_out_flow_ctrl(PweModule(self), flowId);

    regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pla_out_flow_ctrl_PlaOutPSNLenCtrl_, HeaderLengthWriteToHw(self, pw, hdrLenInByte));
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint8 RealHeaderLength(ThaModuleCos self, AtPw pw, uint8 hwValue)
    {
    Tha60210011ModulePwe modulePwe = (Tha60210011ModulePwe)PweModule(self);
    uint8 numAdditionalByte = Tha60210011ModulePweNumberOfAddtionalByte(modulePwe, pw);

    if (hwValue < numAdditionalByte)
        return 0;

    return (uint8)(hwValue - numAdditionalByte);
    }

static uint8 PwHeaderLengthGet(ThaModuleCos self, AtPw pw)
    {
    uint32 regVal;
    uint32 flowId = PwToFlowId((ThaPwAdapter)pw);
    uint32 regAddr = RegAddress_pla_out_flow_ctrl(PweModule(self), flowId);

    regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return RealHeaderLength(self, pw, (uint8)mRegField(regVal, cAf6_pla_out_flow_ctrl_PlaOutPSNLenCtrl_));
    }

static eAtRet PwHeaderLengthReset(ThaModuleCos self, uint8 partId, uint32 localPwId)
    {
    AtUnused(self);
    AtUnused(partId);
    AtUnused(localPwId);
    return cAtOk;
    }

static eAtRet FlowHeaderLengthReset(ThaModuleCos self,uint32 flowId)
    {
    uint32 regVal;
    uint32 regAddr = RegAddress_pla_out_flow_ctrl(PweModule(self), flowId);

    regVal = mModuleHwRead(PweModule(self), regAddr);
    mRegFieldSet(regVal, cAf6_pla_out_flow_ctrl_PlaOutPSNLenCtrl_, 0);
    mModuleHwWrite(PweModule(self), regAddr, regVal);
    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleCos self)
    {
    AtModuleEth moduleEth = (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleEth);
    uint16 numFlow = AtModuleEthMaxFlowsGet(moduleEth);
    uint16 flow;

    for (flow = 0; flow < numFlow; flow++)
        FlowHeaderLengthReset(self, (uint32)flow);
    return cAtOk;
    }

static eAtRet AsyncDefaultSet(ThaModuleCos self)
    {
    return Tha60210012ModuleCosDefaultAsyncReset((Tha60210012ModuleCos)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210012ModuleCos object = (Tha60210012ModuleCos)(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(asyncInitHwDefaultState);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaModuleCos(AtModule self)
    {
    ThaModuleCos cos = (ThaModuleCos)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCosOverride, mMethodsGet(cos), sizeof(m_ThaModuleCosOverride));

        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthSet);
        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthGet);
        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthReset);
        mMethodOverride(m_ThaModuleCosOverride, DefaultSet);
        mMethodOverride(m_ThaModuleCosOverride, AsyncDefaultSet);
        }

    mMethodsSet(cos, &m_ThaModuleCosOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    OverrideThaModuleCos(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleCos);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleCosObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210012ModuleCosNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

void Tha60210012ModuleCosFlowHeaderLengthReset(Tha60210012ModuleCos self, uint32 flow)
    {
    FlowHeaderLengthReset((ThaModuleCos)self, flow);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : COS
 * 
 * File        : Tha60210012ModuleCosAsyncInit.h
 * 
 * Created Date: Aug 24, 2016
 *
 * Description : AsyncInit
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULECOSASYNCINIT_H_
#define _THA60210012MODULECOSASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60210012ModuleCosFlowHeaderLengthReset(Tha60210012ModuleCos self, uint32 flow);
eAtRet Tha60210012ModuleCosDefaultAsyncReset(Tha60210012ModuleCos self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULECOSASYNCINIT_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60210011ModuleCosInternal.h
 * 
 * Created Date: Jul 6, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210012_COS_THA60210011MODULECOSINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210012_COS_THA60210011MODULECOSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/cos/Tha60210011ModuleCosInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleCos *Tha60210012ModuleCos;

typedef struct tTha60210012ModuleCos
    {
    tTha60210011ModuleCos super;
    /*async init*/
    uint32 asyncInitHwDefaultState;

    }tTha60210012ModuleCos;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210012_COS_THA60210011MODULECOSINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210012Tfi5LineTu3Vc.c
 *
 * Created Date: Sep 10, 2015
 *
 * Description :  TFI-5 Vc1x of product 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/concate/AtModuleConcateInternal.h"
#include "../../../default/encap/dynamic/ThaDynamicHdlcChannel.h"
#include "../pwe/Tha60210012ModulePweBlockManager.h"
#include "../common/Tha60210012Channel.h"
#include "../pda/Tha60210012ModulePda.h"
#include "../encap/Tha60210012PhysicalHdlcChannel.h"
#include "Tha60210012ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha60210012Tfi5LineTu3Vc *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet HwResourceAllocate(AtChannel self)
    {
    return Tha60210012PwCircuitSts1VcBlockAllocate(self);
    }

static eAtRet HwResourceDeallocate(AtChannel self)
    {
    return Tha60210012ChannelBlockDeallocate(self);
    }

static eAtRet TxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    uint8 hwSts, hwSlice;
    AtSdhChannel vc3 = (AtSdhChannel)self;
    const uint8 cTu3VcPayloadType = 0x8;
    AtHdlcChannel logicChannel = (AtHdlcChannel)AtChannelBoundEncapChannel(self);

    if (enable)
        {
        ret |= m_AtChannelMethods->TxEncapConnectionEnable(self, enable);
        ret |= Tha60210012TxHdlcServiceSetup(logicChannel, enable, cTu3VcPayloadType);
        ThaSdhChannelHwStsGet(vc3, cThaModulePda, AtSdhChannelSts1Get(vc3), &hwSlice, &hwSts);
        ret |= Tha60210012ModulePdaHdlcHwStsIdSet(ThaHdlcChannelPhysicalChannelGet(logicChannel), hwSts);

        return ret;
        }

    ret |= Tha60210012TxHdlcServiceSetup(logicChannel, enable, 0);
    ret |= m_AtChannelMethods->TxEncapConnectionEnable(self, enable);

    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210012Tfi5LineTu3Vc *object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(vcgBinder);
    Tha60210012ChannelSerializeHwResource((AtChannel)self, encoder);
    }

static void OverrideAtObject(AtSdhVc self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, HwResourceAllocate);
        mMethodOverride(m_AtChannelOverride, HwResourceDeallocate);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012Tfi5LineTu3Vc);
    }

AtSdhVc Tha60210012Tfi5LineTu3VcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5LineTu3VcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60210012Tfi5LineTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012Tfi5LineTu3VcObjectInit(self, channelId, channelType, module);
    }

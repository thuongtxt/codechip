/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210012Tfi5LineAuVc.c
 *
 * Created Date: Sep 10, 2015
 *
 * Description :  TFI-5 AuVc of product 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/concate/AtModuleConcateInternal.h"
#include "../../../default/encap/dynamic/ThaDynamicHdlcChannel.h"
#include "../pwe/Tha60210012ModulePweBlockManager.h"
#include "../common/Tha60210012Channel.h"
#include "../encap/Tha60210012PhysicalHdlcChannel.h"
#include "../encap/Tha60210012ModuleEncap.h"
#include "../pw/Tha60210012ModulePw.h"
#include "Tha60210012ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha60210012Tfi5LineAuVc *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet HwResourceAllocate(AtChannel self)
    {
    return Tha60210012PwCircuitHoVcBlockAllocate(self);
    }

static eAtRet HwResourceDeallocate(AtChannel self)
    {
    return Tha60210012ChannelBlockDeallocate(self);
    }

static eAtRet TxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    const uint8 cVcxPayloadType = 0x0;
    AtHdlcChannel logicChannel = (AtHdlcChannel)AtChannelBoundEncapChannel(self);

    if (enable)
        {
        ret |= m_AtChannelMethods->TxEncapConnectionEnable(self, enable);
        ret |= Tha60210012TxHdlcServiceSetup(logicChannel, enable, cVcxPayloadType);

        return ret;
        }

    ret |= Tha60210012TxHdlcServiceSetup(logicChannel, enable, cVcxPayloadType);
    ret |= m_AtChannelMethods->TxEncapConnectionEnable(self, enable);

    return ret;
    }

static AtModuleEncap ModuleEncap(AtChannel self)
    {
    return (AtModuleEncap)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleEncap);
    }

static AtModuleEncap EncapModule(AtSdhChannel auVc)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)auVc);
    return (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static eAtRet BindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    AtSdhChannel phyChannel = (AtSdhChannel)self;
    eAtRet ret = m_AtChannelMethods->BindToEncapChannel(self, encapChannel);
    eBool needSelectStsNc = Tha60210012ModuleEncapStsNcNeedSelect(EncapModule(phyChannel), phyChannel);

    if (ret != cAtOk)
        return ret;

    if ((AtSdhChannelTypeGet(phyChannel) == cAtSdhChannelTypeVc4) && !needSelectStsNc)
        return Tha60210012ModuleEncapSts3cEnable(ModuleEncap(self), phyChannel, encapChannel ? cAtTrue : cAtFalse);

    if (encapChannel && needSelectStsNc)
        {
        Tha60210012PhysicalHdlcChannel physicalHdlcChannel = (Tha60210012PhysicalHdlcChannel)ThaHdlcChannelPhysicalChannelGet((AtHdlcChannel)encapChannel);
        return Tha60210012PhysicalHdlcChannelStsNcSelect(physicalHdlcChannel, phyChannel);
        }

    return cAtOk;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    eAtRet ret;
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    eBool needSelectStsNc = Tha60210012ModuleEncapStsNcNeedSelect(EncapModule(sdhChannel), sdhChannel);

    ret = m_AtChannelMethods->BindToPseudowire(self, pseudowire);
    if (ret != cAtOk)
        return ret;

    if (pseudowire && needSelectStsNc)
        return Tha60210012PseudowireStsNcEncapByPass(pseudowire, (AtSdhChannel)self);

    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    Tha60210012ChannelSerializeHwResource((AtChannel)self, encoder);
    }

static void OverrideAtObject(AtSdhVc self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, HwResourceAllocate);
        mMethodOverride(m_AtChannelOverride, HwResourceDeallocate);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, BindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012Tfi5LineAuVc);
    }

AtSdhVc Tha60210012Tfi5LineAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051Tfi5LineAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha60210012Tfi5LineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012Tfi5LineAuVcObjectInit(self, channelId, channelType, module);
    }

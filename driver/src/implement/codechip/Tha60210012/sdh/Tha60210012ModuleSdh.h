/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210012ModuleSdh.h
 * 
 * Created Date: Sep 10, 2015
 *
 * Description : 60210012SDH module header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULESDH_H_
#define _THA60210012MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleSdh * Tha60210012ModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh Tha60210012ModuleSdhNew(AtDevice device);
AtSdhVc Tha60210012Tfi5LineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60210012Tfi5LineVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60210012Tfi5LineTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULESDH_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60210012ModuleSdh.c
 *
 * Created Date: Sep 10, 2015
 *
 * Description : SDH module of product 60210012
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210012ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods          m_AtModuleSdhOverride;
static tTha60210011ModuleSdhMethods m_Tha60210011ModuleSdhOverride;

/* Save super implementation */
static const tAtModuleSdhMethods    *m_AtModuleSdhMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumTfi5Lines(AtModuleSdh self)
    {
    AtUnused(self);
    return 2;
    }

static AtSdhChannel Tu3VcObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60210012Tfi5LineTu3VcNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel AuVcObjectCreate(Tha60210011ModuleSdh self, uint8 lineId, uint8 channelType, uint8 channelId)
    {
    AtUnused(lineId);
    return (AtSdhChannel)Tha60210012Tfi5LineAuVcNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel Vc1xObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60210012Tfi5LineVc1xNew(channelId, channelType, (AtModuleSdh)self);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, mMethodsGet(self), sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, NumTfi5Lines);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void OverrideTha60210011ModuleSdh(AtModuleSdh self)
    {
    Tha60210011ModuleSdh module = (Tha60210011ModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleSdhOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleSdhOverride));

        mMethodOverride(m_Tha60210011ModuleSdhOverride, AuVcObjectCreate);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, Tu3VcObjectCreate);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, Vc1xObjectCreate);
        }

    mMethodsSet(module, &m_Tha60210011ModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideTha60210011ModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModuleSdh);
    }

AtModuleSdh Tha60210012ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60210012ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModuleSdhObjectInit(newModule, device);
    }

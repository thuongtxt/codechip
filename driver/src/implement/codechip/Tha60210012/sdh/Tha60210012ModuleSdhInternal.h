/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60210012ModuleSdhInternal.h
 * 
 * Created Date: Jun 17, 2016
 *
 * Description : 60210012 module SDH internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULESDHINTERNAL_H_
#define _THA60210012MODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/sdh/Tha60210051ModuleSdhInternal.h"
#include "../../Tha60210051/sdh/Tha60210051Tfi5LineVcInternal.h"
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineTu3VcInternal.h"
#include "../common/Tha60210012Channel.h"
#include "Tha60210012ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModuleSdh
    {
    tTha60210051ModuleSdh super;
    }tTha60210012ModuleSdh;

typedef struct tTha60210012Tfi5LineAuVc
    {
    tTha60210051Tfi5LineAuVc super;
    }tTha60210012Tfi5LineAuVc;

typedef struct tTha60210012Tfi5LineVc1x
    {
    tTha60210011Tfi5LineVc1x super;
    }tTha60210012Tfi5LineVc1x;

typedef struct tTha60210012Tfi5LineTu3Vc
    {
    tTha60210011Tfi5LineTu3Vc super;

    /* Private data */
    AtVcgBinder vcgBinder;
    }tTha60210012Tfi5LineTu3Vc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh Tha60210012ModuleSdhObjectInit(AtModuleSdh self, AtDevice device);
AtSdhVc Tha60210012Tfi5LineAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60210012Tfi5LineVc1xObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha60210012Tfi5LineTu3VcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULESDHINTERNAL_H_ */


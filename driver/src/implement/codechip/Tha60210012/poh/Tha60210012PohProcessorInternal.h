/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH Module
 * 
 * File        : Tha60210012PohProcessorInternal.h
 * 
 * Created Date: Jul 13, 2016
 *
 * Description : POH
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012POHPROCESSORINTERNAL_H_
#define _THA60210012POHPROCESSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/poh/Tha60210051PohProcessorInternal.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012AuVcPohProcessor
    {
    tTha60210051AuVcPohProcessor super;
    }tTha60210012AuVcPohProcessor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPohProcessor Tha60210012AuVcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha60210012AuVcPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210012POHPROCESSORINTERNAL_H_ */


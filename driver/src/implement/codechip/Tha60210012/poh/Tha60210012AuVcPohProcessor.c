/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210012AuVcPohProcessor.c
 *
 * Created Date: Jul 13, 2016
 *
 * Description : POH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210012PohProcessorInternal.h"
#include "../../Tha60210051/poh/Tha60210051PohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhPathMethods                    m_AtSdhPathOverride;
static tTha60210011AuVcPohProcessorMethods  m_Tha60210011AuVcPohProcessorOverride;

/* Save super implementation */
static const tAtSdhPathMethods                    *m_AtSdhPathMethods = NULL;
static const tTha60210011AuVcPohProcessorMethods  *m_Tha60210011AuVcPohProcessorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CpeStatusRegIsChanged(Tha60210011AuVcPohProcessor self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startHwVersionSupports = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x5, 0x1009);

    return (hwVersion >= startHwVersionSupports) ? cAtTrue : cAtFalse;
    }

static uint32 PohCpeStatusRegAddr(Tha60210011AuVcPohProcessor self)
    {
    if (CpeStatusRegIsChanged(self))
        return 0x2CA80;

    return m_Tha60210011AuVcPohProcessorMethods->PohCpeStatusRegAddr(self);
    }

static uint8 RxPslGet(AtSdhPath self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    Tha60210011AuVcPohProcessor auProcessor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeStsTu3PathOhGrabber1(processor) +
                     mMethodsGet(auProcessor)->PohCpeStsTu3PathOhGrabberRegOffset(auProcessor);

    uint32 regVal[cThaLongRegMaxSize];
    mChannelHwLongRead(self, regAddr, regVal, cThaLongRegMaxSize, cThaModulePoh);
    return (uint8)mRegField(regVal[0], cAf6_pohstspohgrb_C2_);
    }

static void OverrideAtSdhPath(ThaPohProcessor self)
    {
    AtSdhPath path = (AtSdhPath)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhPathMethods = mMethodsGet(path);

        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, m_AtSdhPathMethods, sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, RxPslGet);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideTha60210011AuVcPohProcessor(ThaPohProcessor self)
    {
    Tha60210011AuVcPohProcessor path = (Tha60210011AuVcPohProcessor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011AuVcPohProcessorMethods = mMethodsGet(path);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011AuVcPohProcessorOverride, m_Tha60210011AuVcPohProcessorMethods, sizeof(m_Tha60210011AuVcPohProcessorOverride));

        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohCpeStatusRegAddr);
        }

    mMethodsSet(path, &m_Tha60210011AuVcPohProcessorOverride);
    }

static void Override(ThaPohProcessor self)
    {
    OverrideAtSdhPath(self);
    OverrideTha60210011AuVcPohProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012AuVcPohProcessor);
    }

ThaPohProcessor Tha60210012AuVcPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051AuVcPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60210012AuVcPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210012AuVcPohProcessorObjectInit(newProcessor, vc);
    }

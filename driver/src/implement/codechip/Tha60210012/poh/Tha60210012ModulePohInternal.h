/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60210012ModulePohInternal.h
 * 
 * Created Date: Aug 16, 2016
 *
 * Description : Module POH internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210012MODULEPOHINTERNAL_H_
#define _THA60210012MODULEPOHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/poh/Tha60210051ModulePohInternal.h"
#include "Tha60210012ModulePoh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210012ModulePoh
    {
    tTha60210051ModulePoh super;
    }tTha60210012ModulePoh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210012MODULEPOHINTERNAL_H_ */


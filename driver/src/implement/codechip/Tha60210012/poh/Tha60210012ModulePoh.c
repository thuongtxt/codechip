/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210012ModulePoh.c
 *
 * Created Date: Feb 29, 2016
 *
 * Description : POH module implement
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210012PohProcessorInternal.h"
#include "Tha60210012ModulePohInternal.h"
#include "../man/Tha60210012Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModulePohMethods         m_ThaModulePohOverride;
static tTha60210011ModulePohMethods m_Tha60210011ModulePohOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumberStsSlices(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 NumberVtSlices(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 4;
    }

static ThaPohProcessor AuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60210012AuVcPohProcessorNew(vc);
    }

static ThaPohProcessor Tu3VcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60210051Tu3VcPohProcessorNew(vc);
    }

static ThaPohProcessor Vc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60210051Vc1xPohProcessorNew(vc);
    }

static uint32 StartVersionSupportSdSfInterrupt(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint32 StartVersionSupportBlockModeCounters(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 0x0;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    return Tha60210012IsPohRegister(AtModuleDeviceGet(self), address);
    }

static uint32 StartVersionSupportPohMonitorEnabling(ThaModulePoh self)
    {
    AtUnused(self);
    return 0;
    }

static eBool PslTtiInterruptIsSupported(ThaModulePoh self)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    if (hwVersion >= ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x4, 0x1007))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 StartVersionSupportSeparateRdiErdiS(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x5, 0x9, 0x1085);
    }

static eBool UpsrIsSupported(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PohInterruptIsSupported(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideTha60210011ModulePoh(AtModule self)
    {
    Tha60210011ModulePoh pohModule = (Tha60210011ModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePohOverride, mMethodsGet(pohModule), sizeof(m_Tha60210011ModulePohOverride));

        mMethodOverride(m_Tha60210011ModulePohOverride, NumberStsSlices);
        mMethodOverride(m_Tha60210011ModulePohOverride, NumberVtSlices);
        mMethodOverride(m_Tha60210011ModulePohOverride, StartVersionSupportSdSfInterrupt);
        mMethodOverride(m_Tha60210011ModulePohOverride, StartVersionSupportBlockModeCounters);
        mMethodOverride(m_Tha60210011ModulePohOverride, PslTtiInterruptIsSupported);
        mMethodOverride(m_Tha60210011ModulePohOverride, StartVersionSupportSeparateRdiErdiS);
        mMethodOverride(m_Tha60210011ModulePohOverride, UpsrIsSupported);
        mMethodOverride(m_Tha60210011ModulePohOverride, PohInterruptIsSupported);
        }

    mMethodsSet(pohModule, &m_Tha60210011ModulePohOverride);
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, AuVcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Tu3VcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Vc1xPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, StartVersionSupportPohMonitorEnabling);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, RegisterIsInRange);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePoh(self);
    OverrideTha60210011ModulePoh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210012ModulePoh);
    }

AtModule Tha60210012ModulePohObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePohObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210012ModulePohNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210012ModulePohObjectInit(newModule, device);
    }


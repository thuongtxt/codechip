/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60290061IpCore.c
 *
 * Created Date: Jun 19, 2018
 *
 * Description : IpCore implementation for the master device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/ThaIpCoreInternal.h"
#include "../../../../../../platform/hal/AtHalSimInternal.h"
#include "Tha60290061Device.h"
#include "Tha60290061IpCore.h"
#include "Tha60290061SubDevice.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)     ((Tha60290061IpCore)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290061IpCore
    {
    tThaIpCorePwV2 super;
    }tTha60290061IpCore;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtIpCoreMethods         m_AtIpCoreOverride;
static tThaIpCoreMethods        m_ThaIpCoreOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;
static const tAtIpCoreMethods  *m_AtIpCoreMethods = NULL;
static const tThaIpCoreMethods *m_ThaIpCoreMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290061IpCore);
    }

static AtHal RealHalGet(AtIpCore self)
    {
    AtDevice parent = AtDeviceParentDeviceGet(AtIpCoreDeviceGet(self));

    /* If a child device, just ask its parent to do that */
    if (parent)
        return AtDeviceIpCoreHalGet(parent, 0);

    return m_AtIpCoreMethods->HalGet(self);
    }

static eBool IsOffloadAddress(uint32 address)
    {
    static const uint32 cOffloadBaseAddress = 0x3F0000UL;
    return mInRange(address, cOffloadBaseAddress, cOffloadBaseAddress + 0x40);
    }

static void DidWriteRegister(AtHal self, void *listener, uint32 address, uint32 value)
    {
    AtHal simHal = listener;
    AtUnused(self);

    /* Do not need to write offload addresses for simulation HAL */
    if (IsOffloadAddress(address))
        return;

    AtHalWrite(simHal, address, value);
    }

static void DidWriteEnable(AtHal self, void *listener, eBool enabled)
    {
    AtHal simHal = listener;
    AtUnused(self);
    AtHalWriteOperationEnable(simHal, enabled);
    }

static void DidReadEnable(AtHal self, void *listener, eBool enabled)
    {
    AtHal simHal = listener;
    AtUnused(self);
    AtHalReadOperationEnable(simHal, enabled);
    }

static tAtHalListener *HalListener(void)
    {
    static tAtHalListener listener;
    static tAtHalListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.DidWriteRegister = DidWriteRegister;
    listener.DidWriteEnable   = DidWriteEnable;
    listener.DidReadEnable    = DidReadEnable;

    pListener = &listener;
    return pListener;
    }

static void RealHalSetWithListener(AtIpCore self, AtHal hal,
                                   const tAtHalListener *listener,
                                   void (*Handler)(AtHal, void*, const tAtHalListener*))
    {
    AtHal realHal = (hal) ? hal : RealHalGet(self);
    AtHal simHal = IpCoreHalGet(self);

    Handler(realHal, simHal, listener);
    }

static eAtRet AllSubCoresHalSet(AtIpCore self)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtIpCoreDeviceGet(self);
    uint8 numSubDevices = AtDeviceNumSubDeviceGet(device);
    uint8 i;

    for (i = 0; i < numSubDevices; i++)
        {
        AtDevice subDevice = AtDeviceSubDeviceGet(device, i);
        AtIpCore subCore = AtDeviceIpCoreGet(subDevice, 0);
        AtHal hal = Tha60290011HalSimNew(cAtHalSimDefaultMemorySizeInMbyte);

        if (!hal)
            return cAtErrorRsrcNoAvail;

        ret = AtIpCoreHalSet(subCore, hal);
        if (ret != cAtOk)
            return ret;
        }

    return ret;
    }

static eAtRet HalSet(AtIpCore self, AtHal hal)
    {
    eAtRet ret;

    ret  = m_AtIpCoreMethods->HalSet(self, hal);
    if (ret != cAtOk)
        return ret;

    return AllSubCoresHalSet(self);
    }

static eBool IsOfParentDevice(AtIpCore self)
    {
    AtDevice device = AtIpCoreDeviceGet(self);
    return AtDeviceParentDeviceGet(device) ? cAtFalse : cAtTrue;
    }

static AtHal SubCoreHalGet(AtIpCore self)
    {
    AtDevice device = AtIpCoreDeviceGet(self);

    if (Tha60290061SubDeviceAsyncRoleGet(device) == cAtDeviceRoleActive)
        return RealHalGet(self);

    return m_AtIpCoreMethods->HalGet(self);
    }

static AtHal HalGet(AtIpCore self)
    {
    if (IsOfParentDevice(self))
        return m_AtIpCoreMethods->HalGet(self);

    return SubCoreHalGet(self);
    }

static eAtRet InterruptEnable(AtIpCore self, eBool enable)
    {
    eAtRet ret = m_AtIpCoreMethods->InterruptEnable(self, enable);

    /* Save interrupt state, to be restored when switching back to standby. */
    if (ret == cAtOk)
        ((ThaIpCore)self)->interruptIsEnabled = enable;

    return ret;
    }

static eAtRet RamSoftReset(ThaIpCore self)
    {
    if (IsOfParentDevice((AtIpCore)self))
        return m_ThaIpCoreMethods->RamSoftReset(self);

    /* Does not grant RAM soft reset for sub-device. It causes lost of some
     * information related to sub-device */
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    }

static void SubCoreHalDelete(AtIpCore self)
    {
    AtHalDelete(self->hal);
    self->hal = NULL;
    }

static void Delete(AtObject self)
    {
    AtIpCore core = (AtIpCore)self;

    if (!IsOfParentDevice(core))
        SubCoreHalDelete(core);

    m_AtObjectMethods->Delete(self);
    }

static const char *ToString(AtObject self)
    {
    static char buffer[64];
    AtDevice device = AtIpCoreDeviceGet((AtIpCore)self);
    AtSnprintf(buffer, sizeof(buffer) - 1, "%s_ipcore", AtObjectToString((AtObject)device));
    return buffer;
    }

static void OverrideThaIpCore(AtIpCore self)
    {
    ThaIpCore core = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreMethods = mMethodsGet(core);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreMethods, sizeof(m_ThaIpCoreOverride));
        mMethodOverride(m_ThaIpCoreOverride, RamSoftReset);
        }

    mMethodsSet(core, &m_ThaIpCoreOverride);
    }

static void OverrideAtIpCore(AtIpCore self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtIpCoreMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtIpCoreOverride, m_AtIpCoreMethods, sizeof(m_AtIpCoreOverride));
        mMethodOverride(m_AtIpCoreOverride, HalSet);
        mMethodOverride(m_AtIpCoreOverride, HalGet);
        mMethodOverride(m_AtIpCoreOverride, InterruptEnable);
        }

    mMethodsSet(self, &m_AtIpCoreOverride);
    }

static void OverrideAtObject(AtIpCore self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtIpCore self)
    {
    OverrideThaIpCore(self);
    OverrideAtIpCore(self);
    OverrideAtObject(self);
    }

static AtIpCore ObjectInit(AtIpCore self, uint8 coreId, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaIpCorePwV2ObjectInit(self, coreId, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIpCore Tha60290061IpCoreNew(uint8 coreId, AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtIpCore newCore = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newCore == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newCore, coreId, device);
    }

void Tha60290061IpCoreHalSet(AtIpCore self, AtHal realHal)
    {
    if (self == NULL)
        return;

    if (realHal)
        RealHalSetWithListener(self, realHal, HalListener(), AtHalListenerAdd);
    else
        {
        ThaIpCore ipCore = (ThaIpCore)self;
        RealHalSetWithListener(self, realHal, HalListener(), AtHalListenerRemove);
        AtIpCoreInterruptEnable((AtIpCore)ipCore->intrController, ipCore->interruptIsEnabled);
        }
    }

AtHal Tha60290061IpCoreHalGet(AtIpCore self)
    {
    if (self)
        return RealHalGet(self);
    return NULL;
    }

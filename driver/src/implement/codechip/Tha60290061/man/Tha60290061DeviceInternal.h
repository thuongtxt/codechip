/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60290061DeviceInternal.h
 * 
 * Created Date: Jun 14, 2018
 *
 * Description : Internal data for the 60291011 device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061DEVICEINTERNAL_H_
#define _THA60290061DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60291011/man/Tha60291011DeviceInternal.h"
#include "Tha60290061Device.h"
#include "Tha60290061SubDeviceDebugger.h"
#include "ddrlookup/Tha60290061DdrLookupTable.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290061DeviceMethods
    {
    AtDevice (*SubDeviceCreate)(Tha60290061Device self, uint8 subDeviceIdx);
    Tha60290061DdrLookupTable (*DdrLookupTableCreate)(Tha60290061Device self);
    }tTha60290061DeviceMethods;

typedef struct tTha60290061Device
    {
    tTha60291011Device super;
    const tTha60290061DeviceMethods *methods;

    /* Private data */
    AtDevice *subDevices;
    uint8 offloadState;
    eBool isInitializing;
    eBool auditEnable;
    Tha60290061DdrLookupTable ddrLookupTable;
    }tTha60290061Device;

typedef struct tTha60290061SubDevice
    {
    tTha60290061Device super;

    /* Private data */
    AtDevice parent;
    uint8 deviceId;
    Tha60290061SubDeviceDebugger debugger;
    }tTha60290061SubDevice;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60290061DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

Tha60290061DdrLookupTable Tha60290061DeviceDdrLookupTableGet(Tha60290061Device self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061DEVICEINTERNAL_H_ */


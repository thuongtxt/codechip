/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60290061Device.c
 *
 * Created Date: Jun 14, 2018
 *
 * Description : Implement logic device for the PDH 1:N protection card
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../../../platform/hal/codechip/Tha60290011HalSimInternal.h"
#include "../ram/Tha60290061ModuleRam.h"
#include "Tha60290061DeviceInternal.h"
#include "Tha60290061DeviceDdrOffload.h"
#include "Tha60290061DeviceGlbReg.h"
#include "Tha60290061DeviceDdrOffloadReg.h"
#include "Tha60290061IpCore.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290061Device)(self))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/
typedef enum eTha60290061OffloadState
    {
    cTha60290061OffloadStateUnknown,
    cTha60290061OffloadStateInProgress,
    cTha60290061OffloadStateDone
    }eTha60290061OffloadState;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290061DeviceMethods  m_methods;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtDeviceMethods             m_AtDeviceOverride;
static tTha60150011DeviceMethods    m_Tha60150011DeviceOverride;
static tTha60290011DeviceMethods    m_Tha60290011DeviceOverride;

/* Save Super Implementation */
static const tAtObjectMethods           *m_AtObjectMethods = NULL;
static const tAtDeviceMethods           *m_AtDeviceMethods = NULL;
static const tTha60150011DeviceMethods  *m_Tha60150011DeviceMethods = NULL;
static const tTha60290011DeviceMethods  *m_Tha60290011DeviceMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice SubDeviceCreate(Tha60290061Device self, uint8 subDeviceIdx)
    {
    return Tha60290061SubDeviceNew((AtDevice)self, subDeviceIdx);
    }

static AtIpCore IpCoreCreate(AtDevice self, uint8 coreId)
    {
    return Tha60290061IpCoreNew(coreId, self);
    }

static uint8 NumSubDevices(AtDevice self)
    {
    AtUnused(self);
    return 5;
    }

static eBool SubDeviceIndexIsValid(AtDevice self, uint32 subDeviceIndex)
    {
    if (subDeviceIndex < AtDeviceNumSubDeviceGet(self))
        return cAtTrue;

    return cAtFalse;
    }

static AtDevice SubDeviceGet(AtDevice self, uint8 subDeviceIndex)
    {
    if (!SubDeviceIndexIsValid(self, subDeviceIndex) || mThis(self)->subDevices == NULL)
        return NULL;

    return mThis(self)->subDevices[subDeviceIndex];
    }

static AtDevice* AllSubDevicesGet(AtDevice self, uint8 *numSubDevices)
    {
    if (numSubDevices)
        *numSubDevices = AtDeviceNumSubDeviceGet(self);

    return mThis(self)->subDevices;
    }

static const char *ToString(AtObject self)
    {
    static char string[64];
    AtDevice dev = (AtDevice)self;

    if (AtDriverMultiDevPrefixApiLogIsEnabled())
        AtSprintf(string, "%s%08x", AtDeviceIdToString(dev), AtDeviceProductCodeGet(dev));
    else
        AtSprintf(string, "device_1_N_%08x", AtDeviceProductCodeGet((AtDevice)self));

    return string;
    }

static const char* DeviceIdToString(AtDevice self)
    {
    static char string[16];

    if (AtDriverMultiDevPrefixApiLogIsEnabled())
        {
        uint32 devid = (uint32)(AtDriverDeviceIndexGet(AtDriverSharedDriverGet(), self) + (uint32)1);
        AtSprintf(string, "mdev_%u_", devid);
        }
    else
        AtSprintf(string, "%s", "");

    return string;
    }

static eAtRet CreateAllSubDevices(AtDevice self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 numSubDevices = AtDeviceNumSubDeviceGet(self);
    AtDevice *subDevices;
    uint8 i, j;

    if (numSubDevices == 0)
        return cAtOk;

    subDevices = mMethodsGet(osal)->MemAlloc(osal, sizeof(AtDevice) * numSubDevices);
    if (!subDevices)
        return cAtErrorRsrcNoAvail;

    for (i = 0; i < numSubDevices; i++)
        {
        subDevices[i] = mMethodsGet(mThis(self))->SubDeviceCreate(mThis(self), i);
        if (subDevices[i] == NULL)
            {
            for (j = 0; j < i; j++)
                AtObjectDelete((AtObject)subDevices[j]);

            /* And memory that hold them */
            mMethodsGet(osal)->MemFree(osal, subDevices);

            return cAtErrorRsrcNoAvail;
            }
        }

    mThis(self)->subDevices = subDevices;
    return cAtOk;
    }

static eAtRet AllSubDevicesSetup(AtDevice self)
    {
    eAtRet ret = cAtOk;
    uint8 numSubDevices;
    uint8 i;

    if (mThis(self)->subDevices == NULL)
        ret = CreateAllSubDevices(self);

    if (ret != cAtOk)
        return ret;

    numSubDevices = AtDeviceNumSubDeviceGet(self);
    for (i = 0; i < numSubDevices; i++)
        {
        AtDevice subDevice = mThis(self)->subDevices[i];

        ret = AtDeviceSetup(subDevice);
        if (ret != cAtOk)
            {
            AtDeviceLog(subDevice, cAtLogLevelCritical,
                        AtSourceLocation, "AtDeviceSetup() failed\r\n");
            return ret;
            }
        }

    return ret;
    }

static eAtRet Setup(AtDevice self)
    {
    eAtRet ret = m_AtDeviceMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    if (AtDeviceNumSubDeviceGet(self))
        ret = AllSubDevicesSetup(self);

    return ret;
    }

static void AllSubDevicesDelete(AtDevice self)
    {
    Tha60290061Device device = (Tha60290061Device)self;
    uint8 numDevices = AtDeviceNumSubDeviceGet(self);
    uint8 idx;

    if (device->subDevices == NULL)
        return;

    for (idx = 0; idx < numDevices; idx++)
        {
        if (device->subDevices[idx])
            {
            AtDevice subDeviceObject = (AtDevice)device->subDevices[idx];

            AtDeviceWillDelete(subDeviceObject);
            AtObjectDelete((AtObject)subDeviceObject);
            device->subDevices[idx] = NULL;
            }
        }

    AtOsalMemFree(device->subDevices);
    }

static void Delete(AtObject self)
    {
    Tha60290061DeviceAuditEnable(mThis(self), cAtFalse);
    AllSubDevicesDelete((AtDevice)self);
    AtObjectDelete((AtObject)mThis(self)->ddrLookupTable);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290061Device object = (Tha60290061Device)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectsWithHandler(subDevices, AtDeviceNumSubDeviceGet((AtDevice)self), AtObjectSerialize);
    mEncodeUInt(offloadState);
    mEncodeUInt(isInitializing);
    mEncodeObjectDescription(ddrLookupTable);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId == cAtModulePdh)   return (AtModule)Tha60290061ModulePdhNew(self);
    if (moduleId == cAtModuleSur)   return (AtModule)Tha60290061ModuleSurNew(self);
    if (moduleId == cAtModuleRam)   return (AtModule)Tha60290061ModuleRamNew(self);
    if (moduleId == cAtModuleEth)   return (AtModule)Tha60290061ModuleEthNew(self);

    /* Internal module */
    if (phyModule == cThaModulePwe)   return Tha60290061ModulePweNew(self);
    if (phyModule == cThaModuleCdr)   return Tha60290061ModuleCdrNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static uint16 ReadOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    if (!SubDeviceIndexIsValid(self, subDeviceId))
        return 0;

    return Tha60290061DeviceReadOnDdrOffload(self, subDeviceId, localAddress, dataBuffer, bufferLen, coreId);
    }

static uint16 WriteOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 regAddress, const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    if (!SubDeviceIndexIsValid(self, subDeviceId))
        return 0;

    return Tha60290061DeviceWriteOnDdrOffload(self, subDeviceId, ddrAddress, regAddress, dataBuffer, bufferLen, coreId);
    }

static void OffloadStart(AtDevice self)
    {
    mThis(self)->offloadState = cTha60290061OffloadStateInProgress;
    }

static void OffloadComplete(AtDevice self)
    {
    mThis(self)->offloadState = cTha60290061OffloadStateDone;
    }

static eBool OffloadIsCompleted(AtDevice self)
    {
    return (mThis(self)->offloadState == cTha60290061OffloadStateDone) ? cAtTrue : cAtFalse;
    }

static eBool OffloadIsInProgress(AtDevice self)
    {
    return (mThis(self)->offloadState == cTha60290061OffloadStateInProgress) ? cAtTrue : cAtFalse;
    }

static eAtRet OffloadReset(AtDevice self)
    {
    const uint32 cIdleSubDevice = 0x7;
    return Tha60290061DeviceDdrOffloadIdSet(self, cIdleSubDevice, 0);
    }

static eAtRet AllSubDevicesInit(AtDevice self)
    {
    uint8 numSubDevices = AtDeviceNumSubDeviceGet(self);
    uint8 i;

    if (numSubDevices == 0)
        return cAtOk;

    for (i = 0; i < numSubDevices; i++)
        {
        eAtRet ret;
        AtDevice subDevice = AtDeviceSubDeviceGet(self, i);

        ret = AtDeviceInit(subDevice);
        if (ret != cAtOk)
            {
            AtDeviceLog(subDevice, cAtLogLevelCritical, AtSourceLocation,
                        "AtDeviceInit() failed %s\r\n", AtRet2String(ret));
            return ret;
            }
        }

    return cAtOk;
    }

static eAtRet AllSubDevicesDeActive(AtDevice self)
    {
    eAtRet ret = cAtOk;
    uint8 numSubDevices = AtDeviceNumSubDeviceGet(self);
    uint8 i;

    if (numSubDevices == 0)
        return cAtOk;

    for (i = 0; i < numSubDevices; i++)
        {
        AtDevice subDevice = AtDeviceSubDeviceGet(self, i);

        /* We assume that initializing the master device shall make any active
         * sub-device fall back to standby role. */
        ret |= AtDeviceRoleSet(subDevice, cAtDeviceRoleStandby);
        }

    if (ret != cAtOk)
        return ret;

    /* Invalidate sub-device selection */
    return OffloadReset(self);
    }

static eAtRet SubPortVlanDefaultSet(ThaModulePw pwModule)
    {
    eAtRet ret = cAtOk;
    ret  = ThaModulePwSubPortVlanInsertionEnable(pwModule, cAtTrue);
    ret |= ThaModulePwSubPortVlanCheckingEnable(pwModule, cAtTrue);
    return ret;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret;

    /* Entering device initialization to free some duplicated operations on
     * its sub-devices. */
    mThis(self)->isInitializing = cAtTrue;

    /* Need to de-activate all sub-device before reset core. Otherwise, the
     * latest device offload ID shall be reset */
    ret = AllSubDevicesDeActive(self);
    if (ret != cAtOk)
        return ret;

    ret = m_AtDeviceMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret = AllSubDevicesInit(self);
    if (ret != cAtOk)
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation,
                    "AllSubDevicesInit() failed\r\n");
        return ret;
        }

    /* Exiting device initialization */
    mThis(self)->isInitializing = cAtFalse;
    mThis(self)->offloadState = 0;

    /* TODO: it is better to move to ThaModulePw->Init() */
    return SubPortVlanDefaultSet((ThaModulePw)AtDeviceModuleGet(self, cAtModulePw));
    }

static void Debug(AtDevice self)
    {
    m_AtDeviceMethods->Debug(self);
    Tha60290061DeviceDdrOffloadSwitchTimeShow(self);
    }

static void HwFlushPda(ThaDevice self)
    {
    ThaDeviceMemoryLongFlush(self, 0x243000, 0x24353f);
    ThaDeviceMemoryFlush(self, 0x244000, 0x24453f, 0);
    ThaDeviceMemoryFlush(self, 0x245000, 0x24553f, 0);
    ThaDeviceMemoryFlush(self, 0x247000, 0x24753f, 0);
    ThaDeviceMemoryFlush(self, 0x24f000, 0x24f53f, 0);
    }

static void HwFlushOcn(ThaDevice self)
    {
    /* Loopback and AIS forcing registers */
    ThaDeviceMemoryFlush(self, 0xA00017, 0xA0001D, 0);

    ThaDeviceMemoryFlush(self, 0xA22000, 0xA2201F, 0);
    ThaDeviceMemoryFlush(self, 0xA23000, 0xA2301F, 0);

    ThaDeviceMemoryFlush(self, 0xA21000, 0xA2101F, 0);
    ThaDeviceMemoryFlush(self, 0xA21040, 0xA2105F, 0);
    ThaDeviceMemoryFlush(self, 0xA24200, 0xA2421F, 0);

    ThaDeviceMemoryFlush(self, 0xA40000, 0xA4001F, 0);
    ThaDeviceMemoryFlush(self, 0xA60000, 0xA6001F, 0);
    ThaDeviceMemoryFlush(self, 0xA40800, 0xA40BFF, 0);
    ThaDeviceMemoryFlush(self, 0xA60800, 0xA60BFF, 0);
    /* Flush the Liu over CEP */
    ThaDeviceMemoryFlush(self, 0xA00010, 0xA00010, 0);
    }

static void HwFlushBer(ThaDevice self)
    {
    ThaDeviceMemoryLongFlush(self, 0xB62000, 0xB622FF);
    }

static void HwFlushPoh(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0xBD0000, 0xBD001F, 0);
    ThaDeviceMemoryFlush(self, 0xB40400, 0xB4041F, 0);

    ThaDeviceMemoryFlush(self, 0xB44000, 0xB442FF, 0);
    ThaDeviceMemoryFlush(self, 0xB28000, 0xB282FF, 0);
    ThaDeviceMemoryFlush(self, 0xBE0000, 0xBE02FF, 0);

    ThaDeviceMemoryFlush(self, 0xB2A000, 0xB2A07F, 0);
    }

static void HwFlushPweForDdrOffload(ThaDevice self)
    {
    /* Psedowire Transmit Global Control: need for DDR offload which could not
     * be default in HW */
    ThaDeviceMemoryFlush(self, 0x320000, 0x320000, 0x0404FFF0);

    /* FMPM Force Reset Core: Bit17 HW reset to 1, SDK should be set it to
     * 1 for ddr offload */
    ThaDeviceMemoryFlush(self, 0x900005, 0x900005, 0x020000);
    }

static void HwFlushDdrOffload(ThaDevice self)
    {
    const uint8 allSubDevicesToFlush = 5;

    if (AtDeviceNumSubDeviceGet((AtDevice)self))
        Tha60290061DeviceDdrOffloadHwFlush((AtDevice)self, allSubDevicesToFlush, 0);
    }

static void HwFlushPmFm(ThaDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    ThaDeviceMemoryFlush(device, 0x918110, 0x91811F, 0x0);
    ThaDeviceMemoryFlush(device, 0x91B800, 0x91B9FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x918090, 0x91809F, 0x0);
    ThaDeviceMemoryFlush(device, 0x91A200, 0x91A3FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x9180B0, 0x9180BF, 0x0);
    ThaDeviceMemoryFlush(device, 0x91AA00, 0x91ABFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x91AE00, 0x91AFFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x928000, 0x92BFFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x91B200, 0x91B3FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x934000, 0x937FFF, 0x0);

    ThaDeviceMemoryFlush(device, 0x91B500, 0x91B529, 0x0);
    ThaDeviceMemoryFlush(device, 0x91E000, 0x91E53F, 0x0);

    ThaDeviceMemoryFlush(device, 0x958110, 0x95811F, 0x0);
    ThaDeviceMemoryFlush(device, 0x95B800, 0x95B9FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x958090, 0x95809F, 0x0);
    ThaDeviceMemoryFlush(device, 0x95A200, 0x95A3FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x9580B0, 0x9580BF, 0x0);
    ThaDeviceMemoryFlush(device, 0x95A800, 0x95A9FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x95AE00, 0x95AFFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x968000, 0x96BFFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x95B200, 0x95B3FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x974000, 0x977FFF, 0x0);

    ThaDeviceMemoryFlush(device, 0x95B500, 0x95B529, 0x0);
    ThaDeviceMemoryFlush(device, 0x95E000, 0x95E53F, 0x0);
    }

static void HwFlush(Tha60150011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    Tha60210031DeviceHwFlushPdhSlice0(device);
    Tha60210031DeviceHwFlushMap0(device);
    Tha60210031DeviceHwFlushCdr0(device);
    Tha60210031DeviceHwFlushPla(device);
    Tha60210031DeviceHwFlushPwe(device);
    HwFlushPda(device);
    Tha60210031DeviceHwFlushPrm(device);
    Tha60210031DeviceHwFlushMdl(device);
    HwFlushOcn(device);
    HwFlushBer(device);
    HwFlushPoh(device);
    Tha60210031DeviceHwFlushRamErrorGenerator(device);
    HwFlushPmFm(device);
    HwFlushPweForDdrOffload(device);
    HwFlushDdrOffload(device);
    }

static eBool TwoDot5GitSerdesPrbsIsRemoved(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 GlobalInterruptMaskRegister(AtDevice self)
    {
    AtUnused(self);
    return cAf6Reg_inten_ctrl_Base;
    }

static uint32 GlobalInterruptStatusRegister(AtDevice self)
    {
    AtUnused(self);
    return cAf6Reg_inten_status_Base;
    }

static uint32 DdrOffloadDoneInterruptStkRegister(AtDevice self)
    {
    AtUnused(self);
    return cAf6Reg_upen_stk_add_wrong_Base + Tha60290061DeviceDdrOffloadBaseAddress(self);
    }

static void  DevAlarmInterruptProcess(AtDevice self)
    {
    uint32 maskAddress = GlobalInterruptMaskRegister(self);
    uint32 statusAddress = GlobalInterruptStatusRegister(self);
    uint32 stkAddress = DdrOffloadDoneInterruptStkRegister(self);
    AtIpCore core0 = AtDeviceIpCoreGet(self, 0);
    AtHal hal = AtIpCoreHalGet(core0);
    uint32 maskVal = AtHalRead(hal, maskAddress);
    uint32 statusVal = AtHalRead(hal, statusAddress);
    uint32 alarm = 0, alarmChange = 0;

    if (maskVal & statusVal & cAf6_inten_ctrl_OFFLOADIntEnable_Mask)
        {
        uint32 interrupStkVal = AtHalRead(hal, stkAddress);
        if (interrupStkVal & cAf6_upen_stk_add_wrong_stk_init_done_Mask)
            {
            AtHalWrite(hal, stkAddress, cAf6_upen_stk_add_wrong_stk_init_done_Mask);
            alarmChange |= cAtDeviceAlarmProtectionSwitched;
            alarm |= cAtDeviceAlarmProtectionSwitched;
            AtDeviceAllAlarmListenersCall(self, alarmChange, alarm);
            }
        }
    }

static uint32 AlarmHistoryGet(AtDevice self)
    {
    uint32 stkAddress = DdrOffloadDoneInterruptStkRegister(self);
    AtIpCore core0 = AtDeviceIpCoreGet(self, 0);
    AtHal hal = AtIpCoreHalGet(core0);
    uint32 interrupStkVal = AtHalRead(hal, stkAddress);

    if (interrupStkVal & cAf6_upen_stk_add_wrong_stk_init_done_Mask)
        return cAtDeviceAlarmProtectionSwitched;

    return 0;
    }

static uint32 AlarmHistoryClear(AtDevice self)
    {
    uint32 stkAddress = DdrOffloadDoneInterruptStkRegister(self);
    AtIpCore core0 = AtDeviceIpCoreGet(self, 0);
    AtHal hal = AtIpCoreHalGet(core0);
    uint32 interrupStkVal = AtHalRead(hal, stkAddress);

    if (interrupStkVal & cAf6_upen_stk_add_wrong_stk_init_done_Mask)
        {
        AtHalWrite(hal, stkAddress, cAf6_upen_stk_add_wrong_stk_init_done_Mask);
        return cAtDeviceAlarmProtectionSwitched;
        }

    return 0;
    }

static uint32 AlarmGet(AtDevice self)
    {
    return AlarmHistoryGet(self);
    }

static eBool AlarmIsSupported(AtDevice self, uint32 alarmType)
    {
    AtUnused(self);

    if (alarmType & cAtDeviceAlarmProtectionSwitched)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 SupportedInterruptMasks(AtDevice self)
    {
    AtUnused(self);
    return cAtDeviceAlarmProtectionSwitched;
    }

static eAtRet InterruptMaskSet(AtDevice self, uint32 alarmMask, uint32 enableMask)
    {
    uint32 maskAddress = GlobalInterruptMaskRegister(self);
    AtIpCore core0 = AtDeviceIpCoreGet(self, 0);
    AtHal hal = AtIpCoreHalGet(core0);
    uint32 maskVal = AtHalRead(hal, maskAddress);
    uint32 enable =  0;

    if (alarmMask & enableMask & cAtDeviceAlarmProtectionSwitched)
        enable = 1;
    else if ((alarmMask & cAtDeviceAlarmProtectionSwitched) & ((enableMask & cAtDeviceAlarmProtectionSwitched) == 0))
        enable = 0;
    else
        return cAtErrorInvalidOperation;

    mRegFieldSet(maskVal, cAf6_inten_ctrl_OFFLOADIntEnable_, enable);
    AtHalWrite(hal, maskAddress, maskVal);
    return cAtOk;
    }

static uint32 InterruptMaskGet(AtDevice self)
    {
    uint32 maskAddress = GlobalInterruptMaskRegister(self);
    AtIpCore core0 = AtDeviceIpCoreGet(self, 0);
    AtHal hal = AtIpCoreHalGet(core0);
    uint32 maskVal = AtHalRead(hal, maskAddress);

    if (maskVal & cAf6_inten_ctrl_OFFLOADIntEnable_Mask)
        return cAtDeviceAlarmProtectionSwitched;
    return 0;
    }

static void InterruptProcess(AtDevice self)
    {
    uint8 numSubDevices = AtDeviceNumSubDeviceGet(self);
    uint8 i;

    /* Process sub-devices */
    for (i = 0; i < numSubDevices; i++)
        {
        AtDevice subDevice = mThis(self)->subDevices[i];
        if (subDevice)
            {
            /* Just need to process the active sub-device only */
            if (mMethodsGet(subDevice)->RoleGet(subDevice) == cAtDeviceRoleActive)
                {
                mMethodsGet(subDevice)->InterruptProcess(subDevice);
                return;
                }
            }
        }

    m_AtDeviceMethods->InterruptProcess(self);
    }

static Tha60290061DdrLookupTable DdrLookupTableCreate(Tha60290061Device self)
    {
    if (Tha60291011Device8ColissionInLookupTableIsSupported((AtDevice)self))
        return Tha60290061DdrLookupTableV2New();

    return Tha60290061DdrLookupTableNew();
    }

static Tha60290061DdrLookupTable DdrLookupTableGet(Tha60290061Device self)
    {
    if (self->ddrLookupTable == NULL)
        self->ddrLookupTable = DdrLookupTableCreate(self);

    return self->ddrLookupTable;
    }

static eBool ShouldEnableLosPpmDetection(Tha60290011Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290061Device);
    }

static void MethodsInit(AtDevice self)
    {
    Tha60290061Device device = (Tha60290061Device)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SubDeviceCreate);
        }

    mMethodsSet(device, &m_methods);
    }

static void OverrideAtObject(AtDevice self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, NumSubDevices);
        mMethodOverride(m_AtDeviceOverride, SubDeviceGet);
        mMethodOverride(m_AtDeviceOverride, AllSubDevicesGet);
        mMethodOverride(m_AtDeviceOverride, IpCoreCreate);
        mMethodOverride(m_AtDeviceOverride, Setup);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, ReadOnDdrOffload);
        mMethodOverride(m_AtDeviceOverride, WriteOnDdrOffload);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, Debug);
        mMethodOverride(m_AtDeviceOverride, DevAlarmInterruptProcess);
        mMethodOverride(m_AtDeviceOverride, AlarmIsSupported);
        mMethodOverride(m_AtDeviceOverride, AlarmGet);
        mMethodOverride(m_AtDeviceOverride, AlarmHistoryGet);
        mMethodOverride(m_AtDeviceOverride, AlarmHistoryClear);
        mMethodOverride(m_AtDeviceOverride, InterruptMaskSet);
        mMethodOverride(m_AtDeviceOverride, InterruptMaskGet);
        mMethodOverride(m_AtDeviceOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtDeviceOverride, InterruptProcess);
        mMethodOverride(m_AtDeviceOverride, DeviceIdToString);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideTha60150011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60150011DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, m_Tha60150011DeviceMethods, sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, HwFlush);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void OverrideTha60290011Device(AtDevice self)
    {
    Tha60290011Device object = (Tha60290011Device)(self);
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290011DeviceMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011DeviceOverride, m_Tha60290011DeviceMethods, sizeof(m_Tha60290011DeviceOverride));

        mMethodOverride(m_Tha60290011DeviceOverride, TwoDot5GitSerdesPrbsIsRemoved);
        mMethodOverride(m_Tha60290011DeviceOverride, ShouldEnableLosPpmDetection);
        }

    mMethodsSet(object, &m_Tha60290011DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtObject(self);
    OverrideAtDevice(self);
    OverrideTha60150011Device(self);
    OverrideTha60290011Device(self);
    }

AtDevice Tha60290061DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60291011DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60290061DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60290061DeviceObjectInit(newDevice, driver, productCode);
    }

void Tha60290061DeviceSubDeviceHwFlush(AtDevice self, uint32 subDeviceId)
    {
    if (Tha60290061DeviceDdrOffloadHwFlush(self, subDeviceId, 0) == cAtOk)
        return;

    AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation,
                "DDR HW flush for offload ID %d failed\r\n", subDeviceId);
    }

void Tha60290061DeviceAuditEnable(Tha60290061Device self, eBool enable)
    {
    mThis(self)->auditEnable = enable;
    }

eBool Tha60290061DeviceAuditIsEnabled(Tha60290061Device self)
    {
    return mThis(self)->auditEnable;
    }

eAtDeviceRole Tha60290061DeviceSubRoleGet(AtDevice self, uint32 subDeviceId)
    {
    uint32 offloadId = Tha60290061DeviceDdrOffloadIdGet(self, 0);

    if (subDeviceId != offloadId)
        return cAtDeviceRoleStandby;

    if (OffloadIsCompleted(self))
        return cAtDeviceRoleActive;

    /* Offload is neither started nor done. */
    if (!OffloadIsInProgress(self))
        return cAtDeviceRoleStandby;

    if (Tha60290061DeviceDdrOffloadCfgLoadIsDone(self, subDeviceId, 0))
        {
        OffloadComplete(self);
        AtDeviceRoleChangeNotify(AtDeviceSubDeviceGet(self, (uint8)subDeviceId), cAtDeviceRoleActive);

        return cAtDeviceRoleActive;
        }

    return cAtDeviceRoleStandby;
    }

eAtRet Tha60290061DeviceSubRoleSet(AtDevice self, uint32 subDeviceId, eAtDeviceRole role)
    {
    eAtRet ret = cAtOk;
    uint32 offloadId = Tha60290061DeviceDdrOffloadIdGet(self, 0);

    if (OffloadIsInProgress(self))
        return cAtErrorDevBusy;

    if (role == cAtDeviceRoleActive)
        {
        /* Need to switch old device to standby priorly */
        if (offloadId != subDeviceId)
            Tha60290061DeviceSubRoleSet(self, offloadId, cAtDeviceRoleStandby);

        OffloadStart(self);
        ret = Tha60290061DeviceDdrOffloadCfgLoad(self, subDeviceId, 0);

        if (AtDeviceInAccessible(self))
            OffloadComplete(self);

        return ret;
        }

    if (role == cAtDeviceRoleStandby)
        {
        if (offloadId != subDeviceId)
            return cAtOk;

        ret = OffloadReset(self);
        AtDeviceRoleChangeNotify(AtDeviceSubDeviceGet(self, (uint8)subDeviceId), role);
        return ret;
        }

    return cAtOk;
    }

Tha60290061DdrLookupTable Tha60290061DeviceDdrLookupTableGet(Tha60290061Device self)
    {
    if (self)
        return DdrLookupTableGet(self);
    return NULL;
    }

eBool Tha60290061DeviceIsInitializing(AtDevice self)
    {
    if (self)
        return mThis(self)->isInitializing;
    return cAtFalse;
    }

eBool Tha60290061DeviceClsVlanIsSupported(Tha60290061Device self)
    {
    return ThaDeviceSupportFeatureFromVersion((AtDevice)self, 0, 3, 0x0001);
    }

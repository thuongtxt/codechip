/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60290061DdrLookupTableCommonCode.h
 * 
 * Created Date: Nov 6, 2018
 *
 * Description : Common code for 60290061 DDR lookup table
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061DDRLOOKUPTABLECOMMONCODE_H_
#define _THA60290061DDRLOOKUPTABLECOMMONCODE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
static tTha60290061DdrAddressEntry* Pmc_Address_DdrOffloadLookup(uint32 reg_address);

/*--------------------------- Entries ----------------------------------------*/
static tTha60290061DdrAddressEntry* Module_Address_DdrOffloadLookup(uint32 reg_address,
                                                                    tTha60290061DdrAddressEntry *lookupArray, uint32 arrayLen)
    {
    uint32 index = 0;

    for (index = 0; index < arrayLen; index++)
        {
        uint32 addressMin = lookupArray[index].regBase;
        uint32 addressMax = addressMin + lookupArray[index].regDepth;
        if (reg_address >= addressMin && reg_address < addressMax)
            return &lookupArray[index];
        }
    return NULL;
    }

static tTha60290061DdrAddressEntry* Ocn_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, ocn_DdrLookupArray, mCount(ocn_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* Poh_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, poh_DdrLookupArray, mCount(poh_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* Pdh_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, pdh_DdrLookupArray, mCount(pdh_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* Map_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, map_DdrLookupArray, mCount(map_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* Cdr_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, cdr_DdrLookupArray, mCount(cdr_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* Pla_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, pla_DdrLookupArray, mCount(pla_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* Pwe_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, pwe_DdrLookupArray, mCount(pwe_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* Cla_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, cla_DdrLookupArray, mCount(cla_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* Pda_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, pda_DdrLookupArray, mCount(pda_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* BertMon_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, bertmon_DdrLookupArray, mCount(bertmon_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* BertGen_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, bertgen_DdrLookupArray, mCount(bertgen_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* BertMonPw_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, bertmonpw_DdrLookupArray, mCount(bertmonpw_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* Prm_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, prm_DdrLookupArray, mCount(prm_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* PdhMux_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, pdhmux_DdrLookupArray, mCount(pdhmux_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* PmFm_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, pmfm_DdrLookupArray, mCount(pmfm_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* Dcc_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, dcc_DdrLookupArray, mCount(dcc_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* Address_To_DdrOffload_Info(uint32 reg_address)
    {
    uint32 moduleAddressMask = reg_address & cBit31_20;

    if (moduleAddressMask == ocn_BaseAddress)
        return Ocn_Address_DdrOffloadLookup(reg_address);
    if (moduleAddressMask == poh_BaseAddress)
        return Poh_Address_DdrOffloadLookup(reg_address);
    if (moduleAddressMask == pdh_BaseAddress)
        return Pdh_Address_DdrOffloadLookup(reg_address);
    if ((reg_address >= map_BaseAddress) && (reg_address < 0x1C0000))
        return Map_Address_DdrOffloadLookup(reg_address);
    if ((reg_address >= cdr_BaseAddress) && (reg_address < 0x2B0000))
        return Cdr_Address_DdrOffloadLookup(reg_address);
    if ((reg_address >= pla_BaseAddress) && (reg_address < 0x210000))
        return Pla_Address_DdrOffloadLookup(reg_address);
    if ((reg_address >= pwe_BaseAddress && reg_address < 0x330000))
        return Pwe_Address_DdrOffloadLookup(reg_address);
    if ((reg_address >= cla_BaseAddress && reg_address < 0x460000))
        return Cla_Address_DdrOffloadLookup(reg_address);
    if (reg_address >= pda_BaseAddress && reg_address < 0x250000)
        return Pda_Address_DdrOffloadLookup(reg_address);
    if (reg_address >= bertgen_BaseAddress && reg_address < 0x360000)
        return BertGen_Address_DdrOffloadLookup(reg_address);
    if (reg_address >= bertmon_BaseAddress && reg_address < 0x370000)
        return BertMon_Address_DdrOffloadLookup(reg_address);
    if (reg_address >= bertmonpw_BaseAddress && reg_address < 0x380000)
        return BertMonPw_Address_DdrOffloadLookup(reg_address);
    if (reg_address >= prm_BaseAddress && reg_address < 0x620000)
        return Prm_Address_DdrOffloadLookup(reg_address);
    if (reg_address >= pdhmux_BaseAddress && reg_address < 0xD00000)
        return PdhMux_Address_DdrOffloadLookup(reg_address);
    if (reg_address >= cls2_5g_BaseAddress && reg_address < 0x490000)
        return NULL;/* CPU load directly*/
    if (reg_address >= pmfm_BaseAddress && reg_address < 0xA00000)
        return PmFm_Address_DdrOffloadLookup(reg_address);
    if (reg_address >= dcc_BaseAddress && reg_address < 0xE00000)
        return Dcc_Address_DdrOffloadLookup(reg_address);
    if (moduleAddressMask == pmc_BaseAddress)
        return Pmc_Address_DdrOffloadLookup(reg_address);
    if (reg_address < 0x200)
        return NULL;/* CPU load directly*/
    return NULL;
    }

static tTha60290061DdrAddressEntry* AddressToDdrOffloadInfo(Tha60290061DdrLookupTable self, uint32 reg_address)
    {
    AtUnused(self);
    return Address_To_DdrOffload_Info(reg_address);
    }

static tTha60290061DdrAddressEntry* GlbEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(glb_DdrLookupArray);
    return glb_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* Cls2Dot5EntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(cls2_5g_DdrLookupArray);
    return cls2_5g_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* OcnEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(ocn_DdrLookupArray);
    return ocn_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* PohEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(poh_DdrLookupArray);
    return poh_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* PdhEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(pdh_DdrLookupArray);
    return pdh_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* MapEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(map_DdrLookupArray);
    return map_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* CdrEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(cdr_DdrLookupArray);
    return cdr_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* PlaEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(pla_DdrLookupArray);
    return pla_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* PweEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(pwe_DdrLookupArray);
    return pwe_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* ClaEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(cla_DdrLookupArray);
    return cla_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* PdaEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(pda_DdrLookupArray);
    return pda_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* BertMonEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(bertmon_DdrLookupArray);
    return bertmon_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* BertGenEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(bertgen_DdrLookupArray);
    return bertgen_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* PrmEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(prm_DdrLookupArray);
    return prm_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* PdhMuxEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(pdhmux_DdrLookupArray);
    return pdhmux_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* PmFmEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(pmfm_DdrLookupArray);
    return pmfm_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* DccEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(dcc_DdrLookupArray);
    return dcc_DdrLookupArray;
    }

static tTha60290061DdrAddressEntry* TopEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(top_DdrLookupArray);
    return top_DdrLookupArray;
    }

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061DDRLOOKUPTABLECOMMONCODE_H_ */


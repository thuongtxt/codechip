/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60290061DdrOffloadLookupTableInternal.h
 * 
 * Created Date: Nov 6, 2018
 *
 * Description : Internal data of the DDR offload lookup table
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061DDROFFLOADLOOKUPTABLEINTERNAL_H_
#define _THA60290061DDROFFLOADLOOKUPTABLEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "../../../../default/man/ThaDeviceInternal.h"
#include "Tha60290061DdrLookupTable.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha60290061DdrLookupTableMethods
    {
    tTha60290061DdrAddressEntry* (*AddressToDdrOffloadInfo)(Tha60290061DdrLookupTable self, uint32 reg_address);
    tTha60290061DdrAddressEntry* (*GlbEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*Cls2Dot5EntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*OcnEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*PohEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*PdhEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*MapEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*CdrEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*PlaEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*PweEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*ClaEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*PdaEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*BertMonEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*BertGenEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*PrmEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*PdhMuxEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*PmFmEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*DccEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*TopEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    tTha60290061DdrAddressEntry* (*PmcEntryArray)(Tha60290061DdrLookupTable self, uint32 *numEntry);
    }tTha60290061DdrLookupTableMethods;

typedef struct tTha60290061DdrLookupTable
    {
    tAtObject super;
    const tTha60290061DdrLookupTableMethods *methods;

    }tTha60290061DdrLookupTable;

typedef struct tTha60290061DdrLookupTableV2
    {
    tTha60290061DdrLookupTable super;

    }tTha60290061DdrLookupTableV2;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60290061DdrLookupTable Tha60290061DdrLookupTableObjectInit(Tha60290061DdrLookupTable self);
Tha60290061DdrLookupTable Tha60290061DdrLookupTableV2ObjectInit(Tha60290061DdrLookupTable self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061DDROFFLOADLOOKUPTABLEINTERNAL_H_ */


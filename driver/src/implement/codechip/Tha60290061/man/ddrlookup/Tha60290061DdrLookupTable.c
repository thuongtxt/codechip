/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60290061DdrLookupTableV1.c
 *
 * Created Date: Nov 6, 2018
 *
 * Description : DDR lookup table for offload
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290061DdrLookupTableInternal.h"
#include "Tha60290061DeviceDdrOffloadLookupV1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290061DdrLookupTableMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60290061DdrLookupTableCommonCode.h"

static tTha60290061DdrAddressEntry* Pmc_Address_DdrOffloadLookup(uint32 reg_address)
    {
    AtUnused(reg_address);
    return NULL;
    }

static tTha60290061DdrAddressEntry* PmcEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = 0;
    return NULL;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290061DdrLookupTable);
    }

static void MethodsInit(Tha60290061DdrLookupTable self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, AddressToDdrOffloadInfo);
        mMethodOverride(m_methods, GlbEntryArray);
        mMethodOverride(m_methods, Cls2Dot5EntryArray);
        mMethodOverride(m_methods, OcnEntryArray);
        mMethodOverride(m_methods, PohEntryArray);
        mMethodOverride(m_methods, PdhEntryArray);
        mMethodOverride(m_methods, MapEntryArray);
        mMethodOverride(m_methods, CdrEntryArray);
        mMethodOverride(m_methods, PlaEntryArray);
        mMethodOverride(m_methods, PweEntryArray);
        mMethodOverride(m_methods, ClaEntryArray);
        mMethodOverride(m_methods, PdaEntryArray);
        mMethodOverride(m_methods, BertMonEntryArray);
        mMethodOverride(m_methods, BertGenEntryArray);
        mMethodOverride(m_methods, PrmEntryArray);
        mMethodOverride(m_methods, PmFmEntryArray);
        mMethodOverride(m_methods, PdhMuxEntryArray);
        mMethodOverride(m_methods, DccEntryArray);
        mMethodOverride(m_methods, TopEntryArray);
        mMethodOverride(m_methods, PmcEntryArray);
        }

    mMethodsSet(self, &m_methods);
    }

Tha60290061DdrLookupTable Tha60290061DdrLookupTableObjectInit(Tha60290061DdrLookupTable self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

Tha60290061DdrLookupTable Tha60290061DdrLookupTableNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60290061DdrLookupTable object = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (object == NULL)
        return NULL;

    /* Construct it */
    return Tha60290061DdrLookupTableObjectInit(object);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableAddressToDdrOffloadInfo(Tha60290061DdrLookupTable self, uint32 reg_address)
    {
    return mMethodsGet(self)->AddressToDdrOffloadInfo(self, reg_address);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableGlbEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->GlbEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableCls2Dot5EntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->Cls2Dot5EntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableOcnEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->OcnEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePohEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->PohEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePdhEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->PdhEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableMapEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->MapEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableCdrEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->CdrEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePlaEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->PlaEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePweEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->PweEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableClaEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->ClaEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePdaEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->PdaEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableBertMonEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->BertMonEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableBertGenEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->BertGenEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePrmEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->PrmEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePdhMuxEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->PdhMuxEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePmFmEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->PmFmEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableDccEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->DccEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableTopEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->TopEntryArray(self, numEntry);
    }

tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePmcEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    return mMethodsGet(self)->PmcEntryArray(self, numEntry);
    }

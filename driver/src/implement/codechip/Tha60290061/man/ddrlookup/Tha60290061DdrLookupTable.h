/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60290061DdrLookupTable.h
 * 
 * Created Date: Nov 6, 2018
 *
 * Description : Interface of the DDR lookup table
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061DDRLOOKUPTABLE_H_
#define _THA60290061DDRLOOKUPTABLE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290061DdrAddressEntry
    {
    uint8 regName[128];
    uint32 ddrAddress;
    uint32 regBase;
    uint32 regDepth;
    uint32 regWidth;
    } tTha60290061DdrAddressEntry;

typedef struct tTha60290061DdrLookupTable *Tha60290061DdrLookupTable;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60290061DdrLookupTable Tha60290061DdrLookupTableNew(void);
Tha60290061DdrLookupTable Tha60290061DdrLookupTableV2New(void);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableAddressToDdrOffloadInfo(Tha60290061DdrLookupTable self, uint32 reg_address);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableGlbEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableCls2Dot5EntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableOcnEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePohEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePdhEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableMapEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableCdrEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePlaEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePweEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableClaEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePdaEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableBertMonEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableBertGenEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePrmEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePdhMuxEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePmFmEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableDccEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTableTopEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);
tTha60290061DdrAddressEntry* Tha60290061DdrLookupTablePmcEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061DDRLOOKUPTABLE_H_ */


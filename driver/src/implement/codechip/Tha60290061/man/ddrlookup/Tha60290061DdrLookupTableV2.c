/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60290061DddrLookupTableV2.c
 *
 * Created Date: Nov 6, 2018
 *
 * Description : Implementation of version 2 DDR lookup due to change in HBCE lookup table control
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290061DdrLookupTableInternal.h"
#include "Tha60290061DeviceDdrOffloadLookupV2.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290061DdrLookupTableMethods m_Tha60290061DdrLookupTableOveride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#include "Tha60290061DdrLookupTableCommonCode.h"

static tTha60290061DdrAddressEntry* Pmc_Address_DdrOffloadLookup(uint32 reg_address)
    {
    return Module_Address_DdrOffloadLookup(reg_address, pmc_DdrLookupArray, mCount(pmc_DdrLookupArray));
    }

static tTha60290061DdrAddressEntry* PmcEntryArray(Tha60290061DdrLookupTable self, uint32 *numEntry)
    {
    AtUnused(self);
    *numEntry = mCount(pmc_DdrLookupArray);
    return pmc_DdrLookupArray;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290061DdrLookupTableV2);
    }

static void OverrideTha60290061DdrLookupTable(Tha60290061DdrLookupTable self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290061DdrLookupTableOveride, mMethodsGet(self), sizeof(m_Tha60290061DdrLookupTableOveride));

        mMethodOverride(m_Tha60290061DdrLookupTableOveride, AddressToDdrOffloadInfo);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, GlbEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, Cls2Dot5EntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, OcnEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, PohEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, PdhEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, MapEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, CdrEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, PlaEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, PweEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, ClaEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, PdaEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, BertMonEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, BertGenEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, PrmEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, PmFmEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, PdhMuxEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, DccEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, TopEntryArray);
        mMethodOverride(m_Tha60290061DdrLookupTableOveride, PmcEntryArray);
        }

    mMethodsSet(self, &m_Tha60290061DdrLookupTableOveride);
    }

static void Override(Tha60290061DdrLookupTable self)
    {
    OverrideTha60290061DdrLookupTable(self);
    }

Tha60290061DdrLookupTable Tha60290061DdrLookupTableV2ObjectInit(Tha60290061DdrLookupTable self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290061DdrLookupTableObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha60290061DdrLookupTable Tha60290061DdrLookupTableV2New(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60290061DdrLookupTable object = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (object == NULL)
        return NULL;

    /* Construct it */
    return Tha60290061DdrLookupTableV2ObjectInit(object);
    }

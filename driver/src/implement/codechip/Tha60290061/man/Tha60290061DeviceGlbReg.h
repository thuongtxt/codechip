/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0061_RD_GLB_H_
#define _AF6_REG_AF6CNC0061_RD_GLB_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Device Version ID
Reg Addr   : 0x00_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicates the module' name and version.

------------------------------------------------------------------------------*/
#define cAf6Reg_VersionID_Base                                                                        0x000000

/*--------------------------------------
BitField Name: Year
BitField Type: RO
BitField Desc: 2013
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_VersionID_Year_Mask                                                                     cBit31_24
#define cAf6_VersionID_Year_Shift                                                                           24

/*--------------------------------------
BitField Name: Week
BitField Type: RO
BitField Desc: Week Synthesized FPGA
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_VersionID_Week_Mask                                                                     cBit23_16
#define cAf6_VersionID_Week_Shift                                                                           16

/*--------------------------------------
BitField Name: Day
BitField Type: RO
BitField Desc: Day Synthesized FPGA
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_VersionID_Day_Mask                                                                       cBit15_8
#define cAf6_VersionID_Day_Shift                                                                             8

/*--------------------------------------
BitField Name: VerID
BitField Type: RO
BitField Desc: FPGA Version
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_VersionID_VerID_Mask                                                                      cBit7_4
#define cAf6_VersionID_VerID_Shift                                                                           4

/*--------------------------------------
BitField Name: NumID
BitField Type: RO
BitField Desc: Number of FPGA Version
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_VersionID_NumID_Mask                                                                      cBit3_0
#define cAf6_VersionID_NumID_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : GLB Sub-Core Active
Reg Addr   : 0x00_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicates the active ports.

------------------------------------------------------------------------------*/
#define cAf6Reg_Active_Base                                                                           0x000001
#define cAf6Reg_Active_WidthVal                                                                             32

/*--------------------------------------
BitField Name: PmcPact
BitField Type: RW
BitField Desc: PMC Active 1: Enable 0: Disable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_Active_PmcPact_Mask                                                                         cBit9
#define cAf6_Active_PmcPact_Shift                                                                            9

/*--------------------------------------
BitField Name: EthPact
BitField Type: RW
BitField Desc: Ethernet Interface Active 1: Enable 0: Disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Active_EthPact_Mask                                                                         cBit8
#define cAf6_Active_EthPact_Shift                                                                            8

/*--------------------------------------
BitField Name: cdrPact
BitField Type: RW
BitField Desc: CDR Active 1: Enable 0: Disable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Active_cdrPact_Mask                                                                         cBit7
#define cAf6_Active_cdrPact_Shift                                                                            7

/*--------------------------------------
BitField Name: claPact
BitField Type: RW
BitField Desc: CLA Active 1: Enable 0: Disable
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Active_claPact_Mask                                                                         cBit6
#define cAf6_Active_claPact_Shift                                                                            6

/*--------------------------------------
BitField Name: PwePact
BitField Type: RW
BitField Desc: PWE Active 1: Enable 0: Disable
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Active_PwePact_Mask                                                                         cBit5
#define cAf6_Active_PwePact_Shift                                                                            5

/*--------------------------------------
BitField Name: PdaPact
BitField Type: RW
BitField Desc: PDA Active 1: Enable 0: Disable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Active_PdaPact_Mask                                                                         cBit4
#define cAf6_Active_PdaPact_Shift                                                                            4

/*--------------------------------------
BitField Name: PlaPact
BitField Type: RW
BitField Desc: PLA Active 1: Enable 0: Disable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Active_PlaPact_Mask                                                                         cBit3
#define cAf6_Active_PlaPact_Shift                                                                            3

/*--------------------------------------
BitField Name: MapPact
BitField Type: RW
BitField Desc: MAP Active 1: Enable 0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Active_MapPact_Mask                                                                         cBit2
#define cAf6_Active_MapPact_Shift                                                                            2

/*--------------------------------------
BitField Name: PdhPact
BitField Type: RW
BitField Desc: PDH Active 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Active_PdhPact_Mask                                                                         cBit1
#define cAf6_Active_PdhPact_Shift                                                                            1

/*--------------------------------------
BitField Name: OcnPact
BitField Type: RW
BitField Desc: OCN Active 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Active_OcnPact_Mask                                                                         cBit0
#define cAf6_Active_OcnPact_Shift                                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Chip Interrupt Status
Reg Addr   : 0x00_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_status_Base                                                                     0x000002

/*--------------------------------------
BitField Name: DCCIntEnable
BitField Type: RW
BitField Desc: DCC Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [31]
--------------------------------------*/
#define cAf6_inten_status_DCCIntEnable_Mask                                                             cBit31
#define cAf6_inten_status_DCCIntEnable_Shift                                                                31

/*--------------------------------------
BitField Name: OFFLOADIntEnable
BitField Type: RW
BitField Desc: DDR OFFLOAD Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [30]
--------------------------------------*/
#define cAf6_inten_status_OFFLOADIntEnable_Mask                                                         cBit30
#define cAf6_inten_status_OFFLOADIntEnable_Shift                                                            30

/*--------------------------------------
BitField Name: CDR2IntEnable
BitField Type: RW
BitField Desc: CDR Group1 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_inten_status_CDR2IntEnable_Mask                                                            cBit29
#define cAf6_inten_status_CDR2IntEnable_Shift                                                               29

/*--------------------------------------
BitField Name: CDR1IntEnable
BitField Type: RW
BitField Desc: CDR Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [28]
--------------------------------------*/
#define cAf6_inten_status_CDR1IntEnable_Mask                                                            cBit28
#define cAf6_inten_status_CDR1IntEnable_Shift                                                               28

/*--------------------------------------
BitField Name: SysMonTempEnable
BitField Type: RW
BitField Desc: SysMon temperature Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [27]
--------------------------------------*/
#define cAf6_inten_status_SysMonTempEnable_Mask                                                         cBit27
#define cAf6_inten_status_SysMonTempEnable_Shift                                                            27

/*--------------------------------------
BitField Name: ParIntEnable
BitField Type: RW
BitField Desc: RAM Parity Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [26]
--------------------------------------*/
#define cAf6_inten_status_ParIntEnable_Mask                                                             cBit26
#define cAf6_inten_status_ParIntEnable_Shift                                                                26

/*--------------------------------------
BitField Name: MDLIntEnable
BitField Type: RW
BitField Desc: MDL DE3 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [25]
--------------------------------------*/
#define cAf6_inten_status_MDLIntEnable_Mask                                                             cBit25
#define cAf6_inten_status_MDLIntEnable_Shift                                                                25

/*--------------------------------------
BitField Name: PRMIntEnable
BitField Type: RW
BitField Desc: PRM DE1 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [24]
--------------------------------------*/
#define cAf6_inten_status_PRMIntEnable_Mask                                                             cBit24
#define cAf6_inten_status_PRMIntEnable_Shift                                                                24

/*--------------------------------------
BitField Name: Mac10gIntEnable
BitField Type: RW
BitField Desc: Two MAC10G Interrupt Enable  1: Set  0: Disable
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_inten_status_Mac10gIntEnable_Mask                                                       cBit23_22
#define cAf6_inten_status_Mac10gIntEnable_Shift                                                             22

/*--------------------------------------
BitField Name: PMIntEnable
BitField Type: RW
BitField Desc: PM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [21]
--------------------------------------*/
#define cAf6_inten_status_PMIntEnable_Mask                                                              cBit21
#define cAf6_inten_status_PMIntEnable_Shift                                                                 21

/*--------------------------------------
BitField Name: FMIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_inten_status_FMIntEnable_Mask                                                              cBit20
#define cAf6_inten_status_FMIntEnable_Shift                                                                 20

/*--------------------------------------
BitField Name: SEUIntEnble
BitField Type: RW
BitField Desc: SEU Interrupt Alarm
BitField Bits: [19]
--------------------------------------*/
#define cAf6_inten_status_SEUIntEnble_Mask                                                              cBit19
#define cAf6_inten_status_SEUIntEnble_Shift                                                                 19

/*--------------------------------------
BitField Name: EthIntEnable
BitField Type: RW
BitField Desc: ETH Port Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_inten_status_EthIntEnable_Mask                                                             cBit18
#define cAf6_inten_status_EthIntEnable_Shift                                                                18

/*--------------------------------------
BitField Name: StacntIntEnable
BitField Type: RW
BitField Desc: Stacnt  Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_inten_status_StacntIntEnable_Mask                                                          cBit17
#define cAf6_inten_status_StacntIntEnable_Shift                                                             17

/*--------------------------------------
BitField Name: PWIntEnable
BitField Type: RW
BitField Desc: PW Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_inten_status_PWIntEnable_Mask                                                              cBit16
#define cAf6_inten_status_PWIntEnable_Shift                                                                 16

/*--------------------------------------
BitField Name: DE1Grp1IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_inten_status_DE1Grp1IntEnable_Mask                                                         cBit13
#define cAf6_inten_status_DE1Grp1IntEnable_Shift                                                            13

/*--------------------------------------
BitField Name: DE3Grp1IntEnable
BitField Type: RW
BitField Desc: PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_inten_status_DE3Grp1IntEnable_Mask                                                         cBit12
#define cAf6_inten_status_DE3Grp1IntEnable_Shift                                                            12

/*--------------------------------------
BitField Name: DE1Grp0IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_inten_status_DE1Grp0IntEnable_Mask                                                          cBit9
#define cAf6_inten_status_DE1Grp0IntEnable_Shift                                                             9

/*--------------------------------------
BitField Name: DE3Grp0IntEnable
BitField Type: RW
BitField Desc: PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_inten_status_DE3Grp0IntEnable_Mask                                                          cBit8
#define cAf6_inten_status_DE3Grp0IntEnable_Shift                                                             8

/*--------------------------------------
BitField Name: OCNVTIntEnable
BitField Type: RW
BitField Desc: OCN VT Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_inten_status_OCNVTIntEnable_Mask                                                            cBit2
#define cAf6_inten_status_OCNVTIntEnable_Shift                                                               2

/*--------------------------------------
BitField Name: OCNSTSIntEnable
BitField Type: RW
BitField Desc: OCN STS Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_inten_status_OCNSTSIntEnable_Mask                                                           cBit1
#define cAf6_inten_status_OCNSTSIntEnable_Shift                                                              1

/*--------------------------------------
BitField Name: OCNLineIntEnable
BitField Type: RW
BitField Desc: OCN Line Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_inten_status_OCNLineIntEnable_Mask                                                          cBit0
#define cAf6_inten_status_OCNLineIntEnable_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Chip Interrupt Enable Control
Reg Addr   : 0x00_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_ctrl_Base                                                                       0x000003

/*--------------------------------------
BitField Name: DCCIntEnable
BitField Type: RW
BitField Desc: DCC Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [31]
--------------------------------------*/
#define cAf6_inten_ctrl_DCCIntEnable_Mask                                                               cBit31
#define cAf6_inten_ctrl_DCCIntEnable_Shift                                                                  31

/*--------------------------------------
BitField Name: OFFLOADIntEnable
BitField Type: RW
BitField Desc: DDR OFFLOAD Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [30]
--------------------------------------*/
#define cAf6_inten_ctrl_OFFLOADIntEnable_Mask                                                           cBit30
#define cAf6_inten_ctrl_OFFLOADIntEnable_Shift                                                              30

/*--------------------------------------
BitField Name: CDR2IntEnable
BitField Type: RW
BitField Desc: CDR Group1 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_inten_ctrl_CDR2IntEnable_Mask                                                              cBit29
#define cAf6_inten_ctrl_CDR2IntEnable_Shift                                                                 29

/*--------------------------------------
BitField Name: CDR1IntEnable
BitField Type: RW
BitField Desc: CDR Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [28]
--------------------------------------*/
#define cAf6_inten_ctrl_CDR1IntEnable_Mask                                                              cBit28
#define cAf6_inten_ctrl_CDR1IntEnable_Shift                                                                 28

/*--------------------------------------
BitField Name: SysMonTempEnable
BitField Type: RW
BitField Desc: SysMon temperature Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [27]
--------------------------------------*/
#define cAf6_inten_ctrl_SysMonTempEnable_Mask                                                           cBit27
#define cAf6_inten_ctrl_SysMonTempEnable_Shift                                                              27

/*--------------------------------------
BitField Name: ParIntEnable
BitField Type: RW
BitField Desc: RAM Parity Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [26]
--------------------------------------*/
#define cAf6_inten_ctrl_ParIntEnable_Mask                                                               cBit26
#define cAf6_inten_ctrl_ParIntEnable_Shift                                                                  26

/*--------------------------------------
BitField Name: MDLIntEnable
BitField Type: RW
BitField Desc: MDL DE3 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [25]
--------------------------------------*/
#define cAf6_inten_ctrl_MDLIntEnable_Mask                                                               cBit25
#define cAf6_inten_ctrl_MDLIntEnable_Shift                                                                  25

/*--------------------------------------
BitField Name: PRMIntEnable
BitField Type: RW
BitField Desc: PRM DE1 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [24]
--------------------------------------*/
#define cAf6_inten_ctrl_PRMIntEnable_Mask                                                               cBit24
#define cAf6_inten_ctrl_PRMIntEnable_Shift                                                                  24

/*--------------------------------------
BitField Name: Mac10gIntEnable
BitField Type: RW
BitField Desc: Two Mac10G Interrupt Alarm  1: Set  0: Disable
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_inten_ctrl_Mac10gIntEnable_Mask                                                         cBit23_22
#define cAf6_inten_ctrl_Mac10gIntEnable_Shift                                                               22

/*--------------------------------------
BitField Name: PMIntEnable
BitField Type: RW
BitField Desc: PM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [21]
--------------------------------------*/
#define cAf6_inten_ctrl_PMIntEnable_Mask                                                                cBit21
#define cAf6_inten_ctrl_PMIntEnable_Shift                                                                   21

/*--------------------------------------
BitField Name: FMIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_inten_ctrl_FMIntEnable_Mask                                                                cBit20
#define cAf6_inten_ctrl_FMIntEnable_Shift                                                                   20

/*--------------------------------------
BitField Name: SEUIntEnble
BitField Type: RW
BitField Desc: SEU Interrupt Alarm
BitField Bits: [19]
--------------------------------------*/
#define cAf6_inten_ctrl_SEUIntEnble_Mask                                                                cBit19
#define cAf6_inten_ctrl_SEUIntEnble_Shift                                                                   19

/*--------------------------------------
BitField Name: EthIntEnable
BitField Type: RW
BitField Desc: ETH Port Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_inten_ctrl_EthIntEnable_Mask                                                               cBit18
#define cAf6_inten_ctrl_EthIntEnable_Shift                                                                  18

/*--------------------------------------
BitField Name: StacntIntEnable
BitField Type: RW
BitField Desc: Stacnt Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_inten_ctrl_StacntIntEnable_Mask                                                            cBit17
#define cAf6_inten_ctrl_StacntIntEnable_Shift                                                               17

/*--------------------------------------
BitField Name: PWIntEnable
BitField Type: RW
BitField Desc: PW Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_inten_ctrl_PWIntEnable_Mask                                                                cBit16
#define cAf6_inten_ctrl_PWIntEnable_Shift                                                                   16

/*--------------------------------------
BitField Name: DE1Grp1IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_inten_ctrl_DE1Grp1IntEnable_Mask                                                           cBit13
#define cAf6_inten_ctrl_DE1Grp1IntEnable_Shift                                                              13

/*--------------------------------------
BitField Name: DE3Grp1IntEnable
BitField Type: RW
BitField Desc: PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_inten_ctrl_DE3Grp1IntEnable_Mask                                                           cBit12
#define cAf6_inten_ctrl_DE3Grp1IntEnable_Shift                                                              12

/*--------------------------------------
BitField Name: DE1Grp0IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_inten_ctrl_DE1Grp0IntEnable_Mask                                                            cBit9
#define cAf6_inten_ctrl_DE1Grp0IntEnable_Shift                                                               9

/*--------------------------------------
BitField Name: DE3Grp0IntEnable
BitField Type: RW
BitField Desc: PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_inten_ctrl_DE3Grp0IntEnable_Mask                                                            cBit8
#define cAf6_inten_ctrl_DE3Grp0IntEnable_Shift                                                               8

/*--------------------------------------
BitField Name: OCNVTIntEnable
BitField Type: RW
BitField Desc: OCN VT Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_inten_ctrl_OCNVTIntEnable_Mask                                                              cBit2
#define cAf6_inten_ctrl_OCNVTIntEnable_Shift                                                                 2

/*--------------------------------------
BitField Name: OCNSTSIntEnable
BitField Type: RW
BitField Desc: OCN STS Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_inten_ctrl_OCNSTSIntEnable_Mask                                                             cBit1
#define cAf6_inten_ctrl_OCNSTSIntEnable_Shift                                                                1

/*--------------------------------------
BitField Name: OCNLineIntEnable
BitField Type: RW
BitField Desc: OCN Line Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_inten_ctrl_OCNLineIntEnable_Mask                                                            cBit0
#define cAf6_inten_ctrl_OCNLineIntEnable_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Chip Interrupt Restore Control
Reg Addr   : 0x00_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt restore enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_restore_Base                                                                    0x000004

/*--------------------------------------
BitField Name: DCCIntEnable
BitField Type: RW
BitField Desc: DCC Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [31]
--------------------------------------*/
#define cAf6_inten_restore_DCCIntEnable_Mask                                                            cBit31
#define cAf6_inten_restore_DCCIntEnable_Shift                                                               31

/*--------------------------------------
BitField Name: CDR2IntEnable
BitField Type: RW
BitField Desc: CDR Group1 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [29]
--------------------------------------*/
#define cAf6_inten_restore_CDR2IntEnable_Mask                                                           cBit29
#define cAf6_inten_restore_CDR2IntEnable_Shift                                                              29

/*--------------------------------------
BitField Name: CDR1IntEnable
BitField Type: RW
BitField Desc: CDR Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [28]
--------------------------------------*/
#define cAf6_inten_restore_CDR1IntEnable_Mask                                                           cBit28
#define cAf6_inten_restore_CDR1IntEnable_Shift                                                              28

/*--------------------------------------
BitField Name: SysMonTempEnable
BitField Type: RW
BitField Desc: SysMon temperature Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [27]
--------------------------------------*/
#define cAf6_inten_restore_SysMonTempEnable_Mask                                                        cBit27
#define cAf6_inten_restore_SysMonTempEnable_Shift                                                           27

/*--------------------------------------
BitField Name: ParIntEnable
BitField Type: RW
BitField Desc: RAM Parity Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [26]
--------------------------------------*/
#define cAf6_inten_restore_ParIntEnable_Mask                                                            cBit26
#define cAf6_inten_restore_ParIntEnable_Shift                                                               26

/*--------------------------------------
BitField Name: MDLIntEnable
BitField Type: RW
BitField Desc: MDL DE3 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [25]
--------------------------------------*/
#define cAf6_inten_restore_MDLIntEnable_Mask                                                            cBit25
#define cAf6_inten_restore_MDLIntEnable_Shift                                                               25

/*--------------------------------------
BitField Name: PRMIntEnable
BitField Type: RW
BitField Desc: PRM DE1 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [24]
--------------------------------------*/
#define cAf6_inten_restore_PRMIntEnable_Mask                                                            cBit24
#define cAf6_inten_restore_PRMIntEnable_Shift                                                               24

/*--------------------------------------
BitField Name: Mac10gIntEnable
BitField Type: RW
BitField Desc: Two Mac10G Interrupt Enable  1: Set  0: Disable
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_inten_restore_Mac10gIntEnable_Mask                                                      cBit23_22
#define cAf6_inten_restore_Mac10gIntEnable_Shift                                                            22

/*--------------------------------------
BitField Name: PMIntEnable
BitField Type: RW
BitField Desc: PM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [21]
--------------------------------------*/
#define cAf6_inten_restore_PMIntEnable_Mask                                                             cBit21
#define cAf6_inten_restore_PMIntEnable_Shift                                                                21

/*--------------------------------------
BitField Name: FMIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_inten_restore_FMIntEnable_Mask                                                             cBit20
#define cAf6_inten_restore_FMIntEnable_Shift                                                                20

/*--------------------------------------
BitField Name: EthIntEnable
BitField Type: RW
BitField Desc: ETH Port Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_inten_restore_EthIntEnable_Mask                                                            cBit18
#define cAf6_inten_restore_EthIntEnable_Shift                                                               18

/*--------------------------------------
BitField Name: PWIntEnable
BitField Type: RW
BitField Desc: PW Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_inten_restore_PWIntEnable_Mask                                                             cBit16
#define cAf6_inten_restore_PWIntEnable_Shift                                                                16

/*--------------------------------------
BitField Name: DE1Grp1IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [13]
--------------------------------------*/
#define cAf6_inten_restore_DE1Grp1IntEnable_Mask                                                        cBit13
#define cAf6_inten_restore_DE1Grp1IntEnable_Shift                                                           13

/*--------------------------------------
BitField Name: DE3Grp1IntEnable
BitField Type: RW
BitField Desc: PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [12]
--------------------------------------*/
#define cAf6_inten_restore_DE3Grp1IntEnable_Mask                                                        cBit12
#define cAf6_inten_restore_DE3Grp1IntEnable_Shift                                                           12

/*--------------------------------------
BitField Name: DE1Grp0IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_inten_restore_DE1Grp0IntEnable_Mask                                                         cBit9
#define cAf6_inten_restore_DE1Grp0IntEnable_Shift                                                            9

/*--------------------------------------
BitField Name: DE3Grp0IntEnable
BitField Type: RW
BitField Desc: PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_inten_restore_DE3Grp0IntEnable_Mask                                                         cBit8
#define cAf6_inten_restore_DE3Grp0IntEnable_Shift                                                            8

/*--------------------------------------
BitField Name: OCNVTIntEnable
BitField Type: RW
BitField Desc: OCN VT Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_inten_restore_OCNVTIntEnable_Mask                                                           cBit2
#define cAf6_inten_restore_OCNVTIntEnable_Shift                                                              2

/*--------------------------------------
BitField Name: OCNSTSIntEnable
BitField Type: RW
BitField Desc: OCN STS Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_inten_restore_OCNSTSIntEnable_Mask                                                          cBit1
#define cAf6_inten_restore_OCNSTSIntEnable_Shift                                                             1

/*--------------------------------------
BitField Name: OCNLineIntEnable
BitField Type: RW
BitField Desc: OCN Line Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_inten_restore_OCNLineIntEnable_Mask                                                         cBit0
#define cAf6_inten_restore_OCNLineIntEnable_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Chip Interrupt PIN Control
Reg Addr   : 0x00_0005
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt PIN enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_PIN_Base                                                                        0x000005

/*--------------------------------------
BitField Name: DefectPINEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_inten_PIN_DefectPINEnable_Mask                                                              cBit0
#define cAf6_inten_PIN_DefectPINEnable_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : EPort Interrupt Enable Control
Reg Addr   : 0x00_00C0
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_Eport_Base                                                                      0x0000C0

/*--------------------------------------
BitField Name: XFI_3
BitField Type: RW
BitField Desc: XFI_3    Interrupt Enable  1: Enable  0: Disable
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_inten_Eport_XFI_3_Mask                                                                     cBit19
#define cAf6_inten_Eport_XFI_3_Shift                                                                        19

/*--------------------------------------
BitField Name: XFI_2
BitField Type: RW
BitField Desc: XFI_2    Interrupt Enable  1: Enable  0: Disable
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_inten_Eport_XFI_2_Mask                                                                     cBit18
#define cAf6_inten_Eport_XFI_2_Shift                                                                        18

/*--------------------------------------
BitField Name: XFI_1
BitField Type: RW
BitField Desc: XFI_1    Interrupt Enable  1: Enable  0: Disable
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_inten_Eport_XFI_1_Mask                                                                     cBit17
#define cAf6_inten_Eport_XFI_1_Shift                                                                        17

/*--------------------------------------
BitField Name: XFI_0
BitField Type: RW
BitField Desc: XFI_0    Interrupt Enable  1: Enable  0: Disable
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_inten_Eport_XFI_0_Mask                                                                     cBit16
#define cAf6_inten_Eport_XFI_0_Shift                                                                        16

/*--------------------------------------
BitField Name: QSGMII_3
BitField Type: RW
BitField Desc: QSGMII_3 Interrupt Enable  1: Enable  0: Disable
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_inten_Eport_QSGMII_3_Mask                                                                  cBit12
#define cAf6_inten_Eport_QSGMII_3_Shift                                                                     12

/*--------------------------------------
BitField Name: QSGMII_2
BitField Type: RW
BitField Desc: QSGMII_2 Interrupt Enable  1: Enable  0: Disable
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_inten_Eport_QSGMII_2_Mask                                                                   cBit8
#define cAf6_inten_Eport_QSGMII_2_Shift                                                                      8

/*--------------------------------------
BitField Name: QSGMII_1
BitField Type: RW
BitField Desc: QSGMII_1 Interrupt Enable  1: Enable  0: Disable
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_inten_Eport_QSGMII_1_Mask                                                                   cBit4
#define cAf6_inten_Eport_QSGMII_1_Shift                                                                      4

/*--------------------------------------
BitField Name: QSGMII_0
BitField Type: RW
BitField Desc: QSGMII_0 Interrupt Enable  1: Enable  0: Disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_inten_Eport_QSGMII_0_Mask                                                                   cBit0
#define cAf6_inten_Eport_QSGMII_0_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : EPort Link Event
Reg Addr   : 0x00_00C4
Reg Formula: 
    Where  : 
Reg Desc   : 
This register report state change of link status.

------------------------------------------------------------------------------*/
#define cAf6Reg_linkalm_Eport_Base                                                                    0x0000C4

/*--------------------------------------
BitField Name: XFI_3
BitField Type: WC
BitField Desc: XFI_3    Link State change Event  1: Changed  0: Normal
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_linkalm_Eport_XFI_3_Mask                                                                   cBit19
#define cAf6_linkalm_Eport_XFI_3_Shift                                                                      19

/*--------------------------------------
BitField Name: XFI_2
BitField Type: WC
BitField Desc: XFI_2    Link State change Event  1: Changed  0: Normal
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_linkalm_Eport_XFI_2_Mask                                                                   cBit18
#define cAf6_linkalm_Eport_XFI_2_Shift                                                                      18

/*--------------------------------------
BitField Name: XFI_1
BitField Type: WC
BitField Desc: XFI_1    Link State change Event  1: Changed  0: Normal
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_linkalm_Eport_XFI_1_Mask                                                                   cBit17
#define cAf6_linkalm_Eport_XFI_1_Shift                                                                      17

/*--------------------------------------
BitField Name: XFI_0
BitField Type: WC
BitField Desc: XFI_0    Link State change Event  1: Changed  0: Normal
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_linkalm_Eport_XFI_0_Mask                                                                   cBit16
#define cAf6_linkalm_Eport_XFI_0_Shift                                                                      16

/*--------------------------------------
BitField Name: QSGMII_3
BitField Type: WC
BitField Desc: QSGMII_3 Link State change Event  1: Changed  0: Normal
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_linkalm_Eport_QSGMII_3_Mask                                                                cBit12
#define cAf6_linkalm_Eport_QSGMII_3_Shift                                                                   12

/*--------------------------------------
BitField Name: QSGMII_2
BitField Type: WC
BitField Desc: QSGMII_2 Link State change Event  1: Changed  0: Normal
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_linkalm_Eport_QSGMII_2_Mask                                                                 cBit8
#define cAf6_linkalm_Eport_QSGMII_2_Shift                                                                    8

/*--------------------------------------
BitField Name: QSGMII_1
BitField Type: WC
BitField Desc: QSGMII_1 Link State change Event  1: Changed  0: Normal
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_linkalm_Eport_QSGMII_1_Mask                                                                 cBit4
#define cAf6_linkalm_Eport_QSGMII_1_Shift                                                                    4

/*--------------------------------------
BitField Name: QSGMII_0
BitField Type: WC
BitField Desc: QSGMII_0 Link State change Event  1: Changed  0: Normal
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_linkalm_Eport_QSGMII_0_Mask                                                                 cBit0
#define cAf6_linkalm_Eport_QSGMII_0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : EPort Link Status
Reg Addr   : 0x00_00C8
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_linksta_Eport_Base                                                                    0x0000C8

/*--------------------------------------
BitField Name: XFI_3
BitField Type: RO
BitField Desc: XFI_3 Link Status  1: Up  0: Down
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_linksta_Eport_XFI_3_Mask                                                                   cBit19
#define cAf6_linksta_Eport_XFI_3_Shift                                                                      19

/*--------------------------------------
BitField Name: XFI_2
BitField Type: RO
BitField Desc: XFI_2 Link Status  1: Up  0: Down
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_linksta_Eport_XFI_2_Mask                                                                   cBit18
#define cAf6_linksta_Eport_XFI_2_Shift                                                                      18

/*--------------------------------------
BitField Name: XFI_1
BitField Type: RO
BitField Desc: XFI_1 Link Status  1: Up  0: Down
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_linksta_Eport_XFI_1_Mask                                                                   cBit17
#define cAf6_linksta_Eport_XFI_1_Shift                                                                      17

/*--------------------------------------
BitField Name: XFI_0
BitField Type: RO
BitField Desc: XFI_0 Link Status  1: Up  0: Down
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_linksta_Eport_XFI_0_Mask                                                                   cBit16
#define cAf6_linksta_Eport_XFI_0_Shift                                                                      16

/*--------------------------------------
BitField Name: QSGMII_3
BitField Type: RO
BitField Desc: QSGMII_3 Link Status 1: Up  0: Down
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_linksta_Eport_QSGMII_3_Mask                                                                cBit12
#define cAf6_linksta_Eport_QSGMII_3_Shift                                                                   12

/*--------------------------------------
BitField Name: QSGMII_2
BitField Type: RO
BitField Desc: QSGMII_2 Link Status 1: Up  0: Down
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_linksta_Eport_QSGMII_2_Mask                                                                 cBit8
#define cAf6_linksta_Eport_QSGMII_2_Shift                                                                    8

/*--------------------------------------
BitField Name: QSGMII_1
BitField Type: RO
BitField Desc: QSGMII_1 Link Status 1: Up  0: Down
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_linksta_Eport_QSGMII_1_Mask                                                                 cBit4
#define cAf6_linksta_Eport_QSGMII_1_Shift                                                                    4

/*--------------------------------------
BitField Name: QSGMII_0
BitField Type: RO
BitField Desc: QSGMII_0 Link Status 1: Up  0: Down
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_linksta_Eport_QSGMII_0_Mask                                                                 cBit0
#define cAf6_linksta_Eport_QSGMII_0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : EPort Link Force
Reg Addr   : 0x00_00CC
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_linkfrc_Eport_Base                                                                    0x0000CC

/*--------------------------------------
BitField Name: XFI_3
BitField Type: RO
BitField Desc: XFI_3 Link Force  1: Up  0: Down
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_linkfrc_Eport_XFI_3_Mask                                                                   cBit19
#define cAf6_linkfrc_Eport_XFI_3_Shift                                                                      19

/*--------------------------------------
BitField Name: XFI_2
BitField Type: RO
BitField Desc: XFI_2 Link Force  1: Up  0: Down
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_linkfrc_Eport_XFI_2_Mask                                                                   cBit18
#define cAf6_linkfrc_Eport_XFI_2_Shift                                                                      18

/*--------------------------------------
BitField Name: XFI_1
BitField Type: RO
BitField Desc: XFI_1 Link Force  1: Up  0: Down
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_linkfrc_Eport_XFI_1_Mask                                                                   cBit17
#define cAf6_linkfrc_Eport_XFI_1_Shift                                                                      17

/*--------------------------------------
BitField Name: XFI_0
BitField Type: RO
BitField Desc: XFI_0 Link Force  1: Up  0: Down
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_linkfrc_Eport_XFI_0_Mask                                                                   cBit16
#define cAf6_linkfrc_Eport_XFI_0_Shift                                                                      16

/*--------------------------------------
BitField Name: QSGMII_3
BitField Type: RO
BitField Desc: QSGMII_3 Link Force 1: Up  0: Down
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_linkfrc_Eport_QSGMII_3_Mask                                                                cBit12
#define cAf6_linkfrc_Eport_QSGMII_3_Shift                                                                   12

/*--------------------------------------
BitField Name: QSGMII_2
BitField Type: RO
BitField Desc: QSGMII_2 Link Force 1: Up  0: Down
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_linkfrc_Eport_QSGMII_2_Mask                                                                 cBit8
#define cAf6_linkfrc_Eport_QSGMII_2_Shift                                                                    8

/*--------------------------------------
BitField Name: QSGMII_1
BitField Type: RO
BitField Desc: QSGMII_1 Link Force 1: Up  0: Down
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_linkfrc_Eport_QSGMII_1_Mask                                                                 cBit4
#define cAf6_linkfrc_Eport_QSGMII_1_Shift                                                                    4

/*--------------------------------------
BitField Name: QSGMII_0
BitField Type: RO
BitField Desc: QSGMII_0 Link Force 1: Up  0: Down
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_linkfrc_Eport_QSGMII_0_Mask                                                                 cBit0
#define cAf6_linkfrc_Eport_QSGMII_0_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Write Hold Data63_32
Reg Addr   : 0x00_0011
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to write dword #2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_wr_hold_63_32_Base                                                                    0x000011

/*--------------------------------------
BitField Name: WrHold63_32
BitField Type: RW
BitField Desc: The write dword #2
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_wr_hold_63_32_WrHold63_32_Mask                                                           cBit31_0
#define cAf6_wr_hold_63_32_WrHold63_32_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Write Hold Data95_64
Reg Addr   : 0x00_0012
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to write dword #3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_wr_hold_95_64_Base                                                                    0x000012

/*--------------------------------------
BitField Name: WrHold95_64
BitField Type: RW
BitField Desc: The write dword #3
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_wr_hold_95_64_WrHold95_64_Mask                                                           cBit31_0
#define cAf6_wr_hold_95_64_WrHold95_64_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Write Hold Data127_96
Reg Addr   : 0x00_0013
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to write dword #4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_wr_hold_127_96_Base                                                                   0x000013

/*--------------------------------------
BitField Name: WrHold127_96
BitField Type: RW
BitField Desc: The write dword #4
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_wr_hold_127_96_WrHold127_96_Mask                                                         cBit31_0
#define cAf6_wr_hold_127_96_WrHold127_96_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read Hold Data63_32
Reg Addr   : 0x00_0021
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to Read dword #2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rd_hold_63_32_Base                                                                    0x000021

/*--------------------------------------
BitField Name: rdHold63_32
BitField Type: RW
BitField Desc: The Read dword #2
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rd_hold_63_32_rdHold63_32_Mask                                                           cBit31_0
#define cAf6_rd_hold_63_32_rdHold63_32_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Read Hold Data95_64
Reg Addr   : 0x00_0022
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to Read dword #3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rd_hold_95_64_Base                                                                    0x000022

/*--------------------------------------
BitField Name: rdHold95_64
BitField Type: RW
BitField Desc: The Read dword #3
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rd_hold_95_64_rdHold95_64_Mask                                                           cBit31_0
#define cAf6_rd_hold_95_64_rdHold95_64_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Read Hold Data127_96
Reg Addr   : 0x00_0023
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to Read dword #4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rd_hold_127_96_Base                                                                   0x000023

/*--------------------------------------
BitField Name: rdHold127_96
BitField Type: RW
BitField Desc: The Read dword #4
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rd_hold_127_96_rdHold127_96_Mask                                                         cBit31_0
#define cAf6_rd_hold_127_96_rdHold127_96_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Mac Address Bit31_0 Control
Reg Addr   : 0x00_0100
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 31 to 0 of MAC address.

------------------------------------------------------------------------------*/
#define cAf6Reg_mac_add_31_0_ctrl_Base                                                                0x000100

/*--------------------------------------
BitField Name: MacAddress31_0
BitField Type: RW
BitField Desc: Bit 31 to 0 of MAC Address. Bit 31 is MSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mac_add_31_0_ctrl_MacAddress31_0_Mask                                                    cBit31_0
#define cAf6_mac_add_31_0_ctrl_MacAddress31_0_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Mac Address Bit47_32 Control
Reg Addr   : 0x00_0101
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 47to 32 of MAC address.

------------------------------------------------------------------------------*/
#define cAf6Reg_mac_add_47_32_ctrl_Base                                                               0x000101

/*--------------------------------------
BitField Name: MacAddress47_32
BitField Type: RW
BitField Desc: Bit 47 to 32 of MAC Address. Bit 47 is MSB bit
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_MacAddress47_32_Mask                                                  cBit15_0
#define cAf6_mac_add_47_32_ctrl_MacAddress47_32_Shift                                                        0

/*--------------------------------------
BitField Name: PWLoop
BitField Type: RW
BitField Desc: Local PW loopback, from PWE to CLA, 0:Dis; 1:Enb
BitField Bits: [20]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_PWLoop_Mask                                                             cBit20
#define cAf6_mac_add_47_32_ctrl_PWLoop_Shift                                                                20

/*--------------------------------------
BitField Name: CDRDDRDis
BitField Type: RW
BitField Desc: CDR access DDR disable, 0:Enb; 1:Dis
BitField Bits: [22]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_CDRDDRDis_Mask                                                          cBit22
#define cAf6_mac_add_47_32_ctrl_CDRDDRDis_Shift                                                             22

/*--------------------------------------
BitField Name: PMFMDDRDis
BitField Type: RW
BitField Desc: PMFM access DDR enable, 0:Enb; 1:Dis
BitField Bits: [23]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_PMFMDDRDis_Mask                                                         cBit23
#define cAf6_mac_add_47_32_ctrl_PMFMDDRDis_Shift                                                            23

/*--------------------------------------
BitField Name: GeMode
BitField Type: RW
BitField Desc: GE mode 0:XGMII; 1:GMII
BitField Bits: [26]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_GeMode_Mask                                                             cBit26
#define cAf6_mac_add_47_32_ctrl_GeMode_Shift                                                                26

/*--------------------------------------
BitField Name: PDHLoopOut
BitField Type: RW
BitField Desc: PDH parallel loopback
BitField Bits: [27]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_PDHLoopOut_Mask                                                         cBit27
#define cAf6_mac_add_47_32_ctrl_PDHLoopOut_Shift                                                            27

/*--------------------------------------
BitField Name: PDHGlbMode
BitField Type: RW
BitField Desc: PDH global mode for loopback, 0:DS3; 1:E3
BitField Bits: [28]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_PDHGlbMode_Mask                                                         cBit28
#define cAf6_mac_add_47_32_ctrl_PDHGlbMode_Shift                                                            28

/*--------------------------------------
BitField Name: DCCEnMode
BitField Type: RW
BitField Desc: enable DCC interface via 1G port, it just effect when traffic in
XGMII mode
BitField Bits: [29]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_DCCEnMode_Mask                                                          cBit29
#define cAf6_mac_add_47_32_ctrl_DCCEnMode_Shift                                                             29

/*--------------------------------------
BitField Name: POHDDRDis
BitField Type: RW
BitField Desc: POH access DDR disable, 0:Enb; 1:Dis
BitField Bits: [30]
--------------------------------------*/
#define cAf6_mac_add_47_32_ctrl_POHDDRDis_Mask                                                          cBit30
#define cAf6_mac_add_47_32_ctrl_POHDDRDis_Shift                                                             30


/*------------------------------------------------------------------------------
Reg Name   : IP Address Bit31_0 Control
Reg Addr   : 0x00_0102
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 31 to 0 of IP address.

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_add_31_0_ctrl_Base                                                                 0x000102

/*--------------------------------------
BitField Name: IpAddress31_0
BitField Type: RW
BitField Desc: Bit 31 to 0 of IP Address. Bit 31 is MSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ip_add_31_0_ctrl_IpAddress31_0_Mask                                                      cBit31_0
#define cAf6_ip_add_31_0_ctrl_IpAddress31_0_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : IP Address Bit63_32 Control
Reg Addr   : 0x00_0103
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 63to 32 of IP address.

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_add_63_32_ctrl_Base                                                                0x000103

/*--------------------------------------
BitField Name: IpAddress63_32
BitField Type: RW
BitField Desc: Bit 63 to 32 of IP Address. Bit 63 is MSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ip_add_63_32_ctrl_IpAddress63_32_Mask                                                    cBit31_0
#define cAf6_ip_add_63_32_ctrl_IpAddress63_32_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : IP Address Bit95_64 Control
Reg Addr   : 0x00_0104
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 95 to 64 of IP address.

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_add_95_64_ctrl_Base                                                                0x000104

/*--------------------------------------
BitField Name: IpAddress95_64
BitField Type: RW
BitField Desc: Bit 95 to 64 of IP Address. Bit 95 is MSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ip_add_95_64_ctrl_IpAddress95_64_Mask                                                    cBit31_0
#define cAf6_ip_add_95_64_ctrl_IpAddress95_64_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : IP Address Bit127_96 Control
Reg Addr   : 0x00_0105
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 127 to 96 of IP address.

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_add_127_96_ctrl_Base                                                               0x000105

/*--------------------------------------
BitField Name: IpAddress127_96
BitField Type: RW
BitField Desc: Bit 127 to 96 of IP Address. Bit 127 is MSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ip_add_127_96_ctrl_IpAddress127_96_Mask                                                  cBit31_0
#define cAf6_ip_add_127_96_ctrl_IpAddress127_96_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : CDR REF OUT 8KHZ Control
Reg Addr   : 0x00_010c
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get the status of RX NCO engine

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_refout_8khz_ctrl_Base                                                             0x00010c

/*--------------------------------------
BitField Name: Cdr1RefMaskLosDis
BitField Type: RW
BitField Desc: : 0: enable mask out8k in case of LOS 1: disable mask out8k in
case of LOS
BitField Bits: [31]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefMaskLosDis_Mask                                                cBit31
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefMaskLosDis_Shift                                                   31

/*--------------------------------------
BitField Name: Cdr1RefID
BitField Type: RW
BitField Desc: CDR engine ID (in CDR mode) or Rx LIU ID (in loop mode,
bit[10:5]) that used for 8Khz ref out
BitField Bits: [30:20]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefID_Mask                                                     cBit30_20
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefID_Shift                                                           20

/*--------------------------------------
BitField Name: Cdr1RefDs1
BitField Type: RW
BitField Desc: 0: Line ID used for 8KHZ ref out is E3 mode 1: Line ID used for
8KHZ ref out is DS3 mode 2: Line ID used for 8KHZ ref out is EC1 mode
BitField Bits: [19:18]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefDs1_Mask                                                    cBit19_18
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefDs1_Shift                                                          18

/*--------------------------------------
BitField Name: Cdr1RefSelect
BitField Type: RW
BitField Desc: : 0: System mode, used system clock to generate 8Khz ref out 1:
Loop timing mode,  used rx LIU to generate 8Khz ref out 2: CDR timing mode,
used clock from CDR engine to generate 8KHZ out. CDR clock can be
ACR/DCR/Loop/System/Ext1/...
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefSelect_Mask                                                 cBit17_16
#define cAf6_cdr_refout_8khz_ctrl_Cdr1RefSelect_Shift                                                       16

/*--------------------------------------
BitField Name: Cdr0RefMaskLosDis
BitField Type: RW
BitField Desc: : 0: enable mask out8k in case of LOS 1: disable mask out8k in
case of LOS
BitField Bits: [15]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefMaskLosDis_Mask                                                cBit15
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefMaskLosDis_Shift                                                   15

/*--------------------------------------
BitField Name: Cdr0RefID
BitField Type: RW
BitField Desc: CDR engine ID (in CDR mode) or Rx LIU ID (in loop mode,
bit[10:5]) that used for 8Khz ref out
BitField Bits: [14:4]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefID_Mask                                                      cBit14_4
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefID_Shift                                                            4

/*--------------------------------------
BitField Name: Cdr0RefDs1
BitField Type: RW
BitField Desc: : 0: Line ID used for 8KHZ ref out is E3 mode 1: Line ID used for
8KHZ ref out is DS3 mode 2: Line ID used for 8KHZ ref out is EC1 mode
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefDs1_Mask                                                      cBit3_2
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefDs1_Shift                                                           2

/*--------------------------------------
BitField Name: Cdr0RefSelect
BitField Type: RW
BitField Desc: 0: System mode, used system clock to generate 8Khz ref out 1:
Loop timing mode,  used rx LIU  to generate 8Khz ref out 2: CDR timing mode,
used clock from CDR engine to generate 8KHZ out. CDR clock can be
ACR/DCR/Loop/System/Ext1/...
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefSelect_Mask                                                   cBit1_0
#define cAf6_cdr_refout_8khz_ctrl_Cdr0RefSelect_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : RAM Parity Force Control
Reg Addr   : 0x130
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Parity_Force_Control_Base                                                            0x130

/*--------------------------------------
BitField Name: DDRCDR_ParErrFrc
BitField Type: RW
BitField Desc: Force parity error for CDR DDR
BitField Bits: [87]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DDRCDR_ParErrFrc_Mask                                             cBit23
#define cAf6_RAM_Parity_Force_Control_DDRCDR_ParErrFrc_Shift                                                23

/*--------------------------------------
BitField Name: DDRTxEccNonCorErr_ErrFrc
BitField Type: RW
BitField Desc: Force ECC non-correctable error for Tx DDR
BitField Bits: [86]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DDRTxEccNonCorErr_ErrFrc_Mask                                     cBit22
#define cAf6_RAM_Parity_Force_Control_DDRTxEccNonCorErr_ErrFrc_Shift                                        22

/*--------------------------------------
BitField Name: DDRTxEccCorErr_ErrFrc
BitField Type: RW
BitField Desc: Force ECC correctable error for Tx DDR
BitField Bits: [85]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DDRTxEccCorErr_ErrFrc_Mask                                        cBit21
#define cAf6_RAM_Parity_Force_Control_DDRTxEccCorErr_ErrFrc_Shift                                           21

/*--------------------------------------
BitField Name: DDRRxEccNonCorErr_ErrFrc
BitField Type: RW
BitField Desc: Force ECC non-correctable error for Rx DDR
BitField Bits: [84]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DDRRxEccNonCorErr_ErrFrc_Mask                                     cBit20
#define cAf6_RAM_Parity_Force_Control_DDRRxEccNonCorErr_ErrFrc_Shift                                        20

/*--------------------------------------
BitField Name: DDRRxEccCorErr_ErrFrc
BitField Type: RW
BitField Desc: Force ECC correctable error for Rx DDR
BitField Bits: [83]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DDRRxEccCorErr_ErrFrc_Mask                                        cBit19
#define cAf6_RAM_Parity_Force_Control_DDRRxEccCorErr_ErrFrc_Shift                                           19

/*--------------------------------------
BitField Name: CLASoftwaveCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Softwave Control"
BitField Bits: [82]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLASoftwaveCtl_ParErrFrc_Mask                                     cBit18
#define cAf6_RAM_Parity_Force_Control_CLASoftwaveCtl_ParErrFrc_Shift                                        18

/*--------------------------------------
BitField Name: CDRPweLookupCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR Pseudowire Look Up Control"
BitField Bits: [81]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRPweLookupCtl_ParErrFrc_Mask                                    cBit17
#define cAf6_RAM_Parity_Force_Control_CDRPweLookupCtl_ParErrFrc_Shift                                       17

/*--------------------------------------
BitField Name: CLAPerGrpEnbCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Classify Per Group Enable Control"
BitField Bits: [80]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAPerGrpEnbCtl_ParErrFrc_Mask                                    cBit16
#define cAf6_RAM_Parity_Force_Control_CLAPerGrpEnbCtl_ParErrFrc_Shift                                       16

/*--------------------------------------
BitField Name: CLAMefOverMplsCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Classify Pseudowire MEF over MPLS
Control"
BitField Bits: [79]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAMefOverMplsCtl_ParErrFrc_Mask                                  cBit15
#define cAf6_RAM_Parity_Force_Control_CLAMefOverMplsCtl_ParErrFrc_Shift                                      15

/*--------------------------------------
BitField Name: CLAPerPweTypeCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Classify Per Pseudowire Type
Control"
BitField Bits: [78]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAPerPweTypeCtl_ParErrFrc_Mask                                   cBit14
#define cAf6_RAM_Parity_Force_Control_CLAPerPweTypeCtl_ParErrFrc_Shift                                      14

/*--------------------------------------
BitField Name: CLAHbceLookupInfCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Classify HBCE Looking Up
Information Control"
BitField Bits: [77]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAHbceLookupInfCtl_ParErrFrc_Mask                                  cBit13
#define cAf6_RAM_Parity_Force_Control_CLAHbceLookupInfCtl_ParErrFrc_Shift                                      13

/*--------------------------------------
BitField Name: CLAHbceHashTab1Ctl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Classify HBCE Hashing Table
Control" PageID 1
BitField Bits: [76]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab1Ctl_ParErrFrc_Mask                                  cBit12
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab1Ctl_ParErrFrc_Shift                                      12

/*--------------------------------------
BitField Name: CLAHbceHashTab0Ctl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Classify HBCE Hashing Table
Control" PageID 0
BitField Bits: [75]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab0Ctl_ParErrFrc_Mask                                  cBit11
#define cAf6_RAM_Parity_Force_Control_CLAHbceHashTab0Ctl_ParErrFrc_Shift                                      11

/*--------------------------------------
BitField Name: PWETxHspwGrpProCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit HSPW Group
Protection Control"
BitField Bits: [74]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxHspwGrpProCtl_ParErrFrc_Mask                                  cBit10
#define cAf6_RAM_Parity_Force_Control_PWETxHspwGrpProCtl_ParErrFrc_Shift                                      10

/*--------------------------------------
BitField Name: PWETxUpsaGrpEnbCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit UPSR Group
Enable Control"
BitField Bits: [73]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaGrpEnbCtl_ParErrFrc_Mask                                   cBit9
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaGrpEnbCtl_ParErrFrc_Shift                                       9

/*--------------------------------------
BitField Name: PWETxUpsaHspwModCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit UPSR and HSPW
mode Control"
BitField Bits: [72]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaHspwModCtl_ParErrFrc_Mask                                   cBit8
#define cAf6_RAM_Parity_Force_Control_PWETxUpsaHspwModCtl_ParErrFrc_Shift                                       8

/*--------------------------------------
BitField Name: PWETxHspwLabelCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit HSPW Label
Control"
BitField Bits: [71]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxHspwLabelCtl_ParErrFrc_Mask                                   cBit7
#define cAf6_RAM_Parity_Force_Control_PWETxHspwLabelCtl_ParErrFrc_Shift                                       7

/*--------------------------------------
BitField Name: PWETxHdrRtpSsrcCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit Header RTP SSRC
Value Control "
BitField Bits: [70]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxHdrRtpSsrcCtl_ParErrFrc_Mask                                   cBit6
#define cAf6_RAM_Parity_Force_Control_PWETxHdrRtpSsrcCtl_ParErrFrc_Shift                                       6

/*--------------------------------------
BitField Name: PWETxEthHdrLenCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit Ethernet Header
Length Control"
BitField Bits: [69]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrLenCtl_ParErrFrc_Mask                                   cBit5
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrLenCtl_ParErrFrc_Shift                                       5

/*--------------------------------------
BitField Name: PWETxEthHdrValCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit Ethernet Header
Value Control"
BitField Bits: [68]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrValCtl_ParErrFrc_Mask                                   cBit4
#define cAf6_RAM_Parity_Force_Control_PWETxEthHdrValCtl_ParErrFrc_Shift                                       4

/*--------------------------------------
BitField Name: PWETxEnbCtl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire Transmit Enable Control"
BitField Bits: [67]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWETxEnbCtl_ParErrFrc_Mask                                         cBit3
#define cAf6_RAM_Parity_Force_Control_PWETxEnbCtl_ParErrFrc_Shift                                            3

/*--------------------------------------
BitField Name: PDAddrCrc_ErrFrc
BitField Type: RW
BitField Desc: Force check DDR CRC error
BitField Bits: [66]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDAddrCrc_ErrFrc_Mask                                              cBit2
#define cAf6_RAM_Parity_Force_Control_PDAddrCrc_ErrFrc_Shift                                                 2

/*--------------------------------------
BitField Name: PDATdmMode_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire PDA TDM Mode Control"
BitField Bits: [65]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDATdmMode_ParErrFrc_Mask                                          cBit1
#define cAf6_RAM_Parity_Force_Control_PDATdmMode_ParErrFrc_Shift                                             1

/*--------------------------------------
BitField Name: PDAJitbuf_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire PDA Jitter Buffer
Control"
BitField Bits: [64]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDAJitbuf_ParErrFrc_Mask                                           cBit0
#define cAf6_RAM_Parity_Force_Control_PDAJitbuf_ParErrFrc_Shift                                              0

/*--------------------------------------
BitField Name: PDAReor_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Pseudowire PDA Reorder Control"
BitField Bits: [63]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDAReor_ParErrFrc_Mask                                            cBit31
#define cAf6_RAM_Parity_Force_Control_PDAReor_ParErrFrc_Shift                                               31

/*--------------------------------------
BitField Name: PLAPaylSizeSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa PDHPW Payload Assemble
Payload Size Control"SliceId 1
BitField Bits: [62]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc1_ParErrFrc_Mask                                    cBit30
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc1_ParErrFrc_Shift                                       30

/*--------------------------------------
BitField Name: PLAPaylSizeSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa PDHPW Payload Assemble
Payload Size Control"SliceId 0
BitField Bits: [61]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc0_ParErrFrc_Mask                                    cBit29
#define cAf6_RAM_Parity_Force_Control_PLAPaylSizeSlc0_ParErrFrc_Shift                                       29

/*--------------------------------------
BitField Name: CDRACRTimingCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR ACR Engine Timing control"
SliceId 1
BitField Bits: [60]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice1_Mask                                  cBit28
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice1_Shift                                      28

/*--------------------------------------
BitField Name: CDRVTTimingCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR VT Timing control" SliceId 1
BitField Bits: [59]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice1_Mask                                  cBit27
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice1_Shift                                      27

/*--------------------------------------
BitField Name: CDRSTSTimingCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR STS Timing control" SliceId 1
BitField Bits: [58]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice1_Mask                                  cBit26
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice1_Shift                                      26

/*--------------------------------------
BitField Name: CDRACRTimingCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR ACR Engine Timing control"
SliceId 0
BitField Bits: [57]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice0_Mask                                  cBit25
#define cAf6_RAM_Parity_Force_Control_CDRACRTimingCtrl_ParErrFrc_Slice0_Shift                                      25

/*--------------------------------------
BitField Name: CDRVTTimingCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR VT Timing control" SliceId 0
BitField Bits: [56]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice0_Mask                                  cBit24
#define cAf6_RAM_Parity_Force_Control_CDRVTTimingCtrl_ParErrFrc_Slice0_Shift                                      24

/*--------------------------------------
BitField Name: CDRSTSTimingCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "CDR STS Timing control" SliceId 0
BitField Bits: [55]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice0_Mask                                  cBit23
#define cAf6_RAM_Parity_Force_Control_CDRSTSTimingCtrl_ParErrFrc_Slice0_Shift                                      23

/*--------------------------------------
BitField Name: DeMapChlCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "DEMAP Thalassa Demap Channel
Control" SliceId 1
BitField Bits: [54]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice1_Mask                                  cBit22
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice1_Shift                                      22

/*--------------------------------------
BitField Name: DeMapChlCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "DEMAP Thalassa Demap Channel
Control" SliceId 0
BitField Bits: [53]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice0_Mask                                  cBit21
#define cAf6_RAM_Parity_Force_Control_DeMapChlCtrl_ParErrFrc_Slice0_Shift                                      21

/*--------------------------------------
BitField Name: MAPLineCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "MAP Thalassa Map Line Control"
SliceId 1
BitField Bits: [52]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice1_Mask                                  cBit20
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice1_Shift                                      20

/*--------------------------------------
BitField Name: MAPChlCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "MAP Thalassa Map Channel Control"
SliceId 1
BitField Bits: [51]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice1_Mask                                  cBit19
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice1_Shift                                      19

/*--------------------------------------
BitField Name: MAPLineCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "MAP Thalassa Map Line Control"
SliceId 0
BitField Bits: [50]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice0_Mask                                  cBit18
#define cAf6_RAM_Parity_Force_Control_MAPLineCtrl_ParErrFrc_Slice0_Shift                                      18

/*--------------------------------------
BitField Name: MAPChlCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "MAP Thalassa Map Channel Control"
SliceId 0
BitField Bits: [49]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice0_Mask                                  cBit17
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrl_ParErrFrc_Slice0_Shift                                      17

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH VT Async Map Control" SliceId 1
BitField Bits: [48]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice1_Mask                                  cBit16
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice1_Shift                                      16

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH STS/VT Map Control" SliceId 1
BitField Bits: [47]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice1_Mask                                  cBit15
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice1_Shift                                      15

/*--------------------------------------
BitField Name: PDHTxM23E23Trace_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"
SliceId 1
BitField Bits: [46]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice1_Mask                                  cBit14
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice1_Shift                                      14

/*--------------------------------------
BitField Name: PDHTxM23E23Ctrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM23E23 Control" SliceId 1
BitField Bits: [45]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice1_Mask                                  cBit13
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice1_Shift                                      13

/*--------------------------------------
BitField Name: PDHTxM12E12Ctrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM12E12 Control" SliceId 1
BitField Bits: [44]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice1_Mask                                  cBit12
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice1_Shift                                      12

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling
Control" SliceId 1
BitField Bits: [43]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice1_Mask                                  cBit11
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice1_Shift                                      11

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"
SliceId 1
BitField Bits: [42]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice1_Mask                                  cBit10
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice1_Shift                                      10

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"
SliceId 1
BitField Bits: [41]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice1_Mask                                   cBit9
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice1_Shift                                       9

/*--------------------------------------
BitField Name: PDHRxM12E12Ctrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxM12E12 Control" SliceId 1
BitField Bits: [40]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice1_Mask                                   cBit8
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice1_Shift                                       8

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxDS3E3 OH Pro Control" SliceId
1
BitField Bits: [39]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice1_Mask                                   cBit7
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice1_Shift                                       7

/*--------------------------------------
BitField Name: PDHRxM23E23Ctrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxM23E23 Control" SliceId 1
BitField Bits: [38]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice1_Mask                                   cBit6
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice1_Shift                                       6

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH STS/VT Demap Control" SliceId 1
BitField Bits: [37]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice1_Mask                                   cBit5
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice1_Shift                                       5

/*--------------------------------------
BitField Name: PDHMuxCtrl_ParErrFrc_Slice1
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control" SliceId 1
BitField Bits: [36]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice1_Mask                                   cBit4
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice1_Shift                                       4

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH VT Async Map Control" SliceId 0
BitField Bits: [35]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice0_Mask                                   cBit3
#define cAf6_RAM_Parity_Force_Control_PDHVTAsyncMapCtrl_ParErrFrc_Slice0_Shift                                       3

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH STS/VT Map Control" SliceId 0
BitField Bits: [34]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice0_Mask                                   cBit2
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTMapCtrl_ParErrFrc_Slice0_Shift                                       2

/*--------------------------------------
BitField Name: PDHTxM23E23Trace_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"
SliceId 0
BitField Bits: [33]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice0_Mask                                   cBit1
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Trace_ParErrFrc_Slice0_Shift                                       1

/*--------------------------------------
BitField Name: PDHTxM23E23Ctrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM23E23 Control" SliceId 0
BitField Bits: [32]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice0_Mask                                cBit32_0
#define cAf6_RAM_Parity_Force_Control_PDHTxM23E23Ctrl_ParErrFrc_Slice0_Shift                                       0

/*--------------------------------------
BitField Name: PDHTxM12E12Ctrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH TxM12E12 Control" SliceId 0
BitField Bits: [31]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice0_Mask                                  cBit31
#define cAf6_RAM_Parity_Force_Control_PDHTxM12E12Ctrl_ParErrFrc_Slice0_Shift                                      31

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling
Control" SliceId 0
BitField Bits: [30]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice0_Mask                                  cBit30
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1SigCtrl_ParErrFrc_Slice0_Shift                                      30

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"
SliceId 0
BitField Bits: [29]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice0_Mask                                  cBit29
#define cAf6_RAM_Parity_Force_Control_PDHTxDE1FrmCtrl_ParErrFrc_Slice0_Shift                                      29

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"
SliceId 0
BitField Bits: [28]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice0_Mask                                  cBit28
#define cAf6_RAM_Parity_Force_Control_PDHRxDE1FrmCtrl_ParErrFrc_Slice0_Shift                                      28

/*--------------------------------------
BitField Name: PDHRxM12E12Ctrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxM12E12 Control" SliceId 0
BitField Bits: [27]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice0_Mask                                  cBit27
#define cAf6_RAM_Parity_Force_Control_PDHRxM12E12Ctrl_ParErrFrc_Slice0_Shift                                      27

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxDS3E3 OH Pro Control" SliceId
0
BitField Bits: [26]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice0_Mask                                  cBit26
#define cAf6_RAM_Parity_Force_Control_PDHRxDS3E3OHCtrl_ParErrFrc_Slice0_Shift                                      26

/*--------------------------------------
BitField Name: PDHRxM23E23Ctrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH RxM23E23 Control" SliceId 0
BitField Bits: [25]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice0_Mask                                  cBit25
#define cAf6_RAM_Parity_Force_Control_PDHRxM23E23Ctrl_ParErrFrc_Slice0_Shift                                      25

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH STS/VT Demap Control" SliceId 0
BitField Bits: [24]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice0_Mask                                  cBit24
#define cAf6_RAM_Parity_Force_Control_PDHSTSVTDeMapCtrl_ParErrFrc_Slice0_Shift                                      24

/*--------------------------------------
BitField Name: PDHMuxCtrl_ParErrFrc_Slice0
BitField Type: RW
BitField Desc: Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control" SliceId 0
BitField Bits: [23]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice0_Mask                                  cBit23
#define cAf6_RAM_Parity_Force_Control_PDHMuxCtrl_ParErrFrc_Slice0_Shift                                      23

/*--------------------------------------
BitField Name: POHWrCtrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "POH BER Control VT/DSN", "POH BER
Control STS/TU3"
BitField Bits: [22]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_POHWrCtrl_ParErrFrc_Mask                                          cBit22
#define cAf6_RAM_Parity_Force_Control_POHWrCtrl_ParErrFrc_Shift                                             22

/*--------------------------------------
BitField Name: POHWrTrsh_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "POH BER Threshold 2"
BitField Bits: [21]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_POHWrTrsh_ParErrFrc_Mask                                          cBit21
#define cAf6_RAM_Parity_Force_Control_POHWrTrsh_ParErrFrc_Shift                                             21

/*--------------------------------------
BitField Name: POHTerCtrlLo_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "POH Termintate Insert Control
VT/TU3"
BitField Bits: [20]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlLo_ParErrFrc_Mask                                       cBit20
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlLo_ParErrFrc_Shift                                          20

/*--------------------------------------
BitField Name: POHTerCtrlHi_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "POH Termintate Insert Control STS"
BitField Bits: [19]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlHi_ParErrFrc_Mask                                       cBit19
#define cAf6_RAM_Parity_Force_Control_POHTerCtrlHi_ParErrFrc_Shift                                          19

/*--------------------------------------
BitField Name: POHCpeVtCtr_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "POH CPE VT Control Register"
BitField Bits: [18]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_POHCpeVtCtr_ParErrFrc_Mask                                        cBit18
#define cAf6_RAM_Parity_Force_Control_POHCpeVtCtr_ParErrFrc_Shift                                           18

/*--------------------------------------
BitField Name: POHCpeStsCtr_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "POH CPE STS/TU3 Control Register"
BitField Bits: [17]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_POHCpeStsCtr_ParErrFrc_Mask                                       cBit17
#define cAf6_RAM_Parity_Force_Control_POHCpeStsCtr_ParErrFrc_Shift                                          17

/*--------------------------------------
BitField Name: OCNTohCtlSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN TOH Monitoring Per Channel
Control" SliceId 1
BitField Bits: [16]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNTohCtlSlc1_ParErrFrc_Mask                                      cBit16
#define cAf6_RAM_Parity_Force_Control_OCNTohCtlSlc1_ParErrFrc_Shift                                         16

/*--------------------------------------
BitField Name: OCNSpiCtlSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN STS Pointer Interpreter Per
Channel Control" SliceId 1
BitField Bits: [15]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc1_ParErrFrc_Mask                                      cBit15
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc1_ParErrFrc_Shift                                         15

/*--------------------------------------
BitField Name: OCNSpiCtlSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN STS Pointer Interpreter Per
Channel Control" SliceId 0
BitField Bits: [14]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc0_ParErrFrc_Mask                                      cBit14
#define cAf6_RAM_Parity_Force_Control_OCNSpiCtlSlc0_ParErrFrc_Shift                                         14

/*--------------------------------------
BitField Name: OCNVpiDemSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN RXPP Per STS payload Control"
SliceId 1
BitField Bits: [13]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc1_ParErrFrc_Mask                                      cBit13
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc1_ParErrFrc_Shift                                         13

/*--------------------------------------
BitField Name: OCNVpiDemSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN RXPP Per STS payload Control"
SliceId 0
BitField Bits: [12]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc0_ParErrFrc_Mask                                      cBit12
#define cAf6_RAM_Parity_Force_Control_OCNVpiDemSlc0_ParErrFrc_Shift                                         12

/*--------------------------------------
BitField Name: OCNVpiCtlSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN VTTU Pointer Interpreter Per
Channel Control" SliceId 1
BitField Bits: [11]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc1_ParErrFrc_Mask                                      cBit11
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc1_ParErrFrc_Shift                                         11

/*--------------------------------------
BitField Name: OCNVpiCtlSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN VTTU Pointer Interpreter Per
Channel Control" SliceId 0
BitField Bits: [10]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc0_ParErrFrc_Mask                                      cBit10
#define cAf6_RAM_Parity_Force_Control_OCNVpiCtlSlc0_ParErrFrc_Shift                                         10

/*--------------------------------------
BitField Name: OCNVpgDemSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN TXPP Per STS Multiplexing
Control" SliceId 1
BitField Bits: [9]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc1_ParErrFrc_Mask                                       cBit9
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc1_ParErrFrc_Shift                                          9

/*--------------------------------------
BitField Name: OCNVpgDemSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN TXPP Per STS Multiplexing
Control" SliceId 0
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc0_ParErrFrc_Mask                                       cBit8
#define cAf6_RAM_Parity_Force_Control_OCNVpgDemSlc0_ParErrFrc_Shift                                          8

/*--------------------------------------
BitField Name: OCNVpgCtlSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN VTTU Pointer Generator Per
Channel Control" SliceId 1
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc1_ParErrFrc_Mask                                       cBit7
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc1_ParErrFrc_Shift                                          7

/*--------------------------------------
BitField Name: OCNVpgCtlSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN VTTU Pointer Generator Per
Channel Control" SliceId 0
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc0_ParErrFrc_Mask                                       cBit6
#define cAf6_RAM_Parity_Force_Control_OCNVpgCtlSlc0_ParErrFrc_Shift                                          6

/*--------------------------------------
BitField Name: OCNSpgCtlSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN STS Pointer Generator Per
Channel Control" SliceId 1
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc1_ParErrFrc_Mask                                       cBit5
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc1_ParErrFrc_Shift                                          5

/*--------------------------------------
BitField Name: OCNSpgCtlSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN STS Pointer Generator Per
Channel Control" SliceId 0
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc0_ParErrFrc_Mask                                       cBit4
#define cAf6_RAM_Parity_Force_Control_OCNSpgCtlSlc0_ParErrFrc_Shift                                          4

/*--------------------------------------
BitField Name: OCNTfmCtlSlc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN Tx Framer Per Channel Control"
SliceId 1
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc1_ParErrFrc_Mask                                       cBit3
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc1_ParErrFrc_Shift                                          3

/*--------------------------------------
BitField Name: OCNTfmCtlSlc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN Tx Framer Per Channel Control"
SliceId 0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc0_ParErrFrc_Mask                                       cBit2
#define cAf6_RAM_Parity_Force_Control_OCNTfmCtlSlc0_ParErrFrc_Shift                                          2

/*--------------------------------------
BitField Name: OCNTfmJ0Slc1_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN Tx J0 Insertion Buffer" SliceId
1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc1_ParErrFrc_Mask                                        cBit1
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc1_ParErrFrc_Shift                                           1

/*--------------------------------------
BitField Name: OCNTfmJ0Slc0_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "OCN Tx J0 Insertion Buffer" SliceId
0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc0_ParErrFrc_Mask                                        cBit0
#define cAf6_RAM_Parity_Force_Control_OCNTfmJ0Slc0_ParErrFrc_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : RAM parity Disable Control
Reg Addr   : 0x131
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Parity_Disable_Control_Base                                                          0x131

/*--------------------------------------
BitField Name: DDRCDR_ParDis
BitField Type: RW
BitField Desc: Disable check parity error for CDR DDR
BitField Bits: [87]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DDRCDR_ParDis_Mask                                              cBit23
#define cAf6_RAM_Parity_Disable_Control_DDRCDR_ParDis_Shift                                                 23

/*--------------------------------------
BitField Name: DDRTxEccNonCorErr_ErrDis
BitField Type: RW
BitField Desc: Disable check ECC non-correctable error for Tx DDR
BitField Bits: [86]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccNonCorErr_ErrDis_Mask                                   cBit22
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccNonCorErr_ErrDis_Shift                                      22

/*--------------------------------------
BitField Name: DDRTxEccCorErr_ErrDis
BitField Type: RW
BitField Desc: Disable check ECC correctable error for Tx DDR
BitField Bits: [85]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccCorErr_ErrDis_Mask                                      cBit21
#define cAf6_RAM_Parity_Disable_Control_DDRTxEccCorErr_ErrDis_Shift                                         21

/*--------------------------------------
BitField Name: DDRRxEccNonCorErr_ErrDis
BitField Type: RW
BitField Desc: Disable check ECC non-correctable error for Rx DDR
BitField Bits: [84]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccNonCorErr_ErrDis_Mask                                   cBit20
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccNonCorErr_ErrDis_Shift                                      20

/*--------------------------------------
BitField Name: DDRRxEccCorErr_ErrDis
BitField Type: RW
BitField Desc: Disable check ECC correctable error for Rx DDR
BitField Bits: [83]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccCorErr_ErrDis_Mask                                      cBit19
#define cAf6_RAM_Parity_Disable_Control_DDRRxEccCorErr_ErrDis_Shift                                         19

/*--------------------------------------
BitField Name: CLASoftwaveCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Softwave Control"
BitField Bits: [82]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLASoftwaveCtl_ParDis_Mask                                      cBit18
#define cAf6_RAM_Parity_Disable_Control_CLASoftwaveCtl_ParDis_Shift                                         18

/*--------------------------------------
BitField Name: CDRPweLookupCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR Pseudowire Look Up Control"
BitField Bits: [81]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRPweLookupCtl_ParDis_Mask                                     cBit17
#define cAf6_RAM_Parity_Disable_Control_CDRPweLookupCtl_ParDis_Shift                                        17

/*--------------------------------------
BitField Name: CLAPerGrpEnbCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Classify Per Group Enable
Control"
BitField Bits: [80]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAPerGrpEnbCtl_ParDis_Mask                                     cBit16
#define cAf6_RAM_Parity_Disable_Control_CLAPerGrpEnbCtl_ParDis_Shift                                        16

/*--------------------------------------
BitField Name: CLAMefOverMplsCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Classify Pseudowire MEF over MPLS
Control"
BitField Bits: [79]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAMefOverMplsCtl_ParDis_Mask                                   cBit15
#define cAf6_RAM_Parity_Disable_Control_CLAMefOverMplsCtl_ParDis_Shift                                      15

/*--------------------------------------
BitField Name: CLAPerPweTypeCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Classify Per Pseudowire Type
Control"
BitField Bits: [78]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAPerPweTypeCtl_ParDis_Mask                                    cBit14
#define cAf6_RAM_Parity_Disable_Control_CLAPerPweTypeCtl_ParDis_Shift                                       14

/*--------------------------------------
BitField Name: CLAHbceLookupInfCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Classify HBCE Looking Up
Information Control"
BitField Bits: [77]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAHbceLookupInfCtl_ParDis_Mask                                  cBit13
#define cAf6_RAM_Parity_Disable_Control_CLAHbceLookupInfCtl_ParDis_Shift                                      13

/*--------------------------------------
BitField Name: CLAHbceHashTab1Ctl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Classify HBCE Hashing Table
Control" PageID 1
BitField Bits: [76]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab1Ctl_ParDis_Mask                                  cBit12
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab1Ctl_ParDis_Shift                                      12

/*--------------------------------------
BitField Name: CLAHbceHashTab0Ctl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Classify HBCE Hashing Table
Control" PageID 0
BitField Bits: [75]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab0Ctl_ParDis_Mask                                  cBit11
#define cAf6_RAM_Parity_Disable_Control_CLAHbceHashTab0Ctl_ParDis_Shift                                      11

/*--------------------------------------
BitField Name: PWETxHspwGrpProCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit HSPW Group
Protection Control"
BitField Bits: [74]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwGrpProCtl_ParDis_Mask                                  cBit10
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwGrpProCtl_ParDis_Shift                                      10

/*--------------------------------------
BitField Name: PWETxUpsaGrpEnbCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit UPSR Group
Enable Control"
BitField Bits: [73]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaGrpEnbCtl_ParDis_Mask                                   cBit9
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaGrpEnbCtl_ParDis_Shift                                       9

/*--------------------------------------
BitField Name: PWETxUpsaHspwModCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit UPSR and HSPW
mode Control"
BitField Bits: [72]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaHspwModCtl_ParDis_Mask                                   cBit8
#define cAf6_RAM_Parity_Disable_Control_PWETxUpsaHspwModCtl_ParDis_Shift                                       8

/*--------------------------------------
BitField Name: PWETxHspwLabelCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit HSPW Label
Control"
BitField Bits: [71]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwLabelCtl_ParDis_Mask                                    cBit7
#define cAf6_RAM_Parity_Disable_Control_PWETxHspwLabelCtl_ParDis_Shift                                       7

/*--------------------------------------
BitField Name: PWETxHdrRtpSsrcCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit Header RTP
SSRC Value Control "
BitField Bits: [70]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxHdrRtpSsrcCtl_ParDis_Mask                                   cBit6
#define cAf6_RAM_Parity_Disable_Control_PWETxHdrRtpSsrcCtl_ParDis_Shift                                       6

/*--------------------------------------
BitField Name: PWETxEthHdrLenCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit Ethernet
Header Length Control"
BitField Bits: [69]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrLenCtl_ParDis_Mask                                    cBit5
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrLenCtl_ParDis_Shift                                       5

/*--------------------------------------
BitField Name: PWETxEthHdrValCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit Ethernet
Header Value Control"
BitField Bits: [68]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrValCtl_ParDis_Mask                                    cBit4
#define cAf6_RAM_Parity_Disable_Control_PWETxEthHdrValCtl_ParDis_Shift                                       4

/*--------------------------------------
BitField Name: PWETxEnbCtl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire Transmit Enable
Control"
BitField Bits: [67]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWETxEnbCtl_ParDis_Mask                                          cBit3
#define cAf6_RAM_Parity_Disable_Control_PWETxEnbCtl_ParDis_Shift                                             3

/*--------------------------------------
BitField Name: PDAddrCrc_ErrDis
BitField Type: RW
BitField Desc: Disable check DDR CRC error
BitField Bits: [66]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDAddrCrc_ErrDis_Mask                                            cBit2
#define cAf6_RAM_Parity_Disable_Control_PDAddrCrc_ErrDis_Shift                                               2

/*--------------------------------------
BitField Name: PDATdmMode_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire PDA TDM Mode Control"
BitField Bits: [65]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDATdmMode_ParDis_Mask                                           cBit1
#define cAf6_RAM_Parity_Disable_Control_PDATdmMode_ParDis_Shift                                              1

/*--------------------------------------
BitField Name: PDAJitbuf_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire PDA Jitter Buffer
Control"
BitField Bits: [64]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDAJitbuf_ParDis_Mask                                            cBit0
#define cAf6_RAM_Parity_Disable_Control_PDAJitbuf_ParDis_Shift                                               0

/*--------------------------------------
BitField Name: PDAReor_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Pseudowire PDA Reorder Control"
BitField Bits: [63]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDAReor_ParDis_Mask                                             cBit31
#define cAf6_RAM_Parity_Disable_Control_PDAReor_ParDis_Shift                                                31

/*--------------------------------------
BitField Name: PLAPaylSizeSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa PDHPW Payload Assemble
Payload Size Control"SliceId 1
BitField Bits: [62]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc1_ParDis_Mask                                     cBit30
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc1_ParDis_Shift                                        30

/*--------------------------------------
BitField Name: PLAPaylSizeSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa PDHPW Payload Assemble
Payload Size Control"SliceId 0
BitField Bits: [61]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc0_ParDis_Mask                                     cBit29
#define cAf6_RAM_Parity_Disable_Control_PLAPaylSizeSlc0_ParDis_Shift                                        29

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR ACR Engine Timing control"
SliceId 1
BitField Bits: [60]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParDis_Mask                                  cBit28
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc1_ParDis_Shift                                      28

/*--------------------------------------
BitField Name: CDRVTTimingCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR VT Timing control" SliceId 1
BitField Bits: [59]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc1_ParDis_Mask                                  cBit27
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc1_ParDis_Shift                                      27

/*--------------------------------------
BitField Name: CDRSTSTimingCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR STS Timing control" SliceId 1
BitField Bits: [58]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc1_ParDis_Mask                                  cBit26
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc1_ParDis_Shift                                      26

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR ACR Engine Timing control"
SliceId 0
BitField Bits: [57]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParDis_Mask                                  cBit25
#define cAf6_RAM_Parity_Disable_Control_CDRACRTimingCtrlSlc0_ParDis_Shift                                      25

/*--------------------------------------
BitField Name: CDRVTTimingCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR VT Timing control" SliceId 0
BitField Bits: [56]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc0_ParDis_Mask                                  cBit24
#define cAf6_RAM_Parity_Disable_Control_CDRVTTimingCtrlSlc0_ParDis_Shift                                      24

/*--------------------------------------
BitField Name: CDRSTSTimingCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "CDR STS Timing control" SliceId 0
BitField Bits: [55]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc0_ParDis_Mask                                  cBit23
#define cAf6_RAM_Parity_Disable_Control_CDRSTSTimingCtrlSlc0_ParDis_Shift                                      23

/*--------------------------------------
BitField Name: DeMapChlCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "DEMAP Thalassa Demap Channel
Control" SliceId 1
BitField Bits: [54]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc1_ParDis_Mask                                    cBit22
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc1_ParDis_Shift                                       22

/*--------------------------------------
BitField Name: DeMapChlCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "DEMAP Thalassa Demap Channel
Control" SliceId 0
BitField Bits: [53]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc0_ParDis_Mask                                    cBit21
#define cAf6_RAM_Parity_Disable_Control_DeMapChlCtrlSlc0_ParDis_Shift                                       21

/*--------------------------------------
BitField Name: MAPLineCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "MAP Thalassa Map Line Control"
SliceId 1
BitField Bits: [52]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc1_ParDis_Mask                                     cBit20
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc1_ParDis_Shift                                        20

/*--------------------------------------
BitField Name: MAPChlCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "MAP Thalassa Map Channel Control"
SliceId 1
BitField Bits: [51]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc1_ParDis_Mask                                      cBit19
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc1_ParDis_Shift                                         19

/*--------------------------------------
BitField Name: MAPLineCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "MAP Thalassa Map Line Control"
SliceId 0
BitField Bits: [50]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc0_ParDis_Mask                                     cBit18
#define cAf6_RAM_Parity_Disable_Control_MAPLineCtrlSlc0_ParDis_Shift                                        18

/*--------------------------------------
BitField Name: MAPChlCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "MAP Thalassa Map Channel Control"
SliceId 0
BitField Bits: [49]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc0_ParDis_Mask                                      cBit17
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlSlc0_ParDis_Shift                                         17

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH VT Async Map Control" SliceId
1
BitField Bits: [48]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc1_ParDis_Mask                                  cBit16
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc1_ParDis_Shift                                      16

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH STS/VT Map Control" SliceId 1
BitField Bits: [47]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc1_ParDis_Mask                                  cBit15
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc1_ParDis_Shift                                      15

/*--------------------------------------
BitField Name: PDHTxM23E23TraceSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"
SliceId 1
BitField Bits: [46]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc1_ParDis_Mask                                  cBit14
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc1_ParDis_Shift                                      14

/*--------------------------------------
BitField Name: PDHTxM23E23CtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM23E23 Control" SliceId 1
BitField Bits: [45]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc1_ParDis_Mask                                  cBit13
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc1_ParDis_Shift                                      13

/*--------------------------------------
BitField Name: PDHTxM12E12CtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM12E12 Control" SliceId 1
BitField Bits: [44]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc1_ParDis_Mask                                  cBit12
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc1_ParDis_Shift                                      12

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer
Signalling Control" SliceId 1
BitField Bits: [43]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc1_ParDis_Mask                                  cBit11
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc1_ParDis_Shift                                      11

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"
SliceId 1
BitField Bits: [42]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc1_ParDis_Mask                                  cBit10
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc1_ParDis_Shift                                      10

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"
SliceId 1
BitField Bits: [41]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc1_ParDis_Mask                                   cBit9
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc1_ParDis_Shift                                       9

/*--------------------------------------
BitField Name: PDHRxM12E12CtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxM12E12 Control" SliceId 1
BitField Bits: [40]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc1_ParDis_Mask                                   cBit8
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc1_ParDis_Shift                                       8

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxDS3E3 OH Pro Control"
SliceId 1
BitField Bits: [39]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc1_ParDis_Mask                                   cBit7
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc1_ParDis_Shift                                       7

/*--------------------------------------
BitField Name: PDHRxM23E23CtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxM23E23 Control" SliceId 1
BitField Bits: [38]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc1_ParDis_Mask                                   cBit6
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc1_ParDis_Shift                                       6

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH STS/VT Demap Control" SliceId
1
BitField Bits: [37]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc1_ParDis_Mask                                   cBit5
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc1_ParDis_Shift                                       5

/*--------------------------------------
BitField Name: PDHMuxCtrlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control" SliceId 1
BitField Bits: [36]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrlSlc1_ParDis_Mask                                       cBit4
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrlSlc1_ParDis_Shift                                          4

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH VT Async Map Control" SliceId
0
BitField Bits: [35]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc0_ParDis_Mask                                   cBit3
#define cAf6_RAM_Parity_Disable_Control_PDHVTAsyncMapCtrlSlc0_ParDis_Shift                                       3

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH STS/VT Map Control" SliceId 0
BitField Bits: [34]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc0_ParDis_Mask                                   cBit2
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTMapCtrlSlc0_ParDis_Shift                                       2

/*--------------------------------------
BitField Name: PDHTxM23E23TraceSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"
SliceId 0
BitField Bits: [33]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc0_ParDis_Mask                                   cBit1
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23TraceSlc0_ParDis_Shift                                       1

/*--------------------------------------
BitField Name: PDHTxM23E23CtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM23E23 Control" SliceId 0
BitField Bits: [32]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc0_ParDis_Mask                                cBit32_0
#define cAf6_RAM_Parity_Disable_Control_PDHTxM23E23CtrlSlc0_ParDis_Shift                                       0

/*--------------------------------------
BitField Name: PDHTxM12E12CtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH TxM12E12 Control" SliceId 0
BitField Bits: [31]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc0_ParDis_Mask                                  cBit31
#define cAf6_RAM_Parity_Disable_Control_PDHTxM12E12CtrlSlc0_ParDis_Shift                                      31

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer
Signalling Control" SliceId 0
BitField Bits: [30]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc0_ParDis_Mask                                  cBit30
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1SigCtrlSlc0_ParDis_Shift                                      30

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"
SliceId 0
BitField Bits: [29]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc0_ParDis_Mask                                  cBit29
#define cAf6_RAM_Parity_Disable_Control_PDHTxDE1FrmCtrlSlc0_ParDis_Shift                                      29

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"
SliceId 0
BitField Bits: [28]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc0_ParDis_Mask                                  cBit28
#define cAf6_RAM_Parity_Disable_Control_PDHRxDE1FrmCtrlSlc0_ParDis_Shift                                      28

/*--------------------------------------
BitField Name: PDHRxM12E12CtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxM12E12 Control" SliceId 0
BitField Bits: [27]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc0_ParDis_Mask                                  cBit27
#define cAf6_RAM_Parity_Disable_Control_PDHRxM12E12CtrlSlc0_ParDis_Shift                                      27

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxDS3E3 OH Pro Control"
SliceId 0
BitField Bits: [26]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc0_ParDis_Mask                                  cBit26
#define cAf6_RAM_Parity_Disable_Control_PDHRxDS3E3OHCtrlSlc0_ParDis_Shift                                      26

/*--------------------------------------
BitField Name: PDHRxM23E23CtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH RxM23E23 Control" SliceId 0
BitField Bits: [25]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc0_ParDis_Mask                                  cBit25
#define cAf6_RAM_Parity_Disable_Control_PDHRxM23E23CtrlSlc0_ParDis_Shift                                      25

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH STS/VT Demap Control" SliceId
0
BitField Bits: [24]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc0_ParDis_Mask                                  cBit24
#define cAf6_RAM_Parity_Disable_Control_PDHSTSVTDeMapCtrlSlc0_ParDis_Shift                                      24

/*--------------------------------------
BitField Name: PDHMuxCtrl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control" SliceId 0
BitField Bits: [23]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrl_ParDis_Mask                                          cBit23
#define cAf6_RAM_Parity_Disable_Control_PDHMuxCtrl_ParDis_Shift                                             23

/*--------------------------------------
BitField Name: POHWrCtrl_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "POH BER Control VT/DSN", "POH BER
Control STS/TU3"
BitField Bits: [22]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_POHWrCtrl_ParDis_Mask                                           cBit22
#define cAf6_RAM_Parity_Disable_Control_POHWrCtrl_ParDis_Shift                                              22

/*--------------------------------------
BitField Name: POHWrTrsh_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "POH BER Threshold 2"
BitField Bits: [21]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_POHWrTrsh_ParDis_Mask                                           cBit21
#define cAf6_RAM_Parity_Disable_Control_POHWrTrsh_ParDis_Shift                                              21

/*--------------------------------------
BitField Name: POHTerCtrlLo_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "POH Termintate Insert Control
VT/TU3"
BitField Bits: [20]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlLo_ParDis_Mask                                        cBit20
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlLo_ParDis_Shift                                           20

/*--------------------------------------
BitField Name: POHTerCtrlHi_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "POH Termintate Insert Control
STS"
BitField Bits: [19]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlHi_ParDis_Mask                                        cBit19
#define cAf6_RAM_Parity_Disable_Control_POHTerCtrlHi_ParDis_Shift                                           19

/*--------------------------------------
BitField Name: POHCpeVtCtr_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "POH CPE VT Control Register"
BitField Bits: [18]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_POHCpeVtCtr_ParDis_Mask                                         cBit18
#define cAf6_RAM_Parity_Disable_Control_POHCpeVtCtr_ParDis_Shift                                            18

/*--------------------------------------
BitField Name: POHCpeStsCtr_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "POH CPE STS/TU3 Control Register"
BitField Bits: [17]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_POHCpeStsCtr_ParDis_Mask                                        cBit17
#define cAf6_RAM_Parity_Disable_Control_POHCpeStsCtr_ParDis_Shift                                           17

/*--------------------------------------
BitField Name: OCNTohCtlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN TOH Monitoring Per Channel
Control" SliceId 1
BitField Bits: [16]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNTohCtlSlc1_ParDis_Mask                                       cBit16
#define cAf6_RAM_Parity_Disable_Control_OCNTohCtlSlc1_ParDis_Shift                                          16

/*--------------------------------------
BitField Name: OCNSpiCtlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN STS Pointer Interpreter Per
Channel Control" SliceId 1
BitField Bits: [15]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc1_ParDis_Mask                                       cBit15
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc1_ParDis_Shift                                          15

/*--------------------------------------
BitField Name: OCNSpiCtlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN STS Pointer Interpreter Per
Channel Control" SliceId 0
BitField Bits: [14]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc0_ParDis_Mask                                       cBit14
#define cAf6_RAM_Parity_Disable_Control_OCNSpiCtlSlc0_ParDis_Shift                                          14

/*--------------------------------------
BitField Name: OCNVpiDemSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN RXPP Per STS payload Control"
SliceId 1
BitField Bits: [13]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc1_ParDis_Mask                                       cBit13
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc1_ParDis_Shift                                          13

/*--------------------------------------
BitField Name: OCNVpiDemSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN RXPP Per STS payload Control"
SliceId 0
BitField Bits: [12]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc0_ParDis_Mask                                       cBit12
#define cAf6_RAM_Parity_Disable_Control_OCNVpiDemSlc0_ParDis_Shift                                          12

/*--------------------------------------
BitField Name: OCNVpiCtlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN VTTU Pointer Interpreter Per
Channel Control" SliceId 1
BitField Bits: [11]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc1_ParDis_Mask                                       cBit11
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc1_ParDis_Shift                                          11

/*--------------------------------------
BitField Name: OCNVpiCtlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN VTTU Pointer Interpreter Per
Channel Control" SliceId 0
BitField Bits: [10]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc0_ParDis_Mask                                       cBit10
#define cAf6_RAM_Parity_Disable_Control_OCNVpiCtlSlc0_ParDis_Shift                                          10

/*--------------------------------------
BitField Name: OCNVpgDemSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN TXPP Per STS Multiplexing
Control" SliceId 1
BitField Bits: [9]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc1_ParDis_Mask                                        cBit9
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc1_ParDis_Shift                                           9

/*--------------------------------------
BitField Name: OCNVpgDemSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN TXPP Per STS Multiplexing
Control" SliceId 0
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc0_ParDis_Mask                                        cBit8
#define cAf6_RAM_Parity_Disable_Control_OCNVpgDemSlc0_ParDis_Shift                                           8

/*--------------------------------------
BitField Name: OCNVpgCtlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN VTTU Pointer Generator Per
Channel Control" SliceId 1
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc1_ParDis_Mask                                        cBit7
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc1_ParDis_Shift                                           7

/*--------------------------------------
BitField Name: OCNVpgCtlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN VTTU Pointer Generator Per
Channel Control" SliceId 0
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc0_ParDis_Mask                                        cBit6
#define cAf6_RAM_Parity_Disable_Control_OCNVpgCtlSlc0_ParDis_Shift                                           6

/*--------------------------------------
BitField Name: OCNSpgCtlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN STS Pointer Generator Per
Channel Control" SliceId 1
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc1_ParDis_Mask                                        cBit5
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc1_ParDis_Shift                                           5

/*--------------------------------------
BitField Name: OCNSpgCtlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN STS Pointer Generator Per
Channel Control" SliceId 0
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc0_ParDis_Mask                                        cBit4
#define cAf6_RAM_Parity_Disable_Control_OCNSpgCtlSlc0_ParDis_Shift                                           4

/*--------------------------------------
BitField Name: OCNTfmCtlSlc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN Tx Framer Per Channel
Control" SliceId 1
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc1_ParDis_Mask                                        cBit3
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc1_ParDis_Shift                                           3

/*--------------------------------------
BitField Name: OCNTfmCtlSlc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN Tx Framer Per Channel
Control" SliceId 0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc0_ParDis_Mask                                        cBit2
#define cAf6_RAM_Parity_Disable_Control_OCNTfmCtlSlc0_ParDis_Shift                                           2

/*--------------------------------------
BitField Name: OCNTfmJ0Slc1_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN Tx J0 Insertion Buffer"
SliceId 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc1_ParDis_Mask                                         cBit1
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc1_ParDis_Shift                                            1

/*--------------------------------------
BitField Name: OCNTfmJ0Slc0_ParDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "OCN Tx J0 Insertion Buffer"
SliceId 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc0_ParDis_Mask                                         cBit0
#define cAf6_RAM_Parity_Disable_Control_OCNTfmJ0Slc0_ParDis_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : RAM parity Error Sticky
Reg Addr   : 0x112
Reg Formula: 0x112 + Wrd
    Where  : 
           + $Wrd(0-2): RAM parity Error Word 0:wrd[31:0]; 1:wrd[63:32];2:wrd[95:64]
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Parity_Error_Sticky_Base                                                             0x112

/*--------------------------------------
BitField Name: DDRCDR_ParErrStk
BitField Type: W1C
BitField Desc: Error Sticky Parity error for CDR DDR
BitField Bits: [87]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DDRCDR_ParErrStk_Mask                                              cBit23
#define cAf6_RAM_Parity_Error_Sticky_DDRCDR_ParErrStk_Shift                                                 23

/*--------------------------------------
BitField Name: DDRTxEccNonCorErr_ErrStk
BitField Type: W1C
BitField Desc: Error Sticky ECC non-correctable error for Tx DDR
BitField Bits: [86]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccNonCorErr_ErrStk_Mask                                      cBit22
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccNonCorErr_ErrStk_Shift                                         22

/*--------------------------------------
BitField Name: DDRTxEccCorErr_ErrStk
BitField Type: W1C
BitField Desc: Error Sticky ECC correctable error for Tx DDR
BitField Bits: [85]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccCorErr_ErrStk_Mask                                         cBit21
#define cAf6_RAM_Parity_Error_Sticky_DDRTxEccCorErr_ErrStk_Shift                                            21

/*--------------------------------------
BitField Name: DDRRxEccNonCorErr_ErrStk
BitField Type: W1C
BitField Desc: Error Sticky ECC non-correctable error for Rx DDR
BitField Bits: [84]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccNonCorErr_ErrStk_Mask                                      cBit20
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccNonCorErr_ErrStk_Shift                                         20

/*--------------------------------------
BitField Name: DDRRxEccCorErr_ErrStk
BitField Type: W1C
BitField Desc: Error Sticky ECC correctable error for Rx DDR
BitField Bits: [83]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccCorErr_ErrStk_Mask                                         cBit19
#define cAf6_RAM_Parity_Error_Sticky_DDRRxEccCorErr_ErrStk_Shift                                            19

/*--------------------------------------
BitField Name: CLASoftwaveCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Softwave Control"
BitField Bits: [82]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLASoftwaveCtl_ParErrStk_Mask                                      cBit18
#define cAf6_RAM_Parity_Error_Sticky_CLASoftwaveCtl_ParErrStk_Shift                                         18

/*--------------------------------------
BitField Name: CDRPweLookupCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "CDR Pseudowire Look Up
Control"
BitField Bits: [81]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CDRPweLookupCtl_ParErrStk_Mask                                     cBit17
#define cAf6_RAM_Parity_Error_Sticky_CDRPweLookupCtl_ParErrStk_Shift                                        17

/*--------------------------------------
BitField Name: CLAPerGrpEnbCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Classify Per Group Enable
Control"
BitField Bits: [80]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAPerGrpEnbCtl_ParErrStk_Mask                                     cBit16
#define cAf6_RAM_Parity_Error_Sticky_CLAPerGrpEnbCtl_ParErrStk_Shift                                        16

/*--------------------------------------
BitField Name: CLAMefOverMplsCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Classify Pseudowire MEF over
MPLS Control"
BitField Bits: [79]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAMefOverMplsCtl_ParErrStk_Mask                                   cBit15
#define cAf6_RAM_Parity_Error_Sticky_CLAMefOverMplsCtl_ParErrStk_Shift                                      15

/*--------------------------------------
BitField Name: CLAPerPweTypeCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Classify Per Pseudowire Type
Control"
BitField Bits: [78]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAPerPweTypeCtl_ParErrStk_Mask                                    cBit14
#define cAf6_RAM_Parity_Error_Sticky_CLAPerPweTypeCtl_ParErrStk_Shift                                       14

/*--------------------------------------
BitField Name: CLAHbceLookupInfCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Classify HBCE Looking Up
Information Control"
BitField Bits: [77]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceLookupInfCtl_ParErrStk_Mask                                  cBit13
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceLookupInfCtl_ParErrStk_Shift                                      13

/*--------------------------------------
BitField Name: CLAHbceHashTab1Ctl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Classify HBCE Hashing Table
Control" PageID 1
BitField Bits: [76]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab1Ctl_ParErrStk_Mask                                  cBit12
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab1Ctl_ParErrStk_Shift                                      12

/*--------------------------------------
BitField Name: CLAHbceHashTab0Ctl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Classify HBCE Hashing Table
Control" PageID 0
BitField Bits: [75]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab0Ctl_ParErrStk_Mask                                  cBit11
#define cAf6_RAM_Parity_Error_Sticky_CLAHbceHashTab0Ctl_ParErrStk_Shift                                      11

/*--------------------------------------
BitField Name: PWETxHspwGrpProCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit HSPW
Group Protection Control"
BitField Bits: [74]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwGrpProCtl_ParErrStk_Mask                                  cBit10
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwGrpProCtl_ParErrStk_Shift                                      10

/*--------------------------------------
BitField Name: PWETxUpsaGrpEnbCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit UPSR
Group Enable Control"
BitField Bits: [73]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaGrpEnbCtl_ParErrStk_Mask                                   cBit9
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaGrpEnbCtl_ParErrStk_Shift                                       9

/*--------------------------------------
BitField Name: PWETxUpsaHspwModCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit UPSR and
HSPW mode Control"
BitField Bits: [72]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaHspwModCtl_ParErrStk_Mask                                   cBit8
#define cAf6_RAM_Parity_Error_Sticky_PWETxUpsaHspwModCtl_ParErrStk_Shift                                       8

/*--------------------------------------
BitField Name: PWETxHspwLabelCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit HSPW
Label Control"
BitField Bits: [71]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwLabelCtl_ParErrStk_Mask                                    cBit7
#define cAf6_RAM_Parity_Error_Sticky_PWETxHspwLabelCtl_ParErrStk_Shift                                       7

/*--------------------------------------
BitField Name: PWETxHdrRtpSsrcCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit Header
RTP SSRC Value Control "
BitField Bits: [70]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxHdrRtpSsrcCtl_ParErrStk_Mask                                   cBit6
#define cAf6_RAM_Parity_Error_Sticky_PWETxHdrRtpSsrcCtl_ParErrStk_Shift                                       6

/*--------------------------------------
BitField Name: PWETxEthHdrLenCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit Ethernet
Header Length Control"
BitField Bits: [69]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrLenCtl_ParErrStk_Mask                                    cBit5
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrLenCtl_ParErrStk_Shift                                       5

/*--------------------------------------
BitField Name: PWETxEthHdrValCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit Ethernet
Header Value Control"
BitField Bits: [68]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrValCtl_ParErrStk_Mask                                    cBit4
#define cAf6_RAM_Parity_Error_Sticky_PWETxEthHdrValCtl_ParErrStk_Shift                                       4

/*--------------------------------------
BitField Name: PWETxEnbCtl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire Transmit Enable
Control"
BitField Bits: [67]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWETxEnbCtl_ParErrStk_Mask                                          cBit3
#define cAf6_RAM_Parity_Error_Sticky_PWETxEnbCtl_ParErrStk_Shift                                             3

/*--------------------------------------
BitField Name: PDAddrCrc_ErrStk
BitField Type: W1C
BitField Desc: Sticky check DDR CRC error
BitField Bits: [66]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDAddrCrc_ErrStk_Mask                                               cBit2
#define cAf6_RAM_Parity_Error_Sticky_PDAddrCrc_ErrStk_Shift                                                  2

/*--------------------------------------
BitField Name: PDATdmMode_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire PDA TDM Mode
Control"
BitField Bits: [65]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDATdmMode_ParErrStk_Mask                                           cBit1
#define cAf6_RAM_Parity_Error_Sticky_PDATdmMode_ParErrStk_Shift                                              1

/*--------------------------------------
BitField Name: PDAJitbuf_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire PDA Jitter Buffer
Control"
BitField Bits: [64]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDAJitbuf_ParErrStk_Mask                                            cBit0
#define cAf6_RAM_Parity_Error_Sticky_PDAJitbuf_ParErrStk_Shift                                               0

/*--------------------------------------
BitField Name: PDAReor_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Pseudowire PDA Reorder
Control"
BitField Bits: [63]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDAReor_ParErrStk_Mask                                             cBit31
#define cAf6_RAM_Parity_Error_Sticky_PDAReor_ParErrStk_Shift                                                31

/*--------------------------------------
BitField Name: PLAPaylSizeSlc1_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Thalassa PDHPW Payload
Assemble Payload Size Control"SliceId 1
BitField Bits: [62]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc1_ParErrStk_Mask                                     cBit30
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc1_ParErrStk_Shift                                        30

/*--------------------------------------
BitField Name: PLAPaylSizeSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "Thalassa PDHPW Payload
Assemble Payload Size Control"SliceId 0
BitField Bits: [61]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc0_ParErrStk_Mask                                     cBit29
#define cAf6_RAM_Parity_Error_Sticky_PLAPaylSizeSlc0_ParErrStk_Shift                                        29

/*--------------------------------------
BitField Name: CDRACRTimingCtrlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "CDR ACR Engine Timing
control" SliceId 0
BitField Bits: [57]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_Mask                                  cBit25
#define cAf6_RAM_Parity_Error_Sticky_CDRACRTimingCtrlSlc0_ParErrStk_Shift                                      25

/*--------------------------------------
BitField Name: CDRVTTimingCtrlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "CDR VT Timing control"
SliceId 0
BitField Bits: [56]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc0_ParErrStk_Mask                                  cBit24
#define cAf6_RAM_Parity_Error_Sticky_CDRVTTimingCtrlSlc0_ParErrStk_Shift                                      24

/*--------------------------------------
BitField Name: CDRSTSTimingCtrlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "CDR STS Timing control"
SliceId 0
BitField Bits: [55]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc0_ParErrStk_Mask                                  cBit23
#define cAf6_RAM_Parity_Error_Sticky_CDRSTSTimingCtrlSlc0_ParErrStk_Shift                                      23

/*--------------------------------------
BitField Name: DeMapChlCtrlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "DEMAP Thalassa Demap Channel
Control" SliceId 0
BitField Bits: [53]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc0_ParErrStk_Mask                                    cBit21
#define cAf6_RAM_Parity_Error_Sticky_DeMapChlCtrlSlc0_ParErrStk_Shift                                       21

/*--------------------------------------
BitField Name: MAPLineCtrlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "MAP Thalassa Map Line
Control" SliceId 0
BitField Bits: [50]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc0_ParErrStk_Mask                                     cBit18
#define cAf6_RAM_Parity_Error_Sticky_MAPLineCtrlSlc0_ParErrStk_Shift                                        18

/*--------------------------------------
BitField Name: MAPChlCtrlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "MAP Thalassa Map Channel
Control" SliceId 0
BitField Bits: [49]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc0_ParErrStk_Mask                                      cBit17
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlSlc0_ParErrStk_Shift                                         17

/*--------------------------------------
BitField Name: PDHVTAsyncMapCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH VT Async Map Control"
SliceId 0
BitField Bits: [35]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrl_Slc0ParErrStk_Mask                                   cBit3
#define cAf6_RAM_Parity_Error_Sticky_PDHVTAsyncMapCtrl_Slc0ParErrStk_Shift                                       3

/*--------------------------------------
BitField Name: PDHSTSVTMapCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH STS/VT Map Control"
SliceId 0
BitField Bits: [34]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrl_Slc0ParErrStk_Mask                                   cBit2
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTMapCtrl_Slc0ParErrStk_Shift                                       2

/*--------------------------------------
BitField Name: PDHTxM23E23Trace_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH TxM23E23 E3g832 Trace
Byte" SliceId 0
BitField Bits: [33]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Trace_ParErrStk_Mask                                     cBit1
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Trace_ParErrStk_Shift                                        1

/*--------------------------------------
BitField Name: PDHTxM23E23Ctrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH TxM23E23 Control"
SliceId 0
BitField Bits: [32]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Ctrl_Slc0ParErrStk_Mask                                cBit32_0
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM23E23Ctrl_Slc0ParErrStk_Shift                                       0

/*--------------------------------------
BitField Name: PDHTxM12E12Ctrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH TxM12E12 Control"
SliceId 0
BitField Bits: [31]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12Ctrl_Slc0ParErrStk_Mask                                  cBit31
#define cAf6_RAM_Parity_Error_Sticky_PDHTxM12E12Ctrl_Slc0ParErrStk_Shift                                      31

/*--------------------------------------
BitField Name: PDHTxDE1SigCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Tx Framer
Signalling Control" SliceId 0
BitField Bits: [30]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrl_Slc0ParErrStk_Mask                                  cBit30
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1SigCtrl_Slc0ParErrStk_Shift                                      30

/*--------------------------------------
BitField Name: PDHTxDE1FrmCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Tx Framer
Control" SliceId 0
BitField Bits: [29]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrl_Slc0ParErrStk_Mask                                  cBit29
#define cAf6_RAM_Parity_Error_Sticky_PDHTxDE1FrmCtrl_Slc0ParErrStk_Shift                                      29

/*--------------------------------------
BitField Name: PDHRxDE1FrmCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Rx Framer
Control" SliceId 0
BitField Bits: [28]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrl_Slc0ParErrStk_Mask                                  cBit28
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDE1FrmCtrl_Slc0ParErrStk_Shift                                      28

/*--------------------------------------
BitField Name: PDHRxM12E12Ctrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH RxM12E12 Control"
SliceId 0
BitField Bits: [27]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12Ctrl_Slc0ParErrStk_Mask                                  cBit27
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM12E12Ctrl_Slc0ParErrStk_Shift                                      27

/*--------------------------------------
BitField Name: PDHRxDS3E3OHCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH RxDS3E3 OH Pro Control"
SliceId 0
BitField Bits: [26]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrl_Slc0ParErrStk_Mask                                  cBit26
#define cAf6_RAM_Parity_Error_Sticky_PDHRxDS3E3OHCtrl_Slc0ParErrStk_Shift                                      26

/*--------------------------------------
BitField Name: PDHRxM23E23Ctrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH RxM23E23 Control"
SliceId 0
BitField Bits: [25]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23Ctrl_Slc0ParErrStk_Mask                                  cBit25
#define cAf6_RAM_Parity_Error_Sticky_PDHRxM23E23Ctrl_Slc0ParErrStk_Shift                                      25

/*--------------------------------------
BitField Name: PDHSTSVTDeMapCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH STS/VT Demap Control"
SliceId 0
BitField Bits: [24]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrl_Slc0ParErrStk_Mask                                  cBit24
#define cAf6_RAM_Parity_Error_Sticky_PDHSTSVTDeMapCtrl_Slc0ParErrStk_Shift                                      24

/*--------------------------------------
BitField Name: PDHMuxCtrl_Slc0ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Rx Framer Mux
Control" SliceId 0
BitField Bits: [23]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrl_Slc0ParErrStk_Mask                                      cBit23
#define cAf6_RAM_Parity_Error_Sticky_PDHMuxCtrl_Slc0ParErrStk_Shift                                         23

/*--------------------------------------
BitField Name: POHWrCtrl_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "POH BER Control VT/DSN",
"POH BER Control STS/TU3"
BitField Bits: [22]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_POHWrCtrl_ParErrStk_Mask                                           cBit22
#define cAf6_RAM_Parity_Error_Sticky_POHWrCtrl_ParErrStk_Shift                                              22

/*--------------------------------------
BitField Name: POHWrTrsh_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "POH BER Threshold 2"
BitField Bits: [21]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_POHWrTrsh_ParErrStk_Mask                                           cBit21
#define cAf6_RAM_Parity_Error_Sticky_POHWrTrsh_ParErrStk_Shift                                              21

/*--------------------------------------
BitField Name: POHTerCtrlLo_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "POH Termintate Insert
Control VT/TU3"
BitField Bits: [20]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlLo_ParErrStk_Mask                                        cBit20
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlLo_ParErrStk_Shift                                           20

/*--------------------------------------
BitField Name: POHTerCtrlHi_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "POH Termintate Insert
Control STS"
BitField Bits: [19]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlHi_ParErrStk_Mask                                        cBit19
#define cAf6_RAM_Parity_Error_Sticky_POHTerCtrlHi_ParErrStk_Shift                                           19

/*--------------------------------------
BitField Name: POHCpeVtCtr_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "POH CPE VT Control Register"
BitField Bits: [18]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_POHCpeVtCtr_ParErrStk_Mask                                         cBit18
#define cAf6_RAM_Parity_Error_Sticky_POHCpeVtCtr_ParErrStk_Shift                                            18

/*--------------------------------------
BitField Name: POHCpeStsCtr_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "POH CPE STS/TU3 Control
Register"
BitField Bits: [17]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_POHCpeStsCtr_ParErrStk_Mask                                        cBit17
#define cAf6_RAM_Parity_Error_Sticky_POHCpeStsCtr_ParErrStk_Shift                                           17

/*--------------------------------------
BitField Name: OCNSpiCtlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN STS Pointer Interpreter
Per Channel Control" SliceId 0
BitField Bits: [14]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc0_ParErrStk_Mask                                       cBit14
#define cAf6_RAM_Parity_Error_Sticky_OCNSpiCtlSlc0_ParErrStk_Shift                                          14

/*--------------------------------------
BitField Name: OCNVpiDemSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN RXPP Per STS payload
Control" SliceId 0
BitField Bits: [12]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc0_ParErrStk_Mask                                       cBit12
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiDemSlc0_ParErrStk_Shift                                          12

/*--------------------------------------
BitField Name: OCNVpiCtlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN VTTU Pointer Interpreter
Per Channel Control" SliceId 0
BitField Bits: [10]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc0_ParErrStk_Mask                                       cBit10
#define cAf6_RAM_Parity_Error_Sticky_OCNVpiCtlSlc0_ParErrStk_Shift                                          10

/*--------------------------------------
BitField Name: OCNVpgDemSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN TXPP Per STS
Multiplexing Control" SliceId 0
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc0_ParErrStk_Mask                                        cBit8
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgDemSlc0_ParErrStk_Shift                                           8

/*--------------------------------------
BitField Name: OCNVpgCtlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN VTTU Pointer Generator
Per Channel Control" SliceId 0
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc0_ParErrStk_Mask                                        cBit6
#define cAf6_RAM_Parity_Error_Sticky_OCNVpgCtlSlc0_ParErrStk_Shift                                           6

/*--------------------------------------
BitField Name: OCNSpgCtlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN STS Pointer Generator
Per Channel Control" SliceId 0
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc0_ParErrStk_Mask                                        cBit4
#define cAf6_RAM_Parity_Error_Sticky_OCNSpgCtlSlc0_ParErrStk_Shift                                           4

/*--------------------------------------
BitField Name: OCNTfmCtlSlc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN Tx Framer Per Channel
Control" SliceId 0
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc0_ParErrStk_Mask                                        cBit2
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmCtlSlc0_ParErrStk_Shift                                           2

/*--------------------------------------
BitField Name: OCNTfmJ0Slc0_ParErrStk
BitField Type: W1C
BitField Desc: Parity Error Sticky For RAM Control "OCN Tx J0 Insertion Buffer"
SliceId 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc0_ParErrStk_Mask                                         cBit0
#define cAf6_RAM_Parity_Error_Sticky_OCNTfmJ0Slc0_ParErrStk_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data127_96
Reg Addr   : 0x00121
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Parity_Error_Intr_Group_Base                                                       0x00121

/*--------------------------------------
BitField Name: Wrd2
BitField Type: RW
BitField Desc: Word2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd2_Mask                                                       cBit2
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd2_Shift                                                          2

/*--------------------------------------
BitField Name: Wrd1
BitField Type: RW
BitField Desc: Word1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd1_Mask                                                       cBit1
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd1_Shift                                                          1

/*--------------------------------------
BitField Name: Wrd0
BitField Type: RW
BitField Desc: Word0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd0_Mask                                                       cBit0
#define cAf6_RAM_Parity_Error_Intr_Group_Wrd0_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit3_0 Control
Reg Addr   : 0x00200
Reg Formula: 0x00200 + HaAddr3_0
    Where  : 
           + $HaAddr3_0(0-15): HA Address Bit3_0
Reg Desc   : 
This register is used to send HA read address bit3_0 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha3_0_control_Base                                                                   0x00200
#define cAf6Reg_rdha3_0_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr3_0
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr3_0
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha3_0_control_ReadAddr3_0_Mask                                                         cBit19_0
#define cAf6_rdha3_0_control_ReadAddr3_0_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit7_4 Control
Reg Addr   : 0x00210
Reg Formula: 0x00210 + HaAddr7_4
    Where  : 
           + $HaAddr7_4(0-15): HA Address Bit7_4
Reg Desc   : 
This register is used to send HA read address bit7_4 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha7_4_control_Base                                                                   0x00210
#define cAf6Reg_rdha7_4_control_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ReadAddr7_4
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr7_4
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha7_4_control_ReadAddr7_4_Mask                                                         cBit19_0
#define cAf6_rdha7_4_control_ReadAddr7_4_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit11_8 Control
Reg Addr   : 0x00220
Reg Formula: 0x00220 + HaAddr11_8
    Where  : 
           + $HaAddr11_8(0-15): HA Address Bit11_8
Reg Desc   : 
This register is used to send HA read address bit11_8 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha11_8_control_Base                                                                  0x00220
#define cAf6Reg_rdha11_8_control_WidthVal                                                                   32

/*--------------------------------------
BitField Name: ReadAddr11_8
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr11_8
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha11_8_control_ReadAddr11_8_Mask                                                       cBit19_0
#define cAf6_rdha11_8_control_ReadAddr11_8_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit15_12 Control
Reg Addr   : 0x00230
Reg Formula: 0x00230 + HaAddr15_12
    Where  : 
           + $HaAddr15_12(0-15): HA Address Bit15_12
Reg Desc   : 
This register is used to send HA read address bit15_12 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha15_12_control_Base                                                                 0x00230
#define cAf6Reg_rdha15_12_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr15_12
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr15_12
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha15_12_control_ReadAddr15_12_Mask                                                     cBit19_0
#define cAf6_rdha15_12_control_ReadAddr15_12_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit19_16 Control
Reg Addr   : 0x00240
Reg Formula: 0x00240 + HaAddr19_16
    Where  : 
           + $HaAddr19_16(0-15): HA Address Bit19_16
Reg Desc   : 
This register is used to send HA read address bit19_16 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha19_16_control_Base                                                                 0x00240
#define cAf6Reg_rdha19_16_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr19_16
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr19_16
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha19_16_control_ReadAddr19_16_Mask                                                     cBit19_0
#define cAf6_rdha19_16_control_ReadAddr19_16_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit23_20 Control
Reg Addr   : 0x00250
Reg Formula: 0x00250 + HaAddr23_20
    Where  : 
           + $HaAddr23_20(0-15): HA Address Bit23_20
Reg Desc   : 
This register is used to send HA read address bit23_20 to HA engine

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha23_20_control_Base                                                                 0x00250
#define cAf6Reg_rdha23_20_control_WidthVal                                                                  32

/*--------------------------------------
BitField Name: ReadAddr23_20
BitField Type: RW
BitField Desc: Read value will be 0x01000 plus HaAddr23_20
BitField Bits: [19:0]
--------------------------------------*/
#define cAf6_rdha23_20_control_ReadAddr23_20_Mask                                                     cBit19_0
#define cAf6_rdha23_20_control_ReadAddr23_20_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Address Bit24 and Data Control
Reg Addr   : 0x00260
Reg Formula: 0x00260 + HaAddr24
    Where  : 
           + $HaAddr24(0-1): HA Address Bit24
Reg Desc   : 
This register is used to send HA read address bit24 to HA engine to read data

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha24data_control_Base                                                                0x00260

/*--------------------------------------
BitField Name: ReadHaData31_0
BitField Type: RW
BitField Desc: HA read data bit31_0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha24data_control_ReadHaData31_0_Mask                                                   cBit31_0
#define cAf6_rdha24data_control_ReadHaData31_0_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data63_32
Reg Addr   : 0x00270
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdha_hold63_32_Base                                                                    0x00270

/*--------------------------------------
BitField Name: ReadHaData63_32
BitField Type: RW
BitField Desc: HA read data bit63_32
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdha_hold63_32_ReadHaData63_32_Mask                                                      cBit31_0
#define cAf6_rdha_hold63_32_ReadHaData63_32_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data95_64
Reg Addr   : 0x00271
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold95_64_Base                                                                  0x00271

/*--------------------------------------
BitField Name: ReadHaData95_64
BitField Type: RW
BitField Desc: HA read data bit95_64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Mask                                                    cBit31_0
#define cAf6_rdindr_hold95_64_ReadHaData95_64_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Read HA Hold Data127_96
Reg Addr   : 0x00272
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read HA dword4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rdindr_hold127_96_Base                                                                 0x00272

/*--------------------------------------
BitField Name: ReadHaData127_96
BitField Type: RW
BitField Desc: HA read data bit127_96
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Mask                                                  cBit31_0
#define cAf6_rdindr_hold127_96_ReadHaData127_96_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Temperate Event
Reg Addr   : 0x00_00D0
Reg Formula: 
    Where  : 
Reg Desc   : 
This register report state change of Temperate status.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_temp_Base                                                                         0x0000D0

/*--------------------------------------
BitField Name: Temp_alarm
BitField Type: WC
BitField Desc: Temperate change Event  1: Changed  0: Normal
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_alm_temp_Temp_alarm_Mask                                                                    cBit0
#define cAf6_alm_temp_Temp_alarm_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : PDA DDR CRC Error Counter
Reg Addr   : 0x000142(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of CRC error detected

------------------------------------------------------------------------------*/
#define cAf6Reg_PDA_DDR_CRC_Error_Counter_ro_Base                                                     0x000142

/*--------------------------------------
BitField Name: PDADDRCRCError
BitField Type: RW
BitField Desc: This counter count the number of DDR CRC error.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_PDA_DDR_CRC_Error_Counter_ro_PDADDRCRCError_Mask                                         cBit31_0
#define cAf6_PDA_DDR_CRC_Error_Counter_ro_PDADDRCRCError_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : PDA DDR CRC Error Counter
Reg Addr   : 0x000141(R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of CRC error detected

------------------------------------------------------------------------------*/
#define cAf6Reg_PDA_DDR_CRC_Error_Counter_r2c_Base                                                    0x000141

/*--------------------------------------
BitField Name: PDADDRCRCError
BitField Type: RW
BitField Desc: This counter count the number of DDR CRC error.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_PDA_DDR_CRC_Error_Counter_r2c_PDADDRCRCError_Mask                                        cBit31_0
#define cAf6_PDA_DDR_CRC_Error_Counter_r2c_PDADDRCRCError_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : PDA DDR Force Error Control
Reg Addr   : 0x000140
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_PDA_DDR_errins_en_cfg_Base                                                            0x000140

/*--------------------------------------
BitField Name: PDADDRErrRate
BitField Type: RW
BitField Desc: 0: One-shot with number of errors; 1:Line rate
BitField Bits: [28]
--------------------------------------*/
#define cAf6_PDA_DDR_errins_en_cfg_PDADDRErrRate_Mask                                                   cBit28
#define cAf6_PDA_DDR_errins_en_cfg_PDADDRErrRate_Shift                                                      28

/*--------------------------------------
BitField Name: PDADDRErrThr
BitField Type: RW
BitField Desc: Error Threshold in bit unit or the number of error in one-shot
mode. Valid value from 1
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_PDA_DDR_errins_en_cfg_PDADDRErrThr_Mask                                                  cBit27_0
#define cAf6_PDA_DDR_errins_en_cfg_PDADDRErrThr_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Rx DDR ECC Correctable Error Counter
Reg Addr   : 0x00014a(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of Rx DDR ECC Correctable error detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Rx_DDR_ECC_Cor_Error_Counter_ro_Base                                                  0x00014a

/*--------------------------------------
BitField Name: RxDDRECCCorError
BitField Type: RW
BitField Desc: This counter count the number of RxDDR ECC Correctable error.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Rx_DDR_ECC_Cor_Error_Counter_ro_RxDDRECCCorError_Mask                                    cBit31_0
#define cAf6_Rx_DDR_ECC_Cor_Error_Counter_ro_RxDDRECCCorError_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : Rx DDR ECC Correctable Error Counter
Reg Addr   : 0x000149(R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of Rx DDR ECC Correctable error detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Rx_DDR_ECC_Cor_Error_Counter_r2c_Base                                                 0x000149

/*--------------------------------------
BitField Name: RxDDRECCCorError
BitField Type: RW
BitField Desc: This counter count the number of RxDDR ECC Correctable error.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Rx_DDR_ECC_Cor_Error_Counter_r2c_RxDDRECCCorError_Mask                                   cBit31_0
#define cAf6_Rx_DDR_ECC_Cor_Error_Counter_r2c_RxDDRECCCorError_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : Rx DDR ECC UnCorrectable Error Counter
Reg Addr   : 0x00014C(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of Rx DDR ECC UnCorrectable error detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Rx_DDR_ECC_UnCor_Error_Counter_ro_Base                                                0x00014C

/*--------------------------------------
BitField Name: RxDDRECCUnCorError
BitField Type: RW
BitField Desc: This counter count the number of Rx DDR ECC unCorrectable error.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Rx_DDR_ECC_UnCor_Error_Counter_ro_RxDDRECCUnCorError_Mask                                cBit31_0
#define cAf6_Rx_DDR_ECC_UnCor_Error_Counter_ro_RxDDRECCUnCorError_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Rx DDR ECC UnCorrectable Error Counter
Reg Addr   : 0x00014B(R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of Rx DDR ECC UnCorrectable error detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Rx_DDR_ECC_UnCor_Error_Counter_r2c_Base                                               0x00014B

/*--------------------------------------
BitField Name: RxDDRECCUnCorError
BitField Type: RW
BitField Desc: This counter count the number of Rx DDR ECC unCorrectable error.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Rx_DDR_ECC_UnCor_Error_Counter_r2c_RxDDRECCUnCorError_Mask                                cBit31_0
#define cAf6_Rx_DDR_ECC_UnCor_Error_Counter_r2c_RxDDRECCUnCorError_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Rx DDR Force Error Control
Reg Addr   : 0x000148
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Rx_DDR_errins_en_cfg_Base                                                             0x000148

/*--------------------------------------
BitField Name: RxDDRErrMode
BitField Type: RW
BitField Desc: 0: Correctable (1bit); 1:UnCorrectable (2bit)
BitField Bits: [29]
--------------------------------------*/
#define cAf6_Rx_DDR_errins_en_cfg_RxDDRErrMode_Mask                                                     cBit29
#define cAf6_Rx_DDR_errins_en_cfg_RxDDRErrMode_Shift                                                        29

/*--------------------------------------
BitField Name: RxDDRErrRate
BitField Type: RW
BitField Desc: 0: One-shot with number of errors; 1:Line rate
BitField Bits: [28]
--------------------------------------*/
#define cAf6_Rx_DDR_errins_en_cfg_RxDDRErrRate_Mask                                                     cBit28
#define cAf6_Rx_DDR_errins_en_cfg_RxDDRErrRate_Shift                                                        28

/*--------------------------------------
BitField Name: RxDDRErrThr
BitField Type: RW
BitField Desc: Error Threshold in bit unit or the number of error in one-shot
mode. Valid value from 1
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_Rx_DDR_errins_en_cfg_RxDDRErrThr_Mask                                                    cBit27_0
#define cAf6_Rx_DDR_errins_en_cfg_RxDDRErrThr_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Tx DDR ECC Correctable Error Counter
Reg Addr   : 0x000152(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of Tx DDR ECC Correctable error detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Tx_DDR_ECC_Cor_Error_Counter_ro_Base                                                  0x000152

/*--------------------------------------
BitField Name: TxDDRECCCorError
BitField Type: RW
BitField Desc: This counter count the number of TxDDR ECC Correctable error.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Tx_DDR_ECC_Cor_Error_Counter_ro_TxDDRECCCorError_Mask                                    cBit31_0
#define cAf6_Tx_DDR_ECC_Cor_Error_Counter_ro_TxDDRECCCorError_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : Tx DDR ECC Correctable Error Counter
Reg Addr   : 0x000151(R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of Tx DDR ECC Correctable error detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Tx_DDR_ECC_Cor_Error_Counter_r2c_Base                                                 0x000151

/*--------------------------------------
BitField Name: TxDDRECCCorError
BitField Type: RW
BitField Desc: This counter count the number of TxDDR ECC Correctable error.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Tx_DDR_ECC_Cor_Error_Counter_r2c_TxDDRECCCorError_Mask                                   cBit31_0
#define cAf6_Tx_DDR_ECC_Cor_Error_Counter_r2c_TxDDRECCCorError_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : Tx DDR ECC UnCorrectable Error Counter
Reg Addr   : 0x000154(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of Rx DDR ECC UnCorrectable error detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Tx_DDR_ECC_UnCor_Error_Counter_ro_Base                                                0x000154

/*--------------------------------------
BitField Name: TxDDRECCUnCorError
BitField Type: RW
BitField Desc: This counter count the number of Tx DDR ECC unCorrectable error.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Tx_DDR_ECC_UnCor_Error_Counter_ro_TxDDRECCUnCorError_Mask                                cBit31_0
#define cAf6_Tx_DDR_ECC_UnCor_Error_Counter_ro_TxDDRECCUnCorError_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Tx DDR ECC UnCorrectable Error Counter
Reg Addr   : 0x000153(R2C)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of Rx DDR ECC UnCorrectable error detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Tx_DDR_ECC_UnCor_Error_Counter_r2c_Base                                               0x000153

/*--------------------------------------
BitField Name: TxDDRECCUnCorError
BitField Type: RW
BitField Desc: This counter count the number of Tx DDR ECC unCorrectable error.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Tx_DDR_ECC_UnCor_Error_Counter_r2c_TxDDRECCUnCorError_Mask                                cBit31_0
#define cAf6_Tx_DDR_ECC_UnCor_Error_Counter_r2c_TxDDRECCUnCorError_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Tx DDR Force Error Control
Reg Addr   : 0x000150
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Tx_DDR_errins_en_cfg_Base                                                             0x000150

/*--------------------------------------
BitField Name: TxDDRErrMode
BitField Type: RW
BitField Desc: 0: Correctable (1bit); 1:UnCorrectable (2bit)
BitField Bits: [29]
--------------------------------------*/
#define cAf6_Tx_DDR_errins_en_cfg_TxDDRErrMode_Mask                                                     cBit29
#define cAf6_Tx_DDR_errins_en_cfg_TxDDRErrMode_Shift                                                        29

/*--------------------------------------
BitField Name: TxDDRErrRate
BitField Type: RW
BitField Desc: 0: One-shot with number of errors; 1:Line rate
BitField Bits: [28]
--------------------------------------*/
#define cAf6_Tx_DDR_errins_en_cfg_TxDDRErrRate_Mask                                                     cBit28
#define cAf6_Tx_DDR_errins_en_cfg_TxDDRErrRate_Shift                                                        28

/*--------------------------------------
BitField Name: TxDDRErrThr
BitField Type: RW
BitField Desc: Error Threshold in bit unit or the number of error in one-shot
mode. Valid value from 1
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_Tx_DDR_errins_en_cfg_TxDDRErrThr_Mask                                                    cBit27_0
#define cAf6_Tx_DDR_errins_en_cfg_TxDDRErrThr_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Tx PW SubPort VLAN Control
Reg Addr   : 0x00010E
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Tx_PW_SubPort_VLAN_Ctrl_Base                                                          0x00010E

/*--------------------------------------
BitField Name: TxPWSubPortVlan2
BitField Type: RW
BitField Desc: SubPort VLAN 2 to insert PW packet
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_Tx_PW_SubPort_VLAN_Ctrl_TxPWSubPortVlan2_Mask                                            cBit31_0
#define cAf6_Tx_PW_SubPort_VLAN_Ctrl_TxPWSubPortVlan2_Shift                                                  0

/*--------------------------------------
BitField Name: TxPWSubPortVlan1
BitField Type: RW
BitField Desc: SubPort VLAN 1 to insert PW packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Tx_PW_SubPort_VLAN_Ctrl_TxPWSubPortVlan1_Mask                                            cBit31_0
#define cAf6_Tx_PW_SubPort_VLAN_Ctrl_TxPWSubPortVlan1_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Rx PW SubPort VLAN Control
Reg Addr   : 0x00010F
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_Rx_PW_SubPort_VLAN_Ctrl_Base                                                          0x00010F

/*--------------------------------------
BitField Name: RxPWSubPortVlan2
BitField Type: RW
BitField Desc: SubPort VLAN 2 to check PW packet
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_Rx_PW_SubPort_VLAN_Ctrl_RxPWSubPortVlan2_Mask                                            cBit31_0
#define cAf6_Rx_PW_SubPort_VLAN_Ctrl_RxPWSubPortVlan2_Shift                                                  0

/*--------------------------------------
BitField Name: RxPWSubPortVlan1
BitField Type: RW
BitField Desc: SubPort VLAN 1 to check PW packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Rx_PW_SubPort_VLAN_Ctrl_RxPWSubPortVlan1_Mask                                            cBit31_0
#define cAf6_Rx_PW_SubPort_VLAN_Ctrl_RxPWSubPortVlan1_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Chip Init Request Control
Reg Addr   : 0x00_0160
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure init request.

------------------------------------------------------------------------------*/
#define cAf6Reg_init_request_Base                                                                     0x000160

/*--------------------------------------
BitField Name: InitReq
BitField Type: RW
BitField Desc: Init request. CPU set 1 to request init. HW clear 0 upon Init
Done. Each bit for each module InitReq[15]: DCC InitReq[14]: PMFM InitReq[13]:
CLA InitReq[12]: PWE InitReq[11]: PDA InitReq[10]: PLA InitReq[9]: CDR
InitReq[8]: DeMAP InitReq[7]: MAP InitReq[6]: PRM InitReq[5]: PDH InitReq[4]:
POH InitReq[3]: CLS 1G InitReq[2]: MAC 1G for DIM InitReq[1]: CLS 2.5G
InitReq[0]: MAC 2.5G
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_init_request_InitReq_Mask                                                                cBit31_0
#define cAf6_init_request_InitReq_Shift                                                                      0

#endif /* _AF6_REG_AF6CNC0061_RD_GLB_H_ */

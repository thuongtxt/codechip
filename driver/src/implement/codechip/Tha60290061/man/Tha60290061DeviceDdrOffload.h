/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60290061DeviceDdrOffload.h
 * 
 * Created Date: Aug 13, 2018
 *
 * Description : 60290061 Device DDR Offload interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061DEVICEDDROFFLOAD_H_
#define _THA60290061DEVICEDDROFFLOAD_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290061DeviceDdrOffloadCfgLoad(AtDevice self, uint32 subDeviceId, uint8 coreId);
eBool Tha60290061DeviceDdrOffloadCfgLoadIsDone(AtDevice self, uint32 subDeviceId, uint8 coreId);
uint16 Tha60290061DeviceReadOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId);
uint16 Tha60290061DeviceWriteOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 regAddress,const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId);

/* Read/write access to DDR offload */
uint32 Tha60290061DeviceDdrOffloadRead(AtDevice self, uint32 subDeviceId, uint32 address);
void Tha60290061DeviceDdrOffloadWrite(AtDevice self, uint32 subDeviceId, uint32 address, uint32 value);
uint16 Tha60290061DeviceDdrOffloadLongWrite(AtDevice self, uint32 subDeviceId, uint32 address, const uint32 *value, uint16 numValues);
eAtRet Tha60290061DeviceDdrOffloadHwFlush(AtDevice self, uint32 subDeviceId, uint8 coreId);

void Tha60290061DeviceDdrOffloadSwitchTimeShow(AtDevice self);
eAtRet Tha60290061DeviceDdrOffloadIdSet(AtDevice self, uint32 subDeviceId, uint8 coreId);
uint32 Tha60290061DeviceDdrOffloadIdGet(AtDevice self, uint8 coreId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061DEVICEDDROFFLOAD_H_ */


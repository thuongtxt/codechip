/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60290061SubDevice.h
 * 
 * Created Date: Aug 29, 2018
 *
 * Description : 60290061 Sub-devices
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061SUBDEVICE_H_
#define _THA60290061SUBDEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "Tha60290061SubDeviceDebugger.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290061SubDevice *Tha60290061SubDevice;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60290061SubDeviceDebugger Tha60290061SubDeviceDebuggerGet(AtDevice self);
uint8 Tha60290061SubDeviceIdGet(AtDevice self);

eAtDeviceRole Tha60290061SubDeviceAsyncRoleGet(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061SUBDEVICE_H_ */


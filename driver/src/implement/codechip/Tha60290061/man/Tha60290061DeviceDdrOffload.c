/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60290061DeviceDdrOffload.c
 *
 * Created Date: Aug 13, 2018
 *
 * Description : 60290061 device ddr off load implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtIpCoreInternal.h"
#include "Tha60290061DeviceInternal.h"
#include "Tha60290061DeviceDdrOffloadReg.h"
#include "Tha60290061DeviceDdrOffload.h"
#include "Tha60290061Device.h"
#include "Tha60290061SubDeviceDebugger.h"
#include "Tha60290061SubDevice.h"
#include "Tha60290061DeviceGlbReg.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290061Device)(self))
#define cDdrCfgDbRegLenghtInDword 4
#define cDdrCfgDbRegAddressIndex  3
#define cDdrCfgDbRegAddressMask        cBit23_0
#define cDdrCfgDbRegAddressValidMask   cBit24
#define cDdrCfgDbRegAddressValidShift  24

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290061DdrLookupTable TableGet(AtDevice self)
    {
    return Tha60290061DeviceDdrLookupTableGet((Tha60290061Device)self);
    }

static void DdrOffloadInfoDisplay(AtDevice self, tTha60290061DdrAddressEntry* entry)
    {
    AtDeviceLog(self, cAtLogLevelCritical, __FILE__, __LINE__, "RegName=%s\r\n", entry->regName);
    AtDeviceLog(self, cAtLogLevelCritical, __FILE__, __LINE__, "DdrAddressBase=0x%x\r\n", entry->ddrAddress);
    AtDeviceLog(self, cAtLogLevelCritical, __FILE__, __LINE__, "RegBase=0x%x\r\n", entry->regBase);
    AtDeviceLog(self, cAtLogLevelCritical, __FILE__, __LINE__, "RegDepth=%d\r\n", entry->regDepth);
    AtDeviceLog(self, cAtLogLevelCritical, __FILE__, __LINE__, "RegWidth=%d\r\n", entry->regWidth);
    }

static eAtRet ShortRegAudit(AtDevice self, uint32 reg_address,
                           uint32 ddr_address, char* regName,
                           uint32 subDeviceId, AtFile file,
                           eTha60290061AuditMode auditMode)
    {
    AtIpCore ipCore = AtDeviceIpCoreGet(self, 0);
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 regVal = AtHalRead(hal, reg_address);
    uint32 ddrlongRegVal[cThaLongRegMaxSize] = {0};
    uint16 readNumDword = 0;
    eAtRet ret = cAtOk;
    uint32 addresFromDdr, regValFromDdr, validBitVal;

    readNumDword = AtDeviceReadOnDdrOffload(self, subDeviceId, ddr_address, ddrlongRegVal, cThaLongRegMaxSize, 0);
    if (readNumDword < cDdrCfgDbRegLenghtInDword)
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "DDROffload Cfg Reg Read not enough 128 bits\r\n");
        return cAtError;
        }

    addresFromDdr = ddrlongRegVal[cDdrCfgDbRegAddressIndex] & cDdrCfgDbRegAddressMask;
    validBitVal = ddrlongRegVal[cDdrCfgDbRegAddressIndex] & cDdrCfgDbRegAddressValidMask;
    regValFromDdr = ddrlongRegVal[0];

    if (auditMode == cTha60290061AuditNoCompare)
        {
        AtFilePrintf(file, "Short Reg %s Address@0x%x, regVal = 0x%x And in DdrCfgDb DDRAddress@0x%x regVal = %s\r\n",
                             regName, reg_address, regVal, ddr_address, ThaDeviceUtilLongRegValue2String(ddrlongRegVal, cDdrCfgDbRegLenghtInDword));
        return ret;
        }

    if (regVal != regValFromDdr)
        {
        AtFilePrintf(file, "Short Reg %s Address@0x%x, regVal = 0x%x but in DdrCfgDb DDRAddress@0x%x regVal = 0x%x\r\n",
                     regName, reg_address, regVal, ddr_address, regValFromDdr);
        ret = cAtError;
        }

    if (reg_address != addresFromDdr)
        {
        AtFilePrintf(file, "Short Reg %s Address@0x%x, but in DdrCfgDb DDRAddress@0x%x regAddress = 0x%x\r\n",
                     regName, reg_address, ddr_address, addresFromDdr);
        ret = cAtError;
        }

    if (validBitVal == 0)
        {
        AtFilePrintf(file, "Short Reg %s Address@0x%x, but in DdrCfgDb DDRAddress@0x%x regAddress = 0x%x is not valid\r\n",
                     regName, reg_address, ddr_address, addresFromDdr);
        ret = cAtError;
        }

    return ret;
    }

static eAtRet LongRegAudit(AtDevice self, uint32 reg_address, uint32 ddr_address,
                          uint16 regSizeInDword, char *regName,
                          uint32 subDeviceId, AtFile file,
                          eTha60290061AuditMode auditMode)
    {
    uint32 longregVal[cThaLongRegMaxSize] = {0};
    uint32 ddrlongRegVal[cThaLongRegMaxSize] = {0};
    uint16 readNumDword = 0;
    eAtRet ret = cAtOk;
    uint32 addresFromDdr, validBitVal;
    char tempString[150];

    readNumDword = AtDeviceReadOnDdrOffload(self, subDeviceId, ddr_address, ddrlongRegVal, cThaLongRegMaxSize, 0);
    if (readNumDword < cDdrCfgDbRegLenghtInDword)
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "DDROffload Cfg Reg Read not enough 128 bits\r\n");
        return cAtError;
        }

    readNumDword = AtDeviceLongReadOnCore(self, reg_address, longregVal, cThaLongRegMaxSize, 0);


    addresFromDdr = ddrlongRegVal[cDdrCfgDbRegAddressIndex] & cDdrCfgDbRegAddressMask;
    validBitVal = ddrlongRegVal[cDdrCfgDbRegAddressIndex] & cDdrCfgDbRegAddressValidMask;

    if (auditMode == cTha60290061AuditNoCompare)
        {
        AtSprintf(tempString, "%s", ThaDeviceUtilLongRegValue2String(longregVal, regSizeInDword));
        AtFilePrintf(file, "Long Reg %s Address@0x%x, regVal = %s And in DdrCfgDb DDRAddress@0x%x regVal = %s\r\n", regName, reg_address,
                     tempString,
                     ddr_address, ThaDeviceUtilLongRegValue2String(ddrlongRegVal, cDdrCfgDbRegLenghtInDword));
        return ret;
        }

    if (AtOsalMemCmp(longregVal, ddrlongRegVal, regSizeInDword * sizeof(uint32)))
        {
        AtSprintf(tempString, "%s", ThaDeviceUtilLongRegValue2String(longregVal, regSizeInDword));
        AtFilePrintf(file, "Long Reg %s Address@0x%x, regVal = %s but in DdrCfgDb DDRAddress@0x%x regVal = %s\r\n", regName, reg_address,
                     tempString,
                     ddr_address, ThaDeviceUtilLongRegValue2String(ddrlongRegVal, regSizeInDword));
        ret = cAtError;
        }

    if (reg_address != addresFromDdr)
        {
        AtFilePrintf(file, "Long Reg %s Address@0x%x, but in DdrCfgDb DDRAddress@0x%x regAddress = 0x%x\r\n",
                     regName, reg_address, ddr_address, addresFromDdr);
        ret = cAtError;
        }

    if (validBitVal == 0)
        {
        AtFilePrintf(file, "Long Reg %s Address@0x%x, but in DdrCfgDb DDRAddress@0x%x regAddress = 0x%x is not valid\r\n",
                     regName, reg_address, ddr_address, addresFromDdr);
        ret = cAtError;
        }

    return ret;
    }

static uint16 RegSizeInBitToInDword(uint16 numBits)
    {
    uint16 ret = 0;
    ret = (uint16)(numBits / 32);
    if (numBits % 32)
        ret = (uint16)(ret + 1);
    return ret;
    }

static eAtRet Module_DdrOffloadAudit(AtDevice self, ThaBitMask usedRegMask, uint32 subDeviceId,
                                     tTha60290061DdrAddressEntry *lookupArray, uint32 arrayLen,
                                     AtFile file, eTha60290061AuditMode auditMode)
    {
    uint32 index = 0;
    uint32 reg_address = 0, ddr_address, addressMin, addressMax;
    eAtRet ret = cAtOk; /*matched*/
    tTha60290061DdrAddressEntry* entry = NULL;

    for (index = 0; index < arrayLen; index++)
        {
        entry = &lookupArray[index];
        addressMin = entry->regBase;
        addressMax = addressMin + entry->regDepth;
        ddr_address = entry->ddrAddress;
        for (reg_address = addressMin; reg_address < addressMax; reg_address++)
            {
            if (ThaBitMaskBitVal(usedRegMask, reg_address))
                {
                uint16 regSizeInDwork = RegSizeInBitToInDword((uint16)entry->regWidth);
                if (regSizeInDwork < 2)
                    ret |= ShortRegAudit(self, reg_address, ddr_address, (char*)entry->regName, subDeviceId,
                                         file, auditMode);
                else
                    ret |= LongRegAudit(self, reg_address, ddr_address, regSizeInDwork, (char*)entry->regName, subDeviceId,
                                        file, auditMode);
                }
            ddr_address++;
            }
        }

    return ret;
    }

static eAtRet All_Modules_DdrOffloadAudit(AtDevice self, ThaBitMask usedRegMask, uint32 subDeviceId,
                                          AtFile file, eTha60290061AuditMode auditMode)
    {
    eAtRet ret = cAtOk;
    Tha60290061DdrLookupTable lookupTable = TableGet(self);
    uint32 numEntry = 0;

    tTha60290061DdrAddressEntry *entryArray = Tha60290061DdrLookupTableOcnEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTablePohEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTablePdhEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTableMapEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTableCdrEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTablePlaEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTablePweEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTableClaEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTablePdaEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTableBertMonEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTableBertGenEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTablePrmEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTablePdhMuxEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTablePmFmEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTableDccEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    entryArray = Tha60290061DdrLookupTablePmcEntryArray(lookupTable, &numEntry);
    ret |= Module_DdrOffloadAudit(self, usedRegMask, subDeviceId, entryArray, numEntry, file, auditMode);

    return ret;
    }

static eBool ShouldLogForTest(void)
    {
    return cAtFalse;
    }

static tTha60290061DdrAddressEntry* ShortReg_To_DdrOffload_Info(AtDevice self, uint32 reg_address)
    {
    Tha60290061DdrLookupTable lookupTable = TableGet(self);
    tTha60290061DdrAddressEntry* ret = Tha60290061DdrLookupTableAddressToDdrOffloadInfo(lookupTable, reg_address);

    if (ret == NULL)
        return ret;

    if (ret->regWidth > 32)
        {
        if (ShouldLogForTest())
            {
            AtDeviceLog(self, cAtLogLevelCritical, __FILE__, __LINE__, "Address 0x%x is short reg but has long reg in ddr offload lookup.\r\n", reg_address);
            DdrOffloadInfoDisplay(self, ret);
            }
        return NULL;
        }

    return ret;
    }

static tTha60290061DdrAddressEntry* LongReg_To_DdrOffload_Info(AtDevice self, uint32 reg_address)
    {
    Tha60290061DdrLookupTable lookupTable = TableGet(self);
    tTha60290061DdrAddressEntry* ret = Tha60290061DdrLookupTableAddressToDdrOffloadInfo(lookupTable, reg_address);

    if (ret == NULL)
        return ret;

    if (ret->regWidth > 32)
        return ret;

    if (ShouldLogForTest())
        {
        AtDeviceLog(self, cAtLogLevelCritical, __FILE__, __LINE__, "Address 0x%x is long reg but has short reg in ddr offload lookup.\r\n", reg_address);
        DdrOffloadInfoDisplay(self, ret);
        }

    return NULL;
    }

static uint32 RegAddessToDdrAddress(AtDevice self, tTha60290061DdrAddressEntry* entry, uint32 reg_address)
    {
    uint32 ddrBaseAddress = entry->ddrAddress;
    int32 offset = (int32)(reg_address - entry->regBase);
    uint32 ddrAddress = (uint32)((int32)ddrBaseAddress + offset);

    if (offset < 0)
        {
        AtDeviceLog(self, cAtLogLevelCritical, __FILE__, __LINE__, "Address 0x%x is violate ddr offload lookup.\r\n", reg_address);
        DdrOffloadInfoDisplay(self, entry);
        }

    return ddrAddress;
    }

static uint32 DdrOffloadBaseAddress(AtDevice self)
    {
    AtUnused(self);
    return 0x3F0000;
    }

static uint32 DdrOffloadInitAddress(AtDevice self)
    {
    uint32 address = cAf6Reg_upen_cfg_init_Base + DdrOffloadBaseAddress(self);
    return address;
    }

static uint32 DdrCfgDbHwAutoFlushAddress(AtDevice self)
    {
    uint32 address = cAf6Reg_upen_cfg_flush_Base + DdrOffloadBaseAddress(self);
    return address;
    }

static uint32 DdrCfgDbIdForFlushAddress(AtDevice self)
    {
    uint32 address = cAf6Reg_upen_cfg_db_flush_Base + DdrOffloadBaseAddress(self);
    return address;
    }

static uint32 DdrOffloadSubmoduleInitDoneStikyAddress(AtDevice self)
    {
    uint32 address = cAf6Reg_upen_stk_sub_done_Base + DdrOffloadBaseAddress(self);
    return address;
    }

static uint32 DdrOffloadCfgDbAddress(AtDevice self)
    {
    uint32 address = cAf6Reg_upen_cfg_db_Base + DdrOffloadBaseAddress(self);
    return address;
    }

static uint32 DdrOffloadReadWriteRequestAddress(AtDevice self)
    {
    uint32 address = cAf6Reg_upen_cfg_ctl_Base + DdrOffloadBaseAddress(self);
    return address;
    }

static uint32 DdrOffloadWriteContentAddress(AtDevice self)
    {
    uint32 address = cAf6Reg_upen_cfg_wrdat_Base + DdrOffloadBaseAddress(self);
    return address;
    }

static uint32 DdrOffloadReadContentAddress(AtDevice self)
    {
    uint32 address = cAf6Reg_upen_cfg_rddat_Base + DdrOffloadBaseAddress(self);
    return address;
    }

static uint32 DdrOffloadSwitchTimeAddress(AtDevice self, eBool r2c)
    {
    uint32 address = cAf6Reg_upen_cnt_time_Base + (uint32)(r2c ? 1 : 0) + DdrOffloadBaseAddress(self);
    return address;
    }

static eBool NeedToWaitCfgDone(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet DdrOffloadCfgLoad(AtDevice self, uint32 subDeviceId, uint8 coreId)
    {
    uint32 cfgDbAddress = DdrOffloadCfgDbAddress(self);
    uint32 initDbAddress = DdrOffloadInitAddress(self);
    uint32 submoduleOffloadDoneAddress = DdrOffloadSubmoduleInitDoneStikyAddress(self);
    AtHal hal = AtDeviceIpCoreHalGet(self, coreId);
    uint32 regVal = 0;
    const uint32 cMaxWaitDddrOffloadDone = 30; /*30ms*/
    uint32 timeWait = 0;
    tAtOsalCurTime startTime, midleTime;

    /*clear submodule sticky*/
    AtHalWrite(hal, submoduleOffloadDoneAddress, 0xFFFFFFFF);

    AtHalWrite(hal, cfgDbAddress, subDeviceId);
    AtHalWrite(hal, initDbAddress, 1);

    if (!NeedToWaitCfgDone(self) || AtDeviceIsSimulated(self) || AtDeviceWarmRestoreIsStarted(self))
        return cAtOk;

    regVal = AtHalRead(hal, initDbAddress);
    AtOsalCurTimeGet(&startTime);
    while ((regVal & cAf6_upen_cfg_init_req_init_Mask) && (timeWait < cMaxWaitDddrOffloadDone))
        {
        regVal = AtHalRead(hal, initDbAddress);
        AtOsalCurTimeGet(&midleTime);
        timeWait = AtOsalDifferenceTimeInMs(&midleTime, &startTime);
        }

    AtDeviceLog(self, cAtLogLevelDebug, __FILE__, __LINE__, "DDR off load for sub device %d, take %d (ms) \r\n", subDeviceId, timeWait);
    if (timeWait < cMaxWaitDddrOffloadDone)
        return cAtOk;

    return cAtErrorOperationTimeout;
    }

static eBool DdrOffloadCfgLoadIsDone(AtDevice self, uint32 subDeviceId, uint8 coreId)
    {
    uint32 initDbAddress = DdrOffloadInitAddress(self);
    AtHal hal = AtDeviceIpCoreHalGet(self, coreId);
    uint32 regVal = 0;
    uint32 hwDeviceId = Tha60290061DeviceDdrOffloadIdGet(self, coreId);

    if (AtDeviceWarmRestoreIsStarted(self))
        return cAtTrue;

    if (AtDeviceIsSimulated(self))
        return cAtTrue;

    if (hwDeviceId != subDeviceId)
        {
        AtDeviceLog(self, cAtLogLevelWarning, AtSourceLocation,
                    "Current HW device ID %d differs from input device ID %d\r\n",
                    hwDeviceId, subDeviceId);
        return cAtFalse;
        }

    regVal = AtHalRead(hal, initDbAddress);
    if (((regVal & cAf6_upen_cfg_init_req_init_Mask) == 0))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet DdrOffloadCfgIdSet(AtDevice self, uint32 subDeviceId, uint8 coreId)
    {
    uint32 cfgDbAddress = DdrOffloadCfgDbAddress(self);
    AtHal hal = AtDeviceIpCoreHalGet(self, coreId);

    AtHalWrite(hal, cfgDbAddress, subDeviceId);

    return cAtOk;
    }

static uint32 DdrOffloadCfgIdGet(AtDevice self, uint8 coreId)
    {
    uint32 cfgDbAddress = DdrOffloadCfgDbAddress(self);
    AtHal hal = AtDeviceIpCoreHalGet(self, coreId);
    uint32 regVal = 0;

    regVal = AtHalRead(hal, cfgDbAddress);

    return (uint32)(regVal & cAf6_upen_cfg_db_out_cfg_db_Mask);
    }

static void DdrOffloadSwitchTimeShow(AtDevice self)
    {
    uint32 address = DdrOffloadSwitchTimeAddress(self, cAtTrue);
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 regVal = AtHalRead(hal, address);

    AtPrintf("DdrOffload switch time counter (units of 155.52Mhz): %d\r\n", regVal);
    }

static void DdrWriteContentFlush(AtDevice self, uint8 coreId)
    {
    uint32 contentAddress = DdrOffloadWriteContentAddress(self);
    AtLongRegisterAccess longRegAccess = AtDeviceGlobalLongRegisterAccess(self);
    AtHal hal = AtDeviceIpCoreHalGet(self, coreId);
    uint32 offloadBuff[cThaLongRegMaxSize] = {0};

    AtLongRegisterAccessWrite(longRegAccess, hal, contentAddress, offloadBuff, cThaLongRegMaxSize);
    }

static eAtRet DdrOffloadHwFlush(AtDevice self, uint32 subDeviceId, uint8 coreId)
    {
    uint32 cfgDbAddress = DdrCfgDbIdForFlushAddress(self);
    uint32 flushDbAddress = DdrCfgDbHwAutoFlushAddress(self);
    AtHal hal = AtDeviceIpCoreHalGet(self, coreId);
    uint32 regVal = 0;
    const uint32 cMaxWaitDddrFlushDone = 1000; /*1*/
    uint32 timeWait = 0;
    tAtOsalCurTime startTime, midleTime;

    DdrWriteContentFlush(self, coreId);
    AtHalWrite(hal, cfgDbAddress, subDeviceId);
    AtHalWrite(hal, flushDbAddress, 1);

    if (AtDeviceIsSimulated(self) || AtDeviceWarmRestoreIsStarted(self))
        return cAtOk;

    regVal = AtHalRead(hal, flushDbAddress);
    AtOsalCurTimeGet(&startTime);
    while ((regVal & cAf6_upen_cfg_flush_req_flush_Mask) && (timeWait < cMaxWaitDddrFlushDone))
        {
        regVal = AtHalRead(hal, flushDbAddress);
        AtOsalCurTimeGet(&midleTime);
        timeWait = AtOsalDifferenceTimeInMs(&midleTime, &startTime);
        }

    AtDeviceLog(self, cAtLogLevelDebug, __FILE__, __LINE__, "Flush DDR Cfg Db at sub device %d, take %d (ms) \r\n", subDeviceId, timeWait);
    if (timeWait < cMaxWaitDddrFlushDone)
        return cAtOk;

    return cAtErrorOperationTimeout;
    }

static eAtRet DirectRegOffloadCfgEntryArray(AtDevice self, uint32 subDeviceId, uint8 coreId,
                                            tTha60290061DdrAddressEntry *entryArray, uint32 numEntry)
    {
    uint32 entry_i, offset_i;
    tTha60290061DdrAddressEntry *entry = NULL;
    uint32 address = 0;
    uint32 longReg[cThaLongRegMaxSize] = {0};
    AtIpCore deviceCore = AtDeviceIpCoreGet(self, coreId);
    AtHal deviceHal = AtIpCoreHalGet(deviceCore);
    AtDevice subDevice = AtDeviceSubDeviceGet(self, (uint8)subDeviceId);
    AtIpCore subDeviceCore = AtDeviceIpCoreGet(subDevice, coreId);
    AtHal subDeviceHal = IpCoreHalGet(subDeviceCore);

    for (entry_i = 0; entry_i < numEntry; entry_i++)
        {
        entry = &entryArray[entry_i];
        for (offset_i = 0; offset_i < entry->regDepth; offset_i ++)
            {
            address = entry->regBase + offset_i;
            if (entry->regWidth <= 32)
                {
                longReg[0] = AtHalRead(subDeviceHal, address);
                AtHalWrite(deviceHal, address, longReg[0]);
                }
            else
                {
                AtDeviceLongReadOnCore(subDevice, address, longReg, cThaLongRegMaxSize, coreId);
                AtDeviceLongWriteOnCore(self, address, longReg, cThaLongRegMaxSize, coreId);
                }
            }
        }

    return cAtOk;
    }

static eAtRet DirectRegOffloadCfgArray(AtDevice self, uint32 subDeviceId, uint8 coreId,
                                       tTha60290061DdrAddressEntry *(*EntryArrayGet)(Tha60290061DdrLookupTable, uint32 *))
    {
    Tha60290061DdrLookupTable lookupTable = TableGet(self);
    tTha60290061DdrAddressEntry *entryArray;
    uint32 numEntry;

    entryArray = EntryArrayGet(lookupTable, &numEntry);
    if (numEntry)
        return DirectRegOffloadCfgEntryArray(self, subDeviceId, coreId, entryArray, numEntry);

    return cAtOk;
    }

static eAtRet DirectRegOffloadCfg(AtDevice self, uint32 subDeviceId, uint8 coreId)
    {
    eAtRet ret;
    ret  = DirectRegOffloadCfgArray(self, subDeviceId, coreId, Tha60290061DdrLookupTableGlbEntryArray);
    ret |= DirectRegOffloadCfgArray(self, subDeviceId, coreId, Tha60290061DdrLookupTableCls2Dot5EntryArray);
    ret |= DirectRegOffloadCfgArray(self, subDeviceId, coreId, Tha60290061DdrLookupTableTopEntryArray);
    return ret;
    }

static eAtRet DdrOffloadAccessRequest(AtDevice self, uint32 subDeviceId, uint32 localAddress, eBool isRead, uint8 coreId)
    {
    uint32 readwriteRequestAddress = DdrOffloadReadWriteRequestAddress(self);
    AtHal hal = AtDeviceIpCoreHalGet(self, coreId);
    uint32 regVal = 0;
    const uint32 cMaxWaitDddrOffloadRdWrDone = 1; /*1ms*/
    uint32 timeWait = 0;
    tAtOsalCurTime startTime, midleTime;

    mRegFieldSet(regVal, cAf6_upen_cfg_ctl_cfg_ddradd_, localAddress);
    mRegFieldSet(regVal, cAf6_upen_cfg_ctl_cfg_db_, subDeviceId);
    mRegFieldSet(regVal, cAf6_upen_cfg_ctl_cfg_rnw_, (isRead ? 1 : 0));
    mRegFieldSet(regVal, cAf6_upen_cfg_ctl_enb_req_, 1);
    AtHalWrite(hal, readwriteRequestAddress, regVal);

    if (AtDeviceIsSimulated(self) || AtDeviceWarmRestoreIsStarted(self))
        return cAtOk;

    regVal = AtHalRead(hal, readwriteRequestAddress);
    AtOsalCurTimeGet(&startTime);
    while ((regVal & cAf6_upen_cfg_ctl_enb_req_Mask) && (timeWait < cMaxWaitDddrOffloadRdWrDone))
        {
        regVal = AtHalRead(hal, readwriteRequestAddress);
        AtOsalCurTimeGet(&midleTime);
        timeWait = AtOsalDifferenceTimeInMs(&midleTime, &startTime);
        }

    if (timeWait < cMaxWaitDddrOffloadRdWrDone)
        return cAtOk;

    return cAtErrorOperationTimeout;
    }

static uint16 ReadOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 localAddress,
                               uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    uint16 numRegs = 0;
    uint32 contentAddress = DdrOffloadReadContentAddress(self);
    AtLongRegisterAccess longRegAccess = AtDeviceGlobalLongRegisterAccess(self);
    AtHal hal = AtDeviceIpCoreHalGet(self, coreId);
    eAtRet ret = DdrOffloadAccessRequest(self, subDeviceId, localAddress, cAtTrue, coreId);

    if (ret == cAtOk)
        numRegs = AtLongRegisterAccessRead(longRegAccess, hal, contentAddress, dataBuffer, bufferLen);
    else
        AtDeviceLog(self, cAtLogLevelWarning, AtSourceLocation, "DDR offload Read registers 0x%08x time out\r\n", localAddress);

    return (uint16)(numRegs);
    }

static uint16 WriteOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 regAddress,
                                const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    uint16 numRegs = 0;
    uint32 contentAddress = DdrOffloadWriteContentAddress(self);
    AtLongRegisterAccess longRegAccess = AtDeviceGlobalLongRegisterAccess(self);
    AtHal hal = AtDeviceIpCoreHalGet(self, coreId);
    eAtRet ret = cAtOk;
    uint32 offloadBuff[cThaLongRegMaxSize] = {0};
    uint32 maxbufferSize = (uint32)(bufferLen > cThaLongRegMaxSize ? cThaLongRegMaxSize : bufferLen);
    maxbufferSize = maxbufferSize * sizeof(uint32);
    AtOsalMemCpy(offloadBuff, dataBuffer, maxbufferSize);
    offloadBuff[cDdrCfgDbRegAddressIndex] = regAddress & cDdrCfgDbRegAddressMask;
    mFieldIns(&offloadBuff[cDdrCfgDbRegAddressIndex], cDdrCfgDbRegAddressValidMask, cDdrCfgDbRegAddressValidShift, 1);

    numRegs = AtLongRegisterAccessWrite(longRegAccess, hal, contentAddress, offloadBuff, cDdrCfgDbRegLenghtInDword);
    ret = DdrOffloadAccessRequest(self, subDeviceId, ddrAddress, cAtFalse, coreId);

    if (ret != cAtOk)
        AtDeviceLog(self, cAtLogLevelWarning, AtSourceLocation, "DDR offload Write registers 0x%08x time out\r\n", ddrAddress);

    return numRegs;
    }

static eAtRet DeviceAllModulesActiveEnable(AtDevice self, eBool enable)
    {
    uint32 activeAddress = cAf6Reg_Active_Base;
    AtIpCore core = AtDeviceIpCoreGet(self, 0);
    AtHal hal = AtIpCoreHalGet(core);

    if (enable)
        AtHalWrite(hal, activeAddress, 0xFFFF);
    else
        AtHalWrite(hal, activeAddress, 0x0);

    return cAtOk;
    }

static eAtRet DeviceAllModulesInitRequest(AtDevice self)
    {
    uint32 moduleInitDoneAddress = cAf6Reg_init_request_Base;
    AtIpCore core = AtDeviceIpCoreGet(self, 0);
    AtHal hal = AtIpCoreHalGet(core);
    uint32 regVal = 0;
    const uint32 cMaxWaitAllModuleInitDone = 30; /*30ms*/
    uint32 timeWait = 0;
    tAtOsalCurTime startTime, midleTime;

    AtHalWrite(hal, moduleInitDoneAddress, 0xFFFF);

    if (AtDeviceIsSimulated(self) || AtDeviceWarmRestoreIsStarted(self))
        return cAtOk;

    regVal = AtHalRead(hal, moduleInitDoneAddress);
    AtOsalCurTimeGet(&startTime);
    while ((regVal > 0) && (timeWait < cMaxWaitAllModuleInitDone))
        {
        regVal = AtHalRead(hal, moduleInitDoneAddress);
        AtOsalCurTimeGet(&midleTime);
        timeWait = AtOsalDifferenceTimeInMs(&midleTime, &startTime);
        }

    if (timeWait < cMaxWaitAllModuleInitDone)
        {
        AtDeviceLog(self, cAtLogLevelDebug, AtSourceLocation,
                    "Request Offload AllModuleInit done, take %d (ms) \r\n", timeWait);
        return cAtOk;
        }

    AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation,
                "Request Offload AllModuleInit timout\r\n");

    return cAtErrorOperationTimeout;
    }

eAtRet Tha60290061DeviceDdrOffloadCfgLoad(AtDevice self, uint32 subDeviceId, uint8 coreId)
    {
    eAtRet ret = cAtOk;

    ret = DeviceAllModulesActiveEnable(self, cAtFalse);
    if (ret != cAtOk)
        return ret;

    ret = DeviceAllModulesInitRequest(self);
    if (ret != cAtOk)
        return ret;

    ret = DdrOffloadCfgLoad(self, subDeviceId, coreId);
    if (ret != cAtOk)
        return ret;

    ret = DirectRegOffloadCfg(self, subDeviceId, coreId);
    if (ret != cAtOk)
        return ret;

    ret = DeviceAllModulesActiveEnable(self, cAtTrue);
    if (ret != cAtOk)
        return ret;

    ret = AtDeviceInterruptRestore(self);
    if (ret != cAtOk)
        return ret;

    return ret;
    }

eBool Tha60290061DeviceDdrOffloadCfgLoadIsDone(AtDevice self, uint32 subDeviceId, uint8 coreId)
    {
    return DdrOffloadCfgLoadIsDone(self, subDeviceId, coreId);
    }

uint16 Tha60290061DeviceReadOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 localAddress,
                                         uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    return ReadOnDdrOffload(self, subDeviceId, localAddress, dataBuffer, bufferLen, coreId);
    }

uint16 Tha60290061DeviceWriteOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 regAddress,
                                          const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    return WriteOnDdrOffload(self, subDeviceId, ddrAddress, regAddress, dataBuffer, bufferLen, coreId);
    }

uint32 Tha60290061DeviceDdrOffloadRead(AtDevice self, uint32 subDeviceId, uint32 address)
    {
    tTha60290061DdrAddressEntry* entry = ShortReg_To_DdrOffload_Info(self, address);
    uint32 ddrAddress = 0;
    uint32 longReg[cThaLongRegMaxSize] = {0};

    if (entry == NULL)
        return 0;

    ddrAddress = RegAddessToDdrAddress(self, entry, address);
    AtDeviceReadOnDdrOffload(self, subDeviceId, ddrAddress, longReg, cThaLongRegMaxSize, 0);

    return longReg[0];
    }

void Tha60290061DeviceDdrOffloadWrite(AtDevice self, uint32 subDeviceId, uint32 address, uint32 value)
    {
    tTha60290061DdrAddressEntry* entry = ShortReg_To_DdrOffload_Info(self, address);
    uint32 ddrAddress = 0;
    uint32 longReg[cThaLongRegMaxSize] = {0};

    if (entry == NULL)
        return;
    longReg[0] = value;
    ddrAddress = RegAddessToDdrAddress(self, entry, address);
    AtDeviceWriteOnDdrOffload(self, subDeviceId, ddrAddress, address, longReg, cThaLongRegMaxSize, 0);

    if (Tha60290061DeviceAuditIsEnabled(mThis(self)))
        {
        AtDevice subDevice = AtDeviceSubDeviceGet(self, (uint8)subDeviceId);
        Tha60290061SubDeviceDebugger debugger = Tha60290061SubDeviceDebuggerGet(subDevice);
        Tha60290061SubDeviceDebuggerAddressUse(debugger,  address);
        }
    }

uint16 Tha60290061DeviceDdrOffloadLongWrite(AtDevice self, uint32 subDeviceId,
                                            uint32 address, const uint32 *value, uint16 numValues)
    {
    tTha60290061DdrAddressEntry* entry = LongReg_To_DdrOffload_Info(self, address);
    uint32 ddrAddress = 0;
    uint16 numDwords;

    if (entry == NULL)
        return 0;

    ddrAddress = RegAddessToDdrAddress(self, entry, address);
    numDwords = AtDeviceWriteOnDdrOffload(self, subDeviceId, ddrAddress, address, value, numValues, 0);

    if (Tha60290061DeviceAuditIsEnabled(mThis(self)))
        {
        AtDevice subDevice = AtDeviceSubDeviceGet(self, (uint8)subDeviceId);
        Tha60290061SubDeviceDebugger debugger = Tha60290061SubDeviceDebuggerGet(subDevice);
        Tha60290061SubDeviceDebuggerAddressUse(debugger,  address);
        }

    return numDwords;
    }

eAtRet Tha60290061DeviceDdrOffloadHwFlush(AtDevice self, uint32 subDeviceId, uint8 coreId)
    {
    return DdrOffloadHwFlush(self, subDeviceId, coreId);
    }

eAtRet Tha60290061DeviceDdrOffloadIdSet(AtDevice self, uint32 subDeviceId, uint8 coreId)
    {
    return DdrOffloadCfgIdSet(self, subDeviceId, coreId);
    }

uint32 Tha60290061DeviceDdrOffloadIdGet(AtDevice self, uint8 coreId)
    {
    return DdrOffloadCfgIdGet(self, coreId);
    }

void Tha60290061DeviceDdrOffloadSwitchTimeShow(AtDevice self)
    {
    DdrOffloadSwitchTimeShow(self);
    }

eAtRet Tha60290061DeviceDdrOffloadDbAudit(AtDevice self, uint32 subDeviceId,
                                          AtFile outputFile, eTha60290061AuditMode auditMode)
    {
    eAtRet ret = cAtErrorModeNotSupport;
    if (Tha60290061DeviceAuditIsEnabled(mThis(self)))
        {
        AtDevice subDevice = AtDeviceSubDeviceGet(self, (uint8)subDeviceId);
        Tha60290061SubDeviceDebugger debugger = Tha60290061SubDeviceDebuggerGet(subDevice);
        ThaBitMask regBitMask = Tha60290061SubDeviceDebuggerAddressBitMaskGet(debugger);
        ret = All_Modules_DdrOffloadAudit(self, regBitMask, subDeviceId, outputFile, auditMode);
        }

    return ret;
    }

eAtRet Tha60290061DeviceDdrOffloadDbAuditInit(AtDevice self, uint32 subDeviceId)
    {
    eAtRet ret = cAtOk;
    if (Tha60290061DeviceAuditIsEnabled(mThis(self)))
        {
        AtDevice subDevice = AtDeviceSubDeviceGet(self, (uint8)subDeviceId);
        Tha60290061SubDeviceDebugger debugger = Tha60290061SubDeviceDebuggerGet(subDevice);
        Tha60290061SubDeviceDebuggerInit(debugger);
        }

    return ret;
    }

uint32 Tha60290061DeviceDdrOffloadBaseAddress(AtDevice self)
    {
    return DdrOffloadBaseAddress(self);
    }

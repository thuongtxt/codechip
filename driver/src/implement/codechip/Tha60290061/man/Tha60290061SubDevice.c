/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60290061SubDevice.c
 *
 * Created Date: Jun 19, 2018
 *
 * Description : Tha60291011 sub devcice implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../../../platform/hal/AtHalSimInternal.h"
#include "../../../../generic/man/AtIpCoreInternal.h"
#include "../../Tha60290011/pdh/Tha60290011ModulePdh.h"
#include "Tha60290061DeviceInternal.h"
#include "Tha60290061DeviceDdrOffload.h"
#include "Tha60290061SubDevice.h"
#include "Tha60290061IpCore.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290061SubDevice)(self))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods          m_AtObjectOverride;
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;
static tTha60210031DeviceMethods m_Tha60210031DeviceOverride;
static tTha60290061DeviceMethods m_Tha60290061DeviceOverride;

/* Save Super Implementation */
static const tAtObjectMethods           *m_AtObjectMethods  = NULL;
static const tAtDeviceMethods           *m_AtDeviceMethods  = NULL;
static const tThaDeviceMethods          *m_ThaDeviceMethods = NULL;
static const tTha60150011DeviceMethods  *m_Tha60150011DeviceMethods = NULL;
static const tTha60210031DeviceMethods  *m_Tha60210031DeviceMethods = NULL;
static const tTha60290061DeviceMethods  *m_Tha60290061DeviceMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice SubDeviceCreate(Tha60290061Device self, uint8 subDeviceIndex)
    {
    AtUnused(self);
    AtUnused(subDeviceIndex);
    return NULL;
    }

static uint8 NumSubDevices(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static AtDevice SubDeviceGet(AtDevice self, uint8 subDeviceIndex)
    {
    AtUnused(self);
    AtUnused(subDeviceIndex);
    return NULL;
    }

static AtDevice ParentDeviceGet(AtDevice self)
    {
    return mThis(self)->parent;
    }

static eBool ShouldCacheHalOnChannel(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290061SubDevice object = (Tha60290061SubDevice)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(deviceId);
    mEncodeObjectDescription(parent);
    mEncodeNone(debugger);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->debugger);
    m_AtObjectMethods->Delete(self);
    }

static AtDevice ParentGet(AtDevice self)
    {
    return AtDeviceParentDeviceGet(self);
    }

static uint8 IdGet(AtDevice self)
    {
    return mThis(self)->deviceId;
    }

static uint16 ReadOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    AtUnused(subDeviceId);
    return m_AtDeviceMethods->ReadOnDdrOffload(ParentGet(self), IdGet(self), ddrAddress, dataBuffer, bufferLen, coreId);
    }

static uint16 WriteOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 regAddress, const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    AtUnused(subDeviceId);
    return m_AtDeviceMethods->WriteOnDdrOffload(ParentGet(self), IdGet(self), ddrAddress, regAddress, dataBuffer, bufferLen, coreId);
    }

static eAtDeviceRole RoleGet(AtDevice self)
    {
    return Tha60290061DeviceSubRoleGet(ParentGet(self), IdGet(self));
    }

static eAtRet RoleSet(AtDevice self, eAtDeviceRole role)
    {
    Tha60290061SubDeviceAsyncRoleGet(self);
    return Tha60290061DeviceSubRoleSet(ParentGet(self), IdGet(self), role);
    }

static uint32 VersionNumber(AtDevice self)
    {
    return AtDeviceVersionNumber(ParentGet(self));
    }

static uint32 BuiltNumber(AtDevice self)
    {
    return AtDeviceBuiltNumber(ParentGet(self));
    }

static uint32 VersionNumberFromHwGet(AtDevice self)
    {
    return AtDeviceVersionNumberFromHwGet(ParentGet(self));
    }

static uint32 BuiltNumberFromHwGet(AtDevice self)
    {
    return AtDeviceBuiltNumberFromHwGet(ParentGet(self));
    }

static char *VersionRead(AtDevice self, char *buffer, uint32 bufferLen)
    {
    return m_AtDeviceMethods->VersionRead(ParentGet(self), buffer, bufferLen);
    }

static uint32 ProductCodeRead(AtDevice self)
    {
    return AtDeviceProductCodeGet(ParentGet(self));
    }

static void DidWriteRegister(AtDevice self, uint32 address, uint32 value, uint8 coreId, void *userData)
    {
    AtUnused(coreId);
    AtUnused(userData);

    if (AtDeviceAccessible(self))
        Tha60290061DeviceDdrOffloadWrite(ParentGet(self), IdGet(self), address, value);
    }

static void DidLongWriteOnCore(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 numDwords, uint8 coreId, void *userData)
    {
    AtUnused(coreId);
    AtUnused(userData);

    if (AtDeviceAccessible(self))
        Tha60290061DeviceDdrOffloadLongWrite(ParentGet(self), IdGet(self), address, dataBuffer, numDwords);
    }

static void ChangeHalNotify(AtDevice self, AtHal realHal)
    {
    AtSerdesManager manager = AtDeviceSerdesManagerGet(self);
    AtIpCore ipCore = AtDeviceIpCoreGet(self, 0);
    AtHal hal = (realHal) ? realHal : IpCoreHalGet(ipCore);
    eBool simulated = realHal ? cAtFalse : cAtTrue;
    uint8 serdesId;

    Tha60290061IpCoreHalSet(ipCore, realHal);
    AtDeviceSimulate(self, simulated);

    for (serdesId = 0; serdesId < AtSerdesManagerNumSerdesControllers(manager); serdesId++)
        {
        AtSerdesController serdes = AtSerdesManagerSerdesControllerGet(manager, serdesId);
        AtMdio mdio = AtSerdesControllerMdio(serdes);
        if (mdio)
            {
            AtMdioHalSet(mdio, hal);
            AtMdioSimulateEnable(mdio, simulated);
            }
        }
    }

static void DidChangeRole(AtDevice self, uint32 role, void *userData)
    {
    AtDevice parent = AtDeviceParentDeviceGet(self);
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(parent, cAtModulePdh);
    AtModulePrbs modulePrbs = (AtModulePrbs)AtDeviceModuleGet(self, cAtModulePrbs);
    AtHal realHal = NULL;

    AtUnused(userData);

    /* Enable/disable TX PDHMUX on master device if any sub-device gets change
     * of its role. */
    Tha60290011ModulePdhMuxTxEthEnable(modulePdh, (role == cAtDeviceRoleActive) ? cAtTrue : cAtFalse);

    /* Parent device is being simulation, so there is no real device at all. */
    if (AtDeviceIsSimulated(parent))
        return;

    /* THEORY: When a sub-device completely switches to active role, it will
     * switch to use real HAL (owned by its parent device). Subsequently, the
     * real HAL shall be registered for "write notification handler". This handler
     * takes responsibility for same write operation making on the simulation
     * HAL of that sub-device.
     *
     * Otherwise, this handler MUST be unregistered.
     */

    if (role == cAtDeviceRoleActive)
        realHal = AtDeviceIpCoreHalGet(parent, 0);

    ChangeHalNotify(self, realHal);

    /* Clear all PRBS status once offload process is completed. */
    if (role == cAtDeviceRoleActive)
        AtModuleStatusClear((AtModule)modulePrbs);
    }

static tAtDeviceListener *AccessListener(void)
    {
    static tAtDeviceListener listener;
    static tAtDeviceListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.DidWriteRegister   = DidWriteRegister;
    listener.DidLongWriteOnCore = DidLongWriteOnCore;
    listener.DidChangeRole      = DidChangeRole;
    pListener = &listener;

    return pListener;
    }

static eAtRet ListenerInstall(Tha60210031Device self)
    {
    AtHal simHal = IpCoreHalGet(AtDeviceIpCoreGet((AtDevice)self, 0));
    return AtDeviceListenerAdd((AtDevice)self, AccessListener(), simHal);
    }

static eAtRet Init(AtDevice self)
    {
    Tha60290061SubDeviceAsyncRoleGet(self);
    return m_AtDeviceMethods->Init(self);
    }

static uint32 MakeSimulationCode(AtDevice self, uint32 productCode)
    {
    return (productCode & cBit27_0) | (mMethodsGet(self)->SimulationCode(self) << 28);
    }

static void ProductCodeSet(AtDevice self, uint32 productCode)
    {
    self->productCode = productCode;
    }

static eAtRet Setup(AtDevice self)
    {
    eAtRet ret;
    uint32 productCode = AtDeviceProductCodeGet(self);

    /* Make indication to simulation context. */
    ProductCodeSet(self, MakeSimulationCode(self, productCode));
    ret = m_AtDeviceMethods->Setup(self);
    ProductCodeSet(self, productCode);

    return ret;
    }

static void InterruptProcess(AtDevice self)
    {
    Tha60290061SubDeviceAsyncRoleGet(self);

    /* Standby device shall be fallen to simulation */
    if (AtDeviceIsSimulated(self))
        return;

    m_AtDeviceMethods->InterruptProcess(self);
    }

static Tha60290061DdrLookupTable DdrLookupTableCreate(Tha60290061Device self)
    {
    AtUnused(self);
    return NULL;
    }

static ThaVersionReader DeviceVersionReader(ThaDevice self)
    {
    return m_ThaDeviceMethods->DeviceVersionReader((ThaDevice)(ParentGet((AtDevice)self)));
    }

static void HwFlush(Tha60150011Device self)
    {
    AtDevice device = (AtDevice)self;

    /* If sub-device flushing is being in the context of master device
     * initialization, we do not need this action anymore. The master device
     * took initialization of all DDR spaces. */
    if (!Tha60290061DeviceIsInitializing(ParentGet(device)))
        {
        /* We must flush DDR space priorly. Because this action shall clear
         * valid flag of all DDR cell. */
        Tha60290061DeviceSubDeviceHwFlush(ParentGet(device), IdGet(device));
        }

    /* Flush simulation HAL as well as fill DDR offload space to set valid flag
     * for each DDR cell. */
    m_Tha60150011DeviceMethods->HwFlush(self);
    }

static uint32 DeviceId(AtDevice self)
    {
    return (uint32)(mThis(self)->deviceId + 1);
    }

static const char* DeviceIdToString(AtDevice self)
    {
    static char string[16];
    if (AtDriverMultiDevPrefixApiLogIsEnabled())
        {
        AtDevice master = AtDeviceParentDeviceGet(self);
        uint32 masterid = AtDriverDeviceIndexGet(AtDriverSharedDriverGet(), master) + (uint32)1;
        uint32 devid = AtDeviceIdGet(self);
        AtSprintf(string, "sdev_%u.%u_", masterid, devid);
        }
    else
        AtSprintf(string, "%s", "");

    return string;
    }

static void OverrideTha60290061Device(AtDevice self)
    {
    Tha60290061Device object = (Tha60290061Device)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290061DeviceMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290061DeviceOverride, m_Tha60290061DeviceMethods, sizeof(m_Tha60290061DeviceOverride));

        mMethodOverride(m_Tha60290061DeviceOverride, SubDeviceCreate);
        mMethodOverride(m_Tha60290061DeviceOverride, DdrLookupTableCreate);
        }

    mMethodsSet(object, &m_Tha60290061DeviceOverride);
    }

static void OverrideTha60210031Device(AtDevice self)
    {
    Tha60210031Device device = (Tha60210031Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210031DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210031DeviceOverride));

        mMethodOverride(m_Tha60210031DeviceOverride, ListenerInstall);
        }

    mMethodsSet(device, &m_Tha60210031DeviceOverride);
    }

static void OverrideTha60150011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60150011DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, HwFlush);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice object = (ThaDevice)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDeviceMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, m_ThaDeviceMethods, sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, DeviceVersionReader);
        }

    mMethodsSet(object, &m_ThaDeviceOverride);
    }

static void OverrideAtObject(AtDevice self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, ParentDeviceGet);
        mMethodOverride(m_AtDeviceOverride, NumSubDevices);
        mMethodOverride(m_AtDeviceOverride, SubDeviceGet);
        mMethodOverride(m_AtDeviceOverride, RoleGet);
        mMethodOverride(m_AtDeviceOverride, ReadOnDdrOffload);
        mMethodOverride(m_AtDeviceOverride, WriteOnDdrOffload);
        mMethodOverride(m_AtDeviceOverride, RoleSet);
        mMethodOverride(m_AtDeviceOverride, VersionNumber);
        mMethodOverride(m_AtDeviceOverride, BuiltNumber);
        mMethodOverride(m_AtDeviceOverride, VersionNumberFromHwGet);
        mMethodOverride(m_AtDeviceOverride, BuiltNumberFromHwGet);
        mMethodOverride(m_AtDeviceOverride, VersionRead);
        mMethodOverride(m_AtDeviceOverride, ProductCodeRead);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, Setup);
        mMethodOverride(m_AtDeviceOverride, ShouldCacheHalOnChannel);
        mMethodOverride(m_AtDeviceOverride, InterruptProcess);
        mMethodOverride(m_AtDeviceOverride, DeviceId);
        mMethodOverride(m_AtDeviceOverride, DeviceIdToString);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtObject(self);
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60150011Device(self);
    OverrideTha60210031Device(self);
    OverrideTha60290061Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290061SubDevice);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode, uint8 deviceId, AtDevice parent)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290061DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    mThis(self)->deviceId = deviceId;
    mThis(self)->parent = parent;

    return self;
    }

AtDevice Tha60290061SubDeviceNew(AtDevice parent, uint8 deviceId)
    {
    /* Allocate memory */
    AtDriver driver = AtDeviceDriverGet(parent);
    uint32 productCode = AtDeviceProductCodeGet(parent);
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode, deviceId, parent);
    }

Tha60290061SubDeviceDebugger Tha60290061SubDeviceDebuggerGet(AtDevice self)
    {
    if (mThis(self)->debugger == NULL)
        mThis(self)->debugger = Tha60290061SubDeviceDebuggerNew(self);
    return mThis(self)->debugger;
    }

uint8 Tha60290061SubDeviceIdGet(AtDevice self)
    {
    if (self)
        return IdGet(self);
    return cInvalidUint8;
    }

/* To complete asynchronous change of device role */
eAtDeviceRole Tha60290061SubDeviceAsyncRoleGet(AtDevice self)
    {
    if (self)
        return RoleGet(self);
    return cAtDeviceRoleUnknown;
    }

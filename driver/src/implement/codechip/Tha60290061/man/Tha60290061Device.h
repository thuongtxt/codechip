/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60290061Device.h
 * 
 * Created Date: Jun 14, 2018
 *
 * Description : Interface of the 60290061 device functions
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061DEVICE_H_
#define _THA60290061DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "AtHal.h"
#include "AtIpCore.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290061Device *Tha60290061Device;

typedef enum eTha60290061AuditMode
    {
    cTha60290061AuditCompare = 0,
    cTha60290061AuditNoCompare = 1
    }eTha60290061AuditMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60290061SubDeviceNew(AtDevice parent, uint8 deviceId);

eBool Tha60290061DeviceIsInitializing(AtDevice self);

/* Sub-device utilities */
void Tha60290061DeviceSubDeviceHwFlush(AtDevice self, uint32 subDeviceId);
eAtDeviceRole Tha60290061DeviceSubRoleGet(AtDevice self, uint32 subDeviceId);
eAtRet Tha60290061DeviceSubRoleSet(AtDevice self, uint32 subDeviceId, eAtDeviceRole role);

/* Audit the cfg db in fpga, ddr db, and internal sdk of the subdevice after ddroffload active */
eBool Tha60290061DeviceDdrOffloadCfgIsMatched(AtDevice self, uint32 subDeviceId);
eAtRet Tha60290061DeviceDdrOffloadCfgToFile(AtDevice self, uint32 subDeviceId, AtFile afile);
void Tha60290061DeviceAuditEnable(Tha60290061Device self, eBool enable);
eBool Tha60290061DeviceAuditIsEnabled(Tha60290061Device self);
eAtRet Tha60290061DeviceDdrOffloadDbAudit(AtDevice self, uint32 subDeviceId, AtFile outputFile, eTha60290061AuditMode auditMode);
eAtRet Tha60290061DeviceDdrOffloadDbAuditInit(AtDevice self, uint32 subDeviceId);
uint32 Tha60290061DeviceDdrOffloadBaseAddress(AtDevice self);

/* Backward compatible. */
eBool Tha60290061DeviceClsVlanIsSupported(Tha60290061Device self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061DEVICE_H_ */


/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0061_RD_DDR_OFFLOAD_H_
#define _AF6_REG_AF6CNC0061_RD_DDR_OFFLOAD_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : CONFIG START GET CONFIGURATION FROM DATABASE
Reg Addr   : 0x01
Reg Formula: 
    Where  : 
Reg Desc   : 
config init to get database configuration to sub module, format data 128bit of Database = {reserve,valid,address_submodule[23:0],data_cfg[95:0]},
need clear sticky SUB MODULE LOAD DONE before set init

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_init_Base                                                                        0x01
#define cAf6Reg_upen_cfg_init_WidthVal                                                                      32

/*--------------------------------------
BitField Name: req_init
BitField Type: R/W
BitField Desc: CPU set value (1) to start init, when done, engine will clear
(0), need clear sticky SUB MODULE LOAD DONE before set init
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_init_req_init_Mask                                                                 cBit0
#define cAf6_upen_cfg_init_req_init_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG SELECT DATABASE TO GET CONFIGURATION
Reg Addr   : 0x02
Reg Formula: 
    Where  : 
Reg Desc   : 
configure value from 0 --> 4 for corresponding 5 Database on DDR for offloading

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_db_Base                                                                          0x02
#define cAf6Reg_upen_cfg_db_WidthVal                                                                        32

/*--------------------------------------
BitField Name: out_cfg_db
BitField Type: R/W
BitField Desc: If load databse, configure value from 0 --> 4 for loading
corresponding one of 5 Database on DDR, value 7 is IDLE
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_cfg_db_out_cfg_db_Mask                                                               cBit2_0
#define cAf6_upen_cfg_db_out_cfg_db_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL READ/WRITE DDR
Reg Addr   : 0x03
Reg Formula: 
    Where  : 
Reg Desc   : 
config control READ/WRITE DATABASE TO DDR  FROM CPU

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ctl_Base                                                                         0x03
#define cAf6Reg_upen_cfg_ctl_WidthVal                                                                       32

/*--------------------------------------
BitField Name: enb_req
BitField Type: R/W
BitField Desc: start request read/write, set value (1) is start read or write,
engine will clear when request is accept
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_upen_cfg_ctl_enb_req_Mask                                                                  cBit25
#define cAf6_upen_cfg_ctl_enb_req_Shift                                                                     25

/*--------------------------------------
BitField Name: cfg_rnw
BitField Type: R/W
BitField Desc: (1) is read DDR, (0) is write DDR
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_upen_cfg_ctl_cfg_rnw_Mask                                                                  cBit24
#define cAf6_upen_cfg_ctl_cfg_rnw_Shift                                                                     24

/*--------------------------------------
BitField Name: cfg_db
BitField Type: R/W
BitField Desc: database on DDR for read or write
BitField Bits: [20:18]
--------------------------------------*/
#define cAf6_upen_cfg_ctl_cfg_db_Mask                                                                cBit20_18
#define cAf6_upen_cfg_ctl_cfg_db_Shift                                                                      18

/*--------------------------------------
BitField Name: cfg_ddradd
BitField Type: R/W
BitField Desc: DDR address for read or write
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_cfg_ctl_cfg_ddradd_Mask                                                             cBit17_0
#define cAf6_upen_cfg_ctl_cfg_ddradd_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG DATA WRITE TO DDR
Reg Addr   : 0x04
Reg Formula: 
    Where  : 
Reg Desc   : 
config DATA to WRITE TO DDR FROM CPU

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_wrdat_Base                                                                       0x04

/*--------------------------------------
BitField Name: cfg_ctl_wrd
BitField Type: R/W
BitField Desc: 128 bit data to write DDR, format data 128bit of Database =
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_cfg_wrdat_cfg_ctl_wrd_01_Mask                                                       cBit31_0
#define cAf6_upen_cfg_wrdat_cfg_ctl_wrd_01_Shift                                                             0
#define cAf6_upen_cfg_wrdat_cfg_ctl_wrd_02_Mask                                                       cBit31_0
#define cAf6_upen_cfg_wrdat_cfg_ctl_wrd_02_Shift                                                             0
#define cAf6_upen_cfg_wrdat_cfg_ctl_wrd_03_Mask                                                       cBit31_0
#define cAf6_upen_cfg_wrdat_cfg_ctl_wrd_03_Shift                                                             0
#define cAf6_upen_cfg_wrdat_cfg_ctl_wrd_04_Mask                                                       cBit31_0
#define cAf6_upen_cfg_wrdat_cfg_ctl_wrd_04_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : DATA REAN FROM DDR
Reg Addr   : 0x05
Reg Formula: 
    Where  : 
Reg Desc   : 
DATA is READ FROM DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_rddat_Base                                                                       0x05

/*--------------------------------------
BitField Name: cfg_ctl_rdd
BitField Type: R_O
BitField Desc: 128 bit data is read from DDR
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_cfg_rddat_cfg_ctl_rdd_01_Mask                                                       cBit31_0
#define cAf6_upen_cfg_rddat_cfg_ctl_rdd_01_Shift                                                             0
#define cAf6_upen_cfg_rddat_cfg_ctl_rdd_02_Mask                                                       cBit31_0
#define cAf6_upen_cfg_rddat_cfg_ctl_rdd_02_Shift                                                             0
#define cAf6_upen_cfg_rddat_cfg_ctl_rdd_03_Mask                                                       cBit31_0
#define cAf6_upen_cfg_rddat_cfg_ctl_rdd_03_Shift                                                             0
#define cAf6_upen_cfg_rddat_cfg_ctl_rdd_04_Mask                                                       cBit31_0
#define cAf6_upen_cfg_rddat_cfg_ctl_rdd_04_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STATUS READ/WRITE DDR
Reg Addr   : 0x06
Reg Formula: 
    Where  : 
Reg Desc   : 
Status read/write DDR for CPU

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ctl_done_Base                                                                    0x06
#define cAf6Reg_upen_cfg_ctl_done_WidthVal                                                                  32

/*--------------------------------------
BitField Name: wr_done
BitField Type: R_O
BitField Desc: write DDR done,(1) is done,data is wrtie done, (0) is not done
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_cfg_ctl_done_wr_done_Mask                                                              cBit3
#define cAf6_upen_cfg_ctl_done_wr_done_Shift                                                                 3

/*--------------------------------------
BitField Name: wr_rdy
BitField Type: R_O
BitField Desc: ready to write DDR,(1) is ready, (0) is busy
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_cfg_ctl_done_wr_rdy_Mask                                                               cBit2
#define cAf6_upen_cfg_ctl_done_wr_rdy_Shift                                                                  2

/*--------------------------------------
BitField Name: rd_done
BitField Type: R_O
BitField Desc: read DDR done, (1) is done,data is ready for read, (0) is not
done, still wait done for read data
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cfg_ctl_done_rd_done_Mask                                                              cBit1
#define cAf6_upen_cfg_ctl_done_rd_done_Shift                                                                 1

/*--------------------------------------
BitField Name: rd_rdy
BitField Type: R_O
BitField Desc: ready to read DDR, (1) is ready, (0) is busy
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_ctl_done_rd_rdy_Mask                                                               cBit0
#define cAf6_upen_cfg_ctl_done_rd_rdy_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : STICKY SUB MODULE LOAD DONE
Reg Addr   : 0x07
Reg Formula: 
    Where  : 
Reg Desc   : 
STICKY SUB MODULE LOAD DONE

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stk_sub_done_Base                                                                    0x07

/*--------------------------------------
BitField Name: ocn_reqdone
BitField Type: RW
BitField Desc: OCN request configuration done
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_ocn_reqdone_Mask                                                         cBit31
#define cAf6_upen_stk_sub_done_ocn_reqdone_Shift                                                            31

/*--------------------------------------
BitField Name: poh_reqdone
BitField Type: RW
BitField Desc: POH request configuration done
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_poh_reqdone_Mask                                                         cBit30
#define cAf6_upen_stk_sub_done_poh_reqdone_Shift                                                            30

/*--------------------------------------
BitField Name: pdh_reqdone
BitField Type: RW
BitField Desc: PDH request configuration done
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_pdh_reqdone_Mask                                                         cBit29
#define cAf6_upen_stk_sub_done_pdh_reqdone_Shift                                                            29

/*--------------------------------------
BitField Name: map_reqdone
BitField Type: RW
BitField Desc: MAP request configuration done
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_map_reqdone_Mask                                                         cBit28
#define cAf6_upen_stk_sub_done_map_reqdone_Shift                                                            28

/*--------------------------------------
BitField Name: cdr_reqdone
BitField Type: RW
BitField Desc: CDR request configuration done
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_cdr_reqdone_Mask                                                         cBit27
#define cAf6_upen_stk_sub_done_cdr_reqdone_Shift                                                            27

/*--------------------------------------
BitField Name: pla_reqdone
BitField Type: RW
BitField Desc: PLA request configuration done
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_pla_reqdone_Mask                                                         cBit26
#define cAf6_upen_stk_sub_done_pla_reqdone_Shift                                                            26

/*--------------------------------------
BitField Name: pwe_reqdone
BitField Type: RW
BitField Desc: PWE request configuration done
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_pwe_reqdone_Mask                                                         cBit25
#define cAf6_upen_stk_sub_done_pwe_reqdone_Shift                                                            25

/*--------------------------------------
BitField Name: cla_reqdone
BitField Type: RW
BitField Desc: CLA request configuration done
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_cla_reqdone_Mask                                                         cBit24
#define cAf6_upen_stk_sub_done_cla_reqdone_Shift                                                            24

/*--------------------------------------
BitField Name: pda_reqdone
BitField Type: RW
BitField Desc: PDA request configuration done
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_pda_reqdone_Mask                                                         cBit23
#define cAf6_upen_stk_sub_done_pda_reqdone_Shift                                                            23

/*--------------------------------------
BitField Name: bertgen_reqdone
BitField Type: RW
BitField Desc: BERTGEN request configuration done
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_bertgen_reqdone_Mask                                                     cBit22
#define cAf6_upen_stk_sub_done_bertgen_reqdone_Shift                                                        22

/*--------------------------------------
BitField Name: bertmon_reqdone
BitField Type: RW
BitField Desc: BERTMON request configuration done
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_bertmon_reqdone_Mask                                                     cBit21
#define cAf6_upen_stk_sub_done_bertmon_reqdone_Shift                                                        21

/*--------------------------------------
BitField Name: bertmonpw_reqdone
BitField Type: RW
BitField Desc: BERTMONPW request configuration done
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_bertmonpw_reqdone_Mask                                                   cBit20
#define cAf6_upen_stk_sub_done_bertmonpw_reqdone_Shift                                                      20

/*--------------------------------------
BitField Name: prm_reqdone
BitField Type: RW
BitField Desc: PRM request configuration done
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_prm_reqdone_Mask                                                         cBit19
#define cAf6_upen_stk_sub_done_prm_reqdone_Shift                                                            19

/*--------------------------------------
BitField Name: pdhmux_reqdone
BitField Type: RW
BitField Desc: PDHMUX request configuration done
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_pdhmux_reqdone_Mask                                                      cBit18
#define cAf6_upen_stk_sub_done_pdhmux_reqdone_Shift                                                         18

/*--------------------------------------
BitField Name: pmfm_reqdone
BitField Type: RW
BitField Desc: PMFM request configuration done
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_pmfm_reqdone_Mask                                                        cBit17
#define cAf6_upen_stk_sub_done_pmfm_reqdone_Shift                                                           17

/*--------------------------------------
BitField Name: dcc_reqdone
BitField Type: RW
BitField Desc: DCC request configuration done
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_dcc_reqdone_Mask                                                         cBit16
#define cAf6_upen_stk_sub_done_dcc_reqdone_Shift                                                            16

/*--------------------------------------
BitField Name: ocn_loaddone
BitField Type: RW
BitField Desc: OCN load configuration done
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_ocn_loaddone_Mask                                                        cBit15
#define cAf6_upen_stk_sub_done_ocn_loaddone_Shift                                                           15

/*--------------------------------------
BitField Name: poh_loaddone
BitField Type: RW
BitField Desc: POH load configuration done
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_poh_loaddone_Mask                                                        cBit14
#define cAf6_upen_stk_sub_done_poh_loaddone_Shift                                                           14

/*--------------------------------------
BitField Name: pdh_loaddone
BitField Type: RW
BitField Desc: PDH load configuration done
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_pdh_loaddone_Mask                                                        cBit13
#define cAf6_upen_stk_sub_done_pdh_loaddone_Shift                                                           13

/*--------------------------------------
BitField Name: map_loaddone
BitField Type: RW
BitField Desc: MAP load configuration done
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_map_loaddone_Mask                                                        cBit12
#define cAf6_upen_stk_sub_done_map_loaddone_Shift                                                           12

/*--------------------------------------
BitField Name: cdr_loaddone
BitField Type: RW
BitField Desc: CDR load configuration done
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_cdr_loaddone_Mask                                                        cBit11
#define cAf6_upen_stk_sub_done_cdr_loaddone_Shift                                                           11

/*--------------------------------------
BitField Name: pla_loaddone
BitField Type: RW
BitField Desc: PLA load configuration done
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_pla_loaddone_Mask                                                        cBit10
#define cAf6_upen_stk_sub_done_pla_loaddone_Shift                                                           10

/*--------------------------------------
BitField Name: pwe_loaddone
BitField Type: RW
BitField Desc: PWE load configuration done
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_pwe_loaddone_Mask                                                         cBit9
#define cAf6_upen_stk_sub_done_pwe_loaddone_Shift                                                            9

/*--------------------------------------
BitField Name: cla_loaddone
BitField Type: RW
BitField Desc: CLA load configuration done
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_cla_loaddone_Mask                                                         cBit8
#define cAf6_upen_stk_sub_done_cla_loaddone_Shift                                                            8

/*--------------------------------------
BitField Name: pda_loaddone
BitField Type: RW
BitField Desc: PDA load configuration done
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_pda_loaddone_Mask                                                         cBit7
#define cAf6_upen_stk_sub_done_pda_loaddone_Shift                                                            7

/*--------------------------------------
BitField Name: bertgen_loaddone
BitField Type: RW
BitField Desc: BERTGEN load configuration done
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_bertgen_loaddone_Mask                                                     cBit6
#define cAf6_upen_stk_sub_done_bertgen_loaddone_Shift                                                        6

/*--------------------------------------
BitField Name: bertmon_loaddone
BitField Type: RW
BitField Desc: BERTMON load configuration done
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_bertmon_loaddone_Mask                                                     cBit5
#define cAf6_upen_stk_sub_done_bertmon_loaddone_Shift                                                        5

/*--------------------------------------
BitField Name: bertmonpw_loaddone
BitField Type: RW
BitField Desc: BERTMONPW load configuration done
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_bertmonpw_loaddone_Mask                                                   cBit4
#define cAf6_upen_stk_sub_done_bertmonpw_loaddone_Shift                                                      4

/*--------------------------------------
BitField Name: prm_loaddone
BitField Type: RW
BitField Desc: PRM load configuration done
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_prm_loaddone_Mask                                                         cBit3
#define cAf6_upen_stk_sub_done_prm_loaddone_Shift                                                            3

/*--------------------------------------
BitField Name: pdhmux_loaddone
BitField Type: RW
BitField Desc: PDHMUX load configuration done
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_pdhmux_loaddone_Mask                                                      cBit2
#define cAf6_upen_stk_sub_done_pdhmux_loaddone_Shift                                                         2

/*--------------------------------------
BitField Name: pmfm_loaddone
BitField Type: RW
BitField Desc: PMFM load configuration done
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_pmfm_loaddone_Mask                                                        cBit1
#define cAf6_upen_stk_sub_done_pmfm_loaddone_Shift                                                           1

/*--------------------------------------
BitField Name: dcc_loaddone
BitField Type: RW
BitField Desc: DCC load configuration done
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_stk_sub_done_dcc_loaddone_Mask                                                         cBit0
#define cAf6_upen_stk_sub_done_dcc_loaddone_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : STICKY SUB MODULE ADDRESS WRONG
Reg Addr   : 0x08
Reg Formula: 
    Where  : 
Reg Desc   : 
STICKY SUB MODULE ADDRESS WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stk_add_wrong_Base                                                                   0x08

/*--------------------------------------
BitField Name: stk_init_done
BitField Type: RW
BitField Desc: sticky init done
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_stk_init_done_Mask                                                      cBit20
#define cAf6_upen_stk_add_wrong_stk_init_done_Shift                                                         20

/*--------------------------------------
BitField Name: stk_nreq_init
BitField Type: RW
BitField Desc: sticky not request init
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_stk_nreq_init_Mask                                                      cBit19
#define cAf6_upen_stk_add_wrong_stk_nreq_init_Shift                                                         19

/*--------------------------------------
BitField Name: stk_req_init
BitField Type: RW
BitField Desc: sticky request init
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_stk_req_init_Mask                                                       cBit18
#define cAf6_upen_stk_add_wrong_stk_req_init_Shift                                                          18

/*--------------------------------------
BitField Name: fifo_addreq_full
BitField Type: RW
BitField Desc: FIFO store add request full
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_fifo_addreq_full_Mask                                                   cBit17
#define cAf6_upen_stk_add_wrong_fifo_addreq_full_Shift                                                      17

/*--------------------------------------
BitField Name: fifo_addreq_nempt
BitField Type: RW
BitField Desc: FIFO store add request not empty
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_fifo_addreq_nempt_Mask                                                  cBit16
#define cAf6_upen_stk_add_wrong_fifo_addreq_nempt_Shift                                                     16

/*--------------------------------------
BitField Name: ocn_add_wrong
BitField Type: RW
BitField Desc: OCN sticky address wrong
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_ocn_add_wrong_Mask                                                      cBit15
#define cAf6_upen_stk_add_wrong_ocn_add_wrong_Shift                                                         15

/*--------------------------------------
BitField Name: poh_add_wrong
BitField Type: RW
BitField Desc: POH sticky address wrong
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_poh_add_wrong_Mask                                                      cBit14
#define cAf6_upen_stk_add_wrong_poh_add_wrong_Shift                                                         14

/*--------------------------------------
BitField Name: pdh_add_wrong
BitField Type: RW
BitField Desc: PDH sticky address wrong
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_pdh_add_wrong_Mask                                                      cBit13
#define cAf6_upen_stk_add_wrong_pdh_add_wrong_Shift                                                         13

/*--------------------------------------
BitField Name: map_add_wrong
BitField Type: RW
BitField Desc: MAP sticky address wrong
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_map_add_wrong_Mask                                                      cBit12
#define cAf6_upen_stk_add_wrong_map_add_wrong_Shift                                                         12

/*--------------------------------------
BitField Name: cdr_add_wrong
BitField Type: RW
BitField Desc: CDR sticky address wrong
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_cdr_add_wrong_Mask                                                      cBit11
#define cAf6_upen_stk_add_wrong_cdr_add_wrong_Shift                                                         11

/*--------------------------------------
BitField Name: pla_add_wrong
BitField Type: RW
BitField Desc: PLA sticky address wrong
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_pla_add_wrong_Mask                                                      cBit10
#define cAf6_upen_stk_add_wrong_pla_add_wrong_Shift                                                         10

/*--------------------------------------
BitField Name: pwe_add_wrong
BitField Type: RW
BitField Desc: PWE sticky address wrong
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_pwe_add_wrong_Mask                                                       cBit9
#define cAf6_upen_stk_add_wrong_pwe_add_wrong_Shift                                                          9

/*--------------------------------------
BitField Name: cla_add_wrong
BitField Type: RW
BitField Desc: CLA sticky address wrong
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_cla_add_wrong_Mask                                                       cBit8
#define cAf6_upen_stk_add_wrong_cla_add_wrong_Shift                                                          8

/*--------------------------------------
BitField Name: pda_add_wrong
BitField Type: RW
BitField Desc: PDA sticky address wrong
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_pda_add_wrong_Mask                                                       cBit7
#define cAf6_upen_stk_add_wrong_pda_add_wrong_Shift                                                          7

/*--------------------------------------
BitField Name: bertgen_add_wrong
BitField Type: RW
BitField Desc: BERTGEN sticky address wrong
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_bertgen_add_wrong_Mask                                                   cBit6
#define cAf6_upen_stk_add_wrong_bertgen_add_wrong_Shift                                                      6

/*--------------------------------------
BitField Name: bertmon_add_wrong
BitField Type: RW
BitField Desc: BERTMON sticky address wrong
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_bertmon_add_wrong_Mask                                                   cBit5
#define cAf6_upen_stk_add_wrong_bertmon_add_wrong_Shift                                                      5

/*--------------------------------------
BitField Name: bertmonpw_add_wrong
BitField Type: RW
BitField Desc: BERTMONPW sticky address wrong
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_bertmonpw_add_wrong_Mask                                                 cBit4
#define cAf6_upen_stk_add_wrong_bertmonpw_add_wrong_Shift                                                    4

/*--------------------------------------
BitField Name: prm_add_wrong
BitField Type: RW
BitField Desc: PRM sticky address wrong
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_prm_add_wrong_Mask                                                       cBit3
#define cAf6_upen_stk_add_wrong_prm_add_wrong_Shift                                                          3

/*--------------------------------------
BitField Name: pdhmux_add_wrong
BitField Type: RW
BitField Desc: PDHMUX sticky address wrong
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_pdhmux_add_wrong_Mask                                                    cBit2
#define cAf6_upen_stk_add_wrong_pdhmux_add_wrong_Shift                                                       2

/*--------------------------------------
BitField Name: pmfm_add_wrong
BitField Type: RW
BitField Desc: PMFM sticky address wrong
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_pmfm_add_wrong_Mask                                                      cBit1
#define cAf6_upen_stk_add_wrong_pmfm_add_wrong_Shift                                                         1

/*--------------------------------------
BitField Name: dcc_add_wrong
BitField Type: RW
BitField Desc: DCC sticky address wrong
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_stk_add_wrong_dcc_add_wrong_Mask                                                       cBit0
#define cAf6_upen_stk_add_wrong_dcc_add_wrong_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG START FLUSH DATABASE DDR
Reg Addr   : 0x09
Reg Formula: 
    Where  : 
Reg Desc   : 
config FLUSH database DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_flush_Base                                                                       0x09
#define cAf6Reg_upen_cfg_flush_WidthVal                                                                     32

/*--------------------------------------
BitField Name: req_flush
BitField Type: R/W
BitField Desc: CPU set value (1) to start flush, when done, engine will clear
(0)
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_flush_req_flush_Mask                                                               cBit0
#define cAf6_upen_cfg_flush_req_flush_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : STATUS TIME LOAD DATABASE DDR DONE
Reg Addr   : 0x0A - 0x0B
Reg Formula: 0x0A+ $UPR2C
    Where  : 
           + $UPR2C (0-1) : upen read to clear
Reg Desc   : 
value is the interval between the time which start init load database until all submodule load done.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cnt_time_Base                                                                        0x0A

/*--------------------------------------
BitField Name: cnt_time_load
BitField Type: R/W
BitField Desc: value timer which finish loading database to all sub module
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cnt_time_cnt_time_load_Mask                                                         cBit31_0
#define cAf6_upen_cnt_time_cnt_time_load_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : STICKY SUB MODULE DDRR2CPU INTERFACE
Reg Addr   : 0x0C
Reg Formula: 
    Where  : 
Reg Desc   : 
STICKY SUB MODULE DDRR2CPU INTERFACE

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stk_ffempty_Base                                                                     0x0C

/*--------------------------------------
BitField Name: ocn_add_nack
BitField Type: RW
BitField Desc: OCN address configuration which is not returned ACK
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_ocn_add_nack_Mask                                                         cBit31
#define cAf6_upen_stk_ffempty_ocn_add_nack_Shift                                                            31

/*--------------------------------------
BitField Name: poh_add_nack
BitField Type: RW
BitField Desc: POH address configuration which is not returned ACK
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_poh_add_nack_Mask                                                         cBit30
#define cAf6_upen_stk_ffempty_poh_add_nack_Shift                                                            30

/*--------------------------------------
BitField Name: pdh_add_nack
BitField Type: RW
BitField Desc: PDH address configuration which is not returned ACK
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_pdh_add_nack_Mask                                                         cBit29
#define cAf6_upen_stk_ffempty_pdh_add_nack_Shift                                                            29

/*--------------------------------------
BitField Name: map_add_nack
BitField Type: RW
BitField Desc: MAP address configuration which is not returned ACK
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_map_add_nack_Mask                                                         cBit28
#define cAf6_upen_stk_ffempty_map_add_nack_Shift                                                            28

/*--------------------------------------
BitField Name: cdr_add_nack
BitField Type: RW
BitField Desc: CDR address configuration which is not returned ACK
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_cdr_add_nack_Mask                                                         cBit27
#define cAf6_upen_stk_ffempty_cdr_add_nack_Shift                                                            27

/*--------------------------------------
BitField Name: pla_add_nack
BitField Type: RW
BitField Desc: PLA address configuration which is not returned ACK
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_pla_add_nack_Mask                                                         cBit26
#define cAf6_upen_stk_ffempty_pla_add_nack_Shift                                                            26

/*--------------------------------------
BitField Name: pwe_add_nack
BitField Type: RW
BitField Desc: PWE address configuration which is not returned ACK
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_pwe_add_nack_Mask                                                         cBit25
#define cAf6_upen_stk_ffempty_pwe_add_nack_Shift                                                            25

/*--------------------------------------
BitField Name: cla_add_nack
BitField Type: RW
BitField Desc: CLA address configuration which is not returned ACK
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_cla_add_nack_Mask                                                         cBit24
#define cAf6_upen_stk_ffempty_cla_add_nack_Shift                                                            24

/*--------------------------------------
BitField Name: pda_add_nack
BitField Type: RW
BitField Desc: PDA address configuration which is not returned ACK
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_pda_add_nack_Mask                                                         cBit23
#define cAf6_upen_stk_ffempty_pda_add_nack_Shift                                                            23

/*--------------------------------------
BitField Name: bertgen_add_nack
BitField Type: RW
BitField Desc: BERTGEN address configuration which is not returned ACK
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_bertgen_add_nack_Mask                                                     cBit22
#define cAf6_upen_stk_ffempty_bertgen_add_nack_Shift                                                        22

/*--------------------------------------
BitField Name: bertmon_add_nack
BitField Type: RW
BitField Desc: BERTMON address configuration which is not returned ACK
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_bertmon_add_nack_Mask                                                     cBit21
#define cAf6_upen_stk_ffempty_bertmon_add_nack_Shift                                                        21

/*--------------------------------------
BitField Name: bertmonpw_add_nack
BitField Type: RW
BitField Desc: BERTMONPW address configuration which is not returned ACK
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_bertmonpw_add_nack_Mask                                                   cBit20
#define cAf6_upen_stk_ffempty_bertmonpw_add_nack_Shift                                                      20

/*--------------------------------------
BitField Name: prm_add_nack
BitField Type: RW
BitField Desc: PRM address configuration which is not returned ACK
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_prm_add_nack_Mask                                                         cBit19
#define cAf6_upen_stk_ffempty_prm_add_nack_Shift                                                            19

/*--------------------------------------
BitField Name: pdhmux_add_nack
BitField Type: RW
BitField Desc: PDHMUX address configuration which is not returned ACK
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_pdhmux_add_nack_Mask                                                      cBit18
#define cAf6_upen_stk_ffempty_pdhmux_add_nack_Shift                                                         18

/*--------------------------------------
BitField Name: pmfm_add_nack
BitField Type: RW
BitField Desc: PMFM address configuration which is not returned ACK
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_pmfm_add_nack_Mask                                                        cBit17
#define cAf6_upen_stk_ffempty_pmfm_add_nack_Shift                                                           17

/*--------------------------------------
BitField Name: dcc_add_nack
BitField Type: RW
BitField Desc: DCC address configuration which is not returned ACK
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_dcc_add_nack_Mask                                                         cBit16
#define cAf6_upen_stk_ffempty_dcc_add_nack_Shift                                                            16

/*--------------------------------------
BitField Name: ocn_ffempty
BitField Type: RW
BitField Desc: OCN fifo store data load is emtpy
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_ocn_ffempty_Mask                                                          cBit15
#define cAf6_upen_stk_ffempty_ocn_ffempty_Shift                                                             15

/*--------------------------------------
BitField Name: poh_ffempty
BitField Type: RW
BitField Desc: POH fifo store data load is emtpy
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_poh_ffempty_Mask                                                          cBit14
#define cAf6_upen_stk_ffempty_poh_ffempty_Shift                                                             14

/*--------------------------------------
BitField Name: pdh_ffempty
BitField Type: RW
BitField Desc: PDH fifo store data load is emtpy
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_pdh_ffempty_Mask                                                          cBit13
#define cAf6_upen_stk_ffempty_pdh_ffempty_Shift                                                             13

/*--------------------------------------
BitField Name: map_ffempty
BitField Type: RW
BitField Desc: MAP fifo store data load is emtpy
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_map_ffempty_Mask                                                          cBit12
#define cAf6_upen_stk_ffempty_map_ffempty_Shift                                                             12

/*--------------------------------------
BitField Name: cdr_ffempty
BitField Type: RW
BitField Desc: CDR fifo store data load is emtpy
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_cdr_ffempty_Mask                                                          cBit11
#define cAf6_upen_stk_ffempty_cdr_ffempty_Shift                                                             11

/*--------------------------------------
BitField Name: pla_ffempty
BitField Type: RW
BitField Desc: PLA fifo store data load is emtpy
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_pla_ffempty_Mask                                                          cBit10
#define cAf6_upen_stk_ffempty_pla_ffempty_Shift                                                             10

/*--------------------------------------
BitField Name: pwe_ffempty
BitField Type: RW
BitField Desc: PWE fifo store data load is emtpy
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_pwe_ffempty_Mask                                                           cBit9
#define cAf6_upen_stk_ffempty_pwe_ffempty_Shift                                                              9

/*--------------------------------------
BitField Name: cla_ffempty
BitField Type: RW
BitField Desc: CLA fifo store data load is emtpy
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_cla_ffempty_Mask                                                           cBit8
#define cAf6_upen_stk_ffempty_cla_ffempty_Shift                                                              8

/*--------------------------------------
BitField Name: pda_ffempty
BitField Type: RW
BitField Desc: PDA fifo store data load is emtpy
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_pda_ffempty_Mask                                                           cBit7
#define cAf6_upen_stk_ffempty_pda_ffempty_Shift                                                              7

/*--------------------------------------
BitField Name: bertgen_ffempty
BitField Type: RW
BitField Desc: BERTGEN fifo store data load is emtpy
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_bertgen_ffempty_Mask                                                       cBit6
#define cAf6_upen_stk_ffempty_bertgen_ffempty_Shift                                                          6

/*--------------------------------------
BitField Name: bertmon_ffempty
BitField Type: RW
BitField Desc: BERTMON fifo store data load is emtpy
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_bertmon_ffempty_Mask                                                       cBit5
#define cAf6_upen_stk_ffempty_bertmon_ffempty_Shift                                                          5

/*--------------------------------------
BitField Name: bertmonpw_ffempty
BitField Type: RW
BitField Desc: BERTMONPW fifo store data load is emtpy
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_bertmonpw_ffempty_Mask                                                     cBit4
#define cAf6_upen_stk_ffempty_bertmonpw_ffempty_Shift                                                        4

/*--------------------------------------
BitField Name: prm_ffempty
BitField Type: RW
BitField Desc: PRM fifo store data load is emtpy
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_prm_ffempty_Mask                                                           cBit3
#define cAf6_upen_stk_ffempty_prm_ffempty_Shift                                                              3

/*--------------------------------------
BitField Name: pdhmux_ffempty
BitField Type: RW
BitField Desc: PDHMUX fifo store data load is emtpy
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_pdhmux_ffempty_Mask                                                        cBit2
#define cAf6_upen_stk_ffempty_pdhmux_ffempty_Shift                                                           2

/*--------------------------------------
BitField Name: pmfm_ffempty
BitField Type: RW
BitField Desc: PMFM fifo store data load is emtpy
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_pmfm_ffempty_Mask                                                          cBit1
#define cAf6_upen_stk_ffempty_pmfm_ffempty_Shift                                                             1

/*--------------------------------------
BitField Name: dcc_ffempty
BitField Type: RW
BitField Desc: DCC fifo store data load is emtpy
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_stk_ffempty_dcc_ffempty_Mask                                                           cBit0
#define cAf6_upen_stk_ffempty_dcc_ffempty_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VALUE DATA TO FLUSH DDR
Reg Addr   : 0x0D
Reg Formula: 
    Where  : 
Reg Desc   : 
config DATA to WRITE TO DDR FOR FLUSHING

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_wrdat_flush_Base                                                                 0x0D

/*--------------------------------------
BitField Name: cfg_wrdat_flush
BitField Type: R/W
BitField Desc: 128 bit data to write DDR for flushing
BitField Bits: [127:00]
--------------------------------------*/
#define cAf6_upen_cfg_wrdat_flush_cfg_wrdat_flush_01_Mask                                             cBit31_0
#define cAf6_upen_cfg_wrdat_flush_cfg_wrdat_flush_01_Shift                                                   0
#define cAf6_upen_cfg_wrdat_flush_cfg_wrdat_flush_02_Mask                                             cBit31_0
#define cAf6_upen_cfg_wrdat_flush_cfg_wrdat_flush_02_Shift                                                   0
#define cAf6_upen_cfg_wrdat_flush_cfg_wrdat_flush_03_Mask                                             cBit31_0
#define cAf6_upen_cfg_wrdat_flush_cfg_wrdat_flush_03_Shift                                                   0
#define cAf6_upen_cfg_wrdat_flush_cfg_wrdat_flush_04_Mask                                             cBit31_0
#define cAf6_upen_cfg_wrdat_flush_cfg_wrdat_flush_04_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG SELECT DATABASE TO FLUSH
Reg Addr   : 0x0E
Reg Formula: 
    Where  : 
Reg Desc   : 
configure value from 0 --> 4 for corresponding 5 Database on DDR for flushing

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_db_flush_Base                                                                    0x0E
#define cAf6Reg_upen_cfg_db_flush_WidthVal                                                                  32

/*--------------------------------------
BitField Name: out_cfg_db
BitField Type: R/W
BitField Desc: configure value from 0 --> 4 for flushing corresponding one of 5
Database on DDR, configure value 5 for flushing all Database on DDR, value 7 is
IDLE
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_cfg_db_flush_out_cfg_db_Mask                                                         cBit2_0
#define cAf6_upen_cfg_db_flush_out_cfg_db_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS OCN WRONG
Reg Addr   : 0x10
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS OCN WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_ocn_wrong_Base                                                                   0x10

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_ocn_wrong_reserve1_Mask                                                        cBit31_21
#define cAf6_upen_add_ocn_wrong_reserve1_Shift                                                              21

/*--------------------------------------
BitField Name: ddr_add_ocn_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_ocn_wrong_ddr_add_ocn_wrong_Mask                                                cBit20_0
#define cAf6_upen_add_ocn_wrong_ddr_add_ocn_wrong_Shift                                                      0

/*--------------------------------------
BitField Name: sub_add_ocn_wrong
BitField Type: R_O
BitField Desc: address of module OCN WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_ocn_wrong_sub_add_ocn_wrong_Mask                                                cBit23_0
#define cAf6_upen_add_ocn_wrong_sub_add_ocn_wrong_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS POH WRONG
Reg Addr   : 0x11
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS POH WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_poh_wrong_Base                                                                   0x11

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_poh_wrong_reserve1_Mask                                                        cBit31_21
#define cAf6_upen_add_poh_wrong_reserve1_Shift                                                              21

/*--------------------------------------
BitField Name: ddr_add_poh_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_poh_wrong_ddr_add_poh_wrong_Mask                                                cBit20_0
#define cAf6_upen_add_poh_wrong_ddr_add_poh_wrong_Shift                                                      0

/*--------------------------------------
BitField Name: sub_add_poh_wrong
BitField Type: R_O
BitField Desc: address of module POH WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_poh_wrong_sub_add_poh_wrong_Mask                                                cBit23_0
#define cAf6_upen_add_poh_wrong_sub_add_poh_wrong_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PDH WRONG
Reg Addr   : 0x12
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PDH WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_pdh_wrong_Base                                                                   0x12

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_pdh_wrong_reserve1_Mask                                                        cBit31_21
#define cAf6_upen_add_pdh_wrong_reserve1_Shift                                                              21

/*--------------------------------------
BitField Name: ddr_add_pdh_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_pdh_wrong_ddr_add_pdh_wrong_Mask                                                cBit20_0
#define cAf6_upen_add_pdh_wrong_ddr_add_pdh_wrong_Shift                                                      0

/*--------------------------------------
BitField Name: sub_add_pdh_wrong
BitField Type: R_O
BitField Desc: address of module PDH WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_pdh_wrong_sub_add_pdh_wrong_Mask                                                cBit23_0
#define cAf6_upen_add_pdh_wrong_sub_add_pdh_wrong_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS MAP WRONG
Reg Addr   : 0x13
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS MAP WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_map_wrong_Base                                                                   0x13

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_map_wrong_reserve1_Mask                                                        cBit31_21
#define cAf6_upen_add_map_wrong_reserve1_Shift                                                              21

/*--------------------------------------
BitField Name: ddr_add_map_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_map_wrong_ddr_add_map_wrong_Mask                                                cBit20_0
#define cAf6_upen_add_map_wrong_ddr_add_map_wrong_Shift                                                      0

/*--------------------------------------
BitField Name: sub_add_map_wrong
BitField Type: R_O
BitField Desc: address of module MAP WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_map_wrong_sub_add_map_wrong_Mask                                                cBit23_0
#define cAf6_upen_add_map_wrong_sub_add_map_wrong_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS CDR WRONG
Reg Addr   : 0x14
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS CDR WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_cdr_wrong_Base                                                                   0x14

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_cdr_wrong_reserve1_Mask                                                        cBit31_21
#define cAf6_upen_add_cdr_wrong_reserve1_Shift                                                              21

/*--------------------------------------
BitField Name: ddr_add_cdr_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_cdr_wrong_ddr_add_cdr_wrong_Mask                                                cBit20_0
#define cAf6_upen_add_cdr_wrong_ddr_add_cdr_wrong_Shift                                                      0

/*--------------------------------------
BitField Name: sub_add_cdr_wrong
BitField Type: R_O
BitField Desc: address of module CDR WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_cdr_wrong_sub_add_cdr_wrong_Mask                                                cBit23_0
#define cAf6_upen_add_cdr_wrong_sub_add_cdr_wrong_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PLA WRONG
Reg Addr   : 0x15
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PLA WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_pla_wrong_Base                                                                   0x15

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_pla_wrong_reserve1_Mask                                                        cBit31_21
#define cAf6_upen_add_pla_wrong_reserve1_Shift                                                              21

/*--------------------------------------
BitField Name: ddr_add_pla_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_pla_wrong_ddr_add_pla_wrong_Mask                                                cBit20_0
#define cAf6_upen_add_pla_wrong_ddr_add_pla_wrong_Shift                                                      0

/*--------------------------------------
BitField Name: sub_add_pla_wrong
BitField Type: R_O
BitField Desc: address of module PLA WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_pla_wrong_sub_add_pla_wrong_Mask                                                cBit23_0
#define cAf6_upen_add_pla_wrong_sub_add_pla_wrong_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PWE WRONG
Reg Addr   : 0x16
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PWE WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_pwe_wrong_Base                                                                   0x16

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_pwe_wrong_reserve1_Mask                                                        cBit31_21
#define cAf6_upen_add_pwe_wrong_reserve1_Shift                                                              21

/*--------------------------------------
BitField Name: ddr_add_pwe_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_pwe_wrong_ddr_add_pwe_wrong_Mask                                                cBit20_0
#define cAf6_upen_add_pwe_wrong_ddr_add_pwe_wrong_Shift                                                      0

/*--------------------------------------
BitField Name: sub_add_pwe_wrong
BitField Type: R_O
BitField Desc: address of module PWE WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_pwe_wrong_sub_add_pwe_wrong_Mask                                                cBit23_0
#define cAf6_upen_add_pwe_wrong_sub_add_pwe_wrong_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS CLA WRONG
Reg Addr   : 0x17
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS CLA WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_cla_wrong_Base                                                                   0x17

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_cla_wrong_reserve1_Mask                                                        cBit31_21
#define cAf6_upen_add_cla_wrong_reserve1_Shift                                                              21

/*--------------------------------------
BitField Name: ddr_add_cla_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_cla_wrong_ddr_add_cla_wrong_Mask                                                cBit20_0
#define cAf6_upen_add_cla_wrong_ddr_add_cla_wrong_Shift                                                      0

/*--------------------------------------
BitField Name: sub_add_cla_wrong
BitField Type: R_O
BitField Desc: address of module CLA WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_cla_wrong_sub_add_cla_wrong_Mask                                                cBit23_0
#define cAf6_upen_add_cla_wrong_sub_add_cla_wrong_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PDA WRONG
Reg Addr   : 0x18
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PDA WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_pda_wrong_Base                                                                   0x18

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_pda_wrong_reserve1_Mask                                                        cBit31_21
#define cAf6_upen_add_pda_wrong_reserve1_Shift                                                              21

/*--------------------------------------
BitField Name: ddr_add_pda_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_pda_wrong_ddr_add_pda_wrong_Mask                                                cBit20_0
#define cAf6_upen_add_pda_wrong_ddr_add_pda_wrong_Shift                                                      0

/*--------------------------------------
BitField Name: sub_add_pda_wrong
BitField Type: R_O
BitField Desc: address of module PDA WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_pda_wrong_sub_add_pda_wrong_Mask                                                cBit23_0
#define cAf6_upen_add_pda_wrong_sub_add_pda_wrong_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS BERTGEN WRONG
Reg Addr   : 0x19
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS BERTGEN WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_bertgen_wrong_Base                                                               0x19

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_bertgen_wrong_reserve1_Mask                                                    cBit31_21
#define cAf6_upen_add_bertgen_wrong_reserve1_Shift                                                          21

/*--------------------------------------
BitField Name: ddr_add_bertgen_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_bertgen_wrong_ddr_add_bertgen_wrong_Mask                                        cBit20_0
#define cAf6_upen_add_bertgen_wrong_ddr_add_bertgen_wrong_Shift                                              0

/*--------------------------------------
BitField Name: sub_add_bertgen_wrong
BitField Type: R_O
BitField Desc: address of module BERTGEN WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_bertgen_wrong_sub_add_bertgen_wrong_Mask                                        cBit23_0
#define cAf6_upen_add_bertgen_wrong_sub_add_bertgen_wrong_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS BERTMON WRONG
Reg Addr   : 0x1A
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS BERTMON WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_bertmon_wrong_Base                                                               0x1A

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_bertmon_wrong_reserve1_Mask                                                    cBit31_21
#define cAf6_upen_add_bertmon_wrong_reserve1_Shift                                                          21

/*--------------------------------------
BitField Name: ddr_add_bertmon_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_bertmon_wrong_ddr_add_bertmon_wrong_Mask                                        cBit20_0
#define cAf6_upen_add_bertmon_wrong_ddr_add_bertmon_wrong_Shift                                              0

/*--------------------------------------
BitField Name: sub_add_bertmon_wrong
BitField Type: R_O
BitField Desc: address of module BERTMON WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_bertmon_wrong_sub_add_bertmon_wrong_Mask                                        cBit23_0
#define cAf6_upen_add_bertmon_wrong_sub_add_bertmon_wrong_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS BERTMONPW WRONG
Reg Addr   : 0x1B
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS BERTMONPW WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_bertmonpw_wrong_Base                                                             0x1B

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_bertmonpw_wrong_reserve1_Mask                                                  cBit31_21
#define cAf6_upen_add_bertmonpw_wrong_reserve1_Shift                                                        21

/*--------------------------------------
BitField Name: ddr_add_bertmonpw_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_bertmonpw_wrong_ddr_add_bertmonpw_wrong_Mask                                    cBit20_0
#define cAf6_upen_add_bertmonpw_wrong_ddr_add_bertmonpw_wrong_Shift                                          0

/*--------------------------------------
BitField Name: sub_add_bertmonpw_wrong
BitField Type: R_O
BitField Desc: address of module BERTMONPW WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_bertmonpw_wrong_sub_add_bertmonpw_wrong_Mask                                    cBit23_0
#define cAf6_upen_add_bertmonpw_wrong_sub_add_bertmonpw_wrong_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PRM WRONG
Reg Addr   : 0x1C
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PRM WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_prm_wrong_Base                                                                   0x1C

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_prm_wrong_reserve1_Mask                                                        cBit31_21
#define cAf6_upen_add_prm_wrong_reserve1_Shift                                                              21

/*--------------------------------------
BitField Name: ddr_add_prm_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_prm_wrong_ddr_add_prm_wrong_Mask                                                cBit20_0
#define cAf6_upen_add_prm_wrong_ddr_add_prm_wrong_Shift                                                      0

/*--------------------------------------
BitField Name: sub_add_prm_wrong
BitField Type: R_O
BitField Desc: address of module PRM WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_prm_wrong_sub_add_prm_wrong_Mask                                                cBit23_0
#define cAf6_upen_add_prm_wrong_sub_add_prm_wrong_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PDHMUX WRONG
Reg Addr   : 0x1D
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PDHMUX WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_pdhmux_wrong_Base                                                                0x1D

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_pdhmux_wrong_reserve1_Mask                                                     cBit31_21
#define cAf6_upen_add_pdhmux_wrong_reserve1_Shift                                                           21

/*--------------------------------------
BitField Name: ddr_add_pdhmux_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_pdhmux_wrong_ddr_add_pdhmux_wrong_Mask                                          cBit20_0
#define cAf6_upen_add_pdhmux_wrong_ddr_add_pdhmux_wrong_Shift                                                0

/*--------------------------------------
BitField Name: sub_add_pdhmux_wrong
BitField Type: R_O
BitField Desc: address of module PDHMUX WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_pdhmux_wrong_sub_add_pdhmux_wrong_Mask                                          cBit23_0
#define cAf6_upen_add_pdhmux_wrong_sub_add_pdhmux_wrong_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PMFM WRONG
Reg Addr   : 0x1E
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PMFM WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_pmfm_wrong_Base                                                                  0x1E

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_pmfm_wrong_reserve1_Mask                                                       cBit31_21
#define cAf6_upen_add_pmfm_wrong_reserve1_Shift                                                             21

/*--------------------------------------
BitField Name: ddr_add_pmfm_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_pmfm_wrong_ddr_add_pmfm_wrong_Mask                                              cBit20_0
#define cAf6_upen_add_pmfm_wrong_ddr_add_pmfm_wrong_Shift                                                    0

/*--------------------------------------
BitField Name: sub_add_pmfm_wrong
BitField Type: R_O
BitField Desc: address of module PMFM WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_pmfm_wrong_sub_add_pmfm_wrong_Mask                                              cBit23_0
#define cAf6_upen_add_pmfm_wrong_sub_add_pmfm_wrong_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS DCC WRONG
Reg Addr   : 0x1F
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS DCC WRONG

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_dcc_wrong_Base                                                                   0x1F

/*--------------------------------------
BitField Name: reserve1
BitField Type: R_O
BitField Desc: reserve1
BitField Bits: [63:53]
--------------------------------------*/
#define cAf6_upen_add_dcc_wrong_reserve1_Mask                                                        cBit31_21
#define cAf6_upen_add_dcc_wrong_reserve1_Shift                                                              21

/*--------------------------------------
BitField Name: ddr_add_dcc_wrong
BitField Type: R_O
BitField Desc: location DDR add is wrong
BitField Bits: [52:32]
--------------------------------------*/
#define cAf6_upen_add_dcc_wrong_ddr_add_dcc_wrong_Mask                                                cBit20_0
#define cAf6_upen_add_dcc_wrong_ddr_add_dcc_wrong_Shift                                                      0

/*--------------------------------------
BitField Name: sub_add_dcc_wrong
BitField Type: R_O
BitField Desc: address of module DCC WRONG
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_dcc_wrong_sub_add_dcc_wrong_Mask                                                cBit23_0
#define cAf6_upen_add_dcc_wrong_sub_add_dcc_wrong_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS OCN NACK
Reg Addr   : 0x20
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS OCN NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_ocn_nack_Base                                                                    0x20
#define cAf6Reg_upen_add_ocn_nack_WidthVal                                                                  32

/*--------------------------------------
BitField Name: sub_add_ocn_nack
BitField Type: R_O
BitField Desc: address of module OCN NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_ocn_nack_sub_add_ocn_nack_Mask                                                  cBit23_0
#define cAf6_upen_add_ocn_nack_sub_add_ocn_nack_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS POH NACK
Reg Addr   : 0x21
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS POH NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_poh_nack_Base                                                                    0x21
#define cAf6Reg_upen_add_poh_nack_WidthVal                                                                  32

/*--------------------------------------
BitField Name: sub_add_poh_nack
BitField Type: R_O
BitField Desc: address of module POH NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_poh_nack_sub_add_poh_nack_Mask                                                  cBit23_0
#define cAf6_upen_add_poh_nack_sub_add_poh_nack_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PDH NACK
Reg Addr   : 0x22
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PDH NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_pdh_nack_Base                                                                    0x22
#define cAf6Reg_upen_add_pdh_nack_WidthVal                                                                  32

/*--------------------------------------
BitField Name: sub_add_pdh_nack
BitField Type: R_O
BitField Desc: address of module PDH NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_pdh_nack_sub_add_pdh_nack_Mask                                                  cBit23_0
#define cAf6_upen_add_pdh_nack_sub_add_pdh_nack_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS MAP NACK
Reg Addr   : 0x23
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS MAP NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_map_nack_Base                                                                    0x23
#define cAf6Reg_upen_add_map_nack_WidthVal                                                                  32

/*--------------------------------------
BitField Name: sub_add_map_nack
BitField Type: R_O
BitField Desc: address of module MAP NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_map_nack_sub_add_map_nack_Mask                                                  cBit23_0
#define cAf6_upen_add_map_nack_sub_add_map_nack_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS CDR NACK
Reg Addr   : 0x24
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS CDR NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_cdr_nack_Base                                                                    0x24
#define cAf6Reg_upen_add_cdr_nack_WidthVal                                                                  32

/*--------------------------------------
BitField Name: sub_add_cdr_nack
BitField Type: R_O
BitField Desc: address of module CDR NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_cdr_nack_sub_add_cdr_nack_Mask                                                  cBit23_0
#define cAf6_upen_add_cdr_nack_sub_add_cdr_nack_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PLA NACK
Reg Addr   : 0x25
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PLA NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_pla_nack_Base                                                                    0x25
#define cAf6Reg_upen_add_pla_nack_WidthVal                                                                  32

/*--------------------------------------
BitField Name: sub_add_pla_nack
BitField Type: R_O
BitField Desc: address of module PLA NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_pla_nack_sub_add_pla_nack_Mask                                                  cBit23_0
#define cAf6_upen_add_pla_nack_sub_add_pla_nack_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PWE NACK
Reg Addr   : 0x26
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PWE NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_pwe_nack_Base                                                                    0x26
#define cAf6Reg_upen_add_pwe_nack_WidthVal                                                                  32

/*--------------------------------------
BitField Name: sub_add_pwe_nack
BitField Type: R_O
BitField Desc: address of module PWE NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_pwe_nack_sub_add_pwe_nack_Mask                                                  cBit23_0
#define cAf6_upen_add_pwe_nack_sub_add_pwe_nack_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS CLA NACK
Reg Addr   : 0x27
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS CLA NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_cla_nack_Base                                                                    0x27
#define cAf6Reg_upen_add_cla_nack_WidthVal                                                                  32

/*--------------------------------------
BitField Name: sub_add_cla_nack
BitField Type: R_O
BitField Desc: address of module CLA NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_cla_nack_sub_add_cla_nack_Mask                                                  cBit23_0
#define cAf6_upen_add_cla_nack_sub_add_cla_nack_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PDA NACK
Reg Addr   : 0x28
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PDA NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_pda_nack_Base                                                                    0x28
#define cAf6Reg_upen_add_pda_nack_WidthVal                                                                  32

/*--------------------------------------
BitField Name: sub_add_pda_nack
BitField Type: R_O
BitField Desc: address of module PDA NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_pda_nack_sub_add_pda_nack_Mask                                                  cBit23_0
#define cAf6_upen_add_pda_nack_sub_add_pda_nack_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS BERTGEN NACK
Reg Addr   : 0x29
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS BERTGEN NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_bertgen_nack_Base                                                                0x29
#define cAf6Reg_upen_add_bertgen_nack_WidthVal                                                              32

/*--------------------------------------
BitField Name: sub_add_bertgen_nack
BitField Type: R_O
BitField Desc: address of module BERTGEN NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_bertgen_nack_sub_add_bertgen_nack_Mask                                          cBit23_0
#define cAf6_upen_add_bertgen_nack_sub_add_bertgen_nack_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS BERTMON NACK
Reg Addr   : 0x2A
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS BERTMON NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_bertmon_nack_Base                                                                0x2A
#define cAf6Reg_upen_add_bertmon_nack_WidthVal                                                              32

/*--------------------------------------
BitField Name: sub_add_bertmon_nack
BitField Type: R_O
BitField Desc: address of module BERTMON NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_bertmon_nack_sub_add_bertmon_nack_Mask                                          cBit23_0
#define cAf6_upen_add_bertmon_nack_sub_add_bertmon_nack_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS BERTMONPW NACK
Reg Addr   : 0x2B
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS BERTMONPW NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_bertmonpw_nack_Base                                                              0x2B
#define cAf6Reg_upen_add_bertmonpw_nack_WidthVal                                                            32

/*--------------------------------------
BitField Name: sub_add_bertmonpw_nack
BitField Type: R_O
BitField Desc: address of module BERTMONPW NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_bertmonpw_nack_sub_add_bertmonpw_nack_Mask                                      cBit23_0
#define cAf6_upen_add_bertmonpw_nack_sub_add_bertmonpw_nack_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PRM NACK
Reg Addr   : 0x2C
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PRM NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_prm_nack_Base                                                                    0x2C
#define cAf6Reg_upen_add_prm_nack_WidthVal                                                                  32

/*--------------------------------------
BitField Name: sub_add_prm_nack
BitField Type: R_O
BitField Desc: address of module PRM NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_prm_nack_sub_add_prm_nack_Mask                                                  cBit23_0
#define cAf6_upen_add_prm_nack_sub_add_prm_nack_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PDHMUX NACK
Reg Addr   : 0x2D
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PDHMUX NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_pdhmux_nack_Base                                                                 0x2D
#define cAf6Reg_upen_add_pdhmux_nack_WidthVal                                                               32

/*--------------------------------------
BitField Name: sub_add_pdhmux_nack
BitField Type: R_O
BitField Desc: address of module PDHMUX NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_pdhmux_nack_sub_add_pdhmux_nack_Mask                                            cBit23_0
#define cAf6_upen_add_pdhmux_nack_sub_add_pdhmux_nack_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS PMFM NACK
Reg Addr   : 0x2E
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS PMFM NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_pmfm_nack_Base                                                                   0x2E
#define cAf6Reg_upen_add_pmfm_nack_WidthVal                                                                 32

/*--------------------------------------
BitField Name: sub_add_pmfm_nack
BitField Type: R_O
BitField Desc: address of module PMFM NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_pmfm_nack_sub_add_pmfm_nack_Mask                                                cBit23_0
#define cAf6_upen_add_pmfm_nack_sub_add_pmfm_nack_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : STATUS VALUE OF ADDRESS DCC NACK
Reg Addr   : 0x2F
Reg Formula: 
    Where  : 
Reg Desc   : 
STATUS ADDRESS DCC NACK

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_add_dcc_nack_Base                                                                    0x2F
#define cAf6Reg_upen_add_dcc_nack_WidthVal                                                                  32

/*--------------------------------------
BitField Name: sub_add_dcc_nack
BitField Type: R_O
BitField Desc: address of module DCC NACK
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_add_dcc_nack_sub_add_dcc_nack_Mask                                                  cBit23_0
#define cAf6_upen_add_dcc_nack_sub_add_dcc_nack_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : STICKY ECC ERROR
Reg Addr   : 0x30
Reg Formula: 
    Where  : 
Reg Desc   : 
DECODE 128 bit ECC, 4 lane  32 bit ECC --> 26 bit data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stk_ecc_Base                                                                         0x30

/*--------------------------------------
BitField Name: ecc_err_corr
BitField Type: RW
BitField Desc: ECC error correct, bit[8] is error of 26 bit LSB which DECOCDE
ECC (lane 0)
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_upen_stk_ecc_ecc_err_corr_Mask                                                           cBit11_8
#define cAf6_upen_stk_ecc_ecc_err_corr_Shift                                                                 8

/*--------------------------------------
BitField Name: ecc_err_det
BitField Type: RW
BitField Desc: ECC error detect, bit[4] is error of 26 bit LSB which DECOCDE ECC
(lane 0)
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_upen_stk_ecc_ecc_err_det_Mask                                                             cBit7_4
#define cAf6_upen_stk_ecc_ecc_err_det_Shift                                                                  4

/*--------------------------------------
BitField Name: ecc_err_fatal
BitField Type: RW
BitField Desc: ECC error fatal, bit[0] is error of 26 bit LSB which DECOCDE ECC
(lane 0)
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_stk_ecc_ecc_err_fatal_Mask                                                           cBit3_0
#define cAf6_upen_stk_ecc_ecc_err_fatal_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG FORCE ECC ERROR ENABLE
Reg Addr   : 0x31
Reg Formula: 
    Where  : 
Reg Desc   : 
enable force error ECC

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_force_err_ecc_enb_Base                                                               0x31
#define cAf6Reg_upen_force_err_ecc_enb_WidthVal                                                             32

/*--------------------------------------
BitField Name: enb_force_err_ecc
BitField Type: RW
BitField Desc: enable force error ECC,
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_force_err_ecc_enb_enb_force_err_ecc_Mask                                               cBit0
#define cAf6_upen_force_err_ecc_enb_enb_force_err_ecc_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : FORCE ECC ERROR VALUE LANE 0
Reg Addr   : 0x32
Reg Formula: 
    Where  : 
Reg Desc   : 
value which force error ECC at lane 0

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_force_err_ecc_lane0_Base                                                             0x32

/*--------------------------------------
BitField Name: lane0_err_ecc
BitField Type: RW
BitField Desc: value which force error ECC at lane 0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_force_err_ecc_lane0_lane0_err_ecc_Mask                                              cBit31_0
#define cAf6_upen_force_err_ecc_lane0_lane0_err_ecc_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : FORCE ECC ERROR VALUE LANE 1
Reg Addr   : 0x33
Reg Formula: 
    Where  : 
Reg Desc   : 
value which force error ECC at lane 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_force_err_ecc_lane1_Base                                                             0x33

/*--------------------------------------
BitField Name: lane1_err_ecc
BitField Type: RW
BitField Desc: value which force error ECC at lane 1
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_force_err_ecc_lane1_lane1_err_ecc_Mask                                              cBit31_0
#define cAf6_upen_force_err_ecc_lane1_lane1_err_ecc_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : FORCE ECC ERROR VALUE LANE 2
Reg Addr   : 0x34
Reg Formula: 
    Where  : 
Reg Desc   : 
value which force error ECC at lane 2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_force_err_ecc_lane2_Base                                                             0x34

/*--------------------------------------
BitField Name: lane2_err_ecc
BitField Type: RW
BitField Desc: value which force error ECC at lane 2
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_force_err_ecc_lane2_lane2_err_ecc_Mask                                              cBit31_0
#define cAf6_upen_force_err_ecc_lane2_lane2_err_ecc_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : FORCE ECC ERROR VALUE LANE 3
Reg Addr   : 0x35
Reg Formula: 
    Where  : 
Reg Desc   : 
value which force error ECC at lane 3

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_force_err_ecc_lane3_Base                                                             0x35

/*--------------------------------------
BitField Name: lane3_err_ecc
BitField Type: RW
BitField Desc: value which force error ECC at lane 3
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_force_err_ecc_lane3_lane3_err_ecc_Mask                                              cBit31_0
#define cAf6_upen_force_err_ecc_lane3_lane3_err_ecc_Shift                                                    0

#endif /* _AF6_REG_AF6CNC0061_RD_DDR_OFFLOAD_H_ */

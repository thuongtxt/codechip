/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60290061IpCore.h
 * 
 * Created Date: Mar 11, 2019
 *
 * Description : 60290061 IP core
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061IPCORE_H_
#define _THA60290061IPCORE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290061IpCore *Tha60290061IpCore;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIpCore Tha60290061IpCoreNew(uint8 coreId, AtDevice device);

/* To handle the real HAL to access real hardware */
void Tha60290061IpCoreHalSet(AtIpCore self, AtHal hal);
AtHal Tha60290061IpCoreHalGet(AtIpCore self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061IPCORE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60290061SubDeviceDebugger.h
 * 
 * Created Date: Aug 28, 2018
 *
 * Description : 60290061 DDR offload debug internal data and interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061SUBDEVICEDEBUGGER_H_
#define _THA60290061SUBDEVICEDEBUGGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "../../../../generic/common/AtObjectInternal.h"
#include "../../../default/util/ThaBitMask.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cTha60290061NumAddress cBit24

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290061SubDeviceDebugger *Tha60290061SubDeviceDebugger;

typedef struct tTha60290061SubDeviceDebuggerMethods
    {
    eAtRet (*Init)(Tha60290061SubDeviceDebugger self);
    }tTha60290061SubDeviceDebuggerMethods;

typedef struct tTha60290061SubDeviceDebugger
    {
    tAtObject super;
    const tTha60290061SubDeviceDebuggerMethods *methods;

    ThaBitMask accessedRegs;/* 24 bit address ==> 2^24 bit masks */
    AtDevice subDevice;
    }tTha60290061SubDeviceDebugger;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60290061SubDeviceDebugger Tha60290061SubDeviceDebuggerNew(AtDevice device);
void Tha60290061SubDeviceDebuggerAddressUse(Tha60290061SubDeviceDebugger self, uint32 address);
ThaBitMask Tha60290061SubDeviceDebuggerAddressBitMaskGet(Tha60290061SubDeviceDebugger self);
void Tha60290061SubDeviceDebuggerInit(Tha60290061SubDeviceDebugger self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061SUBDEVICEDEBUGGER_H_ */


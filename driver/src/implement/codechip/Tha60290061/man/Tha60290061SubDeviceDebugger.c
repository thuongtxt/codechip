/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60290061DeviceDdrOffloadDebug.c
 *
 * Created Date: Aug 28, 2018
 *
 * Description : 60290061 DDR offload debug implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtStd.h"
#include "AtOsal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/util/ThaBitMask.h"
#include "../../../../generic/man/AtDeviceInternal.h"
#include "Tha60290061SubDeviceDebugger.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290061SubDeviceDebugger)(self))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290061SubDeviceDebuggerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(Tha60290061SubDeviceDebugger self)
    {
    AtUnused(self);
    return cAtOk;
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "DdrOffloadDebugger";
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->accessedRegs));
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtObject self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

static void Override(AtObject self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(Tha60290061SubDeviceDebugger self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290061SubDeviceDebugger);
    }

static Tha60290061SubDeviceDebugger ObjectInit(Tha60290061SubDeviceDebugger self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override((AtObject)self);
    m_methodsInit = 1;

    /* Save private data */
    if (mThis(self)->accessedRegs == NULL)
        mThis(self)->accessedRegs = ThaBitMaskNew(cTha60290061NumAddress);
    mThis(self)->subDevice = device;

    return self;
    }

Tha60290061SubDeviceDebugger Tha60290061SubDeviceDebuggerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60290061SubDeviceDebugger newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    return ObjectInit(newController, device);
    }

void Tha60290061SubDeviceDebuggerAddressUse(Tha60290061SubDeviceDebugger self, uint32 address)
    {
    if (mThis(self)->accessedRegs == NULL)
        {
        AtDeviceLog(mThis(self)->subDevice, cAtLogLevelCritical, AtSourceLocation, "accessedRegs is NULL\r\n");
        return;
        }

    ThaBitMaskSetBit(mThis(self)->accessedRegs, address);
    }

ThaBitMask Tha60290061SubDeviceDebuggerAddressBitMaskGet(Tha60290061SubDeviceDebugger self)
    {
    return mThis(self)->accessedRegs;
    }

void Tha60290061SubDeviceDebuggerInit(Tha60290061SubDeviceDebugger self)
    {
    if (mThis(self)->accessedRegs == NULL)
        {
        AtDeviceLog(mThis(self)->subDevice, cAtLogLevelCritical, AtSourceLocation, "accessedRegs is NULL\r\n");
        return;
        }

    ThaBitMaskInit(mThis(self)->accessedRegs);
    }

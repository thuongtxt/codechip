/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290061Axi4EthPort.c
 *
 * Created Date: Apr 1, 2019
 *
 * Description : AXI4 2.5G ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/util/ThaUtil.h"
#include "../../Tha60290011/eth/Tha60290011Axi4EthPortInternal.h"
#include "../pdh/Tha60290061ModulePdh.h"
#include "Tha60290061ModuleEth.h"
#include "Tha60290061Axi4EthPortReg.h"
#include "Tha60290061ClsInterfaceEthPort.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290061Axi4EthPort
    {
    tTha60290011Axi4EthPort super;
    }tTha60290061Axi4EthPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods                      m_AtChannelOverride;
static tAtEthPortMethods                      m_AtEthPortOverride;
static tTha60290011ClsInterfaceEthPortMethods m_Tha60290011ClsInterfaceEthPortOverride;

/* Save super implementation */
static const tAtChannelMethods                      *m_AtChannelMethods                      = NULL;
static const tAtEthPortMethods                      *m_AtEthPortMethods                      = NULL;
static const tTha60290011ClsInterfaceEthPortMethods *m_Tha60290011ClsInterfaceEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePdh PdhModule(AtEthPort self)
    {
    return (ThaModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePdh);
    }

static uint32 RegisterWithBaseAddress(AtEthPort self, uint32 localAddress)
    {
    return localAddress + Tha60290011ClsInterfaceEthPortClsBaseAddress((ThaEthPort)self);
    }

static eAtRet ExpectedCVlanSet(AtEthPort self, const tAtVlan *cVlan)
    {
    eAtRet ret;

    ret = Tha60290061ClsInterfaceEthPortVlanCheckEnable(self, cVlan ? cAtTrue : cAtFalse);
    if (cVlan)
        ret |= Tha60290061ClsInterfaceEthPortVlanSet(self, cVlan);

    return ret;
    }

static tAtVlan *ExpectedCVlanGet(AtEthPort self, tAtVlan *cVlan)
    {
    eAtRet ret;

    if (!Tha60290061ClsInterfaceEthPortVlanCheckIsEnabled(self))
        return NULL;

    ret = Tha60290061ClsInterfaceEthPortVlanGet(self, cVlan);
    if (ret != cAtOk)
        return NULL;

    return cVlan;
    }

static eAtRet TxCVlanSet(AtEthPort self, const tAtVlan *cVlan)
    {
    eAtRet ret;

    ret = Tha60290061ModulePdhVlanInsertionEnable(PdhModule(self), cVlan ? cAtTrue : cAtFalse);
    if (cVlan)
        ret |= Tha60290061ModulePdhTxVlanSet(PdhModule(self), cVlan);

    return ret;
    }

static tAtVlan *TxCVlanGet(AtEthPort self, tAtVlan *cVlan)
    {
    eAtRet ret;

    if (!Tha60290061ModulePdhVlanInsertionIsEnabled(PdhModule(self)))
        return NULL;

    ret = Tha60290061ModulePdhTxVlanGet(PdhModule(self), cVlan);
    if (ret != cAtOk)
        return NULL;

    return cVlan;
    }

static eBool HasCVlan(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet DefaultSet(AtEthPort self)
    {
    uint32 regAddr = RegisterWithBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_mask_rmvlan_datapkt_, 1); /* Default remove VLAN */
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet((AtEthPort)self);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, ExpectedCVlanSet);
        mMethodOverride(m_AtEthPortOverride, ExpectedCVlanGet);
        mMethodOverride(m_AtEthPortOverride, TxCVlanSet);
        mMethodOverride(m_AtEthPortOverride, TxCVlanGet);
        mMethodOverride(m_AtEthPortOverride, HasCVlan);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideTha60290011ClsInterfaceEthPort(AtEthPort self)
    {
    Tha60290011ClsInterfaceEthPort ethPort = (Tha60290011ClsInterfaceEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290011ClsInterfaceEthPortMethods = mMethodsGet(ethPort);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011ClsInterfaceEthPortOverride, m_Tha60290011ClsInterfaceEthPortMethods, sizeof(m_Tha60290011ClsInterfaceEthPortOverride));

        }

    mMethodsSet(ethPort, &m_Tha60290011ClsInterfaceEthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideTha60290011ClsInterfaceEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011Axi4EthPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011Axi4EthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290061Axi4EthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

eAtRet Tha60290061ClsInterfaceEthPortVlanCheckEnable(AtEthPort self, eBool enable)
    {
    uint32 regAddr = RegisterWithBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_mask_Vlanid_mask_, enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

eBool Tha60290061ClsInterfaceEthPortVlanCheckIsEnabled(AtEthPort self)
    {
    uint32 regAddr = RegisterWithBaseAddress(self, cAf6Reg_upen_mask);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_upen_mask_Vlanid_mask_) ? cAtTrue : cAtFalse;
    }

eAtRet Tha60290061ClsInterfaceEthPortVlanSet(AtEthPort self, const tAtVlan *vlan)
    {
    uint32 regAddr = RegisterWithBaseAddress(self, cAf6Reg_upen_vlan);
    uint32 regVal = ThaPktUtilVlanToUint32(vlan);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

eAtRet Tha60290061ClsInterfaceEthPortVlanGet(AtEthPort self, tAtVlan *vlan)
    {
    uint32 regAddr = RegisterWithBaseAddress(self, cAf6Reg_upen_vlan);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    ThaPktUtilRegUint32ToVlan(regVal, vlan);

    return cAtOk;
    }


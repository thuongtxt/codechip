/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290061ModuleEth.h
 * 
 * Created Date: Apr 1, 2019
 *
 * Description : 60290061 module ETH
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061MODULEETH_H_
#define _THA60290061MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290011/eth/Tha60290011ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290061Axi4EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290061SerdesBackplaneEthPortNew(uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061MODULEETH_H_ */


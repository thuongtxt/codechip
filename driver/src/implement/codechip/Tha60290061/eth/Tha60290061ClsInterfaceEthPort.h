/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290061ClsInterfaceEthPort.h
 * 
 * Created Date: Apr 1, 2019
 *
 * Description : CLS interface ETH port.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061CLSINTERFACEETHPORT_H_
#define _THA60290061CLSINTERFACEETHPORT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290061ClsInterfaceEthPortVlanCheckEnable(AtEthPort self, eBool enable);
eBool Tha60290061ClsInterfaceEthPortVlanCheckIsEnabled(AtEthPort self);
eAtRet Tha60290061ClsInterfaceEthPortVlanSet(AtEthPort self, const tAtVlan *vlan);
eAtRet Tha60290061ClsInterfaceEthPortVlanGet(AtEthPort self, tAtVlan *vlan);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061CLSINTERFACEETHPORT_H_ */


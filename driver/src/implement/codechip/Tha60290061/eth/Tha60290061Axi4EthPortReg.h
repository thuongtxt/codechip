/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC1011_RD_CLS_H_
#define _AF6_REG_AF6CNC1011_RD_CLS_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Ethernet DA bit47_32 Configuration
Reg Addr   : 0x_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the DA bit47_32

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dasa                                                                               0x0000

/*--------------------------------------
BitField Name: DA_bit47_32
BitField Type: R/W
BitField Desc: DA bit47_32
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_dasa_DA_bit47_32_Mask                                                              cBit31_16
#define cAf6_upen_dasa_DA_bit47_32_Shift                                                                    16

/*--------------------------------------
BitField Name: SA_bit47_32
BitField Type: R/W
BitField Desc: SA bit47_32
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_dasa_SA_bit47_32_Mask                                                               cBit15_0
#define cAf6_upen_dasa_SA_bit47_32_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet DA bit31_00 Configuration
Reg Addr   : 0x_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the DA bit32_00

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_da31                                                                               0x0001

/*--------------------------------------
BitField Name: DA_bit31_00
BitField Type: R/W
BitField Desc: DA bit31_00
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_da31_DA_bit31_00_Mask                                                               cBit31_0
#define cAf6_upen_da31_DA_bit31_00_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet SA bit31_00 Configuration
Reg Addr   : 0x_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the SA bit31_00

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sa31                                                                               0x0002

/*--------------------------------------
BitField Name: SA_bit31_00
BitField Type: R/W
BitField Desc: SA bit31_00
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_sa31_SA_bit31_00_Mask                                                               cBit31_0
#define cAf6_upen_sa31_SA_bit31_00_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Sub_Type of packet control Configuration
Reg Addr   : 0x_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure E-type and Sub_Type

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_type                                                                               0x0003

/*--------------------------------------
BitField Name: E_type
BitField Type: R/W
BitField Desc: E_type of dat pkt
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_type_E_type_Mask                                                                    cBit15_0
#define cAf6_upen_type_E_type_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Classified Ethernet field mask & Lengh pkt  Configuration
Reg Addr   : 0x_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to mask some field of packet. These mask field are checked to discard or forward pkt

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mask                                                                               0x0004

/*--------------------------------------
BitField Name: Eth_len
BitField Type: R/W
BitField Desc: Len field , same as pdhmux cfg
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_mask_Eth_len_Mask                                                                  cBit31_16
#define cAf6_upen_mask_Eth_len_Shift                                                                        16

/*--------------------------------------
BitField Name: cbchk_mode
BitField Type: R/W
BitField Desc: Count bytes chk compare with length as mode , only use for RTL
self-test 00              : use length cfg Others         : use length calc by
RTL
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_upen_mask_cbchk_mode_Mask                                                               cBit15_14
#define cAf6_upen_mask_cbchk_mode_Shift                                                                     14

/*--------------------------------------
BitField Name: Seqerr_force
BitField Type: R/W
BitField Desc: only use for RTL self-test
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_upen_mask_Seqerr_force_Mask                                                             cBit13_12
#define cAf6_upen_mask_Seqerr_force_Shift                                                                   12

/*--------------------------------------
BitField Name: rmvlan_datapkt
BitField Type: R/W
BitField Desc: Remove Vlan of data pkt
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_mask_rmvlan_datapkt_Mask                                                               cBit9
#define cAf6_upen_mask_rmvlan_datapkt_Shift                                                                  9

/*--------------------------------------
BitField Name: rmvlan_ctrlpkt
BitField Type: R/W
BitField Desc: Remove Vlan of control pkt
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_mask_rmvlan_ctrlpkt_Mask                                                               cBit8
#define cAf6_upen_mask_rmvlan_ctrlpkt_Shift                                                                  8

/*--------------------------------------
BitField Name: Vlanid_mask
BitField Type: R/W
BitField Desc: Ingress Vlan ID check enable
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_mask_Vlanid_mask_Mask                                                                  cBit7
#define cAf6_upen_mask_Vlanid_mask_Shift                                                                     7

/*--------------------------------------
BitField Name: default_out
BitField Type: R/W
BitField Desc: Default output in etype check disable case CLS 2.5G : 0 : forward
to MAC 1G   , 1 : forward to PDH MUX CLS   1G : 0 : forward to MAC 2.5G , 1 :
forward to xxx (reserved)
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_mask_default_out_Mask                                                                  cBit6
#define cAf6_upen_mask_default_out_Shift                                                                     6

/*--------------------------------------
BitField Name: Subtype_mask
BitField Type: R/W
BitField Desc: sub-type check enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_mask_Subtype_mask_Mask                                                                 cBit5
#define cAf6_upen_mask_Subtype_mask_Shift                                                                    5

/*--------------------------------------
BitField Name: Sequence_mask
BitField Type: R/W
BitField Desc: sequence check enable
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_mask_Sequence_mask_Mask                                                                cBit4
#define cAf6_upen_mask_Sequence_mask_Shift                                                                   4

/*--------------------------------------
BitField Name: Length_mask
BitField Type: R/W
BitField Desc: length check enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_mask_Length_mask_Mask                                                                  cBit3
#define cAf6_upen_mask_Length_mask_Shift                                                                     3

/*--------------------------------------
BitField Name: Etype_mask
BitField Type: R/W
BitField Desc: E_type check enable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_mask_Etype_mask_Mask                                                                   cBit2
#define cAf6_upen_mask_Etype_mask_Shift                                                                      2

/*--------------------------------------
BitField Name: SA_mask
BitField Type: R/W
BitField Desc: SA check enable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_mask_SA_mask_Mask                                                                      cBit1
#define cAf6_upen_mask_SA_mask_Shift                                                                         1

/*--------------------------------------
BitField Name: DA_mask
BitField Type: R/W
BitField Desc: DA check enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_mask_DA_mask_Mask                                                                      cBit0
#define cAf6_upen_mask_DA_mask_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet SA bit31_00 Configuration
Reg Addr   : 0x_0008
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure the Ingress Vlan Tag

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_vlan                                                                               0x0008

/*--------------------------------------
BitField Name: igtpid_val
BitField Type: R/W
BitField Desc: Ingress TPID
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_vlan_igtpid_val_Mask                                                               cBit31_16
#define cAf6_upen_vlan_igtpid_val_Shift                                                                     16

/*--------------------------------------
BitField Name: igvlan_id
BitField Type: R/W
BitField Desc: Ingress Vlan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_vlan_igvlan_id_Mask                                                                 cBit11_0
#define cAf6_upen_vlan_igvlan_id_Shift                                                                       0

/*------------------------------------------------------------------------------
Reg Name   : Ethernet Encapsulation EtheType and EtherLength  Configuration
Reg Addr   : 0x1000 - 0x101F
Reg Formula: 0x1000 + $ro*16 + $id
    Where  : 
           + $ro(0-1) : ro bit
           + $id(0-15) : Counter ID
Reg Desc   : 
This register is used to read classified packet counters , support both mode r2c and ro
Counter ID detail :
+ 0 	: DS1      packet counter (only count if etype check & subt_ype check anable)
+ 1 	: DS3      packet counter (only count if etype check & sub_type check anable)
+ 2 	: Control  packet counter (only count if etype check anable)
+ 4		: err seq counter
+ 5		: fcs err counter
+ 8		: da err packet counter
+ 9		: sa err  packet counter
+ 10	: no use
+ 11	: len err  packet counter
+ 7		: unused
ro			: enable mean read only (not clear)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_count_Base                                                                         0x1000
#define cAf6Reg_upen_count(ro, id)                                                       (0x1000+(ro)*16+(id))

/*--------------------------------------
BitField Name: Ether_Type
BitField Type: R/W
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_count_Ether_Type_Mask                                                               cBit31_0
#define cAf6_upen_count_Ether_Type_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Dump packet  Configuration
Reg Addr   : 0x_0006
Reg Formula: 
    Where  : 
Reg Desc   : 
This register  is used to configure dump packet type , use to dump pkt & debug

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dump                                                                               0x0006

/*--------------------------------------
BitField Name: dump_typ
BitField Type: R/W
BitField Desc: Dump pkt type 00 : dump pkt input 01 : dump pkt ds1 output 10 :
dump pkt ds3 output 11 : dump pkt control output
BitField Bits: [02:01]
--------------------------------------*/
#define cAf6_upen_dump_dump_typ_Mask                                                                   cBit2_1
#define cAf6_upen_dump_dump_typ_Shift                                                                        1

/*--------------------------------------
BitField Name: dfsop_enb
BitField Type: R/W
BitField Desc: dump from sop  enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_dump_dfsop_enb_Mask                                                                    cBit0
#define cAf6_upen_dump_dfsop_enb_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Dump packet data
Reg Addr   : 0x2000 - 0x2FFF
Reg Formula: 0x2000 + $id
    Where  : 
           + $id(0-4095) : Counter ID
Reg Desc   : 
This register is used to read data of dump packets,use to debug

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dbpkt_Base                                                                         0x2000
#define cAf6Reg_upen_dbpkt(id)                                                                   (0x2000+(id))

/*--------------------------------------
BitField Name: pkt_dat
BitField Type: R/W
BitField Desc: Data of dump packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dbpkt_pkt_dat_Mask                                                                  cBit31_0
#define cAf6_upen_dbpkt_pkt_dat_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Debug Stick 0
Reg Addr   : 0x0007
Reg Formula: 
    Where  : 
Reg Desc   : 
This sticky is used to debug. Write 0xFFFF_FFFF to clear

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stk                                                                                0x0007

/*--------------------------------------
BitField Name: Len_err_stk
BitField Type: R/W
BitField Desc: Length error sticky
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_stk_Len_err_stk_Mask                                                                  cBit13
#define cAf6_upen_stk_Len_err_stk_Shift                                                                     13

/*--------------------------------------
BitField Name: Etp_err_stk
BitField Type: R/W
BitField Desc: E-type error sticky
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_stk_Etp_err_stk_Mask                                                                  cBit12
#define cAf6_upen_stk_Etp_err_stk_Shift                                                                     12

/*--------------------------------------
BitField Name: Sa_err_stk
BitField Type: R/W
BitField Desc: SA Cnt error sticky
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_stk_Sa_err_stk_Mask                                                                   cBit11
#define cAf6_upen_stk_Sa_err_stk_Shift                                                                      11

/*--------------------------------------
BitField Name: Da_err_stk
BitField Type: R/W
BitField Desc: DA Cnt error sticky
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_stk_Da_err_stk_Mask                                                                   cBit10
#define cAf6_upen_stk_Da_err_stk_Shift                                                                      10

/*--------------------------------------
BitField Name: Fcn_err_stk
BitField Type: R/W
BitField Desc: Fcs Cnt  error sticky
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_stk_Fcn_err_stk_Mask                                                                   cBit9
#define cAf6_upen_stk_Fcn_err_stk_Shift                                                                      9

/*--------------------------------------
BitField Name: Seq_err_stk
BitField Type: R/W
BitField Desc: Sequence error sticky
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_stk_Seq_err_stk_Mask                                                                   cBit8
#define cAf6_upen_stk_Seq_err_stk_Shift                                                                      8

/*--------------------------------------
BitField Name: Ctl_pkt_stk
BitField Type: R/W
BitField Desc: Control packet output sticky
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_stk_Ctl_pkt_stk_Mask                                                                   cBit5
#define cAf6_upen_stk_Ctl_pkt_stk_Shift                                                                      5

/*--------------------------------------
BitField Name: Ds3_pkt_stk
BitField Type: R/W
BitField Desc: Ds3     packet output sticky
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_stk_Ds3_pkt_stk_Mask                                                                   cBit4
#define cAf6_upen_stk_Ds3_pkt_stk_Shift                                                                      4

/*--------------------------------------
BitField Name: Ds1_pkt_stk
BitField Type: R/W
BitField Desc: DS1     packet output sticky
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_stk_Ds1_pkt_stk_Mask                                                                   cBit3
#define cAf6_upen_stk_Ds1_pkt_stk_Shift                                                                      3

/*--------------------------------------
BitField Name: Fcs_pkt_stk
BitField Type: R/W
BitField Desc: FCS     packet  input sticky
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_stk_Fcs_pkt_stk_Mask                                                                   cBit2
#define cAf6_upen_stk_Fcs_pkt_stk_Shift                                                                      2

/*--------------------------------------
BitField Name: Eop_vld_stk
BitField Type: R/W
BitField Desc: Eop     valid   input sticky
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_stk_Eop_vld_stk_Mask                                                                   cBit1
#define cAf6_upen_stk_Eop_vld_stk_Shift                                                                      1

/*--------------------------------------
BitField Name: Pkt_vld_stk
BitField Type: R/W
BitField Desc: Pkt     valid   input sticky
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_stk_Pkt_vld_stk_Mask                                                                   cBit0
#define cAf6_upen_stk_Pkt_vld_stk_Shift                                                                      0

#endif /* _AF6_REG_AF6CNC1011_RD_CLS_H_ */

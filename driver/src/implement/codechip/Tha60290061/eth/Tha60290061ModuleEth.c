/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290061ModuleEth.c
 *
 * Created Date: Apr 1, 2019
 *
 * Description : 60290011 Module ETH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/eth/Tha60290011ModuleEthInternal.h"
#include "../man/Tha60290061Device.h"
#include "Tha60290061ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290061ModuleEth
    {
    tTha60290011ModuleEth super;
    }tTha60290061ModuleEth;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods                 m_AtModuleOverride;
static tAtModuleEthMethods              m_AtModuleEthOverride;
static tThaModuleEthMethods             m_ThaModuleEthOverride;
static tTha60290011ModuleEthMethods     m_Tha60290011ModuleEthOverride;

/* Save super implementation */
static const tAtModuleEthMethods            *m_AtModuleEthMethods  = NULL;
static const tAtModuleMethods               *m_AtModuleMethods     = NULL;
static const tTha60290011ModuleEthMethods   *m_Tha60290011ModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ClsVlanIsSupported(Tha60290011ModuleEth self)
    {
    return Tha60290061DeviceClsVlanIsSupported((Tha60290061Device)AtModuleDeviceGet((AtModule)self));
    }

static AtEthPort Axi4PortObjectCreate(Tha60290011ModuleEth self, uint8 portId)
    {
    if (ClsVlanIsSupported(self))
        return Tha60290061Axi4EthPortNew(portId, (AtModuleEth)self);

    return m_Tha60290011ModuleEthMethods->Axi4PortObjectCreate(self, portId);
    }

static AtEthPort SerdesBackplaneEthPortObjectCreate(Tha60290011ModuleEth self, uint8 portId)
    {
    return Tha60290061SerdesBackplaneEthPortNew(portId, (AtModuleEth)self);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return ret;
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, m_AtModuleEthMethods, sizeof(m_AtModuleEthOverride));
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideTha60290011ModuleEth(AtModuleEth self)
    {
    Tha60290011ModuleEth module = (Tha60290011ModuleEth)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290011ModuleEthMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011ModuleEthOverride,
                                         m_Tha60290011ModuleEthMethods, sizeof(m_Tha60290011ModuleEthOverride));

        mMethodOverride(m_Tha60290011ModuleEthOverride, Axi4PortObjectCreate);
        mMethodOverride(m_Tha60290011ModuleEthOverride, SerdesBackplaneEthPortObjectCreate);
        }

    mMethodsSet(module, &m_Tha60290011ModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModule(self);
    OverrideThaModuleEth(self);
    OverrideAtModuleEth(self);
    OverrideTha60290011ModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290061ModuleEth);
    }

static AtModuleEth ObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60290061ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60291011ModulePdh.c
 *
 * Created Date: Mar 13, 2019
 *
 * Description : 60290061 Module PDH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "Tha60290061ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdhMethods         m_ThaModulePdhOverride;

/* Save super implementation */
static const tThaModulePdhMethods *m_ThaModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool TxRetimingSignalingIsSupported(ThaModulePdh self)
    {
    return ThaDeviceSupportFeatureFromVersion(AtModuleDeviceGet((AtModule)self), 0, 3, 0x11);
    }

static eBool De1FastSearchIsSupported(ThaModulePdh self)
    {
    return ThaDeviceSupportFeatureFromVersion(AtModuleDeviceGet((AtModule)self), 4, 0, 0x0100);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, m_ThaModulePdhMethods, sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, TxRetimingSignalingIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De1FastSearchIsSupported);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideThaModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290061ModulePdh);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60291011ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60290061ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

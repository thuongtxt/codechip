/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290061ModulePdhMuxLiu.c
 *
 * Created Date: Apr 1, 2019
 *
 * Description : PDH MUX
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/util/ThaUtil.h"
#include "../../Tha60290011/pdh/Tha60290011ModulePdh.h"
#include "Tha60290061ModulePdhInternal.h"
#include "Tha60290061ModulePdhMuxReg.h"
#include "Tha60290061ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MuxRegisterWithBaseAddress(ThaModulePdh self, uint32 localAddress)
    {
    return localAddress + Tha60290011ModulePdhMuxBaseAddress(self);
    }

eAtRet Tha60290061ModulePdhTxVlanSet(ThaModulePdh self, const tAtVlan *vlan)
    {
    uint32 regAddr = MuxRegisterWithBaseAddress(self, cAf6Reg_upen18);
    uint32 regVal = ThaPktUtilVlanToUint32(vlan);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

eAtRet Tha60290061ModulePdhTxVlanGet(ThaModulePdh self, tAtVlan *vlan)
    {
    uint32 regAddr = MuxRegisterWithBaseAddress(self, cAf6Reg_upen18);
    uint32 regVal = mModuleHwRead(self, regAddr);
    ThaPktUtilRegUint32ToVlan(regVal, vlan);

    return cAtOk;
    }

eAtRet Tha60290061ModulePdhVlanInsertionEnable(ThaModulePdh self, eBool enable)
    {
    uint32 regAddr = MuxRegisterWithBaseAddress(self, cAf6Reg_upen0);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen0_Tx_VLAN_Insert_, enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

eBool Tha60290061ModulePdhVlanInsertionIsEnabled(ThaModulePdh self)
    {
    uint32 regAddr = MuxRegisterWithBaseAddress(self, cAf6Reg_upen0);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_upen0_Tx_VLAN_Insert_) ? cAtTrue : cAtFalse;
    }


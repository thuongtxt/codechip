/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290061ModulePdh.h
 * 
 * Created Date: Apr 1, 2019
 *
 * Description : 60290061 Module PDH
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061MODULEPDH_H_
#define _THA60290061MODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290061ModulePdhTxVlanSet(ThaModulePdh self, const tAtVlan *vlan);
eAtRet Tha60290061ModulePdhTxVlanGet(ThaModulePdh self, tAtVlan *vlan);
eAtRet Tha60290061ModulePdhVlanInsertionEnable(ThaModulePdh self, eBool enable);
eBool Tha60290061ModulePdhVlanInsertionIsEnabled(ThaModulePdh self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061MODULEPDH_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290061ModulePdhInternal.h
 * 
 * Created Date: Mar 13, 2019
 *
 * Description : 60290061 Module PDH
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061MODULEPDHINTERNAL_H_
#define _THA60290061MODULEPDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60291011/pdh/Tha60291011ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290061ModulePdh
    {
    tTha60291011ModulePdh super;

    }tTha60290061ModulePdh;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290061MODULEPDHINTERNAL_H_ */


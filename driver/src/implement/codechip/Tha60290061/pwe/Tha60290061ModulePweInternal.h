/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60290061ModulePweInternal.h
 * 
 * Created Date: Aug 5, 2018
 *
 * Description : 60290061 module PWE internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061MODULEPWEINTERNAL_H_
#define _THA60290061MODULEPWEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60291011/pwe/Tha60291011ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290061ModulePwe
    {
    tTha60291011ModulePwe super;

    }tTha60290061ModulePwe;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290061MODULEPWEINTERNAL_H_ */


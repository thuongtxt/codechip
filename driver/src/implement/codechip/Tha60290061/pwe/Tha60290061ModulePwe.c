/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60290061ModulePwe.c
 *
 * Created Date: Aug 5, 2018
 *
 * Description : 60290061 module PWE implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pwe/ThaModulePwe.h"
#include "Tha60290061ModulePweInternal.h"
#include "Tha60290061ModulePwePlaReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cPwStateIdle                             0
#define cPwStateAdding                           1
#define cPwStateAddDone                          2
#define cPwStateRemoving                         3
#define cThaPwePlaBaseAddress                    0x200000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePweMethods         m_ThaModulePweOverride;

/* Save super implementation */
static const tThaModulePweMethods *m_ThaModulePweMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PDHPWAddingProtocolToPreventBurstAddress(ThaModulePwe self, ThaPwAdapter adapter)
    {
    uint32 address = cAf6Reg_add_pro_2_prevent_burst_Base +  cThaPwePlaBaseAddress + mPwPlaOffset(self, (AtPw)adapter);
    return address;
    }

static eBool IsSimulated(ThaModulePwe self)
    {
    return AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)self));
    }

static eBool PwStateIsReady(ThaModulePwe self, AtPw pw, uint32 state)
    {
    const uint32 cTimeoutMs = 100;
    uint32 regAddr = PDHPWAddingProtocolToPreventBurstAddress(self, (ThaPwAdapter)pw);
    uint32 regVal  = 0;
    uint32 elapseTime = 0;
    tAtOsalCurTime startTime, curTime;
    AtOsal osal = AtSharedDriverOsalGet();
    AtUnused(self);

    if (IsSimulated(self))
        return cAtTrue;

    mMethodsGet(osal)->CurTimeGet(osal, &startTime);

    while (elapseTime <= cTimeoutMs)
        {
        regVal = mChannelHwRead(pw, regAddr, cThaModulePwe);
        if (mRegField(regVal, cAf6_add_pro_2_prevent_burst_AddRmPwState_) == state)
            return cAtTrue;

        /* Retry */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtChannelLog((AtChannel)pw, cAtLogLevelWarning, AtSourceLocation, "Cannot wait before removing\r\n");
    return cAtFalse;
    }

static eAtRet PwAddingPrepare(ThaModulePwe self, AtPw pw)
    {
    /*
     * Protocol state to add pw:
     * ==================================
     * Step 1: Add initial or pw idle, the protocol state value is 0
     * Step 2: For adding the pw, CPU will Write 1 value for the protocol state to start adding pw.
     *         The protocol state value is 1.
     * Step 3: CPU enables pw at DEMAP to finish the adding pw process.
     *         HW will "automatically" change the protocol state value to 2 to run the pw, and keep this state.
     */
    uint32 regAddr = PDHPWAddingProtocolToPreventBurstAddress(self, (ThaPwAdapter)pw);
    uint32 regVal =  0;

    mRegFieldSet(regVal, cAf6_add_pro_2_prevent_burst_AddRmPwState_, cPwStateAdding);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);
    return cAtOk;
    }

static eAtRet PwAddingStart(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    /* As described above, do nothing */
    return cAtOk;
    }

static eBool PwRemovingIsReady(ThaModulePwe self, AtPw pw)
    {
    return PwStateIsReady(self, pw, cPwStateIdle);
    }

static eAtRet PwRemovingStart(ThaModulePwe self, AtPw pw)
    {
    /*
     * Protocol state to remove pw:
     * ==================================
     * Step 1: For removing the pw, CPU will write value 3 for the protocol state to start removing pw.
     *         The protocol state value is 3.
     * Step 2: Poll the protocol state until return value 0 value,
     *         "after" that CPU disables pw at DEMAP to finish the removing pw process.
     */

    uint32 regAddr;
    uint32 regVal = 0;

    if (!ThaPwAdapterHwIsEnabled((ThaPwAdapter)pw))
        return cAtOk;

    regAddr = PDHPWAddingProtocolToPreventBurstAddress(self, (ThaPwAdapter)pw);

    mRegFieldSet(regVal, cAf6_add_pro_2_prevent_burst_AddRmPwState_, cPwStateRemoving);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    /* Wait for state to become 0 (step 2) */
    if (PwRemovingIsReady(self, pw))
        return cAtOk;

    return cAtErrorOperationTimeout;
    }

static eAtRet PwRemovingFinish(ThaModulePwe self, AtPw pw)
    {
    /* Re-add PW */
    if (!ThaPwAdapterHwIsEnabled((ThaPwAdapter)pw))
        return cAtOk;

    return PwAddingPrepare(self, pw);
    }

static eAtRet PwRemovingPrepare(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    /* This protocol is not supported in this product */
    return cAtTrue;
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, m_ThaModulePweMethods, sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PwAddingPrepare);
        mMethodOverride(m_ThaModulePweOverride, PwAddingStart);
        mMethodOverride(m_ThaModulePweOverride, PwRemovingPrepare);
        mMethodOverride(m_ThaModulePweOverride, PwRemovingStart);
        mMethodOverride(m_ThaModulePweOverride, PwRemovingFinish);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290061ModulePwe);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290061ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

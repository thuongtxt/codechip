/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC1011_RD_PLA_H_
#define _AF6_REG_AF6CNC1011_RD_PLA_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Thalassa PDHPW Payload Assemble Signaling Convert 1 to 4 Status
Reg Addr   : 0x00008000-0x0000AF77
Reg Formula: 0x00008000  + 1024*$stsid + 128*$vtg + 32*$vtn + $ds0id
    Where  : 
           + $stsid(0-3)
           + $vtg(0-6)
           + $vtn(0-3)
           + $ds0id(0-31)
Reg Desc   : 
These registers are used to convert

------------------------------------------------------------------------------*/
#define cAf6Reg_sig_conv_sta_Base                                                                   0x00008000

/*--------------------------------------
BitField Name: PDHPWPlaAsmSigConv1to4Sta
BitField Type: RW
BitField Desc: convert 1 to 4 signaling bits of timeslots of DS1 mode
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_sig_conv_sta_PDHPWPlaAsmSigConv1to4Sta_Mask                                               cBit2_0
#define cAf6_sig_conv_sta_PDHPWPlaAsmSigConv1to4Sta_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Thalassa PDHPW Adding Protocol To Prevent Burst
Reg Addr   : 0x00001000-0x0000153F
Reg Formula: 0x00001000 +  $PWID
    Where  : 
           + $PWID(0-1343): Pseudowire ID
Reg Desc   : 
These registers are used to configure add or remove in each PW channel

------------------------------------------------------------------------------*/
#define cAf6Reg_add_pro_2_prevent_burst_Base                                                        0x00001000
#define cAf6Reg_add_pro_2_prevent_burst_WidthVal                                                            32

/*--------------------------------------
BitField Name: AddRmPwState
BitField Type: RW
BitField Desc: State machine to add or remove a PDH PW Step1: Add intitial or pw
idle, the protocol state value is "zero" Step2: For adding the pw, CPU will
Write "1" value for the protocol state to start adding pw. The protocol state
value is "1". Step3: CPU enables pw at demap to finish the adding pw process. HW
will automatically change the protocol state value to "2" to run the pw, and
keep this state. Step4: For removing the pw, CPU will write "3" value for the
protocol state to start removing pw. The protocol state value is "3". Step5:
Poll the protocol state until return value "0" value, after that CPU disables pw
at demap to finish the removing pw process
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_add_pro_2_prevent_burst_AddRmPwState_Mask                                                 cBit1_0
#define cAf6_add_pro_2_prevent_burst_AddRmPwState_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Thalassa PDHPW Payload Assemble Payload Size Control
Reg Addr   : 0x00002000-0x0000253F
Reg Formula: 0x00002000 +  $PWID
    Where  : 
           + $PWID(0-1343): Pseudowire ID
Reg Desc   : 
These registers are used to configure payload size in each PW channel

------------------------------------------------------------------------------*/
#define cAf6Reg_pyld_size_ctrl_Base                                                                 0x00002000

/*--------------------------------------
BitField Name: RTPTsShapperEnb
BitField Type: RW
BitField Desc: RTP TS Shapper Enable. Should be enable in DS1/E1 SAToP. In
CESoPSN only enable for PW which is used as Source for DCR
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pyld_size_ctrl_RTPTsShapperEnb_Mask                                                        cBit18
#define cAf6_pyld_size_ctrl_RTPTsShapperEnb_Shift                                                           18

/*--------------------------------------
BitField Name: PDHPWPlaAsmPldTypeCtrl
BitField Type: RW
BitField Desc: Payload type in the PW channel 0: SAToP 4: CES with CAS 5: CES
without CAS 6: CEP
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_pyld_size_ctrl_PDHPWPlaAsmPldTypeCtrl_Mask                                              cBit17_15
#define cAf6_pyld_size_ctrl_PDHPWPlaAsmPldTypeCtrl_Shift                                                    15

/*--------------------------------------
BitField Name: PDHPWPlaAsmPldSizeCtrl
BitField Type: RW
BitField Desc: Payload size in the PW channel SAToP mode: [11:0] payload size in
a packet CES mode [5:0] number of DS0 [14:6] number of NxDS0 basic CEP mode:
[11:0] payload size in a packetizer fractional CEP mode: [3:0] Because value of
payload size may be 3/9, 4/9, 5/9, 6/9, 7/9, 8/9, or 9/9 of VC-3 or VC-4 SPE,
depended on fractional VC-3 or fractional VC-4 CEP mode. And a VC-3 or VC-4
//SPE has 9 row. So configured value will be number of rows of a SPE. Example,
if value of payload size is 3/9, it means packetizer will assemble payload data
of 3 rows of a SPE,  so value of the field //will be 3.
BitField Bits: [14:0]
--------------------------------------*/
#define cAf6_pyld_size_ctrl_PDHPWPlaAsmPldSizeCtrl_Mask                                               cBit14_0
#define cAf6_pyld_size_ctrl_PDHPWPlaAsmPldSizeCtrl_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Thalassa PDHPW Payload Assemble uP Address Convert Control
Reg Addr   : 0x00000000
Reg Formula: 
    Where  : 
Reg Desc   : 
These registers are used to enable converting mode for uP address as accessing into some RAMs which request to convert identification.

------------------------------------------------------------------------------*/
#define cAf6Reg_up_add_conv_ctrl_Base                                                               0x00000000

/*--------------------------------------
BitField Name: PDHPWPlaAsmCPUAdrConvEnCtrl
BitField Type: RW
BitField Desc: Enable uP address converting 1: enable 0: disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_up_add_conv_ctrl_PDHPWPlaAsmCPUAdrConvEnCtrl_Mask                                           cBit1
#define cAf6_up_add_conv_ctrl_PDHPWPlaAsmCPUAdrConvEnCtrl_Shift                                              1

#endif /* _AF6_REG_AF6CNC1011_RD_PLA_H_ */

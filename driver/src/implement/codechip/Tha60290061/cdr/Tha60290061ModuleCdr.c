/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60290061ModuleCdr.c
 *
 * Created Date: Aug 16, 2018
 *
 * Description : 60290061 module CDR implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290061ModuleCdrInternal.h"
#include "Tha60290061ModuleCdrReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaModuelCdrBaseAddress 0x280000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleCdrMethods m_ThaModuleCdrOverride;
static tThaModuleCdrStmMethods m_ThaModuleCdrStmOverride;
static tTha60150011ModuleCdrMethods m_Tha60150011ModuleCdrOverride;
static tTha60290011ModuleCdrMethods m_Tha60290011ModuleCdrOverride;

/* Save super implementation */
static const tThaModuleCdrMethods * m_ThaModuleCdrMethods = NULL;
static const tThaModuleCdrStmMethods * m_ThaModuleCdrStmMethods = NULL;
static const tTha60150011ModuleCdrMethods * m_Tha60150011ModuleCdrMethods = NULL;
static const tTha60290011ModuleCdrMethods * m_Tha60290011ModuleCdrMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 LineModeCtrlOffset(uint8 hwStsId)
    {
    return (hwStsId);
    }

static uint32 CDRLineModeControlRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAf6Reg_cdr_line_mode_ctrl_Base + cThaModuelCdrBaseAddress;
    }

static uint32 CDRLineModeControlRegisterAddress(ThaModuleCdr self, uint8 hwSts)
    {
    uint32 address = mMethodsGet(self)->CDRLineModeControlRegister(self) + LineModeCtrlOffset(hwSts);
    return address;
    }

static uint32 VtgType_Mask(uint8 vtgId)
    {
    return cBit0 << vtgId;
    }

static uint32 VtgType_Shift(uint8 vtgId)
    {
    return (uint32)vtgId;
    }

static eAtRet HwVtgPldDefaultSet(ThaModuleCdr self, uint8 hwSts, uint8 vtgId, uint32 hwPayloadType)
    {
    uint32 regVal;
    uint32 regAddr;
    uint32 vtgType_Mask = VtgType_Mask(vtgId);
    uint32 vtgType_Shift = VtgType_Shift(vtgId);

    regAddr = CDRLineModeControlRegisterAddress(self, hwSts);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, vtgType_, hwPayloadType);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HwStsPldDefaultSet(ThaModuleCdr self, uint8 hwSts, uint8 hwStsMd, uint8 hwDe3Md)
    {
    uint32 regVal;
    uint32 regAddr;

    regAddr = CDRLineModeControlRegisterAddress(self, hwSts);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_cdr_line_mode_ctrl_STS1STSMode_, hwStsMd);
    mRegFieldSet(regVal, cAf6_cdr_line_mode_ctrl_DE3Mode_, hwDe3Md);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet Vc3CepModeEnable(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, eBool enable)
    {
    uint32 regVal;
    uint32 regAddr;

    AtUnused(channel);
    AtUnused(slice);
    regAddr = CDRLineModeControlRegisterAddress(self, hwSts);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_cdr_line_mode_ctrl_VC3Mode_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HwVtgPldSet(Tha60150011ModuleCdr self, AtChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType, uint32 lineModeCtrlOffset)
    {
    ThaModuleCdr module = (ThaModuleCdr)self;

    AtUnused(channel);
    AtUnused(slice);
    AtUnused(lineModeCtrlOffset);

    return HwVtgPldDefaultSet(module, hwSts, vtgId, Tha60150011ModuleCdrHwVtType(payloadType));
    }

static eAtRet HwStsPldSet(Tha60150011ModuleCdr self, AtChannel channel, uint8 hwStsMd, uint8 hwSts, uint8 hwPayloadType, uint32 lineModeCtrlOffset)
    {
    ThaModuleCdr module = (ThaModuleCdr)self;

    AtUnused(channel);
    AtUnused(lineModeCtrlOffset);
    return HwStsPldDefaultSet(module, hwSts, hwStsMd, hwPayloadType);
    }

static eAtRet HwStsModeSet(Tha60150011ModuleCdr self,  AtChannel channel, uint8 hwStsMd, uint8 hwSts, uint32 lineModeCtrlOffset)
    {
    uint32 regVal;
    uint32 regAddr;
    ThaModuleCdr module = (ThaModuleCdr)self;

    AtUnused(channel);
    AtUnused(lineModeCtrlOffset);

    regAddr = CDRLineModeControlRegisterAddress(module, hwSts);
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_cdr_line_mode_ctrl_STS1STSMode_, hwStsMd);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrMethods = mMethodsGet(cdrModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, m_ThaModuleCdrMethods, sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, CDRLineModeControlRegister);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void OverrideTha60290011ModuleCdr(AtModule self)
    {
    Tha60290011ModuleCdr cdrModule = (Tha60290011ModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290011ModuleCdrMethods = mMethodsGet(cdrModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011ModuleCdrOverride, m_Tha60290011ModuleCdrMethods, sizeof(m_Tha60290011ModuleCdrOverride));

        mMethodOverride(m_Tha60290011ModuleCdrOverride, HwVtgPldDefaultSet);
        mMethodOverride(m_Tha60290011ModuleCdrOverride, HwStsPldDefaultSet);
        }

    mMethodsSet(cdrModule, &m_Tha60290011ModuleCdrOverride);
    }

static void OverrideTha60150011ModuleCdr(AtModule self)
    {
    Tha60150011ModuleCdr module = (Tha60150011ModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60150011ModuleCdrMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011ModuleCdrOverride, m_Tha60150011ModuleCdrMethods, sizeof(m_Tha60150011ModuleCdrOverride));

        mMethodOverride(m_Tha60150011ModuleCdrOverride, HwVtgPldSet);
        mMethodOverride(m_Tha60150011ModuleCdrOverride, HwStsPldSet);
        mMethodOverride(m_Tha60150011ModuleCdrOverride, HwStsModeSet);
        }

    mMethodsSet(module, &m_Tha60150011ModuleCdrOverride);
    }

static void OverrideThaModuleCdrStm(AtModule self)
    {
    ThaModuleCdrStm module = (ThaModuleCdrStm)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrStmMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrStmOverride, m_ThaModuleCdrStmMethods, sizeof(m_ThaModuleCdrStmOverride));

        mMethodOverride(m_ThaModuleCdrStmOverride, Vc3CepModeEnable);
        }

    mMethodsSet(module, &m_ThaModuleCdrStmOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCdr(self);
    OverrideThaModuleCdrStm(self);
    OverrideTha60150011ModuleCdr(self);
    OverrideTha60290011ModuleCdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290061ModuleCdr);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290061ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

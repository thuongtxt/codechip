/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60290061ModuleCdrInternal.h
 * 
 * Created Date: Aug 16, 2018
 *
 * Description : 60290061 module CDR internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061MODULECDRINTERNAL_H_
#define _THA60290061MODULECDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290011/cdr/Tha60290011ModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290061ModuleCdr
    {
    tTha60290011ModuleCdr super;
    }tTha60290061ModuleCdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290061MODULECDRINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0061_RD_CDR_H_
#define _AF6_REG_AF6CNC0061_RD_CDR_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : CDR line mode control
Reg Addr   : 0x00000200 - 0x0000022F
Reg Formula: 0x00000200 +  STS
    Where  : 
           + $STS(0-23):
Reg Desc   : 
The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_line_mode_ctrl_Base                                                             0x00000200
#define cAf6Reg_cdr_line_mode_ctrl_WidthVal                                                                 32

/*--------------------------------------
BitField Name: VC3Mode
BitField Type: RW
BitField Desc: VC3 or TU3 mode, each bit is used configured for each STS (only
valid in TU3 CEP) 1: TU3 CEP 0: Other mode
BitField Bits: [9]
--------------------------------------*/
#define cAf6_cdr_line_mode_ctrl_VC3Mode_Mask                                                             cBit9
#define cAf6_cdr_line_mode_ctrl_VC3Mode_Shift                                                                9

/*--------------------------------------
BitField Name: DE3Mode
BitField Type: RW
BitField Desc: DS3 or E3 mode, each bit is used configured for each STS. 1: DS3
mode 0: E3 mod
BitField Bits: [8]
--------------------------------------*/
#define cAf6_cdr_line_mode_ctrl_DE3Mode_Mask                                                             cBit8
#define cAf6_cdr_line_mode_ctrl_DE3Mode_Shift                                                                8

/*--------------------------------------
BitField Name: STS1STSMode
BitField Type: RW
BitField Desc: STS mode of STS1 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode
BitField Bits: [7]
--------------------------------------*/
#define cAf6_cdr_line_mode_ctrl_STS1STSMode_Mask                                                         cBit7
#define cAf6_cdr_line_mode_ctrl_STS1STSMode_Shift                                                            7

/*--------------------------------------
BitField Name: STS1VTType
BitField Type: RW
BitField Desc: VT Type of 7 VTG in STS1, each bit is used configured for each
VTG. 1: DS1/VT15 mode 0: E1/VT2 mode
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_cdr_line_mode_ctrl_STS1VTType_Mask                                                        cBit6_0
#define cAf6_cdr_line_mode_ctrl_STS1VTType_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : CDR Timing External reference control
Reg Addr   : 0x000_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure for the 2Khz coefficient of two external reference signal in order to generate timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_timing_extref_ctrl_Base                                                          0x0000003

/*--------------------------------------
BitField Name: Ext3N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the third external reference signal,
default 32'd62500 (txethclk 125Mhz)
BitField Bits: [49:32]
--------------------------------------*/
#define cAf6_cdr_timing_extref_ctrl_Ext3N2k_Mask                                                      cBit17_0
#define cAf6_cdr_timing_extref_ctrl_Ext3N2k_Shift                                                            0

/*--------------------------------------
BitField Name: Ext2N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the second external reference signal,
default 16'd9720 (clk19.44Mhz)
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_cdr_timing_extref_ctrl_Ext2N2k_Mask                                                     cBit31_16
#define cAf6_cdr_timing_extref_ctrl_Ext2N2k_Shift                                                           16

/*--------------------------------------
BitField Name: Ext1N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the first external reference signal,
default 16'd9720 (clk19.44Mhz)
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_cdr_timing_extref_ctrl_Ext1N2k_Mask                                                      cBit15_0
#define cAf6_cdr_timing_extref_ctrl_Ext1N2k_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : CDR STS Timing control
Reg Addr   : 0x000_0800-0x000_081F
Reg Formula: 0x000_0800+STS
    Where  : 
           + $STS(0-23):
Reg Desc   : 
This register is used to configure timing mode for per STS

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_sts_timing_ctrl_Base                                                             0x0000800

/*--------------------------------------
BitField Name: CDRChid
BitField Type: RW
BitField Desc: CDR channel ID in CDR mode.
BitField Bits: [29:20]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_CDRChid_Mask                                                        cBit29_20
#define cAf6_cdr_sts_timing_ctrl_CDRChid_Shift                                                              20

/*--------------------------------------
BitField Name: STSCdrChEnb
BitField Type: RW
BitField Desc: Unused
BitField Bits: [19]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_STSCdrChEnb_Mask                                                       cBit19
#define cAf6_cdr_sts_timing_ctrl_STSCdrChEnb_Shift                                                          19

/*--------------------------------------
BitField Name: De3LIUMode
BitField Type: RW
BitField Desc: Unused
BitField Bits: [18]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_De3LIUMode_Mask                                                        cBit18
#define cAf6_cdr_sts_timing_ctrl_De3LIUMode_Shift                                                           18

/*--------------------------------------
BitField Name: VC3EParEn
BitField Type: RW
BitField Desc: VC3 EPAR mode 0 0: Disable 1: Enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_VC3EParEn_Mask                                                         cBit17
#define cAf6_cdr_sts_timing_ctrl_VC3EParEn_Shift                                                            17

/*--------------------------------------
BitField Name: MapSTSMode
BitField Type: RW
BitField Desc: Map STS mode 0: Payload STS mode 1: Payload DE3 mode
BitField Bits: [16]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_MapSTSMode_Mask                                                        cBit16
#define cAf6_cdr_sts_timing_ctrl_MapSTSMode_Shift                                                           16

/*--------------------------------------
BitField Name: VC3STSTimeMode
BitField Type: RW
BitField Desc: VC3 STS time mode (For DS3 over SONET) 0: Internal timing mode 1:
Reserve 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing
mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing
mode 8: Reserve 9: OCN System timing mode 10: Reserve
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_VC3STSTimeMode_Mask                                                  cBit11_8
#define cAf6_cdr_sts_timing_ctrl_VC3STSTimeMode_Shift                                                        8

/*--------------------------------------
BitField Name: DE3TimeMode
BitField Type: RW
BitField Desc: DE3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR
timing mode 9: OCN System timing mode 10: DCR timing mode
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_DE3TimeMode_Mask                                                      cBit7_4
#define cAf6_cdr_sts_timing_ctrl_DE3TimeMode_Shift                                                           4

/*--------------------------------------
BitField Name: VC3TimeMode
BitField Type: RW
BitField Desc: VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR
timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP
mode
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_sts_timing_ctrl_VC3TimeMode_Mask                                                      cBit3_0
#define cAf6_cdr_sts_timing_ctrl_VC3TimeMode_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : CDR VT Timing control
Reg Addr   : 0x000_0C00-0x000_0FFF
Reg Formula: 0x000_0C00 + STS*32 + TUG*4 + VT
    Where  : 
           + $STS(0-23):
           + $TUG(0 -6):
           + $VT(0-3): (0-2)in E1 and (0-3) in DS1
Reg Desc   : 
This register is used to configure timing mode for per VT

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_vt_timing_ctrl_Base                                                              0x0000C00
#define cAf6Reg_cdr_vt_timing_ctrl_WidthVal                                                                 32

/*--------------------------------------
BitField Name: CDRChid
BitField Type: RW
BitField Desc: CDR channel ID in CDR mode
BitField Bits: [21:12]
--------------------------------------*/
#define cAf6_cdr_vt_timing_ctrl_CDRChid_Mask                                                         cBit21_12
#define cAf6_cdr_vt_timing_ctrl_CDRChid_Shift                                                               12

/*--------------------------------------
BitField Name: VTCdrChEnb
BitField Type: RW
BitField Desc: Unused
BitField Bits: [11]
--------------------------------------*/
#define cAf6_cdr_vt_timing_ctrl_VTCdrChEnb_Mask                                                         cBit11
#define cAf6_cdr_vt_timing_ctrl_VTCdrChEnb_Shift                                                            11

/*--------------------------------------
BitField Name: De1LIUMode
BitField Type: RW
BitField Desc: Unused
BitField Bits: [10]
--------------------------------------*/
#define cAf6_cdr_vt_timing_ctrl_De1LIUMode_Mask                                                         cBit10
#define cAf6_cdr_vt_timing_ctrl_De1LIUMode_Shift                                                            10

/*--------------------------------------
BitField Name: VTEParEn
BitField Type: RW
BitField Desc: VT EPAR mode 0: Disable 1: Enable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_cdr_vt_timing_ctrl_VTEParEn_Mask                                                            cBit9
#define cAf6_cdr_vt_timing_ctrl_VTEParEn_Shift                                                               9

/*--------------------------------------
BitField Name: MapVTMode
BitField Type: RW
BitField Desc: Map VT mode 0: Payload VT15/VT2 mode 1: Payload DS1/E1 mode
BitField Bits: [8]
--------------------------------------*/
#define cAf6_cdr_vt_timing_ctrl_MapVTMode_Mask                                                           cBit8
#define cAf6_cdr_vt_timing_ctrl_MapVTMode_Shift                                                              8

/*--------------------------------------
BitField Name: DE1TimeMode
BitField Type: RW
BitField Desc: DE1 time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR
timing mode 9: OCN System timing mode 10: DCR timing mode
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_cdr_vt_timing_ctrl_DE1TimeMode_Mask                                                       cBit7_4
#define cAf6_cdr_vt_timing_ctrl_DE1TimeMode_Shift                                                            4

/*--------------------------------------
BitField Name: VTTimeMode
BitField Type: RW
BitField Desc: VT time mode 0: Internal timing mode 1: Loop timing mode 2: Line
OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line
OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR
timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP
mode
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_vt_timing_ctrl_VTTimeMode_Mask                                                        cBit3_0
#define cAf6_cdr_vt_timing_ctrl_VTTimeMode_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : CDR ToP Timing Frame sync 8K control
Reg Addr   : 0x000_0820
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure timing source to generate the 125us signal frame sync for ToP

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_top_timing_frmsync_ctrl_Base                                                     0x0000820

/*--------------------------------------
BitField Name: ToPPDHLineType
BitField Type: RW
BitField Desc: Line type of DS1/E1 loop time or PDH CDR 0: E1 1: DS1 2: VT2 3:
VT15 4: E3 5: DS3 6: STS1 7: Reserve
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_cdr_top_timing_frmsync_ctrl_ToPPDHLineType_Mask                                         cBit18_16
#define cAf6_cdr_top_timing_frmsync_ctrl_ToPPDHLineType_Shift                                               16

/*--------------------------------------
BitField Name: ToPPDHLineID
BitField Type: RW
BitField Desc: Line ID of DS1/E1 loop time or PDH/CEP CDR
BitField Bits: [13:4]
--------------------------------------*/
#define cAf6_cdr_top_timing_frmsync_ctrl_ToPPDHLineID_Mask                                            cBit13_4
#define cAf6_cdr_top_timing_frmsync_ctrl_ToPPDHLineID_Shift                                                  4

/*--------------------------------------
BitField Name: ToPTimeMode
BitField Type: RW
BitField Desc: Time mode  for ToP 0: System timing mode 1: DS1/E1 Loop timing
mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing
mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing
mode 8: PDH CDR timing mode 9: OCN System timing mode
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_top_timing_frmsync_ctrl_ToPTimeMode_Mask                                              cBit3_0
#define cAf6_cdr_top_timing_frmsync_ctrl_ToPTimeMode_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CDR Reference Sync 8K Master Output control
Reg Addr   : 0x000_0821
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure timing source to generate the reference sync master output signal

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_refsync_master_octrl_Base                                                        0x0000821

/*--------------------------------------
BitField Name: RefOut1PDHLineType
BitField Type: RW
BitField Desc: Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2
3: VT15 4: E3 5: DS3 6: STS1 7: Reserve
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_cdr_refsync_master_octrl_RefOut1PDHLineType_Mask                                        cBit18_16
#define cAf6_cdr_refsync_master_octrl_RefOut1PDHLineType_Shift                                              16

/*--------------------------------------
BitField Name: RefOut1PDHLineID
BitField Type: RW
BitField Desc: Line ID of DS1/E1 loop time or PDH CDR
BitField Bits: [13:4]
--------------------------------------*/
#define cAf6_cdr_refsync_master_octrl_RefOut1PDHLineID_Mask                                           cBit13_4
#define cAf6_cdr_refsync_master_octrl_RefOut1PDHLineID_Shift                                                 4

/*--------------------------------------
BitField Name: RefOut1TimeMode
BitField Type: RW
BitField Desc: Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing
mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing
mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing
mode 8: PDH CDR timing mode 9: External Clock sync
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_refsync_master_octrl_RefOut1TimeMode_Mask                                             cBit3_0
#define cAf6_cdr_refsync_master_octrl_RefOut1TimeMode_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : CDR Reference Sync 8k Slaver Output control
Reg Addr   : 0x000_0822
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure timing source to generate the reference sync slaver output signal

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_refsync_slaver_octrl_Base                                                        0x0000822

/*--------------------------------------
BitField Name: RefOut2PDHLineType
BitField Type: RW
BitField Desc: Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2
3: VT15 4: E3 5: DS3 6: STS1 7: Reserve
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_cdr_refsync_slaver_octrl_RefOut2PDHLineType_Mask                                        cBit18_16
#define cAf6_cdr_refsync_slaver_octrl_RefOut2PDHLineType_Shift                                              16

/*--------------------------------------
BitField Name: RefOut2PDHLineID
BitField Type: RW
BitField Desc: Line ID of DS1/E1 loop time or PDH CDR
BitField Bits: [13:4]
--------------------------------------*/
#define cAf6_cdr_refsync_slaver_octrl_RefOut2PDHLineID_Mask                                           cBit13_4
#define cAf6_cdr_refsync_slaver_octrl_RefOut2PDHLineID_Shift                                                 4

/*--------------------------------------
BitField Name: RefOut2TimeMode
BitField Type: RW
BitField Desc: Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing
mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing
mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing
mode 8: PDH CDR timing mode 9: External Clock sync
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_refsync_slaver_octrl_RefOut2TimeMode_Mask                                             cBit3_0
#define cAf6_cdr_refsync_slaver_octrl_RefOut2TimeMode_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : CDR ACR Timing External reference  and PRC control
Reg Addr   : 0x0020000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure for the 2Khz coefficient of external reference signal, PRC clock in order to generate the sync 125us timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_acr_timing_extref_prc_ctrl_Base                                                  0x0020000
#define cAf6Reg_cdr_acr_timing_extref_prc_ctrl_WidthVal                                                     96

/*--------------------------------------
BitField Name: PcrN2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the PRC clock signal
BitField Bits: [79:64]
--------------------------------------*/
#define cAf6_cdr_acr_timing_extref_prc_ctrl_PcrN2k_Mask                                               cBit15_0
#define cAf6_cdr_acr_timing_extref_prc_ctrl_PcrN2k_Shift                                                     0

/*--------------------------------------
BitField Name: TxOCNN2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the Ethernet clock signal, default 125Mhz
input clock
BitField Bits: [63:48]
--------------------------------------*/
#define cAf6_cdr_acr_timing_extref_prc_ctrl_TxOCNN2k_Mask                                            cBit31_16
#define cAf6_cdr_acr_timing_extref_prc_ctrl_TxOCNN2k_Shift                                                  16

/*--------------------------------------
BitField Name: Ext2N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the second external reference signal,
default 8khz input signal
BitField Bits: [47:32]
--------------------------------------*/
#define cAf6_cdr_acr_timing_extref_prc_ctrl_Ext2N2k_Mask                                              cBit15_0
#define cAf6_cdr_acr_timing_extref_prc_ctrl_Ext2N2k_Shift                                                    0

/*--------------------------------------
BitField Name: Ext1N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the first external reference signal,
default 8khz input signal
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_cdr_acr_timing_extref_prc_ctrl_Ext1N2k_Mask                                             cBit31_16
#define cAf6_cdr_acr_timing_extref_prc_ctrl_Ext1N2k_Shift                                                   16

/*--------------------------------------
BitField Name: Ocn1N2k
BitField Type: RW
BitField Desc: The 2Khz coefficient of the SONET interface signal, default 8khz
input signal
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_cdr_acr_timing_extref_prc_ctrl_Ocn1N2k_Mask                                              cBit15_0
#define cAf6_cdr_acr_timing_extref_prc_ctrl_Ocn1N2k_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : CDR ACR Engine Timing control
Reg Addr   : 0x0020800-0x0020BFF
Reg Formula: 0x0020800 + CHID
    Where  : 
           + $CHID(0-768):
Reg Desc   : 
This register is used to configure timing mode for per STS

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_acr_eng_timing_ctrl_Base                                                         0x0020800

/*--------------------------------------
BitField Name: cfgrelearn
BitField Type: RW
BitField Desc: Set 1 to enable output current value mode in ReLearn State.
Default is Hold value mode
BitField Bits: [30]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_cfgrelearn_Mask                                                    cBit30
#define cAf6_cdr_acr_eng_timing_ctrl_cfgrelearn_Shift                                                       30

/*--------------------------------------
BitField Name: cfglowp
BitField Type: RW
BitField Desc: Set 1 to select LOW performance mode
BitField Bits: [29]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_cfglowp_Mask                                                       cBit29
#define cAf6_cdr_acr_eng_timing_ctrl_cfglowp_Shift                                                          29

/*--------------------------------------
BitField Name: cfgdefdis
BitField Type: RW
BitField Desc: Set 1 to disable PLL go to Holdover state in Lbit packet
BitField Bits: [28]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_cfgdefdis_Mask                                                     cBit28
#define cAf6_cdr_acr_eng_timing_ctrl_cfgdefdis_Shift                                                        28

/*--------------------------------------
BitField Name: cfgpripll
BitField Type: RW
BitField Desc: Set 1 to select output PLL in lock state is PRI. Default is SEC
BitField Bits: [27]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_cfgpripll_Mask                                                     cBit27
#define cAf6_cdr_acr_eng_timing_ctrl_cfgpripll_Shift                                                        27

/*--------------------------------------
BitField Name: VC4_4c
BitField Type: RW
BitField Desc: VC4_4c/TU3 mode 0: STS1/VC4 1: TU3/VC4-4c
BitField Bits: [26]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_VC4_4c_Mask                                                        cBit26
#define cAf6_cdr_acr_eng_timing_ctrl_VC4_4c_Shift                                                           26

/*--------------------------------------
BitField Name: PktLen_ind
BitField Type: RW
BitField Desc: The payload packet  length for jumbo frame
BitField Bits: [25]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_ind_Mask                                                    cBit25
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_ind_Shift                                                       25

/*--------------------------------------
BitField Name: HoldValMode
BitField Type: RW
BitField Desc: Hold value mode of NCO, default value 0 0: Hardware calculated
and auto update 1: Software calculated and update, hardware is disabled
BitField Bits: [24]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_HoldValMode_Mask                                                   cBit24
#define cAf6_cdr_acr_eng_timing_ctrl_HoldValMode_Shift                                                      24

/*--------------------------------------
BitField Name: SeqMode
BitField Type: RW
BitField Desc: Sequence mode mode, default value 0 0: Wrap zero 1: Skip zero
BitField Bits: [23]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_SeqMode_Mask                                                       cBit23
#define cAf6_cdr_acr_eng_timing_ctrl_SeqMode_Shift                                                          23

/*--------------------------------------
BitField Name: LineType
BitField Type: RW
BitField Desc: Line type mode 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6:
STS1/TU3 7: VC4/VC4-4c
BitField Bits: [22:20]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_LineType_Mask                                                   cBit22_20
#define cAf6_cdr_acr_eng_timing_ctrl_LineType_Shift                                                         20

/*--------------------------------------
BitField Name: PktLen
BitField Type: RW
BitField Desc: The payload packet  length parameter to create a packet. SAToP
mode: The number payload of bit. CESoPSN mode: The number of bit which converted
to full DS1/E1 rate mode. In CESoPSN mode, the payload is assembled by NxDS0
with M frame, the value configured to this register is Mx256 bits for E1 mode,
Mx193 bits for DS1 mode.
BitField Bits: [19:4]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_Mask                                                      cBit19_4
#define cAf6_cdr_acr_eng_timing_ctrl_PktLen_Shift                                                            4

/*--------------------------------------
BitField Name: CDRTimeMode
BitField Type: RW
BitField Desc: CDR time mode 0: System mode 1: Loop timing mode, transparency
service Rx clock to service Tx clock 2: LIU timing mode, using service Rx clock
for CDR source to generate service Tx clock 3: Prc timing mode, using Prc clock
for CDR source to generate service Tx clock 4: Ext#1 timing mode, using Ext#1
clock for CDR source to generate service Tx clock 5: Ext#2 timing mode, using
Ext#2 clock for CDR source to generate service Tx clock 6: Tx SONET timing mode,
using Tx Line OCN clock for CDR source to generate service Tx clock 7: Free
timing mode, using system clock for CDR source to generate service Tx clock 8:
ACR timing mode 9: Reserve 10: DCR timing mode 11: Reserve 12: ACR Fast lock
mode
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_cdr_acr_eng_timing_ctrl_CDRTimeMode_Mask                                                  cBit3_0
#define cAf6_cdr_acr_eng_timing_ctrl_CDRTimeMode_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : CDR Adjust State status
Reg Addr   : 0x0021000-0x00213FF
Reg Formula: 0x0021000 + CHID
    Where  : 
           + $CHID(0-768):
Reg Desc   : 
This register is used to store status or configure  some parameter of per CDR engine

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_adj_state_stat_Base                                                              0x0021000
#define cAf6Reg_cdr_adj_state_stat_WidthVal                                                                 32

/*--------------------------------------
BitField Name: Adjstate
BitField Type: RW
BitField Desc: Adjust state 0: Load state 1: Wait state 2: Init state 3: Learn
State 4: ReLearn State 5: Lock State 6: ReInit State 7: Holdover State
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_cdr_adj_state_stat_Adjstate_Mask                                                          cBit2_0
#define cAf6_cdr_adj_state_stat_Adjstate_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : CDR Adjust Holdover value status
Reg Addr   : 0x0021800-0x0021BFF
Reg Formula: 0x0021800 + CHID
    Where  : 
           + $CHID(0-768):
Reg Desc   : 
This register is used to store status or configure  some parameter of per CDR engine

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_adj_holdover_value_stat_Base                                                     0x0021800

/*--------------------------------------
BitField Name: HoldVal
BitField Type: RW
BitField Desc: NCO Holdover value parameter E1RATE   = 32'd3817748707;
Resolution =    0.262 parameter DS1RATE  = 32'd3837632815;   Resolution =
0.260 parameter VT2RATE  = 32'd4175662648;   Resolution =    0.239 parameter
VT15RATE = 32'd4135894433;   Resolution =    0.241 parameter E3RATE   =
32'd2912117977;   Resolution =    0.343 parameter DS3RATE  = 32'd3790634015;
Resolution =    0.263 parameter TU3RATE  = 32'd4148547956;   Resolution =
0.239 parameter STS1RATE = 32'd4246160849;   Resolution =    0.239
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_adj_holdover_value_stat_HoldVal_Mask                                                 cBit31_0
#define cAf6_cdr_adj_holdover_value_stat_HoldVal_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCR TX Engine Active Control
Reg Addr   : 0x0010000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to activate the DCR TX Engine. Active high.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_tx_eng_active_ctrl_Base                                                          0x0010000

/*--------------------------------------
BitField Name: data
BitField Type: RW
BitField Desc: DCR TX Engine Active Control
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dcr_tx_eng_active_ctrl_data_Mask                                                         cBit31_0
#define cAf6_dcr_tx_eng_active_ctrl_data_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : DCR PRC Source Select Configuration
Reg Addr   : 0x0010001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure to select PRC source for PRC timer. The PRC clock selected must be less than 19.44 Mhz.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_prc_src_sel_cfg_Base                                                             0x0010001
#define cAf6Reg_dcr_prc_src_sel_cfg_WidthVal                                                                32

/*--------------------------------------
BitField Name: DcrPrcSourceSel
BitField Type: RW
BitField Desc: PRC source selection. 0: PRC Reference Clock 1: System Clock
19Mhz 2: External Reference Clock 1. 3: External Reference Clock 2. 4: Ocn Line
Clock Port 1 5: Ocn Line Clock Port 2 6: Ocn Line Clock Port 3 7: Ocn Line Clock
Port 4
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_Mask                                                  cBit2_0
#define cAf6_dcr_prc_src_sel_cfg_DcrPrcSourceSel_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCR PRC Frequency Configuration
Reg Addr   : 0x001000b
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure the frequency of PRC clock.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_prc_freq_cfg_Base                                                                0x001000b
#define cAf6Reg_dcr_prc_freq_cfg_WidthVal                                                                   32

/*--------------------------------------
BitField Name: DCRPrcFrequency
BitField Type: RW
BitField Desc: Frequency of PRC clock. This is used to configure the frequency
of PRC clock in Khz. Unit is Khz. Exp: The PRC clock is 19.44Mhz. This register
will be configured to 19440.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dcr_prc_freq_cfg_DCRPrcFrequency_Mask                                                    cBit15_0
#define cAf6_dcr_prc_freq_cfg_DCRPrcFrequency_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCR RTP Frequency Configuration
Reg Addr   : 0x001000C
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure the frequency of PRC clock.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_rtp_freq_cfg_Base                                                                0x001000C
#define cAf6Reg_dcr_rtp_freq_cfg_WidthVal                                                                   32

/*--------------------------------------
BitField Name: DCRRTPFreq
BitField Type: RW
BitField Desc: Frequency of RTP clock. This is used to configure the frequency
of RTP clock in Khz. Unit is Khz. Exp: The RTP clock is 19.44Mhz. This register
will be configured to 19440.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dcr_rtp_freq_cfg_DCRRTPFreq_Mask                                                         cBit15_0
#define cAf6_dcr_rtp_freq_cfg_DCRRTPFreq_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : DCR Tx Engine Timing control
Reg Addr   : 0x0010400
Reg Formula: 0x0010400 + CHID
    Where  : 
           + $CHID(0-768):
Reg Desc   : 
This register is used to configure timing mode for per STS

------------------------------------------------------------------------------*/
#define cAf6Reg_dcr_tx_eng_timing_ctrl_Base                                                          0x0010400

/*--------------------------------------
BitField Name: PktLen_bit
BitField Type: RW
BitField Desc: The payload packet length BIT, PktLen[2:0]
BitField Bits: [21:19]
--------------------------------------*/
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_bit_Mask                                                  cBit21_19
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_bit_Shift                                                        19

/*--------------------------------------
BitField Name: LineType
BitField Type: RW
BitField Desc: Line type mode 0: DS1 1: E1 2: DS3 3: E3 4: VT15 5: VT2 6:
STS1/TU3 7: Reserve
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_dcr_tx_eng_timing_ctrl_LineType_Mask                                                    cBit18_16
#define cAf6_dcr_tx_eng_timing_ctrl_LineType_Shift                                                          16

/*--------------------------------------
BitField Name: PktLen_byte
BitField Type: RW
BitField Desc: The payload packet length Byte, PktLen[17:3]
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_byte_Mask                                                  cBit15_0
#define cAf6_dcr_tx_eng_timing_ctrl_PktLen_byte_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Interrupt Enable Control
Reg Addr   : 0x00025000-0x000253FF
Reg Formula: 0x00025000 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-23): STS-1/VC-3 ID
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel interrupt enable of CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_intr_en_ctrl_Base                                                       0x00025000

/*--------------------------------------
BitField Name: CDRUnlokcedIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change UnLocked te event to generate an
interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_Mask                                             cBit0
#define cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Interrupt Status
Reg Addr   : 0x00025400-0x000257FF
Reg Formula: 0x00025400 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-23): STS-1/VC-3 ID
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel interrupt tus of CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_intr_stat_Base                                                          0x00025400

/*--------------------------------------
BitField Name: CDRUnLockedIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in UnLocked the event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_Mask                                                  cBit0
#define cAf6_cdr_per_chn_intr_stat_CDRUnLockedIntr_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Current Status
Reg Addr   : 0x00025800-0x00025BFF
Reg Formula: 0x00025800 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-23): STS-1/VC-3 ID
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel Current tus of CDR

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_curr_stat_Base                                                          0x00025800

/*--------------------------------------
BitField Name: CDRUnLockedCurrSta
BitField Type: RW
BitField Desc: Current tus of UnLocked event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_Mask                                               cBit0
#define cAf6_cdr_per_chn_curr_stat_CDRUnLockedCurrSta_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : CDR per Channel Interrupt OR Status
Reg Addr   : 0x00025C00-0x00025C1F
Reg Formula: 0x00025C00 +  StsID
    Where  : 
           + $StsID(0-23): STS-1/VC-3 ID
Reg Desc   : 
The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the CDR. Each bit is used to store Interrupt OR tus of the related DS1/E1.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_chn_intr_or_stat_Base                                                       0x00025C00

/*--------------------------------------
BitField Name: CDRVtIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding DS1/E1 is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_Mask                                             cBit31_0
#define cAf6_cdr_per_chn_intr_or_stat_CDRVtIntrOrSta_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CDR per STS/VC Interrupt OR Status
Reg Addr   : 0x00025FFF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 24 bits for 24 STS/VCs of the CDR. Each bit is used to store Interrupt OR tus of the related STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_stsvc_intr_or_stat_Base                                                     0x00025FFF

/*--------------------------------------
BitField Name: CDRStsIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding STS/VC is
set and its interrupt is enabled
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_Mask                                          cBit31_0
#define cAf6_cdr_per_stsvc_intr_or_stat_CDRStsIntrOrSta_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : CDR per STS/VC Interrupt Enable Control
Reg Addr   : 0x00025FFE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 24 interrupt enable bits for 24 STS/VCs in the Rx DS1/E1/J1 Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base                                                     0x00025FFE

/*--------------------------------------
BitField Name: CDRStsIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related STS/VC to generate interrupt.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_Mask                                             cBit31_0
#define cAf6_cdr_per_stsvc_intr_en_ctrl_CDRStsIntrEn_Shift                                                   0

#endif /* _AF6_REG_AF6CNC0061_RD_CDR_H_ */

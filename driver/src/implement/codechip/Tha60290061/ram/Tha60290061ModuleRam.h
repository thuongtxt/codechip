/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60290061ModuleRam.h
 * 
 * Created Date: Oct 15, 2018
 *
 * Description : Tha60290061 Module Ram interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061MODULERAM_H_
#define _THA60290061MODULERAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha60290061ModuleRamNew(AtDevice device);
uint32 Tha60290061ModuleRamNumParentClassInternalRam(AtModule self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061MODULERAM_H_ */


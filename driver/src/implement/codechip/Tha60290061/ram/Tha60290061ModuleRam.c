/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290061ModuleRam.c
 *
 * Created Date: Oct 15, 2018
 *
 * Description : 60290061 module RAM implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290061ModuleRamInternal.h"
#include "Tha60290061InternalRamEcc.h"
#include "Tha60290061ModuleRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tThaModuleRamMethods m_ThaModuleRamOverride;

/* Super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tThaModuleRamMethods *m_ThaModuleRamMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumParentClassInternalRam(AtModule self)
    {
    return m_AtModuleMethods->NumInternalRamsGet(self);
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    if (localRamId >= NumParentClassInternalRam(self))
        return Tha60290061InternalRamEccNew(self, ramId, localRamId);

    return m_AtModuleMethods->InternalRamCreate(self, ramId, localRamId);
    }

static uint32 NumInternalRamsGet(AtModule self)
    {
    uint32 numRams;
    numRams = NumParentClassInternalRam(self) + 4;
    return numRams;
    }

static AtErrorGenerator ErrorGeneratorObjectCreate(ThaModuleRam self, AtInternalRam ram)
    {
    uint32 localRamId = AtInternalRamLocalIdGet(ram);

    if (localRamId < NumParentClassInternalRam((AtModule)self))
        return m_ThaModuleRamMethods->ErrorGeneratorObjectCreate(self, ram);

    return NULL;
    }

static void OverrideAtModule(AtModuleRam self)
    {
    AtModule ramModule = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(ramModule);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(ramModule), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, NumInternalRamsGet);
        }

    mMethodsSet(ramModule, &m_AtModuleOverride);
    }

static void OverrideThaModuleRam(AtModuleRam self)
    {
    ThaModuleRam ramModule = (ThaModuleRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleRamMethods = mMethodsGet(ramModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleRamOverride, m_ThaModuleRamMethods, sizeof(m_ThaModuleRamOverride));

        mMethodOverride(m_ThaModuleRamOverride, ErrorGeneratorObjectCreate);
        }

    mMethodsSet(ramModule, &m_ThaModuleRamOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModule(self);
    OverrideThaModuleRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290061ModuleRam);
    }

static AtModuleRam ObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60291011ModuleRamObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam Tha60290061ModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

uint32 Tha60290061ModuleRamNumParentClassInternalRam(AtModule self)
    {
    return NumParentClassInternalRam(self);
    }

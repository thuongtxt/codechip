/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60290061ModuleRamInternal.h
 * 
 * Created Date: Oct 15, 2018
 *
 * Description : 60290061 module RAM internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061MODULERAMINTERNAL_H_
#define _THA60290061MODULERAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60291011/ram/Tha60291011ModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290061ModuleRam
    {
    tTha60291011ModuleRam super;
    } tTha60290061ModuleRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290061MODULERAMINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60290061InternalRamEcc.h
 * 
 * Created Date: Oct 15, 2018
 *
 * Description : 60290061 Internal RAM ECC interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061INTERNALRAMECC_H_
#define _THA60290061INTERNALRAMECC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInternalRam Tha60290061InternalRamEccNew(AtModule phyModule, uint32 ramId, uint32 localId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290061INTERNALRAMECC_H_ */


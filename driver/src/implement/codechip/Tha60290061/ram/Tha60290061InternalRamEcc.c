/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290061InternalRamEcc.c
 *
 * Created Date: Oct 15, 2018
 *
 * Description : 60290061 Internal RAM ECC implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60290061InternalRamInternal.h"
#include "../man/Tha60290061DeviceDdrOffloadReg.h"
#include "../man/Tha60290061Device.h"
#include "../man/Tha60290061DeviceDdrOffload.h"
#include "../man/Tha60290061SubDevice.h"
#include "Tha60290061ModuleRam.h"
#include "Tha60290061InternalRamEcc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaInternalRamMethods    m_ThaInternalRamOverride;
static tAtInternalRamMethods  m_AtInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet ErrorMonitorEnable(ThaInternalRam self, eBool enable)
    {
    AtUnused(self);

    if (enable)
        return cAtOk;
    else
        return cAtErrorModeNotSupport;
    }

static eBool ErrorMonitorIsEnabled(ThaInternalRam self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool EccMonitorCanEnable(AtInternalRam self, eBool enable)
    {
    AtUnused(self);
    return enable;
    }

static uint32 ForcedCorrectableMask(ThaInternalRam self)
    {
    AtUnused(self);
    return cBit0;
    }

static uint32 ForcedUncorrectableMask(ThaInternalRam self)
    {
    AtUnused(self);
    return cBit2_1;
    }

static uint32 LocalIndex(ThaInternalRam self)
    {
    AtModuleRam module = AtInternalRamModuleRamGet((AtInternalRam)self);
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    uint32 localIndex = (uint32)(localId - Tha60290061ModuleRamNumParentClassInternalRam((AtModule)module));

    return localIndex;
    }

static AtDevice Device(ThaInternalRam self)
    {
    AtModuleRam module = AtInternalRamModuleRamGet((AtInternalRam)self);
    return AtModuleDeviceGet((AtModule)module);
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    uint32 laneId = LocalIndex(self);
    uint32 address = Tha60290061DeviceDdrOffloadBaseAddress(Device(self)) + cAf6Reg_upen_force_err_ecc_lane0_Base + laneId;
    return address;
    }

static eAtRet ForcingTrigger(ThaInternalRam self)
    {
    uint32 address = 0xA00000;/*"OCN Global Rx Framer Master Control"*/
    AtDevice parent = NULL, device = Device(self);
    uint32 regVal = 0;
    uint32 subDeviceId = 0;

    if (AtDeviceParentDeviceGet(device) == NULL)
        {
        parent = device;
        subDeviceId = 0;
        }
    else
        {
        parent = AtDeviceParentDeviceGet(device);
        subDeviceId = (uint32)Tha60290061SubDeviceIdGet(device);
        }

    regVal = Tha60290061DeviceDdrOffloadRead(parent, subDeviceId, address);
    Tha60290061DeviceDdrOffloadWrite(parent, subDeviceId, address, regVal);
    Tha60290061DeviceDdrOffloadRead(parent, subDeviceId, address);
    return cAtOk;
    }

static eAtRet HwForcedErrorSet(ThaInternalRam self, uint32 error, eBool enable)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = ErrorForceRegister(self);
    uint32 regVal = 0, regMask = 0;

    if (error & cAtRamAlarmEccCorrectable)
        regMask |= ForcedCorrectableMask(self);

    if (error & cAtRamAlarmEccUnnorrectable)
        regMask |= ForcedUncorrectableMask(self);

    regVal = mModuleHwRead(module, regAddr);
    regVal = enable ? (regVal | regMask) : (regVal & ~regMask);
    mModuleHwWrite(module, regAddr, regVal);

    return ForcingTrigger(self);
    }

static uint32 HwForcedErrorGet(ThaInternalRam self)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = ErrorForceRegister(self);
    uint32 regVal = 0, errors = 0;

    regVal = mModuleHwRead(module, regAddr);

    if (regVal & ForcedCorrectableMask(self))
        errors |= cAtRamAlarmEccCorrectable;

    if (regVal & ForcedUncorrectableMask(self))
        errors |= cAtRamAlarmEccUnnorrectable;

    return errors;
    }

static eBool CounterTypeIsSupported(AtInternalRam self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return cAtFalse;
    }

static uint32 StickyCorrectableMask(ThaInternalRam self)
    {
    uint32 localIndex = LocalIndex(self);
    uint32 ret = (cBit0 << (localIndex + cAf6_upen_stk_ecc_ecc_err_corr_Shift));
    return ret;
    }

static uint32 StickyUncorrectableMask(ThaInternalRam self)
    {
    uint32 localIndex = LocalIndex(self);
    uint32 ret = (cBit0 << (localIndex + cAf6_upen_stk_ecc_ecc_err_fatal_Shift));
    return ret;
    }

static uint32 ErrorStickyRegister(ThaInternalRam self)
    {
    uint32 address = Tha60290061DeviceDdrOffloadBaseAddress(Device(self)) + cAf6Reg_upen_stk_ecc_Base;
    return address;
    }

static uint32 ErrorRead2Clear(ThaInternalRam self, eBool clear)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = ErrorStickyRegister(self);
    uint32 regVal  = mModuleHwRead(module, regAddr);
    uint32 errors = 0;
    uint32 regCorrMask = StickyCorrectableMask(self);
    uint32 regUncorrMask = StickyUncorrectableMask(self);

    if (clear)
        mModuleHwWrite(module, regAddr, regCorrMask|regUncorrMask);

    if (regVal & regCorrMask)
        errors |= cAtRamAlarmEccCorrectable;

    if (regVal & regUncorrMask)
        errors |= cAtRamAlarmEccUnnorrectable;

    return errors;
    }

static const char* Name(AtInternalRam self)
    {
    static const char * description[] =
        {
         "DDR for Offload Lane#0",
         "DDR for Offload Lane#1",
         "DDR for Offload Lane#2",
         "DDR for Offload Lane#3"
        };
    uint32 localIndex = LocalIndex((ThaInternalRam)self);

    return (const char*) description[localIndex];
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290061InternalRamEcc);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        /* Common */
        mMethodOverride(m_ThaInternalRamOverride, ErrorMonitorEnable);
        mMethodOverride(m_ThaInternalRamOverride, ErrorMonitorIsEnabled);
        mMethodOverride(m_ThaInternalRamOverride, HwForcedErrorSet);
        mMethodOverride(m_ThaInternalRamOverride, HwForcedErrorGet);
        mMethodOverride(m_ThaInternalRamOverride, ErrorRead2Clear);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, CounterTypeIsSupported);
        mMethodOverride(m_AtInternalRamOverride, Name);
        mMethodOverride(m_AtInternalRamOverride, EccMonitorCanEnable);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    OverrideAtInternalRam(self);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031InternalRamEccObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60290061InternalRamEccNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60290061InternalRamInternal.h
 * 
 * Created Date: Oct 15, 2018
 *
 * Description : 60290061 Internal RAM Internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290061INTERNALRAMINTERNAL_H_
#define _THA60290061INTERNALRAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/ram/Tha60210031InternalRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290061InternalRamEcc
    {
    tTha60210031InternalRamEcc super;
    }tTha60290061InternalRamEcc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290061INTERNALRAMINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210021ModuleEth.c
 *
 * Created Date: May 20, 2015
 *
 * Description : ETH Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../../../default/sur/hard/ThaModuleHardSurInternal.h"
#include "../../../default/sur/hard/engine/ThaHardSurEngineInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "Tha60210021ModuleSur.h"
#include "Tha60210021ModuleSurReg.h"
#include "../man/Tha60210021DeviceReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210021ModuleEth *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021ModuleSur
    {
    tThaModuleHardSur super;
    }tTha60210021ModuleSur;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods          m_AtModuleOverride;
static tAtModuleSurMethods       m_AtModuleSurOverride;
static tThaModuleHardSurMethods  m_ThaModuleHardSurOverride;

static const tAtModuleMethods    *m_AtModuleMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *CapacityDescription(AtModule self)
    {
    AtUnused(self);
    return "engine: pdh_de1, pw;tca_thres:8; k_thres:8;";
    }

static AtSurEngine SdhLineSurEngineObjectCreate(ThaModuleHardSur self, AtChannel line)
    {
    AtUnused(self);
    AtUnused(line);
    return NULL;
    }

static AtSurEngine SdhHoPathEngineObjectCreate(ThaModuleHardSur self, AtChannel path)
    {
    AtUnused(self);
    AtUnused(path);
    return NULL;
    }

static AtSurEngine SdhLoPathEngineObjectCreate(ThaModuleHardSur self, AtChannel path)
    {
    AtUnused(self);
    AtUnused(path);
    return NULL;
    }

static AtSurEngine PdhDe3EngineObjectCreate(ThaModuleHardSur self, AtChannel de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return NULL;
    }

static AtSurEngine PdhDe1EngineObjectCreate(ThaModuleHardSur self, AtChannel de1)
    {
    return Tha60210021SurEnginePdhDe1New((AtModuleSur)self, de1);
    }

static eBool IsLoPw(ThaModuleHardSur self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtTrue;
    }

static uint32 BaseAddress(ThaModuleHardSur self)
    {
    AtUnused(self);
    return 0x900000UL;
    }

static uint32 IndirectDe1AddressShift(ThaModuleHardSur self)
    {
    AtUnused(self);
    return 5;
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    AtUnused(managerIndex);
    return Tha60210021SurInterruptManagerNew(self);
    }

static uint32 StartVersionChangeTo112dot50MhzClock(AtDevice device)
    {
    /* Initial version uses system clock of 116.64Mhz. Since this version, FPGA
     * use system clock of 112.50Mhz. */
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x16, 0x04, 0x05, 0x46);
    }

static uint32 Clock125usDefaultValue(ThaModuleHardSur self)
    {
    const uint32 Clock116dot64Mhz = 0x38F2;
    const uint32 Clock112dot50Mhz = 0x36ED;
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceVersionNumber(device) >= StartVersionChangeTo112dot50MhzClock(device))
        return Clock112dot50Mhz;
    else
        return Clock116dot64Mhz;
    }

static eBool LineIsSupported(AtModuleSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool FailureTimersConfigurable(AtModuleSur self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4644);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool FailureForwardingStatusIsSupported(ThaModuleHardSur self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4647);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool FailureHoldOffTimerConfigurable(AtModuleSur self)
    {
    return FailureTimersConfigurable(self);
    }

static eBool FailureHoldOnTimerConfigurable(AtModuleSur self)
    {
    return FailureTimersConfigurable(self);
    }

static void OverrideAtModuleSur(AtModuleSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSurOverride, mMethodsGet(self), sizeof(m_AtModuleSurOverride));

        mMethodOverride(m_AtModuleSurOverride, LineIsSupported);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOffTimerConfigurable);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOnTimerConfigurable);
        }

    mMethodsSet(self, &m_AtModuleSurOverride);
    }

static void OverrideAtModule(AtModuleSur self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideThaModuleHardSur(AtModuleSur self)
    {
    ThaModuleHardSur ethModule = (ThaModuleHardSur)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardSurOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleHardSurOverride));

        mMethodOverride(m_ThaModuleHardSurOverride, BaseAddress);
        mMethodOverride(m_ThaModuleHardSurOverride, SdhLineSurEngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, SdhHoPathEngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, SdhLoPathEngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, PdhDe1EngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, PdhDe3EngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, IsLoPw);
        mMethodOverride(m_ThaModuleHardSurOverride, BaseAddress);
        mMethodOverride(m_ThaModuleHardSurOverride, IndirectDe1AddressShift);
        mMethodOverride(m_ThaModuleHardSurOverride, Clock125usDefaultValue);
        mMethodOverride(m_ThaModuleHardSurOverride, FailureForwardingStatusIsSupported);
        }

    mMethodsSet(ethModule, &m_ThaModuleHardSurOverride);
    }

static void Override(AtModuleSur self)
    {
    OverrideAtModule(self);
    OverrideAtModuleSur(self);
    OverrideThaModuleHardSur(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModuleSur);
    }

static AtModuleSur ObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleHardSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSur Tha60210021ModuleSurNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60210021SurInterruptManager.c
 *
 * Created Date: Dec 30, 2015
 *
 * Description : Surveillance interrupt manager for product 60210021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/sur/hard/ThaSurInterruptManagerInternal.h"
#include "../man/Tha60210021DeviceReg.h"
#include "Tha60210021ModuleSur.h"
#include "Tha60210021ModuleSurReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021SurInterruptManager
    {
    tThaSurInterruptManager super;
    }tTha60210021SurInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods       m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods      m_ThaInterruptManagerOverride;
static tThaSurInterruptManagerMethods   m_ThaSurInterruptManagerOverride;

/* Save superclass implementation. */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static AtDevice DeviceGet(ThaSurInterruptManager self)
    {
    return AtModuleDeviceGet(AtInterruptManagerModuleGet((AtInterruptManager)self));
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cAf6_inten_status_FMIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 2;
    }

static AtModulePdh PdhModule(ThaSurInterruptManager self)
    {
    return (AtModulePdh)AtDeviceModuleGet(DeviceGet(self), cAtModulePdh);
    }

static AtModulePw PwModule(ThaSurInterruptManager self)
    {
    return (AtModulePw)AtDeviceModuleGet(DeviceGet(self), cAtModulePw);
    }

static void De1InterruptProcess(ThaSurInterruptManager self, AtHal hal)
    {
    const uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self));
    uint32 intrOR = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_DS1E1_Group_Interrupt_OR);
    AtModulePdh pdhModule = PdhModule(self);
    uint32 numSlices = ThaInterruptManagerNumSlices(mIntrManager(self));
    uint32 slice;

    for (slice = 0; slice < numSlices; slice++)
        {
        if (intrOR & cIteratorMask(slice))
            {
            uint32 intrSts24Val = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_Base + slice);
            uint32 de1;

            for (de1 = 0; de1 < 24; de1++)
                {
                if (intrSts24Val & cIteratorMask(de1))
                    {
                    AtPdhDe1 de1Channel = AtModulePdhDe1Get(pdhModule, (slice * 24 + de1));
                    uint32 offset = (slice << 5) | de1;

                    /* Execute path interrupt process */
                    AtSurEngineNotificationProcess(AtChannelSurEngineGet((AtChannel)de1Channel), offset);
                    }
                }
            }
        }
    }

static uint32 NumPwGroups(ThaSurInterruptManager self)
    {
    AtUnused(self);
    return 6;
    }

static void PwInterruptProcess(ThaSurInterruptManager self, AtHal hal)
    {
    const uint32 baseAddress = ThaInterruptManagerBaseAddress(mIntrManager(self));
    uint32 intrOR = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_PW_Group_Interrupt_OR);
    AtModulePw pwModule = PwModule(self);
    uint32 numGroup = NumPwGroups(self);
    uint32 grpid;

    for (grpid = 0; grpid < numGroup; grpid++)
        {
        if (intrOR & cIteratorMask(grpid))
            {
            uint32 intrStat32 = AtHalRead(hal, baseAddress + cAf6Reg_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW(grpid));
            uint32 subid;

            for (subid = 0; subid < 32; subid++)
                {
                if (intrStat32 & cIteratorMask(subid))
                    {
                    uint32 pwid = (grpid << 5) + subid;

                    /* Get PW channel */
                    AtPw pwChannel = AtModulePwGetPw(pwModule, pwid);

                    /* Process channel interrupt. */
                    AtSurEngineNotificationProcess(AtChannelSurEngineGet((AtChannel)pwChannel), pwid);
                    }
                }
            }
        }
    }

static eBool LineHasInterrupt(ThaSurInterruptManager self, uint32 glbIntr)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    return cAtFalse;
    }

static eBool StsHasInterrupt(ThaSurInterruptManager self, uint32 glbIntr)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    return cAtFalse;
    }

static eBool VtHasInterrupt(ThaSurInterruptManager self, uint32 glbIntr)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    return cAtFalse;
    }

static eBool De3HasInterrupt(ThaSurInterruptManager self, uint32 glbIntr)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    return cAtFalse;
    }

static uint32 InterruptMask(ThaSurInterruptManager self)
    {
    AtUnused(self);
    return (cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS1E1_Mask  |
            cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_PW_Mask);
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideThaSurInterruptManager(AtInterruptManager self)
    {
    ThaSurInterruptManager manager = (ThaSurInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSurInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaSurInterruptManagerOverride));

        mMethodOverride(m_ThaSurInterruptManagerOverride, De1InterruptProcess);
        mMethodOverride(m_ThaSurInterruptManagerOverride, PwInterruptProcess);
        mMethodOverride(m_ThaSurInterruptManagerOverride, LineHasInterrupt);
        mMethodOverride(m_ThaSurInterruptManagerOverride, StsHasInterrupt);
        mMethodOverride(m_ThaSurInterruptManagerOverride, VtHasInterrupt);
        mMethodOverride(m_ThaSurInterruptManagerOverride, De3HasInterrupt);
        mMethodOverride(m_ThaSurInterruptManagerOverride, InterruptMask);
        }

    mMethodsSet(manager, &m_ThaSurInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    OverrideThaSurInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021SurInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSurInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60210021SurInterruptManagerNew(AtModule module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newInterruptManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newInterruptManager == NULL)
        return NULL;

    return ObjectInit(newInterruptManager, module);
    }

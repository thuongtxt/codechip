/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SUR
 *
 * File        : Tha60210021ModuleSur.h
 *
 * Created Date: Sep 8, 2015
 *
 * Description : SUR module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021MODULESUR_H_
#define _THA60210021MODULESUR_H_

/*--------------------------- Include files ----------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSurEngine Tha60210021SurEnginePdhDe1New(AtModuleSur module, AtChannel channel);

AtInterruptManager Tha60210021SurInterruptManagerNew(AtModule module);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210021MODULESUR_H_ */

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0021_RD_PM_H_
#define _AF6_REG_AF6CCI0021_RD_PM_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : FMPM Block Version
Reg Addr   : 0x00000
Reg Formula: 
    Where  : 
Reg Desc   : 
PM Block Version

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Block_Version_Base                                                                0x00000
#define cAf6Reg_FMPM_Block_Version                                                                     0x00000
#define cAf6Reg_FMPM_Block_Version_WidthVal                                                                 32
#define cAf6Reg_FMPM_Block_Version_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: day
BitField Type: R_O
BitField Desc: day
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_FMPM_Block_Version_day_Bit_Start                                                               24
#define cAf6_FMPM_Block_Version_day_Bit_End                                                                 31
#define cAf6_FMPM_Block_Version_day_Mask                                                             cBit31_24
#define cAf6_FMPM_Block_Version_day_Shift                                                                   24
#define cAf6_FMPM_Block_Version_day_MaxVal                                                                0xff
#define cAf6_FMPM_Block_Version_day_MinVal                                                                 0x0
#define cAf6_FMPM_Block_Version_day_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: month
BitField Type: R_O
BitField Desc: month
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_FMPM_Block_Version_month_Bit_Start                                                             20
#define cAf6_FMPM_Block_Version_month_Bit_End                                                               23
#define cAf6_FMPM_Block_Version_month_Mask                                                           cBit23_20
#define cAf6_FMPM_Block_Version_month_Shift                                                                 20
#define cAf6_FMPM_Block_Version_month_MaxVal                                                               0xf
#define cAf6_FMPM_Block_Version_month_MinVal                                                               0x0
#define cAf6_FMPM_Block_Version_month_RstVal                                                               0x0

/*--------------------------------------
BitField Name: year
BitField Type: R_O
BitField Desc: year
BitField Bits: [19:12]
--------------------------------------*/
#define cAf6_FMPM_Block_Version_year_Bit_Start                                                              12
#define cAf6_FMPM_Block_Version_year_Bit_End                                                                19
#define cAf6_FMPM_Block_Version_year_Mask                                                            cBit19_12
#define cAf6_FMPM_Block_Version_year_Shift                                                                  12
#define cAf6_FMPM_Block_Version_year_MaxVal                                                               0xff
#define cAf6_FMPM_Block_Version_year_MinVal                                                                0x0
#define cAf6_FMPM_Block_Version_year_RstVal                                                                0x0

/*--------------------------------------
BitField Name: number
BitField Type: R_O
BitField Desc: number
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_FMPM_Block_Version_number_Bit_Start                                                             0
#define cAf6_FMPM_Block_Version_number_Bit_End                                                              11
#define cAf6_FMPM_Block_Version_number_Mask                                                           cBit11_0
#define cAf6_FMPM_Block_Version_number_Shift                                                                 0
#define cAf6_FMPM_Block_Version_number_MaxVal                                                            0xfff
#define cAf6_FMPM_Block_Version_number_MinVal                                                              0x0
#define cAf6_FMPM_Block_Version_number_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Read DDR Control
Reg Addr   : 0x00001
Reg Formula: 
    Where  : 
Reg Desc   : 
Read Parameters or Statistic counters of DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Read_DDR_Control_Base                                                             0x00001
#define cAf6Reg_FMPM_Read_DDR_Control                                                                  0x00001
#define cAf6Reg_FMPM_Read_DDR_Control_WidthVal                                                              32
#define cAf6Reg_FMPM_Read_DDR_Control_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: Done
BitField Type: R/W/C
BitField Desc: Access DDR Is Done
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_FMPM_Read_DDR_Control_Done_Bit_Start                                                           31
#define cAf6_FMPM_Read_DDR_Control_Done_Bit_End                                                             31
#define cAf6_FMPM_Read_DDR_Control_Done_Mask                                                            cBit31
#define cAf6_FMPM_Read_DDR_Control_Done_Shift                                                               31
#define cAf6_FMPM_Read_DDR_Control_Done_MaxVal                                                             0x1
#define cAf6_FMPM_Read_DDR_Control_Done_MinVal                                                             0x0
#define cAf6_FMPM_Read_DDR_Control_Done_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Start
BitField Type: R/W
BitField Desc: Trigger 0->1 to start access DDR
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_FMPM_Read_DDR_Control_Start_Bit_Start                                                          30
#define cAf6_FMPM_Read_DDR_Control_Start_Bit_End                                                            30
#define cAf6_FMPM_Read_DDR_Control_Start_Mask                                                           cBit30
#define cAf6_FMPM_Read_DDR_Control_Start_Shift                                                              30
#define cAf6_FMPM_Read_DDR_Control_Start_MaxVal                                                            0x1
#define cAf6_FMPM_Read_DDR_Control_Start_MinVal                                                            0x0
#define cAf6_FMPM_Read_DDR_Control_Start_RstVal                                                            0x0

/*--------------------------------------
BitField Name: Page
BitField Type: R/W
BitField Desc: Page access
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_FMPM_Read_DDR_Control_Page_Bit_Start                                                           29
#define cAf6_FMPM_Read_DDR_Control_Page_Bit_End                                                             29
#define cAf6_FMPM_Read_DDR_Control_Page_Mask                                                            cBit29
#define cAf6_FMPM_Read_DDR_Control_Page_Shift                                                               29
#define cAf6_FMPM_Read_DDR_Control_Page_MaxVal                                                             0x1
#define cAf6_FMPM_Read_DDR_Control_Page_MinVal                                                             0x0
#define cAf6_FMPM_Read_DDR_Control_Page_RstVal                                                             0x0

/*--------------------------------------
BitField Name: R2C
BitField Type: R/W
BitField Desc: Read mode
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_FMPM_Read_DDR_Control_R2C_Bit_Start                                                            28
#define cAf6_FMPM_Read_DDR_Control_R2C_Bit_End                                                              28
#define cAf6_FMPM_Read_DDR_Control_R2C_Mask                                                             cBit28
#define cAf6_FMPM_Read_DDR_Control_R2C_Shift                                                                28
#define cAf6_FMPM_Read_DDR_Control_R2C_MaxVal                                                              0x1
#define cAf6_FMPM_Read_DDR_Control_R2C_MinVal                                                              0x0
#define cAf6_FMPM_Read_DDR_Control_R2C_RstVal                                                              0x0

/*--------------------------------------
BitField Name: Type
BitField Type: R/W
BitField Desc: Counter Type
BitField Bits: [27:20]
--------------------------------------*/
#define cAf6_FMPM_Read_DDR_Control_Type_Bit_Start                                                           20
#define cAf6_FMPM_Read_DDR_Control_Type_Bit_End                                                             27
#define cAf6_FMPM_Read_DDR_Control_Type_Mask                                                         cBit27_20
#define cAf6_FMPM_Read_DDR_Control_Type_Shift                                                               20
#define cAf6_FMPM_Read_DDR_Control_Type_MaxVal                                                            0xff
#define cAf6_FMPM_Read_DDR_Control_Type_MinVal                                                             0x0
#define cAf6_FMPM_Read_DDR_Control_Type_RstVal                                                             0x0

/*--------------------------------------
BitField Name: ADR
BitField Type: R/W
BitField Desc: Address of Counter
BitField Bits: [13:00]
--------------------------------------*/
#define cAf6_FMPM_Read_DDR_Control_ADR_Bit_Start                                                             0
#define cAf6_FMPM_Read_DDR_Control_ADR_Bit_End                                                              13
#define cAf6_FMPM_Read_DDR_Control_ADR_Mask                                                           cBit13_0
#define cAf6_FMPM_Read_DDR_Control_ADR_Shift                                                                 0
#define cAf6_FMPM_Read_DDR_Control_ADR_MaxVal                                                           0x3fff
#define cAf6_FMPM_Read_DDR_Control_ADR_MinVal                                                              0x0
#define cAf6_FMPM_Read_DDR_Control_ADR_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Change Page DDR
Reg Addr   : 0x00002
Reg Formula: 
    Where  : 
Reg Desc   : 
Change Page for DDR when SW want to get Current Period Parameters

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Change_Page_DDR_Base                                                              0x00002
#define cAf6Reg_FMPM_Change_Page_DDR                                                                   0x00002
#define cAf6Reg_FMPM_Change_Page_DDR_WidthVal                                                               32
#define cAf6Reg_FMPM_Change_Page_DDR_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: HW_Page
BitField Type: R_O
BitField Desc: Get Current Page of DDR
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_FMPM_Change_Page_DDR_HW_Page_Bit_Start                                                          8
#define cAf6_FMPM_Change_Page_DDR_HW_Page_Bit_End                                                            8
#define cAf6_FMPM_Change_Page_DDR_HW_Page_Mask                                                           cBit8
#define cAf6_FMPM_Change_Page_DDR_HW_Page_Shift                                                              8
#define cAf6_FMPM_Change_Page_DDR_HW_Page_MaxVal                                                           0x1
#define cAf6_FMPM_Change_Page_DDR_HW_Page_MinVal                                                           0x0
#define cAf6_FMPM_Change_Page_DDR_HW_Page_RstVal                                                           0x0

/*--------------------------------------
BitField Name: CHG_DONE
BitField Type: R/W/C
BitField Desc: Change page is done
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_Change_Page_DDR_CHG_DONE_Bit_Start                                                         4
#define cAf6_FMPM_Change_Page_DDR_CHG_DONE_Bit_End                                                           4
#define cAf6_FMPM_Change_Page_DDR_CHG_DONE_Mask                                                          cBit4
#define cAf6_FMPM_Change_Page_DDR_CHG_DONE_Shift                                                             4
#define cAf6_FMPM_Change_Page_DDR_CHG_DONE_MaxVal                                                          0x1
#define cAf6_FMPM_Change_Page_DDR_CHG_DONE_MinVal                                                          0x0
#define cAf6_FMPM_Change_Page_DDR_CHG_DONE_RstVal                                                          0x0

/*--------------------------------------
BitField Name: CHG_START
BitField Type: R/W
BitField Desc: Trigger 0->1 to request change page
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Change_Page_DDR_CHG_START_Bit_Start                                                        0
#define cAf6_FMPM_Change_Page_DDR_CHG_START_Bit_End                                                          0
#define cAf6_FMPM_Change_Page_DDR_CHG_START_Mask                                                         cBit0
#define cAf6_FMPM_Change_Page_DDR_CHG_START_Shift                                                            0
#define cAf6_FMPM_Change_Page_DDR_CHG_START_MaxVal                                                         0x1
#define cAf6_FMPM_Change_Page_DDR_CHG_START_MinVal                                                         0x0
#define cAf6_FMPM_Change_Page_DDR_CHG_START_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Config Parity
Reg Addr   : 0x00003
Reg Formula: 
    Where  : 
Reg Desc   : 
Change Page for DDR when SW want to get Current Period Parameters

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_config_Parity_Base                                                                0x00003
#define cAf6Reg_FMPM_config_Parity                                                                     0x00003
#define cAf6Reg_FMPM_config_Parity_WidthVal                                                                 32
#define cAf6Reg_FMPM_config_Parity_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: Par_Stk_pw_def_msk_chn
BitField Type: W1C
BitField Desc: FMPM PW Def Framer Interrupt MASK Per PW
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_chn_Bit_Start                                            31
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_chn_Bit_End                                              31
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_chn_Mask                                             cBit31
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_chn_Shift                                                31
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_chn_MaxVal                                              0x1
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_chn_MinVal                                              0x0
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_chn_RstVal                                              0x0

/*--------------------------------------
BitField Name: Par_Stk_pw_def_msk_typ
BitField Type: W1C
BitField Desc: FMPM PW Def Interrupt MASK Per Type Per PW
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_typ_Bit_Start                                            30
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_typ_Bit_End                                              30
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_typ_Mask                                             cBit30
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_typ_Shift                                                30
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_typ_MaxVal                                              0x1
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_typ_MinVal                                              0x0
#define cAf6_FMPM_config_Parity_Par_Stk_pw_def_msk_typ_RstVal                                              0x0

/*--------------------------------------
BitField Name: Dis_par
BitField Type: R/W
BitField Desc: Disable parity check
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_config_Parity_Dis_par_Bit_Start                                                            1
#define cAf6_FMPM_config_Parity_Dis_par_Bit_End                                                              1
#define cAf6_FMPM_config_Parity_Dis_par_Mask                                                             cBit1
#define cAf6_FMPM_config_Parity_Dis_par_Shift                                                                1
#define cAf6_FMPM_config_Parity_Dis_par_MaxVal                                                             0x1
#define cAf6_FMPM_config_Parity_Dis_par_MinVal                                                             0x0
#define cAf6_FMPM_config_Parity_Dis_par_RstVal                                                             0x0

/*--------------------------------------
BitField Name: FORCE_PAR
BitField Type: R/W
BitField Desc: Force error for Parity
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_config_Parity_FORCE_PAR_Bit_Start                                                          0
#define cAf6_FMPM_config_Parity_FORCE_PAR_Bit_End                                                            0
#define cAf6_FMPM_config_Parity_FORCE_PAR_Mask                                                           cBit0
#define cAf6_FMPM_config_Parity_FORCE_PAR_Shift                                                              0
#define cAf6_FMPM_config_Parity_FORCE_PAR_MaxVal                                                           0x1
#define cAf6_FMPM_config_Parity_FORCE_PAR_MinVal                                                           0x0
#define cAf6_FMPM_config_Parity_FORCE_PAR_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Parity Sticky
Reg Addr   : 0x00004
Reg Formula: 
    Where  : 
Reg Desc   : 
Change Page for DDR when SW want to get Current Period Parameters

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Pariry_Sticky_Base                                                                0x00004
#define cAf6Reg_FMPM_Pariry_Sticky                                                                     0x00004
#define cAf6Reg_FMPM_Pariry_Sticky_WidthVal                                                                 32
#define cAf6Reg_FMPM_Pariry_Sticky_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: Par_Stk_pm_ds1_msk_chn
BitField Type: W1C
BitField Desc: FMPM PM DS1E1 Framer Interrupt MASK Per DS1E1
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_chn_Bit_Start                                            27
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_chn_Bit_End                                              27
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_chn_Mask                                             cBit27
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_chn_Shift                                                27
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_chn_MaxVal                                              0x1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_chn_MinVal                                              0x0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_chn_RstVal                                              0x0

/*--------------------------------------
BitField Name: Par_Stk_pm_pw_msk_chn
BitField Type: W1C
BitField Desc: FMPM PM PW Framer Interrupt MASK Per PW
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_chn_Bit_Start                                             26
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_chn_Bit_End                                               26
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_chn_Mask                                              cBit26
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_chn_Shift                                                 26
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_chn_MaxVal                                               0x1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_chn_MinVal                                               0x0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_chn_RstVal                                               0x0

/*--------------------------------------
BitField Name: Par_Stk_pm_ds1_msk_typ
BitField Type: W1C
BitField Desc: FMPM PM DS1E1 Interrupt MASK Per Type Per DS1E1
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_typ_Bit_Start                                            22
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_typ_Bit_End                                              22
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_typ_Mask                                             cBit22
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_typ_Shift                                                22
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_typ_MaxVal                                              0x1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_typ_MinVal                                              0x0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_ds1_msk_typ_RstVal                                              0x0

/*--------------------------------------
BitField Name: Par_Stk_pm_pw_msk_typ
BitField Type: W1C
BitField Desc: FMPM PM PW Interrupt MASK Per Type Per PW
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_typ_Bit_Start                                             21
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_typ_Bit_End                                               21
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_typ_Mask                                              cBit21
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_typ_Shift                                                 21
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_typ_MaxVal                                               0x1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_typ_MinVal                                               0x0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pm_pw_msk_typ_RstVal                                               0x0

/*--------------------------------------
BitField Name: Par_Stk_ds1_msk_chn
BitField Type: W1C
BitField Desc: FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_chn_Bit_Start                                               17
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_chn_Bit_End                                                 17
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_chn_Mask                                                cBit17
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_chn_Shift                                                   17
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_chn_MaxVal                                                 0x1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_chn_MinVal                                                 0x0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_chn_RstVal                                                 0x0

/*--------------------------------------
BitField Name: Par_Stk_pw_msk_chn
BitField Type: W1C
BitField Desc: FMPM FM PW Framer Interrupt MASK Per PW
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_chn_Bit_Start                                                16
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_chn_Bit_End                                                  16
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_chn_Mask                                                 cBit16
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_chn_Shift                                                    16
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_chn_MaxVal                                                  0x1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_chn_MinVal                                                  0x0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_chn_RstVal                                                  0x0

/*--------------------------------------
BitField Name: Par_Stk_ds1_msk_typ
BitField Type: W1C
BitField Desc: FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_typ_Bit_Start                                               12
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_typ_Bit_End                                                 12
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_typ_Mask                                                cBit12
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_typ_Shift                                                   12
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_typ_MaxVal                                                 0x1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_typ_MinVal                                                 0x0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_ds1_msk_typ_RstVal                                                 0x0

/*--------------------------------------
BitField Name: Par_Stk_pw_msk_typ
BitField Type: W1C
BitField Desc: FMPM FM PW Interrupt MASK Per Type Per PW
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_typ_Bit_Start                                                11
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_typ_Bit_End                                                  11
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_typ_Mask                                                 cBit11
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_typ_Shift                                                    11
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_typ_MaxVal                                                  0x1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_typ_MinVal                                                  0x0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_pw_msk_typ_RstVal                                                  0x0

/*--------------------------------------
BitField Name: Par_Stk_config_ds1_typ
BitField Type: W1C
BitField Desc: FMPM Threshold Type of DS1
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_typ_Bit_Start                                             7
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_typ_Bit_End                                               7
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_typ_Mask                                              cBit7
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_typ_Shift                                                 7
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_typ_MaxVal                                              0x1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_typ_MinVal                                              0x0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_typ_RstVal                                              0x0

/*--------------------------------------
BitField Name: Par_Stk_config_pw_typ
BitField Type: W1C
BitField Desc: FMPM Threshold Type of PW
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_typ_Bit_Start                                              6
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_typ_Bit_End                                                6
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_typ_Mask                                               cBit6
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_typ_Shift                                                  6
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_typ_MaxVal                                               0x1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_typ_MinVal                                               0x0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_typ_RstVal                                               0x0

/*--------------------------------------
BitField Name: Par_Stk_config_tca
BitField Type: W1C
BitField Desc: FMPM TCA Threshold
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_tca_Bit_Start                                                 5
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_tca_Bit_End                                                   5
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_tca_Mask                                                  cBit5
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_tca_Shift                                                     5
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_tca_MaxVal                                                  0x1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_tca_MinVal                                                  0x0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_tca_RstVal                                                  0x0

/*--------------------------------------
BitField Name: Par_Stk_config_ds1_k
BitField Type: W1C
BitField Desc: FMPM K Threshold of DS1
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_k_Bit_Start                                               1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_k_Bit_End                                                 1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_k_Mask                                                cBit1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_k_Shift                                                   1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_k_MaxVal                                                0x1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_k_MinVal                                                0x0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_ds1_k_RstVal                                                0x0

/*--------------------------------------
BitField Name: Par_Stk_config_pw_k
BitField Type: W1C
BitField Desc: FMPM K Threshold of PW
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_k_Bit_Start                                                0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_k_Bit_End                                                  0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_k_Mask                                                 cBit0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_k_Shift                                                    0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_k_MaxVal                                                 0x1
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_k_MinVal                                                 0x0
#define cAf6_FMPM_Pariry_Sticky_Par_Stk_config_pw_k_RstVal                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Force Reset Core
Reg Addr   : 0x00005
Reg Formula: 
    Where  : 
Reg Desc   : 
Force Reset PM Core

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Force_Reset_Core_Base                                                             0x00005
#define cAf6Reg_FMPM_Force_Reset_Core                                                                  0x00005
#define cAf6Reg_FMPM_Force_Reset_Core_WidthVal                                                              32
#define cAf6Reg_FMPM_Force_Reset_Core_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: drst
BitField Type: R/W/C
BitField Desc: PM Core reset done
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_Force_Reset_Core_drst_Bit_Start                                                            4
#define cAf6_FMPM_Force_Reset_Core_drst_Bit_End                                                              4
#define cAf6_FMPM_Force_Reset_Core_drst_Mask                                                             cBit4
#define cAf6_FMPM_Force_Reset_Core_drst_Shift                                                                4
#define cAf6_FMPM_Force_Reset_Core_drst_MaxVal                                                             0x1
#define cAf6_FMPM_Force_Reset_Core_drst_MinVal                                                             0x0
#define cAf6_FMPM_Force_Reset_Core_drst_RstVal                                                             0x0

/*--------------------------------------
BitField Name: frst
BitField Type: R/W
BitField Desc: Trigger 0->1 to reset PM Core
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_Force_Reset_Core_frst_Bit_Start                                                            0
#define cAf6_FMPM_Force_Reset_Core_frst_Bit_End                                                              0
#define cAf6_FMPM_Force_Reset_Core_frst_Mask                                                             cBit0
#define cAf6_FMPM_Force_Reset_Core_frst_Shift                                                                0
#define cAf6_FMPM_Force_Reset_Core_frst_MaxVal                                                             0x1
#define cAf6_FMPM_Force_Reset_Core_frst_MinVal                                                             0x0
#define cAf6_FMPM_Force_Reset_Core_frst_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold00 Data Of Read DDR
Reg Addr   : 0x00020
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold00 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold00_Data_Of_Read_DDR_Base                                                      0x00020
#define cAf6Reg_FMPM_Hold00_Data_Of_Read_DDR                                                           0x00020
#define cAf6Reg_FMPM_Hold00_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold00_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold00
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold00_Data_Of_Read_DDR_Hold00_Bit_Start                                                   0
#define cAf6_FMPM_Hold00_Data_Of_Read_DDR_Hold00_Bit_End                                                    31
#define cAf6_FMPM_Hold00_Data_Of_Read_DDR_Hold00_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold00_Data_Of_Read_DDR_Hold00_Shift                                                       0
#define cAf6_FMPM_Hold00_Data_Of_Read_DDR_Hold00_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold00_Data_Of_Read_DDR_Hold00_MinVal                                                    0x0
#define cAf6_FMPM_Hold00_Data_Of_Read_DDR_Hold00_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold01 Data Of Read DDR
Reg Addr   : 0x00021
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold01 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold01_Data_Of_Read_DDR_Base                                                      0x00021
#define cAf6Reg_FMPM_Hold01_Data_Of_Read_DDR                                                           0x00021
#define cAf6Reg_FMPM_Hold01_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold01_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold01
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold01_Data_Of_Read_DDR_Hold01_Bit_Start                                                   0
#define cAf6_FMPM_Hold01_Data_Of_Read_DDR_Hold01_Bit_End                                                    31
#define cAf6_FMPM_Hold01_Data_Of_Read_DDR_Hold01_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold01_Data_Of_Read_DDR_Hold01_Shift                                                       0
#define cAf6_FMPM_Hold01_Data_Of_Read_DDR_Hold01_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold01_Data_Of_Read_DDR_Hold01_MinVal                                                    0x0
#define cAf6_FMPM_Hold01_Data_Of_Read_DDR_Hold01_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold02 Data Of Read DDR
Reg Addr   : 0x00022
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold02 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold02_Data_Of_Read_DDR_Base                                                      0x00022
#define cAf6Reg_FMPM_Hold02_Data_Of_Read_DDR                                                           0x00022
#define cAf6Reg_FMPM_Hold02_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold02_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold02
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold02_Data_Of_Read_DDR_Hold02_Bit_Start                                                   0
#define cAf6_FMPM_Hold02_Data_Of_Read_DDR_Hold02_Bit_End                                                    31
#define cAf6_FMPM_Hold02_Data_Of_Read_DDR_Hold02_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold02_Data_Of_Read_DDR_Hold02_Shift                                                       0
#define cAf6_FMPM_Hold02_Data_Of_Read_DDR_Hold02_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold02_Data_Of_Read_DDR_Hold02_MinVal                                                    0x0
#define cAf6_FMPM_Hold02_Data_Of_Read_DDR_Hold02_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold03 Data Of Read DDR
Reg Addr   : 0x00023
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold03 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold03_Data_Of_Read_DDR_Base                                                      0x00023
#define cAf6Reg_FMPM_Hold03_Data_Of_Read_DDR                                                           0x00023
#define cAf6Reg_FMPM_Hold03_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold03_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold03
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold03_Data_Of_Read_DDR_Hold03_Bit_Start                                                   0
#define cAf6_FMPM_Hold03_Data_Of_Read_DDR_Hold03_Bit_End                                                    31
#define cAf6_FMPM_Hold03_Data_Of_Read_DDR_Hold03_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold03_Data_Of_Read_DDR_Hold03_Shift                                                       0
#define cAf6_FMPM_Hold03_Data_Of_Read_DDR_Hold03_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold03_Data_Of_Read_DDR_Hold03_MinVal                                                    0x0
#define cAf6_FMPM_Hold03_Data_Of_Read_DDR_Hold03_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold04 Data Of Read DDR
Reg Addr   : 0x00024
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold04 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold04_Data_Of_Read_DDR_Base                                                      0x00024
#define cAf6Reg_FMPM_Hold04_Data_Of_Read_DDR                                                           0x00024
#define cAf6Reg_FMPM_Hold04_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold04_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold04
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold04_Data_Of_Read_DDR_Hold04_Bit_Start                                                   0
#define cAf6_FMPM_Hold04_Data_Of_Read_DDR_Hold04_Bit_End                                                    31
#define cAf6_FMPM_Hold04_Data_Of_Read_DDR_Hold04_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold04_Data_Of_Read_DDR_Hold04_Shift                                                       0
#define cAf6_FMPM_Hold04_Data_Of_Read_DDR_Hold04_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold04_Data_Of_Read_DDR_Hold04_MinVal                                                    0x0
#define cAf6_FMPM_Hold04_Data_Of_Read_DDR_Hold04_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold05 Data Of Read DDR
Reg Addr   : 0x00025
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold05 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold05_Data_Of_Read_DDR_Base                                                      0x00025
#define cAf6Reg_FMPM_Hold05_Data_Of_Read_DDR                                                           0x00025
#define cAf6Reg_FMPM_Hold05_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold05_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold05
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold05_Data_Of_Read_DDR_Hold05_Bit_Start                                                   0
#define cAf6_FMPM_Hold05_Data_Of_Read_DDR_Hold05_Bit_End                                                    31
#define cAf6_FMPM_Hold05_Data_Of_Read_DDR_Hold05_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold05_Data_Of_Read_DDR_Hold05_Shift                                                       0
#define cAf6_FMPM_Hold05_Data_Of_Read_DDR_Hold05_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold05_Data_Of_Read_DDR_Hold05_MinVal                                                    0x0
#define cAf6_FMPM_Hold05_Data_Of_Read_DDR_Hold05_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold06 Data Of Read DDR
Reg Addr   : 0x00026
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold06 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold06_Data_Of_Read_DDR_Base                                                      0x00026
#define cAf6Reg_FMPM_Hold06_Data_Of_Read_DDR                                                           0x00026
#define cAf6Reg_FMPM_Hold06_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold06_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold06
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold06_Data_Of_Read_DDR_Hold06_Bit_Start                                                   0
#define cAf6_FMPM_Hold06_Data_Of_Read_DDR_Hold06_Bit_End                                                    31
#define cAf6_FMPM_Hold06_Data_Of_Read_DDR_Hold06_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold06_Data_Of_Read_DDR_Hold06_Shift                                                       0
#define cAf6_FMPM_Hold06_Data_Of_Read_DDR_Hold06_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold06_Data_Of_Read_DDR_Hold06_MinVal                                                    0x0
#define cAf6_FMPM_Hold06_Data_Of_Read_DDR_Hold06_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold07 Data Of Read DDR
Reg Addr   : 0x00027
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold07 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold07_Data_Of_Read_DDR_Base                                                      0x00027
#define cAf6Reg_FMPM_Hold07_Data_Of_Read_DDR                                                           0x00027
#define cAf6Reg_FMPM_Hold07_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold07_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold07
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold07_Data_Of_Read_DDR_Hold07_Bit_Start                                                   0
#define cAf6_FMPM_Hold07_Data_Of_Read_DDR_Hold07_Bit_End                                                    31
#define cAf6_FMPM_Hold07_Data_Of_Read_DDR_Hold07_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold07_Data_Of_Read_DDR_Hold07_Shift                                                       0
#define cAf6_FMPM_Hold07_Data_Of_Read_DDR_Hold07_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold07_Data_Of_Read_DDR_Hold07_MinVal                                                    0x0
#define cAf6_FMPM_Hold07_Data_Of_Read_DDR_Hold07_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold08 Data Of Read DDR
Reg Addr   : 0x00028
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold08 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold08_Data_Of_Read_DDR_Base                                                      0x00028
#define cAf6Reg_FMPM_Hold08_Data_Of_Read_DDR                                                           0x00028
#define cAf6Reg_FMPM_Hold08_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold08_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold08
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold08_Data_Of_Read_DDR_Hold08_Bit_Start                                                   0
#define cAf6_FMPM_Hold08_Data_Of_Read_DDR_Hold08_Bit_End                                                    31
#define cAf6_FMPM_Hold08_Data_Of_Read_DDR_Hold08_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold08_Data_Of_Read_DDR_Hold08_Shift                                                       0
#define cAf6_FMPM_Hold08_Data_Of_Read_DDR_Hold08_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold08_Data_Of_Read_DDR_Hold08_MinVal                                                    0x0
#define cAf6_FMPM_Hold08_Data_Of_Read_DDR_Hold08_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold09 Data Of Read DDR
Reg Addr   : 0x00029
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold09 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold09_Data_Of_Read_DDR_Base                                                      0x00029
#define cAf6Reg_FMPM_Hold09_Data_Of_Read_DDR                                                           0x00029
#define cAf6Reg_FMPM_Hold09_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold09_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold09
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold09_Data_Of_Read_DDR_Hold09_Bit_Start                                                   0
#define cAf6_FMPM_Hold09_Data_Of_Read_DDR_Hold09_Bit_End                                                    31
#define cAf6_FMPM_Hold09_Data_Of_Read_DDR_Hold09_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold09_Data_Of_Read_DDR_Hold09_Shift                                                       0
#define cAf6_FMPM_Hold09_Data_Of_Read_DDR_Hold09_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold09_Data_Of_Read_DDR_Hold09_MinVal                                                    0x0
#define cAf6_FMPM_Hold09_Data_Of_Read_DDR_Hold09_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold10 Data Of Read DDR
Reg Addr   : 0x0002A
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold10 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold10_Data_Of_Read_DDR_Base                                                      0x0002A
#define cAf6Reg_FMPM_Hold10_Data_Of_Read_DDR                                                           0x0002A
#define cAf6Reg_FMPM_Hold10_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold10_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold10
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold10_Data_Of_Read_DDR_Hold10_Bit_Start                                                   0
#define cAf6_FMPM_Hold10_Data_Of_Read_DDR_Hold10_Bit_End                                                    31
#define cAf6_FMPM_Hold10_Data_Of_Read_DDR_Hold10_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold10_Data_Of_Read_DDR_Hold10_Shift                                                       0
#define cAf6_FMPM_Hold10_Data_Of_Read_DDR_Hold10_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold10_Data_Of_Read_DDR_Hold10_MinVal                                                    0x0
#define cAf6_FMPM_Hold10_Data_Of_Read_DDR_Hold10_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold11 Data Of Read DDR
Reg Addr   : 0x0002B
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold11 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold11_Data_Of_Read_DDR_Base                                                      0x0002B
#define cAf6Reg_FMPM_Hold11_Data_Of_Read_DDR                                                           0x0002B
#define cAf6Reg_FMPM_Hold11_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold11_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold11
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold11_Data_Of_Read_DDR_Hold11_Bit_Start                                                   0
#define cAf6_FMPM_Hold11_Data_Of_Read_DDR_Hold11_Bit_End                                                    31
#define cAf6_FMPM_Hold11_Data_Of_Read_DDR_Hold11_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold11_Data_Of_Read_DDR_Hold11_Shift                                                       0
#define cAf6_FMPM_Hold11_Data_Of_Read_DDR_Hold11_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold11_Data_Of_Read_DDR_Hold11_MinVal                                                    0x0
#define cAf6_FMPM_Hold11_Data_Of_Read_DDR_Hold11_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold12 Data Of Read DDR
Reg Addr   : 0x0002C
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold12 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold12_Data_Of_Read_DDR_Base                                                      0x0002C
#define cAf6Reg_FMPM_Hold12_Data_Of_Read_DDR                                                           0x0002C
#define cAf6Reg_FMPM_Hold12_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold12_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold12
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold12_Data_Of_Read_DDR_Hold12_Bit_Start                                                   0
#define cAf6_FMPM_Hold12_Data_Of_Read_DDR_Hold12_Bit_End                                                    31
#define cAf6_FMPM_Hold12_Data_Of_Read_DDR_Hold12_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold12_Data_Of_Read_DDR_Hold12_Shift                                                       0
#define cAf6_FMPM_Hold12_Data_Of_Read_DDR_Hold12_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold12_Data_Of_Read_DDR_Hold12_MinVal                                                    0x0
#define cAf6_FMPM_Hold12_Data_Of_Read_DDR_Hold12_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold13 Data Of Read DDR
Reg Addr   : 0x0002D
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold13 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold13_Data_Of_Read_DDR_Base                                                      0x0002D
#define cAf6Reg_FMPM_Hold13_Data_Of_Read_DDR                                                           0x0002D
#define cAf6Reg_FMPM_Hold13_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold13_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold13
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold13_Data_Of_Read_DDR_Hold13_Bit_Start                                                   0
#define cAf6_FMPM_Hold13_Data_Of_Read_DDR_Hold13_Bit_End                                                    31
#define cAf6_FMPM_Hold13_Data_Of_Read_DDR_Hold13_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold13_Data_Of_Read_DDR_Hold13_Shift                                                       0
#define cAf6_FMPM_Hold13_Data_Of_Read_DDR_Hold13_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold13_Data_Of_Read_DDR_Hold13_MinVal                                                    0x0
#define cAf6_FMPM_Hold13_Data_Of_Read_DDR_Hold13_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold14 Data Of Read DDR
Reg Addr   : 0x0002E
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold14 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold14_Data_Of_Read_DDR_Base                                                      0x0002E
#define cAf6Reg_FMPM_Hold14_Data_Of_Read_DDR                                                           0x0002E
#define cAf6Reg_FMPM_Hold14_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold14_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold14
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold14_Data_Of_Read_DDR_Hold14_Bit_Start                                                   0
#define cAf6_FMPM_Hold14_Data_Of_Read_DDR_Hold14_Bit_End                                                    31
#define cAf6_FMPM_Hold14_Data_Of_Read_DDR_Hold14_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold14_Data_Of_Read_DDR_Hold14_Shift                                                       0
#define cAf6_FMPM_Hold14_Data_Of_Read_DDR_Hold14_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold14_Data_Of_Read_DDR_Hold14_MinVal                                                    0x0
#define cAf6_FMPM_Hold14_Data_Of_Read_DDR_Hold14_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold15 Data Of Read DDR
Reg Addr   : 0x0002F
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold15 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold15_Data_Of_Read_DDR_Base                                                      0x0002F
#define cAf6Reg_FMPM_Hold15_Data_Of_Read_DDR                                                           0x0002F
#define cAf6Reg_FMPM_Hold15_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold15_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold15
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold15_Data_Of_Read_DDR_Hold15_Bit_Start                                                   0
#define cAf6_FMPM_Hold15_Data_Of_Read_DDR_Hold15_Bit_End                                                    31
#define cAf6_FMPM_Hold15_Data_Of_Read_DDR_Hold15_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold15_Data_Of_Read_DDR_Hold15_Shift                                                       0
#define cAf6_FMPM_Hold15_Data_Of_Read_DDR_Hold15_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold15_Data_Of_Read_DDR_Hold15_MinVal                                                    0x0
#define cAf6_FMPM_Hold15_Data_Of_Read_DDR_Hold15_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold16 Data Of Read DDR
Reg Addr   : 0x00030
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold16 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold16_Data_Of_Read_DDR_Base                                                      0x00030
#define cAf6Reg_FMPM_Hold16_Data_Of_Read_DDR                                                           0x00030
#define cAf6Reg_FMPM_Hold16_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold16_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold16
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold16_Data_Of_Read_DDR_Hold16_Bit_Start                                                   0
#define cAf6_FMPM_Hold16_Data_Of_Read_DDR_Hold16_Bit_End                                                    31
#define cAf6_FMPM_Hold16_Data_Of_Read_DDR_Hold16_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold16_Data_Of_Read_DDR_Hold16_Shift                                                       0
#define cAf6_FMPM_Hold16_Data_Of_Read_DDR_Hold16_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold16_Data_Of_Read_DDR_Hold16_MinVal                                                    0x0
#define cAf6_FMPM_Hold16_Data_Of_Read_DDR_Hold16_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold17 Data Of Read DDR
Reg Addr   : 0x00031
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold17 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold17_Data_Of_Read_DDR_Base                                                      0x00031
#define cAf6Reg_FMPM_Hold17_Data_Of_Read_DDR                                                           0x00031
#define cAf6Reg_FMPM_Hold17_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold17_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold17
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold17_Data_Of_Read_DDR_Hold17_Bit_Start                                                   0
#define cAf6_FMPM_Hold17_Data_Of_Read_DDR_Hold17_Bit_End                                                    31
#define cAf6_FMPM_Hold17_Data_Of_Read_DDR_Hold17_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold17_Data_Of_Read_DDR_Hold17_Shift                                                       0
#define cAf6_FMPM_Hold17_Data_Of_Read_DDR_Hold17_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold17_Data_Of_Read_DDR_Hold17_MinVal                                                    0x0
#define cAf6_FMPM_Hold17_Data_Of_Read_DDR_Hold17_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM Hold18 Data Of Read DDR
Reg Addr   : 0x00032
Reg Formula: 
    Where  : 
Reg Desc   : 
Hold18 Data Of Read DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Hold18_Data_Of_Read_DDR_Base                                                      0x00032
#define cAf6Reg_FMPM_Hold18_Data_Of_Read_DDR                                                           0x00032
#define cAf6Reg_FMPM_Hold18_Data_Of_Read_DDR_WidthVal                                                       32
#define cAf6Reg_FMPM_Hold18_Data_Of_Read_DDR_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: Hold18
BitField Type: R_O
BitField Desc: Missing bit is unused
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_Hold18_Data_Of_Read_DDR_Hold18_Bit_Start                                                   0
#define cAf6_FMPM_Hold18_Data_Of_Read_DDR_Hold18_Bit_End                                                    31
#define cAf6_FMPM_Hold18_Data_Of_Read_DDR_Hold18_Mask                                                 cBit31_0
#define cAf6_FMPM_Hold18_Data_Of_Read_DDR_Hold18_Shift                                                       0
#define cAf6_FMPM_Hold18_Data_Of_Read_DDR_Hold18_MaxVal                                             0xffffffff
#define cAf6_FMPM_Hold18_Data_Of_Read_DDR_Hold18_MinVal                                                    0x0
#define cAf6_FMPM_Hold18_Data_Of_Read_DDR_Hold18_RstVal                                                       


/*------------------------------------------------------------------------------
Reg Name   : FMPM TCA Threshold
Reg Addr   : 0x00050-0x00057
Reg Formula: 0x00050+$id
    Where  : 
           + $id(0-7) : Location
Reg Desc   : 
TCA Threshold

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_TCA_Threshold_Base                                                                0x00050
#define cAf6Reg_FMPM_TCA_Threshold(id)                                                          (0x00050+(id))
#define cAf6Reg_FMPM_TCA_Threshold_WidthVal                                                                 32
#define cAf6Reg_FMPM_TCA_Threshold_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: tca
BitField Type: R/W
BitField Desc: TCA threshold
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_FMPM_TCA_Threshold_tca_Bit_Start                                                                0
#define cAf6_FMPM_TCA_Threshold_tca_Bit_End                                                                  9
#define cAf6_FMPM_TCA_Threshold_tca_Mask                                                               cBit9_0
#define cAf6_FMPM_TCA_Threshold_tca_Shift                                                                    0
#define cAf6_FMPM_TCA_Threshold_tca_MaxVal                                                               0x3ff
#define cAf6_FMPM_TCA_Threshold_tca_MinVal                                                                 0x0
#define cAf6_FMPM_TCA_Threshold_tca_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM K Threshold of DS1
Reg Addr   : 0x000B0-0x000B7
Reg Formula: 0x000B0+$id
    Where  : 
           + $id(0-7) : Type ID
Reg Desc   : 
K threshold of DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_K_Threshold_of_DS1_Base                                                           0x000B0
#define cAf6Reg_FMPM_K_Threshold_of_DS1(id)                                                     (0x000B0+(id))
#define cAf6Reg_FMPM_K_Threshold_of_DS1_WidthVal                                                            32
#define cAf6Reg_FMPM_K_Threshold_of_DS1_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: kval
BitField Type: R/W
BitField Desc: K threshold of DS1
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_FMPM_K_Threshold_of_DS1_kval_Bit_Start                                                          0
#define cAf6_FMPM_K_Threshold_of_DS1_kval_Bit_End                                                           15
#define cAf6_FMPM_K_Threshold_of_DS1_kval_Mask                                                        cBit15_0
#define cAf6_FMPM_K_Threshold_of_DS1_kval_Shift                                                              0
#define cAf6_FMPM_K_Threshold_of_DS1_kval_MaxVal                                                        0xffff
#define cAf6_FMPM_K_Threshold_of_DS1_kval_MinVal                                                           0x0
#define cAf6_FMPM_K_Threshold_of_DS1_kval_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM K Threshold of PW
Reg Addr   : 0x000C0-0x000C7
Reg Formula: 0x000C0+$id
    Where  : 
           + $id(0-7) : Type ID
Reg Desc   : 
K threshold of PW

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_K_Threshold_of_PW_Base                                                            0x000C0
#define cAf6Reg_FMPM_K_Threshold_of_PW(id)                                                      (0x000C0+(id))
#define cAf6Reg_FMPM_K_Threshold_of_PW_WidthVal                                                             32
#define cAf6Reg_FMPM_K_Threshold_of_PW_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: kval
BitField Type: R/W
BitField Desc: K threshold of PW
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_FMPM_K_Threshold_of_PW_kval_Bit_Start                                                           0
#define cAf6_FMPM_K_Threshold_of_PW_kval_Bit_End                                                            15
#define cAf6_FMPM_K_Threshold_of_PW_kval_Mask                                                         cBit15_0
#define cAf6_FMPM_K_Threshold_of_PW_kval_Shift                                                               0
#define cAf6_FMPM_K_Threshold_of_PW_kval_MaxVal                                                         0xffff
#define cAf6_FMPM_K_Threshold_of_PW_kval_MinVal                                                            0x0
#define cAf6_FMPM_K_Threshold_of_PW_kval_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Threshold Type of DS1
Reg Addr   : 0x08000-0x0BFFF
Reg Formula: 0x08000+$H*0x20+$I
    Where  : 
           + $H(0-1) : DS1/E1 Group
           + $I(0-23) : DS1/E1 Framer
Reg Desc   : 
Threshold Type of DS1

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Threshold_Type_of_DS1_Base                                                        0x08000
#define cAf6Reg_FMPM_Threshold_Type_of_DS1(H, I)                                        (0x08000+(H)*0x20+(I))
#define cAf6Reg_FMPM_Threshold_Type_of_DS1_WidthVal                                                         32
#define cAf6Reg_FMPM_Threshold_Type_of_DS1_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: K_TYPE
BitField Type: R/W
BitField Desc: Use to read K threshold table
BitField Bits: [10:08]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_DS1_K_TYPE_Bit_Start                                                     8
#define cAf6_FMPM_Threshold_Type_of_DS1_K_TYPE_Bit_End                                                      10
#define cAf6_FMPM_Threshold_Type_of_DS1_K_TYPE_Mask                                                   cBit10_8
#define cAf6_FMPM_Threshold_Type_of_DS1_K_TYPE_Shift                                                         8
#define cAf6_FMPM_Threshold_Type_of_DS1_K_TYPE_MaxVal                                                      0x7
#define cAf6_FMPM_Threshold_Type_of_DS1_K_TYPE_MinVal                                                      0x0
#define cAf6_FMPM_Threshold_Type_of_DS1_K_TYPE_RstVal                                                      0x0

/*--------------------------------------
BitField Name: TCA_TYPE
BitField Type: R/W
BitField Desc: Use to read TCA threshold table
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_DS1_TCA_TYPE_Bit_Start                                                   0
#define cAf6_FMPM_Threshold_Type_of_DS1_TCA_TYPE_Bit_End                                                     3
#define cAf6_FMPM_Threshold_Type_of_DS1_TCA_TYPE_Mask                                                  cBit3_0
#define cAf6_FMPM_Threshold_Type_of_DS1_TCA_TYPE_Shift                                                       0
#define cAf6_FMPM_Threshold_Type_of_DS1_TCA_TYPE_MaxVal                                                    0xf
#define cAf6_FMPM_Threshold_Type_of_DS1_TCA_TYPE_MinVal                                                    0x0
#define cAf6_FMPM_Threshold_Type_of_DS1_TCA_TYPE_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM Threshold Type of PW
Reg Addr   : 0x0C000-0x0DFFF
Reg Formula: 0x0C000+$id
    Where  : 
           + $id(0-191) : PW ID
Reg Desc   : 
Threshold Type of PW

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_Threshold_Type_of_PW_Base                                                         0x0C000
#define cAf6Reg_FMPM_Threshold_Type_of_PW(id)                                                   (0x0C000+(id))
#define cAf6Reg_FMPM_Threshold_Type_of_PW_WidthVal                                                          32
#define cAf6Reg_FMPM_Threshold_Type_of_PW_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: K_TYPE
BitField Type: R/W
BitField Desc: Use to read K threshold table
BitField Bits: [10:08]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_PW_K_TYPE_Bit_Start                                                      8
#define cAf6_FMPM_Threshold_Type_of_PW_K_TYPE_Bit_End                                                       10
#define cAf6_FMPM_Threshold_Type_of_PW_K_TYPE_Mask                                                    cBit10_8
#define cAf6_FMPM_Threshold_Type_of_PW_K_TYPE_Shift                                                          8
#define cAf6_FMPM_Threshold_Type_of_PW_K_TYPE_MaxVal                                                       0x7
#define cAf6_FMPM_Threshold_Type_of_PW_K_TYPE_MinVal                                                       0x0
#define cAf6_FMPM_Threshold_Type_of_PW_K_TYPE_RstVal                                                       0x0

/*--------------------------------------
BitField Name: TCA_TYPE
BitField Type: R/W
BitField Desc: Use to read TCA threshold table
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_FMPM_Threshold_Type_of_PW_TCA_TYPE_Bit_Start                                                    0
#define cAf6_FMPM_Threshold_Type_of_PW_TCA_TYPE_Bit_End                                                      3
#define cAf6_FMPM_Threshold_Type_of_PW_TCA_TYPE_Mask                                                   cBit3_0
#define cAf6_FMPM_Threshold_Type_of_PW_TCA_TYPE_Shift                                                        0
#define cAf6_FMPM_Threshold_Type_of_PW_TCA_TYPE_MaxVal                                                     0xf
#define cAf6_FMPM_Threshold_Type_of_PW_TCA_TYPE_MinVal                                                     0x0
#define cAf6_FMPM_Threshold_Type_of_PW_TCA_TYPE_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM Interrupt
Reg Addr   : 0x14000
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_Interrupt_Base                                                                 0x14000
#define cAf6Reg_FMPM_FM_Interrupt                                                                      0x14000
#define cAf6Reg_FMPM_FM_Interrupt_WidthVal                                                                  32
#define cAf6Reg_FMPM_FM_Interrupt_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: FMPM_FM_Intr_PW
BitField Type: R_O
BitField Desc: PW Interrupt
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_PW_Bit_Start                                                     5
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_PW_Bit_End                                                       5
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_PW_Mask                                                      cBit5
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_PW_Shift                                                         5
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_PW_MaxVal                                                      0x1
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_PW_MinVal                                                      0x0
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_PW_RstVal                                                      0x0

/*--------------------------------------
BitField Name: FMPM_FM_Intr_DS1E1
BitField Type: R_O
BitField Desc: DS1/E1 Interrupt
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS1E1_Bit_Start                                                  4
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS1E1_Bit_End                                                    4
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS1E1_Mask                                                   cBit4
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS1E1_Shift                                                      4
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS1E1_MaxVal                                                   0x1
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS1E1_MinVal                                                   0x0
#define cAf6_FMPM_FM_Interrupt_FMPM_FM_Intr_DS1E1_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM Interrupt Mask
Reg Addr   : 0x14001
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_Interrupt_Mask_Base                                                            0x14001
#define cAf6Reg_FMPM_FM_Interrupt_Mask                                                                 0x14001
#define cAf6Reg_FMPM_FM_Interrupt_Mask_WidthVal                                                             32
#define cAf6Reg_FMPM_FM_Interrupt_Mask_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: FMPM_FM_Intr_PW
BitField Type: R/W
BitField Desc: PW Interrupt
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_PW_Bit_Start                                                5
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_PW_Bit_End                                                  5
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_PW_Mask                                                 cBit5
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_PW_Shift                                                    5
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_PW_MaxVal                                                 0x1
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_PW_MinVal                                                 0x0
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_PW_RstVal                                                 0x0

/*--------------------------------------
BitField Name: FMPM_FM_Intr_DS1E1
BitField Type: R/W
BitField Desc: DS1/E1 Interrupt
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_DS1E1_Bit_Start                                             4
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_DS1E1_Bit_End                                               4
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_DS1E1_Mask                                              cBit4
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_DS1E1_Shift                                                 4
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_DS1E1_MaxVal                                              0x1
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_DS1E1_MinVal                                              0x0
#define cAf6_FMPM_FM_Interrupt_Mask_FMPM_FM_Intr_DS1E1_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS1E1 Group Interrupt OR
Reg Addr   : 0x18003
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS1E1_Group_Interrupt_OR_Base                                                  0x18003
#define cAf6Reg_FMPM_FM_DS1E1_Group_Interrupt_OR                                                       0x18003
#define cAf6Reg_FMPM_FM_DS1E1_Group_Interrupt_OR_WidthVal                                                   32
#define cAf6Reg_FMPM_FM_DS1E1_Group_Interrupt_OR_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_GRP_OR
BitField Type: R_O
BitField Desc: DS1/E1 Group Interrupt OR
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Group_Interrupt_OR_FMPM_FM_DS1E1_GRP_OR_Bit_Start                                       0
#define cAf6_FMPM_FM_DS1E1_Group_Interrupt_OR_FMPM_FM_DS1E1_GRP_OR_Bit_End                                       1
#define cAf6_FMPM_FM_DS1E1_Group_Interrupt_OR_FMPM_FM_DS1E1_GRP_OR_Mask                                 cBit1_0
#define cAf6_FMPM_FM_DS1E1_Group_Interrupt_OR_FMPM_FM_DS1E1_GRP_OR_Shift                                       0
#define cAf6_FMPM_FM_DS1E1_Group_Interrupt_OR_FMPM_FM_DS1E1_GRP_OR_MaxVal                                     0x3
#define cAf6_FMPM_FM_DS1E1_Group_Interrupt_OR_FMPM_FM_DS1E1_GRP_OR_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Group_Interrupt_OR_FMPM_FM_DS1E1_GRP_OR_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS1E1 Framer Interrupt OR AND MASK Per DS1E1
Reg Addr   : 0x1B000-0x1B1FF
Reg Formula: 0x1B000+$H
    Where  : 
           + $H(0-1) : DS1/E1 Group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_Base                                 0x1B000
#define cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1(H)                           (0x1B000+(H))
#define cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_WidthVal                                      32
#define cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_OAMSK
BitField Type: R_O
BitField Desc: DS1/E1 Framer Interrupt
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_FM_DS1E1_OAMSK_Bit_Start                                       0
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_FM_DS1E1_OAMSK_Bit_End                                      23
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_FM_DS1E1_OAMSK_Mask                                cBit23_0
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_FM_DS1E1_OAMSK_Shift                                       0
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_FM_DS1E1_OAMSK_MaxVal                                0xffffff
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_FM_DS1E1_OAMSK_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_FM_DS1E1_OAMSK_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS1E1 Framer Interrupt MASK Per DS1E1
Reg Addr   : 0x1B200-0x1B3FF
Reg Formula: 0x1B200+$H
    Where  : 
           + $H(0-1) : DS1/E1 Group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_Base                                     0x1B200
#define cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1(H)                                 (0x1B200+(H))
#define cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_WidthVal                                      32
#define cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK
BitField Type: R/W
BitField Desc: DS1/E1 Framer Interrupt
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_FM_DS1E1_MSK_Bit_Start                                       0
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_FM_DS1E1_MSK_Bit_End                                      23
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_FM_DS1E1_MSK_Mask                                cBit23_0
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_FM_DS1E1_MSK_Shift                                       0
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_FM_DS1E1_MSK_MaxVal                                0xffffff
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_FM_DS1E1_MSK_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_FM_DS1E1_MSK_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS1E1 Interrupt Sticky Per Type Per DS1E1
Reg Addr   : 0x30000-0x33FFF
Reg Formula: 0x30000+$H*0x20+$I
    Where  : 
           + $H(0-1) : DS1/E1 Group
           + $I(0-23) : DS1/E1
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_Base                                 0x30000
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1(H, I)                  (0x30000+(H)*0x20+(I))
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_WidthVal                                      32
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_STK_RAI_CI
BitField Type: W1C
BitField Desc: RAI_CI
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_CI_Bit_Start                                       5
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_CI_Bit_End                                       5
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_CI_Mask                                   cBit5
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_CI_Shift                                       5
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_CI_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_CI_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_CI_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_STK_RAI
BitField Type: W1C
BitField Desc: RAI
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_Bit_Start                                       4
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_Bit_End                                       4
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_Mask                                   cBit4
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_Shift                                       4
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_RAI_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_STK_LOF
BitField Type: W1C
BitField Desc: LOF
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOF_Bit_Start                                       3
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOF_Bit_End                                       3
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOF_Mask                                   cBit3
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOF_Shift                                       3
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOF_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOF_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOF_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_STK_AIS_CI
BitField Type: W1C
BitField Desc: AIS_CI
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_CI_Bit_Start                                       2
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_CI_Bit_End                                       2
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_CI_Mask                                   cBit2
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_CI_Shift                                       2
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_CI_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_CI_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_CI_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_STK_AIS
BitField Type: W1C
BitField Desc: AIS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_Bit_Start                                       1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_Bit_End                                       1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_Mask                                   cBit1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_Shift                                       1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_AIS_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_STK_LOS
BitField Type: W1C
BitField Desc: LOS
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOS_Bit_Start                                       0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOS_Bit_End                                       0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOS_Mask                                   cBit0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOS_Shift                                       0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOS_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOS_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_STK_LOS_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1
Reg Addr   : 0x34000-0x37FFF
Reg Formula: 0x34000+$H*0x20+$I
    Where  : 
           + $H(0-1) : DS1/E1 Group
           + $I(0-23) : DS1/E1
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_Base                                   0x34000
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1(H, I)                   (0x34000+(H)*0x20+(I))
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_WidthVal                                      32
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK_RAI_CI
BitField Type: R/W
BitField Desc: RAI_CI
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_CI_Bit_Start                                       5
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_CI_Bit_End                                       5
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_CI_Mask                                   cBit5
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_CI_Shift                                       5
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_CI_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_CI_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_CI_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK_RAI
BitField Type: R/W
BitField Desc: RAI
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_Bit_Start                                       4
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_Bit_End                                       4
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_Mask                                   cBit4
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_Shift                                       4
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_RAI_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK_LOF
BitField Type: R/W
BitField Desc: LOF
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOF_Bit_Start                                       3
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOF_Bit_End                                       3
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOF_Mask                                   cBit3
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOF_Shift                                       3
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOF_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOF_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOF_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK_AIS_CI
BitField Type: R/W
BitField Desc: AIS_CI
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_CI_Bit_Start                                       2
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_CI_Bit_End                                       2
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_CI_Mask                                   cBit2
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_CI_Shift                                       2
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_CI_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_CI_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_CI_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK_AIS
BitField Type: R/W
BitField Desc: AIS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_Bit_Start                                       1
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_Bit_End                                       1
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_Mask                                   cBit1
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_Shift                                       1
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_AIS_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_MSK_LOS
BitField Type: R/W
BitField Desc: LOS
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOS_Bit_Start                                       0
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOS_Bit_End                                       0
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOS_Mask                                   cBit0
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOS_Shift                                       0
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOS_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOS_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_MSK_LOS_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM DS1E1 Interrupt Current Status Per Type Per DS1E1
Reg Addr   : 0x38000-0x3BFFF
Reg Formula: 0x38000+$H*0x20+$I
    Where  : 
           + $H(0-1) : DS1/E1 Group
           + $I(0-23) : DS1/E1
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_Base                                 0x38000
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1(H, I)                  (0x38000+(H)*0x20+(I))
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_WidthVal                                      32
#define cAf6Reg_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_AMSK_RAI_CI
BitField Type: R_O
BitField Desc: RAI_CI
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_CI_Bit_Start                                      21
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_CI_Bit_End                                      21
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_CI_Mask                                  cBit21
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_CI_Shift                                      21
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_CI_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_CI_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_CI_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_AMSK_RAI
BitField Type: R_O
BitField Desc: RAI
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_Bit_Start                                      20
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_Bit_End                                      20
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_Mask                                  cBit20
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_Shift                                      20
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_RAI_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_AMSK_LOF
BitField Type: R_O
BitField Desc: LOF
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOF_Bit_Start                                      19
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOF_Bit_End                                      19
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOF_Mask                                  cBit19
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOF_Shift                                      19
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOF_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOF_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOF_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_AMSK_AIS_CI
BitField Type: R_O
BitField Desc: AIS_CI
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_CI_Bit_Start                                      18
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_CI_Bit_End                                      18
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_CI_Mask                                  cBit18
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_CI_Shift                                      18
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_CI_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_CI_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_CI_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_AMSK_AIS
BitField Type: R_O
BitField Desc: AIS
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_Bit_Start                                      17
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_Bit_End                                      17
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_Mask                                  cBit17
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_Shift                                      17
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_AIS_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_AMSK_LOS
BitField Type: R_O
BitField Desc: LOS
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOS_Bit_Start                                      16
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOS_Bit_End                                      16
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOS_Mask                                  cBit16
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOS_Shift                                      16
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOS_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOS_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_AMSK_LOS_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_CUR_RAI_CI
BitField Type: R_O
BitField Desc: RAI_CI
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_CI_Bit_Start                                       5
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_CI_Bit_End                                       5
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_CI_Mask                                   cBit5
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_CI_Shift                                       5
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_CI_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_CI_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_CI_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_CUR_RAI
BitField Type: R_O
BitField Desc: RAI
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_Bit_Start                                       4
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_Bit_End                                       4
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_Mask                                   cBit4
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_Shift                                       4
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_RAI_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_CUR_LOF
BitField Type: R_O
BitField Desc: LOF
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOF_Bit_Start                                       3
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOF_Bit_End                                       3
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOF_Mask                                   cBit3
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOF_Shift                                       3
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOF_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOF_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOF_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_CUR_AIS_CI
BitField Type: R_O
BitField Desc: AIS_CI
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_CI_Bit_Start                                       2
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_CI_Bit_End                                       2
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_CI_Mask                                   cBit2
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_CI_Shift                                       2
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_CI_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_CI_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_CI_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_CUR_AIS
BitField Type: R_O
BitField Desc: AIS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_Bit_Start                                       1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_Bit_End                                       1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_Mask                                   cBit1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_Shift                                       1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_AIS_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_DS1E1_CUR_LOS
BitField Type: R_O
BitField Desc: LOS
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOS_Bit_Start                                       0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOS_Bit_End                                       0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOS_Mask                                   cBit0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOS_Shift                                       0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOS_MaxVal                                     0x1
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOS_MinVal                                     0x0
#define cAf6_FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_FM_DS1E1_CUR_LOS_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Group Interrupt OR
Reg Addr   : 0x18004
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Group_Interrupt_OR_Base                                                     0x18004
#define cAf6Reg_FMPM_FM_PW_Group_Interrupt_OR                                                          0x18004
#define cAf6Reg_FMPM_FM_PW_Group_Interrupt_OR_WidthVal                                                      32
#define cAf6Reg_FMPM_FM_PW_Group_Interrupt_OR_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_GRP_OR
BitField Type: R_O
BitField Desc: PW Group Interrupt OR, bit per group
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Group_Interrupt_OR_FMPM_FM_PW_GRP_OR_Bit_Start                                       0
#define cAf6_FMPM_FM_PW_Group_Interrupt_OR_FMPM_FM_PW_GRP_OR_Bit_End                                         5
#define cAf6_FMPM_FM_PW_Group_Interrupt_OR_FMPM_FM_PW_GRP_OR_Mask                                      cBit5_0
#define cAf6_FMPM_FM_PW_Group_Interrupt_OR_FMPM_FM_PW_GRP_OR_Shift                                           0
#define cAf6_FMPM_FM_PW_Group_Interrupt_OR_FMPM_FM_PW_GRP_OR_MaxVal                                       0x3f
#define cAf6_FMPM_FM_PW_Group_Interrupt_OR_FMPM_FM_PW_GRP_OR_MinVal                                        0x0
#define cAf6_FMPM_FM_PW_Group_Interrupt_OR_FMPM_FM_PW_GRP_OR_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Framer Interrupt OR AND MASK Per PW
Reg Addr   : 0x1B400-0x1B4FF
Reg Formula: 0x1B400+$J
    Where  : 
           + $J(0-5) : PW Group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_Base                                    0x1B400
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW(J)                                (0x1B400+(J))
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_WidthVal                                      32
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_OAMSK
BitField Type: R_O
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_FM_PW_OAMSK_Bit_Start                                       0
#define cAf6_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_FM_PW_OAMSK_Bit_End                                      31
#define cAf6_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_FM_PW_OAMSK_Mask                                cBit31_0
#define cAf6_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_FM_PW_OAMSK_Shift                                       0
#define cAf6_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_FM_PW_OAMSK_MaxVal                              0xffffffff
#define cAf6_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_FM_PW_OAMSK_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_FM_PW_OAMSK_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Framer Interrupt MASK Per PW
Reg Addr   : 0x1B500-0x1B5FF
Reg Formula: 0x1B500+$J
    Where  : 
           + $J(0-5) : PW Group
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_Base                                           0x1B500
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW(J)                                       (0x1B500+(J))
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_WidthVal                                            32
#define cAf6Reg_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_WriteMask                                          0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK
BitField Type: R/W
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_FM_PW_MSK_Bit_Start                                       0
#define cAf6_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_FM_PW_MSK_Bit_End                                      31
#define cAf6_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_FM_PW_MSK_Mask                                cBit31_0
#define cAf6_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_FM_PW_MSK_Shift                                       0
#define cAf6_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_FM_PW_MSK_MaxVal                              0xffffffff
#define cAf6_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_FM_PW_MSK_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_FM_PW_MSK_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Interrupt Sticky Per Type Per PW
Reg Addr   : 0x1C000-0x1DFFF
Reg Formula: 0x1C000+$J*0x20+$K
    Where  : 
           + $J(0-5) : PW  Group
           + $K(0-31) : PW-ID
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_Base                                       0x1C000
#define cAf6Reg_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW(J, K)                       (0x1C000+(J)*0x20+(K))
#define cAf6Reg_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_WidthVal                                        32
#define cAf6Reg_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_WriteMask                                      0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_RBIT
BitField Type: W1C
BitField Desc: RBIT
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_RBIT_Bit_Start                                       7
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_RBIT_Bit_End                                       7
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_RBIT_Mask                                   cBit7
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_RBIT_Shift                                       7
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_RBIT_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_RBIT_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_RBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_STRAY
BitField Type: W1C
BitField Desc: STRAY
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_STRAY_Bit_Start                                       6
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_STRAY_Bit_End                                       6
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_STRAY_Mask                                   cBit6
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_STRAY_Shift                                       6
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_STRAY_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_STRAY_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_STRAY_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_MALF
BitField Type: W1C
BitField Desc: MALF
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_MALF_Bit_Start                                       5
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_MALF_Bit_End                                       5
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_MALF_Mask                                   cBit5
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_MALF_Shift                                       5
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_MALF_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_MALF_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_MALF_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_LBIT
BitField Type: W1C
BitField Desc: LBIT
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LBIT_Bit_Start                                       4
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LBIT_Bit_End                                       4
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LBIT_Mask                                   cBit4
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LBIT_Shift                                       4
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LBIT_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LBIT_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_BufUder
BitField Type: W1C
BitField Desc: BufUder
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufUder_Bit_Start                                       3
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufUder_Bit_End                                       3
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufUder_Mask                                   cBit3
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufUder_Shift                                       3
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufUder_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufUder_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufUder_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_BufOver
BitField Type: W1C
BitField Desc: BufOver
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufOver_Bit_Start                                       2
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufOver_Bit_End                                       2
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufOver_Mask                                   cBit2
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufOver_Shift                                       2
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufOver_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufOver_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_BufOver_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_LOPS
BitField Type: W1C
BitField Desc: LOPS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPS_Bit_Start                                       1
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPS_Bit_End                                       1
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPS_Mask                                   cBit1
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPS_Shift                                       1
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPS_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPS_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPS_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_STK_LOPSTA
BitField Type: W1C
BitField Desc: LOPSTA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPSTA_Bit_Start                                       0
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPSTA_Bit_End                                       0
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPSTA_Mask                                   cBit0
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPSTA_Shift                                       0
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPSTA_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPSTA_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_FM_PW_STK_LOPSTA_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Interrupt MASK Per Type Per PW
Reg Addr   : 0x1E000-0x1FFFF
Reg Formula: 0x1E000+$J*0x20+$K
    Where  : 
           + $J(0-5) : PW  Group
           + $K(0-31) : PW-ID
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_Base                                         0x1E000
#define cAf6Reg_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW(J, K)                         (0x1E000+(J)*0x20+(K))
#define cAf6Reg_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_WidthVal                                          32
#define cAf6Reg_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_WriteMask                                        0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_RBIT
BitField Type: R/W
BitField Desc: RBIT
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_RBIT_Bit_Start                                       7
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_RBIT_Bit_End                                       7
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_RBIT_Mask                                   cBit7
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_RBIT_Shift                                       7
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_RBIT_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_RBIT_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_RBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_STRAY
BitField Type: R/W
BitField Desc: STRAY
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_STRAY_Bit_Start                                       6
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_STRAY_Bit_End                                       6
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_STRAY_Mask                                   cBit6
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_STRAY_Shift                                       6
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_STRAY_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_STRAY_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_STRAY_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_MALF
BitField Type: R/W
BitField Desc: MALF
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_MALF_Bit_Start                                       5
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_MALF_Bit_End                                       5
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_MALF_Mask                                   cBit5
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_MALF_Shift                                       5
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_MALF_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_MALF_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_MALF_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_LBIT
BitField Type: R/W
BitField Desc: LBIT
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LBIT_Bit_Start                                       4
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LBIT_Bit_End                                       4
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LBIT_Mask                                   cBit4
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LBIT_Shift                                       4
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LBIT_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LBIT_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_BufUder
BitField Type: R/W
BitField Desc: BufUder
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufUder_Bit_Start                                       3
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufUder_Bit_End                                       3
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufUder_Mask                                   cBit3
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufUder_Shift                                       3
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufUder_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufUder_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufUder_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_BufOver
BitField Type: R/W
BitField Desc: BufOver
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufOver_Bit_Start                                       2
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufOver_Bit_End                                       2
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufOver_Mask                                   cBit2
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufOver_Shift                                       2
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufOver_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufOver_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_BufOver_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_LOPS
BitField Type: R/W
BitField Desc: LOPS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPS_Bit_Start                                       1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPS_Bit_End                                       1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPS_Mask                                   cBit1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPS_Shift                                       1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPS_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPS_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPS_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_MSK_LOPSTA
BitField Type: R/W
BitField Desc: LOPSTA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPSTA_Bit_Start                                       0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPSTA_Bit_End                                       0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPSTA_Mask                                   cBit0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPSTA_Shift                                       0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPSTA_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPSTA_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_FM_PW_MSK_LOPSTA_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM FM PW Interrupt Current Status Per Type Per PW
Reg Addr   : 0x20000-0x21FFF
Reg Formula: 0x20000+$J*0x20+$K
    Where  : 
           + $J(0-5) : PW  Group
           + $K(0-31) : PW-ID
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_Base                                 0x20000
#define cAf6Reg_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW(J, K)                  (0x20000+(J)*0x20+(K))
#define cAf6Reg_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_WidthVal                                      32
#define cAf6Reg_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_RBIT
BitField Type: R_O
BitField Desc: RBIT
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Bit_Start                                      23
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Bit_End                                      23
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Mask                                  cBit23
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Shift                                      23
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_STRAY
BitField Type: R_O
BitField Desc: STRAY
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Bit_Start                                      22
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Bit_End                                      22
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Mask                                  cBit22
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Shift                                      22
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_MALF
BitField Type: R_O
BitField Desc: MALF
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Bit_Start                                      21
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Bit_End                                      21
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Mask                                  cBit21
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Shift                                      21
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LBIT
BitField Type: R_O
BitField Desc: LBIT
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Bit_Start                                      20
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Bit_End                                      20
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Mask                                  cBit20
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Shift                                      20
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_BufUder
BitField Type: R_O
BitField Desc: BufUder
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Bit_Start                                      19
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Bit_End                                      19
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Mask                                  cBit19
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Shift                                      19
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_BufOver
BitField Type: R_O
BitField Desc: BufOver
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Bit_Start                                      18
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Bit_End                                      18
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Mask                                  cBit18
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Shift                                      18
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LOPS
BitField Type: R_O
BitField Desc: LOPS
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_Bit_Start                                      17
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_Bit_End                                      17
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_Mask                                  cBit17
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_Shift                                      17
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LOPSTA
BitField Type: R_O
BitField Desc: LOPSTA
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Bit_Start                                      16
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Bit_End                                      16
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Mask                                  cBit16
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Shift                                      16
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_RBIT
BitField Type: R_O
BitField Desc: RBIT
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_Bit_Start                                       7
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_Bit_End                                       7
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_Mask                                   cBit7
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_Shift                                       7
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_RBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_STRAY
BitField Type: R_O
BitField Desc: STRAY
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_Bit_Start                                       6
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_Bit_End                                       6
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_Mask                                   cBit6
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_Shift                                       6
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_STRAY_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_MALF
BitField Type: R_O
BitField Desc: MALF
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_Bit_Start                                       5
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_Bit_End                                       5
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_Mask                                   cBit5
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_Shift                                       5
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_MALF_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_LBIT
BitField Type: R_O
BitField Desc: LBIT
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_Bit_Start                                       4
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_Bit_End                                       4
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_Mask                                   cBit4
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_Shift                                       4
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_BufUder
BitField Type: R_O
BitField Desc: BufUder
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_Bit_Start                                       3
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_Bit_End                                       3
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_Mask                                   cBit3
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_Shift                                       3
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufUder_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_BufOver
BitField Type: R_O
BitField Desc: BufOver
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_Bit_Start                                       2
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_Bit_End                                       2
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_Mask                                   cBit2
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_Shift                                       2
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_BufOver_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_LOPS
BitField Type: R_O
BitField Desc: LOPS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPS_Bit_Start                                       1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPS_Bit_End                                       1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPS_Mask                                   cBit1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPS_Shift                                       1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPS_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPS_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPS_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_CUR_LOPSTA
BitField Type: R_O
BitField Desc: LOPSTA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_Bit_Start                                       0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_Bit_End                                       0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_Mask                                   cBit0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_Shift                                       0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_MaxVal                                     0x1
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_MinVal                                     0x0
#define cAf6_FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_CUR_LOPSTA_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM Interrupt
Reg Addr   : 0x54000
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM PM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_Interrupt_Base                                                                 0x54000
#define cAf6Reg_FMPM_PM_Interrupt                                                                      0x54000
#define cAf6Reg_FMPM_PM_Interrupt_WidthVal                                                                  32
#define cAf6Reg_FMPM_PM_Interrupt_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: FMPM_PM_Intr_PW
BitField Type: R_O
BitField Desc: PW interrupt
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_PW_Bit_Start                                                     5
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_PW_Bit_End                                                       5
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_PW_Mask                                                      cBit5
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_PW_Shift                                                         5
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_PW_MaxVal                                                      0x1
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_PW_MinVal                                                      0x0
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_PW_RstVal                                                      0x0

/*--------------------------------------
BitField Name: FMPM_PM_Intr_DS1E1
BitField Type: R_O
BitField Desc: DS1/E1 interrupt
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_DS1E1_Bit_Start                                                  4
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_DS1E1_Bit_End                                                    4
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_DS1E1_Mask                                                   cBit4
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_DS1E1_Shift                                                      4
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_DS1E1_MaxVal                                                   0x1
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_DS1E1_MinVal                                                   0x0
#define cAf6_FMPM_PM_Interrupt_FMPM_PM_Intr_DS1E1_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM Interrupt Mask
Reg Addr   : 0x54001
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_Interrupt_Mask_Base                                                            0x54001
#define cAf6Reg_FMPM_PM_Interrupt_Mask                                                                 0x54001
#define cAf6Reg_FMPM_PM_Interrupt_Mask_WidthVal                                                             32
#define cAf6Reg_FMPM_PM_Interrupt_Mask_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: FMPM_PM_Intr_MSK_PW
BitField Type: R/W
BitField Desc: PW interrupt
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_PW_Bit_Start                                            5
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_PW_Bit_End                                              5
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_PW_Mask                                             cBit5
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_PW_Shift                                                5
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_PW_MaxVal                                             0x1
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_PW_MinVal                                             0x0
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_PW_RstVal                                             0x0

/*--------------------------------------
BitField Name: FMPM_PM_Intr_MSK_DS1E1
BitField Type: R/W
BitField Desc: DS1/E1 interrupt
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_DS1E1_Bit_Start                                         4
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_DS1E1_Bit_End                                           4
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_DS1E1_Mask                                          cBit4
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_DS1E1_Shift                                             4
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_DS1E1_MaxVal                                          0x1
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_DS1E1_MinVal                                          0x0
#define cAf6_FMPM_PM_Interrupt_Mask_FMPM_PM_Intr_MSK_DS1E1_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM DS1E1 Group Interrupt OR
Reg Addr   : 0x58003
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM PM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_DS1E1_Group_Interrupt_OR_Base                                                  0x58003
#define cAf6Reg_FMPM_PM_DS1E1_Group_Interrupt_OR                                                       0x58003
#define cAf6Reg_FMPM_PM_DS1E1_Group_Interrupt_OR_WidthVal                                                   32
#define cAf6Reg_FMPM_PM_DS1E1_Group_Interrupt_OR_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_LEV2_OR
BitField Type: R_O
BitField Desc: DS1/E1 Group Interrupt OR
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Group_Interrupt_OR_FMPM_PM_DS1E1_LEV2_OR_Bit_Start                                       0
#define cAf6_FMPM_PM_DS1E1_Group_Interrupt_OR_FMPM_PM_DS1E1_LEV2_OR_Bit_End                                       1
#define cAf6_FMPM_PM_DS1E1_Group_Interrupt_OR_FMPM_PM_DS1E1_LEV2_OR_Mask                                 cBit1_0
#define cAf6_FMPM_PM_DS1E1_Group_Interrupt_OR_FMPM_PM_DS1E1_LEV2_OR_Shift                                       0
#define cAf6_FMPM_PM_DS1E1_Group_Interrupt_OR_FMPM_PM_DS1E1_LEV2_OR_MaxVal                                     0x3
#define cAf6_FMPM_PM_DS1E1_Group_Interrupt_OR_FMPM_PM_DS1E1_LEV2_OR_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Group_Interrupt_OR_FMPM_PM_DS1E1_LEV2_OR_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM DS1E1 Framer Interrupt OR AND MASK Per DS1E1
Reg Addr   : 0x5B000-0x5B1FF
Reg Formula: 0x5B000+$H
    Where  : 
           + $H(0-1) : DS1/E1 Level-2 Group
Reg Desc   : 
FMPM PM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_Base                                 0x5B000
#define cAf6Reg_FMPM_PM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1(H)                           (0x5B000+(H))
#define cAf6Reg_FMPM_PM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_WidthVal                                      32
#define cAf6Reg_FMPM_PM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_OAMSK
BitField Type: R_O
BitField Desc: DS1/E1 Framer Interrupt
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_PM_DS1E1_OAMSK_Bit_Start                                       0
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_PM_DS1E1_OAMSK_Bit_End                                      23
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_PM_DS1E1_OAMSK_Mask                                cBit23_0
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_PM_DS1E1_OAMSK_Shift                                       0
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_PM_DS1E1_OAMSK_MaxVal                                0xffffff
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_PM_DS1E1_OAMSK_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1_FMPM_PM_DS1E1_OAMSK_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM DS1E1 Framer Interrupt MASK Per DS1E1
Reg Addr   : 0x5B200-0x5B3FF
Reg Formula: 0x5B200+$H
    Where  : 
           + $H(0-1) : DS1/E1 Group
Reg Desc   : 
FMPM PM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_Base                                     0x5B200
#define cAf6Reg_FMPM_PM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1(H)                                 (0x5B200+(H))
#define cAf6Reg_FMPM_PM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_WidthVal                                      32
#define cAf6Reg_FMPM_PM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_MSK
BitField Type: R/W
BitField Desc: DS1/E1 Framer Interrupt
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_PM_DS1E1_MSK_Bit_Start                                       0
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_PM_DS1E1_MSK_Bit_End                                      23
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_PM_DS1E1_MSK_Mask                                cBit23_0
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_PM_DS1E1_MSK_Shift                                       0
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_PM_DS1E1_MSK_MaxVal                                0xffffff
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_PM_DS1E1_MSK_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_FMPM_PM_DS1E1_MSK_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM DS1E1 Interrupt Sticky Per Type Per DS1E1
Reg Addr   : 0x70000-0x73FFF
Reg Formula: 0x70000+$H*0x20+$I
    Where  : 
           + $H(0-1) : DS1/E1 Group
           + $I(0-23) : DS1/E1
Reg Desc   : 
FMPM PM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_Base                                 0x70000
#define cAf6Reg_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1(H, I)                  (0x70000+(H)*0x20+(I))
#define cAf6Reg_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_WidthVal                                      32
#define cAf6Reg_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_STK_ES_L
BitField Type: W1C
BitField Desc: ES_L
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_L_Bit_Start                                       5
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_L_Bit_End                                       5
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_L_Mask                                   cBit5
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_L_Shift                                       5
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_L_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_L_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_L_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_STK_SES_L
BitField Type: W1C
BitField Desc: SES_L
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_L_Bit_Start                                       4
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_L_Bit_End                                       4
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_L_Mask                                   cBit4
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_L_Shift                                       4
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_L_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_L_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_L_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_STK_ES_P
BitField Type: W1C
BitField Desc: ES_P
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_P_Bit_Start                                       3
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_P_Bit_End                                       3
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_P_Mask                                   cBit3
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_P_Shift                                       3
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_P_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_P_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_P_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_STK_SES_P
BitField Type: W1C
BitField Desc: SES_P
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_P_Bit_Start                                       2
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_P_Bit_End                                       2
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_P_Mask                                   cBit2
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_P_Shift                                       2
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_P_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_P_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_P_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_STK_ES_PFE
BitField Type: W1C
BitField Desc: ES_PFE
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_PFE_Bit_Start                                       1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_PFE_Bit_End                                       1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_PFE_Mask                                   cBit1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_PFE_Shift                                       1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_PFE_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_PFE_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_ES_PFE_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_STK_SES_PFE
BitField Type: W1C
BitField Desc: SES_PFE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_PFE_Bit_Start                                       0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_PFE_Bit_End                                       0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_PFE_Mask                                   cBit0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_PFE_Shift                                       0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_PFE_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_PFE_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_STK_SES_PFE_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM DS1E1 Interrupt MASK Per Type Per DS1E1
Reg Addr   : 0x74000-0x77FFF
Reg Formula: 0x74000+$H*0x20+$I
    Where  : 
           + $H(0-1) : DS1/E1 Group
           + $I(0-23) : DS1/E1
Reg Desc   : 
FMPM PM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_Base                                   0x74000
#define cAf6Reg_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1(H, I)                   (0x74000+(H)*0x20+(I))
#define cAf6Reg_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_WidthVal                                      32
#define cAf6Reg_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_MSK_ES_L
BitField Type: R/W
BitField Desc: ES_L
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_L_Bit_Start                                       5
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_L_Bit_End                                       5
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_L_Mask                                   cBit5
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_L_Shift                                       5
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_L_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_L_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_L_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_MSK_SES_L
BitField Type: R/W
BitField Desc: SES_L
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_L_Bit_Start                                       4
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_L_Bit_End                                       4
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_L_Mask                                   cBit4
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_L_Shift                                       4
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_L_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_L_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_L_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_MSK_ES_P
BitField Type: R/W
BitField Desc: ES_P
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_P_Bit_Start                                       3
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_P_Bit_End                                       3
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_P_Mask                                   cBit3
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_P_Shift                                       3
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_P_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_P_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_P_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_MSK_SES_P
BitField Type: R/W
BitField Desc: SES_P
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_P_Bit_Start                                       2
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_P_Bit_End                                       2
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_P_Mask                                   cBit2
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_P_Shift                                       2
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_P_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_P_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_P_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_MSK_ES_PFE
BitField Type: R/W
BitField Desc: ES_PFE
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_PFE_Bit_Start                                       1
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_PFE_Bit_End                                       1
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_PFE_Mask                                   cBit1
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_PFE_Shift                                       1
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_PFE_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_PFE_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_ES_PFE_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_MSK_SES_PFE
BitField Type: R/W
BitField Desc: SES_PFE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_PFE_Bit_Start                                       0
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_PFE_Bit_End                                       0
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_PFE_Mask                                   cBit0
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_PFE_Shift                                       0
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_PFE_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_PFE_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_MSK_SES_PFE_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM DS1E1 Interrupt Current Status Per Type Per DS1E1
Reg Addr   : 0x78000-0x7BFFF
Reg Formula: 0x78000+$H*0x20+$I
    Where  : 
           + $H(0-1) : DS1/E1 Group
           + $I(0-23) : DS1/E1
Reg Desc   : 
FMPM FM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_Base                                 0x78000
#define cAf6Reg_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1(H, I)                  (0x78000+(H)*0x20+(I))
#define cAf6Reg_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_WidthVal                                      32
#define cAf6Reg_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_AMSK_ES_L
BitField Type: R_O
BitField Desc: ES_L
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_L_Bit_Start                                      21
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_L_Bit_End                                      21
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_L_Mask                                  cBit21
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_L_Shift                                      21
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_L_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_L_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_L_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_AMSK_SES_L
BitField Type: R_O
BitField Desc: SES_L
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_L_Bit_Start                                      20
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_L_Bit_End                                      20
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_L_Mask                                  cBit20
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_L_Shift                                      20
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_L_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_L_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_L_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_AMSK_ES_P
BitField Type: R_O
BitField Desc: ES_P
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_P_Bit_Start                                      19
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_P_Bit_End                                      19
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_P_Mask                                  cBit19
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_P_Shift                                      19
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_P_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_P_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_P_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_AMSK_SES_P
BitField Type: R_O
BitField Desc: SES_P
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_P_Bit_Start                                      18
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_P_Bit_End                                      18
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_P_Mask                                  cBit18
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_P_Shift                                      18
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_P_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_P_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_P_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_AMSK_ES_PFE
BitField Type: R_O
BitField Desc: ES_PFE
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_PFE_Bit_Start                                      17
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_PFE_Bit_End                                      17
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_PFE_Mask                                  cBit17
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_PFE_Shift                                      17
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_PFE_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_PFE_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_ES_PFE_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_DS1E1_AMSK_SES_PFE
BitField Type: R_O
BitField Desc: SES_PFE
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_PFE_Bit_Start                                      16
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_PFE_Bit_End                                      16
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_PFE_Mask                                  cBit16
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_PFE_Shift                                      16
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_PFE_MaxVal                                     0x1
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_PFE_MinVal                                     0x0
#define cAf6_FMPM_PM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1_FMPM_PM_DS1E1_AMSK_SES_PFE_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM PW Group Interrupt OR
Reg Addr   : 0x58004
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM PM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_PW_Group_Interrupt_OR_Base                                                     0x58004
#define cAf6Reg_FMPM_PM_PW_Group_Interrupt_OR                                                          0x58004
#define cAf6Reg_FMPM_PM_PW_Group_Interrupt_OR_WidthVal                                                      32
#define cAf6Reg_FMPM_PM_PW_Group_Interrupt_OR_WriteMask                                                    0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_GRP_OR
BitField Type: R_O
BitField Desc: PW Group Interrupt OR, bit per group
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Group_Interrupt_OR_FMPM_PM_PW_GRP_OR_Bit_Start                                       0
#define cAf6_FMPM_PM_PW_Group_Interrupt_OR_FMPM_PM_PW_GRP_OR_Bit_End                                         5
#define cAf6_FMPM_PM_PW_Group_Interrupt_OR_FMPM_PM_PW_GRP_OR_Mask                                      cBit5_0
#define cAf6_FMPM_PM_PW_Group_Interrupt_OR_FMPM_PM_PW_GRP_OR_Shift                                           0
#define cAf6_FMPM_PM_PW_Group_Interrupt_OR_FMPM_PM_PW_GRP_OR_MaxVal                                       0x3f
#define cAf6_FMPM_PM_PW_Group_Interrupt_OR_FMPM_PM_PW_GRP_OR_MinVal                                        0x0
#define cAf6_FMPM_PM_PW_Group_Interrupt_OR_FMPM_PM_PW_GRP_OR_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM PW Framer Interrupt OR AND MASK Per PW
Reg Addr   : 0x5B400-0x5B4FF
Reg Formula: 0x5B400+$J
    Where  : 
           + $J(0-5) : PW Group
Reg Desc   : 
FMPM PM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_Base                                    0x5B400
#define cAf6Reg_FMPM_PM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW(J)                                (0x5B400+(J))
#define cAf6Reg_FMPM_PM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_WidthVal                                      32
#define cAf6Reg_FMPM_PM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_OAMSK
BitField Type: R_O
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PM_PW_OAMSK_Bit_Start                                       0
#define cAf6_FMPM_PM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PM_PW_OAMSK_Bit_End                                      31
#define cAf6_FMPM_PM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PM_PW_OAMSK_Mask                                cBit31_0
#define cAf6_FMPM_PM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PM_PW_OAMSK_Shift                                       0
#define cAf6_FMPM_PM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PM_PW_OAMSK_MaxVal                              0xffffffff
#define cAf6_FMPM_PM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PM_PW_OAMSK_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PM_PW_OAMSK_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM PW Framer Interrupt MASK Per PW
Reg Addr   : 0x5B500-0x5B5FF
Reg Formula: 0x5B500+$J
    Where  : 
           + $J(0-5) : PW Group
Reg Desc   : 
FMPM PM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_PW_Framer_Interrupt_MASK_Per_PW_Base                                           0x5B500
#define cAf6Reg_FMPM_PM_PW_Framer_Interrupt_MASK_Per_PW(J)                                       (0x5B500+(J))
#define cAf6Reg_FMPM_PM_PW_Framer_Interrupt_MASK_Per_PW_WidthVal                                            32
#define cAf6Reg_FMPM_PM_PW_Framer_Interrupt_MASK_Per_PW_WriteMask                                          0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_MSK
BitField Type: R/W
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_PM_PW_MSK_Bit_Start                                       0
#define cAf6_FMPM_PM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_PM_PW_MSK_Bit_End                                      31
#define cAf6_FMPM_PM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_PM_PW_MSK_Mask                                cBit31_0
#define cAf6_FMPM_PM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_PM_PW_MSK_Shift                                       0
#define cAf6_FMPM_PM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_PM_PW_MSK_MaxVal                              0xffffffff
#define cAf6_FMPM_PM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_PM_PW_MSK_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Framer_Interrupt_MASK_Per_PW_FMPM_PM_PW_MSK_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM PW Interrupt Sticky Per Type Per PW
Reg Addr   : 0x5C000-0x5DFFF
Reg Formula: 0x5C000+$J*0x20+$K
    Where  : 
           + $J(0-5) : PW Group
           + $K(0-31) : PW-ID
Reg Desc   : 
FMPM PM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_Base                                       0x5C000
#define cAf6Reg_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW(J, K)                       (0x5C000+(J)*0x20+(K))
#define cAf6Reg_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_WidthVal                                        32
#define cAf6Reg_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_WriteMask                                      0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_STK_ES
BitField Type: W1C
BitField Desc: ES
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_Bit_Start                                       3
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_Bit_End                                       3
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_Mask                                   cBit3
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_Shift                                       3
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_MaxVal                                     0x1
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_STK_SES
BitField Type: W1C
BitField Desc: SES
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_Bit_Start                                       2
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_Bit_End                                       2
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_Mask                                   cBit2
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_Shift                                       2
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_MaxVal                                     0x1
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_STK_ES_FE
BitField Type: W1C
BitField Desc: ES_FE
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_FE_Bit_Start                                       1
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_FE_Bit_End                                       1
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_FE_Mask                                   cBit1
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_FE_Shift                                       1
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_FE_MaxVal                                     0x1
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_FE_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_ES_FE_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_STK_SES_FE
BitField Type: W1C
BitField Desc: SES_FE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_FE_Bit_Start                                       0
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_FE_Bit_End                                       0
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_FE_Mask                                   cBit0
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_FE_Shift                                       0
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_FE_MaxVal                                     0x1
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_FE_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PM_PW_STK_SES_FE_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM PW Interrupt MASK Per Type Per PW
Reg Addr   : 0x5E000-0x5FFFF
Reg Formula: 0x5E000+$J*0x20+$K
    Where  : 
           + $J(0-5) : PW Group
           + $K(0-31) : PW-ID
Reg Desc   : 
FMPM PM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_Base                                         0x5E000
#define cAf6Reg_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW(J, K)                         (0x5E000+(J)*0x20+(K))
#define cAf6Reg_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_WidthVal                                          32
#define cAf6Reg_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_WriteMask                                        0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_MSK_ES
BitField Type: R/W
BitField Desc: ES
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_Bit_Start                                       3
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_Bit_End                                       3
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_Mask                                   cBit3
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_Shift                                       3
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_MaxVal                                     0x1
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_MSK_SES
BitField Type: R/W
BitField Desc: SES
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_Bit_Start                                       2
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_Bit_End                                       2
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_Mask                                   cBit2
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_Shift                                       2
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_MaxVal                                     0x1
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_MSK_ES_FE
BitField Type: R/W
BitField Desc: ES_FE
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_FE_Bit_Start                                       1
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_FE_Bit_End                                       1
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_FE_Mask                                   cBit1
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_FE_Shift                                       1
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_FE_MaxVal                                     0x1
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_FE_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_ES_FE_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_MSK_SES_FE
BitField Type: R/W
BitField Desc: SES_FE
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_FE_Bit_Start                                       0
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_FE_Bit_End                                       0
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_FE_Mask                                   cBit0
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_FE_Shift                                       0
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_FE_MaxVal                                     0x1
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_FE_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Interrupt_MASK_Per_Type_Per_PW_FMPM_PM_PW_MSK_SES_FE_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PM PW Interrupt Current Status Per Type Per PW
Reg Addr   : 0x60000-0x61FFF
Reg Formula: 0x60000+$J*0x20+$K
    Where  : 
           + $J(0-5) : PW Group
           + $K(0-31) : PW-ID
Reg Desc   : 
FMPM PM Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_Base                                 0x60000
#define cAf6Reg_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW(J, K)                  (0x60000+(J)*0x20+(K))
#define cAf6Reg_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_WidthVal                                      32
#define cAf6Reg_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_AMSK_ES
BitField Type: R_O
BitField Desc: ES
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_Bit_Start                                      19
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_Bit_End                                      19
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_Mask                                  cBit19
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_Shift                                      19
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_MaxVal                                     0x1
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_AMSK_SES
BitField Type: R_O
BitField Desc: SES
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_Bit_Start                                      18
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_Bit_End                                      18
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_Mask                                  cBit18
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_Shift                                      18
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_MaxVal                                     0x1
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_AMSK_ES_FE
BitField Type: R_O
BitField Desc: ES_FE
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_FE_Bit_Start                                      17
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_FE_Bit_End                                      17
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_FE_Mask                                  cBit17
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_FE_Shift                                      17
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_FE_MaxVal                                     0x1
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_FE_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_ES_FE_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PM_PW_AMSK_SES_FE
BitField Type: R_O
BitField Desc: SES_FE
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_FE_Bit_Start                                      16
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_FE_Bit_End                                      16
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_FE_Mask                                  cBit16
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_FE_Shift                                      16
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_FE_MaxVal                                     0x1
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_FE_MinVal                                     0x0
#define cAf6_FMPM_PM_PW_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_PM_PW_AMSK_SES_FE_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Interrupt
Reg Addr   : 0x94000
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Interrupt_Base                                                             0x94000
#define cAf6Reg_FMPM_PW_Def_Interrupt                                                                  0x94000
#define cAf6Reg_FMPM_PW_Def_Interrupt_WidthVal                                                              32
#define cAf6Reg_FMPM_PW_Def_Interrupt_WriteMask                                                            0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_Intr_PW
BitField Type: R_O
BitField Desc: PW defect interrupt
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_FMPM_PW_Def_Intr_PW_Bit_Start                                             5
#define cAf6_FMPM_PW_Def_Interrupt_FMPM_PW_Def_Intr_PW_Bit_End                                               5
#define cAf6_FMPM_PW_Def_Interrupt_FMPM_PW_Def_Intr_PW_Mask                                              cBit5
#define cAf6_FMPM_PW_Def_Interrupt_FMPM_PW_Def_Intr_PW_Shift                                                 5
#define cAf6_FMPM_PW_Def_Interrupt_FMPM_PW_Def_Intr_PW_MaxVal                                              0x1
#define cAf6_FMPM_PW_Def_Interrupt_FMPM_PW_Def_Intr_PW_MinVal                                              0x0
#define cAf6_FMPM_PW_Def_Interrupt_FMPM_PW_Def_Intr_PW_RstVal                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Interrupt Mask
Reg Addr   : 0x94001
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Interrupt_Mask_Base                                                        0x94001
#define cAf6Reg_FMPM_PW_Def_Interrupt_Mask                                                             0x94001
#define cAf6Reg_FMPM_PW_Def_Interrupt_Mask_WidthVal                                                         32
#define cAf6Reg_FMPM_PW_Def_Interrupt_Mask_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_Intr_MSK_PW
BitField Type: R/W
BitField Desc: PW defect interrupt
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Mask_FMPM_PW_Def_Intr_MSK_PW_Bit_Start                                       5
#define cAf6_FMPM_PW_Def_Interrupt_Mask_FMPM_PW_Def_Intr_MSK_PW_Bit_End                                       5
#define cAf6_FMPM_PW_Def_Interrupt_Mask_FMPM_PW_Def_Intr_MSK_PW_Mask                                     cBit5
#define cAf6_FMPM_PW_Def_Interrupt_Mask_FMPM_PW_Def_Intr_MSK_PW_Shift                                        5
#define cAf6_FMPM_PW_Def_Interrupt_Mask_FMPM_PW_Def_Intr_MSK_PW_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Mask_FMPM_PW_Def_Intr_MSK_PW_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Mask_FMPM_PW_Def_Intr_MSK_PW_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Level1 Group Interrupt OR
Reg Addr   : 0x98004
Reg Formula: 
    Where  : 
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Level1_Group_Interrupt_OR_Base                                             0x98004
#define cAf6Reg_FMPM_PW_Def_Level1_Group_Interrupt_OR                                                  0x98004
#define cAf6Reg_FMPM_PW_Def_Level1_Group_Interrupt_OR_WidthVal                                              32
#define cAf6Reg_FMPM_PW_Def_Level1_Group_Interrupt_OR_WriteMask                                            0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_GRP_OR
BitField Type: R_O
BitField Desc: PW Group Interrupt OR, bit per group
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Level1_Group_Interrupt_OR_FMPM_PW_Def_GRP_OR_Bit_Start                                       0
#define cAf6_FMPM_PW_Def_Level1_Group_Interrupt_OR_FMPM_PW_Def_GRP_OR_Bit_End                                       5
#define cAf6_FMPM_PW_Def_Level1_Group_Interrupt_OR_FMPM_PW_Def_GRP_OR_Mask                                 cBit5_0
#define cAf6_FMPM_PW_Def_Level1_Group_Interrupt_OR_FMPM_PW_Def_GRP_OR_Shift                                       0
#define cAf6_FMPM_PW_Def_Level1_Group_Interrupt_OR_FMPM_PW_Def_GRP_OR_MaxVal                                    0x3f
#define cAf6_FMPM_PW_Def_Level1_Group_Interrupt_OR_FMPM_PW_Def_GRP_OR_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Level1_Group_Interrupt_OR_FMPM_PW_Def_GRP_OR_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Framer Interrupt OR AND MASK Per PW
Reg Addr   : 0x9B400-0x9B4FF
Reg Formula: 0x9B400+$J
    Where  : 
           + $J(0-5) : PW Group
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW_Base                                   0x9B400
#define cAf6Reg_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW(J)                               (0x9B400+(J))
#define cAf6Reg_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW_WidthVal                                      32
#define cAf6Reg_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_OAMSK
BitField Type: R_O
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PW_Def_OAMSK_Bit_Start                                       0
#define cAf6_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PW_Def_OAMSK_Bit_End                                      31
#define cAf6_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PW_Def_OAMSK_Mask                                cBit31_0
#define cAf6_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PW_Def_OAMSK_Shift                                       0
#define cAf6_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PW_Def_OAMSK_MaxVal                              0xffffffff
#define cAf6_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PW_Def_OAMSK_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW_FMPM_PW_Def_OAMSK_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Framer Interrupt MASK Per PW
Reg Addr   : 0x9B500-0x9B5FF
Reg Formula: 0x9B500+$J
    Where  : 
           + $J(0-5) : PW Group
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_Base                                          0x9B500
#define cAf6Reg_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW(J)                                      (0x9B500+(J))
#define cAf6Reg_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_WidthVal                                           32
#define cAf6Reg_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_WriteMask                                         0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK
BitField Type: R/W
BitField Desc: Interrupt OR AND MASK per PW, bit per PW
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_FMPM_PW_Def_MSK_Bit_Start                                       0
#define cAf6_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_FMPM_PW_Def_MSK_Bit_End                                      31
#define cAf6_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_FMPM_PW_Def_MSK_Mask                                cBit31_0
#define cAf6_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_FMPM_PW_Def_MSK_Shift                                       0
#define cAf6_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_FMPM_PW_Def_MSK_MaxVal                              0xffffffff
#define cAf6_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_FMPM_PW_Def_MSK_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW_FMPM_PW_Def_MSK_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Interrupt Sticky Per Type Per PW
Reg Addr   : 0x9C000-0x9DFFF
Reg Formula: 0x9C000+$J*0x20+$K
    Where  : 
           + $J(0-5) : PW Group
           + $K(0-31) : PW-ID
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_Base                                      0x9C000
#define cAf6Reg_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW(J, K)                      (0x9C000+(J)*0x20+(K))
#define cAf6Reg_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_WidthVal                                       32
#define cAf6Reg_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_RBIT
BitField Type: W1C
BitField Desc: RBIT
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_Bit_Start                                       7
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_Bit_End                                       7
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_Mask                                   cBit7
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_Shift                                       7
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_STRAY
BitField Type: W1C
BitField Desc: STRAY
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_Bit_Start                                       6
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_Bit_End                                       6
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_Mask                                   cBit6
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_Shift                                       6
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_MALF
BitField Type: W1C
BitField Desc: MALF
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_Bit_Start                                       5
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_Bit_End                                       5
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_Mask                                   cBit5
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_Shift                                       5
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_LBIT
BitField Type: W1C
BitField Desc: LBIT
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_Bit_Start                                       4
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_Bit_End                                       4
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_Mask                                   cBit4
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_Shift                                       4
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_BufUder
BitField Type: W1C
BitField Desc: BufUder
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_Bit_Start                                       3
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_Bit_End                                       3
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_Mask                                   cBit3
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_Shift                                       3
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_BufOver
BitField Type: W1C
BitField Desc: BufOver
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_Bit_Start                                       2
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_Bit_End                                       2
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_Mask                                   cBit2
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_Shift                                       2
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_LOPS
BitField Type: W1C
BitField Desc: LOPS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_Bit_Start                                       1
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_Bit_End                                       1
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_Mask                                   cBit1
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_Shift                                       1
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_LOPSTA
BitField Type: W1C
BitField Desc: LOPSTA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_Bit_Start                                       0
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_Bit_End                                       0
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_Mask                                   cBit0
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_Shift                                       0
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Interrupt MASK Per Type Per PW
Reg Addr   : 0x9E000-0x9FFFF
Reg Formula: 0x9E000+$J*0x20+$K
    Where  : 
           + $J(0-5) : PW Group
           + $K(0-31) : PW-ID
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_Base                                        0x9E000
#define cAf6Reg_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW(J, K)                        (0x9E000+(J)*0x20+(K))
#define cAf6Reg_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_WidthVal                                         32
#define cAf6Reg_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_WriteMask                                       0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_RBIT
BitField Type: R/W
BitField Desc: RBIT
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_Bit_Start                                       7
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_Bit_End                                       7
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_Mask                                   cBit7
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_Shift                                       7
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_RBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_STRAY
BitField Type: R/W
BitField Desc: STRAY
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_Bit_Start                                       6
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_Bit_End                                       6
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_Mask                                   cBit6
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_Shift                                       6
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_STRAY_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_MALF
BitField Type: R/W
BitField Desc: MALF
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_Bit_Start                                       5
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_Bit_End                                       5
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_Mask                                   cBit5
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_Shift                                       5
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_MALF_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_LBIT
BitField Type: R/W
BitField Desc: LBIT
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_Bit_Start                                       4
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_Bit_End                                       4
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_Mask                                   cBit4
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_Shift                                       4
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_BufUder
BitField Type: R/W
BitField Desc: BufUder
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_Bit_Start                                       3
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_Bit_End                                       3
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_Mask                                   cBit3
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_Shift                                       3
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufUder_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_BufOver
BitField Type: R/W
BitField Desc: BufOver
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_Bit_Start                                       2
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_Bit_End                                       2
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_Mask                                   cBit2
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_Shift                                       2
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_BufOver_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_LOPS
BitField Type: R/W
BitField Desc: LOPS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_Bit_Start                                       1
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_Bit_End                                       1
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_Mask                                   cBit1
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_Shift                                       1
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPS_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_PW_Def_MSK_LOPSTA
BitField Type: R/W
BitField Desc: LOPSTA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_Bit_Start                                       0
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_Bit_End                                       0
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_Mask                                   cBit0
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_Shift                                       0
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW_FMPM_PW_Def_MSK_LOPSTA_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : FMPM PW Def Interrupt Current Status Per Type Per PW
Reg Addr   : 0xA0000-0xA1FFF
Reg Formula: 0xA0000+$J*0x20+$K
    Where  : 
           + $J(0-5) : PW Group
           + $K(0-31) : PW-ID
Reg Desc   : 
FMPM PW defect Interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_Base                                 0xA0000
#define cAf6Reg_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW(J, K)                  (0xA0000+(J)*0x20+(K))
#define cAf6Reg_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_WidthVal                                      32
#define cAf6Reg_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_WriteMask                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_RBIT
BitField Type: R_O
BitField Desc: RBIT
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Bit_Start                                      23
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Bit_End                                      23
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Mask                                  cBit23
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_Shift                                      23
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_RBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_STRAY
BitField Type: R_O
BitField Desc: STRAY
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Bit_Start                                      22
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Bit_End                                      22
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Mask                                  cBit22
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_Shift                                      22
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_STRAY_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_MALF
BitField Type: R_O
BitField Desc: MALF
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Bit_Start                                      21
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Bit_End                                      21
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Mask                                  cBit21
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_Shift                                      21
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_MALF_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LBIT
BitField Type: R_O
BitField Desc: LBIT
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Bit_Start                                      20
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Bit_End                                      20
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Mask                                  cBit20
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_Shift                                      20
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LBIT_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_BufUder
BitField Type: R_O
BitField Desc: BufUder
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Bit_Start                                      19
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Bit_End                                      19
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Mask                                  cBit19
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_Shift                                      19
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufUder_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_BufOver
BitField Type: R_O
BitField Desc: BufOver
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Bit_Start                                      18
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Bit_End                                      18
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Mask                                  cBit18
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_Shift                                      18
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_BufOver_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LOPS
BitField Type: R_O
BitField Desc: LOPS
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_Bit_Start                                      17
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_Bit_End                                      17
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_Mask                                  cBit17
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_Shift                                      17
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPS_RstVal                                     0x0

/*--------------------------------------
BitField Name: FMPM_FM_PW_AMSK_LOPSTA
BitField Type: R_O
BitField Desc: LOPSTA
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Bit_Start                                      16
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Bit_End                                      16
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Mask                                  cBit16
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_Shift                                      16
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_MaxVal                                     0x1
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_MinVal                                     0x0
#define cAf6_FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW_FMPM_FM_PW_AMSK_LOPSTA_RstVal                                     0x0

#endif /* _AF6_REG_AF6CCI0021_RD_PM_H_ */

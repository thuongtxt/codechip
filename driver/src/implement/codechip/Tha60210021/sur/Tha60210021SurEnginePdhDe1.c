/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60210021HardSurEnginePdhDe1.c
 *
 * Created Date: Mar 21, 2015
 *
 * Description : DS1/E1 Surveillance engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../default/sur/hard/ThaModuleHardSurInternal.h"
#include "../../../default/sur/hard/engine/ThaHardSurEngineInternal.h"
#include "Tha60210021ModuleSur.h"
#include "Tha60210021ModuleSurReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021SurEnginePdhDe1
    {
    tThaHardSurEnginePdhDe1 super;
    }tTha60210021SurEnginePdhDe1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSurEngineMethods      m_AtSurEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultOffset(AtSurEngine self)
    {
    uint32 de1Id = AtChannelHwIdGet(AtSurEngineChannelGet(self));
    return ((de1Id / 24UL) << 5UL) | (de1Id % 24UL);
    }

static ThaModuleHardSur Module(AtSurEngine self)
    {
    return (ThaModuleHardSur)AtSurEngineModuleGet(self);
    }

static eAtRet HwNotificationEnable(AtSurEngine self, uint32 baseAddr, eBool enable)
    {
    ThaModuleHardSur module = Module(self);
    uint32 de1Id = AtChannelHwIdGet(AtSurEngineChannelGet(self));
    uint32 regAddr = ThaModuleHardSurBaseAddress(module) + baseAddr + (de1Id / 24);
    uint32 bitMask = cBit0 << (de1Id % 24);
    uint32 regVal  = mModuleHwRead(module, regAddr);

    regVal = (enable) ? (regVal | bitMask) : (regVal & ~bitMask);
    mModuleHwWrite(module, regAddr, regVal);

    return cAtOk;
    }

static eBool HwNotificationIsEnabled(AtSurEngine self, uint32 baseAddr)
    {
    ThaModuleHardSur module = Module(self);
    uint32 de1Id = AtChannelHwIdGet(AtSurEngineChannelGet(self));
    uint32 regAddr = ThaModuleHardSurBaseAddress(module) + baseAddr + (de1Id / 24);
    uint32 bitMask = cBit0 << (de1Id % 24);
    uint32 regVal  = mModuleHwRead(module, regAddr);
    return (regVal & bitMask) ? cAtTrue : cAtFalse;
    }

static eAtRet FailureNotificationEnable(AtSurEngine self, eBool enable)
    {
    return HwNotificationEnable(self, cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_Base, enable);
    }

static eBool FailureNotificationIsEnabled(AtSurEngine self)
    {
    return HwNotificationIsEnabled(self, cAf6Reg_FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1_Base);
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, mMethodsGet(self), sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, DefaultOffset);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationEnable);
        mMethodOverride(m_AtSurEngineOverride, FailureNotificationIsEnabled);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtSurEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021SurEnginePdhDe1);
    }

static AtSurEngine ObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (ThaHardSurEnginePdhDe1ObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngine Tha60210021SurEnginePdhDe1New(AtModuleSur module, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSurEngine newSurEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSurEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newSurEngine, module, channel);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60210021ModuleMap.c
 *
 * Created Date: May 19, 2015
 *
 * Description : MAP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/map/ThaModuleMapInternal.h"
#include "../../../default/util/ThaUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaMapChnTypeMask                          cBit17_16
#define cThaMapChnTypeShift                         16

/*--------------------------------------
 BitField Name: MapPWIdField[9:0]
 BitField Type: R/W
 BitField Desc: PWID for the DS3/E3 physical port in SAToP mode
 BitField Bits: 17_8
 --------------------------------------*/
#define cThaMapFirstTsMask                          cBit15
#define cThaMapFirstTsShift                         15

#define cThaMapTsEnMask                             cBit14
#define cThaMapTsEnShift                            14

#define cThaMapChnIdMask                            cBit13_8
#define cThaMapChnIdShift                           8

#define cThaMapPWIdFieldMask                        cBit7_0
#define cThaMapPWIdFieldShift                       0

/*--------------------------------------
 BitField Name: cThaMapTmDs1LoopcodeMask
 BitField Type: R/W
 BitField Desc: bit [10:8] MapDs1Loopcode[2:0]:
 BitField Bits: 10:8
 --------------------------------------*/
#define cThaMapTmDs1LoopcodeMask                     cBit10_8
#define cThaMapTmDs1LoopcodeShift                    8

/*--------------------------------------
 BitField Name: cThaMapTmDs1E1FrmMdMask
 BitField Type: R/W
 BitField Desc: bit [6] MapDs1FrmMode[0]:
 1: DS1 SF/ESF mode
 0: Other modes (default)
 BitField Bits: 6
 --------------------------------------*/
#define cThaMapTmDs1E1FrmMdMask                      cBit7
#define cThaMapTmDs1E1FrmMdShift                     7

/*--------------------------------------
 BitField Name: MapTimeDs1E1Md
 BitField Type: R/W
 BitField Desc: MapTimeDs1E1Md[0]:
 0: E1 mode (default)
 1: DS1 mode
 BitField Bits: 5
 --------------------------------------*/
#define cThaMapTmDs1E1MdMask                      cBit6
#define cThaMapTmDs1E1MdShift                     6

/*--------------------------------------
 BitField Name: MaptimeSrcId
 BitField Type: R/W
 BitField Desc: The physical port ID uses for timing reference.
 BitField Bits: 4_0
 --------------------------------------*/
#define cThaMapTmSrcIdMask                      cBit5_0
#define cThaMapTmSrcIdShift                     0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021ModuleMap
    {
    tThaModuleMap super;
    }tTha60210021ModuleMap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleMapMethods m_ThaModuleMapOverride;

/* Save super implementations */
static const tThaModuleMapMethods *m_ThaModuleMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleMap, MapChnType)
mDefineMaskShift(ThaModuleMap, MapFirstTs)
mDefineMaskShift(ThaModuleMap, MapTsEn)
mDefineMaskShift(ThaModuleMap, MapPWIdField)

mDefineMaskShift(ThaModuleMap, MapTmDs1Loopcode)
mDefineMaskShift(ThaModuleMap, MapTmDs1E1FrmMd)
mDefineMaskShift(ThaModuleMap, MapTmDs1E1Md)
mDefineMaskShift(ThaModuleMap, MapTmSrcId)

static uint32 De1MapChnCtrl(ThaModuleMap self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return 0x1A1000;
    }

static void OverrideThaModuleMap(ThaModuleMap self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleMapMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, m_ThaModuleMapMethods, sizeof(m_ThaModuleMapOverride));

        mMethodOverride(m_ThaModuleMapOverride, De1MapChnCtrl);

        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapChnType)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapFirstTs)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTsEn)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapPWIdField)

        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTmDs1Loopcode)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTmDs1E1FrmMd)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTmDs1E1Md)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTmSrcId)
        }

    mMethodsSet(self, &m_ThaModuleMapOverride);
    }

static void Override(ThaModuleMap self)
    {
    OverrideThaModuleMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModuleMap);
    }

static AtModule Tha60210021ModuleMapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override((ThaModuleMap)self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210021ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021ModuleMapObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DEMAP
 *
 * File        : Tha60210021ModuleDemap.c
 *
 * Created Date: Jun 2, 2015
 *
 * Description : DEMAO module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/map/ThaModuleDemapInternal.h"
#include "../../../default/util/ThaUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaDmapChnTypeMask  cBit12_10
#define cThaDmapChnTypeShift 10
#define cThaDmapFirstTsMask  cBit9
#define cThaDmapFirstTsShift 9
#define cThaDmapTsEnMask     cBit8
#define cThaDmapTsEnShift    8
#define cThaDmapPwIdMask     cBit7_0
#define cThaDmapPwIdShift    0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021ModuleDemap
    {
    tThaModuleDemap super;
    }tTha60210021ModuleDemap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleDemapMethods m_ThaModuleDemapOverride;

/* Save super implementations */
static const tThaModuleDemapMethods *m_ThaModuleDemapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleDemap, DmapChnType)
mDefineMaskShift(ThaModuleDemap, DmapFirstTs)
mDefineMaskShift(ThaModuleDemap, DmapTsEn)
mDefineMaskShift(ThaModuleDemap, DmapPwId)

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModuleDemap);
    }

static void OverrideThaModuleDemap(ThaModuleDemap self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleDemapMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDemapOverride, m_ThaModuleDemapMethods, sizeof(m_ThaModuleDemapOverride));

        /* Override bit fields */
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapChnType)
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapFirstTs)
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapTsEn)
        mBitFieldOverride(ThaModuleDemap, m_ThaModuleDemapOverride, DmapPwId)
        }

    mMethodsSet(self, &m_ThaModuleDemapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleDemap((ThaModuleDemap)self);
    }

static AtModule Tha60210021ModuleDemapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210021ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021ModuleDemapObjectInit(newModule, device);
    }

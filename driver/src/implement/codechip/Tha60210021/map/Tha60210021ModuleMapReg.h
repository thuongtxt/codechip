/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0021_RD_MAP_H_
#define _AF6_REG_AF6CCI0021_RD_MAP_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Thalassa Demap Channel Control
Reg Addr   : 0x000000 - 0x0007FF
Reg Formula: 0x000000 + liuid[5:0]*32 + tsid[4:0]
    Where  : 
           + $liuid(0 - 47):
           + $tsid(0 - 31):
Reg Desc   : 
The registers provide the pseudo-wire configurations for PDH ? pseudo-wire side..

------------------------------------------------------------------------------*/
#define cAf6Reg_demap_channel_ctrl_Base                                                               0x000000
#define cAf6Reg_demap_channel_ctrl(liuid, tsid)                         (0x000000+(liuid)[5:0]*32+(tsid)[4:0])
#define cAf6Reg_demap_channel_ctrl_WidthVal                                                                 32
#define cAf6Reg_demap_channel_ctrl_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: DemapChannelType
BitField Type: RW
BitField Desc: Specify which type of mapping for this. 0: SAToP encapsulation
from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: CESoP pseudowire with
CAS
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapChannelType_Bit_Start                                                  10
#define cAf6_demap_channel_ctrl_DemapChannelType_Bit_End                                                    12
#define cAf6_demap_channel_ctrl_DemapChannelType_Mask                                                cBit12_10
#define cAf6_demap_channel_ctrl_DemapChannelType_Shift                                                      10
#define cAf6_demap_channel_ctrl_DemapChannelType_MaxVal                                                    0x7
#define cAf6_demap_channel_ctrl_DemapChannelType_MinVal                                                    0x0
#define cAf6_demap_channel_ctrl_DemapChannelType_RstVal                                                    0x0

/*--------------------------------------
BitField Name: DemapFirstTs
BitField Type: RW
BitField Desc: First timeslot indication. Use for CESoP mode only.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapFirstTs_Bit_Start                                                       9
#define cAf6_demap_channel_ctrl_DemapFirstTs_Bit_End                                                         9
#define cAf6_demap_channel_ctrl_DemapFirstTs_Mask                                                        cBit9
#define cAf6_demap_channel_ctrl_DemapFirstTs_Shift                                                           9
#define cAf6_demap_channel_ctrl_DemapFirstTs_MaxVal                                                        0x1
#define cAf6_demap_channel_ctrl_DemapFirstTs_MinVal                                                        0x0
#define cAf6_demap_channel_ctrl_DemapFirstTs_RstVal                                                        0x0

/*--------------------------------------
BitField Name: DemapTsEn
BitField Type: RW
BitField Desc: Enable for DS0 timeslot.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapTsEn_Bit_Start                                                          8
#define cAf6_demap_channel_ctrl_DemapTsEn_Bit_End                                                            8
#define cAf6_demap_channel_ctrl_DemapTsEn_Mask                                                           cBit8
#define cAf6_demap_channel_ctrl_DemapTsEn_Shift                                                              8
#define cAf6_demap_channel_ctrl_DemapTsEn_MaxVal                                                           0x1
#define cAf6_demap_channel_ctrl_DemapTsEn_MinVal                                                           0x0
#define cAf6_demap_channel_ctrl_DemapTsEn_RstVal                                                           0x0

/*--------------------------------------
BitField Name: DemapPwId
BitField Type: RW
BitField Desc: Pseudo-wire ID.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapPwId_Bit_Start                                                          0
#define cAf6_demap_channel_ctrl_DemapPwId_Bit_End                                                            7
#define cAf6_demap_channel_ctrl_DemapPwId_Mask                                                         cBit7_0
#define cAf6_demap_channel_ctrl_DemapPwId_Shift                                                              0
#define cAf6_demap_channel_ctrl_DemapPwId_MaxVal                                                          0xff
#define cAf6_demap_channel_ctrl_DemapPwId_MinVal                                                           0x0
#define cAf6_demap_channel_ctrl_DemapPwId_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Thalassa Map Channel Control
Reg Addr   : 0x001000 - 0x0017FF
Reg Formula: 0x001000 + liuid[5:0]*32 + tsid[4:0]
    Where  : 
           + $liuid(0 - 47):
           + $tsid(0 - 31):
Reg Desc   : 
The registers provide the pseudo-wire configurations for pseudo-wire to PDH side..

------------------------------------------------------------------------------*/
#define cAf6Reg_map_channel_ctrl_Base                                                                 0x001000
#define cAf6Reg_map_channel_ctrl(liuid, tsid)                           (0x001000+(liuid)[5:0]*32+(tsid)[4:0])
#define cAf6Reg_map_channel_ctrl_WidthVal                                                                   32
#define cAf6Reg_map_channel_ctrl_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: MapChannelType
BitField Type: RW
BitField Desc: Specify which type of mapping for this. 0: SAToP encapsulation
from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: CESoP pseudowire with
CAS
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_map_channel_ctrl_MapChannelType_Bit_Start                                                      16
#define cAf6_map_channel_ctrl_MapChannelType_Bit_End                                                        17
#define cAf6_map_channel_ctrl_MapChannelType_Mask                                                    cBit17_16
#define cAf6_map_channel_ctrl_MapChannelType_Shift                                                          16
#define cAf6_map_channel_ctrl_MapChannelType_MaxVal                                                        0x3
#define cAf6_map_channel_ctrl_MapChannelType_MinVal                                                        0x0
#define cAf6_map_channel_ctrl_MapChannelType_RstVal                                                        0x0

/*--------------------------------------
BitField Name: MapFirstTs
BitField Type: RW
BitField Desc: First timeslot indication. Use for CESoP mode only.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_map_channel_ctrl_MapFirstTs_Bit_Start                                                          15
#define cAf6_map_channel_ctrl_MapFirstTs_Bit_End                                                            15
#define cAf6_map_channel_ctrl_MapFirstTs_Mask                                                           cBit15
#define cAf6_map_channel_ctrl_MapFirstTs_Shift                                                              15
#define cAf6_map_channel_ctrl_MapFirstTs_MaxVal                                                            0x1
#define cAf6_map_channel_ctrl_MapFirstTs_MinVal                                                            0x0
#define cAf6_map_channel_ctrl_MapFirstTs_RstVal                                                            0x0

/*--------------------------------------
BitField Name: MapTsEn
BitField Type: RW
BitField Desc: Enable for DS0 timeslot.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_map_channel_ctrl_MapTsEn_Bit_Start                                                             14
#define cAf6_map_channel_ctrl_MapTsEn_Bit_End                                                               14
#define cAf6_map_channel_ctrl_MapTsEn_Mask                                                              cBit14
#define cAf6_map_channel_ctrl_MapTsEn_Shift                                                                 14
#define cAf6_map_channel_ctrl_MapTsEn_MaxVal                                                               0x1
#define cAf6_map_channel_ctrl_MapTsEn_MinVal                                                               0x0
#define cAf6_map_channel_ctrl_MapTsEn_RstVal                                                               0x0

/*--------------------------------------
BitField Name: MapChannelId
BitField Type: RW
BitField Desc: Channel ID.
BitField Bits: [13:8]
--------------------------------------*/
#define cAf6_map_channel_ctrl_MapChannelId_Bit_Start                                                         8
#define cAf6_map_channel_ctrl_MapChannelId_Bit_End                                                          13
#define cAf6_map_channel_ctrl_MapChannelId_Mask                                                       cBit13_8
#define cAf6_map_channel_ctrl_MapChannelId_Shift                                                             8
#define cAf6_map_channel_ctrl_MapChannelId_MaxVal                                                         0x3f
#define cAf6_map_channel_ctrl_MapChannelId_MinVal                                                          0x0
#define cAf6_map_channel_ctrl_MapChannelId_RstVal                                                          0x0

/*--------------------------------------
BitField Name: MapPwId
BitField Type: RW
BitField Desc: Pseudo-wire ID.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_map_channel_ctrl_MapPwId_Bit_Start                                                              0
#define cAf6_map_channel_ctrl_MapPwId_Bit_End                                                                7
#define cAf6_map_channel_ctrl_MapPwId_Mask                                                             cBit7_0
#define cAf6_map_channel_ctrl_MapPwId_Shift                                                                  0
#define cAf6_map_channel_ctrl_MapPwId_MaxVal                                                              0xff
#define cAf6_map_channel_ctrl_MapPwId_MinVal                                                               0x0
#define cAf6_map_channel_ctrl_MapPwId_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Thalassa Map Timing Reference Control
Reg Addr   : 0x000800 - 0x00083F
Reg Formula: 0x000800 + liuid[5:0]
    Where  : 
           + $liuid(0 - 47):
Reg Desc   : 
The registers provide the timing reference source for physical DS1/E1 ports.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_timing_reference_ctrl_Base                                                        0x000800
#define cAf6Reg_map_timing_reference_ctrl(liuid)                                       (0x000800+(liuid)[5:0])
#define cAf6Reg_map_timing_reference_ctrl_WidthVal                                                          32
#define cAf6Reg_map_timing_reference_ctrl_WriteMask                                                        0x0

/*--------------------------------------
BitField Name: MapDs1LcEn
BitField Type: RW
BitField Desc: DS1E1 Loop code insert enable 0: Disable 1: CSU Loop up 2: CSU
Loop down 3: FA1 Loop up 4: FA1 Loop down 5: FA2 Loop up 6: FA2 Loop down
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_map_timing_reference_ctrl_MapDs1LcEn_Bit_Start                                                  8
#define cAf6_map_timing_reference_ctrl_MapDs1LcEn_Bit_End                                                   10
#define cAf6_map_timing_reference_ctrl_MapDs1LcEn_Mask                                                cBit10_8
#define cAf6_map_timing_reference_ctrl_MapDs1LcEn_Shift                                                      8
#define cAf6_map_timing_reference_ctrl_MapDs1LcEn_MaxVal                                                   0x7
#define cAf6_map_timing_reference_ctrl_MapDs1LcEn_MinVal                                                   0x0
#define cAf6_map_timing_reference_ctrl_MapDs1LcEn_RstVal                                                   0x0

/*--------------------------------------
BitField Name: MapDs1FrmMode
BitField Type: RW
BitField Desc: DS1E1 Frame mode 0: Unfrm mode (default) 1: DS1 SF/ESF mode
BitField Bits: [7]
--------------------------------------*/
#define cAf6_map_timing_reference_ctrl_MapDs1FrmMode_Bit_Start                                               7
#define cAf6_map_timing_reference_ctrl_MapDs1FrmMode_Bit_End                                                 7
#define cAf6_map_timing_reference_ctrl_MapDs1FrmMode_Mask                                                cBit7
#define cAf6_map_timing_reference_ctrl_MapDs1FrmMode_Shift                                                   7
#define cAf6_map_timing_reference_ctrl_MapDs1FrmMode_MaxVal                                                0x1
#define cAf6_map_timing_reference_ctrl_MapDs1FrmMode_MinVal                                                0x0
#define cAf6_map_timing_reference_ctrl_MapDs1FrmMode_RstVal                                                0x0

/*--------------------------------------
BitField Name: MapDs1E1Mode
BitField Type: RW
BitField Desc: DS1E1 Line Type mode 0: E1 mode (default) 1: DS1 mode
BitField Bits: [6]
--------------------------------------*/
#define cAf6_map_timing_reference_ctrl_MapDs1E1Mode_Bit_Start                                                6
#define cAf6_map_timing_reference_ctrl_MapDs1E1Mode_Bit_End                                                  6
#define cAf6_map_timing_reference_ctrl_MapDs1E1Mode_Mask                                                 cBit6
#define cAf6_map_timing_reference_ctrl_MapDs1E1Mode_Shift                                                    6
#define cAf6_map_timing_reference_ctrl_MapDs1E1Mode_MaxVal                                                 0x1
#define cAf6_map_timing_reference_ctrl_MapDs1E1Mode_MinVal                                                 0x0
#define cAf6_map_timing_reference_ctrl_MapDs1E1Mode_RstVal                                                 0x0

/*--------------------------------------
BitField Name: MaptimeSrcId
BitField Type: RW
BitField Desc: The physical port ID uses for timing reference.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_map_timing_reference_ctrl_MaptimeSrcId_Bit_Start                                                0
#define cAf6_map_timing_reference_ctrl_MaptimeSrcId_Bit_End                                                  5
#define cAf6_map_timing_reference_ctrl_MaptimeSrcId_Mask                                               cBit5_0
#define cAf6_map_timing_reference_ctrl_MaptimeSrcId_Shift                                                    0
#define cAf6_map_timing_reference_ctrl_MaptimeSrcId_MaxVal                                                0x3f
#define cAf6_map_timing_reference_ctrl_MaptimeSrcId_MinVal                                                 0x0
#define cAf6_map_timing_reference_ctrl_MaptimeSrcId_RstVal                                                 0x0

#endif /* _AF6_REG_AF6CCI0021_RD_MAP_H_ */

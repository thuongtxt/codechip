/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210021PwBackupPsnController.c
 *
 * Created Date: Aug 28, 2015
 *
 * Description : Backup Psn Controller, this controller current is used in case of simulation only
 *               implementation need to be updated when HW support this feature
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/man/AtDriverInternal.h"
#include "../../../../../generic/pw/psn/AtPwPsnInternal.h"
#include "Tha60210021PwHeaderController.h"
#include "../../pwe/Tha60210021ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwHeaderControllerMethods m_ThaPwHeaderControllerOverride;

/* Save super implementation */
static const tThaPwHeaderControllerMethods *m_ThaPwHeaderControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedProtectOnTxDirection(ThaPwHeaderController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePwRet HeaderRead(ThaPwHeaderController self, uint8 *buffer, uint8 headerLengthInByte)
    {
    return mMethodsGet(self)->HeaderRead(ThaPwAdapterHeaderController(ThaPwHeaderControllerAdapterGet(self)), buffer, headerLengthInByte);
    }

static eAtModulePwRet HeaderWrite(ThaPwHeaderController self, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn, eThaPwAdapterHeaderWriteMode writeMode)
    {
    AtUnused(self);
    AtUnused(buffer);
    AtUnused(headerLenInByte);
    AtUnused(psn);
    AtUnused(writeMode);
    return cAtOk;
    }

static ThaModulePweV2 PweModule(ThaPwHeaderController self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaPwHeaderControllerAdapterGet(self));
    return (ThaModulePweV2)AtDeviceModuleGet(device, cThaModulePwe);
    }

static uint32 HwHsPwLabelBuild(ThaPwHeaderController self, AtPwPsn psn)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPw adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    eAtPwPsnType psnType = AtPwPsnTypeGet(psn);
    uint32 buffSize;
    uint8 lenght, *buffer;
    buffer = ThaModulePwSharedPsnBuffer((ThaModulePw)AtChannelModuleGet((AtChannel)adapter), &buffSize);
    mMethodsGet(osal)->MemInit(osal, buffer, 0, sizeof(uint8) * buffSize);
    lenght = AtPwPsnHeaderBuild(psn, adapter, buffer);

    if (lenght < 4)
        return 0;

    if (psnType == cAtPwPsnTypeUdp)
        return ThaPktUtilPutDataToRegister(&buffer[lenght - 8], 0);

    return ThaPktUtilPutDataToRegister(&buffer[lenght - 4], 0);
    }

static eAtModulePwRet PsnSet(ThaPwHeaderController self, AtPwPsn psn)
    {
    AtPw adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    AtPwPsn primaryPsn = AtPwPsnGet(adapter);
    eAtPwPsnType primaryPsnType = AtPwPsnTypeGet(primaryPsn);
    eAtPwPsnType psnType = AtPwPsnTypeGet(psn);

    if (psnType != primaryPsnType)
        return cAtErrorInvlParm;

    if (psnType == cAtPwPsnTypeMpls)
        {
        if (!AtPwMplsPsnOuterLabelsAreIdentical((AtPwMplsPsn)psn, (AtPwMplsPsn)primaryPsn))
            return cAtErrorInvlParm;
        }

    if (!AtPwPsnIsIdentical(AtPwPsnLowerPsnGet(psn), AtPwPsnLowerPsnGet(primaryPsn)))
        return cAtErrorInvlParm;

    /* At this point, we are sure that back up PSN is the same with
     * primary PSN except label */
    ThaModulePweV2HsPwLabelSet(PweModule(self), adapter, HwHsPwLabelBuild(self, psn));
    return m_ThaPwHeaderControllerMethods->PsnSet(self, psn);
    }

static void OverrideThaPwHeaderController(ThaPwHeaderController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        m_ThaPwHeaderControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwHeaderControllerOverride, m_ThaPwHeaderControllerMethods, sizeof(m_ThaPwHeaderControllerOverride));

        mMethodOverride(m_ThaPwHeaderControllerOverride, NeedProtectOnTxDirection);
        mMethodOverride(m_ThaPwHeaderControllerOverride, HeaderRead);
        mMethodOverride(m_ThaPwHeaderControllerOverride, HeaderWrite);
        mMethodOverride(m_ThaPwHeaderControllerOverride, PsnSet);
        }

    mMethodsSet(self, &m_ThaPwHeaderControllerOverride);
    }

static void Override(ThaPwHeaderController self)
    {
    OverrideThaPwHeaderController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PwBackupHeaderController);
    }

ThaPwHeaderController Tha60210021PwBackupHeaderControllerObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwBackupHeaderControllerObjectInit(self, adapter) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwHeaderController Tha60210021PwBackupHeaderControllerNew(ThaPwAdapter adapter)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderController newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021PwBackupHeaderControllerObjectInit(newProvider, adapter);
    }


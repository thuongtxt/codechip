/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210021PwHeaderController.c
 *
 * Created Date: Nov 5, 2015
 *
 * Description : PW header controller of 60210021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/man/AtDriverInternal.h"
#include "Tha60210021PwHeaderController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwHeaderControllerMethods m_ThaPwHeaderControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool QueueIdIsValid(uint8 queueId)
    {
    /* Only support queue 0 */
    if (queueId > 0)
        return cAtFalse;

    return cAtTrue;
    }

static eBool EthPortIsQsgmii(AtEthPort self)
    {
    if (AtEthPortInterfaceGet(self) == cAtEthPortInterfaceQSgmii)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet EthPortQueueSet(ThaPwHeaderController self, uint8 queueId)
    {
    AtPw adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    if (!EthPortIsQsgmii(AtPwEthPortGet(adapter)) || !QueueIdIsValid(queueId))
        return cAtErrorModeNotSupport;

    /* HW default to queue 0 */
    return cAtOk;
    }

static uint8 EthPortQueueGet(ThaPwHeaderController self)
    {
    AtPw adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);

    /* HW default to queue 0 */
    if (EthPortIsQsgmii(AtPwEthPortGet(adapter)))
        return 0;

    return cAtInvalidPwEthPortQueueId;
    }

static uint8 EthPortMaxNumQueues(ThaPwHeaderController self)
    {
    AtPw adapter = (AtPw)ThaPwHeaderControllerAdapterGet(self);
    return EthPortIsQsgmii(AtPwEthPortGet(adapter)) ? 1 : 0;
    }

static void OverrideThaPwHeaderController(ThaPwHeaderController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwHeaderControllerOverride, mMethodsGet(self), sizeof(m_ThaPwHeaderControllerOverride));

        mMethodOverride(m_ThaPwHeaderControllerOverride, EthPortQueueSet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, EthPortQueueGet);
        mMethodOverride(m_ThaPwHeaderControllerOverride, EthPortMaxNumQueues);
        }

    mMethodsSet(self, &m_ThaPwHeaderControllerOverride);
    }

static void Override(ThaPwHeaderController self)
    {
    OverrideThaPwHeaderController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PwHeaderController);
    }

static ThaPwHeaderController ObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwHeaderControllerObjectInit(self, adapter) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwHeaderController Tha60210021PwHeaderControllerNew(ThaPwAdapter adapter)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwHeaderController newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProvider, adapter);
    }

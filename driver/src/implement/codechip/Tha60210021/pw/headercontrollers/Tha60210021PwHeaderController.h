/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210021PwHeaderController.h
 * 
 * Created Date: Aug 28, 2015
 *
 * Description : PW header controller of 60210021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha60210021PwHeaderController_H_
#define _Tha60210021PwHeaderController_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/headercontrollers/ThaPwHeaderControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210021PwBackupHeaderController
    {
    tThaPwBackupHeaderController super;
    }tTha60210021PwBackupHeaderController;

typedef struct tTha60210021PwHeaderController
    {
    tThaPwHeaderController super;
    }tTha60210021PwHeaderController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwHeaderController Tha60210021PwBackupHeaderControllerNew(ThaPwAdapter adapter);
ThaPwHeaderController Tha60210021PwHeaderControllerNew(ThaPwAdapter adapter);

ThaPwHeaderController Tha60210021PwBackupHeaderControllerObjectInit(ThaPwHeaderController self, ThaPwAdapter adapter);
#ifdef __cplusplus
}
#endif
#endif /* _Tha60210021PwHeaderController_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210021PwInterruptProcessor.c
 *
 * Created Date: Oct 30, 2015
 *
 * Description : PW interrupt processor implementation for 60210021 product.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/pw/AtPwInternal.h"
#include "../../pmc/Tha60210021ModulePmcReg.h"
#include "Tha60210021PwInterruptProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_pw_per_chn_intr_Mask(id) (1U << (id))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021PwInterruptProcessor
    {
    tThaPwInterruptProcessor super;
    } tTha60210021PwInterruptProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwInterruptProcessorMethods m_ThaPwInterruptProcessorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return 0x500000U;
    }

static void Process(ThaPwInterruptProcessor self, uint32 glbIntr, AtHal hal)
    {
    AtModulePw module = (AtModulePw)ThaPwInterruptProcessorModuleGet(self);
    const uint32 baseAddress = mMethodsGet(self)->BaseAddress(self);
    uint32 intrSta = AtHalRead(hal, baseAddress + cAf6Reg_counter_per_group_intr_or_stat);
    uint32 intrMask = AtHalRead(hal, baseAddress + cAf6Reg_counter_per_group_intr_en_ctrl);
    uint32 grpID;
    AtUnused(glbIntr);

    intrSta &= intrMask;

    for (grpID = 0; grpID < 6; grpID++)
        {
        if (intrSta & cAf6_pw_per_chn_intr_Mask(grpID))
            {
            uint32 intrChannel = AtHalRead(hal, baseAddress + cAf6Reg_Counter_Interrupt_OR_Status(grpID));
            uint32 subID;

            for (subID = 0; subID < 32; subID++)
                {
                if (intrChannel & cAf6_pw_per_chn_intr_Mask(subID))
                    {
                    uint32 pwID = (grpID << 5) + subID;
                    AtPw pwChannel = AtModulePwGetPw(module, pwID);

                    /* Execute channel interrupt process. */
                    AtPwInterruptProcess(pwChannel, pwID, hal);
                    }
                }
            }
        }
    }

static uint32 CurrentStatusRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_Counter_Per_Alarm_Current_Status_Base;
    }

static uint32 InterruptStatusRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_Counter_Per_Alarm_Interrupt_Status_Base;
    }

static uint32 InterruptMaskRegister(ThaPwInterruptProcessor self)
    {
    AtUnused(self);
    return cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_Base;
    }

static void OverrideThaPwInterruptProcessor(ThaPwInterruptProcessor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwInterruptProcessorOverride, mMethodsGet(self), sizeof(m_ThaPwInterruptProcessorOverride));

        mMethodOverride(m_ThaPwInterruptProcessorOverride, BaseAddress);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, Process);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, CurrentStatusRegister);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaPwInterruptProcessorOverride, InterruptMaskRegister);
        }

    mMethodsSet(self, &m_ThaPwInterruptProcessorOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PwInterruptProcessor);
    }

static ThaPwInterruptProcessor ObjectInit(ThaPwInterruptProcessor self, ThaModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwInterruptProcessorObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    OverrideThaPwInterruptProcessor(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwInterruptProcessor Tha60210021PwInterruptProcessorNew(ThaModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwInterruptProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, module);
    }

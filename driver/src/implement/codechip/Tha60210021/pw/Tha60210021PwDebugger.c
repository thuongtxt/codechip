/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210021PwDebugger.c
 *
 * Created Date: Sep 4, 2015
 *
 * Description : PW PRBS checker for debugging
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../default/pw/debugger/ThaPwDebuggerInternal.h"
#include "../../Tha60210031/pw/Tha60210031PwDebugger.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaPrbsIsRunningMask  cBit14_0
#define cThaPrbsIsRunningShift 0
#define cThaPrbsSyncMask       cBit15
#define cThaPrbsSyncShift      15
#define cThaPrbsErrorMask      cBit16
#define cThaPrbsErrorShift     16

#define cThaPdaPrbsStatus 0x247000
#define cThaEncPrbsStatus 0x246800

#define cPwDebugIdSelect  0x180000
#define cPwSpeedMap2Pla   0x180001
#define cPwSpeedPla2Pwe   0x180002
#define cPwSpeedRxEth     0x180003
#define cPwSpeedCla2Pda   0x180004
#define cPwSpeedMap2Pda   0x180005
#define cPwPktNum         0x180006
#define cPwBlkNum         0x180007

#define cPwPlaDump        0x180008
#define cPwPlaCaNumMask   cBit31_16
#define cPwPlaCaNumShift  16
#define cPwPlaBlkNumMask  cBit15_0
#define cPwPlaBlkNumShift 0

#define cPwPweDump        0x180009
#define cPwPweCaNumMask   cBit31_0
#define cPwPweCaNumShift  0

#define cPwPweSegNumMask  cBit7_0 /* Dword 1 */
#define cPwPweSegNumShift 0
#define cPwPwePktNumMask  cBit15_8 /* Dword 1 */
#define cPwPwePktNumShift 8

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021PwDebugger
    {
    tThaPwDebugger super;
    }tTha60210021PwDebugger;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwDebuggerMethods m_ThaPwDebuggerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PrbsCheckAtPda(ThaPwDebugger self, AtPw pwAdapter)
    {
    return Tha60210031PwPrbsCheck(self, cThaPdaPrbsStatus, pwAdapter, cThaPwPrbsPdaError, cThaPwPrbsPdaNotSync);
    }

static uint32 PrbsCheckAtEnc(ThaPwDebugger self, AtPw pwAdapter)
    {
    return Tha60210031PwPrbsCheck(self, cThaEncPrbsStatus, pwAdapter, cThaPwPrbsEncError, cThaPwPrbsEncNotSync);
    }

static void PwIdSelect(ThaPwDebugger self, AtPw pw)
    {
    AtUnused(self);
    mChannelHwWrite(pw, cPwDebugIdSelect, AtChannelIdGet((AtChannel)pw), cAtModulePw);
    }

static void PwSpeedPrint(AtPw pw, const char* title, uint32 reg)
    {
    uint32 regVal = mChannelHwRead(pw, reg, cAtModulePw);
    AtPrintc(cSevNormal, "    %-12s: %u (0x%08x = 0x%08x)\r\n", title, regVal, reg, regVal);
    }

static void PlaInfoPrint(AtPw pw)
    {
    uint32 regVal = mChannelHwRead(pw, cPwPlaDump, cThaModulePwe);
    AtPrintc(cSevNormal, "    %-12s: %u (0x%08x = 0x%08x)\r\n", "PlaCaNum", mRegField(regVal, cPwPlaCaNum), cPwPlaDump, regVal);
    AtPrintc(cSevNormal, "    %-12s: %u (0x%08x = 0x%08x)\r\n", "PlaBlkNum", mRegField(regVal, cPwPlaBlkNum), cPwPlaDump, regVal);
    }

static void PweInfoPrint(AtPw pw)
    {
    uint32 longReg[cThaLongRegMaxSize];
    mChannelHwLongRead(pw, cPwPweDump, longReg, cThaLongRegMaxSize, cThaModulePwe);
    AtPrintc(cSevNormal, "    %-12s: %u\r\n", "PweCaNum", longReg[0]);
    AtPrintc(cSevNormal, "    %-12s: %u\r\n", "PweSegNum", mRegField(longReg[1], cPwPweSegNum));
    AtPrintc(cSevNormal, "    %-12s: %u\r\n", "PwePktNum", mRegField(longReg[1], cPwPwePktNum));
    }

static void PwSpeedHwClear(AtPw pw)
    {
    mChannelHwRead(pw, cPwSpeedMap2Pla, cAtModulePw);
    mChannelHwRead(pw, cPwSpeedPla2Pwe, cAtModulePw);
    mChannelHwRead(pw, cPwSpeedRxEth, cAtModulePw);
    mChannelHwRead(pw, cPwSpeedCla2Pda, cAtModulePw);
    mChannelHwRead(pw, cPwSpeedMap2Pda, cAtModulePw);
    mChannelHwRead(pw, cPwPktNum, cAtModulePw);
    mChannelHwRead(pw, cPwBlkNum, cAtModulePw);
    }

static void PwSpeedShow(ThaPwDebugger self, AtPw pw)
    {
    const uint32 cDelayTimeBeforeReadPwSpeedInMs = 1;

    PwIdSelect(self, pw);
    AtOsalSleep(1);
    AtPrintc(cSevInfo, "* Pseudowire %u speed:\r\n", AtChannelIdGet((AtChannel)pw) + 1);

    /* As hardware recommend, need do some actions:
     * - Read first time to clear engine
     * - Delay 1s for hardware update engine
     * - Read to show the real values */
    PwSpeedHwClear(pw);
    AtOsalUSleep(cDelayTimeBeforeReadPwSpeedInMs * 1000);

    PwSpeedPrint(pw, "MAP to PLA" , cPwSpeedMap2Pla);
    PwSpeedPrint(pw, "PLA to PWE" , cPwSpeedPla2Pwe);
    PwSpeedPrint(pw, "Rx Ethernet", cPwSpeedRxEth);
    PwSpeedPrint(pw, "CLA to PDA" , cPwSpeedCla2Pda);
    PwSpeedPrint(pw, "MAP to PDA" , cPwSpeedMap2Pda);
    PwSpeedPrint(pw, "Num packet" , cPwPktNum);
    PwSpeedPrint(pw, "Num block"  , cPwBlkNum);
    PlaInfoPrint(pw);
    PweInfoPrint(pw);
    }

static uint32 RegFieldIsRunningMask(ThaPwDebugger self)
    {
    AtUnused(self);
    return cThaPrbsIsRunningMask;
    }

static uint32 RegFieldIsRunningShift(ThaPwDebugger self)
    {
    AtUnused(self);
    return cThaPrbsIsRunningShift;
    }

static uint32 RegFieldSyncMask(ThaPwDebugger self)
    {
    AtUnused(self);
    return cThaPrbsSyncMask;
    }

static uint32 RegFieldSyncShift(ThaPwDebugger self)
    {
    AtUnused(self);
    return cThaPrbsSyncShift;
    }

static uint32 RegFieldErrorMask(ThaPwDebugger self)
    {
    AtUnused(self);
    return cThaPrbsErrorMask;
    }

static uint32 RegFieldErrorShift(ThaPwDebugger self)
    {
    AtUnused(self);
    return cThaPrbsErrorShift;
    }

static void OverrideThaPwDebugger(ThaPwDebugger self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDebuggerOverride, mMethodsGet(self), sizeof(m_ThaPwDebuggerOverride));
        mMethodOverride(m_ThaPwDebuggerOverride, PrbsCheckAtPda);
        mMethodOverride(m_ThaPwDebuggerOverride, PrbsCheckAtEnc);
        mMethodOverride(m_ThaPwDebuggerOverride, PwSpeedShow);
        mMethodOverride(m_ThaPwDebuggerOverride, RegFieldIsRunningMask);
        mMethodOverride(m_ThaPwDebuggerOverride, RegFieldIsRunningShift);
        mMethodOverride(m_ThaPwDebuggerOverride, RegFieldSyncMask);
        mMethodOverride(m_ThaPwDebuggerOverride, RegFieldSyncShift);
        mMethodOverride(m_ThaPwDebuggerOverride, RegFieldErrorMask);
        mMethodOverride(m_ThaPwDebuggerOverride, RegFieldErrorShift);
        }

    mMethodsSet(self, &m_ThaPwDebuggerOverride);
    }

static void Override(ThaPwDebugger self)
    {
    OverrideThaPwDebugger(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PwDebugger);
    }

static ThaPwDebugger ObjectInit(ThaPwDebugger self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031PwDebuggerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDebugger Tha60210021PwDebuggerNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDebugger newChecker = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChecker == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChecker);
    }

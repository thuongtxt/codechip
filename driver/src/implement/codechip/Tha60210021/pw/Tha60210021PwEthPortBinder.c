/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210021PwEthPortBinder.c
 *
 * Created Date: Oct 26, 2015
 *
 * Description : 60210051 PW Ethernet port binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pw/ethportbinder/ThaPwEthPortBinderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021PwEthPortBinder
    {
    tThaPwEthPortBinder super;
    }tTha60210021PwEthPortBinder;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwEthPortBinderMethods m_ThaPwBwControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedToManageEthPortBandwidth(ThaPwEthPortBinder self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaPwBwController(ThaPwEthPortBinder self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwBwControllerOverride, mMethodsGet(self), sizeof(m_ThaPwBwControllerOverride));

        mMethodOverride(m_ThaPwBwControllerOverride, NeedToManageEthPortBandwidth);
        }

    mMethodsSet(self, &m_ThaPwBwControllerOverride);
    }

static void Override(ThaPwEthPortBinder self)
    {
    OverrideThaPwBwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PwEthPortBinder);
    }

static ThaPwEthPortBinder ObjectInit(ThaPwEthPortBinder self, ThaModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwEthPortBinderObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwEthPortBinder Tha60210021PwEthPortBinderNew(ThaModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwEthPortBinder newActivator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newActivator == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newActivator, module);
    }

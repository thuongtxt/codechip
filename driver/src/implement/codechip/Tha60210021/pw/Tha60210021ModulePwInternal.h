/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210021ModulePwInternal.h
 * 
 * Created Date: Dec 24, 2015
 *
 * Description : Pseudowire module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021MODULEPWINTERNAL_H_
#define _THA60210021MODULEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaPdhPwProduct/pw/ThaPdhPwProductModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210021ModulePw
    {
    tThaPdhPwProductModulePw super;
    }tTha60210021ModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw Tha60210021ModulePwObjectInit(AtModulePw self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021MODULEPWINTERNAL_H_ */


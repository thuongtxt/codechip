/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210021ModulePw.c
 *
 * Created Date: Jun 1, 2013
 *
 * Description : PW module for product 60210021
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pw/hspw/ThaPwHsGroup.h"
#include "../../Tha60210011/common/Tha602100xxCommon.h"
#include "../../Tha60210011/cdr/Tha60210011ModuleCdr.h"
#include "../man/Tha60210021DeviceReg.h"
#include "../pmc/Tha60210021ModulePmcReg.h"
#include "../man/Tha60210021Device.h"
#include "../cla/Tha60210021ModuleCla.h"
#include "headercontrollers/Tha60210021PwHeaderController.h"
#include "interruptprocessor/Tha60210021PwInterruptProcessor.h"
#include "defectcontroller/Tha60210021PwDefectController.h"
#include "Tha60210021ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cRegModulePwJitterBufferCenter 0x243500
#define cRegModulePwJitterBufferCenterEnableMask cBit8
#define cRegModulePwJitterBufferCenterEnableShift 8

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePwMethods m_ThaModulePwOverride;
static tAtModulePwMethods  m_AtModulePwOverride;

/* Save super implementation */
static const tThaModulePwMethods *m_ThaModulePwMethods = NULL;
static const tAtModulePwMethods  *m_AtModulePwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern ThaPwDefectController Tha60210021PwDefectControllerNew(AtPw adapter);

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 192;
    }

static uint32 MaxHsGroupsGet(AtModulePw self)
    {
    AtUnused(self);
    return 64;
    }

static ThaPwHeaderController BackupHeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    AtUnused(self);
    return Tha60210021PwBackupHeaderControllerNew((ThaPwAdapter)adapter);
    }

static eBool MBitDefectIsSupported(ThaModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210021DeviceStartVersionSupportMbitAlarm(device))
        return cAtTrue;

    return cAtFalse;
    }

static ThaPwDefectController DefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    if (MBitDefectIsSupported(self))
        return Tha60210021PwDefectControllerV2New(pw);
    else
        return Tha60210021PwDefectControllerNew(pw);
    }

static ThaPwInterruptProcessor InterruptProcessorCreate(ThaModulePw self)
    {
    return Tha60210021PwInterruptProcessorNew(self);
    }

static eAtModulePwRet JitterBufferCenteringEnable(AtModulePw self, eBool enable)
    {
    uint32 reg = cRegModulePwJitterBufferCenter;
    uint32 val = mModuleHwRead(self, reg);
    
    mFieldIns(&val, cRegModulePwJitterBufferCenterEnableMask, cRegModulePwJitterBufferCenterEnableShift, enable ? 1 : 0);
    mModuleHwWrite(self, reg, val);

    return cAtOk;
    }

static eBool JitterBufferCenteringIsEnabled(AtModulePw self)
    {
    eBool ret;
    uint32 reg = cRegModulePwJitterBufferCenter;
    uint32 val = mModuleHwRead(self, reg);
    
    mFieldGet(val, cRegModulePwJitterBufferCenterEnableMask, cRegModulePwJitterBufferCenterEnableShift, eBool, &ret);
    ret = mBinToBool(ret);

    return ret;
    }

static eBool JitterBufferCenteringIsSupported(AtModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaPwDebugger PwDebuggerCreate(ThaModulePw self)
    {
    AtUnused(self);
    return Tha60210021PwDebuggerNew();
    }

static eBool PwMbitCounterIsSupported(ThaModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PwIdleCodeIsSupported(ThaModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210021DeviceStartVersionSupportPwIdleCode(device))
        return cAtTrue;

    return cAtFalse;
    }

static AtPwGroup ApsGroupObjectCreate(AtModulePw self, uint32 groupId)
    {
    AtUnused(self);
    AtUnused(groupId);
    return NULL;
    }

static uint32 MaxApsGroupsGet(AtModulePw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet HsGroupDelete(AtModulePw self, uint32 groupId)
    {
    AtPwGroup pwGroup;
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleCla);

    if (groupId >= AtModulePwMaxHsGroupsGet(self))
        return cAtErrorOutOfRangParm;

    pwGroup = AtModulePwHsGroupGet(self, groupId);
    if (pwGroup == NULL)
        return cAtOk;

    if (AtPwGroupNumPws(pwGroup) > 0)
        return cAtErrorDevBusy;

    AtPwGroupTxLabelSetSelect(pwGroup, cAtPwGroupLabelSetPrimary);
    AtPwGroupRxLabelSetSelect(pwGroup, cAtPwGroupLabelSetPrimary);
    ThaModuleClaPwV2HsGroupEnable(claModule, pwGroup, cAtFalse);

    return m_AtModulePwMethods->HsGroupDelete(self, groupId);
    }

static uint32 StartVersionSupportHspw(ThaModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    
    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return 0x0;
        
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x10, 0x15, 00);
    }

static ThaPwEthPortBinder PwEthPortBinderCreate(ThaModulePw self)
    {
    return Tha60210021PwEthPortBinderNew(self);
    }

static ThaPwHeaderController HeaderControllerObjectCreate(ThaModulePw self, AtPw adapter)
    {
    AtUnused(self);
    return Tha60210021PwHeaderControllerNew((ThaPwAdapter)adapter);
    }

static eAtRet PwFinishRemoving(ThaModulePw self, AtPw adapter)
    {
    eAtRet ret = cAtOk;
    AtUnused(self);

    if (AtModuleInAccessible((AtModule)self))
        return cAtOk;

    ret |= ThaPwAdapterStopCenterJitterBuffer(adapter);
    ret |= ThaPwAdapterPweFinishRemoving(adapter);

    return ret;
    }

static eAtRet PwFinishRemovingForBinding(ThaModulePw self, AtPw adapter)
    {
    eAtRet ret = cAtOk;
    AtUnused(self);

    if (AtModuleInAccessible((AtModule)self))
        return cAtOk;

    ret |= ThaPwAdapterStopCenterJitterBufferForBinding(adapter);
    ret |= ThaPwAdapterPweFinishRemovingForBinding(adapter);

    return ret;
    }

static eBool HitlessHeaderChangeShouldBeEnabledByDefault(ThaModulePw self)
    {
    return Tha602100xxModulePwHitlessHeaderChangeShouldBeEnabledByDefault(self);
    }

static eBool AutoRxMBitShouldEnable(AtModulePw self)
    {
    return Tha602100xxModuleAutoRxMBitShouldEnable(self);
    }

static ThaModuleCdr CdrModule(AtModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    }

static eBool RtpTimestampFrequencyIsSupported(AtModulePw self, uint32 khz)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)CdrModule(self);

    if (Tha60210011ModuleCdrFlexibleFrequency(cdrModule))
        return ThaModuleCdrTimestampFrequencyInRange(cdrModule, khz);

    return m_AtModulePwMethods->RtpTimestampFrequencyIsSupported(self, khz);
    }

static eAtModulePwRet CepRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    return ThaModuleCdrDcrRtpTimestampFrequencyRegSet(CdrModule(self), khz);
    }

static uint32 CepRtpTimestampFrequencyGet(AtModulePw self)
    {
    return ThaModuleCdrDcrRtpTimestampFrequencyFromRegGet(CdrModule(self));
    }

static eAtModulePwRet CesRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    return ThaModuleCdrDcrRtpTimestampFrequencyRegSet(CdrModule(self), khz);
    }

static uint32 CesRtpTimestampFrequencyGet(AtModulePw self)
    {
    return ThaModuleCdrDcrRtpTimestampFrequencyFromRegGet(CdrModule(self));
    }

static eAtModulePwRet RtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    return ThaModuleCdrDcrRtpTimestampFrequencyRegSet(CdrModule(self), khz);
    }

static uint32 RtpTimestampFrequencyGet(AtModulePw self)
    {
    return ThaModuleCdrDcrRtpTimestampFrequencyFromRegGet(CdrModule(self));
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, mMethodsGet(self), sizeof(tAtModulePwMethods));
        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        mMethodOverride(m_AtModulePwOverride, MaxHsGroupsGet);
        mMethodOverride(m_AtModulePwOverride, MaxApsGroupsGet);
        mMethodOverride(m_AtModulePwOverride, ApsGroupObjectCreate);
        mMethodOverride(m_AtModulePwOverride, HsGroupDelete);
        mMethodOverride(m_AtModulePwOverride, JitterBufferCenteringEnable);
        mMethodOverride(m_AtModulePwOverride, JitterBufferCenteringIsEnabled);
        mMethodOverride(m_AtModulePwOverride, JitterBufferCenteringIsSupported);
        mMethodOverride(m_AtModulePwOverride, AutoRxMBitShouldEnable);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencyIsSupported);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, CepRtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, CepRtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, CesRtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, CesRtpTimestampFrequencyGet);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePwMethods = mMethodsGet(pwModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(pwModule), sizeof(m_ThaModulePwOverride));
        mMethodOverride(m_ThaModulePwOverride, BackupHeaderControllerObjectCreate);
        mMethodOverride(m_ThaModulePwOverride, DefectControllerCreate);
        mMethodOverride(m_ThaModulePwOverride, PwDebuggerCreate);
        mMethodOverride(m_ThaModulePwOverride, PwMbitCounterIsSupported);
        mMethodOverride(m_ThaModulePwOverride, PwIdleCodeIsSupported);
        mMethodOverride(m_ThaModulePwOverride, StartVersionSupportHspw);
        mMethodOverride(m_ThaModulePwOverride, PwEthPortBinderCreate);
        mMethodOverride(m_ThaModulePwOverride, InterruptProcessorCreate);
        mMethodOverride(m_ThaModulePwOverride, HeaderControllerObjectCreate);
        mMethodOverride(m_ThaModulePwOverride, PwFinishRemoving);
        mMethodOverride(m_ThaModulePwOverride, PwFinishRemovingForBinding);
        mMethodOverride(m_ThaModulePwOverride, HitlessHeaderChangeShouldBeEnabledByDefault);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideThaModulePw(self);
    OverrideAtModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModulePw);
    }

AtModulePw Tha60210021ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60210021ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021ModulePwObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210021PwDefectController.h
 * 
 * Created Date: Nov 3, 2015
 *
 * Description : PW defect controller interface for 60210021 product.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021PWDEFECTCONTROLLER_H_
#define _THA60210021PWDEFECTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/defectcontrollers/ThaPwDefectControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210021PwDefectController
    {
    tThaPwDefectController super;
    }tTha60210021PwDefectController;

/*--------------------------- Forward declarations ---------------------------*/
ThaPwDefectController Tha60210021PwDefectControllerObjectInit(ThaPwDefectController self, AtPw pw);
ThaPwDefectController Tha60210021PwDefectControllerNew(AtPw pw);

ThaPwDefectController Tha60210021PwDefectControllerV2New(AtPw pw);
/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021PWDEFECTCONTROLLER_H_ */


/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : PW
 *
 * File        : Tha60210021PwDefectController.c
 *
 * Created Date: Aug 5, 2015
 *
 * Description : PW channel defect control.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/pw/ThaModulePw.h"
#include "../../pmc/Tha60210021ModulePmcReg.h"
#include "../../man/Tha60210021Device.h"
#include "../../pda/Tha60210021ModulePda.h"
#include "Tha60210021PwDefectController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, hwMask_)         \
    do                                                                         \
        {                                                                      \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? (regVal | hwMask_) : (regVal & (~hwMask_)); \
        }while(0)

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods                m_AtChannelOverride;
static tAtPwMethods                     m_AtPwOverride;
static tThaPwDefectControllerMethods    m_ThaPwDefectControllerOverride;

/* Save super's implementation */
static const tAtPwMethods *m_AtPwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CurrentStatusRegister(AtChannel self)
    {
    return ThaModulePwDefectCurrentStatusRegister((ThaModulePw)AtChannelModuleGet(self));
    }

static uint32 InterruptStatusRegister(AtChannel self)
    {
    return ThaModulePwDefectInterruptStatusRegister((ThaModulePw)AtChannelModuleGet(self));
    }

static uint32 InterruptMaskRegister(AtChannel self)
    {
    return ThaModulePwDefectInterruptMaskRegister((ThaModulePw)AtChannelModuleGet(self));
    }

static uint32 PwDefectMaskGet(AtChannel self, uint32 regVal)
    {
    ThaPwDefectController controller = (ThaPwDefectController)self;
    uint32 defects = 0;

    if (regVal & cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrEn_Mask)
        defects |= cAtPwAlarmTypeRBit;
    if (regVal & cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrEn_Mask)
        defects |= cAtPwAlarmTypeJitterBufferUnderrun;
    if (regVal & cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrEn_Mask)
        defects |= cAtPwAlarmTypeJitterBufferOverrun;
    if (regVal & mMethodsGet(controller)->LopsMask(controller))
        defects |= cAtPwAlarmTypeLops;
    if (regVal & mMethodsGet(controller)->MissingMask(controller))
        defects |= cAtPwAlarmTypeMissingPacket;
    if (regVal & cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrEn_Mask)
        defects |= cAtPwAlarmTypeLBit;
    if (regVal & mMethodsGet(controller)->MalformedMask(controller))
        defects |= cAtPwAlarmTypeMalformedPacket;
    if (regVal & mMethodsGet(controller)->StrayMask(controller))
        defects |= cAtPwAlarmTypeStrayPacket;
    if (regVal & mMethodsGet(controller)->MbitMask(controller))
        defects |= cAtPwAlarmTypeMBit;

    return defects;
    }

static uint32 DefectHwGet(AtChannel self)
    {
    const uint32 address = CurrentStatusRegister(self) + ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePw);
    return PwDefectMaskGet(self, regVal);
    }

static uint32 DefectGet(AtChannel self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter((AtPw)self)))
        return 0;

    return DefectHwGet(self);
    }

static uint32 DefectHistoryHwGet(AtChannel self, eBool read2Clear)
    {
    const uint32 address = InterruptStatusRegister(self) + ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePw);
    uint32 defects = PwDefectMaskGet(self, regVal);

    if (read2Clear)
        mChannelHwWrite(self, address, regVal, cAtModulePw);

    return defects;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter((AtPw)self)))
        return 0;

    return DefectHistoryHwGet(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter((AtPw)self)))
        return 0;

    return DefectHistoryHwGet(self, cAtTrue);
    }

static void InterruptProcess(AtPw self, uint32 pwOffset, AtHal hal)
    {
    const uint32 intrAddress = InterruptStatusRegister((AtChannel)self) + pwOffset;
    ThaPwDefectController controller = (ThaPwDefectController)self;
    uint32 intrMask   = AtHalRead(hal, InterruptMaskRegister((AtChannel)self) + pwOffset);
    uint32 intrStatus = AtHalRead(hal, intrAddress);
    uint32 currStatus;
    uint32 defects    = 0;
    uint32 state      = 0;

    intrStatus &= intrMask;
    /* Clear interrupt */
    AtHalWrite(hal, intrAddress, intrStatus);
    currStatus = AtHalRead(hal, CurrentStatusRegister((AtChannel)self) + pwOffset);

    if (intrStatus & cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrEn_Mask)
        {
        defects |= cAtPwAlarmTypeRBit;
        if (currStatus & cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_Mask)
            state |= cAtPwAlarmTypeRBit;
        }

    if (intrStatus & mMethodsGet(controller)->LopsMask(controller))
        {
        defects |= cAtPwAlarmTypeLops;
        if (currStatus & mMethodsGet(controller)->LopsMask(controller))
            state |= cAtPwAlarmTypeLops;
        }

    if (intrStatus & cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrEn_Mask)
        {
        defects |= cAtPwAlarmTypeJitterBufferUnderrun;
        if (currStatus & cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Mask)
            state |= cAtPwAlarmTypeJitterBufferUnderrun;
        }

    if (intrStatus & cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrEn_Mask)
        {
        defects |= cAtPwAlarmTypeJitterBufferOverrun;
        if (currStatus & cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Mask)
            state |= cAtPwAlarmTypeJitterBufferOverrun;
        }

    if (intrStatus & mMethodsGet(controller)->MissingMask(controller))
        {
        defects |= cAtPwAlarmTypeMissingPacket;
        if (currStatus & mMethodsGet(controller)->MissingMask(controller))
            state |= cAtPwAlarmTypeMissingPacket;
        }

    if (intrStatus & cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrEn_Mask)
        {
        defects |= cAtPwAlarmTypeLBit;
        if (currStatus & cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Mask)
            state |= cAtPwAlarmTypeLBit;
        }

    if (intrStatus & mMethodsGet(controller)->MbitMask(controller))
        {
        defects |= cAtPwAlarmTypeMBit;
        if (currStatus & mMethodsGet(controller)->MbitMask(controller))
            state |= cAtPwAlarmTypeMBit;
        }

    if (intrStatus & mMethodsGet(controller)->MalformedMask(controller))
        {
        defects |= cAtPwAlarmTypeMalformedPacket;
        if (currStatus & mMethodsGet(controller)->MalformedMask(controller))
            state |= cAtPwAlarmTypeMalformedPacket;
        }

    if (intrStatus & mMethodsGet(controller)->StrayMask(controller))
        {
        defects |= cAtPwAlarmTypeStrayPacket;
        if (currStatus & mMethodsGet(controller)->StrayMask(controller))
            state |= cAtPwAlarmTypeStrayPacket;
        }

    AtChannelAllAlarmListenersCall((AtChannel)ThaPwAdapterPwGet(mAdapter(self)), defects, state);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    static uint32 supportedMasks = (cAtPwAlarmTypeLBit|cAtPwAlarmTypeRBit|cAtPwAlarmTypeLops|
                                    cAtPwAlarmTypeMissingPacket|cAtPwAlarmTypeJitterBufferUnderrun|
                                    cAtPwAlarmTypeJitterBufferOverrun|cAtPwAlarmTypeStrayPacket|
                                    cAtPwAlarmTypeMalformedPacket|cAtPwAlarmTypeMBit);
    AtUnused(self);
    return supportedMasks;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    const uint32 address = InterruptMaskRegister(self) + ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self);
    ThaPwDefectController controller = (ThaPwDefectController)self;
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePw);

    mSetIntrBitMask(cAtPwAlarmTypeLBit, defectMask, enableMask, cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeRBit, defectMask, enableMask, cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeLops, defectMask, enableMask, mMethodsGet(controller)->LopsMask(controller));
    mSetIntrBitMask(cAtPwAlarmTypeJitterBufferUnderrun, defectMask, enableMask, cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeJitterBufferOverrun,  defectMask, enableMask, cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Mask);
    mSetIntrBitMask(cAtPwAlarmTypeMalformedPacket,  defectMask, enableMask, mMethodsGet(controller)->MalformedMask(controller));
    mSetIntrBitMask(cAtPwAlarmTypeStrayPacket,      defectMask, enableMask, mMethodsGet(controller)->StrayMask(controller));
    mSetIntrBitMask(cAtPwAlarmTypeMBit, defectMask, enableMask, mMethodsGet(controller)->MbitMask(controller));
    mSetIntrBitMask(cAtPwAlarmTypeMissingPacket, defectMask, enableMask, mMethodsGet(controller)->MissingMask(controller));
    mChannelHwWrite(self, address, regVal, cAtModulePw);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    const uint32 address = InterruptMaskRegister(self) + ThaPwDefectControllerPwDefaultOffset((ThaPwDefectController)self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePw);
    return PwDefectMaskGet(self, regVal);
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);

    switch (alarmType)
        {
        case cAtPwAlarmTypeLBit:                return cAtTrue;
        case cAtPwAlarmTypeRBit:                return cAtTrue;
        case cAtPwAlarmTypeLops:                return cAtTrue;
        case cAtPwAlarmTypeJitterBufferUnderrun:return cAtTrue;
        case cAtPwAlarmTypeJitterBufferOverrun: return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 LopsMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Mask;
    }

static uint32 MbitMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 StrayMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MalformedMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return 0;
    }

static eBool LopsClearThresholdIsSupported(AtPw self)
    {
    ThaModulePda pdaModule = (ThaModulePda)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePda);
    return Tha60210021ModulePdaLofsClearThresholdIsSupported(pdaModule);
    }

static eAtModulePwRet LopsClearThresholdSet(AtPw self, uint32 numPackets)
    {
    if (AtPwLopsClearThresholdIsSupported(self))
        return m_AtPwMethods->LopsClearThresholdSet(self, numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 LopsClearThresholdGet(AtPw self)
    {
    if (AtPwLopsClearThresholdIsSupported(self))
        return m_AtPwMethods->LopsClearThresholdGet(self);

    return 0;
    }

static void OverrideThaPwDefectController(ThaPwDefectController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDefectControllerOverride, mMethodsGet(self), sizeof(m_ThaPwDefectControllerOverride));

        mMethodOverride(m_ThaPwDefectControllerOverride, LopsMask);
        mMethodOverride(m_ThaPwDefectControllerOverride, MbitMask);
        mMethodOverride(m_ThaPwDefectControllerOverride, StrayMask);
        mMethodOverride(m_ThaPwDefectControllerOverride, MalformedMask);
        }

    mMethodsSet(self, &m_ThaPwDefectControllerOverride);
    }

static void OverrideAtPw(ThaPwDefectController self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, InterruptProcess);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdIsSupported);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdGet);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void OverrideAtChannel(ThaPwDefectController self)
    {
    AtChannel pw = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(pw), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(pw, &m_AtChannelOverride);
    }

static void Override(ThaPwDefectController self)
    {
    OverrideThaPwDefectController(self);
    OverrideAtPw(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PwDefectController);
    }

ThaPwDefectController Tha60210021PwDefectControllerObjectInit(ThaPwDefectController self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwDefectControllerObjectInit(self, pw) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDefectController Tha60210021PwDefectControllerNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDefectController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021PwDefectControllerObjectInit(newController, pw);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60210021PwDefectControllerV2.c
 *
 * Created Date: Nov 3, 2015
 *
 * Description : PW defect controller implementation version 2: M-bit/Stray/Malformed supported.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pw/ThaModulePw.h"
#include "../../pmc/Tha60210021ModulePmcReg.h"
#include "Tha60210021PwDefectController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021PwDefectControllerV2
    {
    tTha60210021PwDefectController super;
    }tTha60210021PwDefectControllerV2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods                m_AtChannelOverride;
static tThaPwDefectControllerMethods    m_ThaPwDefectControllerOverride;

/* Save super's implementation */
static const tAtChannelMethods          *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MbitMask(ThaPwDefectController self)
    {
    return (AtPwTypeGet((AtPw)self) == cAtPwTypeCESoP) ? cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_Mask : 0;
    }

static uint32 StrayMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_Mask;
    }

static uint32 MalformedMask(ThaPwDefectController self)
    {
    AtUnused(self);
    return cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_Mask;
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    if ((alarmType == cAtPwAlarmTypeStrayPacket)   ||
        (alarmType == cAtPwAlarmTypeMalformedPacket))
        return cAtTrue;

    if ((alarmType == cAtPwAlarmTypeMBit) && (AtPwTypeGet((AtPw)self) == cAtPwTypeCESoP))
        return cAtTrue;

    return m_AtChannelMethods->AlarmIsSupported(self, alarmType);
    }

static void OverrideThaPwDefectController(ThaPwDefectController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDefectControllerOverride, mMethodsGet(self), sizeof(m_ThaPwDefectControllerOverride));

        mMethodOverride(m_ThaPwDefectControllerOverride, MbitMask);
        mMethodOverride(m_ThaPwDefectControllerOverride, StrayMask);
        mMethodOverride(m_ThaPwDefectControllerOverride, MalformedMask);
        }

    mMethodsSet(self, &m_ThaPwDefectControllerOverride);
    }

static void OverrideAtChannel(ThaPwDefectController self)
    {
    AtChannel pw = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(pw), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(pw, &m_AtChannelOverride);
    }

static void Override(ThaPwDefectController self)
    {
    OverrideThaPwDefectController(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PwDefectControllerV2);
    }

static ThaPwDefectController ObjectInit(ThaPwDefectController self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210021PwDefectControllerObjectInit(self, pw) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDefectController Tha60210021PwDefectControllerV2New(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDefectController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, pw);
    }


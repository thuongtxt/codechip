/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0021_RD_PMC_H_
#define _AF6_REG_AF6CCI0021_RD_PMC_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive FCS Error Packet Counter
Reg Addr   : 0x000000(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of FCS error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_FCS_Error_Packet_Counter_ro_Base                                     0x000000
#define cAf6Reg_Ethernet_Receive_FCS_Error_Packet_Counter_ro                                          0x000000
#define cAf6Reg_Ethernet_Receive_FCS_Error_Packet_Counter_ro_WidthVal                                       32
#define cAf6Reg_Ethernet_Receive_FCS_Error_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxFcsErrorPk
BitField Type: RW
BitField Desc: This counter count the number of FCS error Ethernet PHY packets
received.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_ro_RxFcsErrorPk_Bit_Start                                       0
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_ro_RxFcsErrorPk_Bit_End                                      15
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_ro_RxFcsErrorPk_Mask                                cBit15_0
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_ro_RxFcsErrorPk_Shift                                       0
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_ro_RxFcsErrorPk_MaxVal                                  0xffff
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_ro_RxFcsErrorPk_MinVal                                     0x0
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_ro_RxFcsErrorPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive FCS Error Packet Counter
Reg Addr   : 0x000001(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of FCS error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_FCS_Error_Packet_Counter_rc_Base                                     0x000001
#define cAf6Reg_Ethernet_Receive_FCS_Error_Packet_Counter_rc                                          0x000001
#define cAf6Reg_Ethernet_Receive_FCS_Error_Packet_Counter_rc_WidthVal                                       32
#define cAf6Reg_Ethernet_Receive_FCS_Error_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxFcsErrorPk
BitField Type: RW
BitField Desc: This counter count the number of FCS error Ethernet PHY packets
received.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_rc_RxFcsErrorPk_Bit_Start                                       0
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_rc_RxFcsErrorPk_Bit_End                                      15
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_rc_RxFcsErrorPk_Mask                                cBit15_0
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_rc_RxFcsErrorPk_Shift                                       0
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_rc_RxFcsErrorPk_MaxVal                                  0xffff
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_rc_RxFcsErrorPk_MinVal                                     0x0
#define cAf6_Ethernet_Receive_FCS_Error_Packet_Counter_rc_RxFcsErrorPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Header Error Packet Counter
Reg Addr   : 0x000002(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of Ethernet MAC header error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_Header_Error_Packet_Counter_ro_Base                                  0x000002
#define cAf6Reg_Ethernet_Receive_Header_Error_Packet_Counter_ro                                       0x000002
#define cAf6Reg_Ethernet_Receive_Header_Error_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_Header_Error_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxEthErrorPk
BitField Type: RW
BitField Desc: This counter count the number of packets with MAC Header Error.
This counter just count up when FCS is already checked good. MAC Header error
packet can be on of following: DA does not match AF6FHW0013 MAC address
(broadcast MAC address is an exception) Ethernet Type fields do not match 0x8100
(C-VLAN), 0x88A8 (S-VLAN), 0x0800 (Ipv4), 0x86DD(Ipv6), 0x9947(MPLS),
0x88D8(MEF-8) and some of specific values for OAM. Packet with total length
(from DA to FCS) > 2000 bytes (oversize packet) Packet with total length (from
DA to FCS) < 64 bytes (undersize packet)
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_ro_RxEthErrorPk_Bit_Start                                       0
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_ro_RxEthErrorPk_Bit_End                                      15
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_ro_RxEthErrorPk_Mask                                cBit15_0
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_ro_RxEthErrorPk_Shift                                       0
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_ro_RxEthErrorPk_MaxVal                                  0xffff
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_ro_RxEthErrorPk_MinVal                                     0x0
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_ro_RxEthErrorPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Header Error Packet Counter
Reg Addr   : 0x000003(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of Ethernet MAC header error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_Header_Error_Packet_Counter_rc_Base                                  0x000003
#define cAf6Reg_Ethernet_Receive_Header_Error_Packet_Counter_rc                                       0x000003
#define cAf6Reg_Ethernet_Receive_Header_Error_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_Header_Error_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxEthErrorPk
BitField Type: RW
BitField Desc: This counter count the number of packets with MAC Header Error.
This counter just count up when FCS is already checked good. MAC Header error
packet can be on of following: DA does not match AF6FHW0013 MAC address
(broadcast MAC address is an exception) Ethernet Type fields do not match 0x8100
(C-VLAN), 0x88A8 (S-VLAN), 0x0800 (Ipv4), 0x86DD(Ipv6), 0x9947(MPLS),
0x88D8(MEF-8) and some of specific values for OAM. Packet with total length
(from DA to FCS) > 2000 bytes (oversize packet) Packet with total length (from
DA to FCS) < 64 bytes (undersize packet)
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_rc_RxEthErrorPk_Bit_Start                                       0
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_rc_RxEthErrorPk_Bit_End                                      15
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_rc_RxEthErrorPk_Mask                                cBit15_0
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_rc_RxEthErrorPk_Shift                                       0
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_rc_RxEthErrorPk_MaxVal                                  0xffff
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_rc_RxEthErrorPk_MinVal                                     0x0
#define cAf6_Ethernet_Receive_Header_Error_Packet_Counter_rc_RxEthErrorPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive PSN Header Error Packet Counter
Reg Addr   : 0x000004(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of PSN header error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro_Base                                0x000004
#define cAf6Reg_Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro                                   0x000004
#define cAf6Reg_Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPsnErrorPk
BitField Type: RW
BitField Desc: This counter count the number of packets with PSN Header error.
This counter just count up when FCS and MAC Header are already checked good. PSN
Header Error packet can be on of following: Ipv4 header error: Checksum error,
IP destination address does not match AF6FHW0013 IP address. Ipv6 header error:
IP destination address does not match AF6FHW0013 IP address. MEF-8 header error:
ECID field will be Pseudowire ID. Counter will count up if the pseudowire ID
differs from RxEthPwid value defined in Ethernet Receive Pseudowire
Identification Control register. MPLS header error: Bit 31 to 12 of outer label
(if exist, also called tunnel label) differs from RxEthMplsOutLabel value
defined in Global Ethernet Control register will be Pseudowire ID. Counter will
count up if the pseudowire ID differs from RxEthPwid value defined in Ethernet
Receive Pseudowire Identification Control register.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro_RxPsnErrorPk_Bit_Start                                       0
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro_RxPsnErrorPk_Bit_End                                      15
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro_RxPsnErrorPk_Mask                                cBit15_0
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro_RxPsnErrorPk_Shift                                       0
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro_RxPsnErrorPk_MaxVal                                  0xffff
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro_RxPsnErrorPk_MinVal                                     0x0
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro_RxPsnErrorPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive PSN Header Error Packet Counter
Reg Addr   : 0x000005(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of PSN header error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc_Base                                0x000005
#define cAf6Reg_Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc                                   0x000005
#define cAf6Reg_Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPsnErrorPk
BitField Type: RW
BitField Desc: This counter count the number of packets with PSN Header error.
This counter just count up when FCS and MAC Header are already checked good. PSN
Header Error packet can be on of following: Ipv4 header error: Checksum error,
IP destination address does not match AF6FHW0013 IP address. Ipv6 header error:
IP destination address does not match AF6FHW0013 IP address. MEF-8 header error:
ECID field will be Pseudowire ID. Counter will count up if the pseudowire ID
differs from RxEthPwid value defined in Ethernet Receive Pseudowire
Identification Control register. MPLS header error: Bit 31 to 12 of outer label
(if exist, also called tunnel label) differs from RxEthMplsOutLabel value
defined in Global Ethernet Control register will be Pseudowire ID. Counter will
count up if the pseudowire ID differs from RxEthPwid value defined in Ethernet
Receive Pseudowire Identification Control register.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc_RxPsnErrorPk_Bit_Start                                       0
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc_RxPsnErrorPk_Bit_End                                      15
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc_RxPsnErrorPk_Mask                                cBit15_0
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc_RxPsnErrorPk_Shift                                       0
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc_RxPsnErrorPk_MaxVal                                  0xffff
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc_RxPsnErrorPk_MinVal                                     0x0
#define cAf6_Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc_RxPsnErrorPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive UDP Port Error Packet Counter
Reg Addr   : 0x000006(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of UDP port error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro_Base                                0x000006
#define cAf6Reg_Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro                                     0x000006
#define cAf6Reg_Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxUdpErrorPk
BitField Type: RW
BitField Desc: This counter count the number of packets with UDP port error.
This counter just count up when FCS, MAC Header and PSN Header (exactly Ipv4 and
Ipv6 PSN header) are already checked good. If RxEthUdpMode defined in Global
Ethernet Control register is not set, AF6FHW0013 will automatically search for
UDP port number with value 0x085E, the remaining UDP port number will be
Pseudowire ID. The counter will count up if the  Pseudowire ID differ from
RxEthPwid value defined in Ethernet Receive Pseudowire Identification Control
register. If  RxEthUdpMode is set, then depending on the  RxEthUdpSel (also
defined in Global Ethernet Control register), UDP source port number or UDP
destination port number is used for Pseudowire ID. The counter will count up if
the  Pseudowire ID differ from RxEthPwid value defined in Ethernet Receive
Pseudowire Identification Control register.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro_RxUdpErrorPk_Bit_Start                                       0
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro_RxUdpErrorPk_Bit_End                                      15
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro_RxUdpErrorPk_Mask                                cBit15_0
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro_RxUdpErrorPk_Shift                                       0
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro_RxUdpErrorPk_MaxVal                                  0xffff
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro_RxUdpErrorPk_MinVal                                     0x0
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro_RxUdpErrorPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive UDP Port Error Packet Counter
Reg Addr   : 0x000007(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of UDP port error packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc_Base                                0x000007
#define cAf6Reg_Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc                                     0x000007
#define cAf6Reg_Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxUdpErrorPk
BitField Type: RW
BitField Desc: This counter count the number of packets with UDP port error.
This counter just count up when FCS, MAC Header and PSN Header (exactly Ipv4 and
Ipv6 PSN header) are already checked good. If RxEthUdpMode defined in Global
Ethernet Control register is not set, AF6FHW0013 will automatically search for
UDP port number with value 0x085E, the remaining UDP port number will be
Pseudowire ID. The counter will count up if the  Pseudowire ID differ from
RxEthPwid value defined in Ethernet Receive Pseudowire Identification Control
register. If  RxEthUdpMode is set, then depending on the  RxEthUdpSel (also
defined in Global Ethernet Control register), UDP source port number or UDP
destination port number is used for Pseudowire ID. The counter will count up if
the  Pseudowire ID differ from RxEthPwid value defined in Ethernet Receive
Pseudowire Identification Control register.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc_RxUdpErrorPk_Bit_Start                                       0
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc_RxUdpErrorPk_Bit_End                                      15
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc_RxUdpErrorPk_Mask                                cBit15_0
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc_RxUdpErrorPk_Shift                                       0
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc_RxUdpErrorPk_MaxVal                                  0xffff
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc_RxUdpErrorPk_MinVal                                     0x0
#define cAf6_Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc_RxUdpErrorPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive OAM Packet Counter
Reg Addr   : 0x00000E(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of classified OAM packets (ARP, ICMP,...) received from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_OAM_Packet_Counter_ro_Base                                           0x00000E
#define cAf6Reg_Ethernet_Receive_OAM_Packet_Counter_ro                                                0x00000E
#define cAf6Reg_Ethernet_Receive_OAM_Packet_Counter_ro_WidthVal                                             32
#define cAf6Reg_Ethernet_Receive_OAM_Packet_Counter_ro_WriteMask                                           0x0

/*--------------------------------------
BitField Name: RxOamPkt
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_ro_RxOamPkt_Bit_Start                                       0
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_ro_RxOamPkt_Bit_End                                        15
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_ro_RxOamPkt_Mask                                     cBit15_0
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_ro_RxOamPkt_Shift                                           0
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_ro_RxOamPkt_MaxVal                                     0xffff
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_ro_RxOamPkt_MinVal                                        0x0
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_ro_RxOamPkt_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive OAM Packet Counter
Reg Addr   : 0x00000F(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of classified OAM packets (ARP, ICMP,...) received from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_OAM_Packet_Counter_rc_Base                                           0x00000F
#define cAf6Reg_Ethernet_Receive_OAM_Packet_Counter_rc                                                0x00000F
#define cAf6Reg_Ethernet_Receive_OAM_Packet_Counter_rc_WidthVal                                             32
#define cAf6Reg_Ethernet_Receive_OAM_Packet_Counter_rc_WriteMask                                           0x0

/*--------------------------------------
BitField Name: RxOamPkt
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_rc_RxOamPkt_Bit_Start                                       0
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_rc_RxOamPkt_Bit_End                                        15
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_rc_RxOamPkt_Mask                                     cBit15_0
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_rc_RxOamPkt_Shift                                           0
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_rc_RxOamPkt_MaxVal                                     0xffff
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_rc_RxOamPkt_MinVal                                        0x0
#define cAf6_Ethernet_Receive_OAM_Packet_Counter_rc_RxOamPkt_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit OAM Packet Counter
Reg Addr   : 0x000010(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of transmitted OAM packets (ARP, ICMP,...) to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_OAM_Packet_Counter_ro_Base                                          0x000010
#define cAf6Reg_Ethernet_Transmit_OAM_Packet_Counter_ro                                               0x000010
#define cAf6Reg_Ethernet_Transmit_OAM_Packet_Counter_ro_WidthVal                                            32
#define cAf6Reg_Ethernet_Transmit_OAM_Packet_Counter_ro_WriteMask                                          0x0

/*--------------------------------------
BitField Name: TxOamPkt
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_ro_TxOamPkt_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_ro_TxOamPkt_Bit_End                                       15
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_ro_TxOamPkt_Mask                                    cBit15_0
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_ro_TxOamPkt_Shift                                          0
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_ro_TxOamPkt_MaxVal                                    0xffff
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_ro_TxOamPkt_MinVal                                       0x0
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_ro_TxOamPkt_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit OAM Packet Counter
Reg Addr   : 0x000011(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of transmitted OAM packets (ARP, ICMP,...) to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_OAM_Packet_Counter_rc_Base                                          0x000011
#define cAf6Reg_Ethernet_Transmit_OAM_Packet_Counter_rc                                               0x000011
#define cAf6Reg_Ethernet_Transmit_OAM_Packet_Counter_rc_WidthVal                                            32
#define cAf6Reg_Ethernet_Transmit_OAM_Packet_Counter_rc_WriteMask                                          0x0

/*--------------------------------------
BitField Name: TxOamPkt
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_rc_TxOamPkt_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_rc_TxOamPkt_Bit_End                                       15
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_rc_TxOamPkt_Mask                                    cBit15_0
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_rc_TxOamPkt_Shift                                          0
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_rc_TxOamPkt_MaxVal                                    0xffff
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_rc_TxOamPkt_MinVal                                       0x0
#define cAf6_Ethernet_Transmit_OAM_Packet_Counter_rc_TxOamPkt_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Total Byte Counter
Reg Addr   : 0x000012(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of bytes received from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_Total_Byte_Counter_ro_Base                                           0x000012
#define cAf6Reg_Ethernet_Receive_Total_Byte_Counter_ro                                                0x000012
#define cAf6Reg_Ethernet_Receive_Total_Byte_Counter_ro_WidthVal                                             32
#define cAf6Reg_Ethernet_Receive_Total_Byte_Counter_ro_WriteMask                                           0x0

/*--------------------------------------
BitField Name: RxByte
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_Total_Byte_Counter_ro_RxByte_Bit_Start                                         0
#define cAf6_Ethernet_Receive_Total_Byte_Counter_ro_RxByte_Bit_End                                          31
#define cAf6_Ethernet_Receive_Total_Byte_Counter_ro_RxByte_Mask                                       cBit31_0
#define cAf6_Ethernet_Receive_Total_Byte_Counter_ro_RxByte_Shift                                             0
#define cAf6_Ethernet_Receive_Total_Byte_Counter_ro_RxByte_MaxVal                                   0xffffffff
#define cAf6_Ethernet_Receive_Total_Byte_Counter_ro_RxByte_MinVal                                          0x0
#define cAf6_Ethernet_Receive_Total_Byte_Counter_ro_RxByte_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Total Byte Counter
Reg Addr   : 0x000013(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of bytes received from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_Total_Byte_Counter_rc_Base                                           0x000013
#define cAf6Reg_Ethernet_Receive_Total_Byte_Counter_rc                                                0x000013
#define cAf6Reg_Ethernet_Receive_Total_Byte_Counter_rc_WidthVal                                             32
#define cAf6Reg_Ethernet_Receive_Total_Byte_Counter_rc_WriteMask                                           0x0

/*--------------------------------------
BitField Name: RxByte
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_Total_Byte_Counter_rc_RxByte_Bit_Start                                         0
#define cAf6_Ethernet_Receive_Total_Byte_Counter_rc_RxByte_Bit_End                                          31
#define cAf6_Ethernet_Receive_Total_Byte_Counter_rc_RxByte_Mask                                       cBit31_0
#define cAf6_Ethernet_Receive_Total_Byte_Counter_rc_RxByte_Shift                                             0
#define cAf6_Ethernet_Receive_Total_Byte_Counter_rc_RxByte_MaxVal                                   0xffffffff
#define cAf6_Ethernet_Receive_Total_Byte_Counter_rc_RxByte_MinVal                                          0x0
#define cAf6_Ethernet_Receive_Total_Byte_Counter_rc_RxByte_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Total Packet Counter
Reg Addr   : 0x000014(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of packets received from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_Total_Packet_Counter_ro_Base                                         0x000014
#define cAf6Reg_Ethernet_Receive_Total_Packet_Counter_ro                                              0x000014
#define cAf6Reg_Ethernet_Receive_Total_Packet_Counter_ro_WidthVal                                           32
#define cAf6Reg_Ethernet_Receive_Total_Packet_Counter_ro_WriteMask                                         0x0

/*--------------------------------------
BitField Name: RxPacket
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_Total_Packet_Counter_ro_RxPacket_Bit_Start                                       0
#define cAf6_Ethernet_Receive_Total_Packet_Counter_ro_RxPacket_Bit_End                                      31
#define cAf6_Ethernet_Receive_Total_Packet_Counter_ro_RxPacket_Mask                                   cBit31_0
#define cAf6_Ethernet_Receive_Total_Packet_Counter_ro_RxPacket_Shift                                         0
#define cAf6_Ethernet_Receive_Total_Packet_Counter_ro_RxPacket_MaxVal                               0xffffffff
#define cAf6_Ethernet_Receive_Total_Packet_Counter_ro_RxPacket_MinVal                                      0x0
#define cAf6_Ethernet_Receive_Total_Packet_Counter_ro_RxPacket_RstVal                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Total Packet Counter
Reg Addr   : 0x000015(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of packets received from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_Total_Packet_Counter_rc_Base                                         0x000015
#define cAf6Reg_Ethernet_Receive_Total_Packet_Counter_rc                                              0x000015
#define cAf6Reg_Ethernet_Receive_Total_Packet_Counter_rc_WidthVal                                           32
#define cAf6Reg_Ethernet_Receive_Total_Packet_Counter_rc_WriteMask                                         0x0

/*--------------------------------------
BitField Name: RxPacket
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_Total_Packet_Counter_rc_RxPacket_Bit_Start                                       0
#define cAf6_Ethernet_Receive_Total_Packet_Counter_rc_RxPacket_Bit_End                                      31
#define cAf6_Ethernet_Receive_Total_Packet_Counter_rc_RxPacket_Mask                                   cBit31_0
#define cAf6_Ethernet_Receive_Total_Packet_Counter_rc_RxPacket_Shift                                         0
#define cAf6_Ethernet_Receive_Total_Packet_Counter_rc_RxPacket_MaxVal                               0xffffffff
#define cAf6_Ethernet_Receive_Total_Packet_Counter_rc_RxPacket_MinVal                                      0x0
#define cAf6_Ethernet_Receive_Total_Packet_Counter_rc_RxPacket_RstVal                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Classified Packet Counter
Reg Addr   : 0x000016(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of classified packets (PW packets plus OAM packets) from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_Classified_Packet_Counter_ro_Base                                    0x000016
#define cAf6Reg_Ethernet_Receive_Classified_Packet_Counter_ro                                         0x000016
#define cAf6Reg_Ethernet_Receive_Classified_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_Classified_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxClassifyPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_ro_RxClassifyPk_Bit_Start                                       0
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_ro_RxClassifyPk_Bit_End                                      31
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_ro_RxClassifyPk_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_ro_RxClassifyPk_Shift                                       0
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_ro_RxClassifyPk_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_ro_RxClassifyPk_MinVal                                     0x0
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_ro_RxClassifyPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Classified Packet Counter
Reg Addr   : 0x000017(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count number of classified packets (PW packets plus OAM packets) from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_Classified_Packet_Counter_rc_Base                                    0x000017
#define cAf6Reg_Ethernet_Receive_Classified_Packet_Counter_rc                                         0x000017
#define cAf6Reg_Ethernet_Receive_Classified_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_Classified_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxClassifyPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_rc_RxClassifyPk_Bit_Start                                       0
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_rc_RxClassifyPk_Bit_End                                      31
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_rc_RxClassifyPk_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_rc_RxClassifyPk_Shift                                       0
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_rc_RxClassifyPk_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_rc_RxClassifyPk_MinVal                                     0x0
#define cAf6_Ethernet_Receive_Classified_Packet_Counter_rc_RxClassifyPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit Byte Counter
Reg Addr   : 0x000018(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  bytes transmitted to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_Byte_Counter_ro_Base                                                0x000018
#define cAf6Reg_Ethernet_Transmit_Byte_Counter_ro                                                     0x000018
#define cAf6Reg_Ethernet_Transmit_Byte_Counter_ro_WidthVal                                                  32
#define cAf6Reg_Ethernet_Transmit_Byte_Counter_ro_WriteMask                                                0x0

/*--------------------------------------
BitField Name: TxByte
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_Byte_Counter_ro_TxByte_Bit_Start                                              0
#define cAf6_Ethernet_Transmit_Byte_Counter_ro_TxByte_Bit_End                                               31
#define cAf6_Ethernet_Transmit_Byte_Counter_ro_TxByte_Mask                                            cBit31_0
#define cAf6_Ethernet_Transmit_Byte_Counter_ro_TxByte_Shift                                                  0
#define cAf6_Ethernet_Transmit_Byte_Counter_ro_TxByte_MaxVal                                        0xffffffff
#define cAf6_Ethernet_Transmit_Byte_Counter_ro_TxByte_MinVal                                               0x0
#define cAf6_Ethernet_Transmit_Byte_Counter_ro_TxByte_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit Byte Counter
Reg Addr   : 0x000019(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  bytes transmitted to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_Byte_Counter_rc_Base                                                0x000019
#define cAf6Reg_Ethernet_Transmit_Byte_Counter_rc                                                     0x000019
#define cAf6Reg_Ethernet_Transmit_Byte_Counter_rc_WidthVal                                                  32
#define cAf6Reg_Ethernet_Transmit_Byte_Counter_rc_WriteMask                                                0x0

/*--------------------------------------
BitField Name: TxByte
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_Byte_Counter_rc_TxByte_Bit_Start                                              0
#define cAf6_Ethernet_Transmit_Byte_Counter_rc_TxByte_Bit_End                                               31
#define cAf6_Ethernet_Transmit_Byte_Counter_rc_TxByte_Mask                                            cBit31_0
#define cAf6_Ethernet_Transmit_Byte_Counter_rc_TxByte_Shift                                                  0
#define cAf6_Ethernet_Transmit_Byte_Counter_rc_TxByte_MaxVal                                        0xffffffff
#define cAf6_Ethernet_Transmit_Byte_Counter_rc_TxByte_MinVal                                               0x0
#define cAf6_Ethernet_Transmit_Byte_Counter_rc_TxByte_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit Packet Counter
Reg Addr   : 0x00001A(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  packets transmitted to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_Packet_Counter_ro_Base                                              0x00001A
#define cAf6Reg_Ethernet_Transmit_Packet_Counter_ro                                                   0x00001A
#define cAf6Reg_Ethernet_Transmit_Packet_Counter_ro_WidthVal                                                32
#define cAf6Reg_Ethernet_Transmit_Packet_Counter_ro_WriteMask                                              0x0

/*--------------------------------------
BitField Name: TxPacket
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_Packet_Counter_ro_TxPacket_Bit_Start                                          0
#define cAf6_Ethernet_Transmit_Packet_Counter_ro_TxPacket_Bit_End                                           31
#define cAf6_Ethernet_Transmit_Packet_Counter_ro_TxPacket_Mask                                        cBit31_0
#define cAf6_Ethernet_Transmit_Packet_Counter_ro_TxPacket_Shift                                              0
#define cAf6_Ethernet_Transmit_Packet_Counter_ro_TxPacket_MaxVal                                    0xffffffff
#define cAf6_Ethernet_Transmit_Packet_Counter_ro_TxPacket_MinVal                                           0x0
#define cAf6_Ethernet_Transmit_Packet_Counter_ro_TxPacket_RstVal                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit Packet Counter
Reg Addr   : 0x00001B(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  packets transmitted to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_Packet_Counter_rc_Base                                              0x00001B
#define cAf6Reg_Ethernet_Transmit_Packet_Counter_rc                                                   0x00001B
#define cAf6Reg_Ethernet_Transmit_Packet_Counter_rc_WidthVal                                                32
#define cAf6Reg_Ethernet_Transmit_Packet_Counter_rc_WriteMask                                              0x0

/*--------------------------------------
BitField Name: TxPacket
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_Packet_Counter_rc_TxPacket_Bit_Start                                          0
#define cAf6_Ethernet_Transmit_Packet_Counter_rc_TxPacket_Bit_End                                           31
#define cAf6_Ethernet_Transmit_Packet_Counter_rc_TxPacket_Mask                                        cBit31_0
#define cAf6_Ethernet_Transmit_Packet_Counter_rc_TxPacket_Shift                                              0
#define cAf6_Ethernet_Transmit_Packet_Counter_rc_TxPacket_MaxVal                                    0xffffffff
#define cAf6_Ethernet_Transmit_Packet_Counter_rc_TxPacket_MinVal                                           0x0
#define cAf6_Ethernet_Transmit_Packet_Counter_rc_TxPacket_RstVal                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Good Packet Counter
Reg Addr   : 0x011800 - 0x0118FF(RO)
Reg Formula: 0x011800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDsu
Reg Desc   : 
Count number of CESoETH packets transmitted to Ethernet side.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_ro_Base                                       0x011800
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_ro(PwId)                             (0x011800+(PwId))
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_ro_WidthVal                                         32
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_ro_WriteMask                                       0x0

/*--------------------------------------
BitField Name: TxPwGoodPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_ro_TxPwGoodPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Good Packet Counter
Reg Addr   : 0x011000 - 0x0110FF(RC)
Reg Formula: 0x011000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDsu
Reg Desc   : 
Count number of CESoETH packets transmitted to Ethernet side.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_rc_Base                                       0x011000
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_rc(PwId)                             (0x011000+(PwId))
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_rc_WidthVal                                         32
#define cAf6Reg_Pseudowire_Transmit_Good_Packet_Counter_rc_WriteMask                                       0x0

/*--------------------------------------
BitField Name: TxPwGoodPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_Good_Packet_Counter_rc_TxPwGoodPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Payload Octet Counter
Reg Addr   : 0x012800 - 0x0128FF(RO)
Reg Formula: 0x012800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of pseudowire payload octet received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_ro_Base                                      0x012800
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_ro(PwId)                            (0x012800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_ro_WidthVal                                        32
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_ro_WriteMask                                      0x0

/*--------------------------------------
BitField Name: RxPwPayOct
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_Shift                                       0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_ro_RxPwPayOct_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Payload Octet Counter
Reg Addr   : 0x012000 - 0x0120FF(RC)
Reg Formula: 0x012000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of pseudowire payload octet received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_rc_Base                                      0x012000
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_rc(PwId)                            (0x012000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_rc_WidthVal                                        32
#define cAf6Reg_Pseudowire_Receive_Payload_Octet_Counter_rc_WriteMask                                      0x0

/*--------------------------------------
BitField Name: RxPwPayOct
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_Shift                                       0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Payload_Octet_Counter_rc_RxPwPayOct_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Good Packet Counter
Reg Addr   : 0x013800 - 0x0138FF(RO)
Reg Formula: 0x013800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of CESoETH packets received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_ro_Base                                        0x013800
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_ro(PwId)                              (0x013800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_ro_WidthVal                                          32
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_ro_WriteMask                                        0x0

/*--------------------------------------
BitField Name: RxPwGoodPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_ro_RxPwGoodPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Good Packet Counter
Reg Addr   : 0x013000 - 0x0130FF(RC)
Reg Formula: 0x013000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of CESoETH packets received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_rc_Base                                        0x013000
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_rc(PwId)                              (0x013000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_rc_WidthVal                                          32
#define cAf6Reg_Pseudowire_Receive_Good_Packet_Counter_rc_WriteMask                                        0x0

/*--------------------------------------
BitField Name: RxPwGoodPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Good_Packet_Counter_rc_RxPwGoodPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Reorder Drop Packet Counter
Reg Addr   : 0x014800 - 0x0148FF(RO)
Reg Formula: 0x014800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of dropped packet (too late or too soon) by reorder function

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_Base                                0x014800
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro(PwId)                       (0x014800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwReorDropPk
BitField Type: RW
BitField Desc: Reorder Dropped Packet Counter count up when a packet comes too
soon or too late compared with expected sequence number
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro_RxPwReorDropPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Reorder Drop Packet Counter
Reg Addr   : 0x014000 - 0x0140FF(RC)
Reg Formula: 0x014000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of dropped packet (too late or too soon) by reorder function

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_Base                                0x014000
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc(PwId)                       (0x014000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwReorDropPk
BitField Type: RW
BitField Desc: Reorder Dropped Packet Counter count up when a packet comes too
soon or too late compared with expected sequence number
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc_RxPwReorDropPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Reorder Out Of Sequence  Packet Counter
Reg Addr   : 0x015800 - 0x0158FF(RO)
Reg Formula: 0x015800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of frames received that are out-of-sequence, but successfully re-ordered

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro_Base                                0x015800
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro(PwId)                       (0x015800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwReorOSeqPk
BitField Type: RW
BitField Desc: This counter count the number of packets that are out-of-sequence
but successfully re-ordered.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro_RxPwReorOSeqPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro_RxPwReorOSeqPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro_RxPwReorOSeqPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro_RxPwReorOSeqPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro_RxPwReorOSeqPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro_RxPwReorOSeqPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro_RxPwReorOSeqPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Reorder Out Of Sequence  Packet Counter
Reg Addr   : 0x015000 - 0x0150FF(RC)
Reg Formula: 0x015000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of frames received that are out-of-sequence, but successfully re-ordered

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc_Base                                0x015000
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc(PwId)                       (0x015000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwReorOSeqPk
BitField Type: RW
BitField Desc: This counter count the number of packets that are out-of-sequence
but successfully re-ordered.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc_RxPwReorOSeqPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc_RxPwReorOSeqPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc_RxPwReorOSeqPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc_RxPwReorOSeqPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc_RxPwReorOSeqPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc_RxPwReorOSeqPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc_RxPwReorOSeqPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive LOFS Transition Counter
Reg Addr   : 0x016800 - 0x0168FF(RO)
Reg Formula: 0x016800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of transitions from the normal to the loss of frames state (LOFS)

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_LOFS_Transition_Counter_ro_Base                                    0x016800
#define cAf6Reg_Pseudowire_Receive_LOFS_Transition_Counter_ro(PwId)                          (0x016800+(PwId))
#define cAf6Reg_Pseudowire_Receive_LOFS_Transition_Counter_ro_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_LOFS_Transition_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwLofsEvent
BitField Type: RW
BitField Desc: This counter count the number of transitions from normal sate to
loss of frame state
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_ro_RxPwLofsEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_ro_RxPwLofsEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_ro_RxPwLofsEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_ro_RxPwLofsEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_ro_RxPwLofsEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_ro_RxPwLofsEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_ro_RxPwLofsEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive LOFS Transition Counter
Reg Addr   : 0x016000 - 0x0160FF(RC)
Reg Formula: 0x016000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of transitions from the normal to the loss of frames state (LOFS)

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_LOFS_Transition_Counter_rc_Base                                    0x016000
#define cAf6Reg_Pseudowire_Receive_LOFS_Transition_Counter_rc(PwId)                          (0x016000+(PwId))
#define cAf6Reg_Pseudowire_Receive_LOFS_Transition_Counter_rc_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_LOFS_Transition_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwLofsEvent
BitField Type: RW
BitField Desc: This counter count the number of transitions from normal sate to
loss of frame state
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_rc_RxPwLofsEvent_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_rc_RxPwLofsEvent_Bit_End                                      31
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_rc_RxPwLofsEvent_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_rc_RxPwLofsEvent_Shift                                       0
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_rc_RxPwLofsEvent_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_rc_RxPwLofsEvent_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_LOFS_Transition_Counter_rc_RxPwLofsEvent_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Jitter Buffer Overrun Event Counter
Reg Addr   : 0x017800 - 0x0178FF(RO)
Reg Formula: 0x017800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of jitter buffer overrun events

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro_Base                                0x017800
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro(PwId)                       (0x017800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxOverrunPk
BitField Type: RW
BitField Desc: Counter value.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro_RxOverrunPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro_RxOverrunPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro_RxOverrunPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro_RxOverrunPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro_RxOverrunPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro_RxOverrunPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro_RxOverrunPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Jitter Buffer Overrun Event Counter
Reg Addr   : 0x017000 - 0x0170FF(RC)
Reg Formula: 0x017000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of jitter buffer overrun events

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc_Base                                0x017000
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc(PwId)                       (0x017000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxOverrunPk
BitField Type: RW
BitField Desc: Counter value.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc_RxOverrunPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc_RxOverrunPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc_RxOverrunPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc_RxOverrunPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc_RxOverrunPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc_RxOverrunPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc_RxOverrunPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Reorder Lost Packet Counter
Reg Addr   : 0x018800 - 0x0188FF(RO)
Reg Formula: 0x018800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of lost packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_Base                                0x018800
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro(PwId)                       (0x018800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwReorLostPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro_RxPwReorLostPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Reorder Lost Packet Counter
Reg Addr   : 0x018000 - 0x0180FF(RC)
Reg Formula: 0x018000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of lost packets detected

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_Base                                0x018000
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc(PwId)                       (0x018000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwReorLostPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc_RxPwReorLostPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Malform Packet Counter
Reg Addr   : 0x019800 - 0x0198FF(RO)
Reg Formula: 0x019800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of malformed packets received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Malform_Packet_Counter_ro_Base                                     0x019800
#define cAf6Reg_Pseudowire_Receive_Malform_Packet_Counter_ro(PwId)                           (0x019800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Malform_Packet_Counter_ro_WidthVal                                       32
#define cAf6Reg_Pseudowire_Receive_Malform_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwMalformPk
BitField Type: RW
BitField Desc: This counter count the number of pseudowire good packets that
have the length differ than PwPayloadLen defined in Pseudowire Payload Control
register or PT value in RTP header mismatch
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_ro_RxPwMalformPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_ro_RxPwMalformPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_ro_RxPwMalformPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_ro_RxPwMalformPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_ro_RxPwMalformPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_ro_RxPwMalformPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_ro_RxPwMalformPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Malform Packet Counter
Reg Addr   : 0x019000 - 0x0190FF(RC)
Reg Formula: 0x019000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of malformed packets received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Malform_Packet_Counter_rc_Base                                     0x019000
#define cAf6Reg_Pseudowire_Receive_Malform_Packet_Counter_rc(PwId)                           (0x019000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Malform_Packet_Counter_rc_WidthVal                                       32
#define cAf6Reg_Pseudowire_Receive_Malform_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxPwMalformPk
BitField Type: RW
BitField Desc: This counter count the number of pseudowire good packets that
have the length differ than PwPayloadLen defined in Pseudowire Payload Control
register or PT value in RTP header mismatch
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_rc_RxPwMalformPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_rc_RxPwMalformPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_rc_RxPwMalformPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_rc_RxPwMalformPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_rc_RxPwMalformPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_rc_RxPwMalformPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Malform_Packet_Counter_rc_RxPwMalformPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Stray Packet Counter
Reg Addr   : 0x010800 - 0x0108FF(RO)
Reg Formula: 0x010800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of stray packets received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Stray_Packet_Counter_ro_Base                                       0x010800
#define cAf6Reg_Pseudowire_Receive_Stray_Packet_Counter_ro(PwId)                             (0x010800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Stray_Packet_Counter_ro_WidthVal                                         32
#define cAf6Reg_Pseudowire_Receive_Stray_Packet_Counter_ro_WriteMask                                       0x0

/*--------------------------------------
BitField Name: RxPwStrayPk
BitField Type: RW
BitField Desc: This counter count the number of pseudowire good packets that
have the RTP SSRC field mismatch
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_ro_RxPwStrayPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_ro_RxPwStrayPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_ro_RxPwStrayPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_ro_RxPwStrayPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_ro_RxPwStrayPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_ro_RxPwStrayPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_ro_RxPwStrayPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Stray Packet Counter
Reg Addr   : 0x010000 - 0x0100FF(RC)
Reg Formula: 0x010000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of stray packets received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Stray_Packet_Counter_rc_Base                                       0x010000
#define cAf6Reg_Pseudowire_Receive_Stray_Packet_Counter_rc(PwId)                             (0x010000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Stray_Packet_Counter_rc_WidthVal                                         32
#define cAf6Reg_Pseudowire_Receive_Stray_Packet_Counter_rc_WriteMask                                       0x0

/*--------------------------------------
BitField Name: RxPwStrayPk
BitField Type: RW
BitField Desc: This counter count the number of pseudowire good packets that
have the RTP SSRC field mismatch
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_rc_RxPwStrayPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_rc_RxPwStrayPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_rc_RxPwStrayPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_rc_RxPwStrayPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_rc_RxPwStrayPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_rc_RxPwStrayPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Stray_Packet_Counter_rc_RxPwStrayPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit L Bit Packet Counter
Reg Addr   : 0x01A800 - 0x01A8FF(RO)
Reg Formula: 0x01A800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of good pseudowire packets transmitted with L bit set

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_L_Bit_Packet_Counter_ro_Base                                      0x01A800
#define cAf6Reg_Pseudowire_Transmit_L_Bit_Packet_Counter_ro(PwId)                            (0x01A800+(PwId))
#define cAf6Reg_Pseudowire_Transmit_L_Bit_Packet_Counter_ro_WidthVal                                        32
#define cAf6Reg_Pseudowire_Transmit_L_Bit_Packet_Counter_ro_WriteMask                                      0x0

/*--------------------------------------
BitField Name: TxPwLbitPk
BitField Type: RW
BitField Desc: This counter counts the number of packets with L bit set at the
PSN transmit side.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_ro_TxPwLbitPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_ro_TxPwLbitPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_ro_TxPwLbitPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_ro_TxPwLbitPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_ro_TxPwLbitPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_ro_TxPwLbitPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_ro_TxPwLbitPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit L Bit Packet Counter
Reg Addr   : 0x01A000 - 0x01A0FF(RC)
Reg Formula: 0x01A000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of good pseudowire packets transmitted with L bit set

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_L_Bit_Packet_Counter_rc_Base                                      0x01A000
#define cAf6Reg_Pseudowire_Transmit_L_Bit_Packet_Counter_rc(PwId)                            (0x01A000+(PwId))
#define cAf6Reg_Pseudowire_Transmit_L_Bit_Packet_Counter_rc_WidthVal                                        32
#define cAf6Reg_Pseudowire_Transmit_L_Bit_Packet_Counter_rc_WriteMask                                      0x0

/*--------------------------------------
BitField Name: TxPwLbitPk
BitField Type: RW
BitField Desc: This counter counts the number of packets with L bit set at the
PSN transmit side.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_rc_TxPwLbitPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_rc_TxPwLbitPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_rc_TxPwLbitPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_rc_TxPwLbitPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_rc_TxPwLbitPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_rc_TxPwLbitPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_L_Bit_Packet_Counter_rc_TxPwLbitPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive L Bit Packet Counter
Reg Addr   : 0x01B800 - 0x01B8FF(RO)
Reg Formula: 0x01B800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of good pseudowire packets received with L bit set

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_L_Bit_Packet_Counter_ro_Base                                       0x01B800
#define cAf6Reg_Pseudowire_Receive_L_Bit_Packet_Counter_ro(PwId)                             (0x01B800+(PwId))
#define cAf6Reg_Pseudowire_Receive_L_Bit_Packet_Counter_ro_WidthVal                                         32
#define cAf6Reg_Pseudowire_Receive_L_Bit_Packet_Counter_ro_WriteMask                                       0x0

/*--------------------------------------
BitField Name: RxPwMalformPk
BitField Type: RW
BitField Desc: This counter count the number of good packets received with L bit
set
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_ro_RxPwMalformPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_ro_RxPwMalformPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_ro_RxPwMalformPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_ro_RxPwMalformPk_Shift                                       0
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_ro_RxPwMalformPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_ro_RxPwMalformPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_ro_RxPwMalformPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive L Bit Packet Counter
Reg Addr   : 0x01B000 - 0x01B0FF(RC)
Reg Formula: 0x01B000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of good pseudowire packets received with L bit set

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_L_Bit_Packet_Counter_rc_Base                                       0x01B000
#define cAf6Reg_Pseudowire_Receive_L_Bit_Packet_Counter_rc(PwId)                             (0x01B000+(PwId))
#define cAf6Reg_Pseudowire_Receive_L_Bit_Packet_Counter_rc_WidthVal                                         32
#define cAf6Reg_Pseudowire_Receive_L_Bit_Packet_Counter_rc_WriteMask                                       0x0

/*--------------------------------------
BitField Name: RxPwMalformPk
BitField Type: RW
BitField Desc: This counter count the number of good packets received with L bit
set
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_rc_RxPwMalformPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_rc_RxPwMalformPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_rc_RxPwMalformPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_rc_RxPwMalformPk_Shift                                       0
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_rc_RxPwMalformPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_rc_RxPwMalformPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_L_Bit_Packet_Counter_rc_RxPwMalformPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit R Bit Packet Counter
Reg Addr   : 0x01C800 - 0x01C8FF(RO)
Reg Formula: 0x01C800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of good pseudowire packets transmitted with R bit set

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_R_Bit_Packet_Counter_ro_Base                                      0x01C800
#define cAf6Reg_Pseudowire_Transmit_R_Bit_Packet_Counter_ro(PwId)                            (0x01C800+(PwId))
#define cAf6Reg_Pseudowire_Transmit_R_Bit_Packet_Counter_ro_WidthVal                                        32
#define cAf6Reg_Pseudowire_Transmit_R_Bit_Packet_Counter_ro_WriteMask                                      0x0

/*--------------------------------------
BitField Name: TxPwLbitPk
BitField Type: RW
BitField Desc: This counter count the number of packets with R bit set at the
PSN transmit side.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_ro_TxPwLbitPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_ro_TxPwLbitPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_ro_TxPwLbitPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_ro_TxPwLbitPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_ro_TxPwLbitPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_ro_TxPwLbitPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_ro_TxPwLbitPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit R Bit Packet Counter
Reg Addr   : 0x01C000 - 0x01C0FF(RC)
Reg Formula: 0x01C000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of good pseudowire packets transmitted with R bit set

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_R_Bit_Packet_Counter_rc_Base                                      0x01C000
#define cAf6Reg_Pseudowire_Transmit_R_Bit_Packet_Counter_rc(PwId)                            (0x01C000+(PwId))
#define cAf6Reg_Pseudowire_Transmit_R_Bit_Packet_Counter_rc_WidthVal                                        32
#define cAf6Reg_Pseudowire_Transmit_R_Bit_Packet_Counter_rc_WriteMask                                      0x0

/*--------------------------------------
BitField Name: TxPwLbitPk
BitField Type: RW
BitField Desc: This counter count the number of packets with R bit set at the
PSN transmit side.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_rc_TxPwLbitPk_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_rc_TxPwLbitPk_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_rc_TxPwLbitPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_rc_TxPwLbitPk_Shift                                       0
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_rc_TxPwLbitPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_rc_TxPwLbitPk_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_R_Bit_Packet_Counter_rc_TxPwLbitPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive R Bit Packet Counter
Reg Addr   : 0x01D800 - 0x01D8FF(RO)
Reg Formula: 0x01D800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of good pseudowire packets received with R bit set

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_R_Bit_Packet_Counter_ro_Base                                       0x01D800
#define cAf6Reg_Pseudowire_Receive_R_Bit_Packet_Counter_ro(PwId)                             (0x01D800+(PwId))
#define cAf6Reg_Pseudowire_Receive_R_Bit_Packet_Counter_ro_WidthVal                                         32
#define cAf6Reg_Pseudowire_Receive_R_Bit_Packet_Counter_ro_WriteMask                                       0x0

/*--------------------------------------
BitField Name: RxPwMalformPk
BitField Type: RW
BitField Desc: This counter count the number of good packets received with R bit
set
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_ro_RxPwMalformPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_ro_RxPwMalformPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_ro_RxPwMalformPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_ro_RxPwMalformPk_Shift                                       0
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_ro_RxPwMalformPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_ro_RxPwMalformPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_ro_RxPwMalformPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive R Bit Packet Counter
Reg Addr   : 0x01D000 - 0x01D0FF(RC)
Reg Formula: 0x01D000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of good pseudowire packets received with R bit set

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_R_Bit_Packet_Counter_rc_Base                                       0x01D000
#define cAf6Reg_Pseudowire_Receive_R_Bit_Packet_Counter_rc(PwId)                             (0x01D000+(PwId))
#define cAf6Reg_Pseudowire_Receive_R_Bit_Packet_Counter_rc_WidthVal                                         32
#define cAf6Reg_Pseudowire_Receive_R_Bit_Packet_Counter_rc_WriteMask                                       0x0

/*--------------------------------------
BitField Name: RxPwMalformPk
BitField Type: RW
BitField Desc: This counter count the number of good packets received with R bit
set
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_rc_RxPwMalformPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_rc_RxPwMalformPk_Bit_End                                      31
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_rc_RxPwMalformPk_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_rc_RxPwMalformPk_Shift                                       0
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_rc_RxPwMalformPk_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_rc_RxPwMalformPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_R_Bit_Packet_Counter_rc_RxPwMalformPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Payload Octet Counter
Reg Addr   : 0x01E800 - 0x01E8FF(RO)
Reg Formula: 0x01E800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of pseudowire payload octet transmitted

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_Payload_Octet_Counter_ro_Base                                     0x01E800
#define cAf6Reg_Pseudowire_Transmit_Payload_Octet_Counter_ro(PwId)                           (0x01E800+(PwId))
#define cAf6Reg_Pseudowire_Transmit_Payload_Octet_Counter_ro_WidthVal                                       32
#define cAf6Reg_Pseudowire_Transmit_Payload_Octet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: TxPwPayOct
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_ro_TxPwPayOct_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_ro_TxPwPayOct_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_ro_TxPwPayOct_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_ro_TxPwPayOct_Shift                                       0
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_ro_TxPwPayOct_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_ro_TxPwPayOct_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_ro_TxPwPayOct_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Payload Octet Counter
Reg Addr   : 0x01E000 - 0x01E0FF(RC)
Reg Formula: 0x01E000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of pseudowire payload octet transmitted

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_Payload_Octet_Counter_rc_Base                                     0x01E000
#define cAf6Reg_Pseudowire_Transmit_Payload_Octet_Counter_rc(PwId)                           (0x01E000+(PwId))
#define cAf6Reg_Pseudowire_Transmit_Payload_Octet_Counter_rc_WidthVal                                       32
#define cAf6Reg_Pseudowire_Transmit_Payload_Octet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: TxPwPayOct
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_rc_TxPwPayOct_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_rc_TxPwPayOct_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_rc_TxPwPayOct_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_rc_TxPwPayOct_Shift                                       0
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_rc_TxPwPayOct_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_rc_TxPwPayOct_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_Payload_Octet_Counter_rc_TxPwPayOct_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Mbit Counter
Reg Addr   : 0x020800 - 0x0208FF(RO)
Reg Formula: 0x020800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of pseudowire Mbit packet transmitted

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_Mbit_Counter_ro_Base                                              0x020800
#define cAf6Reg_Pseudowire_Transmit_Mbit_Counter_ro(PwId)                                    (0x020800+(PwId))
#define cAf6Reg_Pseudowire_Transmit_Mbit_Counter_ro_WidthVal                                                32
#define cAf6Reg_Pseudowire_Transmit_Mbit_Counter_ro_WriteMask                                              0x0

/*--------------------------------------
BitField Name: TxPwMbitPkt
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_Mbit_Counter_ro_TxPwMbitPkt_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_Mbit_Counter_ro_TxPwMbitPkt_Bit_End                                        31
#define cAf6_Pseudowire_Transmit_Mbit_Counter_ro_TxPwMbitPkt_Mask                                     cBit31_0
#define cAf6_Pseudowire_Transmit_Mbit_Counter_ro_TxPwMbitPkt_Shift                                           0
#define cAf6_Pseudowire_Transmit_Mbit_Counter_ro_TxPwMbitPkt_MaxVal                                 0xffffffff
#define cAf6_Pseudowire_Transmit_Mbit_Counter_ro_TxPwMbitPkt_MinVal                                        0x0
#define cAf6_Pseudowire_Transmit_Mbit_Counter_ro_TxPwMbitPkt_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Mbit Counter
Reg Addr   : 0x020000 - 0x0200FF(RC)
Reg Formula: 0x020000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of pseudowire Mbit packet transmitted

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_Mbit_Counter_rc_Base                                              0x020000
#define cAf6Reg_Pseudowire_Transmit_Mbit_Counter_rc(PwId)                                    (0x020000+(PwId))
#define cAf6Reg_Pseudowire_Transmit_Mbit_Counter_rc_WidthVal                                                32
#define cAf6Reg_Pseudowire_Transmit_Mbit_Counter_rc_WriteMask                                              0x0

/*--------------------------------------
BitField Name: TxPwMbitPkt
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_Mbit_Counter_rc_TxPwMbitPkt_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_Mbit_Counter_rc_TxPwMbitPkt_Bit_End                                        31
#define cAf6_Pseudowire_Transmit_Mbit_Counter_rc_TxPwMbitPkt_Mask                                     cBit31_0
#define cAf6_Pseudowire_Transmit_Mbit_Counter_rc_TxPwMbitPkt_Shift                                           0
#define cAf6_Pseudowire_Transmit_Mbit_Counter_rc_TxPwMbitPkt_MaxVal                                 0xffffffff
#define cAf6_Pseudowire_Transmit_Mbit_Counter_rc_TxPwMbitPkt_MinVal                                        0x0
#define cAf6_Pseudowire_Transmit_Mbit_Counter_rc_TxPwMbitPkt_RstVal                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Mbit Counter
Reg Addr   : 0x021800 - 0x0218FF(RO)
Reg Formula: 0x021800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of pseudowire Mbit packet transmitted

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Mbit_Counter_ro_Base                                               0x021800
#define cAf6Reg_Pseudowire_Receive_Mbit_Counter_ro(PwId)                                     (0x021800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Mbit_Counter_ro_WidthVal                                                 32
#define cAf6Reg_Pseudowire_Receive_Mbit_Counter_ro_WriteMask                                               0x0

/*--------------------------------------
BitField Name: RxPwMbitPkt
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Mbit_Counter_ro_RxPwMbitPkt_Bit_Start                                        0
#define cAf6_Pseudowire_Receive_Mbit_Counter_ro_RxPwMbitPkt_Bit_End                                         31
#define cAf6_Pseudowire_Receive_Mbit_Counter_ro_RxPwMbitPkt_Mask                                      cBit31_0
#define cAf6_Pseudowire_Receive_Mbit_Counter_ro_RxPwMbitPkt_Shift                                            0
#define cAf6_Pseudowire_Receive_Mbit_Counter_ro_RxPwMbitPkt_MaxVal                                  0xffffffff
#define cAf6_Pseudowire_Receive_Mbit_Counter_ro_RxPwMbitPkt_MinVal                                         0x0
#define cAf6_Pseudowire_Receive_Mbit_Counter_ro_RxPwMbitPkt_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Mbit Counter
Reg Addr   : 0x021000 - 0x0210FF(RC)
Reg Formula: 0x021000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of pseudowire Mbit packet transmitted

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Mbit_Counter_rc_Base                                               0x021000
#define cAf6Reg_Pseudowire_Receive_Mbit_Counter_rc(PwId)                                     (0x021000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Mbit_Counter_rc_WidthVal                                                 32
#define cAf6Reg_Pseudowire_Receive_Mbit_Counter_rc_WriteMask                                               0x0

/*--------------------------------------
BitField Name: RxPwMbitPkt
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Mbit_Counter_rc_RxPwMbitPkt_Bit_Start                                        0
#define cAf6_Pseudowire_Receive_Mbit_Counter_rc_RxPwMbitPkt_Bit_End                                         31
#define cAf6_Pseudowire_Receive_Mbit_Counter_rc_RxPwMbitPkt_Mask                                      cBit31_0
#define cAf6_Pseudowire_Receive_Mbit_Counter_rc_RxPwMbitPkt_Shift                                            0
#define cAf6_Pseudowire_Receive_Mbit_Counter_rc_RxPwMbitPkt_MaxVal                                  0xffffffff
#define cAf6_Pseudowire_Receive_Mbit_Counter_rc_RxPwMbitPkt_MinVal                                         0x0
#define cAf6_Pseudowire_Receive_Mbit_Counter_rc_RxPwMbitPkt_RstVal                                         0x0
/*------------------------------------------------------------------------------
Reg Name   : Pseudowire_Receive_Duplicate_Counter
Reg Addr   : 0x022800 - 0x0228FF(RO)
Reg Formula: 0x022800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of pseudowire Duplicate packet received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Duplicate_Counter_ro_Base                                          0x022800
#define cAf6Reg_Pseudowire_Receive_Duplicate_Counter_ro(PwId)                                (0x022800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Duplicate_Counter_ro_WidthVal                                            32
#define cAf6Reg_Pseudowire_Receive_Duplicate_Counter_ro_WriteMask                                          0x0

/*--------------------------------------
BitField Name: RxPwDuplicatePkt
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Duplicate_Counter_ro_RxPwDuplicatePkt_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Duplicate_Counter_ro_RxPwDuplicatePkt_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Duplicate_Counter_ro_RxPwDuplicatePkt_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Duplicate_Counter_ro_RxPwDuplicatePkt_Shift                                       0
#define cAf6_Pseudowire_Receive_Duplicate_Counter_ro_RxPwDuplicatePkt_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Duplicate_Counter_ro_RxPwDuplicatePkt_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Duplicate_Counter_ro_RxPwDuplicatePkt_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire_Receive_Duplicate_Counter
Reg Addr   : 0x022000 - 0x0220FF(RC)
Reg Formula: 0x022000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of pseudowire Duplicate packet received

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Duplicate_Counter_rc_Base                                          0x022000
#define cAf6Reg_Pseudowire_Receive_Duplicate_Counter_rc(PwId)                                (0x022000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Duplicate_Counter_rc_WidthVal                                            32
#define cAf6Reg_Pseudowire_Receive_Duplicate_Counter_rc_WriteMask                                          0x0

/*--------------------------------------
BitField Name: RxPwDuplicatePkt
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Duplicate_Counter_rc_RxPwDuplicatePkt_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Duplicate_Counter_rc_RxPwDuplicatePkt_Bit_End                                      31
#define cAf6_Pseudowire_Receive_Duplicate_Counter_rc_RxPwDuplicatePkt_Mask                                cBit31_0
#define cAf6_Pseudowire_Receive_Duplicate_Counter_rc_RxPwDuplicatePkt_Shift                                       0
#define cAf6_Pseudowire_Receive_Duplicate_Counter_rc_RxPwDuplicatePkt_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Receive_Duplicate_Counter_rc_RxPwDuplicatePkt_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Duplicate_Counter_rc_RxPwDuplicatePkt_RstVal

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Jitter Buffer Underrun Event Counter
Reg Addr   : 0x01F800 - 0x01F8FF(RO)
Reg Formula: 0x01F800 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of jitter buffer transition events from normal to underrun status

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro_Base                                0x01F800
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro(PwId)                       (0x01F800+(PwId))
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxUnderrunPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro_RxUnderrunPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro_RxUnderrunPk_Bit_End                                      15
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro_RxUnderrunPk_Mask                                cBit15_0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro_RxUnderrunPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro_RxUnderrunPk_MaxVal                                  0xffff
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro_RxUnderrunPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro_RxUnderrunPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Receive Jitter Buffer Underrun Event Counter
Reg Addr   : 0x01F000 - 0x01F0FF(RC)
Reg Formula: 0x01F000 + PwId
    Where  : 
           + $PwId (0 - 255): Pseudowire IDs
Reg Desc   : 
Count number of jitter buffer transition events from normal to underrun status

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc_Base                                0x01F000
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc(PwId)                       (0x01F000+(PwId))
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc_WidthVal                                      32
#define cAf6Reg_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: RxUnderrunPk
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc_RxUnderrunPk_Bit_Start                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc_RxUnderrunPk_Bit_End                                      15
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc_RxUnderrunPk_Mask                                cBit15_0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc_RxUnderrunPk_Shift                                       0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc_RxUnderrunPk_MaxVal                                  0xffff
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc_RxUnderrunPk_MinVal                                     0x0
#define cAf6_Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc_RxUnderrunPk_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet and Pseudowire Counter Sticky
Reg Addr   : 0x000008
Reg Formula: 
    Where  : 
Reg Desc   : 
This register sticky AF6FHW0013 error counter alarms. This register must be cleared by written to all one value after power up.

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_and_Pseudowire_Counter_Sticky_Base                                           0x000008
#define cAf6Reg_Ethernet_and_Pseudowire_Counter_Sticky                                                0x000008
#define cAf6Reg_Ethernet_and_Pseudowire_Counter_Sticky_WidthVal                                             32
#define cAf6Reg_Ethernet_and_Pseudowire_Counter_Sticky_WriteMask                                           0x0

/*--------------------------------------
BitField Name: UnderrunEvt
BitField Type: RW
BitField Desc: This bit set when Pseudowire Receive Jitter Buffer Underrun Event
Counter counts up.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UnderrunEvt_Bit_Start                                      14
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UnderrunEvt_Bit_End                                      14
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UnderrunEvt_Mask                                    cBit14
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UnderrunEvt_Shift                                       14
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UnderrunEvt_MaxVal                                     0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UnderrunEvt_MinVal                                     0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UnderrunEvt_RstVal                                     0x0

/*--------------------------------------
BitField Name: FcsErrEvt
BitField Type: RW
BitField Desc: This bit set when Ethernet Receive FCS Error Packet Counter
counts up.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_FcsErrEvt_Bit_Start                                      13
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_FcsErrEvt_Bit_End                                       13
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_FcsErrEvt_Mask                                      cBit13
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_FcsErrEvt_Shift                                         13
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_FcsErrEvt_MaxVal                                       0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_FcsErrEvt_MinVal                                       0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_FcsErrEvt_RstVal                                       0x0

/*--------------------------------------
BitField Name: EthErrEvt
BitField Type: RW
BitField Desc: This bit set when Ethernet Receive Header Error Packet Counter
counts up.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_EthErrEvt_Bit_Start                                      12
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_EthErrEvt_Bit_End                                       12
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_EthErrEvt_Mask                                      cBit12
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_EthErrEvt_Shift                                         12
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_EthErrEvt_MaxVal                                       0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_EthErrEvt_MinVal                                       0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_EthErrEvt_RstVal                                       0x0

/*--------------------------------------
BitField Name: PsnErrEvt
BitField Type: RW
BitField Desc: This bit set when Ethernet Receive PSN Header Error Packet
Counter counts up.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_PsnErrEvt_Bit_Start                                      11
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_PsnErrEvt_Bit_End                                       11
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_PsnErrEvt_Mask                                      cBit11
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_PsnErrEvt_Shift                                         11
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_PsnErrEvt_MaxVal                                       0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_PsnErrEvt_MinVal                                       0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_PsnErrEvt_RstVal                                       0x0

/*--------------------------------------
BitField Name: UdpErrEvt
BitField Type: RW
BitField Desc: This bit set when Ethernet Receive UDP Port Error Packet Counter
counts up.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UdpErrEvt_Bit_Start                                      10
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UdpErrEvt_Bit_End                                       10
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UdpErrEvt_Mask                                      cBit10
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UdpErrEvt_Shift                                         10
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UdpErrEvt_MaxVal                                       0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UdpErrEvt_MinVal                                       0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_UdpErrEvt_RstVal                                       0x0

/*--------------------------------------
BitField Name: ReorDropEvt
BitField Type: RW
BitField Desc: This bit set when Pseudowire Receive Reorder Drop Packet Counter
counts up.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorDropEvt_Bit_Start                                       9
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorDropEvt_Bit_End                                       9
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorDropEvt_Mask                                     cBit9
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorDropEvt_Shift                                        9
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorDropEvt_MaxVal                                     0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorDropEvt_MinVal                                     0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorDropEvt_RstVal                                     0x0

/*--------------------------------------
BitField Name: ReorOSeqEvt
BitField Type: RW
BitField Desc: This bit set when Pseudowire Receive Reorder Out Of Sequence
Packet Counter counts up.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorOSeqEvt_Bit_Start                                       8
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorOSeqEvt_Bit_End                                       8
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorOSeqEvt_Mask                                     cBit8
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorOSeqEvt_Shift                                        8
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorOSeqEvt_MaxVal                                     0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorOSeqEvt_MinVal                                     0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorOSeqEvt_RstVal                                     0x0

/*--------------------------------------
BitField Name: LofsEvt
BitField Type: RW
BitField Desc: This bit set when Pseudowire Receive LOFS Transition Counter
counts up.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_LofsEvt_Bit_Start                                        7
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_LofsEvt_Bit_End                                          7
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_LofsEvt_Mask                                         cBit7
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_LofsEvt_Shift                                            7
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_LofsEvt_MaxVal                                         0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_LofsEvt_MinVal                                         0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_LofsEvt_RstVal                                         0x0

/*--------------------------------------
BitField Name: OverrunEvt
BitField Type: RW
BitField Desc: This bit set when Pseudowire Receive Jitter Buffer Overrun Packet
Counter counts up.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_OverrunEvt_Bit_Start                                       6
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_OverrunEvt_Bit_End                                       6
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_OverrunEvt_Mask                                      cBit6
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_OverrunEvt_Shift                                         6
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_OverrunEvt_MaxVal                                      0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_OverrunEvt_MinVal                                      0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_OverrunEvt_RstVal                                      0x0

/*--------------------------------------
BitField Name: ReorLostEvt
BitField Type: RW
BitField Desc: This bit set when Pseudowire Receive Reorder Lost Packet Counter
counts up.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorLostEvt_Bit_Start                                       5
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorLostEvt_Bit_End                                       5
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorLostEvt_Mask                                     cBit5
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorLostEvt_Shift                                        5
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorLostEvt_MaxVal                                     0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorLostEvt_MinVal                                     0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_ReorLostEvt_RstVal                                     0x0

/*--------------------------------------
BitField Name: MalformEvt
BitField Type: RW
BitField Desc: This bit set when Pseudowire Receive Malform Packet Counter
counts up.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_MalformEvt_Bit_Start                                       4
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_MalformEvt_Bit_End                                       4
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_MalformEvt_Mask                                      cBit4
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_MalformEvt_Shift                                         4
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_MalformEvt_MaxVal                                      0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_MalformEvt_MinVal                                      0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_MalformEvt_RstVal                                      0x0

/*--------------------------------------
BitField Name: TxLbitEvt
BitField Type: RW
BitField Desc: This bit set when Pseudowire Pseudowire Transmit L Bit Packet
Counter counts up.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxLbitEvt_Bit_Start                                       3
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxLbitEvt_Bit_End                                        3
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxLbitEvt_Mask                                       cBit3
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxLbitEvt_Shift                                          3
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxLbitEvt_MaxVal                                       0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxLbitEvt_MinVal                                       0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxLbitEvt_RstVal                                       0x0

/*--------------------------------------
BitField Name: RxLbitEvt
BitField Type: RW
BitField Desc: This bit set when Pseudowire Pseudowire Receive L Bit Packet
Counter counts up.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxLbitEvt_Bit_Start                                       2
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxLbitEvt_Bit_End                                        2
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxLbitEvt_Mask                                       cBit2
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxLbitEvt_Shift                                          2
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxLbitEvt_MaxVal                                       0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxLbitEvt_MinVal                                       0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxLbitEvt_RstVal                                       0x0

/*--------------------------------------
BitField Name: TxRbitEvt
BitField Type: RW
BitField Desc: This bit set when Pseudowire Pseudowire Transmit R Bit Packet
Counter counts up.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxRbitEvt_Bit_Start                                       1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxRbitEvt_Bit_End                                        1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxRbitEvt_Mask                                       cBit1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxRbitEvt_Shift                                          1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxRbitEvt_MaxVal                                       0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxRbitEvt_MinVal                                       0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_TxRbitEvt_RstVal                                       0x0

/*--------------------------------------
BitField Name: RxRbitEvt
BitField Type: RW
BitField Desc: This bit set when Pseudowire Pseudowire Receive R Bit Packet
Counter counts up.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxRbitEvt_Bit_Start                                       0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxRbitEvt_Bit_End                                        0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxRbitEvt_Mask                                       cBit0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxRbitEvt_Shift                                          0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxRbitEvt_MaxVal                                       0x1
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxRbitEvt_MinVal                                       0x0
#define cAf6_Ethernet_and_Pseudowire_Counter_Sticky_RxRbitEvt_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet Header Error Sticky
Reg Addr   : 0x00000A
Reg Formula: 
    Where  : 
Reg Desc   : 
This register stores AF6FHW0013 Jitter Buffer empty alarms which will cause interrupt to CPU. This register must be cleared by written to all one value after power up.

------------------------------------------------------------------------------*/
#define cAf6Reg_Receive_Ethernet_Header_Error_Sticky_Base                                             0x00000A
#define cAf6Reg_Receive_Ethernet_Header_Error_Sticky                                                  0x00000A
#define cAf6Reg_Receive_Ethernet_Header_Error_Sticky_WidthVal                                               32
#define cAf6Reg_Receive_Ethernet_Header_Error_Sticky_WriteMask                                             0x0

/*--------------------------------------
BitField Name: RxEthUdpLenErr
BitField Type: RW
BitField Desc: Set 1 to indicate UDP length field error.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpLenErr_Bit_Start                                      10
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpLenErr_Bit_End                                      10
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpLenErr_Mask                                   cBit10
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpLenErr_Shift                                      10
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpLenErr_MaxVal                                     0x1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpLenErr_MinVal                                     0x0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpLenErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: RxEthUdpPortErr
BitField Type: RW
BitField Desc: Set 1 to indicate UDP port differ from pseudowire identification
descibed in Classify Pseudowire Identification Control register.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpPortErr_Bit_Start                                       9
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpPortErr_Bit_End                                       9
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpPortErr_Mask                                   cBit9
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpPortErr_Shift                                       9
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpPortErr_MaxVal                                     0x1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpPortErr_MinVal                                     0x0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUdpPortErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: RxEthMplsOutLabelErr
BitField Type: RW
BitField Desc: Set 1 to indicate MPLS outer label differ from RxPsnMplsOutLabel
field descibed in Classify Global PSN Control register.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsOutLabelErr_Bit_Start                                       8
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsOutLabelErr_Bit_End                                       8
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsOutLabelErr_Mask                                   cBit8
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsOutLabelErr_Shift                                       8
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsOutLabelErr_MaxVal                                     0x1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsOutLabelErr_MinVal                                     0x0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsOutLabelErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: RxEthMplsInLabelErr
BitField Type: RW
BitField Desc: Set 1 to indicate MPLS inner label differ from pseudowire
identification descibed in Classify Pseudowire Identification Control register.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsInLabelErr_Bit_Start                                       7
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsInLabelErr_Bit_End                                       7
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsInLabelErr_Mask                                   cBit7
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsInLabelErr_Shift                                       7
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsInLabelErr_MaxVal                                     0x1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsInLabelErr_MinVal                                     0x0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMplsInLabelErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: RxEthMefEcidLabelErr
BitField Type: RW
BitField Desc: Set 1 to indicate MEF8 ECID differ from pseudowire identification
descibed in Classify Pseudowire Identification Control register.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMefEcidLabelErr_Bit_Start                                       6
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMefEcidLabelErr_Bit_End                                       6
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMefEcidLabelErr_Mask                                   cBit6
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMefEcidLabelErr_Shift                                       6
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMefEcidLabelErr_MaxVal                                     0x1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMefEcidLabelErr_MinVal                                     0x0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthMefEcidLabelErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: RxEthIpv6HdrErr
BitField Type: RW
BitField Desc: Set 1 to indicate Ipv6 header error
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv6HdrErr_Bit_Start                                       5
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv6HdrErr_Bit_End                                       5
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv6HdrErr_Mask                                   cBit5
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv6HdrErr_Shift                                       5
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv6HdrErr_MaxVal                                     0x1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv6HdrErr_MinVal                                     0x0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv6HdrErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: RxEthIpv4HdrErr
BitField Type: RW
BitField Desc: Set 1 to indicate Ipv4 header error
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv4HdrErr_Bit_Start                                       4
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv4HdrErr_Bit_End                                       4
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv4HdrErr_Mask                                   cBit4
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv4HdrErr_Shift                                       4
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv4HdrErr_MaxVal                                     0x1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv4HdrErr_MinVal                                     0x0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthIpv4HdrErr_RstVal                                     0x0

/*--------------------------------------
BitField Name: RxEthOversize
BitField Type: RW
BitField Desc: Set 1 to indicate an oversize packet received
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthOversize_Bit_Start                                       3
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthOversize_Bit_End                                       3
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthOversize_Mask                                     cBit3
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthOversize_Shift                                        3
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthOversize_MaxVal                                     0x1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthOversize_MinVal                                     0x0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthOversize_RstVal                                     0x0

/*--------------------------------------
BitField Name: RxEthUndersize
BitField Type: RW
BitField Desc: Set 1 to indicate an undersize packet received
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUndersize_Bit_Start                                       2
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUndersize_Bit_End                                       2
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUndersize_Mask                                    cBit2
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUndersize_Shift                                       2
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUndersize_MaxVal                                     0x1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUndersize_MinVal                                     0x0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthUndersize_RstVal                                     0x0

/*--------------------------------------
BitField Name: RxEthTypeErr
BitField Type: RW
BitField Desc: Set 1 to indicate reception of packets with Ethernet Type field
not supported by AF6FHW0013
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthTypeErr_Bit_Start                                       1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthTypeErr_Bit_End                                       1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthTypeErr_Mask                                      cBit1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthTypeErr_Shift                                         1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthTypeErr_MaxVal                                      0x1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthTypeErr_MinVal                                      0x0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthTypeErr_RstVal                                      0x0

/*--------------------------------------
BitField Name: RxEthFcsErr
BitField Type: RW
BitField Desc: Set 1 to indicate reception of packets with FCS error or Ethernet
PHY error
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthFcsErr_Bit_Start                                       0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthFcsErr_Bit_End                                        0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthFcsErr_Mask                                       cBit0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthFcsErr_Shift                                          0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthFcsErr_MaxVal                                       0x1
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthFcsErr_MinVal                                       0x0
#define cAf6_Receive_Ethernet_Header_Error_Sticky_RxEthFcsErr_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Per_Alarm_Interrupt_Enable_Control
Reg Addr   : 0x001000 - 0x0010FF
Reg Formula: 0x001000 +  GrpID*32 + BitID
    Where  : 
           + $GrpID(0-7):  Pseudowire ID bits[7:5]
           + $BitID(0-31): Pseudowire ID bits [4:0]
Reg Desc   : 
This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_Base                                       0x001000
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control(GrpID, BitID)           (0x001000+(GrpID)*32+(BitID))
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_WidthVal                                         32
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Enable_Control_WriteMask                                       0x0
/*--------------------------------------
BitField Name: StrayStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Stray packet event to generate an interrupt
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_Bit_Start                         8
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_Bit_End                           8
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_Mask                              cBit8
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_Shift                             8
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_MaxVal                            0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_MinVal                            0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_StrayStateChgIntrEn_RstVal                            0x0

/*--------------------------------------
BitField Name: MalformStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Malform packet event to generate an interrupt
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_Bit_Start                                    7
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_Bit_End                                      7
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_Mask                                     cBit7
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_Shift                                        7
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MalformStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: MbitStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Mbit packet event to generate an interrupt
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_Bit_Start                                       6
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_Bit_End                                       6
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_Mask                                   cBit6
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_Shift                                       6
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_MbitStateChgIntrEn_RstVal

/*--------------------------------------
BitField Name: RbitStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Rbit packet event to generate an interrupt
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Bit_Start                                       5
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Bit_End                                       5
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Mask                                   cBit5
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_Shift                                       5
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_RbitStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: LostPkStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable lost of packet event to generate an interrupt
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LostPkStateChgIntrEn_Bit_Start                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LostPkStateChgIntrEn_Bit_End                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LostPkStateChgIntrEn_Mask                                   cBit4
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LostPkStateChgIntrEn_Shift                                       4
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LostPkStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LostPkStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LostPkStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: UnderrunStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change jitter buffer state event from normal to
underrun and vice versa in the related pseudowire to generate an interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Bit_Start                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Bit_End                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Mask                                   cBit3
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_Shift                                       3
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_UnderrunStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: OverrunStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change jitter buffer state event from normal to
overrun and vice versa in the related pseudowire to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Bit_Start                                       2
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Bit_End                                       2
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Mask                                   cBit2
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_Shift                                       2
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_OverrunStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: LofsStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change lost of frame state(LOFS) event from
normal to LOFS and vice versa in the related pseudowire to generate an
interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Bit_Start                                       1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Bit_End                                       1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Mask                                   cBit1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_Shift                                       1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LofsStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: LbitStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Lbit packet event to generate an interrupt
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Bit_Start                                       0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Bit_End                                       0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Mask                                   cBit0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_Shift                                       0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Enable_Control_LbitStateChgIntrEn_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Per_Alarm_Interrupt_Status
Reg Addr   : 0x001100 - 0x0011FF
Reg Formula: 0x001100 +  GrpID*32 + BitID
    Where  : 
           + $GrpID(0-7):  Pseudowire ID bits[7:5]
           + $BitID(0-31): Pseudowire ID bits [4:0]
Reg Desc   : 
This is the per Alarm interrupt status of pseudowires. Each register is used to store 5 sticky bits for 5 alarms in pseudowires

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status_Base                                               0x001100
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status(GrpID, BitID)                 (0x001100+(GrpID)*32+(BitID))
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status_WidthVal                                                 32
#define cAf6Reg_Counter_Per_Alarm_Interrupt_Status_WriteMask                                               0x0
/*--------------------------------------
BitField Name: StrayStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 when a Stray packet event is detected in the pseudowire
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrEn_Bit_Start                                    8
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrEn_Bit_End                                      8
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrEn_Mask                                     cBit8
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrEn_Shift                                        8
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_StrayStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: MalformStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 when a Malform packet event is detected in the pseudowire
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrEn_Bit_Start                                    7
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrEn_Bit_End                                      7
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrEn_Mask                                     cBit7
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrEn_Shift                                        7
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MalformStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: MbitStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 when a Mbit packet event is detected in the pseudowire
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrEn_Bit_Start                                    6
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrEn_Bit_End                                      6
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrEn_Mask                                     cBit6
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrEn_Shift                                        6
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_MbitStateChgIntrEn_RstVal

/*--------------------------------------
BitField Name: RbitStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 when a Rbit packet event is detected in the pseudowire
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrEn_Bit_Start                                 5
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrEn_Bit_End                                   5
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrEn_Mask                                  cBit5
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrEn_Shift                                     5
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrEn_MaxVal                                  0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrEn_MinVal                                  0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_RbitStateChgIntrEn_RstVal                                  0x0

/*--------------------------------------
BitField Name: LostPkStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 when a lost of packet event is detected in the pseudowire
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LostPkStateChgIntrEn_Bit_Start                               4
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LostPkStateChgIntrEn_Bit_End                                 4
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LostPkStateChgIntrEn_Mask                                cBit4
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LostPkStateChgIntrEn_Shift                                   4
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LostPkStateChgIntrEn_MaxVal                                0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LostPkStateChgIntrEn_MinVal                                0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LostPkStateChgIntrEn_RstVal                                0x0

/*--------------------------------------
BitField Name: UnderrunStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 when there is a change in jitter buffer underrun state  in
the related pseudowire
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrEn_Bit_Start                                    3
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrEn_Bit_End                                      3
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrEn_Mask                                     cBit3
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrEn_Shift                                        3
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_UnderrunStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: OverrunStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 when there is a change in jitter buffer overrun state in
the related pseudowire
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrEn_Bit_Start                                    2
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrEn_Bit_End                                      2
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrEn_Mask                                     cBit2
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrEn_Shift                                        2
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_OverrunStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: LofsStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 when there is a change in lost of frame state(LOFS) in the
related pseudowire
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrEn_Bit_Start                                    1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrEn_Bit_End                                      1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrEn_Mask                                     cBit1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrEn_Shift                                        1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LofsStateChgIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: LbitStateChgIntrEn
BitField Type: RW
BitField Desc: Set 1 when a Lbit packet event is detected in the pseudowire
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrEn_Bit_Start                                    0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrEn_Bit_End                                      0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrEn_Mask                                     cBit0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrEn_Shift                                        0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrEn_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrEn_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Interrupt_Status_LbitStateChgIntrEn_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter Per Alarm Current Status
Reg Addr   : 0x001200 - 0x0012FF
Reg Formula: 0x001200 +  GrpID*32 + BitID
    Where  : 
           + $GrpID(0-7):  Pseudowire ID bits[7:5]
           + $BitID(0-31): Pseudowire ID bits [4:0]
Reg Desc   : 
This is the per Alarm current status of pseudowires. Each register is used to store 4 current status of 4 alarms in pseudowires

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Per_Alarm_Current_Status_Base                                                 0x001200
#define cAf6Reg_Counter_Per_Alarm_Current_Status(GrpID, BitID)                   (0x001200+(GrpID)*32+(BitID))
#define cAf6Reg_Counter_Per_Alarm_Current_Status_WidthVal                                                   32
#define cAf6Reg_Counter_Per_Alarm_Current_Status_WriteMask                                                 0x0
/*--------------------------------------
BitField Name: StrayCurStatus
BitField Type: RW
BitField Desc: Stray current status in the related pseudowire. When it changes
from 0 to 1 or vice versa, the  MalformStateChgIntr bit in the Counter Per Alarm
Interrupt Status register of the related pseudowire is set or clear.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_Bit_Start                                       8
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_Bit_End                                         8
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_Mask                                        cBit8
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_Shift                                           8
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_MaxVal                                        0x1
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_MinVal                                        0x0
#define cAf6_Counter_Per_Alarm_Current_Status_StrayCurStatus_RstVal                                        0x0

/*--------------------------------------
BitField Name: MalformCurStatus
BitField Type: RW
BitField Desc: Malform current status in the related pseudowire. When it changes
from 0 to 1 or vice versa, the  MalformStateChgIntr bit in the Counter Per Alarm
Interrupt Status register of the related pseudowire is set or clear.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_Bit_Start                                     7
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_Bit_End                                       7
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_Mask                                      cBit7
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_Shift                                         7
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_MaxVal                                      0x1
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_MinVal                                      0x0
#define cAf6_Counter_Per_Alarm_Current_Status_MalformCurStatus_RstVal                                      0x0

/*--------------------------------------
BitField Name: MbitCurStatus
BitField Type: RW
BitField Desc: Mbits current status in the related pseudowire. When it changes
from 0 to 1 or vice versa, the  MbitStateChgIntr bit in the Counter Per Alarm
Interrupt Status register of the related pseudowire is set or clear.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_Counter Per Alarm Current Status_MbitCurStatus_Bit_Start                                        6
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_Bit_End                                          6
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_Mask                                         cBit6
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_Shift                                            6
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_MaxVal                                         0x1
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_MinVal                                         0x0
#define cAf6_Counter_Per_Alarm_Current_Status_MbitCurStatus_RstVal

/*--------------------------------------
BitField Name: RbitCurStatus
BitField Type: RW
BitField Desc: Rbits current status in the related pseudowire. When it changes
from 0 to 1 or vice versa, the  LbitStateChgIntr bit in the Counter Per Alarm
Interrupt Status register of the related pseudowire is set or clear.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_Bit_Start                                        5
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_Bit_End                                          5
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_Mask                                         cBit5
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_Shift                                            5
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_MaxVal                                         0x1
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_MinVal                                         0x0
#define cAf6_Counter_Per_Alarm_Current_Status_RbitCurStatus_RstVal                                         0x0

/*--------------------------------------
BitField Name: LostPkCurStatus
BitField Type: RW
BitField Desc: Lost of packet current status in the related pseudowire. When it
changes from 0 to 1 or vice versa, the  LbitStateChgIntr bit in the Counter Per
Alarm Interrupt Status register of the related pseudowire is set or clear.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_LostPkCurStatus_Bit_Start                                      4
#define cAf6_Counter_Per_Alarm_Current_Status_LostPkCurStatus_Bit_End                                        4
#define cAf6_Counter_Per_Alarm_Current_Status_LostPkCurStatus_Mask                                       cBit4
#define cAf6_Counter_Per_Alarm_Current_Status_LostPkCurStatus_Shift                                          4
#define cAf6_Counter_Per_Alarm_Current_Status_LostPkCurStatus_MaxVal                                       0x1
#define cAf6_Counter_Per_Alarm_Current_Status_LostPkCurStatus_MinVal                                       0x0
#define cAf6_Counter_Per_Alarm_Current_Status_LostPkCurStatus_RstVal                                       0x0

/*--------------------------------------
BitField Name: UnderrunCurStatus
BitField Type: RW
BitField Desc: Jitter buffer underrun current status in the related pseudowire.
When it changes from 0 to 1 or vice versa, the  UnderrunStateChgIntr bit in the
Counter_Per_Alarm_Interrupt_Status register of the related pseudowire is set or
clear.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Bit_Start                                    3
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Bit_End                                      3
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Mask                                     cBit3
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_Shift                                        3
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Current_Status_UnderrunCurStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: OverrunCurStatus
BitField Type: RW
BitField Desc: Jitter buffer overrun current status in the related pseudowire.
When it changes from 0 to 1 or vice versa, the  OverrunStateChgIntr bit in the
Counter_Per_Alarm_Interrupt_Status register of the related pseudowire is set or
clear.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Bit_Start                                     2
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Bit_End                                       2
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Mask                                      cBit2
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_Shift                                         2
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_MaxVal                                      0x1
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_MinVal                                      0x0
#define cAf6_Counter_Per_Alarm_Current_Status_OverrunCurStatus_RstVal                                      0x0

/*--------------------------------------
BitField Name: LofsStateCurStatus
BitField Type: RW
BitField Desc: Lost of frame state current status in the related pseudowire.
When it changes from 0 to 1 or vice versa, the  LofsStateChgIntr bit in the
Counter_Per_Alarm_Interrupt_Status register of the related pseudowire is set or
clear.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_Bit_Start                                    1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_Bit_End                                      1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_Mask                                     cBit1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_Shift                                        1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Current_Status_LofsStateCurStatus_RstVal                                     0x0

/*--------------------------------------
BitField Name: LbitStateCurStatus
BitField Type: RW
BitField Desc: Lbits current status in the related pseudowire. When it changes
from 0 to 1 or vice versa, the  LbitStateChgIntr bit in the Counter Per Alarm
Interrupt Status register of the related pseudowire is set or clear.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Bit_Start                                    0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Bit_End                                      0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Mask                                     cBit0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_Shift                                        0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_MaxVal                                     0x1
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_MinVal                                     0x0
#define cAf6_Counter_Per_Alarm_Current_Status_LbitStateCurStatus_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter Interrupt OR Status
Reg Addr   : 0x001300 - 0x001307
Reg Formula: 0x001300 +  GrpID
    Where  : 
           + $GrpID(0-7): Pseudowire ID bits[PWID-1:5]
Reg Desc   : 
The register consists of 32 bits for 32 pseudowires. Each bit is used to store Interrupt OR status of the related pseudowires.

------------------------------------------------------------------------------*/
#define cAf6Reg_Counter_Interrupt_OR_Status_Base                                                      0x001300
#define cAf6Reg_Counter_Interrupt_OR_Status(GrpID)                                          (0x001300+(GrpID))
#define cAf6Reg_Counter_Interrupt_OR_Status_WidthVal                                                        32
#define cAf6Reg_Counter_Interrupt_OR_Status_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: IntrORStatus
BitField Type: RW
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the Counter_Per_Alarm_Interrupt_Status register of the related pseudowires to be
set and they are enabled to raise interrupt. Bit 0 of GrpID#0 for pseudowire 0,
bit 31 of GrpID#0 for pseudowire 31, bit 0 of GrpID#1 for pseudowire 32,
respectively.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_Bit_Start                                              0
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_Bit_End                                               31
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_Mask                                            cBit31_0
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_Shift                                                  0
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_MaxVal                                        0xffffffff
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_MinVal                                               0x0
#define cAf6_Counter_Interrupt_OR_Status_IntrORStatus_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter per Group Interrupt OR Status
Reg Addr   : 0x0013FF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 8 bits for 8 Group of the PW Counter. Each bit is used to store Interrupt OR status of the related Group.

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_per_group_intr_or_stat_Base                                                   0x0013FF
#define cAf6Reg_counter_per_group_intr_or_stat                                                        0x0013FF
#define cAf6Reg_counter_per_group_intr_or_stat_WidthVal                                                     32
#define cAf6Reg_counter_per_group_intr_or_stat_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: GroupIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt bit of corresponding Group is set and
its interrupt is enabled
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_Bit_Start                                         0
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_Bit_End                                           7
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_Mask                                        cBit7_0
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_Shift                                             0
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_MaxVal                                         0xff
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_MinVal                                          0x0
#define cAf6_counter_per_group_intr_or_stat_GroupIntrOrSta_RstVal                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Counter per Group Interrupt Enable Control
Reg Addr   : 0x00013FE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 8 interrupt enable bits for 8 group in the PW counter.

------------------------------------------------------------------------------*/
#define cAf6Reg_counter_per_group_intr_en_ctrl_Base                                                  0x00013FE
#define cAf6Reg_counter_per_group_intr_en_ctrl                                                       0x00013FE
#define cAf6Reg_counter_per_group_intr_en_ctrl_WidthVal                                                     32
#define cAf6Reg_counter_per_group_intr_en_ctrl_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: GroupIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related Group to generate interrupt.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Bit_Start                                            0
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Bit_End                                              7
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Mask                                           cBit7_0
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_Shift                                                0
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_MaxVal                                            0xff
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_MinVal                                             0x0
#define cAf6_counter_per_group_intr_en_ctrl_GroupIntrEn_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 1to64Byte Packet Counter
Reg Addr   : 0x000020(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 1 to 64 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_1to64Byte_Packet_Counter_ro_Base                                     0x000020
#define cAf6Reg_Ethernet_Receive_1to64Byte_Packet_Counter_ro                                          0x000020
#define cAf6Reg_Ethernet_Receive_1to64Byte_Packet_Counter_ro_WidthVal                                       32
#define cAf6Reg_Ethernet_Receive_1to64Byte_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_ro_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_ro_PacketNum_Shift                                       0
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 1to64Byte Packet Counter
Reg Addr   : 0x000021(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 1 to 64 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_1to64Byte_Packet_Counter_rc_Base                                     0x000021
#define cAf6Reg_Ethernet_Receive_1to64Byte_Packet_Counter_rc                                          0x000021
#define cAf6Reg_Ethernet_Receive_1to64Byte_Packet_Counter_rc_WidthVal                                       32
#define cAf6Reg_Ethernet_Receive_1to64Byte_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_rc_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_rc_PacketNum_Shift                                       0
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_1to64Byte_Packet_Counter_rc_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 1to64Byte Packet Counter
Reg Addr   : 0x000030(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 1 to 64 bytes to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_1to64Byte_Packet_Counter_ro_Base                                    0x000030
#define cAf6Reg_Ethernet_Transmit_1to64Byte_Packet_Counter_ro                                         0x000030
#define cAf6Reg_Ethernet_Transmit_1to64Byte_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Transmit_1to64Byte_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_ro_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_ro_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 1to64Byte Packet Counter
Reg Addr   : 0x000031(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 1 to 64 bytes to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_1to64Byte_Packet_Counter_rc_Base                                    0x000031
#define cAf6Reg_Ethernet_Transmit_1to64Byte_Packet_Counter_rc                                         0x000031
#define cAf6Reg_Ethernet_Transmit_1to64Byte_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Transmit_1to64Byte_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_rc_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_rc_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_1to64Byte_Packet_Counter_rc_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 65to128Byte Packet Counter
Reg Addr   : 0x000022(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 65 to 128 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_65to128Byte_Packet_Counter_ro_Base                                   0x000022
#define cAf6Reg_Ethernet_Receive_65to128Byte_Packet_Counter_ro                                        0x000022
#define cAf6Reg_Ethernet_Receive_65to128Byte_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_65to128Byte_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_ro_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_ro_PacketNum_Shift                                       0
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 65to128Byte Packet Counter
Reg Addr   : 0x000023(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 65 to 128 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_65to128Byte_Packet_Counter_rc_Base                                   0x000023
#define cAf6Reg_Ethernet_Receive_65to128Byte_Packet_Counter_rc                                        0x000023
#define cAf6Reg_Ethernet_Receive_65to128Byte_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_65to128Byte_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_rc_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_rc_PacketNum_Shift                                       0
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_65to128Byte_Packet_Counter_rc_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 65to128Byte Packet Counter
Reg Addr   : 0x000032(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 65 to 128 bytes to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_65to128Byte_Packet_Counter_ro_Base                                  0x000032
#define cAf6Reg_Ethernet_Transmit_65to128Byte_Packet_Counter_ro                                       0x000032
#define cAf6Reg_Ethernet_Transmit_65to128Byte_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Transmit_65to128Byte_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_ro_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_ro_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 65to128Byte Packet Counter
Reg Addr   : 0x000033(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 65 to 128 bytes to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_65to128Byte_Packet_Counter_rc_Base                                  0x000033
#define cAf6Reg_Ethernet_Transmit_65to128Byte_Packet_Counter_rc                                       0x000033
#define cAf6Reg_Ethernet_Transmit_65to128Byte_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Transmit_65to128Byte_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_rc_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_rc_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_65to128Byte_Packet_Counter_rc_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 129to256Byte Packet Counter
Reg Addr   : 0x000024(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 129 to 256 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_129to256Byte_Packet_Counter_ro_Base                                  0x000024
#define cAf6Reg_Ethernet_Receive_129to256Byte_Packet_Counter_ro                                       0x000024
#define cAf6Reg_Ethernet_Receive_129to256Byte_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_129to256Byte_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_ro_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_ro_PacketNum_Shift                                       0
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 129to256Byte Packet Counter
Reg Addr   : 0x000025(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 129 to 256 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_129to256Byte_Packet_Counter_rc_Base                                  0x000025
#define cAf6Reg_Ethernet_Receive_129to256Byte_Packet_Counter_rc                                       0x000025
#define cAf6Reg_Ethernet_Receive_129to256Byte_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_129to256Byte_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_rc_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_rc_PacketNum_Shift                                       0
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_129to256Byte_Packet_Counter_rc_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 129to256Byte Packet Counter
Reg Addr   : 0x000034(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 129 to 256 bytes to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_129to256Byte_Packet_Counter_ro_Base                                 0x000034
#define cAf6Reg_Ethernet_Transmit_129to256Byte_Packet_Counter_ro                                      0x000034
#define cAf6Reg_Ethernet_Transmit_129to256Byte_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Transmit_129to256Byte_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_ro_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_ro_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 129to256Byte Packet Counter
Reg Addr   : 0x000035(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 129 to 256 bytes to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_129to256Byte_Packet_Counter_rc_Base                                 0x000035
#define cAf6Reg_Ethernet_Transmit_129to256Byte_Packet_Counter_rc                                      0x000035
#define cAf6Reg_Ethernet_Transmit_129to256Byte_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Transmit_129to256Byte_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_rc_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_rc_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_129to256Byte_Packet_Counter_rc_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 257to512Byte Packet Counter
Reg Addr   : 0x000026(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 257 to 512 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_257to512Byte_Packet_Counter_ro_Base                                  0x000026
#define cAf6Reg_Ethernet_Receive_257to512Byte_Packet_Counter_ro                                       0x000026
#define cAf6Reg_Ethernet_Receive_257to512Byte_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_257to512Byte_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_ro_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_ro_PacketNum_Shift                                       0
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 257to512Byte Packet Counter
Reg Addr   : 0x000027(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 257 to 512 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_257to512Byte_Packet_Counter_rc_Base                                  0x000027
#define cAf6Reg_Ethernet_Receive_257to512Byte_Packet_Counter_rc                                       0x000027
#define cAf6Reg_Ethernet_Receive_257to512Byte_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_257to512Byte_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_rc_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_rc_PacketNum_Shift                                       0
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_257to512Byte_Packet_Counter_rc_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 257to512Byte Packet Counter
Reg Addr   : 0x000036(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 257 to 512 bytes to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_257to512Byte_Packet_Counter_ro_Base                                 0x000036
#define cAf6Reg_Ethernet_Transmit_257to512Byte_Packet_Counter_ro                                      0x000036
#define cAf6Reg_Ethernet_Transmit_257to512Byte_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Transmit_257to512Byte_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_ro_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_ro_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 257to512Byte Packet Counter
Reg Addr   : 0x000037(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 257 to 512 bytes to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_257to512Byte_Packet_Counter_rc_Base                                 0x000037
#define cAf6Reg_Ethernet_Transmit_257to512Byte_Packet_Counter_rc                                      0x000037
#define cAf6Reg_Ethernet_Transmit_257to512Byte_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Transmit_257to512Byte_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_rc_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_rc_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_257to512Byte_Packet_Counter_rc_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 513to1024Byte Packet Counter
Reg Addr   : 0x000028(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 513  to 1024 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_513to1024Byte_Packet_Counter_ro_Base                                 0x000028
#define cAf6Reg_Ethernet_Receive_513to1024Byte_Packet_Counter_ro                                      0x000028
#define cAf6Reg_Ethernet_Receive_513to1024Byte_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_513to1024Byte_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_ro_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_ro_PacketNum_Shift                                       0
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 513to1024Byte Packet Counter
Reg Addr   : 0x000029(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 513  to 1024 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_513to1024Byte_Packet_Counter_rc_Base                                 0x000029
#define cAf6Reg_Ethernet_Receive_513to1024Byte_Packet_Counter_rc                                      0x000029
#define cAf6Reg_Ethernet_Receive_513to1024Byte_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_513to1024Byte_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_rc_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_rc_PacketNum_Shift                                       0
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_513to1024Byte_Packet_Counter_rc_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 513to1024Byte Packet Counter
Reg Addr   : 0x000038(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 513  to 1024 bytes to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_513to1024Byte_Packet_Counter_ro_Base                                0x000038
#define cAf6Reg_Ethernet_Transmit_513to1024Byte_Packet_Counter_ro                                     0x000038
#define cAf6Reg_Ethernet_Transmit_513to1024Byte_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Transmit_513to1024Byte_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_ro_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_ro_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 513to1024Byte Packet Counter
Reg Addr   : 0x000039(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 513  to 1024 bytes to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_513to1024Byte_Packet_Counter_rc_Base                                0x000039
#define cAf6Reg_Ethernet_Transmit_513to1024Byte_Packet_Counter_rc                                     0x000039
#define cAf6Reg_Ethernet_Transmit_513to1024Byte_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Transmit_513to1024Byte_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_rc_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_rc_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_513to1024Byte_Packet_Counter_rc_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 1025to1528Byte Packet Counter
Reg Addr   : 0x00002A(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 1025   to 1528 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_1025to1528Byte_Packet_Counter_ro_Base                                0x00002A
#define cAf6Reg_Ethernet_Receive_1025to1528Byte_Packet_Counter_ro                                     0x00002A
#define cAf6Reg_Ethernet_Receive_1025to1528Byte_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_1025to1528Byte_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_ro_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_ro_PacketNum_Shift                                       0
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive 1025to1528Byte Packet Counter
Reg Addr   : 0x00002B(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length from 1025   to 1528 bytes from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_1025to1528Byte_Packet_Counter_rc_Base                                0x00002B
#define cAf6Reg_Ethernet_Receive_1025to1528Byte_Packet_Counter_rc                                     0x00002B
#define cAf6Reg_Ethernet_Receive_1025to1528Byte_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Receive_1025to1528Byte_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_rc_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_rc_PacketNum_Shift                                       0
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_1025to1528Byte_Packet_Counter_rc_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 1025to1528Byte Packet Counter
Reg Addr   : 0x00003A(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 1025   to 1528 bytes to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro_Base                                0x00003A
#define cAf6Reg_Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro                                    0x00003A
#define cAf6Reg_Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro_WidthVal                                      32
#define cAf6Reg_Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit 1025to1528Byte Packet Counter
Reg Addr   : 0x00003B(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length from 1025   to 1528 bytes to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc_Base                                0x00003B
#define cAf6Reg_Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc                                    0x00003B
#define cAf6Reg_Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc_WidthVal                                      32
#define cAf6Reg_Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc_WriteMask                                     0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc_PacketNum_Mask                                cBit31_0
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Jumbo Packet Counter
Reg Addr   : 0x00002C(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length Jumbo from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_Jumbo_Packet_Counter_ro_Base                                         0x00002C
#define cAf6Reg_Ethernet_Receive_Jumbo_Packet_Counter_ro                                              0x00002C
#define cAf6Reg_Ethernet_Receive_Jumbo_Packet_Counter_ro_WidthVal                                           32
#define cAf6Reg_Ethernet_Receive_Jumbo_Packet_Counter_ro_WriteMask                                         0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_ro_PacketNum_Mask                                  cBit31_0
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_ro_PacketNum_Shift                                        0
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Receive Jumbo Packet Counter
Reg Addr   : 0x00002D(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  received packets length Jumbo from Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Receive_Jumbo_Packet_Counter_rc_Base                                         0x00002D
#define cAf6Reg_Ethernet_Receive_Jumbo_Packet_Counter_rc                                              0x00002D
#define cAf6Reg_Ethernet_Receive_Jumbo_Packet_Counter_rc_WidthVal                                           32
#define cAf6Reg_Ethernet_Receive_Jumbo_Packet_Counter_rc_WriteMask                                         0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_rc_PacketNum_Mask                                  cBit31_0
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_rc_PacketNum_Shift                                        0
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Receive_Jumbo_Packet_Counter_rc_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit Jumbo Packet Counter
Reg Addr   : 0x00003C(RO)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length jumbo to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_Jumbo_Packet_Counter_ro_Base                                        0x00003C
#define cAf6Reg_Ethernet_Transmit_Jumbo_Packet_Counter_ro                                             0x00003C
#define cAf6Reg_Ethernet_Transmit_Jumbo_Packet_Counter_ro_WidthVal                                          32
#define cAf6Reg_Ethernet_Transmit_Jumbo_Packet_Counter_ro_WriteMask                                        0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_ro_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_ro_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_ro_PacketNum_Mask                                 cBit31_0
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_ro_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_ro_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_ro_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_ro_PacketNum_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Transmit Jumbo Packet Counter
Reg Addr   : 0x00003D(RC)
Reg Formula: 
    Where  : 
Reg Desc   : 
Count total number of  transmitted packets length jumbo to Ethernet port

------------------------------------------------------------------------------*/
#define cAf6Reg_Ethernet_Transmit_Jumbo_Packet_Counter_rc_Base                                        0x00003D
#define cAf6Reg_Ethernet_Transmit_Jumbo_Packet_Counter_rc                                             0x00003D
#define cAf6Reg_Ethernet_Transmit_Jumbo_Packet_Counter_rc_WidthVal                                          32
#define cAf6Reg_Ethernet_Transmit_Jumbo_Packet_Counter_rc_WriteMask                                        0x0

/*--------------------------------------
BitField Name: PacketNum
BitField Type: RW
BitField Desc: Counter value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_rc_PacketNum_Bit_Start                                       0
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_rc_PacketNum_Bit_End                                      31
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_rc_PacketNum_Mask                                 cBit31_0
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_rc_PacketNum_Shift                                       0
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_rc_PacketNum_MaxVal                              0xffffffff
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_rc_PacketNum_MinVal                                     0x0
#define cAf6_Ethernet_Transmit_Jumbo_Packet_Counter_rc_PacketNum_RstVal                                     0x0

#endif /* _AF6_REG_AF6CCI0021_RD_PMC_H_ */

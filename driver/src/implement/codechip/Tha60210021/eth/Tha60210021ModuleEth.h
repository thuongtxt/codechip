/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210021ModuleEth.h
 * 
 * Created Date: Aug 30, 2015
 *
 * Description : ETH Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021MODULEETH_H_
#define _THA60210021MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60210021ModuleEthQSgmiiSupported(AtModuleEth self);
void Tha60210021EthErrorStickyDebug(AtModule self);
void Tha60210021EthCounterStickyDebug(AtModule self);
void Tha60210021QSGMIIorSGMIIErrorStickyDebug(AtModule self);
void Tha60210021XfiErrorStickyDebug(AtModule self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021MODULEETH_H_ */


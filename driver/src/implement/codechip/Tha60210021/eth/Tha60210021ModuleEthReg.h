/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use copying transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : Tha60210021ModuleEthReg.h
 * 
 * Created Date: Sep 25 2015
 *
 * Description : 60210021 module ethernet registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021MODULEETHREG_H_
#define _THA60210021MODULEETHREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#define cEthCounterSticky  0x500008
#define cEthCntrxqunderrun cBit14
#define cEthPhyerr         cBit13
#define cEthCntetherr      cBit12
#define cEthCntpsnerr      cBit11
#define cEthCntudperr      cBit10
#define cEthCntrxlate      cBit9
#define cEthCntrxearly     cBit8
#define cEthCntrxqlofs     cBit7
#define cEthCntrxqoverrun  cBit6
#define cEthCntrxqlost     cBit5
#define cEthMalform        cBit4
#define cEthCnttxlbit      cBit3
#define cEthCntrxqlbit     cBit2
#define cEthCnttxrbit      cBit1
#define cEthCntrxrbit      cBit0

#define cEthErrorSticky    0x50000C
#define cEthJitfullcnt     cBit17
#define cEthReorfullcnt    cBit16
#define cEthErrssrc        cBit15
#define cEthErrcw          cBit14
#define cEthErrpwid        cBit13
#define cEthErripv6add     cBit12
#define cEthErripv6ver     cBit11
#define cEthErripv4sum     cBit10
#define cEthErripv4add     cBit9
#define cEthErripv4ver     cBit8
#define cEthUndrsize       cBit7
#define cEthOversize       cBit6
#define cEthErrtyp         cBit5
#define cEthErrmac         cBit4
#define cEthErrphy         cBit3
#define cEthErreop         cBit2
#define cEthClainit        cBit1
#define cEthClafull        cBit0

#define cQSGMIIorSGMIIMac  0x401048
#define cTxfm_irer         cBit3
#define cTxfmmoderr        cBit2
#define cTxfmsoperr        cBit1
#define cTxfmeoperr        cBit0

#define cXfiMac            0x3c100a
#define cRxif_sfdmis1      cBit8
#define cRxif_outsel1      cBit7
#define cRxif_over1        cBit6
#define cRxif_under1       cBit5
#define cRxff_rderr1       cBit4
#define cTxif_gmerr1       cBit3
#define cTxff_wrerr1       cBit2
#define cTxsh_err1         cBit1_0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021MODULEETHREG_H_ */


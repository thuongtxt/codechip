/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210021EthPort.c
 *
 * Created Date: May 20, 2015
 *
 * Description : ETH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/eth/ThaEthPortInternal.h"
#include "../../../default/eth/ThaEthPort.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../../../default/eth/ThaEthPortReg.h"
#include "../../../default/man/ThaDevice.h"
#include "../../default/ThaPdhPwProduct/eth/ThaPdhPwProductEthPortInternal.h"
#include "../../Tha60210011/eth/Tha6021ModuleEth.h"
#include "../../Tha60210031/eth/Tha60210031EthPortErrorGenerator.h"
#include "../../Tha60210031/eth/Tha60210031EthPort.h"
#include "Tha60210021ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
/*
 * Mac Address Bit47_32 Control
 * Field: [26]  %%  GeMode %% GE mode 0:XGMII; 1:SGMII   %% RW %% 0x0 %% 0x0
 */
#define cGlbRegGeModeMask  cBit26
#define cGlbRegGeModeShift 26

/*--------------------------- Macros -----------------------------------------*/
#define mPortId(self) (uint8)AtChannelIdGet((AtChannel)self)
#define mThaEthPort(self) ((ThaEthPort)self)
#define mDefaultOffset(self) (ThaEthPortDefaultOffset(mThaEthPort(self)) + ThaEthPortMacBaseAddress(mThaEthPort(self)))
#define mXgmiiDefaultOffset(self) (ThaEthPortDefaultOffset(mThaEthPort(self)) + 0x3C1000UL)
#define mThis(self) ((tTha60210021EthPort *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021EthPort
    {
    tThaPdhPwProductEthPort super;

    /* Private data */
    uint8 interface;
    AtErrorGenerator errorGenerator;
    }tTha60210021EthPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthPortMethods     m_ThaEthPortOverride;
static tAtEthPortMethods      m_AtEthPortOverride;
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;

/* Save super implementation */
static const tThaEthPortMethods *m_ThaEthPortMethods = NULL;
static const tAtEthPortMethods  *m_AtEthPortMethods  = NULL;
static const tAtObjectMethods   *m_AtObjectMethods   = NULL;
static const tAtChannelMethods  *m_AtChannelMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    return Tha6021DsxEthPortRxSerdesSelect(self, serdes);
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    return Tha6021DsxEthPortRxSelectedSerdes(self);
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    return Tha6021DsxEthPortTxSerdesBridge(self, serdes);
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    return Tha6021DsxEthPortTxBridgedSerdes(self);
    }

static eBool TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    return Tha6021DsxEthPortTxSerdesCanBridge(self, serdes);
    }

static eAtRet LoopInEnable(ThaEthPort self, eBool enable)
    {
    uint32 regVal, regAddr;

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mDefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripSmacLoopinMask, cThaIpEthTripSmacLoopinShift, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mXgmiiDefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripSmacLoopinMask, cThaIpEthTripSmacLoopinShift, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet LoopOutEnable(ThaEthPort self, eBool enable)
    {
    uint32 regVal, regAddr;

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mDefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripSmacLoopoutMask, cThaIpEthTripSmacLoopoutShift, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mXgmiiDefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripSmacLoopoutMask, cThaIpEthTripSmacLoopoutShift, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool UseQSgmii(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    return Tha60210021ModuleEthQSgmiiSupported(ethModule);
    }

static eBool InterfaceIsSupported(AtEthPort self, eAtEthPortInterface interface)
    {
    if (interface == cAtEthPortInterfaceXGMii)
        return cAtTrue;

    if (UseQSgmii(self))
        return (interface == cAtEthPortInterfaceQSgmii) ? cAtTrue : cAtFalse;

    return (interface == cAtEthPortInterfaceSgmii) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    uint32 regVal;
    uint32 regAddr;
    uint8 geMode = (interface == cAtEthPortInterfaceXGMii) ? 0 : 1;

    if (ThaDeviceIsEp((ThaDevice)AtChannelDeviceGet((AtChannel)self)))
        return cAtOk;

    if (!InterfaceIsSupported(self, interface))
        return cAtErrorModeNotSupport;

    regAddr = cThaRegEthMacAddr47_32;
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cGlbRegGeModeMask, cGlbRegGeModeShift, geMode);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    mThis(self)->interface = interface;

    if (ThaModuleEthCanEnableTxSerdes((ThaModuleEth)AtChannelModuleGet((AtChannel)self)))
        return AtSerdesControllerEnable(AtEthPortSerdesController((AtEthPort)self), AtChannelIsEnabled((AtChannel)self));
    return cAtOk;
    }

static eAtEthPortInterface CachedInterface(AtEthPort self)
    {
    if (mThis(self)->interface == 0)
        mThis(self)->interface = mMethodsGet(self)->DefaultInterface(self);
    return mThis(self)->interface;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    uint32 regVal;

    if (AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet((AtChannel)self)))
        return CachedInterface(self);

    regVal = mChannelHwRead(self, cThaRegEthMacAddr47_32, cAtModuleEth);
    if (regVal & cGlbRegGeModeMask)
        return UseQSgmii(self) ? cAtEthPortInterfaceQSgmii : cAtEthPortInterfaceSgmii;

    return cAtEthPortInterfaceXGMii;
    }

static eBool PllIsLocked(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet PcsReset(ThaEthPort self)
    {
    uint32 offset = mDefaultOffset(self);
    uint32 regAddr, regVal;

    /* Speed Global Interface Control */
    regAddr = cThaRegIPEthernetTripleSpdGlbIntfCtrl + offset;
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cThaIpEthTripGlbCfgTbi, cThaIpEthTripGlbCfgTbiRstVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    /* Speed PCS Advertisement */
    regAddr = cThaRegIPEthernetTripleSpdPcsAdvertisementWith1000BASEXMd + offset;
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripPcsAnAdvPauMask, cThaIpEthTripPcsAnAdvPauShift, cThaIpEthTripPcsAnAdvPauRstVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    /* Speed PCS An Next Page Transmitter */
    regAddr = cThaRegIPEthernetTripleSpdPcsAnNextPageTxter + offset;
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cThaIpEthTripPcsAnNpTxNextPage, cThaIpEthTripPcsAnNpTxNextPageRstVal);
    mRegFieldSet(regVal, cThaIpEthTripPcsAnNpTxMsgPage, cThaIpEthTripPcsAnNpTxMsgPageRstVal);
    mRegFieldSet(regVal, cThaIpEthTripPcsAnNpTxAck, cThaIpEthTripPcsAnNpTxAckRstVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    /* Speed PCS Option Control */
    regAddr = cThaRegIPEthernetTripleSpdPcsOptCtrl + offset;
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cThaIpEthTripPcsCtlFifoAdjC2, cThaIpEthTripPcsCtlFifoAdjC2RstVal);
    mRegFieldSet(regVal, cThaIpEthTripPcsCtlFifoDepth, cThaIpEthTripPcsCtlFifoDepthRstVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    /* Speed PCS Option Link Timer */
    regAddr = cThaRegIPEthernetTripleSpdPcsOptLinkTimer + offset;
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cThaIpEthTripPcsCfgLinkTimerSgmii, cThaIpEthTripPcsCfgLinkTimerSgmiiRstVal);
    mRegFieldSet(regVal, cThaIpEthTripPcsCfgLinkTimerBasex, cThaIpEthTripPcsCfgLinkTimerBasexRstVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    /* Speed PCS Option Sync Timer */
    regAddr = cThaRegIPEthernetTripleSpdPcsOptSyncTimer + offset;
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cThaIpEthTripPcsCfgSyncTimer, cThaIpEthTripPcsCfgSyncTimerRstVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    /* Speed PCS Option PRBS Test Control */
    regAddr = cThaRegIPEthernetTripleSpdPcsOptPrpsTestCtrl + offset;
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cThaIPEthTripPcsTestFixDat, cThaIPEthTripPcsTestFixDatRstVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    /* Speed Simple MAC Active Control */
    ThaEthPortMacActivate(mThaEthPort(self), cAtTrue);

    /* Speed Simple MAC Receiver Control */
    regAddr = cThaRegIPEthernetTripleSpdSimpleMACRxrCtrl + offset;
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cThaIpEthTripSmacRxMask, cThaIpEthTripSmacRxMaskRstVal);
    mRegFieldSet(regVal, cThaIpEthTripSmacRxPauSaChk, cThaIpEthTripSmacRxPauSaChkRstVal);
    mRegFieldSet(regVal, cThaIpEthTripSamcRxPauDaChk, cThaIpEthTripSamcRxPauDaChkRstVal);
    mRegFieldSet(regVal, cThaIpEthTripSmacRxPauDiChk, cThaIpEthTripSmacRxPauDiChkRstVal);
    mRegFieldSet(regVal, cThaIpEthTripSmacRxFcsPass, mMethodsGet(self)->FcsByPassed(self) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    /* Speed Simple MAC Data Inter Frame */
    regAddr = cThaRegIPEthernetTripleSpdSimpleMACDataInterFrm + offset;
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cThaIpEthTripSmacIdleTxData, cThaIpEthTripSmacIdleTxDataRstVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet HwAutoControlPhysicalEnable(ThaEthPort self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);

    return cAtOk;
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    ThaEthSerdesManager manager = ThaModuleEthSerdesManager(ethModule);
    return ThaEthSerdesManagerSerdesController(manager, AtChannelIdGet((AtChannel)self));
    }

static eAtRet XgmiiMacActivate(ThaEthPort self, eBool activate)
    {
    uint32 regAddr, regVal;
    uint8 hwActivate;
    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mXgmiiDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    hwActivate = activate ? 1 : 0;
    mFieldIns(&regVal, cThaIpEthTripSmacTxActMask, cThaIpEthTripSmacTxActShift, hwActivate);
    mFieldIns(&regVal, cThaIpEthTripSmacRxActMask, cThaIpEthTripSmacRxActShift, hwActivate);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtRet MacActivate(ThaEthPort self, eBool activate)
    {
    eAtRet ret = m_ThaEthPortMethods->MacActivate(self, activate);
    if (ret == cAtOk)
        ret = XgmiiMacActivate(self, activate);
    return ret;
    }

static uint32 MaxBandwidthInKbps(ThaEthPort self)
    {
    AtUnused(self);
    return 900000; /* 900M */
    }

static uint32 RemainingBpsOfMaxBandwidth(ThaEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210021EthPort * object = (tTha60210021EthPort *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(interface);
    mEncodeObject(errorGenerator);
    }

static AtErrorGenerator ErrorGeneratorGet(AtEthPort self, eAtSide side)
    {
    if (!ThaDeviceErrorGeneratorIsSupported((ThaDevice)AtChannelDeviceGet((AtChannel)self)))
        return NULL;

    if (mThis(self)->errorGenerator == NULL)
        mThis(self)->errorGenerator = Tha60210031EthPortErrorGeneratorNew((AtChannel)self);

    if (AtErrorGeneratorIsStarted(mThis(self)->errorGenerator))
        {
        if (Tha60210031EthPortErrorGeneratorSideGet(mThis(self)->errorGenerator) != side)
            return NULL;
        }

    Tha60210031EthPortErrorGeneratorSideSet(mThis(self)->errorGenerator, side);
    return mThis(self)->errorGenerator;
    }

static AtErrorGenerator TxErrorGeneratorGet(AtEthPort self)
    {
    return ErrorGeneratorGet(self, cAtSideTx);
    }

static AtErrorGenerator RxErrorGeneratorGet(AtEthPort self)
    {
    return ErrorGeneratorGet(self, cAtSideRx);
    }

static void ErrorGeneratorDelete(AtChannel self)
    {
    AtObjectDelete((AtObject)mThis(self)->errorGenerator);
    mThis(self)->errorGenerator = NULL;
    }

static void ErrorGeneratorDefaultInit(AtChannel self)
    {
    Tha60210031EthPortErrorGeneratorHwDefaultSet(self);
    ErrorGeneratorDelete(self);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (!ThaDeviceErrorGeneratorIsSupported((ThaDevice)AtChannelDeviceGet((AtChannel)self)))
        ErrorGeneratorDefaultInit(self);

    return ret;
    }

static void Delete(AtObject self)
    {
    ErrorGeneratorDelete((AtChannel)self);

    /* Fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, RxSerdesSelect);
        mMethodOverride(m_AtEthPortOverride, RxSelectedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesBridge);
        mMethodOverride(m_AtEthPortOverride, TxBridgedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesCanBridge);
        mMethodOverride(m_AtEthPortOverride, TxErrorGeneratorGet);
        mMethodOverride(m_AtEthPortOverride, RxErrorGeneratorGet);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort port = (ThaEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPortMethods = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, m_ThaEthPortMethods, sizeof(m_ThaEthPortOverride));

        mMethodOverride(m_ThaEthPortOverride, PllIsLocked);
        mMethodOverride(m_ThaEthPortOverride, PcsReset);
        mMethodOverride(m_ThaEthPortOverride, HwAutoControlPhysicalEnable);
        mMethodOverride(m_ThaEthPortOverride, LoopInEnable);
        mMethodOverride(m_ThaEthPortOverride, LoopOutEnable);
        mMethodOverride(m_ThaEthPortOverride, MacActivate);
        mMethodOverride(m_ThaEthPortOverride, MaxBandwidthInKbps);
        mMethodOverride(m_ThaEthPortOverride, RemainingBpsOfMaxBandwidth);
        }

    mMethodsSet(port, &m_ThaEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021EthPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductEthPortObjectInit((AtEthPort)self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60210021EthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

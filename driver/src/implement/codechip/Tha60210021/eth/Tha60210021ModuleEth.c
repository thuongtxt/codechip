/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210021ModuleEth.c
 *
 * Created Date: May 20, 2015
 *
 * Description : ETH Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../default/ThaPdhPwProduct/eth/ThaPdhPwProductModuleEth.h"
#include "../../../default/eth/ThaEthPort.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "../../Tha60210011/prbs/Tha6021EthPortSerdesPrbsEngine.h"
#include "../../Tha60210011/eth/Tha6021ModuleEth.h"
#include "../man/Tha60210021DeviceReg.h"
#include "../man/Tha6021DebugPrint.h"
#include "Tha60210021ModuleEthReg.h"
#include "Tha60210021ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_linkalm_Eport_XFI_Mask(xfi)            (cBit16 << (xfi))
#define cAf6_linkalm_Eport_QSGMII_Mask(id)          (cBit0 << ((id) << 2))

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210021ModuleEth *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021ModuleEth
    {
    tThaPdhPwProductModuleEth super;

    /* Private data */
    AtDrp qsgmiiDrp;
    }tTha60210021ModuleEth;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtModuleMethods     m_AtModuleOverride;
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tThaModuleEthMethods *m_ThaModuleEthMethods = NULL;
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tAtModuleEthMethods  *m_AtModuleEthMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    return Tha60210021EthPortNew(portId, self);
    }

static eBool PortSerdesPrbsIsSupported(ThaModuleEth self, AtEthPort ethPort)
    {
    AtUnused(self);
    AtUnused(ethPort);
    return cAtTrue;
    }

static uint32 DiagBaseAddress(ThaModuleEth self, uint32 serdesId)
    {
    uint32 interface = Tha6021ModuleEthSerdesInterface(self, serdesId);
    if (interface == cAtEthPortInterfaceXGMii)
        return Tha6021ModuleEthDsCardXfiDiagBaseAddress(self, serdesId);
    return Tha6021ModuleEthSgmiiDiagBaseAddress(self, serdesId);
    }

static AtPrbsEngine PortSerdesPrbsEngineCreate(ThaModuleEth self, AtEthPort ethPort, uint32 serdesId)
    {
    AtUnused(ethPort);
    return Tha6021DsxXfiSerdesPrbsEngineNew(AtModuleEthSerdesController((AtModuleEth)self, serdesId));
    }

static eBool SerdesHasMdio(AtModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 SerdesMdioBaseAddress(AtModuleEth self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return 0xFB0000;
    }

static uint32 SerdesDrpBaseAddress(AtModuleEth self)
    {
    AtUnused(self);
    return 0xF00000;
    }

static eBool SerdesHasDrp(AtModuleEth self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAtTrue;
    }

static uint32 DrpPortOffset(AtDrp drp, uint32 selectedPortId)
    {
    AtUnused(drp);

    if ((selectedPortId == 0) || (selectedPortId == 1))
        return 0xC0000;
    if ((selectedPortId == 2) || (selectedPortId == 3))
        return 0xd0000;

    return cBit31_0;
    }

static tAtDrpAddressCalculator *SerdesXfiDrpAddressCalculator(void)
    {
    static tAtDrpAddressCalculator calculator;
    static tAtDrpAddressCalculator *pCalculator = NULL;

    if (pCalculator)
        return pCalculator;

    AtOsalMemInit(&calculator, 0, sizeof(tAtDrpAddressCalculator));
    calculator.PortOffset = DrpPortOffset;
    pCalculator = &calculator;

    return pCalculator;
    }

static AtDrp SerdesXfiDrpCreate(AtModuleEth self, uint32 serdesId)
    {
    AtDrp drp = m_AtModuleEthMethods->SerdesDrpCreate(self, serdesId);
    AtDrpAddressCalculatorSet(drp, SerdesXfiDrpAddressCalculator());
    return drp;
    }

static AtDrp SerdesQsgmiiDrpCreate(AtModuleEth self)
    {
    static const uint32 cQsgmiiDrpBaseAddress = 0xFE0000;
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtHal hal = AtDeviceIpCoreHalGet(device, AtModuleDefaultCoreGet((AtModule)self));
    AtDrp drp = AtDrpNew(cQsgmiiDrpBaseAddress, hal);

    AtDrpNumberOfSerdesControllersSet(drp, AtModuleEthNumSerdesControllers(self));
    AtDrpNeedSelectPortSet(drp, cAtFalse);
    return drp;
    }

static AtDrp SerdesDrpCreate(AtModuleEth self, uint32 serdesId)
    {
    AtEthPort port = (AtEthPort)AtSerdesControllerPhysicalPortGet(AtModuleEthSerdesController(self, serdesId));

    if (AtEthPortInterfaceGet(port) == cAtEthPortInterfaceQSgmii)
        return SerdesQsgmiiDrpCreate(self);

    return SerdesXfiDrpCreate(self, serdesId);
    }

static AtDrp SerdesDrp(AtModuleEth self, uint32 serdesId)
    {
    AtEthPort port = (AtEthPort)AtSerdesControllerPhysicalPortGet(AtModuleEthSerdesController(self, serdesId));

    if (AtEthPortInterfaceGet(port) == cAtEthPortInterfaceXGMii)
        return m_AtModuleEthMethods->SerdesDrp(self, serdesId);

    if (!mMethodsGet(self)->SerdesHasDrp(self, serdesId))
        return NULL;

    if (mThis(self)->qsgmiiDrp == NULL)
        mThis(self)->qsgmiiDrp = mMethodsGet(self)->SerdesDrpCreate(self, serdesId);

    return mThis(self)->qsgmiiDrp;
    }

static eBool HasSerdesControllers(AtModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
    AtUnused(self);
    return 1;
    }

static ThaEthSerdesManager SerdesManagerCreate(ThaModuleEth self)
    {
    return Tha60210021EthSerdesManagerNew((AtModuleEth)self);
    }

static ThaVersionReader VersionReader(AtModuleEth self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartVersionSupportQSgmiiAndSerdesAps(AtModuleEth self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x08, 0x29, 0x25);
    }

static eAtEthPortInterface PortDefaultInterface(AtModuleEth self, AtEthPort port)
    {
    AtUnused(port);
    return Tha60210021ModuleEthQSgmiiSupported(self) ? cAtEthPortInterfaceQSgmii : cAtEthPortInterfaceSgmii;
    }

static uint16 QsgmiiSerdesNumLanes(ThaModuleEth self, uint8 serdesId)
    {
    if (Tha60210021ModuleEthQSgmiiSupported((AtModuleEth)self))
        return m_ThaModuleEthMethods->QsgmiiSerdesNumLanes(self, serdesId);
    return 1;
    }

static uint32 StartVersionSupportsSerdesAps(ThaModuleEth self)
    {
    return StartVersionSupportQSgmiiAndSerdesAps((AtModuleEth)self);
    }

static eBool FullSerdesPrbsSupported(ThaModuleEth self)
    {
    return AtModuleEthSerdesApsSupported((AtModuleEth)self) ? cAtFalse : cAtTrue;
    }

static eBool CanEnableTxSerdes(ThaModuleEth self)
    {
    return AtModuleEthSerdesApsSupported((AtModuleEth)self) ? cAtTrue : cAtFalse;
    }

static eBool SerdesCauseInterrupt(ThaModuleEth self, uint32 glbIntr)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    return cAtTrue;
    }

static void SerdesInterruptProcess(ThaModuleEth self, uint32 ethIntr, AtHal hal)
    {
    uint32 serdesId;
    uint32 numSerdeses = AtModuleEthNumSerdesControllers((AtModuleEth)self);
    uint32 intrVal = AtHalRead(hal, cAf6Reg_linkalm_Eport);
    uint32 intrEn = AtHalRead(hal, cAf6Reg_inten_Eport);
    uint32 currVal = AtHalRead(hal, cAf6Reg_linksta_Eport);
    AtUnused(ethIntr);

    intrVal &= intrEn;
    for (serdesId = 0; serdesId < numSerdeses; serdesId++)
        {
        uint32 state = 0;
        uint32 defect = 0;

        if (intrVal & cAf6_linkalm_Eport_XFI_Mask(serdesId))
            {
            defect = cAtSerdesAlarmTypeLinkDown;
            if (~currVal & cAf6_linkalm_Eport_XFI_Mask(serdesId))
                state = cAtSerdesAlarmTypeLinkDown;
            }
        else if (intrVal & cAf6_linkalm_Eport_QSGMII_Mask(serdesId))
            {
            defect = cAtSerdesAlarmTypeLinkDown;
            if (~currVal & cAf6_linkalm_Eport_QSGMII_Mask(serdesId))
                state = cAtSerdesAlarmTypeLinkDown;
            }

        if (defect)
            AtSerdesControllerAlarmNotify(AtModuleEthSerdesController((AtModuleEth)self, serdesId), defect, state);
        }

    /* Clear interrupt. */
    AtHalWrite(hal, cAf6Reg_linkalm_Eport, intrVal);
    return ;
    }

static eBool InterruptIsSupported(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet AllPortsEnable(AtModuleEth self)
    {
    uint8 i;
    eAtRet ret = cAtOk;
    uint8 numPorts = mMethodsGet(self)->MaxPortsGet(self);

    for (i = 0; i < numPorts; i++)
        {
        AtEthPort port = AtModuleEthPortGet((AtModuleEth)self, i);
        ret |= AtChannelEnable((AtChannel)port, cAtTrue);
        }

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;
        
    return AllPortsEnable((AtModuleEth)self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    AtDrpDelete(mThis(self)->qsgmiiDrp);
    mThis(self)->qsgmiiDrp = NULL;

    /* Super deletion */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210021ModuleEth* object = (tTha60210021ModuleEth*)self;

    m_AtObjectMethods->Serialize(self, encoder);

    AtCoderEncodeString(encoder, AtDrpToString(object->qsgmiiDrp), "qsgmiiDrp");
    }

static eBool QsgmiiSerdesLaneIsUsed(ThaModuleEth self, uint8 serdesId, uint16 laneId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return (laneId == 0) ? cAtTrue : cAtFalse;
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtObject(AtModuleEth self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        mMethodOverride(m_AtModuleEthOverride, HasSerdesControllers);
        mMethodOverride(m_AtModuleEthOverride, SerdesHasMdio);
        mMethodOverride(m_AtModuleEthOverride, SerdesMdioBaseAddress);
        mMethodOverride(m_AtModuleEthOverride, SerdesDrpBaseAddress);
        mMethodOverride(m_AtModuleEthOverride, SerdesHasDrp);
        mMethodOverride(m_AtModuleEthOverride, SerdesDrpCreate);
        mMethodOverride(m_AtModuleEthOverride, SerdesDrp);
        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, PortDefaultInterface);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleEthMethods = mMethodsGet(ethModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, m_ThaModuleEthMethods, sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, PortSerdesPrbsIsSupported);
        mMethodOverride(m_ThaModuleEthOverride, DiagBaseAddress);
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesPrbsEngineCreate);
        mMethodOverride(m_ThaModuleEthOverride, SerdesManagerCreate);
        mMethodOverride(m_ThaModuleEthOverride, QsgmiiSerdesNumLanes);
        mMethodOverride(m_ThaModuleEthOverride, StartVersionSupportsSerdesAps);
        mMethodOverride(m_ThaModuleEthOverride, FullSerdesPrbsSupported);
        mMethodOverride(m_ThaModuleEthOverride, CanEnableTxSerdes);
        mMethodOverride(m_ThaModuleEthOverride, SerdesCauseInterrupt);
        mMethodOverride(m_ThaModuleEthOverride, SerdesInterruptProcess);
        mMethodOverride(m_ThaModuleEthOverride, QsgmiiSerdesLaneIsUsed);
        mMethodOverride(m_ThaModuleEthOverride, InterruptIsSupported);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtObject(self);
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModuleEth);
    }

static AtModuleEth ObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60210021ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

uint32 Tha6021ModuleEthDsCardXfiDiagBaseAddress(ThaModuleEth self, uint32 serdesId)
    {
    if (AtModuleEthSerdesApsSupported((AtModuleEth)self))
        return 0x3C1000;

    /* And older versions, although diagnostic is supported for all ports. But
     * they do not support APS */
    switch (serdesId)
        {
        case 0: return 0xF20000;
        case 1: return 0xF80000;
        case 2: return 0xF90000;
        case 3: return 0xFA0000;
        default:
            return cBit31_0;
        }
    }

uint32 Tha6021ModuleEthQSgmiiDiagBaseAddress(ThaModuleEth self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return 0xFC0000;
    }

uint32 Tha6021ModuleEthSgmiiDiagBaseAddress(ThaModuleEth self, uint32 serdesId)
    {
    /* Only one SGMII diagnostic engine is supported if SERDES APS is supported */
    if (AtModuleEthSerdesApsSupported((AtModuleEth)self))
        return 0xF71000;

    /* And for older versions, diagnostic is supported for all ports but APS is
     * not supported */
    switch (serdesId)
        {
        case 0: return 0xF71000;
        case 1: return 0xF72000;
        case 2: return 0xF73000;
        case 3: return 0xF74000;
        default:
            return cBit31_0;
        }
    }

eAtEthPortInterface Tha6021ModuleEthSerdesInterface(ThaModuleEth self, uint32 serdesId)
    {
    uint32 portId = serdesId % AtModuleEthMaxPortsGet((AtModuleEth)self);
    AtEthPort port = AtModuleEthPortGet((AtModuleEth)self, (uint8)portId);
    return AtEthPortInterfaceGet(port);
    }

eBool Tha60210021ModuleEthQSgmiiSupported(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;
    return (AtDeviceVersionNumber(device) >= StartVersionSupportQSgmiiAndSerdesAps(self)) ? cAtTrue : cAtFalse;
    }

void Tha60210021EthCounterStickyDebug(AtModule self)
    {
    uint32 regAddres, regValue;
    AtDebugger debugger = NULL;

    regAddres = cEthCounterSticky;
    regValue  = mModuleHwRead(self, regAddres);

    Tha6021DebugPrintRegName (debugger, "* Ethernet counters sticky", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, " cntrxqunderrun", regValue, cEthCntrxqunderrun);
    Tha6021DebugPrintErrorBit(debugger, " phy_err"       , regValue, cEthPhyerr        );
    Tha6021DebugPrintErrorBit(debugger, " cntetherr"     , regValue, cEthCntetherr     );
    Tha6021DebugPrintErrorBit(debugger, " cntpsnerr"     , regValue, cEthCntpsnerr     );
    Tha6021DebugPrintErrorBit(debugger, " cntudperr"     , regValue, cEthCntudperr     );
    Tha6021DebugPrintErrorBit(debugger, " cntrxlate"     , regValue, cEthCntrxlate     );
    Tha6021DebugPrintErrorBit(debugger, " cntrxearly"    , regValue, cEthCntrxearly    );
    Tha6021DebugPrintErrorBit(debugger, " cntrxqlofs"    , regValue, cEthCntrxqlofs    );
    Tha6021DebugPrintErrorBit(debugger, " cntrxqoverrun" , regValue, cEthCntrxqoverrun );
    Tha6021DebugPrintErrorBit(debugger, " cntrxqlost"    , regValue, cEthCntrxqlost    );
    Tha6021DebugPrintErrorBit(debugger, " malform"       , regValue, cEthMalform       );
    Tha6021DebugPrintErrorBit(debugger, " cnttxlbit"     , regValue, cEthCnttxlbit     );
    Tha6021DebugPrintErrorBit(debugger, " cntrxqlbit"    , regValue, cEthCntrxqlbit    );
    Tha6021DebugPrintErrorBit(debugger, " cnttxrbit"     , regValue, cEthCnttxrbit     );
    Tha6021DebugPrintErrorBit(debugger, " cntrxrbit"     , regValue, cEthCntrxrbit     );
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    mModuleHwWrite(self, regAddres, regValue);
    }

void Tha60210021EthErrorStickyDebug(AtModule self)
    {
    uint32 regAddres, regValue;
    AtDebugger debugger = NULL;

    regAddres = cEthErrorSticky;
    regValue  = mModuleHwRead(self, regAddres);

    Tha6021DebugPrintRegName (debugger, "* Ethernet error sticky", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, " jitfullcnt" , regValue, cEthJitfullcnt );
    Tha6021DebugPrintErrorBit(debugger, " reorfullcnt", regValue, cEthReorfullcnt);
    Tha6021DebugPrintErrorBit(debugger, " errssrc"    , regValue, cEthErrssrc    );
    Tha6021DebugPrintErrorBit(debugger, " errcw"      , regValue, cEthErrcw      );
    Tha6021DebugPrintErrorBit(debugger, " errpwid"    , regValue, cEthErrpwid    );
    Tha6021DebugPrintErrorBit(debugger, " erripv6add" , regValue, cEthErripv6add );
    Tha6021DebugPrintErrorBit(debugger, " erripv6ver" , regValue, cEthErripv6ver );
    Tha6021DebugPrintErrorBit(debugger, " erripv4sum" , regValue, cEthErripv4sum );
    Tha6021DebugPrintErrorBit(debugger, " erripv4add" , regValue, cEthErripv4add );
    Tha6021DebugPrintErrorBit(debugger, " erripv4ver" , regValue, cEthErripv4ver );
    Tha6021DebugPrintErrorBit(debugger, " undrsize"   , regValue, cEthUndrsize   );
    Tha6021DebugPrintErrorBit(debugger, " oversize"   , regValue, cEthOversize   );
    Tha6021DebugPrintErrorBit(debugger, " errtyp"     , regValue, cEthErrtyp     );
    Tha6021DebugPrintErrorBit(debugger, " errmac"     , regValue, cEthErrmac     );
    Tha6021DebugPrintErrorBit(debugger, " errphy"     , regValue, cEthErrphy     );
    Tha6021DebugPrintErrorBit(debugger, " erreop"     , regValue, cEthErreop     );
    Tha6021DebugPrintErrorBit(debugger, " clainit"    , regValue, cEthClainit    );
    Tha6021DebugPrintErrorBit(debugger, " clafull"    , regValue, cEthClafull    );
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    mModuleHwWrite(self, regAddres, regValue);
    }

void Tha60210021QSGMIIorSGMIIErrorStickyDebug(AtModule self)
    {
    uint32 regAddres, regValue;
    AtDebugger debugger = NULL;

    regAddres = cQSGMIIorSGMIIMac;
    regValue  = mModuleHwRead(self, regAddres);

    Tha6021DebugPrintRegName (debugger, "* SGMII/SGMII error sticky", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, " txfm_irer" , regValue, cTxfm_irer );
    Tha6021DebugPrintErrorBit(debugger, " txfmmoderr", regValue, cTxfmmoderr);
    Tha6021DebugPrintErrorBit(debugger, " txfmsoperr", regValue, cTxfmsoperr);
    Tha6021DebugPrintErrorBit(debugger, " txfmeoperr", regValue, cTxfmeoperr);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    mModuleHwWrite(self, regAddres, regValue);
    }

void Tha60210021XfiErrorStickyDebug(AtModule self)
    {
    uint32 regAddres, regValue;
    AtDebugger debugger = NULL;

    regAddres = cXfiMac;
    regValue  = mModuleHwRead(self, regAddres);

    Tha6021DebugPrintRegName (debugger, "* XFI error sticky", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, " i_rxif_sfdmis1", regValue, cRxif_sfdmis1);
    Tha6021DebugPrintErrorBit(debugger, " i_rxif_outsel1", regValue, cRxif_outsel1);
    Tha6021DebugPrintErrorBit(debugger, " i_rxif_over1"  , regValue, cRxif_over1  );
    Tha6021DebugPrintErrorBit(debugger, " i_rxif_under1" , regValue, cRxif_under1 );
    Tha6021DebugPrintErrorBit(debugger, " i_rxff_rderr1" , regValue, cRxff_rderr1 );
    Tha6021DebugPrintErrorBit(debugger, " i_txif_gmerr1" , regValue, cTxif_gmerr1 );
    Tha6021DebugPrintErrorBit(debugger, " i_txff_wrerr1" , regValue, cTxff_wrerr1 );
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    mModuleHwWrite(self, regAddres, regValue);
    }


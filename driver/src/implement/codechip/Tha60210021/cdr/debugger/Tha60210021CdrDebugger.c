/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210021CdrDebugger.c
 *
 * Created Date: Sep 10, 2015
 *
 * Description : CDR debugger of 60210021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60150011/cdr/debugger/Tha60150011CdrDebuggerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cBasicNcoValE1   3619790848UL
#define cBasicNcoValDs1  2728982944UL
#define c1000NcoValPpbOffsetE1    276
#define c1000NcoValPpbOffsetDS1   366

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021CdrDebugger
    {
    tTha60150011CdrDebugger super;
    }tTha60210021CdrDebugger;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrDebuggerMethods m_ThaCdrDebuggerOverride;

/* Save super implementation */
static const tThaCdrDebuggerMethods *m_ThaCdrDebuggerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TimingBaseAddress(ThaCdrDebugger self, ThaCdrController controller)
    {
    AtUnused(self);
    AtUnused(controller);

    return 0x281000;
    }

static uint32 PwHeaderBaseAddress(ThaCdrDebugger self, ThaCdrController controller)
    {
    AtUnused(self);
    AtUnused(controller);

    return 0x281100;
    }

static int32 PpbCalculate(ThaCdrDebugger self, uint32 ncoValue, uint32 baseNco, uint32 _1000NcoOffsetPpb)
    {
    uint32 unsignOffset = (ncoValue > baseNco) ? (ncoValue - baseNco) : (baseNco - ncoValue);
    uint32 absPpm = (unsignOffset * _1000NcoOffsetPpb) / 1000;

    AtUnused(self);

    return (ncoValue > baseNco) ? (int32)absPpm : (int32)(0 - absPpm);
    }

static int32 E1PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpbCalculate(self, ncoValue, cBasicNcoValE1, c1000NcoValPpbOffsetE1);
    }

static int32 Ds1PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpbCalculate(self, ncoValue, cBasicNcoValDs1, c1000NcoValPpbOffsetDS1);
    }

static uint32 HoldOverPerChannelAddress(ThaCdrDebugger self, ThaCdrController controller)
    {
    AtUnused(self);
    AtUnused(controller);
    return 0x280a00 + ThaCdrControllerDefaultOffset(controller);
    }

static eBool  HoldOverPerChannelIsSupported(ThaCdrDebugger self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaCdrDebugger(ThaCdrDebugger self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrDebuggerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrDebuggerOverride, m_ThaCdrDebuggerMethods, sizeof(m_ThaCdrDebuggerOverride));

        mMethodOverride(m_ThaCdrDebuggerOverride, TimingBaseAddress);
        mMethodOverride(m_ThaCdrDebuggerOverride, PwHeaderBaseAddress);
        mMethodOverride(m_ThaCdrDebuggerOverride, E1PpbCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, Ds1PpbCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, HoldOverPerChannelIsSupported);
        mMethodOverride(m_ThaCdrDebuggerOverride, HoldOverPerChannelAddress);
        }

    mMethodsSet(self, &m_ThaCdrDebuggerOverride);
    }

static void Override(ThaCdrDebugger self)
    {
    OverrideThaCdrDebugger(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021CdrDebugger);
    }

static ThaCdrDebugger ObjectInit(ThaCdrDebugger self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60150011CdrDebuggerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrDebugger Tha60210021CdrDebuggerNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrDebugger newDebugger = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDebugger == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDebugger);
    }

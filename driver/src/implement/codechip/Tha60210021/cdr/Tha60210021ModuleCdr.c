/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210021ModuleCdr.c
 *
 * Created Date: May 14, 2015
 *
 * Description : CDR module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cdr/ThaModuleCdrIntrReg.h"
#include "../../default/ThaPdhPwProduct/cdr/ThaPdhPwProductModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_lodcr_prc_freq_cfg_DCRPrcFrequency_Mask             cBit17_0
#define cAf6_lodcr_prc_freq_cfg_DCRPrcFrequency_Shift            0
#define cAf6_ThaDcrRtpFreqCfgMask                                cBit17_0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021ModuleCdr
    {
    tThaPdhPwProductModuleCdr super;
    }tTha60210021ModuleCdr;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleCdrMethods m_ThaModuleCdrOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultExtFrequency(ThaModuleCdr self)
    {
    AtUnused(self);
    return 0x30D4;
    }

static eBool NeedToSetExtDefaultFrequency(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaCdrController De1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1)
    {
    AtChannel channel = (AtChannel)de1;
    AtUnused(self);
    return Tha60210021De1CdrControllerNew(AtChannelIdGet(channel), channel);
    }

static eBool NeedActivateRxEngine(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaCdrDebugger DebuggerObjectCreate(ThaModuleCdr self)
    {
    AtUnused(self);
    return Tha60210021CdrDebuggerNew();
    }

static uint32 StartVersionSupportInterrupt(ThaModuleCdr self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x03, 0x39);
    }

static eBool ClockSubStateIsSupported(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtClockState ClockStateFromHwStateGet(ThaModuleCdr self, uint8 hwState)
    {
    AtUnused(self);

    if ((hwState == 2) || (hwState == 6)) return cAtClockStateInit;
    if ((hwState <= 1) || (hwState == 7)) return cAtClockStateHoldOver;
    if (hwState == 5)                     return cAtClockStateLocked;

    return cAtClockStateLearning;
    }

static uint32 DefaultDcrClockFrequency(ThaModuleCdr self)
    {
    AtUnused(self);
    return 25000;
    }

static uint32 DcrClockFrequencyRegMask(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAf6_lodcr_prc_freq_cfg_DCRPrcFrequency_Mask;
    }

static uint32 DcrRtpTimestampFrequencyRegMask(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAf6_ThaDcrRtpFreqCfgMask;
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, mMethodsGet(cdrModule), sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, De1CdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, NeedActivateRxEngine);
        mMethodOverride(m_ThaModuleCdrOverride, DefaultExtFrequency);
        mMethodOverride(m_ThaModuleCdrOverride, NeedToSetExtDefaultFrequency);
        mMethodOverride(m_ThaModuleCdrOverride, DebuggerObjectCreate);
        mMethodOverride(m_ThaModuleCdrOverride, StartVersionSupportInterrupt);
        mMethodOverride(m_ThaModuleCdrOverride, ClockSubStateIsSupported);
        mMethodOverride(m_ThaModuleCdrOverride, ClockStateFromHwStateGet);
        mMethodOverride(m_ThaModuleCdrOverride, DefaultDcrClockFrequency);
        mMethodOverride(m_ThaModuleCdrOverride, DcrClockFrequencyRegMask);
        mMethodOverride(m_ThaModuleCdrOverride, DcrRtpTimestampFrequencyRegMask);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModuleCdr);
    }

static AtModule Tha60210021ModuleCdrObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210021ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021ModuleCdrObjectInit(newModule, device);
    }

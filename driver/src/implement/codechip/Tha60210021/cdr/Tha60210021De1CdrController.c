/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60210021De1CdrController.c
 *
 * Created Date: May 14, 2015
 *
 * Description : CDR controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cdr/ThaModuleCdrReg.h"
#include "../../../default/cdr/controllers/ThaCdrControllerInternal.h"
#include "../../../default/cdr/ThaModuleCdrIntrReg.h"
#include "../../../default/map/ThaModuleMapReg.h"
#include "../../../default/pdh/ThaModulePdhReg.h"
#include "../../../default/pw/adapters/ThaPwAdapter.h"
#include "../../default/ThaPdhPwProduct/cdr/ThaPdhPwProductDe1CdrController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021De1CdrController
    {
    tThaPdhPwProductDe1CdrController super;
    }tTha60210021De1CdrController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaDe1CdrControllerMethods m_ThaDe1CdrControllerOverride;
static tThaCdrControllerMethods m_ThaCdrControllerOverride;

/* Save super implementation */
static const tThaDe1CdrControllerMethods *m_ThaDe1CdrControllerMethods = NULL;
static const tThaCdrControllerMethods *m_ThaCdrControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet LiuTimingModeSet(ThaDe1CdrController self, eAtTimingMode timingMode)
    {
    uint32 longRegVal[cThaNumDwordsInLongReg];
    ThaCdrController controller = (ThaCdrController)self;
    AtChannel de1 = ThaCdrControllerChannelGet(controller);
    uint32 de1Id = AtChannelIdGet(de1);
    uint32 posIn32Bit = de1Id % 32;
    uint32 pos32Bit = de1Id / 32;

    mChannelHwLongRead(de1, cThaRegPdhLIULooptimeCfg, longRegVal, cThaNumDwordsInLongReg, cAtModulePdh);
    mFieldIns(&longRegVal[pos32Bit], (cBit0 << posIn32Bit) , posIn32Bit, (timingMode == cAtTimingModeLoop) ? 0x1 : 0x0);
    mChannelHwLongRead(de1, cThaRegPdhLIULooptimeCfg, longRegVal, cThaNumDwordsInLongReg, cAtModulePdh);

    return cAtOk;
    }

static eBool TimingModeIsSupported(ThaCdrController self, eAtTimingMode timingMode)
    {
    if (timingMode == cAtTimingModePrc)
        return cAtTrue;
    return m_ThaCdrControllerMethods->TimingModeIsSupported(self, timingMode);
    }

static eBool TimingIsSupported(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
	if (timingMode == cAtTimingModePrc)
		return cAtTrue;
    /* Let super do other cases */
    return m_ThaCdrControllerMethods->TimingIsSupported(self, timingMode, timingSource);
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    ThaCdrController me = (ThaCdrController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(me);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, TimingModeIsSupported);
        mMethodOverride(m_ThaCdrControllerOverride, TimingIsSupported);
        }

    mMethodsSet(me, &m_ThaCdrControllerOverride);
    }

static void OverrideThaDe1CdrController(ThaCdrController self)
    {
    ThaDe1CdrController me = (ThaDe1CdrController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDe1CdrControllerMethods = mMethodsGet(me);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDe1CdrControllerOverride, m_ThaDe1CdrControllerMethods, sizeof(m_ThaDe1CdrControllerOverride));

        mMethodOverride(m_ThaDe1CdrControllerOverride, LiuTimingModeSet);
        }

    mMethodsSet(me, &m_ThaDe1CdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
	OverrideThaCdrController(self);
    OverrideThaDe1CdrController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021De1CdrController);
    }

static ThaCdrController Tha60210021De1CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductDe1CdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController Tha60210021De1CdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021De1CdrControllerObjectInit(newController, engineId, channel);
    }

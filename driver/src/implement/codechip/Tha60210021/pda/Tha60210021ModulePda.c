/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Tha60210021Pda Module Name
 *
 * File        : Tha60210021ModulePda.c
 *
 * Created Date: Jul 1, 2015
 *
 * Description : PDA module override functions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210021ModulePda.h"
#include "../../Tha60210051/pda/Tha60210051ModulePda.h"
#include "../../Tha60210031/pda/Tha60210031ModulePda.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegPWPdaJitBufCtrlSetLofsHeadMask         cBit1_0
#define cThaRegPWPdaJitBufCtrlSetLofsHeadShift        0

#define cThaRegPWPdaJitBufCtrlSetLofsTailMask         cBit31_25
#define cThaRegPWPdaJitBufCtrlSetLofsTailShift        25

#define cThaRegPDAAisOffMask              cBit26
#define cThaRegPDAAisOffShift             26
#define cThaRegPDALbitPkRepModeMask       cBit27
#define cThaRegPDALbitPkRepModeShift      27
#define cThaRegPDALopsPkRepModeMask       cBit28
#define cThaRegPDALopsPkRepModeShift      28

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021ModulePda
    {
    tThaPdhPwProductModulePda super;
    }tTha60210021ModulePda;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdaMethods   m_ThaModulePdaOverride;

/* Save super implementation */
static const tThaModulePdaMethods * m_ThaModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumJitterBufferBlocks(ThaModulePda self)
    {
    AtUnused(self);
    return 16384;
    }

static uint32 JitterBufferBlockSizeInByte(ThaModulePda self)
    {
    AtUnused(self);
    return 64;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModulePda);
    }

static eBool PwRxPacketsSentToTdmCounterIsSupported(ThaModulePda self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 HwLimitNumPktForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(payloadSize);
    return 512;
    }

static uint16 SAToPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)AtPwBoundCircuitGet(pw);
    AtUnused(self);

    if ((pdhChannel == NULL) || (AtPdhChannelTypeGet(pdhChannel) != cAtPdhChannelTypeE1))
        return 96;

    return 128;
    }

static uint16 MaxPayloadSize(uint16 mtu, AtPw pw)
    {
    uint32 totalHeaderLength = ThaPwAdapterTotalHeaderLengthGet(pw);
    if (mtu < totalHeaderLength)
        return 0;

    return (uint16)(mtu - totalHeaderLength);
    }

static uint16 SAToPMaxPayloadSize(ThaModulePda self, AtPw pw)
    {
    const uint16 cSAToPMtu = 2048;
    AtUnused(self);

    return MaxPayloadSize(cSAToPMtu, pw);
    }

static uint16 CESoPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    return Tha6021ModulePdaCESoPMinPayloadSize(self, pw);
    }

static uint32 MinNumMicrosecondsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    AtUnused(payloadSize);
    AtUnused(pw);
    AtUnused(self);
    return 250;
    }

static uint32 MaxNumMicrosecondsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    AtUnused(payloadSize);
    AtUnused(pw);
    AtUnused(self);
    return 256000;
    }

static eAtRet PwLofsSetThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 address;
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaModulePdaV2 module = (ThaModulePdaV2)self;

    if (Tha60210021ModulePdaLofsClearThresholdIsSupported(self))
        return m_ThaModulePdaMethods->PwLofsSetThresholdSet(self, pw, numPackets);

    address = mMethodsGet(module)->PWPdaJitBufCtrl(module) + mPwOffset(self, pw);
    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mRegFieldSet(longRegVal[1], cThaRegPWPdaJitBufCtrlSetLofsHead, (numPackets >> 7));
    mRegFieldSet(longRegVal[0], cThaRegPWPdaJitBufCtrlSetLofsTail, numPackets);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    return cAtOk;
    }

static uint32 PwLofsSetThresholdGet(ThaModulePda self, AtPw pw)
    {
    uint32 setThres1;
    uint32 setThres2;
    uint32 address;
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaModulePdaV2 module = (ThaModulePdaV2)self;

    if (Tha60210021ModulePdaLofsClearThresholdIsSupported(self))
        return m_ThaModulePdaMethods->PwLofsSetThresholdGet(self, pw);

    address = mMethodsGet(module)->PWPdaJitBufCtrl(module) + mPwOffset(self, pw);
    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    setThres1 = mRegField(longRegVal[1], cThaRegPWPdaJitBufCtrlSetLofsHead);
    setThres2 = mRegField(longRegVal[0], cThaRegPWPdaJitBufCtrlSetLofsTail);

    return (uint32)((setThres1 << 7) | (setThres2));
    }

static uint32 PwLopsThresholdMin(ThaModulePda self, AtPw adapter)
    {
    if (Tha60210021ModulePdaLofsClearThresholdIsSupported(self))
        return m_ThaModulePdaMethods->PwLopsThresholdMin(self, adapter);
    return 1;
    }

static uint32 PwLopsThresholdMax(ThaModulePda self, AtPw adapter)
    {
    if (Tha60210021ModulePdaLofsClearThresholdIsSupported(self))
        return m_ThaModulePdaMethods->PwLopsThresholdMax(self, adapter);
    return cBit8_0;
    }

static eAtRet PwLofsClearThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    if (Tha60210021ModulePdaLofsClearThresholdIsSupported(self))
        return m_ThaModulePdaMethods->PwLofsClearThresholdSet(self, pw, numPackets);
    return cAtOk;
    }

static uint32 PwLofsClearThresholdGet(ThaModulePda self, AtPw pw)
    {
    if (Tha60210021ModulePdaLofsClearThresholdIsSupported(self))
        return m_ThaModulePdaMethods->PwLofsClearThresholdGet(self, pw);
    return 0;
    }

static ThaVersionReader VersionReader(ThaModulePda self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartVersionStopSupportLofsClearThreshold(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return 0x0;

    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x16, 0x01, 0x11, 0x43);
    }

static uint32 NumCurrentAdditionalBytesInJitterBuffer(ThaModulePda self, AtPw pwAdapter)
    {
    if (Tha6021DsxModulePdaJitterBufferAdditionalBytesIsSupported(self))
        return m_ThaModulePdaMethods->NumCurrentAdditionalBytesInJitterBuffer(self, pwAdapter);

    return 0;
    }

static eBool PwCwAutoRxMBitIsConfigurable(ThaModulePda self)
	{
	AtUnused(self);
	return cAtTrue;
	}

static uint32 NumFreeBlocksHwGet(ThaModulePda self)
    {
    return mModuleHwRead(self, 0x180007);
    }

static eBool CanControlLopsPktReplaceMode(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startVersionSupport = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4808);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(device));
    return (currentVersion >= startVersionSupport) ? cAtTrue : cAtFalse;
    }

static eAtModulePwRet LbitPktReplaceModeSet(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    uint32 address, regVal;

    address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);

    mRegFieldSet(regVal, cThaRegPDALbitPkRepMode, ThaModulePdaLbitPktReplaceModeSw2Hw(pktReplaceMode));
    mRegFieldSet(regVal, cThaRegPDAAisOff, 0);
    mChannelHwWrite(pw, address, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtPwPktReplaceMode LbitPktReplaceModeGet(ThaModulePda self, AtPw pw)
    {
    uint32 address, regVal;

    address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);

    return ThaModulePdaLbitPktReplaceModeHw2Sw(mRegField(regVal, cThaRegPDALbitPkRepMode));
    }

static eAtRet LopPktReplaceModeSet(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    uint32 address, regVal;

    address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);

    mRegFieldSet(regVal, cThaRegPDALopsPkRepMode, ThaModulePdaLbitPktReplaceModeSw2Hw(pktReplaceMode));
    mRegFieldSet(regVal, cThaRegPDAAisOff, 0);
    mChannelHwWrite(pw, address, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtPwPktReplaceMode LopPktReplaceModeGet(ThaModulePda self, AtPw pw)
    {
    uint32 address, regVal;
    eAtPwPktReplaceMode replaceMode;

    address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);

    replaceMode = ThaModulePdaLbitPktReplaceModeHw2Sw(mRegField(regVal, cThaRegPDALopsPkRepMode));

    return replaceMode;
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, m_ThaModulePdaMethods, sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, PwRxPacketsSentToTdmCounterIsSupported);
        mMethodOverride(m_ThaModulePdaOverride, MaxNumJitterBufferBlocks);
        mMethodOverride(m_ThaModulePdaOverride, JitterBufferBlockSizeInByte);
        mMethodOverride(m_ThaModulePdaOverride, HwLimitNumPktForJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, JitterBufferBlockSizeInByte);
        mMethodOverride(m_ThaModulePdaOverride, SAToPMaxPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, SAToPMinPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, CESoPMinPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, MinNumMicrosecondsForJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, MaxNumMicrosecondsForJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsSetThresholdSet);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsSetThresholdGet);
        mMethodOverride(m_ThaModulePdaOverride, PwLopsThresholdMin);
        mMethodOverride(m_ThaModulePdaOverride, PwLopsThresholdMax);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsClearThresholdSet);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsClearThresholdGet);
        mMethodOverride(m_ThaModulePdaOverride, NumCurrentAdditionalBytesInJitterBuffer);
        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitIsConfigurable);
        mMethodOverride(m_ThaModulePdaOverride, NumFreeBlocksHwGet);
        mMethodOverride(m_ThaModulePdaOverride, CanControlLopsPktReplaceMode);
        mMethodOverride(m_ThaModulePdaOverride, LbitPktReplaceModeSet);
        mMethodOverride(m_ThaModulePdaOverride, LbitPktReplaceModeGet);
        mMethodOverride(m_ThaModulePdaOverride, LopPktReplaceModeSet);
        mMethodOverride(m_ThaModulePdaOverride, LopPktReplaceModeGet);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePda(self);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210021ModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eBool Tha60210021ModulePdaLofsClearThresholdIsSupported(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtDeviceVersionNumber(device) >= StartVersionStopSupportLofsClearThreshold(self)) ? cAtFalse : cAtTrue;
    }

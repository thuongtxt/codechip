/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0021_RD_PDH_H_
#define _AF6_REG_AF6CCI0021_RD_PDH_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : PDH Global Control
Reg Addr   : 0x00000000 - 0x00000000
Reg Formula: 0x00000000
    Where  : 
Reg Desc   : 
This is the global configuration register for the PDH

------------------------------------------------------------------------------*/
#define cAf6Reg_PDH_Global_Control_Base                                                             0x00000000
#define cAf6Reg_PDH_Global_Control                                                                  0x00000000
#define cAf6Reg_PDH_Global_Control_WidthVal                                                                 32
#define cAf6Reg_PDH_Global_Control_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: PDHFullLineLoop
BitField Type: RW
BitField Desc: Set 1 to enable full Line Loop Back at LIU side
BitField Bits: [7]
--------------------------------------*/
#define cAf6_PDH_Global_Control_PDHFullLineLoop_Bit_Start                                                    7
#define cAf6_PDH_Global_Control_PDHFullLineLoop_Bit_End                                                      7
#define cAf6_PDH_Global_Control_PDHFullLineLoop_Mask                                                     cBit7
#define cAf6_PDH_Global_Control_PDHFullLineLoop_Shift                                                        7
#define cAf6_PDH_Global_Control_PDHFullLineLoop_MaxVal                                                     0x1
#define cAf6_PDH_Global_Control_PDHFullLineLoop_MinVal                                                     0x0
#define cAf6_PDH_Global_Control_PDHFullLineLoop_RstVal                                                     0x0

/*--------------------------------------
BitField Name: PDHFullPayLoop
BitField Type: RW
BitField Desc: Set 1 to enable full Payload Loop back at Data map side
BitField Bits: [6]
--------------------------------------*/
#define cAf6_PDH_Global_Control_PDHFullPayLoop_Bit_Start                                                     6
#define cAf6_PDH_Global_Control_PDHFullPayLoop_Bit_End                                                       6
#define cAf6_PDH_Global_Control_PDHFullPayLoop_Mask                                                      cBit6
#define cAf6_PDH_Global_Control_PDHFullPayLoop_Shift                                                         6
#define cAf6_PDH_Global_Control_PDHFullPayLoop_MaxVal                                                      0x1
#define cAf6_PDH_Global_Control_PDHFullPayLoop_MinVal                                                      0x0
#define cAf6_PDH_Global_Control_PDHFullPayLoop_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PDHSaturation
BitField Type: RW
BitField Desc: Set 1 to enable Saturation mode of all PDH counters. Set 0 for
Roll-Over mode
BitField Bits: [3]
--------------------------------------*/
#define cAf6_PDH_Global_Control_PDHSaturation_Bit_Start                                                      3
#define cAf6_PDH_Global_Control_PDHSaturation_Bit_End                                                        3
#define cAf6_PDH_Global_Control_PDHSaturation_Mask                                                       cBit3
#define cAf6_PDH_Global_Control_PDHSaturation_Shift                                                          3
#define cAf6_PDH_Global_Control_PDHSaturation_MaxVal                                                       0x1
#define cAf6_PDH_Global_Control_PDHSaturation_MinVal                                                       0x0
#define cAf6_PDH_Global_Control_PDHSaturation_RstVal                                                       0x0

/*--------------------------------------
BitField Name: PDHReservedBit
BitField Type: RW
BitField Desc: Transmit Reserved Bit
BitField Bits: [2]
--------------------------------------*/
#define cAf6_PDH_Global_Control_PDHReservedBit_Bit_Start                                                     2
#define cAf6_PDH_Global_Control_PDHReservedBit_Bit_End                                                       2
#define cAf6_PDH_Global_Control_PDHReservedBit_Mask                                                      cBit2
#define cAf6_PDH_Global_Control_PDHReservedBit_Shift                                                         2
#define cAf6_PDH_Global_Control_PDHReservedBit_MaxVal                                                      0x1
#define cAf6_PDH_Global_Control_PDHReservedBit_MinVal                                                      0x0
#define cAf6_PDH_Global_Control_PDHReservedBit_RstVal                                                      0x0

/*--------------------------------------
BitField Name: PDHStufftBit
BitField Type: RW
BitField Desc: Transmit Stuffing Bit
BitField Bits: [1]
--------------------------------------*/
#define cAf6_PDH_Global_Control_PDHStufftBit_Bit_Start                                                       1
#define cAf6_PDH_Global_Control_PDHStufftBit_Bit_End                                                         1
#define cAf6_PDH_Global_Control_PDHStufftBit_Mask                                                        cBit1
#define cAf6_PDH_Global_Control_PDHStufftBit_Shift                                                           1
#define cAf6_PDH_Global_Control_PDHStufftBit_MaxVal                                                        0x1
#define cAf6_PDH_Global_Control_PDHStufftBit_MinVal                                                        0x0
#define cAf6_PDH_Global_Control_PDHStufftBit_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : PDH LIU Tx SW Center FIFO Configuration
Reg Addr   : 0x00000301 - 0x00000301
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the configuration center the FIFO of transmit side on the LIU interface

------------------------------------------------------------------------------*/
#define cAf6Reg_PDH_LIU_Tx_SW_Center_FIFO_Configuration_Base                                        0x00000301
#define cAf6Reg_PDH_LIU_Tx_SW_Center_FIFO_Configuration                                             0x00000301
#define cAf6Reg_PDH_LIU_Tx_SW_Center_FIFO_Configuration_WidthVal                                            64
#define cAf6Reg_PDH_LIU_Tx_SW_Center_FIFO_Configuration_WriteMask                                          0x0

/*--------------------------------------
BitField Name: PDHLiuTxFifoCenterCfg
BitField Type: RW
BitField Desc: Each bit set to 1 to center the Tx FIFO of corresponding LIU
line, set 0 for normal operation
BitField Bits: [47:0]
--------------------------------------*/
#define cAf6_PDH_LIU_Tx_SW_Center_FIFO_Configuration_PDHLiuTxFifoCenterCfg_Bit_Start                                       0
#define cAf6_PDH_LIU_Tx_SW_Center_FIFO_Configuration_PDHLiuTxFifoCenterCfg_Bit_End                                      47
#define cAf6_PDH_LIU_Tx_SW_Center_FIFO_Configuration_PDHLiuTxFifoCenterCfg_Mask_01                                cBit31_0
#define cAf6_PDH_LIU_Tx_SW_Center_FIFO_Configuration_PDHLiuTxFifoCenterCfg_Shift_01                                       0
#define cAf6_PDH_LIU_Tx_SW_Center_FIFO_Configuration_PDHLiuTxFifoCenterCfg_Mask_02                                cBit15_0
#define cAf6_PDH_LIU_Tx_SW_Center_FIFO_Configuration_PDHLiuTxFifoCenterCfg_Shift_02                                       0


/*------------------------------------------------------------------------------
Reg Name   : PDH LIU Looptime Configuration
Reg Addr   : 0x00000302 - 0x00000302
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the configuration for the looptime mode in the LIU interface

------------------------------------------------------------------------------*/
#define cAf6Reg_PDH_LIU_Looptime_Configuration_Base                                                 0x00000302
#define cAf6Reg_PDH_LIU_Looptime_Configuration                                                      0x00000302
#define cAf6Reg_PDH_LIU_Looptime_Configuration_WidthVal                                                     64
#define cAf6Reg_PDH_LIU_Looptime_Configuration_WriteMask                                                   0x0

/*--------------------------------------
BitField Name: PDHLiuLooptimeCfg
BitField Type: RW
BitField Desc: Each bit set to 1 to enable the looptime mode of corresponding
LIU line, set 0 for timing from CDR Block operation
BitField Bits: [47:0]
--------------------------------------*/
#define cAf6_PDH_LIU_Looptime_Configuration_PDHLiuLooptimeCfg_Bit_Start                                       0
#define cAf6_PDH_LIU_Looptime_Configuration_PDHLiuLooptimeCfg_Bit_End                                       47
#define cAf6_PDH_LIU_Looptime_Configuration_PDHLiuLooptimeCfg_Mask_01                                 cBit31_0
#define cAf6_PDH_LIU_Looptime_Configuration_PDHLiuLooptimeCfg_Shift_01                                       0
#define cAf6_PDH_LIU_Looptime_Configuration_PDHLiuLooptimeCfg_Mask_02                                 cBit15_0
#define cAf6_PDH_LIU_Looptime_Configuration_PDHLiuLooptimeCfg_Shift_02                                       0


/*------------------------------------------------------------------------------
Reg Name   : PDH LIU Tx Clock Invert Configuration
Reg Addr   : 0x00000304 - 0x00000304
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the configuration for the transmit clock edge in the LIU interface

------------------------------------------------------------------------------*/
#define cAf6Reg_PDH_LIU_Tx_Clock_Invert_Configuration_Base                                          0x00000304
#define cAf6Reg_PDH_LIU_Tx_Clock_Invert_Configuration                                               0x00000304
#define cAf6Reg_PDH_LIU_Tx_Clock_Invert_Configuration_WidthVal                                              64
#define cAf6Reg_PDH_LIU_Tx_Clock_Invert_Configuration_WriteMask                                            0x0

/*--------------------------------------
BitField Name: PDHLiuTxClkInvCfg
BitField Type: RW
BitField Desc: Each bit set to 1 for sending the LIU Tx data on positive clock
edge of corresponding LIU line, clear to 0 for negative clock edge
BitField Bits: [47:0]
--------------------------------------*/
#define cAf6_PDH_LIU_Tx_Clock_Invert_Configuration_PDHLiuTxClkInvCfg_Bit_Start                                       0
#define cAf6_PDH_LIU_Tx_Clock_Invert_Configuration_PDHLiuTxClkInvCfg_Bit_End                                      47
#define cAf6_PDH_LIU_Tx_Clock_Invert_Configuration_PDHLiuTxClkInvCfg_Mask_01                                cBit31_0
#define cAf6_PDH_LIU_Tx_Clock_Invert_Configuration_PDHLiuTxClkInvCfg_Shift_01                                       0
#define cAf6_PDH_LIU_Tx_Clock_Invert_Configuration_PDHLiuTxClkInvCfg_Mask_02                                cBit15_0
#define cAf6_PDH_LIU_Tx_Clock_Invert_Configuration_PDHLiuTxClkInvCfg_Shift_02                                       0


/*------------------------------------------------------------------------------
Reg Name   : PDH LIU Tx Fifo Full Sticky
Reg Addr   : 0x00000310 - 0x00000310
Reg Formula: 
    Where  : 
Reg Desc   : 
This register reports the FIFO Full status of the LIU Tx

------------------------------------------------------------------------------*/
#define cAf6Reg_PDH_LIU_Tx_Fifo_Full_Sticky_Base                                                    0x00000310
#define cAf6Reg_PDH_LIU_Tx_Fifo_Full_Sticky                                                         0x00000310
#define cAf6Reg_PDH_LIU_Tx_Fifo_Full_Sticky_WidthVal                                                        64
#define cAf6Reg_PDH_LIU_Tx_Fifo_Full_Sticky_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: PDHLiuTxFiFoFull
BitField Type: RW
BitField Desc: Each bit set to 1 to report the Tx FIFO full status of
corresponding LIU line
BitField Bits: [47:0]
--------------------------------------*/
#define cAf6_PDH_LIU_Tx_Fifo_Full_Sticky_PDHLiuTxFiFoFull_Bit_Start                                          0
#define cAf6_PDH_LIU_Tx_Fifo_Full_Sticky_PDHLiuTxFiFoFull_Bit_End                                           47
#define cAf6_PDH_LIU_Tx_Fifo_Full_Sticky_PDHLiuTxFiFoFull_Mask_01                                     cBit31_0
#define cAf6_PDH_LIU_Tx_Fifo_Full_Sticky_PDHLiuTxFiFoFull_Shift_01                                           0
#define cAf6_PDH_LIU_Tx_Fifo_Full_Sticky_PDHLiuTxFiFoFull_Mask_02                                     cBit15_0
#define cAf6_PDH_LIU_Tx_Fifo_Full_Sticky_PDHLiuTxFiFoFull_Shift_02                                           0


/*------------------------------------------------------------------------------
Reg Name   : PDH LIU Tx Fifo Empty Sticky
Reg Addr   : 0x00000311 - 0x00000311
Reg Formula: 
    Where  : 
Reg Desc   : 
This register reports the FIFO Empty status of the LIU Rx

------------------------------------------------------------------------------*/
#define cAf6Reg_PDH_LIU_Tx_Fifo_Empty_Sticky_Base                                                   0x00000311
#define cAf6Reg_PDH_LIU_Tx_Fifo_Empty_Sticky                                                        0x00000311
#define cAf6Reg_PDH_LIU_Tx_Fifo_Empty_Sticky_WidthVal                                                       64
#define cAf6Reg_PDH_LIU_Tx_Fifo_Empty_Sticky_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: PDHLiuTxFifoEmpty
BitField Type: RW
BitField Desc: Each bit set to 1 to report the Rx FIFO empty status of
corresponding LIU line
BitField Bits: [47:0]
--------------------------------------*/
#define cAf6_PDH_LIU_Tx_Fifo_Empty_Sticky_PDHLiuTxFifoEmpty_Bit_Start                                        0
#define cAf6_PDH_LIU_Tx_Fifo_Empty_Sticky_PDHLiuTxFifoEmpty_Bit_End                                         47
#define cAf6_PDH_LIU_Tx_Fifo_Empty_Sticky_PDHLiuTxFifoEmpty_Mask_01                                   cBit31_0
#define cAf6_PDH_LIU_Tx_Fifo_Empty_Sticky_PDHLiuTxFifoEmpty_Shift_01                                         0
#define cAf6_PDH_LIU_Tx_Fifo_Empty_Sticky_PDHLiuTxFifoEmpty_Mask_02                                   cBit15_0
#define cAf6_PDH_LIU_Tx_Fifo_Empty_Sticky_PDHLiuTxFifoEmpty_Shift_02                                         0


/*------------------------------------------------------------------------------
Reg Name   : PDH LIU Rx Fifo Full Sticky
Reg Addr   : 0x00000312 - 0x00000312
Reg Formula: 
    Where  : 
Reg Desc   : 
This register reports the FIFO Full status of the LIU Rx

------------------------------------------------------------------------------*/
#define cAf6Reg_PDH_LIU_Rx_Fifo_Full_Sticky_Base                                                    0x00000312
#define cAf6Reg_PDH_LIU_Rx_Fifo_Full_Sticky                                                         0x00000312
#define cAf6Reg_PDH_LIU_Rx_Fifo_Full_Sticky_WidthVal                                                        64
#define cAf6Reg_PDH_LIU_Rx_Fifo_Full_Sticky_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: PDHLiuRxFifoFull
BitField Type: RW
BitField Desc: Each bit set to 1 to report the Rx FIFO full status of
corresponding LIU line
BitField Bits: [47:0]
--------------------------------------*/
#define cAf6_PDH_LIU_Rx_Fifo_Full_Sticky_PDHLiuRxFifoFull_Bit_Start                                          0
#define cAf6_PDH_LIU_Rx_Fifo_Full_Sticky_PDHLiuRxFifoFull_Bit_End                                           47
#define cAf6_PDH_LIU_Rx_Fifo_Full_Sticky_PDHLiuRxFifoFull_Mask_01                                     cBit31_0
#define cAf6_PDH_LIU_Rx_Fifo_Full_Sticky_PDHLiuRxFifoFull_Shift_01                                           0
#define cAf6_PDH_LIU_Rx_Fifo_Full_Sticky_PDHLiuRxFifoFull_Mask_02                                     cBit15_0
#define cAf6_PDH_LIU_Rx_Fifo_Full_Sticky_PDHLiuRxFifoFull_Shift_02                                           0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer FBE Threshold Control
Reg Addr   : 0x00050003 - 0x00050003
Reg Formula: 0x00050003
    Where  : 
Reg Desc   : 
This is the FBE threshold configuration register for the PDH DS1/E1/J1 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rxfrm_fbe_thrsh_Base                                                           0x00050003
#define cAf6Reg_dej1_rxfrm_fbe_thrsh                                                                0x00050003
#define cAf6Reg_dej1_rxfrm_fbe_thrsh_WidthVal                                                               32
#define cAf6Reg_dej1_rxfrm_fbe_thrsh_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: RxDE1FbeThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the FBE counter exceed this
threshold
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rxfrm_fbe_thrsh_RxDE1FbeThres_Bit_Start                                                    0
#define cAf6_dej1_rxfrm_fbe_thrsh_RxDE1FbeThres_Bit_End                                                      7
#define cAf6_dej1_rxfrm_fbe_thrsh_RxDE1FbeThres_Mask                                                   cBit7_0
#define cAf6_dej1_rxfrm_fbe_thrsh_RxDE1FbeThres_Shift                                                        0
#define cAf6_dej1_rxfrm_fbe_thrsh_RxDE1FbeThres_MaxVal                                                    0xff
#define cAf6_dej1_rxfrm_fbe_thrsh_RxDE1FbeThres_MinVal                                                     0x0
#define cAf6_dej1_rxfrm_fbe_thrsh_RxDE1FbeThres_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer CRC Threshold Control
Reg Addr   : 0x00050004 - 0x00050004
Reg Formula: 0x00050004
    Where  : 
Reg Desc   : 
This is the CRC threshold configuration register for the PDH DS1/E1/J1 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rxfrm_crc_thrsh_Base                                                           0x00050004
#define cAf6Reg_dej1_rxfrm_crc_thrsh                                                                0x00050004
#define cAf6Reg_dej1_rxfrm_crc_thrsh_WidthVal                                                               32
#define cAf6Reg_dej1_rxfrm_crc_thrsh_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: RxDE1CrcThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the CRC counter exceed this
threshold
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rxfrm_crc_thrsh_RxDE1CrcThres_Bit_Start                                                    0
#define cAf6_dej1_rxfrm_crc_thrsh_RxDE1CrcThres_Bit_End                                                      7
#define cAf6_dej1_rxfrm_crc_thrsh_RxDE1CrcThres_Mask                                                   cBit7_0
#define cAf6_dej1_rxfrm_crc_thrsh_RxDE1CrcThres_Shift                                                        0
#define cAf6_dej1_rxfrm_crc_thrsh_RxDE1CrcThres_MaxVal                                                    0xff
#define cAf6_dej1_rxfrm_crc_thrsh_RxDE1CrcThres_MinVal                                                     0x0
#define cAf6_dej1_rxfrm_crc_thrsh_RxDE1CrcThres_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer REI Threshold Control
Reg Addr   : 0x00050005 - 0x00050005
Reg Formula: 0x00050005
    Where  : 
Reg Desc   : 
This is the REI threshold configuration register for the PDH DS1/E1/J1 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rxfrm_rei_thrsh_Base                                                           0x00050005
#define cAf6Reg_dej1_rxfrm_rei_thrsh                                                                0x00050005
#define cAf6Reg_dej1_rxfrm_rei_thrsh_WidthVal                                                               32
#define cAf6Reg_dej1_rxfrm_rei_thrsh_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: RxDE1ReiThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the FBE counter exceed this
threshold
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rxfrm_rei_thrsh_RxDE1ReiThres_Bit_Start                                                    0
#define cAf6_dej1_rxfrm_rei_thrsh_RxDE1ReiThres_Bit_End                                                      7
#define cAf6_dej1_rxfrm_rei_thrsh_RxDE1ReiThres_Mask                                                   cBit7_0
#define cAf6_dej1_rxfrm_rei_thrsh_RxDE1ReiThres_Shift                                                        0
#define cAf6_dej1_rxfrm_rei_thrsh_RxDE1ReiThres_MaxVal                                                    0xff
#define cAf6_dej1_rxfrm_rei_thrsh_RxDE1ReiThres_MinVal                                                     0x0
#define cAf6_dej1_rxfrm_rei_thrsh_RxDE1ReiThres_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer Control
Reg Addr   : 0x00054000 - 0x0005402F
Reg Formula: 0x00054000 + liuid
    Where  : 
           + $liuid(0-47):
Reg Desc   : 
DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rxfrm_ctrl_Base                                                                0x00054000
#define cAf6Reg_dej1_rxfrm_ctrl(liuid)                                                    (0x00054000+(liuid))
#define cAf6Reg_dej1_rxfrm_ctrl_WidthVal                                                                    32
#define cAf6Reg_dej1_rxfrm_ctrl_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: RxDE1thres
BitField Type: RW
BitField Desc: Rx threshold of detected Synchronization Status Message
BitField Bits: [31:28]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDE1thres_Bit_Start                                                           28
#define cAf6_dej1_rxfrm_ctrl_RxDE1thres_Bit_End                                                             31
#define cAf6_dej1_rxfrm_ctrl_RxDE1thres_Mask                                                         cBit31_28
#define cAf6_dej1_rxfrm_ctrl_RxDE1thres_Shift                                                               28
#define cAf6_dej1_rxfrm_ctrl_RxDE1thres_MaxVal                                                             0xf
#define cAf6_dej1_rxfrm_ctrl_RxDE1thres_MinVal                                                             0x0
#define cAf6_dej1_rxfrm_ctrl_RxDE1thres_RstVal                                                             0x6

/*--------------------------------------
BitField Name: RxDE1FrcLos
BitField Type: RW
BitField Desc: Set 1 to force LOS
BitField Bits: [27]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLos_Bit_Start                                                          27
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLos_Bit_End                                                            27
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLos_Mask                                                           cBit27
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLos_Shift                                                              27
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLos_MaxVal                                                            0x1
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLos_MinVal                                                            0x0
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLos_RstVal                                                            0x0

/*--------------------------------------
BitField Name: RxDE1MFASChken
BitField Type: RW
BitField Desc: Set 1 to enable check MFAS to go INFRAME in case of E1CRC mode.
Default is 0
BitField Bits: [26]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDE1MFASChken_Bit_Start                                                       26
#define cAf6_dej1_rxfrm_ctrl_RxDE1MFASChken_Bit_End                                                         26
#define cAf6_dej1_rxfrm_ctrl_RxDE1MFASChken_Mask                                                        cBit26
#define cAf6_dej1_rxfrm_ctrl_RxDE1MFASChken_Shift                                                           26
#define cAf6_dej1_rxfrm_ctrl_RxDE1MFASChken_MaxVal                                                         0x1
#define cAf6_dej1_rxfrm_ctrl_RxDE1MFASChken_MinVal                                                         0x0
#define cAf6_dej1_rxfrm_ctrl_RxDE1MFASChken_RstVal                                                         0x0

/*--------------------------------------
BitField Name: RxDE1LosCntThres
BitField Type: RW
BitField Desc: Threshold of LOS monitoring block. This function always monitors
the number of bit 1 of the incoming signal in each of two consecutive frame
periods (256*2 bits         in E1, 193*3 bits in DS1). If it is less than the
desire number, LOS will be set then.
BitField Bits: [25:23]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDE1LosCntThres_Bit_Start                                                     23
#define cAf6_dej1_rxfrm_ctrl_RxDE1LosCntThres_Bit_End                                                       25
#define cAf6_dej1_rxfrm_ctrl_RxDE1LosCntThres_Mask                                                   cBit25_23
#define cAf6_dej1_rxfrm_ctrl_RxDE1LosCntThres_Shift                                                         23
#define cAf6_dej1_rxfrm_ctrl_RxDE1LosCntThres_MaxVal                                                       0x7
#define cAf6_dej1_rxfrm_ctrl_RxDE1LosCntThres_MinVal                                                       0x0
#define cAf6_dej1_rxfrm_ctrl_RxDE1LosCntThres_RstVal                                                       0x0

/*--------------------------------------
BitField Name: RxDE1MonOnly
BitField Type: RW
BitField Desc: Set 1 to enable the Monitor Only mode at the RXDS1/E1 framer
BitField Bits: [22]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDE1MonOnly_Bit_Start                                                         22
#define cAf6_dej1_rxfrm_ctrl_RxDE1MonOnly_Bit_End                                                           22
#define cAf6_dej1_rxfrm_ctrl_RxDE1MonOnly_Mask                                                          cBit22
#define cAf6_dej1_rxfrm_ctrl_RxDE1MonOnly_Shift                                                             22
#define cAf6_dej1_rxfrm_ctrl_RxDE1MonOnly_MaxVal                                                           0x1
#define cAf6_dej1_rxfrm_ctrl_RxDE1MonOnly_MinVal                                                           0x0
#define cAf6_dej1_rxfrm_ctrl_RxDE1MonOnly_RstVal                                                           0x0

/*--------------------------------------
BitField Name: RxDE1LocalLineLoop
BitField Type: RW
BitField Desc: Set 1 to enable Local Line loop back
BitField Bits: [21]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDE1LocalLineLoop_Bit_Start                                                   21
#define cAf6_dej1_rxfrm_ctrl_RxDE1LocalLineLoop_Bit_End                                                     21
#define cAf6_dej1_rxfrm_ctrl_RxDE1LocalLineLoop_Mask                                                    cBit21
#define cAf6_dej1_rxfrm_ctrl_RxDE1LocalLineLoop_Shift                                                       21
#define cAf6_dej1_rxfrm_ctrl_RxDE1LocalLineLoop_MaxVal                                                     0x1
#define cAf6_dej1_rxfrm_ctrl_RxDE1LocalLineLoop_MinVal                                                     0x0
#define cAf6_dej1_rxfrm_ctrl_RxDE1LocalLineLoop_RstVal                                                     0x0

/*--------------------------------------
BitField Name: RxDe1LocalPayLoop
BitField Type: RW
BitField Desc: Set 1 to enable Local Payload loop back
BitField Bits: [20]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDe1LocalPayLoop_Bit_Start                                                    20
#define cAf6_dej1_rxfrm_ctrl_RxDe1LocalPayLoop_Bit_End                                                      20
#define cAf6_dej1_rxfrm_ctrl_RxDe1LocalPayLoop_Mask                                                     cBit20
#define cAf6_dej1_rxfrm_ctrl_RxDe1LocalPayLoop_Shift                                                        20
#define cAf6_dej1_rxfrm_ctrl_RxDe1LocalPayLoop_MaxVal                                                      0x1
#define cAf6_dej1_rxfrm_ctrl_RxDe1LocalPayLoop_MinVal                                                      0x0
#define cAf6_dej1_rxfrm_ctrl_RxDe1LocalPayLoop_RstVal                                                      0x0

/*--------------------------------------
BitField Name: RxDE1E1SaCfg
BitField Type: RW
BitField Desc: Select position of rx Sa bit 011 : bit3 of frame 1,3,5,7 100 :
bit4 of frame 1,3,5,7 101 : bit5 of frame 1,3,5,7 110 : bit6 of frame 1,3,5,7
111 : bit7 of frame 1,3,5,7
BitField Bits: [19:17]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDE1E1SaCfg_Bit_Start                                                         17
#define cAf6_dej1_rxfrm_ctrl_RxDE1E1SaCfg_Bit_End                                                           19
#define cAf6_dej1_rxfrm_ctrl_RxDE1E1SaCfg_Mask                                                       cBit19_17
#define cAf6_dej1_rxfrm_ctrl_RxDE1E1SaCfg_Shift                                                             17
#define cAf6_dej1_rxfrm_ctrl_RxDE1E1SaCfg_MaxVal                                                           0x7
#define cAf6_dej1_rxfrm_ctrl_RxDE1E1SaCfg_MinVal                                                           0x0
#define cAf6_dej1_rxfrm_ctrl_RxDE1E1SaCfg_RstVal                                                           0x3

/*--------------------------------------
BitField Name: RxDE1LofThres
BitField Type: RW
BitField Desc: Threshold for FAS error to declare LOF in DS1/E1/J1 mode
BitField Bits: [16:13]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDE1LofThres_Bit_Start                                                        13
#define cAf6_dej1_rxfrm_ctrl_RxDE1LofThres_Bit_End                                                          16
#define cAf6_dej1_rxfrm_ctrl_RxDE1LofThres_Mask                                                      cBit16_13
#define cAf6_dej1_rxfrm_ctrl_RxDE1LofThres_Shift                                                            13
#define cAf6_dej1_rxfrm_ctrl_RxDE1LofThres_MaxVal                                                          0xf
#define cAf6_dej1_rxfrm_ctrl_RxDE1LofThres_MinVal                                                          0x0
#define cAf6_dej1_rxfrm_ctrl_RxDE1LofThres_RstVal                                                          0x0

/*--------------------------------------
BitField Name: RxDE1AisCntThres
BitField Type: RW
BitField Desc: Threshold of AIS monitoring block. This function always monitors
the     number of bit 0 of the incoming signal in each of two consecutive frame
periods (256*2 bits in  E1, 193*3 bits in DS1). If it is less than the desire
number, AIS will be set then.
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisCntThres_Bit_Start                                                     10
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisCntThres_Bit_End                                                       12
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisCntThres_Mask                                                   cBit12_10
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisCntThres_Shift                                                         10
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisCntThres_MaxVal                                                       0x7
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisCntThres_MinVal                                                       0x0
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisCntThres_RstVal                                                       0x0

/*--------------------------------------
BitField Name: RxDE1RaiCntThres
BitField Type: RW
BitField Desc: Threshold of RAI monitoring block
BitField Bits: [9:7]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDE1RaiCntThres_Bit_Start                                                      7
#define cAf6_dej1_rxfrm_ctrl_RxDE1RaiCntThres_Bit_End                                                        9
#define cAf6_dej1_rxfrm_ctrl_RxDE1RaiCntThres_Mask                                                     cBit9_7
#define cAf6_dej1_rxfrm_ctrl_RxDE1RaiCntThres_Shift                                                          7
#define cAf6_dej1_rxfrm_ctrl_RxDE1RaiCntThres_MaxVal                                                       0x7
#define cAf6_dej1_rxfrm_ctrl_RxDE1RaiCntThres_MinVal                                                       0x0
#define cAf6_dej1_rxfrm_ctrl_RxDE1RaiCntThres_RstVal                                                       0x0

/*--------------------------------------
BitField Name: RxDE1AisMaskEn
BitField Type: RW
BitField Desc: Set 1 to enable output data of the DS1/E1/J1 framer to be set to
all one (1)     during LOF
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisMaskEn_Bit_Start                                                        6
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisMaskEn_Bit_End                                                          6
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisMaskEn_Mask                                                         cBit6
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisMaskEn_Shift                                                            6
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisMaskEn_MaxVal                                                         0x1
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisMaskEn_MinVal                                                         0x0
#define cAf6_dej1_rxfrm_ctrl_RxDE1AisMaskEn_RstVal                                                         0x0

/*--------------------------------------
BitField Name: RxDE1En
BitField Type: RW
BitField Desc: Set 1 to enable the DS1/E1/J1 framer.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDE1En_Bit_Start                                                               5
#define cAf6_dej1_rxfrm_ctrl_RxDE1En_Bit_End                                                                 5
#define cAf6_dej1_rxfrm_ctrl_RxDE1En_Mask                                                                cBit5
#define cAf6_dej1_rxfrm_ctrl_RxDE1En_Shift                                                                   5
#define cAf6_dej1_rxfrm_ctrl_RxDE1En_MaxVal                                                                0x1
#define cAf6_dej1_rxfrm_ctrl_RxDE1En_MinVal                                                                0x0
#define cAf6_dej1_rxfrm_ctrl_RxDE1En_RstVal                                                                0x0

/*--------------------------------------
BitField Name: RxDE1FrcLof
BitField Type: RW
BitField Desc: Set 1 to force Re-frame (default 0)
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLof_Bit_Start                                                           4
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLof_Bit_End                                                             4
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLof_Mask                                                            cBit4
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLof_Shift                                                               4
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLof_MaxVal                                                            0x1
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLof_MinVal                                                            0x0
#define cAf6_dej1_rxfrm_ctrl_RxDE1FrcLof_RstVal                                                            0x0

/*--------------------------------------
BitField Name: RxDs1Md
BitField Type: RW
BitField Desc: Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF
(D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1
Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dej1_rxfrm_ctrl_RxDs1Md_Bit_Start                                                               0
#define cAf6_dej1_rxfrm_ctrl_RxDs1Md_Bit_End                                                                 3
#define cAf6_dej1_rxfrm_ctrl_RxDs1Md_Mask                                                              cBit3_0
#define cAf6_dej1_rxfrm_ctrl_RxDs1Md_Shift                                                                   0
#define cAf6_dej1_rxfrm_ctrl_RxDs1Md_MaxVal                                                                0xf
#define cAf6_dej1_rxfrm_ctrl_RxDs1Md_MinVal                                                                0x0
#define cAf6_dej1_rxfrm_ctrl_RxDs1Md_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer HW Status
Reg Addr   : 0x00054400 - 0x0005402F
Reg Formula: 0x00054400 + liuid
    Where  : 
           + $liuid(0-47):
Reg Desc   : 
These bit[2:0] of registers are used for Hardware status only, bit[19:16] of registers are indicate SSM E1 value

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rxfrm_hw_stat_Base0                                                             0x00054400
#define cAf6Reg_dej1_rxfrm_hw_stat(liuid)                                                 (0x00054400+(liuid))
#define cAf6Reg_dej1_rxfrm_hw_stat_WidthVal                                                                 32
#define cAf6Reg_dej1_rxfrm_hw_stat_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: RXE1SSMSta
BitField Type: RO
BitField Desc: Current value of SSM message
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_Bit_Start                                                        16
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_Bit_End                                                          19
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_Mask                                                      cBit19_16
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_Shift                                                            16
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_MaxVal                                                          0xf
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_MinVal                                                          0x0
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_RstVal                                                          0x0

/*--------------------------------------
BitField Name: RxDE1Sta
BitField Type: RO
BitField Desc: 5 In Frame
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_Bit_Start                                                           0
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_Bit_End                                                             2
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_Mask                                                          cBit2_0
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_Shift                                                               0
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_MaxVal                                                            0x7
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_MinVal                                                            0x0
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer CRC Error Counter
Reg Addr   : 0x00056800 - 0x0005687F
Reg Formula: 0x00056800 + 64*Rd2Clr + LiuID
    Where  : 
           + $Rd2Clr(0 - 1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $LiuID(0 - 47):
Reg Desc   : 
This is the per channel CRC error counter for DS1/E1/J1 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rxfrm_crc_err_cnt_Base                                                         0x00056800
#define cAf6Reg_dej1_rxfrm_crc_err_cnt(Rd2Clr, LiuID)                         (0x00056800+64*(Rd2Clr)+(LiuID))
#define cAf6Reg_dej1_rxfrm_crc_err_cnt_WidthVal                                                             32
#define cAf6Reg_dej1_rxfrm_crc_err_cnt_WriteMask                                                           0x0

/*--------------------------------------
BitField Name: DE1CrcErrCnt
BitField Type: RW
BitField Desc: DS1/E1/J1 CRC Error Accumulator
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rxfrm_crc_err_cnt_DE1CrcErrCnt_Bit_Start                                                   0
#define cAf6_dej1_rxfrm_crc_err_cnt_DE1CrcErrCnt_Bit_End                                                     7
#define cAf6_dej1_rxfrm_crc_err_cnt_DE1CrcErrCnt_Mask                                                  cBit7_0
#define cAf6_dej1_rxfrm_crc_err_cnt_DE1CrcErrCnt_Shift                                                       0
#define cAf6_dej1_rxfrm_crc_err_cnt_DE1CrcErrCnt_MaxVal                                                   0xff
#define cAf6_dej1_rxfrm_crc_err_cnt_DE1CrcErrCnt_MinVal                                                    0x0
#define cAf6_dej1_rxfrm_crc_err_cnt_DE1CrcErrCnt_RstVal                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer REI Counter
Reg Addr   : 0x00057000 - 0x0005707F
Reg Formula: 0x00057000 + 64*Rd2Clr + LiuID
    Where  : 
           + $Rd2Clr(0 - 1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $LiuID(0-47):
Reg Desc   : 
This is the per channel REI counter for DS1/E1/J1 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rxfrm_rei_cnt_Base                                                             0x00057000
#define cAf6Reg_dej1_rxfrm_rei_cnt(Rd2Clr, LiuID)                             (0x00057000+64*(Rd2Clr)+(LiuID))
#define cAf6Reg_dej1_rxfrm_rei_cnt_WidthVal                                                                 32
#define cAf6Reg_dej1_rxfrm_rei_cnt_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: DE1ReiCnt
BitField Type: RW
BitField Desc: DS1/E1/J1 REI error accumulator
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rxfrm_rei_cnt_DE1ReiCnt_Bit_Start                                                          0
#define cAf6_dej1_rxfrm_rei_cnt_DE1ReiCnt_Bit_End                                                            7
#define cAf6_dej1_rxfrm_rei_cnt_DE1ReiCnt_Mask                                                         cBit7_0
#define cAf6_dej1_rxfrm_rei_cnt_DE1ReiCnt_Shift                                                              0
#define cAf6_dej1_rxfrm_rei_cnt_DE1ReiCnt_MaxVal                                                          0xff
#define cAf6_dej1_rxfrm_rei_cnt_DE1ReiCnt_MinVal                                                           0x0
#define cAf6_dej1_rxfrm_rei_cnt_DE1ReiCnt_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer FBE Counter
Reg Addr   : 0x00056000 - 0x0005607F
Reg Formula: 0x00056000 + 64*Rd2Clr + LiuID
    Where  : 
           + $Rd2Clr(0 - 1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $LiuID(0-47):
Reg Desc   : 
This is the per channel REI counter for DS1/E1/J1 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rxfrm_fbe_cnt_Base                                                             0x00056000
#define cAf6Reg_dej1_rxfrm_fbe_cnt(Rd2Clr, LiuID)                             (0x00056000+64*(Rd2Clr)+(LiuID))
#define cAf6Reg_dej1_rxfrm_fbe_cnt_WidthVal                                                                 32
#define cAf6Reg_dej1_rxfrm_fbe_cnt_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: DE1FbeCnt
BitField Type: RW
BitField Desc: DS1/E1/J1 F-bit error accumulator
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_dej1_rxfrm_fbe_cnt_DE1FbeCnt_Bit_Start                                                          0
#define cAf6_dej1_rxfrm_fbe_cnt_DE1FbeCnt_Bit_End                                                            7
#define cAf6_dej1_rxfrm_fbe_cnt_DE1FbeCnt_Mask                                                         cBit7_0
#define cAf6_dej1_rxfrm_fbe_cnt_DE1FbeCnt_Shift                                                              0
#define cAf6_dej1_rxfrm_fbe_cnt_DE1FbeCnt_MaxVal                                                          0xff
#define cAf6_dej1_rxfrm_fbe_cnt_DE1FbeCnt_MinVal                                                           0x0
#define cAf6_dej1_rxfrm_fbe_cnt_DE1FbeCnt_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per Channel Interrupt Enable Control
Reg Addr   : 0x00055000-0x0005503F
Reg Formula: 0x00055000 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-1): STS-1/VC-3 ID
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_chn_intr_en_ctrl_Base                                            0x00055000
#define cAf6Reg_dej1_rx_framer_per_chn_intr_en_ctrl(StsID, VtnID)              (0x00055000+(StsID)*32+(VtnID))
#define cAf6Reg_dej1_rx_framer_per_chn_intr_en_ctrl_WidthVal                                                32
#define cAf6Reg_dej1_rx_framer_per_chn_intr_en_ctrl_WriteMask                                              0x0

/*--------------------------------------
BitField Name: DE1BerSdIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable the BER-SD interrupt.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_Bit_Start                                      13
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_Bit_End                                      13
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_Mask                                    cBit13
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_Shift                                       13
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_MaxVal                                     0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSdIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: DE1BerSfIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable the BER-SF interrupt.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_Bit_Start                                      12
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_Bit_End                                      12
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_Mask                                    cBit12
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_Shift                                       12
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_MaxVal                                     0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1BerSfIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: DE1oblcIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable the new Outband LoopCode detected to generate an
interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_Bit_Start                                      11
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_Bit_End                                      11
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_Mask                                     cBit11
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_Shift                                        11
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_MaxVal                                      0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_MinVal                                      0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1oblcIntrEn_RstVal                                      0x0

/*--------------------------------------
BitField Name: DE1iblcIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable the new Inband LoopCode detected to generate an
interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_Bit_Start                                      10
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_Bit_End                                      10
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_Mask                                     cBit10
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_Shift                                        10
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_MaxVal                                      0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_MinVal                                      0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1iblcIntrEn_RstVal                                      0x0

/*--------------------------------------
BitField Name: DE1SigRAIIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Signalling RAI event to generate an interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_Bit_Start                                       9
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_Bit_End                                       9
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_Mask                                    cBit9
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_Shift                                       9
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_MaxVal                                     0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigRAIIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: DE1SigLOFIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable Signalling LOF event to generate an interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_Bit_Start                                       8
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_Bit_End                                       8
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_Mask                                    cBit8
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_Shift                                       8
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_MaxVal                                     0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1SigLOFIntrEn_RstVal                                     0x0

/*--------------------------------------
BitField Name: DE1FbeIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change FBE te event to generate an interrupt.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_Bit_Start                                       7
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_Bit_End                                        7
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_Mask                                       cBit7
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_Shift                                          7
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1FbeIntrEn_RstVal                                       0x0

/*--------------------------------------
BitField Name: DE1CrcIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change CRC te event to generate an interrupt.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_Bit_Start                                       6
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_Bit_End                                        6
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_Mask                                       cBit6
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_Shift                                          6
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1CrcIntrEn_RstVal                                       0x0

/*--------------------------------------
BitField Name: DE1ReiIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change REI te event to generate an interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_Bit_Start                                       5
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_Bit_End                                        5
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_Mask                                       cBit5
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_Shift                                          5
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1ReiIntrEn_RstVal                                       0x0

/*--------------------------------------
BitField Name: DE1LomfIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOMF te event to generate an interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_Bit_Start                                       4
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_Bit_End                                       4
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_Mask                                      cBit4
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_Shift                                         4
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_MaxVal                                      0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_MinVal                                      0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LomfIntrEn_RstVal                                      0x0

/*--------------------------------------
BitField Name: DE1LosIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOS te event to generate an interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_Bit_Start                                       3
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_Bit_End                                        3
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_Mask                                       cBit3
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_Shift                                          3
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LosIntrEn_RstVal                                       0x0

/*--------------------------------------
BitField Name: DE1AisIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change AIS te event to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_Bit_Start                                       2
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_Bit_End                                        2
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_Mask                                       cBit2
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_Shift                                          2
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1AisIntrEn_RstVal                                       0x0

/*--------------------------------------
BitField Name: DE1RaiIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change RAI te event to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_Bit_Start                                       1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_Bit_End                                        1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_Mask                                       cBit1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_Shift                                          1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1RaiIntrEn_RstVal                                       0x0

/*--------------------------------------
BitField Name: DE1LofIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOF te event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_Bit_Start                                       0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_Bit_End                                        0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_Mask                                       cBit0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_Shift                                          0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_MaxVal                                       0x1
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_MinVal                                       0x0
#define cAf6_dej1_rx_framer_per_chn_intr_en_ctrl_DE1LofIntrEn_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per Channel Interrupt Status
Reg Addr   : 0x00055040-0x0005507F
Reg Formula: 0x00055040 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-1): STS-1/VC-3 ID
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel interrupt tus of DS1/E1/J1 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_chn_intr_stat_Base                                               0x00055040
#define cAf6Reg_dej1_rx_framer_per_chn_intr_stat(StsID, VtnID)                 (0x00055040+(StsID)*32+(VtnID))
#define cAf6Reg_dej1_rx_framer_per_chn_intr_stat_WidthVal                                                   32
#define cAf6Reg_dej1_rx_framer_per_chn_intr_stat_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: DE1BerSdIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change of BER-SD.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerTcaIntr_Mask                                        cBit14
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerTcaIntr_Shift                                           14

#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_Bit_Start                                        13
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_Bit_End                                          13
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_Mask                                         cBit13
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_Shift                                            13
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_MaxVal                                          0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_MinVal                                          0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSdIntr_RstVal                                          0x0

/*--------------------------------------
BitField Name: DE1BerSfIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change of BER-SF.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_Bit_Start                                        12
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_Bit_End                                          12
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_Mask                                         cBit12
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_Shift                                            12
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_MaxVal                                          0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_MinVal                                          0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1BerSfIntr_RstVal                                          0x0

/*--------------------------------------
BitField Name: DE1oblcIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change the new Outband LoopCode detected to
generate an interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_Bit_Start                                         11
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_Bit_End                                           11
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_Mask                                          cBit11
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_Shift                                             11
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_MaxVal                                           0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_MinVal                                           0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1oblcIntr_RstVal                                           0x0

/*--------------------------------------
BitField Name: DE1iblcIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change the new Inband LoopCode detected to
generate an interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_Bit_Start                                         10
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_Bit_End                                           10
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_Mask                                          cBit10
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_Shift                                             10
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_MaxVal                                           0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_MinVal                                           0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1iblcIntr_RstVal                                           0x0

/*--------------------------------------
BitField Name: DE1SigRAIIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change Signalling RAI event to generate an
interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_Bit_Start                                        9
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_Bit_End                                          9
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_Mask                                         cBit9
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_Shift                                            9
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigRAIIntr_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1SigLOFIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change Signalling LOF event to generate an
interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_Bit_Start                                        8
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_Bit_End                                          8
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_Mask                                         cBit8
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_Shift                                            8
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1SigLOFIntr_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1FbeIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in FBE exceed threshold te event.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_Bit_Start                                           7
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_Bit_End                                             7
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_Mask                                            cBit7
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_Shift                                               7
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1FbeIntr_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE1CrcIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in CRC exceed threshold te event.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_Bit_Start                                           6
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_Bit_End                                             6
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_Mask                                            cBit6
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_Shift                                               6
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1CrcIntr_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE1ReiIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in REI exceed threshold te event.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_Bit_Start                                           5
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_Bit_End                                             5
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_Mask                                            cBit5
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_Shift                                               5
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1ReiIntr_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE1LomfIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in LOMF te event.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_Bit_Start                                          4
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_Bit_End                                            4
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_Mask                                           cBit4
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_Shift                                              4
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_MaxVal                                           0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_MinVal                                           0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LomfIntr_RstVal                                           0x0

/*--------------------------------------
BitField Name: DE1LosIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in LOS te event.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_Bit_Start                                           3
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_Bit_End                                             3
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_Mask                                            cBit3
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_Shift                                               3
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LosIntr_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE1AisIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in AIS te event.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_Bit_Start                                           2
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_Bit_End                                             2
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_Mask                                            cBit2
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_Shift                                               2
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1AisIntr_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE1RaiIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in RAI te event.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_Bit_Start                                           1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_Bit_End                                             1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_Mask                                            cBit1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_Shift                                               1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1RaiIntr_RstVal                                            0x0

/*--------------------------------------
BitField Name: DE1LofIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change in LOF te event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_Bit_Start                                           0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_Bit_End                                             0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_Mask                                            cBit0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_Shift                                               0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_MaxVal                                            0x1
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_MinVal                                            0x0
#define cAf6_dej1_rx_framer_per_chn_intr_stat_DE1LofIntr_RstVal                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per Channel Current Status
Reg Addr   : 0x00055080-0x000550BF
Reg Formula: 0x00055080 +  StsID*32 + VtnID
    Where  : 
           + $StsID(0-1): STS-1/VC-3 ID
           + $VtnID(0-31): VT/TU number ID in the Group
Reg Desc   : 
This is the per Channel Current tus of DS1/E1/J1 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_chn_curr_stat_Base                                               0x00055080
#define cAf6Reg_dej1_rx_framer_per_chn_curr_stat(StsID, VtnID)                 (0x00055080+(StsID)*32+(VtnID))
#define cAf6Reg_dej1_rx_framer_per_chn_curr_stat_WidthVal                                                   32
#define cAf6Reg_dej1_rx_framer_per_chn_curr_stat_WriteMask                                                 0x0

/*--------------------------------------
BitField Name: DE1BerSdIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change of BER-SD.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_Bit_Start                                        13
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_Bit_End                                          13
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_Mask                                         cBit13
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_Shift                                            13
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_MaxVal                                          0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_MinVal                                          0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSdIntr_RstVal                                          0x0

/*--------------------------------------
BitField Name: DE1BerSfIntr
BitField Type: RW
BitField Desc: Set 1 if there is a change of BER-SF.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_Bit_Start                                        12
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_Bit_End                                          12
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_Mask                                         cBit12
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_Shift                                            12
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_MaxVal                                          0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_MinVal                                          0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1BerSfIntr_RstVal                                          0x0

/*--------------------------------------
BitField Name: DE1oblcIntrSta
BitField Type: RW
BitField Desc: Current status the new Outband LoopCode detected to generate an
interrupt.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_Bit_Start                                      11
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_Bit_End                                        11
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_Mask                                       cBit11
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_Shift                                          11
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_MaxVal                                        0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_MinVal                                        0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1oblcIntrSta_RstVal                                        0x0

/*--------------------------------------
BitField Name: DE1iblcIntrSta
BitField Type: RW
BitField Desc: Current status the new Inband LoopCode detected to generate an
interrupt.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_Bit_Start                                      10
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_Bit_End                                        10
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_Mask                                       cBit10
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_Shift                                          10
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_MaxVal                                        0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_MinVal                                        0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1iblcIntrSta_RstVal                                        0x0

/*--------------------------------------
BitField Name: DE1SigRAIIntrSta
BitField Type: RW
BitField Desc: Current status Signalling RAI event to generate an interrupt.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_Bit_Start                                       9
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_Bit_End                                       9
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_Mask                                      cBit9
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_Shift                                         9
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_MaxVal                                      0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_MinVal                                      0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigRAIIntrSta_RstVal                                      0x0

/*--------------------------------------
BitField Name: DE1SigLOFIntrSta
BitField Type: RW
BitField Desc: Current status Signalling LOF event to generate an interrupt.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_Bit_Start                                       8
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_Bit_End                                       8
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_Mask                                      cBit8
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_Shift                                         8
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_MaxVal                                      0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_MinVal                                      0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1SigLOFIntrSta_RstVal                                      0x0

/*--------------------------------------
BitField Name: DE1FbeCurrSta
BitField Type: RW
BitField Desc: Current tus of FBE exceed threshold event.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_Bit_Start                                        7
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_Bit_End                                          7
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_Mask                                         cBit7
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_Shift                                            7
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1FbeCurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1CrcCurrSta
BitField Type: RW
BitField Desc: Current tus of CRC exceed threshold event.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_Bit_Start                                        6
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_Bit_End                                          6
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_Mask                                         cBit6
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_Shift                                            6
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1CrcCurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1ReiCurrSta
BitField Type: RW
BitField Desc: Current tus of REI exceed threshold event.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_Bit_Start                                        5
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_Bit_End                                          5
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_Mask                                         cBit5
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_Shift                                            5
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1ReiCurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1LomfCurrSta
BitField Type: RW
BitField Desc: Current tus of LOMF event.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_Bit_Start                                       4
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_Bit_End                                         4
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_Mask                                        cBit4
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_Shift                                           4
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_MaxVal                                        0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_MinVal                                        0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LomfCurrSta_RstVal                                        0x0

/*--------------------------------------
BitField Name: DE1LosCurrSta
BitField Type: RW
BitField Desc: Current tus of LOS event.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_Bit_Start                                        3
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_Bit_End                                          3
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_Mask                                         cBit3
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_Shift                                            3
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LosCurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1AisCurrSta
BitField Type: RW
BitField Desc: Current tus of AIS event.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_Bit_Start                                        2
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_Bit_End                                          2
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_Mask                                         cBit2
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_Shift                                            2
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1AisCurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1RaiCurrSta
BitField Type: RW
BitField Desc: Current tus of RAI event.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_Bit_Start                                        1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_Bit_End                                          1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_Mask                                         cBit1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_Shift                                            1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1RaiCurrSta_RstVal                                         0x0

/*--------------------------------------
BitField Name: DE1LofCurrSta
BitField Type: RW
BitField Desc: Current tus of LOF event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_Bit_Start                                        0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_Bit_End                                          0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_Mask                                         cBit0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_Shift                                            0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_MaxVal                                         0x1
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_MinVal                                         0x0
#define cAf6_dej1_rx_framer_per_chn_curr_stat_DE1LofCurrSta_RstVal                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per Channel Interrupt OR Status
Reg Addr   : 0x000550C0-0x000550C1
Reg Formula: 0x000550C0 +  StsID
    Where  : 
           + $StsID(0-1): STS-1/VC-3 ID
Reg Desc   : 
The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat_Base                                            0x000550C0
#define cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat(StsID)                                (0x000550C0+(StsID))
#define cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat_WidthVal                                                32
#define cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat_WriteMask                                              0x0

/*--------------------------------------
BitField Name: RxDE1VtIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding DS1/E1/J1
is set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_Bit_Start                                       0
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_Bit_End                                      31
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_Mask                                cBit31_0
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_Shift                                       0
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_MaxVal                              0xffffffff
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_chn_intr_or_stat_RxDE1VtIntrOrSta_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per STS/VC Interrupt OR Status
Reg Addr   : 0x000550FF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits for 2 STS/VCs of the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related STS/VC.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_Base                                          0x000550FF
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat                                               0x000550FF
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_WidthVal                                              32
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat_WriteMask                                            0x0

/*--------------------------------------
BitField Name: RxDE1StsIntrOrSta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding STS/VC is
set and its interrupt is enabled
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_Bit_Start                                       0
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_Bit_End                                       1
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_Mask                                 cBit1_0
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_Shift                                       0
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_MaxVal                                     0x3
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_stsvc_intr_or_stat_RxDE1StsIntrOrSta_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer per STS/VC Interrupt Enable Control
Reg Addr   : 0x000550FE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 interrupt enable bits for 2 STS/VCs in the Rx DS1/E1/J1 Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_Base                                          0x000550FE
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl                                               0x000550FE
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_WidthVal                                              32
#define cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl_WriteMask                                            0x0

/*--------------------------------------
BitField Name: RxDE1StsIntrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related STS/VC to generate interrupt.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_Bit_Start                                       0
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_Bit_End                                       1
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_Mask                                 cBit1_0
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_Shift                                       0
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_MaxVal                                     0x3
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_MinVal                                     0x0
#define cAf6_dej1_rx_framer_per_stsvc_intr_en_ctrl_RxDE1StsIntrEn_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Control
Reg Addr   : 0x00094000 - 0x0009402F
Reg Formula: 0x00094000 + LiuID
    Where  : 
           + $LiuID(0-47):
Reg Desc   : 
DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_txfrm_ctrl_Base                                                                0x00094000
#define cAf6Reg_dej1_txfrm_ctrl(LiuID)                                                    (0x00094000+(LiuID))
#define cAf6Reg_dej1_txfrm_ctrl_WidthVal                                                                   128
#define cAf6Reg_dej1_txfrm_ctrl_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: unuse
BitField Type: RW
BitField Desc:
BitField Bits: [127:32]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_unuse_Bit_Start                                                                32
#define cAf6_dej1_txfrm_ctrl_unuse_Bit_End                                                                 127
#define cAf6_dej1_txfrm_ctrl_unuse_Mask_01                                                            cBit31_0
#define cAf6_dej1_txfrm_ctrl_unuse_Shift_01                                                                  0
#define cAf6_dej1_txfrm_ctrl_unuse_Mask_02                                                            cBit31_0
#define cAf6_dej1_txfrm_ctrl_unuse_Shift_02                                                                  0
#define cAf6_dej1_txfrm_ctrl_unuse_Mask_03                                                            cBit31_0
#define cAf6_dej1_txfrm_ctrl_unuse_Shift_03                                                                  0

/*--------------------------------------
BitField Name: thr_ssm2
BitField Type: RW
BitField Desc: Configured number of SSM message transmit consequent,if all bit
thr_ssm2 set, transmit permanent
BitField Bits: [31:27]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_thr_ssm2_Bit_Start                                                             27
#define cAf6_dej1_txfrm_ctrl_thr_ssm2_Bit_End                                                               31
#define cAf6_dej1_txfrm_ctrl_thr_ssm2_Mask                                                           cBit31_27
#define cAf6_dej1_txfrm_ctrl_thr_ssm2_Shift                                                                 27
#define cAf6_dej1_txfrm_ctrl_thr_ssm2_MaxVal                                                              0x1f
#define cAf6_dej1_txfrm_ctrl_thr_ssm2_MinVal                                                               0x0
#define cAf6_dej1_txfrm_ctrl_thr_ssm2_RstVal                                                               0x0

/*--------------------------------------
BitField Name: sa_loc_conf2
BitField Type: RW
BitField Desc: Configured possition of SSM bit in E1,  (bit 3/4/5/6/7) of TS0
frame 1/3/5/7
BitField Bits: [26:24]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_sa_loc_conf2_Bit_Start                                                         24
#define cAf6_dej1_txfrm_ctrl_sa_loc_conf2_Bit_End                                                           26
#define cAf6_dej1_txfrm_ctrl_sa_loc_conf2_Mask                                                       cBit26_24
#define cAf6_dej1_txfrm_ctrl_sa_loc_conf2_Shift                                                             24
#define cAf6_dej1_txfrm_ctrl_sa_loc_conf2_MaxVal                                                           0x7
#define cAf6_dej1_txfrm_ctrl_sa_loc_conf2_MinVal                                                           0x0
#define cAf6_dej1_txfrm_ctrl_sa_loc_conf2_RstVal                                                           0x3

/*--------------------------------------
BitField Name: mdl_en
BitField Type: RW
BitField Desc: enable mdl ,set 1 to enable, clr 0 to disable
BitField Bits: [23]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_mdl_en_Bit_Start                                                               23
#define cAf6_dej1_txfrm_ctrl_mdl_en_Bit_End                                                                 23
#define cAf6_dej1_txfrm_ctrl_mdl_en_Mask                                                                cBit23
#define cAf6_dej1_txfrm_ctrl_mdl_en_Shift                                                                   23
#define cAf6_dej1_txfrm_ctrl_mdl_en_MaxVal                                                                 0x1
#define cAf6_dej1_txfrm_ctrl_mdl_en_MinVal                                                                 0x0
#define cAf6_dej1_txfrm_ctrl_mdl_en_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: TxDE1TxSaBit
BitField Type: RW
BitField Desc: Transmitted unused Sa bit
BitField Bits: [22]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1TxSaBit_Bit_Start                                                         22
#define cAf6_dej1_txfrm_ctrl_TxDE1TxSaBit_Bit_End                                                           22
#define cAf6_dej1_txfrm_ctrl_TxDE1TxSaBit_Mask                                                          cBit22
#define cAf6_dej1_txfrm_ctrl_TxDE1TxSaBit_Shift                                                             22
#define cAf6_dej1_txfrm_ctrl_TxDE1TxSaBit_MaxVal                                                           0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1TxSaBit_MinVal                                                           0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1TxSaBit_RstVal                                                           0x0

/*--------------------------------------
BitField Name: TxDE1FbitBypass
BitField Type: RW
BitField Desc: Set 1 to bypass Fbit insertion in Tx DS1/E1 Framer
BitField Bits: [21]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1FbitBypass_Bit_Start                                                      21
#define cAf6_dej1_txfrm_ctrl_TxDE1FbitBypass_Bit_End                                                        21
#define cAf6_dej1_txfrm_ctrl_TxDE1FbitBypass_Mask                                                       cBit21
#define cAf6_dej1_txfrm_ctrl_TxDE1FbitBypass_Shift                                                          21
#define cAf6_dej1_txfrm_ctrl_TxDE1FbitBypass_MaxVal                                                        0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1FbitBypass_MinVal                                                        0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1FbitBypass_RstVal                                                        0x0

/*--------------------------------------
BitField Name: TxDE1LineAisIns
BitField Type: RW
BitField Desc: Set 1 to enable Line AIS Insert
BitField Bits: [20]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1LineAisIns_Bit_Start                                                      20
#define cAf6_dej1_txfrm_ctrl_TxDE1LineAisIns_Bit_End                                                        20
#define cAf6_dej1_txfrm_ctrl_TxDE1LineAisIns_Mask                                                       cBit20
#define cAf6_dej1_txfrm_ctrl_TxDE1LineAisIns_Shift                                                          20
#define cAf6_dej1_txfrm_ctrl_TxDE1LineAisIns_MaxVal                                                        0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1LineAisIns_MinVal                                                        0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1LineAisIns_RstVal                                                        0x0

/*--------------------------------------
BitField Name: TxDE1PayAisIns
BitField Type: RW
BitField Desc: Set 1 to enable Payload AIS Insert
BitField Bits: [19]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1PayAisIns_Bit_Start                                                       19
#define cAf6_dej1_txfrm_ctrl_TxDE1PayAisIns_Bit_End                                                         19
#define cAf6_dej1_txfrm_ctrl_TxDE1PayAisIns_Mask                                                        cBit19
#define cAf6_dej1_txfrm_ctrl_TxDE1PayAisIns_Shift                                                           19
#define cAf6_dej1_txfrm_ctrl_TxDE1PayAisIns_MaxVal                                                         0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1PayAisIns_MinVal                                                         0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1PayAisIns_RstVal                                                         0x0

/*--------------------------------------
BitField Name: TxDE1RmtLineloop
BitField Type: RW
BitField Desc: Set 1 to enable remote Line Loop back
BitField Bits: [18]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtLineloop_Bit_Start                                                     18
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtLineloop_Bit_End                                                       18
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtLineloop_Mask                                                      cBit18
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtLineloop_Shift                                                         18
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtLineloop_MaxVal                                                       0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtLineloop_MinVal                                                       0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtLineloop_RstVal                                                       0x0

/*--------------------------------------
BitField Name: TxDE1RmtPayloop
BitField Type: RW
BitField Desc: Set 1 to enable remote Payload Loop back
BitField Bits: [17]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtPayloop_Bit_Start                                                      17
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtPayloop_Bit_End                                                        17
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtPayloop_Mask                                                       cBit17
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtPayloop_Shift                                                          17
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtPayloop_MaxVal                                                        0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtPayloop_MinVal                                                        0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1RmtPayloop_RstVal                                                        0x0

/*--------------------------------------
BitField Name: TxDE1AutoAis
BitField Type: RW
BitField Desc: Set 1 to enable AIS indication from data map block to
automatically transmit all         1s at DS1/E1/J1 Tx framer
BitField Bits: [16]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoAis_Bit_Start                                                         16
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoAis_Bit_End                                                           16
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoAis_Mask                                                          cBit16
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoAis_Shift                                                             16
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoAis_MaxVal                                                           0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoAis_MinVal                                                           0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoAis_RstVal                                                           0x0

/*--------------------------------------
BitField Name: TxDE1DlkBypass
BitField Type: RW
BitField Desc: Set 1 to bypass FDL insertion
BitField Bits: [15]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkBypass_Bit_Start                                                       15
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkBypass_Bit_End                                                         15
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkBypass_Mask                                                        cBit15
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkBypass_Shift                                                           15
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkBypass_MaxVal                                                         0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkBypass_MinVal                                                         0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkBypass_RstVal                                                         0x0

/*--------------------------------------
BitField Name: TxDE1DlkSwap
BitField Type: RW
BitField Desc: Set 1 ti Swap FDL transmit bit
BitField Bits: [14]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkSwap_Bit_Start                                                         14
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkSwap_Bit_End                                                           14
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkSwap_Mask                                                          cBit14
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkSwap_Shift                                                             14
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkSwap_MaxVal                                                           0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkSwap_MinVal                                                           0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1DlkSwap_RstVal                                                           0x0

/*--------------------------------------
BitField Name: TxDE1DLCfg
BitField Type: RW
BitField Desc: Configured for transmit E1 SSM message : bit[13] set: enable,
bit[12:9] Value of SSM.
BitField Bits: [13:9]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1DLCfg_Bit_Start                                                            9
#define cAf6_dej1_txfrm_ctrl_TxDE1DLCfg_Bit_End                                                             13
#define cAf6_dej1_txfrm_ctrl_TxDE1DLCfg_Mask                                                          cBit13_9
#define cAf6_dej1_txfrm_ctrl_TxDE1DLCfg_Shift                                                                9
#define cAf6_dej1_txfrm_ctrl_TxDE1DLCfg_MaxVal                                                            0x1f
#define cAf6_dej1_txfrm_ctrl_TxDE1DLCfg_MinVal                                                             0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1DLCfg_RstVal                                                             0x0

/*--------------------------------------
BitField Name: TxDE1AutoYel
BitField Type: RW
BitField Desc: Auto Yellow generation enable 1: Yellow alarm detected from Rx
Framer will be automatically transmitted in Tx Framer 0: No automatically Yellow
alarm transmitted
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoYel_Bit_Start                                                          8
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoYel_Bit_End                                                            8
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoYel_Mask                                                           cBit8
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoYel_Shift                                                              8
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoYel_MaxVal                                                           0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoYel_MinVal                                                           0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoYel_RstVal                                                           0x0

/*--------------------------------------
BitField Name: TxDE1FrcYel
BitField Type: RW
BitField Desc: SW force to Tx Yellow alarm
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcYel_Bit_Start                                                           7
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcYel_Bit_End                                                             7
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcYel_Mask                                                            cBit7
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcYel_Shift                                                               7
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcYel_MaxVal                                                            0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcYel_MinVal                                                            0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcYel_RstVal                                                            0x0

/*--------------------------------------
BitField Name: TxDE1AutoCrcErr
BitField Type: RW
BitField Desc: Auto CRC error enable 1: CRC Error detected from Rx Framer will
be automatically transmitted in REI bit of Tx Framer 0: No automatically CRC
Error alarm transmitted
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoCrcErr_Bit_Start                                                       6
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoCrcErr_Bit_End                                                         6
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoCrcErr_Mask                                                        cBit6
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoCrcErr_Shift                                                           6
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoCrcErr_MaxVal                                                        0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoCrcErr_MinVal                                                        0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1AutoCrcErr_RstVal                                                        0x0

/*--------------------------------------
BitField Name: TxDE1FrcCrcErr
BitField Type: RW
BitField Desc: SW force to Tx CRC Error alarm (REI bit)
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcCrcErr_Bit_Start                                                        5
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcCrcErr_Bit_End                                                          5
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcCrcErr_Mask                                                         cBit5
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcCrcErr_Shift                                                            5
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcCrcErr_MaxVal                                                         0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcCrcErr_MinVal                                                         0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1FrcCrcErr_RstVal                                                         0x0

/*--------------------------------------
BitField Name: TxDE1En
BitField Type: RW
BitField Desc: Set 1 to enable the DS1/E1/J1 framer.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_TxDE1En_Bit_Start                                                               4
#define cAf6_dej1_txfrm_ctrl_TxDE1En_Bit_End                                                                 4
#define cAf6_dej1_txfrm_ctrl_TxDE1En_Mask                                                                cBit4
#define cAf6_dej1_txfrm_ctrl_TxDE1En_Shift                                                                   4
#define cAf6_dej1_txfrm_ctrl_TxDE1En_MaxVal                                                                0x1
#define cAf6_dej1_txfrm_ctrl_TxDE1En_MinVal                                                                0x0
#define cAf6_dej1_txfrm_ctrl_TxDE1En_RstVal                                                                0x0

/*--------------------------------------
BitField Name: RxDE1Md
BitField Type: RW
BitField Desc: Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF
(D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1
Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dej1_txfrm_ctrl_RxDE1Md_Bit_Start                                                               0
#define cAf6_dej1_txfrm_ctrl_RxDE1Md_Bit_End                                                                 3
#define cAf6_dej1_txfrm_ctrl_RxDE1Md_Mask                                                              cBit3_0
#define cAf6_dej1_txfrm_ctrl_RxDE1Md_Shift                                                                   0
#define cAf6_dej1_txfrm_ctrl_RxDE1Md_MaxVal                                                                0xf
#define cAf6_dej1_txfrm_ctrl_RxDE1Md_MinVal                                                                0x0
#define cAf6_dej1_txfrm_ctrl_RxDE1Md_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Status
Reg Addr   : 0x00094400 - 0x0009442F
Reg Formula: 0x00094400 + LiuID
    Where  : 
           + $LiuID(0-47):
Reg Desc   : 
These registers are used for Hardware status only

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_txfrm_stat_Base                                                                0x00094400
#define cAf6Reg_dej1_txfrm_stat(LiuID)                                                    (0x00094400+(LiuID))
#define cAf6Reg_dej1_txfrm_stat_WidthVal                                                                    32
#define cAf6Reg_dej1_txfrm_stat_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: RxDE1Sta
BitField Type: RW
BitField Desc:
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_dej1_txfrm_stat_RxDE1Sta_Bit_Start                                                              0
#define cAf6_dej1_txfrm_stat_RxDE1Sta_Bit_End                                                                2
#define cAf6_dej1_txfrm_stat_RxDE1Sta_Mask                                                             cBit2_0
#define cAf6_dej1_txfrm_stat_RxDE1Sta_Shift                                                                  0
#define cAf6_dej1_txfrm_stat_RxDE1Sta_MaxVal                                                               0x7
#define cAf6_dej1_txfrm_stat_RxDE1Sta_MinVal                                                               0x0
#define cAf6_dej1_txfrm_stat_RxDE1Sta_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling Insertion Control
Reg Addr   : 0x00094800 - 0x0009482F
Reg Formula: 0x00094800 + LiuID
    Where  : 
           + $LiuID(0-47):
Reg Desc   : 
DS1/E1/J1 Rx framer signaling insertion control is used to configure for signaling operation modes at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_txfrm_sign_insert_ctrl_Base                                                    0x00094800
#define cAf6Reg_dej1_txfrm_sign_insert_ctrl(LiuID)                                        (0x00094800+(LiuID))
#define cAf6Reg_dej1_txfrm_sign_insert_ctrl_WidthVal                                                        32
#define cAf6Reg_dej1_txfrm_sign_insert_ctrl_WriteMask                                                      0x0

/*--------------------------------------
BitField Name: TxDE1SigMfrmEn
BitField Type: RW
BitField Desc: Set 1 to enable the Tx DS1/E1/J1 framer to sync the signaling
multiframe to      the incoming data flow. No applicable for DS1
BitField Bits: [30]
--------------------------------------*/
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigMfrmEn_Bit_Start                                           30
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigMfrmEn_Bit_End                                             30
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigMfrmEn_Mask                                            cBit30
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigMfrmEn_Shift                                               30
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigMfrmEn_MaxVal                                             0x1
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigMfrmEn_MinVal                                             0x0
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigMfrmEn_RstVal                                             0x0

/*--------------------------------------
BitField Name: TxDE1SigBypass
BitField Type: RW
BitField Desc: 30 signaling bypass bit for 32 DS0 in an E1 frame. In DS1 mode,
only     24 LSB bits is used. Set 1 to bypass Signaling insertion in Tx DS1/E1
Framer
BitField Bits: [29:0]
--------------------------------------*/
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigBypass_Bit_Start                                            0
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigBypass_Bit_End                                             29
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigBypass_Mask                                          cBit29_0
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigBypass_Shift                                                0
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigBypass_MaxVal                                      0x3fffffff
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigBypass_MinVal                                             0x0
#define cAf6_dej1_txfrm_sign_insert_ctrl_TxDE1SigBypass_RstVal                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling ID Conversion Control
Reg Addr   : 0x000C2000 - 0x000C207F
Reg Formula: 0x000C2000 + pwid
    Where  : 
           + $pwid(0-128):
Reg Desc   : 
DS1/E1/J1 Rx framer signaling ID conversion control is used to convert the PW ID to DS1/E1/J1 Line ID at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_txfrm_sign_id_conv_ctrl_Base                                                   0x000C2000
#define cAf6Reg_dej1_txfrm_sign_id_conv_ctrl(pwid)                                         (0x000C2000+(pwid))
#define cAf6Reg_dej1_txfrm_sign_id_conv_ctrl_WidthVal                                                       64
#define cAf6Reg_dej1_txfrm_sign_id_conv_ctrl_WriteMask                                                     0x0

/*--------------------------------------
BitField Name: TxDE1SigDS1
BitField Type: RW
BitField Desc: Set 1 if the PW carry DS0 for the DS1/J1, set 0 for E1
BitField Bits: [41]
--------------------------------------*/
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigDS1_Bit_Start                                             41
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigDS1_Bit_End                                               41
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigDS1_Mask                                               cBit9
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigDS1_Shift                                                  9
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigDS1_MaxVal                                               0x0
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigDS1_MinVal                                               0x0
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigDS1_RstVal                                               0x0

/*--------------------------------------
BitField Name: TxDE1SigLineID
BitField Type: RW
BitField Desc: Output DS1/E1/J1 Line ID of the conversion.
BitField Bits: [40:32]
--------------------------------------*/
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigLineID_Bit_Start                                          32
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigLineID_Bit_End                                            40
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigLineID_Mask                                          cBit8_0
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigLineID_Shift                                               0
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigLineID_MaxVal                                            0x0
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigLineID_MinVal                                            0x0
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigLineID_RstVal                                            0x0

/*--------------------------------------
BitField Name: TxDE1SigEnb
BitField Type: RW
BitField Desc: 32 signaling enable bit for 32 DS0 in an E1 frame. In DS1 mode,
only 24 LSB bits is used.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigEnb_Bit_Start                                              0
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigEnb_Bit_End                                               31
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigEnb_Mask                                            cBit31_0
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigEnb_Shift                                                  0
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigEnb_MaxVal                                        0xffffffff
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigEnb_MinVal                                               0x0
#define cAf6_dej1_txfrm_sign_id_conv_ctrl_TxDE1SigEnb_RstVal                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling CPU Insert Enable Control
Reg Addr   : 0x000C8001 - 0x000C8001
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the CPU signaling insert global enable bit

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_txfrm_sign_cpu_insert_en_ctrl_Base                                             0x000C8001
#define cAf6Reg_dej1_txfrm_sign_cpu_insert_en_ctrl                                                  0x000C8001
#define cAf6Reg_dej1_txfrm_sign_cpu_insert_en_ctrl_WidthVal                                                 32
#define cAf6Reg_dej1_txfrm_sign_cpu_insert_en_ctrl_WriteMask                                               0x0

/*--------------------------------------
BitField Name: TxDE1SigCPUMd
BitField Type: RW
BitField Desc: Set 1 to enable CPU mode (all signaling is from CPU)
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_txfrm_sign_cpu_insert_en_ctrl_TxDE1SigCPUMd_Bit_Start                                       0
#define cAf6_dej1_txfrm_sign_cpu_insert_en_ctrl_TxDE1SigCPUMd_Bit_End                                        0
#define cAf6_dej1_txfrm_sign_cpu_insert_en_ctrl_TxDE1SigCPUMd_Mask                                       cBit0
#define cAf6_dej1_txfrm_sign_cpu_insert_en_ctrl_TxDE1SigCPUMd_Shift                                          0
#define cAf6_dej1_txfrm_sign_cpu_insert_en_ctrl_TxDE1SigCPUMd_MaxVal                                       0x1
#define cAf6_dej1_txfrm_sign_cpu_insert_en_ctrl_TxDE1SigCPUMd_MinVal                                       0x0
#define cAf6_dej1_txfrm_sign_cpu_insert_en_ctrl_TxDE1SigCPUMd_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling CPU DS1/E1 signaling Write Control
Reg Addr   : 0x000C8002 - 0x000C8002
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the CPU signaling write control

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_txfrm_sign_cpu_de1_sign_wr_ctrl_Base                                           0x000C8002
#define cAf6Reg_dej1_txfrm_sign_cpu_de1_sign_wr_ctrl                                                0x000C8002
#define cAf6Reg_dej1_txfrm_sign_cpu_de1_sign_wr_ctrl_WidthVal                                               32
#define cAf6Reg_dej1_txfrm_sign_cpu_de1_sign_wr_ctrl_WriteMask                                             0x0

/*--------------------------------------
BitField Name: TxDE1SigCPUWrCtrl
BitField Type: RW
BitField Desc: Set 1 if signaling value written to the Signaling buffer in next
CPU cylce       is for DS1/J1, set 0 for E1
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dej1_txfrm_sign_cpu_de1_sign_wr_ctrl_TxDE1SigCPUWrCtrl_Bit_Start                                       0
#define cAf6_dej1_txfrm_sign_cpu_de1_sign_wr_ctrl_TxDE1SigCPUWrCtrl_Bit_End                                       0
#define cAf6_dej1_txfrm_sign_cpu_de1_sign_wr_ctrl_TxDE1SigCPUWrCtrl_Mask                                   cBit0
#define cAf6_dej1_txfrm_sign_cpu_de1_sign_wr_ctrl_TxDE1SigCPUWrCtrl_Shift                                       0
#define cAf6_dej1_txfrm_sign_cpu_de1_sign_wr_ctrl_TxDE1SigCPUWrCtrl_MaxVal                                     0x1
#define cAf6_dej1_txfrm_sign_cpu_de1_sign_wr_ctrl_TxDE1SigCPUWrCtrl_MinVal                                     0x0
#define cAf6_dej1_txfrm_sign_cpu_de1_sign_wr_ctrl_TxDE1SigCPUWrCtrl_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling Buffer
Reg Addr   : 0x000CC000 - 0x000CC7FF
Reg Formula: 0x000CC000 + 32*LiuID + tscnt
    Where  : 
           + $LiuID(0-47):
           + $tscnt(0-31): 0 - 23 (ds1) or 0 - 31 (e1)
Reg Desc   : 
DS1/E1/J1 Rx framer signaling ABCD bit store for each DS0 at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_txfrm_sign_buf_Base                                                            0x000CC000
#define cAf6Reg_dej1_txfrm_sign_buf(LiuID, tscnt)                              (0x000CC000+32*(LiuID)+(tscnt))
#define cAf6Reg_dej1_txfrm_sign_buf_WidthVal                                                                32
#define cAf6Reg_dej1_txfrm_sign_buf_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: TxDE1SigABCD
BitField Type: RW
BitField Desc: 4-bit ABCD signaling for each DS0 (Note: for DS1/J1, must write
the      0x000C8002 value 1 before writing this ABCD, for E1  must write the
0x000C8002 value 0  before writing this ABCD)
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dej1_txfrm_sign_buf_TxDE1SigABCD_Bit_Start                                                      0
#define cAf6_dej1_txfrm_sign_buf_TxDE1SigABCD_Bit_End                                                        3
#define cAf6_dej1_txfrm_sign_buf_TxDE1SigABCD_Mask                                                     cBit3_0
#define cAf6_dej1_txfrm_sign_buf_TxDE1SigABCD_Shift                                                          0
#define cAf6_dej1_txfrm_sign_buf_TxDE1SigABCD_MaxVal                                                       0xf
#define cAf6_dej1_txfrm_sign_buf_TxDE1SigABCD_MinVal                                                       0x0
#define cAf6_dej1_txfrm_sign_buf_TxDE1SigABCD_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Register Control Indirect Access
Reg Addr   : 0x90008
Reg Formula: 
    Where  : 
Reg Desc   : 
Use to enable one Liu id which want to config indirect for FDL insert

------------------------------------------------------------------------------*/
#define cAf6Reg_indctrl_pen_Base                                                                       0x90008
#define cAf6Reg_indctrl_pen                                                                            0x90008
#define cAf6Reg_indctrl_pen_WidthVal                                                                        32
#define cAf6Reg_indctrl_pen_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: unuse
BitField Type: RW
BitField Desc: unuse
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_indctrl_pen_unuse_Bit_Start                                                                    16
#define cAf6_indctrl_pen_unuse_Bit_End                                                                      31
#define cAf6_indctrl_pen_unuse_Mask                                                                  cBit31_16
#define cAf6_indctrl_pen_unuse_Shift                                                                        16
#define cAf6_indctrl_pen_unuse_MaxVal                                                                   0xffff
#define cAf6_indctrl_pen_unuse_MinVal                                                                      0x0
#define cAf6_indctrl_pen_unuse_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: pinden
BitField Type: RW
BitField Desc: set 1 to enable indirect access
BitField Bits: [15]
--------------------------------------*/
#define cAf6_indctrl_pen_pinden_Bit_Start                                                                   15
#define cAf6_indctrl_pen_pinden_Bit_End                                                                     15
#define cAf6_indctrl_pen_pinden_Mask                                                                    cBit15
#define cAf6_indctrl_pen_pinden_Shift                                                                       15
#define cAf6_indctrl_pen_pinden_MaxVal                                                                     0x1
#define cAf6_indctrl_pen_pinden_MinVal                                                                     0x0
#define cAf6_indctrl_pen_pinden_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: dont care
BitField Type: RW
BitField Desc: unuse
BitField Bits: [14:07]
--------------------------------------*/
#define cAf6_indctrl_pen_dont_care_Bit_Start                                                                 7
#define cAf6_indctrl_pen_dont_care_Bit_End                                                                  14
#define cAf6_indctrl_pen_dont_care_Mask                                                               cBit14_7
#define cAf6_indctrl_pen_dont_care_Shift                                                                     7
#define cAf6_indctrl_pen_dont_care_MaxVal                                                                 0xff
#define cAf6_indctrl_pen_dont_care_MinVal                                                                  0x0
#define cAf6_indctrl_pen_dont_care_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: indctrlpdo
BitField Type: RW
BitField Desc: bit [6] set 1 : read indirect , bit [6] clear 0 : write indicate,
bit [5:0]     : Liu id access indirect
BitField Bits: [06:00]
--------------------------------------*/
#define cAf6_indctrl_pen_indctrlpdo_Bit_Start                                                                0
#define cAf6_indctrl_pen_indctrlpdo_Bit_End                                                                  6
#define cAf6_indctrl_pen_indctrlpdo_Mask                                                               cBit6_0
#define cAf6_indctrl_pen_indctrlpdo_Shift                                                                    0
#define cAf6_indctrl_pen_indctrlpdo_MaxVal                                                                0x7f
#define cAf6_indctrl_pen_indctrlpdo_MinVal                                                                 0x0
#define cAf6_indctrl_pen_indctrlpdo_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Info Cpu write Indirect Access
Reg Addr   : 0x90009
Reg Formula: 
    Where  : 
Reg Desc   : 
Use to write FDL config

------------------------------------------------------------------------------*/
#define cAf6Reg_indwr_pen_Base                                                                         0x90009
#define cAf6Reg_indwr_pen                                                                              0x90009
#define cAf6Reg_indwr_pen_WidthVal                                                                          32
#define cAf6Reg_indwr_pen_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: unuse
BitField Type: RW
BitField Desc: unuse
BitField Bits: [31:13]
--------------------------------------*/
#define cAf6_indwr_pen_unuse_Bit_Start                                                                      13
#define cAf6_indwr_pen_unuse_Bit_End                                                                        31
#define cAf6_indwr_pen_unuse_Mask                                                                    cBit31_13
#define cAf6_indwr_pen_unuse_Shift                                                                          13
#define cAf6_indwr_pen_unuse_MaxVal                                                                    0x7ffff
#define cAf6_indwr_pen_unuse_MinVal                                                                        0x0
#define cAf6_indwr_pen_unuse_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: idle_gap
BitField Type: RW
BitField Desc: number idle pattern beetwen 2 fdl message
BitField Bits: [12:11]
--------------------------------------*/
#define cAf6_indwr_pen_idle_gap_Bit_Start                                                                   11
#define cAf6_indwr_pen_idle_gap_Bit_End                                                                     12
#define cAf6_indwr_pen_idle_gap_Mask                                                                 cBit12_11
#define cAf6_indwr_pen_idle_gap_Shift                                                                       11
#define cAf6_indwr_pen_idle_gap_MaxVal                                                                     0x3
#define cAf6_indwr_pen_idle_gap_MinVal                                                                     0x0
#define cAf6_indwr_pen_idle_gap_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: thr_rep
BitField Type: RW
BitField Desc: Times fdl message is repeated
BitField Bits: [10:07]
--------------------------------------*/
#define cAf6_indwr_pen_thr_rep_Bit_Start                                                                     7
#define cAf6_indwr_pen_thr_rep_Bit_End                                                                      10
#define cAf6_indwr_pen_thr_rep_Mask                                                                   cBit10_7
#define cAf6_indwr_pen_thr_rep_Shift                                                                         7
#define cAf6_indwr_pen_thr_rep_MaxVal                                                                      0xf
#define cAf6_indwr_pen_thr_rep_MinVal                                                                      0x0
#define cAf6_indwr_pen_thr_rep_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: force_bom
BitField Type: RW
BitField Desc: cpu create rising edge trigger to gen bom message
BitField Bits: [06]
--------------------------------------*/
#define cAf6_indwr_pen_force_bom_Bit_Start                                                                   6
#define cAf6_indwr_pen_force_bom_Bit_End                                                                     6
#define cAf6_indwr_pen_force_bom_Mask                                                                    cBit6
#define cAf6_indwr_pen_force_bom_Shift                                                                       6
#define cAf6_indwr_pen_force_bom_MaxVal                                                                    0x1
#define cAf6_indwr_pen_force_bom_MinVal                                                                    0x0
#define cAf6_indwr_pen_force_bom_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: patt_bom2
BitField Type: RW
BitField Desc: 6bit x of pattern  bom message in format is 0xxxxxx0
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_indwr_pen_patt_bom2_Bit_Start                                                                   0
#define cAf6_indwr_pen_patt_bom2_Bit_End                                                                     5
#define cAf6_indwr_pen_patt_bom2_Mask                                                                  cBit5_0
#define cAf6_indwr_pen_patt_bom2_Shift                                                                       0
#define cAf6_indwr_pen_patt_bom2_MaxVal                                                                   0x3f
#define cAf6_indwr_pen_patt_bom2_MinVal                                                                    0x0
#define cAf6_indwr_pen_patt_bom2_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Info Cpu write Indirect Access
Reg Addr   : 0x9000A
Reg Formula: 
    Where  : 
Reg Desc   : 
Use to check config FDL

------------------------------------------------------------------------------*/
#define cAf6Reg_indrd_pen_Base                                                                         0x9000A
#define cAf6Reg_indrd_pen                                                                              0x9000A
#define cAf6Reg_indrd_pen_WidthVal                                                                          32
#define cAf6Reg_indrd_pen_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: unuse
BitField Type: RW
BitField Desc: unuse
BitField Bits: [31:13]
--------------------------------------*/
#define cAf6_indrd_pen_unuse_Bit_Start                                                                      13
#define cAf6_indrd_pen_unuse_Bit_End                                                                        31
#define cAf6_indrd_pen_unuse_Mask                                                                    cBit31_13
#define cAf6_indrd_pen_unuse_Shift                                                                          13
#define cAf6_indrd_pen_unuse_MaxVal                                                                    0x7ffff
#define cAf6_indrd_pen_unuse_MinVal                                                                        0x0
#define cAf6_indrd_pen_unuse_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: indrdpdo
BitField Type: RW
BitField Desc: read to check config fdl indirect
BitField Bits: [12:00]
--------------------------------------*/
#define cAf6_indrd_pen_indrdpdo_Bit_Start                                                                    0
#define cAf6_indrd_pen_indrdpdo_Bit_End                                                                     12
#define cAf6_indrd_pen_indrdpdo_Mask                                                                  cBit12_0
#define cAf6_indrd_pen_indrdpdo_Shift                                                                        0
#define cAf6_indrd_pen_indrdpdo_MaxVal                                                                  0x1fff
#define cAf6_indrd_pen_indrdpdo_MinVal                                                                     0x0
#define cAf6_indrd_pen_indrdpdo_RstVal                                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Sticky Insert done liu 0 to 31
Reg Addr   : 0x9000B
Reg Formula: 
    Where  : 
Reg Desc   : 
Use to indicate insert done

------------------------------------------------------------------------------*/
#define cAf6Reg_stkgen031_pen_Base                                                                     0x9000B
#define cAf6Reg_stkgen031_pen                                                                          0x9000B
#define cAf6Reg_stkgen031_pen_WidthVal                                                                      32
#define cAf6Reg_stkgen031_pen_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: gendone0_31
BitField Type: WC
BitField Desc: sticky gen done for liu0 to liu 31
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_stkgen031_pen_gendone0_31_Bit_Start                                                             0
#define cAf6_stkgen031_pen_gendone0_31_Bit_End                                                              31
#define cAf6_stkgen031_pen_gendone0_31_Mask                                                           cBit31_0
#define cAf6_stkgen031_pen_gendone0_31_Shift                                                                 0
#define cAf6_stkgen031_pen_gendone0_31_MaxVal                                                       0xffffffff
#define cAf6_stkgen031_pen_gendone0_31_MinVal                                                              0x0
#define cAf6_stkgen031_pen_gendone0_31_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Sticky Insert done liu 31 to 47
Reg Addr   : 0x9000C
Reg Formula: 
    Where  : 
Reg Desc   : 
Use to indicate insert done

------------------------------------------------------------------------------*/
#define cAf6Reg_stkgen3247_pen_Base                                                                    0x9000C
#define cAf6Reg_stkgen3247_pen                                                                         0x9000C
#define cAf6Reg_stkgen3247_pen_WidthVal                                                                     32
#define cAf6Reg_stkgen3247_pen_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: gendone32_47
BitField Type: WC
BitField Desc: sticky gen done for liu32 to liu 47
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_stkgen3247_pen_gendone32_47_Bit_Start                                                           0
#define cAf6_stkgen3247_pen_gendone32_47_Bit_End                                                            31
#define cAf6_stkgen3247_pen_gendone32_47_Mask                                                         cBit31_0
#define cAf6_stkgen3247_pen_gendone32_47_Shift                                                               0
#define cAf6_stkgen3247_pen_gendone32_47_MaxVal                                                     0xffffffff
#define cAf6_stkgen3247_pen_gendone32_47_MinVal                                                            0x0
#define cAf6_stkgen3247_pen_gendone32_47_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Sticky Indicate change SSM messsage of ID0-ID31
Reg Addr   : 0x0_50006
Reg Formula: 
    Where  : 
Reg Desc   : 
Use to indicate SSM message of ID0-ID31 change

------------------------------------------------------------------------------*/
#define cAf6Reg_stkrxssmch1_Base                                                                      0x050006
#define cAf6Reg_stkrxssmch1                                                                           0x050006
#define cAf6Reg_stkrxssmch1_WidthVal                                                                        32
#define cAf6Reg_stkrxssmch1_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: stk_ssm0031_pen
BitField Type: WC
BitField Desc: sticky change for liu0 to liu 31
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_stkrxssmch1_stk_ssm0031_pen_Bit_Start                                                           0
#define cAf6_stkrxssmch1_stk_ssm0031_pen_Bit_End                                                            31
#define cAf6_stkrxssmch1_stk_ssm0031_pen_Mask                                                         cBit31_0
#define cAf6_stkrxssmch1_stk_ssm0031_pen_Shift                                                               0
#define cAf6_stkrxssmch1_stk_ssm0031_pen_MaxVal                                                     0xffffffff
#define cAf6_stkrxssmch1_stk_ssm0031_pen_MinVal                                                            0x0
#define cAf6_stkrxssmch1_stk_ssm0031_pen_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Sticky Indicate change SSM messsage of ID32-ID47
Reg Addr   : 0x0_50007
Reg Formula: 
    Where  : 
Reg Desc   : 
Use to indicate SSM message of ID32-ID47 change

------------------------------------------------------------------------------*/
#define cAf6Reg_stkrxssmch2_Base                                                                      0x050007
#define cAf6Reg_stkrxssmch2                                                                           0x050007
#define cAf6Reg_stkrxssmch2_WidthVal                                                                        32
#define cAf6Reg_stkrxssmch2_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: stk_ssm3247_pen
BitField Type: WC
BitField Desc: sticky change for liu32 to liu 47
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_stkrxssmch2_stk_ssm3247_pen_Bit_Start                                                           0
#define cAf6_stkrxssmch2_stk_ssm3247_pen_Bit_End                                                            31
#define cAf6_stkrxssmch2_stk_ssm3247_pen_Mask                                                         cBit31_0
#define cAf6_stkrxssmch2_stk_ssm3247_pen_Shift                                                               0
#define cAf6_stkrxssmch2_stk_ssm3247_pen_MaxVal                                                     0xffffffff
#define cAf6_stkrxssmch2_stk_ssm3247_pen_MinVal                                                            0x0
#define cAf6_stkrxssmch2_stk_ssm3247_pen_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : Current Status SSM message of E1
Reg Addr   : 0x0_54600 - 0x05462f
Reg Formula: 0x0_54600 + LiuID
    Where  : 
           + $LiuID(0-47):
Reg Desc   : 
These registers are used for Softwave decode SSM message

------------------------------------------------------------------------------*/
#define cAf6Reg_curstassme1_Base                                                                      0x054600
#define cAf6Reg_curstassme1(LiuID)                                                          (0x054600+(LiuID))
#define cAf6Reg_curstassme1_WidthVal                                                                        32
#define cAf6Reg_curstassme1_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: unuse
BitField Type: RW
BitField Desc: unuse
BitField Bits: [31:04]
--------------------------------------*/
#define cAf6_curstassme1_unuse_Bit_Start                                                                     4
#define cAf6_curstassme1_unuse_Bit_End                                                                      31
#define cAf6_curstassme1_unuse_Mask                                                                   cBit31_4
#define cAf6_curstassme1_unuse_Shift                                                                         4
#define cAf6_curstassme1_unuse_MaxVal                                                                0xfffffff
#define cAf6_curstassme1_unuse_MinVal                                                                      0x0
#define cAf6_curstassme1_unuse_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: crr_ssm_sta2
BitField Type: RO
BitField Desc: 4bit message of SSM receive
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_curstassme1_crr_ssm_sta2_Bit_Start                                                              0
#define cAf6_curstassme1_crr_ssm_sta2_Bit_End                                                                3
#define cAf6_curstassme1_crr_ssm_sta2_Mask                                                             cBit3_0
#define cAf6_curstassme1_crr_ssm_sta2_Shift                                                                  0
#define cAf6_curstassme1_crr_ssm_sta2_MaxVal                                                               0xf
#define cAf6_curstassme1_crr_ssm_sta2_MinVal                                                               0x0
#define cAf6_curstassme1_crr_ssm_sta2_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : LC test reg1
Reg Addr   : 0xE_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
For testing only

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_test1_Base                                                                        0xE0000
#define cAf6Reg_upen_test1                                                                             0xE0000
#define cAf6Reg_upen_test1_WidthVal                                                                         32
#define cAf6Reg_upen_test1_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: test_reg1
BitField Type: RW
BitField Desc: value of reg1
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_test1_test_reg1_Bit_Start                                                                  0
#define cAf6_upen_test1_test_reg1_Bit_End                                                                   31
#define cAf6_upen_test1_test_reg1_Mask                                                                cBit31_0
#define cAf6_upen_test1_test_reg1_Shift                                                                      0
#define cAf6_upen_test1_test_reg1_MaxVal                                                            0xffffffff
#define cAf6_upen_test1_test_reg1_MinVal                                                                   0x0
#define cAf6_upen_test1_test_reg1_RstVal                                                           0x1004_2015


/*------------------------------------------------------------------------------
Reg Name   : LC test reg2
Reg Addr   : 0xE_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
For testing only

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_test2_Base                                                                        0xE0001
#define cAf6Reg_upen_test2                                                                             0xE0001
#define cAf6Reg_upen_test2_WidthVal                                                                        128
#define cAf6Reg_upen_test2_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: test_reg2
BitField Type: RW
BitField Desc: value of reg2
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_test2_test_reg2_Bit_Start                                                                  0
#define cAf6_upen_test2_test_reg2_Bit_End                                                                   31
#define cAf6_upen_test2_test_reg2_Mask                                                                cBit31_0
#define cAf6_upen_test2_test_reg2_Shift                                                                      0
#define cAf6_upen_test2_test_reg2_MaxVal                                                            0xffffffff
#define cAf6_upen_test2_test_reg2_MinVal                                                                   0x0
#define cAf6_upen_test2_test_reg2_RstVal                                                                  0x00


/*------------------------------------------------------------------------------
Reg Name   : Config Limit error
Reg Addr   : 0xE_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
Check pattern error in state matching pattern

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_lim_err_Base                                                                      0xE0002
#define cAf6Reg_upen_lim_err                                                                           0xE0002
#define cAf6Reg_upen_lim_err_WidthVal                                                                       32
#define cAf6Reg_upen_lim_err_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: cfg_lim_err
BitField Type: RW
BitField Desc: if cnt err more than limit error,search failed
BitField Bits: [6:0]
--------------------------------------*/
#define cAf6_upen_lim_err_cfg_lim_err_Bit_Start                                                              0
#define cAf6_upen_lim_err_cfg_lim_err_Bit_End                                                                6
#define cAf6_upen_lim_err_cfg_lim_err_Mask                                                             cBit6_0
#define cAf6_upen_lim_err_cfg_lim_err_Shift                                                                  0
#define cAf6_upen_lim_err_cfg_lim_err_MaxVal                                                              0x7f
#define cAf6_upen_lim_err_cfg_lim_err_MinVal                                                               0x0
#define cAf6_upen_lim_err_cfg_lim_err_RstVal                                                               0xF


/*------------------------------------------------------------------------------
Reg Name   : Config limit match
Reg Addr   : 0xE_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
Used to check parttern match

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_lim_mat_Base                                                                      0xE0003
#define cAf6Reg_upen_lim_mat                                                                           0xE0003
#define cAf6Reg_upen_lim_mat_WidthVal                                                                       32
#define cAf6Reg_upen_lim_mat_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: cfg_lim_mat
BitField Type: RW
BitField Desc: if cnt match more than limit mat, serch ok
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_lim_mat_cfg_lim_mat_Bit_Start                                                              0
#define cAf6_upen_lim_mat_cfg_lim_mat_Bit_End                                                               15
#define cAf6_upen_lim_mat_cfg_lim_mat_Mask                                                            cBit15_0
#define cAf6_upen_lim_mat_cfg_lim_mat_Shift                                                                  0
#define cAf6_upen_lim_mat_cfg_lim_mat_MaxVal                                                            0xffff
#define cAf6_upen_lim_mat_cfg_lim_mat_MinVal                                                               0x0
#define cAf6_upen_lim_mat_cfg_lim_mat_RstVal                                                             0xFFE


/*------------------------------------------------------------------------------
Reg Name   : Config thresh fdl parttern
Reg Addr   : 0xE_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
Check fdl message codes match

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_th_fdl_mat_Base                                                                   0xE0004
#define cAf6Reg_upen_th_fdl_mat                                                                        0xE0004
#define cAf6Reg_upen_th_fdl_mat_WidthVal                                                                    32
#define cAf6Reg_upen_th_fdl_mat_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: cfg_th_fdl_mat
BitField Type: RW
BitField Desc: if message codes repeat more than thresh, search ok,
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_th_fdl_mat_cfg_th_fdl_mat_Bit_Start                                                        0
#define cAf6_upen_th_fdl_mat_cfg_th_fdl_mat_Bit_End                                                          7
#define cAf6_upen_th_fdl_mat_cfg_th_fdl_mat_Mask                                                       cBit7_0
#define cAf6_upen_th_fdl_mat_cfg_th_fdl_mat_Shift                                                            0
#define cAf6_upen_th_fdl_mat_cfg_th_fdl_mat_MaxVal                                                        0xff
#define cAf6_upen_th_fdl_mat_cfg_th_fdl_mat_MinVal                                                         0x0
#define cAf6_upen_th_fdl_mat_cfg_th_fdl_mat_RstVal                                                         0x5


/*------------------------------------------------------------------------------
Reg Name   : Config thersh error
Reg Addr   : 0xE_0005
Reg Formula: 
    Where  : 
Reg Desc   : 
Max error for one pattern

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_th_err_Base                                                                       0xE0005
#define cAf6Reg_upen_th_err                                                                            0xE0005
#define cAf6Reg_upen_th_err_WidthVal                                                                        32
#define cAf6Reg_upen_th_err_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: cfg_th_err
BitField Type: RW
BitField Desc: if pattern error more than thresh , change to check another
pattern
BitField Bits: [06:00]
--------------------------------------*/
#define cAf6_upen_th_err_cfg_th_err_Bit_Start                                                                0
#define cAf6_upen_th_err_cfg_th_err_Bit_End                                                                  6
#define cAf6_upen_th_err_cfg_th_err_Mask                                                               cBit6_0
#define cAf6_upen_th_err_cfg_th_err_Shift                                                                    0
#define cAf6_upen_th_err_cfg_th_err_MaxVal                                                                0x7f
#define cAf6_upen_th_err_cfg_th_err_MinVal                                                                 0x0
#define cAf6_upen_th_err_cfg_th_err_RstVal                                                                 0xF


/*------------------------------------------------------------------------------
Reg Name   : Sticky pattern detected
Reg Addr   : 0xE_13FF
Reg Formula: 
    Where  : 
Reg Desc   : 
Indicate status of fifo inband partern empty or full

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stk_Base                                                                          0xE13FF
#define cAf6Reg_upen_stk                                                                               0xE13FF
#define cAf6Reg_upen_stk_WidthVal                                                                           32
#define cAf6Reg_upen_stk_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: updo_stk
BitField Type: R2C
BitField Desc: bit[0] = 1 :empty bit[1] = 1 :full, bit[2] = 1 :interrupt
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_upen_stk_updo_stk_Bit_Start                                                                     0
#define cAf6_upen_stk_updo_stk_Bit_End                                                                       2
#define cAf6_upen_stk_updo_stk_Mask                                                                    cBit2_0
#define cAf6_upen_stk_updo_stk_Shift                                                                         0
#define cAf6_upen_stk_updo_stk_MaxVal                                                                      0x7
#define cAf6_upen_stk_updo_stk_MinVal                                                                      0x0
#define cAf6_upen_stk_updo_stk_RstVal                                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Config User programmble Pattern User Codes
Reg Addr   : 0xE_13F0
Reg Formula: 0xE_13F0 + $pat
    Where  : 
           + $pat(0-7)
Reg Desc   : 
config patter search

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pat_Base                                                                          0xE13F0
#define cAf6Reg_upen_pat(pat)                                                                  (0xE13F0+(pat))
#define cAf6Reg_upen_pat_WidthVal                                                                           32
#define cAf6Reg_upen_pat_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: updo_pat
BitField Type: RO
BitField Desc: patt codes
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_upen_pat_updo_pat_Bit_Start                                                                     0
#define cAf6_upen_pat_updo_pat_Bit_End                                                                      10
#define cAf6_upen_pat_updo_pat_Mask                                                                   cBit10_0
#define cAf6_upen_pat_updo_pat_Shift                                                                         0
#define cAf6_upen_pat_updo_pat_MaxVal                                                                    0x7ff
#define cAf6_upen_pat_updo_pat_MinVal                                                                      0x0
#define cAf6_upen_pat_updo_pat_RstVal                                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : Number of pattern
Reg Addr   : 0xE_13FE
Reg Formula: 
    Where  : 
Reg Desc   : 
Indicate number of pattern is detected

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_len_Base                                                                          0xE13FE
#define cAf6Reg_upen_len                                                                               0xE13FE
#define cAf6Reg_upen_len_WidthVal                                                                           32
#define cAf6Reg_upen_len_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: info_len_l
BitField Type: RO
BitField Desc: number patt detected
BitField Bits: [10:00]
--------------------------------------*/
#define cAf6_upen_len_info_len_l_Bit_Start                                                                   0
#define cAf6_upen_len_info_len_l_Bit_End                                                                    10
#define cAf6_upen_len_info_len_l_Mask                                                                 cBit10_0
#define cAf6_upen_len_info_len_l_Shift                                                                       0
#define cAf6_upen_len_info_len_l_MaxVal                                                                  0x7ff
#define cAf6_upen_len_info_len_l_MinVal                                                                    0x0
#define cAf6_upen_len_info_len_l_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : Cur Pattern Inband Detected
Reg Addr   : 0xE_0C00 - 0xE_0C2F
Reg Formula: 0xE_0C00 + lid
    Where  : 
           + $lid (0-47):
Reg Desc   : 
Used to report current status of id0 to id47

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_info_Base                                                                         0xE0C00
#define cAf6Reg_upen_info(lid)                                                                 (0xE0C00+(lid))
#define cAf6Reg_upen_info_WidthVal                                                                          32
#define cAf6Reg_upen_info_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: updo_sta
BitField Type: RO
BitField Desc: bit [2:0] == 0x0  CSU UP bit [2:0] == 0x1  CSU DOWN bit [2:0] ==
0x2  FAC1 UP bit [2:0] == 0x3  FAC1 DOWN bit [2:0] == 0x4  FAC2 UP bit [2:0] ==
0x5  FAC2 DOWN bit [2:0] == 0x7  idle
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_info_updo_sta_Bit_Start                                                                    0
#define cAf6_upen_info_updo_sta_Bit_End                                                                      2
#define cAf6_upen_info_updo_sta_Mask                                                                   cBit2_0
#define cAf6_upen_info_updo_sta_Shift                                                                        0
#define cAf6_upen_info_updo_sta_MaxVal                                                                     0x7
#define cAf6_upen_info_updo_sta_MinVal                                                                     0x0
#define cAf6_upen_info_updo_sta_RstVal                                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Sticky FDL message detected
Reg Addr   : 0xE_0BFF
Reg Formula: 
    Where  : 
Reg Desc   : 
Indicate status of fifo fdl message empty or full

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_fdl_stk_Base                                                                      0xE0BFF
#define cAf6Reg_upen_fdl_stk                                                                           0xE0BFF
#define cAf6Reg_upen_fdl_stk_WidthVal                                                                       32
#define cAf6Reg_upen_fdl_stk_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: updo_fdlstk
BitField Type: R2C
BitField Desc: bit[0] = 1 :empty, bit[1] = 1 :full, bit[2] = 1 :interrupt
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_upen_fdl_stk_updo_fdlstk_Bit_Start                                                              0
#define cAf6_upen_fdl_stk_updo_fdlstk_Bit_End                                                                2
#define cAf6_upen_fdl_stk_updo_fdlstk_Mask                                                             cBit2_0
#define cAf6_upen_fdl_stk_updo_fdlstk_Shift                                                                  0
#define cAf6_upen_fdl_stk_updo_fdlstk_MaxVal                                                               0x7
#define cAf6_upen_fdl_stk_updo_fdlstk_MinVal                                                               0x0
#define cAf6_upen_fdl_stk_updo_fdlstk_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Config User programmble Pattern User Codes
Reg Addr   : 0xE_0BFD
Reg Formula: 
    Where  : 
Reg Desc   : 
check fdl message codes

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_fdlpat_Base                                                                       0xE0BFD
#define cAf6Reg_upen_fdlpat                                                                            0xE0BFD
#define cAf6Reg_upen_fdlpat_WidthVal                                                                       128
#define cAf6Reg_upen_fdlpat_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: updo_fdl_mess
BitField Type: RO
BitField Desc: mess codes
BitField Bits: [55:00]
--------------------------------------*/
#define cAf6_upen_fdlpat_updo_fdl_mess_Bit_Start                                                             0
#define cAf6_upen_fdlpat_updo_fdl_mess_Bit_End                                                              55
#define cAf6_upen_fdlpat_updo_fdl_mess_Mask_01                                                        cBit31_0
#define cAf6_upen_fdlpat_updo_fdl_mess_Shift_01                                                              0
#define cAf6_upen_fdlpat_updo_fdl_mess_Mask_02                                                        cBit23_0
#define cAf6_upen_fdlpat_updo_fdl_mess_Shift_02                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Number of message
Reg Addr   : 0xE_0BFE
Reg Formula: 
    Where  : 
Reg Desc   : 
Indicate number of message codes is detected

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_len_mess_Base                                                                     0xE0BFE
#define cAf6Reg_upen_len_mess                                                                          0xE0BFE
#define cAf6Reg_upen_len_mess_WidthVal                                                                      32
#define cAf6Reg_upen_len_mess_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: info_len_l
BitField Type: RO
BitField Desc: number patt detected
BitField Bits: [10:00]
--------------------------------------*/
#define cAf6_upen_len_mess_info_len_l_Bit_Start                                                              0
#define cAf6_upen_len_mess_info_len_l_Bit_End                                                               10
#define cAf6_upen_len_mess_info_len_l_Mask                                                            cBit10_0
#define cAf6_upen_len_mess_info_len_l_Shift                                                                  0
#define cAf6_upen_len_mess_info_len_l_MaxVal                                                             0x7ff
#define cAf6_upen_len_mess_info_len_l_MinVal                                                               0x0
#define cAf6_upen_len_mess_info_len_l_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : Cur Message FDL Detected
Reg Addr   : 0xE_0400 - 0xE_042F
Reg Formula: 0xE_0400 + $fdl_id
    Where  : 
           + $fdl_id (0-47)
Reg Desc   : 
Info fdl message detected

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_fdl_info_Base                                                                     0xE0400
#define cAf6Reg_upen_fdl_info(fdlid)                                                         (0xE0400+(fdlid))
#define cAf6Reg_upen_fdl_info_WidthVal                                                                      32
#define cAf6Reg_upen_fdl_info_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: mess_info_l
BitField Type: RO
BitField Desc: message info bit[15:8] : 8bit BOM pattern format 0xxxxxx0 bit[4]
: set 1 indicate BOM message  bit[3:0]== 0x0 11111111_01111110 bit[3:0]== 0x1
line loop up bit[3:0]== 0x2 line loop down bit[3:0]== 0x3 payload loop up
bit[3:0]== 0x4 payload loop down bit[3:0]== 0x5 smartjack act bit[3:0]== 0x6
smartjact deact bit[3:0]== 0x7 idle
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_fdl_info_mess_info_l_Bit_Start                                                             0
#define cAf6_upen_fdl_info_mess_info_l_Bit_End                                                              15
#define cAf6_upen_fdl_info_mess_info_l_Mask                                                           cBit15_0
#define cAf6_upen_fdl_info_mess_info_l_Shift                                                                 0
#define cAf6_upen_fdl_info_mess_info_l_MaxVal                                                           0xffff
#define cAf6_upen_fdl_info_mess_info_l_MinVal                                                              0x0
#define cAf6_upen_fdl_info_mess_info_l_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Config Theshold Pattern Report
Reg Addr   : 0xE_0006
Reg Formula: 
    Where  : 
Reg Desc   : 
Maximum Pattern Report

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_th_stt_int_Base                                                                   0xE0006
#define cAf6Reg_upen_th_stt_int                                                                        0xE0006
#define cAf6Reg_upen_th_stt_int_WidthVal                                                                    32
#define cAf6Reg_upen_th_stt_int_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: updo_th_stt_int
BitField Type: RW
BitField Desc: if number of pattern detected more than threshold, interrupt
enable
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_th_stt_int_updo_th_stt_int_Bit_Start                                                       0
#define cAf6_upen_th_stt_int_updo_th_stt_int_Bit_End                                                        31
#define cAf6_upen_th_stt_int_updo_th_stt_int_Mask                                                     cBit31_0
#define cAf6_upen_th_stt_int_updo_th_stt_int_Shift                                                           0
#define cAf6_upen_th_stt_int_updo_th_stt_int_MaxVal                                                 0xffffffff
#define cAf6_upen_th_stt_int_updo_th_stt_int_MinVal                                                        0x0
#define cAf6_upen_th_stt_int_updo_th_stt_int_RstVal                                                       0xFF


/*------------------------------------------------------------------------------
Reg Name   : Sticky Loopcode detected id0 to id31
Reg Addr   : 0xE_13FA
Reg Formula: 
    Where  : 
Reg Desc   : 
Indicate loopcode is detected

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stk_rs031_Base                                                                    0xE13FA
#define cAf6Reg_upen_stk_rs031                                                                         0xE13FA
#define cAf6Reg_upen_stk_rs031_WidthVal                                                                     32
#define cAf6Reg_upen_stk_rs031_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: updo_stk_rs031
BitField Type: RW
BitField Desc: bit map sticky of id0,...,id31
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_stk_rs031_updo_stk_rs031_Bit_Start                                                         0
#define cAf6_upen_stk_rs031_updo_stk_rs031_Bit_End                                                          31
#define cAf6_upen_stk_rs031_updo_stk_rs031_Mask                                                       cBit31_0
#define cAf6_upen_stk_rs031_updo_stk_rs031_Shift                                                             0
#define cAf6_upen_stk_rs031_updo_stk_rs031_MaxVal                                                   0xffffffff
#define cAf6_upen_stk_rs031_updo_stk_rs031_MinVal                                                          0x0
#define cAf6_upen_stk_rs031_updo_stk_rs031_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Sticky Loopcode detected id32 to id47
Reg Addr   : 0xE_13F9
Reg Formula: 
    Where  : 
Reg Desc   : 
Indicate loopcode is detected

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stk_rs3247_Base                                                                   0xE13F9
#define cAf6Reg_upen_stk_rs3247                                                                        0xE13F9
#define cAf6Reg_upen_stk_rs3247_WidthVal                                                                    32
#define cAf6Reg_upen_stk_rs3247_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: updo_stk_rs3247
BitField Type: RW
BitField Desc: bit map sticky of id32,...,id47
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_stk_rs3247_updo_stk_rs3247_Bit_Start                                                       0
#define cAf6_upen_stk_rs3247_updo_stk_rs3247_Bit_End                                                        31
#define cAf6_upen_stk_rs3247_updo_stk_rs3247_Mask                                                     cBit31_0
#define cAf6_upen_stk_rs3247_updo_stk_rs3247_Shift                                                           0
#define cAf6_upen_stk_rs3247_updo_stk_rs3247_MaxVal                                                 0xffffffff
#define cAf6_upen_stk_rs3247_updo_stk_rs3247_MinVal                                                        0x0
#define cAf6_upen_stk_rs3247_updo_stk_rs3247_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : Sticky Loopcode down id0 to id31
Reg Addr   : 0xE_13FC
Reg Formula: 
    Where  : 
Reg Desc   : 
Indicate loopcode is down

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stk_fl031_Base                                                                    0xE13FC
#define cAf6Reg_upen_stk_fl031                                                                         0xE13FC
#define cAf6Reg_upen_stk_fl031_WidthVal                                                                     32
#define cAf6Reg_upen_stk_fl031_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: updo_stk_fl031
BitField Type: RW
BitField Desc: bit map sticky of id0,...,id31
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_stk_fl031_updo_stk_fl031_Bit_Start                                                         0
#define cAf6_upen_stk_fl031_updo_stk_fl031_Bit_End                                                          31
#define cAf6_upen_stk_fl031_updo_stk_fl031_Mask                                                       cBit31_0
#define cAf6_upen_stk_fl031_updo_stk_fl031_Shift                                                             0
#define cAf6_upen_stk_fl031_updo_stk_fl031_MaxVal                                                   0xffffffff
#define cAf6_upen_stk_fl031_updo_stk_fl031_MinVal                                                          0x0
#define cAf6_upen_stk_fl031_updo_stk_fl031_RstVal                                                          0x0


/*------------------------------------------------------------------------------
Reg Name   : Sticky Loopcode down id32 to id47
Reg Addr   : 0xE_13FB
Reg Formula: 
    Where  : 
Reg Desc   : 
Indicate loopcode is down

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stk_fl3247_Base                                                                   0xE13FB
#define cAf6Reg_upen_stk_fl3247                                                                        0xE13FB
#define cAf6Reg_upen_stk_fl3247_WidthVal                                                                    32
#define cAf6Reg_upen_stk_fl3247_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: updo_stk_fl3247
BitField Type: RW
BitField Desc: bit map sticky of id32,...,id47
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_stk_fl3247_updo_stk_fl3247_Bit_Start                                                       0
#define cAf6_upen_stk_fl3247_updo_stk_fl3247_Bit_End                                                        31
#define cAf6_upen_stk_fl3247_updo_stk_fl3247_Mask                                                     cBit31_0
#define cAf6_upen_stk_fl3247_updo_stk_fl3247_Shift                                                           0
#define cAf6_upen_stk_fl3247_updo_stk_fl3247_MaxVal                                                 0xffffffff
#define cAf6_upen_stk_fl3247_updo_stk_fl3247_MinVal                                                        0x0
#define cAf6_upen_stk_fl3247_updo_stk_fl3247_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Global Control
Reg Addr   : 0x06_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to enable STS,VT,DSN globally.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbenb_Base                                                                      0x060000
#define cAf6Reg_pcfg_glbenb                                                                           0x060000
#define cAf6Reg_pcfg_glbenb_WidthVal                                                                        32
#define cAf6Reg_pcfg_glbenb_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: timerenb
BitField Type: RW
BitField Desc: Enable timer
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbenb_timerenb_Bit_Start                                                                  3
#define cAf6_pcfg_glbenb_timerenb_Bit_End                                                                    3
#define cAf6_pcfg_glbenb_timerenb_Mask                                                                   cBit3
#define cAf6_pcfg_glbenb_timerenb_Shift                                                                      3
#define cAf6_pcfg_glbenb_timerenb_MaxVal                                                                   0x1
#define cAf6_pcfg_glbenb_timerenb_MinVal                                                                   0x0
#define cAf6_pcfg_glbenb_timerenb_RstVal                                                                   0x1

/*--------------------------------------
BitField Name: stsenb
BitField Type: RW
BitField Desc: reserve1
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbenb_stsenb_Bit_Start                                                                    2
#define cAf6_pcfg_glbenb_stsenb_Bit_End                                                                      2
#define cAf6_pcfg_glbenb_stsenb_Mask                                                                     cBit2
#define cAf6_pcfg_glbenb_stsenb_Shift                                                                        2
#define cAf6_pcfg_glbenb_stsenb_MaxVal                                                                     0x1
#define cAf6_pcfg_glbenb_stsenb_MinVal                                                                     0x0
#define cAf6_pcfg_glbenb_stsenb_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: vtenb
BitField Type: RW
BitField Desc: reserve2
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbenb_vtenb_Bit_Start                                                                     1
#define cAf6_pcfg_glbenb_vtenb_Bit_End                                                                       1
#define cAf6_pcfg_glbenb_vtenb_Mask                                                                      cBit1
#define cAf6_pcfg_glbenb_vtenb_Shift                                                                         1
#define cAf6_pcfg_glbenb_vtenb_MaxVal                                                                      0x1
#define cAf6_pcfg_glbenb_vtenb_MinVal                                                                      0x0
#define cAf6_pcfg_glbenb_vtenb_RstVal                                                                      0x1

/*--------------------------------------
BitField Name: dsnsenb
BitField Type: RW
BitField Desc: Enable DE1 channel
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbenb_dsnsenb_Bit_Start                                                                   0
#define cAf6_pcfg_glbenb_dsnsenb_Bit_End                                                                     0
#define cAf6_pcfg_glbenb_dsnsenb_Mask                                                                    cBit0
#define cAf6_pcfg_glbenb_dsnsenb_Shift                                                                       0
#define cAf6_pcfg_glbenb_dsnsenb_MaxVal                                                                    0x1
#define cAf6_pcfg_glbenb_dsnsenb_MinVal                                                                    0x0
#define cAf6_pcfg_glbenb_dsnsenb_RstVal                                                                    0x1


/*------------------------------------------------------------------------------
Reg Name   : POH BER Threshold 1
Reg Addr   : 0x06_2047
Reg Formula: 0x06_2047 + $Rate[2:0]*8  + $Rate[6:3]*128
    Where  : 
           + $Rate(0-127): DS1,E1 (69,71)
Reg Desc   : 
This register is used to configure threshold of BER level 3.

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwptrsh1_Base                                                                     0x062047
#define cAf6Reg_imemrwptrsh1(Rate)                                    (0x062047+(Rate)[2:0]*8+(Rate)[6:3]*128)
#define cAf6Reg_imemrwptrsh1_WidthVal                                                                       32
#define cAf6Reg_imemrwptrsh1_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: setthres
BitField Type: RW
BitField Desc: SetThreshold
BitField Bits: [17:9]
--------------------------------------*/
#define cAf6_imemrwptrsh1_setthres_Bit_Start                                                                 9
#define cAf6_imemrwptrsh1_setthres_Bit_End                                                                  17
#define cAf6_imemrwptrsh1_setthres_Mask                                                               cBit17_9
#define cAf6_imemrwptrsh1_setthres_Shift                                                                     9
#define cAf6_imemrwptrsh1_setthres_MaxVal                                                                0x1ff
#define cAf6_imemrwptrsh1_setthres_MinVal                                                                  0x0
#define cAf6_imemrwptrsh1_setthres_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: winthres
BitField Type: RW
BitField Desc: WindowThreshold
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_imemrwptrsh1_winthres_Bit_Start                                                                 0
#define cAf6_imemrwptrsh1_winthres_Bit_End                                                                   8
#define cAf6_imemrwptrsh1_winthres_Mask                                                                cBit8_0
#define cAf6_imemrwptrsh1_winthres_Shift                                                                     0
#define cAf6_imemrwptrsh1_winthres_MaxVal                                                                0x1ff
#define cAf6_imemrwptrsh1_winthres_MinVal                                                                  0x0
#define cAf6_imemrwptrsh1_winthres_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Threshold 2
Reg Addr   : 0x06_0400
Reg Formula: 0x06_0400 + $Rate*8 + $Thresloc
    Where  : 
           + $Rate(0-63): DS1,E1
           + $Thresloc(0-7): Set/Clear/Window threshold for BER level from 4 to 8
Reg Desc   : 
This register is used to configure threshold of BER level 4 to level 8.

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwptrsh2_Base                                                                     0x060400
#define cAf6Reg_imemrwptrsh2(Rate, Thresloc)                                    (0x060400+(Rate)*8+(Thresloc))
#define cAf6Reg_imemrwptrsh2_WidthVal                                                                      128
#define cAf6Reg_imemrwptrsh2_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: scwthres1
BitField Type: RW
BitField Desc: Set/Clear/Window Threshold
BitField Bits: [33:17]
--------------------------------------*/
#define cAf6_imemrwptrsh2_scwthres1_Bit_Start                                                               17
#define cAf6_imemrwptrsh2_scwthres1_Bit_End                                                                 33
#define cAf6_imemrwptrsh2_scwthres1_Mask_01                                                          cBit31_17
#define cAf6_imemrwptrsh2_scwthres1_Shift_01                                                                17
#define cAf6_imemrwptrsh2_scwthres1_Mask_02                                                            cBit1_0
#define cAf6_imemrwptrsh2_scwthres1_Shift_02                                                                 0

/*--------------------------------------
BitField Name: scwthres2
BitField Type: RW
BitField Desc: Set/Clear/Window Threshold
BitField Bits: [16:00]
--------------------------------------*/
#define cAf6_imemrwptrsh2_scwthres2_Bit_Start                                                                0
#define cAf6_imemrwptrsh2_scwthres2_Bit_End                                                                 16
#define cAf6_imemrwptrsh2_scwthres2_Mask                                                              cBit16_0
#define cAf6_imemrwptrsh2_scwthres2_Shift                                                                    0
#define cAf6_imemrwptrsh2_scwthres2_MaxVal                                                             0x1ffff
#define cAf6_imemrwptrsh2_scwthres2_MinVal                                                                 0x0
#define cAf6_imemrwptrsh2_scwthres2_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Control VT/DSN
Reg Addr   : 0x06_2030
Reg Formula: 0x06_2030 + $GID
    Where  : 
           + $GID(0-11)   : group ds1 ID
Reg Desc   : 
This register is used to enable and set threshold SD SF .

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwpctrl1_Base                                                                     0x062030
#define cAf6Reg_imemrwpctrl1(GID)                                                             (0x062030+(GID))
#define cAf6Reg_imemrwpctrl1_WidthVal                                                                       32
#define cAf6Reg_imemrwpctrl1_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: etype4
BitField Type: RW
BitField Desc: 0:DS1 1:E1 channel 3
BitField Bits: [31]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype4_Bit_Start                                                                  31
#define cAf6_imemrwpctrl1_etype4_Bit_End                                                                    31
#define cAf6_imemrwpctrl1_etype4_Mask                                                                   cBit31
#define cAf6_imemrwpctrl1_etype4_Shift                                                                      31
#define cAf6_imemrwpctrl1_etype4_MaxVal                                                                    0x1
#define cAf6_imemrwpctrl1_etype4_MinVal                                                                    0x0
#define cAf6_imemrwpctrl1_etype4_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sftrsh4
BitField Type: RW
BitField Desc: SF threshold raise channel 3
BitField Bits: [30:28]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh4_Bit_Start                                                                 28
#define cAf6_imemrwpctrl1_sftrsh4_Bit_End                                                                   30
#define cAf6_imemrwpctrl1_sftrsh4_Mask                                                               cBit30_28
#define cAf6_imemrwpctrl1_sftrsh4_Shift                                                                     28
#define cAf6_imemrwpctrl1_sftrsh4_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sftrsh4_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sftrsh4_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: sdtrsh4
BitField Type: RW
BitField Desc: SD threshold raise channel 3
BitField Bits: [27:25]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh4_Bit_Start                                                                 25
#define cAf6_imemrwpctrl1_sdtrsh4_Bit_End                                                                   27
#define cAf6_imemrwpctrl1_sdtrsh4_Mask                                                               cBit27_25
#define cAf6_imemrwpctrl1_sdtrsh4_Shift                                                                     25
#define cAf6_imemrwpctrl1_sdtrsh4_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sdtrsh4_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sdtrsh4_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ena4
BitField Type: RW
BitField Desc: Enable channel 3
BitField Bits: [24]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena4_Bit_Start                                                                    24
#define cAf6_imemrwpctrl1_ena4_Bit_End                                                                      24
#define cAf6_imemrwpctrl1_ena4_Mask                                                                     cBit24
#define cAf6_imemrwpctrl1_ena4_Shift                                                                        24
#define cAf6_imemrwpctrl1_ena4_MaxVal                                                                      0x1
#define cAf6_imemrwpctrl1_ena4_MinVal                                                                      0x0
#define cAf6_imemrwpctrl1_ena4_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: etype3
BitField Type: RW
BitField Desc: 0:DS1 1:E1 channel 2
BitField Bits: [23]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype3_Bit_Start                                                                  23
#define cAf6_imemrwpctrl1_etype3_Bit_End                                                                    23
#define cAf6_imemrwpctrl1_etype3_Mask                                                                   cBit23
#define cAf6_imemrwpctrl1_etype3_Shift                                                                      23
#define cAf6_imemrwpctrl1_etype3_MaxVal                                                                    0x1
#define cAf6_imemrwpctrl1_etype3_MinVal                                                                    0x0
#define cAf6_imemrwpctrl1_etype3_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sftrsh3
BitField Type: RW
BitField Desc: SF threshold raise channel 2
BitField Bits: [22:20]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh3_Bit_Start                                                                 20
#define cAf6_imemrwpctrl1_sftrsh3_Bit_End                                                                   22
#define cAf6_imemrwpctrl1_sftrsh3_Mask                                                               cBit22_20
#define cAf6_imemrwpctrl1_sftrsh3_Shift                                                                     20
#define cAf6_imemrwpctrl1_sftrsh3_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sftrsh3_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sftrsh3_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: sdtrsh3
BitField Type: RW
BitField Desc: SD threshold raise channel 2
BitField Bits: [19:17]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh3_Bit_Start                                                                 17
#define cAf6_imemrwpctrl1_sdtrsh3_Bit_End                                                                   19
#define cAf6_imemrwpctrl1_sdtrsh3_Mask                                                               cBit19_17
#define cAf6_imemrwpctrl1_sdtrsh3_Shift                                                                     17
#define cAf6_imemrwpctrl1_sdtrsh3_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sdtrsh3_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sdtrsh3_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ena3
BitField Type: RW
BitField Desc: Enable channel 2
BitField Bits: [16]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena3_Bit_Start                                                                    16
#define cAf6_imemrwpctrl1_ena3_Bit_End                                                                      16
#define cAf6_imemrwpctrl1_ena3_Mask                                                                     cBit16
#define cAf6_imemrwpctrl1_ena3_Shift                                                                        16
#define cAf6_imemrwpctrl1_ena3_MaxVal                                                                      0x1
#define cAf6_imemrwpctrl1_ena3_MinVal                                                                      0x0
#define cAf6_imemrwpctrl1_ena3_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: etype2
BitField Type: RW
BitField Desc: 0:DS1 1:E1 channel 1
BitField Bits: [15]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype2_Bit_Start                                                                  15
#define cAf6_imemrwpctrl1_etype2_Bit_End                                                                    15
#define cAf6_imemrwpctrl1_etype2_Mask                                                                   cBit15
#define cAf6_imemrwpctrl1_etype2_Shift                                                                      15
#define cAf6_imemrwpctrl1_etype2_MaxVal                                                                    0x1
#define cAf6_imemrwpctrl1_etype2_MinVal                                                                    0x0
#define cAf6_imemrwpctrl1_etype2_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sftrsh2
BitField Type: RW
BitField Desc: SF threshold raise channel 1
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh2_Bit_Start                                                                 12
#define cAf6_imemrwpctrl1_sftrsh2_Bit_End                                                                   14
#define cAf6_imemrwpctrl1_sftrsh2_Mask                                                               cBit14_12
#define cAf6_imemrwpctrl1_sftrsh2_Shift                                                                     12
#define cAf6_imemrwpctrl1_sftrsh2_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sftrsh2_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sftrsh2_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: sdtrsh2
BitField Type: RW
BitField Desc: SD threshold raise channel 1
BitField Bits: [11:9]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh2_Bit_Start                                                                  9
#define cAf6_imemrwpctrl1_sdtrsh2_Bit_End                                                                   11
#define cAf6_imemrwpctrl1_sdtrsh2_Mask                                                                cBit11_9
#define cAf6_imemrwpctrl1_sdtrsh2_Shift                                                                      9
#define cAf6_imemrwpctrl1_sdtrsh2_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sdtrsh2_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sdtrsh2_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ena2
BitField Type: RW
BitField Desc: Enable channel 1
BitField Bits: [8]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena2_Bit_Start                                                                     8
#define cAf6_imemrwpctrl1_ena2_Bit_End                                                                       8
#define cAf6_imemrwpctrl1_ena2_Mask                                                                      cBit8
#define cAf6_imemrwpctrl1_ena2_Shift                                                                         8
#define cAf6_imemrwpctrl1_ena2_MaxVal                                                                      0x1
#define cAf6_imemrwpctrl1_ena2_MinVal                                                                      0x0
#define cAf6_imemrwpctrl1_ena2_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: etype1
BitField Type: RW
BitField Desc: 0:DS1 1:E1 channel 0
BitField Bits: [7]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype1_Bit_Start                                                                   7
#define cAf6_imemrwpctrl1_etype1_Bit_End                                                                     7
#define cAf6_imemrwpctrl1_etype1_Mask                                                                    cBit7
#define cAf6_imemrwpctrl1_etype1_Shift                                                                       7
#define cAf6_imemrwpctrl1_etype1_MaxVal                                                                    0x1
#define cAf6_imemrwpctrl1_etype1_MinVal                                                                    0x0
#define cAf6_imemrwpctrl1_etype1_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sftrsh1
BitField Type: RW
BitField Desc: SF threshold raise channel 0
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh1_Bit_Start                                                                  4
#define cAf6_imemrwpctrl1_sftrsh1_Bit_End                                                                    6
#define cAf6_imemrwpctrl1_sftrsh1_Mask                                                                 cBit6_4
#define cAf6_imemrwpctrl1_sftrsh1_Shift                                                                      4
#define cAf6_imemrwpctrl1_sftrsh1_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sftrsh1_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sftrsh1_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: sdtrsh1
BitField Type: RW
BitField Desc: SD threshold raise channel 0
BitField Bits: [3:1]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh1_Bit_Start                                                                  1
#define cAf6_imemrwpctrl1_sdtrsh1_Bit_End                                                                    3
#define cAf6_imemrwpctrl1_sdtrsh1_Mask                                                                 cBit3_1
#define cAf6_imemrwpctrl1_sdtrsh1_Shift                                                                      1
#define cAf6_imemrwpctrl1_sdtrsh1_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sdtrsh1_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sdtrsh1_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ena1
BitField Type: RW
BitField Desc: Enable channel 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena1_Bit_Start                                                                     0
#define cAf6_imemrwpctrl1_ena1_Bit_End                                                                       0
#define cAf6_imemrwpctrl1_ena1_Mask                                                                      cBit0
#define cAf6_imemrwpctrl1_ena1_Shift                                                                         0
#define cAf6_imemrwpctrl1_ena1_MaxVal                                                                      0x1
#define cAf6_imemrwpctrl1_ena1_MinVal                                                                      0x0
#define cAf6_imemrwpctrl1_ena1_RstVal                                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Report VT/DSN
Reg Addr   : 0x06_80A8
Reg Formula: 0x06_80A8 +  $LID
    Where  : 
           + $LID(0-47)
Reg Desc   : 
This register is used to get current BER rate .

------------------------------------------------------------------------------*/
#define cAf6Reg_ramberratevtds_Base                                                                   0x0680A8
#define cAf6Reg_ramberratevtds(LID)                                                           (0x0680A8+(LID))
#define cAf6Reg_ramberratevtds_WidthVal                                                                     32
#define cAf6Reg_ramberratevtds_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: hwsta
BitField Type: RW
BitField Desc: Hardware status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ramberratevtds_hwsta_Bit_Start                                                                  3
#define cAf6_ramberratevtds_hwsta_Bit_End                                                                    3
#define cAf6_ramberratevtds_hwsta_Mask                                                                   cBit3
#define cAf6_ramberratevtds_hwsta_Shift                                                                      3
#define cAf6_ramberratevtds_hwsta_MaxVal                                                                   0x1
#define cAf6_ramberratevtds_hwsta_MinVal                                                                   0x0
#define cAf6_ramberratevtds_hwsta_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: rate
BitField Type: RW
BitField Desc: BER rate
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ramberratevtds_rate_Bit_Start                                                                   0
#define cAf6_ramberratevtds_rate_Bit_End                                                                     2
#define cAf6_ramberratevtds_rate_Mask                                                                  cBit2_0
#define cAf6_ramberratevtds_rate_Shift                                                                       0
#define cAf6_ramberratevtds_rate_MaxVal                                                                    0x7
#define cAf6_ramberratevtds_rate_MinVal                                                                    0x0
#define cAf6_ramberratevtds_rate_RstVal                                                                    0x0

#endif /* _AF6_REG_AF6CCI0021_RD_PDH_H_ */

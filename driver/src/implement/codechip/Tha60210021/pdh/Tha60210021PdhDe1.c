/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210021De1.c
 *
 * Created Date: May 14, 2015
 *
 * Description : DS1/E1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaModulePdhReg.h"
#include "../../../default/map/ThaModuleMapReg.h"
#include "../../../default/map/ThaModuleMap.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210031/pdh/Tha60210031PdhDe1.h"
#include "Tha60210021ModulePdh.h"
#include "Tha60210021ModulePdhReg.h"
#include "../map/Tha60210021ModuleMapReg.h"
#include "Tha60210021ModulePdhMdlPrmReg.h"
#include "Tha60210021PdhDe1.h"

/*--------------------------- Define -----------------------------------------*/
#define cFirst24PortsStickyRegAddr	0xF30006
#define cLast24PortsStickyRegAddr	0xF40006

/* BOM RCV MASK & SHIFT */
#define cBomRcvMask         cBit14_9
#define cBomRcvShift        9

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210021PdhDe1 *)self)
#define mThaDe1(self) ((ThaPdhDe1)self)

#define mIntrFieldIns(pRegVal, prefix, defect, enable, mask) \
    if (defect & mask)\
        mFieldIns(pRegVal, prefix##_Mask, prefix##_Shift, (enable & mask) ? 1 : 0); \

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPdhDe1Methods    m_ThaPdhDe1Override;
static tAtChannelMethods    m_AtChannelOverride;
static tAtPdhChannelMethods m_AtPdhChannelOverride;

/* Save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtPdhChannelMethods    *m_AtPdhChannelMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* This function is to set Led state of a DS1/E1 channel */
static eAtRet LedStateSet(AtPdhChannel self, eAtLedState ledState)
    {
    AtUnused(self);
    AtUnused(ledState);
    return cAtErrorModeNotSupport;
    }

/* This function is to get Led state of a DS1/E1 channel */
static eAtLedState LedStateGet(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static AtModulePdh PdhModule(AtChannel self)
    {
    return (AtModulePdh) AtChannelModuleGet(self);
    }

static uint32 BaseAddress(ThaPdhDe1 self)
    {
    return ThaModulePdhBaseAddress((ThaModulePdh)PdhModule((AtChannel)self));
    }

static eAtRet TxHwBomSet(ThaPdhDe1 self, uint8 message, uint32 threshold)
    {
    uint32 data;
    eAtRet ret = Tha60210021ModulePdhlcIndirectRead(PdhModule((AtChannel)self), (AtPdhDe1)self, &data);
    if (ret != cAtOk)
        return ret;

    mRegFieldSet(data, cAf6_indwr_pen_force_bom_, 0);
    ret = Tha60210021ModulePdhlcIndirectWrite(PdhModule((AtChannel)self), (AtPdhDe1)self, data);
    if (ret != cAtOk)
        return ret;

    mRegFieldSet(data, cAf6_indwr_pen_force_bom_, 1);
    mRegFieldSet(data, cAf6_indwr_pen_thr_rep_, threshold);
    mRegFieldSet(data, cAf6_indwr_pen_patt_bom2_, message);
    return Tha60210021ModulePdhlcIndirectWrite(PdhModule((AtChannel)self), (AtPdhDe1)self, data);
    }

static uint32 TxHwBomThresGet(ThaPdhDe1 self)
    {
    uint32 data;
    eAtRet ret = Tha60210021ModulePdhlcIndirectRead(PdhModule((AtChannel)self), (AtPdhDe1)self, &data);
    if (ret != cAtOk)
        return 0;

    return (uint32)mRegField(data, cAf6_indwr_pen_thr_rep_);
    }

static uint8 TxHwBomGet(ThaPdhDe1 self)
    {
    uint32 data;
    eAtRet ret = Tha60210021ModulePdhlcIndirectRead(PdhModule((AtChannel)self), (AtPdhDe1)self, &data);
    if (ret != cAtOk)
        return 0;

    return (uint8)mRegField(data, cAf6_indwr_pen_patt_bom2_);
    }

static uint8 RxHwBomGet(ThaPdhDe1 self)
    {
    uint32 channelId = AtChannelIdGet((AtChannel) self);
    uint32 regVal = mChannelHwRead(self, cAf6Reg_upen_fdl_info_Base + channelId + BaseAddress(self), cAtModulePdh);
    return (uint8)((regVal & cBomRcvMask) >> cBomRcvShift);
    }

static uint8 RxHwLoopcodeGet(ThaPdhDe1 self)
    {
    uint32 channelId = AtChannelIdGet((AtChannel)self);
    uint32 address = cAf6Reg_upen_info(channelId) + BaseAddress(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    return (uint8) (regVal & cAf6_upen_info_updo_sta_Mask);
    }

static eBool HasLineLayer(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool LiuIsLoc(AtPdhChannel self)
    {
    uint32 de1Id, addr, regVal;

    if (!AtPdhChannelHasLineLayer(self))
        return cAtFalse;

    de1Id = AtChannelIdGet((AtChannel)self);
    addr = de1Id < 24 ? cFirst24PortsStickyRegAddr : cLast24PortsStickyRegAddr;
    regVal = mChannelHwRead(self, addr, cAtModulePdh);

    return (regVal & (cBit0 << (de1Id % 24))) ? cAtTrue : cAtFalse;
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    /* Let super deal with working logic */
    return m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool IsDiagnostic(AtChannel self)
    {
    return AtDeviceDiagnosticModeIsEnabled(AtChannelDeviceGet(self));
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret = m_AtChannelMethods->LoopbackSet(self, loopbackMode);
    if (ret != cAtOk)
        return ret;

    if (IsDiagnostic(self))
        return ThaModulePdhDiagLiuLoopbackSet(self, loopbackMode);

    return cAtOk;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    uint8 loopbackMode = m_AtChannelMethods->LoopbackGet(self);

    if (loopbackMode != cAtPdhLoopbackModeRelease)
        return loopbackMode;

    if (IsDiagnostic(self))
        return ThaModulePdhDiagLiuLoopbackGet(self);

    return loopbackMode;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (IsDiagnostic(self))
        ThaModulePdhDiagLiuLoopbackSet(self, cAtPdhLoopbackModeRelease);

    return cAtOk;
    }

static uint32 De1ErrCounterOffset(ThaPdhDe1 de1, eBool r2cEn)
    {
    uint32 offset = mMethodsGet(de1)->Ds1E1J1DefaultOffset(de1);
    return r2cEn ? offset : (64 + offset);
    }

static eBool LedStateIsSupported(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldConsiderLofWhenLos(ThaPdhDe1 self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    return Tha6021PdhDe1SupportedInterruptMasks(self);
    }

static void OverrideAtChannel(ThaPdhDe1 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPdhChannel(ThaPdhDe1 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(tAtPdhChannelMethods));

        mMethodOverride(m_AtPdhChannelOverride, LedStateSet);
        mMethodOverride(m_AtPdhChannelOverride, LedStateGet);
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtPdhChannelOverride, LedStateIsSupported);
        mMethodOverride(m_AtPdhChannelOverride, HasLineLayer);
        mMethodOverride(m_AtPdhChannelOverride, LiuIsLoc);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideThaPdhDe1(ThaPdhDe1 self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe1Override, mMethodsGet(self), sizeof(m_ThaPdhDe1Override));

        mMethodOverride(m_ThaPdhDe1Override, TxHwBomSet);
        mMethodOverride(m_ThaPdhDe1Override, TxHwBomThresGet);
        mMethodOverride(m_ThaPdhDe1Override, TxHwBomGet);
        mMethodOverride(m_ThaPdhDe1Override, RxHwBomGet);
        mMethodOverride(m_ThaPdhDe1Override, RxHwLoopcodeGet);
        mMethodOverride(m_ThaPdhDe1Override, De1ErrCounterOffset);
        mMethodOverride(m_ThaPdhDe1Override, ShouldConsiderLofWhenLos);
        }

    mMethodsSet(self, &m_ThaPdhDe1Override);
    }

static void Override(ThaPdhDe1 self)
    {
    OverrideThaPdhDe1(self);
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PdhDe1);
    }

AtPdhDe1 Tha60210021PdhDe1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhDe1ObjectInit((AtPdhDe1)self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaPdhDe1)self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe1 Tha60210021PdhDe1New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newDe1 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe1 == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021PdhDe1ObjectInit(newDe1, channelId, module);
    }

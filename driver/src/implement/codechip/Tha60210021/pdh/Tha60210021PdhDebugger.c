/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210021PdhDebugger.c
 *
 * Created Date: Nov 12, 2015
 *
 * Description : PDH Debugger
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/debugger/ThaPdhDebuggerInternal.h"
#include "../../../../generic/man/AtModuleInternal.h"

#include "Tha60210021ModulePdhMdlPrmReg.h"
#include "Tha60210021ModulePdhPrm.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaPrmPayloadBitNmNiHwMask  cBit7_6
#define cThaPrmPayloadBitNmNiHwShift 6
#define cThaPrmPayloadBitLBHwMask    cBit8
#define cThaPrmPayloadBitLBHwShift   8
#define cThaPrmPayloadBitSLHwMask    cBit9
#define cThaPrmPayloadBitSLHwShift   9
#define cThaPrmPayloadBitLVHwMask    cBit10
#define cThaPrmPayloadBitLVHwShift   10
#define cThaPrmPayloadBitFEHwMask    cBit11
#define cThaPrmPayloadBitFEHwShift   11
#define cThaPrmPayloadBitSEHwMask    cBit12
#define cThaPrmPayloadBitSEHwShift   12
#define cThaPrmPayloadBitGHwMask     cBit15_13
#define cThaPrmPayloadBitGHwShift    13

#define cThaPrmPayloadForcedHwMask   cBit1
#define cThaPrmPayloadForcedHwShift  1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021PdhDebugger
    {
    tThaPdhDebugger super;
    }tTha60210021PdhDebugger;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPdhDebuggerMethods m_ThaPdhDebuggerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePdhRet PrmChannelSelect(ThaPdhDebugger self, AtPdhDe1 de1)
    {
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 regAddr = cAf6Reg_upen_cfgid_mon_Base + ThaModulePdhPrmBaseAddress(pdhModule);
    uint32 regVal = mModuleHwRead(pdhModule, regAddr);
    mRegFieldSet(regVal, cAf6_upen_cfgid_mon_out_cfgid_mon_, AtChannelIdGet((AtChannel)de1));
    mModuleHwWrite(pdhModule, regAddr, regVal);

    return cAtOk;
    }

static uint32 PrmChannelSelectGet(ThaPdhDebugger self)
    {
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 regAddr = cAf6Reg_upen_cfgid_mon_Base + ThaModulePdhPrmBaseAddress(pdhModule);
    uint32 reggVal = mModuleHwRead(pdhModule, regAddr);
    return mRegField(reggVal, cAf6_upen_cfgid_mon_out_cfgid_mon_);
    }

static uint32 DebugRegister(ThaModulePdh pdhModule)
    {
    return cAf6Reg_upen_cfg_debug_Base + ThaModulePdhPrmBaseAddress(pdhModule);
    }

static eAtModulePdhRet PrmMonitorEnable(ThaPdhDebugger self, eBool enable)
    {
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 regAddr = DebugRegister(pdhModule);
    uint32 regVal = mModuleHwRead(pdhModule, regAddr);
    mRegFieldSet(regVal, cAf6_upen_cfg_debug_out_cfg_dis_, enable ? 0x0 : 0x1);
    mModuleHwWrite(pdhModule, regAddr, regVal);

    return cAtOk;
    }

static eBool PrmMonitorIsEnabled(ThaPdhDebugger self)
    {
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 values = mModuleHwRead(pdhModule, DebugRegister(pdhModule));
    return  mRegField(values, cAf6_upen_cfg_debug_out_cfg_dis_) ? cAtFalse : cAtTrue;
    }

static eAtModulePdhRet PrmForcedFullPrmPayload(ThaPdhDebugger self, eBool enable)
    {
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 regAddr = DebugRegister(pdhModule);
    uint32 regVal = mModuleHwRead(pdhModule, regAddr);
    mRegFieldSet(regVal, cAf6_upen_cfg_debug_out_cfg_selinfo2_, mBoolToBin(enable));
    mModuleHwWrite(pdhModule, regAddr, regVal);

    return cAtOk;
    }

static eBool PrmIsForcedFullPrmPayload(ThaPdhDebugger self)
    {
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 regVal = mModuleHwRead(pdhModule, DebugRegister(pdhModule));
    return mRegField(regVal, cAf6_upen_cfg_debug_out_cfg_selinfo2_) ? cAtTrue : cAtFalse;
    }

static eAtModulePdhRet PrmForce8BitPrmPayload(ThaPdhDebugger self, eBool enable)
    {
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 regAddr = DebugRegister(pdhModule);
    uint32 regVal = mModuleHwRead(pdhModule, regAddr);
    mRegFieldSet(regVal, cAf6_upen_cfg_debug_out_cfg_selinfo1_, mBoolToBin(enable));
    mModuleHwWrite(pdhModule, regAddr, regVal);

    return cAtOk;
    }

static eBool PrmIsForced8BitPrmPayload(ThaPdhDebugger self)
    {
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 regVal = mModuleHwRead(pdhModule, DebugRegister(pdhModule));
    return mRegField(regVal, cAf6_upen_cfg_debug_out_cfg_selinfo1_) ? cAtTrue : cAtFalse;
    }

static eAtModulePdhRet PrmForcePayLoad(ThaPdhDebugger self, uint32 payloadMap, eBool forced)
    {
    uint8 hwValue = (uint8)mBoolToBin(forced);
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 regAddr = DebugRegister(pdhModule);
    uint32 regVal = mModuleHwRead(pdhModule, regAddr);

    if (payloadMap & cThaPdhDebuggerPrmPayloadBitG1)
        mRegFieldSet(regVal, cThaPrmPayloadBitGHw, forced ? 0x1 : 0x0);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitG2)
        mRegFieldSet(regVal, cThaPrmPayloadBitGHw, forced ? 0x2 : 0x0);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitG3)
        mRegFieldSet(regVal, cThaPrmPayloadBitGHw, forced ? 0x3 : 0x0);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitG4)
        mRegFieldSet(regVal, cThaPrmPayloadBitGHw, forced ? 0x4 : 0x0);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitG5)
        mRegFieldSet(regVal, cThaPrmPayloadBitGHw, forced ? 0x5 : 0x0);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitG6)
        mRegFieldSet(regVal, cThaPrmPayloadBitGHw, forced ? 0x6 : 0x0);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitSE)
        mRegFieldSet(regVal, cThaPrmPayloadBitSEHw, hwValue);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitFE)
        mRegFieldSet(regVal, cThaPrmPayloadBitFEHw, hwValue);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitLV)
        mRegFieldSet(regVal, cThaPrmPayloadBitLVHw, hwValue);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitSL)
        mRegFieldSet(regVal, cThaPrmPayloadBitSLHw, hwValue);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitLB)
        mRegFieldSet(regVal, cThaPrmPayloadBitLBHw, hwValue);

    mRegFieldSet(regVal, cThaPrmPayloadForcedHw, forced ? 0x1 : 0x0);

    mModuleHwWrite(pdhModule, regAddr, regVal);

    return cAtOk;
    }

static uint32 PrmIsForcedPayLoad(ThaPdhDebugger self, uint32 *payLoadValue)
    {
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 values, hwPayLoadValue, numberBit;

    numberBit = hwPayLoadValue = 0;
    values = mModuleHwRead(pdhModule, DebugRegister(pdhModule));
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitGHw, 0x1, cThaPdhDebuggerPrmPayloadBitG1)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitGHw, 0x2, cThaPdhDebuggerPrmPayloadBitG2)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitGHw, 0x3, cThaPdhDebuggerPrmPayloadBitG3)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitGHw, 0x4, cThaPdhDebuggerPrmPayloadBitG4)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitGHw, 0x5, cThaPdhDebuggerPrmPayloadBitG5)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitGHw, 0x6, cThaPdhDebuggerPrmPayloadBitG6)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitSEHw, 0x1, cThaPdhDebuggerPrmPayloadBitSE)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitFEHw, 0x1, cThaPdhDebuggerPrmPayloadBitFE)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitLVHw, 0x1, cThaPdhDebuggerPrmPayloadBitLV)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitSLHw, 0x1, cThaPdhDebuggerPrmPayloadBitSL)
    mPrmDebugForcedPayload(values, cThaPrmPayloadBitLBHw, 0x1, cThaPdhDebuggerPrmPayloadBitLB)

    *payLoadValue = hwPayLoadValue;
    return numberBit;
    }

static eAtModulePdhRet PrmForceNmNi(ThaPdhDebugger self, uint32 payloadMap, uint8 swValue)
    {
    uint32 regValues, regAddr;
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);

    if (swValue > 3)
        return cAtErrorInvlParm;

    regAddr = DebugRegister(pdhModule);
    regValues = mModuleHwRead(pdhModule, regAddr);
    if (payloadMap & cThaPdhDebuggerPrmPayloadBitNmNi)
        mRegFieldSet(regValues, cThaPrmPayloadBitNmNiHw, swValue);
    mModuleHwWrite(pdhModule, regAddr, regValues);

    return cAtOk;
    }

static uint8 PrmUnForceNmNi(ThaPdhDebugger self)
    {
    ThaModulePdh pdhModule = ThaPdhDebuggerModuleGet(self);
    uint32 regVal = mModuleHwRead(pdhModule, DebugRegister(pdhModule));
    return (uint8)mRegField(regVal, cThaPrmPayloadBitNmNiHw);
    }

static void OverrideThaPdhDebugger(ThaPdhDebugger self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDebuggerOverride, mMethodsGet(self), sizeof(m_ThaPdhDebuggerOverride));

        mMethodOverride(m_ThaPdhDebuggerOverride, PrmChannelSelect);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmChannelSelectGet);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmMonitorEnable);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmMonitorIsEnabled);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmForcedFullPrmPayload);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmIsForcedFullPrmPayload);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmForce8BitPrmPayload);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmIsForced8BitPrmPayload);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmForcePayLoad);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmIsForcedPayLoad);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmForceNmNi);
        mMethodOverride(m_ThaPdhDebuggerOverride, PrmUnForceNmNi);
        }

    mMethodsSet(self, &m_ThaPdhDebuggerOverride);
    }

static void Override(ThaPdhDebugger self)
    {
    OverrideThaPdhDebugger(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PdhDebugger);
    }

static ThaPdhDebugger ObjectInit(ThaPdhDebugger self, ThaModulePdh pdhModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhDebuggerObjectInit(self, pdhModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPdhDebugger Tha60210021PdhDebuggerNew(ThaModulePdh pdhModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPdhDebugger newChecker = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChecker == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChecker, pdhModule);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210021PdhPrmController.c
 *
 * Created Date: Oct 28, 2015
 *
 * Description : PRM controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/pdh/ThaPdhPrmControllerInternal.h"
#include "../../../default/pdh/ThaModulePdhReg.h"

#include "Tha60210021ModulePdhMdlPrmReg.h"
#include "Tha60210021ModulePdhPrm.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021PdhPrmController
    {
    tThaPdhPrmController super;
    }tTha60210021PdhPrmController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPdhPrmControllerMethods  m_AtPdhPrmControllerOverride;
static tThaPdhPrmControllerMethods m_ThaPdhPrmControllerOverride;

/* Save super implementation */
static const tAtPdhPrmControllerMethods *m_AtPdhPrmControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPdhDe1 De1Get(AtPdhPrmController self)
    {
    return (ThaPdhDe1)AtPdhPrmControllerDe1Get(self);
    }

static uint32 BaseAddress(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = De1Get(self);
    return ThaModulePdhPrmBaseAddress((ThaModulePdh)AtChannelModuleGet((AtChannel)de1));
    }

static uint32 PrmOffset(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = De1Get(self);
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)de1);
    return ThaModulePdhDe1PrmOffset(pdhModule, de1);
    }

static eBool IsEnabled(AtPdhPrmController self)
    {
    uint32 regAddress = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset(self);
    uint32 regValue = mChannelHwRead(De1Get(self), regAddress, cAtModulePdh);
    return mRegField(regValue, cAf6_upen_prm_txcfgcr_cfg_prmen_tx_) ? cAtTrue : cAtFalse;
    }

static eAtModulePdhRet HwTxStandardSet(ThaPdhPrmController self, uint8 standard)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset((AtPdhPrmController)self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_, standard);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtPdhDe1PrmStandard TxStandardGet(AtPdhPrmController self)
    {
    uint32 regAddr = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset(self);
    uint32 regVal = mChannelHwRead(De1Get(self), regAddr, cAtModulePdh);
    return mRegField(regVal, cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_) ? cAtPdhDe1PrmStandardAtt : cAtPdhDe1PrmStandardAnsi;
    }

static eAtModulePdhRet HwRxStandardSet(ThaPdhPrmController self, uint8 standard)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_upen_rxprm_cfgstd_Base + PrmOffset((AtPdhPrmController)self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_rxprm_cfgstd_cfg_std_prm_, standard);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtPdhDe1PrmStandard RxStandardGet(AtPdhPrmController self)
    {
    uint32 regAddr = cAf6Reg_upen_rxprm_cfgstd_Base + PrmOffset(self);
    uint32 regVal = mChannelHwRead(De1Get(self), regAddr, cAtModulePdh);
    return mRegField(regVal, cAf6_upen_rxprm_cfgstd_cfg_std_prm_) ? cAtPdhDe1PrmStandardAtt : cAtPdhDe1PrmStandardAnsi;
    }

static uint32 TxMessageRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_txprm_cnt_pkt_Base;
    }

static uint32 TxByteRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_txprm_cnt_byte_Base;
    }

static uint32 RxGoodMessageRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_gmess_Base;
    }

static uint32 RxByteRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_cnt_byte_Base;
    }

static uint32 RxDiscardRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_drmess_Base;
    }

static uint32 RxMissRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_mmess_Base;
    }

static uint32 SwChannelId2HwAlarmBitMask(uint32 swChannelId)
    {
    if (swChannelId > 31)
        return (cBit0 << (swChannelId - 31));
    return (cBit0 << (swChannelId));
    }

static uint32 DicardAlarmRead2Clear(AtPdhPrmController self, eBool read2Clear)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 offset, regVal, address;
    uint32 channelId = AtChannelIdGet((AtChannel)de1);
    uint32 hwChannelId = SwChannelId2HwAlarmBitMask(channelId);

    address = cAf6Reg_upen_rxdrmess_stk2_Base;
    if (channelId < 32)
        address = cAf6Reg_upen_rxdrmess_stk1_Base;

    offset = BaseAddress(self);
    regVal = mChannelHwRead(de1, address + offset, cAtModulePdh);
    if (read2Clear)
        mChannelHwWrite(de1, address + offset, hwChannelId, cAtModulePdh);

    if (hwChannelId & regVal)
        return cAtPdhDe1PrmAlarmDiscardMessage;

    return cAtPdhDe1PrmAlarmNone;
    }

static uint32 MissAlarmRead2Clear(AtPdhPrmController self, eBool read2Clear)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 offset, regVal, address;
    uint32 channelId = AtChannelIdGet((AtChannel)de1);
    uint32 hwChannelId = SwChannelId2HwAlarmBitMask(channelId);

    address = cAf6Reg_upen_rxmmess_stk2_Base;
    if (channelId < 32)
        address = cAf6Reg_upen_rxmmess_stk1_Base;

    offset = BaseAddress(self);
    regVal = mChannelHwRead(de1, address + offset, cAtModulePdh);
    if (read2Clear)
        mChannelHwWrite(de1, address + offset, hwChannelId, cAtModulePdh);

    if (hwChannelId & regVal)
        return cAtPdhDe1PrmAlarmMissMessage;

    return cAtPdhDe1PrmAlarmNone;
    }

static uint32 AlarmInterruptReadClear(AtPdhPrmController self, eBool read2Clear)
    {
    return (DicardAlarmRead2Clear(self, read2Clear) | MissAlarmRead2Clear(self, read2Clear));
    }

static uint32 AlarmInterruptGet(AtPdhPrmController self)
    {
    return AlarmInterruptReadClear(self, cAtFalse);
    }

static uint32 AlarmInterruptClear(AtPdhPrmController self)
    {
    return AlarmInterruptReadClear(self, cAtTrue);
    }

static uint8 TxCRBitGet(AtPdhPrmController self)
    {
    uint32 regAddress = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset(self);
    uint32 regValue = mChannelHwRead(De1Get(self), regAddress, cAtModulePdh);
    return (uint8)mRegField(regValue, cAf6_upen_prm_txcfgcr_cfg_prm_cr_);
    }

static uint8 TxLBBitGet(AtPdhPrmController self)
    {
    uint32 regAddress = cAf6Reg_upen_prm_txcfglb_Base + PrmOffset(self);
    uint32 regValue = mChannelHwRead(De1Get(self), regAddress, cAtModulePdh);
    return (uint8)mRegField(regValue, cAf6_upen_prm_txcfglb_cfg_prm_lb_);
    }

static eBool TxLBBitIsEnabled(AtPdhPrmController self)
    {
    uint32 regAddress = cAf6Reg_upen_prm_txcfglb_Base + PrmOffset(self);
    uint32 regValue = mChannelHwRead(De1Get(self), regAddress, cAtModulePdh);
    return mRegField(regValue, cAf6_upen_prm_txcfglb_cfg_prm_enlb_) ? cAtTrue : cAtFalse;
    }

static eAtModulePdhRet HwTxCRBitSet(ThaPdhPrmController self, uint8 value)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddr, regVal;

    regAddr = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset((AtPdhPrmController)self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_prm_txcfgcr_cfg_prm_cr_, value);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePdhRet HwTxLBBitSet(ThaPdhPrmController self, uint8 value)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_upen_prm_txcfglb_Base + PrmOffset((AtPdhPrmController)self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_prm_txcfglb_cfg_prm_lb_, value);
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePdhRet TxLBBitEnable(AtPdhPrmController self, eBool enable)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_upen_prm_txcfglb_Base + PrmOffset(self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_prm_txcfglb_cfg_prm_enlb_, mBoolToBin(enable));
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePdhRet TxHwEnable(ThaPdhPrmController self, eBool enable)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset((AtPdhPrmController)self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_prm_txcfgcr_cfg_prmen_tx_, mBoolToBin(enable));
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePdhRet RxHwEnable(ThaPdhPrmController self, eBool enable)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_upen_rxprm_cfgstd_Base + PrmOffset((AtPdhPrmController)self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_rxprm_cfgstd_cfg_prm_rxen_, (enable ? 1 : 0));
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);
    return cAtOk;
    }

static uint32 StartVersionHasRxEnabledCfgRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4647);
    }

static eAtModulePdhRet HwEnable(ThaPdhPrmController self, eBool enable)
    {
    eAtRet ret = TxHwEnable(self, enable);

    if (ThaPdhPrmControllerHasRxEnabledCfgRegister(self))
    	ret |= RxHwEnable(self, enable);

    return ret;
    }

static uint8 RxLBBitGet(AtPdhPrmController self)
    {
    uint32 regAddress = cAf6Reg_prm_lb_int_crrsta_Base + PrmOffset(self);
    uint32 regValue = mChannelHwRead(De1Get(self), regAddress, cAtModulePdh);
    return (uint8)mRegField(regValue, cAf6_prm_lb_int_crrsta_prm_lbint_crrsta_);
    }

static uint32 InterruptMaskGet(AtPdhPrmController self)
    {
    uint32 regVal, regAddr;
    regAddr = cAf6Reg_prm_cfg_lben_int_Base + PrmOffset(self);
    regVal = mChannelHwRead(De1Get(self), regAddr, cAtModulePdh);
    if (regVal & cAf6_prm_cfg_lben_int_prm_cfglben_int_Mask)
        return cAtPdhDs1AlarmPrmLBBitChange;

    return 0;
    }

static eAtModulePdhRet InterruptMaskSet(AtPdhPrmController self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regVal, address;
    ThaPdhDe1 de1 = De1Get(self);

    /* Enable PRM LB bit interrupt mask */
    address = cAf6Reg_prm_cfg_lben_int_Base + PrmOffset(self);
    regVal  = mChannelHwRead(de1, address, cAtModulePdh);
    if (defectMask & cAtPdhDs1AlarmPrmLBBitChange)
        {
        if (enableMask & cAtPdhDs1AlarmPrmLBBitChange)
            regVal |= cAf6_prm_cfg_lben_int_prm_cfglben_int_Mask;
        else
            regVal &= (uint32)(~cAf6_prm_cfg_lben_int_prm_cfglben_int_Mask);
        }
    mChannelHwWrite(de1, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static void InterruptProcess(AtPdhPrmController self, uint8 slice, uint8 de3Id, uint8 de2Id, uint8 de1Id, AtHal hal)
    {
    ThaPdhDe1 de1 = De1Get(self);
    const uint32 baseAddress = ThaModulePdhPrmBaseAddress((ThaModulePdh)AtChannelModuleGet((AtChannel)de1));
    uint32 offset = baseAddress + de1Id;
    uint32 intrMask = AtHalRead(hal, cAf6Reg_prm_cfg_lben_int_Base + offset);
    uint32 intrStatus = AtHalRead(hal, cAf6Reg_prm_lb_int_sta_Base + offset);
    uint32 events = 0;
    uint32 defects = 0;
    AtUnused(slice);
    AtUnused(de3Id);
    AtUnused(de2Id);

    intrStatus &= intrMask;

    if (intrStatus & cAf6_prm_lb_int_sta_prm_lbint_sta_Mask)
        {
        events |= cAtPdhDs1AlarmPrmLBBitChange;
        }

    /* Clear interrupt */
    AtHalWrite(hal, cAf6Reg_prm_lb_int_sta_Base + offset, intrStatus);

    AtChannelAllAlarmListenersCall((AtChannel)de1, events, defects);
    }

static uint32 DefectHistoryReadToClear(ThaPdhPrmController self, eBool r2c)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)De1Get((AtPdhPrmController)self);
    uint32 address = cAf6Reg_prm_lb_int_sta_Base + PrmOffset((AtPdhPrmController)self);
    uint32 regValue = mChannelHwRead(de1, address, cAtModulePdh);

    if (r2c)
        mChannelHwWrite(de1, address, regValue, cAtModulePdh);
    return mRegField(regValue, cAf6_prm_lb_int_sta_prm_lbint_sta_) ? cAtPdhDs1AlarmPrmLBBitChange : 0;
    }

static eAtModulePdhRet HwCRBitExpectSet(ThaPdhPrmController self, uint8 value)
    {
    ThaPdhDe1 de1 = De1Get((AtPdhPrmController)self);
    uint32 regAddress, regValue;

    regAddress = cAf6Reg_upen_rxprm_cfgstd_Base + PrmOffset((AtPdhPrmController)self);
    regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    mRegFieldSet(regValue, cAf6_upen_rxprm_cfgstd_cfg_prm_cr_, value);
    mChannelHwWrite(de1, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static uint8 CRBitExpectGet(AtPdhPrmController self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)De1Get(self);
    uint32 regAddress = cAf6Reg_upen_rxprm_cfgstd_Base + PrmOffset(self);
    uint32 regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);

    return (uint8)mRegField(regValue, cAf6_upen_rxprm_cfgstd_cfg_prm_cr_);
    }

static eAtModulePdhRet HwFcsBitOrderSet(AtPdhPrmController self, eAtBitOrder order)
    {
    ThaPdhDe1 de1 = De1Get(self);
    uint32 regAddr, regVal;

    /* Check if invalid input */
    if ((order != cAtBitOrderMsb) && (order != cAtBitOrderLsb))
        return cAtErrorInvlParm;

    regAddr = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset(self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_prm_txcfgcr_cfg_prm_txfcs_mode_, (order == cAtBitOrderLsb) ? 0 : 1);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    regAddr = cAf6Reg_upen_rxprm_cfgstd_Base + PrmOffset(self);
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_rxprm_cfgstd_cfg_prm_rxfcs_mode_, (order == cAtBitOrderLsb) ? 0 : 1);
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtBitOrder HwFcsBitOrderGet(AtPdhPrmController self)
    {
    uint32 regAddr = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset(self);
    uint32 regVal = mChannelHwRead(De1Get(self), regAddr, cAtModulePdh);

    return mRegField(regVal, cAf6_upen_prm_txcfgcr_cfg_prm_txfcs_mode_) ? cAtBitOrderMsb : cAtBitOrderLsb;
    }

static eAtModulePdhRet HwDefault(AtPdhPrmController self)
    {
    uint32 regVal;
    ThaPdhDe1 de1 = De1Get(self);
    uint32 regAddr = cAf6Reg_upen_prm_txcfgcr_Base + PrmOffset(self);

    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_prm_txcfgcr_cfg_prm_txdisstuff_, 0); /* Enable stuffing, FCS Bit Order Mode */
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    regAddr = cAf6Reg_upen_rxprm_cfgstd_Base + PrmOffset(self);
    regVal  = mChannelHwRead(de1, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_rxprm_cfgstd_cfg_prm_rxdisstuff_, 0); /* Enable stuffing, FCS Bit Order Mode */
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtModulePdhRet Init(AtPdhPrmController self)
    {
    HwDefault(self);

    return m_AtPdhPrmControllerMethods->Init(self);
    }

static void OverrideAtPdhPrmController(AtPdhPrmController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhPrmControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhPrmControllerOverride, m_AtPdhPrmControllerMethods, sizeof(m_AtPdhPrmControllerOverride));

        mMethodOverride(m_AtPdhPrmControllerOverride, Init);
        mMethodOverride(m_AtPdhPrmControllerOverride, IsEnabled);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxStandardGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, RxStandardGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, AlarmInterruptGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, AlarmInterruptClear);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxCRBitGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxLBBitGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxLBBitIsEnabled);
        mMethodOverride(m_AtPdhPrmControllerOverride, RxLBBitGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, InterruptMaskGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, InterruptMaskSet);
        mMethodOverride(m_AtPdhPrmControllerOverride, InterruptProcess);
        mMethodOverride(m_AtPdhPrmControllerOverride, CRBitExpectGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxLBBitEnable);
        }

    mMethodsSet(self, &m_AtPdhPrmControllerOverride);
    }

static void OverrideThaPdhPrmController(AtPdhPrmController self)
    {
    ThaPdhPrmController controller = (ThaPdhPrmController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhPrmControllerOverride, mMethodsGet(controller), sizeof(m_ThaPdhPrmControllerOverride));

        mMethodOverride(m_ThaPdhPrmControllerOverride, TxMessageRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, TxByteRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxGoodMessageRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxByteRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxDiscardRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, RxMissRegister);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwEnable);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwTxStandardSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwRxStandardSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwTxCRBitSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwTxLBBitSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwCRBitExpectSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, DefectHistoryReadToClear);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwFcsBitOrderSet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, HwFcsBitOrderGet);
        mMethodOverride(m_ThaPdhPrmControllerOverride, StartVersionHasRxEnabledCfgRegister);
        }

    mMethodsSet(controller, &m_ThaPdhPrmControllerOverride);
    }

static void Override(AtPdhPrmController self)
    {
    OverrideAtPdhPrmController(self);
    OverrideThaPdhPrmController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PdhPrmController);
    }

static AtPdhPrmController ObjectInit(AtPdhPrmController self, AtPdhDe1 de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPrmControllerObjectInit(self, de1) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhPrmController Tha60210021PdhPrmControllerNew(AtPdhDe1 de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhPrmController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, de1);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210021PdhDe1.h
 * 
 * Created Date: Jun 11, 2016
 *
 * Description : DE1 Interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210021_PDH_THA60210021PDHDE1_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210021_PDH_THA60210021PDHDE1_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/util/ThaUtil.h"
#include "../../../default/pdh/ThaPdhDe1Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210021PdhDe1
    {
    tThaPdhDe1 super;
    }tTha60210021PdhDe1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 Tha60210021PdhDe1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210021_PDH_THA60210021PDHDE1_H_ */


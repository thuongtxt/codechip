/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210021PdhPrmInterruptManager.c
 *
 * Created Date: Sep 20, 2017
 *
 * Description : PRM interrupt manager implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/pdh/AtPdhPrmControllerInternal.h"
#include "../../../../../generic/pdh/AtPdhDe1Internal.h"
#include "../../../../default/pdh/prm/ThaPdhPrmInterruptManagerInternal.h"
#include "../Tha60210021ModulePdhMdlPrmReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021PdhPrmInterruptManager
    {
    tThaPdhPrmInterruptManager super;
    }tTha60210021PdhPrmInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods  m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods m_ThaInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePdh PdhModule(AtInterruptManager self)
    {
    return (AtModulePdh)AtInterruptManagerModuleGet(self);
    }

static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    AtModulePdh pdhModule = PdhModule(self);
    uint32 baseAddress = ThaInterruptManagerBaseAddress((ThaInterruptManager)self);
    uint32 intrStatus = AtHalRead(hal, (baseAddress + cAf6Reg_prm_lb_sta_int_Base));
    uint32 intrMask = AtHalRead(hal, (baseAddress + cAf6Reg_prm_lb_en_int_Base));
    uint32 stsvc;
    AtUnused(glbIntr);
    intrStatus &= intrMask;

    for (stsvc = 0; stsvc < 2; stsvc++)
        {
        if (intrStatus & cIteratorMask(stsvc))
            {
            uint32 intrStatus32 = AtHalRead(hal, (baseAddress + (uint32)cAf6Reg_prm_lb_intsta_Base + stsvc));
            uint8 de1InVc;
            uint32 de1Id = 0;

            for (de1InVc = 0; de1InVc < 32; de1InVc++)
                {
                de1Id = (stsvc << 5) | de1InVc;
                if (de1Id >= AtModulePdhNumberOfDe1sGet(pdhModule))
                    return;

                if (intrStatus32 & cIteratorMask(de1InVc))
                    {
                    AtPdhDe1 de1Channel = AtModulePdhDe1Get(pdhModule, de1Id);
                    AtPdhPrmController controller = AtPdhDe1PrmControllerGet(de1Channel);

                    /* Execute channel interrupt. */
                    AtPdhPrmControllerInterruptProcess(controller, 0, (uint8)stsvc, 0, de1InVc, hal);
                    }
                }
            }
        }
    }

static void InterruptHwEnable(ThaInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    if (hal)
        {
        uint32 baseAddress = ThaInterruptManagerBaseAddress(self);
        AtHalWrite(hal, baseAddress + cAf6Reg_prm_lb_en_int_Base, (enable) ? cAf6_prm_lb_en_int_prm_lben_int_Mask : 0x0);
        }
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, InterruptHwEnable);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PdhPrmInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPrmInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha60210021PdhPrmInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newManager, module);
    }

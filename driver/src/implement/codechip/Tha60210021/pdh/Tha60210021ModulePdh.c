/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60210021ModulePdh.c
 *
 * Created Date: Jul 24, 2013
 *
 * Description : PDH module of product 60210021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/pdh/AtPdhPrmControllerInternal.h"
#include "../../../default/pdh/prm/ThaPdhPrmInterruptManager.h"
#include "../../../default/pdh/ThaModulePdhReg.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaPdhPrmController.h"
#include "../../../default/pdh/diag/ThaPdhErrorGenerator.h"
#include "../../Tha60210011/common/Tha602100xxCommon.h"
#include "../man/Tha60210021DeviceReg.h"
#include "../man/Tha60210021Device.h"
#include "Tha60210021ModulePdh.h"
#include "Tha60210021ModulePdhReg.h"
#include "Tha60210021ModulePdhMdlPrmReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cLiuPortMonitorRegAddr 0xF0004a
#define cLiuRxClockFreqRegAddr 0xF00068
#define cLiuTxClockFreqRegAddr 0xF00069

#define cAf6_indctrl_pen_rd_Val                                                                    1
#define cAf6_indctrl_pen_wr_Val                                                                    0

#define cAf6_indctrl_pen_rd_nwr_Mask                                                               cBit6
#define cAf6_indctrl_pen_rd_nwr_Shift                                                              6

#define cAf6_indctrl_pen_chn_id_Mask                                                               cBit5_0
#define cAf6_indctrl_pen_chn_id_Shift                                                              0

#define cAf6_dej1_rxfrm_chn_intr_stat_Intr_Mask(id)             (1U << (id))
#define cAf6_dej1_rx_framer_per_stsvc_intr_Mask(id)             (1U << (id))

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210021ModulePdh)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods 	m_AtObjectOverride;
static tAtModuleMethods     m_AtModuleOverride;
static tAtModulePdhMethods  m_AtModulePdhOverride;
static tThaModulePdhMethods m_ThaModulePdhOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tThaModulePdhMethods *m_ThaModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CasSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }
 
static eAtRet MakeRequest(AtModulePdh self, AtPdhDe1 de1, uint32 data, uint8 requestType)
    {
    static const uint32 cTimeoutMs = 100;
    uint32 elapsedTimeMs = 0;
    uint32 regVal;
    tAtOsalCurTime startTime, curTime;
    uint32 baseAddress = ThaModulePdhBaseAddress((ThaModulePdh)self);

    /* Engine may be busy */
    regVal = mModuleHwRead(self, cAf6Reg_indctrl_pen + baseAddress);
    if (regVal & cAf6_indctrl_pen_pinden_Mask)
        return cAtErrorDevBusy;

    /* Make request */
    if (requestType == cAf6_indctrl_pen_wr_Val)
        mModuleHwWrite(self, cAf6Reg_indwr_pen_Base + baseAddress, data);

    mRegFieldSet(regVal, cAf6_indctrl_pen_pinden_, 1);
    mRegFieldSet(regVal, cAf6_indctrl_pen_rd_nwr_, requestType);
    mRegFieldSet(regVal, cAf6_indctrl_pen_chn_id_, AtChannelIdGet((AtChannel)de1));
    mModuleHwWrite(self, cAf6Reg_indctrl_pen + baseAddress, regVal);

    /* Wait for hardware */
    AtOsalCurTimeGet(&startTime);
    while (elapsedTimeMs < cTimeoutMs)
        {
        regVal = mModuleHwRead(self , cAf6Reg_indctrl_pen + baseAddress);
        if ((regVal & cAf6_indctrl_pen_pinden_Mask) == 0)
            return cAtOk;

        /* Retry */
        AtOsalCurTimeGet(&curTime);
        elapsedTimeMs = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorIndrAcsTimeOut;
    }

static AtOsalMutex LoopcodeLocker(AtModulePdh self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (mThis(self)->loopCodeMutex == NULL)
        mThis(self)->loopCodeMutex = mMethodsGet(osal)->MutexCreate(osal);

    return mThis(self)->loopCodeMutex;
    }

static void LoopcodeLock(AtModulePdh self)
    {
    AtOsalMutexLock(LoopcodeLocker(self));
    }

static void LoopcodeUnLock(AtModulePdh self)
    {
    AtOsalMutexUnLock(LoopcodeLocker(self));
    }

static eAtRet LoopcodeIndirectRead(AtModulePdh self, AtPdhDe1 de1, uint32 *data)
    {
    static uint32 cAnyValue = 0;
    eAtRet ret;
    uint32 baseAddress = ThaModulePdhBaseAddress((ThaModulePdh)self);

    LoopcodeLock(self);
    ret = MakeRequest(self, de1, cAnyValue, cAf6_indctrl_pen_rd_Val);
    if (ret == cAtOk)
        *data = mModuleHwRead(self , cAf6Reg_indrd_pen_Base + baseAddress);

    LoopcodeUnLock(self);

    return ret;
    }

static eAtRet LoopcodeIndirectWrite(AtModulePdh self, AtPdhDe1 de1, uint32 data)
    {
    eAtRet ret;

    LoopcodeLock(self);
    ret = MakeRequest(self, de1, data, cAf6_indctrl_pen_wr_Val);
    LoopcodeUnLock(self);

    return ret;
    }

static uint32 Rxdestuff_ctrl0_Base(ThaModulePdh self)
    {
    return cAf6Reg_upen_rxdestuff_ctrl0_Base + ThaModulePdhPrmBaseAddress((ThaModulePdh)self);
    }

static uint32 Txcfg_ctrl0_Base(ThaModulePdh self)
    {
    return cAf6Reg_upen_txcfg_ctrl0_Base + ThaModulePdhPrmBaseAddress((ThaModulePdh)self);
    }

static eAtRet PrmDefaultSet(AtModule self)
    {
    uint32 regAddr, regVal;

    if (!ThaModulePdhPrmIsSupported((ThaModulePdh)self))
        return cAtOk;

    /* Rx Global Default */
    regAddr = Rxdestuff_ctrl0_Base((ThaModulePdh)self);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_rxdestuff_ctrl0_cfg_en_ivl_, 1); /* Enable Invalid PRM inform to PM */
    mRegFieldSet(regVal, cAf6_upen_rxdestuff_ctrl0_addctrmod_, 0); /* Disable add/ctrl monitor */
    mRegFieldSet(regVal, cAf6_upen_rxdestuff_ctrl0_sapimon_, 1);
    mRegFieldSet(regVal, cAf6_upen_rxdestuff_ctrl0_sapimod_, 0);
    mRegFieldSet(regVal, cAf6_upen_rxdestuff_ctrl0_fcsmon_, 1);    /* Enable FCS monitor */
    mRegFieldSet(regVal, cAf6_upen_rxdestuff_ctrl0_fcsmod32_, 0);  /* FCS#16 */
    mRegFieldSet(regVal, cAf6_upen_rxdestuff_ctrl0_cfg_crmon_, 0); /* Disable CR bit compare */
    mModuleHwWrite(self, regAddr, regVal);

    /* Tx Global Default */
    regAddr = Txcfg_ctrl0_Base((ThaModulePdh)self);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_txcfg_ctrl0_flagmod_, 0);       /* 2 Flag */
    mRegFieldSet(regVal, cAf6_upen_txcfg_ctrl0_fcsinscfg_, 1);   /* Enable FCS insert */
    mRegFieldSet(regVal, cAf6_upen_txcfg_ctrl0_fcsmod32_, 0);      /* FCS#16 */
    mRegFieldSet(regVal, cAf6_upen_txcfg_ctrl0_ctraddinscfg_, 0);  /* Disable Add/Ctrl insert */
    mRegFieldSet(regVal, cAf6_upen_txcfg_ctrl0_sapinscfg_, 0);     /* Disable SAPI insert */
    mRegFieldSet(regVal, cAf6_upen_txcfg_ctrl0_sapimod_, 0);
    mRegFieldSet(regVal, cAf6_upen_txcfg_ctrl0_idlemod_, 0);       /* Idle: flag mode */
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static void AisCorrectionDefaultSet(AtModule self)
    {
    mModuleHwWrite(self, cAf6_Reg_Ds1_TxAis_Correction, 0);
    mModuleHwWrite(self, cAf6_Reg_Ds1_TxAis_Correction1, 0);
    }

static eAtRet PartDefaultSet(AtModule self, uint8 partId)
    {
    mModuleHwWrite(self, ThaModulePdhBaseAddress((ThaModulePdh)self) + ThaModulePdhPartOffset(self, partId), cThaRegPdhGlbCtrlRstVal);
    PrmDefaultSet(self);
    return cAtOk;
    }

static eBool InbandLoopcodeIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportBom(ThaModulePdh self)
    {
    AtUnused(self);
    return 0;
    }

static eBool ShouldInvertLiuClock(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasDe3Interrupt(ThaModulePdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    AtUnused(ipCore);
    return cAtFalse;
    }

static eBool HasDe1Interrupt(ThaModulePdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(ipCore);
    return (glbIntr & cAf6_inten_status_DE1IntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static void De1InterruptProcess(ThaModulePdh self, uint32 de1Intr, AtHal hal)
    {
    const uint32 baseAddress = ThaModulePdhBaseAddress(self);
    uint32 intrMask = AtHalRead(hal, baseAddress + cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl);
    uint32 intrStatus = AtHalRead(hal, baseAddress + cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat);
    uint32 stsvc;
    AtUnused(de1Intr);

    intrStatus &= intrMask;

    for (stsvc = 0; stsvc < 2; stsvc++)
        {
        if (intrStatus & cAf6_dej1_rx_framer_per_stsvc_intr_Mask(stsvc))
            {
            uint32 intrStatus32 = AtHalRead(hal, baseAddress + cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat(stsvc));
            uint8 de1InVc;
            uint32 de1Id = 0;

            for (de1InVc = 0; de1InVc < 32; de1InVc++)
                {
                de1Id = (stsvc << 5) | de1InVc;
                if (de1Id >= AtModulePdhNumberOfDe1sGet((AtModulePdh)self))
                    return;

                if (intrStatus32 & cAf6_dej1_rxfrm_chn_intr_stat_Intr_Mask(de1InVc))
                    {
                    AtPdhDe1 de1Channel = AtModulePdhDe1Get((AtModulePdh)self, de1Id);

                    /* Execute channel interrupt. */
                    AtChannelInterruptProcess((AtChannel)de1Channel, de1Id);
                    }
                }
            }
        }
    }

static void ErrorGeneratorDelete(ThaModulePdh self)
    {
    AtObjectDelete((AtObject)mThis(self)->de1RxErrorGenerator);
    mThis(self)->de1RxErrorGenerator = NULL;
    AtObjectDelete((AtObject)mThis(self)->de1TxErrorGenerator);
    mThis(self)->de1TxErrorGenerator = NULL;
    }

static void ErrorGeneratorDefaultSet(ThaModulePdh self)
    {
    ThaPdhDe1TxErrorGeneratorHwDefaultSet(self);
    ThaPdhDe1RxErrorGeneratorHwDefaultSet(self);
    ErrorGeneratorDelete(self);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    PartDefaultSet(self, 0);

    if (ThaModulePdhLiuForcedAisIsSupported((ThaModulePdh)self))
        AisCorrectionDefaultSet(self);

    if (ThaDeviceErrorGeneratorIsSupported((ThaDevice)AtModuleDeviceGet(self)))
        ErrorGeneratorDefaultSet((ThaModulePdh)self);

    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static uint32 NumberOfDe1sGet(AtModulePdh self)
    {
    AtUnused(self);
    return 48;
    }

static AtPdhDe1 De1Create(AtModulePdh self, uint32 de1Id)
    {
    return Tha60210021PdhDe1New(de1Id, self);
    }

static eAtRet LiuFrequencyMonitorSet(AtModulePdh self, AtPdhChannel channel)
    {
    uint32 de1Id = AtChannelIdGet((AtChannel)channel);
    mModuleHwWrite(self, cLiuPortMonitorRegAddr, de1Id);
    return cAtOk;
    }

static eAtRet LiuFrequencyMonitorGet(AtModulePdh self, tAtModulePdhLiuMonitor *clockMonitor)
    {
    uint32 de1Id = mModuleHwRead(self, cLiuPortMonitorRegAddr);
    clockMonitor->rxClockRate = mModuleHwRead(self, cLiuRxClockFreqRegAddr);
    clockMonitor->txClockRate = mModuleHwRead(self, cLiuTxClockFreqRegAddr);
    clockMonitor->channel = (AtPdhChannel)AtModulePdhDe1Get(self, de1Id);
    return cAtOk;
    }

static uint32 De1RxFrmrPerChnCurrentAlmBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_per_chn_curr_stat_Base;
    }

static uint32 De1RxFrmrPerChnInterruptAlmBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_dej1_rx_framer_per_chn_intr_stat_Base;
    }

static eBool DetectDe1LosByAllZeroPattern(ThaModulePdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x10, 0x07, 00);
    }

static uint32 StartVersionSupportDe1Ssm(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x10, 0x13, 00);
    }

static uint32 StartVersionSupportDe1SsmSendingReadyIndication(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x10, 0x20, 00);
    }

static uint32 StartVersionSupportAisDownStream(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x10, 0x15, 00);
    }

static eBool PrmFcsBitOrderIsSupported(ThaModulePdh self)
    {
    uint32 startVersionSupport;
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (ThaDeviceIsEp((ThaDevice)device) || AtDeviceIsSimulated(device))
        return cAtTrue;

    startVersionSupport = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4624);
    if (ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(device)) >= startVersionSupport)
        return cAtTrue;

    return cAtFalse;
    }

static eAtModulePdhRet PrmCrCompareEnable(AtModulePdh self, eBool enable)
    {
    uint32 regAddress, regValue;

    if (!ThaModulePdhPrmIsSupported((ThaModulePdh)self))
        return cAtErrorModeNotSupport;

    regAddress = Rxdestuff_ctrl0_Base((ThaModulePdh)self);
    regValue = mModuleHwRead(self, regAddress);
    mRegFieldSet(regValue, cAf6_upen_rxdestuff_ctrl0_cfg_crmon_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddress, regValue);

    return cAtOk;
    }

static eBool PrmCrCompareIsEnabled(AtModulePdh self)
    {
    uint32 regAddress = Rxdestuff_ctrl0_Base((ThaModulePdh)self);
    uint32 regValue = mModuleHwRead(self, regAddress);
    return mRegField(regValue, cAf6_upen_rxdestuff_ctrl0_cfg_crmon_) ? cAtTrue : cAtFalse;
    }

static AtPdhPrmController PrmControllerObjectCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return Tha60210021PdhPrmControllerNew(de1);
    }

static ThaPdhDebugger DebuggerObjectCreate(ThaModulePdh self)
    {
    return Tha60210021PdhDebuggerNew(self);
    }

static ThaVersionReader VersionReader(ThaModulePdh self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartDateSupportTxLiuAis(ThaModulePdh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x12, 0x01, 0x39);
    }

static uint32 StartBuiltNumberSupportTxLiuAis(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x3, 0x9, 0x01);
    }

static eBool LiuForcedAisIsSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceVersionNumber(device) > StartDateSupportTxLiuAis(self))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) == StartDateSupportTxLiuAis(self))
        return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportTxLiuAis(self)) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static eBool ForcedAisCorrectionIsSupported(ThaModulePdh self)
    {
    return LiuForcedAisIsSupported(self);
    }

static AtErrorGenerator De1TxErrorGeneratorGet(ThaModulePdh self)
    {
    if (!ThaDeviceErrorGeneratorIsSupported((ThaDevice)AtModuleDeviceGet((AtModule)self)))
        return NULL;

    if (mThis(self)->de1TxErrorGenerator == NULL)
        mThis(self)->de1TxErrorGenerator = ThaPdhDe1TxErrorGeneratorNew(NULL);

    return mThis(self)->de1TxErrorGenerator;
    }

static AtErrorGenerator De1RxErrorGeneratorGet(ThaModulePdh self)
    {
    if (!ThaDeviceErrorGeneratorIsSupported((ThaDevice)AtModuleDeviceGet((AtModule)self)))
        return NULL;

    if (mThis(self)->de1RxErrorGenerator == NULL)
        mThis(self)->de1RxErrorGenerator = ThaPdhDe1RxErrorGeneratorNew(NULL);

    return mThis(self)->de1RxErrorGenerator;
    }

static void Delete(AtObject self)
    {
    AtOsalMutexDestroy(mThis(self)->loopCodeMutex);
    mThis(self)->loopCodeMutex = NULL;
    ErrorGeneratorDelete((ThaModulePdh)self);

    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210021ModulePdh object = (Tha60210021ModulePdh)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(de1TxErrorGenerator);
    mEncodeObject(de1RxErrorGenerator);
    mEncodeNone(loopCodeMutex);
    }

static eBool ShouldDetectAisAllOnesWithUnframedMode(ThaModulePdh self)
    {
    return Tha602100xxModulePdhShouldDetectAisAllOnesWithUnframedMode(self);
    }

static eBool De1LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    return Tha602100xxModulePdhDe1LiuShouldReportLofWhenLos(self);
    }

static eBool De3LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    return Tha602100xxModulePdhDe3LiuShouldReportLofWhenLos(self);
    }

static eBool CanHotChangeFrameType(ThaModulePdh self)
    {
    AtUnused(self);

    /* Frame type can be hot changed to reduce register
     * accesses during add/remove Pseudowire */
    return cAtTrue;
    }

static uint32 StartVersionSupportDe1LomfConsequentialAction(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x04, 0x06, 0x4647);
    }

static uint32 StartVersionSupportAlarmForwardingStatus(ThaModulePdh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x17, 0x10, 0x05, 0x46);
    }

static AtInterruptManager PrmInterruptManagerObjectCreate(ThaModulePdh self)
    {
    return Tha60210021PdhPrmInterruptManagerNew((AtModule)self);
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 De1PrmOffset(ThaModulePdh self, ThaPdhDe1 de1)
    {
    uint32 baseAddress = ThaModulePdhPrmBaseAddress(self);
    return baseAddress + ThaPdhDe1DefaultOffset(de1);
    }

static uint32 DefaultInbandLoopcodeThreshold(ThaModulePdh self)
    {
    static uint32 cPdhSlice24CoreClockInHz = 112500000; /* Hz or clocks per second */
    uint32 numClocksIn5s = (3 * cPdhSlice24CoreClockInHz);
    AtUnused(self);
    return numClocksIn5s;
    }

static void OverrideAtObject(AtModulePdh self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(self), sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, NumberOfDe1sGet);
        mMethodOverride(m_AtModulePdhOverride, De1Create);
        mMethodOverride(m_AtModulePdhOverride, PrmCrCompareEnable);
        mMethodOverride(m_AtModulePdhOverride, PrmCrCompareIsEnabled);
        mMethodOverride(m_AtModulePdhOverride, LiuFrequencyMonitorGet);
        mMethodOverride(m_AtModulePdhOverride, LiuFrequencyMonitorSet);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, m_ThaModulePdhMethods, sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, ShouldInvertLiuClock);
        mMethodOverride(m_ThaModulePdhOverride, InbandLoopcodeIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De1RxFrmrPerChnCurrentAlmBase);
        mMethodOverride(m_ThaModulePdhOverride, De1RxFrmrPerChnInterruptAlmBase);
        mMethodOverride(m_ThaModulePdhOverride, CasSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportBom);
        mMethodOverride(m_ThaModulePdhOverride, DetectDe1LosByAllZeroPattern);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1Ssm);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1SsmSendingReadyIndication);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportPrm);
        mMethodOverride(m_ThaModulePdhOverride, PrmControllerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportAisDownStream);
        mMethodOverride(m_ThaModulePdhOverride, HasDe3Interrupt);
        mMethodOverride(m_ThaModulePdhOverride, HasDe1Interrupt);
        mMethodOverride(m_ThaModulePdhOverride, De1InterruptProcess);
        mMethodOverride(m_ThaModulePdhOverride, DebuggerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, LiuForcedAisIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, ForcedAisCorrectionIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De1TxErrorGeneratorGet);
        mMethodOverride(m_ThaModulePdhOverride, De1RxErrorGeneratorGet);
        mMethodOverride(m_ThaModulePdhOverride, ShouldDetectAisAllOnesWithUnframedMode);
        mMethodOverride(m_ThaModulePdhOverride, PrmFcsBitOrderIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De1LiuShouldReportLofWhenLos);
        mMethodOverride(m_ThaModulePdhOverride, De3LiuShouldReportLofWhenLos);
        mMethodOverride(m_ThaModulePdhOverride, CanHotChangeFrameType);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1LomfConsequentialAction);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportAlarmForwardingStatus);
        mMethodOverride(m_ThaModulePdhOverride, PrmInterruptManagerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, De1PrmOffset);
        mMethodOverride(m_ThaModulePdhOverride, DefaultInbandLoopcodeThreshold);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideThaModulePdh(self);
    OverrideAtModulePdh(self);
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModulePdh);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModulePdhObjectInit((AtModulePdh)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60210021ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

eAtRet Tha60210021ModulePdhlcIndirectRead(AtModulePdh self, AtPdhDe1 de1, uint32 *data)
    {
    if (self)
        return LoopcodeIndirectRead(self, de1, data);
    return cAtError;
    }

eAtRet Tha60210021ModulePdhlcIndirectWrite(AtModulePdh self, AtPdhDe1 de1, uint32 data)
    {
    if (self)
        return LoopcodeIndirectWrite(self, de1, data);
    return cAtError;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH module name
 *
 * File        : Tha60210011ModuleSdh.h
 *
 * Created Date: Mar 26, 2015
 *
 * Description : 60210011SDH module header
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA60210011MODULEPDH_H_
#define _THA60210011MODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaPdhPwProduct/pdh/ThaPdhPwProductModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210021ModulePdh
    {
    tThaPdhPwProductModulePdh super;

    /* Private data */
    AtErrorGenerator de1TxErrorGenerator;
    AtErrorGenerator de1RxErrorGenerator;
    AtOsalMutex loopCodeMutex;
    } tTha60210021ModulePdh;

typedef struct tTha60210021ModulePdh * Tha60210021ModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210021ModulePdhlcIndirectRead(AtModulePdh self, AtPdhDe1 de1, uint32 *data);
eAtRet Tha60210021ModulePdhlcIndirectWrite(AtModulePdh self, AtPdhDe1 de1, uint32 data);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210011MODULEPDH_H_ */


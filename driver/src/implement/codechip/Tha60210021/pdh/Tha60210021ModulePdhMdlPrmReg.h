/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60210021ModulePdhMdlPrmReg.h
 * 
 * Created Date: Sep 22, 2015
 *
 * Description : Register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021MODULEPDHMDLPRMREG_H_
#define _THA60210021MODULEPDHMDLPRMREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : BUFFER STORE TX INFO FROM PDH FOR DEBUG BOARD
Reg Addr   : 0x2000 - 0x27FF
Reg Formula:
    Where  :
Reg Desc   :
STORE TX INFO FROM PDH

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_store_txinfo_Base                                                                  0x2000
#define cAf6Reg_upen_store_txinfo                                                                       0x2000
#define cAf6Reg_upen_store_txinfo_WidthVal                                                                  32
#define cAf6Reg_upen_store_txinfo_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: dta_txinfo_pdh
BitField Type: R/W
BitField Desc: info from PDH
BitField Bits: [17:08]
--------------------------------------*/
#define cAf6_upen_store_txinfo_dta_txinfo_pdh_Bit_Start                                                      8
#define cAf6_upen_store_txinfo_dta_txinfo_pdh_Bit_End                                                       17
#define cAf6_upen_store_txinfo_dta_txinfo_pdh_Mask                                                    cBit17_8
#define cAf6_upen_store_txinfo_dta_txinfo_pdh_Shift                                                          8
#define cAf6_upen_store_txinfo_dta_txinfo_pdh_MaxVal                                                     0x3ff
#define cAf6_upen_store_txinfo_dta_txinfo_pdh_MinVal                                                       0x0
#define cAf6_upen_store_txinfo_dta_txinfo_pdh_RstVal                                                       0x0

/*--------------------------------------
BitField Name: dta_txresevre
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_upen_store_txinfo_dta_txresevre_Bit_Start                                                       6
#define cAf6_upen_store_txinfo_dta_txresevre_Bit_End                                                         7
#define cAf6_upen_store_txinfo_dta_txresevre_Mask                                                      cBit7_6
#define cAf6_upen_store_txinfo_dta_txresevre_Shift                                                           6
#define cAf6_upen_store_txinfo_dta_txresevre_MaxVal                                                        0x3
#define cAf6_upen_store_txinfo_dta_txresevre_MinVal                                                        0x0
#define cAf6_upen_store_txinfo_dta_txresevre_RstVal                                                        0x0

/*--------------------------------------
BitField Name: txcid
BitField Type: R/W
BitField Desc: channel ID corresponding with INFO from PDH
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_upen_store_txinfo_txcid_Bit_Start                                                               0
#define cAf6_upen_store_txinfo_txcid_Bit_End                                                                 5
#define cAf6_upen_store_txinfo_txcid_Mask                                                              cBit5_0
#define cAf6_upen_store_txinfo_txcid_Shift                                                                   0
#define cAf6_upen_store_txinfo_txcid_MaxVal                                                               0x3f
#define cAf6_upen_store_txinfo_txcid_MinVal                                                                0x0
#define cAf6_upen_store_txinfo_txcid_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : BUFFER STORE RX INFO TO PM FOR DEBUG BOARD
Reg Addr   : 0x3000 - 0x37FF
Reg Formula:
    Where  :
Reg Desc   :
STORE RX INFO TO PM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_store_rx2pm_Base                                                                   0x3000
#define cAf6Reg_upen_store_rx2pm                                                                        0x3000
#define cAf6Reg_upen_store_rx2pm_WidthVal                                                                   32
#define cAf6Reg_upen_store_rx2pm_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: dta_rxinfo_pm
BitField Type: R/W
BitField Desc: info to PM
BitField Bits: [16:08]
--------------------------------------*/
#define cAf6_upen_store_rx2pm_dta_rxinfo_pm_Bit_Start                                                        8
#define cAf6_upen_store_rx2pm_dta_rxinfo_pm_Bit_End                                                         16
#define cAf6_upen_store_rx2pm_dta_rxinfo_pm_Mask                                                      cBit16_8
#define cAf6_upen_store_rx2pm_dta_rxinfo_pm_Shift                                                            8
#define cAf6_upen_store_rx2pm_dta_rxinfo_pm_MaxVal                                                       0x1ff
#define cAf6_upen_store_rx2pm_dta_rxinfo_pm_MinVal                                                         0x0
#define cAf6_upen_store_rx2pm_dta_rxinfo_pm_RstVal                                                         0x0

/*--------------------------------------
BitField Name: rx_reserve
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_upen_store_rx2pm_rx_reserve_Bit_Start                                                           6
#define cAf6_upen_store_rx2pm_rx_reserve_Bit_End                                                             7
#define cAf6_upen_store_rx2pm_rx_reserve_Mask                                                          cBit7_6
#define cAf6_upen_store_rx2pm_rx_reserve_Shift                                                               6
#define cAf6_upen_store_rx2pm_rx_reserve_MaxVal                                                            0x3
#define cAf6_upen_store_rx2pm_rx_reserve_MinVal                                                            0x0
#define cAf6_upen_store_rx2pm_rx_reserve_RstVal                                                            0x0

/*--------------------------------------
BitField Name: rxcid
BitField Type: R/W
BitField Desc: channel ID corresponding with INFO to PM
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_upen_store_rx2pm_rxcid_Bit_Start                                                                0
#define cAf6_upen_store_rx2pm_rxcid_Bit_End                                                                  5
#define cAf6_upen_store_rx2pm_rxcid_Mask                                                               cBit5_0
#define cAf6_upen_store_rx2pm_rxcid_Shift                                                                    0
#define cAf6_upen_store_rx2pm_rxcid_MaxVal                                                                0x3f
#define cAf6_upen_store_rx2pm_rxcid_MinVal                                                                 0x0
#define cAf6_upen_store_rx2pm_rxcid_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : BUFFER BYTE TX MESSAGE AFTER STUFF FOR DEBUG BOARD
Reg Addr   : 0x4000 - 0x47FF
Reg Formula:
    Where  :
Reg Desc   :
Buffer byte Tx After stuff

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_store_txmess_Base                                                                  0x4000
#define cAf6Reg_upen_store_txmess                                                                       0x4000
#define cAf6Reg_upen_store_txmess_WidthVal                                                                  32
#define cAf6Reg_upen_store_txmess_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: dta_txmess_aft
BitField Type: R/W
BitField Desc: byte data mesage before stuff
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_store_txmess_dta_txmess_aft_Bit_Start                                                      0
#define cAf6_upen_store_txmess_dta_txmess_aft_Bit_End                                                        7
#define cAf6_upen_store_txmess_dta_txmess_aft_Mask                                                     cBit7_0
#define cAf6_upen_store_txmess_dta_txmess_aft_Shift                                                          0
#define cAf6_upen_store_txmess_dta_txmess_aft_MaxVal                                                      0xff
#define cAf6_upen_store_txmess_dta_txmess_aft_MinVal                                                       0x0
#define cAf6_upen_store_txmess_dta_txmess_aft_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : BUFFER BYTE RX MESSAGE BEFORE DESTUFF FOR DEBUG BOARD
Reg Addr   : 0x5000 - 0x57FF
Reg Formula:
    Where  :
Reg Desc   :
Buffer byte Rx before destuff PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_store_rxmess_Base                                                                  0x5000
#define cAf6Reg_upen_store_rxmess                                                                       0x5000
#define cAf6Reg_upen_store_rxmess_WidthVal                                                                  32
#define cAf6Reg_upen_store_rxmess_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: dta_rxmess_bef
BitField Type: R/W
BitField Desc: byte data mesage before destuff
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_store_rxmess_dta_rxmess_bef_Bit_Start                                                      0
#define cAf6_upen_store_rxmess_dta_rxmess_bef_Bit_End                                                        7
#define cAf6_upen_store_rxmess_dta_rxmess_bef_Mask                                                     cBit7_0
#define cAf6_upen_store_rxmess_dta_rxmess_bef_Shift                                                          0
#define cAf6_upen_store_rxmess_dta_rxmess_bef_MaxVal                                                      0xff
#define cAf6_upen_store_rxmess_dta_rxmess_bef_MinVal                                                       0x0
#define cAf6_upen_store_rxmess_dta_rxmess_bef_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : Version
Reg Addr   : 0x6000
Reg Formula:
    Where  :
Reg Desc   :
config ID to monitor tx message

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ver_Base                                                                       0x6000
#define cAf6Reg_upen_cfg_ver                                                                            0x6000
#define cAf6Reg_upen_cfg_ver_WidthVal                                                                       32
#define cAf6Reg_upen_cfg_ver_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: version
BitField Type: R_O
BitField Desc: 32'hDDAA_3110
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cfg_ver_version_Bit_Start                                                                  0
#define cAf6_upen_cfg_ver_version_Bit_End                                                                   31
#define cAf6_upen_cfg_ver_version_Mask                                                                cBit31_0
#define cAf6_upen_cfg_ver_version_Shift                                                                      0
#define cAf6_upen_cfg_ver_version_MaxVal                                                            0xffffffff
#define cAf6_upen_cfg_ver_version_MinVal                                                                   0x0
#define cAf6_upen_cfg_ver_version_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE MONITOR FOR DEBUG BOARD
Reg Addr   : 0x6001
Reg Formula:
    Where  :
Reg Desc   :
config enable monitor data before stuff

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_debug_Base                                                                     0x6001
#define cAf6Reg_upen_cfg_debug                                                                          0x6001
#define cAf6Reg_upen_cfg_debug_WidthVal                                                                     32
#define cAf6Reg_upen_cfg_debug_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: out_cfg_bar
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_cfg_debug_out_cfg_bar_Bit_Start                                                           16
#define cAf6_upen_cfg_debug_out_cfg_bar_Bit_End                                                             31
#define cAf6_upen_cfg_debug_out_cfg_bar_Mask                                                         cBit31_16
#define cAf6_upen_cfg_debug_out_cfg_bar_Shift                                                               16
#define cAf6_upen_cfg_debug_out_cfg_bar_MaxVal                                                          0xffff
#define cAf6_upen_cfg_debug_out_cfg_bar_MinVal                                                             0x0
#define cAf6_upen_cfg_debug_out_cfg_bar_RstVal                                                             0x0

/*--------------------------------------
BitField Name: out_info_force
BitField Type: R/W
BitField Desc: 10 bits info force [15:13] : 3'd0 : to set bit G1 3'd1 :  to set
bit G2 3'd2 :  to set bit G3 3'd3 : to set bit G4 3'd4 : to set bit G5 3'd5 :
to set bit G6 [12] : to set bit SE [11] : to set bit FE [10] : to set bit LV [9]
: to set bit SL [8] : to set bit LB [7:6] to set 2bit NmNi (00-01-10-11)
BitField Bits: [15:06]
--------------------------------------*/
#define cAf6_upen_cfg_debug_out_info_force_Bit_Start                                                         6
#define cAf6_upen_cfg_debug_out_info_force_Bit_End                                                          15
#define cAf6_upen_cfg_debug_out_info_force_Mask                                                       cBit15_6
#define cAf6_upen_cfg_debug_out_info_force_Shift                                                             6
#define cAf6_upen_cfg_debug_out_info_force_MaxVal                                                        0x3ff
#define cAf6_upen_cfg_debug_out_info_force_MinVal                                                          0x0
#define cAf6_upen_cfg_debug_out_info_force_RstVal                                                          0x0

/*--------------------------------------
BitField Name: out_cfg_selinfo2
BitField Type: R/W
BitField Desc: select all 10 bit INFO from PRM FORCE , (1) is enable, (0) is
disable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_cfg_debug_out_cfg_selinfo2_Bit_Start                                                       2
#define cAf6_upen_cfg_debug_out_cfg_selinfo2_Bit_End                                                         2
#define cAf6_upen_cfg_debug_out_cfg_selinfo2_Mask                                                        cBit2
#define cAf6_upen_cfg_debug_out_cfg_selinfo2_Shift                                                           2
#define cAf6_upen_cfg_debug_out_cfg_selinfo2_MaxVal                                                        0x1
#define cAf6_upen_cfg_debug_out_cfg_selinfo2_MinVal                                                        0x0
#define cAf6_upen_cfg_debug_out_cfg_selinfo2_RstVal                                                        0x0

/*--------------------------------------
BitField Name: out_cfg_selinfo1
BitField Type: R/W
BitField Desc: select just 8 bit INFO from PRM FORCE but except 2 bit NmNi, (1)
is enable, (0) is disable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_Bit_Start                                                       1
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_Bit_End                                                         1
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_Mask                                                        cBit1
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_Shift                                                           1
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_MaxVal                                                        0x1
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_MinVal                                                        0x0
#define cAf6_upen_cfg_debug_out_cfg_selinfo1_RstVal                                                        0x1

/*--------------------------------------
BitField Name: out_cfg_dis
BitField Type: R/W
BitField Desc: disable monitor, reset address buffer monitor, (1) is disable,
(0) is enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_debug_out_cfg_dis_Bit_Start                                                            0
#define cAf6_upen_cfg_debug_out_cfg_dis_Bit_End                                                              0
#define cAf6_upen_cfg_debug_out_cfg_dis_Mask                                                             cBit0
#define cAf6_upen_cfg_debug_out_cfg_dis_Shift                                                                0
#define cAf6_upen_cfg_debug_out_cfg_dis_MaxVal                                                             0x1
#define cAf6_upen_cfg_debug_out_cfg_dis_MinVal                                                             0x0
#define cAf6_upen_cfg_debug_out_cfg_dis_RstVal                                                             0x1


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ID TO MONITOR FOR DEBUG BOARD
Reg Addr   : 0x6002
Reg Formula:
    Where  :
Reg Desc   :
config ID to monitor tx message

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgid_mon_Base                                                                     0x6002
#define cAf6Reg_upen_cfgid_mon                                                                          0x6002
#define cAf6Reg_upen_cfgid_mon_WidthVal                                                                     32
#define cAf6Reg_upen_cfgid_mon_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: out_cfgid_mon
BitField Type: R/W
BitField Desc: ID which is configured to monitor
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_upen_cfgid_mon_out_cfgid_mon_Bit_Start                                                          0
#define cAf6_upen_cfgid_mon_out_cfgid_mon_Bit_End                                                            5
#define cAf6_upen_cfgid_mon_out_cfgid_mon_Mask                                                         cBit5_0
#define cAf6_upen_cfgid_mon_out_cfgid_mon_Shift                                                              0
#define cAf6_upen_cfgid_mon_out_cfgid_mon_MaxVal                                                          0x3f
#define cAf6_upen_cfgid_mon_out_cfgid_mon_MinVal                                                           0x0
#define cAf6_upen_cfgid_mon_out_cfgid_mon_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG PRM BIT C/R & STANDARD TX
Reg Addr   : 0x0000 - 0x002F
Reg Formula: 0x0000+$PRMID
    Where  :
           + $PRMID (0-47) : PRM ID
Reg Desc   :
Config PRM bit C/R & Standard

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_prm_txcfgcr_Base                                                                   0x0000
#define cAf6Reg_upen_prm_txcfgcr(PRMID)                                                       (0x0000+(PRMID))
#define cAf6Reg_upen_prm_txcfgcr_WidthVal                                                                   32
#define cAf6Reg_upen_prm_txcfgcr_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cfg_prmen_tx
BitField Type: R/W
BitField Desc: config enable Tx, (0) is disable (1) is enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Bit_Start                                                         3
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Bit_End                                                           3
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Mask                                                          cBit3
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Shift                                                             3
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_MaxVal                                                          0x1
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_MinVal                                                          0x0
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_RstVal                                                          0x0

/*--------------------------------------
BitField Name: cfg_prm_txdisstuff
BitField Type: R/W
BitField Desc: config enable stuff message, (1) is disable, (0) is enbale
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prm_txdisstuff_Bit_Start                                                   2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txdisstuff_Bit_End                                                     2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txdisstuff_Mask                                                    cBit2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txdisstuff_Shift                                                       2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txdisstuff_MaxVal                                                    0x1
#define cAf6_upen_prm_txcfgcr_cfg_prm_txdisstuff_MinVal                                                    0x0
#define cAf6_upen_prm_txcfgcr_cfg_prm_txdisstuff_RstVal                                                    0x0
/* Hardware change 2016-June-10 */
#define cAf6_upen_prm_txcfgcr_cfg_prm_txfcs_mode_Mask                                                     cBit2
#define cAf6_upen_prm_txcfgcr_cfg_prm_txfcs_mode_Shift                                                        2

/*--------------------------------------
BitField Name: cfg_prmstd_tx
BitField Type: R/W
BitField Desc: config standard Tx, (0) is ANSI, (1) is AT&T
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Bit_Start                                                        1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Bit_End                                                          1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Mask                                                         cBit1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Shift                                                            1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_MaxVal                                                         0x1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_MinVal                                                         0x0
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_RstVal                                                         0x0

/*--------------------------------------
BitField Name: cfg_prm_cr
BitField Type: R/W
BitField Desc: config bit command/respond PRM message
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Bit_Start                                                           0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Bit_End                                                             0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Mask                                                            cBit0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Shift                                                               0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_MaxVal                                                            0x1
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_MinVal                                                            0x0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG PRM BIT LB & STANDARD Tx
Reg Addr   : 0x0300- - 0x032F
Reg Formula: 0x0300+$PRMID
    Where  :
           + $PRMID (0-47) : PRM ID
Reg Desc   :
Config PRM bit LB & Standard

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_prm_txcfglb_Base                                                                   0x0300
#define cAf6Reg_upen_prm_txcfglb(PRMID)                                                       (0x0300+(PRMID))
#define cAf6Reg_upen_prm_txcfglb_WidthVal                                                                   32
#define cAf6Reg_upen_prm_txcfglb_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cfg_prm_enlb
BitField Type: R/W
BitField Desc: config enable CPU config LB bit, (0) is disable, (1) is enable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Bit_Start                                                         1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Bit_End                                                           1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Mask                                                          cBit1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Shift                                                             1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_MaxVal                                                          0x1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_MinVal                                                          0x0
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_RstVal                                                          0x0

/*--------------------------------------
BitField Name: cfg_prm_lb
BitField Type: R/W
BitField Desc: config bit Loopback PRM message
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Bit_Start                                                           0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Bit_End                                                             0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Mask                                                            cBit0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Shift                                                               0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_MaxVal                                                            0x1
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_MinVal                                                            0x0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL TX STUFF 0
Reg Addr   : 0x00C0
Reg Formula:
    Where  :
Reg Desc   :
config control Stuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txcfg_ctrl0_Base                                                                   0x00C0
#define cAf6Reg_upen_txcfg_ctrl0                                                                        0x00C0
#define cAf6Reg_upen_txcfg_ctrl0_WidthVal                                                                   32
#define cAf6Reg_upen_txcfg_ctrl0_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: fcs_err_ins
BitField Type: R/W
BitField Desc: (0): disable,(1): enable insert error
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_txcfg_ctrl0_fcs_err_ins_Bit_Start                                                          7
#define cAf6_upen_txcfg_ctrl0_fcs_err_ins_Bit_End                                                            7
#define cAf6_upen_txcfg_ctrl0_fcs_err_ins_Mask                                                           cBit7
#define cAf6_upen_txcfg_ctrl0_fcs_err_ins_Shift                                                              7
#define cAf6_upen_txcfg_ctrl0_fcs_err_ins_MaxVal                                                           0x1
#define cAf6_upen_txcfg_ctrl0_fcs_err_ins_MinVal                                                           0x0
#define cAf6_upen_txcfg_ctrl0_fcs_err_ins_RstVal                                                           0x0

/*--------------------------------------
BitField Name: idlemod
BitField Type: R/W
BitField Desc: (0): transmit flag,(1): transmit ABORT: 1 pattern
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_txcfg_ctrl0_idlemod_Bit_Start                                                              6
#define cAf6_upen_txcfg_ctrl0_idlemod_Bit_End                                                                6
#define cAf6_upen_txcfg_ctrl0_idlemod_Mask                                                               cBit6
#define cAf6_upen_txcfg_ctrl0_idlemod_Shift                                                                  6
#define cAf6_upen_txcfg_ctrl0_idlemod_MaxVal                                                               0x1
#define cAf6_upen_txcfg_ctrl0_idlemod_MinVal                                                               0x0
#define cAf6_upen_txcfg_ctrl0_idlemod_RstVal                                                               0x0

/*--------------------------------------
BitField Name: sapimod
BitField Type: R/W
BitField Desc: mode byte sapi, reserve
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_txcfg_ctrl0_sapimod_Bit_Start                                                              5
#define cAf6_upen_txcfg_ctrl0_sapimod_Bit_End                                                                5
#define cAf6_upen_txcfg_ctrl0_sapimod_Mask                                                               cBit5
#define cAf6_upen_txcfg_ctrl0_sapimod_Shift                                                                  5
#define cAf6_upen_txcfg_ctrl0_sapimod_MaxVal                                                               0x1
#define cAf6_upen_txcfg_ctrl0_sapimod_MinVal                                                               0x0
#define cAf6_upen_txcfg_ctrl0_sapimod_RstVal                                                               0x0

/*--------------------------------------
BitField Name: ctraddinscfg
BitField Type: R/W
BitField Desc: byte ctradd insert, insert header 3
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_txcfg_ctrl0_ctraddinscfg_Bit_Start                                                         4
#define cAf6_upen_txcfg_ctrl0_ctraddinscfg_Bit_End                                                           4
#define cAf6_upen_txcfg_ctrl0_ctraddinscfg_Mask                                                          cBit4
#define cAf6_upen_txcfg_ctrl0_ctraddinscfg_Shift                                                             4
#define cAf6_upen_txcfg_ctrl0_ctraddinscfg_MaxVal                                                          0x1
#define cAf6_upen_txcfg_ctrl0_ctraddinscfg_MinVal                                                          0x0
#define cAf6_upen_txcfg_ctrl0_ctraddinscfg_RstVal                                                          0x0

/*--------------------------------------
BitField Name: sapinscfg
BitField Type: R/W
BitField Desc: byte sapi insert, insert header 1,2
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_txcfg_ctrl0_sapinscfg_Bit_Start                                                            3
#define cAf6_upen_txcfg_ctrl0_sapinscfg_Bit_End                                                              3
#define cAf6_upen_txcfg_ctrl0_sapinscfg_Mask                                                             cBit3
#define cAf6_upen_txcfg_ctrl0_sapinscfg_Shift                                                                3
#define cAf6_upen_txcfg_ctrl0_sapinscfg_MaxVal                                                             0x1
#define cAf6_upen_txcfg_ctrl0_sapinscfg_MinVal                                                             0x0
#define cAf6_upen_txcfg_ctrl0_sapinscfg_RstVal                                                             0x0

/*--------------------------------------
BitField Name: fcsmod32
BitField Type: R/W
BitField Desc: select FCS 32 or 16,  (0) : 16, (1) : 32
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_txcfg_ctrl0_fcsmod32_Bit_Start                                                             2
#define cAf6_upen_txcfg_ctrl0_fcsmod32_Bit_End                                                               2
#define cAf6_upen_txcfg_ctrl0_fcsmod32_Mask                                                              cBit2
#define cAf6_upen_txcfg_ctrl0_fcsmod32_Shift                                                                 2
#define cAf6_upen_txcfg_ctrl0_fcsmod32_MaxVal                                                              0x1
#define cAf6_upen_txcfg_ctrl0_fcsmod32_MinVal                                                              0x0
#define cAf6_upen_txcfg_ctrl0_fcsmod32_RstVal                                                              0x0

/*--------------------------------------
BitField Name: fcsinscfg
BitField Type: R/W
BitField Desc: (1) enable insert FCS, (0) disable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_txcfg_ctrl0_fcsinscfg_Bit_Start                                                            1
#define cAf6_upen_txcfg_ctrl0_fcsinscfg_Bit_End                                                              1
#define cAf6_upen_txcfg_ctrl0_fcsinscfg_Mask                                                             cBit1
#define cAf6_upen_txcfg_ctrl0_fcsinscfg_Shift                                                                1
#define cAf6_upen_txcfg_ctrl0_fcsinscfg_MaxVal                                                             0x1
#define cAf6_upen_txcfg_ctrl0_fcsinscfg_MinVal                                                             0x0
#define cAf6_upen_txcfg_ctrl0_fcsinscfg_RstVal                                                             0x1

/*--------------------------------------
BitField Name: flagmod
BitField Type: R/W
BitField Desc: (0): 2 flag; 1 start flag and 1 end flag, (1): 1 flag: shared
flag between two package
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_txcfg_ctrl0_flagmod_Bit_Start                                                              0
#define cAf6_upen_txcfg_ctrl0_flagmod_Bit_End                                                                0
#define cAf6_upen_txcfg_ctrl0_flagmod_Mask                                                               cBit0
#define cAf6_upen_txcfg_ctrl0_flagmod_Shift                                                                  0
#define cAf6_upen_txcfg_ctrl0_flagmod_MaxVal                                                               0x1
#define cAf6_upen_txcfg_ctrl0_flagmod_MinVal                                                               0x0
#define cAf6_upen_txcfg_ctrl0_flagmod_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL TX STUFF 1
Reg Addr   : 0x00C1
Reg Formula:
    Where  :
Reg Desc   :
config control Stuff global, config header

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txcfg_ctrl1_Base                                                                   0x00C1
#define cAf6Reg_upen_txcfg_ctrl1                                                                        0x00C1
#define cAf6Reg_upen_txcfg_ctrl1_WidthVal                                                                   32
#define cAf6Reg_upen_txcfg_ctrl1_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: sapibyte
BitField Type: R/W
BitField Desc: first sapi byte, header 1
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_txcfg_ctrl1_sapibyte_Bit_Start                                                            24
#define cAf6_upen_txcfg_ctrl1_sapibyte_Bit_End                                                              31
#define cAf6_upen_txcfg_ctrl1_sapibyte_Mask                                                          cBit31_24
#define cAf6_upen_txcfg_ctrl1_sapibyte_Shift                                                                24
#define cAf6_upen_txcfg_ctrl1_sapibyte_MaxVal                                                             0xff
#define cAf6_upen_txcfg_ctrl1_sapibyte_MinVal                                                              0x0
#define cAf6_upen_txcfg_ctrl1_sapibyte_RstVal                                                              0x0

/*--------------------------------------
BitField Name: teibyte
BitField Type: R/W
BitField Desc: tei byte, header 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_txcfg_ctrl1_teibyte_Bit_Start                                                             16
#define cAf6_upen_txcfg_ctrl1_teibyte_Bit_End                                                               23
#define cAf6_upen_txcfg_ctrl1_teibyte_Mask                                                           cBit23_16
#define cAf6_upen_txcfg_ctrl1_teibyte_Shift                                                                 16
#define cAf6_upen_txcfg_ctrl1_teibyte_MaxVal                                                              0xff
#define cAf6_upen_txcfg_ctrl1_teibyte_MinVal                                                               0x0
#define cAf6_upen_txcfg_ctrl1_teibyte_RstVal                                                               0x0

/*--------------------------------------
BitField Name: ctrbyte
BitField Type: R/W
BitField Desc: control byte, header 3
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_txcfg_ctrl1_ctrbyte_Bit_Start                                                              8
#define cAf6_upen_txcfg_ctrl1_ctrbyte_Bit_End                                                               15
#define cAf6_upen_txcfg_ctrl1_ctrbyte_Mask                                                            cBit15_8
#define cAf6_upen_txcfg_ctrl1_ctrbyte_Shift                                                                  8
#define cAf6_upen_txcfg_ctrl1_ctrbyte_MaxVal                                                              0xff
#define cAf6_upen_txcfg_ctrl1_ctrbyte_MinVal                                                               0x0
#define cAf6_upen_txcfg_ctrl1_ctrbyte_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX PRM
Reg Addr   : 0x0100 - 0x016F
Reg Formula: 0x0100 +$UPRO * 64 + $PRMID
    Where  :
           + $PRMID (0-47) : PRM ID
           + $UPRO (0-1) : upen read only
Reg Desc   :
counter byte message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txprm_cnt_byte_Base                                                                0x0100
#define cAf6Reg_upen_txprm_cnt_byte(PRMID, UPRO)                                    (0x0100+(UPRO)*64+(PRMID))
#define cAf6Reg_upen_txprm_cnt_byte_WidthVal                                                                32
#define cAf6Reg_upen_txprm_cnt_byte_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cnt_byte_prm
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Bit_Start                                                      0
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Bit_End                                                       31
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Mask                                                    cBit31_0
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Shift                                                          0
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_MaxVal                                                0xffffffff
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_MinVal                                                       0x0
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER PACKET MESSAGE TX PRM
Reg Addr   : 0x0180 - 0x01EF
Reg Formula: 0x0180 +$UPRO * 64 + $PRMID
    Where  :
           + $PRMID (0-47) : PRM ID
           + $UPRO (0-1) : upen read only
Reg Desc   :
counter packet message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txprm_cnt_pkt_Base                                                                 0x0180
#define cAf6Reg_upen_txprm_cnt_pkt(PRMID, UPRO)                                     (0x0180+(UPRO)*64+(PRMID))
#define cAf6Reg_upen_txprm_cnt_pkt_WidthVal                                                                 32
#define cAf6Reg_upen_txprm_cnt_pkt_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: cnt_pkt_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Bit_Start                                                        0
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Bit_End                                                         14
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Mask                                                      cBit14_0
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Shift                                                            0
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_MaxVal                                                      0x7fff
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_MinVal                                                         0x0
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID INFO TX PRM
Reg Addr   : 0x0200 - 0x026F
Reg Formula: 0x0200 +$UPRO * 64 + $PRMID
    Where  :
           + $PRMID (0-47) : PRM ID
           + $UPRO (0-1) : upen read only
Reg Desc   :
counter valid info PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txprm_cnt_info_Base                                                                0x0200
#define cAf6Reg_upen_txprm_cnt_info(PRMID, UPRO)                                    (0x0200+(UPRO)*64+(PRMID))
#define cAf6Reg_upen_txprm_cnt_info_WidthVal                                                                32
#define cAf6Reg_upen_txprm_cnt_info_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cnt_vld_prm_info
BitField Type: R/W
BitField Desc: value counter valid info
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_Bit_Start                                                  0
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_Bit_End                                                   14
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_Mask                                                cBit14_0
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_Shift                                                      0
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_MaxVal                                                0x7fff
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_MinVal                                                   0x0
#define cAf6_upen_txprm_cnt_info_cnt_vld_prm_info_RstVal                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE MONITOR TX MESSAGE FOR DEBUG BOARD
Reg Addr   : 0x00C2
Reg Formula:
    Where  :
Reg Desc   :
config enable monitor data before stuff

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txcfg_par_Base                                                                     0x00C2
#define cAf6Reg_upen_txcfg_par                                                                          0x00C2
#define cAf6Reg_upen_txcfg_par_WidthVal                                                                     32
#define cAf6Reg_upen_txcfg_par_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: out_cfg_bar
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [31:04]
--------------------------------------*/
#define cAf6_upen_txcfg_par_out_cfg_bar_Bit_Start                                                            4
#define cAf6_upen_txcfg_par_out_cfg_bar_Bit_End                                                             31
#define cAf6_upen_txcfg_par_out_cfg_bar_Mask                                                          cBit31_4
#define cAf6_upen_txcfg_par_out_cfg_bar_Shift                                                                4
#define cAf6_upen_txcfg_par_out_cfg_bar_MaxVal                                                       0xfffffff
#define cAf6_upen_txcfg_par_out_cfg_bar_MinVal                                                             0x0
#define cAf6_upen_txcfg_par_out_cfg_bar_RstVal                                                             0x0

/*--------------------------------------
BitField Name: out_cfg_dis
BitField Type: R/W
BitField Desc: (1) disable monitor, to reset address
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_txcfg_par_out_cfg_dis_Bit_Start                                                            3
#define cAf6_upen_txcfg_par_out_cfg_dis_Bit_End                                                              3
#define cAf6_upen_txcfg_par_out_cfg_dis_Mask                                                             cBit3
#define cAf6_upen_txcfg_par_out_cfg_dis_Shift                                                                3
#define cAf6_upen_txcfg_par_out_cfg_dis_MaxVal                                                             0x1
#define cAf6_upen_txcfg_par_out_cfg_dis_MinVal                                                             0x0
#define cAf6_upen_txcfg_par_out_cfg_dis_RstVal                                                             0x0

/*--------------------------------------
BitField Name: out_cfg_bar1
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_txcfg_par_out_cfg_bar1_Bit_Start                                                           0
#define cAf6_upen_txcfg_par_out_cfg_bar1_Bit_End                                                             2
#define cAf6_upen_txcfg_par_out_cfg_bar1_Mask                                                          cBit2_0
#define cAf6_upen_txcfg_par_out_cfg_bar1_Shift                                                               0
#define cAf6_upen_txcfg_par_out_cfg_bar1_MaxVal                                                            0x7
#define cAf6_upen_txcfg_par_out_cfg_bar1_MinVal                                                            0x0
#define cAf6_upen_txcfg_par_out_cfg_bar1_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ID TO MONITOR TX MESSAGE FOR DEBUG BOARD
Reg Addr   : 0x00C4
Reg Formula:
    Where  :
Reg Desc   :
config ID to monitor tx message

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txcfgid_Base                                                                       0x00C4
#define cAf6Reg_upen_txcfgid                                                                            0x00C4
#define cAf6Reg_upen_txcfgid_WidthVal                                                                       32
#define cAf6Reg_upen_txcfgid_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: out_cfgid
BitField Type: R/W
BitField Desc: ID which is configured to monitor Tx message
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_upen_txcfgid_out_cfgid_Bit_Start                                                                0
#define cAf6_upen_txcfgid_out_cfgid_Bit_End                                                                  5
#define cAf6_upen_txcfgid_out_cfgid_Mask                                                               cBit5_0
#define cAf6_upen_txcfgid_out_cfgid_Shift                                                                    0
#define cAf6_upen_txcfgid_out_cfgid_MaxVal                                                                0x3f
#define cAf6_upen_txcfgid_out_cfgid_MinVal                                                                 0x0
#define cAf6_upen_txcfgid_out_cfgid_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : BUFFER BYTE TX MESSAGE BEFORE STUFF FOR DEBUG BOARD
Reg Addr   : 0x0800 - 0xFFF
Reg Formula:
    Where  :
Reg Desc   :
Buffer byte Tx before stuff

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txprestuff_Base                                                                    0x0800
#define cAf6Reg_upen_txprestuff                                                                         0x0800
#define cAf6Reg_upen_txprestuff_WidthVal                                                                    32
#define cAf6Reg_upen_txprestuff_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: dta_txmess_mon
BitField Type: R/W
BitField Desc: byte data mesage before stuff
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_txprestuff_dta_txmess_mon_Bit_Start                                                        0
#define cAf6_upen_txprestuff_dta_txmess_mon_Bit_End                                                          7
#define cAf6_upen_txprestuff_dta_txmess_mon_Mask                                                       cBit7_0
#define cAf6_upen_txprestuff_dta_txmess_mon_Shift                                                            0
#define cAf6_upen_txprestuff_dta_txmess_mon_MaxVal                                                        0xff
#define cAf6_upen_txprestuff_dta_txmess_mon_MinVal                                                         0x0
#define cAf6_upen_txprestuff_dta_txmess_mon_RstVal                                                         0x0

/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message PRM RX STANDARD
Reg Addr   : 0x1000 - 0x102F
Reg Formula: 0x1000+$PRMID
    Where  :
           + $PRMID(0-47)  : PRM ID
Reg Desc   :
config standard message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_cfgstd_Base                                                                  0x1000
#define cAf6Reg_upen_rxprm_cfgstd(PRMID)                                                      (0x1000+(PRMID))
#define cAf6Reg_upen_rxprm_cfgstd_WidthVal                                                                  32
#define cAf6Reg_upen_rxprm_cfgstd_WriteMask                                                                0x0

/*---------------------------------------
 * BitField Name: cfg_prm_rxen
 */
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxen_Mask  														cBit3
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxen_Shift 														3

/*--------------------------------------
BitField Name: cfg_prm_cr
BitField Type: R/W
BitField Desc: config C/R bit expected
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Bit_Start                                                          2
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Bit_End                                                            2
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Mask                                                           cBit2
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Shift                                                              2
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_MaxVal                                                           0x1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_MinVal                                                           0x0
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_RstVal                                                           0x0

/*--------------------------------------
BitField Name: cfg_prm_rxdisstuff
BitField Type: R/W
BitField Desc: enable destuff   (1) is disable, (0) is enable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxdisstuff_Bit_Start                                                  1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxdisstuff_Bit_End                                                    1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxdisstuff_Mask                                                   cBit1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxdisstuff_Shift                                                      1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxdisstuff_MaxVal                                                   0x1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxdisstuff_MinVal                                                   0x0
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxdisstuff_RstVal                                                   0x0
/* Hardware change 2016-June-10 */
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxfcs_mode_Mask                                                    cBit1
#define cAf6_upen_rxprm_cfgstd_cfg_prm_rxfcs_mode_Shift                                                       1
/*--------------------------------------
BitField Name: cfg_std_prm
BitField Type: R/W
BitField Desc: config standard   (0) is ANSI, (1) is AT&T
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Bit_Start                                                         0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Bit_End                                                           0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Mask                                                          cBit0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Shift                                                             0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_MaxVal                                                          0x1
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_MinVal                                                          0x0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_RstVal                                                          0x0

/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL RX DESTUFF 0
Reg Addr   : 0x10C0
Reg Formula:
    Where  :
Reg Desc   :
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxdestuff_ctrl0_Base                                                               0x10C0
#define cAf6Reg_upen_rxdestuff_ctrl0                                                                    0x10C0
#define cAf6Reg_upen_rxdestuff_ctrl0_WidthVal                                                               32
#define cAf6Reg_upen_rxdestuff_ctrl0_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: cfg_en_ivl
BitField Type: R/W
BitField Desc: (0) : disable, (1) : enable
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_rxdestuff_ctrl0_cfg_en_ivl_Bit_Start                                                        8
#define cAf6_upen_rxdestuff_ctrl0_cfg_en_ivl_Bit_End                                                          8
#define cAf6_upen_rxdestuff_ctrl0_cfg_en_ivl_Mask                                                         cBit8
#define cAf6_upen_rxdestuff_ctrl0_cfg_en_ivl_Shift                                                            8

/*--------------------------------------
BitField Name: cfg_crmon
BitField Type: R/W
BitField Desc: (0) : disable, (1) : enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_rxdestuff_ctrl0_cfg_crmon_Bit_Start                                                        5
#define cAf6_upen_rxdestuff_ctrl0_cfg_crmon_Bit_End                                                          5
#define cAf6_upen_rxdestuff_ctrl0_cfg_crmon_Mask                                                         cBit5
#define cAf6_upen_rxdestuff_ctrl0_cfg_crmon_Shift                                                            5
#define cAf6_upen_rxdestuff_ctrl0_cfg_crmon_MaxVal                                                         0x1
#define cAf6_upen_rxdestuff_ctrl0_cfg_crmon_MinVal                                                         0x0
#define cAf6_upen_rxdestuff_ctrl0_cfg_crmon_RstVal                                                         0x0

/*--------------------------------------
BitField Name: fcsmod32
BitField Type: R/W
BitField Desc: select FCS 32 or 16, (0) : 16, (1) : 32
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_rxdestuff_ctrl0_fcsmod32_Bit_Start                                                         4
#define cAf6_upen_rxdestuff_ctrl0_fcsmod32_Bit_End                                                           4
#define cAf6_upen_rxdestuff_ctrl0_fcsmod32_Mask                                                          cBit4
#define cAf6_upen_rxdestuff_ctrl0_fcsmod32_Shift                                                             4
#define cAf6_upen_rxdestuff_ctrl0_fcsmod32_MaxVal                                                          0x1
#define cAf6_upen_rxdestuff_ctrl0_fcsmod32_MinVal                                                          0x0
#define cAf6_upen_rxdestuff_ctrl0_fcsmod32_RstVal                                                          0x0

/*--------------------------------------
BitField Name: fcsmon
BitField Type: R/W
BitField Desc: (0) : disable monitor, (1) : enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_rxdestuff_ctrl0_fcsmon_Bit_Start                                                           3
#define cAf6_upen_rxdestuff_ctrl0_fcsmon_Bit_End                                                             3
#define cAf6_upen_rxdestuff_ctrl0_fcsmon_Mask                                                            cBit3
#define cAf6_upen_rxdestuff_ctrl0_fcsmon_Shift                                                               3
#define cAf6_upen_rxdestuff_ctrl0_fcsmon_MaxVal                                                            0x1
#define cAf6_upen_rxdestuff_ctrl0_fcsmon_MinVal                                                            0x0
#define cAf6_upen_rxdestuff_ctrl0_fcsmon_RstVal                                                            0x1

/*--------------------------------------
BitField Name: sapimod
BitField Type: R/W
BitField Desc: mode byte sapi
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_rxdestuff_ctrl0_sapimod_Bit_Start                                                          2
#define cAf6_upen_rxdestuff_ctrl0_sapimod_Bit_End                                                            2
#define cAf6_upen_rxdestuff_ctrl0_sapimod_Mask                                                           cBit2
#define cAf6_upen_rxdestuff_ctrl0_sapimod_Shift                                                              2
#define cAf6_upen_rxdestuff_ctrl0_sapimod_MaxVal                                                           0x1
#define cAf6_upen_rxdestuff_ctrl0_sapimod_MinVal                                                           0x0
#define cAf6_upen_rxdestuff_ctrl0_sapimod_RstVal                                                           0x0

/*--------------------------------------
BitField Name: addctrmod
BitField Type: R/W
BitField Desc: monitor header 3 -(1) : enable byte monitor,     (0) disable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxdestuff_ctrl0_addctrmod_Bit_Start                                                        1
#define cAf6_upen_rxdestuff_ctrl0_addctrmod_Bit_End                                                          1
#define cAf6_upen_rxdestuff_ctrl0_addctrmod_Mask                                                         cBit1
#define cAf6_upen_rxdestuff_ctrl0_addctrmod_Shift                                                            1
#define cAf6_upen_rxdestuff_ctrl0_addctrmod_MaxVal                                                         0x1
#define cAf6_upen_rxdestuff_ctrl0_addctrmod_MinVal                                                         0x0
#define cAf6_upen_rxdestuff_ctrl0_addctrmod_RstVal                                                         0x0

/*--------------------------------------
BitField Name: sapimon
BitField Type: R/W
BitField Desc: monitor header 1,2 - (1) : enable monitor, (0) disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxdestuff_ctrl0_sapimon_Bit_Start                                                          0
#define cAf6_upen_rxdestuff_ctrl0_sapimon_Bit_End                                                            0
#define cAf6_upen_rxdestuff_ctrl0_sapimon_Mask                                                           cBit0
#define cAf6_upen_rxdestuff_ctrl0_sapimon_Shift                                                              0
#define cAf6_upen_rxdestuff_ctrl0_sapimon_MaxVal                                                           0x1
#define cAf6_upen_rxdestuff_ctrl0_sapimon_MinVal                                                           0x0
#define cAf6_upen_rxdestuff_ctrl0_sapimon_RstVal                                                           0x0

/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL RX DESTUFF 1
Reg Addr   : 0x10C1
Reg Formula:
    Where  :
Reg Desc   :
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxdestuff_ctrl1_Base                                                               0x10C1
#define cAf6Reg_upen_rxdestuff_ctrl1                                                                    0x10C1
#define cAf6Reg_upen_rxdestuff_ctrl1_WidthVal                                                               32
#define cAf6Reg_upen_rxdestuff_ctrl1_WriteMask                                                             0x0

/*--------------------------------------
BitField Name: sapibyte
BitField Type: R/W
BitField Desc: first sapi byte, header 1
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_rxdestuff_ctrl1_sapibyte_Bit_Start                                                        24
#define cAf6_upen_rxdestuff_ctrl1_sapibyte_Bit_End                                                          31
#define cAf6_upen_rxdestuff_ctrl1_sapibyte_Mask                                                      cBit31_24
#define cAf6_upen_rxdestuff_ctrl1_sapibyte_Shift                                                            24
#define cAf6_upen_rxdestuff_ctrl1_sapibyte_MaxVal                                                         0xff
#define cAf6_upen_rxdestuff_ctrl1_sapibyte_MinVal                                                          0x0
#define cAf6_upen_rxdestuff_ctrl1_sapibyte_RstVal                                                          0x0

/*--------------------------------------
BitField Name: teibyte
BitField Type: R/W
BitField Desc: tei byte, header 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_rxdestuff_ctrl1_teibyte_Bit_Start                                                         16
#define cAf6_upen_rxdestuff_ctrl1_teibyte_Bit_End                                                           23
#define cAf6_upen_rxdestuff_ctrl1_teibyte_Mask                                                       cBit23_16
#define cAf6_upen_rxdestuff_ctrl1_teibyte_Shift                                                             16
#define cAf6_upen_rxdestuff_ctrl1_teibyte_MaxVal                                                          0xff
#define cAf6_upen_rxdestuff_ctrl1_teibyte_MinVal                                                           0x0
#define cAf6_upen_rxdestuff_ctrl1_teibyte_RstVal                                                           0x0

/*--------------------------------------
BitField Name: ctrbyte
BitField Type: R/W
BitField Desc: control byte, header 3
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_rxdestuff_ctrl1_ctrbyte_Bit_Start                                                          8
#define cAf6_upen_rxdestuff_ctrl1_ctrbyte_Bit_End                                                           15
#define cAf6_upen_rxdestuff_ctrl1_ctrbyte_Mask                                                        cBit15_8
#define cAf6_upen_rxdestuff_ctrl1_ctrbyte_Shift                                                              8
#define cAf6_upen_rxdestuff_ctrl1_ctrbyte_MaxVal                                                          0xff
#define cAf6_upen_rxdestuff_ctrl1_ctrbyte_MinVal                                                           0x0
#define cAf6_upen_rxdestuff_ctrl1_ctrbyte_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : STICKY RECEIVE GOOD MESSAGE RX PRM1
Reg Addr   : 0x10C4
Reg Formula:
    Where  :
Reg Desc   :
sticky good message from ID 0-31

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxgmess_stk1_Base                                                                  0x10C4
#define cAf6Reg_upen_rxgmess_stk1                                                                       0x10C4
#define cAf6Reg_upen_rxgmess_stk1_WidthVal                                                                  32
#define cAf6Reg_upen_rxgmess_stk1_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: prm_gmess_stk1
BitField Type: R/W
BitField Desc: sticky good message, bit '0' is ID '0'
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxgmess_stk1_prm_gmess_stk1_Bit_Start                                                      0
#define cAf6_upen_rxgmess_stk1_prm_gmess_stk1_Bit_End                                                       31
#define cAf6_upen_rxgmess_stk1_prm_gmess_stk1_Mask                                                    cBit31_0
#define cAf6_upen_rxgmess_stk1_prm_gmess_stk1_Shift                                                          0
#define cAf6_upen_rxgmess_stk1_prm_gmess_stk1_MaxVal                                                0xffffffff
#define cAf6_upen_rxgmess_stk1_prm_gmess_stk1_MinVal                                                       0x0
#define cAf6_upen_rxgmess_stk1_prm_gmess_stk1_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : STICKY RECEIVE GOOD MESSAGE RX PRM2
Reg Addr   : 0x10C5
Reg Formula:
    Where  :
Reg Desc   :
sticky good message from ID 32-47

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxgmess_stk2_Base                                                                  0x10C5
#define cAf6Reg_upen_rxgmess_stk2                                                                       0x10C5
#define cAf6Reg_upen_rxgmess_stk2_WidthVal                                                                  32
#define cAf6Reg_upen_rxgmess_stk2_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: prm_gmess_stk2
BitField Type: R/W
BitField Desc: sticky good message, bit '0' is ID '32'
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_rxgmess_stk2_prm_gmess_stk2_Bit_Start                                                      0
#define cAf6_upen_rxgmess_stk2_prm_gmess_stk2_Bit_End                                                       15
#define cAf6_upen_rxgmess_stk2_prm_gmess_stk2_Mask                                                    cBit15_0
#define cAf6_upen_rxgmess_stk2_prm_gmess_stk2_Shift                                                          0
#define cAf6_upen_rxgmess_stk2_prm_gmess_stk2_MaxVal                                                    0xffff
#define cAf6_upen_rxgmess_stk2_prm_gmess_stk2_MinVal                                                       0x0
#define cAf6_upen_rxgmess_stk2_prm_gmess_stk2_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : STICKY RECEIVE DROP MESSAGE RX PRM1
Reg Addr   : 0x10C6
Reg Formula:
    Where  :
Reg Desc   :
sticky drop message from ID 0-31

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxdrmess_stk1_Base                                                                 0x10C6
#define cAf6Reg_upen_rxdrmess_stk1                                                                      0x10C6
#define cAf6Reg_upen_rxdrmess_stk1_WidthVal                                                                 32
#define cAf6Reg_upen_rxdrmess_stk1_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: prm_drmess_stk1
BitField Type: R/W
BitField Desc: sticky drop message, bit '0' is ID '0'
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxdrmess_stk1_prm_drmess_stk1_Bit_Start                                                    0
#define cAf6_upen_rxdrmess_stk1_prm_drmess_stk1_Bit_End                                                     31
#define cAf6_upen_rxdrmess_stk1_prm_drmess_stk1_Mask                                                  cBit31_0
#define cAf6_upen_rxdrmess_stk1_prm_drmess_stk1_Shift                                                        0
#define cAf6_upen_rxdrmess_stk1_prm_drmess_stk1_MaxVal                                              0xffffffff
#define cAf6_upen_rxdrmess_stk1_prm_drmess_stk1_MinVal                                                     0x0
#define cAf6_upen_rxdrmess_stk1_prm_drmess_stk1_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : STICKY RECEIVE DROP MESSAGE RX PRM2
Reg Addr   : 0x10C7
Reg Formula:
    Where  :
Reg Desc   :
sticky good message from ID 32-47

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxdrmess_stk2_Base                                                                 0x10C7
#define cAf6Reg_upen_rxdrmess_stk2                                                                      0x10C7
#define cAf6Reg_upen_rxdrmess_stk2_WidthVal                                                                 32
#define cAf6Reg_upen_rxdrmess_stk2_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: prm_drmess_stk2
BitField Type: R/W
BitField Desc: sticky drop message, bit '0' is ID '32'
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_rxdrmess_stk2_prm_drmess_stk2_Bit_Start                                                    0
#define cAf6_upen_rxdrmess_stk2_prm_drmess_stk2_Bit_End                                                     15
#define cAf6_upen_rxdrmess_stk2_prm_drmess_stk2_Mask                                                  cBit15_0
#define cAf6_upen_rxdrmess_stk2_prm_drmess_stk2_Shift                                                        0
#define cAf6_upen_rxdrmess_stk2_prm_drmess_stk2_MaxVal                                                  0xffff
#define cAf6_upen_rxdrmess_stk2_prm_drmess_stk2_MinVal                                                     0x0
#define cAf6_upen_rxdrmess_stk2_prm_drmess_stk2_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : STICKY RECEIVE MISS MESSAGE RX PRM1
Reg Addr   : 0x10C8
Reg Formula:
    Where  :
Reg Desc   :
sticky miss message from ID 0-31

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmmess_stk1_Base                                                                  0x10C8
#define cAf6Reg_upen_rxmmess_stk1                                                                       0x10C8
#define cAf6Reg_upen_rxmmess_stk1_WidthVal                                                                  32
#define cAf6Reg_upen_rxmmess_stk1_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: prm_mmess_stk1
BitField Type: R/W
BitField Desc: sticky miss message, bit '0' is ID '0'
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmmess_stk1_prm_mmess_stk1_Bit_Start                                                      0
#define cAf6_upen_rxmmess_stk1_prm_mmess_stk1_Bit_End                                                       31
#define cAf6_upen_rxmmess_stk1_prm_mmess_stk1_Mask                                                    cBit31_0
#define cAf6_upen_rxmmess_stk1_prm_mmess_stk1_Shift                                                          0
#define cAf6_upen_rxmmess_stk1_prm_mmess_stk1_MaxVal                                                0xffffffff
#define cAf6_upen_rxmmess_stk1_prm_mmess_stk1_MinVal                                                       0x0
#define cAf6_upen_rxmmess_stk1_prm_mmess_stk1_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : STICKY RECEIVE MISS MESSAGE RX PRM2
Reg Addr   : 0x10C9
Reg Formula:
    Where  :
Reg Desc   :
sticky miss message from ID 32-47

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmmess_stk2_Base                                                                  0x10C9
#define cAf6Reg_upen_rxmmess_stk2                                                                       0x10C9
#define cAf6Reg_upen_rxmmess_stk2_WidthVal                                                                  32
#define cAf6Reg_upen_rxmmess_stk2_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: prm_mmess_stk2
BitField Type: R/W
BitField Desc: sticky miss message, bit '0' is ID '32'
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_rxmmess_stk2_prm_mmess_stk2_Bit_Start                                                      0
#define cAf6_upen_rxmmess_stk2_prm_mmess_stk2_Bit_End                                                       15
#define cAf6_upen_rxmmess_stk2_prm_mmess_stk2_Mask                                                    cBit15_0
#define cAf6_upen_rxmmess_stk2_prm_mmess_stk2_Shift                                                          0
#define cAf6_upen_rxmmess_stk2_prm_mmess_stk2_MaxVal                                                    0xffff
#define cAf6_upen_rxmmess_stk2_prm_mmess_stk2_MinVal                                                       0x0
#define cAf6_upen_rxmmess_stk2_prm_mmess_stk2_RstVal                                                       0x0

/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX PRM
Reg Addr   : 0x1100 - 0x116F
Reg Formula: 0x1100 +$UPRO * 64 + $PRMID
    Where  :
           + $PRMID (0-47) : PRM ID
           + $UPRO (0-1) : upen read only
Reg Desc   :
counter good message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_gmess_Base                                                                   0x1100
#define cAf6Reg_upen_rxprm_gmess(PRMID, UPRO)                                       (0x1100+(UPRO)*64+(PRMID))
#define cAf6Reg_upen_rxprm_gmess_WidthVal                                                                   32
#define cAf6Reg_upen_rxprm_gmess_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cnt_gmess_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Bit_Start                                                        0
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Bit_End                                                         14
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Mask                                                      cBit14_0
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Shift                                                            0
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_MaxVal                                                      0x7fff
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_MinVal                                                         0x0
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX PRM
Reg Addr   : 0x1180 - 0x11EF
Reg Formula: 0x1180 +$UPRO * 64 + $PRMID
    Where  :
           + $PRMID (0-47) : PRM ID
           + $UPRO (0-1) : upen read only
Reg Desc   :
counter drop message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_drmess_Base                                                                  0x1180
#define cAf6Reg_upen_rxprm_drmess(PRMID, UPRO)                                      (0x1180+(UPRO)*64+(PRMID))
#define cAf6Reg_upen_rxprm_drmess_WidthVal                                                                  32
#define cAf6Reg_upen_rxprm_drmess_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: cnt_drmess_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Bit_Start                                                      0
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Bit_End                                                       14
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Mask                                                    cBit14_0
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Shift                                                          0
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_MaxVal                                                    0x7fff
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_MinVal                                                       0x0
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER MISS MESSAGE RX PRM
Reg Addr   : 0x1200 - 0x126F
Reg Formula: 0x1200 +$UPRO * 64 + $PRMID
    Where  :
           + $PRMID (0-47) : PRM ID
           + $UPRO (0-1) : upen read only
Reg Desc   :
counter miss message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_mmess_Base                                                                   0x1200
#define cAf6Reg_upen_rxprm_mmess(PRMID, UPRO)                                       (0x1200+(UPRO)*64+(PRMID))
#define cAf6Reg_upen_rxprm_mmess_WidthVal                                                                   32
#define cAf6Reg_upen_rxprm_mmess_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: cnt_mmess_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Bit_Start                                                        0
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Bit_End                                                         14
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Mask                                                      cBit14_0
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Shift                                                            0
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_MaxVal                                                      0x7fff
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_MinVal                                                         0x0
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX PRM
Reg Addr   : 0x1280 - 0x12EF
Reg Formula: 0x1280 +$UPRO * 64 + $PRMID
    Where  :
           + $PRMID (0-47) : PRM ID
           + $UPRO (0-1) : upen read only
Reg Desc   :
counter byte message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_cnt_byte_Base                                                                0x1280
#define cAf6Reg_upen_rxprm_cnt_byte(PRMID, UPRO)                                    (0x1280+(UPRO)*64+(PRMID))
#define cAf6Reg_upen_rxprm_cnt_byte_WidthVal                                                                32
#define cAf6Reg_upen_rxprm_cnt_byte_WriteMask                                                              0x0

/*--------------------------------------
BitField Name: cnt_byte_rxprm
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Bit_Start                                                    0
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Bit_End                                                     31
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Mask                                                  cBit31_0
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Shift                                                        0
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_MaxVal                                              0xffffffff
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_MinVal                                                     0x0
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG MONITOR RX MESSAGE FOR DEBUG BOARD
Reg Addr   : 0x10C2
Reg Formula:
    Where  :
Reg Desc   :
config enable monitor data before stuff

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxcfg_par_Base                                                                     0x10C2
#define cAf6Reg_upen_rxcfg_par                                                                          0x10C2
#define cAf6Reg_upen_rxcfg_par_WidthVal                                                                     32
#define cAf6Reg_upen_rxcfg_par_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: out_rxcfg_bar
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [31:04]
--------------------------------------*/
#define cAf6_upen_rxcfg_par_out_rxcfg_bar_Bit_Start                                                          4
#define cAf6_upen_rxcfg_par_out_rxcfg_bar_Bit_End                                                           31
#define cAf6_upen_rxcfg_par_out_rxcfg_bar_Mask                                                        cBit31_4
#define cAf6_upen_rxcfg_par_out_rxcfg_bar_Shift                                                              4
#define cAf6_upen_rxcfg_par_out_rxcfg_bar_MaxVal                                                     0xfffffff
#define cAf6_upen_rxcfg_par_out_rxcfg_bar_MinVal                                                           0x0
#define cAf6_upen_rxcfg_par_out_rxcfg_bar_RstVal                                                           0x0

/*--------------------------------------
BitField Name: out_rxcfg_dis
BitField Type: R/W
BitField Desc: (1) disable monitor, to reset address
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_rxcfg_par_out_rxcfg_dis_Bit_Start                                                          3
#define cAf6_upen_rxcfg_par_out_rxcfg_dis_Bit_End                                                            3
#define cAf6_upen_rxcfg_par_out_rxcfg_dis_Mask                                                           cBit3
#define cAf6_upen_rxcfg_par_out_rxcfg_dis_Shift                                                              3
#define cAf6_upen_rxcfg_par_out_rxcfg_dis_MaxVal                                                           0x1
#define cAf6_upen_rxcfg_par_out_rxcfg_dis_MinVal                                                           0x0
#define cAf6_upen_rxcfg_par_out_rxcfg_dis_RstVal                                                           0x0

/*--------------------------------------
BitField Name: out_rxcfg_bar1
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [02:01]
--------------------------------------*/
#define cAf6_upen_rxcfg_par_out_rxcfg_bar1_Bit_Start                                                         1
#define cAf6_upen_rxcfg_par_out_rxcfg_bar1_Bit_End                                                           2
#define cAf6_upen_rxcfg_par_out_rxcfg_bar1_Mask                                                        cBit2_1
#define cAf6_upen_rxcfg_par_out_rxcfg_bar1_Shift                                                             1
#define cAf6_upen_rxcfg_par_out_rxcfg_bar1_MaxVal                                                          0x3
#define cAf6_upen_rxcfg_par_out_rxcfg_bar1_MinVal                                                          0x0
#define cAf6_upen_rxcfg_par_out_rxcfg_bar1_RstVal                                                          0x0

/*--------------------------------------
BitField Name: out_rxcfg_sel
BitField Type: R/W
BitField Desc: select info T or T-3 to send PM
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxcfg_par_out_rxcfg_sel_Bit_Start                                                          0
#define cAf6_upen_rxcfg_par_out_rxcfg_sel_Bit_End                                                            0
#define cAf6_upen_rxcfg_par_out_rxcfg_sel_Mask                                                           cBit0
#define cAf6_upen_rxcfg_par_out_rxcfg_sel_Shift                                                              0
#define cAf6_upen_rxcfg_par_out_rxcfg_sel_MaxVal                                                           0x1
#define cAf6_upen_rxcfg_par_out_rxcfg_sel_MinVal                                                           0x0
#define cAf6_upen_rxcfg_par_out_rxcfg_sel_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ID TO MONITOR RX MESSAGE FOR DEBUG BOARD
Reg Addr   : 0x10CA
Reg Formula:
    Where  :
Reg Desc   :
config ID to monitor tx message

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxcfgid_Base                                                                       0x10CA
#define cAf6Reg_upen_rxcfgid                                                                            0x10CA
#define cAf6Reg_upen_rxcfgid_WidthVal                                                                       32
#define cAf6Reg_upen_rxcfgid_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: out_rxcfgid
BitField Type: R/W
BitField Desc: ID which is configured to monitor Rx message
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_upen_rxcfgid_out_rxcfgid_Bit_Start                                                              0
#define cAf6_upen_rxcfgid_out_rxcfgid_Bit_End                                                                5
#define cAf6_upen_rxcfgid_out_rxcfgid_Mask                                                             cBit5_0
#define cAf6_upen_rxcfgid_out_rxcfgid_Shift                                                                  0
#define cAf6_upen_rxcfgid_out_rxcfgid_MaxVal                                                              0x3f
#define cAf6_upen_rxcfgid_out_rxcfgid_MinVal                                                               0x0
#define cAf6_upen_rxcfgid_out_rxcfgid_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : BUFFER BYTE RX MESSAGE AFTER DESTUFF FOR DEBUG BOARD
Reg Addr   : 0x1800 - 0x1FFF
Reg Formula:
    Where  :
Reg Desc   :
Buffer byte Rx after poststuff PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxpoststuff_Base                                                                   0x1800
#define cAf6Reg_upen_rxpoststuff                                                                        0x1800
#define cAf6Reg_upen_rxpoststuff_WidthVal                                                                   32
#define cAf6Reg_upen_rxpoststuff_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: dta_rxmess_mon
BitField Type: R/W
BitField Desc: byte data mesage after destuff
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_rxpoststuff_dta_rxmess_mon_Bit_Start                                                       0
#define cAf6_upen_rxpoststuff_dta_rxmess_mon_Bit_End                                                         7
#define cAf6_upen_rxpoststuff_dta_rxmess_mon_Mask                                                      cBit7_0
#define cAf6_upen_rxpoststuff_dta_rxmess_mon_Shift                                                           0
#define cAf6_upen_rxpoststuff_dta_rxmess_mon_MaxVal                                                       0xff
#define cAf6_upen_rxpoststuff_dta_rxmess_mon_MinVal                                                        0x0
#define cAf6_upen_rxpoststuff_dta_rxmess_mon_RstVal                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB per Channel Interrupt Enable Control
Reg Addr   : 0x1400-0x142F
Reg Formula: 0x1400 +  $PRMID
    Where  :
           + $PRMID (0-47) : PRM ID
Reg Desc   :
This is the per Channel interrupt enable

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_cfg_lben_int_Base                                                                   0x1400
#define cAf6Reg_prm_cfg_lben_int(PRMID)                                                       (0x1400+(PRMID))
#define cAf6Reg_prm_cfg_lben_int_WidthVal                                                                   32
#define cAf6Reg_prm_cfg_lben_int_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: prm_cfglben_int
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int_prm_cfglben_int_Bit_Start                                                      0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int_Bit_End                                                        0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int_Mask                                                       cBit0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int_Shift                                                          0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int_MaxVal                                                       0x1
#define cAf6_prm_cfg_lben_int_prm_cfglben_int_MinVal                                                       0x0
#define cAf6_prm_cfg_lben_int_prm_cfglben_int_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt per Channel Interrupt Status
Reg Addr   : 0x1440-0x146F
Reg Formula: 0x1440 +  $PRMID
    Where  :
           + $PRMID (0-47) : PRM ID
Reg Desc   :
This is the per Channel interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_int_sta_Base                                                                     0x1440
#define cAf6Reg_prm_lb_int_sta(PRMID)                                                         (0x1440+(PRMID))
#define cAf6Reg_prm_lb_int_sta_WidthVal                                                                     32
#define cAf6Reg_prm_lb_int_sta_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: prm_lbint_sta
BitField Type: RW
BitField Desc: Set 1 if there is a change event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_lb_int_sta_prm_lbint_sta_Bit_Start                                                          0
#define cAf6_prm_lb_int_sta_prm_lbint_sta_Bit_End                                                            0
#define cAf6_prm_lb_int_sta_prm_lbint_sta_Mask                                                           cBit0
#define cAf6_prm_lb_int_sta_prm_lbint_sta_Shift                                                              0
#define cAf6_prm_lb_int_sta_prm_lbint_sta_MaxVal                                                           0x1
#define cAf6_prm_lb_int_sta_prm_lbint_sta_MinVal                                                           0x0
#define cAf6_prm_lb_int_sta_prm_lbint_sta_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt per Channel Current Status
Reg Addr   : 0x1480-0x14AF
Reg Formula: 0x1480 +  $PRMID
    Where  :
           + $PRMID (0-47) : PRM ID
Reg Desc   :
This is the per Channel Current status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_int_crrsta_Base                                                                  0x1480
#define cAf6Reg_prm_lb_int_crrsta(PRMID)                                                      (0x1480+(PRMID))
#define cAf6Reg_prm_lb_int_crrsta_WidthVal                                                                  32
#define cAf6Reg_prm_lb_int_crrsta_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: prm_lbint_crrsta
BitField Type: RW
BitField Desc: Current status of event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta_Bit_Start                                                    0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta_Bit_End                                                      0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta_Mask                                                     cBit0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta_Shift                                                        0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta_MaxVal                                                     0x1
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta_MinVal                                                     0x0
#define cAf6_prm_lb_int_crrsta_prm_lbint_crrsta_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt per Channel Interrupt OR Status
Reg Addr   : 0x14C0-0x14C1
Reg Formula: 0x14C0 +  $GID
    Where  :
           + $PRMID (0-1) : group ID
Reg Desc   :
The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_intsta_Base                                                                      0x14C0
#define cAf6Reg_prm_lb_intsta(GID)                                                              (0x14C0+(GID))
#define cAf6Reg_prm_lb_intsta_WidthVal                                                                      32
#define cAf6Reg_prm_lb_intsta_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: prm_lbintsta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding channel is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prm_lb_intsta_prm_lbintsta_Bit_Start                                                            0
#define cAf6_prm_lb_intsta_prm_lbintsta_Bit_End                                                             31
#define cAf6_prm_lb_intsta_prm_lbintsta_Mask                                                          cBit31_0
#define cAf6_prm_lb_intsta_prm_lbintsta_Shift                                                                0
#define cAf6_prm_lb_intsta_prm_lbintsta_MaxVal                                                      0xffffffff
#define cAf6_prm_lb_intsta_prm_lbintsta_MinVal                                                             0x0
#define cAf6_prm_lb_intsta_prm_lbintsta_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt OR Status
Reg Addr   : 0x14FF
Reg Formula:
    Where  :
Reg Desc   :
The register consists of 2 bits. Each bit is used to store Interrupt OR status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_sta_int_Base                                                                     0x14FF
#define cAf6Reg_prm_lb_sta_int                                                                          0x14FF
#define cAf6Reg_prm_lb_sta_int_WidthVal                                                                     32
#define cAf6Reg_prm_lb_sta_int_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: prm_lbsta_int
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit is set and its interrupt is
enabled
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_prm_lb_sta_int_prm_lbsta_int_Bit_Start                                                          0
#define cAf6_prm_lb_sta_int_prm_lbsta_int_Bit_End                                                            1
#define cAf6_prm_lb_sta_int_prm_lbsta_int_Mask                                                         cBit1_0
#define cAf6_prm_lb_sta_int_prm_lbsta_int_Shift                                                              0
#define cAf6_prm_lb_sta_int_prm_lbsta_int_MaxVal                                                           0x3
#define cAf6_prm_lb_sta_int_prm_lbsta_int_MinVal                                                           0x0
#define cAf6_prm_lb_sta_int_prm_lbsta_int_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt Enable Control
Reg Addr   : 0x14FE
Reg Formula:
    Where  :
Reg Desc   :
The register consists of 2 interrupt enable bits .

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_en_int_Base                                                                      0x14FE
#define cAf6Reg_prm_lb_en_int                                                                           0x14FE
#define cAf6Reg_prm_lb_en_int_WidthVal                                                                      32
#define cAf6Reg_prm_lb_en_int_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: prm_lben_int
BitField Type: RW
BitField Desc: Set to 1 to enable to generate interrupt.
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_prm_lb_en_int_prm_lben_int_Bit_Start                                                            0
#define cAf6_prm_lb_en_int_prm_lben_int_Bit_End                                                              1
#define cAf6_prm_lb_en_int_prm_lben_int_Mask                                                           cBit1_0
#define cAf6_prm_lb_en_int_prm_lben_int_Shift                                                                0
#define cAf6_prm_lb_en_int_prm_lben_int_MaxVal                                                             0x3
#define cAf6_prm_lb_en_int_prm_lben_int_MinVal                                                             0x0
#define cAf6_prm_lb_en_int_prm_lben_int_RstVal                                                             0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021MODULEPDHMDLPRMREG_H_ */


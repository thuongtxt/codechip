/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210021PhysicalReg.h
 * 
 * Created Date: Jul 20, 2015
 *
 * Description : Physical registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021PHYSICALREG_H_
#define _THA60210021PHYSICALREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cPllStatusRegister            0xF00060
#define cLinkStatusRegister           0xF00061
#define cLinkStatusChangeRegister     0xF00050
#define cGePllLockedMask(portId)      cBit8
#define cXfiPll1LockedMask            cBit9
#define cXfiPll2LockedMask            cBit10
#define cSgmiiLinkStatusMask(portId)  (cBit28 << (portId))

#define cSgmiiLoopbackRegiter            0xF00040
#define cSgmiiLocalLoopbackMask(portId)  (cBit0 << (portId))
#define cSgmiiLocalLoopbackShift(portId) (portId)

#define cSgmiiLoopbackRegiterNew            0xF00042
#define cSgmiiLocalLoopbackNewMask(portId)  (cBit0 << (portId*4))
#define cSgmiiLocalLoopbackNewShift(portId) (portId*4)

#define cAf6Reg_top_o_control4_txprecursor 0xF00044
#define cAf6Reg_top_o_control5_txposcursor 0xF00045
#define cAf6Reg_top_o_control6_txdiffctrl  0xF00046
#define cAf6Reg_top_o_control6_LPM_enable  0xF00047

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021PHYSICALREG_H_ */


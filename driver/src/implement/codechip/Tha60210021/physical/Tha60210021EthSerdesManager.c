/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210021EthSerdesManager.c
 *
 * Created Date: Jul 18, 2015
 *
 * Description : SERDES manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210031/physical/Tha60210031EthSerdesManagerInternal.h"
#include "../../Tha60210011/physical/Tha6021Physical.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021EthSerdesManager
    {
    tTha60210031EthSerdesManager super;
    }tTha60210021EthSerdesManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthSerdesManagerMethods         m_ThaEthSerdesManagerOverride;
static tTha60210031EthSerdesManagerMethods m_Tha60210031EthSerdesManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController SgmiiSerdesControllerCreate(ThaEthSerdesManager self, AtEthPort port, uint32 serdesId)
    {
    AtUnused(self);
    return Tha60210021EthPortSgmiiSerdesControllerNew(port, serdesId);
    }

static AtSerdesController SerdesControllerCreate(ThaEthSerdesManager self, AtEthPort port, uint32 serdesId)
    {
    AtUnused(self);
    return Tha60210021EthPortXfiSerdesControllerNew(port, serdesId);
    }

static eBool ShouldInitSerdes(ThaEthSerdesManager self, uint32 serdesId)
    {
    return ThaEthSerdesManagerCanControlEqualizer(self, ThaEthSerdesManagerSerdesController(self, serdesId));
    }

static ThaVersionReader VersionReader(ThaEthSerdesManager self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self->ethModule);
    return ThaDeviceVersionReader(device);
    }

static uint32 StartDateSupportXfiEqualizerLpmMode(ThaEthSerdesManager self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x10, 0x28, 0x0);
    }

static uint32 StartBuiltNumberSupportXfiEqualizerControl(ThaEthSerdesManager self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x3, 0x5, 0x0);
    }

static eBool CanControlXfiEqualizer(ThaEthSerdesManager self, AtSerdesController serdes)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(serdes));
    uint32 startSupportDate = StartDateSupportXfiEqualizerLpmMode(self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) > startSupportDate)
        return cAtTrue;
        
    if (AtDeviceVersionNumber(device) == startSupportDate)
        return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportXfiEqualizerControl(self)) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static uint32 StartDateSupportQsgmiiEqualizerLpmMode(ThaEthSerdesManager self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x16, 0x05, 0x06, 0x46);
    }

static uint32 StartBuiltNumberSupportQsgmiiEqualizerControl(ThaEthSerdesManager self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x4, 0x6, 0x09);
    }

static eBool CanControlQsgmiiEqualizer(ThaEthSerdesManager self, AtSerdesController serdes)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(serdes)) ;
    uint32 startSupportDate = StartDateSupportQsgmiiEqualizerLpmMode(self);

    if (AtDeviceVersionNumber(device) > startSupportDate)
        return cAtTrue;

    if (AtDeviceVersionNumber(device) == startSupportDate)
        return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportQsgmiiEqualizerControl(self)) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static eBool CanControlEqualizer(ThaEthSerdesManager self, AtSerdesController serdes)
    {
    AtEthPort port = (AtEthPort)AtSerdesControllerPhysicalPortGet(serdes);

    if (AtEthPortInterfaceGet(port) == cAtEthPortInterfaceXGMii)
        return CanControlXfiEqualizer(self, serdes);

    return CanControlQsgmiiEqualizer(self, serdes);
    }

static uint32 StartVersionSupportQsgmiiTuning(ThaEthSerdesManager self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4611);
    }

static void OverrideThaEthSerdesManager(ThaEthSerdesManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthSerdesManagerOverride, mMethodsGet(self), sizeof(m_ThaEthSerdesManagerOverride));

        mMethodOverride(m_ThaEthSerdesManagerOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaEthSerdesManagerOverride, ShouldInitSerdes);
        mMethodOverride(m_ThaEthSerdesManagerOverride, CanControlEqualizer);
        }

    mMethodsSet(self, &m_ThaEthSerdesManagerOverride);
    }

static void OverrideTha60210031EthSerdesManager(ThaEthSerdesManager self)
    {
    Tha60210031EthSerdesManager manager = (Tha60210031EthSerdesManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031EthSerdesManagerOverride, mMethodsGet(manager), sizeof(m_Tha60210031EthSerdesManagerOverride));

        mMethodOverride(m_Tha60210031EthSerdesManagerOverride, SgmiiSerdesControllerCreate);
        mMethodOverride(m_Tha60210031EthSerdesManagerOverride, StartVersionSupportQsgmiiTuning);
        }

    mMethodsSet(manager, &m_Tha60210031EthSerdesManagerOverride);
    }

static void Override(ThaEthSerdesManager self)
    {
    OverrideThaEthSerdesManager(self);
    OverrideTha60210031EthSerdesManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021EthSerdesManager);
    }

static ThaEthSerdesManager ObjectInit(ThaEthSerdesManager self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031EthSerdesManagerObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthSerdesManager Tha60210021EthSerdesManagerNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthSerdesManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    return ObjectInit(newManager, ethModule);
    }

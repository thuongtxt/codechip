/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210021EthPortXfiSerdesController.c
 *
 * Created Date: Jul 20, 2015
 *
 * Description : XFI SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "../../Tha60210011/physical/Tha6021EthPortXfiSerdesControllerInternal.h"
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "Tha60210021PhysicalReg.h"
#include "../man/Tha60210021DeviceReg.h"
#include "../man/Tha60210021Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cAf6_linkalm_Eport_XFI_Mask(port)       (cBit16 << (port))
#define cAf6_linkalm_Eport_XFI_Shift(port)      ((port) + 16)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021EthPortXfiSerdesController
    {
    tTha6021EthPortXfiSerdesController super;
    }tTha60210021EthPortXfiSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods             m_AtSerdesControllerOverride;
static tTha6021EthPortSerdesControllerMethods m_Tha6021EthPortSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 PortGroup(AtSerdesController self)
    {
    AtChannel port = AtSerdesControllerPhysicalPortGet(self);
    static const uint8 cNumPortsPerGroup = 2;
    return (uint8)(AtChannelIdGet(port) / cNumPortsPerGroup);
    }

static uint32 PllMask(AtSerdesController self)
    {
    if (PortGroup(self) == 0)
        return cXfiPll1LockedMask;
    return cXfiPll2LockedMask;
    }

static AtModuleEth EthModule(AtSerdesController self)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self)) ;
    return (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static uint32 StatusRegister(AtSerdesController self)
    {
    /* From this version, register is changed */
    if (AtModuleEthSerdesApsSupported(EthModule(self)))
        return cPllStatusRegister;

    return cLinkStatusRegister;
    }

static eBool PllIsLocked(AtSerdesController self)
    {
    uint32 regAddr = StatusRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    return (regVal & PllMask(self)) ? cAtTrue : cAtFalse;
    }

static eBool PllChanged(AtSerdesController self)
    {
    uint32 regAddr = cLinkStatusChangeRegister;
    uint32 mask = PllMask(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    AtSerdesControllerWrite(self, regAddr, mask, cAtModuleEth);
    return (regVal & mask) ? cAtTrue : cAtFalse;
    }

static eBool XfiLinkStatusIsReadDirectly(AtSerdesController self)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    if (AtDeviceVersionNumber(device) >= Tha60210021DeviceStartVersionSupportXfiLinkStatusReadDirectly(device))
        return cAtTrue;
    return cAtFalse;
    }

static eAtSerdesLinkStatus DirectLinkStatus(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, cAf6Reg_linksta_Eport, cAtModuleEth);
    uint32 portMask = cAf6_linkalm_Eport_XFI_Mask(AtSerdesControllerIdGet(self));

    if (regVal & portMask)
        return cAtSerdesLinkStatusUp;
    else
        return cAtSerdesLinkStatusDown;
    }

static eAtSerdesLinkStatus LinkStatus(AtSerdesController self)
    {
    if (XfiLinkStatusIsReadDirectly(self))
        return DirectLinkStatus(self);

    return m_AtSerdesControllerMethods->LinkStatus(self);
    }

static uint32 TxPreCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cAf6Reg_top_o_control4_txprecursor;
    }

static uint32 TxPostCursorAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cAf6Reg_top_o_control5_txposcursor;
    }

static uint32 TxDiffCtrlAddress(Tha6021EthPortSerdesController self)
    {
    AtUnused(self);
    return cAf6Reg_top_o_control6_txdiffctrl;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, PllChanged);
        mMethodOverride(m_AtSerdesControllerOverride, LinkStatus);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideTha6021EthPortSerdesController(AtSerdesController self)
    {
    Tha6021EthPortSerdesController controller = (Tha6021EthPortSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6021EthPortSerdesControllerOverride));

        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPreCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxPostCursorAddress);
        mMethodOverride(m_Tha6021EthPortSerdesControllerOverride, TxDiffCtrlAddress);
        }

    mMethodsSet(controller, &m_Tha6021EthPortSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideTha6021EthPortSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021EthPortXfiSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021EthPortXfiSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210021EthPortXfiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, ethPort, serdesId);
    }

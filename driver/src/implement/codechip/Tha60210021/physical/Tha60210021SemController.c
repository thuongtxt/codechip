/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210021SemController.c
 *
 * Created Date: May 3, 2017
 *
 * Description : Sem controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/physical/Tha60210031SemControllerInternal.h"
#include "../man/Tha60210021Device.h"
#include "../man/Tha60210021DeviceInternal.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021SemController
    {
    tTha60210031SemController super;
    }tTha60210021SemController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSemControllerMethods  m_AtSemControllerOverride;

static const tAtSemControllerMethods  *m_AtSemControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char* DeviceDescription(AtSemController self)
    {
    AtUnused(self);
    return "XC7K325T";
    }

static uint32 MaxLfa(AtSemController self)
    {
    AtUnused(self);
    return 0x00005AE1;
    }

static void OverrideAtSemController(AtSemController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSemControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSemControllerOverride, m_AtSemControllerMethods, sizeof(m_AtSemControllerOverride));

        mMethodOverride(m_AtSemControllerOverride, DeviceDescription);
        mMethodOverride(m_AtSemControllerOverride, MaxLfa);
        }

    mMethodsSet(self, &m_AtSemControllerOverride);
    }

static void Override(AtSemController self)
    {
    OverrideAtSemController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021SemController);
    }

static AtSemController ObjectInit(AtSemController self, AtDevice device, uint32 semId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031SemControllerObjectInit(self, device, semId) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSemController Tha60210021SemControllerNew(AtDevice device, uint32 semId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSemController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (controller == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(controller, device, semId);
    }

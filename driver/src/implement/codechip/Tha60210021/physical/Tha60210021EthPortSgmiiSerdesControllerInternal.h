/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60210021EthPortSgmiiSerdesControllerInternal.h
 * 
 * Created Date: Jun 23, 2016
 *
 * Description : SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021ETHPORTSGMIISERDESCONTROLLERINTERNAL_H_
#define _THA60210021ETHPORTSGMIISERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/physical/Tha6021EthPortQsgmiiSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210021EthPortSgmiiSerdesController
    {
    tTha6021EthPortQsgmiiSerdesController super;
    }tTha60210021EthPortSgmiiSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha60210021EthPortSgmiiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021ETHPORTSGMIISERDESCONTROLLERINTERNAL_H_ */


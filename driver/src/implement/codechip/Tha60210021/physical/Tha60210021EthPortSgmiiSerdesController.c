/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60210021EthPortSgmiiSerdesController.c
 *
 * Created Date: Jul 13, 2015
 *
 * Description : SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "../../Tha60210011/physical/Tha6021Physical.h"
#include "../../Tha60210011/prbs/Tha6021EthPortSerdesPrbsEngine.h"
#include "Tha60210021EthPortSgmiiSerdesControllerInternal.h"
#include "Tha60210021PhysicalReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6021EthPortQsgmiiSerdesControllerMethods m_Tha6021EthPortQsgmiiSerdesControllerOverride;
static tAtSerdesControllerMethods                   m_AtSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule(AtSerdesController self)
    {
    AtDevice device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self)) ;
    return (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static eAtSerdesLinkStatus LinkStatusRegister(AtSerdesController self)
    {
    /* From this version, register is changed */
    if (AtModuleEthSerdesApsSupported(EthModule(self)))
        return cPllStatusRegister;

    return cLinkStatusRegister;
    }

static eAtSerdesLinkStatus LinkStatus(AtSerdesController self)
    {
    uint32 regAddr = LinkStatusRegister(self);
    uint32 mask = cSgmiiLinkStatusMask(AtSerdesControllerIdGet(self));
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    return (regVal & mask) ? cAtSerdesLinkStatusUp : cAtSerdesLinkStatusDown;
    }

static eBool PllIsLocked(AtSerdesController self)
    {
    uint32 regAddr = LinkStatusRegister(self);
    uint32 mask = cGePllLockedMask(AtSerdesControllerIdGet(self));
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    return (regVal & mask) ? cAtTrue : cAtFalse;
    }

static void ChangedStatusShow(const char *statusName, uint32 changed)
    {
    AtPrintc(cSevNormal, "%s ", statusName);
    AtPrintc(changed ? cSevWarning : cSevInfo, "%s\r\n", changed ? "changed" : "no changed");
    }

static void LinkStatusChangeShow(AtSerdesController self)
    {
    uint32 regAddr = cLinkStatusChangeRegister;
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 portId = AtSerdesControllerIdGet(self);
    uint32 pllLockedChanged = (regVal & cGePllLockedMask(portId));
    uint32 linksStatusChanged = (regVal & cSgmiiLinkStatusMask(portId));

    ChangedStatusShow("* PLL locked :", pllLockedChanged);
    ChangedStatusShow("* Link status:", linksStatusChanged);

    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    }

static void Debug(AtSerdesController self)
    {
    m_AtSerdesControllerMethods->Debug(self);
    LinkStatusChangeShow(self);
    }

static uint32 QsgmiiSerdesLoopbackRegister(Tha6021EthPortQsgmiiSerdesController self)
    {
    AtUnused(self);
    return 0xF00048;
    }

static void  SgmiiLanesCreate(Tha6021EthPortQsgmiiSerdesController self)
    {
    AtUnused(self);
    }

static void OverrideTha6021EthPortQsgmiiSerdesController(AtSerdesController self)
    {
    Tha6021EthPortQsgmiiSerdesController controller = (Tha6021EthPortQsgmiiSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_Tha6021EthPortQsgmiiSerdesControllerOverride,
                                  mMethodsGet(controller),
                                  sizeof(m_Tha6021EthPortQsgmiiSerdesControllerOverride));
        mMethodOverride(m_Tha6021EthPortQsgmiiSerdesControllerOverride, QsgmiiSerdesLoopbackRegister);
        mMethodOverride(m_Tha6021EthPortQsgmiiSerdesControllerOverride, SgmiiLanesCreate);
        }

    mMethodsSet(controller, &m_Tha6021EthPortQsgmiiSerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, LinkStatus);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, Debug);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideTha6021EthPortQsgmiiSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021EthPortSgmiiSerdesController);
    }

AtSerdesController Tha60210021EthPortSgmiiSerdesControllerObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021EthPortQsgmiiSerdesControllerObjectInit(self, ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60210021EthPortSgmiiSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021EthPortSgmiiSerdesControllerObjectInit(newController, ethPort, serdesId);
    }

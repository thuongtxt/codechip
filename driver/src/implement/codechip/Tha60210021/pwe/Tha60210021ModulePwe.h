/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60210021ModulePwe.h
 * 
 * Created Date: Oct 12, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210021_PWE_THA60210021MODULEPWE_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210021_PWE_THA60210021MODULEPWE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPwGroup.h"
#include "../../../default/pwe/ThaModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210021ModulePweHsPwLabelSet(ThaModulePwe self, AtPw pw, uint32 label);
uint32 Tha60210021ModulePweHsPwLabelGet(ThaModulePwe self, AtPw pw);
eAtRet Tha60210021ModulePweApsGroupEnable(ThaModulePwe self, AtPwGroup pwGroup, eBool enable);
eBool Tha60210021ModulePweApsGroupIsEnabled(ThaModulePwe self, AtPwGroup pwGroup);
eAtRet Tha60210021ModulePweHsGroupLabelSetSelect(ThaModulePwe self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet);
eAtRet Tha60210021ModulePweHsGroupSelectedLabelSetGet(ThaModulePwe self, AtPwGroup pwGroup);
eAtRet Tha60210021ModulePweApsGroupPwAdd(ThaModulePwe self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet Tha60210021ModulePweApsGroupPwRemove(ThaModulePwe self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet Tha60210021ModulePweHsGroupPwAdd(ThaModulePwe self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet Tha60210021ModulePweHsGroupPwRemove(ThaModulePwe self, AtPwGroup pwGroup, AtPw pwAdapter);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210021_PWE_THA60210021MODULEPWE_H_ */


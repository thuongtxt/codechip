/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60210021ModulePwe.c
 *
 * Created Date: Oct 8, 2015
 *
 * Description : PWE module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210021ModulePweReg.h"
#include "Tha60210021ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021ModulePwe
    {
    tThaPdhPwProductModulePwe super;
    }tTha60210021ModulePwe;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePweMethods   m_ThaModulePweOverride;
static tThaModulePweV2Methods m_ThaModulePweV2Override;

/* Save super implementation */
static const tThaModulePweMethods * m_ThaModulePweMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldResetAllPwGroups(ThaModulePweV2 self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModulePw modulePw = (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    return ThaModulePwHspwIsSupported(modulePw);
    }

static eBool IsSimulated(ThaModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return AtDeviceIsSimulated(device);
    }

static eAtRet PwRemovingStart(ThaModulePwe self, AtPw pw)
    {
    eAtRet ret = m_ThaModulePweMethods->PwRemovingStart(self, pw);

    if (!IsSimulated(self))
        AtOsalUSleep(2000);

    return ret;
    }

static uint32 MaxNumCacheBlocks(ThaModulePwe self)
    {
    AtUnused(self);
    return 510;
    }

static uint32 NumFreeBlocksHwGet(ThaModulePwe self)
    {
    return mModuleHwRead(self, 0x180008) & cBit15_0;
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, mMethodsGet(pweModule), sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PwRemovingStart);
        mMethodOverride(m_ThaModulePweOverride, MaxNumCacheBlocks);
        mMethodOverride(m_ThaModulePweOverride, NumFreeBlocksHwGet);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void OverrideThaModulePweV2(AtModule self)
    {
    ThaModulePweV2 pweModule = (ThaModulePweV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweV2Override, mMethodsGet(pweModule), sizeof(m_ThaModulePweV2Override));

        mMethodOverride(m_ThaModulePweV2Override, ShouldResetAllPwGroups);
        }

    mMethodsSet(pweModule, &m_ThaModulePweV2Override);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePwe(self);
    OverrideThaModulePweV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModulePwe);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210021ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60210021ModulePweReg.h
 * 
 * Created Date: Oct 8, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210021_PWE_THA60210021MODULEPWEREG_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210021_PWE_THA60210021MODULEPWEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit HSPW Label Control
Reg Addr   : 0x005000-0x0050FF
Reg Formula: 0x005000 + PwId
    Where  :
           + $PwId(0 - 255): Pseudowire Identification
Reg Desc   :
Used for TDM PW. This register configures "label ID" when HSPW working at protection mode value.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_HSPW_Label_Control_Base                                           0x005000
#define cAf6Reg_Pseudowire_Transmit_HSPW_Label_Control(PwId)                                 (0x005000+(PwId))
#define cAf6Reg_Pseudowire_Transmit_HSPW_Label_Control_WidthVal                                             32
#define cAf6Reg_Pseudowire_Transmit_HSPW_Label_Control_WriteMask                                           0x0

/*--------------------------------------
BitField Name: TxEthPwHspwLabelValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the HSPW label
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_HSPW_Label_Control_TxEthPwHspwLabelValue_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_HSPW_Label_Control_TxEthPwHspwLabelValue_Bit_End                                      31
#define cAf6_Pseudowire_Transmit_HSPW_Label_Control_TxEthPwHspwLabelValue_Mask                                cBit31_0
#define cAf6_Pseudowire_Transmit_HSPW_Label_Control_TxEthPwHspwLabelValue_Shift                                       0
#define cAf6_Pseudowire_Transmit_HSPW_Label_Control_TxEthPwHspwLabelValue_MaxVal                              0xffffffff
#define cAf6_Pseudowire_Transmit_HSPW_Label_Control_TxEthPwHspwLabelValue_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_HSPW_Label_Control_TxEthPwHspwLabelValue_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit UPSR and HSPW Control
Reg Addr   : 0x006000-0x0060FF
Reg Formula: 0x006000 + PwId
    Where  :
           + $PwId(0 - 255): Pseudowire Identification
Reg Desc   :
Used for TDM PW. This register configures UPSR and HSPW value.

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_UPSR_and_HSPW_Control_Base                                        0x006000
#define cAf6Reg_Pseudowire_Transmit_UPSR_and_HSPW_Control(PwId)                              (0x006000+(PwId))
#define cAf6Reg_Pseudowire_Transmit_UPSR_and_HSPW_Control_WidthVal                                          32
#define cAf6Reg_Pseudowire_Transmit_UPSR_and_HSPW_Control_WriteMask                                        0x0

/*--------------------------------------
BitField Name: TxEthPwUpsrUseValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the UPSR using or not 0: PW not join any
UPSR Group 1: PW join a UPSR Group
BitField Bits: [15]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_Bit_Start                                      15
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_Bit_End                                      15
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_Mask                                  cBit15
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_Shift                                      15
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_MaxVal                                     0x1
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrUseValue_RstVal                                     0x0

/*--------------------------------------
BitField Name: TxEthPwHspwUseValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the HSPW using or not 0: PW not join any
HSPW Group 1: PW join a HSPW Group
BitField Bits: [14]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_Bit_Start                                      14
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_Bit_End                                      14
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_Mask                                  cBit14
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_Shift                                      14
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_MaxVal                                     0x1
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwUseValue_RstVal                                     0x0

/*--------------------------------------
BitField Name: TxEthPwUpsrGrpValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the UPSR group
BitField Bits: [13:6]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_Bit_Start                                       6
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_Bit_End                                      13
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_Mask                                cBit13_6
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_Shift                                       6
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_MaxVal                                    0xff
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwUpsrGrpValue_RstVal                                     0x0

/*--------------------------------------
BitField Name: TxEthPwHspwGrpValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the HSPW group
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_Bit_End                                       5
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_Mask                                 cBit5_0
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_Shift                                       0
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_MaxVal                                    0x3f
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_UPSR_and_HSPW_Control_TxEthPwHspwGrpValue_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit UPSR Group Control
Reg Addr   : 0x007000-0x0070FF
Reg Formula: 0x007000 + TxEthPwUpsrGrpValue
    Where  :
           + $TxEthPwUpsrGrpValue(0 - 255): UPSR Group Identification
Reg Desc   :
Used for TDM PW. This register configures UPSR Group enable or not

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_UPSR_Group_Control_Base                                           0x007000
#define cAf6Reg_Pseudowire_Transmit_UPSR_Group_Control(TxEthPwUpsrGrpValue)        (0x007000+(TxEthPwUpsrGrpValue))
#define cAf6Reg_Pseudowire_Transmit_UPSR_Group_Control_WidthVal                                             32
#define cAf6Reg_Pseudowire_Transmit_UPSR_Group_Control_WriteMask                                           0x0

/*--------------------------------------
BitField Name: TxEthPwUpsrEnValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the UPSR Group enable or not 0: UPSR
Group disable 1: UPSR Group enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_UPSR_Group_Control_TxEthPwUpsrEnValue_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_UPSR_Group_Control_TxEthPwUpsrEnValue_Bit_End                                       0
#define cAf6_Pseudowire_Transmit_UPSR_Group_Control_TxEthPwUpsrEnValue_Mask                                   cBit0
#define cAf6_Pseudowire_Transmit_UPSR_Group_Control_TxEthPwUpsrEnValue_Shift                                       0
#define cAf6_Pseudowire_Transmit_UPSR_Group_Control_TxEthPwUpsrEnValue_MaxVal                                     0x1
#define cAf6_Pseudowire_Transmit_UPSR_Group_Control_TxEthPwUpsrEnValue_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_UPSR_Group_Control_TxEthPwUpsrEnValue_RstVal                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit HSPW Protection Control
Reg Addr   : 0x008000-0x00803F
Reg Formula: 0x008000 + TxEthPwHspwGrpValue
    Where  :
           + $TxEthPwHspwGrpValue(0 - 63): UPSR Group Identification
Reg Desc   :
Used for TDM PW. This register configures HSPW Group Protection enable

------------------------------------------------------------------------------*/
#define cAf6Reg_Pseudowire_Transmit_HSPW_Protection_Control_Base                                      0x008000
#define cAf6Reg_Pseudowire_Transmit_HSPW_Protection_Control(TxEthPwHspwGrpValue)        (0x008000+(TxEthPwHspwGrpValue))
#define cAf6Reg_Pseudowire_Transmit_HSPW_Protection_Control_WidthVal                                        32
#define cAf6Reg_Pseudowire_Transmit_HSPW_Protection_Control_WriteMask                                      0x0

/*--------------------------------------
BitField Name: TxEthPwHspwEnValue
BitField Type: RW
BitField Desc: Used for TDM PW, this is the HSPW enable 0: HSPW Group using
normal label 1: HSPW Group using "HSPW Label" as lable for packet transmit
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Pseudowire_Transmit_HSPW_Protection_Control_TxEthPwHspwEnValue_Bit_Start                                       0
#define cAf6_Pseudowire_Transmit_HSPW_Protection_Control_TxEthPwHspwEnValue_Bit_End                                       0
#define cAf6_Pseudowire_Transmit_HSPW_Protection_Control_TxEthPwHspwEnValue_Mask                                   cBit0
#define cAf6_Pseudowire_Transmit_HSPW_Protection_Control_TxEthPwHspwEnValue_Shift                                       0
#define cAf6_Pseudowire_Transmit_HSPW_Protection_Control_TxEthPwHspwEnValue_MaxVal                                     0x1
#define cAf6_Pseudowire_Transmit_HSPW_Protection_Control_TxEthPwHspwEnValue_MinVal                                     0x0
#define cAf6_Pseudowire_Transmit_HSPW_Protection_Control_TxEthPwHspwEnValue_RstVal                                     0x0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60210021_PWE_THA60210021MODULEPWEREG_H_ */


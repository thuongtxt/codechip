/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210021DeviceAsyncInit.h
 * 
 * Created Date: Aug 16, 2016
 *
 * Description : To handle async init
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021DEVICEASYNCINIT_H_
#define _THA60210021DEVICEASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210021DeviceAsyncInit(AtDevice self);
eAtRet Tha60210021DeviceSuperAsyncInit(AtDevice self);
eAtRet Tha60210021DeviceSemCheck(AtDevice self);
void Tha60210021DeviceHwFlush1(AtDevice self);
void Tha60210021DeviceHwFlush2(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021DEVICEASYNCINIT_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha6021DebugPrint.h
 * 
 * Created Date: Sep 26, 2015
 *
 * Description : Debug print utility
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021DEBUGPRINT_H_
#define _THA6021DEBUGPRINT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atclib.h"
#include "AtDebugger.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha6021DebugPrintStart(AtDebugger debugger);
void Tha6021DebugPrintStop(AtDebugger debugger);
void Tha6021DebugPrintRegName(AtDebugger debugger, const char* title, uint32 address, uint32 value);
void Tha6021DebugPrintErrorBit(AtDebugger debugger, const char* title, uint32 hwValue, uint32 mask);
void Tha6021DebugPrintWarningBit(AtDebugger debugger, const char* title, uint32 hwValue, uint32 mask);
void Tha6021DebugPrintInfoBit(AtDebugger debugger, const char* title, uint32 hwValue, uint32 mask);
void Tha6021DebugPrintOkBit(const char* title, uint32 hwValue, uint32 mask);
void Tha6021DebugPrintBitFieldVal(AtDebugger debugger, const char* title, uint32 hwValue, uint32 valMask, uint32 valShift);
void Tha6021DebugPrintNeedTwoCollumnsEnable(eBool enable);
eBool Tha6021DebugPrintTwoCollumnsNeeded(void);
void Tha6021DebugPrintErrorBitWithLabel(const char* title, uint32 hwValue, uint32 mask, const char *setString, const char *clearString);
void Tha6021DebugPrintOkBitWithLabel(const char* title, uint32 hwValue, uint32 mask, const char *setString, const char *clearString);

/* The most flexible function */
void Tha6021DebugPrintBitWithLabelAndColor(const char* title,
                                           uint32 hwValue, uint32 mask,
                                           const char *setString, const char *clearString,
                                           eAtSevLevel setColor, eAtSevLevel clearColor);

#ifdef __cplusplus
}
#endif
#endif /* _THA6021DEBUGPRINT_H_ */


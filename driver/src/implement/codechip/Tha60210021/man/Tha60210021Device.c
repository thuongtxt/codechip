/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210021Device.c
 *
 * Created Date: May 6, 2013
 *
 * Description : Product 60210021 (E1 SAToP/CESoP)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/physical/AtSemControllerInternal.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../../../default/prbs/ThaModulePrbs.h"
#include "../../../default/ber/ThaModuleBer.h"
#include "../../../default/physical/ThaSemController.h"
#include "../../../default/physical/ThaSemControllerInternal.h"
#include "../../default/ThaPdhPwProduct/man/ThaPdhPwProductDeviceInternal.h"
#include "../../Tha60210031/man/Tha60210031Device.h"
#include "../../Tha60210011/eth/Tha6021ModuleEth.h"
#include "../../Tha60210011/man/Tha60210011Device.h"
#include "../clock/Tha60210021ModuleClock.h"
#include "../eth/Tha60210021ModuleEth.h"
#include "Tha60210021DeviceInternal.h"
#include "Tha60210021DeviceReg.h"
#include "Tha6021DebugPrint.h"
#include "Tha60210021DeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210021Device)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods          m_AtObjectOverride;
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;

/* Super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtDeviceMethods  *m_AtDeviceMethods  = NULL;
static const tThaDeviceMethods *m_ThaDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021Device);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModulePktAnalyzer,
                                                 cAtModulePrbs,
                                                 cAtModuleBer,
                                                 cAtModuleSur,
                                                 cAtModuleClock};
    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleValue = moduleId;

    switch (moduleValue)
        {
        case cAtModulePrbs : return cAtTrue;
        case cAtModuleClock: return cAtTrue;
        case cAtModuleBer  : return cAtTrue;
        case cAtModuleSur:   return cAtTrue;
        default:
            return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
        }
    }

static eAtRet ClearSemAlarm(AtSemController self)
    {
    uint32 history;

    AtSemControllerAlarmHistoryClear(self);

    /* Ignore heart-beat sticky which is always updated */
    history = AtSemControllerAlarmHistoryGet(self);
    history = history & (uint32)(~cAtSemControllerAlarmHeartbeat);
    if (history == cAtSemControllerAlarmNone)
        return cAtOk;

    return cAtErrorSemError;
    }

static eAtRet SemCheck(AtDevice self)
    {
    eAtRet ret = cAtOk;
    AtSemController semController = AtDeviceSemControllerGet(self);
    AtDriver driver = AtDeviceDriverGet(self);

    if (AtDeviceInAccessible(self))
        return cAtOk;

    if (semController == NULL)
        return cAtOk;

    ret = AtSemControllerMoveObservationModeBeforeActiveChecking(semController);
    if (ret != cAtOk)
        return ret;

    /* SEM function must be active */
    if (!AtSemControllerIsActive(semController))
        {
        AtDriverLog(driver, cAtLogLevelWarning, "SEM IP is not active\r\n");
        return cAtErrorNotActivate;
        }

    ret = ClearSemAlarm(semController);
    if (ret != cAtOk)
        {
        AtDriverLog(driver, cAtLogLevelWarning, "Can not clear SEM alarm\r\n");
        return ret;
        }

    return cAtOk;
    }

static void RamErrorGeneratorHwFlush(ThaDevice self)
    {
    AtHal hal = AtDeviceIpCoreHalGet((AtDevice)self, 0);
    AtHalWrite(hal, 0x321201, 0x0);
    AtHalWrite(hal, 0x243601, 0x0);
    }

static void HwFlush1(ThaDevice self)
    {
    /*+PDH*/
    ThaDeviceMemoryFlush(self, 0x790002, 0x790003, 0x0);
    ThaDeviceMemoryFlush(self, 0x754000, 0x75403f, 0);
    ThaDeviceMemoryFlush(self, 0x794000, 0x79403f, 0);
    ThaDeviceMemoryFlush(self, 0x794800, 0x79483f, 0);

    /*+ SSM*/
    ThaDeviceMemoryLongFlush(self, 0x7f0000, 0x7f003f);
    ThaDeviceMemoryFlush(self, 0x7fC000, 0x7fC03f, 0);

    /*+ MAP*/
    ThaDeviceMemoryFlush(self, 0x1a0000, 0x1a07FF, 0);
    ThaDeviceMemoryFlush(self, 0x1a1000, 0x1a17FF, 0);
    ThaDeviceMemoryFlush(self, 0x1a0800, 0x1a083F, 0);

    /*+ CDR*/
    ThaDeviceMemoryFlush(self, 0x280200, 0x28023F, 0);
    ThaDeviceMemoryFlush(self, 0x282000, 0x28203F, 0);

    /*+ BERT*/
    ThaDeviceMemoryFlush(self, 0x1A2100,  0x1A210F, 0);
    ThaDeviceMemoryFlush(self, 0x1A2020,  0x1A202F, 0);
    ThaDeviceMemoryFlush(self, 0x1A2060,  0x1A206F, 0);
    ThaDeviceMemoryFlush(self, 0x1A2300,  0x1A230F, 0);
    ThaDeviceMemoryFlush(self, 0x1A2500,  0x1A250F, 0);

    /*+ PLA*/
    ThaDeviceMemoryFlush(self, 0x2C2000, 0x2C20FF, 0);
    ThaDeviceMemoryLongFlush(self, 0x2C2800, 0x2C28FF);
    }

static void HwPmFlush(ThaDevice self)
    {
    /*FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1*/
    ThaDeviceMemoryFlush(self, 0x934000, 0x934030, 0);
    /*FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW*/
    ThaDeviceMemoryFlush(self, 0x91E000, 0x91E0FF, 0);
    }

static void HwFlush2(ThaDevice self)
    {
    /*+ PWE*/
    ThaDeviceMemoryFlush(self, 0x300000, 0x300FFF, 0);
    ThaDeviceMemoryFlush(self, 0x301000, 0x3010FF, 0);
    ThaDeviceMemoryFlush(self, 0x303000, 0x3030FF, 0);
    ThaDeviceMemoryFlush(self, 0x321100, 0x3211FF, 0);
    ThaDeviceMemoryFlush(self, 0x325000, 0x3250FF, 0);
    ThaDeviceMemoryFlush(self, 0x305000, 0x3050FF, 0);
    ThaDeviceMemoryFlush(self, 0x306000, 0x3060FF, 0);
    ThaDeviceMemoryFlush(self, 0x307000, 0x3070FF, 0);
    ThaDeviceMemoryFlush(self, 0x308000, 0x3080FF, 0);

    /*+ CLA*/
    ThaDeviceMemoryLongFlush(self, 0x409C00, 0x409CFF);
    ThaDeviceMemoryFlush(self, 0x408800, 0x408FFF, 0);
    ThaDeviceMemoryLongFlush(self, 0x409000, 0x4097FF);
    ThaDeviceMemoryFlush(self, 0x40C000, 0x40C3FF, 0);
    ThaDeviceMemoryFlush(self, 0x40A000, 0x40A1FF, 0);

    /*+ PDA*/
    ThaDeviceMemoryLongFlush(self, 0x243100, 0x2431ff);
    ThaDeviceMemoryFlush(self, 0x244000, 0x2440ff, 0);
    ThaDeviceMemoryFlush(self, 0x245000, 0x2450ff, 0);
    ThaDeviceMemoryFlush(self, 0x246400, 0x2464ff, 0);

    /*- PRM*/
    ThaDeviceMemoryFlush(self, 0x600000, 0x60002F, 0);
    ThaDeviceMemoryFlush(self, 0x600300, 0x60032F, 0);
    ThaDeviceMemoryFlush(self, 0x601000, 0x60102F, 0);

    /* PMC */
    ThaDeviceMemoryFlush(self, 0x501000, 0x5010FF, 0);

    /* PM/FM */
    HwPmFlush(self);

    /* Ram Error Generator Error Flush */
    if (ThaDeviceErrorGeneratorIsSupported((ThaDevice)self))
        RamErrorGeneratorHwFlush(self);
    }

static void HwFlush(ThaDevice self)
    {
    HwFlush1(self);
    HwFlush2(self);
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret;

    HwFlush((ThaDevice)self);
    ret = m_AtDeviceMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    SemCheck(self);
    return AtDeviceAllModulesHwResourceCheck(self);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    /* Public modules */
    if (moduleId  == cAtModulePdh)   return (AtModule)Tha60210021ModulePdhNew(self);
    if (moduleId  == cAtModulePw)    return (AtModule)Tha60210021ModulePwNew(self);
    if (moduleId  == cAtModuleRam)   return (AtModule)Tha60210021ModuleRamNew(self);
    if (moduleId  == cAtModuleEth)   return (AtModule)Tha60210021ModuleEthNew(self);
    if (moduleId  == cAtModulePrbs)  return (AtModule)Tha60210021ModulePrbsNew(self);
    if (moduleId  == cAtModuleBer)   return (AtModule)Tha60210021ModuleBerNew(self);
    if (moduleId  == cAtModuleSur)   return (AtModule)Tha60210021ModuleSurNew(self);
    if (moduleId  == cAtModuleClock) return (AtModule)Tha60210021ModuleClockNew(self);

    /* Private modules */
    if (phyModule  == cThaModuleCdr)   return (AtModule)Tha60210021ModuleCdrNew(self);
    if (phyModule  == cThaModuleMap)   return (AtModule)Tha60210021ModuleMapNew(self);
    if (phyModule  == cThaModuleDemap) return (AtModule)Tha60210021ModuleDemapNew(self);
    if (phyModule  == cThaModuleCla)   return (AtModule)Tha60210021ModuleClaNew(self);
    if (phyModule  == cThaModulePda)   return (AtModule)Tha60210021ModulePdaNew(self);
    if (phyModule  == cThaModulePwe)   return (AtModule)Tha60210021ModulePweNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtUnused(self);
    return Tha60210021IntrControllerNew(core);
    }

static eBool NeedCheckSSKey(AtDevice self)
    {
    /* This product does not have SSKey */
    AtUnused(self);
    return cAtFalse;
    }

static uint32 PllClkSysStatReg(ThaDevice self)
    {
    if (ThaDeviceIsEp(self))
        return m_ThaDeviceMethods->PllClkSysStatReg(self);
    return cDeviceStickyRegisters;
    }

static uint32 PllClkSysStatBitMask(ThaDevice self)
    {
    if (ThaDeviceIsEp(self))
        return m_ThaDeviceMethods->PllClkSysStatBitMask(self);
    return cDevicePllStickyMask;
    }

static eAtRet Setup(AtDevice self)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtDeviceMethods->Setup(self);

    /* TODO: Remove this after diagnostic finish */
    ret |= AtDeviceDiagnosticCheckEnable(self, cAtFalse);

    return ret;
    }

static eAtRet DiagnosticModeEnable(AtDevice self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtDeviceMethods->DiagnosticModeEnable(self, enable);
    ret |= Tha6021DsxModuleEthHwDiagnosticModeEnable(AtDeviceModuleGet(self, cAtModuleEth), enable);

    return ret;
    }

static uint32 StartVersionSupportSurveillance(ThaDevice self)
    {
    if (AtDeviceAllFeaturesAvailableInSimulation((AtDevice)self))
        return 0;

    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader((AtDevice)self), 0x15, 0x08, 0x21, 0x00);
    }

static void PllStickyShow(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDeviceStickyRegisters;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* Device PLL sticky", regAddres, regValue);

    Tha6021DebugPrintErrorBit(debugger, " Sys pll"        , regValue, cDeviceSysPllLocked     );
    Tha6021DebugPrintErrorBit(debugger, " Sys 25M pll"    , regValue, cDeviceSys25PllLocked   );
    Tha6021DebugPrintErrorBit(debugger, " Ge pll"         , regValue, cDeviceGePllLocked      );
    Tha6021DebugPrintErrorBit(debugger, " Xfi 1 pll"      , regValue, cDeviceXfiPll1Locked    );
    Tha6021DebugPrintErrorBit(debugger, " Xfi 2 pll"      , regValue, cDeviceXfiPll2Locked    );
    Tha6021DebugPrintErrorBit(debugger, " Calib complete" , regValue, cDeviceInitCalibComplete);
    Tha6021DebugPrintErrorBit(debugger, " resetdone_out"  , regValue, cDeviceResetdone_out    );
    Tha6021DebugPrintErrorBit(debugger, " xgmii_rst_out"  , regValue, cDeviceXgmii_rst_out    );
    Tha6021DebugPrintErrorBit(debugger, " xgmii2_rst_out" , regValue, cDeviceXgmii2_rst_out   );
    Tha6021DebugPrintErrorBit(debugger, " gmii1_rst"      , regValue, cDeviceGmii1_rst_out    );
    Tha6021DebugPrintErrorBit(debugger, " p4_resetdone"   , regValue, cDeviceP4_resetdone     );
    Tha6021DebugPrintErrorBit(debugger, " p3_resetdone"   , regValue, cDeviceP3_resetdone     );
    Tha6021DebugPrintErrorBit(debugger, " p2_resetdone"   , regValue, cDeviceP2_resetdone     );
    Tha6021DebugPrintErrorBit(debugger, " p1_resetdone"   , regValue, cDeviceP1_resetdone     );
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

static void PllStatusShow(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDeviceStatusRegisters;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* Device PLL status", regAddres, regValue);
    Tha6021DebugPrintInfoBit(debugger, "Sys pll"        , regValue, cDeviceSysPllLocked     );
    Tha6021DebugPrintInfoBit(debugger, "Sys 25M pll"    , regValue, cDeviceSys25PllLocked   );
    Tha6021DebugPrintInfoBit(debugger, "Ge pll"         , regValue, cDeviceGePllLocked      );
    Tha6021DebugPrintInfoBit(debugger, "Xfi 1 pll"      , regValue, cDeviceXfiPll1Locked    );
    Tha6021DebugPrintInfoBit(debugger, "Xfi 2 pll"      , regValue, cDeviceXfiPll2Locked    );
    Tha6021DebugPrintInfoBit(debugger, "Calib complete" , regValue, cDeviceInitCalibComplete);
    Tha6021DebugPrintInfoBit(debugger, "resetdone_out"  , regValue, cDeviceResetdone_out    );
    Tha6021DebugPrintInfoBit(debugger, "xgmii_rst_out"  , regValue, cDeviceXgmii_rst_out    );
    Tha6021DebugPrintInfoBit(debugger, "xgmii2_rst_out" , regValue, cDeviceXgmii2_rst_out   );
    Tha6021DebugPrintInfoBit(debugger, "gmii1_rst"      , regValue, cDeviceGmii1_rst_out    );
    Tha6021DebugPrintInfoBit(debugger, "p4_resetdone"   , regValue, cDeviceP4_resetdone     );
    Tha6021DebugPrintInfoBit(debugger, "p3_resetdone"   , regValue, cDeviceP3_resetdone     );
    Tha6021DebugPrintInfoBit(debugger, "p2_resetdone"   , regValue, cDeviceP2_resetdone     );
    Tha6021DebugPrintInfoBit(debugger, "p1_resetdone"   , regValue, cDeviceP1_resetdone     );
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

static void MultiModulesDebugStickyShow(AtDevice self)
    {
    uint32 regAddres, regValue;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    regAddres = cDebugSticky;
    regValue  = AtHalRead(hal, regAddres);

    Tha6021DebugPrintRegName (debugger, "* Multiple modules error sticky", regAddres, regValue);
    Tha6021DebugPrintErrorBit(debugger, " olostsop"    , regValue, cDebugOlostsop   );
    Tha6021DebugPrintErrorBit(debugger, " olosteop"    , regValue, cDebugOlosteop   );
    Tha6021DebugPrintErrorBit(debugger, " ombiterr"    , regValue, cDebugOmbiterr   );
    Tha6021DebugPrintErrorBit(debugger, " wrca_emp"    , regValue, cDebugWrca_emp   );
    Tha6021DebugPrintErrorBit(debugger, " wrca_err"    , regValue, cDebugWrca_err   );
    Tha6021DebugPrintErrorBit(debugger, " blkemp"      , regValue, cDebugBlkemp     );
    Tha6021DebugPrintErrorBit(debugger, " sameblk"     , regValue, cDebugSameblk    );
    Tha6021DebugPrintErrorBit(debugger, " encfffull"   , regValue, cDebugEncfffull  );
    Tha6021DebugPrintErrorBit(debugger, " encffempt"   , regValue, cDebugEncffempt  );
    Tha6021DebugPrintErrorBit(debugger, " placa_emp"   , regValue, cDebugPlaca_emp  );
    Tha6021DebugPrintErrorBit(debugger, " placa_same"  , regValue, cDebugPlaca_same );
    Tha6021DebugPrintErrorBit(debugger, " plablk_emp"  , regValue, cDebugPlablk_emp );
    Tha6021DebugPrintErrorBit(debugger, " plablk_same" , regValue, cDebugPlablk_same);
    Tha6021DebugPrintErrorBit(debugger, " pweca_err"   , regValue, cDebugPweca_err  );
    Tha6021DebugPrintErrorBit(debugger, " pweca_same"  , regValue, cDebugPweca_same );
    Tha6021DebugPrintErrorBit(debugger, " placa_werr"  , regValue, cDebugPlaca_werr );
    Tha6021DebugPrintErrorBit(debugger, " plapk_werr"  , regValue, cDebugPlapk_werr );
    Tha6021DebugPrintErrorBit(debugger, " cmdwrfull"   , regValue, cDebugCmdwrfull  );
    Tha6021DebugPrintErrorBit(debugger, " avl_bbgerr"  , regValue, cDebugAvl_bbgerr );
    Tha6021DebugPrintErrorBit(debugger, " avl_enderr"  , regValue, cDebugAvl_enderr );
    Tha6021DebugPrintErrorBit(debugger, " avl_rvlerr"  , regValue, cDebugAvl_rvlerr );
    Tha6021DebugPrintErrorBit(debugger, " avl_rdfull"  , regValue, cDebugAvl_rdfull );
    Tha6021DebugPrintErrorBit(debugger, " vldfull"     , regValue, cDebugVldfull    );
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    AtHalWrite(hal, regAddres, regValue);
    }

static void Debug(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* Input clocks value:\r\n");
    ThaDeviceClockResultPrint(self, debugger, debugger ? "XGMII Tx Clock 156.25Mhz Value Status" : "    XGMII Tx Clock 156.25Mhz Value Status", 0xF00062, 156250000, 30);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "XGMII RX Clock 156.25Mhz Value Status" : "    XGMII RX Clock 156.25Mhz Value Status", 0xF00063, 156250000, 30);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "SGMII#1 Clock 125Mhz Value Status" : "    SGMII#1 Clock 125Mhz Value Status", 0xF00065, 125000000, 30);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "SGMII#2 Clock 125Mhz Value Status" : "    SGMII#2 Clock 125Mhz Value Status", 0xF00066, 125000000, 30);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "DDR3 User Clock 133Mhz ValueStatus" : "    DDR3 User Clock 133Mhz ValueStatus", 0xF00064, 133000000, 5);

    PllStickyShow(self);
    PllStatusShow(self);
    MultiModulesDebugStickyShow(self);
    Tha60210021EthErrorStickyDebug(AtDeviceModuleGet(self, cAtModuleEth));
    Tha60210021EthCounterStickyDebug(AtDeviceModuleGet(self, cAtModuleEth));
    Tha60210021QSGMIIorSGMIIErrorStickyDebug(AtDeviceModuleGet(self, cAtModuleEth));
    Tha60210021XfiErrorStickyDebug(AtDeviceModuleGet(self, cAtModuleEth));
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static ThaVersionReader VersionReaderCreate(ThaDevice self)
    {
    return Tha60210021VersionReaderNew((AtDevice)self);
    }

static eBool EthIntfClockIsGood(AtDevice self)
    {
    uint32 regVal;

    /* Do not need to check Ethernet interface clock for
     * EP here, it will be checked in general PLL check function */
    if (ThaDeviceIsEp((ThaDevice)self))
        return cAtTrue;

    if (ThaDeviceStickyIsGood(self, cDeviceStickyRegisters, cDeviceXfiPll1Locked | cDeviceXfiPll2Locked, 5000))
        {
        AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s, All XFIs done\r\n", AtFunction);
        return cAtTrue;
        }

    regVal = AtHalRead(AtDeviceIpCoreHalGet(self, 0), cDeviceStickyRegisters);

    if (regVal & cDeviceXfiPll1Locked)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: XFI#1 PCS not lock\r\n", AtFunction);

    if (regVal & cDeviceXfiPll2Locked)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: XFI#2 PCS not lock\r\n", AtFunction);

    return cAtFalse;
    }

static eAtRet AllCoresTest(AtDevice self)
    {
    if (EthIntfClockIsGood(self) == cAtFalse)
        {
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelWarning, "Ethernet interface clock fail\r\n");
        return cAtErrorXfiPcsNotLocked;
        }

    return m_AtDeviceMethods->AllCoresTest(self);
    }

static eBool UartIsSupported(AtDevice self)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader(self);
    uint32 startVersionSupportUart = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4624);

    if (ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader) >= startVersionSupportUart)
        return cAtTrue;

    return cAtFalse;
    }

static AtSemController SemControllerObjectCreate(AtDevice self, uint32 semId)
    {
    return Tha60210021SemControllerNew(self, semId);
    }

static eBool SemControllerIsSupported(AtDevice self)
    {
    if (ThaDeviceIsEp((ThaDevice)self))
        return cAtFalse;

    if (AtDeviceAllFeaturesAvailableInSimulation(self))
        return cAtTrue;

    if (UartIsSupported(self))
        return cAtTrue;

    return cAtFalse;
    }

static eBool ThermalSensorIsSupported(AtDevice self)
    {
    if (ThaDeviceIsEp((ThaDevice)self))
        return cAtFalse;

    if (AtDeviceAllFeaturesAvailableInSimulation(self))
        return cAtTrue;

    if (AtDeviceVersionNumber(self) >= Tha60210031StartVersionSupportThermalSensor(self))
        return cAtTrue;

    return cAtFalse;
    }

static eBool ShouldDisableHwAccessBeforeDeleting(AtDevice self)
    {
    return Tha6021ShouldDisableHwAccessBeforeDeleting(self);
    }

static uint8 NumSemControllersGet(AtDevice self)
    {
    if (UartIsSupported(self))
        return 1;
    return 0;
    }

static eBool ErrorGeneratorIsSupported(ThaDevice self)
    {
    uint32 startVersionSupportErrorGenerator = ThaVersionReaderHardwareBuiltNumberBuild(0x4, 0x5, 0x01);
    return (AtDeviceBuiltNumber((AtDevice)self) >= startVersionSupportErrorGenerator) ? cAtTrue : cAtFalse;
    }

static eAtRet AsyncInit(AtDevice self)
    {
    return Tha60210021DeviceAsyncInit(self);
    }

static eBool ClockStateValueV2IsSupported(ThaDevice self)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader((AtDevice)self);
    uint32 startSupportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4627);

    if (ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader) >= startSupportedVersion)
        return cAtTrue;

    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210021Device* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(asyncInitState);
    }

static eBool ShouldCheckHwResource(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtObject(AtDevice self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, Debug);
        mMethodOverride(m_AtDeviceOverride, NeedCheckSSKey);
        mMethodOverride(m_AtDeviceOverride, Setup);
        mMethodOverride(m_AtDeviceOverride, DiagnosticModeEnable);
        mMethodOverride(m_AtDeviceOverride, AllCoresTest);
        mMethodOverride(m_AtDeviceOverride, SemControllerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, SemControllerIsSupported);
        mMethodOverride(m_AtDeviceOverride, ThermalSensorIsSupported);
        mMethodOverride(m_AtDeviceOverride, NumSemControllersGet);
        mMethodOverride(m_AtDeviceOverride, ShouldDisableHwAccessBeforeDeleting);
        mMethodOverride(m_AtDeviceOverride, AsyncInit);
        mMethodOverride(m_AtDeviceOverride, ShouldCheckHwResource);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, m_ThaDeviceMethods, sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        mMethodOverride(m_ThaDeviceOverride, PllClkSysStatReg);
        mMethodOverride(m_ThaDeviceOverride, PllClkSysStatBitMask);
        mMethodOverride(m_ThaDeviceOverride, StartVersionSupportSurveillance);
        mMethodOverride(m_ThaDeviceOverride, VersionReaderCreate);
        mMethodOverride(m_ThaDeviceOverride, ErrorGeneratorIsSupported);
        mMethodOverride(m_ThaDeviceOverride, ClockStateValueV2IsSupported);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtObject(self);
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

AtDevice Tha60210021DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60210021DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210021DeviceObjectInit(newDevice, driver, productCode);
    }

uint32 Tha60210021DeviceStartVersionSupportPwIdleCode(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x09, 0x05, 0x27);
    }

uint32 Tha60210021DeviceStartVersionSupportMbitAlarm(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x09, 0x14, 0x28);
    }

uint32 Tha60210021DeviceStartVersionSupportPwDuplicatedPacket(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x09, 0x14, 0x28);
    }

uint32 Tha60210021DeviceStartVersionSupportXfiLinkStatusReadDirectly(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x09, 0x18, 0x28);
    }

eAtRet Tha60210021DeviceSuperAsyncInit(AtDevice self)
    {
    return m_AtDeviceMethods->AsyncInit(self);
    }

eAtRet Tha60210021DeviceSemCheck(AtDevice self)
    {
    return SemCheck(self);
    }

void Tha60210021DeviceHwFlush1(AtDevice self)
    {
    HwFlush1((ThaDevice)self);
    }

void Tha60210021DeviceHwFlush2(AtDevice self)
    {
    HwFlush2((ThaDevice)self);
    }

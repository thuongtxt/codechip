/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210021VersionReader.c
 *
 * Created Date: Dec 3, 2015
 *
 * Description : Version reader of 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60210011/man/Tha60210011VersionReader.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021VersionReader
    {
    tTha60210011VersionReader super;
    }tTha60210021VersionReader;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011VersionReaderMethods m_Tha60210011VersionReaderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionIgnoreDate(ThaVersionReader self)
    {
    AtUnused(self);
    return 0x15120139;
    }

static uint32 StartBuiltNumberIgnoreDate(ThaVersionReader self)
    {
    AtUnused(self);
    return 0x3901;
    }

static char *BuilNumberStringMake(ThaVersionReader self, uint8 firstIndex, uint8 secondIndex, uint8 thirdIndex)
    {
    static char buf[16];
    uint8 fourthIndex, fifthIndex;
    AtUnused(self);
    fourthIndex = (thirdIndex >> 4) & cBit3_0;
    fifthIndex = (thirdIndex  & cBit3_0);

    AtSprintf(buf, "%X.%X.%X.%X", firstIndex, secondIndex, fourthIndex, fifthIndex);
    return buf;
    }

static void OverrideTha60210011VersionReader(ThaVersionReader self)
    {
    Tha60210011VersionReader reader = (Tha60210011VersionReader)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011VersionReaderOverride, mMethodsGet(reader), sizeof(m_Tha60210011VersionReaderOverride));

        mMethodOverride(m_Tha60210011VersionReaderOverride, StartVersionIgnoreDate);
        mMethodOverride(m_Tha60210011VersionReaderOverride, StartBuiltNumberIgnoreDate);
        mMethodOverride(m_Tha60210011VersionReaderOverride, BuilNumberStringMake);
        }

    mMethodsSet(reader, &m_Tha60210011VersionReaderOverride);
    }

static void Override(ThaVersionReader self)
    {
    OverrideTha60210011VersionReader(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021VersionReader);
    }

static ThaVersionReader ObjectInit(ThaVersionReader self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (device == NULL)
        return NULL;

    /* Super constructor */
    if (Tha60210011VersionReaderObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaVersionReader Tha60210021VersionReaderNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaVersionReader newReader = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newReader == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newReader, device);
    }

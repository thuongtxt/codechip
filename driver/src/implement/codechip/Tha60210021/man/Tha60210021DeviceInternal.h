/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Devices
 * 
 * File        : Tha60210021DeviceInternal.h
 * 
 * Created Date: Dec 21, 2015
 *
 * Description : Devices of 60210021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021DEVICEINTERNAL_H_
#define _THA60210021DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaPdhPwProduct/man/ThaPdhPwProductDeviceInternal.h"
#include "Tha60210021Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210021Device
    {
    tThaPdhPwProductDevice super;

    uint32 asyncInitState;
    }tTha60210021Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60210021DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021DEVICEINTERNAL_H_ */


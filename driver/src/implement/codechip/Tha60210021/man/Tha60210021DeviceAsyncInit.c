/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210021DeviceAsyncInit.c
 *
 * Created Date: Aug 16, 2016
 *
 * Description : To handle async init
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210021DeviceInternal.h"
#include "Tha60210021DeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210021Device)(self))

/*--------------------------- Local typedefs ---------------------------------*/

typedef enum eAsyncStates
    {
    cTha60210021AsyncState_HwFlush1,
    cTha60210021AsyncState_HwFlush2,
    cTha60210021AsyncState_SupperAsync,
    cTha60210021AsyncState_SemCheck,
    cTha60210021AsyncState_ModulesHwResourceCheck,
    } eAsyncStates;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AsyncInitStateSet(AtDevice self, uint32 state)
    {
    mThis(self)->asyncInitState = state;
    }

static void AsyncInitStateReset(AtDevice self)
    {
    mThis(self)->asyncInitState = cTha60210021AsyncState_HwFlush1;
    }

static uint32 AsyncInitStateGet(AtDevice self)
    {
    return mThis(self)->asyncInitState;
    }

eAtRet Tha60210021DeviceAsyncInit(AtDevice self)
    {
    eAtRet ret = cAtOk;
    uint32 state = AsyncInitStateGet(self);
    tAtOsalCurTime profileTime;

    AtOsalCurTimeGet(&profileTime);
    switch (state)
        {
        case cTha60210021AsyncState_HwFlush1:
            Tha60210021DeviceHwFlush1(self);
            AsyncInitStateSet(self, cTha60210021AsyncState_HwFlush2);
            ret = cAtErrorAgain;
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "HwFlush1");
            AtDeviceAccessTimeInMsSinceLastProfileCheck((AtDevice)self, cAtTrue, &profileTime, AtSourceLocation, "HwFlush1");
            break;
            
        case cTha60210021AsyncState_HwFlush2:
            Tha60210021DeviceHwFlush2(self);
            AsyncInitStateSet(self, cTha60210021AsyncState_SupperAsync);
            ret = cAtErrorAgain;
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "HwFlush2");
            AtDeviceAccessTimeInMsSinceLastProfileCheck((AtDevice)self, cAtTrue, &profileTime, AtSourceLocation, "HwFlush2");
            break;
            
        case cTha60210021AsyncState_SupperAsync:
            ret = Tha60210021DeviceSuperAsyncInit(self);
            if (ret == cAtOk)
                {
        	    AsyncInitStateSet(self, cTha60210021AsyncState_SemCheck);
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
        	    AsyncInitStateReset(self);

            AtDeviceAccessTimeInMsSinceLastProfileCheck((AtDevice)self, cAtTrue, &profileTime, AtSourceLocation, "SupperAsync");
            break;

        case cTha60210021AsyncState_SemCheck:
            ret = Tha60210021DeviceSemCheck(self);
            if (ret == cAtOk)
                {
                AsyncInitStateSet(self, cTha60210021AsyncState_ModulesHwResourceCheck);
                ret = cAtErrorAgain;
                }

            else if (!AtDeviceAsyncRetValIsInState(ret))
                AsyncInitStateReset(self);

            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "SemCheck");
            AtDeviceAccessTimeInMsSinceLastProfileCheck((AtDevice)self, cAtTrue, &profileTime, AtSourceLocation, "SemCheck");
            break;

        case cTha60210021AsyncState_ModulesHwResourceCheck:
            ret = AtDeviceAllModulesHwResourceCheck(self);
            AsyncInitStateReset(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "ModulesResourceCheck");
            AtDeviceAccessTimeInMsSinceLastProfileCheck((AtDevice)self, cAtTrue, &profileTime, AtSourceLocation, "ModulesResourceCheck");
            break;

        default:
            AsyncInitStateReset(self);
            ret = cAtErrorDevFail;
        }

    return ret;
    }

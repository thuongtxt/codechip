/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : Interrupt Helper
 *
 * File        : Tha60210021InterruptControllerInternal.h
 *
 * Created Date: Aug 10, 2015
 *
 * Description : Interrupt Helper implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021INTERRUPTCONTROLLERINTERNAL_H_
#define _THA60210021INTERRUPTCONTROLLERINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/intrcontroller/ThaIntrControllerInternal.h"


#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210021InterruptController
    {
    tThaIntrController super;
    }tTha60210021InterruptController;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/

ThaIntrController Tha60210021InterruptControllerObjectInit(ThaIntrController self, AtIpCore ipCore);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210021INTERRUPTCONTROLLERINTERNAL_H_ */

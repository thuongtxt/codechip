/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210021InterruptController.c
 *
 * Created Date: Oct 2, 2013
 *
 * Description : Interrupt controller
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "Tha60210021DeviceReg.h"
#include "../pdh/Tha60210021ModulePdhReg.h"
#include "Tha60210021InterruptControllerInternal.h"
#include "../pmc/Tha60210021ModulePmcReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtIpCoreMethods  m_AtIpCoreOverride;
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Super implementation */
static const tThaIpCoreMethods *m_ThaIpCoreImplement;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet InterruptEnable(AtIpCore self, eBool enable)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    AtHalWrite(hal, cAf6Reg_inten_PIN, enable ? 1 : 0);
    return cAtOk;
    }

static eBool InterruptIsEnabled(AtIpCore self)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    return (AtHalRead(hal, cAf6Reg_inten_PIN) & cBit0) ? cAtTrue : cAtFalse;
    }

static uint32 PmcBaseAddress(void)
    {
    return 0x500000;
    }

static void AllModulesInterruptEnable(ThaIpCore self, uint32 intrMask)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (mMethodsGet(self)->CanEnableHwInterruptPin(self))
        AtIpCoreInterruptEnable((AtIpCore)self, cAtTrue);
    else
        AtHalWrite(hal, cAf6Reg_inten_ctrl, intrMask);
    }

static void HwInterruptEnable(ThaIpCore self, uint32 mask, eBool enable)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    uint32 interruptEnReg = AtHalRead(hal, cAf6Reg_inten_ctrl);

    if (enable)
        interruptEnReg |= mask;
    else
        interruptEnReg &= ~mask;

    AtHalWrite(hal, cAf6Reg_inten_ctrl, interruptEnReg);
    }

static eBool HwInterruptIsEnabled(ThaIpCore self, uint32 mask)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    uint32 regVal = AtHalRead(hal, cAf6Reg_inten_ctrl);
    return (regVal & mask) ? cAtTrue : cAtFalse;
    }

static void PdhHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(AtIpCoreDeviceGet((AtIpCore)self), cAtModulePdh);
    const uint32 baseAddress = ThaModulePdhBaseAddress(pdhModule);

    /* Enable global interrupt mask.*/
    ThaIpCoreHwInterruptEnable(self, cAf6_inten_ctrl_DE1IntEnable_Mask | cAf6_inten_ctrl_PRMIntEnable_Mask, enable);

    /* Enable per STS/VC interrupt */
    AtHalWrite(hal, baseAddress + cAf6Reg_dej1_rx_framer_per_stsvc_intr_en_ctrl, (enable) ? 0x3 : 0x0);
    }

static void PwHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    const uint32 baseAddress = PmcBaseAddress();

    ThaIpCoreHwInterruptEnable(self, cAf6_inten_ctrl_PWIntEnable_Mask, enable);

    /* Enable per PW slice interrupt */
    AtHalWrite(hal, baseAddress + cAf6Reg_counter_per_group_intr_en_ctrl, (enable) ? 0xFF : 0x0);
    }

static void SurHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self, cAf6_inten_ctrl_PMIntEnable_Mask | cAf6_inten_ctrl_FMIntEnable_Mask, enable);
    }

static void EthHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self, cAf6_inten_ctrl_EthIntEnable_Mask, enable);
    }

static void RamHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self, cAf6_inten_ctrl_ParIntEnable_Mask, enable);
    }

static void PhysicalHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self, cAf6_inten_ctrl_SysMonTempEnable_Mask|cAf6_inten_status_SeuIntEnable_Mask, enable);
    }

static void ClockHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self, cAf6_inten_ctrl_CDRIntEnable_Mask, enable);
    }

static eBool ThermalCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & cAf6_inten_ctrl_SysMonTempEnable_Mask) ? cAtTrue : cAtFalse;
    }

static eBool SemCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & cAf6_inten_status_SeuIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static eBool PdhCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    uint32 interruptMask = cAf6_inten_status_DE1IntEnable_Mask | cAf6_inten_status_PRMIntEnable_Mask;
	AtUnused(self);
    return (intrStatus & interruptMask) ? cAtTrue : cAtFalse;
    }

static eBool PwCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & cAf6_inten_status_PWIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static eBool SurCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & (cAf6_inten_status_PMIntEnable_Mask|cAf6_inten_status_FMIntEnable_Mask)) ? cAtTrue : cAtFalse;
    }

static eBool EthCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & cAf6_inten_ctrl_EthIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static eBool RamCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & cAf6_inten_ctrl_ParIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static eBool ClockCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & cAf6_inten_ctrl_CDRIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 InterruptStatusGet(ThaIpCore self)
    {
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (hal == NULL)
        return 0;

    return (AtHalRead(hal, cAf6Reg_inten_status));
    }

static uint32 InterruptDisable(ThaIpCore self)
    {
    uint32 interruptEnable = 0;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (hal == NULL)
        return 0;

    if (mMethodsGet(self)->CanEnableHwInterruptPin(self))
        {
        AtIpCoreInterruptEnable((AtIpCore)self, cAtFalse);
        return 0;
        }

    interruptEnable = AtHalRead(hal, cAf6Reg_inten_ctrl);
    AtHalWrite(hal, cAf6Reg_inten_ctrl, 0);

    return interruptEnable;
    }

static eAtRet SemInterruptEnable(AtIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable((ThaIpCore)self, cAf6_inten_status_SeuIntEnable_Mask, enable);
    return cAtOk;
    }

static eBool SemInterruptIsEnabled(AtIpCore self)
    {
    return HwInterruptIsEnabled((ThaIpCore)self, cAf6_inten_status_SeuIntEnable_Mask);
    }

static eAtRet SysmonInterruptEnable(AtIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable((ThaIpCore)self, cAf6_inten_status_SysMonTempEnable_Mask, enable);
    return cAtOk;
    }

static eBool SysmonInterruptIsEnabled(AtIpCore self)
    {
    return HwInterruptIsEnabled((ThaIpCore)self, cAf6_inten_status_SysMonTempEnable_Mask);
    }

static uint32 StartVersionSupportInterruptRestore(AtDevice device)
    {
    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return 0;

    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x16, 0x01, 0x05, 0x43);
    }

static eBool InterruptRestoreIsSupported(ThaIpCore self)
    {
    AtDevice device = AtIpCoreDeviceGet(mIpCore(self));

    if (AtDeviceVersionNumber(device) >= StartVersionSupportInterruptRestore(device))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet InterruptRestore(ThaIpCore self)
    {
    uint32 interruptEnable = 0;
    AtHal hal = AtIpCoreHalGet(mIpCore(self));
    if (hal == NULL)
        return 0;

    if (!mMethodsGet(self)->InterruptRestoreIsSupported(self))
        return cAtErrorModeNotSupport;

    /* Read device interrupt mask.*/
    interruptEnable = AtHalRead(hal, cAf6Reg_inten_ctrl);

    /* Produce trigger to restore interrupt. */
    AtHalWrite(hal, cAf6Reg_inten_restore, 0);
    AtHalWrite(hal, cAf6Reg_inten_restore, interruptEnable);

    return cAtOk;
    }

static uint32 StartVersionSupportInterruptPinRegister(AtDevice device)
    {
    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return 0;

    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x09, 0x05, 0x27);
    }

static eBool CanEnableHwInterruptPin(ThaIpCore self)
    {
    AtDevice device = AtIpCoreDeviceGet(mIpCore(self));

    if (AtDeviceVersionNumber(device) >= StartVersionSupportInterruptPinRegister(device))
        return cAtTrue;
    return cAtFalse;
    }

static void OverrideThaIpCore(ThaIntrController self)
    {
    ThaIpCore thaIp = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(thaIp);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));

        mMethodOverride(m_ThaIpCoreOverride, InterruptStatusGet);
        mMethodOverride(m_ThaIpCoreOverride, InterruptDisable);
        mMethodOverride(m_ThaIpCoreOverride, InterruptRestore);
        mMethodOverride(m_ThaIpCoreOverride, PdhHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PwHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, SurHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, EthHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, RamHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PhysicalHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, ClockHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PdhCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, PwCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, SurCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, EthCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, RamCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, ThermalCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, SemCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, ClockCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, AllModulesInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, CanEnableHwInterruptPin);
        mMethodOverride(m_ThaIpCoreOverride, InterruptRestoreIsSupported);
        mMethodOverride(m_ThaIpCoreOverride, HwInterruptEnable);
        }

    mMethodsSet(thaIp, &m_ThaIpCoreOverride);
    }

static void OverrideAtIpCore(ThaIntrController self)
    {
    AtIpCore ipCore = (AtIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIpCoreOverride, mMethodsGet(ipCore), sizeof(m_AtIpCoreOverride));

        mMethodOverride(m_AtIpCoreOverride, InterruptEnable);
        mMethodOverride(m_AtIpCoreOverride, InterruptIsEnabled);
        mMethodOverride(m_AtIpCoreOverride, SemInterruptEnable);
        mMethodOverride(m_AtIpCoreOverride, SemInterruptIsEnabled);
        mMethodOverride(m_AtIpCoreOverride, SysmonInterruptEnable);
        mMethodOverride(m_AtIpCoreOverride, SysmonInterruptIsEnabled);
        }

    mMethodsSet(ipCore, &m_AtIpCoreOverride);
    }

static void Override(ThaIntrController self)
    {
    OverrideThaIpCore(self);
    OverrideAtIpCore(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021InterruptController);
    }

ThaIntrController Tha60210021InterruptControllerObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaIntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaIntrController Tha60210021IntrControllerNew(AtIpCore ipCore)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021InterruptControllerObjectInit(newController, ipCore);
    }

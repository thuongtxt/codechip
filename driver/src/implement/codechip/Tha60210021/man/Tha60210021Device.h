/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : Device Manager
 *
 * File        : Tha60210021Device.h
 *
 * Created Date: Jul 28, 2015
 *
 * Description : Device Manager header.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021DEVICE_H_
#define _THA60210021DEVICE_H_

/*--------------------------- Include files ----------------------------------*/


#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210021Device *Tha60210021Device;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
AtDevice Tha60210021DeviceNew(AtDriver driver, uint32 productCode);
uint32 Tha60210021DeviceStartVersionSupportPwIdleCode(AtDevice self);
uint32 Tha60210021DeviceStartVersionSupportMbitAlarm(AtDevice self);
uint32 Tha60210021DeviceStartVersionSupportPwDuplicatedPacket(AtDevice self);
uint32 Tha60210021DeviceStartVersionSupportXfiLinkStatusReadDirectly(AtDevice self);
eBool Tha60210021DeviceErrorGeneratorIsSupported(AtDevice self);
AtSemController Tha60210021SemControllerNew(AtDevice device, uint32 semId);

#ifdef __cplusplus
}
#endif

#endif /* _THA60210021DEVICE_H_ */

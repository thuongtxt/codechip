/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0021_RD_GLB_H_
#define _AF6_REG_AF6CCI0021_RD_GLB_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Chip Version
Reg Addr   : 0x000000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to check CHIP version.

------------------------------------------------------------------------------*/
#define cAf6Reg_chip_ver_Base                                                                         0x000000
#define cAf6Reg_chip_ver                                                                              0x000000
#define cAf6Reg_chip_ver_WidthVal                                                                           64
#define cAf6Reg_chip_ver_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: YMMDDHH
BitField Type: RO
BitField Desc:
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_chip_ver_YMMDDHH_Bit_Start                                                                     32
#define cAf6_chip_ver_YMMDDHH_Bit_End                                                                       63
#define cAf6_chip_ver_YMMDDHH_Mask                                                                    cBit31_0
#define cAf6_chip_ver_YMMDDHH_Shift                                                                          0
#define cAf6_chip_ver_YMMDDHH_MaxVal                                                                       0x0
#define cAf6_chip_ver_YMMDDHH_MinVal                                                                       0x0
#define cAf6_chip_ver_YMMDDHH_RstVal                                                                       0xX

/*--------------------------------------
BitField Name: roductID
BitField Type: RO
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_chip_ver_roductID_Bit_Start                                                                     0
#define cAf6_chip_ver_roductID_Bit_End                                                                      31
#define cAf6_chip_ver_roductID_Mask                                                                   cBit31_0
#define cAf6_chip_ver_roductID_Shift                                                                         0
#define cAf6_chip_ver_roductID_MaxVal                                                               0xffffffff
#define cAf6_chip_ver_roductID_MinVal                                                                      0x0
#define cAf6_chip_ver_roductID_RstVal                                                               0x60031031


/*------------------------------------------------------------------------------
Reg Name   : Chip Active Control
Reg Addr   : 0x000001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure global signal for CHIP.

------------------------------------------------------------------------------*/
#define cAf6Reg_chip_act_ctrl_Base                                                                    0x000001
#define cAf6Reg_chip_act_ctrl                                                                         0x000001
#define cAf6Reg_chip_act_ctrl_WidthVal                                                                      32
#define cAf6Reg_chip_act_ctrl_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: PmcPact
BitField Type: RW
BitField Desc: PMC Active 1: Enable 0: Disable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_chip_act_ctrl_PmcPact_Bit_Start                                                                 9
#define cAf6_chip_act_ctrl_PmcPact_Bit_End                                                                   9
#define cAf6_chip_act_ctrl_PmcPact_Mask                                                                  cBit9
#define cAf6_chip_act_ctrl_PmcPact_Shift                                                                     9
#define cAf6_chip_act_ctrl_PmcPact_MaxVal                                                                  0x1
#define cAf6_chip_act_ctrl_PmcPact_MinVal                                                                  0x0
#define cAf6_chip_act_ctrl_PmcPact_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: EthPact
BitField Type: RW
BitField Desc: Ethernet Interface Active 1: Enable 0: Disable
BitField Bits: [8]
--------------------------------------*/
#define cAf6_chip_act_ctrl_EthPact_Bit_Start                                                                 8
#define cAf6_chip_act_ctrl_EthPact_Bit_End                                                                   8
#define cAf6_chip_act_ctrl_EthPact_Mask                                                                  cBit8
#define cAf6_chip_act_ctrl_EthPact_Shift                                                                     8
#define cAf6_chip_act_ctrl_EthPact_MaxVal                                                                  0x1
#define cAf6_chip_act_ctrl_EthPact_MinVal                                                                  0x0
#define cAf6_chip_act_ctrl_EthPact_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: cdrPact
BitField Type: RW
BitField Desc: CDR Active 1: Enable 0: Disable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_chip_act_ctrl_cdrPact_Bit_Start                                                                 7
#define cAf6_chip_act_ctrl_cdrPact_Bit_End                                                                   7
#define cAf6_chip_act_ctrl_cdrPact_Mask                                                                  cBit7
#define cAf6_chip_act_ctrl_cdrPact_Shift                                                                     7
#define cAf6_chip_act_ctrl_cdrPact_MaxVal                                                                  0x1
#define cAf6_chip_act_ctrl_cdrPact_MinVal                                                                  0x0
#define cAf6_chip_act_ctrl_cdrPact_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: claPact
BitField Type: RW
BitField Desc: CLA Active 1: Enable 0: Disable
BitField Bits: [6]
--------------------------------------*/
#define cAf6_chip_act_ctrl_claPact_Bit_Start                                                                 6
#define cAf6_chip_act_ctrl_claPact_Bit_End                                                                   6
#define cAf6_chip_act_ctrl_claPact_Mask                                                                  cBit6
#define cAf6_chip_act_ctrl_claPact_Shift                                                                     6
#define cAf6_chip_act_ctrl_claPact_MaxVal                                                                  0x1
#define cAf6_chip_act_ctrl_claPact_MinVal                                                                  0x0
#define cAf6_chip_act_ctrl_claPact_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: PwePact
BitField Type: RW
BitField Desc: PWE Active 1: Enable 0: Disable
BitField Bits: [5]
--------------------------------------*/
#define cAf6_chip_act_ctrl_PwePact_Bit_Start                                                                 5
#define cAf6_chip_act_ctrl_PwePact_Bit_End                                                                   5
#define cAf6_chip_act_ctrl_PwePact_Mask                                                                  cBit5
#define cAf6_chip_act_ctrl_PwePact_Shift                                                                     5
#define cAf6_chip_act_ctrl_PwePact_MaxVal                                                                  0x1
#define cAf6_chip_act_ctrl_PwePact_MinVal                                                                  0x0
#define cAf6_chip_act_ctrl_PwePact_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: PdaPact
BitField Type: RW
BitField Desc: PDA Active 1: Enable 0: Disable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_chip_act_ctrl_PdaPact_Bit_Start                                                                 4
#define cAf6_chip_act_ctrl_PdaPact_Bit_End                                                                   4
#define cAf6_chip_act_ctrl_PdaPact_Mask                                                                  cBit4
#define cAf6_chip_act_ctrl_PdaPact_Shift                                                                     4
#define cAf6_chip_act_ctrl_PdaPact_MaxVal                                                                  0x1
#define cAf6_chip_act_ctrl_PdaPact_MinVal                                                                  0x0
#define cAf6_chip_act_ctrl_PdaPact_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: plaPact
BitField Type: RW
BitField Desc: PLA Active 1: Enable 0: Disable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_chip_act_ctrl_plaPact_Bit_Start                                                                 3
#define cAf6_chip_act_ctrl_plaPact_Bit_End                                                                   3
#define cAf6_chip_act_ctrl_plaPact_Mask                                                                  cBit3
#define cAf6_chip_act_ctrl_plaPact_Shift                                                                     3
#define cAf6_chip_act_ctrl_plaPact_MaxVal                                                                  0x1
#define cAf6_chip_act_ctrl_plaPact_MinVal                                                                  0x0
#define cAf6_chip_act_ctrl_plaPact_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: MapPact
BitField Type: RW
BitField Desc: MAP Active 1: Enable 0: Disable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_chip_act_ctrl_MapPact_Bit_Start                                                                 2
#define cAf6_chip_act_ctrl_MapPact_Bit_End                                                                   2
#define cAf6_chip_act_ctrl_MapPact_Mask                                                                  cBit2
#define cAf6_chip_act_ctrl_MapPact_Shift                                                                     2
#define cAf6_chip_act_ctrl_MapPact_MaxVal                                                                  0x1
#define cAf6_chip_act_ctrl_MapPact_MinVal                                                                  0x0
#define cAf6_chip_act_ctrl_MapPact_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: PdhPact
BitField Type: RW
BitField Desc: PDH Active 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_chip_act_ctrl_PdhPact_Bit_Start                                                                 1
#define cAf6_chip_act_ctrl_PdhPact_Bit_End                                                                   1
#define cAf6_chip_act_ctrl_PdhPact_Mask                                                                  cBit1
#define cAf6_chip_act_ctrl_PdhPact_Shift                                                                     1
#define cAf6_chip_act_ctrl_PdhPact_MaxVal                                                                  0x1
#define cAf6_chip_act_ctrl_PdhPact_MinVal                                                                  0x0
#define cAf6_chip_act_ctrl_PdhPact_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: OcnPact
BitField Type: RW
BitField Desc: OCN Active 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_chip_act_ctrl_OcnPact_Bit_Start                                                                 0
#define cAf6_chip_act_ctrl_OcnPact_Bit_End                                                                   0
#define cAf6_chip_act_ctrl_OcnPact_Mask                                                                  cBit0
#define cAf6_chip_act_ctrl_OcnPact_Shift                                                                     0
#define cAf6_chip_act_ctrl_OcnPact_MaxVal                                                                  0x1
#define cAf6_chip_act_ctrl_OcnPact_MinVal                                                                  0x0
#define cAf6_chip_act_ctrl_OcnPact_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : Chip Interrupt Status
Reg Addr   : 0x00_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_status_Base                                                                     0x000002
#define cAf6Reg_inten_status                                                                          0x000002
#define cAf6Reg_inten_status_WidthVal                                                                       32
#define cAf6Reg_inten_status_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: CDRIntEnable
BitField Type: RW
BitField Desc: CDR Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [28]
--------------------------------------*/
#define cAf6_inten_status_CDRIntEnable_Bit_Start                                                            28
#define cAf6_inten_status_CDRIntEnable_Bit_End                                                              28
#define cAf6_inten_status_CDRIntEnable_Mask                                                             cBit28
#define cAf6_inten_status_CDRIntEnable_Shift                                                                28
#define cAf6_inten_status_CDRIntEnable_MaxVal                                                              0x1
#define cAf6_inten_status_CDRIntEnable_MinVal                                                              0x0
#define cAf6_inten_status_CDRIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: SysMonTempEnable
BitField Type: RW
BitField Desc: SysMon temperature Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [27]
--------------------------------------*/
#define cAf6_inten_status_SysMonTempEnable_Bit_Start                                                        27
#define cAf6_inten_status_SysMonTempEnable_Bit_End                                                          27
#define cAf6_inten_status_SysMonTempEnable_Mask                                                         cBit27
#define cAf6_inten_status_SysMonTempEnable_Shift                                                            27
#define cAf6_inten_status_SysMonTempEnable_MaxVal                                                          0x1
#define cAf6_inten_status_SysMonTempEnable_MinVal                                                          0x0
#define cAf6_inten_status_SysMonTempEnable_RstVal                                                          0x0

/*--------------------------------------
BitField Name: ParIntEnable
BitField Type: RW
BitField Desc: RAM Parity Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [26]
--------------------------------------*/
#define cAf6_inten_status_ParIntEnable_Bit_Start                                                            26
#define cAf6_inten_status_ParIntEnable_Bit_End                                                              26
#define cAf6_inten_status_ParIntEnable_Mask                                                             cBit26
#define cAf6_inten_status_ParIntEnable_Shift                                                                26
#define cAf6_inten_status_ParIntEnable_MaxVal                                                              0x1
#define cAf6_inten_status_ParIntEnable_MinVal                                                              0x0
#define cAf6_inten_status_ParIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: PRMIntEnable
BitField Type: RW
BitField Desc: PRM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [24]
--------------------------------------*/
#define cAf6_inten_status_PRMIntEnable_Bit_Start                                                            24
#define cAf6_inten_status_PRMIntEnable_Bit_End                                                              24
#define cAf6_inten_status_PRMIntEnable_Mask                                                             cBit24
#define cAf6_inten_status_PRMIntEnable_Shift                                                                24
#define cAf6_inten_status_PRMIntEnable_MaxVal                                                              0x1
#define cAf6_inten_status_PRMIntEnable_MinVal                                                              0x0
#define cAf6_inten_status_PRMIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: PMIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [21]
--------------------------------------*/
#define cAf6_inten_status_PMIntEnable_Bit_Start                                                             21
#define cAf6_inten_status_PMIntEnable_Bit_End                                                               21
#define cAf6_inten_status_PMIntEnable_Mask                                                              cBit21
#define cAf6_inten_status_PMIntEnable_Shift                                                                 21
#define cAf6_inten_status_PMIntEnable_MaxVal                                                               0x1
#define cAf6_inten_status_PMIntEnable_MinVal                                                               0x0
#define cAf6_inten_status_PMIntEnable_RstVal                                                               0x0

/*--------------------------------------
BitField Name: FMIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_inten_status_FMIntEnable_Bit_Start                                                             20
#define cAf6_inten_status_FMIntEnable_Bit_End                                                               20
#define cAf6_inten_status_FMIntEnable_Mask                                                              cBit20
#define cAf6_inten_status_FMIntEnable_Shift                                                                 20
#define cAf6_inten_status_FMIntEnable_MaxVal                                                               0x1
#define cAf6_inten_status_FMIntEnable_MinVal                                                               0x0
#define cAf6_inten_status_FMIntEnable_RstVal                                                               0x0

/*--------------------------------------
BitField Name: SeuIntEnable
BitField Type: RW
BitField Desc: SEU Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [19]
--------------------------------------*/
#define cAf6_inten_status_SeuIntEnable_Bit_Start                                                            19
#define cAf6_inten_status_SeuIntEnable_Bit_End                                                              19
#define cAf6_inten_status_SeuIntEnable_Mask                                                             cBit19
#define cAf6_inten_status_SeuIntEnable_Shift                                                                19
#define cAf6_inten_status_SeuIntEnable_MaxVal                                                              0x1
#define cAf6_inten_status_SeuIntEnable_MinVal                                                              0x0
#define cAf6_inten_status_SeuIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: ETHIntEnable
BitField Type: RW
BitField Desc: ETH Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_inten_status_ETHIntEnable_Bit_Start                                                            18
#define cAf6_inten_status_ETHIntEnable_Bit_End                                                              18
#define cAf6_inten_status_ETHIntEnable_Mask                                                             cBit18
#define cAf6_inten_status_ETHIntEnable_Shift                                                                18
#define cAf6_inten_status_ETHIntEnable_MaxVal                                                              0x1
#define cAf6_inten_status_ETHIntEnable_MinVal                                                              0x0
#define cAf6_inten_status_ETHIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: PWIntEnable
BitField Type: RW
BitField Desc: PW Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_inten_status_PWIntEnable_Bit_Start                                                             16
#define cAf6_inten_status_PWIntEnable_Bit_End                                                               16
#define cAf6_inten_status_PWIntEnable_Mask                                                              cBit16
#define cAf6_inten_status_PWIntEnable_Shift                                                                 16
#define cAf6_inten_status_PWIntEnable_MaxVal                                                               0x1
#define cAf6_inten_status_PWIntEnable_MinVal                                                               0x0
#define cAf6_inten_status_PWIntEnable_RstVal                                                               0x0

/*--------------------------------------
BitField Name: DE1IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_inten_status_DE1IntEnable_Bit_Start                                                             9
#define cAf6_inten_status_DE1IntEnable_Bit_End                                                               9
#define cAf6_inten_status_DE1IntEnable_Mask                                                              cBit9
#define cAf6_inten_status_DE1IntEnable_Shift                                                                 9
#define cAf6_inten_status_DE1IntEnable_MaxVal                                                              0x1
#define cAf6_inten_status_DE1IntEnable_MinVal                                                              0x0
#define cAf6_inten_status_DE1IntEnable_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : Chip Interrupt Enable Control
Reg Addr   : 0x00_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_ctrl_Base                                                                       0x000003
#define cAf6Reg_inten_ctrl                                                                            0x000003
#define cAf6Reg_inten_ctrl_WidthVal                                                                         32
#define cAf6Reg_inten_ctrl_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: CDRIntEnable
BitField Type: RW
BitField Desc: CDR Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [28]
--------------------------------------*/
#define cAf6_inten_ctrl_CDRIntEnable_Bit_Start                                                              28
#define cAf6_inten_ctrl_CDRIntEnable_Bit_End                                                                28
#define cAf6_inten_ctrl_CDRIntEnable_Mask                                                               cBit28
#define cAf6_inten_ctrl_CDRIntEnable_Shift                                                                  28
#define cAf6_inten_ctrl_CDRIntEnable_MaxVal                                                                0x1
#define cAf6_inten_ctrl_CDRIntEnable_MinVal                                                                0x0
#define cAf6_inten_ctrl_CDRIntEnable_RstVal                                                                0x0

/*--------------------------------------
BitField Name: SysMonTempEnable
BitField Type: RW
BitField Desc: SysMon temperature Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [27]
--------------------------------------*/
#define cAf6_inten_ctrl_SysMonTempEnable_Bit_Start                                                          27
#define cAf6_inten_ctrl_SysMonTempEnable_Bit_End                                                            27
#define cAf6_inten_ctrl_SysMonTempEnable_Mask                                                           cBit27
#define cAf6_inten_ctrl_SysMonTempEnable_Shift                                                              27
#define cAf6_inten_ctrl_SysMonTempEnable_MaxVal                                                            0x1
#define cAf6_inten_ctrl_SysMonTempEnable_MinVal                                                            0x0
#define cAf6_inten_ctrl_SysMonTempEnable_RstVal                                                            0x0

/*--------------------------------------
BitField Name: ParIntEnable
BitField Type: RW
BitField Desc: RAM Parity Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [26]
--------------------------------------*/
#define cAf6_inten_ctrl_ParIntEnable_Bit_Start                                                              26
#define cAf6_inten_ctrl_ParIntEnable_Bit_End                                                                26
#define cAf6_inten_ctrl_ParIntEnable_Mask                                                               cBit26
#define cAf6_inten_ctrl_ParIntEnable_Shift                                                                  26
#define cAf6_inten_ctrl_ParIntEnable_MaxVal                                                                0x1
#define cAf6_inten_ctrl_ParIntEnable_MinVal                                                                0x0
#define cAf6_inten_ctrl_ParIntEnable_RstVal                                                                0x0

/*--------------------------------------
BitField Name: PRMIntEnable
BitField Type: RW
BitField Desc: PRM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [24]
--------------------------------------*/
#define cAf6_inten_ctrl_PRMIntEnable_Bit_Start                                                              24
#define cAf6_inten_ctrl_PRMIntEnable_Bit_End                                                                24
#define cAf6_inten_ctrl_PRMIntEnable_Mask                                                               cBit24
#define cAf6_inten_ctrl_PRMIntEnable_Shift                                                                  24
#define cAf6_inten_ctrl_PRMIntEnable_MaxVal                                                                0x1
#define cAf6_inten_ctrl_PRMIntEnable_MinVal                                                                0x0
#define cAf6_inten_ctrl_PRMIntEnable_RstVal                                                                0x0

/*--------------------------------------
BitField Name: PMIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [21]
--------------------------------------*/
#define cAf6_inten_ctrl_PMIntEnable_Bit_Start                                                               21
#define cAf6_inten_ctrl_PMIntEnable_Bit_End                                                                 21
#define cAf6_inten_ctrl_PMIntEnable_Mask                                                                cBit21
#define cAf6_inten_ctrl_PMIntEnable_Shift                                                                   21
#define cAf6_inten_ctrl_PMIntEnable_MaxVal                                                                 0x1
#define cAf6_inten_ctrl_PMIntEnable_MinVal                                                                 0x0
#define cAf6_inten_ctrl_PMIntEnable_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: FMIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_inten_ctrl_FMIntEnable_Bit_Start                                                               20
#define cAf6_inten_ctrl_FMIntEnable_Bit_End                                                                 20
#define cAf6_inten_ctrl_FMIntEnable_Mask                                                                cBit20
#define cAf6_inten_ctrl_FMIntEnable_Shift                                                                   20
#define cAf6_inten_ctrl_FMIntEnable_MaxVal                                                                 0x1
#define cAf6_inten_ctrl_FMIntEnable_MinVal                                                                 0x0
#define cAf6_inten_ctrl_FMIntEnable_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: EthIntEnable
BitField Type: RW
BitField Desc: ETH Port Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_inten_ctrl_EthIntEnable_Bit_Start                                                              18
#define cAf6_inten_ctrl_EthIntEnable_Bit_End                                                                18
#define cAf6_inten_ctrl_EthIntEnable_Mask                                                               cBit18
#define cAf6_inten_ctrl_EthIntEnable_Shift                                                                  18
#define cAf6_inten_ctrl_EthIntEnable_MaxVal                                                                0x1
#define cAf6_inten_ctrl_EthIntEnable_MinVal                                                                0x0
#define cAf6_inten_ctrl_EthIntEnable_RstVal                                                                0x0

/*--------------------------------------
BitField Name: PWIntEnable
BitField Type: RW
BitField Desc: PW Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_inten_ctrl_PWIntEnable_Bit_Start                                                               16
#define cAf6_inten_ctrl_PWIntEnable_Bit_End                                                                 16
#define cAf6_inten_ctrl_PWIntEnable_Mask                                                                cBit16
#define cAf6_inten_ctrl_PWIntEnable_Shift                                                                   16
#define cAf6_inten_ctrl_PWIntEnable_MaxVal                                                                 0x1
#define cAf6_inten_ctrl_PWIntEnable_MinVal                                                                 0x0
#define cAf6_inten_ctrl_PWIntEnable_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: DE1IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_inten_ctrl_DE1IntEnable_Bit_Start                                                               9
#define cAf6_inten_ctrl_DE1IntEnable_Bit_End                                                                 9
#define cAf6_inten_ctrl_DE1IntEnable_Mask                                                                cBit9
#define cAf6_inten_ctrl_DE1IntEnable_Shift                                                                   9
#define cAf6_inten_ctrl_DE1IntEnable_MaxVal                                                                0x1
#define cAf6_inten_ctrl_DE1IntEnable_MinVal                                                                0x0
#define cAf6_inten_ctrl_DE1IntEnable_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : Chip Interrupt Restore Control
Reg Addr   : 0x00_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt restore enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_restore_Base                                                                    0x000004
#define cAf6Reg_inten_restore                                                                         0x000004
#define cAf6Reg_inten_restore_WidthVal                                                                      32
#define cAf6Reg_inten_restore_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: CDRIntEnable
BitField Type: RW
BitField Desc: CDR Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [28]
--------------------------------------*/
#define cAf6_inten_restore_CDRIntEnable_Bit_Start                                                           28
#define cAf6_inten_restore_CDRIntEnable_Bit_End                                                             28
#define cAf6_inten_restore_CDRIntEnable_Mask                                                            cBit28
#define cAf6_inten_restore_CDRIntEnable_Shift                                                               28
#define cAf6_inten_restore_CDRIntEnable_MaxVal                                                             0x1
#define cAf6_inten_restore_CDRIntEnable_MinVal                                                             0x0
#define cAf6_inten_restore_CDRIntEnable_RstVal                                                             0x0

/*--------------------------------------
BitField Name: SysMonTempEnable
BitField Type: RW
BitField Desc: SysMon temperature Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [27]
--------------------------------------*/
#define cAf6_inten_restore_SysMonTempEnable_Bit_Start                                                       27
#define cAf6_inten_restore_SysMonTempEnable_Bit_End                                                         27
#define cAf6_inten_restore_SysMonTempEnable_Mask                                                        cBit27
#define cAf6_inten_restore_SysMonTempEnable_Shift                                                           27
#define cAf6_inten_restore_SysMonTempEnable_MaxVal                                                         0x1
#define cAf6_inten_restore_SysMonTempEnable_MinVal                                                         0x0
#define cAf6_inten_restore_SysMonTempEnable_RstVal                                                         0x0

/*--------------------------------------
BitField Name: ParIntEnable
BitField Type: RW
BitField Desc: RAM Parity Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [26]
--------------------------------------*/
#define cAf6_inten_restore_ParIntEnable_Bit_Start                                                           26
#define cAf6_inten_restore_ParIntEnable_Bit_End                                                             26
#define cAf6_inten_restore_ParIntEnable_Mask                                                            cBit26
#define cAf6_inten_restore_ParIntEnable_Shift                                                               26
#define cAf6_inten_restore_ParIntEnable_MaxVal                                                             0x1
#define cAf6_inten_restore_ParIntEnable_MinVal                                                             0x0
#define cAf6_inten_restore_ParIntEnable_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PRMIntEnable
BitField Type: RW
BitField Desc: PRM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [24]
--------------------------------------*/
#define cAf6_inten_restore_PRMIntEnable_Bit_Start                                                           24
#define cAf6_inten_restore_PRMIntEnable_Bit_End                                                             24
#define cAf6_inten_restore_PRMIntEnable_Mask                                                            cBit24
#define cAf6_inten_restore_PRMIntEnable_Shift                                                               24
#define cAf6_inten_restore_PRMIntEnable_MaxVal                                                             0x1
#define cAf6_inten_restore_PRMIntEnable_MinVal                                                             0x0
#define cAf6_inten_restore_PRMIntEnable_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PMIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [21]
--------------------------------------*/
#define cAf6_inten_restore_PMIntEnable_Bit_Start                                                            21
#define cAf6_inten_restore_PMIntEnable_Bit_End                                                              21
#define cAf6_inten_restore_PMIntEnable_Mask                                                             cBit21
#define cAf6_inten_restore_PMIntEnable_Shift                                                                21
#define cAf6_inten_restore_PMIntEnable_MaxVal                                                              0x1
#define cAf6_inten_restore_PMIntEnable_MinVal                                                              0x0
#define cAf6_inten_restore_PMIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: FMIntEnable
BitField Type: RW
BitField Desc: FM Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_inten_restore_FMIntEnable_Bit_Start                                                            20
#define cAf6_inten_restore_FMIntEnable_Bit_End                                                              20
#define cAf6_inten_restore_FMIntEnable_Mask                                                             cBit20
#define cAf6_inten_restore_FMIntEnable_Shift                                                                20
#define cAf6_inten_restore_FMIntEnable_MaxVal                                                              0x1
#define cAf6_inten_restore_FMIntEnable_MinVal                                                              0x0
#define cAf6_inten_restore_FMIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: EthIntEnable
BitField Type: RW
BitField Desc: ETH Port Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_inten_restore_EthIntEnable_Bit_Start                                                           18
#define cAf6_inten_restore_EthIntEnable_Bit_End                                                             18
#define cAf6_inten_restore_EthIntEnable_Mask                                                            cBit18
#define cAf6_inten_restore_EthIntEnable_Shift                                                               18
#define cAf6_inten_restore_EthIntEnable_MaxVal                                                             0x1
#define cAf6_inten_restore_EthIntEnable_MinVal                                                             0x0
#define cAf6_inten_restore_EthIntEnable_RstVal                                                             0x0

/*--------------------------------------
BitField Name: PWIntEnable
BitField Type: RW
BitField Desc: PW Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_inten_restore_PWIntEnable_Bit_Start                                                            16
#define cAf6_inten_restore_PWIntEnable_Bit_End                                                              16
#define cAf6_inten_restore_PWIntEnable_Mask                                                             cBit16
#define cAf6_inten_restore_PWIntEnable_Shift                                                                16
#define cAf6_inten_restore_PWIntEnable_MaxVal                                                              0x1
#define cAf6_inten_restore_PWIntEnable_MinVal                                                              0x0
#define cAf6_inten_restore_PWIntEnable_RstVal                                                              0x0

/*--------------------------------------
BitField Name: DE1IntEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [9]
--------------------------------------*/
#define cAf6_inten_restore_DE1IntEnable_Bit_Start                                                            9
#define cAf6_inten_restore_DE1IntEnable_Bit_End                                                              9
#define cAf6_inten_restore_DE1IntEnable_Mask                                                             cBit9
#define cAf6_inten_restore_DE1IntEnable_Shift                                                                9
#define cAf6_inten_restore_DE1IntEnable_MaxVal                                                             0x1
#define cAf6_inten_restore_DE1IntEnable_MinVal                                                             0x0
#define cAf6_inten_restore_DE1IntEnable_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : Chip Interrupt PIN Control
Reg Addr   : 0x00_0005
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt PIN enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_PIN_Base                                                                        0x000005
#define cAf6Reg_inten_PIN                                                                             0x000005
#define cAf6Reg_inten_PIN_WidthVal                                                                          32
#define cAf6Reg_inten_PIN_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: DefectPINEnable
BitField Type: RW
BitField Desc: PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_inten_PIN_DefectPINEnable_Bit_Start                                                             0
#define cAf6_inten_PIN_DefectPINEnable_Bit_End                                                               0
#define cAf6_inten_PIN_DefectPINEnable_Mask                                                              cBit0
#define cAf6_inten_PIN_DefectPINEnable_Shift                                                                 0
#define cAf6_inten_PIN_DefectPINEnable_MaxVal                                                              0x1
#define cAf6_inten_PIN_DefectPINEnable_MinVal                                                              0x0
#define cAf6_inten_PIN_DefectPINEnable_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : EPort Interrupt Enable Control
Reg Addr   : 0x00_00C0
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_inten_Eport_Base                                                                      0x0000C0
#define cAf6Reg_inten_Eport                                                                           0x0000C0
#define cAf6Reg_inten_Eport_WidthVal                                                                        32
#define cAf6Reg_inten_Eport_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: XFI_3
BitField Type: RW
BitField Desc: XFI_3    Interrupt Enable  1: Enable  0: Disable
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_inten_Eport_XFI_3_Bit_Start                                                                    19
#define cAf6_inten_Eport_XFI_3_Bit_End                                                                      19
#define cAf6_inten_Eport_XFI_3_Mask                                                                     cBit19
#define cAf6_inten_Eport_XFI_3_Shift                                                                        19
#define cAf6_inten_Eport_XFI_3_MaxVal                                                                      0x1
#define cAf6_inten_Eport_XFI_3_MinVal                                                                      0x0
#define cAf6_inten_Eport_XFI_3_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: XFI_2
BitField Type: RW
BitField Desc: XFI_2    Interrupt Enable  1: Enable  0: Disable
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_inten_Eport_XFI_2_Bit_Start                                                                    18
#define cAf6_inten_Eport_XFI_2_Bit_End                                                                      18
#define cAf6_inten_Eport_XFI_2_Mask                                                                     cBit18
#define cAf6_inten_Eport_XFI_2_Shift                                                                        18
#define cAf6_inten_Eport_XFI_2_MaxVal                                                                      0x1
#define cAf6_inten_Eport_XFI_2_MinVal                                                                      0x0
#define cAf6_inten_Eport_XFI_2_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: XFI_1
BitField Type: RW
BitField Desc: XFI_1    Interrupt Enable  1: Enable  0: Disable
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_inten_Eport_XFI_1_Bit_Start                                                                    17
#define cAf6_inten_Eport_XFI_1_Bit_End                                                                      17
#define cAf6_inten_Eport_XFI_1_Mask                                                                     cBit17
#define cAf6_inten_Eport_XFI_1_Shift                                                                        17
#define cAf6_inten_Eport_XFI_1_MaxVal                                                                      0x1
#define cAf6_inten_Eport_XFI_1_MinVal                                                                      0x0
#define cAf6_inten_Eport_XFI_1_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: XFI_0
BitField Type: RW
BitField Desc: XFI_0    Interrupt Enable  1: Enable  0: Disable
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_inten_Eport_XFI_0_Bit_Start                                                                    16
#define cAf6_inten_Eport_XFI_0_Bit_End                                                                      16
#define cAf6_inten_Eport_XFI_0_Mask                                                                     cBit16
#define cAf6_inten_Eport_XFI_0_Shift                                                                        16
#define cAf6_inten_Eport_XFI_0_MaxVal                                                                      0x1
#define cAf6_inten_Eport_XFI_0_MinVal                                                                      0x0
#define cAf6_inten_Eport_XFI_0_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: QSGMII_3
BitField Type: RW
BitField Desc: QSGMII_3 Interrupt Enable  1: Enable  0: Disable
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_inten_Eport_QSGMII_3_Bit_Start                                                                 12
#define cAf6_inten_Eport_QSGMII_3_Bit_End                                                                   12
#define cAf6_inten_Eport_QSGMII_3_Mask                                                                  cBit12
#define cAf6_inten_Eport_QSGMII_3_Shift                                                                     12
#define cAf6_inten_Eport_QSGMII_3_MaxVal                                                                   0x1
#define cAf6_inten_Eport_QSGMII_3_MinVal                                                                   0x0
#define cAf6_inten_Eport_QSGMII_3_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: QSGMII_2
BitField Type: RW
BitField Desc: QSGMII_2 Interrupt Enable  1: Enable  0: Disable
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_inten_Eport_QSGMII_2_Bit_Start                                                                  8
#define cAf6_inten_Eport_QSGMII_2_Bit_End                                                                    8
#define cAf6_inten_Eport_QSGMII_2_Mask                                                                   cBit8
#define cAf6_inten_Eport_QSGMII_2_Shift                                                                      8
#define cAf6_inten_Eport_QSGMII_2_MaxVal                                                                   0x1
#define cAf6_inten_Eport_QSGMII_2_MinVal                                                                   0x0
#define cAf6_inten_Eport_QSGMII_2_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: QSGMII_1
BitField Type: RW
BitField Desc: QSGMII_1 Interrupt Enable  1: Enable  0: Disable
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_inten_Eport_QSGMII_1_Bit_Start                                                                  4
#define cAf6_inten_Eport_QSGMII_1_Bit_End                                                                    4
#define cAf6_inten_Eport_QSGMII_1_Mask                                                                   cBit4
#define cAf6_inten_Eport_QSGMII_1_Shift                                                                      4
#define cAf6_inten_Eport_QSGMII_1_MaxVal                                                                   0x1
#define cAf6_inten_Eport_QSGMII_1_MinVal                                                                   0x0
#define cAf6_inten_Eport_QSGMII_1_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: QSGMII_0
BitField Type: RW
BitField Desc: QSGMII_0 Interrupt Enable  1: Enable  0: Disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_inten_Eport_QSGMII_0_Bit_Start                                                                  0
#define cAf6_inten_Eport_QSGMII_0_Bit_End                                                                    0
#define cAf6_inten_Eport_QSGMII_0_Mask                                                                   cBit0
#define cAf6_inten_Eport_QSGMII_0_Shift                                                                      0
#define cAf6_inten_Eport_QSGMII_0_MaxVal                                                                   0x1
#define cAf6_inten_Eport_QSGMII_0_MinVal                                                                   0x0
#define cAf6_inten_Eport_QSGMII_0_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : EPort Link Event
Reg Addr   : 0x00_00C4
Reg Formula: 
    Where  : 
Reg Desc   : 
This register report state change of link status.

------------------------------------------------------------------------------*/
#define cAf6Reg_linkalm_Eport_Base                                                                    0x0000C4
#define cAf6Reg_linkalm_Eport                                                                         0x0000C4
#define cAf6Reg_linkalm_Eport_WidthVal                                                                      32
#define cAf6Reg_linkalm_Eport_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: XFI_3
BitField Type: WC
BitField Desc: XFI_3    Link State change Event  1: Changed  0: Normal
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_linkalm_Eport_XFI_3_Bit_Start                                                                  19
#define cAf6_linkalm_Eport_XFI_3_Bit_End                                                                    19
#define cAf6_linkalm_Eport_XFI_3_Mask                                                                   cBit19
#define cAf6_linkalm_Eport_XFI_3_Shift                                                                      19
#define cAf6_linkalm_Eport_XFI_3_MaxVal                                                                    0x1
#define cAf6_linkalm_Eport_XFI_3_MinVal                                                                    0x0
#define cAf6_linkalm_Eport_XFI_3_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_2
BitField Type: WC
BitField Desc: XFI_2    Link State change Event  1: Changed  0: Normal
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_linkalm_Eport_XFI_2_Bit_Start                                                                  18
#define cAf6_linkalm_Eport_XFI_2_Bit_End                                                                    18
#define cAf6_linkalm_Eport_XFI_2_Mask                                                                   cBit18
#define cAf6_linkalm_Eport_XFI_2_Shift                                                                      18
#define cAf6_linkalm_Eport_XFI_2_MaxVal                                                                    0x1
#define cAf6_linkalm_Eport_XFI_2_MinVal                                                                    0x0
#define cAf6_linkalm_Eport_XFI_2_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_1
BitField Type: WC
BitField Desc: XFI_1    Link State change Event  1: Changed  0: Normal
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_linkalm_Eport_XFI_1_Bit_Start                                                                  17
#define cAf6_linkalm_Eport_XFI_1_Bit_End                                                                    17
#define cAf6_linkalm_Eport_XFI_1_Mask                                                                   cBit17
#define cAf6_linkalm_Eport_XFI_1_Shift                                                                      17
#define cAf6_linkalm_Eport_XFI_1_MaxVal                                                                    0x1
#define cAf6_linkalm_Eport_XFI_1_MinVal                                                                    0x0
#define cAf6_linkalm_Eport_XFI_1_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_0
BitField Type: WC
BitField Desc: XFI_0    Link State change Event  1: Changed  0: Normal
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_linkalm_Eport_XFI_0_Bit_Start                                                                  16
#define cAf6_linkalm_Eport_XFI_0_Bit_End                                                                    16
#define cAf6_linkalm_Eport_XFI_0_Mask                                                                   cBit16
#define cAf6_linkalm_Eport_XFI_0_Shift                                                                      16
#define cAf6_linkalm_Eport_XFI_0_MaxVal                                                                    0x1
#define cAf6_linkalm_Eport_XFI_0_MinVal                                                                    0x0
#define cAf6_linkalm_Eport_XFI_0_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: QSGMII_3
BitField Type: WC
BitField Desc: QSGMII_3 Link State change Event  1: Changed  0: Normal
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_linkalm_Eport_QSGMII_3_Bit_Start                                                               12
#define cAf6_linkalm_Eport_QSGMII_3_Bit_End                                                                 12
#define cAf6_linkalm_Eport_QSGMII_3_Mask                                                                cBit12
#define cAf6_linkalm_Eport_QSGMII_3_Shift                                                                   12
#define cAf6_linkalm_Eport_QSGMII_3_MaxVal                                                                 0x1
#define cAf6_linkalm_Eport_QSGMII_3_MinVal                                                                 0x0
#define cAf6_linkalm_Eport_QSGMII_3_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_2
BitField Type: WC
BitField Desc: QSGMII_2 Link State change Event  1: Changed  0: Normal
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_linkalm_Eport_QSGMII_2_Bit_Start                                                                8
#define cAf6_linkalm_Eport_QSGMII_2_Bit_End                                                                  8
#define cAf6_linkalm_Eport_QSGMII_2_Mask                                                                 cBit8
#define cAf6_linkalm_Eport_QSGMII_2_Shift                                                                    8
#define cAf6_linkalm_Eport_QSGMII_2_MaxVal                                                                 0x1
#define cAf6_linkalm_Eport_QSGMII_2_MinVal                                                                 0x0
#define cAf6_linkalm_Eport_QSGMII_2_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_1
BitField Type: WC
BitField Desc: QSGMII_1 Link State change Event  1: Changed  0: Normal
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_linkalm_Eport_QSGMII_1_Bit_Start                                                                4
#define cAf6_linkalm_Eport_QSGMII_1_Bit_End                                                                  4
#define cAf6_linkalm_Eport_QSGMII_1_Mask                                                                 cBit4
#define cAf6_linkalm_Eport_QSGMII_1_Shift                                                                    4
#define cAf6_linkalm_Eport_QSGMII_1_MaxVal                                                                 0x1
#define cAf6_linkalm_Eport_QSGMII_1_MinVal                                                                 0x0
#define cAf6_linkalm_Eport_QSGMII_1_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_0
BitField Type: WC
BitField Desc: QSGMII_0 Link State change Event  1: Changed  0: Normal
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_linkalm_Eport_QSGMII_0_Bit_Start                                                                0
#define cAf6_linkalm_Eport_QSGMII_0_Bit_End                                                                  0
#define cAf6_linkalm_Eport_QSGMII_0_Mask                                                                 cBit0
#define cAf6_linkalm_Eport_QSGMII_0_Shift                                                                    0
#define cAf6_linkalm_Eport_QSGMII_0_MaxVal                                                                 0x1
#define cAf6_linkalm_Eport_QSGMII_0_MinVal                                                                 0x0
#define cAf6_linkalm_Eport_QSGMII_0_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : EPort Link Status
Reg Addr   : 0x00_00C8
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_linksta_Eport_Base                                                                    0x0000C8
#define cAf6Reg_linksta_Eport                                                                         0x0000C8
#define cAf6Reg_linksta_Eport_WidthVal                                                                      32
#define cAf6Reg_linksta_Eport_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: XFI_3
BitField Type: RO
BitField Desc: XFI_3 Link Status  1: Up  0: Down
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_linksta_Eport_XFI_3_Bit_Start                                                                  19
#define cAf6_linksta_Eport_XFI_3_Bit_End                                                                    19
#define cAf6_linksta_Eport_XFI_3_Mask                                                                   cBit19
#define cAf6_linksta_Eport_XFI_3_Shift                                                                      19
#define cAf6_linksta_Eport_XFI_3_MaxVal                                                                    0x1
#define cAf6_linksta_Eport_XFI_3_MinVal                                                                    0x0
#define cAf6_linksta_Eport_XFI_3_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_2
BitField Type: RO
BitField Desc: XFI_2 Link Status  1: Up  0: Down
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_linksta_Eport_XFI_2_Bit_Start                                                                  18
#define cAf6_linksta_Eport_XFI_2_Bit_End                                                                    18
#define cAf6_linksta_Eport_XFI_2_Mask                                                                   cBit18
#define cAf6_linksta_Eport_XFI_2_Shift                                                                      18
#define cAf6_linksta_Eport_XFI_2_MaxVal                                                                    0x1
#define cAf6_linksta_Eport_XFI_2_MinVal                                                                    0x0
#define cAf6_linksta_Eport_XFI_2_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_1
BitField Type: RO
BitField Desc: XFI_1 Link Status  1: Up  0: Down
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_linksta_Eport_XFI_1_Bit_Start                                                                  17
#define cAf6_linksta_Eport_XFI_1_Bit_End                                                                    17
#define cAf6_linksta_Eport_XFI_1_Mask                                                                   cBit17
#define cAf6_linksta_Eport_XFI_1_Shift                                                                      17
#define cAf6_linksta_Eport_XFI_1_MaxVal                                                                    0x1
#define cAf6_linksta_Eport_XFI_1_MinVal                                                                    0x0
#define cAf6_linksta_Eport_XFI_1_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_0
BitField Type: RO
BitField Desc: XFI_0 Link Status  1: Up  0: Down
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_linksta_Eport_XFI_0_Bit_Start                                                                  16
#define cAf6_linksta_Eport_XFI_0_Bit_End                                                                    16
#define cAf6_linksta_Eport_XFI_0_Mask                                                                   cBit16
#define cAf6_linksta_Eport_XFI_0_Shift                                                                      16
#define cAf6_linksta_Eport_XFI_0_MaxVal                                                                    0x1
#define cAf6_linksta_Eport_XFI_0_MinVal                                                                    0x0
#define cAf6_linksta_Eport_XFI_0_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: QSGMII_3
BitField Type: RO
BitField Desc: QSGMII_3 Link Status 1: Up  0: Down
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_linksta_Eport_QSGMII_3_Bit_Start                                                               12
#define cAf6_linksta_Eport_QSGMII_3_Bit_End                                                                 12
#define cAf6_linksta_Eport_QSGMII_3_Mask                                                                cBit12
#define cAf6_linksta_Eport_QSGMII_3_Shift                                                                   12
#define cAf6_linksta_Eport_QSGMII_3_MaxVal                                                                 0x1
#define cAf6_linksta_Eport_QSGMII_3_MinVal                                                                 0x0
#define cAf6_linksta_Eport_QSGMII_3_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_2
BitField Type: RO
BitField Desc: QSGMII_2 Link Status 1: Up  0: Down
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_linksta_Eport_QSGMII_2_Bit_Start                                                                8
#define cAf6_linksta_Eport_QSGMII_2_Bit_End                                                                  8
#define cAf6_linksta_Eport_QSGMII_2_Mask                                                                 cBit8
#define cAf6_linksta_Eport_QSGMII_2_Shift                                                                    8
#define cAf6_linksta_Eport_QSGMII_2_MaxVal                                                                 0x1
#define cAf6_linksta_Eport_QSGMII_2_MinVal                                                                 0x0
#define cAf6_linksta_Eport_QSGMII_2_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_1
BitField Type: RO
BitField Desc: QSGMII_1 Link Status 1: Up  0: Down
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_linksta_Eport_QSGMII_1_Bit_Start                                                                4
#define cAf6_linksta_Eport_QSGMII_1_Bit_End                                                                  4
#define cAf6_linksta_Eport_QSGMII_1_Mask                                                                 cBit4
#define cAf6_linksta_Eport_QSGMII_1_Shift                                                                    4
#define cAf6_linksta_Eport_QSGMII_1_MaxVal                                                                 0x1
#define cAf6_linksta_Eport_QSGMII_1_MinVal                                                                 0x0
#define cAf6_linksta_Eport_QSGMII_1_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_0
BitField Type: RO
BitField Desc: QSGMII_0 Link Status 1: Up  0: Down
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_linksta_Eport_QSGMII_0_Bit_Start                                                                0
#define cAf6_linksta_Eport_QSGMII_0_Bit_End                                                                  0
#define cAf6_linksta_Eport_QSGMII_0_Mask                                                                 cBit0
#define cAf6_linksta_Eport_QSGMII_0_Shift                                                                    0
#define cAf6_linksta_Eport_QSGMII_0_MaxVal                                                                 0x1
#define cAf6_linksta_Eport_QSGMII_0_MinVal                                                                 0x0
#define cAf6_linksta_Eport_QSGMII_0_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : EPort Link Force
Reg Addr   : 0x00_00CC
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configure interrupt enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_linkfrc_Eport_Base                                                                    0x0000CC
#define cAf6Reg_linkfrc_Eport                                                                         0x0000CC
#define cAf6Reg_linkfrc_Eport_WidthVal                                                                      32
#define cAf6Reg_linkfrc_Eport_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: XFI_3
BitField Type: RO
BitField Desc: XFI_3 Link Force  1: Up  0: Down
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_linkfrc_Eport_XFI_3_Bit_Start                                                                  19
#define cAf6_linkfrc_Eport_XFI_3_Bit_End                                                                    19
#define cAf6_linkfrc_Eport_XFI_3_Mask                                                                   cBit19
#define cAf6_linkfrc_Eport_XFI_3_Shift                                                                      19
#define cAf6_linkfrc_Eport_XFI_3_MaxVal                                                                    0x1
#define cAf6_linkfrc_Eport_XFI_3_MinVal                                                                    0x0
#define cAf6_linkfrc_Eport_XFI_3_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_2
BitField Type: RO
BitField Desc: XFI_2 Link Force  1: Up  0: Down
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_linkfrc_Eport_XFI_2_Bit_Start                                                                  18
#define cAf6_linkfrc_Eport_XFI_2_Bit_End                                                                    18
#define cAf6_linkfrc_Eport_XFI_2_Mask                                                                   cBit18
#define cAf6_linkfrc_Eport_XFI_2_Shift                                                                      18
#define cAf6_linkfrc_Eport_XFI_2_MaxVal                                                                    0x1
#define cAf6_linkfrc_Eport_XFI_2_MinVal                                                                    0x0
#define cAf6_linkfrc_Eport_XFI_2_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_1
BitField Type: RO
BitField Desc: XFI_1 Link Force  1: Up  0: Down
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_linkfrc_Eport_XFI_1_Bit_Start                                                                  17
#define cAf6_linkfrc_Eport_XFI_1_Bit_End                                                                    17
#define cAf6_linkfrc_Eport_XFI_1_Mask                                                                   cBit17
#define cAf6_linkfrc_Eport_XFI_1_Shift                                                                      17
#define cAf6_linkfrc_Eport_XFI_1_MaxVal                                                                    0x1
#define cAf6_linkfrc_Eport_XFI_1_MinVal                                                                    0x0
#define cAf6_linkfrc_Eport_XFI_1_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: XFI_0
BitField Type: RO
BitField Desc: XFI_0 Link Force  1: Up  0: Down
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_linkfrc_Eport_XFI_0_Bit_Start                                                                  16
#define cAf6_linkfrc_Eport_XFI_0_Bit_End                                                                    16
#define cAf6_linkfrc_Eport_XFI_0_Mask                                                                   cBit16
#define cAf6_linkfrc_Eport_XFI_0_Shift                                                                      16
#define cAf6_linkfrc_Eport_XFI_0_MaxVal                                                                    0x1
#define cAf6_linkfrc_Eport_XFI_0_MinVal                                                                    0x0
#define cAf6_linkfrc_Eport_XFI_0_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: QSGMII_3
BitField Type: RO
BitField Desc: QSGMII_3 Link Force 1: Up  0: Down
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_linkfrc_Eport_QSGMII_3_Bit_Start                                                               12
#define cAf6_linkfrc_Eport_QSGMII_3_Bit_End                                                                 12
#define cAf6_linkfrc_Eport_QSGMII_3_Mask                                                                cBit12
#define cAf6_linkfrc_Eport_QSGMII_3_Shift                                                                   12
#define cAf6_linkfrc_Eport_QSGMII_3_MaxVal                                                                 0x1
#define cAf6_linkfrc_Eport_QSGMII_3_MinVal                                                                 0x0
#define cAf6_linkfrc_Eport_QSGMII_3_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_2
BitField Type: RO
BitField Desc: QSGMII_2 Link Force 1: Up  0: Down
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_linkfrc_Eport_QSGMII_2_Bit_Start                                                                8
#define cAf6_linkfrc_Eport_QSGMII_2_Bit_End                                                                  8
#define cAf6_linkfrc_Eport_QSGMII_2_Mask                                                                 cBit8
#define cAf6_linkfrc_Eport_QSGMII_2_Shift                                                                    8
#define cAf6_linkfrc_Eport_QSGMII_2_MaxVal                                                                 0x1
#define cAf6_linkfrc_Eport_QSGMII_2_MinVal                                                                 0x0
#define cAf6_linkfrc_Eport_QSGMII_2_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_1
BitField Type: RO
BitField Desc: QSGMII_1 Link Force 1: Up  0: Down
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_linkfrc_Eport_QSGMII_1_Bit_Start                                                                4
#define cAf6_linkfrc_Eport_QSGMII_1_Bit_End                                                                  4
#define cAf6_linkfrc_Eport_QSGMII_1_Mask                                                                 cBit4
#define cAf6_linkfrc_Eport_QSGMII_1_Shift                                                                    4
#define cAf6_linkfrc_Eport_QSGMII_1_MaxVal                                                                 0x1
#define cAf6_linkfrc_Eport_QSGMII_1_MinVal                                                                 0x0
#define cAf6_linkfrc_Eport_QSGMII_1_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: QSGMII_0
BitField Type: RO
BitField Desc: QSGMII_0 Link Force 1: Up  0: Down
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_linkfrc_Eport_QSGMII_0_Bit_Start                                                                0
#define cAf6_linkfrc_Eport_QSGMII_0_Bit_End                                                                  0
#define cAf6_linkfrc_Eport_QSGMII_0_Mask                                                                 cBit0
#define cAf6_linkfrc_Eport_QSGMII_0_Shift                                                                    0
#define cAf6_linkfrc_Eport_QSGMII_0_MaxVal                                                                 0x1
#define cAf6_linkfrc_Eport_QSGMII_0_MinVal                                                                 0x0
#define cAf6_linkfrc_Eport_QSGMII_0_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : Write Hold Data63_32
Reg Addr   : 0x000011
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to write dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_wr_hold63_32_Base                                                                     0x000011
#define cAf6Reg_wr_hold63_32                                                                          0x000011
#define cAf6Reg_wr_hold63_32_WidthVal                                                                       32
#define cAf6Reg_wr_hold63_32_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: rHold63_32
BitField Type: RW
BitField Desc: The write dword 2
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_wr_hold63_32_rHold63_32_Bit_Start                                                               0
#define cAf6_wr_hold63_32_rHold63_32_Bit_End                                                                31
#define cAf6_wr_hold63_32_rHold63_32_Mask                                                             cBit31_0
#define cAf6_wr_hold63_32_rHold63_32_Shift                                                                   0
#define cAf6_wr_hold63_32_rHold63_32_MaxVal                                                         0xffffffff
#define cAf6_wr_hold63_32_rHold63_32_MinVal                                                                0x0
#define cAf6_wr_hold63_32_rHold63_32_RstVal                                                                0xX


/*------------------------------------------------------------------------------
Reg Name   : Write Hold Data95_64
Reg Addr   : 0x000012
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to write dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_wr_hold95_64_Base                                                                     0x000012
#define cAf6Reg_wr_hold95_64                                                                          0x000012
#define cAf6Reg_wr_hold95_64_WidthVal                                                                       32
#define cAf6Reg_wr_hold95_64_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: rHold95_64
BitField Type: RW
BitField Desc: The write dword 3
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_wr_hold95_64_rHold95_64_Bit_Start                                                               0
#define cAf6_wr_hold95_64_rHold95_64_Bit_End                                                                31
#define cAf6_wr_hold95_64_rHold95_64_Mask                                                             cBit31_0
#define cAf6_wr_hold95_64_rHold95_64_Shift                                                                   0
#define cAf6_wr_hold95_64_rHold95_64_MaxVal                                                         0xffffffff
#define cAf6_wr_hold95_64_rHold95_64_MinVal                                                                0x0
#define cAf6_wr_hold95_64_rHold95_64_RstVal                                                                0xx


/*------------------------------------------------------------------------------
Reg Name   : Write Hold Data127_96
Reg Addr   : 0x000013
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to write dword4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_wr_hold_127_96_Base                                                                   0x000013
#define cAf6Reg_wr_hold_127_96                                                                        0x000013
#define cAf6Reg_wr_hold_127_96_WidthVal                                                                     32
#define cAf6Reg_wr_hold_127_96_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: rHold127_96
BitField Type: RW
BitField Desc: The write dword 4
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_wr_hold_127_96_rHold127_96_Bit_Start                                                            0
#define cAf6_wr_hold_127_96_rHold127_96_Bit_End                                                             31
#define cAf6_wr_hold_127_96_rHold127_96_Mask                                                          cBit31_0
#define cAf6_wr_hold_127_96_rHold127_96_Shift                                                                0
#define cAf6_wr_hold_127_96_rHold127_96_MaxVal                                                      0xffffffff
#define cAf6_wr_hold_127_96_rHold127_96_MinVal                                                             0x0
#define cAf6_wr_hold_127_96_rHold127_96_RstVal                                                             0xx


/*------------------------------------------------------------------------------
Reg Name   : Read Hold Data63_32
Reg Addr   : 0x000021
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read dword2 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rd_hold63_32_Base                                                                     0x000021
#define cAf6Reg_rd_hold63_32                                                                          0x000021
#define cAf6Reg_rd_hold63_32_WidthVal                                                                       32
#define cAf6Reg_rd_hold63_32_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: dHold63_32
BitField Type: RW
BitField Desc: The read dword2
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rd_hold63_32_dHold63_32_Bit_Start                                                               0
#define cAf6_rd_hold63_32_dHold63_32_Bit_End                                                                31
#define cAf6_rd_hold63_32_dHold63_32_Mask                                                             cBit31_0
#define cAf6_rd_hold63_32_dHold63_32_Shift                                                                   0
#define cAf6_rd_hold63_32_dHold63_32_MaxVal                                                         0xffffffff
#define cAf6_rd_hold63_32_dHold63_32_MinVal                                                                0x0
#define cAf6_rd_hold63_32_dHold63_32_RstVal                                                                0xx


/*------------------------------------------------------------------------------
Reg Name   : Read Hold Data95_64
Reg Addr   : 0x000022
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read dword3 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rd_hold95_64_Base                                                                     0x000022
#define cAf6Reg_rd_hold95_64                                                                          0x000022
#define cAf6Reg_rd_hold95_64_WidthVal                                                                       32
#define cAf6Reg_rd_hold95_64_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: dHold95_64
BitField Type: RW
BitField Desc: The read dword3
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rd_hold95_64_dHold95_64_Bit_Start                                                               0
#define cAf6_rd_hold95_64_dHold95_64_Bit_End                                                                31
#define cAf6_rd_hold95_64_dHold95_64_Mask                                                             cBit31_0
#define cAf6_rd_hold95_64_dHold95_64_Shift                                                                   0
#define cAf6_rd_hold95_64_dHold95_64_MaxVal                                                         0xffffffff
#define cAf6_rd_hold95_64_dHold95_64_MinVal                                                                0x0
#define cAf6_rd_hold95_64_dHold95_64_RstVal                                                                0xx


/*------------------------------------------------------------------------------
Reg Name   : Read Hold Data127_96
Reg Addr   : 0x000023
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to read dword4 of data.

------------------------------------------------------------------------------*/
#define cAf6Reg_rd_hold_127_96_Base                                                                   0x000023
#define cAf6Reg_rd_hold_127_96                                                                        0x000023
#define cAf6Reg_rd_hold_127_96_WidthVal                                                                     32
#define cAf6Reg_rd_hold_127_96_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: dHold127_96
BitField Type: RW
BitField Desc: The read dword4
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_rd_hold_127_96_dHold127_96_Bit_Start                                                            0
#define cAf6_rd_hold_127_96_dHold127_96_Bit_End                                                             31
#define cAf6_rd_hold_127_96_dHold127_96_Mask                                                          cBit31_0
#define cAf6_rd_hold_127_96_dHold127_96_Shift                                                                0
#define cAf6_rd_hold_127_96_dHold127_96_MaxVal                                                      0xffffffff
#define cAf6_rd_hold_127_96_dHold127_96_MinVal                                                             0x0
#define cAf6_rd_hold_127_96_dHold127_96_RstVal                                                             0xx


/*------------------------------------------------------------------------------
Reg Name   : Mac Address Bit31_0 Control
Reg Addr   : 0x000100
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 31 to 0 of MAC address.

------------------------------------------------------------------------------*/
#define cAf6Reg_mac_addr_b31_0_Base                                                                   0x000100
#define cAf6Reg_mac_addr_b31_0                                                                        0x000100
#define cAf6Reg_mac_addr_b31_0_WidthVal                                                                     32
#define cAf6Reg_mac_addr_b31_0_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: acAddress31_0
BitField Type: RW
BitField Desc: Bit 31 to 0 of MAC Address. Bit 31 is MSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mac_addr_b31_0_acAddress31_0_Bit_Start                                                          0
#define cAf6_mac_addr_b31_0_acAddress31_0_Bit_End                                                           31
#define cAf6_mac_addr_b31_0_acAddress31_0_Mask                                                        cBit31_0
#define cAf6_mac_addr_b31_0_acAddress31_0_Shift                                                              0
#define cAf6_mac_addr_b31_0_acAddress31_0_MaxVal                                                    0xffffffff
#define cAf6_mac_addr_b31_0_acAddress31_0_MinVal                                                           0x0
#define cAf6_mac_addr_b31_0_acAddress31_0_RstVal                                                           0x0


/*------------------------------------------------------------------------------
Reg Name   : Mac Address Bit47_32 Control
Reg Addr   : 0x000101
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 47 to 32 of MAC address.

------------------------------------------------------------------------------*/
#define cAf6Reg_mac_addr_b47_32_Base                                                                  0x000101
#define cAf6Reg_mac_addr_b47_32                                                                       0x000101
#define cAf6Reg_mac_addr_b47_32_WidthVal                                                                    32
#define cAf6Reg_mac_addr_b47_32_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: GeMode
BitField Type: RW
BitField Desc: GE mode 0:XGMII; 1:GMII
BitField Bits: [26]
--------------------------------------*/
#define cAf6_mac_addr_b47_32_GeMode_Bit_Start                                                               26
#define cAf6_mac_addr_b47_32_GeMode_Bit_End                                                                 26
#define cAf6_mac_addr_b47_32_GeMode_Mask                                                                cBit26
#define cAf6_mac_addr_b47_32_GeMode_Shift                                                                   26
#define cAf6_mac_addr_b47_32_GeMode_MaxVal                                                                 0x1
#define cAf6_mac_addr_b47_32_GeMode_MinVal                                                                 0x0
#define cAf6_mac_addr_b47_32_GeMode_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: acAddress47_32
BitField Type: RW
BitField Desc: Bit 47 to 32 of MAC Address. Bit 47 is MSB bit
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_mac_addr_b47_32_acAddress47_32_Bit_Start                                                        0
#define cAf6_mac_addr_b47_32_acAddress47_32_Bit_End                                                         15
#define cAf6_mac_addr_b47_32_acAddress47_32_Mask                                                      cBit15_0
#define cAf6_mac_addr_b47_32_acAddress47_32_Shift                                                            0
#define cAf6_mac_addr_b47_32_acAddress47_32_MaxVal                                                      0xffff
#define cAf6_mac_addr_b47_32_acAddress47_32_MinVal                                                         0x0
#define cAf6_mac_addr_b47_32_acAddress47_32_RstVal                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : IP Address Bit31_0 Control
Reg Addr   : 0x000102
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures IPv4 address or bit 31 to 0 Ipv6 address.

------------------------------------------------------------------------------*/
#define cAf6Reg_ip_addr_b31_0_Base                                                                    0x000102
#define cAf6Reg_ip_addr_b31_0                                                                         0x000102
#define cAf6Reg_ip_addr_b31_0_WidthVal                                                                      32
#define cAf6Reg_ip_addr_b31_0_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: pAddress
BitField Type: RW
BitField Desc: Ipv4/IPv6 Address . Bit 0 is LSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ip_addr_b31_0_pAddress_Bit_Start                                                                0
#define cAf6_ip_addr_b31_0_pAddress_Bit_End                                                                 31
#define cAf6_ip_addr_b31_0_pAddress_Mask                                                              cBit31_0
#define cAf6_ip_addr_b31_0_pAddress_Shift                                                                    0
#define cAf6_ip_addr_b31_0_pAddress_MaxVal                                                          0xffffffff
#define cAf6_ip_addr_b31_0_pAddress_MinVal                                                                 0x0
#define cAf6_ip_addr_b31_0_pAddress_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : IPv6 Address Bit63_32 Control
Reg Addr   : 0x000103
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 63 to 32 of Ipv6 address.

------------------------------------------------------------------------------*/
#define cAf6Reg_ipv6_addr_b63_32_Base                                                                 0x000103
#define cAf6Reg_ipv6_addr_b63_32                                                                      0x000103
#define cAf6Reg_ipv6_addr_b63_32_WidthVal                                                                   32
#define cAf6Reg_ipv6_addr_b63_32_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: pv6Address63_32
BitField Type: RW
BitField Desc: Ipv6 Address bit 63 to 32. Bit 32 is LSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ipv6_addr_b63_32_pv6Address63_32_Bit_Start                                                      0
#define cAf6_ipv6_addr_b63_32_pv6Address63_32_Bit_End                                                       31
#define cAf6_ipv6_addr_b63_32_pv6Address63_32_Mask                                                    cBit31_0
#define cAf6_ipv6_addr_b63_32_pv6Address63_32_Shift                                                          0
#define cAf6_ipv6_addr_b63_32_pv6Address63_32_MaxVal                                                0xffffffff
#define cAf6_ipv6_addr_b63_32_pv6Address63_32_MinVal                                                       0x0
#define cAf6_ipv6_addr_b63_32_pv6Address63_32_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : IPv6 Address Bit95_64 Control
Reg Addr   : 0x104
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 95 to 64 of Ipv6 address.

------------------------------------------------------------------------------*/
#define cAf6Reg_ipv6_addr_b95_64_Base                                                                    0x104
#define cAf6Reg_ipv6_addr_b95_64                                                                         0x104
#define cAf6Reg_ipv6_addr_b95_64_WidthVal                                                                   32
#define cAf6Reg_ipv6_addr_b95_64_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: pv6Address95_64
BitField Type: RW
BitField Desc: Ipv6 Address bit 95 to 64. Bit 64 is LSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ipv6_addr_b95_64_pv6Address95_64_Bit_Start                                                      0
#define cAf6_ipv6_addr_b95_64_pv6Address95_64_Bit_End                                                       31
#define cAf6_ipv6_addr_b95_64_pv6Address95_64_Mask                                                    cBit31_0
#define cAf6_ipv6_addr_b95_64_pv6Address95_64_Shift                                                          0
#define cAf6_ipv6_addr_b95_64_pv6Address95_64_MaxVal                                                0xffffffff
#define cAf6_ipv6_addr_b95_64_pv6Address95_64_MinVal                                                       0x0
#define cAf6_ipv6_addr_b95_64_pv6Address95_64_RstVal                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : IPv6 Address Bit127_96 Control
Reg Addr   : 0x105
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures bit 127 to 96 of Ipv6 address.

------------------------------------------------------------------------------*/
#define cAf6Reg_ipv6_addr_b127_96_Base                                                                   0x105
#define cAf6Reg_ipv6_addr_b127_96                                                                        0x105
#define cAf6Reg_ipv6_addr_b127_96_WidthVal                                                                  32
#define cAf6Reg_ipv6_addr_b127_96_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: pv6Address127_96
BitField Type: RW
BitField Desc: Ipv6 Address bit 95 to 64. Bit 64 is LSB bit
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ipv6_addr_b127_96_pv6Address127_96_Bit_Start                                                    0
#define cAf6_ipv6_addr_b127_96_pv6Address127_96_Bit_End                                                     31
#define cAf6_ipv6_addr_b127_96_pv6Address127_96_Mask                                                  cBit31_0
#define cAf6_ipv6_addr_b127_96_pv6Address127_96_Shift                                                        0
#define cAf6_ipv6_addr_b127_96_pv6Address127_96_MaxVal                                              0xffffffff
#define cAf6_ipv6_addr_b127_96_pv6Address127_96_MinVal                                                     0x0
#define cAf6_ipv6_addr_b127_96_pv6Address127_96_RstVal                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM Parity Force Control
Reg Addr   : 0x107
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Parity_Force_Control_Base                                                            0x107
#define cAf6Reg_RAM_Parity_Force_Control                                                                 0x107
#define cAf6Reg_RAM_Parity_Force_Control_WidthVal                                                           32
#define cAf6Reg_RAM_Parity_Force_Control_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: DDRCRCErrFrc
BitField Type: RW
BitField Desc: Force parity For DDR
BitField Bits: [24]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DDRCRCErrFrc_Bit_Start                                                24
#define cAf6_RAM_Parity_Force_Control_DDRCRCErrFrc_Bit_End                                                  24
#define cAf6_RAM_Parity_Force_Control_DDRCRCErrFrc_Mask                                                 cBit24
#define cAf6_RAM_Parity_Force_Control_DDRCRCErrFrc_Shift                                                    24
#define cAf6_RAM_Parity_Force_Control_DDRCRCErrFrc_MaxVal                                                  0x1
#define cAf6_RAM_Parity_Force_Control_DDRCRCErrFrc_MinVal                                                  0x0
#define cAf6_RAM_Parity_Force_Control_DDRCRCErrFrc_RstVal                                                  0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl5ParErrFrc
BitField Type: RW
BitField Desc: Force parity For CLA Classify Classify Per Group Enable Control
RAM
BitField Bits: [23]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl5ParErrFrc_Bit_Start                                       23
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl5ParErrFrc_Bit_End                                         23
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl5ParErrFrc_Mask                                        cBit23
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl5ParErrFrc_Shift                                           23
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl5ParErrFrc_MaxVal                                         0x1
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl5ParErrFrc_MinVal                                         0x0
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl5ParErrFrc_RstVal                                         0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl4ParErrFrc
BitField Type: RW
BitField Desc: Force parity For CLA Classify Pseudowire Type Control RAM
BitField Bits: [22]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl4ParErrFrc_Bit_Start                                       22
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl4ParErrFrc_Bit_End                                         22
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl4ParErrFrc_Mask                                        cBit22
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl4ParErrFrc_Shift                                           22
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl4ParErrFrc_MaxVal                                         0x1
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl4ParErrFrc_MinVal                                         0x0
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl4ParErrFrc_RstVal                                         0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl3ParErrFrc
BitField Type: RW
BitField Desc: Force parity For CLA Classify HBCE Looking Up Information Control
RAM
BitField Bits: [21]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl3ParErrFrc_Bit_Start                                       21
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl3ParErrFrc_Bit_End                                         21
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl3ParErrFrc_Mask                                        cBit21
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl3ParErrFrc_Shift                                           21
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl3ParErrFrc_MaxVal                                         0x1
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl3ParErrFrc_MinVal                                         0x0
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl3ParErrFrc_RstVal                                         0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl2ParErrFrc
BitField Type: RW
BitField Desc: Force parity For CLA Classify HBCE Hashing Table page1 Control
RAM
BitField Bits: [20]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl2ParErrFrc_Bit_Start                                       20
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl2ParErrFrc_Bit_End                                         20
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl2ParErrFrc_Mask                                        cBit20
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl2ParErrFrc_Shift                                           20
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl2ParErrFrc_MaxVal                                         0x1
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl2ParErrFrc_MinVal                                         0x0
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl2ParErrFrc_RstVal                                         0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl1ParErrFrc
BitField Type: RW
BitField Desc: Force parity For CLA Classify HBCE Hashing Table page0 Control
RAM
BitField Bits: [19]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl1ParErrFrc_Bit_Start                                       19
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl1ParErrFrc_Bit_End                                         19
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl1ParErrFrc_Mask                                        cBit19
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl1ParErrFrc_Shift                                           19
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl1ParErrFrc_MaxVal                                         0x1
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl1ParErrFrc_MinVal                                         0x0
#define cAf6_RAM_Parity_Force_Control_CLAHBCECtrl1ParErrFrc_RstVal                                         0x0

/*--------------------------------------
BitField Name: CLAVLANCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For CLA Classify VLAN TAG Lookup Control  RAM
BitField Bits: [18]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CLAVLANCtrlParErrFrc_Bit_Start                                        18
#define cAf6_RAM_Parity_Force_Control_CLAVLANCtrlParErrFrc_Bit_End                                          18
#define cAf6_RAM_Parity_Force_Control_CLAVLANCtrlParErrFrc_Mask                                         cBit18
#define cAf6_RAM_Parity_Force_Control_CLAVLANCtrlParErrFrc_Shift                                            18
#define cAf6_RAM_Parity_Force_Control_CLAVLANCtrlParErrFrc_MaxVal                                          0x1
#define cAf6_RAM_Parity_Force_Control_CLAVLANCtrlParErrFrc_MinVal                                          0x0
#define cAf6_RAM_Parity_Force_Control_CLAVLANCtrlParErrFrc_RstVal                                          0x0

/*--------------------------------------
BitField Name: PWEHSGrpExtParErrFrc
BitField Type: RW
BitField Desc: Force parity For PWE Pseudowire Transmit HSPW Group Protection
Control  RAM
BitField Bits: [17]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpExtParErrFrc_Bit_Start                                        17
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpExtParErrFrc_Bit_End                                          17
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpExtParErrFrc_Mask                                         cBit17
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpExtParErrFrc_Shift                                            17
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpExtParErrFrc_MaxVal                                          0x1
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpExtParErrFrc_MinVal                                          0x0
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpExtParErrFrc_RstVal                                          0x0

/*--------------------------------------
BitField Name: PWEUPSRCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For PWE Pseudowire Transmit UPSR Group Enable
Control  RAM
BitField Bits: [16]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWEUPSRCtrlParErrFrc_Bit_Start                                        16
#define cAf6_RAM_Parity_Force_Control_PWEUPSRCtrlParErrFrc_Bit_End                                          16
#define cAf6_RAM_Parity_Force_Control_PWEUPSRCtrlParErrFrc_Mask                                         cBit16
#define cAf6_RAM_Parity_Force_Control_PWEUPSRCtrlParErrFrc_Shift                                            16
#define cAf6_RAM_Parity_Force_Control_PWEUPSRCtrlParErrFrc_MaxVal                                          0x1
#define cAf6_RAM_Parity_Force_Control_PWEUPSRCtrlParErrFrc_MinVal                                          0x0
#define cAf6_RAM_Parity_Force_Control_PWEUPSRCtrlParErrFrc_RstVal                                          0x0

/*--------------------------------------
BitField Name: PWEHSGrpCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For PWE Pseudowire Transmit UPSR and HSPW mode
Control   RAM
BitField Bits: [15]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpCtrlParErrFrc_Bit_Start                                       15
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpCtrlParErrFrc_Bit_End                                         15
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpCtrlParErrFrc_Mask                                        cBit15
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpCtrlParErrFrc_Shift                                           15
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpCtrlParErrFrc_MaxVal                                         0x1
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpCtrlParErrFrc_MinVal                                         0x0
#define cAf6_RAM_Parity_Force_Control_PWEHSGrpCtrlParErrFrc_RstVal                                         0x0

/*--------------------------------------
BitField Name: PWEHSPWCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For PWE Pseudowire Transmit HSPW Label Control  RAM
BitField Bits: [14]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWEHSPWCtrlParErrFrc_Bit_Start                                        14
#define cAf6_RAM_Parity_Force_Control_PWEHSPWCtrlParErrFrc_Bit_End                                          14
#define cAf6_RAM_Parity_Force_Control_PWEHSPWCtrlParErrFrc_Mask                                         cBit14
#define cAf6_RAM_Parity_Force_Control_PWEHSPWCtrlParErrFrc_Shift                                            14
#define cAf6_RAM_Parity_Force_Control_PWEHSPWCtrlParErrFrc_MaxVal                                          0x1
#define cAf6_RAM_Parity_Force_Control_PWEHSPWCtrlParErrFrc_MinVal                                          0x0
#define cAf6_RAM_Parity_Force_Control_PWEHSPWCtrlParErrFrc_RstVal                                          0x0

/*--------------------------------------
BitField Name: PWESRCCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For PWE Pseudowire Transmit Header RTP SSRC Value
Control  RAM
BitField Bits: [13]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWESRCCtrlParErrFrc_Bit_Start                                         13
#define cAf6_RAM_Parity_Force_Control_PWESRCCtrlParErrFrc_Bit_End                                           13
#define cAf6_RAM_Parity_Force_Control_PWESRCCtrlParErrFrc_Mask                                          cBit13
#define cAf6_RAM_Parity_Force_Control_PWESRCCtrlParErrFrc_Shift                                             13
#define cAf6_RAM_Parity_Force_Control_PWESRCCtrlParErrFrc_MaxVal                                           0x1
#define cAf6_RAM_Parity_Force_Control_PWESRCCtrlParErrFrc_MinVal                                           0x0
#define cAf6_RAM_Parity_Force_Control_PWESRCCtrlParErrFrc_RstVal                                           0x0

/*--------------------------------------
BitField Name: PWEPSNModeCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For PWE Pseudowire Transmit Ethernet Header Length
Control  RAM
BitField Bits: [12]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWEPSNModeCtrlParErrFrc_Bit_Start                                      12
#define cAf6_RAM_Parity_Force_Control_PWEPSNModeCtrlParErrFrc_Bit_End                                       12
#define cAf6_RAM_Parity_Force_Control_PWEPSNModeCtrlParErrFrc_Mask                                      cBit12
#define cAf6_RAM_Parity_Force_Control_PWEPSNModeCtrlParErrFrc_Shift                                         12
#define cAf6_RAM_Parity_Force_Control_PWEPSNModeCtrlParErrFrc_MaxVal                                       0x1
#define cAf6_RAM_Parity_Force_Control_PWEPSNModeCtrlParErrFrc_MinVal                                       0x0
#define cAf6_RAM_Parity_Force_Control_PWEPSNModeCtrlParErrFrc_RstVal                                       0x0

/*--------------------------------------
BitField Name: PWEPSNHeaderCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For PWE Pseudowire Transmit Ethernet Header Value
Control  RAM
BitField Bits: [11]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PWEPSNHeaderCtrlParErrFrc_Bit_Start                                      11
#define cAf6_RAM_Parity_Force_Control_PWEPSNHeaderCtrlParErrFrc_Bit_End                                      11
#define cAf6_RAM_Parity_Force_Control_PWEPSNHeaderCtrlParErrFrc_Mask                                    cBit11
#define cAf6_RAM_Parity_Force_Control_PWEPSNHeaderCtrlParErrFrc_Shift                                       11
#define cAf6_RAM_Parity_Force_Control_PWEPSNHeaderCtrlParErrFrc_MaxVal                                     0x1
#define cAf6_RAM_Parity_Force_Control_PWEPSNHeaderCtrlParErrFrc_MinVal                                     0x0
#define cAf6_RAM_Parity_Force_Control_PWEPSNHeaderCtrlParErrFrc_RstVal                                     0x0

/*--------------------------------------
BitField Name: PDADeasCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For Pseudowire PDA TDM Mode Control RAM
BitField Bits: [10]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDADeasCtrlParErrFrc_Bit_Start                                        10
#define cAf6_RAM_Parity_Force_Control_PDADeasCtrlParErrFrc_Bit_End                                          10
#define cAf6_RAM_Parity_Force_Control_PDADeasCtrlParErrFrc_Mask                                         cBit10
#define cAf6_RAM_Parity_Force_Control_PDADeasCtrlParErrFrc_Shift                                            10
#define cAf6_RAM_Parity_Force_Control_PDADeasCtrlParErrFrc_MaxVal                                          0x1
#define cAf6_RAM_Parity_Force_Control_PDADeasCtrlParErrFrc_MinVal                                          0x0
#define cAf6_RAM_Parity_Force_Control_PDADeasCtrlParErrFrc_RstVal                                          0x0

/*--------------------------------------
BitField Name: PDAJitBufCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For Pseudowire PDA Jitter Buffer Control RAM
BitField Bits: [9]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDAJitBufCtrlParErrFrc_Bit_Start                                       9
#define cAf6_RAM_Parity_Force_Control_PDAJitBufCtrlParErrFrc_Bit_End                                         9
#define cAf6_RAM_Parity_Force_Control_PDAJitBufCtrlParErrFrc_Mask                                        cBit9
#define cAf6_RAM_Parity_Force_Control_PDAJitBufCtrlParErrFrc_Shift                                           9
#define cAf6_RAM_Parity_Force_Control_PDAJitBufCtrlParErrFrc_MaxVal                                        0x1
#define cAf6_RAM_Parity_Force_Control_PDAJitBufCtrlParErrFrc_MinVal                                        0x0
#define cAf6_RAM_Parity_Force_Control_PDAJitBufCtrlParErrFrc_RstVal                                        0x0

/*--------------------------------------
BitField Name: PDAReOrCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For Pseudowire PDA Reorder Control RAM
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDAReOrCtrlParErrFrc_Bit_Start                                         8
#define cAf6_RAM_Parity_Force_Control_PDAReOrCtrlParErrFrc_Bit_End                                           8
#define cAf6_RAM_Parity_Force_Control_PDAReOrCtrlParErrFrc_Mask                                          cBit8
#define cAf6_RAM_Parity_Force_Control_PDAReOrCtrlParErrFrc_Shift                                             8
#define cAf6_RAM_Parity_Force_Control_PDAReOrCtrlParErrFrc_MaxVal                                          0x1
#define cAf6_RAM_Parity_Force_Control_PDAReOrCtrlParErrFrc_MinVal                                          0x0
#define cAf6_RAM_Parity_Force_Control_PDAReOrCtrlParErrFrc_RstVal                                          0x0

/*--------------------------------------
BitField Name: PLACtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For Thalassa PDHPW Payload Assemble Payload Size
Control RAM
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PLACtrlParErrFrc_Bit_Start                                             7
#define cAf6_RAM_Parity_Force_Control_PLACtrlParErrFrc_Bit_End                                               7
#define cAf6_RAM_Parity_Force_Control_PLACtrlParErrFrc_Mask                                              cBit7
#define cAf6_RAM_Parity_Force_Control_PLACtrlParErrFrc_Shift                                                 7
#define cAf6_RAM_Parity_Force_Control_PLACtrlParErrFrc_MaxVal                                              0x1
#define cAf6_RAM_Parity_Force_Control_PLACtrlParErrFrc_MinVal                                              0x0
#define cAf6_RAM_Parity_Force_Control_PLACtrlParErrFrc_RstVal                                              0x0

/*--------------------------------------
BitField Name: CDRCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For CDR Engine Timing control RAM
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_CDRCtrlParErrFrc_Bit_Start                                             6
#define cAf6_RAM_Parity_Force_Control_CDRCtrlParErrFrc_Bit_End                                               6
#define cAf6_RAM_Parity_Force_Control_CDRCtrlParErrFrc_Mask                                              cBit6
#define cAf6_RAM_Parity_Force_Control_CDRCtrlParErrFrc_Shift                                                 6
#define cAf6_RAM_Parity_Force_Control_CDRCtrlParErrFrc_MaxVal                                              0x1
#define cAf6_RAM_Parity_Force_Control_CDRCtrlParErrFrc_MinVal                                              0x0
#define cAf6_RAM_Parity_Force_Control_CDRCtrlParErrFrc_RstVal                                              0x0

/*--------------------------------------
BitField Name: MAPSrcCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For Thalassa Map Timing Reference Control RAM
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_MAPSrcCtrlParErrFrc_Bit_Start                                          5
#define cAf6_RAM_Parity_Force_Control_MAPSrcCtrlParErrFrc_Bit_End                                            5
#define cAf6_RAM_Parity_Force_Control_MAPSrcCtrlParErrFrc_Mask                                           cBit5
#define cAf6_RAM_Parity_Force_Control_MAPSrcCtrlParErrFrc_Shift                                              5
#define cAf6_RAM_Parity_Force_Control_MAPSrcCtrlParErrFrc_MaxVal                                           0x1
#define cAf6_RAM_Parity_Force_Control_MAPSrcCtrlParErrFrc_MinVal                                           0x0
#define cAf6_RAM_Parity_Force_Control_MAPSrcCtrlParErrFrc_RstVal                                           0x0

/*--------------------------------------
BitField Name: MAPChlCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For  RAM
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrlParErrFrc_Bit_Start                                          4
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrlParErrFrc_Bit_End                                            4
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrlParErrFrc_Mask                                           cBit4
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrlParErrFrc_Shift                                              4
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrlParErrFrc_MaxVal                                           0x1
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrlParErrFrc_MinVal                                           0x0
#define cAf6_RAM_Parity_Force_Control_MAPChlCtrlParErrFrc_RstVal                                           0x0

/*--------------------------------------
BitField Name: DeMapCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For Thalassa Demap Channel Control RAM
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_DeMapCtrlParErrFrc_Bit_Start                                           3
#define cAf6_RAM_Parity_Force_Control_DeMapCtrlParErrFrc_Bit_End                                             3
#define cAf6_RAM_Parity_Force_Control_DeMapCtrlParErrFrc_Mask                                            cBit3
#define cAf6_RAM_Parity_Force_Control_DeMapCtrlParErrFrc_Shift                                               3
#define cAf6_RAM_Parity_Force_Control_DeMapCtrlParErrFrc_MaxVal                                            0x1
#define cAf6_RAM_Parity_Force_Control_DeMapCtrlParErrFrc_MinVal                                            0x0
#define cAf6_RAM_Parity_Force_Control_DeMapCtrlParErrFrc_RstVal                                            0x0

/*--------------------------------------
BitField Name: PDHDE1TxSigCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For DS1/E1/J1 Tx Framer Signaling Insertion Control
RAM
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxSigCtrlParErrFrc_Bit_Start                                       2
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxSigCtrlParErrFrc_Bit_End                                       2
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxSigCtrlParErrFrc_Mask                                      cBit2
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxSigCtrlParErrFrc_Shift                                         2
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxSigCtrlParErrFrc_MaxVal                                      0x1
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxSigCtrlParErrFrc_MinVal                                      0x0
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxSigCtrlParErrFrc_RstVal                                      0x0

/*--------------------------------------
BitField Name: PDHDE1TxFrmCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For DS1/E1/J1 Tx Framer Control RAM
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxFrmCtrlParErrFrc_Bit_Start                                       1
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxFrmCtrlParErrFrc_Bit_End                                       1
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxFrmCtrlParErrFrc_Mask                                      cBit1
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxFrmCtrlParErrFrc_Shift                                         1
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxFrmCtrlParErrFrc_MaxVal                                      0x1
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxFrmCtrlParErrFrc_MinVal                                      0x0
#define cAf6_RAM_Parity_Force_Control_PDHDE1TxFrmCtrlParErrFrc_RstVal                                      0x0

/*--------------------------------------
BitField Name: PDHDE1RxFrmCtrlParErrFrc
BitField Type: RW
BitField Desc: Force parity For DS1/E1/J1 Rx Framer Control RAM
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Force_Control_PDHDE1RxFrmCtrlParErrFrc_Bit_Start                                       0
#define cAf6_RAM_Parity_Force_Control_PDHDE1RxFrmCtrlParErrFrc_Bit_End                                       0
#define cAf6_RAM_Parity_Force_Control_PDHDE1RxFrmCtrlParErrFrc_Mask                                      cBit0
#define cAf6_RAM_Parity_Force_Control_PDHDE1RxFrmCtrlParErrFrc_Shift                                         0
#define cAf6_RAM_Parity_Force_Control_PDHDE1RxFrmCtrlParErrFrc_MaxVal                                      0x1
#define cAf6_RAM_Parity_Force_Control_PDHDE1RxFrmCtrlParErrFrc_MinVal                                      0x0
#define cAf6_RAM_Parity_Force_Control_PDHDE1RxFrmCtrlParErrFrc_RstVal                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM parity Disable Control
Reg Addr   : 0x108
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Parity_Disable_Control_Base                                                          0x108
#define cAf6Reg_RAM_Parity_Disable_Control                                                               0x108
#define cAf6Reg_RAM_Parity_Disable_Control_WidthVal                                                         32
#define cAf6Reg_RAM_Parity_Disable_Control_WriteMask                                                       0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl5ParDis
BitField Type: RW
BitField Desc: Disable parity For CLA Classify Classify Per Group Enable Control
RAM
BitField Bits: [23]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl5ParDis_Bit_Start                                        23
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl5ParDis_Bit_End                                          23
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl5ParDis_Mask                                         cBit23
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl5ParDis_Shift                                            23
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl5ParDis_MaxVal                                          0x1
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl5ParDis_MinVal                                          0x0
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl5ParDis_RstVal                                          0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl4ParDis
BitField Type: RW
BitField Desc: Disable parity For CLA Classify Pseudowire Type Control RAM
BitField Bits: [22]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl4ParDis_Bit_Start                                        22
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl4ParDis_Bit_End                                          22
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl4ParDis_Mask                                         cBit22
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl4ParDis_Shift                                            22
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl4ParDis_MaxVal                                          0x1
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl4ParDis_MinVal                                          0x0
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl4ParDis_RstVal                                          0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl3ParDis
BitField Type: RW
BitField Desc: Disable parity For CLA Classify HBCE Looking Up Information
Control RAM
BitField Bits: [21]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl3ParDis_Bit_Start                                        21
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl3ParDis_Bit_End                                          21
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl3ParDis_Mask                                         cBit21
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl3ParDis_Shift                                            21
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl3ParDis_MaxVal                                          0x1
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl3ParDis_MinVal                                          0x0
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl3ParDis_RstVal                                          0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl2ParDis
BitField Type: RW
BitField Desc: Disable parity For CLA Classify HBCE Hashing Table page1 Control
RAM
BitField Bits: [20]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl2ParDis_Bit_Start                                        20
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl2ParDis_Bit_End                                          20
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl2ParDis_Mask                                         cBit20
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl2ParDis_Shift                                            20
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl2ParDis_MaxVal                                          0x1
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl2ParDis_MinVal                                          0x0
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl2ParDis_RstVal                                          0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl1ParDis
BitField Type: RW
BitField Desc: Disable parity For CLA Classify HBCE Hashing Table page0 Control
RAM
BitField Bits: [19]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl1ParDis_Bit_Start                                        19
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl1ParDis_Bit_End                                          19
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl1ParDis_Mask                                         cBit19
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl1ParDis_Shift                                            19
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl1ParDis_MaxVal                                          0x1
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl1ParDis_MinVal                                          0x0
#define cAf6_RAM_Parity_Disable_Control_CLAHBCECtrl1ParDis_RstVal                                          0x0

/*--------------------------------------
BitField Name: CLAVLANCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For CLA Classify VLAN TAG Lookup Control  RAM
BitField Bits: [18]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CLAVLANCtrlParDis_Bit_Start                                         18
#define cAf6_RAM_Parity_Disable_Control_CLAVLANCtrlParDis_Bit_End                                           18
#define cAf6_RAM_Parity_Disable_Control_CLAVLANCtrlParDis_Mask                                          cBit18
#define cAf6_RAM_Parity_Disable_Control_CLAVLANCtrlParDis_Shift                                             18
#define cAf6_RAM_Parity_Disable_Control_CLAVLANCtrlParDis_MaxVal                                           0x1
#define cAf6_RAM_Parity_Disable_Control_CLAVLANCtrlParDis_MinVal                                           0x0
#define cAf6_RAM_Parity_Disable_Control_CLAVLANCtrlParDis_RstVal                                           0x0

/*--------------------------------------
BitField Name: PWEHSGrpExtParDis
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit HSPW Group Protection
Control  RAM
BitField Bits: [17]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpExtParDis_Bit_Start                                         17
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpExtParDis_Bit_End                                           17
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpExtParDis_Mask                                          cBit17
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpExtParDis_Shift                                             17
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpExtParDis_MaxVal                                           0x1
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpExtParDis_MinVal                                           0x0
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpExtParDis_RstVal                                           0x0

/*--------------------------------------
BitField Name: PWEUPSRCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit UPSR Group Enable
Control  RAM
BitField Bits: [16]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWEUPSRCtrlParDis_Bit_Start                                         16
#define cAf6_RAM_Parity_Disable_Control_PWEUPSRCtrlParDis_Bit_End                                           16
#define cAf6_RAM_Parity_Disable_Control_PWEUPSRCtrlParDis_Mask                                          cBit16
#define cAf6_RAM_Parity_Disable_Control_PWEUPSRCtrlParDis_Shift                                             16
#define cAf6_RAM_Parity_Disable_Control_PWEUPSRCtrlParDis_MaxVal                                           0x1
#define cAf6_RAM_Parity_Disable_Control_PWEUPSRCtrlParDis_MinVal                                           0x0
#define cAf6_RAM_Parity_Disable_Control_PWEUPSRCtrlParDis_RstVal                                           0x0

/*--------------------------------------
BitField Name: PWEHSGrpCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit UPSR and HSPW mode
Control   RAM
BitField Bits: [15]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpCtrlParDis_Bit_Start                                        15
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpCtrlParDis_Bit_End                                          15
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpCtrlParDis_Mask                                         cBit15
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpCtrlParDis_Shift                                            15
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpCtrlParDis_MaxVal                                          0x1
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpCtrlParDis_MinVal                                          0x0
#define cAf6_RAM_Parity_Disable_Control_PWEHSGrpCtrlParDis_RstVal                                          0x0

/*--------------------------------------
BitField Name: PWEHSPWCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit HSPW Label Control
RAM
BitField Bits: [14]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWEHSPWCtrlParDis_Bit_Start                                         14
#define cAf6_RAM_Parity_Disable_Control_PWEHSPWCtrlParDis_Bit_End                                           14
#define cAf6_RAM_Parity_Disable_Control_PWEHSPWCtrlParDis_Mask                                          cBit14
#define cAf6_RAM_Parity_Disable_Control_PWEHSPWCtrlParDis_Shift                                             14
#define cAf6_RAM_Parity_Disable_Control_PWEHSPWCtrlParDis_MaxVal                                           0x1
#define cAf6_RAM_Parity_Disable_Control_PWEHSPWCtrlParDis_MinVal                                           0x0
#define cAf6_RAM_Parity_Disable_Control_PWEHSPWCtrlParDis_RstVal                                           0x0

/*--------------------------------------
BitField Name: PWESRCCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit Header RTP SSRC Value
Control  RAM
BitField Bits: [13]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWESRCCtrlParDis_Bit_Start                                          13
#define cAf6_RAM_Parity_Disable_Control_PWESRCCtrlParDis_Bit_End                                            13
#define cAf6_RAM_Parity_Disable_Control_PWESRCCtrlParDis_Mask                                           cBit13
#define cAf6_RAM_Parity_Disable_Control_PWESRCCtrlParDis_Shift                                              13
#define cAf6_RAM_Parity_Disable_Control_PWESRCCtrlParDis_MaxVal                                            0x1
#define cAf6_RAM_Parity_Disable_Control_PWESRCCtrlParDis_MinVal                                            0x0
#define cAf6_RAM_Parity_Disable_Control_PWESRCCtrlParDis_RstVal                                            0x0

/*--------------------------------------
BitField Name: PWEPSNModeCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit Ethernet Header Length
Control  RAM
BitField Bits: [12]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWEPSNModeCtrlParDis_Bit_Start                                      12
#define cAf6_RAM_Parity_Disable_Control_PWEPSNModeCtrlParDis_Bit_End                                        12
#define cAf6_RAM_Parity_Disable_Control_PWEPSNModeCtrlParDis_Mask                                       cBit12
#define cAf6_RAM_Parity_Disable_Control_PWEPSNModeCtrlParDis_Shift                                          12
#define cAf6_RAM_Parity_Disable_Control_PWEPSNModeCtrlParDis_MaxVal                                        0x1
#define cAf6_RAM_Parity_Disable_Control_PWEPSNModeCtrlParDis_MinVal                                        0x0
#define cAf6_RAM_Parity_Disable_Control_PWEPSNModeCtrlParDis_RstVal                                        0x0

/*--------------------------------------
BitField Name: PWEPSNHeaderCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit Ethernet Header Value
Control  RAM
BitField Bits: [11]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PWEPSNHeaderCtrlParDis_Bit_Start                                      11
#define cAf6_RAM_Parity_Disable_Control_PWEPSNHeaderCtrlParDis_Bit_End                                      11
#define cAf6_RAM_Parity_Disable_Control_PWEPSNHeaderCtrlParDis_Mask                                     cBit11
#define cAf6_RAM_Parity_Disable_Control_PWEPSNHeaderCtrlParDis_Shift                                        11
#define cAf6_RAM_Parity_Disable_Control_PWEPSNHeaderCtrlParDis_MaxVal                                      0x1
#define cAf6_RAM_Parity_Disable_Control_PWEPSNHeaderCtrlParDis_MinVal                                      0x0
#define cAf6_RAM_Parity_Disable_Control_PWEPSNHeaderCtrlParDis_RstVal                                      0x0

/*--------------------------------------
BitField Name: PDADeasCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For Pseudowire PDA TDM Mode Control RAM
BitField Bits: [10]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDADeasCtrlParDis_Bit_Start                                         10
#define cAf6_RAM_Parity_Disable_Control_PDADeasCtrlParDis_Bit_End                                           10
#define cAf6_RAM_Parity_Disable_Control_PDADeasCtrlParDis_Mask                                          cBit10
#define cAf6_RAM_Parity_Disable_Control_PDADeasCtrlParDis_Shift                                             10
#define cAf6_RAM_Parity_Disable_Control_PDADeasCtrlParDis_MaxVal                                           0x1
#define cAf6_RAM_Parity_Disable_Control_PDADeasCtrlParDis_MinVal                                           0x0
#define cAf6_RAM_Parity_Disable_Control_PDADeasCtrlParDis_RstVal                                           0x0

/*--------------------------------------
BitField Name: PDAJitBufCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For Pseudowire PDA Jitter Buffer Control RAM
BitField Bits: [9]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDAJitBufCtrlParDis_Bit_Start                                        9
#define cAf6_RAM_Parity_Disable_Control_PDAJitBufCtrlParDis_Bit_End                                          9
#define cAf6_RAM_Parity_Disable_Control_PDAJitBufCtrlParDis_Mask                                         cBit9
#define cAf6_RAM_Parity_Disable_Control_PDAJitBufCtrlParDis_Shift                                            9
#define cAf6_RAM_Parity_Disable_Control_PDAJitBufCtrlParDis_MaxVal                                         0x1
#define cAf6_RAM_Parity_Disable_Control_PDAJitBufCtrlParDis_MinVal                                         0x0
#define cAf6_RAM_Parity_Disable_Control_PDAJitBufCtrlParDis_RstVal                                         0x0

/*--------------------------------------
BitField Name: PDAReOrCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For Pseudowire PDA Reorder Control RAM
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDAReOrCtrlParDis_Bit_Start                                          8
#define cAf6_RAM_Parity_Disable_Control_PDAReOrCtrlParDis_Bit_End                                            8
#define cAf6_RAM_Parity_Disable_Control_PDAReOrCtrlParDis_Mask                                           cBit8
#define cAf6_RAM_Parity_Disable_Control_PDAReOrCtrlParDis_Shift                                              8
#define cAf6_RAM_Parity_Disable_Control_PDAReOrCtrlParDis_MaxVal                                           0x1
#define cAf6_RAM_Parity_Disable_Control_PDAReOrCtrlParDis_MinVal                                           0x0
#define cAf6_RAM_Parity_Disable_Control_PDAReOrCtrlParDis_RstVal                                           0x0

/*--------------------------------------
BitField Name: PLACtrlParDis
BitField Type: RW
BitField Desc: Disable parity For Thalassa PDHPW Payload Assemble Payload Size
Control RAM
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PLACtrlParDis_Bit_Start                                              7
#define cAf6_RAM_Parity_Disable_Control_PLACtrlParDis_Bit_End                                                7
#define cAf6_RAM_Parity_Disable_Control_PLACtrlParDis_Mask                                               cBit7
#define cAf6_RAM_Parity_Disable_Control_PLACtrlParDis_Shift                                                  7
#define cAf6_RAM_Parity_Disable_Control_PLACtrlParDis_MaxVal                                               0x1
#define cAf6_RAM_Parity_Disable_Control_PLACtrlParDis_MinVal                                               0x0
#define cAf6_RAM_Parity_Disable_Control_PLACtrlParDis_RstVal                                               0x0

/*--------------------------------------
BitField Name: CDRCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For CDR Engine Timing control RAM
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_CDRCtrlParDis_Bit_Start                                              6
#define cAf6_RAM_Parity_Disable_Control_CDRCtrlParDis_Bit_End                                                6
#define cAf6_RAM_Parity_Disable_Control_CDRCtrlParDis_Mask                                               cBit6
#define cAf6_RAM_Parity_Disable_Control_CDRCtrlParDis_Shift                                                  6
#define cAf6_RAM_Parity_Disable_Control_CDRCtrlParDis_MaxVal                                               0x1
#define cAf6_RAM_Parity_Disable_Control_CDRCtrlParDis_MinVal                                               0x0
#define cAf6_RAM_Parity_Disable_Control_CDRCtrlParDis_RstVal                                               0x0

/*--------------------------------------
BitField Name: MAPSrcCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For Thalassa Map Timing Reference Control RAM
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_MAPSrcCtrlParDis_Bit_Start                                           5
#define cAf6_RAM_Parity_Disable_Control_MAPSrcCtrlParDis_Bit_End                                             5
#define cAf6_RAM_Parity_Disable_Control_MAPSrcCtrlParDis_Mask                                            cBit5
#define cAf6_RAM_Parity_Disable_Control_MAPSrcCtrlParDis_Shift                                               5
#define cAf6_RAM_Parity_Disable_Control_MAPSrcCtrlParDis_MaxVal                                            0x1
#define cAf6_RAM_Parity_Disable_Control_MAPSrcCtrlParDis_MinVal                                            0x0
#define cAf6_RAM_Parity_Disable_Control_MAPSrcCtrlParDis_RstVal                                            0x0

/*--------------------------------------
BitField Name: MAPChlCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For  RAM
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlParDis_Bit_Start                                           4
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlParDis_Bit_End                                             4
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlParDis_Mask                                            cBit4
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlParDis_Shift                                               4
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlParDis_MaxVal                                            0x1
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlParDis_MinVal                                            0x0
#define cAf6_RAM_Parity_Disable_Control_MAPChlCtrlParDis_RstVal                                            0x0

/*--------------------------------------
BitField Name: DeMapCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For Thalassa Demap Channel Control RAM
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_DeMapCtrlParDis_Bit_Start                                            3
#define cAf6_RAM_Parity_Disable_Control_DeMapCtrlParDis_Bit_End                                              3
#define cAf6_RAM_Parity_Disable_Control_DeMapCtrlParDis_Mask                                             cBit3
#define cAf6_RAM_Parity_Disable_Control_DeMapCtrlParDis_Shift                                                3
#define cAf6_RAM_Parity_Disable_Control_DeMapCtrlParDis_MaxVal                                             0x1
#define cAf6_RAM_Parity_Disable_Control_DeMapCtrlParDis_MinVal                                             0x0
#define cAf6_RAM_Parity_Disable_Control_DeMapCtrlParDis_RstVal                                             0x0

/*--------------------------------------
BitField Name: PDHDE1TxSigCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For DS1/E1/J1 Tx Framer Signaling Insertion
Control RAM
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxSigCtrlParDis_Bit_Start                                       2
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxSigCtrlParDis_Bit_End                                        2
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxSigCtrlParDis_Mask                                       cBit2
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxSigCtrlParDis_Shift                                          2
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxSigCtrlParDis_MaxVal                                       0x1
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxSigCtrlParDis_MinVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxSigCtrlParDis_RstVal                                       0x0

/*--------------------------------------
BitField Name: PDHDE1TxFrmCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For DS1/E1/J1 Tx Framer Control RAM
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxFrmCtrlParDis_Bit_Start                                       1
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxFrmCtrlParDis_Bit_End                                        1
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxFrmCtrlParDis_Mask                                       cBit1
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxFrmCtrlParDis_Shift                                          1
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxFrmCtrlParDis_MaxVal                                       0x1
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxFrmCtrlParDis_MinVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_PDHDE1TxFrmCtrlParDis_RstVal                                       0x0

/*--------------------------------------
BitField Name: PDHDE1RxFrmCtrlParDis
BitField Type: RW
BitField Desc: Disable parity For DS1/E1/J1 Rx Framer Control RAM
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Disable_Control_PDHDE1RxFrmCtrlParDis_Bit_Start                                       0
#define cAf6_RAM_Parity_Disable_Control_PDHDE1RxFrmCtrlParDis_Bit_End                                        0
#define cAf6_RAM_Parity_Disable_Control_PDHDE1RxFrmCtrlParDis_Mask                                       cBit0
#define cAf6_RAM_Parity_Disable_Control_PDHDE1RxFrmCtrlParDis_Shift                                          0
#define cAf6_RAM_Parity_Disable_Control_PDHDE1RxFrmCtrlParDis_MaxVal                                       0x1
#define cAf6_RAM_Parity_Disable_Control_PDHDE1RxFrmCtrlParDis_MinVal                                       0x0
#define cAf6_RAM_Parity_Disable_Control_PDHDE1RxFrmCtrlParDis_RstVal                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : RAM parity Error Sticky
Reg Addr   : 0x112
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Parity_Error_Sticky_Base                                                             0x112
#define cAf6Reg_RAM_Parity_Error_Sticky                                                                  0x112
#define cAf6Reg_RAM_Parity_Error_Sticky_WidthVal                                                            32
#define cAf6Reg_RAM_Parity_Error_Sticky_WriteMask                                                          0x0

/*--------------------------------------
BitField Name: DDRCRCErrStk
BitField Type: RW
BitField Desc: Disable parity For DDR
BitField Bits: [24]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DDRCRCErrStk_Bit_Start                                                 24
#define cAf6_RAM_Parity_Error_Sticky_DDRCRCErrStk_Bit_End                                                   24
#define cAf6_RAM_Parity_Error_Sticky_DDRCRCErrStk_Mask                                                  cBit24
#define cAf6_RAM_Parity_Error_Sticky_DDRCRCErrStk_Shift                                                     24
#define cAf6_RAM_Parity_Error_Sticky_DDRCRCErrStk_MaxVal                                                   0x1
#define cAf6_RAM_Parity_Error_Sticky_DDRCRCErrStk_MinVal                                                   0x0
#define cAf6_RAM_Parity_Error_Sticky_DDRCRCErrStk_RstVal                                                   0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl5ErrStk
BitField Type: RW
BitField Desc: Disable parity For CLA Classify Classify Per Group Enable Control
RAM
BitField Bits: [23]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl5ErrStk_Bit_Start                                           23
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl5ErrStk_Bit_End                                             23
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl5ErrStk_Mask                                            cBit23
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl5ErrStk_Shift                                               23
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl5ErrStk_MaxVal                                             0x1
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl5ErrStk_MinVal                                             0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl5ErrStk_RstVal                                             0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl4ErrStk
BitField Type: RW
BitField Desc: Disable parity For CLA Classify Pseudowire Type Control RAM
BitField Bits: [22]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl4ErrStk_Bit_Start                                           22
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl4ErrStk_Bit_End                                             22
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl4ErrStk_Mask                                            cBit22
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl4ErrStk_Shift                                               22
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl4ErrStk_MaxVal                                             0x1
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl4ErrStk_MinVal                                             0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl4ErrStk_RstVal                                             0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl3ErrStk
BitField Type: RW
BitField Desc: Disable parity For CLA Classify HBCE Looking Up Information
Control RAM
BitField Bits: [21]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl3ErrStk_Bit_Start                                           21
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl3ErrStk_Bit_End                                             21
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl3ErrStk_Mask                                            cBit21
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl3ErrStk_Shift                                               21
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl3ErrStk_MaxVal                                             0x1
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl3ErrStk_MinVal                                             0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl3ErrStk_RstVal                                             0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl2ErrStk
BitField Type: RW
BitField Desc: Disable parity For CLA Classify HBCE Hashing Table page1 Control
RAM
BitField Bits: [20]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl2ErrStk_Bit_Start                                           20
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl2ErrStk_Bit_End                                             20
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl2ErrStk_Mask                                            cBit20
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl2ErrStk_Shift                                               20
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl2ErrStk_MaxVal                                             0x1
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl2ErrStk_MinVal                                             0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl2ErrStk_RstVal                                             0x0

/*--------------------------------------
BitField Name: CLAHBCECtrl1ErrStk
BitField Type: RW
BitField Desc: Disable parity For CLA Classify HBCE Hashing Table page0 Control
RAM
BitField Bits: [19]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl1ErrStk_Bit_Start                                           19
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl1ErrStk_Bit_End                                             19
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl1ErrStk_Mask                                            cBit19
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl1ErrStk_Shift                                               19
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl1ErrStk_MaxVal                                             0x1
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl1ErrStk_MinVal                                             0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAHBCECtrl1ErrStk_RstVal                                             0x0

/*--------------------------------------
BitField Name: CLAVLANCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For CLA Classify VLAN TAG Lookup Control  RAM
BitField Bits: [18]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CLAVLANCtrlErrStk_Bit_Start                                            18
#define cAf6_RAM_Parity_Error_Sticky_CLAVLANCtrlErrStk_Bit_End                                              18
#define cAf6_RAM_Parity_Error_Sticky_CLAVLANCtrlErrStk_Mask                                             cBit18
#define cAf6_RAM_Parity_Error_Sticky_CLAVLANCtrlErrStk_Shift                                                18
#define cAf6_RAM_Parity_Error_Sticky_CLAVLANCtrlErrStk_MaxVal                                              0x1
#define cAf6_RAM_Parity_Error_Sticky_CLAVLANCtrlErrStk_MinVal                                              0x0
#define cAf6_RAM_Parity_Error_Sticky_CLAVLANCtrlErrStk_RstVal                                              0x0

/*--------------------------------------
BitField Name: PWEHSGrpExtErrStk
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit HSPW Group Protection
Control  RAM
BitField Bits: [17]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpExtErrStk_Bit_Start                                            17
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpExtErrStk_Bit_End                                              17
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpExtErrStk_Mask                                             cBit17
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpExtErrStk_Shift                                                17
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpExtErrStk_MaxVal                                              0x1
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpExtErrStk_MinVal                                              0x0
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpExtErrStk_RstVal                                              0x0

/*--------------------------------------
BitField Name: PWEUPSRCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit UPSR Group Enable
Control  RAM
BitField Bits: [16]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWEUPSRCtrlErrStk_Bit_Start                                            16
#define cAf6_RAM_Parity_Error_Sticky_PWEUPSRCtrlErrStk_Bit_End                                              16
#define cAf6_RAM_Parity_Error_Sticky_PWEUPSRCtrlErrStk_Mask                                             cBit16
#define cAf6_RAM_Parity_Error_Sticky_PWEUPSRCtrlErrStk_Shift                                                16
#define cAf6_RAM_Parity_Error_Sticky_PWEUPSRCtrlErrStk_MaxVal                                              0x1
#define cAf6_RAM_Parity_Error_Sticky_PWEUPSRCtrlErrStk_MinVal                                              0x0
#define cAf6_RAM_Parity_Error_Sticky_PWEUPSRCtrlErrStk_RstVal                                              0x0

/*--------------------------------------
BitField Name: PWEHSGrpCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit UPSR and HSPW mode
Control   RAM
BitField Bits: [15]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpCtrlErrStk_Bit_Start                                           15
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpCtrlErrStk_Bit_End                                             15
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpCtrlErrStk_Mask                                            cBit15
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpCtrlErrStk_Shift                                               15
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpCtrlErrStk_MaxVal                                             0x1
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpCtrlErrStk_MinVal                                             0x0
#define cAf6_RAM_Parity_Error_Sticky_PWEHSGrpCtrlErrStk_RstVal                                             0x0

/*--------------------------------------
BitField Name: PWEHSPWCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit HSPW Label Control
RAM
BitField Bits: [14]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWEHSPWCtrlErrStk_Bit_Start                                            14
#define cAf6_RAM_Parity_Error_Sticky_PWEHSPWCtrlErrStk_Bit_End                                              14
#define cAf6_RAM_Parity_Error_Sticky_PWEHSPWCtrlErrStk_Mask                                             cBit14
#define cAf6_RAM_Parity_Error_Sticky_PWEHSPWCtrlErrStk_Shift                                                14
#define cAf6_RAM_Parity_Error_Sticky_PWEHSPWCtrlErrStk_MaxVal                                              0x1
#define cAf6_RAM_Parity_Error_Sticky_PWEHSPWCtrlErrStk_MinVal                                              0x0
#define cAf6_RAM_Parity_Error_Sticky_PWEHSPWCtrlErrStk_RstVal                                              0x0

/*--------------------------------------
BitField Name: PWESRCCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit Header RTP SSRC Value
Control  RAM
BitField Bits: [13]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWESRCCtrlErrStk_Bit_Start                                             13
#define cAf6_RAM_Parity_Error_Sticky_PWESRCCtrlErrStk_Bit_End                                               13
#define cAf6_RAM_Parity_Error_Sticky_PWESRCCtrlErrStk_Mask                                              cBit13
#define cAf6_RAM_Parity_Error_Sticky_PWESRCCtrlErrStk_Shift                                                 13
#define cAf6_RAM_Parity_Error_Sticky_PWESRCCtrlErrStk_MaxVal                                               0x1
#define cAf6_RAM_Parity_Error_Sticky_PWESRCCtrlErrStk_MinVal                                               0x0
#define cAf6_RAM_Parity_Error_Sticky_PWESRCCtrlErrStk_RstVal                                               0x0

/*--------------------------------------
BitField Name: PWEPSNModeCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit Ethernet Header Length
Control  RAM
BitField Bits: [12]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNModeCtrlErrStk_Bit_Start                                         12
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNModeCtrlErrStk_Bit_End                                           12
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNModeCtrlErrStk_Mask                                          cBit12
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNModeCtrlErrStk_Shift                                             12
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNModeCtrlErrStk_MaxVal                                           0x1
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNModeCtrlErrStk_MinVal                                           0x0
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNModeCtrlErrStk_RstVal                                           0x0

/*--------------------------------------
BitField Name: PWEPSNHeaderCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For PWE Pseudowire Transmit Ethernet Header Value
Control  RAM
BitField Bits: [11]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNHeaderCtrlErrStk_Bit_Start                                       11
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNHeaderCtrlErrStk_Bit_End                                         11
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNHeaderCtrlErrStk_Mask                                        cBit11
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNHeaderCtrlErrStk_Shift                                           11
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNHeaderCtrlErrStk_MaxVal                                         0x1
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNHeaderCtrlErrStk_MinVal                                         0x0
#define cAf6_RAM_Parity_Error_Sticky_PWEPSNHeaderCtrlErrStk_RstVal                                         0x0

/*--------------------------------------
BitField Name: PDADeasCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For Pseudowire PDA TDM Mode Control RAM
BitField Bits: [10]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDADeasCtrlErrStk_Bit_Start                                            10
#define cAf6_RAM_Parity_Error_Sticky_PDADeasCtrlErrStk_Bit_End                                              10
#define cAf6_RAM_Parity_Error_Sticky_PDADeasCtrlErrStk_Mask                                             cBit10
#define cAf6_RAM_Parity_Error_Sticky_PDADeasCtrlErrStk_Shift                                                10
#define cAf6_RAM_Parity_Error_Sticky_PDADeasCtrlErrStk_MaxVal                                              0x1
#define cAf6_RAM_Parity_Error_Sticky_PDADeasCtrlErrStk_MinVal                                              0x0
#define cAf6_RAM_Parity_Error_Sticky_PDADeasCtrlErrStk_RstVal                                              0x0

/*--------------------------------------
BitField Name: PDAJitBufCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For Pseudowire PDA Jitter Buffer Control RAM
BitField Bits: [9]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDAJitBufCtrlErrStk_Bit_Start                                           9
#define cAf6_RAM_Parity_Error_Sticky_PDAJitBufCtrlErrStk_Bit_End                                             9
#define cAf6_RAM_Parity_Error_Sticky_PDAJitBufCtrlErrStk_Mask                                            cBit9
#define cAf6_RAM_Parity_Error_Sticky_PDAJitBufCtrlErrStk_Shift                                               9
#define cAf6_RAM_Parity_Error_Sticky_PDAJitBufCtrlErrStk_MaxVal                                            0x1
#define cAf6_RAM_Parity_Error_Sticky_PDAJitBufCtrlErrStk_MinVal                                            0x0
#define cAf6_RAM_Parity_Error_Sticky_PDAJitBufCtrlErrStk_RstVal                                            0x0

/*--------------------------------------
BitField Name: PDAReOrCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For Pseudowire PDA Reorder Control RAM
BitField Bits: [8]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDAReOrCtrlErrStk_Bit_Start                                             8
#define cAf6_RAM_Parity_Error_Sticky_PDAReOrCtrlErrStk_Bit_End                                               8
#define cAf6_RAM_Parity_Error_Sticky_PDAReOrCtrlErrStk_Mask                                              cBit8
#define cAf6_RAM_Parity_Error_Sticky_PDAReOrCtrlErrStk_Shift                                                 8
#define cAf6_RAM_Parity_Error_Sticky_PDAReOrCtrlErrStk_MaxVal                                              0x1
#define cAf6_RAM_Parity_Error_Sticky_PDAReOrCtrlErrStk_MinVal                                              0x0
#define cAf6_RAM_Parity_Error_Sticky_PDAReOrCtrlErrStk_RstVal                                              0x0

/*--------------------------------------
BitField Name: PLACtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For Thalassa PDHPW Payload Assemble Payload Size
Control RAM
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PLACtrlErrStk_Bit_Start                                                 7
#define cAf6_RAM_Parity_Error_Sticky_PLACtrlErrStk_Bit_End                                                   7
#define cAf6_RAM_Parity_Error_Sticky_PLACtrlErrStk_Mask                                                  cBit7
#define cAf6_RAM_Parity_Error_Sticky_PLACtrlErrStk_Shift                                                     7
#define cAf6_RAM_Parity_Error_Sticky_PLACtrlErrStk_MaxVal                                                  0x1
#define cAf6_RAM_Parity_Error_Sticky_PLACtrlErrStk_MinVal                                                  0x0
#define cAf6_RAM_Parity_Error_Sticky_PLACtrlErrStk_RstVal                                                  0x0

/*--------------------------------------
BitField Name: CDRCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For CDR Engine Timing control RAM
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_CDRCtrlErrStk_Bit_Start                                                 6
#define cAf6_RAM_Parity_Error_Sticky_CDRCtrlErrStk_Bit_End                                                   6
#define cAf6_RAM_Parity_Error_Sticky_CDRCtrlErrStk_Mask                                                  cBit6
#define cAf6_RAM_Parity_Error_Sticky_CDRCtrlErrStk_Shift                                                     6
#define cAf6_RAM_Parity_Error_Sticky_CDRCtrlErrStk_MaxVal                                                  0x1
#define cAf6_RAM_Parity_Error_Sticky_CDRCtrlErrStk_MinVal                                                  0x0
#define cAf6_RAM_Parity_Error_Sticky_CDRCtrlErrStk_RstVal                                                  0x0

/*--------------------------------------
BitField Name: MAPSrcCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For Thalassa Map Timing Reference Control RAM
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_MAPSrcCtrlErrStk_Bit_Start                                              5
#define cAf6_RAM_Parity_Error_Sticky_MAPSrcCtrlErrStk_Bit_End                                                5
#define cAf6_RAM_Parity_Error_Sticky_MAPSrcCtrlErrStk_Mask                                               cBit5
#define cAf6_RAM_Parity_Error_Sticky_MAPSrcCtrlErrStk_Shift                                                  5
#define cAf6_RAM_Parity_Error_Sticky_MAPSrcCtrlErrStk_MaxVal                                               0x1
#define cAf6_RAM_Parity_Error_Sticky_MAPSrcCtrlErrStk_MinVal                                               0x0
#define cAf6_RAM_Parity_Error_Sticky_MAPSrcCtrlErrStk_RstVal                                               0x0

/*--------------------------------------
BitField Name: MAPChlCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For  RAM
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlErrStk_Bit_Start                                              4
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlErrStk_Bit_End                                                4
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlErrStk_Mask                                               cBit4
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlErrStk_Shift                                                  4
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlErrStk_MaxVal                                               0x1
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlErrStk_MinVal                                               0x0
#define cAf6_RAM_Parity_Error_Sticky_MAPChlCtrlErrStk_RstVal                                               0x0

/*--------------------------------------
BitField Name: DeMapCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For Thalassa Demap Channel Control RAM
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_DeMapCtrlErrStk_Bit_Start                                               3
#define cAf6_RAM_Parity_Error_Sticky_DeMapCtrlErrStk_Bit_End                                                 3
#define cAf6_RAM_Parity_Error_Sticky_DeMapCtrlErrStk_Mask                                                cBit3
#define cAf6_RAM_Parity_Error_Sticky_DeMapCtrlErrStk_Shift                                                   3
#define cAf6_RAM_Parity_Error_Sticky_DeMapCtrlErrStk_MaxVal                                                0x1
#define cAf6_RAM_Parity_Error_Sticky_DeMapCtrlErrStk_MinVal                                                0x0
#define cAf6_RAM_Parity_Error_Sticky_DeMapCtrlErrStk_RstVal                                                0x0

/*--------------------------------------
BitField Name: PDHDE1TxSigCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For DS1/E1/J1 Tx Framer Signaling Insertion
Control RAM
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxSigCtrlErrStk_Bit_Start                                         2
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxSigCtrlErrStk_Bit_End                                           2
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxSigCtrlErrStk_Mask                                          cBit2
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxSigCtrlErrStk_Shift                                             2
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxSigCtrlErrStk_MaxVal                                          0x1
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxSigCtrlErrStk_MinVal                                          0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxSigCtrlErrStk_RstVal                                          0x0

/*--------------------------------------
BitField Name: PDHDE1TxFrmCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For DS1/E1/J1 Tx Framer Control RAM
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxFrmCtrlErrStk_Bit_Start                                         1
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxFrmCtrlErrStk_Bit_End                                           1
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxFrmCtrlErrStk_Mask                                          cBit1
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxFrmCtrlErrStk_Shift                                             1
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxFrmCtrlErrStk_MaxVal                                          0x1
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxFrmCtrlErrStk_MinVal                                          0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1TxFrmCtrlErrStk_RstVal                                          0x0

/*--------------------------------------
BitField Name: PDHDE1RxFrmCtrlErrStk
BitField Type: RW
BitField Desc: Disable parity For DS1/E1/J1 Rx Framer Control RAM
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1RxFrmCtrlErrStk_Bit_Start                                         0
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1RxFrmCtrlErrStk_Bit_End                                           0
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1RxFrmCtrlErrStk_Mask                                          cBit0
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1RxFrmCtrlErrStk_Shift                                             0
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1RxFrmCtrlErrStk_MaxVal                                          0x1
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1RxFrmCtrlErrStk_MinVal                                          0x0
#define cAf6_RAM_Parity_Error_Sticky_PDHDE1RxFrmCtrlErrStk_RstVal                                          0x0

/*------------------------------------------------------------------------------
Reg Name   : Temperate Event
Reg Addr   : 0x00_00D0
Reg Formula: 
    Where  : 
Reg Desc   : 
This register report state change of Temperate status.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_temp_Base                                                                         0x0000D0
#define cAf6Reg_alm_temp                                                                              0x0000D0
#define cAf6Reg_alm_temp_WidthVal                                                                           32
#define cAf6Reg_alm_temp_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: Temp_alarm
BitField Type: WC
BitField Desc: Temperate change Event  1: Changed  0: Normal
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_alm_temp_Temp_alarm_Bit_Start                                                                   0
#define cAf6_alm_temp_Temp_alarm_Bit_End                                                                     0
#define cAf6_alm_temp_Temp_alarm_Mask                                                                    cBit0
#define cAf6_alm_temp_Temp_alarm_Shift                                                                       0
#define cAf6_alm_temp_Temp_alarm_MaxVal                                                                    0x1
#define cAf6_alm_temp_Temp_alarm_MinVal                                                                    0x0
#define cAf6_alm_temp_Temp_alarm_RstVal                                                                    0x0

/* ETH Port Register */
#define cEthPortQSGMIIXFIControlReg             0xF00040
#define cEthPortQSGMIISerDesIdSelectMask        cBit7_6
#define cEthPortQSGMIISerDesIdSelectShift       6
#define cEthPortQSGMIIDiagEnableMask            cBit12
#define cEthPortQSGMIIDiagEnableShift           12
#define cEthPortXFISerDesIdSelectMask           cBit9_8
#define cEthPortXFISerDesIdSelectShift          8
#define cEthPortXFIDiagEnableMask               cBit13
#define cEthPortXFIDiagEnableShift              13

/* Device PLL */
#define cDeviceStatusRegisters      0xf00060
#define cDeviceStickyRegisters      0xf00050
#define cDeviceSysPllLocked         cBit0
#define cDeviceSys25PllLocked       cBit4
#define cDeviceGePllLocked          cBit8
#define cDeviceXfiPll1Locked        cBit9
#define cDeviceXfiPll2Locked        cBit10
#define cDeviceInitCalibComplete    cBit12
#define cDevicePllStickyMask (cDeviceSysPllLocked|cDeviceSys25PllLocked|cDeviceGePllLocked|cDeviceInitCalibComplete)

#define cDeviceP1_resetdone   cBit27
#define cDeviceP2_resetdone   cBit26
#define cDeviceP3_resetdone   cBit25
#define cDeviceP4_resetdone   cBit24
#define cDeviceGmii1_rst_out  cBit22
#define cDeviceXgmii2_rst_out cBit21
#define cDeviceXgmii_rst_out  cBit20
#define cDeviceResetdone_out  cBit16

/* Debug sticky */
#define cDebugSticky      0x50000D
#define cDebugOlostsop    cBit24
#define cDebugOlosteop    cBit23
#define cDebugOmbiterr    cBit22
#define cDebugWrca_emp    cBit21
#define cDebugWrca_err    cBit20
#define cDebugBlkemp      cBit19
#define cDebugSameblk     cBit18
#define cDebugEncfffull   cBit17
#define cDebugEncffempt   cBit16
#define cDebugPlaca_emp   cBit15
#define cDebugPlaca_same  cBit14
#define cDebugPlablk_emp  cBit13
#define cDebugPlablk_same cBit12
#define cDebugPweca_err   cBit11
#define cDebugPweca_same  cBit10
#define cDebugPlaca_werr  cBit9
#define cDebugPlapk_werr  cBit8
#define cDebugCmdwrfull   cBit5
#define cDebugAvl_bbgerr  cBit4
#define cDebugAvl_enderr  cBit3
#define cDebugAvl_rvlerr  cBit2
#define cDebugAvl_rdfull  cBit1
#define cDebugVldfull     cBit0

#endif /* _AF6_REG_AF6CCI0021_RD_GLB_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha6021DebugPrint.c
 *
 * Created Date: Sep 26, 2015
 *
 * Description : Print debug information
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6021DebugPrint.h"
#include "AtStd.h"
#include "atclib.h"
#include "commacro.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static eBool needNewLine = cAtFalse;
static eBool needTwoCollumns = cAtTrue;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void PrintAlignment(void)
    {
    if ((needNewLine) || (needTwoCollumns == cAtFalse))
        AtPrintc(cSevNormal, "\r\n");
    else
        AtPrintc(cSevWarning, "|");
    }

static void PrintBitWithLabelAndColor(const char* title,
                                      uint32 hwValue, uint32 mask,
                                      const char *setString, const char *clearString,
                                      eAtSevLevel setColor, eAtSevLevel clearColor)
    {
    AtPrintc(cSevNormal, "    %-50s: ", title);
    if (hwValue & mask)
        AtPrintc(setColor, "%-10s", setString);
    else
        AtPrintc(clearColor, "%-10s", clearString);

    PrintAlignment();
    AtStdFlush();
    needNewLine = (needNewLine == cAtTrue) ? cAtFalse : cAtTrue;
    }

void Tha6021DebugPrintRegName(AtDebugger debugger, const char* title, uint32 address, uint32 value)
    {
    if (debugger)
        {
        uint32 bufferSize;
        char *buffer = AtDebuggerCharBuffer(debugger, &bufferSize);
        AtDebuggerEntryAdd(debugger, AtDebugEntryStringFormatNew(value ? cSevWarning : cSevNormal,
                           buffer, bufferSize, title, "(0x%08x = 0x%08x)", address, value));
        return;
        }

    Tha6021DebugPrintStart(debugger);
    AtPrintc(cSevInfo, "\r\n%s: (0x%08x = 0x%08x)\r\n", title, address, value);
    return;
    }

void Tha6021DebugPrintErrorBit(AtDebugger debugger, const char* title, uint32 hwValue, uint32 mask)
    {
    if (debugger)
        {
        AtDebuggerEntryAdd(debugger, AtDebugEntryErrorBitNew(title, hwValue, mask));
        return;
        }
    Tha6021DebugPrintErrorBitWithLabel(title, hwValue, mask, "SET", "CLEAR");
    }

void Tha6021DebugPrintWarningBit(AtDebugger debugger, const char* title, uint32 hwValue, uint32 mask)
    {
    if (debugger)
        {
        AtDebuggerEntryAdd(debugger, AtDebugEntryWarningBitNew(title, hwValue, mask));
        return;
        }
    PrintBitWithLabelAndColor(title, hwValue, mask, "SET", "CLEAR", cSevWarning, cSevNormal);
    }

void Tha6021DebugPrintInfoBit(AtDebugger debugger, const char* title, uint32 hwValue, uint32 mask)
    {
    if (debugger)
        {
        AtDebuggerEntryAdd(debugger, AtDebugEntryInfoBitNew(title, hwValue, mask));
        return;
        }

    AtPrintc(cSevNormal, "    %-50s: ", title);
    if (hwValue & mask)
        AtPrintc(cSevDebug, "%-10s", "OK");
    else
        AtPrintc(cSevNormal, "%-10s", "CLEAR");

    PrintAlignment();
    AtStdFlush();
    needNewLine = (needNewLine == cAtTrue) ? cAtFalse : cAtTrue;
    }

void Tha6021DebugPrintOkBit(const char* title, uint32 hwValue, uint32 mask)
    {
    Tha6021DebugPrintOkBitWithLabel(title, hwValue, mask, "OK", "CLEAR");
    }

void Tha6021DebugPrintBitFieldVal(AtDebugger debugger, const char* title, uint32 hwValue, uint32 valMask, uint32 valShift)
    {
    uint32 regFieldVal = mRegField(hwValue, val);

    if (debugger)
        {
        uint32 bufferSize;
        char *buffer = AtDebuggerCharBuffer(debugger, &bufferSize);
        AtDebuggerEntryAdd(debugger, AtDebugEntryStringFormatNew(cSevNormal, buffer, bufferSize, title, "0x%-8x", regFieldVal));
        return;
        }

    AtPrintc(cSevNormal, "    %-50s: ", title);
    AtPrintc(cSevNormal, "0x%-8x", regFieldVal);

    PrintAlignment();
    needNewLine = (needNewLine == cAtTrue) ? cAtFalse : cAtTrue;
    }

void Tha6021DebugPrintNeedTwoCollumnsEnable(eBool enable)
    {
    needTwoCollumns = enable;
    }

eBool Tha6021DebugPrintTwoCollumnsNeeded(void)
    {
    return needTwoCollumns;
    }

void Tha6021DebugPrintStart(AtDebugger debugger)
    {
    if (debugger)
        return;
    if (needNewLine == cAtTrue)
        {
        AtPrintc(cSevNormal, "\r\n");
        needNewLine = cAtFalse;
        }
    }

void Tha6021DebugPrintStop(AtDebugger debugger)
    {
    if (debugger)
        return;
    if (needNewLine == cAtTrue)
        AtPrintc(cSevNormal, "\r\n");
    }

void Tha6021DebugPrintErrorBitWithLabel(const char* title, uint32 hwValue, uint32 mask, const char *setString, const char *clearString)
    {
    PrintBitWithLabelAndColor(title, hwValue, mask, setString, clearString, cSevCritical, cSevInfo);
    }

void Tha6021DebugPrintOkBitWithLabel(const char* title, uint32 hwValue, uint32 mask, const char *setString, const char *clearString)
    {
    AtPrintc(cSevNormal, "    %-50s: ", title);
    if (hwValue & mask)
        AtPrintc(cSevInfo, "%-10s", setString);
    else
        AtPrintc(cSevCritical, "%-10s", clearString);

    PrintAlignment();
    AtStdFlush();
    needNewLine = (needNewLine == cAtTrue) ? cAtFalse : cAtTrue;
    }

void Tha6021DebugPrintBitWithLabelAndColor(const char* title,
                                           uint32 hwValue, uint32 mask,
                                           const char *setString, const char *clearString,
                                           eAtSevLevel setColor, eAtSevLevel clearColor)
    {
    PrintBitWithLabelAndColor(title, hwValue, mask, setString, clearString, setColor, clearColor);
    }

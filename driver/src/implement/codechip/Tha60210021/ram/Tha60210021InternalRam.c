/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210021InternalRam.c
 *
 * Created Date: Dec 15, 2015
 *
 * Description : Internal RAM implement for product 60210021.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ram/ThaInternalRamInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60210021InternalRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021InternalRam
    {
    tThaInternalRam super;
    }tTha60210021InternalRam;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tAtInternalRamMethods  m_AtInternalRamOverride;
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ParityMonitorIsSupported(AtInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet(self);
    return (localId < 24) ? cAtTrue : cAtFalse;
    }

static eBool CrcMonitorIsSupported(AtInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet(self);
    return (localId == 24) ? cAtTrue : cAtFalse;
    }

static uint32 ErrorStickyRegister(ThaInternalRam self)
    {
    AtUnused(self);
    return 0x112;
    }

static uint32 ForcableErrorsGet(AtInternalRam self)
    {
    uint32 localId;

    if (AtInternalRamIsReserved(self))
        return 0;

    localId = AtInternalRamLocalIdGet(self);
    if (localId < 24)
        return cAtRamAlarmParityError;

    if (localId == 24)
        return cAtRamAlarmCrcError;

    return 0;
    }

static eBool CounterTypeIsSupported(AtInternalRam self, uint16 counterType)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);

    if (!ThaModuleRamCrcCounterIsSupported((ThaModuleRam) module))
        return cAtFalse;

    if (AtInternalRamLocalIdGet(self) != 24)
        return cAtFalse;

    return (counterType == cAtRamCounterTypeCrcErrors) ? cAtTrue : cAtFalse;
    }

static uint32 counterTypeRegister(AtInternalRam self,
                                  uint16 counterType,
                                  eBool r2c)
    {
    AtUnused(self);
    AtUnused(counterType);
    AtUnused(self);
    return (r2c) ? 0x0320008 : 0x0320009;
    }

static uint32 HwCounterGet(AtInternalRam self, uint16 counterType, eBool r2c)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = counterTypeRegister(self, counterType, r2c);
    return mModuleHwRead(module, regAddr);
    }

static uint32 CounterGet(AtInternalRam self, uint16 counterType)
    {
    if (!AtInternalRamCounterTypeIsSupported(self, counterType))
        return 0x0;

    return HwCounterGet(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtInternalRam self, uint16 counterType)
    {
    if (!AtInternalRamCounterTypeIsSupported(self, counterType))
        return 0x0;

    return HwCounterGet(self, counterType, cAtTrue);
    }

static uint32 ErrorMask(ThaInternalRam self)
    {
    return (AtInternalRamLocalIdGet((AtInternalRam)self) == 24) ? cAtRamAlarmCrcError : cAtRamAlarmParityError;
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        /* Common */
        mMethodOverride(m_AtInternalRamOverride, ParityMonitorIsSupported);
        mMethodOverride(m_AtInternalRamOverride, CrcMonitorIsSupported);
        mMethodOverride(m_AtInternalRamOverride, ForcableErrorsGet);
        mMethodOverride(m_AtInternalRamOverride, CounterTypeIsSupported);
        mMethodOverride(m_AtInternalRamOverride, CounterGet);
        mMethodOverride(m_AtInternalRamOverride, CounterClear);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorStickyRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorMask);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    OverrideAtInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021InternalRam);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210021InternalRamNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

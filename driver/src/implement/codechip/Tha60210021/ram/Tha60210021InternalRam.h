/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210021InternalRam.h
 * 
 * Created Date: Dec 15, 2015
 *
 * Description : Internal RAM interface for product 60210021.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021INTERNALRAM_H_
#define _THA60210021INTERNALRAM_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInternalRam Tha60210021InternalRamNew(AtModule phyModule, uint32 ramId, uint32 localId);
AtInternalRam Tha60210021InternalRamEccNew(AtModule phyModule, uint32 ramId, uint32 localId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021INTERNALRAM_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210021InternalRamEcc.c
 *
 * Created Date: Mar 28, 2016
 *
 * Description : 60210021 ECC RAM methods implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/ram/ThaInternalRamInternal.h"
#include "Tha60210021InternalRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021InternalRamEcc
    {
    tThaInternalRamEcc super;
    }tTha60210021InternalRamEcc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaInternalRamEccMethods m_ThaInternalRamEccOverride;
static tThaInternalRamMethods    m_ThaInternalRamOverride;
static tAtInternalRamMethods     m_AtInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DisableCheckRegister(ThaInternalRam self)
    {
    AtUnused(self);
    return 0x108;
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    AtUnused(self);
    return 0x107;
    }

static uint32 ErrorStickyRegister(ThaInternalRam self)
    {
    AtUnused(self);
    return 0x112;
    }

static uint32 CorrectableMask(ThaInternalRamEcc self)
    {
    AtUnused(self);
    return cBit25;
    }

static uint32 UncorrectableMask(ThaInternalRamEcc self)
    {
    AtUnused(self);
    return cBit26;
    }

static eBool CounterTypeIsSupported(AtInternalRam self, uint16 counterType)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    
    if (!ThaModuleRamEccCounterIsSupported((ThaModuleRam) module))
        return cAtFalse;

    switch (counterType)
        {
        case cAtRamCounterTypeEccCorrectableErrors: return cAtTrue;
        case cAtRamCounterTypeEccUncorrectableErrors: return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 CounterTypeRegister(AtInternalRam self,
                                  uint16 counterType,
                                  eBool r2c)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtRamCounterTypeEccCorrectableErrors:   return (r2c) ? 0x248008 : 0x248009;
        case cAtRamCounterTypeEccUncorrectableErrors: return (r2c) ? 0x24800A : 0x24800B;
        default: return cBit31_0;
        }
    }

static uint32 HwCounterGet(AtInternalRam self, uint16 counterType, eBool r2c)
    {
    AtModule module = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 regAddr = CounterTypeRegister(self, counterType, r2c);
    return mModuleHwRead(module, regAddr);
    }

static uint32 CounterGet(AtInternalRam self, uint16 counterType)
    {
    if (AtInternalRamCounterTypeIsSupported(self, counterType))
        return HwCounterGet(self, counterType, cAtFalse);
    return 0x0;
    }

static uint32 CounterClear(AtInternalRam self, uint16 counterType)
    {
    if (AtInternalRamCounterTypeIsSupported(self, counterType))
        return HwCounterGet(self, counterType, cAtTrue);
    return 0x0;
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        /* Common */
        mMethodOverride(m_ThaInternalRamOverride, DisableCheckRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, ErrorStickyRegister);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void OverrideThaInternalRamEcc(AtInternalRam self)
    {
    ThaInternalRamEcc ram = (ThaInternalRamEcc)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamEccOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamEccOverride));

        /* Common */
        mMethodOverride(m_ThaInternalRamEccOverride, CorrectableMask);
        mMethodOverride(m_ThaInternalRamEccOverride, UncorrectableMask);
        }

    mMethodsSet(ram, &m_ThaInternalRamEccOverride);
    }

static void OverrideAtInternalRam(AtInternalRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInternalRamOverride, mMethodsGet(self), sizeof(m_AtInternalRamOverride));

        mMethodOverride(m_AtInternalRamOverride, CounterTypeIsSupported);
        mMethodOverride(m_AtInternalRamOverride, CounterGet);
        mMethodOverride(m_AtInternalRamOverride, CounterClear);
        }

    mMethodsSet(self, &m_AtInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRamEcc(self);
    OverrideThaInternalRam(self);
    OverrideAtInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021InternalRamEcc);
    }

static AtInternalRam ObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamEccObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60210021InternalRamEccNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return ObjectInit(newRam, phyModule, ramId, localId);
    }

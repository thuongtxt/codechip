/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210021ModuleRam.c
 *
 * Created Date: Jun 19, 2013
 *
 * Description : RAM module of 60210021 product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/ram/ThaInternalRam.h"
#include "../../Tha60210031/ram/Tha60210031ModuleRamInternal.h"
#include "Tha60210021InternalRam.h"
#include "diag/Tha60210021RamErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60210021ModuleRam)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021ModuleRam *Tha60210021ModuleRam;
typedef struct tTha60210021ModuleRam
    {
    tTha60210031ModuleRam super;
    }tTha60210021ModuleRam;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods         m_AtModuleOverride;
static tAtModuleRamMethods      m_AtModuleRamOverride;
static tThaModuleRamMethods     m_ThaModuleRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    if (localRamId < 25)
        return Tha60210021InternalRamNew(self, ramId, localRamId);
    return Tha60210021InternalRamEccNew(self, ramId, localRamId);
    }

static AtErrorGenerator ErrorGeneratorObjectCreate(ThaModuleRam self, AtInternalRam ram)
    {
    uint32 localRamId = AtInternalRamLocalIdGet(ram);
    
    if (localRamId == 24) /* DDR CRC error */
        return Tha60210021RamCrcErrorGeneratorNew((AtModule)self);

    if (localRamId == 25)
        return Tha60210021RamEccErrorGeneratorNew((AtModule)self);

    /*Default is not supported */
    return NULL;
    }

static uint8 NumDdrGet(AtModuleRam self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 DdrAddressBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 23;
    }

static uint32 DdrDataBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 32;
    }

static eBool BurstTestIsSupported(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedClearWholeMemoryBeforeTesting(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 DdrCalibStatusMask(ThaModuleRam self, uint8 ddrId)
    {
    /* There is only 1 DDR */
    AtUnused(self);
    AtUnused(ddrId);
    return cBit12;
    }

static uint32 StartVersionSupportInternalRamMonitoring(AtDevice device)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x02, 0x37);
    }

static eBool InternalRamMonitoringIsSupported(ThaModuleRam self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= StartVersionSupportInternalRamMonitoring(device))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 InterruptStatusGet(ThaModuleRam self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static eBool PhyModuleHasRamInterrupt(ThaModuleRam self, uint32 moduleId, uint32 ramIntr)
    {
    AtUnused(self);
    AtUnused(ramIntr);
    if (moduleId == cAtModuleRam)
        return cAtTrue;
    return cAtFalse;
    }

static const char ** AllRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] = {
        "PDH DS1/E1/J1 Rx Framer Control RAM",
        "PDH DS1/E1/J1 Tx Framer Control RAM",
        "PDH DS1/E1/J1 Tx Framer Signaling Insertion Control RAM",
        "Demap Channel Control RAM",
        "Map Channel Control RAM",
        "Map Timing Reference Control RAM",
        "CDR Engine Timing control RAM",
        "PLA PDHPW Payload Assemble Payload Size Control RAM",
        "PDA Pseudowire Reorder Control RAM",
        "PDA Pseudowire Jitter Buffer Control RAM",
        "PDA Pseudowire TDM Mode Control RAM",
        "PWE Pseudowire Transmit Ethernet Header Value Control RAM",
        "PWE Pseudowire Transmit Ethernet Header Length Control RAM",
        "PWE Pseudowire Transmit Header RTP SSRC Value Control RAM",
        "PWE Pseudowire Transmit HSPW Label Control RAM",
        "PWE Pseudowire Transmit HSPW Group Enable Control RAM",
        "PWE Pseudowire Transmit UPSR Group Enable Control RAM",
        "PWE Pseudowire Transmit HSPW Group Protection Control RAM",
        "CLA Classify VLAN TAG Lookup Control RAM ",
        "CLA Classify HBCE Hashing Table page0 Control RAM ",
        "CLA Classify HBCE Hashing Table page1 Control RAM ",
        "CLA Classify HBCE Looking Up Information Control RAM ",
        "CLA Classify Pseudowire Type Control RAM ",
        "CLA Classify Per Group Enable Control RAM ",
        "DDR-CRC",
        "DDR-ECC" /*26: ECC Internal Ram */
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    return AllRamsDescription(self, numRams);
    }

static uint32 NumInternalRamsGet(AtModule self)
    {
    uint32 numRams;
    (void)AllRamsDescription(self, &numRams);
    return numRams;
    }

static uint32 PhyModuleRamLocalCellAddress(ThaModuleRam self, uint32 moduleId, uint32 localRamId)
    {
    static uint32 cellAddress[] = {
        /* PDH */
        0x754000,
        0x794000,
        0x794800,

        /* MAP */
        0x1A0000,
        0x1A1000,
        0x1A0800,

        /* CDR */
        0x280200,

        /* PLA */
        0x2c2000,

        /* PDA */
        0x244000,
        0x243100,
        0x245000,

        /* PWE */
        0x300000,
        0x301000,
        0x303000,
        0x305000,
        0x306000,
        0x307000,
        0x308000,

        /* CLA */
        0x44C000,
        0x448800,
        0x448C00,
        0x449000,
        0x449C00,
        0x44A000,
        0x0
        };
    AtUnused(moduleId);
    AtUnused(self);
    if (localRamId >= mCount(cellAddress))
        return cInvalidUint32;

    return cellAddress[localRamId];
    }

static void OverrideThaModuleRam(AtModuleRam self)
    {
    ThaModuleRam ramModule = (ThaModuleRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleRamOverride, mMethodsGet(ramModule), sizeof(m_ThaModuleRamOverride));

        mMethodOverride(m_ThaModuleRamOverride, BurstTestIsSupported);
        mMethodOverride(m_ThaModuleRamOverride, NeedClearWholeMemoryBeforeTesting);
        mMethodOverride(m_ThaModuleRamOverride, DdrCalibStatusMask);
        mMethodOverride(m_ThaModuleRamOverride, InternalRamMonitoringIsSupported);
        mMethodOverride(m_ThaModuleRamOverride, InterruptStatusGet);
        mMethodOverride(m_ThaModuleRamOverride, PhyModuleHasRamInterrupt);
        mMethodOverride(m_ThaModuleRamOverride, ErrorGeneratorObjectCreate);
        mMethodOverride(m_ThaModuleRamOverride, PhyModuleRamLocalCellAddress);
        }

    mMethodsSet(ramModule, &m_ThaModuleRamOverride);
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, mMethodsGet(self), sizeof(m_AtModuleRamOverride));

        mMethodOverride(m_AtModuleRamOverride, NumDdrGet);
        mMethodOverride(m_AtModuleRamOverride, DdrAddressBusSizeGet);
        mMethodOverride(m_AtModuleRamOverride, DdrDataBusSizeGet);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void OverrideAtModule(AtModuleRam self)
    {
    AtModule ramModule = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(ramModule), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, NumInternalRamsGet);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        }

    mMethodsSet(ramModule, &m_AtModuleOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModule(self);
    OverrideAtModuleRam(self);
    OverrideThaModuleRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModuleRam);
    }

static AtModuleRam ObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleRamObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam Tha60210021ModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60210021RamCrcErrorGenerator.c
 *
 * Created Date: Mar 28, 2016
 *
 * Description : Implementation of the Tha60210021CrcRamErrorGenerator, a concrete class of the
 *               AtErrorGenerator.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210021RamErrorGeneratorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60210021RamEccErrorGenerator)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tTha60210031RamCrcErrorGeneratorMethods m_Tha60210031RamCrcErrorGeneratorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ForceErrorRateCtrlRegister(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0x321201;
    }

static uint32 ErrorThresholdInsertCtrlRegister(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0x321201;
    }

static void OverrideTha60210031RamCrcErrorGenerator(AtErrorGenerator self)
    {
    Tha60210031RamCrcErrorGenerator ramCrcErrGen = (Tha60210031RamCrcErrorGenerator)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_Tha60210031RamCrcErrorGeneratorOverride,
                                  mMethodsGet(ramCrcErrGen),
                                  sizeof(m_Tha60210031RamCrcErrorGeneratorOverride));

        /* Setup methods */
        mMethodOverride(m_Tha60210031RamCrcErrorGeneratorOverride, ForceErrorRateCtrlRegister);
        mMethodOverride(m_Tha60210031RamCrcErrorGeneratorOverride, ErrorThresholdInsertCtrlRegister);
        }

    mMethodsSet(ramCrcErrGen, &m_Tha60210031RamCrcErrorGeneratorOverride);
    }

static void Override(AtErrorGenerator self)
    {
    OverrideTha60210031RamCrcErrorGenerator(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021RamEccErrorGenerator);
    }

AtErrorGenerator Tha60210021RamCrcErrorGeneratorObjectInit(AtErrorGenerator self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031RamCrcErrorGeneratorObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtErrorGenerator Tha60210021RamCrcErrorGeneratorNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtErrorGenerator errorGenerator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (errorGenerator == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021RamCrcErrorGeneratorObjectInit(errorGenerator, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210021RamErrorGenerator.h
 * 
 * Created Date: Mar 28, 2016
 *
 * Description : Interface of the Tha60210021RamErrorGenerator, a concrete class of the
 *               AtErrorGenerator.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021RAMERRORGENERATOR_H_
#define _THA60210021RAMERRORGENERATOR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtErrorGenerator Tha60210021RamCrcErrorGeneratorNew(AtModule module);
AtErrorGenerator Tha60210021RamEccErrorGeneratorNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021RAMERRORGENERATOR_H_ */


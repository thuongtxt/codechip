/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60210021RamErrorGeneratorInternal.h
 * 
 * Created Date: Mar 28, 2016
 *
 * Description : Methods and data of the Tha60210021RamErrorGenerator, a concrete class of the
 *               AtErrorGenerator.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021RAMERRORGENERATORINTERNAL_H_
#define _THA60210021RAMERRORGENERATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210031/ram/diag/Tha60210031RamErrorGeneratorInternal.h"
#include "Tha60210021RamErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210021RamEccErrorGenerator *Tha60210021RamEccErrorGenerator;

typedef struct tTha60210021RamEccErrorGenerator
    {
    tTha60210031RamTxDdrEccErrorGenerator super;
    } tTha60210021RamEccErrorGenerator;

typedef struct tTha60210021RamCrcErrorGenerator
    {
    tTha60210031RamCrcErrorGenerator super;
    } tTha60210021RamCrcErrorGenerator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtErrorGenerator Tha60210021RamCrcErrorGeneratorObjectInit(AtErrorGenerator self, AtModule module);
AtErrorGenerator Tha60210021RamEccErrorGeneratorObjectInit(AtErrorGenerator self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021RAMERRORGENERATORINTERNAL_H_ */


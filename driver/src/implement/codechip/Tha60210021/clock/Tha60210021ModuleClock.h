/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60210021ModuleClock.h
 * 
 * Created Date: Aug, 2015
 *
 * Description : Clock module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021MODULECLOCK_H_
#define _THA60210021MODULECLOCK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaPdhPwProduct/clock/ThaPdhPwProductModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
AtModuleClock Tha60210021ModuleClockNew(AtDevice device);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021MODULECLOCK_H_ */


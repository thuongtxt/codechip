/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60210021ModuleClockInternal.h
 * 
 * Created Date: Aug, 2015
 *
 * Description : Clock module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021MODULECLOCKINTERNAL_H_
#define _THA60210021MODULECLOCKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaPdhPwProduct/clock/ThaPdhPwProductModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210021ModuleClock
    {
    tThaPdhPwProductModuleClock super;
    }tTha60210021ModuleClock;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021MODULECLOCKINTERNAL_H_ */


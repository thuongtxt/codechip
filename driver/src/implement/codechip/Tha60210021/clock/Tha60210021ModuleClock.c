/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : ThaPdhPwProductModuleClock.c
 *
 * Created Date: Sep 10, 2013
 *
 * Description : Clock module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210021ModuleClockInternal.h"
#include "Tha60210021ModuleClock.h"

/*--------------------------- Define -----------------------------------------*/
#define cCdrRefIdMask(extractorId)       (((extractorId) == 0) ? cBit9_4 : cBit25_20)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleClockMethods  m_AtModuleClockOverride;
static tThaModuleClockMethods m_ThaModuleClockOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModuleClock);
    }

static uint8 NumExtractors(AtModuleClock self)
    {
	AtUnused(self);
    return 2;
    }

static uint32 RefIdMask(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 extractorId = AtClockExtractorIdGet(extractor);
    AtUnused(self);
    return cCdrRefIdMask(extractorId);
    }

static eBool ShouldEnableCdrInterrupt(ThaModuleClock self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 SquelchingHwAddress(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return 0x2A0000;
    }

static uint32 SquelchingHwMask(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 id = AtClockExtractorIdGet(extractor);

    AtUnused(self);

    if (id == 0) return cBit13_11;
    if (id == 1) return cBit29_27;

    return 0;
    }

static uint32 SquelchingHwShift(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 id = AtClockExtractorIdGet(extractor);

    AtUnused(self);

    if (id == 0) return 11;
    if (id == 1) return 27;

    return 0;
    }

static uint32 OutputCounterHwAddress(ThaModuleClock self, AtClockExtractor extractor)
    {
    AtUnused(self);
    AtUnused(extractor);
    return 0xF0006A;
    }

static uint32 OutputCounterHwMask(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 id = AtClockExtractorIdGet(extractor);
    AtUnused(self);
    return (cBit15_0 << (16 * id));
    }

static uint32 OutputCounterHwShift(ThaModuleClock self, AtClockExtractor extractor)
    {
    uint32 id = AtClockExtractorIdGet(extractor);
    AtUnused(self);
    return (16 * id);
    }

static eBool SquelchingIsSupported(ThaModuleClock self)
    {
    AtModule module = (AtModule)self;
    AtDevice device = AtModuleDeviceGet(module);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 startVersionSupportClockSquel = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x6, 0x4641);

    if (ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader) >= startVersionSupportClockSquel)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtModuleClock(AtModuleClock self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleClockOverride, mMethodsGet(self), sizeof(m_AtModuleClockOverride));

        mMethodOverride(m_AtModuleClockOverride, NumExtractors);
        }

    mMethodsSet(self, &m_AtModuleClockOverride);
    }

static void OverrideThaModuleClock(AtModuleClock self)
    {
    ThaModuleClock clockModule = (ThaModuleClock)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClockOverride, mMethodsGet(clockModule), sizeof(m_ThaModuleClockOverride));

        mMethodOverride(m_ThaModuleClockOverride, RefIdMask);
        mMethodOverride(m_ThaModuleClockOverride, ShouldEnableCdrInterrupt);
        mMethodOverride(m_ThaModuleClockOverride, SquelchingHwAddress);
        mMethodOverride(m_ThaModuleClockOverride, SquelchingHwMask);
        mMethodOverride(m_ThaModuleClockOverride, SquelchingHwShift);
        mMethodOverride(m_ThaModuleClockOverride, OutputCounterHwAddress);
        mMethodOverride(m_ThaModuleClockOverride, OutputCounterHwMask);
        mMethodOverride(m_ThaModuleClockOverride, OutputCounterHwShift);
        mMethodOverride(m_ThaModuleClockOverride, SquelchingIsSupported);
        }

    mMethodsSet(clockModule, &m_ThaModuleClockOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideAtModuleClock(self);
    OverrideThaModuleClock(self);
    }

static AtModuleClock Tha60210021ModuleClockObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModuleClockObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleClock Tha60210021ModuleClockNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleClock newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021ModuleClockObjectInit(newModule, device);
    }

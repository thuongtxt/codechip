/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60210021ModuleBer.h
 * 
 * Created Date: Aug 5, 2015
 *
 * Description : Module BER of 60210021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021MODULEBER_H_
#define _THA60210021MODULEBER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/ber/Tha60210011ModuleBer.h"


/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleBer Tha60210021ModuleBerNew(AtDevice device);
AtBerController Tha60210021PdhDe1PathBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60210021PdhDe1LineBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021MODULEBER_H_ */


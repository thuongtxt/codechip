/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210021PdhDe1LineBerController.c
 *
 * Created Date: Aug 5, 2015
 *
 * Description : DS1/E1 BER controller implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../pdh/Tha60210021ModulePdh.h"
#include "../pdh/Tha60210021ModulePdhReg.h"
#include "Tha60210021ModuleBer.h"
#include "Tha60210021PdhDe1Ber.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerControllerMethods        m_AtBerControllerOverride;

/* Cache super */
static const tAtBerControllerMethods  *m_AtBerControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Enable(AtBerController self, eBool enable)
    {
    eAtRet ret = m_AtBerControllerMethods->Enable(self, enable);
    if (ret != cAtOk)
        return ret;

    if (enable)
        Tha60210021PdhDe1BerLineModeSet((Tha60210011SdhAuVcBerController)self);

    return cAtOk;
    }

static eBool IsEnabled(AtBerController self)
    {
    if (Tha60210021PdhDe1BerIsPathMode((Tha60210011SdhAuVcBerController)self))
        return cAtFalse;

    return Tha60210021PdhDe1BerIsEnabled((Tha60210011SdhAuVcBerController)self);
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtBerControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, m_AtBerControllerMethods, sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, Enable);
        mMethodOverride(m_AtBerControllerOverride, IsEnabled);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PdhDe1LineBerController);
    }

static AtBerController ObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210021PdhDe1PathBerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210021PdhDe1LineBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, controllerId, channel, berModule);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210021PdhDe1PathBerController.c
 *
 * Created Date: Aug 5, 2015
 *
 * Description : DS1/E1 PATH BER controller implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../pdh/Tha60210021ModulePdh.h"
#include "../pdh/Tha60210021ModulePdhReg.h"
#include "Tha60210021ModuleBer.h"
#include "Tha60210021PdhDe1Ber.h"

/*--------------------------- Define -----------------------------------------*/
/*
 * Select ber-mode: 1-line, 0-path
 * */
#define cAf6_bermode_Mask            cBit12
#define cAf6_bermode_Shift           12

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerControllerMethods                 m_AtBerControllerOverride;
static tTha60210011SdhAuVcBerControllerMethods m_Tha60210011SdhAuVcBerControllerOverride;
static tTha60210011SdhVc1xBerControllerMethods m_Tha60210011SdhVc1xBerControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BerInterfaceReg(Tha60210011SdhAuVcBerController self)
    {
    AtChannel de1 = (AtChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    return (AtChannelIdGet(de1)/32) + 0xFFFEUL;
    }

static uint32 BerInterfaceBitPos(Tha60210011SdhAuVcBerController self)
    {
    AtChannel de1 = (AtChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    return (AtChannelIdGet(de1)%32);
    }

static void BerInterfacePathEnable(Tha60210011SdhAuVcBerController self, eBool pathEnable)
    {
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet((AtBerController)self));
    uint32 address = BerInterfaceReg(self) + baseAddress;
    uint32 regVal = AtBerControllerRead((AtBerController)self, address);
    uint32 interfaceShift = BerInterfaceBitPos(self);
    uint32 interfaceMask = (cBit0 << interfaceShift);
    
    mRegFieldSet(regVal, interface, pathEnable ? 0 : 1);
    AtBerControllerWrite((AtBerController)self, address, regVal);
    }

static void BerPathEnable(Tha60210011SdhAuVcBerController self, eBool pathEnable)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet((AtBerController)self));
    uint32 address = cAf6Reg_imemrwpctrl1_Base + mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 berModeMask = cAf6_bermode_Mask << (vt);
    uint8 berModeShift = (uint8)(cAf6_bermode_Shift + (vt));
    
    AtBerControllerLongRead((AtBerController)self, address, regVal);
    mRegFieldSet(regVal[1], berMode, pathEnable ? 0 : 1);
    AtBerControllerLongWrite((AtBerController)self, address, regVal);
    BerInterfacePathEnable(self, pathEnable);
    }

static eBool BerPathIsEnabled(Tha60210011SdhAuVcBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet((AtBerController)self));
    uint32 address = cAf6Reg_imemrwpctrl1_Base + mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 berModeMask = cAf6_bermode_Mask << (vt);
    
    AtBerControllerLongRead((AtBerController)self, address, regVal);
    return (regVal[1] & berModeMask) ? cAtFalse : cAtTrue;
    }

static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    AtChannel de1 = (AtChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    return (AtChannelIdGet(de1) / 4UL);
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    AtChannel de1 = (AtChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    return AtChannelIdGet(de1);
    }

static uint8 ChannelId(Tha60210011SdhVc1xBerController self)
    {
    AtChannel de1 = (AtChannel)AtBerControllerMonitoredChannel((AtBerController)self);
    return AtChannelIdGet(de1) % 4UL;
    }

static uint32 BerReportRegisterAddress(Tha60210011SdhVc1xBerController self)
    {
    AtUnused(self);
    return cAf6Reg_ramberratevtds_Base;
    }

static uint32 BerControlRegisterAddress(Tha60210011SdhVc1xBerController self)
    {
    AtUnused(self);
    return cAf6Reg_imemrwpctrl1_Base;
    }

static eAtRet Enable(AtBerController self, eBool enable)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address = cAf6Reg_imemrwpctrl1_Base + mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 enFieldMask = cAf6_imemrwpctrl1_ena1_Mask << (8 * vt);
    uint32 enFieldShift = (uint8)(cAf6_imemrwpctrl1_ena1_Shift + (8 * vt));
    uint32 channelTypeMask = cAf6_imemrwpctrl1_etype1_Mask << (8 * vt);
    uint32 channelTypeShift = (uint8)(cAf6_imemrwpctrl1_etype1_Shift + (8 * vt));
    
    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[0], enField, mBoolToBin(enable));
    mRegFieldSet(regVal[0], channelType, mMethodsGet(controller)->HwChannelTypeValue(self));
    AtBerControllerLongWrite(self, address, regVal);
    
    if (enable)
        Tha60210021PdhDe1BerPathModeSet((Tha60210011SdhAuVcBerController)self);
        
    return cAtOk;
    }

static eBool IsEnabled(AtBerController self)
    {
    if (Tha60210021PdhDe1BerIsLineMode((Tha60210011SdhAuVcBerController)self))
        return cAtFalse;

    return Tha60210021PdhDe1BerIsEnabled((Tha60210011SdhAuVcBerController)self);
    }

static eAtRet TcaThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address = cAf6Reg_imemrwpctrl1_Base + mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 tcaFieldShift = (uint32)(3UL * vt);
    uint32 tcaFieldMask = cBit2_0 << tcaFieldShift;

    AtBerControllerLongRead(self, address, regVal);
    mRegFieldSet(regVal[1], tcaField, Tha60210011BerRate2HwValue(threshold));
    AtBerControllerLongWrite(self, address, regVal);

    return cAtOk;
    }

static eAtBerRate TcaThresholdGet(AtBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet(self));
    uint32 address = cAf6Reg_imemrwpctrl1_Base + mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 tcaFieldShift = (uint32)(3UL * vt);
    uint32 tcaFieldMask = cBit2_0 << tcaFieldShift;
    
    AtBerControllerLongRead(self, address, regVal);
    return Tha60210011HwValue2BerRate((uint8)(mRegField(regVal[1], tcaField)));
    }

static void OverrideAtBerController(AtBerController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, mMethodsGet(self), sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, Enable);
        mMethodOverride(m_AtBerControllerOverride, IsEnabled);
        mMethodOverride(m_AtBerControllerOverride, TcaThresholdSet);
        mMethodOverride(m_AtBerControllerOverride, TcaThresholdGet);
        }

    mMethodsSet(self, &m_AtBerControllerOverride);
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void OverrideTha60210011SdhVc1xBerController(AtBerController self)
    {
    Tha60210011SdhVc1xBerController controller = (Tha60210011SdhVc1xBerController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhVc1xBerControllerOverride, mMethodsGet(controller), sizeof(m_Tha60210011SdhVc1xBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhVc1xBerControllerOverride, ChannelId);
        mMethodOverride(m_Tha60210011SdhVc1xBerControllerOverride, BerReportRegisterAddress);
        mMethodOverride(m_Tha60210011SdhVc1xBerControllerOverride, BerControlRegisterAddress);
        }

    mMethodsSet(controller, &m_Tha60210011SdhVc1xBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerController(self);
    OverrideTha60210011SdhAuVcBerController(self);
    OverrideTha60210011SdhVc1xBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PdhDe1PathBerController);
    }

AtBerController Tha60210021PdhDe1PathBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PdhDe1BerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60210021PdhDe1PathBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021PdhDe1PathBerControllerObjectInit(newController, controllerId, channel, berModule);
    }

void Tha60210021PdhDe1BerPathModeSet(Tha60210011SdhAuVcBerController self)
    {
    BerPathEnable(self, cAtTrue);
    }

void Tha60210021PdhDe1BerLineModeSet(Tha60210011SdhAuVcBerController self)
    {
    BerPathEnable(self, cAtFalse);
    }

eBool Tha60210021PdhDe1BerIsPathMode(Tha60210011SdhAuVcBerController self)
    {
    return BerPathIsEnabled(self);
    }

eBool Tha60210021PdhDe1BerIsLineMode(Tha60210011SdhAuVcBerController self)
    {
    return BerPathIsEnabled(self) ? cAtFalse : cAtTrue;
    }

eBool Tha60210021PdhDe1BerIsEnabled(Tha60210011SdhAuVcBerController self)
    {
    Tha60210011SdhAuVcBerController controller = (Tha60210011SdhAuVcBerController)self;
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer)AtBerControllerModuleGet((AtBerController)self));
    uint32 address = cAf6Reg_imemrwpctrl1_Base + mMethodsGet(controller)->BerControlOffset(controller) + baseAddress;
    uint32 regVal[cThaLongRegMaxSize];
    uint8 vt = mMethodsGet((Tha60210011SdhVc1xBerController)self)->ChannelId((Tha60210011SdhVc1xBerController)self);
    uint32 enFieldMask = cAf6_imemrwpctrl1_ena1_Mask << (8 * vt);
    
    AtBerControllerLongRead((AtBerController)self, address, regVal);
    return (regVal[0] & enFieldMask) ? cAtTrue: cAtFalse;
    }

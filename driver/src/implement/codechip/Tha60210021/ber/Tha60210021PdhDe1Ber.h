/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60210021ModuleBer.h
 * 
 * Created Date: Aug 5, 2015
 *
 * Description : Module BER of 60210021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021DE1BER_H_
#define _THA60210021DE1BER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/ber/Tha60210011SdhAuVcBerControllerInternal.h"


/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha60210021PdhDe1PathBerController
    {
    tTha60210011PdhDe1BerController super;
    }tTha60210021PdhDe1PathBerController;

typedef struct tTha60210021PdhDe1LineBerController
    {
    tTha60210011PdhDe1BerController super;
    }tTha60210021PdhDe1LineBerController;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60210021PdhDe1BerPathModeSet(Tha60210011SdhAuVcBerController self);
void Tha60210021PdhDe1BerLineModeSet(Tha60210011SdhAuVcBerController self);
eBool Tha60210021PdhDe1BerIsPathMode(Tha60210011SdhAuVcBerController self);
eBool Tha60210021PdhDe1BerIsLineMode(Tha60210011SdhAuVcBerController self);
eBool Tha60210021PdhDe1BerIsEnabled(Tha60210011SdhAuVcBerController self);

AtBerController Tha60210021PdhDe1PathBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021DE1BER_H_ */


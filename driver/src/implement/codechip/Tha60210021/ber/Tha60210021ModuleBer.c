/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210021ModuleBer.c
 *
 * Created Date: Aug 4, 2015
 *
 * Description : Module BER of 60210021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60210011/poh/Tha60210011PohReg.h"
#include "../pdh/Tha60210021ModulePdh.h"

#include "Tha60210021ModuleBer.h"

/*--------------------------- Define -----------------------------------------*/

#define cLineDs1Threshold1Base  0x06248F
#define cLineE1Threshold1Base   0x06249F

#define cLineDs1Threshold2Rate  60
#define cLineE1Threshold2Rate   61
#define cDs1Threshold2Rate      62
#define cE1Threshold2Rate       63

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021ModuleBer
    {
    tTha60210011ModuleBer super;
    }tTha60210021ModuleBer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleBerMethods           m_AtModuleBerOverride;
static tAtModuleMethods              m_AtModuleOverride;
static tTha60210011ModuleBerMethods  m_Tha60210011ModuleBerOverride;

/* Cache super */
static const tAtModuleMethods        *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet BerEnable(Tha60210011ModuleBer self, eBool enable)
    {
    uint32 address = cAf6Reg_pcfg_glbenb_Base + AtModuleBerBaseAddress((AtModuleBer)self);
    uint32 regVal = mModuleHwRead(self, address);
    uint8 enBit = (uint8)mBoolToBin(enable);

    /* Enable ber for DSn, VT */
    mRegFieldSet(regVal, cAf6_pcfg_glbenb_timerenb_, enBit);
    mRegFieldSet(regVal, cAf6_pcfg_glbenb_vtenb_,    enBit);
    mRegFieldSet(regVal, cAf6_pcfg_glbenb_dsnsenb_,  enBit);

    mModuleHwWrite(self, address, regVal);
    return cAtOk;
    }

static void BerLineDs1ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, AtModuleBerBaseAddress((AtModuleBer)self) + cLineDs1Threshold1Base,       cLineDs1Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate),      cLineDs1Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 1,  cLineDs1Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 2,  cLineDs1Threshold2_2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 3,  cLineDs1Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 4,  cLineDs1Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 5,  cLineDs1Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 6,  cLineDs1Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 7,  cLineDs1Threshold2_7);
    }

static void BerLineE1ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, AtModuleBerBaseAddress((AtModuleBer)self) + cLineE1Threshold1Base,      cLineE1Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate),     cLineE1Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 1, cLineE1Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 2, cLineE1Threshold2_2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 3, cLineE1Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 4, cLineE1Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 5, cLineE1Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 6, cLineE1Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 7, cLineE1Threshold2_7);
    }

static void BerPathDs1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        Tha60210011ModuleBerDs1ThresholdInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cDs1Threshold1Rate),     0x1805);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate),     0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 1, 0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 2, 0x1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 3, 0x460040);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 4, 0x520042);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 5, 0x540043);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 6, 0x540043);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cDs1Threshold2Rate) + 7, 0x540043);
    }

static void BerPathE1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        Tha60210011ModuleBerE1ThresholdInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cE1Threshold1Rate),     0x380D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate),     0x40002);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 1, 0x40002);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 2, 0x3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 3, 0x64004b);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 4, 0x6C0052);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 5, 0x6E0053);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 6, 0x6E0053);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cE1Threshold2Rate) + 7, 0x6E0053);
    }

static void BerLineDs1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerLineDs1ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, AtModuleBerBaseAddress((AtModuleBer)self) + cLineDs1Threshold1Base, 0x6015);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate),     0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 1, 0x20001);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 2, 0x2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 3, 0x560044);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 4, 0x560044);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 5, 0x560044);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 6, 0x560044);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineDs1Threshold2Rate) + 7, 0x560044);
    }

static void BerLineE1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerLineE1ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, AtModuleBerBaseAddress((AtModuleBer)self) + cLineE1Threshold1Base, 0x861E);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate),    0x40002);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 1,0x40002);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 2,0x3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 3,0x5a0058);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 4,0x5a0058);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 5,0x5a0058);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 6,0x5a0058);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineE1Threshold2Rate) + 7,0x5a0058);
    }

static void DefaultSet(Tha60210011ModuleBer self)
    {
    BerPathDs1ThresholdInit(self);
    BerPathE1ThresholdInit(self);
    BerLineDs1ThresholdInit(self);
    BerLineE1ThresholdInit(self);
    }

static eAtRet Debug(AtModule self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer)self;
    Tha60210011ModuleBerIsEnabledPrint(module);
    Tha60210011ModuleBerThresholdPrint(module, "E1",  cE1Threshold1Rate , cE1Threshold2Rate );
    Tha60210011ModuleBerThresholdPrint(module, "DS1", cDs1Threshold1Rate, cDs1Threshold2Rate);

    return cAtOk;
    }

static AtBerController PdhChannelPathBerControlerCreate(AtModuleBer self, AtChannel pdhChannel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet((AtPdhChannel)pdhChannel);
    if ((channelType == cAtPdhChannelTypeE1) || (channelType == cAtPdhChannelTypeDs1))
        return Tha60210021PdhDe1PathBerControllerNew(AtChannelIdGet(pdhChannel), pdhChannel, self);
    return NULL;
    }

static AtBerController PdhChannelLineBerControlerCreate(AtModuleBer self, AtChannel pdhChannel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet((AtPdhChannel)pdhChannel);
    if ((channelType == cAtPdhChannelTypeE1) || (channelType == cAtPdhChannelTypeDs1))
        return Tha60210021PdhDe1LineBerControllerNew(AtChannelIdGet(pdhChannel), pdhChannel, self);
    return NULL;
    }

static uint32 BaseAddress(AtModuleBer self)
    {
    return ThaModulePdhBaseAddress((ThaModulePdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePdh));
    }

static void OverrideTha60210011ModuleBer(AtModuleBer self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleBerOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleBerOverride));

        mMethodOverride(m_Tha60210011ModuleBerOverride, DefaultSet);
        mMethodOverride(m_Tha60210011ModuleBerOverride, BerEnable);
        }

    mMethodsSet(module, &m_Tha60210011ModuleBerOverride);
    }

static void OverrideAtModuleBer(AtModuleBer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleBerOverride, mMethodsGet(self), sizeof(m_AtModuleBerOverride));

        mMethodOverride(m_AtModuleBerOverride, PdhChannelPathBerControlerCreate);
        mMethodOverride(m_AtModuleBerOverride, PdhChannelLineBerControlerCreate);
        mMethodOverride(m_AtModuleBerOverride, BaseAddress);
        }

    mMethodsSet(self, &m_AtModuleBerOverride);
    }

static void OverrideAtModule(AtModuleBer self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleBer self)
    {
    OverrideAtModuleBer(self);
    OverrideAtModule(self);
    OverrideTha60210011ModuleBer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModuleBer);
    }

static AtModuleBer Tha60210021ModuleBerObjectInit(AtModuleBer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleBerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleBer Tha60210021ModuleBerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleBer newBerModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBerModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021ModuleBerObjectInit(newBerModule, device);
    }

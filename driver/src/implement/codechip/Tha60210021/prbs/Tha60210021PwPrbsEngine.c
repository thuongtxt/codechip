/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210021PwPrbsEngine.c
 *
 * Created Date: Oct 13, 2015
 *
 * Description : Tha60210021 PW PRBS Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210021PwPrbsEngineInternal.h"
#include "../../../default/pw/ThaModulePwInternal.h"
#include "../../../default/prbs/ThaPrbsEngine.h"
#include "../../../default/prbs/ThaModulePrbsInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
/* Pseudowire PDA PW PRBS Generation */
#define cPwPdaPwPrbsGenCtrlDataMask             cBit31_3
#define cPwPdaPwPrbsGenCtrlDataShift            3

#define cPwPdaPwPrbsGenCtrlErrorForceMask       cBit2
#define cPwPdaPwPrbsGenCtrlErrorForceShift      2

#define cPwPdaPwPrbsGenCtrlModeMask             cBit1
#define cPwPdaPwPrbsGenCtrlModeShift            1

#define cPwPdaPwPrbsGenCtrlEnMask               cBit0
#define cPwPdaPwPrbsGenCtrlEnShift              0

/* Pseudowire PDA PW PRBS Monitoring */
#define cPwPdaPwPrbsMonCtrlStickyMask           cBit17
#define cPwPdaPwPrbsMonCtrlStickyShift          17

#define cPwPdaPwPrbsMonCtrlStatusMask           cBit16
#define cPwPdaPwPrbsMonCtrlStatusShift          16

#define cPwPdaPwPrbsMonCtrlDataMask             cBit15_1
#define cPwPdaPwPrbsMonCtrlDataShift            1

#define cPwPdaPwPrbsMonCtrlModeMask             cBit0
#define cPwPdaPwPrbsMonCtrlModeShift            0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)     ((tTha60210021PwPrbsEngine *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Supper implementation */
static const tAtObjectMethods     *m_AtObjectMethods  = NULL;
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PwPrbsEngine);
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    AtPrbsEngineEnable(self, cAtFalse);
    AtPrbsEngineErrorForce(self, cAtFalse);

    AtPrbsEngineGeneratingSideSet(self, cAtPrbsSideTdm);
    AtPrbsEngineMonitoringSideSet(self, cAtPrbsSidePsn);
    AtPrbsEngineModeSet(self, cAtPrbsModePrbs15);

    mThis(self)->alarmHistory = cAtPrbsEngineAlarmTypeNone;
    mThis(self)->needGetHwAlarmHistory = cAtTrue;
    return cAtOk;
    }

static ThaModulePrbs PrbsModule(AtPrbsEngine self)
    {
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet(self));
    return (ThaModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    ThaModulePrbs module = PrbsModule(self);
    uint32 regAddr = ThaRegPdaPwPrbsGenCtrl(module) + ThaPrbsPwPdaDefaultOffset(module, self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cThaModulePwe);

    mFieldIns(&regVal, cPwPdaPwPrbsGenCtrlEnMask, cPwPdaPwPrbsGenCtrlEnShift, enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    ThaModulePrbs module = PrbsModule(self);
    uint32 regAddr = ThaRegPdaPwPrbsGenCtrl(module) + ThaPrbsPwPdaDefaultOffset(module, self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cThaModulePwe);
    return (regVal & cPwPdaPwPrbsGenCtrlEnMask) ? cAtTrue :cAtFalse;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    ThaModulePrbs module = PrbsModule(self);
    uint32 regAddr = ThaRegPdaPwPrbsGenCtrl(module) + ThaPrbsPwPdaDefaultOffset(module, self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cThaModulePwe);

    mFieldIns(&regVal, cPwPdaPwPrbsGenCtrlErrorForceMask, cPwPdaPwPrbsGenCtrlErrorForceShift, force ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    ThaModulePrbs module = PrbsModule(self);
    uint32 regAddr = ThaRegPdaPwPrbsGenCtrl(module) + ThaPrbsPwPdaDefaultOffset(module, self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cThaModulePwe);
    return (regVal & cPwPdaPwPrbsGenCtrlErrorForceMask) ? cAtTrue : cAtFalse;
    }

static eBool EngineStopped(AtPrbsEngine self)
    {
    const uint32 cDelayTimeBetweenTwoCheckingInMs = 1;
    uint32 cTimeoutMs = 128;
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTimeMs = 0;
    ThaModulePrbs module = PrbsModule(self);
    uint32 regAddr = ThaRegPdaPwPrbsMonCtrl(module) + ThaPrbsPwPdaDefaultOffset(module, self);
    uint32 firstTime = AtPrbsEngineRead(self, regAddr, cThaModulePwe);

    if (AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)module)))
        cTimeoutMs = 1;

    AtOsalCurTimeGet(&startTime);
    while (elapseTimeMs < cTimeoutMs)
        {
        uint32 secondTime;

        /* Engine may be running, not declare it stopped yet */
        AtOsalUSleep(cDelayTimeBetweenTwoCheckingInMs * 1000);
        secondTime = AtPrbsEngineRead(self, regAddr, cThaModulePwe);
        if (firstTime != secondTime)
            return cAtFalse;

        /* Make sure that it really stops after timeout */
        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtTrue;
    }

static uint32 AlarmHwRead(AtPrbsEngine self)
    {
    ThaModulePrbs module = PrbsModule(self);
    uint32 regAddr = ThaRegPdaPwPrbsMonCtrl(module) + ThaPrbsPwPdaDefaultOffset(module, self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cThaModulePwe);

    /* Update alarm history database */
    mThis(self)->alarmHistory = cAtPrbsEngineAlarmTypeNone;
    if (regVal & cPwPdaPwPrbsMonCtrlStickyMask)
        mThis(self)->alarmHistory = cAtPrbsEngineAlarmTypeError;

    if ((regVal & cPwPdaPwPrbsMonCtrlStatusMask) == 0)
        return cAtPrbsEngineAlarmTypeLossSync;

    return (EngineStopped(self)) ? cAtPrbsEngineAlarmTypeLossSync: cAtPrbsEngineAlarmTypeNone;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    mThis(self)->needGetHwAlarmHistory = cAtFalse;
    return AlarmHwRead(self);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    if (mThis(self)->needGetHwAlarmHistory)
        AlarmHwRead(self);

    mThis(self)->needGetHwAlarmHistory = cAtTrue;
    if (mThis(self)->alarmHistory == cAtPrbsEngineAlarmTypeNone)
        return EngineStopped(self) ? cAtPrbsEngineAlarmTypeError : cAtPrbsEngineAlarmTypeNone;
    return mThis(self)->alarmHistory;
    }

static uint8 PrbsModeSw2Hw(eAtPrbsMode prbsMode)
    {
    if (prbsMode == cAtPrbsModePrbs31)
        return 1;

    return 0;
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    if ((prbsMode == cAtPrbsModePrbs15) || (prbsMode == cAtPrbsModePrbs31))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    ThaModulePrbs module = PrbsModule(self);
    uint32 regAddr = ThaRegPdaPwPrbsGenCtrl(module) + ThaPrbsPwPdaDefaultOffset(module, self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cThaModulePwe);

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    mFieldIns(&regVal, cPwPdaPwPrbsGenCtrlModeMask, cPwPdaPwPrbsGenCtrlModeShift, PrbsModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtPrbsMode PrbsModeHw2Sw(uint8 prbsMode)
    {
    if (prbsMode == 1)
        return cAtPrbsModePrbs31;

    return cAtPrbsModePrbs15;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    ThaModulePrbs module = PrbsModule(self);
    uint32 regAddr = ThaRegPdaPwPrbsGenCtrl(module) + ThaPrbsPwPdaDefaultOffset(module, self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cThaModulePwe);

    return PrbsModeHw2Sw((uint8)mRegField(regVal, cPwPdaPwPrbsGenCtrlMode));
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    ThaModulePrbs module = PrbsModule(self);
    uint32 regAddr = ThaRegPdaPwPrbsMonCtrl(module) + ThaPrbsPwPdaDefaultOffset(module, self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cThaModulePwe);

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    mFieldIns(&regVal, cPwPdaPwPrbsMonCtrlModeMask, cPwPdaPwPrbsMonCtrlModeShift, PrbsModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    ThaModulePrbs module = PrbsModule(self);
    uint32 regAddr = ThaRegPdaPwPrbsMonCtrl(module) + ThaPrbsPwPdaDefaultOffset(module, self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cThaModulePwe);

    return PrbsModeHw2Sw((uint8)mRegField(regVal, cPwPdaPwPrbsMonCtrlMode));
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eAtRet ret = AtPrbsEngineTxModeSet(self, prbsMode);
    ret |= AtPrbsEngineRxModeSet(self, prbsMode);

    return ret;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineTxModeGet(self);
    }

static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
    AtUnused(self);
    if (invert)
        return cAtErrorModeNotSupport;
    return cAtOk;
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSidePsn) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSidePsn;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return cAtFalse;
    }

static eBool SideIsSupported(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtTrue : cAtFalse;
    }

static eBool InversionIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210021PwPrbsEngine * object = (tTha60210021PwPrbsEngine *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(alarmHistory);
    mEncodeUInt(needGetHwAlarmHistory);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, Invert);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, SideIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, InversionIsSupported);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

AtPrbsEngine Tha60210021PwPrbsEngineObjectInit(AtPrbsEngine self, AtPw pw, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, (AtChannel)pw, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210021PwPrbsEngineNew(AtPw pw, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021PwPrbsEngineObjectInit(newEngine, pw, engineId);
    }

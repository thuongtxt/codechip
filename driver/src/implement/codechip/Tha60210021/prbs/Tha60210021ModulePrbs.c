/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210021ModulePrbs.c
 *
 * Created Date: May 29, 2015
 *
 * Description : PRBS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/prbs/ThaModulePrbsInternal.h"
#include "../../../default/prbs/ThaPrbsEngine.h"
#include "../../../default/prbs/ThaPrbsRegProvider.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/prbs/Tha60210011ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021ModulePrbs
    {
    tThaModulePrbs super;
    }tTha60210021ModulePrbs;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePrbsMethods m_ThaModulePrbsOverride;
static tAtModulePrbsMethods  m_AtModulePrbsOverride;

/* Save super implementation */
static const tThaModulePrbsMethods *m_ThaModulePrbsMethods = NULL;
static const tAtModulePrbsMethods  *m_AtModulePrbsMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PdaBaseAddress(void)
    {
    return 0x240000;
    }

static uint32 PrbsPwPdaDefaultOffset(ThaModulePrbs self, AtPrbsEngine engine)
    {
    AtUnused(self);
    return AtChannelIdGet(AtPrbsEngineChannelGet(engine)) + PdaBaseAddress();
    }

static AtPrbsEngine NxDs0PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtUnused(self);
    return Tha60210021NxDs0PrbsEngineNew(nxDs0, engineId);
    }

static ThaVersionReader VersionReader(ThaModulePrbs self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartVersionSupportPwPrbsEngine(ThaModulePrbs self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x10, 0x13, 0x00);
    }

static AtPrbsEngine PwPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPw pw)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device) || (AtDeviceVersionNumber(device) >= StartVersionSupportPwPrbsEngine(self)))
        return Tha60210021PwPrbsEngineNew(pw, engineId);
    return NULL;
    }

static uint32 ForceEnableValue(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    AtUnused(self);
    return ThaPrbsEngineErrorRateSw2Hw(engine, cAtBerRate1E3);
    }

static AtPrbsEngine De1LinePrbsEngineCreate(AtModulePrbs self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return Tha6021PdhLinePrbsEngineNew((AtChannel)de1, AtChannelIdGet((AtChannel)de1));
    }

static ThaPrbsRegProvider RegProvider(ThaModulePrbs self)
    {
    AtUnused(self);
    return Tha60210021PrbsRegProvider();
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, m_AtModulePrbsMethods, sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, De1LinePrbsEngineCreate);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideThaModulePrbs(ThaModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePrbsOverride, m_ThaModulePrbsMethods, sizeof(m_ThaModulePrbsOverride));

        mMethodOverride(m_ThaModulePrbsOverride, PrbsPwPdaDefaultOffset);
        mMethodOverride(m_ThaModulePrbsOverride, NxDs0PrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, PwPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, ForceEnableValue);
        mMethodOverride(m_ThaModulePrbsOverride, RegProvider);
        m_ThaModulePrbsOverride.ErrorRateSw2Hw = Tha6021ErrorRateSw2Hw;
        m_ThaModulePrbsOverride.ErrorRateHw2Sw = Tha6021ErrorRateHw2Sw;
        }

    mMethodsSet(self, &m_ThaModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideThaModulePrbs((ThaModulePrbs)self);
    OverrideAtModulePrbs(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModulePrbs);
    }

static AtModulePrbs ObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha60210021ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

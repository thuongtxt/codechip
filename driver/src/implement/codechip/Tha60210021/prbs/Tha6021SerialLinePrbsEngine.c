/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6021SerialLinePrbsEngine.c
 *
 * Created Date: Jun 24, 2015
 *
 * Description : 6021xxxx Serial line PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaModulePdh.h"
#include "../../../default/prbs/ThaModulePrbsInternal.h"
#include "../../../default/prbs/ThaPrbsEngineInternal.h"
#include "Tha6021SerialLinePrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    AtPrbsEngineEnable(self, cAtFalse);
    AtPrbsEngineErrorForce(self, cAtFalse);
    return cAtOk;
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    return AtPrbsEngineEnable(self, enable);
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    return AtPrbsEngineIsEnabled(self);
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    return AtPrbsEngineEnable(self, enable);
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    return AtPrbsEngineIsEnabled(self);
    }

static AtDevice Device(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    return AtChannelDeviceGet(channel);
    }

static ThaModulePdh PdhModule(AtPrbsEngine self)
    {
    return (ThaModulePdh)AtDeviceModuleGet(Device(self), cAtModulePdh);
    }

static AtPdhChannel PdhChannel(AtPrbsEngine self)
    {
    return (AtPdhChannel)AtPrbsEngineChannelGet(self);
    }

static uint32 TestEnableRegister(AtPrbsEngine self)
    {
    return ThaModulePdhDiagLineTestEnableRegister(PdhModule(self), PdhChannel(self));
    }

static uint32 ChannelMask(AtPrbsEngine self)
    {
    return ThaModulePdhDiagLineMask(PdhModule(self), PdhChannel(self));
    }

static uint32 ChannelShift(AtPrbsEngine self)
    {
    return ThaModulePdhDiagLineShift(PdhModule(self), PdhChannel(self));
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddress = TestEnableRegister(self);
    uint32 regValue   = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);

    mFieldIns(&regValue, ChannelMask(self), ChannelShift(self), mBoolToBin(enable));
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regAddress = TestEnableRegister(self);
    uint32 regValue   = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);

    return (regValue & ChannelMask(self)) ? cAtTrue : cAtFalse;
    }

static uint32 TestErrorInsertRegister(AtPrbsEngine self)
    {
    return ThaModulePdhDiagLineTestErrorInsertRegister(PdhModule(self), PdhChannel(self));
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddress = TestErrorInsertRegister(self);
    uint32 regValue   = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);

    mFieldIns(&regValue, ChannelMask(self), ChannelShift(self), mBoolToBin(force));
    AtPrbsEngineWrite(self, regAddress, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddress = TestErrorInsertRegister(self);
    uint32 regValue   = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);

    return (regValue & ChannelMask(self)) ? cAtTrue : cAtFalse;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    return (prbsMode == cAtPrbsModePrbs15) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsModePrbs15;
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    return (prbsMode == cAtPrbsModePrbs15) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
    AtUnused(self);
    AtUnused(invert);
    return invert ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool IsInverted(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
    {
    AtUnused(self);
    AtUnused(invert);
    return invert ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool TxIsInverted(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
    {
    AtUnused(self);
    AtUnused(invert);
    return invert ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool RxIsInverted(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(counterType);
    AtUnused(self);
    return 0;
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(counterType);
    AtUnused(self);
    return 0;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool clear)
    {
    uint32 regAddress = ThaModulePdhDiagLineTestStatusRegister(PdhModule(self), PdhChannel(self));
    uint32 regValue   = AtPrbsEngineRead(self, regAddress, cAtModulePrbs);
    uint32 alarms     = (regValue & ChannelMask(self)) ? cAtPrbsEngineAlarmTypeError : cAtPrbsEngineAlarmTypeNone;
    if (clear)
        AtPrbsEngineWrite(self, regAddress, ChannelMask(self), cAtModulePrbs);
    return alarms;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return AtPrbsEngineModeSet(self, prbsMode);
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineModeGet(self);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return AtPrbsEngineModeSet(self, prbsMode);
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineModeGet(self);
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6021SerialLinePrbsEngine);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, Invert);
        mMethodOverride(m_AtPrbsEngineOverride, IsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, TxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, RxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

AtPrbsEngine Tha6021SerialLinePrbsEngineObjectInit(AtPrbsEngine self, AtChannel channel, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, channel, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6021SerialLinePrbsEngineNew(AtChannel channel, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6021SerialLinePrbsEngineObjectInit(newEngine, channel, engineId);
    }

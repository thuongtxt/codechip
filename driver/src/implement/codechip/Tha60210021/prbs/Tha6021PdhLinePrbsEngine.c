/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6021PdhLinePrbsEngine.c
 *
 * Created Date: Jun 26, 2019
 *
 * Description : 6021xxxx PDH line PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/prbs/ThaPrbsEngine.h"
#include "../../../default/pdh/ThaModulePdh.h"
#include "Tha6021SerialLinePrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6021PdhLinePrbsEngine
    {
    tTha6021SerialLinePrbsEngine super;
    }tTha6021PdhLinePrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void De3FrameTypeDidChange(AtPdhChannel de3, uint16 oldFrameType, uint16 newFrameType, void* userData)
    {
    AtUnused(userData);

    if (AtPdhDe3FrameTypeIsE3(oldFrameType) != AtPdhDe3FrameTypeIsE3(newFrameType))
        ThaModulePdhDiagLiuLineModeSet((AtChannel)de3, AtPdhDe3FrameTypeIsE3(newFrameType) ? 0 : 1);
    }

static void De1FrameTypeDidChange(AtPdhChannel de1, uint16 oldFrameType, uint16 newFrameType, void *userData)
    {
    AtUnused(userData);

    if (AtPdhDe1FrameTypeIsE1(oldFrameType) != AtPdhDe1FrameTypeIsE1(newFrameType))
        ThaModulePdhDiagLiuLineModeSet((AtChannel)de1, AtPdhDe1FrameTypeIsE1(newFrameType) ? 0 : 1);
    }

static tAtPdhChannelPropertyListener* PdhChannelListenerWithHandler(tAtPdhChannelPropertyListener **pHolder,
                                                                    tAtPdhChannelPropertyListener *listener,
                                                                    void (*Handler)(AtPdhChannel, uint16, uint16, void*))
    {
    if (*pHolder)
        return *pHolder;

    AtOsalMemInit(listener, 0, sizeof(tAtPdhChannelPropertyListener));

    listener->FrameTypeDidChange = Handler;

    *pHolder = listener;
    return *pHolder;
    }

static tAtPdhChannelPropertyListener* PdhChannelListener(AtPdhChannel channel)
    {
    static tAtPdhChannelPropertyListener* pDe1Listener = NULL;
    static tAtPdhChannelPropertyListener* pDe3Listener = NULL;
    static tAtPdhChannelPropertyListener de1Listener, de3Listener;
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(channel);

    if ((channelType == cAtPdhChannelTypeDs3) || (channelType == cAtPdhChannelTypeE3))
        return PdhChannelListenerWithHandler(&pDe3Listener, &de3Listener, De3FrameTypeDidChange);

    else if ((channelType == cAtPdhChannelTypeDs1) || (channelType == cAtPdhChannelTypeE1))
        return PdhChannelListenerWithHandler(&pDe1Listener, &de1Listener, De1FrameTypeDidChange);

    /* MUST never reach it */
    AtAssert(0);
    return NULL;
    }

static eAtRet PdhChannelListenerAdd(AtPrbsEngine self)
    {
    AtPdhChannel channel = (AtPdhChannel)AtPrbsEngineChannelGet(self);
    return AtPdhChannelPropertyListenerAdd(channel, PdhChannelListener(channel), self);
    }

static eAtRet DefaultSet(AtPrbsEngine self)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)AtPrbsEngineChannelGet(self);
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(pdhChannel);
    uint16 frameType = AtPdhChannelFrameTypeGet(pdhChannel);

    if ((channelType == cAtPdhChannelTypeDs3) || (channelType == cAtPdhChannelTypeE3))
        return ThaModulePdhDiagLiuLineModeSet((AtChannel)pdhChannel, AtPdhDe3FrameTypeIsE3(frameType) ? 0 : 1);

    else if ((channelType == cAtPdhChannelTypeDs1) || (channelType == cAtPdhChannelTypeE1))
        return ThaModulePdhDiagLiuLineModeSet((AtChannel)pdhChannel, AtPdhDe1FrameTypeIsE1(frameType) ? 0 : 1);

    return cAtOk;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= DefaultSet(self);

    /* To listen the property change of associated channel. */
    ret |= PdhChannelListenerAdd(self);

    return ret;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6021PdhLinePrbsEngine);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtChannel channel, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021SerialLinePrbsEngineObjectInit(self, channel, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6021PdhLinePrbsEngineNew(AtChannel channel, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, channel, engineId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210021NxDs0PrbsEngineInternal.h
 * 
 * Created Date: May 2, 2017
 *
 * Description : NxDS0 PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021NXDS0PRBSENGINEINTERNAL_H_
#define _THA60210021NXDS0PRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/prbs/ThaPrbsEngineNxDs0Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210021NxDs0PrbsEngine
    {
    tThaPrbsEngineNxDs0 super;

    /* Private data */
    eAtBerRate nxds0ErrorRate;
    }tTha60210021NxDs0PrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60210021NxDs0PrbsEngineObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021NXDS0PRBSENGINEINTERNAL_H_ */


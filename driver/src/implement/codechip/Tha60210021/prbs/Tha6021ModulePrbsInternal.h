/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6021ModulePrbsInternal.h
 * 
 * Created Date: Jul 10, 2015
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021MODULEPRBSINTERNAL_H_
#define _THA6021MODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/prbs/ThaModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6021ModulePrbs
    {
    tThaModulePrbs super;
    }tTha6021ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtBerRate Tha6021ErrorRateHw2Sw(ThaModulePrbs self, uint32 errorRate);
uint32 Tha6021ErrorRateSw2Hw(ThaModulePrbs self, eAtBerRate errorRate);

#ifdef __cplusplus
}
#endif
#endif /* _THA6021MODULEPRBSINTERNAL_H_ */


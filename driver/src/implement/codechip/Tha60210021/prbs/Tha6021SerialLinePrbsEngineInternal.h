/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6021SerialLinePrbsEngineInternal.h
 * 
 * Created Date: Jun 26, 2019
 *
 * Description : PDH serial line PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6021SERIALLINEPRBSENGINEINTERNAL_H_
#define _THA6021SERIALLINEPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6021SerialLinePrbsEngine
    {
    tAtPrbsEngine super;
    }tTha6021SerialLinePrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6021SerialLinePrbsEngineObjectInit(AtPrbsEngine self, AtChannel channel, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THA6021SERIALLINEPRBSENGINEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210021PrbsRegProvider.c
 *
 * Created Date: Jan 6, 2016
 *
 * Description : Register provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/prbs/ThaPrbsRegProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cSliceAddressStep                       0x020000UL

#define cThaRegMapBERTGenSelectedChannel        0x1A2100
#define cThaRegMapBERTGenNxDs0ConcateControl    0x1A2060
#define cThaRegMapBERTGenMode                   0x1A2000
#define cThaRegMapBERTGenFixedPatternControl    0x1A2010
#define cThaRegMapBERTGenErrorRateInsert        0x1A2020
#define cThaRegMapBERTGenSingleBitErrInsert     0x1A207E
#define cThaRegMapBERTGenGoodBitCounter         0x1A2080

#define cThaRegMapBERTMonSelectedChannel        0x1A2310
#define cThaRegMapBERTMonNxDs0ConcateControl    0x1A22C0
#define cThaRegMapBERTMonGlobalControl          0x1A2200
#define cThaRegMapBERTMonStickyEnable           0x1A2201
#define cThaRegMapBERTMonSticky                 0x1A2202
#define cThaRegMapBERTMonMode                   0x1A2210
#define cThaRegMapBERTMonFixedPatternControl    0x1A2220
#define cThaRegMapBERTMonErrorCounter           0x1A2260
#define cThaRegMapBERTMonGoodBitCounter         0x1A2280
#define cThaRegMapBERTMonLossBitCounter         0x1A22A0
#define cThaRegMapBERTMonCounterLoadId          0x1A2203
#define cThaRegMapBERTMonErrorCounterLoading    0x1A2204
#define cThaRegMapBERTMonGoodBitCounterLoading  0x1A2205
#define cThaRegMapBERTMonLossBitCounterLoading  0x1A2206

#define cThaRegDemapBERTMonSelectedChannel      0x1A2500
#define cThaRegDemapBERTMonNxDs0ConcateControl  0x1A24C0
#define cThaRegDemapBERTMonGlobalControl        0x1A2400
#define cThaRegDemapBERTMonStickyEnable         0x1A2401
#define cThaRegDemapBERTMonSticky               0x1A2402
#define cThaRegDemapBERTMonMode                 0x1A2410
#define cThaRegDemapBERTMonFixedPatternControl  0x1A2420
#define cThaRegDemapBERTMonErrorCounter         0x1A2460
#define cThaRegDemapBERTMonGoodBitCounter       0x1A2480
#define cThaRegDemapBERTMonLossBitCounter       0x1A24A0
#define cThaRegDemapBERTMonCounterLoadId        0x1A2403
#define cThaRegDemapBERTMonErrorCounterLoading  0x1A2404
#define cThaRegDemapBERTMonGoodBitCounterLoading 0x1A2405
#define cThaRegDemapBERTMonLossBitCounterLoading 0x1A2406

#define cThaRegMapBERTGenTxBerErrMask                  cBit31_0
#define cThaRegMapBERTGenTxBerErrShift                 0
#define cThaRegMapBERTGenTxBerMdMask                   cBit31_0
#define cThaRegMapBERTGenTxBerMdShift                  0

/* Pseudowire PDA PW PRBS Generation */
#define cPwPdaPwPrbsGenCtrl                     0x006400

/* Pseudowire PDA PW PRBS Monitoring */
#define cPwPdaPwPrbsMonCtrl                     0x006800

/* BERT status */
#define cThaRegMapBERTMonStatus                 0x1A2240
#define cThaRegDemapBERTMonStatus               0x1A2440

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021PrbsRegProvider
    {
    tThaPrbsRegProvider super;
    }tTha60210021PrbsRegProvider;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsRegProviderMethods m_ThaPrbsRegProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Offset(uint32 engineId, uint32 slice)
    {
    return engineId + (slice * cSliceAddressStep);
    }

static uint32 RegMapBERTGenTxBerMdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerMdMask;
    }

static uint8  RegMapBERTGenTxBerMdShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerMdShift;
    }

static uint32 RegMapBERTGenTxBerErrMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerErrMask;
    }

static uint8  RegMapBERTGenTxBerErrShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerErrShift;
    }

static uint32 RegMapBERTGenSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenSelectedChannel + Offset(engineId, slice);
    }

static uint32 RegMapBERTGenNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenNxDs0ConcateControl + Offset(engineId, slice);
    }

static uint32 RegMapBERTGenMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenMode + Offset(engineId, slice);
    }

static uint32 RegMapBERTGenFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenFixedPatternControl + Offset(engineId, slice);
    }

static uint32 RegMapBERTGenErrorRateInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenErrorRateInsert + Offset(engineId, slice);
    }

static uint32 RegMapBERTGenSingleBitErrInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenSingleBitErrInsert + Offset(engineId, slice);
    }

static uint32 RegMapBERTGenGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenGoodBitCounter + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonSelectedChannel + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonNxDs0ConcateControl + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonGlobalControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonGlobalControl + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonStickyEnable(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonStickyEnable + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonSticky(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonSticky + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonMode + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonFixedPatternControl + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonErrorCounter + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonGoodBitCounter + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonLossBitCounter + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonCounterLoadId(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonCounterLoadId + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonErrorCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonErrorCounterLoading + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonGoodBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonGoodBitCounterLoading + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonLossBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonLossBitCounterLoading + Offset(engineId, slice);
    }

static uint32 RegDemapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonSelectedChannel + Offset(engineId, slice);
    }

static uint32 RegDemapBERTMonNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonNxDs0ConcateControl + Offset(engineId, slice);
    }

static uint32 RegDemapBERTMonGlobalControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonGlobalControl + Offset(engineId, slice);
    }

static uint32 RegDemapBERTMonStickyEnable(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonStickyEnable + Offset(engineId, slice);
    }

static uint32 RegDemapBERTMonSticky(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonSticky + Offset(engineId, slice);
    }

static uint32 RegDemapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonMode + Offset(engineId, slice);
    }

static uint32 RegDemapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonFixedPatternControl + Offset(engineId, slice);
    }

static uint32 RegDemapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonErrorCounter + Offset(engineId, slice);
    }

static uint32 RegDemapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonGoodBitCounter + Offset(engineId, slice);
    }

static uint32 RegDemapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonLossBitCounter + Offset(engineId, slice);
    }

static uint32 RegDemapBERTMonCounterLoadId(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonCounterLoadId + Offset(engineId, slice);
    }
static uint32 RegDemapBERTMonErrorCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonErrorCounterLoading + Offset(engineId, slice);
    }

static uint32 RegDemapBERTMonGoodBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonGoodBitCounterLoading + Offset(engineId, slice);
    }

static uint32 RegDemapBERTMonLossBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonLossBitCounterLoading + Offset(engineId, slice);
    }

static uint32 RegMapBERTGenTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit6;
    }

static uint32 RegMapBERTGenTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit5_0;
    }

static uint32 RegDemapBERTMonRxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit6;
    }

static uint32 RegDemapBERTMonRxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit5_0;
    }

static uint32 RegMapBERTMonTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit8;
    }

static uint32 RegMapBERTMonTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit7_0;
    }

static uint32 RegPdaPwPrbsGenCtrl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cPwPdaPwPrbsGenCtrl;
    }

static uint32 RegPdaPwPrbsMonCtrl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cPwPdaPwPrbsMonCtrl;
    }

static uint32 RegDemapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonStatus + Offset(engineId, slice);
    }

static uint32 RegMapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonStatus + Offset(engineId, slice);
    }

static void OverrideThaPrbsRegProvider(ThaPrbsRegProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsRegProviderOverride, mMethodsGet(self), sizeof(m_ThaPrbsRegProviderOverride));

        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenNxDs0ConcateControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenMode);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenFixedPatternControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenErrorRateInsert);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenSingleBitErrInsert);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonNxDs0ConcateControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonGlobalControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonStickyEnable);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonSticky);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonMode);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonFixedPatternControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonErrorCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonLossBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonCounterLoadId);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonErrorCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonGoodBitCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonLossBitCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonNxDs0ConcateControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonGlobalControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonStickyEnable);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonSticky);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonMode);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonFixedPatternControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonErrorCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonLossBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonCounterLoadId);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonErrorCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonGoodBitCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonLossBitCounterLoading);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxPtgEnMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxPtgIdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonRxPtgEnMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonRxPtgIdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonTxPtgEnMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonTxPtgIdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegPdaPwPrbsGenCtrl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegPdaPwPrbsMonCtrl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonStatus);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonStatus);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxBerMdMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxBerMdShift);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxBerErrMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxBerErrShift);
        }

    mMethodsSet(self, &m_ThaPrbsRegProviderOverride);
    }

static void Override(ThaPrbsRegProvider self)
    {
    OverrideThaPrbsRegProvider(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021PrbsRegProvider);
    }

static ThaPrbsRegProvider ObjectInit(ThaPrbsRegProvider self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsRegProviderObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPrbsRegProvider Tha60210021PrbsRegProvider(void)
    {
    static tTha60210021PrbsRegProvider shareProvider;
    static ThaPrbsRegProvider pShareProvider = NULL;
    if (pShareProvider == NULL)
        pShareProvider = ObjectInit((ThaPrbsRegProvider)&shareProvider);
    return pShareProvider;
    }

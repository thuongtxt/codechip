/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210021PwPrbsEngineInternal.h
 * 
 * Created Date: Oct 20, 2015
 *
 * Description : Tha60210021 PRBS Engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021PWPRBSENGINEINTERNAL_H_
#define _THA60210021PWPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210021PwPrbsEngine
    {
    tAtPrbsEngine super;

    /* Private data */
    uint32 alarmHistory;
    eBool needGetHwAlarmHistory; /* To prevent reading multiple times that will clear sticky as hardware field is R2C */
    }tTha60210021PwPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60210021PwPrbsEngineObjectInit(AtPrbsEngine self, AtPw pw, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021PWPRBSENGINEINTERNAL_H_ */


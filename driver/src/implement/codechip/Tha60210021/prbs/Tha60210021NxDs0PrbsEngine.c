/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210021NxDs0PrbsEngine.c
 *
 * Created Date: Sep 15, 2015
 *
 * Description : NxDS0 PRBS Engine of Tha60210021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "AtPdhNxDs0.h"
#include "Tha60210021NxDs0PrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)     ((tTha60210021NxDs0PrbsEngine *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsEngineMethods m_ThaPrbsEngineOverride;
static tAtPrbsEngineMethods  m_AtPrbsEngineOverride;
static tAtObjectMethods      m_AtObjectOverride;

/* Super implementation */
static const tAtPrbsEngineMethods  *m_AtPrbsEngineMethods = NULL;
static const tAtObjectMethods      *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021NxDs0PrbsEngine);
    }

static uint32 ErrorRateSw2Hw(ThaPrbsEngine self, eAtBerRate errorRate)
    {
    AtPdhNxDS0 nxds0 = (AtPdhNxDS0)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint32 de1BitRate = (AtPdhDe1IsE1(AtPdhNxDS0De1Get(nxds0)) == cAtTrue) ? 256 : 193;
    uint32 de1ErrorRate = 0;
    uint32 rateConstant = 1;

    switch ((uint32) errorRate)
        {
        case cAtBerRate1E2:
            de1ErrorRate = 100;
            break;

        case cAtBerRate1E3:
            de1ErrorRate = 1000;
            break;
        case cAtBerRate1E4:
            de1ErrorRate = 10000;
            break;
        case cAtBerRate1E5:
            de1ErrorRate = 100000;
            break;
        case cAtBerRate1E6:
            de1ErrorRate = 1000000;
            break;
        case cAtBerRate1E7:
            de1ErrorRate = 1000000;
            rateConstant = 10;
            break;

        case cAtBerRate1E8:
            de1ErrorRate = 1000000;
            rateConstant = 100;
            break;

        case cAtBerRate1E9:
            de1ErrorRate = 1000000;
            rateConstant = 1000;
            break;

        case cAtBerRate1E10:
        case cAtBerRateUnknown:
        default:
            break;
        }

    return (((AtPdhNxDs0NumTimeslotsGet(nxds0) * 8UL) * de1ErrorRate) / de1BitRate) * rateConstant;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    mThis(self)->nxds0ErrorRate = cAtBerRateUnknown;
    return ret;
    }

static eAtModulePrbsRet TxErrorRateSet(AtPrbsEngine self, eAtBerRate errorRate)
    {
    eAtRet ret = m_AtPrbsEngineMethods->TxErrorRateSet(self, errorRate);
    if (ret != cAtOk)
        return ret;

    mThis(self)->nxds0ErrorRate = errorRate;
    return ret;
    }

static eAtBerRate TxErrorRateGet(AtPrbsEngine self)
    {
    return mThis(self)->nxds0ErrorRate;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210021NxDs0PrbsEngine * object = (tTha60210021NxDs0PrbsEngine *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(nxds0ErrorRate);
    }

static void OverrideThaPrbsEngine(ThaPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(self), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, ErrorRateSw2Hw);
        }

    mMethodsSet(self, &m_ThaPrbsEngineOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideThaPrbsEngine((ThaPrbsEngine)self);
    OverrideAtPrbsEngine(self);
    OverrideAtObject(self);
    }

AtPrbsEngine Tha60210021NxDs0PrbsEngineObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineNxDs0ObjectInit(self, nxDs0, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210021NxDs0PrbsEngineNew(AtPdhNxDS0 nxDs0, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210021NxDs0PrbsEngineObjectInit(newEngine, nxDs0, engineId);
    }

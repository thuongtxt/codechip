/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210021ClaPwController.c
 *
 * Created Date: Sep 15, 2015
 *
 * Description : CLA PW controller of 60210021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ThaPdhPwProduct/cla/ThaPdhPwProductClaPwController.h"
#include "../../pmc/Tha60210021ModulePmcReg.h"
#include "../Tha60210021ModuleCla.h"
#include "../hbce/Tha60210021Hbce.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021ClaPwController
    {
    tThaPdhPwProductClaPwController super;

    }tTha60210021ClaPwController;
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaPwControllerMethods m_ThaClaPwControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PmcBaseAddress(eBool clear)
    {
    return (0x500000UL + ((clear) ? 0 : 0x800UL));
    }

static uint32 RxDuplicatedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (ThaModuleClaPwRxDuplicatedPacketsIsSupported(ThaClaControllerModuleGet((ThaClaController)self)))
        return mChannelHwRead(pw, cAf6Reg_Pseudowire_Receive_Duplicate_Counter_rc_Base + PmcBaseAddress(clear) + mClaPwOffset(self, pw), cThaModulePwPmc);
    return 0;
    }

static ThaHbce HbceCreate(ThaClaPwController self, uint8 hbceId, AtIpCore core)
    {
    return Tha60210021HbceNew(hbceId, (AtModule)ThaClaControllerModuleGet((ThaClaController)self), core);
    }

static eBool PwLookupModeIsSupported(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode)
    {
    AtUnused(self);
    AtUnused(pw);
    if (lookupMode == cThaPwLookupModePsn)
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, mMethodsGet(self), sizeof(m_ThaClaPwControllerOverride));

        mMethodOverride(m_ThaClaPwControllerOverride, RxDuplicatedPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, HbceCreate);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLookupModeIsSupported);
        }

   mMethodsSet(self, &m_ThaClaPwControllerOverride);
   }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ClaPwController);
    }

static ThaClaPwController ObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60210021ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cla);
    }


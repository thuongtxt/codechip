/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210021ModuleCla.h
 * 
 * Created Date: Sep 15, 2015
 *
 * Description : 60210021 module CLA
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021MODULECLA_H_
#define _THA60210021MODULECLA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPwGroup.h"
#include "../../default/ThaPdhPwProduct/cla/ThaPdhPwProductModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaPwController Tha60210021ClaPwControllerNew(ThaModuleCla cla);
eAtRet Tha60210021ModuleClaHsGroupPwAdd(ThaModuleCla self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet Tha60210021ModuleClaHsGroupPwRemove(ThaModuleCla self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet Tha60210021ModuleClaHsGroupEnable(ThaModuleCla self, AtPwGroup pwGroup, eBool enable);
eAtRet Tha60210021ModuleClaHsGroupIsEnabled(ThaModuleCla self, AtPwGroup pwGroup);
eAtRet Tha60210021ModuleClaHsGroupLabelSetSelect(ThaModuleCla self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet);
eAtRet Tha60210021ModuleClaHsGroupSelectedLabelSetGet(ThaModuleCla self, AtPwGroup pwGroup);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021MODULECLA_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210021Hbce.c
 *
 * Created Date: Oct 12, 2015
 *
 * Description : HBCE
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/cla/hbce/ThaHbceEntityInternal.h"
#include "Tha60210021Hbce.h"

/*--------------------------- Define -----------------------------------------*/
#define cCLAHbceGrpWorking_Mask          cBit0
#define cCLAHbceGrpWorking_Shift         0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021Hbce
    {
    tThaHbce super;
    }tTha60210021Hbce;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaHbceMethods m_ThaHbceOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaHbceMemoryPool MemoryPoolCreate(ThaHbce self)
    {
    return Tha60210021HbceMemoryPoolNew(self);
    }

static uint32 CellGroupWorkingMask(ThaHbce self)
    {
    AtUnused(self);
    return cCLAHbceGrpWorking_Mask;
    }

static uint32 CellGroupWorkingShift(ThaHbce self)
    {
    AtUnused(self);
    return cCLAHbceGrpWorking_Shift;
    }

static void OverrideThaHbce(ThaHbce self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceOverride, mMethodsGet(self), sizeof(m_ThaHbceOverride));

        mMethodOverride(m_ThaHbceOverride, MemoryPoolCreate);
        mMethodOverride(m_ThaHbceOverride, CellGroupWorkingMask);
        mMethodOverride(m_ThaHbceOverride, CellGroupWorkingShift);
        }

    mMethodsSet(self, &m_ThaHbceOverride);
    }

static void Override(ThaHbce self)
    {
    OverrideThaHbce(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021Hbce);
    }

static ThaHbce ObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHbceObjectInit(self, hbceId, claModule, core) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbce Tha60210021HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbce newHbce = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newHbce == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newHbce, hbceId, claModule, core);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210021Hbce.h
 * 
 * Created Date: Oct 12, 2015
 *
 * Description : HBCE
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021HBCE_H_
#define _THA60210021HBCE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHbce Tha60210021HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core);
ThaHbceMemoryPool Tha60210021HbceMemoryPoolNew(ThaHbce hbce);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210021HBCE_H_ */


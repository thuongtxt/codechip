/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210021HbceMemoryPool.c
 *
 * Created Date: Oct 12, 2015
 *
 * Description : HBCE memory pool
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwGroup.h"
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/cla/hbce/ThaHbceEntityInternal.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryPool.h"
#include "../../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../../default/cla/pw/ThaModuleClaPwReg.h"
#include "../../../../default/cla/pw/ThaModuleClaPwV2Reg.h"
#include "../../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "../../../../default/cla/controllers/ThaClaPwControllerInternal.h"
#include "../Tha60210021ModuleClaReg.h"
#include "../Tha60210021ModuleCla.h"
#include "Tha60210021Hbce.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021HbceMemoryPool
    {
    tThaHbceMemoryPool super;
    }tTha60210021HbceMemoryPool;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override  */
static tThaHbceMemoryPoolMethods m_ThaHbceMemoryPoolOverride;

/* Save super implementation */
static const tThaHbceMemoryPoolMethods *m_ThaHbceMemoryPoolMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleClaPwV2 ClaModule(ThaHbceMemoryPool self)
    {
    return (ThaModuleClaPwV2)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    }

static void ApplyCellToHardware(ThaHbceMemoryPool self, uint32 cellIndex, uint8 page)
    {
    uint32 address;
    ThaPwAdapter pwAdapter;
    AtPwGroup pwGroup;
    ThaHbceMemoryCellContent cellContent;
    ThaPwHeaderController headerController;
    uint32 longRegVal[cThaLongRegMaxSize];
    AtDevice device;

    m_ThaHbceMemoryPoolMethods->ApplyCellToHardware(self, cellIndex, page);

    cellContent = ThaHbceMemoryCellContentGet(ThaHbceMemoryPoolCellAtIndex(self, cellIndex));
    if (cellContent == NULL)
        return;

    if (ThaHbceMemoryCellContentRemainedBitsGet(cellContent, NULL) == NULL)
        return;

    headerController = ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent);
    pwAdapter = ThaPwHeaderControllerAdapterGet(headerController);
    device = AtChannelDeviceGet((AtChannel)pwAdapter);
    address = ThaModuleClaPwV2HbceLookingUpInformationCtrl(ClaModule(self), headerController, page);
    AtDeviceLongReadOnCore(device, address, longRegVal, cThaLongRegMaxSize, 0);
    
    /* Disable cell of backup PSN if PW does not belong to HW group */
    pwGroup = pwAdapter ? AtPwHsGroupGet(ThaPwAdapterPwGet(pwAdapter)) : NULL;
    if ((pwGroup == NULL) && (ThaPwHeaderControllerIsPrimary(headerController) == cAtFalse))
        mRegFieldSet(longRegVal[0], cThaClaHbceFlowEnb, 0);
    else
        mRegFieldSet(longRegVal[0], cThaClaHbceFlowEnb, mBoolToBin(AtChannelIsEnabled((AtChannel)pwAdapter) ? 1 : 0));

    if (pwGroup != NULL)
        {
        uint32 groupId = AtPwGroupIdGet(pwGroup);
        mRegFieldSet(longRegVal[1], cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceFlowDirect_, 0);
        mRegFieldSet(longRegVal[0], cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpIDFlow_, groupId);
        mRegFieldSet(longRegVal[1],
                     cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpWorking_,
                     mBoolToBin(ThaPwHeaderControllerIsPrimary(headerController)));
        }
    else
        mRegFieldSet(longRegVal[1], cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceFlowDirect_, 1);

    AtDeviceLongWriteOnCore(device, address, longRegVal, cThaLongRegMaxSize, 0);
    }

static ThaPwHeaderController StandbyRestoreCellHeaderController(ThaHbceMemoryPool self, ThaPwAdapter pwAdapter, ThaHbceMemoryCell cell, uint8 page)
    {
    uint32 cellIndex;
    uint32 groupId;
    uint32 address;
    uint32 longRegVal[cThaLongRegMaxSize];
    AtDevice device;
    AtPw pw = ThaPwAdapterPwGet(pwAdapter);
    AtPwGroup pwGroup = AtPwHsGroupGet(pw);

    if (pwGroup == NULL)
        return m_ThaHbceMemoryPoolMethods->StandbyRestoreCellHeaderController(self, pwAdapter, cell, page);

    /* Need to read register to see if this cell belongs to primary or backup header controller */
    cellIndex = ThaHbceMemoryCellIndexGet(cell);
    address = ThaModuleClaPwV2HbceCellLookingUpInformationCtrl(ClaModule(self), self, cellIndex, page);
    device = AtChannelDeviceGet((AtChannel)pw);
    AtDeviceLongReadOnCore(device, address, longRegVal, cThaLongRegMaxSize, 0);

    /* For sanity checking */
    groupId = mRegField(longRegVal[0], cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpIDFlow_);
    if (groupId != AtPwGroupIdGet(pwGroup))
        AtChannelLog((AtChannel)pw, cAtLogLevelCritical, AtSourceLocation, "Hardware group ID and SW group ID are mismatched\r\n");

    /* This is actually backup PSN */
    if (longRegVal[1] & cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpWorking_Mask)
        return ThaPwAdapterHeaderController(pwAdapter);
    else
        return ThaPwAdapterBackupHeaderController(pwAdapter);
    }

static uint32 ClaHbceLookingUpInformationCtrl(ThaHbceMemoryPool self, AtModule claModule)
    {
    ThaClaPwController claPwController = ThaModuleClaPwControllerGet((ThaModuleCla)claModule);
    return ThaClaPwControllerClaHbceLookingUpInformationCtrl(claPwController) + ThaHbcePartOffset(self->hbce);
    }

static eAtRet CellEnable(ThaHbceMemoryPool self, ThaHbceMemoryCell cell, eBool enable)
    {
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 longRegVal[cLongRegSizeInDwords];
    uint8 activePage = ThaHbceActivePage(self->hbce);
    uint32 pageStartCell = ThaHbceMemoryPoolMaxNumCells(self) * activePage;
    uint32 address = ClaHbceLookingUpInformationCtrl(self, claModule) + pageStartCell + ThaHbceMemoryCellIndexGet(cell);
    ThaHbceMemoryCellContent cellContent = ThaHbceMemoryCellContentGet(cell);
    ThaClaPwController pwController = ThaModuleClaPwControllerGet((ThaModuleCla)claModule);

    /* Update hardware */
    mModuleHwLongRead(claModule, address, longRegVal, cLongRegSizeInDwords, ThaHbceCoreGet(self->hbce));
    mFieldIns(&longRegVal[0], mFieldMask(pwController, ClaHbceFlowEnb), mFieldShift(pwController, ClaHbceFlowEnb), enable ? 1 : 0);
    mModuleHwLongWrite(claModule, address, longRegVal, cLongRegSizeInDwords, ThaHbceCoreGet(self->hbce));

    /* Update software database */
    ThaHbceMemoryCellContentEnable(cellContent, enable);

    return cAtOk;
    }

static eBool CellIsEnabled(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 longRegVal[cLongRegSizeInDwords];
    uint8 activePage = ThaHbceActivePage(self->hbce);
    uint32 pageStartCell = ThaHbceMemoryPoolMaxNumCells(self) * activePage;
    uint32 address = ClaHbceLookingUpInformationCtrl(self, claModule) + pageStartCell + ThaHbceMemoryCellIndexGet(cell);
    ThaClaPwController pwController = ThaModuleClaPwControllerGet((ThaModuleCla)claModule);

    mModuleHwLongRead(claModule, address, longRegVal, cLongRegSizeInDwords, ThaHbceCoreGet(self->hbce));
    return (longRegVal[0] & mFieldMask(pwController, ClaHbceFlowEnb)) ? cAtTrue : cAtFalse;
    }

static void OverrideThaHbceMemoryPool(ThaHbceMemoryPool self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceMemoryPoolMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceMemoryPoolOverride, m_ThaHbceMemoryPoolMethods, sizeof(m_ThaHbceMemoryPoolOverride));

        mMethodOverride(m_ThaHbceMemoryPoolOverride, ApplyCellToHardware);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellEnable);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, CellIsEnabled);
        mMethodOverride(m_ThaHbceMemoryPoolOverride, StandbyRestoreCellHeaderController);
        }

    mMethodsSet(self, &m_ThaHbceMemoryPoolOverride);
    }

static void Override(ThaHbceMemoryPool self)
    {
    OverrideThaHbceMemoryPool(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021HbceMemoryPool);
    }

static ThaHbceMemoryPool ObjectInit(ThaHbceMemoryPool self, ThaHbce hbce)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHbceMemoryPoolObjectInit(self, hbce) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaHbceMemoryPool Tha60210021HbceMemoryPoolNew(ThaHbce hbce)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceMemoryPool newPool = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPool == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPool, hbce);
    }

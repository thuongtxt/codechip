/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210021ModuleClaReg.h
 * 
 * Created Date: Oct 12, 2015
 *
 * Description : CLA registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210021MODULECLAREG_H_
#define _THA60210021MODULECLAREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Classify Per Group Enable Control
Reg Addr   : 0x0000A000 - 0x0000A1FF
Reg Formula: 0x0000A000 + Working_ID*0x100 + Grp_ID
    Where  :
           + $Working_ID(0-1): CLAHbceGrpWorking
           + $Grp_ID(0-255): CLAHbceGrpIDFlow
Reg Desc   :
This register configures Group that Flowid (or Pseudowire ID) is enable or not

------------------------------------------------------------------------------*/
#define cAf6Reg_cla_per_grp_enb_Base                                                                0x0000A000
#define cAf6Reg_cla_per_grp_enb(WorkingID, GrpID)                       (0x0000A000+(WorkingID)*0x100+(GrpID))
#define cAf6Reg_cla_per_grp_enb_WidthVal                                                                    32
#define cAf6Reg_cla_per_grp_enb_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: CLAGrpPWEn
BitField Type: RW
BitField Desc: This indicate the FlowID (or Pseudowire ID) is enable or not 1:
FlowID enable 0: FlowID disable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Bit_Start                                                            0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Bit_End                                                              0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Mask                                                             cBit0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_Shift                                                                0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_MaxVal                                                             0x1
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_MinVal                                                             0x0
#define cAf6_cla_per_grp_enb_CLAGrpPWEn_RstVal                                                             0x0

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210021MODULECLAREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210021ModuleClaPwV2.c
 *
 * Created Date: Jun 2, 2015
 *
 * Description : CLA Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/pw/ThaModuleClaPwV2Reg.h"
#include "../../../default/cla/hbce/ThaHbceMemoryCell.h"
#include "../../../default/cla/hbce/ThaHbceMemoryPool.h"
#include "../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "Tha60210021ModuleCla.h"
#include "../man/Tha60210021Device.h"
#include "Tha60210021ModuleClaReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210021ModuleCla
    {
    tThaPdhPwProductModuleCla super;
    }tTha60210021ModuleCla;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleClaPwV2Methods m_ThaModuleClaPwV2Override;
static tThaModuleClaMethods     m_ThaModuleClaOverride;

/* Save super implementation */
static const tThaModuleClaPwV2Methods *m_ThaModuleClaPwV2Methods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CDRPwLookupCtrlCdrDisMask(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cBit6;
    }

static uint8  CDRPwLookupCtrlCdrDisShift(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return 6;
    }

static uint32 CDRPwLookupCtrlLineIdMask(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cBit5_0;
    }

static uint8 CDRPwLookupCtrlLineIdShift(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 BaseAddress(void)
    {
    return 0x440000;
    }

static uint32 HspwPerGrpEnbReg(ThaModuleClaPwV2 self, eBool isWorking, uint32 groupIdx)
    {
    AtUnused(self);
    return cAf6Reg_cla_per_grp_enb_Base + (isWorking ? 1:0)*0x100UL + groupIdx + BaseAddress();
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return Tha60210021ClaPwControllerNew(self);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void OverrideThaModuleClaPwV2(ThaModuleClaPwV2 self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleClaPwV2Methods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaPwV2Override, m_ThaModuleClaPwV2Methods, sizeof(m_ThaModuleClaPwV2Override));

        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlCdrDisMask);
        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlCdrDisShift);
        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlLineIdMask);
        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlLineIdShift);

        mMethodOverride(m_ThaModuleClaPwV2Override, HspwPerGrpEnbReg);
        }

    mMethodsSet(self, &m_ThaModuleClaPwV2Override);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleClaPwV2((ThaModuleClaPwV2)self);
    OverrideThaModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210021ModuleCla);
    }

static AtModule Tha60210021ModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210021ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210021ModuleClaObjectInit(newModule, device);
    }


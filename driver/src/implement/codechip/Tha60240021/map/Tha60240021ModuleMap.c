/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60240021ModuleMap.c
 *
 * Created Date: May 7, 2015
 *
 * Description : Tha60240011 module MAP implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/map/ThaModuleMapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegMapChnCtrl        0x1A0800
#define cThaRegMapTimingRefCtrl  0x1A1000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tThaModuleMapMethods m_ThaModuleMapOverride;

/* Save super implementations */
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tThaModuleMapMethods *m_ThaModuleMapMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineRegAdress(ThaModuleMap, MapTimingRefCtrl)

static uint32 De1MapChnCtrl(ThaModuleMap self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return 0x1A0800;
    }

static void OverrideThaModuleMap(AtModule self)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, m_ThaModuleMapMethods, sizeof(m_ThaModuleMapOverride));

        mMethodOverride(m_ThaModuleMapOverride, De1MapChnCtrl);
        mMethodOverride(m_ThaModuleMapOverride, MapTimingRefCtrl);
        }

    mMethodsSet(mapModule, &m_ThaModuleMapOverride);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);
    if ((localAddress >= 0x1A0800) &&
        (localAddress <= 0x1A0FFF))
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, HasRegister);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleMap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60240021ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

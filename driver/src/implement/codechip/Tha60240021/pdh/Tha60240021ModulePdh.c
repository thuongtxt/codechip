/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60240021ModulePdh.c
 *
 * Created Date: May 5, 2015
 *
 * Description : Tha60240021 PDH module implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaPdhPwProduct/pdh/ThaPdhPwProductModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePdhMethods m_AtModulePdhOverride;
static tThaModulePdhMethods m_ThaModulePdhOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPwProductModulePdh);
    }

static uint32 NumberOfDe1sGet(AtModulePdh self)
    {
    AtUnused(self);
    return 128;
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    AtModulePdh module = (AtModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(module), sizeof(m_AtModulePdhOverride));
        mMethodOverride(m_AtModulePdhOverride, NumberOfDe1sGet);
        }

    mMethodsSet(module, &m_AtModulePdhOverride);
    }

static eBool TxLiuIsPositiveClockInvert(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 De1RxFrmrPerChnCurrentAlmBase(ThaModulePdh self)
    {
    AtUnused(self);
    return 0x55080;
    }

static uint32 De1RxFrmrPerChnInterruptAlmBase(ThaModulePdh self)
    {
    AtUnused(self);
    return 0x55040;
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(module), sizeof(m_ThaModulePdhOverride));
        mMethodOverride(m_ThaModulePdhOverride, TxLiuIsPositiveClockInvert);
        mMethodOverride(m_ThaModulePdhOverride, De1RxFrmrPerChnCurrentAlmBase);
        mMethodOverride(m_ThaModulePdhOverride, De1RxFrmrPerChnInterruptAlmBase);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModulePdhObjectInit((AtModulePdh)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60240021ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

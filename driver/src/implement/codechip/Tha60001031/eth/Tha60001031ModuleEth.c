/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60001031ModuleEth.c
 *
 * Created Date: Jan 7, 2015
 *
 * Description : ETH module of product 60001031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/eth/ThaStmPwProductModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60001031ModuleEth
    {
    tThaStmPwProductModuleEth super;
    }tTha60001031ModuleEth;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods  m_ThaModuleEthOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60001031ModuleEth);
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
	AtUnused(self);
    return 1;
    }

static AtSerdesController PortSerdesControllerCreate(ThaModuleEth self, AtEthPort ethPort)
    {
	AtUnused(ethPort);
	AtUnused(self);
    /* This product do not support serdes interface, it used telecom bus */
    return NULL;
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, PortSerdesControllerCreate);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    }

static AtModuleEth ObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleEthObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60001031ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

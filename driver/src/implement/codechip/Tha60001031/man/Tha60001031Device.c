/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60001031Device.c
 *
 * Created Date: Jan 7, 2015
 *
 * Description : Product 60001031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/man/ThaStmPwProductDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60001031Device
    {
    tThaStmPwProductDevice super;
    }tTha60001031Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods  m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 MaxNumParts(ThaDevice self)
    {
	AtUnused(self);
    return 1;
    }

static uint8 NumPartsOfModule(ThaDevice self, eAtModule moduleId)
    {
	AtUnused(moduleId);
    return ThaDeviceMaxNumParts(self);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModuleSdh)  return (AtModule)Tha60001031ModuleSdhNew(self);
    if (moduleId  == cAtModuleEth)  return (AtModule)Tha60001031ModuleEthNew(self);
    if (moduleId  == cAtModuleRam)  return (AtModule)Tha60001031ModuleRamNew(self);
    if (moduleId  == cAtModulePw)   return (AtModule)Tha60001031ModulePwNew(self);

    if (phyModule == cThaModuleCdr) return Tha60001031ModuleCdrNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eBool NeedCheckSSKey(AtDevice self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionWithNewOcn(ThaDevice self)
    {
	AtUnused(self);
    return 0x0;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60001031Device);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, NumPartsOfModule);
        mMethodOverride(m_ThaDeviceOverride, MaxNumParts);
        mMethodOverride(m_ThaDeviceOverride, StartVersionWithNewOcn);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, NeedCheckSSKey);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60001031DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SERDES
 *
 * File        : Tha60001031SdhLineSerdesController.c
 *
 * Created Date: Jan 27, 2015
 *
 * Description : Serdes controller of product 60001031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/physical/ThaStmPwProductSdhLineSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cPhyBaseAddress         0xf40800
#define cSerdesLoopInOffset     0x61
#define cSerdesLockToRefOffset  0x64
#define cSerdesLockToDataOffset 0x65

#define cLoopbackMask(portId)  (cBit0 << (portId))
#define cLoopbackShift(portId) (portId)

#define cAtSdhLineSerdesSubLoopin 0xF00044
#define cAtSdhLineSerdesSubLoopinMask(lineId)  (cBit0 << lineId)
#define cAtSdhLineSerdesSubLoopinShift(lineId) (lineId)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60001031SdhLineSerdesController
    {
    tThaStmPwProductSdhLineSerdesController super;
    }tTha60001031SdhLineSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSerdesControllerMethods        m_ThaSerdesControllerOverride;
static tThaSdhLineSerdesControllerMethods m_ThaSdhLineSerdesControllerOverride;
static tAtSerdesControllerMethods         m_AtSerdesControllerOverride;

/* Save super implement */
static const tThaSerdesControllerMethods *m_ThaSerdesControllerMethods=NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaSerdesController self)
    {
	AtUnused(self);
    return 0xf40000;
    }

static uint32 LoopInRegAddress(ThaSerdesController self)
    {
	AtUnused(self);
    return cPhyBaseAddress + 0x61;
    }

static uint32 LoopInMask(ThaSerdesController self)
    {
    return cLoopbackMask(AtSerdesControllerIdGet((AtSerdesController)self));
    }

static uint32 LoopInShift(ThaSerdesController self)
    {
    return cLoopbackShift(AtSerdesControllerIdGet((AtSerdesController)self));
    }

static uint32 LocalLineId(ThaSerdesController self)
    {
    AtSdhLine line = (AtSdhLine)AtSerdesControllerPhysicalPortGet((AtSerdesController)self);
    return ThaModuleSdhLineLocalId(line);
    }

static uint32 LockToDataRegAddress(ThaSdhLineSerdesController self)
    {
	AtUnused(self);
    return cPhyBaseAddress + 0x64;
    }

static uint32 LockToDataRegMask(ThaSdhLineSerdesController self)
    {
    return cBit0 << LocalLineId((ThaSerdesController)self);
    }

static uint32 LockToDataRegShift(ThaSdhLineSerdesController self)
    {
    return LocalLineId((ThaSerdesController)self);
    }

static uint32 LockToRefRegAddress(ThaSdhLineSerdesController self)
    {
	AtUnused(self);
    return cPhyBaseAddress + 0x65;
    }

static uint32 LockToRefRegMask(ThaSdhLineSerdesController self)
    {
    return cBit0 << LocalLineId((ThaSerdesController)self);
    }

static uint32 LockToRefRegShift(ThaSdhLineSerdesController self)
    {
    return LocalLineId((ThaSerdesController)self);
    }

static uint32 PartOffset(ThaSerdesController self)
    {
	AtUnused(self);
    return 0x0;
    }

static eAtRet LoopInEnable(ThaSerdesController self, eBool enable)
    {
    uint32 regVal, lineId;
    eAtRet ret = m_ThaSerdesControllerMethods->LoopInEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    lineId = AtSerdesControllerIdGet((AtSerdesController)self);
    regVal = AtSerdesControllerRead((AtSerdesController)self, cAtSdhLineSerdesSubLoopin, cAtModuleSdh);
    mFieldIns(&regVal, cAtSdhLineSerdesSubLoopinMask(lineId), cAtSdhLineSerdesSubLoopinShift(lineId), enable);
    AtSerdesControllerWrite((AtSerdesController)self, cAtSdhLineSerdesSubLoopin, regVal, cAtModuleSdh);
    return ret;
    }

static eBool CanControlTimingMode(AtSerdesController self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaSdhLineSerdesController(AtSerdesController self)
    {
    ThaSdhLineSerdesController sdhLineSerdes = (ThaSdhLineSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineSerdesControllerOverride, mMethodsGet(sdhLineSerdes), sizeof(m_ThaSdhLineSerdesControllerOverride));

        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegAddress);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegMask);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToDataRegShift);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegAddress);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegMask);
        mMethodOverride(m_ThaSdhLineSerdesControllerOverride, LockToRefRegShift);
        }

    mMethodsSet(sdhLineSerdes, &m_ThaSdhLineSerdesControllerOverride);
    }

static void OverrideThaSerdesController(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSerdesControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaSerdesControllerOverride));

        mMethodOverride(m_ThaSerdesControllerOverride, BaseAddress);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInRegAddress);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInMask);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInShift);
        mMethodOverride(m_ThaSerdesControllerOverride, PartOffset);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInEnable);
        }

    mMethodsSet(controller, &m_ThaSerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, CanControlTimingMode);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideThaSerdesController(self);
    OverrideThaSdhLineSerdesController(self);
    OverrideAtSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60001031SdhLineSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductSdhLineSerdesControllerObjectInit(self, sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60001031SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newController, sdhLine, serdesId);
    }

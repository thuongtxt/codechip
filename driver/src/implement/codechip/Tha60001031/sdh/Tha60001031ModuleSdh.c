/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60001031ModuleSdh.c
 *
 * Created Date: Jan 7, 2015
 *
 * Description : SDH module of product 60001031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/sdh/ThaStmPwProductModuleSdhInternal.h"
#include "../../../default/sdh/ThaModuleSdhReg.h"
#include "../../../default/physical/ThaSdhLineSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60001031ModuleSdh
    {
    tThaStmPwProductModuleSdh super;
    }tTha60001031ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods  m_AtModuleSdhOverride;
static tThaModuleSdhMethods  m_ThaModuleSdhOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60001031ModuleSdh);
    }

static uint8 MaxLinesGet(AtModuleSdh self)
    {
	AtUnused(self);
    return 4;
    }

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
	AtUnused(self);
    return Tha60001031SdhLineSerdesControllerNew(line, serdesId);
    }

static uint8 SerdesCoreIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
	AtUnused(self);
    return (uint8)AtChannelIdGet((AtChannel)line);
    }

static uint8 SerdesIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
    return SerdesCoreIdOfLine(self, line);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, mMethodsGet(self), sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, MaxLinesGet);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, mMethodsGet(sdhModule), sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesIdOfLine);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesCoreIdOfLine);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60001031ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61210031PrbsEnginePw.c
 *
 * Created Date: Aug 25, 2015
 *
 * Description : PW PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha61210031PrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha61150011PrbsEnginePwMethods m_Tha61150011PrbsEnginePwOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha61210031PrbsEnginePw);
    }

static uint32 PrbsEnableRegister(Tha61150011PrbsEnginePw self)
    {
    AtUnused(self);
    return cThaPrbsEnable;
    }

static uint32 PrbsStatusRegister(Tha61150011PrbsEnginePw self)
    {
    AtUnused(self);
    return cThaPrbsStatus;
    }

static uint32 PrbsSliceOffset(Tha61150011PrbsEnginePw self, uint8 slice)
    {
    AtUnused(self);
    return slice * 0x8000UL;
    }

static void OverrideTha61150011PrbsEnginePw(AtPrbsEngine self)
    {
    Tha61150011PrbsEnginePw prbs = (Tha61150011PrbsEnginePw)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61150011PrbsEnginePwOverride, mMethodsGet(prbs), sizeof(m_Tha61150011PrbsEnginePwOverride));

        mMethodOverride(m_Tha61150011PrbsEnginePwOverride, PrbsEnableRegister);
        mMethodOverride(m_Tha61150011PrbsEnginePwOverride, PrbsStatusRegister);
        mMethodOverride(m_Tha61150011PrbsEnginePwOverride, PrbsSliceOffset);
        }

    mMethodsSet(prbs, &m_Tha61150011PrbsEnginePwOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha61150011PrbsEnginePw(self);
    }

static AtPrbsEngine Tha61210031PrbsEnginePwObjectInit(AtPrbsEngine self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61150011PrbsEnginePwObjectInit(self, pw) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha61210031PrbsEnginePwNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha61210031PrbsEnginePwObjectInit(newEngine, pw);
    }

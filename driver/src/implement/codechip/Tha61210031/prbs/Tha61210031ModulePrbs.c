/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha61210031ModulePrbs.c
 *
 * Created Date: Aug 25, 2015
 *
 * Description : PRBS of 6120031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha61150011/prbs/Tha61150011ModulePrbs.h"

#include "Tha61210031PrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha612110031ModulePrbs
    {
    tTha61150011ModulePrbs super;
    }tTha612110031ModulePrbs;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods              m_AtModuleOverride;
static tTha61031031ModulePrbsMethods m_Tha61031031ModulePrbsOverride;

/* Save super implementation */
static const tAtModuleMethods     *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(AtModule self)
    {
    return AtModuleDeviceGet(self);
    }

static void DefaultSet(AtModule self)
    {
    const uint8 cNumSlice = 2;
    uint32 numPw = AtModulePwMaxPwsGet((AtModulePw)AtDeviceModuleGet(Device(self), cAtModulePw));
    uint32 pw_i;
    uint8 slice_i;
    uint32 address;

    /* This product supports two set of prbs registers according two OCN slice */
    for (slice_i = 0; slice_i < cNumSlice; slice_i++)
        {
        for (pw_i = 0; pw_i < numPw; pw_i++)
            {
            address  = cThaPrbsEnable + pw_i + slice_i * 0x1000UL;
            mModuleHwWrite(self, address, 0x0);
            }
        }
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    DefaultSet(self);
    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static AtPrbsEngine PwPrbsEngineObjectCreate(Tha61031031ModulePrbs self, AtPw pw)
    {
    AtUnused(self);
    return Tha61210031PrbsEnginePwNew(pw);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideTha61031031ModulePrbs(AtModulePrbs self)
    {
    Tha61031031ModulePrbs prbsModule = (Tha61031031ModulePrbs)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031ModulePrbsOverride, mMethodsGet(prbsModule), sizeof(m_Tha61031031ModulePrbsOverride));

        mMethodOverride(m_Tha61031031ModulePrbsOverride, PwPrbsEngineObjectCreate);
        }

    mMethodsSet(prbsModule, &m_Tha61031031ModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtModule(self);
    OverrideTha61031031ModulePrbs(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha612110031ModulePrbs);
    }

static AtModulePrbs Tha61210031ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61150011ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha61210031ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha61210031ModulePrbsObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha61210031PrbsEngine.h
 * 
 * Created Date: Aug 25, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA61210031PRBSENGINE_H_
#define _THA61210031PRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha61150011/prbs/Tha61150011PrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cThaPrbsEnable 0x247000
#define cThaPrbsStatus 0x247800

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha61210031PrbsEnginePw
    {
    tTha61150011PrbsEnginePw super;
    }tTha61210031PrbsEnginePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA61210031PRBSENGINE_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha61210031Device.c
 *
 * Created Date: Jun 8, 2015
 *
 * Description : Product 61210031 (This product is used to test 60210031 with STM16 interface)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/man/Tha60210031Device.h"
#include "../../Tha61031031/prbs/Tha61031031ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tAtObjectMethods  m_AtObjectOverride;

/* Super implementation */
static const tAtDeviceMethods  *m_AtDeviceMethods  = NULL;
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModuleSdh)  return (AtModule)Tha61210031ModuleSdhNew(self);
    if (moduleId  == cAtModulePdh)  return (AtModule)Tha61210031ModulePdhNew(self);
    if (moduleId  == cAtModulePrbs) return (AtModule)Tha61210031ModulePrbsNew(self);
    if (moduleId  == cAtModuleEth)  return (AtModule)Tha61210031ModuleEthNew(self);

    if (phyModule == cThaModulePoh) return Tha60150011ModulePohNew(self);
    if (phyModule == cThaModuleOcn) return Tha60150011ModuleOcnNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eBool ShouldCheckHwResource(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, ShouldCheckHwResource);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static const char *ToString(AtObject self)
    {
    static char string[32];

    AtSprintf(string, "Device 61210031: 0x%p", (void *)self);
    return string;
    }

static void OverrideAtObject(AtDevice self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet((AtObject)self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    /* Change behavior of super class level 1 */
    mMethodsGet((AtObject)self) = &m_AtObjectOverride;
    }

static void Override(AtDevice self)
    {
    OverrideAtObject(self);
    OverrideAtDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031Device);
    }

static AtDevice Tha61210031DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha61210031DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha61210031DeviceObjectInit(newDevice, driver, productCode);
    }

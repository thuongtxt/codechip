/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60071011IpCore.c
 *
 * Created Date: Jul 24, 2013
 *
 * Description : IP Core of product 60071011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaIpCoreInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60071011IpCore
    {
    tThaIpCore super;
    }tTha60071011IpCore;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Save super implementation */
static const tThaIpCoreMethods *m_ThaIpCoreMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60071011IpCore);
    }

static eBool FastRamTestIsEnabled(ThaIpCore self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaIpCore(AtIpCore self)
    {
    ThaIpCore core = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreMethods = mMethodsGet(core);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreMethods, sizeof(m_ThaIpCoreOverride));
        mMethodOverride(m_ThaIpCoreOverride, FastRamTestIsEnabled);
        }

    mMethodsSet(core, &m_ThaIpCoreOverride);
    }

static void Override(AtIpCore self)
    {
    OverrideThaIpCore(self);
    }

static AtIpCore ObjectInit(AtIpCore self, uint8 coreId, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaIpCoreObjectInit((AtIpCore)self, coreId, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIpCore Tha60071011IpCoreNew(uint8 coreId, AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtIpCore newCore = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newCore == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newCore, coreId, device);
    }

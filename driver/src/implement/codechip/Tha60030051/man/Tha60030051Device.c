/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60030051Device.c
 *
 * Created Date: May 6, 2013
 *
 * Description : Product 60030051 (8xSTM-1)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaStmMlppp/man/ThaStmMlpppDeviceInternal.h"
#include "../../../default/ber/ThaModuleBer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030051Device
    {
    tThaStmMlpppDevice super;

    /* Private data */
    }tTha60030051Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030051Device);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId == cAtModuleEth) return (AtModule)Tha60030051ModuleEthNew(self);
    if (moduleId == cAtModuleRam) return (AtModule)Tha60030022ModuleRamNew(self);
    if (moduleId == cAtModuleSdh) return (AtModule)Tha60030051ModuleSdhNew(self);
    if (moduleId == cAtModuleBer) return (AtModule)ThaStmModuleBerNew(self, cThaModuleBerDefaultMaxNumEngines);

    if (phyModule == cThaModuleCla) return Tha60030051ModuleClaPppNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePpp,
                                                 cAtModuleEncap,
                                                 cAtModuleEth,
                                                 cAtModuleSdh,
                                                 cAtModuleRam,
                                                 cAtModuleBer,
                                                 cAtModuleClock,
                                                 cAtModulePktAnalyzer};
	AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    if (moduleId == cAtModuleBer)
        return cAtTrue;
    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return Tha60030051SSKeyCheckerNew(self);
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
	AtUnused(self);
    return Tha60030051IntrControllerNew(core);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(m_AtDeviceOverride));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmMlpppDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60030051DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60030051EthFlowHeaderProvider.c
 *
 * Created Date: Nov 28, 2013
 *
 * Description : ETH flow header provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/controller/ThaEthFlowHeaderProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030051EthFlowHeaderProvider
    {
    tThaEthFlowHeaderProvider super;
    }tTha60030051EthFlowHeaderProvider;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowHeaderProviderMethods m_ThaEthFlowHeaderProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 PsnMode(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    return 0xD;
    }

static uint16 OamEthHeaderNoVlanLengthGet(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    /* Default Ethernet header includes DA, SA */
    return cNumByteOfDestMac + cNumByteOfSourceMac;
    }

static eBool OamFlowHasEthType(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void Override(ThaEthFlowHeaderProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowHeaderProviderOverride, mMethodsGet(self), sizeof(m_ThaEthFlowHeaderProviderOverride));

        mMethodOverride(m_ThaEthFlowHeaderProviderOverride, PsnMode);
        mMethodOverride(m_ThaEthFlowHeaderProviderOverride, OamEthHeaderNoVlanLengthGet);
        mMethodOverride(m_ThaEthFlowHeaderProviderOverride, OamFlowHasEthType);
        }

    mMethodsSet(self, &m_ThaEthFlowHeaderProviderOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030051EthFlowHeaderProvider);
    }

static ThaEthFlowHeaderProvider ObjectInit(ThaEthFlowHeaderProvider self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthFlowHeaderProviderObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowHeaderProvider Tha60030051EthFlowHeaderProviderNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowHeaderProvider newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newProvider, ethModule);
    }

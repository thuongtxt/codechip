/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60030051ClaEthFlowController.c
 *
 * Created Date: Aug 4, 2014
 *
 * Description : CLA Ethernet Flow Controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/controllers/ThaClaEthFlowControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030051ClaEthFlowController
    {
    tThaClaEthFlowController super;
    }tTha60030051ClaEthFlowController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaEthFlowControllerMethods  m_ThaClaEthFlowControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/
uint8 Tha60030051ClaEthFlowControllerQueueHwPriority(ThaClaEthFlowController self, uint8 queueId);

/*--------------------------- Implementation ---------------------------------*/
static uint8 QueueHwPriority(ThaClaEthFlowController self, uint8 queueId)
    {
	AtUnused(self);
    return Tha60030051ClaEthFlowControllerQueueHwPriority(self, queueId);
    }

static eBool NeedInitializeQueuePriority(ThaClaEthFlowController self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030051ClaEthFlowController);
    }

static void OverrideThaClaEthFlowController(ThaClaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthFlowControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthFlowControllerOverride));
        mMethodOverride(m_ThaClaEthFlowControllerOverride, QueueHwPriority);
        mMethodOverride(m_ThaClaEthFlowControllerOverride, NeedInitializeQueuePriority);
        }

    mMethodsSet(self, &m_ThaClaEthFlowControllerOverride);
    }

static void Override(ThaClaEthFlowController self)
    {
    OverrideThaClaEthFlowController(self);
    }

static ThaClaEthFlowController ObjectInit(ThaClaEthFlowController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaEthFlowControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthFlowController Tha60030051ClaEthFlowControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthFlowController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cla);
    }

uint8 Tha60030051ClaEthFlowControllerQueueHwPriority(ThaClaEthFlowController self, uint8 queueId)
    {
    static const uint8 cHighPriority = 0;
    static const uint8 cLowPriority  = 1;
    AtUnused(self);

    if ((queueId == 7) || (queueId == 6))
        return cHighPriority;

    return cLowPriority;
    }

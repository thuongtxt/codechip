/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60030051ModuleSdh.c
 *
 * Created Date: May 8, 2013
 *
 * Description : SDH module of product 60030051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaStmMlppp/sdh/ThaStmMlpppModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030051ModuleSdh
    {
    tThaStmMlpppModuleSdh super;
    }tTha60030051ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods  m_AtModuleSdhOverride;
static tThaModuleSdhMethods m_ThaModuleSdhOverride;

/* Save super implementations */
static const tAtModuleSdhMethods  *m_AtModuleSdhMethods  = NULL;
static const tThaModuleSdhMethods *m_ThaModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasLineInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
	AtUnused(ipCore);
	AtUnused(self);
    return (glbIntr & cBit17) ? cAtFalse : cAtTrue; /* Active low */
    }

static eBool HasStsInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
	AtUnused(ipCore);
	AtUnused(glbIntr);
	AtUnused(self);
    return cAtFalse;
    }

static eBool HasVtInterrupt(ThaModuleSdh self, uint32 glbIntr, AtIpCore ipCore)
    {
	AtUnused(ipCore);
	AtUnused(glbIntr);
	AtUnused(self);
    return cAtFalse;
    }

static eBool SsBitCompareIsEnabledAsDefault(AtModuleSdh self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtSerdesTimingMode DefaultSerdesTimingModeForLine(ThaModuleSdh self, AtSdhLine line)
    {
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm4)
        return cAtSerdesTimingModeAuto;
    return m_ThaModuleSdhMethods->DefaultSerdesTimingModeForLine(self, line);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, HasLineInterrupt);
        mMethodOverride(m_ThaModuleSdhOverride, HasStsInterrupt);
        mMethodOverride(m_ThaModuleSdhOverride, HasVtInterrupt);
        mMethodOverride(m_ThaModuleSdhOverride, DefaultSerdesTimingModeForLine);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));
        mMethodOverride(m_AtModuleSdhOverride, SsBitCompareIsEnabledAsDefault);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030051ModuleSdh);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmMlpppModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60030051ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60030101SSKeyChecker.c
 *
 * Created Date: Mar 6, 2015
 *
 * Description : Product 60030080
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60031032/man/Tha60031032SSKeyChecker.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030101SSKeyChecker
    {
    tTha60031032SSKeyChecker super;
    }tTha60030101SSKeyChecker;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSSKeyCheckerMethods m_AtSSKeyCheckerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030101SSKeyChecker);
    }

static uint32 CodeExpectedLsb(AtSSKeyChecker self)
    {
	AtUnused(self);
    return 0x80261001;
    }

static void OverrideAtSSKeyChecker(AtSSKeyChecker self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSSKeyCheckerOverride, mMethodsGet(self), sizeof(m_AtSSKeyCheckerOverride));

        mMethodOverride(m_AtSSKeyCheckerOverride, CodeExpectedLsb);
        }

    mMethodsSet(self, &m_AtSSKeyCheckerOverride);
    }

static void Override(AtSSKeyChecker self)
    {
    OverrideAtSSKeyChecker(self);
    }

static AtSSKeyChecker ObjectInit(AtSSKeyChecker self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032SSKeyCheckerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSSKeyChecker Tha60030101SSKeyCheckerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSSKeyChecker newChecker = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newChecker, device);
    }

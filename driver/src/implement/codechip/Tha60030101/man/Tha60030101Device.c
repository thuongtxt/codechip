/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device Management
 *
 * File        : Tha60030101Device.c
 *
 * Created Date: Mar 4, 2015
 *
 * Description : Device management of product 60030101
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60031032/man/Tha60031032CommonDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030101Device
    {
    tTha60031032CommonDevice super;
    }tTha60030101Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModuleSdh)         return (AtModule)Tha60030101ModuleSdhNew(self);
    if (moduleId  == cAtModuleClock)       return (AtModule)Tha60030101ModuleClockNew(self);
    if (moduleId  == cAtModuleEth)         return (AtModule)Tha60030101ModuleEthNew(self);

    if (phyModule == cThaModuleCla)        return Tha60030101ModuleClaPppNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static uint8 NumPartsOfModule(ThaDevice self, eAtModule moduleId)
    {
	AtUnused(moduleId);
	AtUnused(self);
    /* Every module has two parts */
    return 2;
    }

static uint8 MaxNumParts(ThaDevice self)
    {
	AtUnused(self);
    return 2;
    }

static eBool CanHaveNewOcn(ThaDevice self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionWithNewOcn(ThaDevice self)
    {
	AtUnused(self);
    return 0x0;
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return Tha60030101SSKeyCheckerNew(self);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, mMethodsGet(self), sizeof(m_AtDeviceOverride));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, NumPartsOfModule);
        mMethodOverride(m_ThaDeviceOverride, MaxNumParts);
        mMethodOverride(m_ThaDeviceOverride, CanHaveNewOcn);
        mMethodOverride(m_ThaDeviceOverride, StartVersionWithNewOcn);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideThaDevice(self);
    OverrideAtDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030101Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Initialize its super */
    if (Tha60031032CommonDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60030101DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Constructor */
    return ObjectInit(newDevice, driver, productCode);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60030101ModuleSdh.c
 *
 * Created Date: Mar 4, 2015
 *
 * Description : SDH Module of product 60030101
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/physical/ThaSerdesController.h"

#include "../../Tha60031032/sdh/Tha60031032ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030101ModuleSdh
    {
    tTha60031032ModuleSdh super;
    }tTha60030101ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleSdhMethods m_ThaModuleSdhOverride;

/* Save super implementation */
static const tThaModuleSdhMethods *m_ThaModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern AtSerdesController Tha60030081SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId);

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030101ModuleSdh);
    }

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
    if (ThaDeviceIsEp((ThaDevice)AtModuleDeviceGet((AtModule)self)))
        return ThaSdhLineSerdesControllerCyclone4New(line, serdesId);

    return Tha60030081SdhLineSerdesControllerNew(line, serdesId);
    }

static uint8 SerdesCoreIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)line);
	AtUnused(self);

    if (lineId == 0) return 0;
    if (lineId == 1) return 1;
    if (lineId == 2) return 2;
    if (lineId == 3) return 3;
    if (lineId == 4) return 0x8;
    if (lineId == 5) return 0x9;
    if (lineId == 6) return 0xA;
    if (lineId == 7) return 0xB;

    /* Return invalid port */
    return 255;
    }

static uint8 SerdesIdOfLine(ThaModuleSdh self, AtSdhLine line)
    {
    return SerdesCoreIdOfLine(self, line);
    }

static eBool BlockErrorCountersSupported(ThaModuleSdh self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesCoreIdOfLine);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesIdOfLine);
        mMethodOverride(m_ThaModuleSdhOverride, BlockErrorCountersSupported);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideThaModuleSdh(self);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60030101ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

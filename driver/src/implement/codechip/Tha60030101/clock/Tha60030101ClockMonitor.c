/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLOCK
 *
 * File        : Tha60030101ClockMonitor.c
 *
 * Created Date: Mar 9, 2015
 *
 * Description : Clock monitor of 60030101
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../default/ThaStmPwProduct/clock/ThaStmPwProductClockMonitor.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030101ClockMonitor
    {
    tThaStmPwProductClockMonitor super;
    }tTha60030101ClockMonitor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClockMonitorMethods m_ThaClockMonitorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *LocalBusClockExpectedFrequenciesInKhz(ThaClockMonitor self, uint8 *numFrequencies)
    {
    static uint32 expectedFrequencies[] = {66666};
	AtUnused(self);
    if (numFrequencies)
        *numFrequencies = mCount(expectedFrequencies);
    return expectedFrequencies;
    }

static void OverrideThaClockMonitor(ThaClockMonitor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockMonitorOverride, mMethodsGet(self), sizeof(m_ThaClockMonitorOverride));

        mMethodOverride(m_ThaClockMonitorOverride, LocalBusClockExpectedFrequenciesInKhz);
        }

    mMethodsSet(self, &m_ThaClockMonitorOverride);
    }

static void Override(ThaClockMonitor self)
    {
    OverrideThaClockMonitor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030101ClockMonitor);
    }

static ThaClockMonitor ObjectInit(ThaClockMonitor self, AtModuleClock clockModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductClockMonitorObjectInit(self, clockModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClockMonitor Tha60030101ClockMonitorNew(AtModuleClock clockModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClockMonitor newMonitor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newMonitor, clockModule);
    }

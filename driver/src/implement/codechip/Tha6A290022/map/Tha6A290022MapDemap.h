/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP/DEMAP
 * 
 * File        : Tha6A290022MapDemap.h
 * 
 * Created Date: Apr 18, 2018
 *
 * Description : Common function for MAP/DEMAP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022MAPDEMAP_H_
#define _THA6A290022MAPDEMAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/map/Tha60290022ModuleMapInternal.h"
#include "../../Tha60290022/map/Tha60290022ModuleDemapInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290022ModuleDemap
    {
	tTha60290022ModuleDemap super;
    }tTha6A290022ModuleDemap;


typedef struct tTha6A290022ModuleMap
	{
	tTha60290022ModuleMap super;
	}tTha6A290022ModuleMap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool SdhChannelBelongsToLoLine(AtSdhChannel sdhChannel);
eBool SdhChannelIsVc3(AtSdhChannel sdhChannel);
eBool SdhChannelIsVcx(AtSdhChannel sdhChannel);
eBool SdhChannelIsTu3Vc3(AtSdhChannel sdhChannel);
AtModule Tha6A290022ModuleDemapObjectInit(AtModule self, AtDevice device);
AtModule Tha6A290022ModuleMapObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022MAPDEMAP_H_ */


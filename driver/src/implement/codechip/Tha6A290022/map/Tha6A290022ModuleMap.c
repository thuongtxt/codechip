/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha6A290022ModuleMap.c
 *
 * Created Date: March 13, 2018
 *
 * Description : Map module of 6A290022
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290022MapDemap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;

/* Save super implementation */
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedEnableAfterBindToPw(ThaModuleAbstractMap self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtTrue;
    }

static eAtRet HwBindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, uint32 pwId)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint8 numSts = AtSdhChannelNumSts((AtSdhChannel)vc);
    uint8 sts_i;
    uint8 startSts = AtSdhChannelSts1Get((AtSdhChannel)vc);
    uint32 mask, shift;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 offset  = mMethodsGet(self)->AuVcStsDefaultOffset(self, vc, (uint8)(startSts + sts_i));
        uint32 address = mMethodsGet(mapModule)->VcMapChnCtrl(mapModule, vc) + offset;
        uint32 regVal  = mChannelHwRead(vc, address, cThaModuleMap);

        if (SdhChannelIsTu3Vc3((AtSdhChannel)vc))
            {
            mask  = mFieldMask(mapModule, MapTsEn);
            shift = mFieldShift(mapModule, MapTsEn);
            mFieldIns(&regVal, mask, shift, mBoolToBin(mMethodsGet(self)->NeedEnableAfterBindToPw(self, NULL)));

            mask  = mFieldMask(mapModule, MapChnType);
            shift = mFieldShift(mapModule, MapChnType);
            mFieldIns(&regVal, mask, shift, 6);

            mask  = mFieldMask(mapModule, MapPWIdField);
            shift = mFieldShift(mapModule, MapPWIdField);
            mFieldIns(&regVal, mask, shift, pwId);
            }
        else
            {
            mask = cBit11_10;
            shift = 10;
            mFieldIns(&regVal, mask  , shift, 0); /* CEP */

            /*0:slave of VC3_N3c  1:master of VC3-N3c*/
            mask  = cBit9;
            shift = 9;
            mFieldIns(&regVal, mask  , shift, sts_i==0?1:0);

            /* Mapsrc_type*/
            mask  = cBit8;
            shift = 8;
            mFieldIns(&regVal, mask  , shift, SdhChannelIsVc3((AtSdhChannel)vc)?0:1);

            /*MapChEn*/
            mask  = cBit7;
            shift = 7;
            mFieldIns(&regVal, mask  , shift, pwId==cInvalidUint32?0:1);
            }
        mChannelHwWrite(vc, address, regVal, cThaModuleMap);
        }

    return cAtOk;
    }

static eAtRet BindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, AtPw pw)
    {
    AtUnused(pw);
    return mMethodsGet(self)->HwBindAuVcToPseudowire(self, vc, 0);
    }

static eAtRet HwBindVc1xToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc1x, uint32 pwId)
    {
    uint32 address, regVal, timeslot_i;
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint16 numTimeslots = ThaModuleStmMapDemapNumTimeSlotInVc1x(vc1x);
    uint32 mask, shift;
    regVal = 0;
    address = mMethodsGet(mapModule)->VcMapChnCtrl(mapModule, vc1x) + mMethodsGet(self)->Vc1xDefaultOffset(self, vc1x);


    mask  = mFieldMask(mapModule, MapFirstTs);
    shift = mFieldShift(mapModule, MapFirstTs);
    mFieldIns(&regVal, mask, shift, 0);

    mask  = mFieldMask(mapModule, MapTsEn);
    shift = mFieldShift(mapModule, MapTsEn);
    mFieldIns(&regVal, mask, shift, mBoolToBin(mMethodsGet(self)->NeedEnableAfterBindToPw(self, NULL)));

    mask  = mFieldMask(mapModule, MapChnType);
    shift = mFieldShift(mapModule, MapChnType);
    mFieldIns(&regVal, mask, shift, 6);

    mask  = mFieldMask(mapModule, MapPWIdField);
    shift = mFieldShift(mapModule, MapPWIdField);
    mFieldIns(&regVal, mask, shift, pwId);

    /* Unbind is default value of regVal = 0*/
    mChannelHwWrite(vc1x, address, regVal, cThaModuleMap);

    /* Set default value = 0 for others timeslot */
    for (timeslot_i = 1; timeslot_i < numTimeslots; timeslot_i++)
        mChannelHwWrite(vc1x, address + timeslot_i, 0, cThaModuleMap);

    return cAtOk;
    }

static eAtRet BindVc1xToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc1x, AtPw pw)
    {
    AtUnused(pw);
    return mMethodsGet(self)->HwBindVc1xToPseudowire(self, vc1x, 0);
    }

static eAtRet HwBindDe3ToPseudowire(ThaModuleAbstractMap self, AtPdhDe3 de3, uint32 pwId)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = mMethodsGet(mapModule)->De3MapChnCtrl(mapModule, de3) + mMethodsGet(self)->De3DefaultOffset(self, de3);
    uint32 regVal = 0;
    ThaModuleMap moduleMap = (ThaModuleMap)self;
    uint32 mask, shift;

    mask  = mFieldMask(moduleMap, MapFirstTs);
    shift = mFieldShift(moduleMap, MapFirstTs);
    mFieldIns(&regVal, mask  , shift, 0);

    mask  = mFieldMask(moduleMap, MapTsEn);
    shift = mFieldShift(moduleMap, MapTsEn);
    mFieldIns(&regVal, mask  , shift, mBoolToBin(mMethodsGet(self)->NeedEnableAfterBindToPw(self, NULL)));

    mask  = mFieldMask(moduleMap, MapChnType);
    shift = mFieldShift(moduleMap, MapChnType);
    mFieldIns(&regVal, mask  , shift, 0);

    mask  = mFieldMask(moduleMap, MapPWIdField);
    shift = mFieldShift(moduleMap, MapPWIdField);
    mFieldIns(&regVal, mask  , shift, pwId);


    mChannelHwWrite(de3, address, regVal, cThaModuleMap);

    return cAtOk;
    }

static eAtRet BindDe3ToPseudowire(ThaModuleAbstractMap self, AtPdhDe3 de3, AtPw pw)
    {
    AtUnused(pw);
    return mMethodsGet(self)->HwBindDe3ToPseudowire(self, de3, 0);
    }
static uint8 GettimeslotId(uint32 ds0BitMask)
    {
    uint8 timeslotId = 0;
    uint8 i = 0;
    for (i=0; i<32; i++)
        {
        if ((ds0BitMask >> i) & cBit0)
            {
            timeslotId = i;
            break;
            }
        }
    return timeslotId;
    }

static eAtRet HwBindDs0ToPseudowire(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask, uint32 pwId)
    {
    uint8 timeslotId = GettimeslotId(ds0BitMask);
    uint32 offset;
    uint32 address, regVal;
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 bit_i = cBit0;
    uint8 isFirstSlot = 1, MapChnType = 1;
    eAtPdhDe1FrameType De1FrameType = AtPdhChannelFrameTypeGet((AtPdhChannel)de1);

    if (De1FrameType == cAtPdhDs1J1UnFrm || De1FrameType == cAtPdhE1UnFrm)
        {
        MapChnType = 0;
        isFirstSlot = 0;
        }

    timeslotId = 0;
    while (ds0BitMask != 0)
        {
        /* Only care 1-bit */
        if (ds0BitMask & bit_i)
            {
            uint32 mask, shift;
            offset = mMethodsGet(self)->Ds0DefaultOffset(self, de1, timeslotId);
            regVal = 0;
            address = mMethodsGet(mapModule)->De1MapChnCtrl(mapModule, de1) + offset;

            mask  = mFieldMask(mapModule, MapFirstTs);
            shift = mFieldShift(mapModule, MapFirstTs);
            mFieldIns(&regVal, mask, shift, isFirstSlot);

            mask  = mFieldMask(mapModule, MapTsEn);
            shift = mFieldShift(mapModule, MapTsEn);
            mFieldIns(&regVal, mask, shift, mBoolToBin(mMethodsGet(self)->NeedEnableAfterBindToPw(self, NULL)));
            mask  = mFieldMask(mapModule, MapChnType);
            shift = mFieldShift(mapModule, MapChnType);
            mFieldIns(&regVal, mask, shift, MapChnType); /* CESoP */

            mask  = mFieldMask(mapModule, MapPWIdField);
            shift = mFieldShift(mapModule, MapPWIdField);
            mFieldIns(&regVal, mask, shift, pwId);


            mChannelHwWrite(de1, address, regVal, cThaModuleMap);

            if (isFirstSlot)
                isFirstSlot = 0;


            }

        /* Next slot */
        ds0BitMask = ds0BitMask & (~bit_i);
        bit_i      = bit_i << 1;
        timeslotId = (uint8)(timeslotId + 1);
        }

    return cAtOk;
    }

static eAtRet BindDs0ToPseudowire(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask, AtPw pw)
    {
    AtUnused(pw);
    return mMethodsGet(self)->HwBindDs0ToPseudowire(self, de1, ds0BitMask, 0);
    }

static void OverrideThaModuleAbstractMap(ThaModuleAbstractMap self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, NeedEnableAfterBindToPw);
        mMethodOverride(m_ThaModuleAbstractMapOverride, HwBindAuVcToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindAuVcToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, HwBindVc1xToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindVc1xToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, HwBindDe3ToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindDe3ToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, HwBindDs0ToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindDs0ToPseudowire);
        }

    mMethodsSet(self, &m_ThaModuleAbstractMapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleAbstractMap((ThaModuleAbstractMap)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290022ModuleMap);
    }

AtModule Tha6A290022ModuleMapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A290022ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290022ModuleMapObjectInit(newModule, device);
    }

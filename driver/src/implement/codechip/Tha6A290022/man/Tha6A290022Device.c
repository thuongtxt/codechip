/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha6A290022Device.c
 *
 * Created Date: March 05, 2018
 *
 * Description : Tha6A290022 Device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290022DeviceInternal.h"
#include "../../Tha60290021/xc/Tha60290021ModuleXc.h"
#include "../ocn/Tha6A290022ModuleOcn.h"
#include "../ram/Tha6A290022ModuleRam.h"
#include "../cdr/Tha6A290022ModuleCdr.h"
#include "../physical/Tha6A290022Physical.h"
#include "Tha6A290022InterruptController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60210011DeviceMethods m_Tha60210011DeviceOverride;
static tTha60290021DeviceMethods m_Tha60290021DeviceOverride;

/* Super implementations */
static const tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static  AtSerdesManager SerdesManagerObjectCreate(AtDevice self)
    {
    return Tha6A290022SerdesManagerNew(self);
    }

static eAtRet AllSerdesReset(Tha60210011Device self)
    {
	AtUnused(self);
    return cAtOk;
    }

static eAtRet PlaHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet PdaHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet ClaHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet PweHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void UnbindAllPws(AtDevice self)
    {
	AtUnused(self);
    }

static eAtRet BerRequestDdrDisable(Tha60210011Device self)
    {
	AtUnused(self);
	return cAtOk;
    }

static eAtRet AttHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet PrbsHwFlush(Tha60210011Device self)
	{
	ThaDevice device = (ThaDevice)self;
	ThaDeviceMemoryFlush(device, 0x68000, 0x680FF, 0x0);
	ThaDeviceMemoryFlush(device, 0x68100, 0x681FF, 0x0);
	return cAtOk;
	}

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = (eThaPhyModule)moduleId;

    if (moduleId  == cAtModuleAps)   return NULL;
    if (moduleId  == cAtModuleEth)  return NULL;
    if (moduleId  == cAtModulePdh)  return (AtModule)Tha6A290022ModulePdhNew(self);
    if (moduleId  == cAtModulePrbs) return (AtModule)Tha6A290022ModulePrbsNew(self);
    if (moduleId  == cAtModulePw)   return NULL;
    if (moduleId  == cAtModuleSdh)  return (AtModule)Tha6A290022ModuleSdhNew(self);
    if (moduleId  == cAtModuleSur)  return (AtModule)Tha6A290022ModuleSurNew(self);
    if (moduleId  == cAtModuleRam)  return (AtModule)Tha6A290022ModuleRamNew(self);

    /* PhyModule */
    if (phyModule  == cThaModuleMap)   return Tha6A290022ModuleMapNew(self);
    if (phyModule  == cThaModuleDemap) return Tha6A290022ModuleDemapNew(self);
    if (phyModule  == cThaModulePwe)   return NULL;
    if (phyModule  == cThaModuleCla)   return NULL;
    if (phyModule  == cThaModulePda)   return NULL;
    if (phyModule  == cThaModuleCos)   return NULL;
    if (phyModule  == cThaModuleBmt)   return NULL;
    if (phyModule  == cThaModuleOcn)   return Tha6A290022ModuleOcnNew(self);
    if (phyModule  == cThaModuleCdr)   return Tha6A290022ModuleCdrNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eAtRet AllModulesInit(AtDevice self)
    {
    eAtRet ret = cAtOk;

    ret |= AtDeviceModuleInit(self, cAtModuleXc);
    ret |= AtDeviceModuleInit(self, cAtModuleSdh);
    ret |= AtDeviceModuleInit(self, cAtModulePdh);
    ret |= AtDeviceModuleInit(self, cAtModuleEncap);
    ret |= AtDeviceModuleInit(self, cAtModuleRam);
    ret |= AtDeviceModuleInit(self, cAtModulePrbs);
    ret |= AtDeviceModuleInit(self, cAtModuleClock);
    ret |= AtDeviceModuleInit(self, cAtModuleConcate);

    /* Initialize all physical modules */
    ret |= AtDeviceModuleInit(self, cThaModuleCdr);
    ret |= AtDeviceModuleInit(self, cThaModuleOcn);
    ret |= AtDeviceModuleInit(self, cThaModulePoh);

    return ret;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret = m_AtDeviceMethods->Init(self);
    ret |= Tha60290021ModuleXcHideModeSet((AtModuleXc)AtDeviceModuleGet(self, cAtModuleXc), cTha60290021XcHideModeDirect);
    return ret;
    }

static eBool DiagnosticModeIsEnabled(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtUnused(self);
    return Tha6A290022IntrControllerNew(core);
    }

static eBool IsRunningOnOtherPlatform(Tha60290021Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet DccHdlcPktGenInit(Tha60290021Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
	eThaPhyModule phyModule = (eThaPhyModule)moduleId;

	if (moduleId == cAtModuleAps) return cAtFalse;
	if (moduleId == cAtModuleSur) return cAtFalse;
    if (moduleId == cAtModulePw) return cAtFalse;
    if (moduleId == cAtModuleEth) return cAtFalse;

    if (phyModule == cThaModuleCla) return cAtFalse;
    if (phyModule == cThaModulePda) return cAtFalse;
    if (phyModule == cThaModulePwe) return cAtFalse;
    if (phyModule == cThaModuleCos) return cAtFalse;
    if (phyModule == cThaModuleBmt) return cAtFalse;

    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 /*cAtModulePw,
												 cAtModuleEth,*/
                                                 cAtModuleRam,
                                                 cAtModuleXc,
                                                 cAtModuleSdh,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleBer,
                                                 cAtModulePrbs,
                                                 cAtModuleClock,
                                                 cAtModuleAps};

    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, AllModulesInit);
        mMethodOverride(m_AtDeviceOverride, SerdesManagerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, DiagnosticModeIsEnabled);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(tThaDeviceMethods));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideTha60210011Device(AtDevice self)
    {
    Tha60210011Device device = (Tha60210011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210011DeviceOverride));
        mMethodOverride(m_Tha60210011DeviceOverride, AllSerdesReset);
        mMethodOverride(m_Tha60210011DeviceOverride, PweHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, PlaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, PdaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, ClaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, PrbsHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, AttHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, UnbindAllPws);
        mMethodOverride(m_Tha60210011DeviceOverride, BerRequestDdrDisable);
        }

    mMethodsSet(device, &m_Tha60210011DeviceOverride);
    }

static void OverrideTha60290021Device(AtDevice self)
    {
    Tha60290021Device device = (Tha60290021Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021DeviceOverride, mMethodsGet(device), sizeof(m_Tha60290021DeviceOverride));

        mMethodOverride(m_Tha60290021DeviceOverride, IsRunningOnOtherPlatform);
        mMethodOverride(m_Tha60290021DeviceOverride, DccHdlcPktGenInit);
        }

    mMethodsSet(device, &m_Tha60290021DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60210011Device(self);
    OverrideTha60290021Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290022Device);
    }

AtDevice Tha6A290022DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha6A290022DeviceNew(AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    return Tha6A290022DeviceObjectInit(newDevice, driver, productCode);
    }

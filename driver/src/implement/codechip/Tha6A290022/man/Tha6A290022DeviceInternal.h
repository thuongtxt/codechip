/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DEVICE
 * 
 * File        : Tha60290022Device.h
 * 
 * Created Date: March 03, 2018
 *
 * Description : Device Management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022DEVICEINTERNAL_H_
#define _THA6A290022DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/man/Tha60290022DeviceInternal.h"
#include "Tha6A290022Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290022Device
    {
    tTha60290022Device super;
    }tTha6A290022Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha6A290022DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022DEVICEINTERNAL_H_ */


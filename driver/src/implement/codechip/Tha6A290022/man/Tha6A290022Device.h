/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha6A290022Device.h
 * 
 * Created Date: May 5, 2018
 *
 * Description : Product 6A290022
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022DEVICE_H_
#define _THA6A290022DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/


/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha6A290022DeviceNew(AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022DEVICE_H_ */


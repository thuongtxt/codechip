/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : Interrupt Controller
 *
 * File        : Tha6A290022InterruptControllerInternal.h
 *
 * Created Date: March 05, 2018
 *
 * Description : Interrupt Controller representation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022INTERRUPTCONTROLLER_H_
#define _THA6A290022INTERRUPTCONTROLLER_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/man/Tha60290021InterruptController.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290022InterruptController
    {
    tTha60290021InterruptController super;
    } tTha6A290022InterruptController;
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
ThaIntrController Tha6A290022IntrControllerNew(AtIpCore core);

#ifdef __cplusplus
}
#endif

#endif /* _THA6A290022INTERRUPTCONTROLLER_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha6A290022ModuleOcn.c
 *
 * Created Date: March 05, 2018
 *
 * Description : OCN source code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290022ModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleOcnMethods m_ThaModuleOcnOverride;

/* Supper implementation */
static const tThaModuleOcnMethods *m_ThaModuleOcnMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet DefaultSet(ThaModuleOcn self)
    {
    eAtRet ret;

    ret = m_ThaModuleOcnMethods->DefaultSet(self);
    if (ret != cAtOk)
        return ret;

    return Tha60290021ModuleOcnPohProcessorSideSet(self, cTha6029LineSideFaceplate);
    }

static eAtRet SohOverEthInit(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool SohOverEthIsSupported(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleOcnMethods = mMethodsGet(ocnModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, m_ThaModuleOcnMethods, sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, DefaultSet);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthInit);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthIsSupported);
        }

    mMethodsSet(ocnModule, &m_ThaModuleOcnOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleOcn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290022ModuleOcn);
    }

AtModule Tha6A290022ModuleOcnObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A290022ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A290022ModuleOcnObjectInit(newModule, device);
    }

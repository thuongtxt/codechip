/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A210031AttPdhDe3.c
 *
 * Created Date: Dec 8, 2015
 *
 * Description : ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/att/ThaAttController.h"
#include "Tha6A290022PdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttControllerMethods                m_AtAttControllerOverride;
static tThaAttControllerMethods    m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0546);
    }

static AtDevice DeviceGet(AtChannel self)
    {
    return AtChannelDeviceGet(self);
    }

static Tha6A290021ModulePdh PdhModule(AtChannel self)
    {
    return (Tha6A290021ModulePdh) AtDeviceModuleGet(DeviceGet(self), cAtModulePdh);
    }

static AtIpCore IpCore(AtChannel self)
    {
    return AtDeviceIpCoreGet(DeviceGet(self), 0);
    }

static uint32 HoldRegisterOffset(AtPdhChannel channel)
    {
    uint8 slice, hwSts;
    ThaPdhChannelHwIdGet(channel, cAtModulePdh, &slice, &hwSts);
    return ThaModulePdhSliceBase((ThaModulePdh)PdhModule((AtChannel)channel), slice);
    }

static uint16 LongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    uint32 holdRegOffset = HoldRegisterOffset((AtPdhChannel)channel);
    return Tha60290021ModulePdhAttLongReadOnCore(PdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel), holdRegOffset);
    }

static uint16 LongWrite(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    uint32 holdRegOffset = HoldRegisterOffset((AtPdhChannel)channel);
    return Tha60290021ModulePdhAttLongWriteOnCore(PdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel), holdRegOffset);
    }

static eAtRet DefaultSet(ThaAttController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideTha6A210031PdhDe3AttController(Tha6A210031PdhDe3AttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(self), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);
        }

    mMethodsSet(self, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride, mMethodsGet(self), sizeof(m_AtAttControllerOverride));
        mMethodOverride(m_AtAttControllerOverride, LongRead);
        mMethodOverride(m_AtAttControllerOverride, LongWrite);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static void OverrideThaAttController(AtAttController self)
    {
    ThaAttController object = (ThaAttController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(object), sizeof(m_ThaAttControllerOverride));

        mMethodOverride(m_ThaAttControllerOverride, DefaultSet);
        }

    mMethodsSet(object, &m_ThaAttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideAtAttController(self);
    OverrideThaAttController(self);
    OverrideTha6A210031PdhDe3AttController((Tha6A210031PdhDe3AttController)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290022PdhDe3AttController);
    }

AtAttController Tha6A290022PdhDe3AttControllerObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A290021PdhDe3AttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A290022PdhDe3AttControllerNew(AtChannel de3)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A290022PdhDe3AttControllerObjectInit(controller, de3);
    }

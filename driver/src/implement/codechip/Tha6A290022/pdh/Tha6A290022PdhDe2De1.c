/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A290021PdhDe2De1.c
 *
 * Created Date: Dec 9, 2018
 *
 * Description : Concrete class for PDH De1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/att/ThaAttController.h"
#include "Tha6A290022PdhDe2De1Internal.h"
#include "Tha6A290022ModulePdhInternal.h"
#include "Tha6A290022PdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha6A290022PdhDe2De1 *)self)
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtPdhDe1Methods m_AtPdhDe1Override;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return AtAttControllerInit(AtChannelAttController(self));
    }

static uint32 TxAlarmsGet(AtChannel self)
	{
    AtAttController attController = AtChannelAttController(self);
    return AtAttControllerTxAlarmsGet(attController);
	}

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcedAlarmGet(self);

    return TxAlarmsGet(self);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcibleAlarmsGet(self);

    return cAtPdhDe3AlarmLos | cAtPdhDe3AlarmAis | cAtPdhDe3AlarmRai | cAtPdhDe3AlarmLof;
    }

static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorTypes, eAtRet (*fForceAlarm)(AtAttController, uint32))
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 errorType = errorTypes & (cBit0 << i);
        if (errorType)
            {
            AtAttControllerForceErrorModeSet(attController, errorType, cAtAttForceErrorModeContinuous);
            ret = fForceAlarm(attController, errorType);
            }
        }
    return ret;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerForceError);
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorUnForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerUnForceError);
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcedErrorGet(self);

    return 0;
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcableErrorsGet(self);

    return cAtPdhDe1CounterRei|cAtPdhDe1CounterFe;
    }

static eBool HasFarEndPerformance(AtPdhDe1 self, uint16 frameType)
    {
    AtUnused(self);
    AtUnused(frameType);
    return cAtTrue;
    }

static void OverrideAtPdhDe1(AtPdhDe1 self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhDe1Override, mMethodsGet(self), sizeof(m_AtPdhDe1Override));

        mMethodOverride(m_AtPdhDe1Override, HasFarEndPerformance);
        }

    mMethodsSet(self, &m_AtPdhDe1Override);
    }

static void OverrideAtChannel(AtChannel self)
	{

	if (!m_methodsInit)
		{
		AtOsal osal = AtSharedDriverOsalGet();
		m_AtChannelMethods = mMethodsGet(self);
		mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

		mMethodOverride(m_AtChannelOverride, Init);
		mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
		mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
		mMethodOverride(m_AtChannelOverride, TxErrorForce);
		mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
		mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
		mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
		}

	mMethodsSet(self, &m_AtChannelOverride);
	}

static void OverrideAtObject(AtObject self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

static void Override(AtPdhDe1 self)
	{
	OverrideAtObject((AtObject) self);
	OverrideAtChannel((AtChannel)self);
	OverrideAtPdhDe1((AtPdhDe1)self);
	}

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290022PdhDe2De1);
    }

AtPdhDe1 Tha6A290022PdhDe2De1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60290021PdhDe2De1ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe1 Tha6A290022PdhDe2De1New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newDe1 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe1 == NULL)
        return NULL;

    /* construct it */
    return Tha6A290022PdhDe2De1ObjectInit(newDe1, channelId, module);
    }

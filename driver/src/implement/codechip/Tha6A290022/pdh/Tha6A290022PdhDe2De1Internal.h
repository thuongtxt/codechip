/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha6A290022PdhDe2De1Internal.h
 * 
 * Created Date: Apr 23, 2018
 *
 * Description : DE1
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022PDHDE2DE1INTERNAL_H_
#define _THA6A290022PDHDE2DE1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/pdh/Tha60290021PdhDe2De1Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290022PdhDe2De1
    {
	tTha60290021PdhDe2De1 super;
    }tTha6A290022PdhDe2De1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 Tha6A290022PdhDe2De1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022PDHDE2DE1INTERNAL_H_ */


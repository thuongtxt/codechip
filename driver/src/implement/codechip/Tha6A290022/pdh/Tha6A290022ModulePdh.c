/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A290021ModulePdh.c
 *
 * Created Date: Dec 3, 2015
 *
 * Description : PDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha6A210031/pdh/Tha6A210031PdhDe3AttReg.h"
#include "../../../default/att/ThaAttPdhManagerInternal.h"
#include "Tha6A290022ModulePdhInternal.h"
#include "Tha6A290022PdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290022ModulePdh*)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tAtModulePdhMethods     m_AtModulePdhOverride;
static tThaModulePdhMethods    m_ThaModulePdhOverride;
static tThaStmModulePdhMethods m_ThaStmModulePdhOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods = NULL;
static const tThaModulePdhMethods *m_ThaModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool MdlIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
	mVersionReset(self);
    }

static eAtRet StsVtDemapVc4FractionalVc3Set(ThaStmModulePdh self, AtSdhChannel sdhVc, eBool isVc4FractionalVc3)
    {
    AtUnused(self);
    AtUnused(sdhVc);
    AtUnused(isVc4FractionalVc3);
    return cAtOk;
    }

static AtPdhDe1 De2De1Create(AtModulePdh self, AtPdhDe2 de2, uint32 de1Id)
    {
    AtUnused(de2);
    return Tha6A290022PdhDe2De1New(de1Id, self);
    }

static AtPdhDe1 VcDe1Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    return Tha6A290022PdhVcDe1New((AtSdhVc)sdhVc, self);
    }

static AtPdhDe3 VcDe3Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    uint8 slice, hwSts;
    ThaSdhChannel2HwMasterStsId(sdhVc, cAtModulePdh, &slice, &hwSts);
    return Tha6A290022PdhVcDe3New(hwSts, self, sdhVc);
    }

static AtAttPdhManager AttPdhManagerCreate(AtModulePdh self)
    {
    return Tha6A290022AttPdhManagerNew(self);
    }

static AtObjectAny De1AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290022PdhDe1AttControllerNew(channel);
    }

static AtObjectAny De3AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290022PdhDe3AttControllerNew(channel);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->attLongRegisterAccess));
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtModulePdh self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(self), sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, De2De1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe3Create);
        mMethodOverride(m_AtModulePdhOverride, AttPdhManagerCreate);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(ThaModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(self), sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, MdlIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportPrm);
        mMethodOverride(m_ThaModulePdhOverride, De1AttControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, De3AttControllerCreate);
        }

    mMethodsSet(self, &m_ThaModulePdhOverride);
    }

static void OverrideThaStmModulePdh(ThaStmModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmModulePdhOverride, mMethodsGet(self), sizeof(m_ThaStmModulePdhOverride));

        mMethodOverride(m_ThaStmModulePdhOverride, StsVtDemapVc4FractionalVc3Set);
        }

    mMethodsSet(self, &m_ThaStmModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtObject(self);
    OverrideAtModulePdh(self);
    OverrideThaModulePdh((ThaModulePdh)self);
    OverrideThaStmModulePdh((ThaStmModulePdh)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290022ModulePdh);
    }

static AtModulePdh Tha6A290022ModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    /* Setup internal data */

    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha6A290022ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290022ModulePdhObjectInit(newModule, device);
    }

    

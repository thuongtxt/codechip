/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A290022PdhVcDe3.c
 *
 * Created Date: Nov 18, 2018
 *
 * Description : VC PDH DE3 implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/xc/Tha60290021ModuleXc.h"
#include "../../../default/att/ThaAttController.h"
#include "Tha6A290022PdhVcDe3Internal.h"
#include "Tha6A290022ModulePdhInternal.h"
#include "Tha6A290022PdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha6A290022PdhVcDe3 *)self)
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods         m_AtChannelOverride;
static tThaPdhDe3Methods         m_ThaPdhDe3Override;


/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 DefaultAisAllOnesThreshold(ThaPdhDe3 self)
    {
    /* In DUT, this is 0 for DS3 and 1 for E3.
     * How ever in ATT should be 1 for both DS3 and E3 */
    AtUnused(self);
    return 1;
    }

static eBool IsManagedByModulePdh(ThaPdhDe3 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet De3FramingBindToPwSatop(ThaPdhDe3 self, AtPw pseudowire)
    {
    AtUnused(self);
    AtUnused(pseudowire);
    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return AtAttControllerInit(AtChannelAttController(self));
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmTypes, eAtRet (*fForceAlarm)(AtAttController, uint32), eAtRet (*fSetClearAlarm)(AtAttController, uint32))
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 alarmType = alarmTypes & (cBit0 << i);
        if (alarmType)
            {
            AtAttControllerForceAlarmModeSet(attController, alarmType, cAtAttForceAlarmModeContinuous);
            fSetClearAlarm(attController, alarmType);
            ret =  fForceAlarm(attController, alarmType);
            }
        }
    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerForceAlarm,AtAttControllerTxAlarmsSet);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerUnForceAlarm,AtAttControllerTxAlarmsClear);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcibleAlarmsGet(self);

    return cAtPdhDe3AlarmLos | cAtPdhDe3AlarmAis | cAtPdhDe3AlarmRai | cAtPdhDe3AlarmLof;
    }

static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorTypes, eBool force)
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 errorType = errorTypes & (cBit0 << i);
        if (errorType)
            {
            eAtAttPdhDe3ErrorType attErrorType = errorTypes;
            AtAttControllerForceErrorModeSet(attController, attErrorType, cAtAttForceErrorModeContinuous);
            if (force)
                ret =  AtAttControllerForceError(attController, attErrorType);
            else
                ret = AtAttControllerUnForceError(attController, attErrorType);
            }
        }
    return ret;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorForce(self, errorType);

    return HelperTxErrorForce(self, errorType, cAtTrue);
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorUnForce(self, errorType);

    return HelperTxErrorForce(self, errorType, cAtFalse);
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcedErrorGet(self);

    return 0;
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcableErrorsGet(self);

    return cAtPdhDe3CounterRei | cAtPdhDe3CounterFBit | cAtPdhDe3CounterCPBit | cAtPdhDe3CounterPBit;
    }



static void OverrideThaPdhDe3(ThaPdhDe3 self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe3Override, mMethodsGet(self), sizeof(m_ThaPdhDe3Override));

        mMethodOverride(m_ThaPdhDe3Override, IsManagedByModulePdh);
        mMethodOverride(m_ThaPdhDe3Override, De3FramingBindToPwSatop);
        mMethodOverride(m_ThaPdhDe3Override, DefaultAisAllOnesThreshold);
        }

    mMethodsSet(self, &m_ThaPdhDe3Override);
    }

static void OverrideAtChannel(AtChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
		mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
		mMethodOverride(m_AtChannelOverride, TxErrorForce);
		mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
		mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
		mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        }

    mMethodsSet(self, &m_AtChannelOverride);
    }


static void OverrideAtObject(AtObject self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

static void Override(AtPdhDe3 self)
    {
	OverrideAtObject((AtObject) self);
    OverrideAtChannel((AtChannel)self);
    OverrideThaPdhDe3((ThaPdhDe3)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290022PdhVcDe3);
    }

AtPdhDe3 Tha6A290022PdhVcDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module, AtSdhChannel sdhChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60290022PdhVcDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    mThis(self)->sdhChannel = sdhChannel;
    return self;
    }

AtPdhDe3 Tha6A290022PdhVcDe3New(uint32 channelId, AtModulePdh module, AtSdhChannel sdhChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe3 == NULL)
        return NULL;

    /* construct it */
    return Tha6A290022PdhVcDe3ObjectInit(newDe3, channelId, module, sdhChannel);
    }

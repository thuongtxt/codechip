/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : Tha6A290021AttControllerInternal.h
 * 
 * Created Date: May 17, 2016
 *
 * Description : ATT controller header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022MODULEPDHINTERNAL_H_
#define _THA6A290022MODULEPDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/pdh/Tha60290022ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290022ModulePdh * Tha6A290022ModulePdh;
typedef struct tTha6A290022ModulePdh
    {
    tTha60290022ModulePdh super;
    AtLongRegisterAccess attLongRegisterAccess;
    }tTha6A290022ModulePdh;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe3 Tha6A290022PdhDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha6A290022PdhVcDe1New(AtSdhVc vc1x, AtModulePdh module);
AtPdhDe1 Tha6A290022PdhDe2De1New(uint32 channelId, AtModulePdh module);
AtPdhDe3 Tha6A290022PdhVcDe3New(uint32 channelId, AtModulePdh module, AtSdhChannel sdhChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021MODULEPDHINTERNAL_H_ */

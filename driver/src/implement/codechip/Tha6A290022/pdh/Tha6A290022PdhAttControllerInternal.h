/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : Tha6A210031AttControllerInternal.h
 * 
 * Created Date: May 17, 2018
 *
 * Description : ATT controller header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022PDHATTCONTROLLERINTERNAL_H_
#define _THA6A290022PDHATTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A290021/pdh/Tha6A290021PdhAttControllerInternal.h"
#include "Tha6A290022ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290022PdhDe3AttController
    {
    tTha6A290021PdhDe3AttController super;
    }tTha6A290022PdhDe3AttController;

typedef struct tTha6A290022PdhDe1AttController
    {
    tTha6A290021PdhDe1AttController super;
    }tTha6A290022PdhDe1AttController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttController Tha6A290022PdhDe3AttControllerNew(AtChannel de3);
AtAttController Tha6A290022PdhDe1AttControllerNew(AtChannel de1);
AtAttController Tha6A290022PdhDe3AttControllerObjectInit(AtAttController self, AtChannel channel);
AtAttController Tha6A290022PdhDe1AttControllerObjectInit(AtAttController self, AtChannel channel);

eBool Tha6A290022AttPdhForceLogicFromDut(AtChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022PDHATTCONTROLLERINTERNAL_H_ */


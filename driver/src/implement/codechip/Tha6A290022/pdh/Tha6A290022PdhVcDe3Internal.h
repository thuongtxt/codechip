/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha6A290022PdhVcDe3Internal.h
 * 
 * Created Date: May 30, 2018
 *
 * Description : VC PDH DE3 implementation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022PDHVCDE3INTERNAL_H_
#define _THA6A290022PDHVCDE3INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/pdh/Tha60290022PdhVcDe3Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha6A290022PdhVcDe3
    {
    tTha60290022PdhVcDe3 super;
    AtSdhChannel sdhChannel;
    }tTha6A290022PdhVcDe3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe3 Tha6A290022PdhVcDe3New(uint32 channelId, AtModulePdh module, AtSdhChannel sdhChannel);
AtPdhDe3 Tha6A290022PdhVcDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module, AtSdhChannel sdhChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022PDHVCDE3INTERNAL_H_ */


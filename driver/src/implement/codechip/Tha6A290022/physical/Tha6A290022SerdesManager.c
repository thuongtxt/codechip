/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha6A290022SerdesManager.c
 *
 * Created Date: March 05, 2018
 *
 * Description : SERDES manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290022SerdesManagerInternal.h"
#include "Tha6A290022Physical.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021SerdesManagerMethods m_Tha60290021SerdesManagerOverride;

/* Save super implementation */
static const tTha60290021SerdesManagerMethods *m_Tha60290021SerdesManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController FaceplateSerdesControllerObjectCreate(Tha60290021SerdesManager self, AtChannel physicalPort, uint32 serdesId)
    {
    return Tha6A290022FaceplateSerdesControllerNew((AtSerdesManager)self, physicalPort, serdesId);
    }

static void OverrideTha60290021SerdesManager(AtSerdesManager self)
    {
    Tha60290021SerdesManager manager = (Tha60290021SerdesManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021SerdesManagerMethods = mMethodsGet(manager);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021SerdesManagerOverride, mMethodsGet(manager), sizeof(m_Tha60290021SerdesManagerOverride));

        mMethodOverride(m_Tha60290021SerdesManagerOverride, FaceplateSerdesControllerObjectCreate);
        }

    mMethodsSet(manager, &m_Tha60290021SerdesManagerOverride);
    }

static void Override(AtSerdesManager self)
    {
    OverrideTha60290021SerdesManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290022SerdesManager);
    }

AtSerdesManager Tha6A290022SerdesManagerObjectInit(AtSerdesManager self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021SerdesManagerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesManager Tha6A290022SerdesManagerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesManager newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290022SerdesManagerObjectInit(newModule, device);
    }

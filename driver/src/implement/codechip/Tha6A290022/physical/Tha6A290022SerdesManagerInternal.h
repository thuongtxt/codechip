/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DEVICE
 * 
 * File        : Tha6020SerdesManagerInternal.h
 * 
 * Created Date: March 05, 2018
 *
 * Description : Serdes Management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022SERDESMANAGERINTERNAL_H_
#define _THA6A290022SERDESMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/physical/Tha60290022SerdesManagerInternal.h"
#include "Tha6A290022Physical.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290022SerdesManager
    {
    tTha60290022SerdesManager super;
    }tTha6A290022SerdesManager;
	
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManager Tha6A290022SerdesManagerObjectInit(AtSerdesManager self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022SERDESMANAGERINTERNAL_H_ */

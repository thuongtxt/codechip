/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60290022Physical.h
 * 
 * Created Date: Apr 25, 2017
 *
 * Description : Physical common header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022PHYSICAL_H_
#define _THA6A290022PHYSICAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtClasses.h"
#include "AtChannelClasses.h"
#include "AtPhysicalClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManager Tha6A290022SerdesManagerNew(AtDevice device);

/* SERDES controller */
AtSerdesController Tha6A290022FaceplateSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022PHYSICAL_H_ */


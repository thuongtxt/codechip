/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : Tha6A290021AttControllerInternal.h
 * 
 * Created Date: Jul 8, 2017
 *
 * Description : ATT SDH controller internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022SDHATTCONTROLLERINTERNAL_H_
#define _THA6A290022SDHATTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A290021/sdh/Tha6A290021SdhAttControllerInternal.h"
#include "Tha6A290022ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290022SdhLineAttController
    {
    tTha6A290021SdhLineAttController super;
    }tTha6A290022SdhLineAttController;

typedef struct tTha6A290022SdhPathAttController
	{
	tTha6A290021SdhPathAttController super;
	}tTha6A290022SdhPathAttController;

typedef struct tTha6A290022SdhVc1xAttController
	{
	tTha6A290021SdhVc1xAttController super;
	}tTha6A290022SdhVc1xAttController;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttController Tha6A290022SdhLineAttControllerNew(AtChannel sdhLine);
AtAttController Tha6A290022SdhPathAttControllerNew(AtChannel sdhPath);
AtAttController Tha6A290022SdhVc1xAttControllerNew(AtChannel sdhPath);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022SDHATTCONTROLLERINTERNAL_H_ */


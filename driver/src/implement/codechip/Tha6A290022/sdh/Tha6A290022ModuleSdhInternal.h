/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6A290022ModuleSdhInternal.h
 * 
 * Created Date: Jul 8, 2018
 *
 * Description : Module SDH internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022MODULESDHINTERNAL_H_
#define _THA6A290022MODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/sdh/Tha60290022ModuleSdhInternal.h"
#include "../../../default/att/ThaAttSdhManagerInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290022ModuleSdh * Tha6A290022ModuleSdh;

typedef struct tTha6A290022ModuleSdh
    {
	tTha60290022ModuleSdh super;
    AtLongRegisterAccess attLongRegisterAccess;
    }tTha6A290022ModuleSdh;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAu Tha6A290022SdhAuNew(uint32 auId, uint8 auType, AtModuleSdh module);
AtSdhLine Tha6A290022SdhLineNew(uint32 channelId, AtModuleSdh module);
AtSdhVc Tha6A290022SdhVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha6A290022SdhTerminatedLineVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha6A290022SdhTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha6A290022SdhVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhTu Tha6A290022SdhTu1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

uint16 Tha60290022ModuleSdhAttLongReadOnCore(Tha6A290022ModuleSdh self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
uint16 Tha60290022ModuleSdhAttLongWriteOnCore(Tha6A290022ModuleSdh self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);

AtModuleSdh Tha6A290022ModuleSdhObjectInit(AtModuleSdh self, AtDevice device);
#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022MODULESDHINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6A290022SdhLineAttReg.h
 * 
 * Created Date: Jul 7, 2018
 *
 * Description : SDH ATT register description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022SDHLINEATTREG_H_
#define _THA6A290022SDHLINEATTREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Force LOS Error Configuration
Reg Addr   : 0x21801-0x21801
Reg Formula:
    Where  : 
Reg Desc   : 
This register is applicable to configure Force LOS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_loscfg_Base                                                                          0x21801

/*--------------------------------------
BitField Name: los_msk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 LOS Event
BitField Bits: [31:16]
--------------------------------------*/
#define c_upen_loscfg_los_msk_pos_Mask                                                               cBit31_16
#define c_upen_loscfg_los_msk_pos_Shift                                                                     16

/*--------------------------------------
BitField Name: los_sec_num
BitField Type: RW
BitField Desc: The T second LOS number
BitField Bits: [15:8]
--------------------------------------*/
#define c_upen_loscfg_los_sec_num_Mask                                                                cBit15_8
#define c_upen_loscfg_los_sec_num_Shift                                                                      8

/*--------------------------------------
BitField Name: los_evtin1s
BitField Type: RW
BitField Desc: The T second LOS number
BitField Bits: [7:4]
--------------------------------------*/
#define c_upen_loscfg_los_evtin1s_Mask                                                                 cBit7_4
#define c_upen_loscfg_los_evtin1s_Shift                                                                      4

/*--------------------------------------
BitField Name: los_frc_ena
BitField Type: RW
BitField Desc: Force Alarm LOS Enable
BitField Bits: [3]
--------------------------------------*/
#define c_upen_loscfg_los_frc_ena_Mask                                                                   cBit3
#define c_upen_loscfg_los_frc_ena_Shift                                                                      3

/*--------------------------------------
BitField Name: los_frc_mod
BitField Type: RW
BitField Desc: Force Alarm LOS mode 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [2:0]
--------------------------------------*/
#define c_upen_loscfg_los_frc_mod_Mask                                                                 cBit2_0
#define c_upen_loscfg_los_frc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force LOF Error Configuration
Reg Addr   : 0x24000-0x24000
Reg Formula: 0x24000 + 2048*SliceId + StsId
    Where  : 
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force LOF Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lofcfg_Base                                                                          0x24000
#define cReg_upen_lofcfg_WidthVal                                                                           64

/*--------------------------------------
BitField Name: loferr_step
BitField Type: RW
BitField Desc: The Total Step Number to Force A1/A2 Bit Congiguration
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_lofcfg_loferr_step_Mask                                                                cBit11_4
#define c_upen_lofcfg_loferr_step_Shift                                                                      4

/*--------------------------------------
BitField Name: lofmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  A1/A2 Bit Congiguration
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_lofcfg_lofmsk_pos_Mask_01                                                             cBit31_20
#define c_upen_lofcfg_lofmsk_pos_Shift_01                                                                   20
#define c_upen_lofcfg_lofmsk_pos_Mask_02                                                               cBit3_0
#define c_upen_lofcfg_lofmsk_pos_Shift_02                                                                    0

/*--------------------------------------
BitField Name: loferr_num
BitField Type: RW
BitField Desc: The Total A1/A2 Bit Error Number Congiguration
BitField Bits: [19:04]
--------------------------------------*/
#define c_upen_lofcfg_loferr_num_Mask                                                                 cBit19_4
#define c_upen_lofcfg_loferr_num_Shift                                                                       4

/*--------------------------------------
BitField Name: lofmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For  A1/A2 Bit Congiguration
BitField Bits: [03]
--------------------------------------*/
#define c_upen_lofcfg_lofmsk_dat_Mask                                                                    cBit3
#define c_upen_lofcfg_lofmsk_dat_Shift                                                                       3

/*--------------------------------------
BitField Name: loffrc_mod
BitField Type: RW
BitField Desc: Force LOF t Mode 000:N error (N = 16bits),N in (Tms) (N = 16bits,
Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_lofcfg_loffrc_mod_Mask                                                                  cBit2_0
#define c_upen_lofcfg_loffrc_mod_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Force Line AIS Error Configuration
Reg Addr   : 0x21800-0x21800
Reg Formula:
    Where  : 
Reg Desc   : 
This register is applicable to configure Force Line AIS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_laiscfg_Base                                                                         0x21800

/*--------------------------------------
BitField Name: ais_msk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 AIS Event
BitField Bits: [31:16]
--------------------------------------*/
#define c_upen_laiscfg_ais_msk_pos_Mask                                                              cBit31_16
#define c_upen_laiscfg_ais_msk_pos_Shift                                                                    16

/*--------------------------------------
BitField Name: ais_sec_num
BitField Type: RW
BitField Desc: The T second AIS number
BitField Bits: [15:8]
--------------------------------------*/
#define c_upen_laiscfg_ais_sec_num_Mask                                                               cBit15_8
#define c_upen_laiscfg_ais_sec_num_Shift                                                                     8

/*--------------------------------------
BitField Name: ais_evtin1s
BitField Type: RW
BitField Desc: The T second AIS number
BitField Bits: [7:4]
--------------------------------------*/
#define c_upen_laiscfg_ais_evtin1s_Mask                                                                cBit7_4
#define c_upen_laiscfg_ais_evtin1s_Shift                                                                     4

/*--------------------------------------
BitField Name: ais_frc_ena
BitField Type: RW
BitField Desc: Force Alarm AIS Enable
BitField Bits: [3]
--------------------------------------*/
#define c_upen_laiscfg_ais_frc_ena_Mask                                                                  cBit3
#define c_upen_laiscfg_ais_frc_ena_Shift                                                                     3

/*--------------------------------------
BitField Name: ais_frc_mod
BitField Type: RW
BitField Desc: Force Alarm AIS mode 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [2:0]
--------------------------------------*/
#define c_upen_laiscfg_ais_frc_mod_Mask                                                                cBit2_0
#define c_upen_laiscfg_ais_frc_mod_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Force B1 Byte Error Configuration
Reg Addr   : 0x24010-0x24010
Reg Formula: 0x24010 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force B1 Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_B1bytcfg_Base                                                                        0x24010
#define cReg_upen_B1bytcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: b1ferr_step
BitField Type: RW
BitField Desc: The Total Step to Force B1 Congiguration
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ferr_step_Mask                                                              cBit11_4
#define c_upen_B1bytcfg_b1ferr_step_Shift                                                                    4

/*--------------------------------------
BitField Name: b1fmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for B1 Congiguration
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_B1bytcfg_b1fmsk_pos_Mask_Lo                                                           cBit31_20
#define c_upen_B1bytcfg_b1fmsk_pos_Shift_Lo                                                                 20
#define c_upen_B1bytcfg_b1fmsk_pos_Mask_Ho                                                             cBit3_0
#define c_upen_B1bytcfg_b1fmsk_pos_Shift_Ho                                                                  0

/*--------------------------------------
BitField Name: b1ferr_num
BitField Type: RW
BitField Desc: The Total B1 Error Number Congiguration
BitField Bits: [19:04]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ferr_num_Mask                                                               cBit19_4
#define c_upen_B1bytcfg_b1ferr_num_Shift                                                                     4

/*--------------------------------------
BitField Name: b1fmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For  B1 Congiguration
BitField Bits: [03]
--------------------------------------*/
#define c_upen_B1bytcfg_b1fmsk_dat_Mask                                                                  cBit3
#define c_upen_B1bytcfg_b1fmsk_dat_Shift                                                                     3

/*--------------------------------------
BitField Name: b1ffrc_mod
BitField Type: RW
BitField Desc: Force B1 Mode 000:N error (N = 16bits),N in (Tms) (N = 16bits,
Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ffrc_mod_Mask                                                                cBit2_0
#define c_upen_B1bytcfg_b1ffrc_mod_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Force Line RDI Error Configuration
Reg Addr   : 0x21803-0x21803
Reg Formula:
    Where  : 
Reg Desc   : 
This register is applicable to configure Force Line RDI Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lrdicfg_Base                                                                         0x21803

/*--------------------------------------
BitField Name: rdi_msk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 RDI Event
BitField Bits: [31:16]
--------------------------------------*/
#define c_upen_lrdicfg_rdi_msk_pos_Mask                                                              cBit31_16
#define c_upen_lrdicfg_rdi_msk_pos_Shift                                                                    16

/*--------------------------------------
BitField Name: rdi_sec_num
BitField Type: RW
BitField Desc: The T second RDI number
BitField Bits: [15:8]
--------------------------------------*/
#define c_upen_lrdicfg_rdi_sec_num_Mask                                                               cBit15_8
#define c_upen_lrdicfg_rdi_sec_num_Shift                                                                     8

/*--------------------------------------
BitField Name: rdi_evtin1s
BitField Type: RW
BitField Desc: The T second RDI number
BitField Bits: [7:4]
--------------------------------------*/
#define c_upen_lrdicfg_rdi_evtin1s_Mask                                                                cBit7_4
#define c_upen_lrdicfg_rdi_evtin1s_Shift                                                                     4

/*--------------------------------------
BitField Name: rdi_frc_ena
BitField Type: RW
BitField Desc: Force Alarm RDI Enable
BitField Bits: [3]
--------------------------------------*/
#define c_upen_lrdicfg_rdi_frc_ena_Mask                                                                  cBit3
#define c_upen_lrdicfg_rdi_frc_ena_Shift                                                                     3

/*--------------------------------------
BitField Name: rdi_frc_mod
BitField Type: RW
BitField Desc: Force Alarm RDI mode 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [2:0]
--------------------------------------*/
#define c_upen_lrdicfg_rdi_frc_mod_Mask                                                                cBit2_0
#define c_upen_lrdicfg_rdi_frc_mod_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Force B2 Byte Error Configuration
Reg Addr   : 0x21802-0x21802
Reg Formula:
    Where  : 
Reg Desc   : 
This register is applicable to configure Force B2 Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_B2bytcfg_Base                                                                        0x21802
#define cReg_upen_B2bytcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: b2ferr_step
BitField Type: RW
BitField Desc: The Total Step to Force B2 Congiguration
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_B2bytcfg_b2ferr_step_Mask                                                              cBit11_4
#define c_upen_B2bytcfg_b2ferr_step_Shift                                                                    4

/*--------------------------------------
BitField Name: b2fmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for B2 Congiguration
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_B2bytcfg_b2fmsk_pos_Mask_01                                                           cBit31_20
#define c_upen_B2bytcfg_b2fmsk_pos_Shift_01                                                                 20
#define c_upen_B2bytcfg_b2fmsk_pos_Mask_02                                                             cBit3_0
#define c_upen_B2bytcfg_b2fmsk_pos_Shift_02                                                                  0

/*--------------------------------------
BitField Name: b2ferr_num
BitField Type: RW
BitField Desc: The Total B2 Error Number Congiguration
BitField Bits: [19:4]
--------------------------------------*/
#define c_upen_B2bytcfg_b2ferr_num_Mask                                                               cBit19_4
#define c_upen_B2bytcfg_b2ferr_num_Shift                                                                     4

/*--------------------------------------
BitField Name: b2fmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For  B2 Congiguration
BitField Bits: [3]
--------------------------------------*/
#define c_upen_B2bytcfg_b2fmsk_dat_Mask                                                                  cBit3
#define c_upen_B2bytcfg_b2fmsk_dat_Shift                                                                     3

/*--------------------------------------
BitField Name: b2ffrc_mod
BitField Type: RW
BitField Desc: Force B2 Mode 000:N error (N = 16bits),N in (Tms) (N = 16bits,
Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_B2bytcfg_b2ffrc_mod_Mask                                                                cBit2_0
#define c_upen_B2bytcfg_b2ffrc_mod_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Force REI Byte Error Configuration
Reg Addr   : 0x21804-0x21804
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure Force REI Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_REIbytcfg_Base                                                                       0x21804
#define cReg_upen_REIbytcfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: reierr_step
BitField Type: RW
BitField Desc: The Total Step to Force REI Congiguration
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_REIbytcfg_reierr_step_Mask                                                             cBit11_4
#define c_upen_REIbytcfg_reierr_step_Shift                                                                   4

/*--------------------------------------
BitField Name: reimsk_pos
BitField Type: RW
BitField Desc: The Position Mask for REI Congiguration
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_REIbytcfg_reimsk_pos_Mask_01                                                          cBit31_20
#define c_upen_REIbytcfg_reimsk_pos_Shift_01                                                                20
#define c_upen_REIbytcfg_reimsk_pos_Mask_02                                                            cBit3_0
#define c_upen_REIbytcfg_reimsk_pos_Shift_02                                                                 0

/*--------------------------------------
BitField Name: reierr_num
BitField Type: RW
BitField Desc: The Total REI Error Number Congiguration
BitField Bits: [19:4]
--------------------------------------*/
#define c_upen_REIbytcfg_reierr_num_Mask                                                              cBit19_4
#define c_upen_REIbytcfg_reierr_num_Shift                                                                    4

/*--------------------------------------
BitField Name: reimsk_dat
BitField Type: RW
BitField Desc: The Data Mask For  REI Congiguration
BitField Bits: [3]
--------------------------------------*/
#define c_upen_REIbytcfg_reimsk_dat_Mask                                                                 cBit3
#define c_upen_REIbytcfg_reimsk_dat_Shift                                                                    3

/*--------------------------------------
BitField Name: reifrc_mod
BitField Type: RW
BitField Desc: Force REI Mode 000:N error (N = 16bits),N in (Tms) (N = 16bits,
Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_REIbytcfg_reifrc_mod_Mask                                                               cBit2_0
#define c_upen_REIbytcfg_reifrc_mod_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Force LOS3G Error Configuration
Reg Addr   : 0x21008-0x21008
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure Force LOS3G Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_los3gcfg_Base                                                                        0x21008

/*--------------------------------------
BitField Name: los3g_msk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 LOS3G Event
BitField Bits: [31:16]
--------------------------------------*/
#define c_upen_los3gcfg_los3g_msk_pos_Mask                                                           cBit31_16
#define c_upen_los3gcfg_los3g_msk_pos_Shift                                                                 16

/*--------------------------------------
BitField Name: los3g_sec_num
BitField Type: RW
BitField Desc: The T second LOS3G number
BitField Bits: [15:8]
--------------------------------------*/
#define c_upen_los3gcfg_los3g_sec_num_Mask                                                            cBit15_8
#define c_upen_los3gcfg_los3g_sec_num_Shift                                                                  8

/*--------------------------------------
BitField Name: los3g_evtin1s
BitField Type: RW
BitField Desc: The T second LOS3G number
BitField Bits: [7:4]
--------------------------------------*/
#define c_upen_los3gcfg_los3g_evtin1s_Mask                                                             cBit7_4
#define c_upen_los3gcfg_los3g_evtin1s_Shift                                                                  4

/*--------------------------------------
BitField Name: los3g_frc_ena
BitField Type: RW
BitField Desc: Force Alarm LOS3G Enable
BitField Bits: [3]
--------------------------------------*/
#define c_upen_los3gcfg_los3g_frc_ena_Mask                                                               cBit3
#define c_upen_los3gcfg_los3g_frc_ena_Shift                                                                  3

/*--------------------------------------
BitField Name: los3g_frc_mod
BitField Type: RW
BitField Desc: Force Alarm LOS3G mode 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [2:0]
--------------------------------------*/
#define c_upen_los3gcfg_los3g_frc_mod_Mask                                                             cBit2_0
#define c_upen_los3gcfg_los3g_frc_mod_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force LOF3G Error Configuration
Reg Addr   : 0x24100-0x24100
Reg Formula: 0x24100 + 32*LineId
    Where  :
           + $LineId(0-7): Line ID
Reg Desc   :
This register is applicable to configure Force LOF3G Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lof3gcfg_Base                                                                        0x24100
#define cReg_upen_lof3gcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: lof3gerr_step
BitField Type: RW
BitField Desc: The Total Step Number to Force A1/A2 Bit Congiguration
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_lof3gcfg_lof3gerr_step_Mask                                                            cBit11_4
#define c_upen_lof3gcfg_lof3gerr_step_Shift                                                                  4

/*--------------------------------------
BitField Name: lof3gmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  A1/A2 Bit Congiguration
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_lof3gcfg_lof3gmsk_pos_Mask_01                                                         cBit31_20
#define c_upen_lof3gcfg_lof3gmsk_pos_Shift_01                                                               20
#define c_upen_lof3gcfg_lof3gmsk_pos_Mask_02                                                           cBit3_0
#define c_upen_lof3gcfg_lof3gmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: lof3gmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For  A1/A2 Bit Congiguration
BitField Bits: [19]
--------------------------------------*/
#define c_upen_lof3gcfg_lof3gmsk_dat_Mask                                                               cBit19
#define c_upen_lof3gcfg_lof3gmsk_dat_Shift                                                                  19

/*--------------------------------------
BitField Name: lof3gerr_num
BitField Type: RW
BitField Desc: The Total A1/A2 Bit Error Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_lof3gcfg_lof3gerr_num_Mask                                                             cBit18_3
#define c_upen_lof3gcfg_lof3gerr_num_Shift                                                                   3

/*--------------------------------------
BitField Name: lof3gfrc_mod
BitField Type: RW
BitField Desc: Force LOF3G t Mode 000:N error (N = 16bits),N in (Tms) (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_lof3gcfg_lof3gfrc_mod_Mask                                                              cBit2_0
#define c_upen_lof3gcfg_lof3gfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force Line AIS3G Error Configuration
Reg Addr   : 0x21008-0x21008
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure Force Line AIS3G Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lais3gcfg_Base                                                                       0x21008

/*--------------------------------------
BitField Name: ais3g_msk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 AIS3G Event
BitField Bits: [31:16]
--------------------------------------*/
#define c_upen_lais3gcfg_ais3g_msk_pos_Mask                                                          cBit31_16
#define c_upen_lais3gcfg_ais3g_msk_pos_Shift                                                                16

/*--------------------------------------
BitField Name: ais3g_sec_num
BitField Type: RW
BitField Desc: The T second AIS3G number
BitField Bits: [15:8]
--------------------------------------*/
#define c_upen_lais3gcfg_ais3g_sec_num_Mask                                                           cBit15_8
#define c_upen_lais3gcfg_ais3g_sec_num_Shift                                                                 8

/*--------------------------------------
BitField Name: ais3g_evtin1s
BitField Type: RW
BitField Desc: The T second AIS3G number
BitField Bits: [7:4]
--------------------------------------*/
#define c_upen_lais3gcfg_ais3g_evtin1s_Mask                                                            cBit7_4
#define c_upen_lais3gcfg_ais3g_evtin1s_Shift                                                                 4

/*--------------------------------------
BitField Name: ais3g_frc_ena
BitField Type: RW
BitField Desc: Force Alarm AIS3G Enable
BitField Bits: [3]
--------------------------------------*/
#define c_upen_lais3gcfg_ais3g_frc_ena_Mask                                                              cBit3
#define c_upen_lais3gcfg_ais3g_frc_ena_Shift                                                                 3

/*--------------------------------------
BitField Name: ais3g_frc_mod
BitField Type: RW
BitField Desc: Force Alarm AIS3G mode 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [2:0]
--------------------------------------*/
#define c_upen_lais3gcfg_ais3g_frc_mod_Mask                                                            cBit2_0
#define c_upen_lais3gcfg_ais3g_frc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force B1 Byte Error 3G Configuration
Reg Addr   : 0x24110-0x24110
Reg Formula: 0x24110 + 32*LineId
    Where  :
           + $LineId(0-7): Line ID
Reg Desc   :
This register is applicable to configure Force B1 3G Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_B13gbytcfg_Base                                                                      0x24110
#define cReg_upen_B13gbytcfg_WidthVal                                                                       64

/*--------------------------------------
BitField Name: b1f3gerr_step
BitField Type: RW
BitField Desc: The Total Step to Force B1 3G Congiguration
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_B13gbytcfg_b1f3gerr_step_Mask                                                          cBit11_4
#define c_upen_B13gbytcfg_b1f3gerr_step_Shift                                                                4

/*--------------------------------------
BitField Name: b1f3gmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for B1 3G Congiguration
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_B13gbytcfg_b1f3gmsk_pos_Mask_01                                                       cBit31_20
#define c_upen_B13gbytcfg_b1f3gmsk_pos_Shift_01                                                             20
#define c_upen_B13gbytcfg_b1f3gmsk_pos_Mask_02                                                         cBit3_0
#define c_upen_B13gbytcfg_b1f3gmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: b1f3gmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For  B1 3G Congiguration
BitField Bits: [19]
--------------------------------------*/
#define c_upen_B13gbytcfg_b1f3gmsk_dat_Mask                                                             cBit19
#define c_upen_B13gbytcfg_b1f3gmsk_dat_Shift                                                                19

/*--------------------------------------
BitField Name: b1f3gerr_num
BitField Type: RW
BitField Desc: The Total B1 3G Error Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_B13gbytcfg_b1f3gerr_num_Mask                                                           cBit18_3
#define c_upen_B13gbytcfg_b1f3gerr_num_Shift                                                                 3

/*--------------------------------------
BitField Name: b1f3gfrc_mod
BitField Type: RW
BitField Desc: Force B1 3G Mode 000:N error (N = 16bits),N in (Tms) (N = 16bits,
Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_B13gbytcfg_b1f3gfrc_mod_Mask                                                            cBit2_0
#define c_upen_B13gbytcfg_b1f3gfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force Line RDI3G Error Configuration
Reg Addr   : 0x2100b-0x2100b
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure Force Line RDI3G Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lrdi3Gcfg_Base                                                                       0x2100b

/*--------------------------------------
BitField Name: rdi3G_msk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 RDI3G Event
BitField Bits: [31:16]
--------------------------------------*/
#define c_upen_lrdi3Gcfg_rdi3G_msk_pos_Mask                                                          cBit31_16
#define c_upen_lrdi3Gcfg_rdi3G_msk_pos_Shift                                                                16

/*--------------------------------------
BitField Name: rdi3G_sec_num
BitField Type: RW
BitField Desc: The T second RDI3G number
BitField Bits: [18:8]
--------------------------------------*/
#define c_upen_lrdi3Gcfg_rdi3G_sec_num_Mask                                                           cBit18_8
#define c_upen_lrdi3Gcfg_rdi3G_sec_num_Shift                                                                 8

/*--------------------------------------
BitField Name: rdi3G_evtin1s
BitField Type: RW
BitField Desc: The T second RDI3G number
BitField Bits: [7:4]
--------------------------------------*/
#define c_upen_lrdi3Gcfg_rdi3G_evtin1s_Mask                                                            cBit7_4
#define c_upen_lrdi3Gcfg_rdi3G_evtin1s_Shift                                                                 4

/*--------------------------------------
BitField Name: rdi3G_frc_ena
BitField Type: RW
BitField Desc: Force Alarm RDI3G Enable
BitField Bits: [3]
--------------------------------------*/
#define c_upen_lrdi3Gcfg_rdi3G_frc_ena_Mask                                                              cBit3
#define c_upen_lrdi3Gcfg_rdi3G_frc_ena_Shift                                                                 3

/*--------------------------------------
BitField Name: rdi3G_frc_mod
BitField Type: RW
BitField Desc: Force Alarm RDI3G mode 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [2:0]
--------------------------------------*/
#define c_upen_lrdi3Gcfg_rdi3G_frc_mod_Mask                                                            cBit2_0
#define c_upen_lrdi3Gcfg_rdi3G_frc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force B23G Byte Error Configuration
Reg Addr   : 0x2100a-0x2100a
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure Force B23G Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_B23Gbytcfg_Base                                                                      0x2100a
#define cReg_upen_B23Gbytcfg_WidthVal                                                                       64

/*--------------------------------------
BitField Name: b2f3gerr_step
BitField Type: RW
BitField Desc: The Total Step to Force B23G Congiguration
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_B23Gbytcfg_b2f3gerr_step_Mask                                                          cBit11_4
#define c_upen_B23Gbytcfg_b2f3gerr_step_Shift                                                                4

/*--------------------------------------
BitField Name: b2f3gmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for B23G Congiguration
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_B23Gbytcfg_b2f3gmsk_pos_Mask_01                                                       cBit31_20
#define c_upen_B23Gbytcfg_b2f3gmsk_pos_Shift_01                                                             20
#define c_upen_B23Gbytcfg_b2f3gmsk_pos_Mask_02                                                         cBit3_0
#define c_upen_B23Gbytcfg_b2f3gmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: b2f3gerr_num
BitField Type: RW
BitField Desc: The Total B23G Error Number Congiguration
BitField Bits: [19:04]
--------------------------------------*/
#define c_upen_B23Gbytcfg_b2f3gerr_num_Mask                                                           cBit19_4
#define c_upen_B23Gbytcfg_b2f3gerr_num_Shift                                                                 4

/*--------------------------------------
BitField Name: b2f3gmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For B23G Congiguration
BitField Bits: [03]
--------------------------------------*/
#define c_upen_B23Gbytcfg_b2f3gmsk_dat_Mask                                                              cBit3
#define c_upen_B23Gbytcfg_b2f3gmsk_dat_Shift                                                                 3

/*--------------------------------------
BitField Name: b2f3gfrc_mod
BitField Type: RW
BitField Desc: Force B2 Mode 000:N error (N = 16bits),N in (Tms) (N = 16bits,
Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_B23Gbytcfg_b2f3gfrc_mod_Mask                                                            cBit2_0
#define c_upen_B23Gbytcfg_b2f3gfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force REI3G Byte Error Configuration
Reg Addr   : 0x2100c-0x2100c
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure Force REI3G Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_REI3Gbytcfg_Base                                                                     0x2100c
#define cReg_upen_REI3Gbytcfg_WidthVal                                                                      64

/*--------------------------------------
BitField Name: rei3gerr_step
BitField Type: RW
BitField Desc: The Total Step to Force REI3G Congiguration
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_REI3Gbytcfg_rei3gerr_step_Mask                                                         cBit11_4
#define c_upen_REI3Gbytcfg_rei3gerr_step_Shift                                                               4

/*--------------------------------------
BitField Name: rei3gmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for REI3G Congiguration
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_REI3Gbytcfg_rei3gmsk_pos_Mask_01                                                      cBit31_20
#define c_upen_REI3Gbytcfg_rei3gmsk_pos_Shift_01                                                            20
#define c_upen_REI3Gbytcfg_rei3gmsk_pos_Mask_02                                                        cBit3_0
#define c_upen_REI3Gbytcfg_rei3gmsk_pos_Shift_02                                                             0

/*--------------------------------------
BitField Name: rei3gerr_num
BitField Type: RW
BitField Desc: The Total REI3G Error Number Congiguration
BitField Bits: [19:04]
--------------------------------------*/
#define c_upen_REI3Gbytcfg_rei3gerr_num_Mask                                                          cBit19_4
#define c_upen_REI3Gbytcfg_rei3gerr_num_Shift                                                                4

/*--------------------------------------
BitField Name: rei3gmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For REI3G Congiguration
BitField Bits: [03]
--------------------------------------*/
#define c_upen_REI3Gbytcfg_rei3gmsk_dat_Mask                                                             cBit3
#define c_upen_REI3Gbytcfg_rei3gmsk_dat_Shift                                                                3

/*--------------------------------------
BitField Name: rei3gfrc_mod
BitField Type: RW
BitField Desc: Force REI Mode 000:N error (N = 16bits),N in (Tms) (N = 16bits,
Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_REI3Gbytcfg_rei3gfrc_mod_Mask                                                           cBit2_0
#define c_upen_REI3Gbytcfg_rei3gfrc_mod_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer VT Error Configuration
Reg Addr   : 0xa0000-0xa03ff
Reg Formula: 0xa0000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  :
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   :
This register is applicable to configure Force Loss Of Pointer VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_LOPvtcfg_Base                                                                        0xa0000
#define cReg_upen_LOPvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: lopvterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force LOP VT
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvterr_step_Mask                                                            cBit11_4
#define c_upen_LOPvtcfg_lopvterr_step_Shift                                                                  4

/*--------------------------------------
BitField Name: lopvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  LOP VT
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvtmsk_pos_Mask_01                                                         cBit31_20
#define c_upen_LOPvtcfg_lopvtmsk_pos_Shift_01                                                               20
#define c_upen_LOPvtcfg_lopvtmsk_pos_Mask_02                                                           cBit3_0
#define c_upen_LOPvtcfg_lopvtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: lopvtmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For LOP VT
BitField Bits: [19]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvtmsk_dat_Mask                                                               cBit19
#define c_upen_LOPvtcfg_lopvtmsk_dat_Shift                                                                  19

/*--------------------------------------
BitField Name: lopvterr_num
BitField Type: RW
BitField Desc: The total LOP VT Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvterr_num_Mask                                                             cBit18_3
#define c_upen_LOPvtcfg_lopvterr_num_Shift                                                                   3

/*--------------------------------------
BitField Name: lopvtfrc_mod
BitField Type: RW
BitField Desc: Force LOP VT t Mode 000:N error (N = 16bits),N in (Tms) (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvtfrc_mod_Mask                                                              cBit2_0
#define c_upen_LOPvtcfg_lopvtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer VT Error Status
Reg Addr   : 0xa0400-0xa07ff
Reg Formula: 0xa0400 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  :
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   :
This register is applicable to see the Force Loss Of Pointer VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_LOPvtsta_Base                                                                        0xa0400
#define cReg_upen_LOPvtsta_WidthVal                                                                         32

/*--------------------------------------
BitField Name: LOPvt_mskposst
BitField Type: RW
BitField Desc: The Position Index LOP VT Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvt_mskposst_Mask                                                          cBit20_17
#define c_upen_LOPvtsta_LOPvt_mskposst_Shift                                                                17

/*--------------------------------------
BitField Name: LOPvt_seccntst
BitField Type: RW
BitField Desc: The Event Counter LOP VT Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvt_seccntst_Mask                                                           cBit16_9
#define c_upen_LOPvtsta_LOPvt_seccntst_Shift                                                                 9

/*--------------------------------------
BitField Name: LOPvtmseccntst
BitField Type: RW
BitField Desc: The Second Counter LOP VT Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvtmseccntst_Mask                                                            cBit8_5
#define c_upen_LOPvtsta_LOPvtmseccntst_Shift                                                                 5

/*--------------------------------------
BitField Name: LOPvt_updcntst
BitField Type: RW
BitField Desc: Updated LOP Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvt_updcntst_Mask                                                            cBit4_3
#define c_upen_LOPvtsta_LOPvt_updcntst_Shift                                                                 3

/*--------------------------------------
BitField Name: LOPvttimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond LOP VT Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvttimmsecst_Mask                                                              cBit2
#define c_upen_LOPvtsta_LOPvttimmsecst_Shift                                                                 2

/*--------------------------------------
BitField Name: LOPvttim_secst
BitField Type: RW
BitField Desc: Posedge Second LOP VT Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvttim_secst_Mask                                                              cBit1
#define c_upen_LOPvtsta_LOPvttim_secst_Shift                                                                 1

/*--------------------------------------
BitField Name: LOPvtosecenbst
BitField Type: RW
BitField Desc: Force Loss Of Pointer VT  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvtosecenbst_Mask                                                              cBit0
#define c_upen_LOPvtsta_LOPvtosecenbst_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS VT Error Configuration
Reg Addr   : 0xa0800-0xa0bff
Reg Formula: 0xa0800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  :
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   :
This register is applicable to configure Force AIS VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_AISvtcfg_Base                                                                        0xa0800
#define cReg_upen_AISvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: AISvtmsk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 event AIS VT
BitField Bits: [30:15]
--------------------------------------*/
#define c_upen_AISvtcfg_AISvtmsk_pos_Mask                                                            cBit30_15
#define c_upen_AISvtcfg_AISvtmsk_pos_Shift                                                                  15

/*--------------------------------------
BitField Name: AISvtsec_num
BitField Type: RW
BitField Desc: The  T second to force alarm  Loss Of Pointer VT
BitField Bits: [14:7]
--------------------------------------*/
#define c_upen_AISvtcfg_AISvtsec_num_Mask                                                             cBit14_7
#define c_upen_AISvtcfg_AISvtsec_num_Shift                                                                   7

/*--------------------------------------
BitField Name: AISvtevtin1s
BitField Type: RW
BitField Desc: The event  Loss Of Pointer VT in one second
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_AISvtcfg_AISvtevtin1s_Mask                                                              cBit6_3
#define c_upen_AISvtcfg_AISvtevtin1s_Shift                                                                   3

/*--------------------------------------
BitField Name: AISvtfrc_ena
BitField Type: RW
BitField Desc: Force Alarm Loss Of Pointer VT Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_AISvtcfg_AISvtfrc_ena_Mask                                                                cBit2
#define c_upen_AISvtcfg_AISvtfrc_ena_Shift                                                                   2

/*--------------------------------------
BitField Name: AISvtfrc_mod
BitField Type: RW
BitField Desc: Force Loss Of Pointer VT in One second Mode 00: Set Alarm in T
second 01: Set or clear 1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_AISvtcfg_AISvtfrc_mod_Mask                                                              cBit1_0
#define c_upen_AISvtcfg_AISvtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS VT Error Status
Reg Addr   : 0xa0C00-0xa0FFF
Reg Formula: 0xa0C00 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force AIS VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_AISvtsta_Base                                                                        0xa0C00
#define cReg_upen_AISvtsta_WidthVal                                                                         32

/*--------------------------------------
BitField Name: AISvt_mskposst
BitField Type: RW
BitField Desc: The Position Index AIS VT Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_AISvtsta_AISvt_mskposst_Mask                                                          cBit20_17
#define c_upen_AISvtsta_AISvt_mskposst_Shift                                                                17

/*--------------------------------------
BitField Name: AISvt_seccntst
BitField Type: RW
BitField Desc: The Event Counter AIS VT Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_AISvtsta_AISvt_seccntst_Mask                                                           cBit16_9
#define c_upen_AISvtsta_AISvt_seccntst_Shift                                                                 9

/*--------------------------------------
BitField Name: AISvtmseccntst
BitField Type: RW
BitField Desc: The Second Counter AIS VT Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_AISvtsta_AISvtmseccntst_Mask                                                            cBit8_5
#define c_upen_AISvtsta_AISvtmseccntst_Shift                                                                 5

/*--------------------------------------
BitField Name: AISvt_updcntst
BitField Type: RW
BitField Desc: Updated AIS Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_AISvtsta_AISvt_updcntst_Mask                                                            cBit4_3
#define c_upen_AISvtsta_AISvt_updcntst_Shift                                                                 3

/*--------------------------------------
BitField Name: AISvttimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond AIS VT Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_AISvtsta_AISvttimmsecst_Mask                                                              cBit2
#define c_upen_AISvtsta_AISvttimmsecst_Shift                                                                 2

/*--------------------------------------
BitField Name: AISvttim_secst
BitField Type: RW
BitField Desc: Posedge Second AIS VT Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_AISvtsta_AISvttim_secst_Mask                                                              cBit1
#define c_upen_AISvtsta_AISvttim_secst_Shift                                                                 1

/*--------------------------------------
BitField Name: AISvtosecenbst
BitField Type: RW
BitField Desc: Force Loss Of Pointer VT  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_AISvtsta_AISvtosecenbst_Mask                                                              cBit0
#define c_upen_AISvtsta_AISvtosecenbst_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force UEQ VT Error Configuration
Reg Addr   : 0xa1000-0xa13FF
Reg Formula: 0xa1000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  :
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   :
This register is applicable to configure Force UEQ VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_UEQvtcfg_Base                                                                        0xa1000
#define cReg_upen_UEQvtcfg_WidthVal                                                                         32

/*--------------------------------------
BitField Name: UEQvtmsk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 event UEQ VT
BitField Bits: [30:15]
--------------------------------------*/
#define c_upen_UEQvtcfg_UEQvtmsk_pos_Mask                                                            cBit30_15
#define c_upen_UEQvtcfg_UEQvtmsk_pos_Shift                                                                  15

/*--------------------------------------
BitField Name: UEQvtsec_num
BitField Type: RW
BitField Desc: The  T second to force alarm  UEQ VT
BitField Bits: [14:7]
--------------------------------------*/
#define c_upen_UEQvtcfg_UEQvtsec_num_Mask                                                             cBit14_7
#define c_upen_UEQvtcfg_UEQvtsec_num_Shift                                                                   7

/*--------------------------------------
BitField Name: UEQvtevtin1s
BitField Type: RW
BitField Desc: The event  UEQ VT in one second
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_UEQvtcfg_UEQvtevtin1s_Mask                                                              cBit6_3
#define c_upen_UEQvtcfg_UEQvtevtin1s_Shift                                                                   3

/*--------------------------------------
BitField Name: UEQvtfrc_ena
BitField Type: RW
BitField Desc: Force Alarm UEQ VT Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_UEQvtcfg_UEQvtfrc_ena_Mask                                                                cBit2
#define c_upen_UEQvtcfg_UEQvtfrc_ena_Shift                                                                   2

/*--------------------------------------
BitField Name: UEQvtfrc_mod
BitField Type: RW
BitField Desc: Force UEQ VT in One second Mode 00: Set Alarm in T second 01: Set
or clear 1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_UEQvtcfg_UEQvtfrc_mod_Mask                                                              cBit1_0
#define c_upen_UEQvtcfg_UEQvtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force UEQ VT Error Status
Reg Addr   : 0xa1400-0xa17FF
Reg Formula: 0xa1400 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force UEQ VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_UEQvtsta_Base                                                                        0xa1400
#define cReg_upen_UEQvtsta_WidthVal                                                                         32

/*--------------------------------------
BitField Name: UEQvt_mskposst
BitField Type: RW
BitField Desc: The Position Index UEQ VT Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvt_mskposst_Mask                                                          cBit20_17
#define c_upen_UEQvtsta_UEQvt_mskposst_Shift                                                                17

/*--------------------------------------
BitField Name: UEQvt_seccntst
BitField Type: RW
BitField Desc: The Event Counter UEQ VT Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvt_seccntst_Mask                                                           cBit16_9
#define c_upen_UEQvtsta_UEQvt_seccntst_Shift                                                                 9

/*--------------------------------------
BitField Name: UEQvtmseccntst
BitField Type: RW
BitField Desc: The Second Counter UEQ VT Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvtmseccntst_Mask                                                            cBit8_5
#define c_upen_UEQvtsta_UEQvtmseccntst_Shift                                                                 5

/*--------------------------------------
BitField Name: UEQvt_updcntst
BitField Type: RW
BitField Desc: Updated UEQ Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvt_updcntst_Mask                                                            cBit4_3
#define c_upen_UEQvtsta_UEQvt_updcntst_Shift                                                                 3

/*--------------------------------------
BitField Name: UEQvttimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond UEQ VT Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvttimmsecst_Mask                                                              cBit2
#define c_upen_UEQvtsta_UEQvttimmsecst_Shift                                                                 2

/*--------------------------------------
BitField Name: UEQvttim_secst
BitField Type: RW
BitField Desc: Posedge Second UEQ VT Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvttim_secst_Mask                                                              cBit1
#define c_upen_UEQvtsta_UEQvttim_secst_Shift                                                                 1

/*--------------------------------------
BitField Name: UEQvtosecenbst
BitField Type: RW
BitField Desc: Force UEQ VT  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvtosecenbst_Mask                                                              cBit0
#define c_upen_UEQvtsta_UEQvtosecenbst_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP VT Error Configuration
Reg Addr   : 0xa1800-0xa1BFF
Reg Formula: 0xa1800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force BIP VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_BIPvtcfg_Base                                                                        0xa1800
#define cReg_upen_BIPvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: cfgBIPvterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force BIP VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_BIPvtcfg_cfgBIPvterr_step_Mask                                                          cBit9_2
#define c_upen_BIPvtcfg_cfgBIPvterr_step_Shift                                                               2

/*--------------------------------------
BitField Name: cfgBIPvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  BIP VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Mask_01                                                      cBit31_18
#define c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Shift_01                                                            18
#define c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Mask_02                                                        cBit1_0
#define c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Shift_02                                                             0

/*--------------------------------------
BitField Name: cfgBIPvtmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  BIP VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_BIPvtcfg_cfgBIPvtmsk_dat_Mask                                                            cBit17
#define c_upen_BIPvtcfg_cfgBIPvtmsk_dat_Shift                                                               17

/*--------------------------------------
BitField Name: cfgBIPvterr_num
BitField Type: RW
BitField Desc: The Total BIP VT Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_BIPvtcfg_cfgBIPvterr_num_Mask                                                          cBit16_1
#define c_upen_BIPvtcfg_cfgBIPvterr_num_Shift                                                                1

/*--------------------------------------
BitField Name: cfgBIPvtfrc_1sen
BitField Type: RW
BitField Desc: Force BIP VT in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPvtcfg_cfgBIPvtfrc_1sen_Mask                                                            cBit0
#define c_upen_BIPvtcfg_cfgBIPvtfrc_1sen_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP VT Error Status
Reg Addr   : 0xa1C00-0xa1FFF
Reg Formula: 0xa1C00 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force BIP VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_BIPvtsta_Base                                                                        0xa1C00
#define cReg_upen_BIPvtsta_WidthVal                                                                         32

/*--------------------------------------
BitField Name: staBIPvterr_stepcnt
BitField Type: RW
BitField Desc: The Event BIP VT Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_BIPvtsta_staBIPvterr_stepcnt_Mask                                                     cBit29_22
#define c_upen_BIPvtsta_staBIPvterr_stepcnt_Shift                                                           22

/*--------------------------------------
BitField Name: staBIPvtmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask BIP VT Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_BIPvtsta_staBIPvtmsk_poscnt_Mask                                                      cBit21_18
#define c_upen_BIPvtsta_staBIPvtmsk_poscnt_Shift                                                            18

/*--------------------------------------
BitField Name: staBIPvterr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error BIP VT Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_BIPvtsta_staBIPvterr_cnt_Mask                                                          cBit17_2
#define c_upen_BIPvtsta_staBIPvterr_cnt_Shift                                                                2

/*--------------------------------------
BitField Name: staBIPvtfrc_err
BitField Type: RW
BitField Desc: BIP VT Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_BIPvtsta_staBIPvtfrc_err_Mask                                                             cBit1
#define c_upen_BIPvtsta_staBIPvtfrc_err_Shift                                                                1

/*--------------------------------------
BitField Name: staBIPvtosecenb
BitField Type: RW
BitField Desc: Force BIP VT in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPvtsta_staBIPvtosecenb_Mask                                                             cBit0
#define c_upen_BIPvtsta_staBIPvtosecenb_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Force REI VT Error Configuration
Reg Addr   : 0xa2000-0xa23FF
Reg Formula: 0xa2000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force REI VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_REIvtcfg_Base                                                                        0xa2000
#define cReg_upen_REIvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: cfgREIvterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force REI VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_REIvtcfg_cfgREIvterr_step_Mask                                                          cBit9_2
#define c_upen_REIvtcfg_cfgREIvterr_step_Shift                                                               2

/*--------------------------------------
BitField Name: cfgREIvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  REI VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_REIvtcfg_cfgREIvtmsk_pos_Mask_01                                                      cBit31_18
#define c_upen_REIvtcfg_cfgREIvtmsk_pos_Shift_01                                                            18
#define c_upen_REIvtcfg_cfgREIvtmsk_pos_Mask_02                                                        cBit1_0
#define c_upen_REIvtcfg_cfgREIvtmsk_pos_Shift_02                                                             0

/*--------------------------------------
BitField Name: cfgREIvtmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  REI VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_REIvtcfg_cfgREIvtmsk_dat_Mask                                                            cBit17
#define c_upen_REIvtcfg_cfgREIvtmsk_dat_Shift                                                               17

/*--------------------------------------
BitField Name: cfgREIvterr_num
BitField Type: RW
BitField Desc: The Total REI VT Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_REIvtcfg_cfgREIvterr_num_Mask                                                          cBit16_1
#define c_upen_REIvtcfg_cfgREIvterr_num_Shift                                                                1

/*--------------------------------------
BitField Name: cfgREIvtfrc_1sen
BitField Type: RW
BitField Desc: Force REI VT in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIvtcfg_cfgREIvtfrc_1sen_Mask                                                            cBit0
#define c_upen_REIvtcfg_cfgREIvtfrc_1sen_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force REI VT Error Status
Reg Addr   : 0xa2400-0xa27FF
Reg Formula: 0xa2400 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force REI VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_REIvtsta_Base                                          0xa2400
#define cReg_upen_REIvtsta_WidthVal                                           32

/*--------------------------------------
BitField Name: staREIvterr_stepcnt
BitField Type: RW
BitField Desc: The Event REI VT Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_REIvtsta_staREIvterr_stepcnt_Mask                                                     cBit29_22
#define c_upen_REIvtsta_staREIvterr_stepcnt_Shift                                                           22

/*--------------------------------------
BitField Name: staREIvtmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask REI VT Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_REIvtsta_staREIvtmsk_poscnt_Mask                                                      cBit21_18
#define c_upen_REIvtsta_staREIvtmsk_poscnt_Shift                                                            18

/*--------------------------------------
BitField Name: staREIvterr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error REI VT Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_REIvtsta_staREIvterr_cnt_Mask                                                          cBit17_2
#define c_upen_REIvtsta_staREIvterr_cnt_Shift                                                                2

/*--------------------------------------
BitField Name: staREIvtfrc_err
BitField Type: RW
BitField Desc: REI VT Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_REIvtsta_staREIvtfrc_err_Mask                                                             cBit1
#define c_upen_REIvtsta_staREIvtfrc_err_Shift                                                                1

/*--------------------------------------
BitField Name: staREIvtosecenb
BitField Type: RW
BitField Desc: Force REI VT in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIvtsta_staREIvtosecenb_Mask                                                             cBit0
#define c_upen_REIvtsta_staREIvtosecenb_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI VT Error Configuration
Reg Addr   : 0xa2800-0xa2BFF
Reg Formula: 0xa2800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  :
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   :
This register is applicable to configure Force RDI VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_RDIvtcfg_Base                                                                        0xa2800
#define cReg_upen_RDIvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: rdivterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force rdi VT
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivterr_step_Mask                                                            cBit11_4
#define c_upen_RDIvtcfg_rdivterr_step_Shift                                                                  4

/*--------------------------------------
BitField Name: rdivtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  rdi VT
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivtmsk_pos_Mask_01                                                         cBit31_20
#define c_upen_RDIvtcfg_rdivtmsk_pos_Shift_01                                                               20
#define c_upen_RDIvtcfg_rdivtmsk_pos_Mask_02                                                           cBit3_0
#define c_upen_RDIvtcfg_rdivtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: rdivtmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For rdi VT
BitField Bits: [19]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivtmsk_dat_Mask                                                               cBit19
#define c_upen_RDIvtcfg_rdivtmsk_dat_Shift                                                                  19

/*--------------------------------------
BitField Name: rdivterr_num
BitField Type: RW
BitField Desc: The total rdi VT Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivterr_num_Mask                                                             cBit18_3
#define c_upen_RDIvtcfg_rdivterr_num_Shift                                                                   3

/*--------------------------------------
BitField Name: rdivtfrc_mod
BitField Type: RW
BitField Desc: Force rdi VT t Mode 000:N error (N = 16bits),N in (Tms) (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivtfrc_mod_Mask                                                              cBit2_0
#define c_upen_RDIvtcfg_rdivtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI VT Error Status
Reg Addr   : 0xa2C00-0xa2FFF
Reg Formula: 0xa2C00 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  :
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   :
This register is applicable to see the Force RDI VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_RDIvtsta_Base                                                                        0xa2C00
#define cReg_upen_RDIvtsta_WidthVal                                                                         32

/*--------------------------------------
BitField Name: RDIvt_mskposst
BitField Type: RW
BitField Desc: The Position Index RDI VT Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvt_mskposst_Mask                                                          cBit20_17
#define c_upen_RDIvtsta_RDIvt_mskposst_Shift                                                                17

/*--------------------------------------
BitField Name: RDIvt_seccntst
BitField Type: RW
BitField Desc: The Event Counter RDI VT Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvt_seccntst_Mask                                                           cBit16_9
#define c_upen_RDIvtsta_RDIvt_seccntst_Shift                                                                 9

/*--------------------------------------
BitField Name: RDIvtmseccntst
BitField Type: RW
BitField Desc: The Second Counter RDI VT Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvtmseccntst_Mask                                                            cBit8_5
#define c_upen_RDIvtsta_RDIvtmseccntst_Shift                                                                 5

/*--------------------------------------
BitField Name: RDIvt_updcntst
BitField Type: RW
BitField Desc: Updated RDI Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvt_updcntst_Mask                                                            cBit4_3
#define c_upen_RDIvtsta_RDIvt_updcntst_Shift                                                                 3

/*--------------------------------------
BitField Name: RDIvttimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond RDI VT Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvttimmsecst_Mask                                                              cBit2
#define c_upen_RDIvtsta_RDIvttimmsecst_Shift                                                                 2

/*--------------------------------------
BitField Name: RDIvttim_secst
BitField Type: RW
BitField Desc: Posedge Second RDI VT Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvttim_secst_Mask                                                              cBit1
#define c_upen_RDIvtsta_RDIvttim_secst_Shift                                                                 1

/*--------------------------------------
BitField Name: RDIvtosecenbst
BitField Type: RW
BitField Desc: Force RDI VT  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvtosecenbst_Mask                                                              cBit0
#define c_upen_RDIvtsta_RDIvtosecenbst_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force RFI VT Error Configuration
Reg Addr   : 0xa3000-0xa33FF
Reg Formula: 0xa3000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-3): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force RFI VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_RFIvtcfg_Base                                                                        0xa3000
#define cReg_upen_RFIvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: rfivterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force rfi VT
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_RFIvtcfg_rfivterr_step_Mask                                                            cBit11_4
#define c_upen_RFIvtcfg_rfivterr_step_Shift                                                                  4

/*--------------------------------------
BitField Name: rfivtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  rfi VT
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_RFIvtcfg_rfivtmsk_pos_Mask_01                                                         cBit31_20
#define c_upen_RFIvtcfg_rfivtmsk_pos_Shift_01                                                               20
#define c_upen_RFIvtcfg_rfivtmsk_pos_Mask_02                                                           cBit3_0
#define c_upen_RFIvtcfg_rfivtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: rfivtmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For rfi VT
BitField Bits: [19]
--------------------------------------*/
#define c_upen_RFIvtcfg_rfivtmsk_dat_Mask                                                               cBit19
#define c_upen_RFIvtcfg_rfivtmsk_dat_Shift                                                                  19

/*--------------------------------------
BitField Name: rfivterr_num
BitField Type: RW
BitField Desc: The total rfi VT Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_RFIvtcfg_rfivterr_num_Mask                                                             cBit18_3
#define c_upen_RFIvtcfg_rfivterr_num_Shift                                                                   3

/*--------------------------------------
BitField Name: rfivtfrc_mod
BitField Type: RW
BitField Desc: Force rfi VT t Mode 000:N error (N = 16bits),N in (Tms) (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_RFIvtcfg_rfivtfrc_mod_Mask                                                              cBit2_0
#define c_upen_RFIvtcfg_rfivtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force RFI VT Error Status
Reg Addr   : 0xa3400-0xa34FF
Reg Formula: 0xa3400 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force RFI VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_RFIvtsta_Base                                                                        0xa3400
#define cReg_upen_RFIvtsta_WidthVal                                                                         32

/*--------------------------------------
BitField Name: staRFIvterr_stepcnt
BitField Type: RW
BitField Desc: The Event RFI VT Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_RFIvtsta_staRFIvterr_stepcnt_Mask                                                     cBit29_22
#define c_upen_RFIvtsta_staRFIvterr_stepcnt_Shift                                                           22

/*--------------------------------------
BitField Name: staRFIvtmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask RFI VT Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_RFIvtsta_staRFIvtmsk_poscnt_Mask                                                      cBit21_18
#define c_upen_RFIvtsta_staRFIvtmsk_poscnt_Shift                                                            18

/*--------------------------------------
BitField Name: staRFIvterr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error RFI VT Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_RFIvtsta_staRFIvterr_cnt_Mask                                                          cBit17_2
#define c_upen_RFIvtsta_staRFIvterr_cnt_Shift                                                                2

/*--------------------------------------
BitField Name: staRFIvtfrc_err
BitField Type: RW
BitField Desc: RFI VT Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_RFIvtsta_staRFIvtfrc_err_Mask                                                             cBit1
#define c_upen_RFIvtsta_staRFIvtfrc_err_Shift                                                                1

/*--------------------------------------
BitField Name: staRFIvtosecenb
BitField Type: RW
BitField Desc: Force RFI VT in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RFIvtsta_staRFIvtosecenb_Mask                                                             cBit0
#define c_upen_RFIvtsta_staRFIvtosecenb_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Configuration n second to force in VT
Reg Addr   : 0xa3c00-0xa3c00
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure n second in  VT

------------------------------------------------------------------------------*/
#define cReg_upennsecvt_Base                                                                           0xa3c00

/*--------------------------------------
BitField Name: cfgnsecvt
BitField Type: RW
BitField Desc: The second number in VT Congiguration
BitField Bits: [31:0]
--------------------------------------*/
#define c_upennsecvt_cfgnsecvt_Mask                                                                   cBit31_0
#define c_upennsecvt_cfgnsecvt_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer STS Error Configuration
Reg Addr   : 0x28000-0x2802f
Reg Formula: 0x28000 + 2048*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force Loss Of Pointer STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_LOPstscfg_Base                                                                       0x28000
#define cReg_upen_LOPstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: lopstserr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force LOP STS
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstserr_step_Mask                                                          cBit11_4
#define c_upen_LOPstscfg_lopstserr_step_Shift                                                                4

/*--------------------------------------
BitField Name: lopstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  LOP STS
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstsmsk_pos_Mask_01                                                       cBit31_20
#define c_upen_LOPstscfg_lopstsmsk_pos_Shift_01                                                             20
#define c_upen_LOPstscfg_lopstsmsk_pos_Mask_02                                                         cBit3_0
#define c_upen_LOPstscfg_lopstsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: lopstsmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For LOP STS
BitField Bits: [19]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstsmsk_dat_Mask                                                             cBit19
#define c_upen_LOPstscfg_lopstsmsk_dat_Shift                                                                19

/*--------------------------------------
BitField Name: lopstserr_num
BitField Type: RW
BitField Desc: The total LOP STS Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstserr_num_Mask                                                           cBit18_3
#define c_upen_LOPstscfg_lopstserr_num_Shift                                                                 3

/*--------------------------------------
BitField Name: lopstsfrc_mod
BitField Type: RW
BitField Desc: Force LOP STS t Mode 000:N error (N = 16bits),N in (Tms) (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstsfrc_mod_Mask                                                            cBit2_0
#define c_upen_LOPstscfg_lopstsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer STS STS Error Status
Reg Addr   : 0x28030-0x2805f
Reg Formula: 0x28030 + 2048*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force Loss Of Pointer STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_LOPstssta_Base                                                                       0x28030
#define cReg_upen_LOPstssta_WidthVal                                                                        32

/*--------------------------------------
BitField Name: LOPsts_mskposst
BitField Type: RW
BitField Desc: The Position Index LOP STS Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_LOPstssta_LOPsts_mskposst_Mask                                                        cBit20_17
#define c_upen_LOPstssta_LOPsts_mskposst_Shift                                                              17

/*--------------------------------------
BitField Name: LOPsts_seccntst
BitField Type: RW
BitField Desc: The Event Counter LOP STS Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_LOPstssta_LOPsts_seccntst_Mask                                                         cBit16_9
#define c_upen_LOPstssta_LOPsts_seccntst_Shift                                                               9

/*--------------------------------------
BitField Name: LOPstsmseccntst
BitField Type: RW
BitField Desc: The Second Counter LOP STS Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_LOPstssta_LOPstsmseccntst_Mask                                                          cBit8_5
#define c_upen_LOPstssta_LOPstsmseccntst_Shift                                                               5

/*--------------------------------------
BitField Name: LOPsts_updcntst
BitField Type: RW
BitField Desc: Updated LOP Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_LOPstssta_LOPsts_updcntst_Mask                                                          cBit4_3
#define c_upen_LOPstssta_LOPsts_updcntst_Shift                                                               3

/*--------------------------------------
BitField Name: LOPststimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond LOP STS Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_LOPstssta_LOPststimmsecst_Mask                                                            cBit2
#define c_upen_LOPstssta_LOPststimmsecst_Shift                                                               2

/*--------------------------------------
BitField Name: LOPststim_secst
BitField Type: RW
BitField Desc: Posedge Second LOP STS Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_LOPstssta_LOPststim_secst_Mask                                                            cBit1
#define c_upen_LOPstssta_LOPststim_secst_Shift                                                               1

/*--------------------------------------
BitField Name: LOPstsosecenbst
BitField Type: RW
BitField Desc: Force Loss Of Pointer STS  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOPstssta_LOPstsosecenbst_Mask                                                            cBit0
#define c_upen_LOPstssta_LOPstsosecenbst_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS STS Error Configuration
Reg Addr   : 0x28080-0x2808af
Reg Formula: 0x28080 + 2048*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force AIS STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_AISstscfg_Base                                                                       0x28080
#define cReg_upen_AISstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: AISstsmsk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 event AIS STS
BitField Bits: [30:15]
--------------------------------------*/
#define c_upen_AISstscfg_AISstsmsk_pos_Mask                                                          cBit30_15
#define c_upen_AISstscfg_AISstsmsk_pos_Shift                                                                15

/*--------------------------------------
BitField Name: AISstssec_num
BitField Type: RW
BitField Desc: The  T second to force alarm  Loss Of Pointer STS
BitField Bits: [14:7]
--------------------------------------*/
#define c_upen_AISstscfg_AISstssec_num_Mask                                                           cBit14_7
#define c_upen_AISstscfg_AISstssec_num_Shift                                                                 7

/*--------------------------------------
BitField Name: AISstsestsin1s
BitField Type: RW
BitField Desc: The event  Loss Of Pointer STS in one second
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_AISstscfg_AISstsestsin1s_Mask                                                           cBit6_3
#define c_upen_AISstscfg_AISstsestsin1s_Shift                                                                3

/*--------------------------------------
BitField Name: AISstsfrc_ena
BitField Type: RW
BitField Desc: Force Alarm Loss Of Pointer STS Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_AISstscfg_AISstsfrc_ena_Mask                                                              cBit2
#define c_upen_AISstscfg_AISstsfrc_ena_Shift                                                                 2

/*--------------------------------------
BitField Name: AISstsfrc_mod
BitField Type: RW
BitField Desc: Force Loss Of Pointer STS in One second Mode 00: Set Alarm in T
second 01: Set or clear 1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_AISstscfg_AISstsfrc_mod_Mask                                                            cBit1_0
#define c_upen_AISstscfg_AISstsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS STS Error Status
Reg Addr   : 0x280b0-0x280df
Reg Formula: 0x280b0 + 2048*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force AIS STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_AISstssta_Base                                                                       0x280b0
#define cReg_upen_AISstssta_WidthVal                                                                        32

/*--------------------------------------
BitField Name: AISsts_mskposst
BitField Type: RW
BitField Desc: The Position Index AIS STS Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_AISstssta_AISsts_mskposst_Mask                                                        cBit20_17
#define c_upen_AISstssta_AISsts_mskposst_Shift                                                              17

/*--------------------------------------
BitField Name: AISsts_seccntst
BitField Type: RW
BitField Desc: The Event Counter AIS STS Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_AISstssta_AISsts_seccntst_Mask                                                         cBit16_9
#define c_upen_AISstssta_AISsts_seccntst_Shift                                                               9

/*--------------------------------------
BitField Name: AISstsmseccntst
BitField Type: RW
BitField Desc: The Second Counter AIS STS Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_AISstssta_AISstsmseccntst_Mask                                                          cBit8_5
#define c_upen_AISstssta_AISstsmseccntst_Shift                                                               5

/*--------------------------------------
BitField Name: AISsts_updcntst
BitField Type: RW
BitField Desc: Updated AIS Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_AISstssta_AISsts_updcntst_Mask                                                          cBit4_3
#define c_upen_AISstssta_AISsts_updcntst_Shift                                                               3

/*--------------------------------------
BitField Name: AISststimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond AIS STS Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_AISstssta_AISststimmsecst_Mask                                                            cBit2
#define c_upen_AISstssta_AISststimmsecst_Shift                                                               2

/*--------------------------------------
BitField Name: AISststim_secst
BitField Type: RW
BitField Desc: Posedge Second AIS STS Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_AISstssta_AISststim_secst_Mask                                                            cBit1
#define c_upen_AISstssta_AISststim_secst_Shift                                                               1

/*--------------------------------------
BitField Name: AISstsosecenbst
BitField Type: RW
BitField Desc: Force Loss Of Pointer STS  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_AISstssta_AISstsosecenbst_Mask                                                            cBit0
#define c_upen_AISstssta_AISstsosecenbst_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force UEQ STS Error Configuration
Reg Addr   : 0x28100-0x2812f
Reg Formula: 0x28100 + 2048*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force UEQ STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_UEQstscfg_Base                                                                       0x28100
#define cReg_upen_UEQstscfg_WidthVal                                                                        32

/*--------------------------------------
BitField Name: UEQstsmsk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 event UEQ STS
BitField Bits: [30:15]
--------------------------------------*/
#define c_upen_UEQstscfg_UEQstsmsk_pos_Mask                                                          cBit30_15
#define c_upen_UEQstscfg_UEQstsmsk_pos_Shift                                                                15

/*--------------------------------------
BitField Name: UEQstssec_num
BitField Type: RW
BitField Desc: The  T second to force alarm  UEQ STS
BitField Bits: [14:7]
--------------------------------------*/
#define c_upen_UEQstscfg_UEQstssec_num_Mask                                                           cBit14_7
#define c_upen_UEQstscfg_UEQstssec_num_Shift                                                                 7

/*--------------------------------------
BitField Name: UEQstsestsin1s
BitField Type: RW
BitField Desc: The event  UEQ STS in one second
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_UEQstscfg_UEQstsestsin1s_Mask                                                           cBit6_3
#define c_upen_UEQstscfg_UEQstsestsin1s_Shift                                                                3

/*--------------------------------------
BitField Name: UEQstsfrc_ena
BitField Type: RW
BitField Desc: Force Alarm UEQ STS Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_UEQstscfg_UEQstsfrc_ena_Mask                                                              cBit2
#define c_upen_UEQstscfg_UEQstsfrc_ena_Shift                                                                 2

/*--------------------------------------
BitField Name: UEQstsfrc_mod
BitField Type: RW
BitField Desc: Force UEQ STS in One second Mode 00: Set Alarm in T second 01:
Set or clear 1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_UEQstscfg_UEQstsfrc_mod_Mask                                                            cBit1_0
#define c_upen_UEQstscfg_UEQstsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force UEQ STS Error Status
Reg Addr   : 0x28130-0x2815f
Reg Formula: 0x28130 + 2048*SliceId + StsId
    Where  :
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   :
This register is applicable to see the Force UEQ STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_UEQstssta_Base                                                                       0x28130
#define cReg_upen_UEQstssta_WidthVal                                                                        32

/*--------------------------------------
BitField Name: UEQsts_mskposst
BitField Type: RW
BitField Desc: The Position Index UEQ STS Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_UEQstssta_UEQsts_mskposst_Mask                                                        cBit20_17
#define c_upen_UEQstssta_UEQsts_mskposst_Shift                                                              17

/*--------------------------------------
BitField Name: UEQsts_seccntst
BitField Type: RW
BitField Desc: The Event Counter UEQ STS Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_UEQstssta_UEQsts_seccntst_Mask                                                         cBit16_9
#define c_upen_UEQstssta_UEQsts_seccntst_Shift                                                               9

/*--------------------------------------
BitField Name: UEQstsmseccntst
BitField Type: RW
BitField Desc: The Second Counter UEQ STS Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_UEQstssta_UEQstsmseccntst_Mask                                                          cBit8_5
#define c_upen_UEQstssta_UEQstsmseccntst_Shift                                                               5

/*--------------------------------------
BitField Name: UEQsts_updcntst
BitField Type: RW
BitField Desc: Updated UEQ Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_UEQstssta_UEQsts_updcntst_Mask                                                          cBit4_3
#define c_upen_UEQstssta_UEQsts_updcntst_Shift                                                               3

/*--------------------------------------
BitField Name: UEQststimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond UEQ STS Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_UEQstssta_UEQststimmsecst_Mask                                                            cBit2
#define c_upen_UEQstssta_UEQststimmsecst_Shift                                                               2

/*--------------------------------------
BitField Name: UEQststim_secst
BitField Type: RW
BitField Desc: Posedge Second UEQ STS Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_UEQstssta_UEQststim_secst_Mask                                                            cBit1
#define c_upen_UEQstssta_UEQststim_secst_Shift                                                               1

/*--------------------------------------
BitField Name: UEQstsosecenbst
BitField Type: RW
BitField Desc: Force UEQ STS  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_UEQstssta_UEQstsosecenbst_Mask                                                            cBit0
#define c_upen_UEQstssta_UEQstsosecenbst_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP STS Error Configuration
Reg Addr   : 0x28180-0x281af
Reg Formula: 0x28180 + 2048*SliceId + StsId
    Where  :
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   :
This register is applicable to configure Force BIP STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_BIPstscfg_Base                                                                       0x28180
#define cReg_upen_BIPstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: cfgBIPstserr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force BIP STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_BIPstscfg_cfgBIPstserr_step_Mask                                                        cBit9_2
#define c_upen_BIPstscfg_cfgBIPstserr_step_Shift                                                             2

/*--------------------------------------
BitField Name: cfgBIPstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  BIP STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_BIPstscfg_cfgBIPstsmsk_pos_Mask_01                                                    cBit31_18
#define c_upen_BIPstscfg_cfgBIPstsmsk_pos_Shift_01                                                          18
#define c_upen_BIPstscfg_cfgBIPstsmsk_pos_Mask_02                                                      cBit1_0
#define c_upen_BIPstscfg_cfgBIPstsmsk_pos_Shift_02                                                           0

/*--------------------------------------
BitField Name: cfgBIPstsmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  BIP STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_BIPstscfg_cfgBIPstsmsk_dat_Mask                                                          cBit17
#define c_upen_BIPstscfg_cfgBIPstsmsk_dat_Shift                                                             17

/*--------------------------------------
BitField Name: cfgBIPstserr_num
BitField Type: RW
BitField Desc: The Total BIP STS Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_BIPstscfg_cfgBIPstserr_num_Mask                                                        cBit16_1
#define c_upen_BIPstscfg_cfgBIPstserr_num_Shift                                                              1

/*--------------------------------------
BitField Name: cfgBIPstsfrc_1sen
BitField Type: RW
BitField Desc: Force BIP STS in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPstscfg_cfgBIPstsfrc_1sen_Mask                                                          cBit0
#define c_upen_BIPstscfg_cfgBIPstsfrc_1sen_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP STS Error Status
Reg Addr   : 0x281c0-0x281df
Reg Formula: 0x281c0 + 2048*SliceId + StsId
    Where  :
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   :
This register is applicable to see the Force BIP STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_BIPstssta_Base                                                                       0x281c0
#define cReg_upen_BIPstssta_WidthVal                                                                        32

/*--------------------------------------
BitField Name: staBIPstserr_stepcnt
BitField Type: RW
BitField Desc: The Event BIP STS Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_BIPstssta_staBIPstserr_stepcnt_Mask                                                   cBit29_22
#define c_upen_BIPstssta_staBIPstserr_stepcnt_Shift                                                         22

/*--------------------------------------
BitField Name: staBIPstsmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask BIP STS Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_BIPstssta_staBIPstsmsk_poscnt_Mask                                                    cBit21_18
#define c_upen_BIPstssta_staBIPstsmsk_poscnt_Shift                                                          18

/*--------------------------------------
BitField Name: staBIPstserr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error BIP STS Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_BIPstssta_staBIPstserr_cnt_Mask                                                        cBit17_2
#define c_upen_BIPstssta_staBIPstserr_cnt_Shift                                                              2

/*--------------------------------------
BitField Name: staBIPstsfrc_err
BitField Type: RW
BitField Desc: BIP STS Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_BIPstssta_staBIPstsfrc_err_Mask                                                           cBit1
#define c_upen_BIPstssta_staBIPstsfrc_err_Shift                                                              1

/*--------------------------------------
BitField Name: staBIPstsosecenb
BitField Type: RW
BitField Desc: Force BIP STS in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPstssta_staBIPstsosecenb_Mask                                                           cBit0
#define c_upen_BIPstssta_staBIPstsosecenb_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Force REI STS Error Configuration
Reg Addr   : 0x28200-0x2822f
Reg Formula: 0x28200 + 2048*SliceId + StsId
    Where  :
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   :
This register is applicable to configure Force REI STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_REIstscfg_Base                                                                       0x28200
#define cReg_upen_REIstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: cfgREIstserr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force REI STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_REIstscfg_cfgREIstserr_step_Mask                                                        cBit9_2
#define c_upen_REIstscfg_cfgREIstserr_step_Shift                                                             2

/*--------------------------------------
BitField Name: cfgREIstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  REI STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_REIstscfg_cfgREIstsmsk_pos_Mask_01                                                    cBit31_18
#define c_upen_REIstscfg_cfgREIstsmsk_pos_Shift_01                                                          18
#define c_upen_REIstscfg_cfgREIstsmsk_pos_Mask_02                                                      cBit1_0
#define c_upen_REIstscfg_cfgREIstsmsk_pos_Shift_02                                                           0

/*--------------------------------------
BitField Name: cfgREIstsmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  REI STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_REIstscfg_cfgREIstsmsk_dat_Mask                                                          cBit17
#define c_upen_REIstscfg_cfgREIstsmsk_dat_Shift                                                             17

/*--------------------------------------
BitField Name: cfgREIstserr_num
BitField Type: RW
BitField Desc: The Total REI STS Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_REIstscfg_cfgREIstserr_num_Mask                                                        cBit16_1
#define c_upen_REIstscfg_cfgREIstserr_num_Shift                                                              1

/*--------------------------------------
BitField Name: cfgREIstsfrc_1sen
BitField Type: RW
BitField Desc: Force REI STS in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIstscfg_cfgREIstsfrc_1sen_Mask                                                          cBit0
#define c_upen_REIstscfg_cfgREIstsfrc_1sen_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Force REI STS Error Status
Reg Addr   : 0x28230-0x2825f
Reg Formula: 0x28230 + 2048*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force REI STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_REIstssta_Base                                                                       0x28230
#define cReg_upen_REIstssta_WidthVal                                                                        32

/*--------------------------------------
BitField Name: staREIstserr_stepcnt
BitField Type: RW
BitField Desc: The Event REI STS Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_REIstssta_staREIstserr_stepcnt_Mask                                                   cBit29_22
#define c_upen_REIstssta_staREIstserr_stepcnt_Shift                                                         22

/*--------------------------------------
BitField Name: staREIstsmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask REI STS Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_REIstssta_staREIstsmsk_poscnt_Mask                                                    cBit21_18
#define c_upen_REIstssta_staREIstsmsk_poscnt_Shift                                                          18

/*--------------------------------------
BitField Name: staREIstserr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error REI STS Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_REIstssta_staREIstserr_cnt_Mask                                                        cBit17_2
#define c_upen_REIstssta_staREIstserr_cnt_Shift                                                              2

/*--------------------------------------
BitField Name: staREIstsfrc_err
BitField Type: RW
BitField Desc: REI STS Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_REIstssta_staREIstsfrc_err_Mask                                                           cBit1
#define c_upen_REIstssta_staREIstsfrc_err_Shift                                                              1

/*--------------------------------------
BitField Name: staREIstsosecenb
BitField Type: RW
BitField Desc: Force REI STS in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIstssta_staREIstsosecenb_Mask                                                           cBit0
#define c_upen_REIstssta_staREIstsosecenb_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI STS Error Configuration
Reg Addr   : 0x28280-0x282af
Reg Formula: 0x28280 + 2048*SliceId + StsId
    Where  :
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   :
This register is applicable to configure Force RDI STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_RDIstscfg_Base                                                                       0x28280
#define cReg_upen_RDIstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: rdistserr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force rdi STS
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_RDIstscfg_rdistserr_step_Mask                                                          cBit11_4
#define c_upen_RDIstscfg_rdistserr_step_Shift                                                                4

/*--------------------------------------
BitField Name: rdistsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  rdi STS
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_RDIstscfg_rdistsmsk_pos_Mask_01                                                       cBit31_20
#define c_upen_RDIstscfg_rdistsmsk_pos_Shift_01                                                             20
#define c_upen_RDIstscfg_rdistsmsk_pos_Mask_02                                                         cBit3_0
#define c_upen_RDIstscfg_rdistsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: rdistsmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For rdi STS
BitField Bits: [19]
--------------------------------------*/
#define c_upen_RDIstscfg_rdistsmsk_dat_Mask                                                             cBit19
#define c_upen_RDIstscfg_rdistsmsk_dat_Shift                                                                19

/*--------------------------------------
BitField Name: rdistserr_num
BitField Type: RW
BitField Desc: The total rdi STS Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_RDIstscfg_rdistserr_num_Mask                                                           cBit18_3
#define c_upen_RDIstscfg_rdistserr_num_Shift                                                                 3

/*--------------------------------------
BitField Name: rdistsfrc_mod
BitField Type: RW
BitField Desc: Force rdi STS t Mode 000:N error (N = 16bits),N in (Tms) (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_RDIstscfg_rdistsfrc_mod_Mask                                                            cBit2_0
#define c_upen_RDIstscfg_rdistsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI STS Error Status
Reg Addr   : 0x282b0-0x28df
Reg Formula: 0x282b0 + 2048*SliceId + StsId
    Where  :
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   :
This register is applicable to see the Force RDI STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_RDIstssta_Base                                                                       0x282b0
#define cReg_upen_RDIstssta_WidthVal                                                                        32

/*--------------------------------------
BitField Name: RDIsts_mskposst
BitField Type: RW
BitField Desc: The Position Index RDI STS Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_RDIstssta_RDIsts_mskposst_Mask                                                        cBit20_17
#define c_upen_RDIstssta_RDIsts_mskposst_Shift                                                              17

/*--------------------------------------
BitField Name: RDIsts_seccntst
BitField Type: RW
BitField Desc: The Event Counter RDI STS Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_RDIstssta_RDIsts_seccntst_Mask                                                         cBit16_9
#define c_upen_RDIstssta_RDIsts_seccntst_Shift                                                               9

/*--------------------------------------
BitField Name: RDIstsmseccntst
BitField Type: RW
BitField Desc: The Second Counter RDI STS Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_RDIstssta_RDIstsmseccntst_Mask                                                          cBit8_5
#define c_upen_RDIstssta_RDIstsmseccntst_Shift                                                               5

/*--------------------------------------
BitField Name: RDIsts_updcntst
BitField Type: RW
BitField Desc: Updated RDI Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_RDIstssta_RDIsts_updcntst_Mask                                                          cBit4_3
#define c_upen_RDIstssta_RDIsts_updcntst_Shift                                                               3

/*--------------------------------------
BitField Name: RDIststimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond RDI STS Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_RDIstssta_RDIststimmsecst_Mask                                                            cBit2
#define c_upen_RDIstssta_RDIststimmsecst_Shift                                                               2

/*--------------------------------------
BitField Name: RDIststim_secst
BitField Type: RW
BitField Desc: Posedge Second RDI STS Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_RDIstssta_RDIststim_secst_Mask                                                            cBit1
#define c_upen_RDIstssta_RDIststim_secst_Shift                                                               1

/*--------------------------------------
BitField Name: RDIstsosecenbst
BitField Type: RW
BitField Desc: Force RDI STS  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RDIstssta_RDIstsosecenbst_Mask                                                            cBit0
#define c_upen_RDIstssta_RDIstsosecenbst_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force LOM STS Error Configuration
Reg Addr   : 0x28300-0x2832f
Reg Formula: 0x28300 + 2048*SliceId + StsId
    Where  :
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   :
This register is applicable to configure Force LOM STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_LOMstscfg_Base                                                                       0x28300
#define cReg_upen_LOMstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: lomstserr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force lom STS
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_LOMstscfg_lomstserr_step_Mask                                                          cBit11_4
#define c_upen_LOMstscfg_lomstserr_step_Shift                                                                4

/*--------------------------------------
BitField Name: lomstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  lom STS
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_LOMstscfg_lomstsmsk_pos_Mask_01                                                       cBit31_20
#define c_upen_LOMstscfg_lomstsmsk_pos_Shift_01                                                             20
#define c_upen_LOMstscfg_lomstsmsk_pos_Mask_02                                                         cBit3_0
#define c_upen_LOMstscfg_lomstsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: lomstsmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For lom STS
BitField Bits: [19]
--------------------------------------*/
#define c_upen_LOMstscfg_lomstsmsk_dat_Mask                                                             cBit19
#define c_upen_LOMstscfg_lomstsmsk_dat_Shift                                                                19

/*--------------------------------------
BitField Name: lomstserr_num
BitField Type: RW
BitField Desc: The total lom STS Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_LOMstscfg_lomstserr_num_Mask                                                           cBit18_3
#define c_upen_LOMstscfg_lomstserr_num_Shift                                                                 3

/*--------------------------------------
BitField Name: lomstsfrc_mod
BitField Type: RW
BitField Desc: Force lom STS t Mode 000:N error (N = 16bits),N in (Tms) (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_LOMstscfg_lomstsfrc_mod_Mask                                                            cBit2_0
#define c_upen_LOMstscfg_lomstsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force LOM STS Error Status
Reg Addr   : 0x28330-0x2835f
Reg Formula: 0x28330 + 2048*SliceId + StsId
    Where  :
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   :
This register is applicable to see the Force LOM STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_LOMstssta_Base                                                                       0x28330
#define cReg_upen_LOMstssta_WidthVal                                                                        32

/*--------------------------------------
BitField Name: LOMsts_mskposst
BitField Type: RW
BitField Desc: The Position Index LOM STS Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_LOMstssta_LOMsts_mskposst_Mask                                                        cBit20_17
#define c_upen_LOMstssta_LOMsts_mskposst_Shift                                                              17

/*--------------------------------------
BitField Name: LOMsts_seccntst
BitField Type: RW
BitField Desc: The Event Counter LOM STS Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_LOMstssta_LOMsts_seccntst_Mask                                                         cBit16_9
#define c_upen_LOMstssta_LOMsts_seccntst_Shift                                                               9

/*--------------------------------------
BitField Name: LOMstsmseccntst
BitField Type: RW
BitField Desc: The Second Counter LOM STS Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_LOMstssta_LOMstsmseccntst_Mask                                                          cBit8_5
#define c_upen_LOMstssta_LOMstsmseccntst_Shift                                                               5

/*--------------------------------------
BitField Name: LOMsts_updcntst
BitField Type: RW
BitField Desc: Updated LOM Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_LOMstssta_LOMsts_updcntst_Mask                                                          cBit4_3
#define c_upen_LOMstssta_LOMsts_updcntst_Shift                                                               3

/*--------------------------------------
BitField Name: LOMststimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond LOM STS Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_LOMstssta_LOMststimmsecst_Mask                                                            cBit2
#define c_upen_LOMstssta_LOMststimmsecst_Shift                                                               2

/*--------------------------------------
BitField Name: LOMststim_secst
BitField Type: RW
BitField Desc: Posedge Second LOM STS Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_LOMstssta_LOMststim_secst_Mask                                                            cBit1
#define c_upen_LOMstssta_LOMststim_secst_Shift                                                               1

/*--------------------------------------
BitField Name: LOMstsosecenbst
BitField Type: RW
BitField Desc: Force LOM STS  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOMstssta_LOMstsosecenbst_Mask                                                            cBit0
#define c_upen_LOMstssta_LOMstsosecenbst_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Configuration n second to force in STS
Reg Addr   : 0x28381-0x28381
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure n second in  STS

------------------------------------------------------------------------------*/
#define cReg_upennsecsts_Base                                                                          0x28381

/*--------------------------------------
BitField Name: erdi_p_en
BitField Type: RW
BitField Desc: E-RDI P Enable
BitField Bits: [19]
--------------------------------------*/
#define c_upennsecsts_erdi_p_en_Mask                                                                    cBit19
#define c_upennsecsts_erdi_p_en_Shift                                                                       19

/*--------------------------------------
BitField Name: erdi_s_en
BitField Type: RW
BitField Desc: E-RDI S Enable
BitField Bits: [18]
--------------------------------------*/
#define c_upennsecsts_erdi_s_en_Mask                                                                    cBit18
#define c_upennsecsts_erdi_s_en_Shift                                                                       18

/*--------------------------------------
BitField Name: erdi_c_en
BitField Type: RW
BitField Desc: E-RDI C Enable
BitField Bits: [17]
--------------------------------------*/
#define c_upennsecsts_erdi_c_en_Mask                                                                    cBit17
#define c_upennsecsts_erdi_c_en_Shift                                                                       17

/*--------------------------------------
BitField Name: rdi_en
BitField Type: RW
BitField Desc: RDI Enable
BitField Bits: [16]
--------------------------------------*/
#define c_upennsecsts_rdi_en_Mask                                                                       cBit16
#define c_upennsecsts_rdi_en_Shift                                                                          16

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021SDHLINEATTREG_H_ */


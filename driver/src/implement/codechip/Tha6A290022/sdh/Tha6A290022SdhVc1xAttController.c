/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : Tha6A290021SdhVc1xAttController.c
 *
 * Created Date: Jul 19, 2016
 *
 * Description : VC1x ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha6A290021/sdh/Tha6A290021SdhAttControllerInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "Tha6A290022SdhAttControllerInternal.h"
#include "Tha6A290022SdhAttReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttControllerMethods                m_AtAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static float DelayGet(AtAttController self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0546);
    }

static uint32 RegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
            return cReg_upen_AISvtcfg_Base;
        case cAtSdhPathAlarmLop:
            return cReg_upen_LOPvtcfg_Base;
        case cAtSdhPathAlarmUneq:
            return cReg_upen_UEQvtcfg_Base;
        case cAtSdhPathAlarmRfi:
            return cReg_upen_RFIvtcfg_Base;
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmErdiC:
            return cReg_upen_RDIvtcfg_Base;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathAlarmAis:
            return cReg_upen_AISvtsta_Base;
        case cAtSdhPathAlarmLop:
            return cReg_upen_LOPvtsta_Base;
        case cAtSdhPathAlarmUneq:
            return cReg_upen_UEQvtsta_Base;
        case cAtSdhPathAlarmRfi:
            return cReg_upen_RFIvtsta_Base;
        case cAtSdhPathAlarmRdi:
            return cReg_upen_RDIvtsta_Base;
        default:
            return cBit31_0;
        }
    }

static uint8 AlarmTypeIndex(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
            return 0;
        case cAtSdhPathAlarmLop:
            return 1;
        case cAtSdhPathAlarmUneq:
            return 2;
        case cAtSdhPathAlarmRfi:
            return 3;
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmErdiC:
            return 4;
        default:
            return 0;
        }
    }

static eBool  AlarmIsFromError(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmErdiC:
        case cAtSdhPathAlarmLop:
        case cAtSdhPathAlarmRfi:
            return cAtTrue;

        default: return cAtFalse;
        }
    }

static uint32 RegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathCounterTypeBip:
            return cReg_upen_BIPvtcfg_Base;
        case cAtSdhPathCounterTypeRei:
            return cReg_upen_REIvtcfg_Base;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathCounterTypeBip:
            return cReg_upen_BIPvtsta_Base;
        case cAtSdhPathCounterTypeRei:
            return cReg_upen_REIvtsta_Base;
        default:
            return cBit31_0;
        }
    }

static uint32 RealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwStsInSlice;
    uint32 vtgId = 0, vtId = 0;
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);

    if ((channelType == cAtSdhChannelTypeTu12) || (channelType == cAtSdhChannelTypeTu11))
        {
        vtId = AtChannelIdGet((AtChannel)channel);
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(channel));
        }
    else if ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11))
        {
        AtSdhChannel tu1x = AtSdhChannelParentChannelGet(channel);
        vtId = AtChannelIdGet((AtChannel)tu1x);
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(tu1x));
        }
    
    if (ThaSdhChannel2HwMasterStsId(channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        {
        /*AtPrintc(cSevInfo, "regAddr 0x%x, hwStsInSlice %d, slice %d, vtg %d, vt %d\n", regAddr, hwStsInSlice, slice, vtgId, vtId);*/
        return regAddr + 32UL*hwStsInSlice + slice * 16384UL + vtgId * 4UL + vtId + Tha60210011ModuleOcnBaseAddress(ocnModule);
        }

    return cInvalidUint32;
    }

static AtDevice DeviceGet(AtChannel self)
    {
    return AtChannelDeviceGet(self);
    }

static Tha6A290021ModuleSdh SdhModule(AtChannel self)
    {
    return (Tha6A290021ModuleSdh) AtDeviceModuleGet(DeviceGet(self), cAtModuleSdh);
    }

static AtIpCore IpCore(AtChannel self)
    {
    return AtDeviceIpCoreGet(DeviceGet(self), 0);
    }

static uint16 LongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return Tha60290021ModuleSdhAttLongReadOnCore(SdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel));
    return mChannelHwLongRead(channel, mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, cAtModuleSdh);
    }

static uint16 LongWrite(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return Tha60290021ModuleSdhAttLongWriteOnCore(SdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel));
    return mChannelHwLongWrite(channel, mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, cAtModuleSdh);
    }

static uint32 Read(AtAttController self, uint32 regAddr)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return mChannelHwRead(channel, mMethodsGet(att)->RealAddress(att, regAddr), cAtModuleSdh);
    }

static void Write(AtAttController self, uint32 regAddr,uint32 value)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    mChannelHwWrite(channel, mMethodsGet(att)->RealAddress(att, regAddr), value, cAtModuleSdh);
    }

static eBool AlarmForceIsSupported(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
        case cAtSdhPathAlarmLop:
        case cAtSdhPathAlarmUneq:
        case cAtSdhPathAlarmRfi:
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiC:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride, mMethodsGet(self), sizeof(m_AtAttControllerOverride));
        mMethodOverride(m_AtAttControllerOverride, LongRead);
        mMethodOverride(m_AtAttControllerOverride, LongWrite);
        mMethodOverride(m_AtAttControllerOverride, Read);
        mMethodOverride(m_AtAttControllerOverride, Write);
        mMethodOverride(m_AtAttControllerOverride, AlarmForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, DelayGet);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static void OverrideTha6A210031PdhDe3AttController(AtAttController self)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(controller), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIsFromError);

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);
        }

    mMethodsSet(controller, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideAtAttController(self);
    OverrideTha6A210031PdhDe3AttController(self);
    }
static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290022SdhVc1xAttController);
    }

static AtAttController Tha6A290022SdhVc1xAttControllerObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031SdhVc1xAttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A290022SdhVc1xAttControllerNew(AtChannel sdhPath)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A290022SdhVc1xAttControllerObjectInit(controller, sdhPath);
    }

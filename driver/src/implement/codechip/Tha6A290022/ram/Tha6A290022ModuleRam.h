/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha6A290022ModuleRam.h
 * 
 * Created Date: March 05, 2018
 *
 * Description : Tha6A290022InternalRam declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022INTERNALRAM_H_
#define _THA6A290022INTERNALRAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha6A290022ModuleRamNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022INTERNALRAM_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha6A290021ModuleRamInternal.h
 * 
 * Created Date: March 05, 2018
 *
 * Description : PWE module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022MODULERAMINTERNAL_H_
#define _THA6A290022MODULERAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/ram/Tha60290022ModuleRamInternal.h"
#include "Tha6A290022ModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290022ModuleRam
    {
	tTha60290022ModuleRam super;
    }tTha6A290022ModuleRam;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha6A290022ModuleRamObjectInit(AtModuleRam self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022MODULEPWEINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SUR
 *
 * File        : Tha6A210031ModuleSur.h
 *
 * Created Date: Sep 8, 2017
 *
 * Description : Surveillance header
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022MODULESUR_H_
#define _THA6A290022MODULESUR_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/sur/Tha60290021ModuleSurInternal.h"


#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSur Tha6A290022ModuleSurNew(AtDevice device);

#ifdef __cplusplus
}
#endif

#endif /* _THA6A210031MODULESUR_H_ */

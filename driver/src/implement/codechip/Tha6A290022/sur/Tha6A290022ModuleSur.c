/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module SUR
 *
 * File        : Tha6A290022ModuleSur.c
 *
 * Created Date: March 05, 2018
 *
 * Description : Ethernet module for product 6A290022
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290022ModuleSurInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSurMethods m_AtModuleSurOverride;
static tThaModuleHardSurMethods m_ThaModuleHardSurOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool  FailureIsSupported(AtModuleSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtSurEngine SdhLineSurEngineObjectCreate(ThaModuleHardSur self, AtChannel line)
    {
    AtUnused(self);
    AtUnused(line);
    return NULL;
    }

static AtSurEngine SdhHoPathEngineObjectCreate(ThaModuleHardSur self, AtChannel path)
    {
    AtUnused(self);
    AtUnused(path);
    return NULL;
    }

static AtSurEngine SdhLoPathEngineObjectCreate(ThaModuleHardSur self, AtChannel path)
    {
    AtUnused(self);
    AtUnused(path);
    return NULL;
    }
/*
static AtSurEngine PdhDe3EngineObjectCreate(ThaModuleHardSur self, AtChannel de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return NULL;
    }*/

static AtSurEngine PwEngineObjectCreate(ThaModuleHardSur self, AtChannel pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return NULL;
    }

static void OverrideThaModuleHardSur(ThaModuleHardSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardSurOverride, mMethodsGet(self), sizeof(m_ThaModuleHardSurOverride));
        mMethodOverride(m_ThaModuleHardSurOverride, SdhHoPathEngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, SdhLoPathEngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, SdhLineSurEngineObjectCreate);
        /*mMethodOverride(m_ThaModuleHardSurOverride, PdhDe3EngineObjectCreate);*/
        mMethodOverride(m_ThaModuleHardSurOverride, PwEngineObjectCreate);
        }

    mMethodsSet(self, &m_ThaModuleHardSurOverride);
    }

static void OverrideAtModuleSur(AtModuleSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSurOverride, mMethodsGet(self), sizeof(m_AtModuleSurOverride));
        mMethodOverride(m_AtModuleSurOverride, FailureIsSupported);
        }

    mMethodsSet(self, &m_AtModuleSurOverride);
    }

static void Override(AtModuleSur self)
    {
    OverrideAtModuleSur(self);
    OverrideThaModuleHardSur((ThaModuleHardSur)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290022ModuleSur);
    }

AtModuleSur Tha6A290022ModuleSurObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSur Tha6A290022ModuleSurNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290022ModuleSurObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : Tha60290022ModuleSurInternal.h
 * 
 * Created Date: March 05, 2018
 *
 * Description : SUR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022MODULESURINTERNAL_H_
#define _THAA0290022MODULESURINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/sur/Tha60290022ModuleSurInternal.h"
#include "Tha6A290022ModuleSur.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha6A290022ModuleSur
    {
    tTha60290022ModuleSur super;
    }tTha6A290022ModuleSur;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSur Tha6A290022ModuleSurObjectInit(AtModuleSur self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011MODULESURINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha6A290022ModuleCdr.h
 * 
 * Created Date: March 13, 2018
 *
 * Description : CDR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha6A290022MODULECDR_H_
#define _Tha6A290022MODULECDR_H_

/*--------------------------- Includes ---------------------------------------*/


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/



/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha6A290022ModuleCdrNew(AtDevice device);

ThaCdrController Tha6A290022Tu3VcCdrControllerNew(uint32 engineId, AtChannel channel);

#endif /* _Tha6A290022MODULECDR_H_ */


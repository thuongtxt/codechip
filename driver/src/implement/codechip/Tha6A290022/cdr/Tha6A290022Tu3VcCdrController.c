/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha6A290021HoVcCdrController.c
 *
 * Created Date: Aug 23, 2017
 *
 * Description : CDR controller for AU-VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cdr/controllers/ThaTu3VcCdrControllerInternal.h"
#include "Tha6A290022ModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290022Tu3VcCdrController
    {
    tThaTu3VcCdrController super;
    }tTha6A290022Tu3VcCdrController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrControllerMethods m_ThaCdrControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw AcrDcrTimingSource(ThaCdrController self)
    {
    AtUnused(self);
    return NULL;
    }

static AtPw CurrentAcrDcrTimingSource(ThaCdrController self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool FreeRunningIsSupported(ThaCdrController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, mMethodsGet(self), sizeof(m_ThaCdrControllerOverride));
        mMethodOverride(m_ThaCdrControllerOverride, FreeRunningIsSupported);
        mMethodOverride(m_ThaCdrControllerOverride, CurrentAcrDcrTimingSource);
        mMethodOverride(m_ThaCdrControllerOverride, AcrDcrTimingSource);
        }

    mMethodsSet(self, &m_ThaCdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
    OverrideThaCdrController((ThaCdrController)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290022Tu3VcCdrController);
    }

static ThaCdrController ObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaTu3VcCdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaCdrController)self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController Tha6A290022Tu3VcCdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, engineId, channel);
    }

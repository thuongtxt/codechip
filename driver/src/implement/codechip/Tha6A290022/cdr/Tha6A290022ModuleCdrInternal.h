/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60290021ModuleCdrInternal.h
 * 
 * Created Date: Apr 19, 2017
 *
 * Description : CDR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022MODULECDRINTERNAL_H_
#define _THA6A290022MODULECDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/cdr/Tha60290022ModuleCdrInternal.h"
#include "Tha6A290022ModuleCdr.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290022ModuleCdr
    {
    tTha60290022ModuleCdr super;
    }tTha6A290022ModuleCdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha6A290022ModuleCdrObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022MODULECDRINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaVcDe1CdrControllerInternal.h
 * 
 * Created Date: Jul 18, 2013
 *
 * Description : VC's DE1 CDR controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022VCDE1CDRCONTROLLERINTERNAL_H_
#define _THA6A290022VCDE1CDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cdr/controllers/ThaVcDe1CdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290022VcDe1CdrController * Tha6A290022VcDe1CdrController;

typedef struct tTha6A290022VcDe1CdrController
    {
    tThaVcDe1CdrController super;

    /* Private data */
    }tTha6A290022VcDe1CdrController;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController Tha6A290022VcDe1CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);
ThaCdrController Tha6A290022VcDe1CdrControllerNew(uint32 engineId, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022VCDE1CDRCONTROLLERINTERNAL_H_ */


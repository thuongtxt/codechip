/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A000010AttPrbsRegProvider.c
 *
 * Created Date: Jan 6, 2018
 *
 * Description : Register provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../prbs/Tha6A290022PrbsHiReg.h"
#include "../prbs/Tha6A290022PrbsLoReg.h"
#include "Tha6A290022AttPrbsRegProviderInternal.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaAttPrbsRegProviderMethods      m_ThaAttPrbsRegProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 PrbsGenReg(ThaAttPrbsRegProvider self, eBool isHo)
    {
    AtUnused(self);
	if (isHo)
		return cAf6Reg_prbsgenctrl4;
	return cAf6Reg_prbsgenctrl4lo;
    }

static uint32 PrbsMonReg(ThaAttPrbsRegProvider self, eBool isHo)
    {
    AtUnused(self);
    if (isHo)
		return cAf6Reg_prbsmonctrl4;
	return cAf6Reg_prbsmonctrl4lo;
    }

static uint8 LoPrbsEngineOc48IdShift(ThaAttPrbsRegProvider self)
    {
    AtUnused(self);
    return 21;
    }
static uint8 LoPrbsEngineOc24SliceIdShift(ThaAttPrbsRegProvider self)
    {
    AtUnused(self);
    return 14;
    }

static uint32 PrbsFixPatternReg(ThaAttPrbsRegProvider self, eBool isHo, eBool isTx)
	{
	AtUnused(self);
	if (isHo)
		return isTx? cAf6Reg_prbsgenfxptdat: cAf6Reg_prbsmonfxptdat;

	return isTx? cAf6Reg_prbsgenfxptdatlo: cAf6Reg_prbsmonfxptdatlo;
	}

static uint32 PrbsDelayConfigReg(ThaAttPrbsRegProvider self, eBool isHo, uint8 slice)
	{
	AtUnused(self);
	if (isHo)
		return cAf6Reg_rtmcidcfg + (uint32) (slice*2048);
	return cAf6Reg_rtmcidcfglo + (uint32) (slice*8192);
	}

static uint32 PrbsDelayIdMask(ThaAttPrbsRegProvider self, eBool isHo)
	{
	AtUnused(self);
	if (isHo)
		return cAf6_rtmcidcfg_rtmcidcfg_Mask;
	return cAf6_rtmcidcfglo_rtmcidcfg_Mask;
	}

static uint32 PrbsDelayEnableMask(ThaAttPrbsRegProvider self, eBool isHo)
	{
	AtUnused(self);
	if (isHo)
		return cAf6_rtmcidcfg_rtmen_Mask;
	return cAf6_rtmcidcfglo_rtmen_Mask;
	}

static uint32 PrbsMaxDelayReg(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice)
	{
	uint32 address = 0;
	AtUnused(self);

	if (isHo)
		{
		address = r2c?cAf6Reg_mxdelayregr2c: cAf6Reg_mxdelayregro;
		return address + (uint32) (slice*2048);
		}

	address = r2c?cAf6Reg_mxdelayregr2clo: cAf6Reg_mxdelayregrolo;
	return address + (uint32) (slice*8192);
	}

static uint32 PrbsMinDelayReg(ThaAttPrbsRegProvider self, eBool isHo, eBool r2c, uint8 slice)
	{
	uint32 address = 0;
	AtUnused(self);
	if (isHo)
		{
		address =  r2c?cAf6Reg_mindelayregr2c: cAf6Reg_mindelayregro;
		return address + (uint32) (slice*2048);
		}

	address = r2c?cAf6Reg_mindelayregr2clo: cAf6Reg_mindelayregrolo;
	return address + (uint32) (slice*8192);
	}

static void OverrideThaAttPrbsRegProvider(ThaAttPrbsRegProvider self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    if (!m_methodsInit)
        {
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttPrbsRegProviderOverride, mMethodsGet(self), sizeof(m_ThaAttPrbsRegProviderOverride));

        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsGenReg);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsMonReg);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, LoPrbsEngineOc48IdShift);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, LoPrbsEngineOc24SliceIdShift);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsFixPatternReg);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsDelayConfigReg);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsDelayIdMask);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsDelayEnableMask);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsMaxDelayReg);
        mMethodOverride(m_ThaAttPrbsRegProviderOverride, PrbsMinDelayReg);
        }

    mMethodsSet(self, &m_ThaAttPrbsRegProviderOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021AttPrbsRegProvider);
    }

static void Override(ThaAttPrbsRegProvider self)
    {
	OverrideThaAttPrbsRegProvider((ThaAttPrbsRegProvider)self);
    }

ThaAttPrbsRegProvider Tha6A290022AttPrbsRegProviderObjectInit(ThaAttPrbsRegProvider self, ThaAttModulePrbs prbsModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A290021AttPrbsRegProviderObjectInit(self, prbsModule) == NULL)
        return NULL;

    /* Setup class */
    m_methodsInit = 1;
    Override(self);
    return self;
    }

ThaAttPrbsRegProvider Tha6A290022AttPrbsRegProviderNew(ThaAttModulePrbs prbsModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaAttPrbsRegProvider newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290022AttPrbsRegProviderObjectInit(newProvider, prbsModule);
    }



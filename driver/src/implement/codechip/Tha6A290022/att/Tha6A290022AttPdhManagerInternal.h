/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha6A290022AttPdhManagerInternal.h
 * 
 * Created Date: July 26, 2018
 *
 * Author      : chaudpt
 *
 * Description : ATT PDH Manager of 6A210021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022ATTPDHMANAGERINTERNAL_H_
#define _THA6A290022ATTPDHMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A290021/att/Tha6A290021AttPdhManagerInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha6A290022AttPdhManager* Tha6A290022AttPdhManager;
typedef struct tTha6A290022AttPdhManager
    {
    tTha6A290021AttPdhManager super;
    }tTha6A290022AttPdhManager;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttPdhManager Tha6A290022AttPdhManagerObjectInit(AtAttPdhManager self, AtModulePdh pdh);

/* Debugger */

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022ATTPDHMANAGERINTERNAL_H_ */


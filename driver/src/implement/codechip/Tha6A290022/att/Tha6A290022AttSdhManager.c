/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A290022AttSdhManager.c
 *
 * Created Date: Aug 23, 2018
 *
 * Author      :
 *
 * Description : Default implementation of Sdh module of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha6A290021/att/Tha6A290021AttSdhManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A290022AttSdhManager)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290022AttSdhManager
    {
    tTha6A290021AttSdhManager super;
    }tTha6A290022AttSdhManager;
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
/* Save super implementations */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtAttSdhManager ha6A290022AttSdhManagerObjectInit(AtAttSdhManager self, AtModuleSdh sdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaAttSdhManager));

    /* Super constructor */
    if (Tha6A290021AttSdhManagerObjectInit(self, sdh) == NULL)
        return NULL;

    /* Setup class */
    /*Override(mThis(self));*/
    m_methodsInit = 1;

    return self;
    }

AtAttSdhManager Tha6A290022AttSdhManagerNew(AtModuleSdh sdh)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttSdhManager newObject = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaAttSdhManager));
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return ha6A290022AttSdhManagerObjectInit(newObject, sdh);
    }



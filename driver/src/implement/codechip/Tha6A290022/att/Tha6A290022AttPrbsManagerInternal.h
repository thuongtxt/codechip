/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha6A210021AttPdhManagerInternal.h
 * 
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : ATT PDH Manager of 6A210021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022ATTPRPSMANAGERINTERNAL_H_
#define _THA6A290022ATTPRPSMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/att/ThaAttPrbsManagerInternal.h"
#include "Tha6A290022AttPrbsManager.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha6A290022AttPrbsManager
    {
    tThaAttPrbsManager super;
    }tTha6A290022AttPrbsManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttPrbsManager Tha6A290022AttPrbsManagerObjectInit(AtAttPrbsManager self, AtModulePrbs prbs);


/* Debugger */

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022ATTPRPSMANAGERINTERNAL_H_ */


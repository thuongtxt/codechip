/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Tha6A210021AttPdhManager
 *
 * File        : Tha6A210021AttPdhManager.h
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : This file contains common prototypes of ATT PDH manager of 6A210021
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022ATTPRBSMANAGER_H_
#define _THA6A290022ATTPRBSMANAGER_H_

/*--------------------------- Include files ----------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedef ----------------------------------------*/
typedef struct tTha6A290022AttPrbsManager * Tha6A290022AttPrbsManager;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022ATTPRPSMANAGER_H_ */

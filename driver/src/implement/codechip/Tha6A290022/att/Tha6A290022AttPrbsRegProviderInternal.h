/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A000010AttPrbsRegProviderInternal.h
 * 
 * Created Date: Jan 6, 2018
 *
 * Description : Register provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022ATTPRBSREGPROVIDERINTERNAL_H_
#define _THA6A290022ATTPRBSREGPROVIDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A290021/att/Tha6A290021AttPrbsRegProviderInternal.h"


/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha6A290022AttPrbsRegProvider
    {
	tTha6A290021AttPrbsRegProvider super;
    }tTha6A290022AttPrbsRegProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaAttPrbsRegProvider Tha6A290022AttPrbsRegProviderObjectInit(ThaAttPrbsRegProvider self, ThaAttModulePrbs prbsModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022ATTPRBSREGPROVIDERINTERNAL_H_ */


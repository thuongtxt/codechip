/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtModulePdh.c
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : Default implementation of PDH module of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290022AttPrbsManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A290022AttPrbsManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttPrbsManagerMethods m_AtAttPrbsManagerOverride;

/* Save super implementations */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet TieReset(AtAttPrbsManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet TieBufRead(AtAttPrbsManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideAtAttPrbsManager(AtAttPrbsManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttPrbsManagerOverride, mMethodsGet(self), sizeof(m_AtAttPrbsManagerOverride));
        mMethodOverride(m_AtAttPrbsManagerOverride, TieBufRead);
        mMethodOverride(m_AtAttPrbsManagerOverride, TieReset);
        }

    mMethodsSet(self, &m_AtAttPrbsManagerOverride);
    }


static void Override(Tha6A290022AttPrbsManager self)
    {
    OverrideAtAttPrbsManager((AtAttPrbsManager)self);
    }


AtAttPrbsManager Tha6A290022AttPrbsManagerObjectInit(AtAttPrbsManager self, AtModulePrbs prbs)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha6A290022AttPrbsManager));

    /* Super constructor */
    if (ThaAttPrbsManagerObjectInit(self, prbs) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtAttPrbsManager Tha6A290022AttPrbsManagerNew(AtModulePrbs prbs)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttPrbsManager newObject = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha6A290022AttPrbsManager));
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290022AttPrbsManagerObjectInit(newObject, prbs);
    }



/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A290022AttPdhManager.c
 *
 * Created Date: July 26, 2018
 *
 * Author      : chaudpt
 *
 * Description : Default implementation of PDH module of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290022AttPdhManagerInternal.h"
#include "../pdh/Tha6A290022ModulePdhInternal.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A290022AttPdhManager)self)

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttPdhManagerMethods m_AtAttPdhManagerOverride;
/* Save super implementations */


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasRegister(AtAttPdhManager self, uint32 address)
	{
	AtUnused(self);
	if ((mInRange(address, 0x1080800, 0x1080FFF))
	    )
		return cAtTrue;
	return cAtFalse;
	}

static uint16 LongRead(AtAttPdhManager self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
	{
	Tha6A290022ModulePdh pdh = (Tha6A290022ModulePdh)(self->pdh);
	AtUnused(pdh);
	AtUnused(localAddress);
	AtUnused(dataBuffer);
	AtUnused(bufferLen);
	AtUnused(core);
	return 0;
	}

static uint16 LongWrite(AtAttPdhManager self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
	{
	Tha6A290022ModulePdh pdh = (Tha6A290022ModulePdh)(self->pdh);
	AtUnused(pdh);
	AtUnused(localAddress);
	AtUnused(dataBuffer);
	AtUnused(bufferLen);
	AtUnused(core);
	return 0;
	}

static void OverrideAtAttPdhManager(AtAttPdhManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttPdhManagerOverride, mMethodsGet(self), sizeof(m_AtAttPdhManagerOverride));
        mMethodOverride(m_AtAttPdhManagerOverride, HasRegister);
        mMethodOverride(m_AtAttPdhManagerOverride, LongRead);
        mMethodOverride(m_AtAttPdhManagerOverride, LongWrite);
        }

    mMethodsSet(self, &m_AtAttPdhManagerOverride);
    }

static void Override(Tha6A290022AttPdhManager self)
    {
    OverrideAtAttPdhManager((AtAttPdhManager)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290022AttPdhManager);
    }

AtAttPdhManager Tha6A290022AttPdhManagerObjectInit(AtAttPdhManager self, AtModulePdh pdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A290021AttPdhManagerObjectInit(self, pdh) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtAttPdhManager Tha6A290022AttPdhManagerNew(AtModulePdh pdh)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttPdhManager newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290022AttPdhManagerObjectInit(newObject, pdh);
    }



/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A290022ModulePrbsInternal.h
 * 
 * Created Date: Nov 14, 2018
 *
 * Description : Module PRBS internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290022MODULEPRBSREGINTERNAL_H_
#define _THA6A290022MODULEPRBSREGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A290021/prbs/Tha6A290021ModulePrbsInternal.h"
#include "Tha6A290022ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290022ModulePrbs
    {
	tTha6A000010ModulePrbs super;
    }tTha6A290022ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha6A290022ModulePrbsObjectInit(AtModulePrbs self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290022MODULEPRBSREGINTERNAL_H_ */


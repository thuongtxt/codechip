/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A290022ModulePrbs.c
 *
 * Created Date: Oct 8, 2018
 *
 * Description : Tha6A290022 module PRBS
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/prbs/ThaModulePrbsInternal.h"
#include "../../../default/att/ThaAttPrbsRegProvider.h"
#include "Tha6A290022ModulePrbsInternal.h"
#include "Tha6A290022PrbsHiReg.h"
#include "Tha6A290022PrbsLoReg.h"
#include "../../Tha6A000010/prbs/Tha6A000010PrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePrbsMethods m_AtModulePrbsOverride;
static tThaAttModulePrbsMethods  m_ThaAttModulePrbsOverride;
static tTha6A000010ModulePrbsMethods m_Tha6A000010ModulePrbsOverride;

/* Save super implementation */
static const tAtModulePrbsMethods *m_AtModulePrbsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaAttPrbsRegProvider RegProviderCreate(ThaAttModulePrbs self)
    {
    return Tha6A290022AttPrbsRegProviderNew(self);
    }

static eBool PrbsModeIsSupported(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode)
	{
	AtUnused(self);
	return ((prbsMode == cAtPrbsModePrbs15) ||
			(prbsMode == cAtPrbsModePrbsSeq)||
			(prbsMode == cAtPrbsModePrbsFixedPattern1Byte)||
			(prbsMode == cAtPrbsModePrbsFixedPattern2Bytes)||
			(prbsMode == cAtPrbsModePrbsFixedPattern3Bytes)||
			(prbsMode == cAtPrbsModePrbsFixedPattern4Bytes)) ? cAtTrue : cAtFalse;
	}

static uint8 PrbsModeToHwValue(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode)
	{
	AtUnused(self);
	if (prbsMode == cAtPrbsModePrbsFixedPattern3Bytes)
		return 0;
	if (prbsMode == cAtPrbsModePrbsFixedPattern1Byte ||
		prbsMode == cAtPrbsModePrbsFixedPattern2Bytes||
		prbsMode == cAtPrbsModePrbsFixedPattern4Bytes)
		return 1;
	if (prbsMode == cAtPrbsModePrbs15)
		return 2;
	if (prbsMode == cAtPrbsModePrbsSeq)
		return 3;
	return 0;
	}

static eAtPrbsMode PrbsModeFromHwValue(Tha6A000010ModulePrbs self,eAtPrbsMode catchedPrbsMode, uint8 mode, uint8 step)
	{
	AtUnused(self);
	AtUnused(step);
	if (mode == 0)
		return cAtPrbsModePrbsFixedPattern3Bytes;
	if (mode == 1)
		return catchedPrbsMode;
	if (mode == 2)
		return cAtPrbsModePrbs15;
	if (mode == 3)
		return cAtPrbsModePrbsSeq;
	return cAtPrbsModePrbs15;
	}

static uint8 HwStepFromPrbsMode(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode)
    {
	AtUnused(self);
	AtUnused(prbsMode);
    return 0;
    }

static eBool NeedShiftUp(Tha6A000010ModulePrbs self)
	{
	AtUnused(self);
	return cAtTrue;
	}

static uint32 PrbsDelayConfigReg(Tha6A000010ModulePrbs self, eBool isHo, uint8 slice)
	{
	AtUnused(self);
	if (isHo)
		return cAf6Reg_rtmcidcfg + (uint32) (slice*2048);
	return cAf6Reg_rtmcidcfglo + (uint32) (slice*8192);
	}

static uint32 PrbsDelayIdMask(Tha6A000010ModulePrbs self, eBool isHo)
	{
	AtUnused(self);
	if (isHo)
		return cAf6_rtmcidcfg_rtmcidcfg_Mask;
	return cAf6_rtmcidcfglo_rtmcidcfg_Mask;
	}

static uint32 PrbsDelayEnableMask(Tha6A000010ModulePrbs self, eBool isHo)
	{
	AtUnused(self);
	if (isHo)
		return cAf6_rtmcidcfg_rtmen_Mask;
	return cAf6_rtmcidcfglo_rtmen_Mask;
	}

static uint32 PrbsFixPatternReg(Tha6A000010ModulePrbs self, eBool isHo, eBool isTx)
	{
	AtUnused(self);
	if (isHo)
		return isTx? cAf6Reg_prbsgenfxptdat: cAf6Reg_prbsmonfxptdat;

	return isTx? cAf6Reg_prbsgenfxptdatlo: cAf6Reg_prbsmonfxptdatlo;
	}

static uint32 PrbsGenReg(Tha6A000010ModulePrbs self, eBool isHo)
	{
	AtUnused(self);
	if (isHo)
		return cAf6Reg_prbsgenctrl4;
	return cAf6Reg_prbsgenctrl4lo;
	}

static uint32 PrbsMonReg(Tha6A000010ModulePrbs self, eBool isHo)
    {
    AtUnused(self);
    if (isHo)
		return cAf6Reg_prbsmonctrl4;
	return cAf6Reg_prbsmonctrl4lo;
    }

static uint8 LoPrbsEngineOc48IdShift(Tha6A000010ModulePrbs self)
    {
    AtUnused(self);
    return 17;
    }

static uint8 LoPrbsEngineOc24SliceIdShift(Tha6A000010ModulePrbs self)
    {
    AtUnused(self);
    return 13;
    }

static uint32 PrbsMaxDelayReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c, uint8 slice)
	{
	uint32 address = 0;
	AtUnused(self);

	if (isHo)
		{
		address = r2c?cAf6Reg_mxdelayregr2c: cAf6Reg_mxdelayregro;
		return address + (uint32) (slice*2048);
		}

	address = r2c?cAf6Reg_mxdelayregr2clo: cAf6Reg_mxdelayregrolo;
	return address + (uint32) (slice*8192);
	}

static uint32 PrbsMinDelayReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c, uint8 slice)
	{
	uint32 address = 0;
	AtUnused(self);
	if (isHo)
		{
		address =  r2c?cAf6Reg_mindelayregr2c: cAf6Reg_mindelayregro;
		return address + (uint32) (slice*2048);
		}

	address = r2c?cAf6Reg_mindelayregr2clo: cAf6Reg_mindelayregrolo;
	return address + (uint32) (slice*8192);
	}

static uint32 PrbsModeMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTx)
	{
	AtUnused(self);
	if (isHo)
		return isTx? cAf6_prbsgenctrl4_cfgmode3_Mask: cAf6_prbsmonctrl4_cfgmode3_Mask;

	return isTx? cAf6_prbsgenctrl4lo_cfgmode3_Mask: cAf6_prbsmonctrl4lo_cfgmode3_Mask;
	}

static uint32 PrbsStepMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTx)
	{
	AtUnused(self);
	if (isHo)
		return isTx? cInvalidUint32: cInvalidUint32;

	return isTx? cInvalidUint32: cInvalidUint32;

	}
static uint32 PrbsEnableMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTx)
	{
	AtUnused(self);
	if (isHo)
		return isTx? cInvalidUint32: cInvalidUint32;

	return isTx? cInvalidUint32: cInvalidUint32;
	}

static uint32 PrbsFixPatternMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	AtUnused(self);
	if (isHo)
		return isTxDirection? cAf6_prbsgenfxptdat_fxptdat_Mask: cAf6_prbsmonfxptdat_fxptdat_Mask;
	return isTxDirection ?cAf6_prbsgenfxptdatlo_fxptdat_Mask: cAf6_prbsmonfxptdatlo_fxptdat_Mask;
	}
static eBool CounterIsSupported(Tha6A000010ModulePrbs self, uint16 counterType)
	{
	AtUnused(self);
	switch (counterType)
		{
		case cAtPrbsEngineCounterRxFrame:      return cAtTrue;
		case cAtPrbsEngineCounterRxErrorFrame: return cAtTrue;
		default: return cAtFalse;
		}
	return cAtFalse;
	}

static uint32 PrbsCounterMask(Tha6A000010ModulePrbs self, uint16 counterType)
	{
	AtUnused(self);
	switch (counterType)
		{
		case cAtPrbsEngineCounterRxFrame:      return cAf6_moncntro_goodcnt_Mask;
		case cAtPrbsEngineCounterRxErrorFrame: return cAf6_moncntro_badcnt_Mask;
		default: return cInvalidUint32;
		}
	return cInvalidUint32;
	}

static uint32 PrbsCounterReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c)
	{
	AtUnused(self);
	if (isHo)
		return r2c ? cAf6Reg_moncntro:cAf6Reg_moncntr2c;
	return r2c ? cAf6Reg_moncntrolo:cAf6Reg_moncntr2clo;
	}

static AtPrbsEngine SdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);
    AtUnused(self);
    AtUnused(engineId);

    if (channelType == cAtSdhChannelTypeVc4 || channelType ==cAtSdhChannelTypeVc4_4c || channelType ==cAtSdhChannelTypeVc4_16c)
        return Tha6A000010PrbsEngineHoVcV2New(sdhVc);

    if (channelType == cAtSdhChannelTypeVc3)
        {
        AtSdhChannel parent = AtSdhChannelParentChannelGet(sdhVc);
        if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeAu3)
            return Tha6A000010PrbsEngineHoVcV2New(sdhVc);
        return Tha6A000010PrbsEngineTu3VcV2New(sdhVc);
        }

    if ((channelType == cAtSdhChannelTypeVc11) || (channelType == cAtSdhChannelTypeVc12))
        return Tha6A000010PrbsEngineVc1xV2New(sdhVc);

    return NULL;
    }

static void OverrideThaAttModulePrbs(ThaAttModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttModulePrbsOverride, mMethodsGet(self), sizeof(m_ThaAttModulePrbsOverride));

        mMethodOverride(m_ThaAttModulePrbsOverride, RegProviderCreate);
        }

    mMethodsSet(self, &m_ThaAttModulePrbsOverride);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, m_AtModulePrbsMethods, sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, SdhVcPrbsEngineCreate);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideTha6A000010ModulePrbs(Tha6A000010ModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A000010ModulePrbsOverride, mMethodsGet(self), sizeof(m_Tha6A000010ModulePrbsOverride));
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsGenReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsMonReg);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, LoPrbsEngineOc48IdShift);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, LoPrbsEngineOc24SliceIdShift);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsMaxDelayReg);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsMinDelayReg);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, NeedShiftUp);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsModeFromHwValue);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, HwStepFromPrbsMode);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsModeToHwValue);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsModeMask);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsStepMask);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsEnableMask);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsFixPatternMask);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsFixPatternReg);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, CounterIsSupported);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsCounterMask);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsCounterReg);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDelayConfigReg);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDelayIdMask);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDelayEnableMask);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsModeIsSupported);
        }

    mMethodsSet(self, &m_Tha6A000010ModulePrbsOverride);
    }
static void Override(AtModulePrbs self)
    {
    OverrideAtModulePrbs(self);
    OverrideThaAttModulePrbs((ThaAttModulePrbs) self);
    OverrideTha6A000010ModulePrbs((Tha6A000010ModulePrbs) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290022ModulePrbs);
    }

AtModulePrbs Tha6A290022ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010ModulePrbsV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha6A290022ModulePrbsNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    return Tha6A290022ModulePrbsObjectInit(newModule, device);
    }

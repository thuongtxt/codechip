/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Prbs Engine
 * 
 * File        : Tha6A290021PrbsEngineDe1Internal.h
 * 
 * Created Date: Dec 20, 2018
 *
 * Description : Prbs Engine for DE1
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021PRBSENGINEDE1INTERNAL_H_
#define _THA6A290021PRBSENGINEDE1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A000010/prbs/Tha6A000010PrbsEngineInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021PrbsEngineDe1
    {
    tTha6A000010PrbsEngineDe1 super;
    }tTha6A290021PrbsEngineDe1;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModulePrbsRet Tha6A290021PrbsEngineDe1TxErrorRateSet(AtPrbsEngine self, eAtBerRate errorRate);
AtPrbsEngine Tha6A290021PrbsEngineDe1ObjectInit(AtPrbsEngine self, AtPdhDe1 de1);
eAtBerRate Tha6A290021PrbsEngineDe1TxErrorRateGet(AtPrbsEngine self);
eBool Tha6A290021PrbsEngineDe1ErrorForcingRateIsSupported(AtPrbsEngine self, uint32 errorRate);
eAtModulePrbsRet Tha6A290021PrbsEngineDe1ErrorForce(AtPrbsEngine self, eBool force);
eBool Tha6A290021PrbsEngineDe1ErrorIsForced(AtPrbsEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290021_PRBS_THA6A290021PRBSENGINEDE1INTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha6A290021PrbsEngineVc1xInternal.h
 * 
 * Created Date: Dec 26, 2018
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290021_PRBS_THA6A290021PRBSENGINEVC1XINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290021_PRBS_THA6A290021PRBSENGINEVC1XINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A000010/prbs/Tha6A000010PrbsEngineInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021PrbsEngineVc1x
    {
	tTha6A000010PrbsEngineVc1x super;
    }tTha6A290021PrbsEngineVc1x;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6A290021PrbsEngineVc1xObjectInit(AtPrbsEngine self, AtSdhChannel Vc1x);
#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290021_PRBS_THA6A290021PRBSENGINEVC1XINTERNAL_H_ */


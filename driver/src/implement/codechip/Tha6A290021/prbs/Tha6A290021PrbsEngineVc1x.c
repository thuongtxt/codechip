/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A290021PrbsEngineVc1x.c
 *
 * Created Date: Dec 26, 2018
 *
 * Description : PRBS VC1x
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/

#include "AtChannel.h"
#include "AtSdhPath.h"
#include "AtCrossConnect.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../Tha60290021/sdh/Tha60290021XcHiding.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../Tha60290021/sdh/Tha60290021SdhLineSideAug.h"
#include "../../Tha6A000010/prbs/Tha6A000010PrbsEngineInternal.h"
#include "Tha6A290021PrbsEngineVc1xInternal.h"
#include "Tha6A290021ModulePrbsLossTimerHiReg.h"
#include "Tha6A290021ModulePrbsLossTimerLoReg.h"
#include "../../Tha6A000010/prbs/Tha6A000010PrbsEngine.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "Tha6A290021ModulePrbs.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6A000010PrbsEngineMethods  m_Tha6A000010PrbsEngineOverride;
static tAtPrbsEngineMethods           m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static AtPw PwCreate(Tha6A000010PrbsEngine self, uint32 pwId)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    return (AtPw)AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic);
    }

static AtChannel CircuitToBind(Tha6A000010PrbsEngine self, AtChannel channel)
    {
    AtUnused(self);

    /* Get PairVc (Faceplate VC from Terminated VC) */
    return channel;
    }

static eAtModulePrbsRet PohDefaultSet(Tha6A000010PrbsEngine self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)mMethodsGet(self)->CircuitToBind(self, AtPrbsEngineChannelGet((AtPrbsEngine)self));
    uint8 startSts, sts_i;
    eAtModulePrbsRet ret = cAtOk;

    startSts = AtSdhChannelSts1Get(sdhChannel);
    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhChannel); sts_i++)
        ret |= ThaOcnStsPohInsertEnable(sdhChannel, (uint8)(startSts + sts_i), cAtTrue);

    ret |= AtSdhPathTxPslSet((AtSdhPath)sdhChannel, 0x2);
    ret |= AtSdhPathExpectedPslSet((AtSdhPath)sdhChannel, 0x2);

    return ret;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Enable(self, enable);

    if (ret != cAtOk)
        return ret;

    return enable ? PohDefaultSet((Tha6A000010PrbsEngine)self) : cAtOk;
    }

static eBool XcHidingShouldRedirect(AtPrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet(self);
    return (Tha60290021ModuleXcHideModeGet((AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc)) == cTha60290021XcHideModeDirect);
    }

static const char *ChannelDescription(AtPrbsEngine self)
    {
    if (XcHidingShouldRedirect(self))
        {
        AtChannel terVc1x = AtPrbsEngineChannelGet(self);
        return AtChannelIdDescriptionBuild(terVc1x);
        }

    return m_AtPrbsEngineMethods->ChannelDescription(self);
    }

static uint32 DisruptionAddressWithOffset(AtPrbsEngine self, uint32 address)
    {
    AtPw pw = Tha6A000010PrbsEnginePwGet(self);
    uint32 globalOffset = Tha6A000010LoPrbsEngineOffset(self, pw);

    return address + globalOffset;/*The pwId is included in globalOffset*/
    }

static uint32 DisruptionAddressWithOffsetExcludePwId(AtPrbsEngine self, uint32 address)
    {
    AtPw pw = Tha6A000010PrbsEnginePwGet(self);
    uint32 globalOffset = Tha6A000010LoPrbsEngineOffset(self,pw);

    return address + globalOffset - AtChannelHwIdGet((AtChannel)pw);
    }

static uint32 UnitToHw(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0x12EBB;
    }

static uint32 HwToUnit(AtPrbsEngine self, uint32 hwUnit)
    {
    AtUnused(self);
    if (hwUnit == 0x12EBB)
        return 1;
    return 1;
    }

static eAtRet SetLossUnit(AtPrbsEngine self, uint32 unit)
    {
    uint32 regVal, regAddr, regBase;

    regBase =  cAtt_loReg_upen_mls_cfg_Base ;
    regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);

    mRegFieldSet(regVal, cAtt_upen_mls_cfg_unit_tim_, unit);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static uint32 GetLossUnit(AtPrbsEngine self)
    {
    uint32 regVal, regAddr, regBase;
    regBase =  cAtt_loReg_upen_mls_cfg_Base;
    regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    return mRegField(regVal, cAtt_upen_mls_cfg_unit_tim_);
     }

static eBool NeedSetDefaultMode(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool  NeedSetDefaultPrbsLoss(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 DisruptionTimeConfigAddress(AtPrbsEngine self)
    {
	AtUnused(self);
    return cAtt_loReg_upen_tim_cfg_Base ;
    }

static uint32 LossConfigurationAddress(AtPrbsEngine self)
    {
	AtUnused(self);
    return  cAtt_loReg_upen_loss_cfg_Base;
    }

static eAtRet SetDefaultLossConfiguration(AtPrbsEngine self, uint32 value)
    {
    uint32 regAddr = DisruptionAddressWithOffsetExcludePwId(self, LossConfigurationAddress(self));
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);

    mRegFieldSet(regVal, cAtt_upen_loss_cfg_loss_max_cfg_, value);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_vld_max_cfg_, value);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_prbs_max_cfg_, value);

    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet DisruptionMaxExpectedTimeSet(AtPrbsEngine self, uint32 time)
    {
    uint32 regAddr = DisruptionAddressWithOffsetExcludePwId(self, DisruptionTimeConfigAddress(self));
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    uint32 unit = UnitToHw(self);
    SetLossUnit(self, unit);

    mRegFieldSet(regVal, cAtt_upen_tim_cfg_prbs_tim_cfg_, time);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);

    SetDefaultLossConfiguration(self, 0x0A);
    return cAtOk;
    }

static uint32 DisruptionMaxExpectedTimeGet(AtPrbsEngine self)
    {
    uint32 regBase =   cAtt_loReg_upen_tim_cfg_Base ;
    uint32 regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    return mRegField(regVal, cAtt_upen_tim_cfg_prbs_tim_cfg_);
    }

static uint32 DisruptionCurTimeGet(AtPrbsEngine self, eBool r2c)
    {
    uint32 regVal, regAddr, regBase, unit, ret;
    regBase =  cAtt_loReg_upen_tim_sta_Base;
    regAddr = DisruptionAddressWithOffset(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);

    unit = HwToUnit(self, GetLossUnit(self));
    ret =  mRegField(regVal, cAtt_upen_tim_sta_tim_out_) * unit;

    if (r2c)
        {
        mRegFieldSet(regVal, cAtt_upen_tim_sta_tim_out_, 0);
        mRegFieldSet(regVal, cAtt_upen_tim_sta_pos_tim_, 0);
        mRegFieldSet(regVal, cAtt_upen_tim_sta_prbslos_, 0);
        mRegFieldSet(regVal, cAtt_upen_tim_sta_max_tim_, 0);
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);
        }

    return ret;
    }

static eAtRet DisruptionLossSigThresSet(AtPrbsEngine self, uint32 thres)
    {
    uint32 regVal, regAddr, regBase;
    regBase = cAtt_loReg_upen_loss_cfg_Base ;
    regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_vld_max_cfg_, thres);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eAtRet DisruptionPrbsSyncDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    uint32 regVal, regAddr, regBase;
    regBase = cAtt_loReg_upen_loss_cfg_Base ;
    regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_prbs_max_cfg_, thres);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eAtRet DisruptionPrbsLossDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    uint32 regVal, regAddr, regBase;
    regBase = cAtt_loReg_upen_loss_cfg_Base;
    regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_loss_max_cfg_, thres);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, ChannelDescription);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultMode);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultPrbsLoss);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionMaxExpectedTimeSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionMaxExpectedTimeGet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionCurTimeGet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionLossSigThresSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionPrbsSyncDectectThresSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionPrbsLossDectectThresSet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021PrbsEngineVc1x);
    }

static void OverrideTha6A000010PrbsEngine(AtPrbsEngine self)
    {
    Tha6A000010PrbsEngine engine = (Tha6A000010PrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A000010PrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6A000010PrbsEngineOverride));

        mMethodOverride(m_Tha6A000010PrbsEngineOverride, PwCreate);
        mMethodOverride(m_Tha6A000010PrbsEngineOverride, CircuitToBind);
        }

    mMethodsSet(engine, &m_Tha6A000010PrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6A000010PrbsEngine(self);
    OverrideAtPrbsEngine(self);
    }

AtPrbsEngine Tha6A290021PrbsEngineVc1xObjectInit(AtPrbsEngine self, AtSdhChannel Vc1x)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010PrbsEngineVc1xObjectInit(self, Vc1x) == NULL)

        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A290021PrbsEngineVc1xNew(AtSdhChannel Vc1x)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290021PrbsEngineVc1xObjectInit(newEngine, Vc1x);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Prbs engine
 *
 * File        : Tha6A290021PrbsEngineDe3.c
 *
 * Created Date: Dec 20, 2018
 *
 * Description : Prbs engine for De3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290021PrbsEngineDe3Internal.h"
#include "../../../default/pdh/ThaPdhDe3.h"
#include "Tha6A290021ModulePrbsLossTimerLoReg.h"
#include "Tha6A290021ModulePrbsLossTimerHiReg.h"
#include "Tha6A290021ModulePrbs.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../../../default/sdh/ThaSdhAugInternal.h"
#include "Tha6A290021ModulePrbsInternal.h"
#include "../pdh/Tha6A290021PdhDe3AttReg.h"
#include "../../Tha6A210031/pdh/Tha6A210031PdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods           m_AtPrbsEngineOverride;

/* To save super implementation */
static const tAtPrbsEngineMethods     *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedSetDefaultMode(AtPrbsEngine self)
    {
    return Tha6A290021ModulePrbsNeedSetDefaultMode(self);
    }

static eBool NeedSetDefaultPrbsLoss(AtPrbsEngine self)
    {
    return Tha6A290021ModulePrbsNeedSetDefaultPrbsLoss(self);
    }

static eAtRet DisruptionMaxExpectedTimeSet(AtPrbsEngine self, uint32 time)
    {
    return Tha6A290021ModulePrbsDisruptionMaxExpectedTimeSet(self, time);
    }

static uint32 DisruptionMaxExpectedTimeGet(AtPrbsEngine self)
    {
    return Tha6A290021ModulePrbsDisruptionMaxExpectedTimeGet(self);
    }

static uint32 DisruptionCurTimeGet(AtPrbsEngine self, eBool r2c)
    {
	return Tha6A290021ModulePrbsDisruptionCurTimeGet(self, r2c);
    }

static eAtRet DisruptionLossSigThresSet(AtPrbsEngine self, uint32 thres)
    {
    return Tha6A290021ModulePrbsDisruptionLossSigThresSet(self, thres);
    }

static eAtRet DisruptionPrbsSyncDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    return Tha6A290021ModulePrbsDisruptionPrbsSyncDectectThresSet(self, thres);
    }

static eAtRet DisruptionPrbsLossDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    return Tha6A290021ModulePrbsDisruptionPrbsLossDectectThresSet(self, thres);
    }

static uint32 ErrorRateSw2Hw(eAtBerRate errorRate)
    {
    switch ((uint32)errorRate)
        {
        case cAtBerRate1E2: return 0;
        case cAtBerRate1E3: return 1;
        case cAtBerRate1E4: return 2;
        case cAtBerRate1E5: return 3;
        case cAtBerRate1E6: return 4;
        case cAtBerRate1E7: return 5;
        default:            return 5;
        }
    }

static eAtBerRate ErrorRateHw2Sw(uint32 errorRate)
    {
    switch (errorRate)
        {
        case 0:  return cAtBerRate1E2;
        case 1:  return cAtBerRate1E3;
        case 2:  return cAtBerRate1E4;
        case 3:  return cAtBerRate1E5;
        case 4:  return cAtBerRate1E6;
        case 5:  return cAtBerRate1E7;
        default: return cAtBerRate1E7;
        }
    }

static eAtModulePrbsRet TxErrorRateSet(AtPrbsEngine self, eAtBerRate errorRate)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    uint32 regAddr, regVal;
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController(channel);

    if (!AtPrbsEngineErrorForcingRateIsSupported(self, errorRate))
        return cAtErrorModeNotSupport;

    regAddr = mMethodsGet(controller)->RealAddress(controller, cReg_de3berctrl_pen);
    regVal = mChannelHwRead(channel,regAddr, cAtModulePdh);


    mRegFieldSet(regVal, c_de3berctrl_pen_BER_mode_,ErrorRateSw2Hw(errorRate));
    mChannelHwWrite(channel, regAddr, regVal, cAtModulePdh);
    return cAtOk;
    }

static eAtBerRate TxErrorRateGet(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController(channel);
    uint32 regAddr = mMethodsGet(controller)->RealAddress(controller, cReg_de3berctrl_pen);
    uint32 regVal = mChannelHwRead(channel,regAddr, cAtModulePdh);
    uint32 errorRate = mRegField(regVal, c_de3berctrl_pen_BER_mode_);

    if (!AtPrbsEngineErrorIsForced(self))
        return cAtBerRateUnknown;
    return ErrorRateHw2Sw(errorRate);
    }

static eBool ErrorForcingRateIsSupported(AtPrbsEngine self, uint32 errorRate)
    {
    AtUnused(self);

    switch (errorRate)
        {
        case cAtBerRate1E2:
        case cAtBerRate1E3:
        case cAtBerRate1E4:
        case cAtBerRate1E5:
        case cAtBerRate1E6:
        case cAtBerRate1E7:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController(channel);
    uint32 regAddr = mMethodsGet(controller)->RealAddress(controller, cReg_de3berctrl_pen);
    uint32 regVal = mChannelHwRead(channel,regAddr, cAtModulePdh);
    mRegFieldSet(regVal, c_de3berctrl_pen_ber_en_, mBoolToBin(force));
    mChannelHwWrite(channel, regAddr, regVal, cAtModulePdh);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController(channel);
    uint32 regAddr = mMethodsGet(controller)->RealAddress(controller, cReg_de3berctrl_pen);
    uint32 regVal = mChannelHwRead(channel,regAddr, cAtModulePdh);
    return (mRegField(regVal, c_de3berctrl_pen_ber_en_) == 1) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;
    return ErrorForce(self, cAtFalse);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultMode);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultPrbsLoss);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionMaxExpectedTimeSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionMaxExpectedTimeGet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionCurTimeGet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionLossSigThresSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionPrbsSyncDectectThresSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionPrbsLossDectectThresSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateGet);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForcingRateIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, Init);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021PrbsEngineDe3);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

AtPrbsEngine Tha6A290021PrbsEngineDe3ObjectInit(AtPrbsEngine self, AtPdhDe3 de3)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010PrbsEngineDe3ObjectInit(self, de3) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A290021PrbsEngineDe3New(AtPdhDe3 de3)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290021PrbsEngineDe3ObjectInit(newEngine, de3);
    }


/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date: March 28, 2018                                                 
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _Tha6A290021PRBSHIREG_H_
#define _Tha6A290021PRBSHIREG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : PRBS Generator Control Register
Reg Addr   : 0x06_8100
Reg Formula: 0x06_8100 + $slice*2048 + $stsid
    Where  : 
           + $stsid(0-47):sts id
           + $slice (0-3):slice
Reg Desc   : 
this register is used to config mode for PRBS GEN and get status of PRBS in case of mode PRBS

------------------------------------------------------------------------------*/
#define cAf6Reg_prbsgenctrl4                                                                          0x068100
/*--------------------------------------
BitField Name: gen_cas_en
BitField Type: RW
BitField Desc: Current PRBS value
BitField Bits: [17]
--------------------------------------*/
#define cAf6_prbsgenctrl4_gen_cas_en_Mask                                                                  cBit17
#define cAf6_prbsgenctrl4_gen_cas_en_Shift                                                                     17


/*--------------------------------------
BitField Name: oprbs
BitField Type: RO
BitField Desc: Current PRBS value
BitField Bits: [16:2]
--------------------------------------*/
#define cAf6_prbsgenctrl4_oprbs_Mask                                                                  cBit16_2
#define cAf6_prbsgenctrl4_oprbs_Shift                                                                        2

/*--------------------------------------
BitField Name: cfgmode3
BitField Type: RW
BitField Desc: data gen mode. 2'b0: Fix-pattern mode 3 bytes 2'b1: Fix-pattern
mode 1-2-4 bytes 2'b2: PRBS mode 2'b3: Sequence mode
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_prbsgenctrl4_cfgmode3_Mask                                                                cBit1_0
#define cAf6_prbsgenctrl4_cfgmode3_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : PRBS Monitor Control Register
Reg Addr   : 0x06_8000
Reg Formula: 0x06_8000 + $slice*2048 + $stsid
    Where  : 
           + $stsid(0-47):sts id
           + $slice (0-3):slice
Reg Desc   : 
this register is used to config mode for PRBS MON and get monitoring status

------------------------------------------------------------------------------*/
#define cAf6Reg_prbsmonctrl4                                                                          0x068000
/*--------------------------------------
BitField Name: mon_cas_en
BitField Type: RW
BitField Desc: 1/0 (vld/no vld). No valid means no data received.
BitField Bits: [20]
--------------------------------------*/
#define cAf6_prbsmonctrl4_mon_cas_en_Mask                                                                     cBit20
#define cAf6_prbsmonctrl4_mon_cas_en_Shift                                                                        20


/*--------------------------------------
BitField Name: ovld
BitField Type: RC
BitField Desc: 1/0 (vld/no vld). No valid means no data received.
BitField Bits: [18]
--------------------------------------*/
#define cAf6_prbsmonctrl4_ovld_Mask                                                                     cBit18
#define cAf6_prbsmonctrl4_ovld_Shift                                                                        18

/*--------------------------------------
BitField Name: oerr
BitField Type: RC
BitField Desc: 1/0 (error/error free)
BitField Bits: [17]
--------------------------------------*/
#define cAf6_prbsmonctrl4_oerr_Mask                                                                     cBit17
#define cAf6_prbsmonctrl4_oerr_Shift                                                                        17

/*--------------------------------------
BitField Name: osyn
BitField Type: RO
BitField Desc: successfully synchronized (for PRBS & sequence mode)
BitField Bits: [16]
--------------------------------------*/
#define cAf6_prbsmonctrl4_osyn_Mask                                                                     cBit16
#define cAf6_prbsmonctrl4_osyn_Shift                                                                        16

/*--------------------------------------
BitField Name: oprbs
BitField Type: RO
BitField Desc: Current PRBS value
BitField Bits: [15:2]
--------------------------------------*/
#define cAf6_prbsmonctrl4_oprbs_Mask                                                                  cBit15_2
#define cAf6_prbsmonctrl4_oprbs_Shift                                                                        2

/*--------------------------------------
BitField Name: cfgmode3
BitField Type: RW
BitField Desc: data mon mode. 2'b0: Fix-pattern mode 3 bytes 2'b1: Fix-pattern
mode 1-2-4 bytes 2'b2: PRBS mode 2'b3: Sequence mode
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_prbsmonctrl4_cfgmode3_Mask                                                                cBit1_0
#define cAf6_prbsmonctrl4_cfgmode3_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : PRBS Generator Fix-Pattern Dat
Reg Addr   : 0x6_8140
Reg Formula: 0x6_8140 + $slice*2048 + $stsid
    Where  : 
           + $stsid(0-47):sts id
           + $slice (0-3):slice
Reg Desc   : 
this register is used to config fix-pattern data for fix-pattern mode

------------------------------------------------------------------------------*/
#define cAf6Reg_prbsgenfxptdat                                                                         0x68140

/*--------------------------------------
BitField Name: fxptdat
BitField Type: RW
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prbsgenfxptdat_fxptdat_Mask                                                              cBit31_0
#define cAf6_prbsgenfxptdat_fxptdat_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : PRBS Monitor Fix-Pattern Dat
Reg Addr   : 0x06_8040
Reg Formula: 0x06_8040 + $slice*2048 + $stsid
    Where  : 0x068040 + 1000 + $slice*2048 + $stsid
           + $stsid(0-47):sts id
           + $slice (0-3):slice
Reg Desc   : 
this register is used to config fix-pattern data for fix-pattern mode

------------------------------------------------------------------------------*/
#define cAf6Reg_prbsmonfxptdat                                                                        0x068040

/*--------------------------------------
BitField Name: fxptdat
BitField Type: RW
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prbsmonfxptdat_fxptdat_Mask                                                              cBit31_0
#define cAf6_prbsmonfxptdat_fxptdat_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Round-trip delay channel config
Reg Addr   : 0x06_8180
Reg Formula: 0x06_8180 + $slice*2048
    Where  : 
           + $slice (0-3):slice
Reg Desc   : 
this register is used to config Round-trip delay channel id to be measured.

------------------------------------------------------------------------------*/
#define cAf6Reg_rtmcidcfg                                                                             0x068180
#define cAf6Reg_rtmcidcfg_WidthVal                                                                          32

/*--------------------------------------
BitField Name: rtmcidcfg
BitField Type: RW
BitField Desc: sts id config
BitField Bits: [6:1]
--------------------------------------*/
#define cAf6_rtmcidcfg_rtmcidcfg_Mask                                                                  cBit6_1
#define cAf6_rtmcidcfg_rtmcidcfg_Shift                                                                       1

/*--------------------------------------
BitField Name: rtmen
BitField Type: RW
BitField Desc: roundtrip delay mode en
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rtmcidcfg_rtmen_Mask                                                                        cBit0
#define cAf6_rtmcidcfg_rtmen_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Monitor Counter Read Only
Reg Addr   : 0x06_8080
Reg Formula: 0x06_8080 + $slice*2048 + $stsid
    Where  : 
           + $stsid(0-47):sts id
           + $slice (0-3):slice
Reg Desc   : 
this register is used to accummulate good and bad monitor counter

------------------------------------------------------------------------------*/
#define cAf6Reg_moncntro                                                                              0x068080

/*--------------------------------------
BitField Name: goodcnt
BitField Type: RO
BitField Desc: good counter
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_moncntro_goodcnt_Mask                                                                   cBit31_16
#define cAf6_moncntro_goodcnt_Shift                                                                         16

/*--------------------------------------
BitField Name: badcnt
BitField Type: RO
BitField Desc: bad counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_moncntro_badcnt_Mask                                                                     cBit15_0
#define cAf6_moncntro_badcnt_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Monitor Counter Read To Clear
Reg Addr   : 0x06_80c0
Reg Formula: 0x06_80c0 + $slice*2048 + $stsid
    Where  : 
           + $stsid(0-47):sts id
           + $slice (0-3):slice
Reg Desc   : 
this register is used to accummulate good and bad monitor counter

------------------------------------------------------------------------------*/
#define cAf6Reg_moncntr2c                                                                             0x0680c0

/*--------------------------------------
BitField Name: goodcnt
BitField Type: RC
BitField Desc: good counter
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_moncntr2c_goodcnt_Mask                                                                  cBit31_16
#define cAf6_moncntr2c_goodcnt_Shift                                                                        16

/*--------------------------------------
BitField Name: badcnt
BitField Type: RC
BitField Desc: bad counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_moncntr2c_badcnt_Mask                                                                    cBit15_0
#define cAf6_moncntr2c_badcnt_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Max Delay Register Read Only
Reg Addr   : 0x06_81c0
Reg Formula: 0x06_81c0 + $slice*2048
    Where  : 
           + $slice (0-3):slice
Reg Desc   : 
this register is used to accummulate max delay for round trip mode

------------------------------------------------------------------------------*/
#define cAf6Reg_mxdelayregro                                                                          0x0681c0

/*--------------------------------------
BitField Name: maxdelay
BitField Type: RO
BitField Desc: max delay
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mxdelayregro_maxdelay_Mask                                                               cBit31_0
#define cAf6_mxdelayregro_maxdelay_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Max Delay Register Read toclear
Reg Addr   : 0x06_81c4
Reg Formula: 0x06_81c4 + $slice*2048
    Where  : 
           + $slice (0-3):slice
Reg Desc   : 
this register is used to accummulate max delay for round trip mode

------------------------------------------------------------------------------*/
#define cAf6Reg_mxdelayregr2c                                                                         0x0681c4

/*--------------------------------------
BitField Name: maxdelay
BitField Type: RC
BitField Desc: max delay
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mxdelayregr2c_maxdelay_Mask                                                              cBit31_0
#define cAf6_mxdelayregr2c_maxdelay_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Min Delay Register Read Only
Reg Addr   : 0x06_81c1
Reg Formula: 0x06_81c1 + $slice*2048
    Where  : 
           + $slice (0-3):slice
Reg Desc   : 
this register is used to accummulate min delay for round trip mode

------------------------------------------------------------------------------*/
#define cAf6Reg_mindelayregro                                                                         0x0681c1

/*--------------------------------------
BitField Name: mindelay
BitField Type: RO
BitField Desc: min delay
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mindelayregro_mindelay_Mask                                                              cBit31_0
#define cAf6_mindelayregro_mindelay_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Min Delay Register Read Only
Reg Addr   : 0x06_81c5
Reg Formula: 0x06_81c5 + $slice*2048
    Where  : 
           + $slice (0-3):slice
Reg Desc   : 
this register is used to accummulate min delay for round trip mode

------------------------------------------------------------------------------*/
#define cAf6Reg_mindelayregr2c                                                                         0x0681c5

/*--------------------------------------
BitField Name: mindelay
BitField Type: RC
BitField Desc: min delay
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mindelayregr2c_mindelay_Mask                                                              cBit31_0
#define cAf6_mindelayregr2c_mindelay_Shift                                                                    0

#endif /* _AF6_REG_AF6CNC0011_RD_PRBS_HI_H_ */

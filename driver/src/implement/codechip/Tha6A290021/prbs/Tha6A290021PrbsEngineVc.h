/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A000010PrbsEngine.h
 * 
 * Created Date: Dec 2, 2015
 *
 * Description : LO-Path PRBS Engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha6A290021PRBSENGINEVCINTERNAL_H_
#define _Tha6A290021PRBSENGINEVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A000010/prbs/Tha6A000010PrbsEngineInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021PrbsEngineVc
    {
    tTha6A000010PrbsEngine super;
    }tTha6A290021PrbsEngineVc;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6A290021PrbsEngineVcNew(AtSdhChannel vc);
AtPrbsEngine Tha6A290021PrbsEngineVcObjectInit(AtPrbsEngine self, AtSdhChannel vc);

#ifdef __cplusplus
}
#endif
#endif /* _Tha6A290021PRBSENGINEVCINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A290021ModulePrbs.c
 *
 * Created Date: Oct 8, 2015
 *
 * Description : Tha6A290021 module PRBS
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/prbs/ThaModulePrbsInternal.h"
#include "../../../default/att/ThaAttPrbsRegProvider.h"
#include "Tha6A290021ModulePrbsInternal.h"
#include "Tha6A290021PrbsEngineVc.h"
#include "Tha6A290021PrbsHiReg.h"
#include "Tha6A290021PrbsLoReg.h"
#include "../../Tha6A000010/prbs/Tha6A000010PrbsEngineInternal.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "Tha6A290021ModulePrbsLossTimerHiReg.h"
#include "Tha6A290021ModulePrbsLossTimerLoReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

static uint8 m_methodsInit = 0;

/* Override */

static tAtModulePrbsMethods m_AtModulePrbsOverride;
static tThaAttModulePrbsMethods  m_ThaAttModulePrbsOverride;
static tTha6A000010ModulePrbsMethods m_Tha6A000010ModulePrbsOverride;

/* Save super implementation */

static const tAtModulePrbsMethods *m_AtModulePrbsMethods = NULL;
static const tTha6A000010ModulePrbsMethods *m_Tha6A000010ModulePrbsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 DisruptionAddressWithOffset(AtPrbsEngine self, uint32 address)
    {
    AtPw pw = Tha6A000010PrbsEnginePwGet(self);
    uint32 globalOffset = Tha6A000010LoPrbsEngineOffset(self,pw);

    return address + globalOffset;/*The pwId is included in globalOffset*/
    }

static uint32 DisruptionAddressWithOffsetExcludePwId(AtPrbsEngine self, uint32 address)
    {
    AtPw pw = Tha6A000010PrbsEnginePwGet(self);
    uint32 globalOffset = Tha6A000010LoPrbsEngineOffset(self,pw);

    return address + globalOffset - AtChannelHwIdGet((AtChannel)pw);
    }

static uint32 UnitToHw(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0x12EBB;
    }

static uint32 HwToUnit(AtPrbsEngine self, uint32 hwUnit)
    {
    AtUnused(self);
    AtUnused(hwUnit);
    /*hwUnit == 0x12EBB*/
    return 1;
    }

static eAtRet SetLossUnit(AtPrbsEngine self, uint32 unit)
    {
    uint32 regVal, regAddr, regBase;

    regBase =  cAtt_loReg_upen_mls_cfg_Base;
    regAddr =  DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);

    mRegFieldSet(regVal, cAtt_upen_mls_cfg_unit_tim_, unit);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static uint32 GetLossUnit(AtPrbsEngine self)
    {
    uint32 regVal, regAddr, regBase;
    regBase = cAtt_loReg_upen_mls_cfg_Base;
    regAddr =  DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    return mRegField(regVal, cAtt_upen_mls_cfg_unit_tim_);
     }

static uint32 DisruptionTimeConfigAddress(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtt_loReg_upen_tim_cfg_Base;
    }

static uint32 LossConfigurationAddress(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtt_loReg_upen_loss_cfg_Base;
    }

static eAtRet SetDefaultLossConfiguration(AtPrbsEngine self, uint32 value)
    {
    uint32 regAddr = DisruptionAddressWithOffsetExcludePwId(self, LossConfigurationAddress(self));
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);

    mRegFieldSet(regVal, cAtt_upen_loss_cfg_loss_max_cfg_, value);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_vld_max_cfg_, value);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_prbs_max_cfg_, value);

    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static ThaAttPrbsRegProvider RegProviderCreate(ThaAttModulePrbs self)
    {
    return Tha6A290021AttPrbsRegProviderNew(self);
    }

static uint32 StartVersionSupportDataMode(Tha6A000010ModulePrbs self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0560);
    }

static uint32 StartVersionSupportFixedPattern4bytes(Tha6A000010ModulePrbs self)
	{
	AtUnused(self);
	return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0548);
	}

static uint32 StartVersionSupportDelay(Tha6A000010ModulePrbs self)
	{
	AtUnused(self);
	return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0548);
	}

static uint32 StartVersionSupportDelayAverage(ThaAttModulePrbs self)
	{
	AtUnused(self);
	return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0549);
	}

static ThaVersionReader VersionReader(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaDeviceVersionReader(device);
    }

static eBool NeedFixedPattern4bytes(Tha6A000010ModulePrbs self)
	{
	uint32 startVersionHasThis = StartVersionSupportFixedPattern4bytes(self);
	uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtModule)self));
	return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
	}

static eBool NeedShiftUp(Tha6A000010ModulePrbs self)
	{
	AtUnused(self);
	return cAtFalse;
	}

static uint32 ConvertSwToHwFixedPattern(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
	{
	uint8 i=0;
	uint32 value = 0;
	AtUnused(self);
	if (prbsMode == cAtPrbsModePrbsFixedPattern1Byte)
		{
		for (i=0; i<4; i++)
		    {
			if (i == 0)
			    value = (fixedPattern & cBit7_0);
			else
			    value = (value << 8) | (fixedPattern & cBit7_0);
		    }
		}
	else if (prbsMode == cAtPrbsModePrbsFixedPattern2Bytes)
		{
		for (i=0; i<2; i++)
		    {
			if (i == 0)
			    value = (fixedPattern & cBit15_0);
			else
			    value = (value << 16) | (fixedPattern & cBit15_0);
		    }
		}
	else if (prbsMode == cAtPrbsModePrbsFixedPattern3Bytes)
		value = (fixedPattern & cBit23_0)<<8;
	else
		value = fixedPattern;

	return value;
	}

static uint32 ConvertHwToSwFixedPattern(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
	{
	uint32 value = fixedPattern;

	AtUnused(self);
	if (prbsMode == cAtPrbsModePrbsFixedPattern1Byte)
		value = fixedPattern & cBit7_0;
	else if (prbsMode == cAtPrbsModePrbsFixedPattern2Bytes)
		value = fixedPattern & cBit15_0;
	else if (prbsMode == cAtPrbsModePrbsFixedPattern3Bytes)
		value = (fixedPattern >> 8) & cBit23_0;
	return value;
	}


static eBool PrbsModeIsSupported(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode)
	{
	if (!NeedFixedPattern4bytes(self))
		return m_Tha6A000010ModulePrbsMethods->PrbsModeIsSupported(self, prbsMode);

	return ((prbsMode == cAtPrbsModePrbs15)
			|| (prbsMode == cAtPrbsModePrbsSeq)
			|| (prbsMode == cAtPrbsModePrbsFixedPattern1Byte)
			|| (prbsMode == cAtPrbsModePrbsFixedPattern2Bytes)
			|| (prbsMode == cAtPrbsModePrbsFixedPattern3Bytes)
			|| (prbsMode == cAtPrbsModePrbsFixedPattern4Bytes))
			? cAtTrue : cAtFalse;
	}

static uint8 PrbsModeToHwValue(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode)
	{
	if (!NeedFixedPattern4bytes(self))
		return m_Tha6A000010ModulePrbsMethods->PrbsModeToHwValue(self, prbsMode);

	if (prbsMode == cAtPrbsModePrbsFixedPattern3Bytes)
		return 0;
	if (prbsMode == cAtPrbsModePrbsFixedPattern1Byte||
		prbsMode == cAtPrbsModePrbsFixedPattern2Bytes||
		prbsMode == cAtPrbsModePrbsFixedPattern4Bytes)
		return 1;
	if (prbsMode == cAtPrbsModePrbs15)
		return 2;
	if (prbsMode == cAtPrbsModePrbsSeq)
		return 3;
	return 0;
	}

static eAtPrbsMode PrbsModeFromHwValue(Tha6A000010ModulePrbs self,eAtPrbsMode catchedPrbsMode, uint8 mode, uint8 step)
	{
	if (!NeedFixedPattern4bytes(self))
		return m_Tha6A000010ModulePrbsMethods->PrbsModeFromHwValue(self, catchedPrbsMode, mode, step);

	if (mode == 0)
		return cAtPrbsModePrbsFixedPattern3Bytes;
	if (mode == 1)
		return catchedPrbsMode;
	if (mode == 2)
		return cAtPrbsModePrbs15;
	if (mode == 3)
		return cAtPrbsModePrbsSeq;
	return cAtPrbsModePrbs15;
	}

static uint8 HwStepFromPrbsMode(Tha6A000010ModulePrbs self, eAtPrbsMode prbsMode)
    {
	if (!NeedFixedPattern4bytes(self))
		return m_Tha6A000010ModulePrbsMethods->HwStepFromPrbsMode(self, prbsMode);

    return 0;
    }

static eBool NeedDelay(Tha6A000010ModulePrbs self)
	{
	uint32 startVersionHasThis = StartVersionSupportDelay(self);
	uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtModule)self));
	return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
	}

static uint32 PrbsDelayConfigReg(Tha6A000010ModulePrbs self, eBool isHo, uint8 slice)
	{
	if (!NeedDelay(self))
		return m_Tha6A000010ModulePrbsMethods->PrbsDelayConfigReg(self, isHo, slice);

	if (isHo)
		return cAf6Reg_rtmcidcfg + (uint32) (slice*2048);
	return cAf6Reg_rtmcidcfglo + (uint32) (slice*8192);
	}

static uint32 PrbsDelayIdMask(Tha6A000010ModulePrbs self, eBool isHo)
	{
	if (!NeedDelay(self))
		return m_Tha6A000010ModulePrbsMethods->PrbsDelayIdMask(self, isHo);

	if (isHo)
		return cAf6_rtmcidcfg_rtmcidcfg_Mask;
	return cAf6_rtmcidcfglo_rtmcidcfg_Mask;
	}

static uint32 PrbsDelayEnableMask(Tha6A000010ModulePrbs self, eBool isHo)
	{
	if (!NeedDelay(self))
		return m_Tha6A000010ModulePrbsMethods->PrbsDelayEnableMask(self, isHo);

	if (isHo)
		return cAf6_rtmcidcfg_rtmen_Mask;
	return cAf6_rtmcidcfglo_rtmen_Mask;
	}

static uint32 PrbsFixPatternReg(Tha6A000010ModulePrbs self, eBool isHo, eBool isTx)
	{
	if (!NeedFixedPattern4bytes(self))
		return m_Tha6A000010ModulePrbsMethods->PrbsFixPatternReg(self, isHo, isTx);

	if (isHo)
		return isTx? cAf6Reg_prbsgenfxptdat: cAf6Reg_prbsmonfxptdat;
	return isTx? cAf6Reg_prbsgenfxptdatlo: cAf6Reg_prbsmonfxptdatlo;
	}

static uint32 PrbsGenReg(Tha6A000010ModulePrbs self, eBool isHo)
	{

	if (!NeedFixedPattern4bytes(self))
		return m_Tha6A000010ModulePrbsMethods->PrbsGenReg(self, isHo);

	if (isHo)
		return cAf6Reg_prbsgenctrl4;
	return cAf6Reg_prbsgenctrl4lo;
	}

static uint32 PrbsMonReg(Tha6A000010ModulePrbs self, eBool isHo)
    {

	if (!NeedFixedPattern4bytes(self))
		{
		if (isHo)
			return m_Tha6A000010ModulePrbsMethods->PrbsMonReg(self, isHo);
		return 0x500800;/*cReg_prbslomon_Base 1024*/
		}

	if (isHo)
		return cAf6Reg_prbsmonctrl4;
	return cAf6Reg_prbsmonctrl4lo;
    }

static uint8 LoPrbsEngineOc48IdShift(Tha6A000010ModulePrbs self)
    {
	if (!NeedFixedPattern4bytes(self))	
    	return 17; /* bit19_17 in code RTL, 4096 */
    return 17;
    }

static uint8 LoPrbsEngineOc24SliceIdShift(Tha6A000010ModulePrbs self)
    {
	if (!NeedFixedPattern4bytes(self))	
	    return 10; /* 1024 */
	return 13;
    }

static uint32 PrbsMaxDelayReg(Tha6A000010ModulePrbs self,eBool isHo, eBool r2c, uint8 slice)
	{
	uint32 address = 0;
	if (!NeedDelay(self))	
		return cInvalidUint32;

	if (isHo)
		{
		address = r2c?cAf6Reg_mxdelayregr2c: cAf6Reg_mxdelayregro;
		return address + (uint32) (slice*2048);
		}

	address = r2c?cAf6Reg_mxdelayregr2clo: cAf6Reg_mxdelayregrolo;
	return address + (uint32) (slice*8192);
	}

static uint32 PrbsMinDelayReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c, uint8 slice)
	{
	uint32 address = 0;
	if (!NeedDelay(self))	
		return cInvalidUint32;

	if (isHo)
		{
		address = r2c?cAf6Reg_mindelayregr2c: cAf6Reg_mindelayregro;
		return address + (uint32) (slice*2048);
		}
	address =  r2c?cAf6Reg_mindelayregr2clo: cAf6Reg_mindelayregrolo;
	return address + (uint32) (slice*8192);
	}

static uint32 PrbsAverageDelayReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c, uint8 slice)
	{
	uint32 address = 0;

	if (!NeedDelay(self))
		return cInvalidUint32;

	if (!ThaAttModulePrbsNeedDelayAverage((ThaAttModulePrbs)self))
		return cInvalidUint32;

	if (isHo)
		{
		address = r2c?cAf6Reg_mindelayregr2c: cAf6Reg_mindelayregro;
		return address + (uint32) (slice*2048) + 1;
		}
	address =  r2c?cAf6Reg_mindelayregr2clo: cAf6Reg_mindelayregrolo;
	return address + (uint32) (slice*8192) + 1;
	}


static eBool PrbsDataModeIsSupported(Tha6A000010ModulePrbs self)
    {
    uint32 startVersionHasThis = StartVersionSupportDataMode(self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtModule)self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 PrbsModeMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	if (!NeedFixedPattern4bytes(self))
		return m_Tha6A000010ModulePrbsMethods->PrbsModeMask(self, isHo, isTxDirection);

	if (isHo)
		return isTxDirection? cAf6_prbsgenctrl4_cfgmode3_Mask: cAf6_prbsmonctrl4_cfgmode3_Mask;

	return isTxDirection? cAf6_prbsgenctrl4lo_cfgmode3_Mask: cAf6_prbsmonctrl4lo_cfgmode3_Mask;
	}

static uint32 PrbsDataModeMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (!PrbsDataModeIsSupported(self))
        return m_Tha6A000010ModulePrbsMethods->PrbsDataModeMask(self, isHo, isTxDirection);

    if (isHo)
        return isTxDirection? cAf6_prbsgenctrl4_gen_cas_en_Mask: cAf6_prbsmonctrl4_mon_cas_en_Mask;

    return isTxDirection? cAf6_prbsgenctrl4lo_gen_cas_en_Mask: cAf6_prbsmonctrl4lo_mon_cas_en_Mask;
    }

static uint32 PrbsStepMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	if (!NeedFixedPattern4bytes(self))
		return m_Tha6A000010ModulePrbsMethods->PrbsStepMask(self, isHo, isTxDirection);

	if (isHo)
		return isTxDirection? cInvalidUint32: cInvalidUint32;

	return isTxDirection? cInvalidUint32: cInvalidUint32;

	}

static uint32 PrbsEnableMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	if (!NeedFixedPattern4bytes(self))
		return m_Tha6A000010ModulePrbsMethods->PrbsEnableMask(self, isHo, isTxDirection);

	if (isHo)
		return isTxDirection? cInvalidUint32: cInvalidUint32;

	return isTxDirection? cInvalidUint32: cInvalidUint32;
	}

static uint32 PrbsFixPatternMask(Tha6A000010ModulePrbs self, eBool isHo, eBool isTxDirection)
	{
	if (!NeedFixedPattern4bytes(self))
		return m_Tha6A000010ModulePrbsMethods->PrbsFixPatternMask(self, isHo, isTxDirection);

	if (isHo)
		return isTxDirection? cAf6_prbsgenfxptdat_fxptdat_Mask: cAf6_prbsmonfxptdat_fxptdat_Mask;
	return isTxDirection ?cAf6_prbsgenfxptdatlo_fxptdat_Mask: cAf6_prbsmonfxptdatlo_fxptdat_Mask;
	}

static eBool CounterIsSupported(Tha6A000010ModulePrbs self, uint16 counterType)
	{
	if (!NeedFixedPattern4bytes(self))
		return cAtFalse;

	switch (counterType)
		{
		case cAtPrbsEngineCounterRxSync:       return cAtTrue;
		case cAtPrbsEngineCounterRxErrorFrame: return cAtTrue;
		default:                               return cAtFalse;
		}
	return cAtFalse;
	}

static uint32 PrbsCounterMask(Tha6A000010ModulePrbs self, uint16 counterType)
	{
	if (!NeedFixedPattern4bytes(self))
		return cInvalidUint32;

	switch (counterType)
		{
		case cAtPrbsEngineCounterRxSync:       return cAf6_moncntro_goodcnt_Mask;
		case cAtPrbsEngineCounterRxErrorFrame: return cAf6_moncntro_badcnt_Mask;
		default: return cInvalidUint32;
		}
	return cInvalidUint32;
	}

static uint32 PrbsCounterReg(Tha6A000010ModulePrbs self, eBool isHo, eBool r2c)
	{	
	if (!NeedFixedPattern4bytes(self))
		return cInvalidUint32;
	
	if (isHo)
		return r2c ? cAf6Reg_moncntr2c:cAf6Reg_moncntro;
	return r2c ? cAf6Reg_moncntr2clo:cAf6Reg_moncntrolo;
	}

static AtPrbsEngine SdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);
    AtUnused(self);
    AtUnused(engineId);

    if ((channelType == cAtSdhChannelTypeVc11) || (channelType == cAtSdhChannelTypeVc12))
        return Tha6A290021PrbsEngineVc1xNew(sdhVc);

    return Tha6A290021PrbsEngineVcNew(sdhVc);
    }

static AtPrbsEngine De3PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A290021PrbsEngineDe3New(de3);
    }

static AtPrbsEngine De1PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A290021PrbsEngineDe1New(de1);
    }

static void OverrideThaAttModulePrbs(ThaAttModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttModulePrbsOverride, mMethodsGet(self), sizeof(m_ThaAttModulePrbsOverride));

        mMethodOverride(m_ThaAttModulePrbsOverride, RegProviderCreate);
        mMethodOverride(m_ThaAttModulePrbsOverride, StartVersionSupportDelayAverage);
        }

    mMethodsSet(self, &m_ThaAttModulePrbsOverride);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, m_AtModulePrbsMethods, sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, SdhVcPrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, De3PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, De1PrbsEngineCreate);

        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideTha6A000010ModulePrbs(Tha6A000010ModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6A000010ModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A000010ModulePrbsOverride, m_Tha6A000010ModulePrbsMethods, sizeof(m_Tha6A000010ModulePrbsOverride));
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, NeedShiftUp);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsGenReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsMonReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, LoPrbsEngineOc48IdShift);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, LoPrbsEngineOc24SliceIdShift);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsMaxDelayReg);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsMinDelayReg);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsAverageDelayReg);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsModeFromHwValue);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, HwStepFromPrbsMode);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsModeToHwValue);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsModeMask);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDataModeMask);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsStepMask);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsEnableMask);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsFixPatternMask);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsFixPatternReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, CounterIsSupported);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsCounterMask);
		mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsCounterReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDelayConfigReg);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDelayIdMask);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDelayEnableMask);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsModeIsSupported);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, ConvertSwToHwFixedPattern);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, ConvertHwToSwFixedPattern);
        mMethodOverride(m_Tha6A000010ModulePrbsOverride, PrbsDataModeIsSupported);
        }

    mMethodsSet(self, &m_Tha6A000010ModulePrbsOverride);
    }
static void Override(AtModulePrbs self)
    {
	OverrideAtModulePrbs(self);
    OverrideThaAttModulePrbs((ThaAttModulePrbs) self);
    OverrideTha6A000010ModulePrbs((Tha6A000010ModulePrbs) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021ModulePrbs);
    }

AtModulePrbs Tha6A290021ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha6A290021ModulePrbsNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    return Tha6A290021ModulePrbsObjectInit(newModule, device);
    }

eBool Tha6A290021ModulePrbsNeedSetDefaultMode(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

eBool  Tha6A290021ModulePrbsNeedSetDefaultPrbsLoss(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

eAtRet Tha6A290021ModulePrbsDisruptionMaxExpectedTimeSet(AtPrbsEngine self, uint32 time)
    {
    uint32 regAddr = DisruptionAddressWithOffsetExcludePwId(self, DisruptionTimeConfigAddress(self));
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    uint32 unit = UnitToHw(self);
    SetLossUnit(self, unit);

    mRegFieldSet(regVal, cAtt_upen_tim_cfg_prbs_tim_cfg_, time);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);

    SetDefaultLossConfiguration(self, 0x0A);
    return cAtOk;
    }

uint32 Tha6A290021ModulePrbsDisruptionMaxExpectedTimeGet(AtPrbsEngine self)
    {
    uint32 regBase =  cAtt_loReg_upen_tim_cfg_Base ;
    uint32 regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    return mRegField(regVal, cAtt_upen_tim_cfg_prbs_tim_cfg_);
    }

uint32 Tha6A290021ModulePrbsDisruptionCurTimeGet(AtPrbsEngine self, eBool r2c)
    {
    uint32 regVal, regAddr, regBase, unit, ret;
    regBase =  cAtt_loReg_upen_tim_sta_Base;
    regAddr = DisruptionAddressWithOffset(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);

    unit = HwToUnit(self, GetLossUnit(self));
    ret =  mRegField(regVal, cAtt_upen_tim_sta_tim_out_) * unit;

    if (r2c)
        {
        mRegFieldSet(regVal, cAtt_upen_tim_sta_tim_out_, 0);
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);
        }

    return ret;
    }

eAtRet Tha6A290021ModulePrbsDisruptionLossSigThresSet(AtPrbsEngine self, uint32 thres)
    {
    uint32 regVal, regAddr, regBase;
    regBase = cAtt_loReg_upen_loss_cfg_Base ;
    regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_vld_max_cfg_, thres);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

eAtRet Tha6A290021ModulePrbsDisruptionPrbsSyncDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    uint32 regVal, regAddr, regBase;
    regBase = cAtt_loReg_upen_loss_cfg_Base;
    regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_prbs_max_cfg_, thres);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

eAtRet Tha6A290021ModulePrbsDisruptionPrbsLossDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    uint32 regVal, regAddr, regBase;
    regBase = cAtt_loReg_upen_loss_cfg_Base;
    regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_loss_max_cfg_, thres);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }


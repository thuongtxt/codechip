/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A290021PrbsEngineHoVc.c
 *
 * Created Date: Sep 10, 2015
 *
 * Description : High-Order PRBS of 6A290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "AtSdhPath.h"
#include "AtCrossConnect.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../Tha60290021/sdh/Tha60290021XcHiding.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../Tha60290021/sdh/Tha60290021SdhLineSideAug.h"
#include "../../Tha6A000010/prbs/Tha6A000010PrbsEngineInternal.h"
#include "Tha6A290021PrbsEngineVc.h"
#include "Tha6A290021ModulePrbsLossTimerHiReg.h"
#include "Tha6A290021ModulePrbsLossTimerLoReg.h"
#include "../../Tha6A000010/prbs/Tha6A000010PrbsEngine.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6A000010PrbsEngineMethods  m_Tha6A000010PrbsEngineOverride;
static tAtPrbsEngineMethods           m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsLoStsPointerAdj(AtPrbsEngine self)
    {/*1d8000*/
    AtChannel channel = AtPrbsEngineChannelGet(self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)channel);
    if (channelType == cAtSdhChannelTypeTu3)
        return cAtTrue;
    else if (channelType == cAtSdhChannelTypeVc3)
        {
        uint8 mapType = AtSdhChannelMapTypeGet((AtSdhChannel)channel); /*(eAtSdhVcMapType)*/
        AtSdhChannel parent = AtSdhChannelParentChannelGet((AtSdhChannel)channel);
        if  (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3)
            return cAtTrue;
        else if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s||
                 mapType == cAtSdhVcMapTypeVc3MapDe3        ||
                 mapType == cAtSdhVcMapTypeVc1xMapC1x   ||
                 mapType == cAtSdhVcMapTypeVc1xMapDe1 )
            return cAtTrue;
        }
    else if (channelType == cAtSdhChannelTypeVc11||
             channelType == cAtSdhChannelTypeVc12)
        return cAtTrue;
    return cAtFalse;
    }

static AtPw PwCreate(Tha6A000010PrbsEngine self, uint32 pwId)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    return (AtPw)AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic);
    }

static AtChannel CircuitToBind(Tha6A000010PrbsEngine self, AtChannel channel)
    {
    AtUnused(self);

    /* Get PairVc (Faceplate VC from Terminated VC) */
    if (AtSdhChannelIsHoVc((AtSdhChannel) channel))
        return AtChannelSourceGet(channel);

    return channel;
    }

static eAtModulePrbsRet PohTu3DefaultSet(AtSdhChannel self)
    {
    uint8 vtgId, vtId;
    eAtModulePrbsRet ret = cAtOk;

    vtgId = AtSdhChannelTug2Get(self);
    vtId  = AtSdhChannelTu1xGet(self);
    ThaOcnVtPohInsertEnable(self, AtSdhChannelSts1Get(self), vtgId, vtId, cAtTrue);

    ret |= AtSdhPathTxPslSet((AtSdhPath)self, 0x2);
    ret |= AtSdhPathExpectedPslSet((AtSdhPath)self, 0x2);

    return ret;
    }

static eAtModulePrbsRet PohDefaultSet(Tha6A000010PrbsEngine self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)mMethodsGet(self)->CircuitToBind(self, AtPrbsEngineChannelGet((AtPrbsEngine)self));
    AtSdhChannel parent = AtSdhChannelParentChannelGet(sdhChannel);
    uint8 startSts, sts_i;
    eAtModulePrbsRet ret = cAtOk;

    startSts = AtSdhChannelSts1Get(sdhChannel);
    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhChannel); sts_i++)
        ret |= ThaOcnStsPohInsertEnable(sdhChannel, (uint8)(startSts + sts_i), cAtTrue);

    ret |= AtSdhPathTxPslSet((AtSdhPath)sdhChannel, 0x2);
    ret |= AtSdhPathExpectedPslSet((AtSdhPath)sdhChannel, 0x2);

    if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3)
        ret |= PohTu3DefaultSet(sdhChannel);

    return ret;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Enable(self, enable);

    if (ret != cAtOk)
        return ret;

    return enable ? PohDefaultSet((Tha6A000010PrbsEngine)self) : cAtOk;
    }

static eBool XcHidingShouldRedirect(AtPrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet(self);
    return (Tha60290021ModuleXcHideModeGet((AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc)) == cTha60290021XcHideModeDirect);
    }

static const char *ChannelDescription(AtPrbsEngine self)
    {
    if (XcHidingShouldRedirect(self))
        {
        AtChannel terVc = AtPrbsEngineChannelGet(self);

        /* Hi-oder: get terVc from faceplate vc */
        if (AtSdhChannelIsHoVc((AtSdhChannel) terVc))
            terVc = AtChannelSourceGet(terVc);

        return AtChannelIdDescriptionBuild(terVc);
        }

    return m_AtPrbsEngineMethods->ChannelDescription(self);
    }

static uint32 DisruptionAddressWithOffset(AtPrbsEngine self, uint32 address)
    {
    AtPw pw = Tha6A000010PrbsEnginePwGet(self);
    uint32 globalOffset = (IsLoStsPointerAdj(self)) ? Tha6A000010LoPrbsEngineOffset(self,pw) : Tha6A000010HoPrbsEngineOffset(self,pw);
    return (address + globalOffset);/*The pwId is included in globalOffset*/
    }

static uint32 DisruptionAddressWithOffsetExcludePwId(AtPrbsEngine self, uint32 address)
    {
	uint8 slice = 0;
	uint32 hwIdInSlice = 0;
    AtPw pw = Tha6A000010PrbsEnginePwGet(self);
    uint32 globalOffset = (IsLoStsPointerAdj(self)) ? Tha6A000010LoPrbsEngineOffset(self,pw) : Tha6A000010HoPrbsEngineOffset(self,pw);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, &hwIdInSlice) != cAtOk)
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");

    return (IsLoStsPointerAdj(self)) ? (address + globalOffset - AtChannelHwIdGet((AtChannel)pw)): (address + globalOffset-hwIdInSlice);
    }

static uint32 UnitToHw(AtPrbsEngine self)
    {
    return (IsLoStsPointerAdj(self)) ? 0x12EBB : 0x25F7F;
    }

static uint32 HwToUnit(AtPrbsEngine self, uint32 hwUnit)
    {
    AtUnused(self);
    if (hwUnit == 0x12EBB||hwUnit == 0x25F7F)
        return 1;
    return 1;
    }

static eAtRet SetLossUnit(AtPrbsEngine self, uint32 unit)
    {
    uint32 regVal, regAddr, regBase;

    regBase = (IsLoStsPointerAdj(self)) ? cAtt_loReg_upen_mls_cfg_Base : cAttReg_upen_mls_cfg_Base;
    regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);

    mRegFieldSet(regVal, cAtt_upen_mls_cfg_unit_tim_, unit);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static uint32 GetLossUnit(AtPrbsEngine self)
    {
    uint32 regVal, regAddr, regBase;
    regBase = (IsLoStsPointerAdj(self)) ? cAtt_loReg_upen_mls_cfg_Base : cAttReg_upen_mls_cfg_Base;
    regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    return mRegField(regVal, cAtt_upen_mls_cfg_unit_tim_);
    }

static eBool NeedSetDefaultMode(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool  NeedSetDefaultPrbsLoss(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 DisruptionTimeConfigAddress(AtPrbsEngine self)
    {
    return IsLoStsPointerAdj(self) ? cAtt_loReg_upen_tim_cfg_Base : cAttReg_upen_tim_cfg_Base;
    }

static uint32 LossConfigurationAddress(AtPrbsEngine self)
    {
    return IsLoStsPointerAdj(self) ? cAtt_loReg_upen_loss_cfg_Base : cAttReg_upen_loss_cfg_Base;
    }

static eAtRet SetDefaultLossConfiguration(AtPrbsEngine self, uint32 value)
    {
    uint32 regAddr = DisruptionAddressWithOffsetExcludePwId(self, LossConfigurationAddress(self));
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);

    mRegFieldSet(regVal, cAtt_upen_loss_cfg_loss_max_cfg_, value);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_vld_max_cfg_, value);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_prbs_max_cfg_, value);

    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet DisruptionMaxExpectedTimeSet(AtPrbsEngine self, uint32 time)
    {
    uint32 regAddr = DisruptionAddressWithOffsetExcludePwId(self, DisruptionTimeConfigAddress(self));
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    uint32 unit = UnitToHw(self);
    SetLossUnit(self, unit);

    mRegFieldSet(regVal, cAtt_upen_tim_cfg_prbs_tim_cfg_, time);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);

    SetDefaultLossConfiguration(self, 0x0A);
    return cAtOk;
    }

static uint32 DisruptionMaxExpectedTimeGet(AtPrbsEngine self)
    {
    uint32 regBase = (IsLoStsPointerAdj(self)) ? cAtt_loReg_upen_tim_cfg_Base : cAttReg_upen_tim_cfg_Base;
    uint32 regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    return mRegField(regVal, cAtt_upen_tim_cfg_prbs_tim_cfg_);
    }

static uint32 DisruptionCurTimeGet(AtPrbsEngine self, eBool r2c)
    {
    uint32 regVal, regAddr, regBase, unit, ret;
    regBase = (IsLoStsPointerAdj(self)) ? cReg_lo_upen_tim_sta : cReg_upen_tim_sta;
    regAddr = DisruptionAddressWithOffset(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);

    unit = HwToUnit(self, GetLossUnit(self));
    ret =  mRegField(regVal, c_upen_tim_sta_tim_loss_) * unit;

    if (r2c)
        AtPrbsEngineWrite(self, regAddr, 0, cAtModuleSdh);

    return ret;
    }

static eAtRet DisruptionLossSigThresSet(AtPrbsEngine self, uint32 thres)
    {
    uint32 regVal, regAddr, regBase;
    regBase = (IsLoStsPointerAdj(self)) ?  cAtt_loReg_upen_loss_cfg_Base : cAttReg_upen_loss_cfg_Base;
    regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_vld_max_cfg_, thres);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eAtRet DisruptionPrbsSyncDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    uint32 regVal, regAddr, regBase;
    regBase = (IsLoStsPointerAdj(self)) ? cAtt_loReg_upen_loss_cfg_Base : cAttReg_upen_loss_cfg_Base;
    regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_prbs_max_cfg_, thres);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eAtRet DisruptionPrbsLossDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    uint32 regVal, regAddr, regBase;
    regBase = (IsLoStsPointerAdj(self)) ? cAtt_loReg_upen_loss_cfg_Base : cAttReg_upen_loss_cfg_Base;
    regAddr = DisruptionAddressWithOffsetExcludePwId(self, regBase);
    regVal  = AtPrbsEngineRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAtt_upen_loss_cfg_loss_max_cfg_, thres);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, ChannelDescription);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultMode);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultPrbsLoss);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionMaxExpectedTimeSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionMaxExpectedTimeGet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionCurTimeGet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionLossSigThresSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionPrbsSyncDectectThresSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionPrbsLossDectectThresSet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021PrbsEngineVc);
    }

static void OverrideTha6A000010PrbsEngine(AtPrbsEngine self)
    {
    Tha6A000010PrbsEngine engine = (Tha6A000010PrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A000010PrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6A000010PrbsEngineOverride));

        mMethodOverride(m_Tha6A000010PrbsEngineOverride, PwCreate);
        mMethodOverride(m_Tha6A000010PrbsEngineOverride, CircuitToBind);
        }

    mMethodsSet(engine, &m_Tha6A000010PrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6A000010PrbsEngine(self);
    OverrideAtPrbsEngine(self);
    }

AtPrbsEngine Tha6A290021PrbsEngineVcObjectInit(AtPrbsEngine self, AtSdhChannel vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A000010PrbsEngineObjectInit(self, (AtChannel)vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A290021PrbsEngineVcNew(AtSdhChannel vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290021PrbsEngineVcObjectInit(newEngine, vc);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS Engine
 * 
 * File        : Tha6A290021PrbsEngineDe3Internal.h
 * 
 * Created Date: Dec 20, 2018
 *
 * Description : PRBS Engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021PRBSENGINEDE3INTERNAL_H_
#define _THA6A290021PRBSENGINEDE3INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A000010/prbs/Tha6A000010PrbsEngineInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021PrbsEngineDe3
    {
    tTha6A000010PrbsEngineDe3 super;
    }tTha6A290021PrbsEngineDe3;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6A290021PrbsEngineDe3ObjectInit(AtPrbsEngine self, AtPdhDe3 de3);
#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A290021_PRBS_THA6A290021PRBSENGINEDE3INTERNAL_H_ */


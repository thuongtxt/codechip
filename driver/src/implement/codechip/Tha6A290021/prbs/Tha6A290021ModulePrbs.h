/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A210031ModulePrbsInternal.h
 * 
 * Created Date: Dec 8, 2015
 *
 * Description : Module PRBS internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021MODULEPRBSREG_H_
#define _THA6A290021MODULEPRBSREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha6A290021ModulePrbsNew(AtDevice device);
AtPrbsEngine Tha6A290021LineDccHdlcPrbsEngineNew(AtHdlcChannel hdlc);
AtPrbsEngine Tha6A290021PrbsEngineDe3New(AtPdhDe3 de3);
AtPrbsEngine Tha6A290021PrbsEngineDe1New(AtPdhDe1 de1);
AtPrbsEngine Tha6A290021PrbsEngineVc1xNew(AtSdhChannel Vc1x);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021MODULEPRBSREG_H_ */


/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _ATT_REG_ATT_LOSS_TIMER_HI_H_
#define _ATT_REG_ATT_LOSS_TIMER_HI_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Timer Control Register
Reg Addr   : 0x685c0-0x685c0
Reg Formula: Address + $slice*2048
    Where  : 
Reg Desc   : 
this register is used to config Time Configuration

------------------------------------------------------------------------------*/
#define cAttReg_upen_tim_cfg_Base                                                                      0x685c0
#define cAttReg_upen_tim_cfg                                                                         00x685c0UL
#define cAttReg_upen_tim_cfg_WidthVal                                                                       32
#define cAttReg_upen_tim_cfg_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: prbs_tim_cfg
BitField Type: RW
BitField Desc: The Max PRBS Timer Number Configuration
BitField Bits: [15:0]
--------------------------------------*/
#define cAtt_upen_tim_cfg_prbs_tim_cfg_Bit_Start                                                             0
#define cAtt_upen_tim_cfg_prbs_tim_cfg_Bit_End                                                              15
#define cAtt_upen_tim_cfg_prbs_tim_cfg_Mask                                                           cBit15_0
#define cAtt_upen_tim_cfg_prbs_tim_cfg_Shift                                                                 0
#define cAtt_upen_tim_cfg_prbs_tim_cfg_MaxVal                                                           0xffff
#define cAtt_upen_tim_cfg_prbs_tim_cfg_MinVal                                                              0x0
#define cAtt_upen_tim_cfg_prbs_tim_cfg_RstVal                                                              0x3


/*------------------------------------------------------------------------------
Reg Name   : PRBS Monitor Control Register
Reg Addr   : 0x685c1-0x685c1
Reg Formula: Address + $slice*2048
    Where  : {$slice (0-3):slice}
Reg Desc   : 
this register is used to config Time Configuration

------------------------------------------------------------------------------*/
#define cAttReg_upen_loss_cfg_Base                                                                     0x686c1
#define cAttReg_upen_loss_cfg                                                                        0x686c1UL
#define cAttReg_upen_loss_cfg_WidthVal                                                                      32
#define cAttReg_upen_loss_cfg_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: loss_max_cfg
BitField Type: RW
BitField Desc: The Max Loss number Configuration use to set Loss
BitField Bits: [23:16]
--------------------------------------*/
#define cAtt_upen_loss_cfg_loss_max_cfg_Bit_Start                                                           16
#define cAtt_upen_loss_cfg_loss_max_cfg_Bit_End                                                             23
#define cAtt_upen_loss_cfg_loss_max_cfg_Mask                                                         cBit23_16
#define cAtt_upen_loss_cfg_loss_max_cfg_Shift                                                               16
#define cAtt_upen_loss_cfg_loss_max_cfg_MaxVal                                                            0xff
#define cAtt_upen_loss_cfg_loss_max_cfg_MinVal                                                             0x0
#define cAtt_upen_loss_cfg_loss_max_cfg_RstVal                                                            0x10

/*--------------------------------------
BitField Name: vld_max_cfg
BitField Type: RW
BitField Desc: The Max Valid Number Configuration use to clear Loss or PRBS
error
BitField Bits: [15:8]
--------------------------------------*/
#define cAtt_upen_loss_cfg_vld_max_cfg_Bit_Start                                                             8
#define cAtt_upen_loss_cfg_vld_max_cfg_Bit_End                                                              15
#define cAtt_upen_loss_cfg_vld_max_cfg_Mask                                                           cBit15_8
#define cAtt_upen_loss_cfg_vld_max_cfg_Shift                                                                 8
#define cAtt_upen_loss_cfg_vld_max_cfg_MaxVal                                                             0xff
#define cAtt_upen_loss_cfg_vld_max_cfg_MinVal                                                              0x0
#define cAtt_upen_loss_cfg_vld_max_cfg_RstVal                                                              0x3

/*--------------------------------------
BitField Name: prbs_max_cfg
BitField Type: RW
BitField Desc: The Max PRBS Number Configuration use to set Prbs error
BitField Bits: [7:0]
--------------------------------------*/
#define cAtt_upen_loss_cfg_prbs_max_cfg_Bit_Start                                                            0
#define cAtt_upen_loss_cfg_prbs_max_cfg_Bit_End                                                              7
#define cAtt_upen_loss_cfg_prbs_max_cfg_Mask                                                           cBit7_0
#define cAtt_upen_loss_cfg_prbs_max_cfg_Shift                                                                0
#define cAtt_upen_loss_cfg_prbs_max_cfg_MaxVal                                                            0xff
#define cAtt_upen_loss_cfg_prbs_max_cfg_MinVal                                                             0x0
#define cAtt_upen_loss_cfg_prbs_max_cfg_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : The unit time  Configuration
Reg Addr   : 0x685c2-0x685c2
Reg Formula: 
    Where  : 
Reg Desc   : 
this register is used to config the unit timer

------------------------------------------------------------------------------*/
#define cAttReg_upen_mls_cfg_Base                                                                      0x685c2
#define cAttReg_upen_mls_cfg                                                                         0x685c2UL
#define cAttReg_upen_mls_cfg_WidthVal                                                                       32
#define cAttReg_upen_mls_cfg_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: unit_tim
BitField Type: RW
BitField Desc: The unit timer
BitField Bits: [31:0]
--------------------------------------*/
#define cAtt_upen_mls_cfg_unit_tim_Bit_Start                                                                 0
#define cAtt_upen_mls_cfg_unit_tim_Bit_End                                                                  31
#define cAtt_upen_mls_cfg_unit_tim_Mask                                                               cBit31_0
#define cAtt_upen_mls_cfg_unit_tim_Shift                                                                     0
#define cAtt_upen_mls_cfg_unit_tim_MaxVal                                                           0xffffffff
#define cAtt_upen_mls_cfg_unit_tim_MinVal                                                                  0x0
#define cAtt_upen_mls_cfg_unit_tim_RstVal                                                              0x12EBB


/*------------------------------------------------------------------------------
Reg Name   : PRBS Loss RAM Status
Reg Addr   : 0x68500-0x6853f
Reg Formula: Address + $slice*2048 + $pwid
    Where  : 
           + $pwid(0-47):pseudowire id
           + $slice (0-3):slice
Reg Desc   : 
this register is used to display  PRBS Loss RAM Status

------------------------------------------------------------------------------*/
#define cAttReg_upen_ram_sta_Base                                                                      0x68500
#define cAttReg_upen_ram_sta(pwid, slice)\
                                                                              (00x68500UL+(slice)*128UL+(pwid))
#define cAttReg_upen_ram_sta_WidthVal                                                                       32
#define cAttReg_upen_ram_sta_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: los_enb
BitField Type: RW
BitField Desc: Loss status
BitField Bits: [26]
--------------------------------------*/
#define cAtt_upen_ram_sta_los_enb_Bit_Start                                                                 26
#define cAtt_upen_ram_sta_los_enb_Bit_End                                                                   26
#define cAtt_upen_ram_sta_los_enb_Mask                                                                  cBit26
#define cAtt_upen_ram_sta_los_enb_Shift                                                                     26
#define cAtt_upen_ram_sta_los_enb_MaxVal                                                                   0x1
#define cAtt_upen_ram_sta_los_enb_MinVal                                                                   0x0
#define cAtt_upen_ram_sta_los_enb_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: prbsenb
BitField Type: RW
BitField Desc: PRBS error status
BitField Bits: [25]
--------------------------------------*/
#define cAtt_upen_ram_sta_prbsenb_Bit_Start                                                                 25
#define cAtt_upen_ram_sta_prbsenb_Bit_End                                                                   25
#define cAtt_upen_ram_sta_prbsenb_Mask                                                                  cBit25
#define cAtt_upen_ram_sta_prbsenb_Shift                                                                     25
#define cAtt_upen_ram_sta_prbsenb_MaxVal                                                                   0x1
#define cAtt_upen_ram_sta_prbsenb_MinVal                                                                   0x0
#define cAtt_upen_ram_sta_prbsenb_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: vld_cnt
BitField Type: RW
BitField Desc: the valid prbs counter
BitField Bits: [24:23]
--------------------------------------*/
#define cAtt_upen_ram_sta_vld_cnt_Bit_Start                                                                 23
#define cAtt_upen_ram_sta_vld_cnt_Bit_End                                                                   24
#define cAtt_upen_ram_sta_vld_cnt_Mask                                                               cBit24_23
#define cAtt_upen_ram_sta_vld_cnt_Shift                                                                     23
#define cAtt_upen_ram_sta_vld_cnt_MaxVal                                                                   0x3
#define cAtt_upen_ram_sta_vld_cnt_MinVal                                                                   0x0
#define cAtt_upen_ram_sta_vld_cnt_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: los_cnt
BitField Type: RW
BitField Desc: Loss or prbs error counter
BitField Bits: [16:8]
--------------------------------------*/
#define cAtt_upen_ram_sta_los_cnt_Bit_Start                                                                  8
#define cAtt_upen_ram_sta_los_cnt_Bit_End                                                                   16
#define cAtt_upen_ram_sta_los_cnt_Mask                                                                cBit16_8
#define cAtt_upen_ram_sta_los_cnt_Shift                                                                      8
#define cAtt_upen_ram_sta_los_cnt_MaxVal                                                                 0x1ff
#define cAtt_upen_ram_sta_los_cnt_MinVal                                                                   0x0
#define cAtt_upen_ram_sta_los_cnt_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: prbscnt
BitField Type: RW
BitField Desc: The  prbs error counter
BitField Bits: [7:0]
--------------------------------------*/
#define cAtt_upen_ram_sta_prbscnt_Bit_Start                                                                  0
#define cAtt_upen_ram_sta_prbscnt_Bit_End                                                                    7
#define cAtt_upen_ram_sta_prbscnt_Mask                                                                 cBit7_0
#define cAtt_upen_ram_sta_prbscnt_Shift                                                                      0
#define cAtt_upen_ram_sta_prbscnt_MaxVal                                                                  0xff
#define cAtt_upen_ram_sta_prbscnt_MinVal                                                                   0x0
#define cAtt_upen_ram_sta_prbscnt_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : PRBS Loss Timer Status
Reg Addr   : 0x68540-6857F
Reg Formula: Address + $slice*2048 + $pwid
    Where  :
           + $pwid(0-63):pseudowire id
           + $slice (0-1):slice
Reg Desc   :
this register is used to display  PRBS Loss RAM Status

------------------------------------------------------------------------------*/
#define cAttReg_upen_tim_sta_Base                                                                      0x68540
#define cAttReg_upen_tim_sta(pwid, slice)\
                                                                              (0x68540UL+(slice)*2048UL+(pwid))
#define cAttReg_upen_tim_sta_WidthVal                                                                       32
#define cAttReg_upen_tim_sta_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: pos_tim
BitField Type: RW
BitField Desc: Posedge of the unit time
BitField Bits: [18]
--------------------------------------*/
#define cAtt_upen_tim_sta_pos_tim_Bit_Start                                                                 18
#define cAtt_upen_tim_sta_pos_tim_Bit_End                                                                   18
#define cAtt_upen_tim_sta_pos_tim_Mask                                                                  cBit18
#define cAtt_upen_tim_sta_pos_tim_Shift                                                                     18
#define cAtt_upen_tim_sta_pos_tim_MaxVal                                                                   0x1
#define cAtt_upen_tim_sta_pos_tim_MinVal                                                                   0x0
#define cAtt_upen_tim_sta_pos_tim_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: prbslos
BitField Type: RW
BitField Desc: Loss or prbs error Enable
BitField Bits: [17]
--------------------------------------*/
#define cAtt_upen_tim_sta_prbslos_Bit_Start                                                                 17
#define cAtt_upen_tim_sta_prbslos_Bit_End                                                                   17
#define cAtt_upen_tim_sta_prbslos_Mask                                                                  cBit17
#define cAtt_upen_tim_sta_prbslos_Shift                                                                     17
#define cAtt_upen_tim_sta_prbslos_MaxVal                                                                   0x1
#define cAtt_upen_tim_sta_prbslos_MinVal                                                                   0x0
#define cAtt_upen_tim_sta_prbslos_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: max_tim
BitField Type: RW
BitField Desc: Max time Enable
BitField Bits: [16]
--------------------------------------*/
#define cAtt_upen_tim_sta_max_tim_Bit_Start                                                                 16
#define cAtt_upen_tim_sta_max_tim_Bit_End                                                                   16
#define cAtt_upen_tim_sta_max_tim_Mask                                                                  cBit16
#define cAtt_upen_tim_sta_max_tim_Shift                                                                     16
#define cAtt_upen_tim_sta_max_tim_MaxVal                                                                   0x1
#define cAtt_upen_tim_sta_max_tim_MinVal                                                                   0x0
#define cAtt_upen_tim_sta_max_tim_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: tim_out
BitField Type: RW
BitField Desc: The time out status
BitField Bits: [15:0]
--------------------------------------*/
#define cAtt_upen_tim_sta_tim_out_Bit_Start                                                                  0
#define cAtt_upen_tim_sta_tim_out_Bit_End                                                                   15
#define cAtt_upen_tim_sta_tim_out_Mask                                                                cBit15_0
#define cAtt_upen_tim_sta_tim_out_Shift                                                                      0
#define cAtt_upen_tim_sta_tim_out_MaxVal                                                                0xffff
#define cAtt_upen_tim_sta_tim_out_MinVal                                                                   0x0
#define cAtt_upen_tim_sta_tim_out_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : PRBS Loss Timer Status
Reg Addr   : 0x68540-6857F
Reg Formula: 0x68540 + $slice*2048 + $pwid
    Where  :
           + $pwid(0-47):pseudowire id
           + $slice (0-3):slice
Reg Desc   :
this register is used to display  PRBS Loss RAM Status

------------------------------------------------------------------------------*/
#define cReg_upen_tim_sta                                                                              0x68540

/*--------------------------------------
BitField Name: tim_loss
BitField Type: RW
BitField Desc: The loss timer
BitField Bits: [31:16]
--------------------------------------*/
#define c_upen_tim_sta_tim_loss_Mask                                                                 cBit31_16
#define c_upen_tim_sta_tim_loss_Shift                                                                       16

/*--------------------------------------
BitField Name: tim_err
BitField Type: RW
BitField Desc: The prbs timer
BitField Bits: [15:0]
--------------------------------------*/
#define c_upen_tim_sta_tim_err_Mask                                                                   cBit15_0
#define c_upen_tim_sta_tim_err_Shift                                                                         0


#endif /* __REG_ATTOTN_LOMUX_RD_new_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A290021ModulePrbsInternal.h
 * 
 * Created Date: Nov 14, 2017
 *
 * Description : Module PRBS internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021MODULEPRBSREGINTERNAL_H_
#define _THA6A290021MODULEPRBSREGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A000010/prbs/Tha6A000010ModulePrbsInternal.h"
#include "Tha6A290021ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021ModulePrbs
    {
    tTha6A000010ModulePrbs super;
    }tTha6A290021ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha6A290021ModulePrbsObjectInit(AtModulePrbs self, AtDevice device);
eBool Tha6A290021ModulePrbsNeedSetDefaultMode(AtPrbsEngine self);
eBool  Tha6A290021ModulePrbsNeedSetDefaultPrbsLoss(AtPrbsEngine self);
eAtRet Tha6A290021ModulePrbsDisruptionMaxExpectedTimeSet(AtPrbsEngine self, uint32 time);
uint32 Tha6A290021ModulePrbsDisruptionMaxExpectedTimeGet(AtPrbsEngine self);
uint32 Tha6A290021ModulePrbsDisruptionCurTimeGet(AtPrbsEngine self, eBool r2c);
eAtRet Tha6A290021ModulePrbsDisruptionLossSigThresSet(AtPrbsEngine self, uint32 thres);
eAtRet Tha6A290021ModulePrbsDisruptionPrbsSyncDectectThresSet(AtPrbsEngine self, uint32 thres);
eAtRet Tha6A290021ModulePrbsDisruptionPrbsLossDectectThresSet(AtPrbsEngine self, uint32 thres);


#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021MODULEPRBSREGINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60290021HdlcChannel.c
 *
 * Created Date: Sep 3, 2016
 *
 * Description : HDLC channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/prbs/AtModulePrbsInternal.h"
#include "../../Tha60290021/encap/Tha60290021HdlcChannelInternal.h"
#include "Tha6A290021HdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha6A290021HdlcChannel *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021HdlcChannel
    {
    tTha60290021HdlcChannel super;
    AtPrbsEngine prbsEngine;
    }tTha6A290021HdlcChannel;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;


/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPrbsEngine PrbsEngineCreate(AtChannel self)
    {
    AtModulePrbs prbsModule = (AtModulePrbs)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePrbs);
    return AtModulePrbsLineDccHdlcPrbsEngineCreate(prbsModule, AtChannelIdGet(self), (AtHdlcChannel)self);
    }

static AtPrbsEngine PrbsEngineGet(AtChannel self)
    {
    if (mThis(self)->prbsEngine == NULL)
        mThis(self)->prbsEngine = PrbsEngineCreate(self);
    return mThis(self)->prbsEngine;
    }

static eBool PrbsEngineIsCreated(AtChannel self)
    {
    if (mThis(self)->prbsEngine)
        return cAtTrue;

    return cAtFalse;
    }

static void Delete(AtObject self)
    {
	if (mThis(self)->prbsEngine != NULL)
		AtObjectDelete((AtObject)mThis(self)->prbsEngine);

	mThis(self)->prbsEngine = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtObject self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, PrbsEngineGet);
        mMethodOverride(m_AtChannelOverride, PrbsEngineIsCreated);
        }

    mMethodsSet(self, &m_AtChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtChannel((AtChannel)self);
    OverrideAtObject((AtObject)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021HdlcChannel);
    }

static AtHdlcChannel ObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021HdlcChannelObjectInit((AtHdlcChannel) self, channelId,line, layers, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->prbsEngine = NULL;

    return self;
    }

AtHdlcChannel Tha6A290021HdlcChannelNew(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChannel, channelId, line, layers, module);
    }

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date: August 16, 2018                                                 
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_MRO10G_ATT_ADJPTR_MERGEV2_H_
#define _AF6_REG_AF6CNC0021_MRO10G_ATT_ADJPTR_MERGEV2_H_

/*--------------------------- Define -----------------------------------------*/



/*------------------------------------------------------------------------------
Reg Name   : OCN Pointer Adjustment Enable Forcing Registers For Hi-order CEP
Reg Addr   : 0x287fe - 0x29ffe
Reg Formula: 0x287fe + SliceId*2048
    Where  : 
           + $SliceId(0-3)
Reg Desc   : 
This is the configuration register for enable OCN pointer adjustment forcing for Hi-order from CEP side

------------------------------------------------------------------------------*/
#define cAf6Reg_enhocepadj                                                                             0x287fe

/*--------------------------------------
BitField Name: cfg_enablefrcho
BitField Type: RW
BitField Desc: Config enable forcing Hi-order 0: Enable forcing 1: Disable
forcing
BitField Bits: [0]
--------------------------------------*/
#define cAf6_enhocepadj_cfg_enablefrcho_Mask                                                             cBit0
#define cAf6_enhocepadj_cfg_enablefrcho_Shift                                                                0

/*------------------------------------------------------------------------------
Reg Name   : OCN Pointer Adjustment Forcing Registers For Hi-order CEP
Reg Addr   : 0x28400 - 0x2bc2f
Reg Formula: 0x28400 + SliceId*2048 + StsId*1
    Where  : 
           + $SliceId(0-3)
           + $StsId(0-47)
Reg Desc   : 
This is the configuration register for the OCN pointer adjustment forcing for Hi-order from CEP side

------------------------------------------------------------------------------*/
#define cAf6Reg_hocepadjV2                                                                               0x28400

/*--------------------------------------
BitField Name: rditype
BitField Type: RW
BitField Desc: Forcing type 3: Increment adjustment 4: Decrement adjustment
others: none forcing
BitField Bits: [62:60]
--------------------------------------*/
#define cAf6_hocepadj_rditype_MaskV2                                                                   cBit30_28
#define cAf6_hocepadj_rditype_ShiftV2                                                                         28

/*--------------------------------------
BitField Name: cfgtimemod
BitField Type: RW
BitField Desc: Config timer mode
BitField Bits: [59:58]
--------------------------------------*/
#define cAf6_hocepadj_cfgtimemod_MaskV2                                                                cBit27_26
#define cAf6_hocepadj_cfgtimemod_ShiftV2                                                                      26

/*--------------------------------------
BitField Name: cfgclralrm
BitField Type: RW
BitField Desc: Config timer to clear alarm
BitField Bits: [57:50]
--------------------------------------*/
#define cAf6_hocepadj_cfgclralrm_MaskV2                                                                cBit25_18
#define cAf6_hocepadj_cfgclralrm_ShiftV2                                                                      18

/*--------------------------------------
BitField Name: cfgsetalrm
BitField Type: RW
BitField Desc: Config timer to set alarm
BitField Bits: [49:42]
--------------------------------------*/
#define cAf6_hocepadj_cfgsetalrm_MaskV2                                                                cBit17_10
#define cAf6_hocepadj_cfgsetalrm_ShiftV2                                                                      10

/*--------------------------------------
BitField Name: cfgerrstep
BitField Type: RW
BitField Desc: Config error step - the gap between 2 errors
BitField Bits: [41:34]
--------------------------------------*/
#define cAf6_hocepadj_cfgerrstep_MaskV2                                                                  cBit9_2
#define cAf6_hocepadj_cfgerrstep_ShiftV2                                                                       2

/*--------------------------------------
BitField Name: cfgmsk_pos
BitField Type: RW
BitField Desc: Config Mask for 16 positions
BitField Bits: [33:18]
--------------------------------------*/
#define cAf6_hocepadj_cfgmsk_pos_Mask_01V2                                                             cBit31_18
#define cAf6_hocepadj_cfgmsk_pos_Shift_01V2                                                                   18
#define cAf6_hocepadj_cfgmsk_pos_Mask_02V2                                                               cBit1_0
#define cAf6_hocepadj_cfgmsk_pos_Shift_02V2                                                                    0

/*--------------------------------------
BitField Name: cfgfrc_ena
BitField Type: RW
BitField Desc: Config force enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_hocepadj_cfgfrc_ena_Mask                                                                   cBit17
#define cAf6_hocepadj_cfgfrc_ena_Shift                                                                      17

/*--------------------------------------
BitField Name: cfgerr_num
BitField Type: RW
BitField Desc: Config number of error for error insertion
BitField Bits: [16:1]
--------------------------------------*/
#define cAf6_hocepadj_cfgerr_num_Mask                                                                 cBit16_1
#define cAf6_hocepadj_cfgerr_num_Shift                                                                       1

/*--------------------------------------
BitField Name: cfgfrcmode
BitField Type: RW
BitField Desc: Config force error mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_hocepadj_cfgfrcmode_MaskV2                                                                    cBit0
#define cAf6_hocepadj_cfgfrcmode_ShiftV2                                                                       0

/*------------------------------------------------------------------------------
Reg Name   : OCN Pointer Adjustment Enable Forcing Registers For Lo-order Multiplexing From PDH side
Reg Addr   : 0xd8fff - 0xdbfff
Reg Formula: 0xd8fff + SliceId*4096
    Where  : 
           + $SliceId(0-3)
Reg Desc   : 
This is the configuration register for enable Forcing Lo-order Multiplexing From PDH side

------------------------------------------------------------------------------*/
#define cAf6Reg_enstsloadj                                                                             0xd8fff

/*--------------------------------------
BitField Name: cfg_enablefrclo
BitField Type: RW
BitField Desc: Config enable forcing Lo-order 0: Enable forcing 1: Disable
forcing
BitField Bits: [0]
--------------------------------------*/
#define cAf6_enstsloadj_cfg_enablefrclo_Mask                                                             cBit0
#define cAf6_enstsloadj_cfg_enablefrclo_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : OCN Registers TxPP Configure per STS For Hi-order Multiplexing from PDH side
Reg Addr   : 0xd0800 - 0xd382f
Reg Formula: 0xd0800 + SliceId*4096 + StsId*1
    Where  : 
           + $SliceId(0-3)
           + $StsId(0-47)
Reg Desc   : 
This is the configuration register for config TxPP per STS for Hi-order multiplexing

------------------------------------------------------------------------------*/
#define cAf6Reg_lopgramctlV2                                                                             0xd0800

/*--------------------------------------
BitField Name: TerStsPgStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master 1: Slaver 0:
Master
BitField Bits: [8]
--------------------------------------*/
#define cAf6_lopgramctl_TerStsPgStsSlvInd_Mask                                                           cBit8
#define cAf6_lopgramctl_TerStsPgStsSlvInd_Shift                                                              8

/*--------------------------------------
BitField Name: TerStsPgStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_lopgramctl_TerStsPgStsMstId_Mask                                                          cBit5_0
#define cAf6_lopgramctl_TerStsPgStsMstId_Shift                                                               0

/*------------------------------------------------------------------------------
Reg Name   : OCN Pointer Adjustment Forcing Registers For Hi-order Multiplexing From PDH side
Reg Addr   : 0xd0000 - 0xd302f
Reg Formula: 0xd0000 + SliceId*4096 + StsId*1
    Where  : 
           + $SliceId(0-3)
           + $StsId(0-47)
Reg Desc   : 
This is the configuration register for the OCN pointer adjustment forcing for Hi-order multiplexing from PDH side

------------------------------------------------------------------------------*/
#define cAf6Reg_stshoadjV2                                                                               0xd0000

/*--------------------------------------
BitField Name: rditype
BitField Type: RW
BitField Desc: Forcing type 3: Increment adjustment 4: Decrement adjustment
others: none forcing
BitField Bits: [62:60]
--------------------------------------*/
#define cAf6_stshoadj_rditype_MaskV2                                                                   cBit30_28
#define cAf6_stshoadj_rditype_ShiftV2                                                                         28

/*--------------------------------------
BitField Name: cfgtimemod
BitField Type: RW
BitField Desc: Config timer mode
BitField Bits: [59:58]
--------------------------------------*/
#define cAf6_stshoadj_cfgtimemod_MaskV2                                                                cBit27_26
#define cAf6_stshoadj_cfgtimemod_ShiftV2                                                                      26

/*--------------------------------------
BitField Name: cfgclralrm
BitField Type: RW
BitField Desc: Config timer to clear alarm
BitField Bits: [57:50]
--------------------------------------*/
#define cAf6_stshoadj_cfgclralrm_Mask                                                                cBit25_18
#define cAf6_stshoadj_cfgclralrm_Shift                                                                      18

/*--------------------------------------
BitField Name: cfgsetalrm
BitField Type: RW
BitField Desc: Config timer to set alarm
BitField Bits: [49:42]
--------------------------------------*/
#define cAf6_stshoadj_cfgsetalrm_MaskV2                                                                cBit17_10
#define cAf6_stshoadj_cfgsetalrm_ShiftV2                                                                      10

/*--------------------------------------
BitField Name: cfgerrstep
BitField Type: RW
BitField Desc: Config error step - the gap between 2 errors
BitField Bits: [41:34]
--------------------------------------*/
#define cAf6_stshoadj_cfgerrstep_MaskV2                                                                  cBit9_2
#define cAf6_stshoadj_cfgerrstep_ShiftV2                                                                       2

/*--------------------------------------
BitField Name: cfgmsk_pos
BitField Type: RW
BitField Desc: Config Mask for 16 positions
BitField Bits: [33:18]
--------------------------------------*/
#define cAf6_stshoadj_cfgmsk_pos_Mask_01V2                                                             cBit31_18
#define cAf6_stshoadj_cfgmsk_pos_Shift_01V2                                                                   18
#define cAf6_stshoadj_cfgmsk_pos_Mask_02V2                                                               cBit1_0
#define cAf6_stshoadj_cfgmsk_pos_Shift_02V2                                                                    0

/*--------------------------------------
BitField Name: cfgfrc_ena
BitField Type: RW
BitField Desc: Config force enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_stshoadj_cfgfrc_ena_MaskV2                                                                   cBit17
#define cAf6_stshoadj_cfgfrc_ena_ShiftV2                                                                      17

/*--------------------------------------
BitField Name: cfgerr_num
BitField Type: RW
BitField Desc: Config number of error for error insertion
BitField Bits: [16:1]
--------------------------------------*/
#define cAf6_stshoadj_cfgerr_num_MaskV2                                                                 cBit16_1
#define cAf6_stshoadj_cfgerr_num_ShiftV2                                                                       1

/*--------------------------------------
BitField Name: cfgfrcmode
BitField Type: RW
BitField Desc: Config force error mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_stshoadj_cfgfrcmode_MaskV2                                                                    cBit0
#define cAf6_stshoadj_cfgfrcmode_ShiftV2                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OCN Pointer Adjustment Forcing Registers For Lo-order Multiplexing From PDH side
Reg Addr   : 0xd8000 - 0xdb5fb
Reg Formula: 0xd8000 + SliceId*4096 + StsId*32 + VtId*1
    Where  : 
           + $SliceId(0-3)
           + $StsId(0-47)
           + $VtId(0-27)
Reg Desc   : 
This is the configuration register for the OCN pointer adjustment forcing for Lo-order multiplexing from PDH side

------------------------------------------------------------------------------*/
#define cAf6Reg_stsloadjV2                                                                               0xd8000

/*--------------------------------------
BitField Name: rditype
BitField Type: RW
BitField Desc: Forcing type 3: Increment adjustment 4: Decrement adjustment
others: none forcing
BitField Bits: [62:60]
--------------------------------------*/
#define cAf6_stsloadj_rditype_MaskV2                                                                   cBit30_28
#define cAf6_stsloadj_rditype_ShiftV2                                                                         28

/*--------------------------------------
BitField Name: cfgtimemod
BitField Type: RW
BitField Desc: Config timer mode
BitField Bits: [59:58]
--------------------------------------*/
#define cAf6_stsloadj_cfgtimemod_MaskV2                                                                cBit27_26
#define cAf6_stsloadj_cfgtimemod_ShiftV2                                                                      26

/*--------------------------------------
BitField Name: cfgclralrm
BitField Type: RW
BitField Desc: Config timer to clear alarm
BitField Bits: [57:50]
--------------------------------------*/
#define cAf6_stsloadj_cfgclralrm_MaskV2                                                                cBit25_18
#define cAf6_stsloadj_cfgclralrm_ShiftV2                                                                      18

/*--------------------------------------
BitField Name: cfgsetalrm
BitField Type: RW
BitField Desc: Config timer to set alarm
BitField Bits: [49:42]
--------------------------------------*/
#define cAf6_stsloadj_cfgsetalrm_Mask                                                                cBit17_10
#define cAf6_stsloadj_cfgsetalrm_Shift                                                                      10

/*--------------------------------------
BitField Name: cfgerrstep
BitField Type: RW
BitField Desc: Config error step - the gap between 2 errors
BitField Bits: [41:34]
--------------------------------------*/
#define cAf6_stsloadj_cfgerrstep_MaskV2                                                                  cBit9_2
#define cAf6_stsloadj_cfgerrstep_ShiftV2                                                                       2

/*--------------------------------------
BitField Name: cfgmsk_pos
BitField Type: RW
BitField Desc: Config Mask for 16 positions
BitField Bits: [33:18]
--------------------------------------*/
#define cAf6_stsloadj_cfgmsk_pos_Mask_01V2                                                             cBit31_18
#define cAf6_stsloadj_cfgmsk_pos_Shift_01V2                                                                   18
#define cAf6_stsloadj_cfgmsk_pos_Mask_02V2                                                               cBit1_0
#define cAf6_stsloadj_cfgmsk_pos_Shift_02V2                                                                    0

/*--------------------------------------
BitField Name: cfgfrc_ena
BitField Type: RW
BitField Desc: Config force enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_stsloadj_cfgfrc_ena_MaskV2                                                                   cBit17
#define cAf6_stsloadj_cfgfrc_ena_ShiftV2                                                                      17

/*--------------------------------------
BitField Name: cfgerr_num
BitField Type: RW
BitField Desc: Config number of error for error insertion
BitField Bits: [16:1]
--------------------------------------*/
#define cAf6_stsloadj_cfgerr_num_MaskV2                                                                 cBit16_1
#define cAf6_stsloadj_cfgerr_num_ShiftV2                                                                       1

/*--------------------------------------
BitField Name: cfgfrcmode
BitField Type: RW
BitField Desc: Config force error mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_stsloadj_cfgfrcmode_MaskV2                                                                    cBit0
#define cAf6_stsloadj_cfgfrcmode_ShiftV2                                                                       0

#endif /* _AF6_REG_AF6CNC0021_MRO10G_ATT_ADJPTR_MERGEV2_H_ */

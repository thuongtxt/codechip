/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : Tha6A290021SdhVc1xAttController.c
 *
 * Created Date: Jul 19, 2016
 *
 * Description : VC1x ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../../default/att/ThaAttSdhManagerInternal.h"
#include "../../Tha6A210031/sdh/Tha6A210031SdhAttControllerInternal.h"
#include "Tha6A290021SdhAttControllerInternal.h"
#include "Tha6A290021SdhAttReg.h"
#include "Tha6A290021SdhAttPointerAdjReg.h"
#include "Tha6A290021SdhAttPointerAdjRegV3.h"
/*--------------------------- Define -----------------------------------------*/
#define cAtt6A290021LoPointerForcingStartAddress 0xd8000
#define cAtt6A290021LoPointerForcingEndAddress   0xdb5fb
#define increase 3
#define decrease 4
#define cAtt6A290021PointerForcingDurLoMask  cBit31_10
#define cAtt6A290021PointerForcingDurLoShift 10
#define cAtt6A290021PointerForcingDurHiMask  cBit4_0
#define cAtt6A290021PointerForcingDurHiShift 0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290021SdhVc1xAttController*)self)
#define mIsForcingAisUneqOptimize(self, alarmType) \
    Tha6A210031PdhDe3AttControllerIsAisUneqOptimizeSupported((Tha6A210031PdhDe3AttController)self, alarmType)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttControllerMethods                m_AtAttControllerOverride;
static tThaAttControllerMethods                m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;

/* To save super implementation */
static const tAtAttControllerMethods       *m_AtAttControllerMethods       = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static eAtRet HwForcePointerG783(AtAttController self, uint32 pointerType, eBool force);

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportDataMask8bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtSdhPathCounterTypeBip)
        return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0556);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportDataMask2bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtSdhPathCounterTypeBip)
        return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0xFFFF);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportConvertStep(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0557);
    }

static uint32 StartVersionSupportPointerAdj(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0546);
    }

static uint8  SwPointerAdjToHw(Tha6A210031PdhDe3AttController self, uint32 pointerAdjType)
	{
	AtUnused(self);
	if (pointerAdjType==cAtSdhPathPointerAdjTypeIncrease)
		return 3;
	return 4;
	}

static uint32 PointerAdjTypeMask(Tha6A210031PdhDe3AttController self,
		uint32 isError, uint32 errorType) {
	AtUnused(self);
	AtUnused(isError);
	AtUnused(errorType);
	return cAf6_stsloadj_rditype_Mask;
}

static uint32 RegPointerAdj(Tha6A210031PdhDe3AttController self)
	{
	AtUnused(self);
	return cAf6Reg_stsloadj;
	}

static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0546);
    }

static uint32 RegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
            if (mIsForcingAisUneqOptimize(self, alarmType))
                return cReg_upen_LOPvtcfg;
            return cReg_upen_AISvtcfg;
        case cAtSdhPathAlarmLop:
            return cReg_upen_LOPvtcfg;
        case cAtSdhPathAlarmUneq:
            if (mIsForcingAisUneqOptimize(self, alarmType))
                return cReg_upen_LOPvtcfg;
            return cReg_upen_UEQvtcfg;
        case cAtSdhPathAlarmRfi:
            return cAttReg_upen_RFIvtcfg_Base;
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmErdiC:
            return cReg_upen_RDIvtcfg;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cBit31_0;
    }

static uint8 AlarmTypeIndex(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
            if (mIsForcingAisUneqOptimize(self, alarmType))
                return 1;
            return 0;
        case cAtSdhPathAlarmLop:
            return 1;
        case cAtSdhPathAlarmUneq:
            if (mIsForcingAisUneqOptimize(self, alarmType))
                return 1;
            return 2;
        case cAtSdhPathAlarmRfi:
            return 3;
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmErdiC:
            return 4;
        default:
            return 0;
        }
    }

static eBool  AlarmIsFromError(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmErdiC:
        case cAtSdhPathAlarmLop:/*
        case cAtSdhPathAlarmRfi:*/
            return cAtTrue;
        case cAtSdhPathAlarmAis:
        case cAtSdhPathAlarmUneq:
            if (mIsForcingAisUneqOptimize(self, alarmType))
                return cAtTrue;
            return cAtFalse;

        default: return cAtFalse;
        }
    }

static uint32 RegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathCounterTypeBip:
            return cReg_upen_BIPvtcfg;
        case cAtSdhPathCounterTypeRei:
            return cReg_upen_REIvtcfg;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused (errorType);
    return cBit31_0;
    }

static uint32 BipCounterGet(AtAttController self, eBool r2c)
    {
    AtSdhChannel channel   = (AtSdhChannel)AtAttControllerChannelGet(self);
    AtModule     sdh       = AtChannelModuleGet((AtChannel)channel);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    uint8        slice     = 0, hwStsInSlice = 0;
    uint32 vtgId = 0;
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    uint32 counter = 0;

    if ((channelType == cAtSdhChannelTypeTu12) || (channelType == cAtSdhChannelTypeTu11))
        {
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(channel));
        }
    else if ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11))
        {
        AtSdhChannel tu1x = AtSdhChannelParentChannelGet(channel);
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(tu1x));
        }
    if (ThaSdhChannel2HwMasterStsId(channel, cAtModuleSdh, &slice, &hwStsInSlice) == cAtOk)
        {
        uint32 regAddr = 0xa381fUL + 16384UL*slice + 32UL*hwStsInSlice + 4UL*vtgId + Tha60210011ModuleOcnBaseAddress(ocnModule);
        counter =  mModuleHwRead(sdh, regAddr);
        if (r2c)
            mModuleHwWrite(sdh, regAddr, 0);
        }

    return counter;
    }

static eBool IsAddressBelongedToPointerForcing(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    AtUnused(self);
    if ((regAddr >= cAtt6A290021LoPointerForcingStartAddress)&&
        (regAddr <= cAtt6A290021LoPointerForcingEndAddress))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 RealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwStsInSlice;
    uint32 vtgId = 0, vtId = 0;
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    uint32 sliceOffset = IsAddressBelongedToPointerForcing(self, regAddr) ? 4096UL : 16384UL;

    if ((channelType == cAtSdhChannelTypeTu12) || (channelType == cAtSdhChannelTypeTu11))
        {
        vtId = AtChannelIdGet((AtChannel)channel);
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(channel));
        }
    else if ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11))
        {
        AtSdhChannel tu1x = AtSdhChannelParentChannelGet(channel);
        vtId = AtChannelIdGet((AtChannel)tu1x);
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(tu1x));
        }
    
    if (ThaSdhChannel2HwMasterStsId(channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        {
        /*AtPrintc(cSevInfo, "regAddr 0x%x, hwStsInSlice %d, slice %d, vtg %d, vt %d\n", regAddr, hwStsInSlice, slice, vtgId, vtId);*/
        return regAddr + 32UL*hwStsInSlice + slice * sliceOffset + vtgId * 4UL + vtId + Tha60210011ModuleOcnBaseAddress(ocnModule);
        }

    return cInvalidUint32;
    }

/*static eBool NeedDouble(AtSdhChannel channel)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet(channel);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    if (rate <= cAtSdhLineRateStm4)
        return cAtTrue;
    return cAtFalse;
    }*/

static uint32 PointerRealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8        slice, hwStsInSlice;
    uint32       vtgId = 0, vtId = 0;
    AtSdhChannel      channel     = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModuleOcn      ocnModule   = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    AtSdhChannel      terVc       = channel;

    if (!Tha60290021ModuleSdhIsTerminatedVc(channel))
        {
        terVc = (AtSdhChannel)AtChannelSourceGet((AtChannel)terVc);
        if (terVc==NULL)
            terVc     = channel;
        }

    if ((channelType == cAtSdhChannelTypeTu12) || (channelType == cAtSdhChannelTypeTu11))
        {
        vtId = AtChannelIdGet((AtChannel)channel);
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(channel));
        }
    else if ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11))
        {
        AtSdhChannel tu1x = AtSdhChannelParentChannelGet(channel);
        vtId = AtChannelIdGet((AtChannel)tu1x);
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(tu1x));
        }

    if (ThaSdhChannel2HwMasterStsId(terVc, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        {
        /*if (NeedDouble(channel))
            return regAddr + 64UL*hwStsInSlice + slice * 4096UL + vtgId * 4UL + vtId + Tha60210011ModuleOcnBaseAddress(ocnModule);*/
        return regAddr + 32UL*hwStsInSlice + slice * 4096UL + vtgId * 4UL + vtId + Tha60210011ModuleOcnBaseAddress(ocnModule);
        }

    return cInvalidUint32;
    }

static AtDevice DeviceGet(AtChannel self)
    {
    return AtChannelDeviceGet(self);
    }

static Tha6A290021ModuleSdh SdhModule(AtChannel self)
    {
    return (Tha6A290021ModuleSdh) AtDeviceModuleGet(DeviceGet(self), cAtModuleSdh);
    }

static AtIpCore IpCore(AtChannel self)
    {
    return AtDeviceIpCoreGet(DeviceGet(self), 0);
    }

static uint16 LongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return Tha60290021ModuleSdhAttLongReadOnCore(SdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel));
    }

static uint16 LongWrite(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return Tha60290021ModuleSdhAttLongWriteOnCore(SdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel));
    }

static uint16 PointerLongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return Tha60290021ModuleSdhAttLongReadOnCore(SdhModule(channel), mMethodsGet(att)->PointerRealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel));
    }

static uint16 PointerLongWrite(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return Tha60290021ModuleSdhAttLongWriteOnCore(SdhModule(channel), mMethodsGet(att)->PointerRealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel));
    }

static uint32 Read(AtAttController self, uint32 regAddr)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return mChannelHwRead(channel, mMethodsGet(att)->RealAddress(att, regAddr), cAtModuleSdh);
    }

static void Write(AtAttController self, uint32 regAddr,uint32 value)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    mChannelHwWrite(channel, mMethodsGet(att)->RealAddress(att, regAddr), value, cAtModuleSdh);
    }

static eBool AlarmForceIsSupported(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
        case cAtSdhPathAlarmLop:
        case cAtSdhPathAlarmUneq:
        case cAtSdhPathAlarmRfi:
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiC:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eBool IsAisUneqOptimizeSupported(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    if ((alarmType == cAtSdhPathAlarmLop) ||
        (alarmType == cAtSdhPathAlarmAis) ||
        (alarmType == cAtSdhPathAlarmUneq))
        {
        uint32 startVersionSupport = ThaVersionReaderReleasedDateBuild(18, 12, 12);
        uint32 hwVersion = ThaVersionReaderVersionNumberFromHwGet(ThaAttControllerVersionReader((ThaAttController) self));
        hwVersion = hwVersion >> 8; /* Ignoge version, just take date */
        if (hwVersion >= startVersionSupport)
            return cAtTrue;
        }
    return cAtFalse;
    }

static void ForcePointerDurSw2Hw(uint32 swDur, uint32 *durLo, uint32 *durHi)
    {
    uint32 duration = swDur * 2000 - 1;
    *durLo = duration & cBit21_0;
    mFieldGet(duration, cBit26_22, 22, uint32, durHi);
    }

static uint32 ForcePointerDurHw2Sw(uint32 durLo, uint32 durHi)
    {
    return ((durLo + (durHi << 22)) + 1) / 2000;
    }

static eAtRet ForcePointerAdjDurationSet(AtAttController self, uint32 pointerType, uint32 duration)
    {
    uint32 durHi, durLo;
    uint32 regAddr = cAttReg_stsloadj_Base;
    uint32 longRegVal[4];
    AtUnused(pointerType);
    mMethodsGet(self)->PointerLongRead(self, regAddr, longRegVal, 4);
    ForcePointerDurSw2Hw(duration, &durLo, &durHi);

    mRegFieldSet(longRegVal[0], cAtt6A290021PointerForcingDurLo, durLo);
    mRegFieldSet(longRegVal[1], cAtt6A290021PointerForcingDurHi, durHi);
    mMethodsGet(self)->PointerLongWrite(self, regAddr, longRegVal, 4);

    return cAtOk;
    }

static uint32 ForcePointerAdjDurationGet(AtAttController self, uint32 pointerType)
    {
    uint32 durHi, durLo;
    uint32 regAddr = cAttReg_stsloadj_Base;
    uint32 longRegVal[4];
    AtUnused(pointerType);
    mMethodsGet(self)->PointerLongRead(self, regAddr, longRegVal, 4);

    durLo = mRegField(longRegVal[0], cAtt6A290021PointerForcingDurLo);
    durHi = mRegField(longRegVal[1], cAtt6A290021PointerForcingDurHi);
    return ForcePointerDurHw2Sw(durLo, durHi);
    }

static eAtRet ForcePointerAdj(AtAttController self, uint32 pointerType)
    {
    return HwForcePointerG783(self, pointerType, cAtTrue);
    }

static eAtRet UnForcePointerAdj(AtAttController self, uint32 pointerType)
    {
    return HwForcePointerG783(self, pointerType, cAtFalse);
    }

static tAtOsalCurTime* ForcePointerBeginTimeGet(ThaAttController self)
    {
    return &(mThis(self)->startTimeForcePointer);
    }

static uint8 ForcePointerAdjModeSwToHwMode(uint8 mode)
    {
    switch (mode)
        {
        case cAtAttForcePointerAdjModeG783a:
            return 0;
        case cAtAttForcePointerAdjModeG783b:
            return 1;
        case cAtAttForcePointerAdjModeG783c:
            return 2;
        case cAtAttForcePointerAdjModeG783d:
            return 3;
        case cAtAttForcePointerAdjModeG783e:
            return 4;
        case cAtAttForcePointerAdjModeG783f:
            return 5;
        case cAtAttForcePointerAdjModeG783gPart2:
            return 7;
        case cAtAttForcePointerAdjModeG783gPart3:
            return 8;
        case cAtAttForcePointerAdjModeG783gPart4:
            return 9;
        case cAtAttForcePointerAdjModeG783hPartb:
            return 11;
        case cAtAttForcePointerAdjModeG783hPartc:
            return 12;
        case cAtAttForcePointerAdjModeG783hPartd:
            return 13;
        case cAtAttForcePointerAdjModeG783i:
            return 14;
        case cAtAttForcePointerAdjModeG783jPartb:
            return 16;
        case cAtAttForcePointerAdjModeG783jPartc:
            return 17;
        case cAtAttForcePointerAdjModeG783jPartd:
            return 18;
        case cAtAttForcePointerAdjModeG7834frames:
            return 31;
        default:
            return 6; /*Invalid value*/
        }
    }

static eAtAttForcePointerAdjMode PointerAdjModeHw2Sw(uint8 mode)
    {
    switch (mode)
        {
        case 0:
            return cAtAttForcePointerAdjModeG783a;
        case 1:
            return cAtAttForcePointerAdjModeG783b;
        case 2:
            return cAtAttForcePointerAdjModeG783c;
        case 3:
            return cAtAttForcePointerAdjModeG783d;
        case 4:
            return cAtAttForcePointerAdjModeG783e;
        case 5:
            return cAtAttForcePointerAdjModeG783f;
        case 7:
            return cAtAttForcePointerAdjModeG783gPart2;
        case 8:
            return cAtAttForcePointerAdjModeG783gPart3;
        case 9:
            return cAtAttForcePointerAdjModeG783gPart4;
        case 11:
            return cAtAttForcePointerAdjModeG783hPartb;
        case 12:
            return cAtAttForcePointerAdjModeG783hPartc;
        case 13:
            return cAtAttForcePointerAdjModeG783hPartd;
        case 14:
            return cAtAttForcePointerAdjModeG783i;
        case 16:
            return cAtAttForcePointerAdjModeG783jPartb;
        case 17:
            return cAtAttForcePointerAdjModeG783jPartc;
        case 18:
            return cAtAttForcePointerAdjModeG783jPartd;
        case 31:
            return cAtAttForcePointerAdjModeG7834frames;

        default:
            return cAtAttForcePointerAdjModeG7834invalid; /*Invalid value*/
        }
    }

static eAtRet ForcePointerAdjModeSet(AtAttController self, uint32 pointerType, eAtAttForcePointerAdjMode mode)
    {

    uint32 regVal, regAddr;
    uint8 hwMode = ForcePointerAdjModeSwToHwMode(mode);
    uint8 continuousMode = (mode == cAtAttForcePointerAdjModeContinuous)? cAtTrue : cAtFalse;
    uint8 oneshotMode = (mode == cAtAttForcePointerAdjModeOneshot)? cAtTrue : cAtFalse;
    uint32 typeOfpointer = (pointerType == 0)? increase: decrease;

    regAddr =cAttReg_stsloadj_Base;


    regVal = Read(self, regAddr);
    if((!continuousMode)&&(!oneshotMode))
        {
        mRegFieldSet(regVal, cAtt_stsloadj_cfg_frcmode_, hwMode);
        mRegFieldSet(regVal, cAtt_stsloadj_cfg_frccont_, cAtFalse);/*one shot mode*/
        }
    else
        {
        mRegFieldSet(regVal, cAtt_stsloadj_cfg_frccont_, continuousMode); /*TODO: one-shot mode, will be updated later*/
        }

    mRegFieldSet(regVal, cAtt_stsloadj_cfg_frctype_, typeOfpointer);

    Write(self, regAddr, regVal);

    return cAtOk;
    }

static eAtAttForcePointerAdjMode ForcePointerAdjModeGet(AtAttController self, uint32 pointerType)
    {
    uint32 regVal, regAddr;
    eAtAttForcePointerAdjMode forcePointerAdjMode;
    regAddr = cAttReg_stsloadj_Base;
    AtUnused(pointerType);
    regVal = Read(self, regAddr);
    forcePointerAdjMode = PointerAdjModeHw2Sw(mRegField(regVal, cAtt_stsloadj_cfg_frcmode_));

    return forcePointerAdjMode;
    }

static eAtRet HwForcePointerG783(AtAttController self, uint32 pointerType, eBool force)
    {
    uint32 regVal, regAddr;
    AtUnused(pointerType);

    regAddr = cAttReg_stsloadj_Base;
    regVal = Read(self, regAddr);
    mRegFieldSet(regVal, cAtt_stsloadj_cfg_frcdtvl_, mBoolToBin(force));
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet Init(AtAttController self)
    {
    eAtRet ret = m_AtAttControllerMethods->Init(self);
    return ret;
    }

static eAtRet FrequenceOffsetSet(AtAttController self, float ppm)
    {
    eAtRet ret = Tha6A290021SdhPathPointerInit(self, ppm);
    ret |= m_AtAttControllerMethods->FrequenceOffsetSet(self, ppm);
    return ret;
    }

static eAtRet PointerAdjEnable(AtAttController self, eBool enable)
    {
    return Tha6A290021SdhPathPointerPointerAdjEnable(self, enable);
    }

static uint8  HwRdiType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    /* ERDI disabled
     * V5[b8]   K4[b5:b7]
     * 1        0        0        0  RDI
     * 0        0        0        0  No RDI
     *
     * ERDI enabled
     * 0        0        0        1  No RDI
     * 0        0        1        0  E-RDI payload defect  PLM
     * 1        1        0        1  E-RDI server defect  AIS, LOP
     * 1        1        1        0  E-RDI connectivity defect
     */

    AtSdhPath path = (AtSdhPath)AtAttControllerChannelGet((AtAttController)self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmRdi  :return 0x8;
        case cAtSdhPathAlarmErdiC:return 0xE;
        case cAtSdhPathAlarmErdiS:return 0xD;
        case cAtSdhPathAlarmErdiP:return 0x2;
        default:
            return (AtSdhPathERdiIsEnabled(path)) ? 0x1 : 0;
        }
    }

static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtAttControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride, m_AtAttControllerMethods, sizeof(m_AtAttControllerOverride));
        mMethodOverride(m_AtAttControllerOverride, Init);
        mMethodOverride(m_AtAttControllerOverride, LongRead);
        mMethodOverride(m_AtAttControllerOverride, LongWrite);
        mMethodOverride(m_AtAttControllerOverride, PointerLongRead);
        mMethodOverride(m_AtAttControllerOverride, PointerLongWrite);
        mMethodOverride(m_AtAttControllerOverride, Read);
        mMethodOverride(m_AtAttControllerOverride, Write);
        mMethodOverride(m_AtAttControllerOverride, AlarmForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, BipCounterGet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjModeSet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjModeGet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdj);
        mMethodOverride(m_AtAttControllerOverride, UnForcePointerAdj);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjDurationSet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjDurationGet);
        mMethodOverride(m_AtAttControllerOverride, FrequenceOffsetSet);
        mMethodOverride(m_AtAttControllerOverride, PointerAdjEnable);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static void OverrideThaAttController(ThaAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(self), sizeof(m_ThaAttControllerOverride));
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportDataMask8bit);
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportDataMask2bit);
        mMethodOverride(m_ThaAttControllerOverride, ForcePointerBeginTimeGet);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void OverrideTha6A210031PdhDe3AttController(AtAttController self)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(controller), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PointerRealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIsFromError);

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegPointerAdj);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PointerAdjTypeMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, SwPointerAdjToHw);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportPointerAdj);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportConvertStep);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, IsAisUneqOptimizeSupported);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwRdiType);
        }

    mMethodsSet(controller, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideAtAttController(self);
    OverrideThaAttController((ThaAttController) self);
    OverrideTha6A210031PdhDe3AttController(self);
    }
static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021SdhVc1xAttController);
    }

static AtAttController ObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031SdhVc1xAttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A290021SdhVc1xAttControllerNew(AtChannel sdhPath)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(controller, sdhPath);
    }

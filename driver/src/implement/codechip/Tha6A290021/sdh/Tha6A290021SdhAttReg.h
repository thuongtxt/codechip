/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6A290021SdhLineAttReg.h
 * 
 * Created Date: July 3, 2018
 *
 * Description : SDH ATT register description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021SDHLINEATTREG_H_
#define _THA6A290021SDHLINEATTREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : Force LOS Error Configuration
Reg Addr   : 0x21008-0x21008
             0x24108-0x24108 (v2)
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure Force LOS3G Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_loscfg                                                                        0x21008
#define cReg_upen_loscfg_WidthVal                                                                           32
#define cReg_upen_loscfg_V2                                                              0x24108
/*--------------------------------------
BitField Name: lostim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_loscfg_lostim_mod_Mask                                                                cBit27_26
#define c_upen_loscfg_lostim_mod_Shift                                                                      26

/*--------------------------------------
BitField Name: loserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_loscfg_loserr_clr_Mask                                                                cBit25_18
#define c_upen_loscfg_loserr_clr_Shift                                                                      18

/*--------------------------------------
BitField Name: loserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_loscfg_loserr_set_Mask                                                                cBit17_10
#define c_upen_loscfg_loserr_set_Shift                                                                      10

/*--------------------------------------
BitField Name: loserr_step
BitField Type: RW
BitField Desc: The Total Step to Force LOS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_loscfg_loserr_step_Mask                                                                 cBit9_2
#define c_upen_loscfg_loserr_step_Shift                                                                      2

/*--------------------------------------
BitField Name: losmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_loscfg_losmsk_pos_Mask_01                                                             cBit31_18
#define c_upen_loscfg_losmsk_pos_Shift_01                                                                   18
#define c_upen_loscfg_losmsk_pos_Mask_02                                                               cBit1_0
#define c_upen_loscfg_losmsk_pos_Shift_02                                                                    0

/*--------------------------------------
BitField Name: losmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_loscfg_losmsk_ena_Mask                                                                   cBit17
#define c_upen_loscfg_losmsk_ena_Shift                                                                      17

/*--------------------------------------
BitField Name: loserr_num
BitField Type: RW
BitField Desc: The Total LOS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_loscfg_loserr_num_Mask                                                                 cBit16_1
#define c_upen_loscfg_loserr_num_Shift                                                                       1

/*--------------------------------------
BitField Name: losfrc_mod
BitField Type: RW
BitField Desc: Force LOS Mode 0: One shot ( N events in T the unit time) 1:
continous (loserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_loscfg_losfrc_mod_Mask                                                                    cBit0
#define c_upen_loscfg_losfrc_mod_Shift                                                                       0

#define c_upen_loscfg_los_sec_num_Mask                                                                cBit14_7
/*------------------------------------------------------------------------------
Reg Name   : Force LOS at STM 64 Rate Configuration
Reg Addr   : 0x24020-0x24020
             OLD: 0x21801-0x21801
Reg Formula:
    Where  : 
Reg Desc   : 
This register is applicable to configure Force LOS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lostm64scfg                                                                          0x24020
#define cReg_upen_lostm64scfg_WidthVal                                                                      32
#define cReg_upen_lostm64scfgold                                                                       0x21801

/*--------------------------------------
BitField Name: losstm64tim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_lostm64scfg_losstm64tim_mod_Mask                                                      cBit27_26
#define c_upen_lostm64scfg_losstm64tim_mod_Shift                                                            26

/*--------------------------------------
BitField Name: losstm64_clr
BitField Type: RW
BitField Desc: Timer LOS at STM 64 Rate is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_lostm64scfg_losstm64_clr_Mask                                                         cBit25_18
#define c_upen_lostm64scfg_losstm64_clr_Shift                                                               18

/*--------------------------------------
BitField Name: losstm64_set
BitField Type: RW
BitField Desc: Timer LOS at STM 64 Rate is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_lostm64scfg_losstm64_set_Mask                                                         cBit17_10
#define c_upen_lostm64scfg_losstm64_set_Shift                                                               10

/*--------------------------------------
BitField Name: losstm64_step
BitField Type: RW
BitField Desc: The Total Step to Force LOS at STM 64 RateCongiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_lostm64scfg_losstm64_step_Mask                                                          cBit9_2
#define c_upen_lostm64scfg_losstm64_step_Shift                                                               2

/*--------------------------------------
BitField Name: losstm64msk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOS at STM 64 RateCongiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_lostm64scfg_losstm64msk_pos_Mask_01                                                   cBit31_18
#define c_upen_lostm64scfg_losstm64msk_pos_Shift_01                                                         18
#define c_upen_lostm64scfg_losstm64msk_pos_Mask_02                                                     cBit1_0
#define c_upen_lostm64scfg_losstm64msk_pos_Shift_02                                                          0

/*--------------------------------------
BitField Name: losstm64msk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOS at STM 64 RateCongiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_lostm64scfg_losstm64msk_ena_Mask                                                         cBit17
#define c_upen_lostm64scfg_losstm64msk_ena_Shift                                                            17

/*--------------------------------------
BitField Name: losstm64err_num
BitField Type: RW
BitField Desc: The Total LOS at STM 64 Rate Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_lostm64scfg_losstm64err_num_Mask                                                       cBit16_1
#define c_upen_lostm64scfg_losstm64err_num_Shift                                                             1

/*--------------------------------------
BitField Name: losstm64_mod
BitField Type: RW
BitField Desc: Force LOS Mode 0: One shot ( N events in T the unit time) 1:
continous (loserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_lostm64scfg_losstm64_mod_Mask                                                             cBit0
#define c_upen_lostm64scfg_losstm64_mod_Shift                                                                0

/*------------------------------------------------------------------------------
Reg Name   : Force LOF Error Configuration
Reg Addr   : 0x24100-0x24100
Reg Formula: 0x24100 + 32*LineId
    Where  :
           + $LineId(0-7): Line ID
Reg Desc   :
This register is applicable to configure Force LOF3G Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lofcfg                                                                               0x24100
#define cReg_upen_lofcfg_WidthVal                                                                           64

/*--------------------------------------
BitField Name: loftim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_lofcfg_loftim_mod_Mask                                                                cBit27_26
#define c_upen_lofcfg_loftim_mod_Shift                                                                      26

/*--------------------------------------
BitField Name: loferr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_lofcfg_loferr_clr_Mask                                                                cBit25_18
#define c_upen_lofcfg_loferr_clr_Shift                                                                      18

/*--------------------------------------
BitField Name: loferr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_lofcfg_loferr_set_Mask                                                                cBit17_10
#define c_upen_lofcfg_loferr_set_Shift                                                                      10

/*--------------------------------------
BitField Name: loferr_step
BitField Type: RW
BitField Desc: The Total Step to Force LOF Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_lofcfg_loferr_step_Mask                                                                 cBit9_2
#define c_upen_lofcfg_loferr_step_Shift                                                                      2

/*--------------------------------------
BitField Name: lofmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOF Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_lofcfg_lofmsk_pos_Mask_01                                                             cBit31_18
#define c_upen_lofcfg_lofmsk_pos_Shift_01                                                                   18
#define c_upen_lofcfg_lofmsk_pos_Mask_02                                                               cBit1_0
#define c_upen_lofcfg_lofmsk_pos_Shift_02                                                                    0

/*--------------------------------------
BitField Name: lofmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOF Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_lofcfg_lofmsk_ena_Mask                                                                   cBit17
#define c_upen_lofcfg_lofmsk_ena_Shift                                                                      17

/*--------------------------------------
BitField Name: loferr_num
BitField Type: RW
BitField Desc: The Total LOF Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_lofcfg_loferr_num_Mask                                                                 cBit16_1
#define c_upen_lofcfg_loferr_num_Shift                                                                       1

/*--------------------------------------
BitField Name: loffrc_mod
BitField Type: RW
BitField Desc: Force LOF Mode 0: One shot ( N events in T the unit time) 1:
continous (loferr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_lofcfg_loffrc_mod_Mask                                                                    cBit0
#define c_upen_lofcfg_loffrc_mod_Shift                                                                       0

/*------------------------------------------------------------------------------
Reg Name   : Force LOF at STM 64 Rate Error Configuration
Reg Addr   : 0x24000-0x24000
Reg Formula: 0x24000 + 32*LineId
    Where  : 
           + $LineId(0-7): Line ID
Reg Desc   : 
This register is applicable to configure Force LOF Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lofstm64cfg                                                                          0x24000
#define cReg_upen_lofstm64cfg_WidthVal                                                                      64

/*--------------------------------------
BitField Name: lofstm64tim_mod
BitField Type: RW
BitField Desc: Timer LOF at STM 64 Rate mode 00: every valid 01: 1ms 10: 10ms
11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_lofstm64cfg_lofstm64tim_mod_Mask                                                      cBit27_26
#define c_upen_lofstm64cfg_lofstm64tim_mod_Shift                                                            26

/*--------------------------------------
BitField Name: lofstm64err_clr
BitField Type: RW
BitField Desc: Timer LOF at STM 64 Rate is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_lofstm64cfg_lofstm64err_clr_Mask                                                      cBit25_18
#define c_upen_lofstm64cfg_lofstm64err_clr_Shift                                                            18

/*--------------------------------------
BitField Name: lofstm64err_set
BitField Type: RW
BitField Desc: Timer LOF at STM 64 Rate is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_lofstm64cfg_lofstm64err_set_Mask                                                      cBit17_10
#define c_upen_lofstm64cfg_lofstm64err_set_Shift                                                            10

/*--------------------------------------
BitField Name: lofstm64err_step
BitField Type: RW
BitField Desc: The Total Step to Force LOF at STM 64 Rate Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_lofstm64cfg_lofstm64err_step_Mask                                                       cBit9_2
#define c_upen_lofstm64cfg_lofstm64err_step_Shift                                                            2

/*--------------------------------------
BitField Name: lofstm64msk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOF at STM 64 Rate Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_lofstm64cfg_lofstm64msk_pos_Mask_01                                                   cBit31_18
#define c_upen_lofstm64cfg_lofstm64msk_pos_Shift_01                                                         18
#define c_upen_lofstm64cfg_lofstm64msk_pos_Mask_02                                                     cBit1_0
#define c_upen_lofstm64cfg_lofstm64msk_pos_Shift_02                                                          0

/*--------------------------------------
BitField Name: lofstm64msk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOF at STM 64 Rate Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_lofstm64cfg_lofstm64msk_ena_Mask                                                         cBit17
#define c_upen_lofstm64cfg_lofstm64msk_ena_Shift                                                            17

/*--------------------------------------
BitField Name: lofstm64err_num
BitField Type: RW
BitField Desc: The Total LOF at STM 64 Rate Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_lofstm64cfg_lofstm64err_num_Mask                                                       cBit16_1
#define c_upen_lofstm64cfg_lofstm64err_num_Shift                                                             1

/*--------------------------------------
BitField Name: lofstm64frc_mod
BitField Type: RW
BitField Desc: Force LOF at STM 64 Rate Mode 0: One shot ( N events in T the
unit time) 1: continous (loferr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_lofstm64cfg_lofstm64frc_mod_Mask                                                          cBit0
#define c_upen_lofstm64cfg_lofstm64frc_mod_Shift                                                             0

/*------------------------------------------------------------------------------
Reg Name   : Force Line AIS Error Configuration
Reg Addr   : 0x21008-0x21008
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure Force Line AIS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_laiscfg                                                                              0x21008
#define cReg_upen_laiscfg_WidthVal                                                                          32

/*--------------------------------------
BitField Name: aistim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_laiscfg_aistim_mod_Mask                                                               cBit27_26
#define c_upen_laiscfg_aistim_mod_Shift                                                                     26

/*--------------------------------------
BitField Name: aiserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_laiscfg_aiserr_clr_Mask                                                               cBit25_18
#define c_upen_laiscfg_aiserr_clr_Shift                                                                     18

/*--------------------------------------
BitField Name: aiserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_laiscfg_aiserr_set_Mask                                                               cBit17_10
#define c_upen_laiscfg_aiserr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: aiserr_step
BitField Type: RW
BitField Desc: The Total Step to Force AIS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_laiscfg_aiserr_step_Mask                                                                cBit9_2
#define c_upen_laiscfg_aiserr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: aismsk_pos
BitField Type: RW
BitField Desc: The Position Mask for AIS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_laiscfg_aismsk_pos_Mask_01                                                            cBit31_18
#define c_upen_laiscfg_aismsk_pos_Shift_01                                                                  18
#define c_upen_laiscfg_aismsk_pos_Mask_02                                                              cBit1_0
#define c_upen_laiscfg_aismsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: aismsk_ena
BitField Type: RW
BitField Desc: The Data Mask For AIS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_laiscfg_aismsk_ena_Mask                                                                  cBit17
#define c_upen_laiscfg_aismsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: aiserr_num
BitField Type: RW
BitField Desc: The Total AIS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_laiscfg_aiserr_num_Mask                                                                cBit16_1
#define c_upen_laiscfg_aiserr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: aisfrc_mod
BitField Type: RW
BitField Desc: Force AIS Mode 0: One shot ( N events in T the unit time) 1:
continous (aiserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_laiscfg_aisfrc_mod_Mask                                                                   cBit0
#define c_upen_laiscfg_aisfrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force Line AIS Error Configuration
Reg Addr   : 0x21800-0x21800
Reg Formula:
    Where  : 
Reg Desc   : 
This register is applicable to configure Force Line AIS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_laisstm64cfg                                                                         0x21800
#define cReg_upen_laisstm64cfg_WidthVal                                                                     32

/*--------------------------------------
BitField Name: aistim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_laisstm64cfg_aistim_mod_Mask                                                          cBit27_26
#define c_upen_laisstm64cfg_aistim_mod_Shift                                                                26

/*--------------------------------------
BitField Name: aisstm64err_clr
BitField Type: RW
BitField Desc: Timer AIS at STM 64 Rate is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_laisstm64cfg_aisstm64err_clr_Mask                                                     cBit25_18
#define c_upen_laisstm64cfg_aisstm64err_clr_Shift                                                           18

/*--------------------------------------
BitField Name: aiserr_set
BitField Type: RW
BitField Desc: Timer AIS at STM 64 Rate is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_laisstm64cfg_aiserr_set_Mask                                                          cBit17_10
#define c_upen_laisstm64cfg_aiserr_set_Shift                                                                10

/*--------------------------------------
BitField Name: aisstm64err_step
BitField Type: RW
BitField Desc: The Total Step to Force AIS at STM 64 Rate Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_laisstm64cfg_aisstm64err_step_Mask                                                      cBit9_2
#define c_upen_laisstm64cfg_aisstm64err_step_Shift                                                           2

/*--------------------------------------
BitField Name: aisstm64msk_pos
BitField Type: RW
BitField Desc: The Position Mask for AIS at STM 64 Rate Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_laisstm64cfg_aisstm64msk_pos_Mask_01                                                  cBit31_18
#define c_upen_laisstm64cfg_aisstm64msk_pos_Shift_01                                                        18
#define c_upen_laisstm64cfg_aisstm64msk_pos_Mask_02                                                    cBit1_0
#define c_upen_laisstm64cfg_aisstm64msk_pos_Shift_02                                                         0

/*--------------------------------------
BitField Name: aisstm64msk_ena
BitField Type: RW
BitField Desc: The Data Mask For AIS at STM 64 Rate Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_laisstm64cfg_aisstm64msk_ena_Mask                                                        cBit17
#define c_upen_laisstm64cfg_aisstm64msk_ena_Shift                                                           17

/*--------------------------------------
BitField Name: aisstm64err_num
BitField Type: RW
BitField Desc: The Total AIS at STM 64 Rate Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_laisstm64cfg_aisstm64err_num_Mask                                                      cBit16_1
#define c_upen_laisstm64cfg_aisstm64err_num_Shift                                                            1

/*--------------------------------------
BitField Name: aisstm64frc_mod
BitField Type: RW
BitField Desc: Force AIS at STM 64 Rate Mode 0: One shot ( N events in T the
unit time) 1: continous (aiserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_laisstm64cfg_aisstm64frc_mod_Mask                                                         cBit0
#define c_upen_laisstm64cfg_aisstm64frc_mod_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Force B1 Byte Error Configuration
Reg Addr   : 0x24110-0x24110
Reg Formula: 0x24110 + 32*LineId
    Where  :
           + $LineId(0-7): Line ID
Reg Desc   :
This register is applicable to configure Force B1 3G Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_B1bytcfg                                                                             0x24110
#define cReg_upen_B1bytcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: b1ftim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ftim_mod_Mask                                                              cBit13_12
#define c_upen_B1bytcfg_b1ftim_mod_Shift                                                                    12

/*--------------------------------------
BitField Name: b1ferr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ferr_clr_Mask                                                                 cBit11
#define c_upen_B1bytcfg_b1ferr_clr_Shift                                                                    11

/*--------------------------------------
BitField Name: b1ferr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ferr_set_Mask                                                                 cBit10
#define c_upen_B1bytcfg_b1ferr_set_Shift                                                                    10

/*--------------------------------------
BitField Name: b1ferr_step
BitField Type: RW
BitField Desc: The Total Step to Force B1 Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ferr_step_Mask                                                               cBit9_2
#define c_upen_B1bytcfg_b1ferr_step_Shift                                                                    2

/*--------------------------------------
BitField Name: b1fmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for B1 Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_B1bytcfg_b1fmsk_pos_Mask_01                                                           cBit31_18
#define c_upen_B1bytcfg_b1fmsk_pos_Shift_01                                                                 18
#define c_upen_B1bytcfg_b1fmsk_pos_Mask_02                                                             cBit1_0
#define c_upen_B1bytcfg_b1fmsk_pos_Shift_02                                                                  0

/*--------------------------------------
BitField Name: b1fmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For  B1 Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_B1bytcfg_b1fmsk_ena_Mask                                                                 cBit17
#define c_upen_B1bytcfg_b1fmsk_ena_Shift                                                                    17

/*--------------------------------------
BitField Name: b1ferr_num
BitField Type: RW
BitField Desc: The Total B1 Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ferr_num_Mask                                                               cBit16_1
#define c_upen_B1bytcfg_b1ferr_num_Shift                                                                     1

/*--------------------------------------
BitField Name: b1ffrc_mod
BitField Type: RW
BitField Desc: Force B1 Mode 0: One shot 1: continous (b1ferr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ffrc_mod_Mask                                                                  cBit0
#define c_upen_B1bytcfg_b1ffrc_mod_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Force B1 Byte at STM 64 Rate Error Configuration
Reg Addr   : 0x24010-0x24010
Reg Formula: 0x24010 + 32*LineId
    Where  : 
           + $LineId(0-7): Line ID
Reg Desc   : 
This register is applicable to configure Force B1 Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_B1bytstm64cfg                                                                        0x24010
#define cReg_upen_B1bytstm64cfg_WidthVal                                                                    64

/*--------------------------------------
BitField Name: b1fstm64tim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_B1bytstm64cfg_b1fstm64tim_mod_Mask                                                    cBit13_12
#define c_upen_B1bytstm64cfg_b1fstm64tim_mod_Shift                                                          12

/*--------------------------------------
BitField Name: b1fstm64err_clr
BitField Type: RW
BitField Desc: Timer B1 at STM 64 Rate is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_B1bytstm64cfg_b1fstm64err_clr_Mask                                                       cBit11
#define c_upen_B1bytstm64cfg_b1fstm64err_clr_Shift                                                          11

/*--------------------------------------
BitField Name: b1fstm64err_set
BitField Type: RW
BitField Desc: Timer B1 is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_B1bytstm64cfg_b1fstm64err_set_Mask                                                       cBit10
#define c_upen_B1bytstm64cfg_b1fstm64err_set_Shift                                                          10

/*--------------------------------------
BitField Name: b1fstm64err_step
BitField Type: RW
BitField Desc: The Total Step to Force B1 at STM 64 Rate Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_B1bytstm64cfg_b1fstm64err_step_Mask                                                     cBit9_2
#define c_upen_B1bytstm64cfg_b1fstm64err_step_Shift                                                          2

/*--------------------------------------
BitField Name: b1fstm64msk_pos
BitField Type: RW
BitField Desc: The Position Mask for B1 at STM 64 Rate Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_B1bytstm64cfg_b1fstm64msk_pos_Mask_01                                                 cBit31_18
#define c_upen_B1bytstm64cfg_b1fstm64msk_pos_Shift_01                                                       18
#define c_upen_B1bytstm64cfg_b1fstm64msk_pos_Mask_02                                                   cBit1_0
#define c_upen_B1bytstm64cfg_b1fstm64msk_pos_Shift_02                                                        0

/*--------------------------------------
BitField Name: b1fstm64msk_ena
BitField Type: RW
BitField Desc: The Data Mask For  B1 at STM 64 Rate Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_B1bytstm64cfg_b1fstm64msk_ena_Mask                                                       cBit17
#define c_upen_B1bytstm64cfg_b1fstm64msk_ena_Shift                                                          17

/*--------------------------------------
BitField Name: b1fstm64err_num
BitField Type: RW
BitField Desc: The Total B1 at STM 64 Rate Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_B1bytstm64cfg_b1fstm64err_num_Mask                                                     cBit16_1
#define c_upen_B1bytstm64cfg_b1fstm64err_num_Shift                                                           1

/*--------------------------------------
BitField Name: b1fstm64frc_mod
BitField Type: RW
BitField Desc: Force B1 Mode 0: One shot 1: continous (b1ferr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_B1bytstm64cfg_b1fstm64frc_mod_Mask                                                        cBit0
#define c_upen_B1bytstm64cfg_b1fstm64frc_mod_Shift                                                           0

/*------------------------------------------------------------------------------
Reg Name   : Force Line RDI Error Configuration
Reg Addr   : 0x2100b-0x2100b
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure Force Line RDI3G Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lrdicfg                                                                              0x2100b
#define cReg_upen_lrdicfg_WidthVal                                                                          32

/*--------------------------------------
BitField Name: rditim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_lrdicfg_rditim_mod_Mask                                                               cBit27_26
#define c_upen_lrdicfg_rditim_mod_Shift                                                                     26

/*--------------------------------------
BitField Name: rdierr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_lrdicfg_rdierr_clr_Mask                                                               cBit25_18
#define c_upen_lrdicfg_rdierr_clr_Shift                                                                     18

/*--------------------------------------
BitField Name: rdierr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_lrdicfg_rdierr_set_Mask                                                               cBit17_10
#define c_upen_lrdicfg_rdierr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: rdierr_step
BitField Type: RW
BitField Desc: The Total Step to Force RDI Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_lrdicfg_rdierr_step_Mask                                                                cBit9_2
#define c_upen_lrdicfg_rdierr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: rdimsk_pos
BitField Type: RW
BitField Desc: The Position Mask for RDI Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_lrdicfg_rdimsk_pos_Mask_01                                                            cBit31_18
#define c_upen_lrdicfg_rdimsk_pos_Shift_01                                                                  18
#define c_upen_lrdicfg_rdimsk_pos_Mask_02                                                              cBit1_0
#define c_upen_lrdicfg_rdimsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: rdimsk_ena
BitField Type: RW
BitField Desc: The Data Mask For RDI Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_lrdicfg_rdimsk_ena_Mask                                                                  cBit17
#define c_upen_lrdicfg_rdimsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: rdierr_num
BitField Type: RW
BitField Desc: The Total RDI Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_lrdicfg_rdierr_num_Mask                                                                cBit16_1
#define c_upen_lrdicfg_rdierr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: rdifrc_mod
BitField Type: RW
BitField Desc: Force RDI Mode 0: One shot ( N events in T the unit time) 1:
continous (rdierr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_lrdicfg_rdifrc_mod_Mask                                                                   cBit0
#define c_upen_lrdicfg_rdifrc_mod_Shift                                                                      0



/*------------------------------------------------------------------------------
Reg Name   : Force Line RDI at STM 64 Rate Error Configuration
Reg Addr   : 0x21803-0x21803
Reg Formula:
    Where  : 
Reg Desc   : 
This register is applicable to configure Force Line RDI Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lrdistm64cfg                                                                         0x21803
#define cReg_upen_lrdistm64cfg_WidthVal                                                                     32

/*--------------------------------------
BitField Name: rdistm64tim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_lrdistm64cfg_rdistm64tim_mod_Mask                                                     cBit27_26
#define c_upen_lrdistm64cfg_rdistm64tim_mod_Shift                                                           26

/*--------------------------------------
BitField Name: rdistm64err_clr
BitField Type: RW
BitField Desc: Timer RDI at STM 64 Rate is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_lrdistm64cfg_rdistm64err_clr_Mask                                                     cBit25_18
#define c_upen_lrdistm64cfg_rdistm64err_clr_Shift                                                           18

/*--------------------------------------
BitField Name: rdistm64err_set
BitField Type: RW
BitField Desc: Timer RDI at STM 64 Rate is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_lrdistm64cfg_rdistm64err_set_Mask                                                     cBit17_10
#define c_upen_lrdistm64cfg_rdistm64err_set_Shift                                                           10

/*--------------------------------------
BitField Name: rdistm64err_step
BitField Type: RW
BitField Desc: The Total Step to Force RDI at STM 64 Rate Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_lrdistm64cfg_rdistm64err_step_Mask                                                      cBit9_2
#define c_upen_lrdistm64cfg_rdistm64err_step_Shift                                                           2

/*--------------------------------------
BitField Name: rdistm64msk_pos
BitField Type: RW
BitField Desc: The Position Mask for RDI at STM 64 Rate Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_lrdistm64cfg_rdistm64msk_pos_Mask_01                                                  cBit31_18
#define c_upen_lrdistm64cfg_rdistm64msk_pos_Shift_01                                                        18
#define c_upen_lrdistm64cfg_rdistm64msk_pos_Mask_02                                                    cBit1_0
#define c_upen_lrdistm64cfg_rdistm64msk_pos_Shift_02                                                         0

/*--------------------------------------
BitField Name: rdistm64msk_ena
BitField Type: RW
BitField Desc: The Data Mask For RDI at STM 64 Rate Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_lrdistm64cfg_rdistm64msk_ena_Mask                                                        cBit17
#define c_upen_lrdistm64cfg_rdistm64msk_ena_Shift                                                           17

/*--------------------------------------
BitField Name: rdistm64err_num
BitField Type: RW
BitField Desc: The Total RDI at STM 64 Rate Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_lrdistm64cfg_rdistm64err_num_Mask                                                      cBit16_1
#define c_upen_lrdistm64cfg_rdistm64err_num_Shift                                                            1

/*--------------------------------------
BitField Name: rdistm64frc_mod
BitField Type: RW
BitField Desc: Force RDI Mode 0: One shot ( N events in T the unit time) 1:
continous (rdierr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_lrdistm64cfg_rdistm64frc_mod_Mask                                                         cBit0
#define c_upen_lrdistm64cfg_rdistm64frc_mod_Shift                                                            0

/*------------------------------------------------------------------------------
Reg Name   : Force Path REI Error Configuration
Reg Addr   : 0x2100c-0x2100c
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure Force Path REI Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_preicfg                                                                              0x2100c
#define cReg_upen_preicfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: reitim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_preicfg_reitim_mod_Mask                                                               cBit13_12
#define c_upen_preicfg_reitim_mod_Shift                                                                     12

/*--------------------------------------
BitField Name: reierr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_preicfg_reierr_clr_Mask                                                                  cBit11
#define c_upen_preicfg_reierr_clr_Shift                                                                     11

/*--------------------------------------
BitField Name: reierr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_preicfg_reierr_set_Mask                                                                  cBit10
#define c_upen_preicfg_reierr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: reierr_step
BitField Type: RW
BitField Desc: The Total Step to Force REI Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_preicfg_reierr_step_Mask                                                                cBit9_2
#define c_upen_preicfg_reierr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: reimsk_pos
BitField Type: RW
BitField Desc: The Position Mask for REI Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_preicfg_reimsk_pos_Mask_01                                                            cBit31_18
#define c_upen_preicfg_reimsk_pos_Shift_01                                                                  18
#define c_upen_preicfg_reimsk_pos_Mask_02                                                              cBit1_0
#define c_upen_preicfg_reimsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: reimsk_ena
BitField Type: RW
BitField Desc: The Data Mask For REI Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_preicfg_reimsk_ena_Mask                                                                  cBit17
#define c_upen_preicfg_reimsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: reierr_num
BitField Type: RW
BitField Desc: The Total REI Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_preicfg_reierr_num_Mask                                                                cBit16_1
#define c_upen_preicfg_reierr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: reifrc_mod
BitField Type: RW
BitField Desc: Force REI Mode 0: One shot 1: continous (reierr_num = 16 bit
FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_preicfg_reifrc_mod_Mask                                                                   cBit0
#define c_upen_preicfg_reifrc_mod_Shift                                                                      0

/*------------------------------------------------------------------------------
Reg Name   : Force Path REI at STM 64 Rate Error Configuration
Reg Addr   : 0x21804-0x21804
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure Force Path REI Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_reistm64cfg                                                                          0x21804
#define cReg_upen_reistm64cfg_WidthVal                                                                      64

/*--------------------------------------
BitField Name: reitim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_reistm64cfg_reitim_mod_Mask                                                           cBit13_12
#define c_upen_reistm64cfg_reitim_mod_Shift                                                                 12

/*--------------------------------------
BitField Name: reistm64err_clr
BitField Type: RW
BitField Desc: Timer REI at STM 64 Rate is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_reistm64cfg_reistm64err_clr_Mask                                                         cBit11
#define c_upen_reistm64cfg_reistm64err_clr_Shift                                                            11

/*--------------------------------------
BitField Name: reistm64err_set
BitField Type: RW
BitField Desc: Timer REI at STM 64 Rate is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_reistm64cfg_reistm64err_set_Mask                                                         cBit10
#define c_upen_reistm64cfg_reistm64err_set_Shift                                                            10

/*--------------------------------------
BitField Name: reistm64err_step
BitField Type: RW
BitField Desc: The Total Step to Force REI at STM 64 Rate Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_reistm64cfg_reistm64err_step_Mask                                                       cBit9_2
#define c_upen_reistm64cfg_reistm64err_step_Shift                                                            2

/*--------------------------------------
BitField Name: reistm64msk_pos
BitField Type: RW
BitField Desc: The Position Mask for REI at STM 64 Rate Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_reistm64cfg_reistm64msk_pos_Mask_01                                                   cBit31_18
#define c_upen_reistm64cfg_reistm64msk_pos_Shift_01                                                         18
#define c_upen_reistm64cfg_reistm64msk_pos_Mask_02                                                     cBit1_0
#define c_upen_reistm64cfg_reistm64msk_pos_Shift_02                                                          0

/*--------------------------------------
BitField Name: reistm64msk_ena
BitField Type: RW
BitField Desc: The Data Mask For REI at STM 64 Rate Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_reistm64cfg_reistm64msk_ena_Mask                                                         cBit17
#define c_upen_reistm64cfg_reistm64msk_ena_Shift                                                            17

/*--------------------------------------
BitField Name: reistm64err_num
BitField Type: RW
BitField Desc: The Total REI at STM 64 Rate Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_reistm64cfg_reistm64err_num_Mask                                                       cBit16_1
#define c_upen_reistm64cfg_reistm64err_num_Shift                                                             1

/*--------------------------------------
BitField Name: reistm64frc_mod
BitField Type: RW
BitField Desc: Force REI Mode 0: One shot 1: continous (reierr_num = 16 bit
FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_reistm64cfg_reistm64frc_mod_Mask                                                          cBit0
#define c_upen_reistm64cfg_reistm64frc_mod_Shift                                                             0

/*------------------------------------------------------------------------------
Reg Name   : Force B2 at the first position Configuration
Reg Addr   : 0x2100a-0x2100a
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure Force  B2 at the first position for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_b2stcfg                                                                              0x2100a
#define cReg_upen_b2stcfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: b20tim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [58:44]
--------------------------------------*/
#define c_upen_b2stcfg_b20tim_mod_Mask                                                               cBit26_12
#define c_upen_b2stcfg_b20tim_mod_Shift                                                                     12

/*--------------------------------------
BitField Name: b20err_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [56]
--------------------------------------*/
#define c_upen_b2stcfg_b20err_clr_Mask                                                                  cBit24
#define c_upen_b2stcfg_b20err_clr_Shift                                                                     24

/*--------------------------------------
BitField Name: b20err_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [55]
--------------------------------------*/
#define c_upen_b2stcfg_b20err_set_Mask                                                                  cBit23
#define c_upen_b2stcfg_b20err_set_Shift                                                                     23

/*--------------------------------------
BitField Name: cfgb20err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force  B2 at the first position
Congiguration
BitField Bits: [54:41]
--------------------------------------*/
#define c_upen_b2stcfg_cfgb20err_step_Mask                                                            cBit22_9
#define c_upen_b2stcfg_cfgb20err_step_Shift                                                                  9

/*--------------------------------------
BitField Name: cfgb20msk_pos
BitField Type: RW
BitField Desc: The Position Mask for  B2 at the first position  Congiguration
BitField Bits: [40:25]
--------------------------------------*/
#define c_upen_b2stcfg_cfgb20msk_pos_Mask_01                                                         cBit31_25
#define c_upen_b2stcfg_cfgb20msk_pos_Shift_01                                                               25
#define c_upen_b2stcfg_cfgb20msk_pos_Mask_02                                                           cBit8_0
#define c_upen_b2stcfg_cfgb20msk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: cfgb20msk_dat
BitField Type: RW
BitField Desc: The Data Mask For B2 at the first position Congiguration
BitField Bits: [24:17]
--------------------------------------*/
#define c_upen_b2stcfg_cfgb20msk_dat_Mask                                                            cBit24_17
#define c_upen_b2stcfg_cfgb20msk_dat_Shift                                                                  17

/*--------------------------------------
BitField Name: cfgb20err_num
BitField Type: RW
BitField Desc: The Total B2 at the first position Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_b2stcfg_cfgb20err_num_Mask                                                             cBit16_1
#define c_upen_b2stcfg_cfgb20err_num_Shift                                                                   1

/*--------------------------------------
BitField Name: cfgb20frc_1sen
BitField Type: RW
BitField Desc: Force  B2 at the first position Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_b2stcfg_cfgb20frc_1sen_Mask                                                               cBit0
#define c_upen_b2stcfg_cfgb20frc_1sen_Shift                                                                  0

/*------------------------------------------------------------------------------
Reg Name   : Force B2 at the  second position Configuration
Reg Addr   : 0x21009-0x21009
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure Force  B2 at the second position for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_b2ndcfg                                                                              0x21009
#define cReg_upen_b2ndcfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: b21tim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [58:44]
--------------------------------------*/
#define c_upen_b2ndcfg_b21tim_mod_Mask                                                               cBit26_12
#define c_upen_b2ndcfg_b21tim_mod_Shift                                                                     12

/*--------------------------------------
BitField Name: b21err_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [56]
--------------------------------------*/
#define c_upen_b2ndcfg_b21err_clr_Mask                                                                  cBit24
#define c_upen_b2ndcfg_b21err_clr_Shift                                                                     24

/*--------------------------------------
BitField Name: b21err_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [55]
--------------------------------------*/
#define c_upen_b2ndcfg_b21err_set_Mask                                                                  cBit23
#define c_upen_b2ndcfg_b21err_set_Shift                                                                     23

/*--------------------------------------
BitField Name: cfgb21err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force  B2 at the second position
Congiguration
BitField Bits: [54:41]
--------------------------------------*/
#define c_upen_b2ndcfg_cfgb21err_step_Mask                                                            cBit22_9
#define c_upen_b2ndcfg_cfgb21err_step_Shift                                                                  9

/*--------------------------------------
BitField Name: cfgb21msk_pos
BitField Type: RW
BitField Desc: The Position Mask for  B2 at the second position  Congiguration
BitField Bits: [40:25]
--------------------------------------*/
#define c_upen_b2ndcfg_cfgb21msk_pos_Mask_01                                                         cBit31_25
#define c_upen_b2ndcfg_cfgb21msk_pos_Shift_01                                                               25
#define c_upen_b2ndcfg_cfgb21msk_pos_Mask_02                                                           cBit8_0
#define c_upen_b2ndcfg_cfgb21msk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: cfgb21msk_dat
BitField Type: RW
BitField Desc: The Data Mask For B2 at the second position Congiguration
BitField Bits: [24:17]
--------------------------------------*/
#define c_upen_b2ndcfg_cfgb21msk_dat_Mask                                                            cBit24_17
#define c_upen_b2ndcfg_cfgb21msk_dat_Shift                                                                  17

/*--------------------------------------
BitField Name: cfgb21err_num
BitField Type: RW
BitField Desc: The Total B2 at the second position Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_b2ndcfg_cfgb21err_num_Mask                                                             cBit16_1
#define c_upen_b2ndcfg_cfgb21err_num_Shift                                                                   1

/*--------------------------------------
BitField Name: cfgb21frc_1sen
BitField Type: RW
BitField Desc: Force  B2 at the second position Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_b2ndcfg_cfgb21frc_1sen_Mask                                                               cBit0
#define c_upen_b2ndcfg_cfgb21frc_1sen_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force B2 at the  second position Configuration
Reg Addr   : 0x2100d-0x2100d
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure Force  B2 at the third position for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_b2rdcfg                                                                              0x2100d
#define cReg_upen_b2rdcfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: b22tim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [58:44]
--------------------------------------*/
#define c_upen_b2rdcfg_b22tim_mod_Mask                                                               cBit26_12
#define c_upen_b2rdcfg_b22tim_mod_Shift                                                                     12

/*--------------------------------------
BitField Name: b22err_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [56]
--------------------------------------*/
#define c_upen_b2rdcfg_b22err_clr_Mask                                                                  cBit24
#define c_upen_b2rdcfg_b22err_clr_Shift                                                                     24

/*--------------------------------------
BitField Name: b22err_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [55]
--------------------------------------*/
#define c_upen_b2rdcfg_b22err_set_Mask                                                                  cBit23
#define c_upen_b2rdcfg_b22err_set_Shift                                                                     23

/*--------------------------------------
BitField Name: cfgb22err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force  B2 at the third position
Congiguration
BitField Bits: [54:41]
--------------------------------------*/
#define c_upen_b2rdcfg_cfgb22err_step_Mask                                                            cBit22_9
#define c_upen_b2rdcfg_cfgb22err_step_Shift                                                                  9

/*--------------------------------------
BitField Name: cfgb22msk_pos
BitField Type: RW
BitField Desc: The Position Mask for  B2 at the third position  Congiguration
BitField Bits: [40:25]
--------------------------------------*/
#define c_upen_b2rdcfg_cfgb22msk_pos_Mask_01                                                         cBit31_25
#define c_upen_b2rdcfg_cfgb22msk_pos_Shift_01                                                               25
#define c_upen_b2rdcfg_cfgb22msk_pos_Mask_02                                                           cBit8_0
#define c_upen_b2rdcfg_cfgb22msk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: cfgb22msk_dat
BitField Type: RW
BitField Desc: The Data Mask For B2 at the third position Congiguration
BitField Bits: [24:17]
--------------------------------------*/
#define c_upen_b2rdcfg_cfgb22msk_dat_Mask                                                            cBit24_17
#define c_upen_b2rdcfg_cfgb22msk_dat_Shift                                                                  17

/*--------------------------------------
BitField Name: cfgb22err_num
BitField Type: RW
BitField Desc: The Total B2 at the third position Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_b2rdcfg_cfgb22err_num_Mask                                                             cBit16_1
#define c_upen_b2rdcfg_cfgb22err_num_Shift                                                                   1

/*--------------------------------------
BitField Name: cfgb22frc_1sen
BitField Type: RW
BitField Desc: Force  B2 at the third position Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_b2rdcfg_cfgb22frc_1sen_Mask                                                               cBit0
#define c_upen_b2rdcfg_cfgb22frc_1sen_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force B2 at the first position at STM 64 Rate Configuration
Reg Addr   : 0x21802-0x21802
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure Force  B2 at the first position for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_b2ststm64cfg                                                                              0x21802
#define cReg_upen_b2stcfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: b20stm64tim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [58:44]
--------------------------------------*/
#define c_upen_b2stcfg_b20stm64tim_mod_Mask                                                          cBit26_12
#define c_upen_b2stcfg_b20stm64tim_mod_Shift                                                                12

/*--------------------------------------
BitField Name: b20stm64err_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [56]
--------------------------------------*/
#define c_upen_b2stcfg_b20stm64err_clr_Mask                                                             cBit24
#define c_upen_b2stcfg_b20stm64err_clr_Shift                                                                24

/*--------------------------------------
BitField Name: b20stm64err_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [55]
--------------------------------------*/
#define c_upen_b2stcfg_b20stm64err_set_Mask                                                             cBit23
#define c_upen_b2stcfg_b20stm64err_set_Shift                                                                23

/*--------------------------------------
BitField Name: cfgb20stm64err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force  B2 at the first position at STM
64 Rate Congiguration
BitField Bits: [54:41]
--------------------------------------*/
#define c_upen_b2stcfg_cfgb20stm64err_step_Mask                                                       cBit22_9
#define c_upen_b2stcfg_cfgb20stm64err_step_Shift                                                             9

/*--------------------------------------
BitField Name: cfgb20stm64msk_pos
BitField Type: RW
BitField Desc: The Position Mask for  B2 at the first position at STM 64 Rate
Congiguration
BitField Bits: [40:25]
--------------------------------------*/
#define c_upen_b2stcfg_cfgb20stm64msk_pos_Mask_01                                                    cBit31_25
#define c_upen_b2stcfg_cfgb20stm64msk_pos_Shift_01                                                          25
#define c_upen_b2stcfg_cfgb20stm64msk_pos_Mask_02                                                      cBit8_0
#define c_upen_b2stcfg_cfgb20stm64msk_pos_Shift_02                                                           0

/*--------------------------------------
BitField Name: cfgb20stm64msk_dat
BitField Type: RW
BitField Desc: The Data Mask For B2 at the first position at STM 64 Rate
Congiguration
BitField Bits: [24:17]
--------------------------------------*/
#define c_upen_b2stcfg_cfgb20stm64msk_dat_Mask                                                       cBit24_17
#define c_upen_b2stcfg_cfgb20stm64msk_dat_Shift                                                             17

/*--------------------------------------
BitField Name: cfgb20stm64err_num
BitField Type: RW
BitField Desc: The Total B2 at the first position Error Number at STM 64 Rate
Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_b2stcfg_cfgb20stm64err_num_Mask                                                        cBit16_1
#define c_upen_b2stcfg_cfgb20stm64err_num_Shift                                                              1

/*--------------------------------------
BitField Name: cfgb20stm64frc_1sen
BitField Type: RW
BitField Desc: Force  B2 at the first position Enable at STM 64 Rate
Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_b2stcfg_cfgb20stm64frc_1sen_Mask                                                          cBit0
#define c_upen_b2stcfg_cfgb20stm64frc_1sen_Shift                                                             0

/*------------------------------------------------------------------------------
Reg Name   : Force B2 at the second position at STM 64 Rate Configuration
Reg Addr   : 0x21801-0x21801
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure Force  B2 at the second position for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_b2stm64ndcfg                                                                         0x21801
#define cReg_upen_b2stm64ndcfg_WidthVal                                                                     64

/*--------------------------------------
BitField Name: b21ndm64tim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [58:44]
--------------------------------------*/
#define c_upen_b2stm64ndcfg_b21ndm64tim_mod_Mask                                                     cBit26_12
#define c_upen_b2stm64ndcfg_b21ndm64tim_mod_Shift                                                           12

/*--------------------------------------
BitField Name: b21ndm64err_clr
BitField Type: RW
BitField Desc: Timer B2 at STM 64 Rate is clear
BitField Bits: [56]
--------------------------------------*/
#define c_upen_b2stm64ndcfg_b21ndm64err_clr_Mask                                                        cBit24
#define c_upen_b2stm64ndcfg_b21ndm64err_clr_Shift                                                           24

/*--------------------------------------
BitField Name: b21ndm64err_set
BitField Type: RW
BitField Desc: Timer B2 at STM 64 Rate is set 0: Force Error x: Force Alarm
BitField Bits: [55]
--------------------------------------*/
#define c_upen_b2stm64ndcfg_b21ndm64err_set_Mask                                                        cBit23
#define c_upen_b2stm64ndcfg_b21ndm64err_set_Shift                                                           23

/*--------------------------------------
BitField Name: cfgb21ndstm64err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force  B2 at the second positionat STM
64 Rate  Congiguration
BitField Bits: [54:41]
--------------------------------------*/
#define c_upen_b2stm64ndcfg_cfgb21ndstm64err_step_Mask                                                cBit22_9
#define c_upen_b2stm64ndcfg_cfgb21ndstm64err_step_Shift                                                      9

/*--------------------------------------
BitField Name: cfgb21ndstm64msk_pos
BitField Type: RW
BitField Desc: The Position Mask for  B2 at the second position  at STM 64 Rate
Congiguration
BitField Bits: [40:25]
--------------------------------------*/
#define c_upen_b2stm64ndcfg_cfgb21ndstm64msk_pos_Mask_01                                             cBit31_25
#define c_upen_b2stm64ndcfg_cfgb21ndstm64msk_pos_Shift_01                                                   25
#define c_upen_b2stm64ndcfg_cfgb21ndstm64msk_pos_Mask_02                                               cBit8_0
#define c_upen_b2stm64ndcfg_cfgb21ndstm64msk_pos_Shift_02                                                    0

/*--------------------------------------
BitField Name: cfgb21ndstm64msk_dat
BitField Type: RW
BitField Desc: The Data Mask For B2 at the second position Congiguration
BitField Bits: [24:17]
--------------------------------------*/
#define c_upen_b2stm64ndcfg_cfgb21ndstm64msk_dat_Mask                                                cBit24_17
#define c_upen_b2stm64ndcfg_cfgb21ndstm64msk_dat_Shift                                                      17

/*--------------------------------------
BitField Name: cfgb21ndstm64err_num
BitField Type: RW
BitField Desc: The Total B2 at the second position Error Number at STM 64 Rate
Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_b2stm64ndcfg_cfgb21ndstm64err_num_Mask                                                 cBit16_1
#define c_upen_b2stm64ndcfg_cfgb21ndstm64err_num_Shift                                                       1

/*--------------------------------------
BitField Name: cfgb21ndstm64frc_1sen
BitField Type: RW
BitField Desc: Force  B2 at the second position Enable at STM 64 Rate
Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_b2stm64ndcfg_cfgb21ndstm64frc_1sen_Mask                                                   cBit0
#define c_upen_b2stm64ndcfg_cfgb21ndstm64frc_1sen_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force B2 at the third position at STM 64 Rate Configuration
Reg Addr   : 0x21805-0x21805
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure Force  B2 at the second position for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_b2stm64rdcfg                                                                         0x21805
#define cReg_upen_b2stm64rdcfg_WidthVal                                                                     64

/*--------------------------------------
BitField Name: b21ndm64tim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [58:44]
--------------------------------------*/
#define c_upen_b2stm64rdcfg_b21ndm64tim_mod_Mask                                                     cBit26_12
#define c_upen_b2stm64rdcfg_b21ndm64tim_mod_Shift                                                           12

/*--------------------------------------
BitField Name: b21rdm64err_clr
BitField Type: RW
BitField Desc: Timer the third B2 at STM 64 Rate is clear
BitField Bits: [56]
--------------------------------------*/
#define c_upen_b2stm64rdcfg_b21rdm64err_clr_Mask                                                        cBit24
#define c_upen_b2stm64rdcfg_b21rdm64err_clr_Shift                                                           24

/*--------------------------------------
BitField Name: b21rdm64err_set
BitField Type: RW
BitField Desc: Timer the third B2 at STM 64 Rate is set 0: Force Error x: Force
Alarm
BitField Bits: [55]
--------------------------------------*/
#define c_upen_b2stm64rdcfg_b21rdm64err_set_Mask                                                        cBit23
#define c_upen_b2stm64rdcfg_b21rdm64err_set_Shift                                                           23

/*--------------------------------------
BitField Name: cfgb21rdstm64err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force the third B2 at the second
positionat STM 64 Rate  Congiguration
BitField Bits: [54:41]
--------------------------------------*/
#define c_upen_b2stm64rdcfg_cfgb21rdstm64err_step_Mask                                                cBit22_9
#define c_upen_b2stm64rdcfg_cfgb21rdstm64err_step_Shift                                                      9

/*--------------------------------------
BitField Name: cfgb21rdstm64msk_pos
BitField Type: RW
BitField Desc: The Position Mask for the third B2 at the second position  at STM
64 Rate Congiguration
BitField Bits: [40:25]
--------------------------------------*/
#define c_upen_b2stm64rdcfg_cfgb21rdstm64msk_pos_Mask_01                                             cBit31_25
#define c_upen_b2stm64rdcfg_cfgb21rdstm64msk_pos_Shift_01                                                   25
#define c_upen_b2stm64rdcfg_cfgb21rdstm64msk_pos_Mask_02                                               cBit8_0
#define c_upen_b2stm64rdcfg_cfgb21rdstm64msk_pos_Shift_02                                                    0

/*--------------------------------------
BitField Name: cfgb21rdstm64msk_dat
BitField Type: RW
BitField Desc: The Data Mask For the third B2 at the second position
Congiguration
BitField Bits: [24:17]
--------------------------------------*/
#define c_upen_b2stm64rdcfg_cfgb21rdstm64msk_dat_Mask                                                cBit24_17
#define c_upen_b2stm64rdcfg_cfgb21rdstm64msk_dat_Shift                                                      17

/*--------------------------------------
BitField Name: cfgb21rdstm64err_num
BitField Type: RW
BitField Desc: The Total the third B2 at the second position Error Number at STM
64 Rate Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_b2stm64rdcfg_cfgb21rdstm64err_num_Mask                                                 cBit16_1
#define c_upen_b2stm64rdcfg_cfgb21rdstm64err_num_Shift                                                       1

/*--------------------------------------
BitField Name: cfgb21rdstm64frc_1sen
BitField Type: RW
BitField Desc: Force the third B2 at the second position Enable at STM 64 Rate
Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_b2stm64rdcfg_cfgb21rdstm64frc_1sen_Mask                                                   cBit0
#define c_upen_b2stm64rdcfg_cfgb21rdstm64frc_1sen_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer VT Error Configuration
Reg Addr   : 0xa0000-0xa05FF
Reg Formula: 0xa0000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force Loss Of Pointer VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_LOPvtcfg                                                                             0xa0000
#define cReg_upen_LOPvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: lopvttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvttim_mod_Mask                                                            cBit27_26
#define c_upen_LOPvtcfg_lopvttim_mod_Shift                                                                  26

/*--------------------------------------
BitField Name: lopvterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvterr_clr_Mask                                                            cBit25_18
#define c_upen_LOPvtcfg_lopvterr_clr_Shift                                                                  18

/*--------------------------------------
BitField Name: lopvterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvterr_set_Mask                                                            cBit17_10
#define c_upen_LOPvtcfg_lopvterr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: lopvterr_step
BitField Type: RW
BitField Desc: The Total Step to Force LOP VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvterr_step_Mask                                                             cBit9_2
#define c_upen_LOPvtcfg_lopvterr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: lopvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOP VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvtmsk_pos_Mask_01                                                         cBit31_18
#define c_upen_LOPvtcfg_lopvtmsk_pos_Shift_01                                                               18
#define c_upen_LOPvtcfg_lopvtmsk_pos_Mask_02                                                           cBit1_0
#define c_upen_LOPvtcfg_lopvtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: lopvtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOP VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvtmsk_ena_Mask                                                               cBit17
#define c_upen_LOPvtcfg_lopvtmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: lopvterr_num
BitField Type: RW
BitField Desc: The Total LOP VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvterr_num_Mask                                                             cBit16_1
#define c_upen_LOPvtcfg_lopvterr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: lopvtfrc_mod
BitField Type: RW
BitField Desc: Force LOP VT Mode 0: One shot ( N events in T the unit time) 1:
continous (lopvterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvtfrc_mod_Mask                                                                cBit0
#define c_upen_LOPvtcfg_lopvtfrc_mod_Shift                                                                   0

/*------------------------------------------------------------------------------
Reg Name   : Force AIS VT Error Configuration
Reg Addr   : 0xa0800-0xa0CFF
Reg Formula: 0xa0800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force AIS VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_AISvtcfg                                                                             0xa0800
#define cReg_upen_AISvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: aisvttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvttim_mod_Mask                                                            cBit27_26
#define c_upen_AISvtcfg_aisvttim_mod_Shift                                                                  26

/*--------------------------------------
BitField Name: aisvterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvterr_clr_Mask                                                            cBit25_18
#define c_upen_AISvtcfg_aisvterr_clr_Shift                                                                  18

/*--------------------------------------
BitField Name: aisvterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvterr_set_Mask                                                            cBit17_10
#define c_upen_AISvtcfg_aisvterr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: aisvterr_step
BitField Type: RW
BitField Desc: The Total Step to Force AIS VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvterr_step_Mask                                                             cBit9_2
#define c_upen_AISvtcfg_aisvterr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: aisvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for AIS VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvtmsk_pos_Mask_01                                                         cBit31_18
#define c_upen_AISvtcfg_aisvtmsk_pos_Shift_01                                                               18
#define c_upen_AISvtcfg_aisvtmsk_pos_Mask_02                                                           cBit1_0
#define c_upen_AISvtcfg_aisvtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: aisvtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For AIS VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvtmsk_ena_Mask                                                               cBit17
#define c_upen_AISvtcfg_aisvtmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: aisvterr_num
BitField Type: RW
BitField Desc: The Total AIS VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvterr_num_Mask                                                             cBit16_1
#define c_upen_AISvtcfg_aisvterr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: aisvtfrc_mod
BitField Type: RW
BitField Desc: Force AIS VT Mode 0: One shot ( N events in T the unit time) 1:
continous (aisvterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvtfrc_mod_Mask                                                                cBit0
#define c_upen_AISvtcfg_aisvtfrc_mod_Shift                                                                   0

/*------------------------------------------------------------------------------
Reg Name   : Force UEQ VT Error Configuration
Reg Addr   : 0xa1000-0xa15FF
Reg Formula: 0xa1000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force UEQ VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_UEQvtcfg                                                                             0xa1000
#define cReg_upen_UEQvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: ueqvttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvttim_mod_Mask                                                            cBit27_26
#define c_upen_UEQvtcfg_ueqvttim_mod_Shift                                                                  26

/*--------------------------------------
BitField Name: ueqvterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvterr_clr_Mask                                                            cBit25_18
#define c_upen_UEQvtcfg_ueqvterr_clr_Shift                                                                  18

/*--------------------------------------
BitField Name: ueqvterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvterr_set_Mask                                                            cBit17_10
#define c_upen_UEQvtcfg_ueqvterr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: ueqvterr_step
BitField Type: RW
BitField Desc: The Total Step to Force UEQ VT Congiguration
 --------------------------------------*/
#define c_upen_UEQvtcfg_ueqvterr_step_Mask                                                             cBit9_2
#define c_upen_UEQvtcfg_ueqvterr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: ueqvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for UEQ VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvtmsk_pos_Mask_01                                                         cBit31_18
#define c_upen_UEQvtcfg_ueqvtmsk_pos_Shift_01                                                               18
#define c_upen_UEQvtcfg_ueqvtmsk_pos_Mask_02                                                           cBit1_0
#define c_upen_UEQvtcfg_ueqvtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: ueqvtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For UEQ VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvtmsk_ena_Mask                                                               cBit17
#define c_upen_UEQvtcfg_ueqvtmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: ueqvterr_num
BitField Type: RW
BitField Desc: The Total UEQ VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvterr_num_Mask                                                             cBit16_1
#define c_upen_UEQvtcfg_ueqvterr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: ueqvtfrc_mod
BitField Type: RW
BitField Desc: Force UEQ VT Mode 0: One shot ( N events in T the unit time) 1:
continous (ueqvterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvtfrc_mod_Mask                                                                cBit0
#define c_upen_UEQvtcfg_ueqvtfrc_mod_Shift                                                                   0

/*------------------------------------------------------------------------------
Reg Name   : Force BIP VT Error Configuration
Reg Addr   : 0xa1800-0xa1CFF
Reg Formula: 0xa1800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force BIP VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_BIPvtcfg                                                                             0xa1800
#define cReg_upen_BIPvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: bipvttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvttim_mod_Mask                                                            cBit13_12
#define c_upen_BIPvtcfg_bipvttim_mod_Shift                                                                  12

/*--------------------------------------
BitField Name: bipvterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvterr_clr_Mask                                                               cBit11
#define c_upen_BIPvtcfg_bipvterr_clr_Shift                                                                  11

/*--------------------------------------
BitField Name: bipvterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvterr_set_Mask                                                               cBit10
#define c_upen_BIPvtcfg_bipvterr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: bipvterr_step
BitField Type: RW
BitField Desc: The Total Step to Force BIP at VT  Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvterr_step_Mask                                                             cBit9_2
#define c_upen_BIPvtcfg_bipvterr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: bipvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for BIP at VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvtmsk_pos_Mask_01                                                         cBit31_18
#define c_upen_BIPvtcfg_bipvtmsk_pos_Shift_01                                                               18
#define c_upen_BIPvtcfg_bipvtmsk_pos_Mask_02                                                           cBit1_0
#define c_upen_BIPvtcfg_bipvtmsk_pos_Shift_02                                                                0



/*--------------------------------------
BitField Name: bipvtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For BIP at VT1 Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvtmsk_ena_Mask                                                               cBit17
#define c_upen_BIPvtcfg_bipvtmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: bipvterr_num
BitField Type: RW
BitField Desc: The Total BIP at VT Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvterr_num_Mask                                                             cBit16_1
#define c_upen_BIPvtcfg_bipvterr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: bipvtfrc_mod
BitField Type: RW
BitField Desc: Force BIP at VT Mode 0: One shot 1: continous (bipvterr_num = 16
bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvtfrc_mod_Mask                                                                cBit0
#define c_upen_BIPvtcfg_bipvtfrc_mod_Shift                                                                   0

/*------------------------------------------------------------------------------
Reg Name   : Force REI VT Error Configuration
Reg Addr   : 0xa2000-0xa25FF
Reg Formula: 0xa2000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force REI VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_REIvtcfg                                                                             0xa2000
#define cReg_upen_REIvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: reivttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_REIvtcfg_reivttim_mod_Mask                                                            cBit13_12
#define c_upen_REIvtcfg_reivttim_mod_Shift                                                                  12

/*--------------------------------------
BitField Name: reivterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_REIvtcfg_reivterr_clr_Mask                                                               cBit11
#define c_upen_REIvtcfg_reivterr_clr_Shift                                                                  11

/*--------------------------------------
BitField Name: reivterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_REIvtcfg_reivterr_set_Mask                                                               cBit10
#define c_upen_REIvtcfg_reivterr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: reivterr_step
BitField Type: RW
BitField Desc: The Total Step to Force REI at VT  Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_REIvtcfg_reivterr_step_Mask                                                             cBit9_2
#define c_upen_REIvtcfg_reivterr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: reivtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for REI at VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_REIvtcfg_reivtmsk_pos_Mask_01                                                         cBit31_18
#define c_upen_REIvtcfg_reivtmsk_pos_Shift_01                                                               18
#define c_upen_REIvtcfg_reivtmsk_pos_Mask_02                                                           cBit1_0
#define c_upen_REIvtcfg_reivtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: reivtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For REI at VT1 Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_REIvtcfg_reivtmsk_ena_Mask                                                               cBit17
#define c_upen_REIvtcfg_reivtmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: reivterr_num
BitField Type: RW
BitField Desc: The Total REI at VT Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_REIvtcfg_reivterr_num_Mask                                                             cBit16_1
#define c_upen_REIvtcfg_reivterr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: reivtfrc_mod
BitField Type: RW
BitField Desc: Force REI at VT Mode 0: One shot 1: continous (reivterr_num = 16
bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIvtcfg_reivtfrc_mod_Mask                                                                cBit0
#define c_upen_REIvtcfg_reivtfrc_mod_Shift                                                                   0

/*------------------------------------------------------------------------------
Reg Name   : Force RDI VT Error Configuration
Reg Addr   : 0xa2800-0xa2CFF
Reg Formula: 0xa2800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force RDI VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_RDIvtcfg                                                                             0xa2800

/*--------------------------------------
BitField Name: v5vtvalue
BitField Type: RW
BitField Desc: bit 8 of v5
BitField Bits: [63]
--------------------------------------*/
#define c_upen_RDIvtcfg_v5vtvalue_Mask                                                                  cBit31
#define c_upen_RDIvtcfg_v5vtvalue_Shift                                                                     31

/*--------------------------------------
BitField Name: k4vtvalue
BitField Type: RW
BitField Desc: bit 5 6 7 of K4
BitField Bits: [62:60]
--------------------------------------*/
#define c_upen_RDIvtcfg_k4vtvalue_Mask                                                               cBit30_28
#define c_upen_RDIvtcfg_k4vtvalue_Shift                                                                     28

/*--------------------------------------
BitField Name: rdivttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivttim_mod_Mask                                                            cBit27_26
#define c_upen_RDIvtcfg_rdivttim_mod_Shift                                                                  26

/*--------------------------------------
BitField Name: rdivterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivterr_clr_Mask                                                            cBit25_18
#define c_upen_RDIvtcfg_rdivterr_clr_Shift                                                                  18

/*--------------------------------------
BitField Name: rdivterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivterr_set_Mask                                                            cBit17_10
#define c_upen_RDIvtcfg_rdivterr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: rdivterr_step
BitField Type: RW
BitField Desc: The Total Step to Force RDI VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivterr_step_Mask                                                             cBit9_2
#define c_upen_RDIvtcfg_rdivterr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: rdivtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for RDI VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivtmsk_pos_Mask_01                                                         cBit31_18
#define c_upen_RDIvtcfg_rdivtmsk_pos_Shift_01                                                               18
#define c_upen_RDIvtcfg_rdivtmsk_pos_Mask_02                                                           cBit1_0
#define c_upen_RDIvtcfg_rdivtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: rdivtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For RDI VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivtmsk_ena_Mask                                                               cBit17
#define c_upen_RDIvtcfg_rdivtmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: rdivterr_num
BitField Type: RW
BitField Desc: The Total RDI VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivterr_num_Mask                                                             cBit16_1
#define c_upen_RDIvtcfg_rdivterr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: rdivtfrc_mod
BitField Type: RW
BitField Desc: Force RDI VT Mode 0: One shot ( N events in T the unit time) 1:
continous (rdivterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivtfrc_mod_Mask                                                                cBit0
#define c_upen_RDIvtcfg_rdivtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Configuration the unit time to force in VT
Reg Addr   : 0xa380f-0xa380f
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure n second in  VT

------------------------------------------------------------------------------*/
#define cReg_upennsecvt                                                                                0xa380f

/*--------------------------------------
BitField Name: cfgmsecvt
BitField Type: RW
BitField Desc: Configuration the unit time  in VT Congiguration
BitField Bits: [31:0]
--------------------------------------*/
#define c_upennsecvt_cfgmsecvt_Mask                                                                   cBit31_0
#define c_upennsecvt_cfgmsecvt_Shift                                                                         0

/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer STS Error Configuration
Reg Addr   : 0x28000-0x2802F
Reg Formula: 0x28000 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force Loss Of Pointer STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_LOPstscfg                                                                            0x28000
#define cReg_upen_LOPstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: lopststim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_LOPstscfg_lopststim_mod_Mask                                                          cBit27_26
#define c_upen_LOPstscfg_lopststim_mod_Shift                                                                26

/*--------------------------------------
BitField Name: lopstserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstserr_clr_Mask                                                          cBit25_18
#define c_upen_LOPstscfg_lopstserr_clr_Shift                                                                18

/*--------------------------------------
BitField Name: lopstserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstserr_set_Mask                                                          cBit17_10
#define c_upen_LOPstscfg_lopstserr_set_Shift                                                                10

/*--------------------------------------
BitField Name: lopstserr_step
BitField Type: RW
BitField Desc: The Total Step to Force LOP STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstserr_step_Mask                                                           cBit9_2
#define c_upen_LOPstscfg_lopstserr_step_Shift                                                                2

/*--------------------------------------
BitField Name: lopstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOP STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstsmsk_pos_Mask_01                                                       cBit31_18
#define c_upen_LOPstscfg_lopstsmsk_pos_Shift_01                                                             18
#define c_upen_LOPstscfg_lopstsmsk_pos_Mask_02                                                         cBit1_0
#define c_upen_LOPstscfg_lopstsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: lopstsmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOP STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstsmsk_ena_Mask                                                             cBit17
#define c_upen_LOPstscfg_lopstsmsk_ena_Shift                                                                17

/*--------------------------------------
BitField Name: lopstserr_num
BitField Type: RW
BitField Desc: The Total LOP STS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstserr_num_Mask                                                           cBit16_1
#define c_upen_LOPstscfg_lopstserr_num_Shift                                                                 1

/*--------------------------------------
BitField Name: lopstsfrc_mod
BitField Type: RW
BitField Desc: Force LOP STS Mode 0: One shot ( N events in T the unit time) 1:
continous (lopstserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstsfrc_mod_Mask                                                              cBit0
#define c_upen_LOPstscfg_lopstsfrc_mod_Shift                                                                 0

/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer STS STS Error Status
Reg Addr   : 0x28040-0x2806F
Reg Formula: 0x28040 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force Loss Of Pointer STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_LOPstssta                                                                            0x28040
#define cReg_upen_LOPstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: lopstssta_almset
BitField Type: RW
BitField Desc: LOP STS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_LOPstssta_lopstssta_almset_Mask                                                          cBit27
#define c_upen_LOPstssta_lopstssta_almset_Shift                                                             27

/*--------------------------------------
BitField Name: lopstsstaclralrm
BitField Type: RW
BitField Desc: LOP STS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstaclralrm_Mask                                                       cBit22_15
#define c_upen_LOPstssta_lopstsstaclralrm_Shift                                                             15

/*--------------------------------------
BitField Name: lopstsstasetalrm
BitField Type: RW
BitField Desc: LOP STS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstasetalrm_Mask                                                        cBit14_7
#define c_upen_LOPstssta_lopstsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: lopstsstatimecnt
BitField Type: RW
BitField Desc: LOP STS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstatimecnt_Mask                                                         cBit6_0
#define c_upen_LOPstssta_lopstsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: lopstsstafrc_ena
BitField Type: RW
BitField Desc: LOP STS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstafrc_ena_Mask                                                          cBit31
#define c_upen_LOPstssta_lopstsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: lopstsstaerrstep
BitField Type: RW
BitField Desc: The Second LOP STS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstaerrstep_Mask                                                       cBit30_23
#define c_upen_LOPstssta_lopstsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: lopstsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask LOP STS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_LOPstssta_lopstsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: lopstsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error LOP STS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_LOPstssta_lopstsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: lopstsstafrc_sta
BitField Type: RW
BitField Desc: LOP STS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_LOPstssta_lopstsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: lopstsstaoneunit
BitField Type: RW
BitField Desc: Force LOP STS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstaoneunit_Mask                                                           cBit0
#define c_upen_LOPstssta_lopstsstaoneunit_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS STS Error Configuration
Reg Addr   : 0x28080-0x280aF
Reg Formula: 0x28080 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force AIS STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_AISstscfg                                                                            0x28080
#define cReg_upen_AISstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: aisststim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_AISstscfg_aisststim_mod_Mask                                                          cBit27_26
#define c_upen_AISstscfg_aisststim_mod_Shift                                                                26

/*--------------------------------------
BitField Name: aisstserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_AISstscfg_aisstserr_clr_Mask                                                          cBit25_18
#define c_upen_AISstscfg_aisstserr_clr_Shift                                                                18

/*--------------------------------------
BitField Name: aisstserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_AISstscfg_aisstserr_set_Mask                                                          cBit17_10
#define c_upen_AISstscfg_aisstserr_set_Shift                                                                10

/*--------------------------------------
BitField Name: aisstserr_step
BitField Type: RW
BitField Desc: The Total Step to Force AIS STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_AISstscfg_aisstserr_step_Mask                                                           cBit9_2
#define c_upen_AISstscfg_aisstserr_step_Shift                                                                2

/*--------------------------------------
BitField Name: aisstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for AIS STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_AISstscfg_aisstsmsk_pos_Mask_01                                                       cBit31_18
#define c_upen_AISstscfg_aisstsmsk_pos_Shift_01                                                             18
#define c_upen_AISstscfg_aisstsmsk_pos_Mask_02                                                         cBit1_0
#define c_upen_AISstscfg_aisstsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: aisstsmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For AIS STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_AISstscfg_aisstsmsk_ena_Mask                                                             cBit17
#define c_upen_AISstscfg_aisstsmsk_ena_Shift                                                                17

/*--------------------------------------
BitField Name: aisstserr_num
BitField Type: RW
BitField Desc: The Total AIS STS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_AISstscfg_aisstserr_num_Mask                                                           cBit16_1
#define c_upen_AISstscfg_aisstserr_num_Shift                                                                 1

/*--------------------------------------
BitField Name: aisstsfrc_mod
BitField Type: RW
BitField Desc: Force AIS STS Mode 0: One shot ( N events in T the unit time) 1:
continous (aisstserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_AISstscfg_aisstsfrc_mod_Mask                                                              cBit0
#define c_upen_AISstscfg_aisstsfrc_mod_Shift                                                                 0



/*------------------------------------------------------------------------------
Reg Name   : Force AIS STS Error Status
Reg Addr   : 0x280C0-0x280eF
Reg Formula: 0x280C0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force AIS STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_AISstssta                                                                            0x280C0
#define cReg_upen_AISstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: aisstssta_almset
BitField Type: RW
BitField Desc: AIS STS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_AISstssta_aisstssta_almset_Mask                                                          cBit27
#define c_upen_AISstssta_aisstssta_almset_Shift                                                             27

/*--------------------------------------
BitField Name: aisstsstaclralrm
BitField Type: RW
BitField Desc: AIS STS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstaclralrm_Mask                                                       cBit22_15
#define c_upen_AISstssta_aisstsstaclralrm_Shift                                                             15

/*--------------------------------------
BitField Name: aisstsstasetalrm
BitField Type: RW
BitField Desc: AIS STS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstasetalrm_Mask                                                        cBit14_7
#define c_upen_AISstssta_aisstsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: aisstsstatimecnt
BitField Type: RW
BitField Desc: AIS STS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstatimecnt_Mask                                                         cBit6_0
#define c_upen_AISstssta_aisstsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: aisstsstafrc_ena
BitField Type: RW
BitField Desc: AIS STS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstafrc_ena_Mask                                                          cBit31
#define c_upen_AISstssta_aisstsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: aisstsstaerrstep
BitField Type: RW
BitField Desc: The Second AIS STS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstaerrstep_Mask                                                       cBit30_23
#define c_upen_AISstssta_aisstsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: aisstsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask AIS STS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_AISstssta_aisstsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: aisstsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error AIS STS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_AISstssta_aisstsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: aisstsstafrc_sta
BitField Type: RW
BitField Desc: AIS STS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_AISstssta_aisstsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: aisstsstaoneunit
BitField Type: RW
BitField Desc: Force AIS STS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstaoneunit_Mask                                                           cBit0
#define c_upen_AISstssta_aisstsstaoneunit_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Force UEQ STS Error Configuration
Reg Addr   : 0x28100-0x2812F
Reg Formula: 0x28100 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force UEQ STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_UEQstscfg                                                                            0x28100
#define cReg_upen_UEQstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: ueqststim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqststim_mod_Mask                                                          cBit27_26
#define c_upen_UEQstscfg_ueqststim_mod_Shift                                                                26

/*--------------------------------------
BitField Name: ueqstserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstserr_clr_Mask                                                          cBit25_18
#define c_upen_UEQstscfg_ueqstserr_clr_Shift                                                                18

/*--------------------------------------
BitField Name: ueqstserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstserr_set_Mask                                                          cBit17_10
#define c_upen_UEQstscfg_ueqstserr_set_Shift                                                                10

/*--------------------------------------
BitField Name: ueqstserr_step
BitField Type: RW
BitField Desc: The Total Step to Force UEQ STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstserr_step_Mask                                                           cBit9_2
#define c_upen_UEQstscfg_ueqstserr_step_Shift                                                                2

/*--------------------------------------
BitField Name: ueqstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for UEQ STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstsmsk_pos_Mask_01                                                       cBit31_18
#define c_upen_UEQstscfg_ueqstsmsk_pos_Shift_01                                                             18
#define c_upen_UEQstscfg_ueqstsmsk_pos_Mask_02                                                         cBit1_0
#define c_upen_UEQstscfg_ueqstsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: ueqstsmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For UEQ STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstsmsk_ena_Mask                                                             cBit17
#define c_upen_UEQstscfg_ueqstsmsk_ena_Shift                                                                17

/*--------------------------------------
BitField Name: ueqstserr_num
BitField Type: RW
BitField Desc: The Total UEQ STS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstserr_num_Mask                                                           cBit16_1
#define c_upen_UEQstscfg_ueqstserr_num_Shift                                                                 1

/*--------------------------------------
BitField Name: ueqstsfrc_mod
BitField Type: RW
BitField Desc: Force UEQ STS Mode 0: One shot ( N events in T the unit time) 1:
continous (ueqstserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstsfrc_mod_Mask                                                              cBit0
#define c_upen_UEQstscfg_ueqstsfrc_mod_Shift                                                                 0

/*------------------------------------------------------------------------------
Reg Name   : Force UEQ STS Error Status
Reg Addr   : 0x28140-0x2816F
Reg Formula: 0x28140 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force UEQ STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_UEQstssta                                                                            0x28140
#define cReg_upen_UEQstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: aisstssta_almset
BitField Type: RW
BitField Desc: AIS STS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_UEQstssta_aisstssta_almset_Mask                                                          cBit27
#define c_upen_UEQstssta_aisstssta_almset_Shift                                                             27

/*--------------------------------------
BitField Name: aisstsstaclralrm
BitField Type: RW
BitField Desc: AIS STS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstaclralrm_Mask                                                       cBit22_15
#define c_upen_UEQstssta_aisstsstaclralrm_Shift                                                             15

/*--------------------------------------
BitField Name: aisstsstasetalrm
BitField Type: RW
BitField Desc: AIS STS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstasetalrm_Mask                                                        cBit14_7
#define c_upen_UEQstssta_aisstsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: aisstsstatimecnt
BitField Type: RW
BitField Desc: AIS STS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstatimecnt_Mask                                                         cBit6_0
#define c_upen_UEQstssta_aisstsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: aisstsstafrc_ena
BitField Type: RW
BitField Desc: AIS STS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstafrc_ena_Mask                                                          cBit31
#define c_upen_UEQstssta_aisstsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: aisstsstaerrstep
BitField Type: RW
BitField Desc: The Second AIS STS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstaerrstep_Mask                                                       cBit30_23
#define c_upen_UEQstssta_aisstsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: aisstsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask AIS STS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_UEQstssta_aisstsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: aisstsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error AIS STS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_UEQstssta_aisstsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: aisstsstafrc_sta
BitField Type: RW
BitField Desc: AIS STS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_UEQstssta_aisstsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: aisstsstaoneunit
BitField Type: RW
BitField Desc: Force AIS STS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstaoneunit_Mask                                                           cBit0
#define c_upen_UEQstssta_aisstsstaoneunit_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP STS Error Configuration
Reg Addr   : 0x28180-0x281aF
Reg Formula: 0x28180 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force BIP STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_BIPstscfg                                                                            0x28180
#define cReg_upen_BIPstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: bipststim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_BIPstscfg_bipststim_mod_Mask                                                          cBit13_12
#define c_upen_BIPstscfg_bipststim_mod_Shift                                                                12

/*--------------------------------------
BitField Name: bipstserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstserr_clr_Mask                                                             cBit11
#define c_upen_BIPstscfg_bipstserr_clr_Shift                                                                11

/*--------------------------------------
BitField Name: bipstserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstserr_set_Mask                                                             cBit10
#define c_upen_BIPstscfg_bipstserr_set_Shift                                                                10

/*--------------------------------------
BitField Name: bipstserr_step
BitField Type: RW
BitField Desc: The Total Step to Force BIP STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstserr_step_Mask                                                           cBit9_2
#define c_upen_BIPstscfg_bipstserr_step_Shift                                                                2

/*--------------------------------------
BitField Name: bipstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for BIP STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstsmsk_pos_Mask_01                                                       cBit31_18
#define c_upen_BIPstscfg_bipstsmsk_pos_Shift_01                                                             18
#define c_upen_BIPstscfg_bipstsmsk_pos_Mask_02                                                         cBit1_0
#define c_upen_BIPstscfg_bipstsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: bipstsmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For BIP STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstsmsk_ena_Mask                                                             cBit17
#define c_upen_BIPstscfg_bipstsmsk_ena_Shift                                                                17

/*--------------------------------------
BitField Name: bipstserr_num
BitField Type: RW
BitField Desc: The Total BIP STS Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstserr_num_Mask                                                           cBit16_1
#define c_upen_BIPstscfg_bipstserr_num_Shift                                                                 1

/*--------------------------------------
BitField Name: bipstsfrc_mod
BitField Type: RW
BitField Desc: Force BIP STS Mode 0: One shot 1: continous (bipstserr_num = 16
bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstsfrc_mod_Mask                                                              cBit0
#define c_upen_BIPstscfg_bipstsfrc_mod_Shift                                                                 0

/*------------------------------------------------------------------------------
Reg Name   : Force BIP STS Error Status
Reg Addr   : 0x281c0-0x281eF
Reg Formula: 0x281c0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force BIP STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_BIPstssta                                                                            0x281c0
#define cReg_upen_BIPstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: reistssta_almset
BitField Type: RW
BitField Desc: REI STS alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_BIPstssta_reistssta_almset_Mask                                                           cBit9
#define c_upen_BIPstssta_reistssta_almset_Shift                                                              9

/*--------------------------------------
BitField Name: reistsstaclralrm
BitField Type: RW
BitField Desc: REI STS clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstaclralrm_Mask                                                           cBit8
#define c_upen_BIPstssta_reistsstaclralrm_Shift                                                              8

/*--------------------------------------
BitField Name: reistsstasetalrm
BitField Type: RW
BitField Desc: REI STS Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstasetalrm_Mask                                                           cBit7
#define c_upen_BIPstssta_reistsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: reistsstatimecnt
BitField Type: RW
BitField Desc: REI STS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstatimecnt_Mask                                                         cBit6_0
#define c_upen_BIPstssta_reistsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: reistsstafrc_ena
BitField Type: RW
BitField Desc: REI STS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstafrc_ena_Mask                                                          cBit31
#define c_upen_BIPstssta_reistsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: reistsstaerrstep
BitField Type: RW
BitField Desc: The Second REI STS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstaerrstep_Mask                                                       cBit30_23
#define c_upen_BIPstssta_reistsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: reistsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask REI STS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_BIPstssta_reistsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: reistsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error REI STS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_BIPstssta_reistsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: reistsstafrc_sta
BitField Type: RW
BitField Desc: REI STS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_BIPstssta_reistsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: reistsstaoneunit
BitField Type: RW
BitField Desc: Force REI STS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstaoneunit_Mask                                                           cBit0
#define c_upen_BIPstssta_reistsstaoneunit_Shift                                                              0



/*------------------------------------------------------------------------------
Reg Name   : Force REI STS Error Configuration
Reg Addr   : 0x28200-0x2822f
Reg Formula: 0x28200 + 512*SliceId + StsId
    Where  :
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   :
This register is applicable to configure Force REI STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_REIstscfg                                                                            0x28200
#define cReg_upen_REIstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: bipststim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_REIstscfg_bipststim_mod_Mask                                                          cBit13_12
#define c_upen_REIstscfg_bipststim_mod_Shift                                                                12

/*--------------------------------------
BitField Name: bipstserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_REIstscfg_bipstserr_clr_Mask                                                             cBit11
#define c_upen_REIstscfg_bipstserr_clr_Shift                                                                11

/*--------------------------------------
BitField Name: bipstserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_REIstscfg_bipstserr_set_Mask                                                             cBit10
#define c_upen_REIstscfg_bipstserr_set_Shift                                                                10

/*--------------------------------------
BitField Name: bipstserr_step
BitField Type: RW
BitField Desc: The Total Step to Force BIP STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_REIstscfg_bipstserr_step_Mask                                                           cBit9_2
#define c_upen_REIstscfg_bipstserr_step_Shift                                                                2

/*--------------------------------------
BitField Name: bipstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for BIP STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_REIstscfg_bipstsmsk_pos_Mask_01                                                       cBit31_18
#define c_upen_REIstscfg_bipstsmsk_pos_Shift_01                                                             18
#define c_upen_REIstscfg_bipstsmsk_pos_Mask_02                                                         cBit1_0
#define c_upen_REIstscfg_bipstsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: bipstsmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For BIP STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_REIstscfg_bipstsmsk_ena_Mask                                                             cBit17
#define c_upen_REIstscfg_bipstsmsk_ena_Shift                                                                17

/*--------------------------------------
BitField Name: bipstserr_num
BitField Type: RW
BitField Desc: The Total BIP STS Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_REIstscfg_bipstserr_num_Mask                                                           cBit16_1
#define c_upen_REIstscfg_bipstserr_num_Shift                                                                 1

/*--------------------------------------
BitField Name: bipstsfrc_mod
BitField Type: RW
BitField Desc: Force BIP STS Mode 0: One shot 1: continous (bipstserr_num = 16
bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIstscfg_bipstsfrc_mod_Mask                                                              cBit0
#define c_upen_REIstscfg_bipstsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force REI STS Error Status
Reg Addr   : 0x28240-0x2826F
Reg Formula: 0x28240 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force REI STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_REIstssta                                                                            0x28240
#define cReg_upen_REIstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: reistssta_almset
BitField Type: RW
BitField Desc: REISTS alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_REIstssta_reistssta_almset_Mask                                                           cBit9
#define c_upen_REIstssta_reistssta_almset_Shift                                                              9

/*--------------------------------------
BitField Name: reistsstaclralrm
BitField Type: RW
BitField Desc: REISTS clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_REIstssta_reistsstaclralrm_Mask                                                           cBit8
#define c_upen_REIstssta_reistsstaclralrm_Shift                                                              8

/*--------------------------------------
BitField Name: reistsstasetalrm
BitField Type: RW
BitField Desc: REISTS Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_REIstssta_reistsstasetalrm_Mask                                                           cBit7
#define c_upen_REIstssta_reistsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: reistsstatimecnt
BitField Type: RW
BitField Desc: REISTS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_REIstssta_reistsstatimecnt_Mask                                                         cBit6_0
#define c_upen_REIstssta_reistsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: reistsstafrc_ena
BitField Type: RW
BitField Desc: REISTS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_REIstssta_reistsstafrc_ena_Mask                                                          cBit31
#define c_upen_REIstssta_reistsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: reistsstaerrstep
BitField Type: RW
BitField Desc: The Second REISTS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_REIstssta_reistsstaerrstep_Mask                                                       cBit30_23
#define c_upen_REIstssta_reistsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: reistsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask REISTS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_REIstssta_reistsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_REIstssta_reistsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: reistsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error REISTS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_REIstssta_reistsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_REIstssta_reistsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: reistsstafrc_sta
BitField Type: RW
BitField Desc: REISTS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_REIstssta_reistsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_REIstssta_reistsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: reistsstaoneunit
BitField Type: RW
BitField Desc: Force REISTS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIstssta_reistsstaoneunit_Mask                                                           cBit0
#define c_upen_REIstssta_reistsstaoneunit_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI STS Error Configuration
Reg Addr   : 0x28280-0x282aF
Reg Formula: 0x28280 + 2048*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force RDI STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_RDIstscfg                                                                            0x28280

/*--------------------------------------
BitField Name: g1vtvalue
BitField Type: RW
BitField Desc: bit 5 6 7 of G1
BitField Bits: [62:60]
--------------------------------------*/
#define c_upen_RDIstscfg_g1vtvalue_Mask                                                              cBit30_28
#define c_upen_RDIstscfg_g1vtvalue_Shift                                                                    28

/*--------------------------------------
BitField Name: rdivttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivttim_mod_Mask                                                           cBit27_26
#define c_upen_RDIstscfg_rdivttim_mod_Shift                                                                 26

/*--------------------------------------
BitField Name: rdivterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivterr_clr_Mask                                                           cBit25_18
#define c_upen_RDIstscfg_rdivterr_clr_Shift                                                                 18

/*--------------------------------------
BitField Name: rdivterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivterr_set_Mask                                                           cBit17_10
#define c_upen_RDIstscfg_rdivterr_set_Shift                                                                 10

/*--------------------------------------
BitField Name: rdivterr_step
BitField Type: RW
BitField Desc: The Total Step to Force RDI VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivterr_step_Mask                                                            cBit9_2
#define c_upen_RDIstscfg_rdivterr_step_Shift                                                                 2

/*--------------------------------------
BitField Name: rdivtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for RDI VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivtmsk_pos_Mask_01                                                        cBit31_18
#define c_upen_RDIstscfg_rdivtmsk_pos_Shift_01                                                              18
#define c_upen_RDIstscfg_rdivtmsk_pos_Mask_02                                                          cBit1_0
#define c_upen_RDIstscfg_rdivtmsk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: rdivtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For RDI VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivtmsk_ena_Mask                                                              cBit17
#define c_upen_RDIstscfg_rdivtmsk_ena_Shift                                                                 17

/*--------------------------------------
BitField Name: rdivterr_num
BitField Type: RW
BitField Desc: The Total RDI VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivterr_num_Mask                                                            cBit16_1
#define c_upen_RDIstscfg_rdivterr_num_Shift                                                                  1

/*--------------------------------------
BitField Name: rdivtfrc_mod
BitField Type: RW
BitField Desc: Force RDI VT Mode 0: One shot ( N events in T the unit time) 1:
continous (rdivterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivtfrc_mod_Mask                                                               cBit0
#define c_upen_RDIstscfg_rdivtfrc_mod_Shift                                                                  0

/*------------------------------------------------------------------------------
Reg Name   : Force RDI STS Error Status
Reg Addr   : 0x282C0-0x282eF
Reg Formula: 0x282C0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force RDI STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_RDIstssta                                                                            0x282C0
#define cReg_upen_RDIstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: rdistssta_almset
BitField Type: RW
BitField Desc: RDI STS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_RDIstssta_rdistssta_almset_Mask                                                          cBit27
#define c_upen_RDIstssta_rdistssta_almset_Shift                                                             27

/*--------------------------------------
BitField Name: rdistsstaclralrm
BitField Type: RW
BitField Desc: RDI STS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstaclralrm_Mask                                                       cBit22_15
#define c_upen_RDIstssta_rdistsstaclralrm_Shift                                                             15

/*--------------------------------------
BitField Name: rdistsstasetalrm
BitField Type: RW
BitField Desc: RDI STS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstasetalrm_Mask                                                        cBit14_7
#define c_upen_RDIstssta_rdistsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: rdistsstatimecnt
BitField Type: RW
BitField Desc: RDI STS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstatimecnt_Mask                                                         cBit6_0
#define c_upen_RDIstssta_rdistsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: rdistsstafrc_ena
BitField Type: RW
BitField Desc: RDI STS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstafrc_ena_Mask                                                          cBit31
#define c_upen_RDIstssta_rdistsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: rdistsstaerrstep
BitField Type: RW
BitField Desc: The Second RDI STS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstaerrstep_Mask                                                       cBit30_23
#define c_upen_RDIstssta_rdistsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: rdistsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask RDI STS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_RDIstssta_rdistsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: rdistsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error RDI STS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_RDIstssta_rdistsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: rdistsstafrc_sta
BitField Type: RW
BitField Desc: RDI STS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_RDIstssta_rdistsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: rdistsstaoneunit
BitField Type: RW
BitField Desc: Force RDI STS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstaoneunit_Mask                                                           cBit0
#define c_upen_RDIstssta_rdistsstaoneunit_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Force LOM STS Error Configuration
Reg Addr   : 0x28300-0x2832F
Reg Formula: 0x28300 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force LOM STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_LOMstscfg                                                                            0x28300

/*--------------------------------------
BitField Name: lomststype
BitField Type: RW
BitField Desc: LOM Type STS 1000: E-LOM P Enable 0100: E-LOM S Enable 0010:
E-LOM C Enable 0001: LOM Enable
BitField Bits: [63:60]
--------------------------------------*/
#define c_upen_LOMstscfg_lomststype_Mask                                                             cBit31_28
#define c_upen_LOMstscfg_lomststype_Shift                                                                   28

/*--------------------------------------
BitField Name: lomvttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvttim_mod_Mask                                                           cBit27_26
#define c_upen_LOMstscfg_lomvttim_mod_Shift                                                                 26

/*--------------------------------------
BitField Name: lomvterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvterr_clr_Mask                                                           cBit25_18
#define c_upen_LOMstscfg_lomvterr_clr_Shift                                                                 18

/*--------------------------------------
BitField Name: lomvterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvterr_set_Mask                                                           cBit17_10
#define c_upen_LOMstscfg_lomvterr_set_Shift                                                                 10

/*--------------------------------------
BitField Name: lomvterr_step
BitField Type: RW
BitField Desc: The Total Step to Force LOM VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvterr_step_Mask                                                            cBit9_2
#define c_upen_LOMstscfg_lomvterr_step_Shift                                                                 2

/*--------------------------------------
BitField Name: lomvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOM VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvtmsk_pos_Mask_01                                                        cBit31_18
#define c_upen_LOMstscfg_lomvtmsk_pos_Shift_01                                                              18
#define c_upen_LOMstscfg_lomvtmsk_pos_Mask_02                                                          cBit1_0
#define c_upen_LOMstscfg_lomvtmsk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: lomvtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOM VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvtmsk_ena_Mask                                                              cBit17
#define c_upen_LOMstscfg_lomvtmsk_ena_Shift                                                                 17

/*--------------------------------------
BitField Name: lomvterr_num
BitField Type: RW
BitField Desc: The Total LOM VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvterr_num_Mask                                                            cBit16_1
#define c_upen_LOMstscfg_lomvterr_num_Shift                                                                  1

/*--------------------------------------
BitField Name: lomvtfrc_mod
BitField Type: RW
BitField Desc: Force LOM VT Mode 0: One shot ( N events in T the unit time) 1:
continous (lomvterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvtfrc_mod_Mask                                                               cBit0
#define c_upen_LOMstscfg_lomvtfrc_mod_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force LOM STS Error Status
Reg Addr   : 0x28340-0x2836f
Reg Formula: 0x28340 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force LOM STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_LOMstssta                                                                            0x28340
#define cReg_upen_LOMstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: lomstssta_almset
BitField Type: RW
BitField Desc: LOM STS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_LOMstssta_lomstssta_almset_Mask                                                          cBit27
#define c_upen_LOMstssta_lomstssta_almset_Shift                                                             27

/*--------------------------------------
BitField Name: lomstsstaclralrm
BitField Type: RW
BitField Desc: LOM STS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstaclralrm_Mask                                                       cBit22_15
#define c_upen_LOMstssta_lomstsstaclralrm_Shift                                                             15

/*--------------------------------------
BitField Name: lomstsstasetalrm
BitField Type: RW
BitField Desc: LOM STS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstasetalrm_Mask                                                        cBit14_7
#define c_upen_LOMstssta_lomstsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: lomstsstatimecnt
BitField Type: RW
BitField Desc: LOM STS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstatimecnt_Mask                                                         cBit6_0
#define c_upen_LOMstssta_lomstsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: lomstsstafrc_ena
BitField Type: RW
BitField Desc: LOM STS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstafrc_ena_Mask                                                          cBit31
#define c_upen_LOMstssta_lomstsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: lomstsstaerrstep
BitField Type: RW
BitField Desc: The Second LOM STS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstaerrstep_Mask                                                       cBit30_23
#define c_upen_LOMstssta_lomstsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: lomstsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask LOM STS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_LOMstssta_lomstsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: lomstsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error LOM STS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_LOMstssta_lomstsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: lomstsstafrc_sta
BitField Type: RW
BitField Desc: LOM STS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_LOMstssta_lomstsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: lomstsstaoneunit
BitField Type: RW
BitField Desc: Force LOM STS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstaoneunit_Mask                                                           cBit0
#define c_upen_LOMstssta_lomstsstaoneunit_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Configuration n second to force in STS
Reg Addr   : 0x28380f-0x28380f
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure n second in  STS

------------------------------------------------------------------------------*/
#define cReg_upennsecsts                                                                              0x28380f

/*--------------------------------------
BitField Name: cfgnsecsts
BitField Type: RW
BitField Desc: The second number in STS Congiguration
BitField Bits: [31:0]
--------------------------------------*/
#define c_upennsecsts_cfgnsecsts_Mask                                                                 cBit31_0
#define c_upennsecsts_cfgnsecsts_Shift                                                                       0

/*------------------------------------------------------------------------------
Reg Name   : Force f VT Error Configuration
Reg Addr   : 0xa3000-0xa35FF
Reg Formula: 0xa3000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  :
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   :
This register is applicable to configure Force RFI VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cAttReg_upen_RFIvtcfg_Base                                                                     0xa3000
#define cAttReg_upen_RFIvtcfg(SliceId, StsId, VtgId, VtId)\
                                                 (0xa3000UL+16384UL*(SliceId)+32UL*(StsId)+4UL*(VtgId)+(VtId))
#define cAttReg_upen_RFIvtcfg_WidthVal                                                                      64
#define cAttReg_upen_RFIvtcfg_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: rfivttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define cAtt_upen_RFIvtcfg_rfivttim_mod_Bit_Start                                                           58
#define cAtt_upen_RFIvtcfg_rfivttim_mod_Bit_End                                                             59
#define cAtt_upen_RFIvtcfg_rfivttim_mod_Mask                                                         cBit27_26
#define cAtt_upen_RFIvtcfg_rfivttim_mod_Shift                                                               26
#define cAtt_upen_RFIvtcfg_rfivttim_mod_MaxVal                                                             0x0
#define cAtt_upen_RFIvtcfg_rfivttim_mod_MinVal                                                             0x0
#define cAtt_upen_RFIvtcfg_rfivttim_mod_RstVal                                                             0x0

/*--------------------------------------
BitField Name: rfivterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define cAtt_upen_RFIvtcfg_rfivterr_clr_Bit_Start                                                           50
#define cAtt_upen_RFIvtcfg_rfivterr_clr_Bit_End                                                             57
#define cAtt_upen_RFIvtcfg_rfivterr_clr_Mask                                                         cBit25_18
#define cAtt_upen_RFIvtcfg_rfivterr_clr_Shift                                                               18
#define cAtt_upen_RFIvtcfg_rfivterr_clr_MaxVal                                                             0x0
#define cAtt_upen_RFIvtcfg_rfivterr_clr_MinVal                                                             0x0
#define cAtt_upen_RFIvtcfg_rfivterr_clr_RstVal                                                             0x0

/*--------------------------------------
BitField Name: rfivterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define cAtt_upen_RFIvtcfg_rfivterr_set_Bit_Start                                                           42
#define cAtt_upen_RFIvtcfg_rfivterr_set_Bit_End                                                             49
#define cAtt_upen_RFIvtcfg_rfivterr_set_Mask                                                         cBit17_10
#define cAtt_upen_RFIvtcfg_rfivterr_set_Shift                                                               10
#define cAtt_upen_RFIvtcfg_rfivterr_set_MaxVal                                                             0x0
#define cAtt_upen_RFIvtcfg_rfivterr_set_MinVal                                                             0x0
#define cAtt_upen_RFIvtcfg_rfivterr_set_RstVal                                                             0x0

/*--------------------------------------
BitField Name: rfivterr_step
BitField Type: RW
BitField Desc: The Total Step to Force RFI VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define cAtt_upen_RFIvtcfg_rfivterr_step_Bit_Start                                                          34
#define cAtt_upen_RFIvtcfg_rfivterr_step_Bit_End                                                            41
#define cAtt_upen_RFIvtcfg_rfivterr_step_Mask                                                          cBit9_2
#define cAtt_upen_RFIvtcfg_rfivterr_step_Shift                                                               2
#define cAtt_upen_RFIvtcfg_rfivterr_step_MaxVal                                                            0x0
#define cAtt_upen_RFIvtcfg_rfivterr_step_MinVal                                                            0x0
#define cAtt_upen_RFIvtcfg_rfivterr_step_RstVal                                                            0x0

/*--------------------------------------
BitField Name: rfivtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for RFI VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define cAtt_upen_RFIvtcfg_rfivtmsk_pos_Bit_Start                                                           18
#define cAtt_upen_RFIvtcfg_rfivtmsk_pos_Bit_End                                                             33
#define cAtt_upen_RFIvtcfg_rfivtmsk_pos_Mask_01                                                      cBit31_18
#define cAtt_upen_RFIvtcfg_rfivtmsk_pos_Shift_01                                                            18
#define cAtt_upen_RFIvtcfg_rfivtmsk_pos_Mask_02                                                        cBit1_0
#define cAtt_upen_RFIvtcfg_rfivtmsk_pos_Shift_02                                                             0

/*--------------------------------------
BitField Name: rfivtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For RFI VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define cAtt_upen_RFIvtcfg_rfivtmsk_ena_Bit_Start                                                           17
#define cAtt_upen_RFIvtcfg_rfivtmsk_ena_Bit_End                                                             17
#define cAtt_upen_RFIvtcfg_rfivtmsk_ena_Mask                                                            cBit17
#define cAtt_upen_RFIvtcfg_rfivtmsk_ena_Shift                                                               17
#define cAtt_upen_RFIvtcfg_rfivtmsk_ena_MaxVal                                                             0x1
#define cAtt_upen_RFIvtcfg_rfivtmsk_ena_MinVal                                                             0x0
#define cAtt_upen_RFIvtcfg_rfivtmsk_ena_RstVal                                                             0x0

/*--------------------------------------
BitField Name: rfivterr_num
BitField Type: RW
BitField Desc: The Total RFI VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define cAtt_upen_RFIvtcfg_rfivterr_num_Bit_Start                                                            1
#define cAtt_upen_RFIvtcfg_rfivterr_num_Bit_End                                                             16
#define cAtt_upen_RFIvtcfg_rfivterr_num_Mask                                                          cBit16_1
#define cAtt_upen_RFIvtcfg_rfivterr_num_Shift                                                                1
#define cAtt_upen_RFIvtcfg_rfivterr_num_MaxVal                                                          0xffff
#define cAtt_upen_RFIvtcfg_rfivterr_num_MinVal                                                             0x0
#define cAtt_upen_RFIvtcfg_rfivterr_num_RstVal                                                             0x0

/*--------------------------------------
BitField Name: rfivtfrc_mod
BitField Type: RW
BitField Desc: Force RFI VT Mode 0: One shot ( N events in T the unit time) 1:
continous (rfivterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define cAtt_upen_RFIvtcfg_rfivtfrc_mod_Bit_Start                                                            0
#define cAtt_upen_RFIvtcfg_rfivtfrc_mod_Bit_End                                                              0
#define cAtt_upen_RFIvtcfg_rfivtfrc_mod_Mask                                                             cBit0
#define cAtt_upen_RFIvtcfg_rfivtfrc_mod_Shift                                                                0
#define cAtt_upen_RFIvtcfg_rfivtfrc_mod_MaxVal                                                             0x1
#define cAtt_upen_RFIvtcfg_rfivtfrc_mod_MinVal                                                             0x0
#define cAtt_upen_RFIvtcfg_rfivtfrc_mod_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx Framer Per Line Control
Reg Addr   : 0x21000 - 0x21700
Reg Formula: 0x21000 + 256*LineId
    Where  :
           + $LineId(0-7)
Reg Desc   :
Each register is used to configure operation modes for Tx framer engine and APS pattern inserted into APS bytes (K1 and K2 bytes) of the SONET/SDH line being relative with the address of the address of the register.

------------------------------------------------------------------------------*/
#define cAf6Reg_tfmregctl_Base                                                                         0x21000

/*--------------------------------------
BitField Name: OCNTxS1Pat
BitField Type: RW
BitField Desc: Pattern to insert into S1 byte.
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxS1Pat_Mask                                                               cBit31_24
#define cAf6_tfmregctl_OCNTxS1Pat_Shift                                                                     24

/*--------------------------------------
BitField Name: OCNTxApsPat
BitField Type: RW
BitField Desc: Pattern to insert into APS bytes (K1, K2).
BitField Bits: [23:8]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxApsPat_Mask                                                               cBit23_8
#define cAf6_tfmregctl_OCNTxApsPat_Shift                                                                     8

/*--------------------------------------
BitField Name: OCNTxS1LDis
BitField Type: RW
BitField Desc: S1 insertion disable 1: Disable 0: Enable.
BitField Bits: [7]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxS1LDis_Mask                                                                  cBit7
#define cAf6_tfmregctl_OCNTxS1LDis_Shift                                                                     7

/*--------------------------------------
BitField Name: OCNTxApsDis
BitField Type: RW
BitField Desc: Disable to process APS 1: Disable 0: Enable.
BitField Bits: [6]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxApsDis_Mask                                                                  cBit6
#define cAf6_tfmregctl_OCNTxApsDis_Shift                                                                     6

/*--------------------------------------
BitField Name: OCNTxReiLDis
BitField Type: RW
BitField Desc: Auto REI_L insertion disable 1: Disable inserting REI_L
automatically. 0: Enable automatically inserting REI_L.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxReiLDis_Mask                                                                 cBit5
#define cAf6_tfmregctl_OCNTxReiLDis_Shift                                                                    5

/*--------------------------------------
BitField Name: OCNTxRdiLDis
BitField Type: RW
BitField Desc: Auto RDI_L insertion disable 1: Disable inserting RDI_L
automatically. 0: Enable automatically inserting RDI_L.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxRdiLDis_Mask                                                                 cBit4
#define cAf6_tfmregctl_OCNTxRdiLDis_Shift                                                                    4

/*--------------------------------------
BitField Name: OCNTxAutoB2Dis
BitField Type: RW
BitField Desc: Auto B2 disable 1: Disable inserting calculated B2 values into B2
positions automatically. 0: Enable automatically inserting calculated B2 values
into B2 positions.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxAutoB2Dis_Mask                                                               cBit3
#define cAf6_tfmregctl_OCNTxAutoB2Dis_Shift                                                                  3

/*--------------------------------------
BitField Name: OCNTxRdiLThresSel
BitField Type: RW
BitField Desc: Select the number of frames being inserted RDI-L defects when
receive direction requests generating RDI-L at transmit direction. 1: Threshold2
is selected. 0: Threshold1 is selected.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxRdiLThresSel_Mask                                                            cBit2
#define cAf6_tfmregctl_OCNTxRdiLThresSel_Shift                                                               2

/*--------------------------------------
BitField Name: OCNTxRdiLFrc
BitField Type: RW
BitField Desc: RDI-L force. 1: Force RDI-L defect into transmit data. 0: Not
force RDI-L
BitField Bits: [1]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxRdiLFrc_Mask                                                                 cBit1
#define cAf6_tfmregctl_OCNTxRdiLFrc_Shift                                                                    1

/*--------------------------------------
BitField Name: OCNTxAisLFrc
BitField Type: RW
BitField Desc: AIS-L force. 1: Force AIS-L defect into transmit data. 0: Not
force AIS-L
BitField Bits: [0]
--------------------------------------*/
#define cAf6_tfmregctl_OCNTxAisLFrc_Mask                                                                 cBit0
#define cAf6_tfmregctl_OCNTxAisLFrc_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OCN Tx Framer Per Line Control
Reg Addr   : 0x21002 - 0x21702
Reg Formula: 0x21002 + 65536*GroupID + 256*LineId
    Where  :
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   :
Each register is used to configure operation modes for Tx framer engine and APS pattern inserted into APS bytes (K1 and K2 bytes) of the SONET/SDH line being relative with the address of the address of the register.

------------------------------------------------------------------------------*/
#define cReg_tfmregctlMs                                                                               0x21002

/*--------------------------------------
BitField Name: MScfg
BitField Type: RW
BitField Desc: Pattern to insert into
BitField Bits: [31:0]
--------------------------------------*/
#define c_tfmregctl_MScfg_Mask                                                                        cBit31_0
#define c_tfmregctl_MScfg_Shift                                                                              0

/*------------------------------------------------------------------------------
Reg Name   : OCN Tx Framer Config M0 Per Line Control
Reg Addr   : 0x21003 - 0x21703
Reg Formula: 0x21003 + 65536*GroupID + 256*LineId
    Where  :
           + $GroupID(0-1)
           + $LineId(0-7)
Reg Desc   :
Each register is used to configure operation modes for Tx framer engine and APS pattern inserted into APS bytes (K1 and K2 bytes) of the SONET/SDH line being relative with the address of the address of the register.

------------------------------------------------------------------------------*/
#define cReg_tfmregctl                                                                                 0x21003
#define cReg_tfmregctl_WidthVal                                                                             32

/*--------------------------------------
BitField Name: M0cfg
BitField Type: RW
BitField Desc: Pattern to insert into M0 byte.
BitField Bits: [7:0]
--------------------------------------*/
#define c_tfmregctl_M0cfg_Mask                                                                         cBit7_0
#define c_tfmregctl_M0cfg_Shift                                                                              0

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021SDHLINEATTREG_H_ */


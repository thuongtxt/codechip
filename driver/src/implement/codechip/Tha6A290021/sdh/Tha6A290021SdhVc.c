/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha6A290021SdhVc.c
 *
 * Created Date: Jul 18, 2016
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/sdh/Tha6029SdhFacePlateLineAuVcInternal.h"
#include "Tha6A290021ModuleSdhInternal.h"
#include "Tha6A290021SdhAttControllerInternal.h"
#include "../../../default/ocn/ThaModuleOcnInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "Tha6A290021SdhAttReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtNumDw       4

#define cPohByteG1ErdiMask      cBit3_1
#define cPohByteG1ErdiShift     1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290021SdhVc *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021SdhVc
    {
    tTha6029SdhFacePlateLineAuVc super;

    /* Private data */
    }tTha6A290021SdhVc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtChannelMethods            m_AtChannelOverride;
static tTha60210011Tfi5LineAuVcMethods  m_Tha60210011Tfi5LineAuVcOverride;
static tTha6029SdhLineSideAuVcMethods  m_Tha6029SdhLineSideAuVcOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtObjectMethods      *m_AtObjectMethods    = NULL;
static const tAtChannelMethods     *m_AtChannelMethods   = NULL;
static const tAtSdhChannelMethods  *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eAtRet ConfigLookUpWithXcHideMode(Tha6029SdhLineSideAuVc self)
    {
    uint8    slice, hwStsInSlice;

    if (ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModulePoh, &slice, &hwStsInSlice)==cAtOk)
        {
        ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleOcn);
        uint32 offset =  Tha60210011ModuleOcnBaseAddress(ocnModule);
        AtSdhChannel terVc  = (AtSdhChannel)AtChannelSourceGet((AtChannel)self);
        if (terVc)
            {

            if ( AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc4||
                 AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc4_4c||
                 AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc4_16c||
                 AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc4_nc)
                {
                uint8 stsStart = AtSdhChannelSts1Get((AtSdhChannel)terVc);
                uint8 numSts   = AtSdhChannelNumSts((AtSdhChannel)terVc);
                uint8 sts_i = 0;
                uint8  sliceVc, hwStsInSliceVc;
                for (sts_i=0; sts_i<numSts; sts_i++)
                    {
                    uint32 address, value, mask, shift;
                    uint8 swSts = (uint8)(stsStart + sts_i);
                    ThaSdhChannelHwStsGet((AtSdhChannel)self, cThaModuleOcn, swSts, &sliceVc, &hwStsInSliceVc);
                    address = 0xffe00 + 64UL*slice + hwStsInSliceVc  + offset;
                    value   = mChannelHwRead(self, address, cAtModuleSdh);
                    mask    = cBit5_0;
                    shift   = 0;
                    mFieldIns(&value, mask, shift, hwStsInSliceVc);
                    mChannelHwWrite(self, address, value, cAtModuleSdh);
                    }
                }
            else
                {
                uint8  sliceTerVc, hwStsInSliceTerVc;
                if (ThaSdhChannel2HwMasterStsId(terVc, cThaModulePoh, &sliceTerVc, &hwStsInSliceTerVc)==cAtOk)
                    {
                    uint32 address = 0xffe00 + 64UL*slice + hwStsInSlice  + offset;
                    uint32 value   = mChannelHwRead(self, address, cAtModuleSdh);
                    uint32 mask    = cBit5_0;
                    uint32 shift   = 0;
                    mFieldIns(&value, mask, shift, hwStsInSliceTerVc);
                    mChannelHwWrite(self, address, value, cAtModuleSdh);
                    }
                }
            }
        }
    return cAtOk;
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret = cAtOk;
    ret = m_AtChannelMethods->TimingSet(self, timingMode, timingSource);
    if (timingMode == cAtTimingModeFreeRunning||cAtTimingModeSdhSys)
        AtAttControllerFrequenceOffsetSet(AtChannelAttController(self), 0);
    if (timingMode == cAtTimingModeLoop)
        {
        AtAttControllerForcePointerAdjModeSet(AtChannelAttController(self), cAtSdhPathPointerAdjTypeIncrease, cAtAttForcePointerAdjModeContinuous);
        AtAttControllerForcePointerAdj(AtChannelAttController(self), cAtSdhPathPointerAdjTypeIncrease);
        AtOsalUSleep(100);
        AtAttControllerUnForcePointerAdj(AtChannelAttController(self), cAtSdhPathPointerAdjTypeIncrease);

        AtAttControllerForcePointerAdjModeSet(AtChannelAttController(self), cAtSdhPathPointerAdjTypeDecrease, cAtAttForcePointerAdjModeContinuous);
        AtAttControllerForcePointerAdj(AtChannelAttController(self), cAtSdhPathPointerAdjTypeDecrease);
        AtOsalUSleep(100);
        AtAttControllerUnForcePointerAdj(AtChannelAttController(self), cAtSdhPathPointerAdjTypeDecrease);
        }
    return ret;
    }

static eBool CanChangedMappingWhenHasPrbsEngine(Tha60210011Tfi5LineAuVc self)
    {
    AtUnused(self);
    return cAtTrue;
    }


static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return AtAttControllerInit(AtChannelAttController(self));
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmTypes, eAtRet (*fForceAlarm)(AtAttController, uint32), eAtRet (*fSetClearAlarm)(AtAttController, uint32))
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
       {
       uint32 alarmType = alarmTypes & (cBit0 << i);
       if (alarmType)
           {
           AtAttControllerForceAlarmModeSet(attController, alarmType, cAtAttForceAlarmModeContinuous);
           fSetClearAlarm(attController, alarmType);
           ret |= fForceAlarm(attController, alarmType);
           }
       }
    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerForceAlarm,AtAttControllerTxAlarmsSet);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerUnForceAlarm,AtAttControllerTxAlarmsClear);
    }


static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcibleAlarmsGet(self);

    return cAtSdhPathAlarmUneq | cAtSdhPathAlarmRdi;
    }

static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorTypes, eAtRet (*fForceError)(AtAttController, uint32))
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 errorType = errorTypes & (cBit0 << i);
        if (errorType)
            {
            AtAttControllerForceErrorModeSet(attController, errorType, cAtAttForceErrorModeContinuous);
            ret |=  fForceError(attController, errorType);
            }
        }
    return ret;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerForceError);
    }
static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorUnForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerUnForceError);
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcableErrorsGet(self);

    return cAtSdhPathCounterTypeBip | cAtSdhPathCounterTypeRei;
    }

static eAtRet TxByteG1ERdiSet(AtSdhChannel self, uint8 overheadByteValue)
    {
    uint32 longRegVal[cAtNumDw];
    uint32 g1ErdiValue = mRegField(overheadByteValue, cPohByteG1Erdi);
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController((AtChannel)self);

    mMethodsGet((AtAttController)controller)->LongRead((AtAttController)controller, cReg_upen_RDIstscfg, longRegVal, cAtNumDw);
    mRegFieldSet(longRegVal[1], c_upen_RDIstscfg_g1vtvalue_, g1ErdiValue);
    mMethodsGet((AtAttController)controller)->LongWrite((AtAttController)controller, cReg_upen_RDIstscfg, longRegVal, cAtNumDw);

    return cAtOk;
    }

static uint8 TxByteG1ERdiGet(AtSdhChannel self)
    {
    uint32 longRegVal[cAtNumDw];
    uint32 g1ERdiValue;
    uint32 g1Byte = 0;
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController((AtChannel)self);

    mMethodsGet((AtAttController)controller)->LongRead((AtAttController)controller, cReg_upen_RDIstscfg, longRegVal, cAtNumDw);
    g1ERdiValue = mRegField(longRegVal[1], c_upen_RDIstscfg_g1vtvalue_);
    mRegFieldSet(g1Byte, cPohByteG1Erdi, g1ERdiValue);

    return (uint8)g1Byte;
    }

static eAtModuleSdhRet TxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 overheadByteValue)
    {
    eAtRet ret = cAtOk;

    if (overheadByte == cAtSdhPathOverheadByteG1)
        {
        /* Only set G1[b5:b7] ERDI here, others should be controlled in super */
        ret |= TxByteG1ERdiSet(self, overheadByteValue);
        }

    ret |= m_AtSdhChannelMethods->TxOverheadByteSet(self, overheadByte, overheadByteValue);
    return ret;
    }

static uint8 TxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    uint8 value = m_AtSdhChannelMethods->TxOverheadByteGet(self, overheadByte);

    if (overheadByte == cAtSdhPathOverheadByteG1)
        {
        uint8 g1ERdiValue = TxByteG1ERdiGet(self);
        value = (uint8)((value & ~cPohByteG1ErdiMask) | g1ERdiValue);
        }

    return value;
    }

static void OverrideAtSdhChannel(AtSdhChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteSet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteGet);
        }

    mMethodsSet(self, &m_AtSdhChannelOverride);
    }

static void OverrideAtObject(AtSdhVc self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideTha60210011Tfi5LineAuVc(Tha60210011Tfi5LineAuVc self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011Tfi5LineAuVcOverride, mMethodsGet(self), sizeof(m_Tha60210011Tfi5LineAuVcOverride));

        mMethodOverride(m_Tha60210011Tfi5LineAuVcOverride, CanChangedMappingWhenHasPrbsEngine);
        }

    mMethodsSet(self, &m_Tha60210011Tfi5LineAuVcOverride);
    }

static void OverrideTha6029SdhLineSideAuVc(Tha6029SdhLineSideAuVc self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SdhLineSideAuVcOverride, mMethodsGet(self), sizeof(m_Tha6029SdhLineSideAuVcOverride));

        mMethodOverride(m_Tha6029SdhLineSideAuVcOverride, ConfigLookUpWithXcHideMode);
        }

    mMethodsSet(self, &m_Tha6029SdhLineSideAuVcOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideTha60210011Tfi5LineAuVc((Tha60210011Tfi5LineAuVc) self);
    OverrideTha6029SdhLineSideAuVc((Tha6029SdhLineSideAuVc) self);
    OverrideAtSdhChannel((AtSdhChannel)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021SdhVc);
    }

static AtSdhVc ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhFacePlateLineAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha6A290021SdhVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, channelId, channelType, module);
    }

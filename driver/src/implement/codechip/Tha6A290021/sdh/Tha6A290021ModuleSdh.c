/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290021ModuleSdh.c
 *
 * Created Date: Jul 8, 2016
 *
 * Description : PWCodechip-60290021 SDH module.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/att/ThaAttSdhManagerInternal.h"
#include "Tha6A290021ModuleSdhInternal.h"
#include "../encap/Tha6A290021HdlcChannel.h"
#include "Tha6A290021SdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cTha6A290021ModuleSdhFacePlateLineIdStart 0
#define cTha6A290021ModuleSdhFacePlateLineIdEnd   15

#define cTha6A290021ModuleSdhTerminatedLineIdStart 24
#define cTha6A290021ModuleSdhTerminatedLineIdEnd   31

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290021ModuleSdh*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021ModuleSdh
    {
	tTha60290021ModuleSdh super;
    AtLongRegisterAccess attLongRegisterAccess;
    }tTha6A290021ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tAtModuleSdhMethods          m_AtModuleSdhOverride;
static tThaModuleSdhMethods         m_ThaModuleSdhOverride;
static tTha60290021ModuleSdhMethods m_Tha60290021ModuleSdhOverride;
static tTha60210011ModuleSdhMethods m_Tha60210011ModuleSdhOverride;

/* Save super implementation */
static const tAtObjectMethods             *m_AtObjectMethods = NULL;
static const tAtModuleMethods             *m_AtModuleMethods = NULL;
static const tAtModuleSdhMethods          *m_AtModuleSdhMethods   = NULL;
static const tTha60290021ModuleSdhMethods *m_Tha60290021ModuleSdhMethods = NULL;
static const tTha60210011ModuleSdhMethods *m_Tha60210011ModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtObjectAny LineAttControllerCreate(ThaModuleSdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290021SdhLineAttControllerNew(channel);
    }
static AtObjectAny PathAttControllerCreate(ThaModuleSdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290021SdhPathAttControllerNew(channel);
    }
static AtObjectAny Vc1xAttControllerCreate(ThaModuleSdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290021SdhVc1xAttControllerNew(channel);
    }

static eBool IsFaceplateLine(AtModuleSdh self, uint8 lineId)
    {
    AtUnused(self);
    return mInRange(lineId, cTha6A290021ModuleSdhFacePlateLineIdStart, cTha6A290021ModuleSdhFacePlateLineIdEnd);
    }

static eBool IsTerminatedLine(AtModuleSdh self, uint8 lineId)
    {
    AtUnused(self);
    return mInRange(lineId, cTha6A290021ModuleSdhTerminatedLineIdStart, cTha6A290021ModuleSdhTerminatedLineIdEnd);
    }

static AtSdhChannel Tu3VcObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha6A290021SdhTu3VcNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel TuObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha6A290021SdhTu1xNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel Vc1xObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha6A290021SdhVc1xNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel ChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    if ((channelType == cAtSdhChannelTypeLine) && (IsFaceplateLine(self, lineId)))
        return (AtSdhChannel)Tha6A290021SdhLineNew(channelId, (AtModuleSdh)self);

    if (((channelType == cAtSdhChannelTypeAu4_16c) ||
         (channelType == cAtSdhChannelTypeAu4_4c)  ||
         (channelType == cAtSdhChannelTypeAu4)     ||
         (channelType == cAtSdhChannelTypeAu3))    &&
         (IsFaceplateLine(self, lineId)))
        return (AtSdhChannel)Tha6A290021SdhAuNew(channelId, channelType, (AtModuleSdh)self);

    if ((channelType == cAtSdhChannelTypeVc4_16c) ||
        (channelType == cAtSdhChannelTypeVc4_4c)  ||
        (channelType == cAtSdhChannelTypeVc4)     ||
        (channelType == cAtSdhChannelTypeVc3))
        {
        if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3)
            return m_AtModuleSdhMethods->ChannelCreate(self, lineId, parent, channelType, channelId);

        if (IsFaceplateLine(self, lineId))
            return (AtSdhChannel)Tha6A290021SdhVcNew(channelId, channelType, (AtModuleSdh)self);

        if (IsTerminatedLine(self, lineId))
            return (AtSdhChannel)Tha6A290021SdhTerminatedLineVcNew(channelId, channelType, (AtModuleSdh)self);
        }

    return m_AtModuleSdhMethods->ChannelCreate(self, lineId, parent, channelType, channelId);
    }

static AtHdlcChannel DccChannelObjectCreate(Tha60290021ModuleSdh self, uint32 dccId,  AtSdhLine line, eAtSdhLineDccLayer layers)
	{
	return Tha6A290021HdlcChannelNew(dccId, line, layers, (AtModuleSdh)self);
	}


static AtAttSdhManager AttSdhManagerCreate(AtModuleSdh self)
    {
    return Tha6A290021AttSdhManagerNew(self);
    }

static eAtRet AllChannelsInterruptDisable(AtModule self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->attLongRegisterAccess));
    mThis(self)->attLongRegisterAccess = NULL;
    m_AtObjectMethods->Delete(self);
    }

static uint32 *AttSdhHoldRegistersGet(Tha6A290021ModuleSdh self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x100008};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = 1;

    return holdRegisters;
    }

static AtLongRegisterAccess AttSdhLongRegisterAccessCreate(Tha6A290021ModuleSdh self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AttSdhHoldRegistersGet(self, &numHoldRegisters);

    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static AtLongRegisterAccess AttSdhLongRegisterAccess(Tha6A290021ModuleSdh self, uint32 localAddress)
    {
    AtUnused(localAddress);

    return self->attLongRegisterAccess;
    }

static uint16 AttSdhLongWriteOnCore(Tha6A290021ModuleSdh self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
    {
    AtLongRegisterAccess registerAccess = AttSdhLongRegisterAccess(self, localAddress);
    if (registerAccess == NULL) return 0;
    return AtLongRegisterAccessWrite(registerAccess, AtIpCoreHalGet(core), localAddress, dataBuffer, bufferLen);
    }

static uint16 AttSdhLongReadOnCore(Tha6A290021ModuleSdh self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
    {
    AtLongRegisterAccess registerAccess = AttSdhLongRegisterAccess(self, localAddress);
    if (registerAccess == NULL) return 0;
    AtOsalMemInit(dataBuffer, 0, bufferLen * sizeof(uint32));
    return AtLongRegisterAccessRead(registerAccess, AtIpCoreHalGet(core), localAddress, dataBuffer, bufferLen);
    }

static void SetupData(AtModuleSdh self)
    {
    Tha6A290021ModuleSdh module = (Tha6A290021ModuleSdh)self;
    if (module->attLongRegisterAccess == NULL)
        module->attLongRegisterAccess = AttSdhLongRegisterAccessCreate(module);
    }

static void OverrideAtObject(AtModuleSdh self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleSdh self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, AllChannelsInterruptDisable);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, ChannelCreate);
        mMethodOverride(m_AtModuleSdhOverride, AttSdhManagerCreate);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void OverrideThaModuleSdh(ThaModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, mMethodsGet(self), sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, LineAttControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, PathAttControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, Vc1xAttControllerCreate);
        }

    mMethodsSet(self, &m_ThaModuleSdhOverride);
    }

static void OverrideTha60290021ModuleSdh(AtModuleSdh self)
    {
    Tha60290021ModuleSdh sdh = (Tha60290021ModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021ModuleSdhMethods = mMethodsGet(sdh);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModuleSdhOverride, m_Tha60290021ModuleSdhMethods, sizeof(m_Tha60290021ModuleSdhOverride));

        mMethodOverride(m_Tha60290021ModuleSdhOverride, DccChannelObjectCreate);
        }

    mMethodsSet(sdh, &m_Tha60290021ModuleSdhOverride);
    }

static void OverrideTha60210011ModuleSdh(AtModuleSdh self)
    {
    Tha60210011ModuleSdh module = (Tha60210011ModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleSdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleSdhOverride, m_Tha60210011ModuleSdhMethods, sizeof(m_Tha60210011ModuleSdhOverride));

        mMethodOverride(m_Tha60210011ModuleSdhOverride, TuObjectCreate);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, Tu3VcObjectCreate);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, Vc1xObjectCreate);
        }

    mMethodsSet(module, &m_Tha60210011ModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh((ThaModuleSdh) self);
    OverrideTha60290021ModuleSdh(self);
    OverrideTha60210011ModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021ModuleSdh);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    /* Setup internal data */
    SetupData(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha6A290021ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

uint16 Tha60290021ModuleSdhAttLongReadOnCore(Tha6A290021ModuleSdh self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
    {
    uint16 numDwords;

    if (self == NULL)
        return 0;

    numDwords = AttSdhLongReadOnCore(self, localAddress, dataBuffer, bufferLen, core);
    if (numDwords == 0) return 0;

    if (AtDriverDebugIsEnabled())
        AtDeviceLongReadNotify(AtModuleDeviceGet((AtModule) self), localAddress, dataBuffer, numDwords, 0);

    return numDwords;
    }

uint16 Tha60290021ModuleSdhAttLongWriteOnCore(Tha6A290021ModuleSdh self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
    {
    uint16 numDwords;

    if (self == NULL)
        return 0;

    numDwords = AttSdhLongWriteOnCore(self, localAddress, dataBuffer, bufferLen, core);
    if (numDwords == 0) return 0;

    if (AtDriverDebugIsEnabled())
        AtDeviceLongWriteNotify(AtModuleDeviceGet((AtModule) self), localAddress, dataBuffer, numDwords, 0);

    return numDwords;
    }


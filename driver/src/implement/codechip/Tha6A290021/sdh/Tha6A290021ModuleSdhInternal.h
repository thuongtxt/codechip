/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6A290021ModuleSdhInternal.h
 * 
 * Created Date: Jul 8, 2016
 *
 * Description : Module SDH internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021MODULESDHINTERNAL_H_
#define _THA6A290021MODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/sdh/Tha60290021ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021ModuleSdh * Tha6A290021ModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAu Tha6A290021SdhAuNew(uint32 auId, uint8 auType, AtModuleSdh module);
AtSdhLine Tha6A290021SdhLineNew(uint32 channelId, AtModuleSdh module);
AtSdhVc Tha6A290021SdhVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha6A290021SdhTerminatedLineVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha6A290021SdhTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha6A290021SdhVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhTu Tha6A290021SdhTu1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

uint16 Tha60290021ModuleSdhAttLongReadOnCore(Tha6A290021ModuleSdh self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
uint16 Tha60290021ModuleSdhAttLongWriteOnCore(Tha6A290021ModuleSdh self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021MODULESDHINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A290021SdhVc1x.c
 *
 * Created Date: Jul 19, 2016
 *
 * Description : SDH VC1x
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/sdh/Tha6029SdhTerminatedLineVc1xInternal.h"
#include "Tha6A290021ModuleSdhInternal.h"
#include "Tha6A290021SdhAttControllerInternal.h"
#include "Tha6A290021SdhAttReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtNumDw       4

#define cPohByteK4ErdiMask      cBit3_1
#define cPohByteK4ErdiShift     1
#define cPohByteV5RdiMask       cBit0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290021SdhVc1x *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021SdhVc1x
    {
    tTha6029SdhTerminatedLineVc1x super;

    /* Private data */
    }tTha6A290021SdhVc1x;

/*--------------------------- Global variables -------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tAtSdhChannelMethods   m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtObjectMethods      *m_AtObjectMethods    = NULL;
static const tAtChannelMethods     *m_AtChannelMethods   = NULL;
static const tAtSdhChannelMethods  *m_AtSdhChannelMethods = NULL;

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret = cAtOk;
    ret = m_AtChannelMethods->TimingSet(self, timingMode, timingSource);
    if (timingMode == cAtTimingModeFreeRunning||cAtTimingModeSdhSys)
        AtAttControllerFrequenceOffsetSet(AtChannelAttController(self), 0);
    if (timingMode == cAtTimingModeLoop)
        {
        AtAttControllerForcePointerAdjModeSet(AtChannelAttController(self), cAtSdhPathPointerAdjTypeIncrease, cAtAttForcePointerAdjModeContinuous);
        AtAttControllerForcePointerAdj(AtChannelAttController(self), cAtSdhPathPointerAdjTypeIncrease);
        AtOsalUSleep(100);
        AtAttControllerUnForcePointerAdj(AtChannelAttController(self), cAtSdhPathPointerAdjTypeIncrease);

        AtAttControllerForcePointerAdjModeSet(AtChannelAttController(self), cAtSdhPathPointerAdjTypeDecrease, cAtAttForcePointerAdjModeContinuous);
        AtAttControllerForcePointerAdj(AtChannelAttController(self), cAtSdhPathPointerAdjTypeDecrease);
        AtOsalUSleep(100);
        AtAttControllerUnForcePointerAdj(AtChannelAttController(self), cAtSdhPathPointerAdjTypeDecrease);
        }
    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return AtAttControllerInit(AtChannelAttController(self));
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmTypes, eAtRet (*fForceAlarm)(AtAttController, uint32), eAtRet (*fSetClearAlarm)(AtAttController, uint32))
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 alarmType = alarmTypes & (cBit0 << i);
        if (alarmType)
            {
            AtAttControllerForceAlarmModeSet(attController, alarmType, cAtAttForceAlarmModeContinuous);
            fSetClearAlarm(attController, alarmType);
            ret = fForceAlarm(attController, alarmType);
            }
        }
    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerForceAlarm,AtAttControllerTxAlarmsSet);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerUnForceAlarm,AtAttControllerTxAlarmsClear);
    }


static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcibleAlarmsGet(self);

    return cAtSdhPathAlarmUneq | cAtSdhPathAlarmRdi;
    }

static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorTypes, eAtRet (*fForceError)(AtAttController, uint32))
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 errorType = errorTypes & (cBit0 << i);
        if (errorType)
            {
            AtAttControllerForceErrorModeSet(attController, errorType, cAtAttForceErrorModeContinuous);
            ret |= fForceError(attController, errorType);
            }
        }
    return ret;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerForceError);
    }
static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorUnForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerUnForceError);
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcableErrorsGet(self);

    return cAtSdhPathCounterTypeBip | cAtSdhPathCounterTypeRei;
    }

static eAtRet TxByteK4ERdiSet(AtSdhChannel self, uint8 overheadByteValue)
    {
    uint32 longRegVal[cAtNumDw];
    uint32 k4ErdiValue = mRegField(overheadByteValue, cPohByteK4Erdi);
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController((AtChannel)self);

    mMethodsGet((AtAttController)controller)->LongRead((AtAttController)controller, cReg_upen_RDIvtcfg, longRegVal, cAtNumDw);
    mRegFieldSet(longRegVal[1], c_upen_RDIvtcfg_k4vtvalue_, k4ErdiValue);
    mMethodsGet((AtAttController)controller)->LongWrite((AtAttController)controller, cReg_upen_RDIvtcfg, longRegVal, cAtNumDw);

    return cAtOk;
    }

static uint8 TxByteK4ERdiGet(AtSdhChannel self)
    {
    uint32 longRegVal[cAtNumDw];
    uint32 k4ERdiValue;
    uint32 k4Byte = 0;
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController((AtChannel)self);
    uint32 regAddr = cReg_upen_RDIvtcfg;

    mMethodsGet((AtAttController)controller)->LongRead((AtAttController)controller, regAddr, longRegVal, cAtNumDw);
    k4ERdiValue = mRegField(longRegVal[1], c_upen_RDIvtcfg_k4vtvalue_);
    mRegFieldSet(k4Byte, cPohByteK4Erdi, k4ERdiValue);

    return (uint8)k4Byte;
    }

static eAtRet TxByteV5RdiSet(AtSdhChannel self, uint8 overheadByteValue)
    {
    uint32 longRegVal[cAtNumDw];
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController((AtChannel)self);
    mMethodsGet((AtAttController)controller)->LongRead((AtAttController)controller, cReg_upen_RDIvtcfg, longRegVal, cAtNumDw);
    mRegFieldSet(longRegVal[1], c_upen_RDIvtcfg_v5vtvalue_, (overheadByteValue & cPohByteV5RdiMask));
    mMethodsGet((AtAttController)controller)->LongWrite((AtAttController)controller, cReg_upen_RDIvtcfg, longRegVal, cAtNumDw);
    return cAtOk;
    }

static uint8 TxByteV5RdiGet(AtSdhChannel self)
    {
    uint32 longRegVal[cAtNumDw];
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController((AtChannel)self);
    mMethodsGet((AtAttController)controller)->LongRead((AtAttController)controller, cReg_upen_RDIvtcfg, longRegVal, cAtNumDw);
    return (uint8)(mRegField(longRegVal[1], c_upen_RDIvtcfg_v5vtvalue_) & cPohByteV5RdiMask);
    }

static eAtModuleSdhRet TxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 overheadByteValue)
    {
    eAtRet ret = cAtOk;

    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteK4:
            ret = TxByteK4ERdiSet(self, overheadByteValue);
            break;

        case cAtSdhPathOverheadByteV5:
            ret = TxByteV5RdiSet(self, overheadByteValue);
            break;

        default:
            break;
        }

    ret |= m_AtSdhChannelMethods->TxOverheadByteSet(self, overheadByte, overheadByteValue);
    return ret;
    }

static uint8 TxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    uint8 k4ERdiValue, v5RdiValue;
    uint8 value = m_AtSdhChannelMethods->TxOverheadByteGet(self, overheadByte);

    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteK4:
            k4ERdiValue = TxByteK4ERdiGet(self);
            value = (uint8)((value & ~cPohByteK4ErdiMask) | k4ERdiValue);
            break;

        case cAtSdhPathOverheadByteV5:
            v5RdiValue = TxByteV5RdiGet(self);
            value = (uint8)((value & ~cPohByteV5RdiMask) | v5RdiValue);
            break;

        default:
            return value;
        }

    return value;
    }

static void OverrideAtSdhChannel(AtSdhChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteSet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteGet);
        }

    mMethodsSet(self, &m_AtSdhChannelOverride);
    }

static void OverrideAtObject(AtSdhVc self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel((AtSdhChannel)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021SdhVc1x);
    }

static AtSdhVc ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhTerminatedLineVc1xObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha6A290021SdhVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    return ObjectInit(self, channelId, channelType, module);
    }

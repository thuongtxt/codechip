/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : Tha6A290021AttControllerInternal.h
 * 
 * Created Date: Jul 8, 2017
 *
 * Description : ATT SDH controller internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021SDHATTCONTROLLERINTERNAL_H_
#define _THA6A290021SDHATTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../pdh/Tha6A290021PdhAttControllerInternal.h"
#include "../../Tha6A210031/sdh/Tha6A210031SdhAttControllerInternal.h"
#include "Tha6A290021ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021SdhLineAttController
    {
    tTha6A210031SdhLineAttController super;
    }tTha6A290021SdhLineAttController;

typedef struct tTha6A290021SdhPathAttController
	{
	tTha6A210031SdhPathAttController super;
	tAtOsalCurTime startTimeForcePointer;
	}tTha6A290021SdhPathAttController;

typedef struct tTha6A290021SdhVc1xAttController
    {
    tTha6A210031SdhVc1xAttController super;
    tAtOsalCurTime startTimeForcePointer;
    }tTha6A290021SdhVc1xAttController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttController Tha6A290021SdhLineAttControllerNew(AtChannel sdhLine);
AtAttController Tha6A290021SdhPathAttControllerNew(AtChannel sdhPath);
AtAttController Tha6A290021SdhVc1xAttControllerNew(AtChannel sdhPath);
eBool Tha6A290021AttSdhForceLogicFromDut(AtChannel self);
eAtRet Tha6A290021SdhPathPointerInit(AtAttController self, float ppm);
eAtRet Tha6A290021SdhPathPointerPointerAdjEnable(AtAttController self, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021SDHATTCONTROLLERINTERNAL_H_ */


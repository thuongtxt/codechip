/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : Tha6A290021SdhTerminatedLineVc.c
 *
 * Created Date: Aug 7, 2017
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/sdh/Tha6029SdhTerminatedLineAuVcInternal.h"
#include "Tha6A290021ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290021SdhTerminatedLineVc *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021SdhTerminatedLineVc
    {
    tTha6029SdhTerminatedLineAuVc super;
    }tTha6A290021SdhTerminatedLineVc;

/*--------------------------- Global variables -------------------------------*/


/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods         m_AtSdhChannelOverride;
static tThaSdhAuVcMethods           m_ThaSdhAuVcOverride;
static tTha60210011Tfi5LineAuVcMethods  m_Tha60210011Tfi5LineAuVcOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eAtRet PgStsSlaverIndicate(ThaSdhAuVc self, eAtSdhVcMapType vcMapType)
    {
    uint8 sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);

    /* First is master */
    eAtRet ret = ThaOcnStsPgSlaveIndicate(sdhChannel, startSts, cAtFalse);

    /* Others depend on vcMapType,
     * As HW recommended, just set PG slaver indicate in case of VC4MapC4 and VC4_4cMapC4_4c
     * because this register is used for concatenation in case of VC4 unchannelized only,
     * when VC4 is channelized, another register is used instead, so need to release concatenation in this register
     */
    for (sts_i = 1; sts_i < AtSdhChannelNumSts(sdhChannel); sts_i++)
        {
        uint8 sts1Id = (uint8)(sts_i + startSts);
        if ((vcMapType == cAtSdhVcMapTypeVc4MapC4)        ||
            (vcMapType == cAtSdhVcMapTypeVc4_4cMapC4_4c)  ||
            (vcMapType == cAtSdhVcMapTypeVc4_16cMapC4_16c)||
            (vcMapType == cAtSdhVcMapTypeVc4_64cMapC4_64c))
            return cAtOk;
        else
            ret |= ThaOcnStsPgSlaveIndicate(sdhChannel, sts1Id, cAtTrue);
        }

    return ret;
    }

static eBool CanChangedMappingWhenHasPrbsEngine(Tha60210011Tfi5LineAuVc self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet SubChannelsInterruptDisable(AtSdhChannel self, eBool onlySubChannels)
    {
    AtUnused(self);
    AtUnused(onlySubChannels);
    return cAtOk;
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(channel), sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, SubChannelsInterruptDisable);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideTha60210011Tfi5LineAuVc(Tha60210011Tfi5LineAuVc self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011Tfi5LineAuVcOverride, mMethodsGet(self), sizeof(m_Tha60210011Tfi5LineAuVcOverride));

        mMethodOverride(m_Tha60210011Tfi5LineAuVcOverride, CanChangedMappingWhenHasPrbsEngine);
        }

    mMethodsSet(self, &m_Tha60210011Tfi5LineAuVcOverride);
    }

static void OverrideThaSdhAuVc(ThaSdhAuVc self)
    {


    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhAuVcOverride, mMethodsGet(self), sizeof(m_ThaSdhAuVcOverride));
        mMethodOverride(m_ThaSdhAuVcOverride, PgStsSlaverIndicate);
        }

    mMethodsSet(self, &m_ThaSdhAuVcOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtSdhChannel(self);
    OverrideThaSdhAuVc((ThaSdhAuVc) self);
    OverrideTha60210011Tfi5LineAuVc((Tha60210011Tfi5LineAuVc) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021SdhTerminatedLineVc);
    }

static AtSdhVc ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhTerminatedLineAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha6A290021SdhTerminatedLineVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, channelId, channelType, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A290021SdhLine.c
 *
 * Created Date: Jul 6, 2016
 *
 * Description : SDH line concrete code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/att/ThaAttSdhManagerInternal.h"
#include "../../Tha60290021/sdh/Tha6029SdhFacePlateLineInternal.h"
#include "Tha6A290021ModuleSdhInternal.h"
#include "Tha6A290021SdhAttControllerInternal.h"
#include "../../Tha60290021/ocn/Tha60290021ModuleOcnReg.h"
#include "Tha6A290021SdhAttReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290021SdhLine *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021SdhLine
    {
    tTha6029SdhFacePlateLine super;

    /* Private data */
    }tTha6A290021SdhLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tAtSdhChannelMethods   m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtObjectMethods      *m_AtObjectMethods    = NULL;
static const tAtChannelMethods     *m_AtChannelMethods   = NULL;
static const tAtSdhChannelMethods  *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eAtRet Init(AtChannel self)
    {
    AtSerdesController serdes=NULL;
    eAtSdhLineRate rate= cAtSdhLineModeUnknown;
    eAtRet ret = m_AtChannelMethods->Init(self);

    if (ret != cAtOk)
        return ret;

    serdes = AtSdhLineSerdesController((AtSdhLine) self);
    rate = AtSdhLineRateGet((AtSdhLine)self);
    switch ((uint32)rate)
        {
        case cAtSdhLineRateStm0:
            ret    = AtSerdesControllerModeSet(serdes, cAtSerdesModeStm0);
            break;
        case cAtSdhLineRateStm1:
            ret    = AtSerdesControllerModeSet(serdes, cAtSerdesModeStm1);
            break;
        case cAtSdhLineRateStm4:
            ret    = AtSerdesControllerModeSet(serdes, cAtSerdesModeStm4);
            break;
        case cAtSdhLineRateStm16:
            ret    = AtSerdesControllerModeSet(serdes, cAtSerdesModeStm16);
            break;
        case cAtSdhLineRateStm64:
            ret    = AtSerdesControllerModeSet(serdes, cAtSerdesModeStm64);
            break;
        default:
            ret    = AtSerdesControllerModeSet(serdes, cAtSerdesModeStm16);
            break;
        }
    return AtAttControllerInit(AtChannelAttController(self));
    }

static eAtRet SubChannelsInterruptDisable(AtSdhChannel self, eBool onlySubChannels)
    {
    AtUnused(self);
    AtUnused(onlySubChannels);
    return cAtOk;
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmTypes, eAtRet (*fForceAlarm)(AtAttController, uint32), eAtRet (*fSetClearAlarm)(AtAttController, uint32))
    {
    uint32 i=0;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 alarmType = alarmTypes & (cBit0 << i);
        if (alarmType)
            {
            AtAttControllerForceAlarmModeSet(attController, alarmType, cAtAttForceAlarmModeContinuous);
            fSetClearAlarm(attController, alarmType);
            ret = fForceAlarm(attController, alarmType);
            }
        }
    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerForceAlarm,AtAttControllerTxAlarmsSet);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerUnForceAlarm,AtAttControllerTxAlarmsClear);
    }


static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcibleAlarmsGet(self);

    return cAtSdhLineAlarmLos | cAtSdhLineAlarmAis | cAtSdhLineAlarmRdi | cAtSdhLineAlarmLof;
    }

static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorTypes, eAtRet (*fForceError)(AtAttController, uint32))
    {
    AtAttController attController = AtChannelAttController(self);
    AtAttControllerForceErrorModeSet(attController, errorTypes, cAtAttForceErrorModeContinuous);
    return fForceError(attController, errorTypes);
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerForceError);
    }
static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorUnForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerUnForceError);
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    if (Tha6A290021AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcableErrorsGet(self);

    return cAtSdhLineCounterTypeB1 | cAtSdhLineCounterTypeB2 | cAtSdhLineCounterTypeRei;
    }

static eAtRet TxByteM1Set(AtSdhChannel self, uint8 overheadByteValue)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController((AtChannel)self);
    uint32 regAddr = mMethodsGet(controller)->RealAddress(controller, cAf6Reg_tfmregctl2_Base);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_tfmregctl2_OCNTxMsOhUndefPat_, overheadByteValue);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static uint8 TxByteM1Get(AtSdhChannel self)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController((AtChannel)self);
    uint32 regAddr = mMethodsGet(controller)->RealAddress(controller, cAf6Reg_tfmregctl2_Base);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleSdh);
    return (uint8)mRegField(regVal, cAf6_tfmregctl2_OCNTxMsOhUndefPat_);
    }

static eAtRet TxByteM0Set(AtSdhChannel self, uint8 overheadByteValue)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController((AtChannel)self);
    uint32 regAddr = mMethodsGet(controller)->RealAddress(controller, cReg_tfmregctl);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, c_tfmregctl_M0cfg_, overheadByteValue);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static uint8 TxByteM0Get(AtSdhChannel self)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)AtChannelAttController((AtChannel)self);
    uint32 regAddr = mMethodsGet(controller)->RealAddress(controller, cReg_tfmregctl);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleSdh);
    return (uint8)mRegField(regVal, c_tfmregctl_M0cfg_);
    }

static eAtModuleSdhRet TxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 overheadByteValue)
    {
    eAtSdhLineRate rate = AtSdhLineRateGet((AtSdhLine)self);

    if ((overheadByte == cAtSdhLineMsOverheadByteM0) && (rate == cAtSdhLineRateStm64))
       return TxByteM0Set(self, overheadByteValue);

    if (overheadByte == cAtSdhLineMsOverheadByteM1)
        return TxByteM1Set(self, overheadByteValue);

    if (overheadByte == cAtSdhLineMsOverheadByteUndefined)
        return cAtErrorModeNotSupport;

    return m_AtSdhChannelMethods->TxOverheadByteSet(self, overheadByte, overheadByteValue);
    }

static uint8 TxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    eAtSdhLineRate rate = AtSdhLineRateGet((AtSdhLine)self);

    if ((overheadByte == cAtSdhLineMsOverheadByteM0) && (rate == cAtSdhLineRateStm64))
        return TxByteM0Get(self);

    if (overheadByte == cAtSdhLineMsOverheadByteM1)
        return TxByteM1Get(self);

    return m_AtSdhChannelMethods->TxOverheadByteGet(self, overheadByte);
    }

static eBool TxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    if ((overheadByte == cAtSdhLineMsOverheadByteM0) || 
        (overheadByte == cAtSdhLineMsOverheadByteM1))
        return cAtTrue;

    return m_AtSdhChannelMethods->TxOverheadByteIsSupported(self, overheadByte);
    }

static void OverrideAtObject(AtSdhLine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhLine self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, SubChannelsInterruptDisable);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteSet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteIsSupported);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021SdhLine);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhFacePlateLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha6A290021SdhLineNew(uint32 channelId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLine, channelId, module);
    }

eBool Tha6A290021AttSdhForceLogicFromDut(AtChannel self)
    {
    AtModuleSdh module = (AtModuleSdh)AtChannelModuleGet(self);
    ThaAttSdhManager attManger = (ThaAttSdhManager)AtModuleSdhAttManager(module);
    return ThaAttSdhManagerDebugForcingLogicGet(attManger);
    }

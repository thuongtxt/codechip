/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date: March 28, 2018                                                 
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_RD_MRO10G_ATT_ADJPTR_H_
#define _AF6_REG_AF6CNC0021_RD_MRO10G_ATT_ADJPTR_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : OCN Pointer Adjustment Forcing Registers For Hi-order CEP
Reg Addr   : 0xc0000 - 0xae02f
Reg Formula: 0xc0000 + SliceId*8192 + StsId*1
    Where  : 
           + $SliceId(0-7)
           + $StsId(0-47)
Reg Desc   : 
This is the configuration register for the OCN pointer adjustment forcing for Hi-order CEP side

------------------------------------------------------------------------------*/
#define cAf6Reg_hocepadj                                                                               0xc0000
/*--------------------------------------
BitField Name: rditype
BitField Type: RW
BitField Desc: Forcing type 3: Increment adjustment 4: Decrement adjustment
others: none forcing
BitField Bits: [62:60]
--------------------------------------*/
#define cAf6_hocepadj_rditype_Mask                                                                   cBit30_28
#define cAf6_hocepadj_rditype_Shift                                                                         28

/*--------------------------------------
BitField Name: cfgtimemod
BitField Type: RW
BitField Desc: Config timer mode
BitField Bits: [59:58]
--------------------------------------*/
#define cAf6_hocepadj_cfgtimemod_Mask                                                                cBit27_26
#define cAf6_hocepadj_cfgtimemod_Shift                                                                      26

/*--------------------------------------
BitField Name: cfgclralrm
BitField Type: RW
BitField Desc: Config timer to clear alarm
BitField Bits: [57:50]
--------------------------------------*/
#define cAf6_hocepadj_cfgclralrm_Mask                                                                cBit25_18
#define cAf6_hocepadj_cfgclralrm_Shift                                                                      18

/*--------------------------------------
BitField Name: cfgsetalrm
BitField Type: RW
BitField Desc: Config timer to set alarm
BitField Bits: [49:42]
--------------------------------------*/
#define cAf6_hocepadj_cfgsetalrm_Mask                                                                cBit17_10
#define cAf6_hocepadj_cfgsetalrm_Shift                                                                      10

/*--------------------------------------
BitField Name: cfgerrstep
BitField Type: RW
BitField Desc: Config error step - the gap between 2 errors
BitField Bits: [41:34]
--------------------------------------*/
#define cAf6_hocepadj_cfgerrstep_Mask                                                                  cBit9_2
#define cAf6_hocepadj_cfgerrstep_Shift                                                                       2

/*--------------------------------------
BitField Name: cfgmsk_pos
BitField Type: RW
BitField Desc: Config Mask for 16 positions
BitField Bits: [33:18]
--------------------------------------*/
#define cAf6_hocepadj_cfgmsk_pos_Mask_01                                                             cBit31_18
#define cAf6_hocepadj_cfgmsk_pos_Shift_01                                                                   18
#define cAf6_hocepadj_cfgmsk_pos_Mask_02                                                               cBit1_0
#define cAf6_hocepadj_cfgmsk_pos_Shift_02                                                                    0

/*--------------------------------------
BitField Name: cfgfrc_ena
BitField Type: RW
BitField Desc: Config force enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_hocepadj_cfgfrc_ena_Mask                                                                   cBit17
#define cAf6_hocepadj_cfgfrc_ena_Shift                                                                      17

/*--------------------------------------
BitField Name: cfgerr_num
BitField Type: RW
BitField Desc: Config number of error for error insertion
BitField Bits: [16:1]
--------------------------------------*/
#define cAf6_hocepadj_cfgerr_num_Mask                                                                 cBit16_1
#define cAf6_hocepadj_cfgerr_num_Shift                                                                       1

/*--------------------------------------
BitField Name: cfgfrcmode
BitField Type: RW
BitField Desc: Config force error mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_hocepadj_cfgfrcmode_Mask                                                                    cBit0
#define cAf6_hocepadj_cfgfrcmode_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OCN Pointer Adjustment Forcing Registers For Hi-order Multiplexing From PDH side
Reg Addr   : 0xd0000 - 0xb702f
Reg Formula: 0xd0000 + SliceId*4096 + StsId*1
    Where  : 
           + $SliceId(0-7)
           + $StsId(0-47)
Reg Desc   : 
This is the configuration register for the OCN pointer adjustment forcing for Hi-order multiplexing from PDH side

------------------------------------------------------------------------------*/
#define cAf6Reg_stshoadj                                                                               0xd0000

/*--------------------------------------
BitField Name: rditype
BitField Type: RW
BitField Desc: Forcing type 3: Increment adjustment 4: Decrement adjustment
others: none forcing
BitField Bits: [62:60]
--------------------------------------*/
#define cAf6_stshoadj_rditype_Mask                                                                   cBit30_28
#define cAf6_stshoadj_rditype_Shift                                                                         28

/*--------------------------------------
BitField Name: cfgtimemod
BitField Type: RW
BitField Desc: Config timer mode
BitField Bits: [59:58]
--------------------------------------*/
#define cAf6_stshoadj_cfgtimemod_Mask                                                                cBit27_26
#define cAf6_stshoadj_cfgtimemod_Shift                                                                      26

/*--------------------------------------
BitField Name: cfgclralrm
BitField Type: RW
BitField Desc: Config timer to clear alarm
BitField Bits: [57:50]
--------------------------------------*/
#define cAf6_stshoadj_cfgclralrm_Mask                                                                cBit25_18
#define cAf6_stshoadj_cfgclralrm_Shift                                                                      18

/*--------------------------------------
BitField Name: cfgsetalrm
BitField Type: RW
BitField Desc: Config timer to set alarm
BitField Bits: [49:42]
--------------------------------------*/
#define cAf6_stshoadj_cfgsetalrm_Mask                                                                cBit17_10
#define cAf6_stshoadj_cfgsetalrm_Shift                                                                      10

/*--------------------------------------
BitField Name: cfgerrstep
BitField Type: RW
BitField Desc: Config error step - the gap between 2 errors
BitField Bits: [41:34]
--------------------------------------*/
#define cAf6_stshoadj_cfgerrstep_Mask                                                                  cBit9_2
#define cAf6_stshoadj_cfgerrstep_Shift                                                                       2

/*--------------------------------------
BitField Name: cfgmsk_pos
BitField Type: RW
BitField Desc: Config Mask for 16 positions
BitField Bits: [33:18]
--------------------------------------*/
#define cAf6_stshoadj_cfgmsk_pos_Mask_01                                                             cBit31_18
#define cAf6_stshoadj_cfgmsk_pos_Shift_01                                                                   18
#define cAf6_stshoadj_cfgmsk_pos_Mask_02                                                               cBit1_0
#define cAf6_stshoadj_cfgmsk_pos_Shift_02                                                                    0

/*--------------------------------------
BitField Name: cfgfrc_ena
BitField Type: RW
BitField Desc: Config force enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_stshoadj_cfgfrc_ena_Mask                                                                   cBit17
#define cAf6_stshoadj_cfgfrc_ena_Shift                                                                      17

/*--------------------------------------
BitField Name: cfgerr_num
BitField Type: RW
BitField Desc: Config number of error for error insertion
BitField Bits: [16:1]
--------------------------------------*/
#define cAf6_stshoadj_cfgerr_num_Mask                                                                 cBit16_1
#define cAf6_stshoadj_cfgerr_num_Shift                                                                       1

/*--------------------------------------
BitField Name: cfgfrcmode
BitField Type: RW
BitField Desc: Config force error mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_stshoadj_cfgfrcmode_Mask                                                                    cBit0
#define cAf6_stshoadj_cfgfrcmode_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OCN Pointer Adjustment Forcing Registers For Lo-order Multiplexing From PDH side
Reg Addr   : 0xd8000 - 0xbf5fb
Reg Formula: 0xd8000 + SliceId*4096 + StsId*32 + VtId*1
    Where  : 
           + $SliceId(0-7)
           + $StsId(0-47)
           + $VtId(0-27)
Reg Desc   : 
This is the configuration register for the OCN pointer adjustment forcing for Lo-order multiplexing from PDH side

------------------------------------------------------------------------------*/
#define cAf6Reg_stsloadj                                                                               0xd8000

/*--------------------------------------
BitField Name: rditype
BitField Type: RW
BitField Desc: Forcing type 3: Increment adjustment 4: Decrement adjustment
others: none forcing
BitField Bits: [62:60]
--------------------------------------*/
#define cAf6_stsloadj_rditype_Mask                                                                   cBit30_28
#define cAf6_stsloadj_rditype_Shift                                                                         28

/*--------------------------------------
BitField Name: cfgtimemod
BitField Type: RW
BitField Desc: Config timer mode
BitField Bits: [59:58]
--------------------------------------*/
#define cAf6_stsloadj_cfgtimemod_Mask                                                                cBit27_26
#define cAf6_stsloadj_cfgtimemod_Shift                                                                      26

/*--------------------------------------
BitField Name: cfgclralrm
BitField Type: RW
BitField Desc: Config timer to clear alarm
BitField Bits: [57:50]
--------------------------------------*/
#define cAf6_stsloadj_cfgclralrm_Mask                                                                cBit25_18
#define cAf6_stsloadj_cfgclralrm_Shift                                                                      18

/*--------------------------------------
BitField Name: cfgsetalrm
BitField Type: RW
BitField Desc: Config timer to set alarm
BitField Bits: [49:42]
--------------------------------------*/
#define cAf6_stsloadj_cfgsetalrm_Mask                                                                cBit17_10
#define cAf6_stsloadj_cfgsetalrm_Shift                                                                      10

/*--------------------------------------
BitField Name: cfgerrstep
BitField Type: RW
BitField Desc: Config error step - the gap between 2 errors
BitField Bits: [41:34]
--------------------------------------*/
#define cAf6_stsloadj_cfgerrstep_Mask                                                                  cBit9_2
#define cAf6_stsloadj_cfgerrstep_Shift                                                                       2

/*--------------------------------------
BitField Name: cfgmsk_pos
BitField Type: RW
BitField Desc: Config Mask for 16 positions
BitField Bits: [33:18]
--------------------------------------*/
#define cAf6_stsloadj_cfgmsk_pos_Mask_01                                                             cBit31_18
#define cAf6_stsloadj_cfgmsk_pos_Shift_01                                                                   18
#define cAf6_stsloadj_cfgmsk_pos_Mask_02                                                               cBit1_0
#define cAf6_stsloadj_cfgmsk_pos_Shift_02                                                                    0

/*--------------------------------------
BitField Name: cfgfrc_ena
BitField Type: RW
BitField Desc: Config force enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_stsloadj_cfgfrc_ena_Mask                                                                   cBit17
#define cAf6_stsloadj_cfgfrc_ena_Shift                                                                      17

/*--------------------------------------
BitField Name: cfgerr_num
BitField Type: RW
BitField Desc: Config number of error for error insertion
BitField Bits: [16:1]
--------------------------------------*/
#define cAf6_stsloadj_cfgerr_num_Mask                                                                 cBit16_1
#define cAf6_stsloadj_cfgerr_num_Shift                                                                       1

/*--------------------------------------
BitField Name: cfgfrcmode
BitField Type: RW
BitField Desc: Config force error mode
BitField Bits: [0]
--------------------------------------*/
#define cAf6_stsloadj_cfgfrcmode_Mask                                                                    cBit0
#define cAf6_stsloadj_cfgfrcmode_Shift                                                                       0

#endif /* _AF6_REG_AF6CNC0021_RD_MRO10G_ATT_ADJPTR_H_ */

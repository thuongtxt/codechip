/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive
 * Technologies. The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      :
 *
 * File        :
 *
 * Created Date:
 *
 * Description : This file contain all constance definitions of  block.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef _ATT_REG_AF6CNC0021_MRO10G_ATT_ADJPTR_G783_H_
#define _ATT_REG_AF6CNC0021_MRO10G_ATT_ADJPTR_G783_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : OCN Pointer Adjustment Forcing Registers For Hi-order CEP
Reg Addr   : 0x28400 - 0x2bc2f
Reg Formula: 0x28400 + SliceId*2048 + StsId*1
    Where  :
           + $SliceId(0-3)
           + $StsId(0-47)
Reg Desc   :
This is the configuration register for the OCN pointer adjustment forcing for Hi-order from CEP side

------------------------------------------------------------------------------*/
#define cAttReg_hocepadj_Base                                                                          0x28400
#define cAttReg_hocepadj(SliceId, StsId)\
                                                                      (0x28400UL+(SliceId)*2048UL+(StsId)*1UL)
#define cAttReg_hocepadj_WidthVal                                                                           32
#define cAttReg_hocepadj_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: cfg_frcdtvl
BitField Type: RW
BitField Desc: Config enable force pointer Hi-order Posedge : Enable force
pointer 0       : Disable force pointer
BitField Bits: [9]
--------------------------------------*/
#define cAtt_hocepadj_cfg_frcdtvl_Bit_Start                                                                  9
#define cAtt_hocepadj_cfg_frcdtvl_Bit_End                                                                    9
#define cAtt_hocepadj_cfg_frcdtvl_Mask                                                                   cBit9
#define cAtt_hocepadj_cfg_frcdtvl_Shift                                                                      9
#define cAtt_hocepadj_cfg_frcdtvl_MaxVal                                                                   0x1
#define cAtt_hocepadj_cfg_frcdtvl_MinVal                                                                   0x0
#define cAtt_hocepadj_cfg_frcdtvl_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: cfg_frccont
BitField Type: RW
BitField Desc: Config force continuous 1: Force continuous 0: Force onetimes
BitField Bits: [8]
--------------------------------------*/
#define cAtt_hocepadj_cfg_frccont_Bit_Start                                                                  8
#define cAtt_hocepadj_cfg_frccont_Bit_End                                                                    8
#define cAtt_hocepadj_cfg_frccont_Mask                                                                   cBit8
#define cAtt_hocepadj_cfg_frccont_Shift                                                                      8
#define cAtt_hocepadj_cfg_frccont_MaxVal                                                                   0x1
#define cAtt_hocepadj_cfg_frccont_MinVal                                                                   0x0
#define cAtt_hocepadj_cfg_frccont_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: cfg_frctype
BitField Type: RW
BitField Desc: Config force increment or decrement 3: Force increment 4: Force
decrement Others: Invalid
BitField Bits: [7:5]
--------------------------------------*/
#define cAtt_hocepadj_cfg_frctype_Bit_Start                                                                  5
#define cAtt_hocepadj_cfg_frctype_Bit_End                                                                    7
#define cAtt_hocepadj_cfg_frctype_Mask                                                                 cBit7_5
#define cAtt_hocepadj_cfg_frctype_Shift                                                                      5
#define cAtt_hocepadj_cfg_frctype_MaxVal                                                                   0x7
#define cAtt_hocepadj_cfg_frctype_MinVal                                                                   0x0
#define cAtt_hocepadj_cfg_frctype_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: cfg_frcmode
BitField Type: RW
BitField Desc: Config force mode G783 5'd0 : Mode A 5'd1 : Mode B 5'd2 : Mode C
5'd3 : Mode D 5'd4 : Mode E 5'd5 : Mode F 5'd6 : Invalid 5'd7 : Mode G2 5'd8 :
Mode G3 5'd9 : Mode G4 5'd10: Invalid 5'd11: Mode H2 5'd12: Mode H3 5'd13: Mode
H4 5'd14: Mode I 5'd15: Invalid 5'd16: Mode J2 5'd17: Mode J3 5'd18: Mode J4
5'd31: Mode 4 frames Other: Invalid
BitField Bits: [4:0]
--------------------------------------*/
#define cAtt_hocepadj_cfg_frcmode_Bit_Start                                                                  0
#define cAtt_hocepadj_cfg_frcmode_Bit_End                                                                    4
#define cAtt_hocepadj_cfg_frcmode_Mask                                                                 cBit4_0
#define cAtt_hocepadj_cfg_frcmode_Shift                                                                      0
#define cAtt_hocepadj_cfg_frcmode_MaxVal                                                                  0x1f
#define cAtt_hocepadj_cfg_frcmode_MinVal                                                                   0x0
#define cAtt_hocepadj_cfg_frcmode_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Registers TxPP Configure per STS For Hi-order Multiplexing from PDH side
Reg Addr   : 0xd0800 - 0xd382f
Reg Formula: 0xd0800 + SliceId*4096 + StsId*1
    Where  :
           + $SliceId(0-3)
           + $StsId(0-47)
Reg Desc   :
This is the configuration register for config TxPP per STS for Hi-order multiplexing

------------------------------------------------------------------------------*/
#define cAttReg_lopgramctl_Base                                                                        0xd0800
#define cAttReg_lopgramctl(SliceId, StsId)\
                                                                      (0xd0800UL+(SliceId)*4096UL+(StsId)*1UL)
#define cAttReg_lopgramctl_WidthVal                                                                         32
#define cAttReg_lopgramctl_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: TerStsPgStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master 1: Slaver 0:
Master
BitField Bits: [8]
--------------------------------------*/
#define cAtt_lopgramctl_TerStsPgStsSlvInd_Bit_Start                                                          8
#define cAtt_lopgramctl_TerStsPgStsSlvInd_Bit_End                                                            8
#define cAtt_lopgramctl_TerStsPgStsSlvInd_Mask                                                           cBit8
#define cAtt_lopgramctl_TerStsPgStsSlvInd_Shift                                                              8
#define cAtt_lopgramctl_TerStsPgStsSlvInd_MaxVal                                                           0x1
#define cAtt_lopgramctl_TerStsPgStsSlvInd_MinVal                                                           0x0
#define cAtt_lopgramctl_TerStsPgStsSlvInd_RstVal                                                           0x0

/*--------------------------------------
BitField Name: TerStsPgStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1
BitField Bits: [5:0]
--------------------------------------*/
#define cAtt_lopgramctl_TerStsPgStsMstId_Bit_Start                                                           0
#define cAtt_lopgramctl_TerStsPgStsMstId_Bit_End                                                             5
#define cAtt_lopgramctl_TerStsPgStsMstId_Mask                                                          cBit5_0
#define cAtt_lopgramctl_TerStsPgStsMstId_Shift                                                               0
#define cAtt_lopgramctl_TerStsPgStsMstId_MaxVal                                                           0x3f
#define cAtt_lopgramctl_TerStsPgStsMstId_MinVal                                                            0x0
#define cAtt_lopgramctl_TerStsPgStsMstId_RstVal                                                            0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Pointer Adjustment Forcing Registers For Hi-order Multiplexing From PDH side
Reg Addr   : 0xd0000 - 0xd302f
Reg Formula: 0xd0000 + SliceId*4096 + StsId*1
    Where  :
           + $SliceId(0-3)
           + $StsId(0-47)
Reg Desc   :
This is the configuration register for the OCN pointer adjustment forcing for Hi-order multiplexing from PDH side

------------------------------------------------------------------------------*/
#define cAttReg_stshoadj_Base                                                                          0xd0000
#define cAttReg_stshoadj(SliceId, StsId)\
                                                                      (0xd0000UL+(SliceId)*4096UL+(StsId)*1UL)
#define cAttReg_stshoadj_WidthVal                                                                           32
#define cAttReg_stshoadj_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: cfg_frcdtvl
BitField Type: RW
BitField Desc: Config enable force pointer Hi-order Posedge : Enable force
pointer 0       : Disable force pointer
BitField Bits: [9]
--------------------------------------*/
#define cAtt_stshoadj_cfg_frcdtvl_Bit_Start                                                                  9
#define cAtt_stshoadj_cfg_frcdtvl_Bit_End                                                                    9
#define cAtt_stshoadj_cfg_frcdtvl_Mask                                                                   cBit9
#define cAtt_stshoadj_cfg_frcdtvl_Shift                                                                      9
#define cAtt_stshoadj_cfg_frcdtvl_MaxVal                                                                   0x1
#define cAtt_stshoadj_cfg_frcdtvl_MinVal                                                                   0x0
#define cAtt_stshoadj_cfg_frcdtvl_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: cfg_frccont
BitField Type: RW
BitField Desc: Config force continuous 1: Force continuous 0: Force onetimes
BitField Bits: [8]
--------------------------------------*/
#define cAtt_stshoadj_cfg_frccont_Bit_Start                                                                  8
#define cAtt_stshoadj_cfg_frccont_Bit_End                                                                    8
#define cAtt_stshoadj_cfg_frccont_Mask                                                                   cBit8
#define cAtt_stshoadj_cfg_frccont_Shift                                                                      8
#define cAtt_stshoadj_cfg_frccont_MaxVal                                                                   0x1
#define cAtt_stshoadj_cfg_frccont_MinVal                                                                   0x0
#define cAtt_stshoadj_cfg_frccont_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: cfg_frctype
BitField Type: RW
BitField Desc: Config force increment or decrement 3: Force increment 4: Force
decrement Others: Invalid
BitField Bits: [7:5]
--------------------------------------*/
#define cAtt_stshoadj_cfg_frctype_Bit_Start                                                                  5
#define cAtt_stshoadj_cfg_frctype_Bit_End                                                                    7
#define cAtt_stshoadj_cfg_frctype_Mask                                                                 cBit7_5
#define cAtt_stshoadj_cfg_frctype_Shift                                                                      5
#define cAtt_stshoadj_cfg_frctype_MaxVal                                                                   0x7
#define cAtt_stshoadj_cfg_frctype_MinVal                                                                   0x0
#define cAtt_stshoadj_cfg_frctype_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: cfg_frcmode
BitField Type: RW
BitField Desc: Config force mode G783 5'd0 : Mode A 5'd1 : Mode B 5'd2 : Mode C
5'd3 : Mode D 5'd4 : Mode E 5'd5 : Mode F 5'd6 : Invalid 5'd7 : Mode G2 5'd8 :
Mode G3 5'd9 : Mode G4 5'd10: Invalid 5'd11: Mode H2 5'd12: Mode H3 5'd13: Mode
H4 5'd14: Mode I 5'd15: Invalid 5'd16: Mode J2 5'd17: Mode J3 5'd18: Mode J4
5'd31: Mode 4 frames Other: Invalid
BitField Bits: [4:0]
--------------------------------------*/
#define cAtt_stshoadj_cfg_frcmode_Bit_Start                                                                  0
#define cAtt_stshoadj_cfg_frcmode_Bit_End                                                                    4
#define cAtt_stshoadj_cfg_frcmode_Mask                                                                 cBit4_0
#define cAtt_stshoadj_cfg_frcmode_Shift                                                                      0
#define cAtt_stshoadj_cfg_frcmode_MaxVal                                                                  0x1f
#define cAtt_stshoadj_cfg_frcmode_MinVal                                                                   0x0
#define cAtt_stshoadj_cfg_frcmode_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Pointer Adjustment Forcing Registers For Lo-order Multiplexing From PDH side
Reg Addr   : 0xd8000 - 0xdb5fb
Reg Formula: 0xd8000 + SliceId*4096 + StsId*32 + VtId*1
    Where  :
           + $SliceId(0-3)
           + $StsId(0-47)
           + $VtId(0-27)
Reg Desc   :
This is the configuration register for the OCN pointer adjustment forcing for Lo-order multiplexing from PDH side

------------------------------------------------------------------------------*/
#define cAttReg_stsloadj_Base                                                                          0xd8000
#define cAttReg_stsloadj(SliceId, StsId, VtId)\
                                                          (0xd8000UL+(SliceId)*4096UL+(StsId)*32UL+(VtId)*1UL)
#define cAttReg_stsloadj_WidthVal                                                                           64
#define cAttReg_stsloadj_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: cfg_frcdtvl
BitField Type: RW
BitField Desc: Config enable force pointer Hi-order Posedge : Enable force
pointer 0       : Disable force pointer
BitField Bits: [9]
--------------------------------------*/
#define cAtt_stsloadj_cfg_frcdtvl_Bit_Start                                                                  9
#define cAtt_stsloadj_cfg_frcdtvl_Bit_End                                                                    9
#define cAtt_stsloadj_cfg_frcdtvl_Mask                                                                   cBit9
#define cAtt_stsloadj_cfg_frcdtvl_Shift                                                                      9
#define cAtt_stsloadj_cfg_frcdtvl_MaxVal                                                                   0x1
#define cAtt_stsloadj_cfg_frcdtvl_MinVal                                                                   0x0
#define cAtt_stsloadj_cfg_frcdtvl_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: cfg_frccont
BitField Type: RW
BitField Desc: Config force continuous 1: Force continuous 0: Force onetimes
BitField Bits: [8]
--------------------------------------*/
#define cAtt_stsloadj_cfg_frccont_Bit_Start                                                                  8
#define cAtt_stsloadj_cfg_frccont_Bit_End                                                                    8
#define cAtt_stsloadj_cfg_frccont_Mask                                                                   cBit8
#define cAtt_stsloadj_cfg_frccont_Shift                                                                      8
#define cAtt_stsloadj_cfg_frccont_MaxVal                                                                   0x1
#define cAtt_stsloadj_cfg_frccont_MinVal                                                                   0x0
#define cAtt_stsloadj_cfg_frccont_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: cfg_frctype
BitField Type: RW
BitField Desc: Config force increment or decrement 3: Force increment 4: Force
decrement Others: Invalid
BitField Bits: [7:5]
--------------------------------------*/
#define cAtt_stsloadj_cfg_frctype_Bit_Start                                                                  5
#define cAtt_stsloadj_cfg_frctype_Bit_End                                                                    7
#define cAtt_stsloadj_cfg_frctype_Mask                                                                 cBit7_5
#define cAtt_stsloadj_cfg_frctype_Shift                                                                      5
#define cAtt_stsloadj_cfg_frctype_MaxVal                                                                   0x7
#define cAtt_stsloadj_cfg_frctype_MinVal                                                                   0x0
#define cAtt_stsloadj_cfg_frctype_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: cfg_frcmode
BitField Type: RW
BitField Desc: Config force mode G783 5'd0 : Mode A 5'd1 : Mode B 5'd2 : Mode C
5'd3 : Mode D 5'd4 : Mode E 5'd5 : Mode F 5'd6 : Invalid 5'd7 : Mode G2 5'd8 :
Mode G3 5'd9 : Mode G4 5'd10: Invalid 5'd11: Mode H2 5'd12: Mode H3 5'd13: Mode
H4 5'd14: Mode I 5'd15: Invalid 5'd16: Mode J2 5'd17: Mode J3 5'd18: Mode J4
5'd31: Mode 4 frames Other: Invalid
BitField Bits: [4:0]
--------------------------------------*/
#define cAtt_stsloadj_cfg_frcmode_Bit_Start                                                                  0
#define cAtt_stsloadj_cfg_frcmode_Bit_End                                                                    4
#define cAtt_stsloadj_cfg_frcmode_Mask                                                                 cBit4_0
#define cAtt_stsloadj_cfg_frcmode_Shift                                                                      0
#define cAtt_stsloadj_cfg_frcmode_MaxVal                                                                  0x1f
#define cAtt_stsloadj_cfg_frcmode_MinVal                                                                   0x0
#define cAtt_stsloadj_cfg_frcmode_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Pointer Adjustment Lookup ID For Hi-order CEP Looptime
Reg Addr   : 0xffe00 - 0xffeef
Reg Formula: 0xffe00 + SliceId*64 + StsId*1
    Where  :
           + $SliceId(0-3)
           + $StsId(0-47)
Reg Desc   :
This is the configuration register for pointer adjustment lookup ID for Hi-order CEP looptime

------------------------------------------------------------------------------*/
#define cAttReg_hocepctrl_Base                                                                         0xffe00
#define cAttReg_hocepctrl(SliceId, StsId)\
                                                                        (0xffe00UL+(SliceId)*64UL+(StsId)*1UL)
#define cAttReg_hocepctrl_WidthVal                                                                          32
#define cAttReg_hocepctrl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: lookupid
BitField Type: RW
BitField Desc: StsID from Line side lookup to StsID at Terminated side for
looptime
BitField Bits: [5:0]
--------------------------------------*/
#define cAtt_hocepctrl_lookupid_Bit_Start                                                                    0
#define cAtt_hocepctrl_lookupid_Bit_End                                                                      5
#define cAtt_hocepctrl_lookupid_Mask                                                                   cBit5_0
#define cAtt_hocepctrl_lookupid_Shift                                                                        0
#define cAtt_hocepctrl_lookupid_MaxVal                                                                    0x3f
#define cAtt_hocepctrl_lookupid_MinVal                                                                     0x0
#define cAtt_hocepctrl_lookupid_RstVal                                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN VTTU Pointer Generator Per Channel Control
Reg Addr   : 0x80800 - 0x94fff
Reg Formula: 0x80800 + 16384*LineId + 32*StsId + 4*VtgId + VtId
    Where  :
           + $LineId(0-3)
           + $StsId(0-47):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   :
Each register is used to configure for VTTU pointer Generator engines.
# HDL_PATH       : itfi5ho.itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgctl.array

------------------------------------------------------------------------------*/
#define cAttReg_vpgramctl_Base                                                                         0x80800
#define cAttReg_vpgramctl(LineId, StsId, VtgId, VtId)\
                                                  (0x80800UL+16384UL*(LineId)+32UL*(StsId)+4UL*(VtgId)+(VtId))
#define cAttReg_vpgramctl_WidthVal                                                                          32
#define cAttReg_vpgramctl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: Vtfrcptrinit
BitField Type: RW
BitField Desc: State init and normal 0: Init and normal 1: State force
BitField Bits: [9]
--------------------------------------*/
#define cAtt_vpgramctl_Vtfrcptrinit_Bit_Start                                                                9
#define cAtt_vpgramctl_Vtfrcptrinit_Bit_End                                                                  9
#define cAtt_vpgramctl_Vtfrcptrinit_Mask                                                                 cBit9
#define cAtt_vpgramctl_Vtfrcptrinit_Shift                                                                    9
#define cAtt_vpgramctl_Vtfrcptrinit_MaxVal                                                                 0x1
#define cAtt_vpgramctl_Vtfrcptrinit_MinVal                                                                 0x0
#define cAtt_vpgramctl_Vtfrcptrinit_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: Vtfrcptren
BitField Type: RW
BitField Desc: Enable force pointer 0: Disable 1: Enable
BitField Bits: [8]
--------------------------------------*/
#define cAtt_vpgramctl_Vtfrcptren_Bit_Start                                                                  8
#define cAtt_vpgramctl_Vtfrcptren_Bit_End                                                                    8
#define cAtt_vpgramctl_Vtfrcptren_Mask                                                                   cBit8
#define cAtt_vpgramctl_Vtfrcptren_Shift                                                                      8
#define cAtt_vpgramctl_Vtfrcptren_MaxVal                                                                   0x1
#define cAtt_vpgramctl_Vtfrcptren_MinVal                                                                   0x0
#define cAtt_vpgramctl_Vtfrcptren_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: VtPgBipErrFrc
BitField Type: RW
BitField Desc: Forcing Bip error. 1: Force 0: Not force
BitField Bits: [7]
--------------------------------------*/
#define cAtt_vpgramctl_VtPgBipErrFrc_Bit_Start                                                               7
#define cAtt_vpgramctl_VtPgBipErrFrc_Bit_End                                                                 7
#define cAtt_vpgramctl_VtPgBipErrFrc_Mask                                                                cBit7
#define cAtt_vpgramctl_VtPgBipErrFrc_Shift                                                                   7
#define cAtt_vpgramctl_VtPgBipErrFrc_MaxVal                                                                0x1
#define cAtt_vpgramctl_VtPgBipErrFrc_MinVal                                                                0x0
#define cAtt_vpgramctl_VtPgBipErrFrc_RstVal                                                                0x0

/*--------------------------------------
BitField Name: VtPgLopFrc
BitField Type: RW
BitField Desc: Forcing LOP. 1: Force 0: Not force
BitField Bits: [6]
--------------------------------------*/
#define cAtt_vpgramctl_VtPgLopFrc_Bit_Start                                                                  6
#define cAtt_vpgramctl_VtPgLopFrc_Bit_End                                                                    6
#define cAtt_vpgramctl_VtPgLopFrc_Mask                                                                   cBit6
#define cAtt_vpgramctl_VtPgLopFrc_Shift                                                                      6
#define cAtt_vpgramctl_VtPgLopFrc_MaxVal                                                                   0x1
#define cAtt_vpgramctl_VtPgLopFrc_MinVal                                                                   0x0
#define cAtt_vpgramctl_VtPgLopFrc_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: VtPgUeqFrc
BitField Type: RW
BitField Desc: Forcing FSM to UEQ state. 1: Force 0: Not force
BitField Bits: [5]
--------------------------------------*/
#define cAtt_vpgramctl_VtPgUeqFrc_Bit_Start                                                                  5
#define cAtt_vpgramctl_VtPgUeqFrc_Bit_End                                                                    5
#define cAtt_vpgramctl_VtPgUeqFrc_Mask                                                                   cBit5
#define cAtt_vpgramctl_VtPgUeqFrc_Shift                                                                      5
#define cAtt_vpgramctl_VtPgUeqFrc_MaxVal                                                                   0x1
#define cAtt_vpgramctl_VtPgUeqFrc_MinVal                                                                   0x0
#define cAtt_vpgramctl_VtPgUeqFrc_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: VtPgAisFrc
BitField Type: RW
BitField Desc: Forcing FSM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAtt_vpgramctl_VtPgAisFrc_Bit_Start                                                                  4
#define cAtt_vpgramctl_VtPgAisFrc_Bit_End                                                                    4
#define cAtt_vpgramctl_VtPgAisFrc_Mask                                                                   cBit4
#define cAtt_vpgramctl_VtPgAisFrc_Shift                                                                      4
#define cAtt_vpgramctl_VtPgAisFrc_MaxVal                                                                   0x1
#define cAtt_vpgramctl_VtPgAisFrc_MinVal                                                                   0x0
#define cAtt_vpgramctl_VtPgAisFrc_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: VtPgSSInsPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to insert to Pointer
value.
BitField Bits: [3:2]
--------------------------------------*/
#define cAtt_vpgramctl_VtPgSSInsPatt_Bit_Start                                                               2
#define cAtt_vpgramctl_VtPgSSInsPatt_Bit_End                                                                 3
#define cAtt_vpgramctl_VtPgSSInsPatt_Mask                                                              cBit3_2
#define cAtt_vpgramctl_VtPgSSInsPatt_Shift                                                                   2
#define cAtt_vpgramctl_VtPgSSInsPatt_MaxVal                                                                0x3
#define cAtt_vpgramctl_VtPgSSInsPatt_MinVal                                                                0x0
#define cAtt_vpgramctl_VtPgSSInsPatt_RstVal                                                                0x0

/*--------------------------------------
BitField Name: VtPgSSInsEn
BitField Type: RW
BitField Desc: Enable/disable SS bits insertion. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAtt_vpgramctl_VtPgSSInsEn_Bit_Start                                                                 1
#define cAtt_vpgramctl_VtPgSSInsEn_Bit_End                                                                   1
#define cAtt_vpgramctl_VtPgSSInsEn_Mask                                                                  cBit1
#define cAtt_vpgramctl_VtPgSSInsEn_Shift                                                                     1
#define cAtt_vpgramctl_VtPgSSInsEn_MaxVal                                                                  0x1
#define cAtt_vpgramctl_VtPgSSInsEn_MinVal                                                                  0x0
#define cAtt_vpgramctl_VtPgSSInsEn_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: VtPgPohIns
BitField Type: RW
BitField Desc: Enable/ disable POH Insertion. High to enable insertion of POH.
BitField Bits: [0]
--------------------------------------*/
#define cAtt_vpgramctl_VtPgPohIns_Bit_Start                                                                  0
#define cAtt_vpgramctl_VtPgPohIns_Bit_End                                                                    0
#define cAtt_vpgramctl_VtPgPohIns_Mask                                                                   cBit0
#define cAtt_vpgramctl_VtPgPohIns_Shift                                                                      0
#define cAtt_vpgramctl_VtPgPohIns_MaxVal                                                                   0x1
#define cAtt_vpgramctl_VtPgPohIns_MinVal                                                                   0x0
#define cAtt_vpgramctl_VtPgPohIns_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN STS Pointer Generator Per Channel Control
Reg Addr   : 0x23000 - 0x23e2f
Reg Formula: 0x23000 + 512*LineId + StsId
    Where  :
           + $LineId(0-3)
           + $StsId(0-47)
Reg Desc   :
Each register is used to configure for STS pointer Generator engines.
# HDL_PATH       : itfi5ho.itxpp_stspp_inst.itxpp_stspp[0].txpg_stspgctl.array

------------------------------------------------------------------------------*/
#define cAttReg_spgramctl_Base                                                                         0x23000
#define cAttReg_spgramctl(LineId, StsId)\
                                                                            (0x23000UL+512UL*(LineId)+(StsId))
#define cAttReg_spgramctl_WidthVal                                                                          32
#define cAttReg_spgramctl_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: Stsfrcptrinit
BitField Type: RW
BitField Desc: State init and normal 0: Init and normal 1: State force
BitField Bits: [18]
--------------------------------------*/
#define cAtt_spgramctl_Stsfrcptrinit_Bit_Start                                                              18
#define cAtt_spgramctl_Stsfrcptrinit_Bit_End                                                                18
#define cAtt_spgramctl_Stsfrcptrinit_Mask                                                               cBit18
#define cAtt_spgramctl_Stsfrcptrinit_Shift                                                                  18
#define cAtt_spgramctl_Stsfrcptrinit_MaxVal                                                                0x1
#define cAtt_spgramctl_Stsfrcptrinit_MinVal                                                                0x0
#define cAtt_spgramctl_Stsfrcptrinit_RstVal                                                                0x0

/*--------------------------------------
BitField Name: Stsfrcptren
BitField Type: RW
BitField Desc: Enable force pointer 0: Disable 1: Enable
BitField Bits: [17]
--------------------------------------*/
#define cAtt_spgramctl_Stsfrcptren_Bit_Start                                                                17
#define cAtt_spgramctl_Stsfrcptren_Bit_End                                                                  17
#define cAtt_spgramctl_Stsfrcptren_Mask                                                                 cBit17
#define cAtt_spgramctl_Stsfrcptren_Shift                                                                    17
#define cAtt_spgramctl_Stsfrcptren_MaxVal                                                                  0x1
#define cAtt_spgramctl_Stsfrcptren_MinVal                                                                  0x0
#define cAtt_spgramctl_Stsfrcptren_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: LineStsPgStsSlvInd
BitField Type: RW
BitField Desc: This is used to configure STS is slaver or master. 1: Slaver.
BitField Bits: [16]
--------------------------------------*/
#define cAtt_spgramctl_LineStsPgStsSlvInd_Bit_Start                                                         16
#define cAtt_spgramctl_LineStsPgStsSlvInd_Bit_End                                                           16
#define cAtt_spgramctl_LineStsPgStsSlvInd_Mask                                                          cBit16
#define cAtt_spgramctl_LineStsPgStsSlvInd_Shift                                                             16
#define cAtt_spgramctl_LineStsPgStsSlvInd_MaxVal                                                           0x1
#define cAtt_spgramctl_LineStsPgStsSlvInd_MinVal                                                           0x0
#define cAtt_spgramctl_LineStsPgStsSlvInd_RstVal                                                           0x0

/*--------------------------------------
BitField Name: LineStsPgStsMstId
BitField Type: RW
BitField Desc: This is the ID of the master STS-1 in the concatenation that
contains this STS-1.
BitField Bits: [13:8]
--------------------------------------*/
#define cAtt_spgramctl_LineStsPgStsMstId_Bit_Start                                                           8
#define cAtt_spgramctl_LineStsPgStsMstId_Bit_End                                                            13
#define cAtt_spgramctl_LineStsPgStsMstId_Mask                                                         cBit13_8
#define cAtt_spgramctl_LineStsPgStsMstId_Shift                                                               8
#define cAtt_spgramctl_LineStsPgStsMstId_MaxVal                                                           0x3f
#define cAtt_spgramctl_LineStsPgStsMstId_MinVal                                                            0x0
#define cAtt_spgramctl_LineStsPgStsMstId_RstVal                                                            0x0

/*--------------------------------------
BitField Name: LineStsPgB3BipErrFrc
BitField Type: RW
BitField Desc: Forcing B3 Bip error. 1: Force 0: Not force
BitField Bits: [7]
--------------------------------------*/
#define cAtt_spgramctl_LineStsPgB3BipErrFrc_Bit_Start                                                        7
#define cAtt_spgramctl_LineStsPgB3BipErrFrc_Bit_End                                                          7
#define cAtt_spgramctl_LineStsPgB3BipErrFrc_Mask                                                         cBit7
#define cAtt_spgramctl_LineStsPgB3BipErrFrc_Shift                                                            7
#define cAtt_spgramctl_LineStsPgB3BipErrFrc_MaxVal                                                         0x1
#define cAtt_spgramctl_LineStsPgB3BipErrFrc_MinVal                                                         0x0
#define cAtt_spgramctl_LineStsPgB3BipErrFrc_RstVal                                                         0x0

/*--------------------------------------
BitField Name: LineStsPgLopFrc
BitField Type: RW
BitField Desc: Forcing LOP. 1: Force 0: Not force
BitField Bits: [6]
--------------------------------------*/
#define cAtt_spgramctl_LineStsPgLopFrc_Bit_Start                                                             6
#define cAtt_spgramctl_LineStsPgLopFrc_Bit_End                                                               6
#define cAtt_spgramctl_LineStsPgLopFrc_Mask                                                              cBit6
#define cAtt_spgramctl_LineStsPgLopFrc_Shift                                                                 6
#define cAtt_spgramctl_LineStsPgLopFrc_MaxVal                                                              0x1
#define cAtt_spgramctl_LineStsPgLopFrc_MinVal                                                              0x0
#define cAtt_spgramctl_LineStsPgLopFrc_RstVal                                                              0x0

/*--------------------------------------
BitField Name: LineStsPgUeqFrc
BitField Type: RW
BitField Desc: Forcing FSM to UEQ state. 1: Force 0: Not force
BitField Bits: [5]
--------------------------------------*/
#define cAtt_spgramctl_LineStsPgUeqFrc_Bit_Start                                                             5
#define cAtt_spgramctl_LineStsPgUeqFrc_Bit_End                                                               5
#define cAtt_spgramctl_LineStsPgUeqFrc_Mask                                                              cBit5
#define cAtt_spgramctl_LineStsPgUeqFrc_Shift                                                                 5
#define cAtt_spgramctl_LineStsPgUeqFrc_MaxVal                                                              0x1
#define cAtt_spgramctl_LineStsPgUeqFrc_MinVal                                                              0x0
#define cAtt_spgramctl_LineStsPgUeqFrc_RstVal                                                              0x0

/*--------------------------------------
BitField Name: LineStsPgAisFrc
BitField Type: RW
BitField Desc: Forcing FSM to AIS state. 1: Force 0: Not force
BitField Bits: [4]
--------------------------------------*/
#define cAtt_spgramctl_LineStsPgAisFrc_Bit_Start                                                             4
#define cAtt_spgramctl_LineStsPgAisFrc_Bit_End                                                               4
#define cAtt_spgramctl_LineStsPgAisFrc_Mask                                                              cBit4
#define cAtt_spgramctl_LineStsPgAisFrc_Shift                                                                 4
#define cAtt_spgramctl_LineStsPgAisFrc_MaxVal                                                              0x1
#define cAtt_spgramctl_LineStsPgAisFrc_MinVal                                                              0x0
#define cAtt_spgramctl_LineStsPgAisFrc_RstVal                                                              0x0

/*--------------------------------------
BitField Name: LineStsPgSSInsPatt
BitField Type: RW
BitField Desc: Configure pattern SS bits that is used to insert to pointer
value.
BitField Bits: [3:2]
--------------------------------------*/
#define cAtt_spgramctl_LineStsPgSSInsPatt_Bit_Start                                                          2
#define cAtt_spgramctl_LineStsPgSSInsPatt_Bit_End                                                            3
#define cAtt_spgramctl_LineStsPgSSInsPatt_Mask                                                         cBit3_2
#define cAtt_spgramctl_LineStsPgSSInsPatt_Shift                                                              2
#define cAtt_spgramctl_LineStsPgSSInsPatt_MaxVal                                                           0x3
#define cAtt_spgramctl_LineStsPgSSInsPatt_MinVal                                                           0x0
#define cAtt_spgramctl_LineStsPgSSInsPatt_RstVal                                                           0x0

/*--------------------------------------
BitField Name: LineStsPgSSInsEn
BitField Type: RW
BitField Desc: Enable/disable SS bits insertion. 1: Enable 0: Disable
BitField Bits: [1]
--------------------------------------*/
#define cAtt_spgramctl_LineStsPgSSInsEn_Bit_Start                                                            1
#define cAtt_spgramctl_LineStsPgSSInsEn_Bit_End                                                              1
#define cAtt_spgramctl_LineStsPgSSInsEn_Mask                                                             cBit1
#define cAtt_spgramctl_LineStsPgSSInsEn_Shift                                                                1
#define cAtt_spgramctl_LineStsPgSSInsEn_MaxVal                                                             0x1
#define cAtt_spgramctl_LineStsPgSSInsEn_MinVal                                                             0x0
#define cAtt_spgramctl_LineStsPgSSInsEn_RstVal                                                             0x0

/*--------------------------------------
BitField Name: LineStsPgPohIns
BitField Type: RW
BitField Desc: Enable/disable POH Insertion. High to enable insertion of POH.
BitField Bits: [0]
--------------------------------------*/
#define cAtt_spgramctl_LineStsPgPohIns_Bit_Start                                                             0
#define cAtt_spgramctl_LineStsPgPohIns_Bit_End                                                               0
#define cAtt_spgramctl_LineStsPgPohIns_Mask                                                              cBit0
#define cAtt_spgramctl_LineStsPgPohIns_Shift                                                                 0
#define cAtt_spgramctl_LineStsPgPohIns_MaxVal                                                              0x1
#define cAtt_spgramctl_LineStsPgPohIns_MinVal                                                              0x0
#define cAtt_spgramctl_LineStsPgPohIns_RstVal                                                              0x0

#endif /* _ATT_REG_AF6CNC0021_MRO10G_ATT_ADJPTR_G783_H_ */

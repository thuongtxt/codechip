/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A290021SdhLineAttController.c
 *
 * Created Date: Jul 6, 2016
 *
 * Description : SDH line ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhLine.h"
#include "../../../default/ocn/ThaModuleOcnInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "Tha6A290021SdhAttControllerInternal.h"
#include "Tha6A290021SdhAttReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttControllerMethods                 m_AtAttControllerOverride;
static tThaAttControllerMethods                m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods  m_Tha6A210031PdhDe3AttControllerOverride;
static tTha6A210031SdhLineAttControllerMethods m_Tha6A210031SdhLineAttControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportStep14bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0550);
    }

static uint32 StartVersionSupportDataMask8bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtSdhLineCounterTypeB1||errorType == cAtSdhLineCounterTypeB2)
    	return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0549);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0547);
    }

static uint32 StartVersionSupportAlarmUnitConfig(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0553);
    }

static uint32 StartVersionSupportTwoB2Force(Tha6A210031SdhLineAttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0551);
    }

static uint32 HwStepMaskHo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(isError);

    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_BIPvtcfg_bipvterr_step_Mask;
        return c_upen_B1bytcfg_b1ferr_step_Mask;
        }

    else if (isError == cThaAttForceError)
        {
        if (mMethodsGet(self)->ErrorIsFromAlarm(self,errorType))
            return c_upen_B1bytcfg_b1ferr_step_Mask;

        if (mMethodsGet(self)->IsErrorForceV2(self, errorType))
            {
            if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
                {
                if (ThaAttControllerIsStep14bit((ThaAttController)self, errorType))
                    return cBit22_9; /* Step 14 bits */

                return cBit16_9; /* Step 8 bits */
                }
            return c_upen_BIPvtcfg_bipvterr_step_Mask;
            }
        return c_upen_B1bytcfg_b1ferr_step_Mask;
        }

    return c_upen_B1bytcfg_b1ferr_step_Mask;
    }

static uint32 HwStepMaskLo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(isError);
    return cBit31_0;
    }

static uint32 PositionMaskConfigurationMaskHo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
    	if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
			return c_upen_BIPvtcfg_bipvtmsk_pos_Mask_02;
        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_B1bytcfg_b1fmsk_pos_Mask_02;
        return c_upen_B1bytcfg_b1fmsk_pos_Mask_02;
        }

    if (mMethodsGet(self)->IsErrorForceV2(self, errorType))
    	{
    	if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
			return cBit8_0;
		return c_upen_BIPvtcfg_bipvtmsk_pos_Mask_02;
    	}
    return c_upen_B1bytcfg_b1fmsk_pos_Mask_02;
    }

static uint32 PositionMaskConfigurationMaskLo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
    	if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
    		return c_upen_BIPvtcfg_bipvtmsk_pos_Mask_01;
        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_loscfg_losmsk_pos_Mask_01;
        }

    if (mMethodsGet(self)->IsErrorForceV2(self, errorType))
    	{
    	if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
			return cBit31_25;
		return c_upen_BIPvtcfg_bipvtmsk_pos_Mask_01;
    	}
    return c_upen_B1bytcfg_b1fmsk_pos_Mask_01;
    }

static uint32 DataMaskConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError ==cThaAttForceAlarm)
        {
    	if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
    		return c_upen_BIPvtcfg_bipvtmsk_ena_Mask;
        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_loscfg_losmsk_ena_Mask;
        }

    if (mMethodsGet(self)->IsErrorForceV2(self, errorType))
    	{
    	if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
    		return cBit24_17;
       	return c_upen_BIPvtcfg_bipvtmsk_ena_Mask;
    	}
    return c_upen_B1bytcfg_b1fmsk_ena_Mask;
    }

static uint32 NumberOfErrorsMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError ==cThaAttForceAlarm)
        {
    	if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
    		return c_upen_BIPvtcfg_bipvterr_num_Mask;
        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_loscfg_loserr_num_Mask;
        }

    if (mMethodsGet(self)->IsErrorForceV2(self, errorType))
    	{
    	return c_upen_BIPvtcfg_bipvterr_num_Mask;
    	}

    return c_upen_B1bytcfg_b1ferr_num_Mask;
    }

static uint32 DurationConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(isError);
    AtUnused(errorType);
	return cInvalidUint32;
    }

static eBool ErrorIsFromAlarm(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (mMethodsGet(self)->IsErrorForceV2(self,errorType))
		return cAtFalse;
    if (errorType==cAtSdhLineCounterTypeB1 || errorType==cAtSdhLineCounterTypeB2|| errorType==cAtSdhLineCounterTypeRei)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 HwForceModeConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError ==cThaAttForceAlarm)
        {
    	if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
    		return c_upen_BIPvtcfg_bipvtfrc_mod_Mask;
        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_loscfg_losfrc_mod_Mask;
        }

    return c_upen_B1bytcfg_b1ffrc_mod_Mask;
    }

static eBool  AlarmIsFromError(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    if (alarmType == cAtSdhLineAlarmLos ||
        alarmType == cAtSdhLineAlarmAis ||
        alarmType == cAtSdhLineAlarmLof ||
        alarmType == cAtSdhLineAlarmRdi)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 RegAddressAlarmLos(uint32 lineId, eAtSdhLineRate rate, eBool forceV2)
    {
    uint32 offset = lineId * 256;
    uint32 localAddress = 0;
    if (rate < cAtSdhLineRateStm64)
    	{
    	if (forceV2)
    		{
    		offset = lineId * 32;
    		localAddress =	cReg_upen_loscfg_V2; /* 0x24108 */
    		}
    	else
    		localAddress = cReg_upen_loscfg;
    	}
    else
    	localAddress = cReg_upen_lostm64scfg;

    return offset + localAddress;
    }

static uint32 RegAddressAlarmLof(uint32 lineId, eAtSdhLineRate rate)
    {
    uint32 offset = lineId * 32;
    uint32 localAddress = ((rate < cAtSdhLineRateStm64) ? cReg_upen_lofcfg : cReg_upen_lofstm64cfg);
    return offset + localAddress;
    }

static uint32 RegAddressAlarmAis(uint32 lineId, eAtSdhLineRate rate)
    {
    uint32 offset = lineId * 256;
    uint32 localAddress = ((rate < cAtSdhLineRateStm64) ? cReg_upen_laiscfg : cReg_upen_laisstm64cfg);
    return offset + localAddress;
    }

static uint32 RegAddressAlarmRdi(uint32 lineId, eAtSdhLineRate rate)
    {
    uint32 offset = lineId * 256;
    uint32 localAddress = ((rate < cAtSdhLineRateStm64) ? cReg_upen_lrdicfg : cReg_upen_lrdistm64cfg);
    return offset + localAddress;
    }

static uint32 RegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtSdhLine line = (AtSdhLine)AtAttControllerChannelGet((AtAttController)self);
    uint32 lineId = AtChannelIdGet((AtChannel)line);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    switch (alarmType)
        {
        case cAtSdhLineAlarmLos:
        	{
        	eBool forceV2 =	mMethodsGet(self)->IsAlarmForceV2(self, alarmType);
            return RegAddressAlarmLos(lineId, rate, forceV2);
        	}
        case cAtSdhLineAlarmAis:
            return RegAddressAlarmAis(lineId, rate);
        case cAtSdhLineAlarmLof:
            return RegAddressAlarmLof(lineId, rate);
        case cAtSdhLineAlarmRdi:
            return RegAddressAlarmRdi(lineId, rate);
        default:
            return cInvalidUint32;
        }
    }

static uint32 StatusRegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cBit31_0;
    }

static uint32 RegAddressAlarmUnitLosLof(Tha6A210031PdhDe3AttController self, eAtSdhLineRate rate)
    {
    AtUnused(self);
    return (rate < cAtSdhLineRateStm64)? 0x2411C: 0x240FC;
    }

static uint32 RegAddressAlarmUnitAisRdi(Tha6A210031PdhDe3AttController self, eAtSdhLineRate rate)
    {
    AtUnused(self);
    return (rate < cAtSdhLineRateStm64)? 0x2100E: 0x21811;
    }

static uint32 RegAddressAlarmUnit(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    if (mMethodsGet(self)->IsNeedAlarmUnitConfig(self))
        {
        AtSdhLine line = (AtSdhLine)AtAttControllerChannelGet((AtAttController)self);
        eAtSdhLineRate rate = AtSdhLineRateGet(line);

        switch (alarmType)
           {
            case cAtSdhLineAlarmLos:
            case cAtSdhLineAlarmLof:
                return RegAddressAlarmUnitLosLof(self, rate);
            case cAtSdhLineAlarmAis:
            case cAtSdhLineAlarmRdi:
                return RegAddressAlarmUnitAisRdi(self, rate);
            default:
                return cInvalidUint32;
           }
        }
    return cInvalidUint32;
    }

static uint32 RegAddressErrorB1(uint32 lineId, eAtSdhLineRate rate)
    {
    uint32 offset = lineId * 32;
    uint32 localAddress = ((rate < cAtSdhLineRateStm64) ? cReg_upen_B1bytcfg : cReg_upen_B1bytstm64cfg);
    return offset + localAddress;
    }

static uint32 RegAddressErrorB2(uint32 lineId, eAtSdhLineRate rate)
    {
    uint32 offset = lineId * 256;
    uint32 localAddress = ((rate < cAtSdhLineRateStm64) ? cReg_upen_b2stcfg : cReg_upen_b2ststm64cfg);
    return offset + localAddress;
    }

static uint32 RegAddressErrorRei(uint32 lineId, eAtSdhLineRate rate)
    {
    uint32 offset = lineId * 256;
    uint32 localAddress = ((rate < cAtSdhLineRateStm64) ? cReg_upen_preicfg : cReg_upen_reistm64cfg);
    return offset + localAddress;
    }

static uint32 RegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtSdhLine line = (AtSdhLine)AtAttControllerChannelGet((AtAttController)self);
    uint32 lineId = AtChannelIdGet((AtChannel)line);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    switch (errorType)
        {
        case cAtSdhLineCounterTypeB1:
            return RegAddressErrorB1(lineId, rate);
        case cAtSdhLineCounterTypeB2:
            return RegAddressErrorB2(lineId, rate);
        case cAtSdhLineCounterTypeRei:
            return RegAddressErrorRei(lineId, rate);
        default:
            return cBit31_0;
        }
    }

static uint32 RegAddressFromErrorType2nd(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    if (errorType == cAtSdhLineCounterTypeB2)
        {
        AtSdhLine line      = (AtSdhLine)AtAttControllerChannelGet((AtAttController)self);
        uint32    lineId    = AtChannelIdGet((AtChannel)line);
        eAtSdhLineRate rate = AtSdhLineRateGet(line);
        uint32 offset       = lineId * 256;
        uint32 localAddress = ((rate < cAtSdhLineRateStm64) ? cReg_upen_b2ndcfg : 0x21801);
        return offset + localAddress;
        }
    return cInvalidUint32;
    }

static uint32 RegAddressFromErrorType3rd(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    if (errorType == cAtSdhLineCounterTypeB2)
        {
        AtSdhLine line      = (AtSdhLine)AtAttControllerChannelGet((AtAttController)self);
        uint32    lineId    = AtChannelIdGet((AtChannel)line);
        eAtSdhLineRate rate = AtSdhLineRateGet(line);
        if (rate == cAtSdhLineRateStm1)
            {
            uint32 offset       = lineId * 256;
            uint32 localAddress = cReg_upen_b2rdcfg;
            return offset + localAddress;
            }
        }
    return cInvalidUint32;
    }

static uint32 StatusRegAddressFromErrorType2nd(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cInvalidUint32;
    }

static uint8 ErrorTypeIndex(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhLineCounterTypeB1:
            return 0;
        case cAtSdhLineCounterTypeB2:
            return 1;
        case cAtSdhLineCounterTypeRei:
            return 2;
        default:
            return 0;
        }
    }

static uint32 StatusRegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cBit31_0;
    }

static eAtRet ForceErrorStatusGet(AtAttController self, uint32 errorType, tAtAttForceErrorStatus *status)
    {
	AtUnused(self);
	AtUnused(errorType);
    status->remainForceErrorStatus = 0;
    status->positionMaskStatus     = 0;
    status->errorStatus            = 0;
    status->oneSecondEnableStatus  = 0;
    return cAtOk;
    }

static uint8 NumErrorTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 3;
    }

static eBool ErrorForceIsSupported(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhLineCounterTypeB1:
        case cAtSdhLineCounterTypeB2:
        case cAtSdhLineCounterTypeRei:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static uint32 RealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);

    return regAddr + Tha60210011ModuleOcnBaseAddress(ocnModule);
    }
static AtDevice DeviceGet(AtChannel self)
    {
    return AtChannelDeviceGet(self);
    }

static Tha6A290021ModuleSdh SdhModule(AtChannel self)
    {
    return (Tha6A290021ModuleSdh) AtDeviceModuleGet(DeviceGet(self), cAtModuleSdh);
    }

static AtIpCore IpCore(AtChannel self)
    {
    return AtDeviceIpCoreGet(DeviceGet(self), 0);
    }

static uint16 LongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return Tha60290021ModuleSdhAttLongReadOnCore(SdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel));
    }

static uint16 LongWrite(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return Tha60290021ModuleSdhAttLongWriteOnCore(SdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel));
    }

static uint32 Read(AtAttController self, uint32 regAddr)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return mChannelHwRead(channel, mMethodsGet(att)->RealAddress(att, regAddr), cAtModuleSdh);
    }

static void Write(AtAttController self, uint32 regAddr,uint32 value)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    mChannelHwWrite(channel, mMethodsGet(att)->RealAddress(att, regAddr), value, cAtModuleSdh);
    }

static uint32 AlarmUnitToHwValue(Tha6A210031PdhDe3AttController self, uint32 alarmType, eAlarmUnit unit)
    {
    AtSdhLine      line = (AtSdhLine)AtAttControllerChannelGet((AtAttController)self);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    AtUnused(alarmType);
    if (rate >= cAtSdhLineRateStm64)
        return (unit ==cAtAlarmUnit1ms)? 0x25F7F: 0x38E1;
    return (unit ==cAtAlarmUnit1ms)? 0x25F7F: 0x25D;
    }

static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride, mMethodsGet(self), sizeof(m_AtAttControllerOverride));
        mMethodOverride(m_AtAttControllerOverride, LongRead);
        mMethodOverride(m_AtAttControllerOverride, LongWrite);
        mMethodOverride(m_AtAttControllerOverride, Read);
        mMethodOverride(m_AtAttControllerOverride, Write);
        mMethodOverride(m_AtAttControllerOverride, ErrorForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorStatusGet);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static void OverrideThaAttController(ThaAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(self), sizeof(m_ThaAttControllerOverride));
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportStep14bit);
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportDataMask8bit);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void OverrideTha6A210031PdhDe3AttController(Tha6A210031PdhDe3AttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(self), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PositionMaskConfigurationMaskLo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PositionMaskConfigurationMaskHo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, DataMaskConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumberOfErrorsMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, DurationConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwForceModeConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, ErrorIsFromAlarm);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwStepMaskHo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwStepMaskLo);

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIsFromError);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressAlarmUnit);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType2nd);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType3rd);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromErrorType2nd);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, ErrorTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumErrorTypes);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmUnitConfig);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmUnitToHwValue);
        }

    mMethodsSet(self, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void OverrideTha6A210031SdhLineAttController(Tha6A210031SdhLineAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031SdhLineAttControllerOverride, mMethodsGet(self), sizeof(m_Tha6A210031SdhLineAttControllerOverride));

        mMethodOverride(m_Tha6A210031SdhLineAttControllerOverride, StartVersionSupportTwoB2Force);
        }

    mMethodsSet(self, &m_Tha6A210031SdhLineAttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideAtAttController(self);
    OverrideThaAttController((ThaAttController)self);
    OverrideTha6A210031PdhDe3AttController((Tha6A210031PdhDe3AttController)self);
    OverrideTha6A210031SdhLineAttController((Tha6A210031SdhLineAttController)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021SdhLineAttController);
    }

static AtAttController ObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031SdhLineAttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A290021SdhLineAttControllerNew(AtChannel sdhLine)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(controller, sdhLine);
    }

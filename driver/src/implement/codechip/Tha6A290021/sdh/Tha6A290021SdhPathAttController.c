/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A290021SdhPathAttController.c
 *
 * Created Date: Jul 8, 2016
 *
 * Description : SDH path ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../../default/att/ThaAttSdhManagerInternal.h"
#include "Tha6A290021SdhAttControllerInternal.h"
#include "Tha6A290021SdhAttReg.h"
#include "Tha6A290021SdhAttPointerAdjReg.h"
#include "Tha6A290021SdhAttPointerAdjRegV2.h"
#include "Tha6A290021SdhAttPointerAdjRegV3.h"
/*--------------------------- Define -----------------------------------------*/
#define cAtt6A290021LoPointerForcingStartAddress 0xd8000
#define cAtt6A290021LoPointerForcingEndAddress   0xdb5fb
#define increase 3
#define decrease 4
#define cAtt6A290021PointerForcingDurLoMask  cBit31_10
#define cAtt6A290021PointerForcingDurLoShift 10
#define cAtt6A290021PointerForcingDurHiMask  cBit4_0
#define cAtt6A290021PointerForcingDurHiShift 0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290021SdhPathAttController*)self)
#define mIsForcingAisUneqOptimize(self, alarmType) \
    Tha6A210031PdhDe3AttControllerIsAisUneqOptimizeSupported((Tha6A210031PdhDe3AttController)self, alarmType)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttControllerMethods                m_AtAttControllerOverride;
static tThaAttControllerMethods                m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;

/* To save super implementation */
static const tAtAttControllerMethods                    *m_AtAttControllerMethods       = NULL;
static const tTha6A210031PdhDe3AttControllerMethods     *m_Tha6A210031PdhDe3AttControllerMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static eAtRet HwForcePointerG783(AtAttController self, uint32 pointerType, eBool force);

/*--------------------------- Implementation ---------------------------------*/
static uint32 BipCounterGet(AtAttController self, eBool r2c)
    {
    AtSdhChannel channel   = (AtSdhChannel)AtAttControllerChannelGet(self);
    AtModule     sdh       = AtChannelModuleGet((AtChannel)channel);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    uint8        slice     = 0, hwStsInSlice = 0;
    uint32 counter = 0;
    if (ThaSdhChannel2HwMasterStsId(channel, cAtModuleSdh, &slice, &hwStsInSlice) == cAtOk)
        {
        uint32 regAddr = 0x2839fUL  + 2048UL*slice + Tha60210011ModuleOcnBaseAddress(ocnModule);
        counter =  mModuleHwRead(sdh, regAddr);
        if (r2c)
            mModuleHwWrite(sdh, regAddr, 0);
        }
    return counter;
    }

static uint32 StartVersionSupportConvertStep(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0558);
    }

static uint32 StartVersionSupportDataMask8bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtSdhPathCounterTypeBip)
        return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0556);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportPointerAdjV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0555);
    }

static eBool IsPointerAdjV2(Tha6A210031PdhDe3AttController self)
    {
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaAttControllerVersionReader((ThaAttController) self));
    AtUnused(self);

    if (hwVersion >= mMethodsGet(self)->StartVersionSupportPointerAdjV2(self))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 StartVersionSupportPointerAdj(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0546);
    }

static uint32 HwForceModeConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    if (isError == cThaAttForcePointerAdj)
		return cAf6_hocepadj_cfgfrcmode_Mask;

	return m_Tha6A210031PdhDe3AttControllerMethods->HwForceModeConfigurationMask(self, isError, errorType);
    }

static eAtRet ForcePointerAdj(AtAttController self, uint32 pointerType)
    {
    return HwForcePointerG783(self, pointerType, cAtTrue);
    }

static eAtRet UnForcePointerAdj(AtAttController self, uint32 pointerType)
    {
    return HwForcePointerG783(self, pointerType, cAtFalse);
    }

static tAtOsalCurTime* ForcePointerBeginTimeGet(ThaAttController self)
    {
    return &(mThis(self)->startTimeForcePointer);
    }

static eBool IsHoStsPointerAdj(Tha6A210031PdhDe3AttController self)/* AU4 */
    {/*1d0000*/
    AtChannel channel = AtAttControllerChannelGet((AtAttController)self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)channel);
    if (channelType == cAtSdhChannelTypeAu3)
        {
        AtSdhChannel sub = AtSdhChannelSubChannelGet((AtSdhChannel)channel, 0);
        uint8 mapType = AtSdhChannelMapTypeGet((AtSdhChannel)sub);
        if (mapType == cAtSdhVcMapTypeVc3MapC3)
           return cAtFalse;
        return cAtTrue;
        }
    else if (channelType == cAtSdhChannelTypeVc3)
        {
        AtSdhChannel parent = AtSdhChannelParentChannelGet((AtSdhChannel)channel);
        if  (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeAu3)
            {
            uint8 mapType = AtSdhChannelMapTypeGet((AtSdhChannel)channel);
            if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s||
                mapType == cAtSdhVcMapTypeVc3MapDe3    ||
                mapType == cAtSdhVcMapTypeVc1xMapC1x   ||
                mapType == cAtSdhVcMapTypeVc1xMapDe1 )
                return cAtTrue;
            }
        }
    else if (channelType == cAtSdhChannelTypeAu4)
        {
        AtSdhChannel sub = AtSdhChannelSubChannelGet((AtSdhChannel)channel, 0);
        uint8 mapType = AtSdhChannelMapTypeGet((AtSdhChannel)sub);
        if (mapType == cAtSdhVcMapTypeVc4MapC4)
           return cAtFalse;
        return cAtTrue;
        }
    else if (channelType == cAtSdhChannelTypeVc4)
        {
        uint8 mapType = AtSdhChannelMapTypeGet((AtSdhChannel)channel);
        if (mapType == cAtSdhVcMapTypeVc4Map3xTug3s)
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool IsLoStsPointerAdj(Tha6A210031PdhDe3AttController self)
    {
    AtChannel channel = AtAttControllerChannelGet((AtAttController)self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)channel);
    if (channelType == cAtSdhChannelTypeTu3)
        return cAtTrue;
    else if (channelType == cAtSdhChannelTypeVc3)
        {
        uint8 mapType = AtSdhChannelMapTypeGet((AtSdhChannel)channel); /*(eAtSdhVcMapType)*/
        AtSdhChannel parent = AtSdhChannelParentChannelGet((AtSdhChannel)channel);
        if  (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3)
            return cAtTrue;
        else if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s||
                 mapType == cAtSdhVcMapTypeVc3MapDe3        ||
                 mapType == cAtSdhVcMapTypeVc1xMapC1x   ||
                 mapType == cAtSdhVcMapTypeVc1xMapDe1 )
            return cAtTrue;
        }
    else if (channelType == cAtSdhChannelTypeVc11||
             channelType == cAtSdhChannelTypeVc12)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 PointerAdjTypeMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
	{
	AtUnused(self);
	AtUnused(isError);
	AtUnused(errorType);
	return cAf6_hocepadj_rditype_Mask;
	}

static uint8  SwPointerAdjToHw(Tha6A210031PdhDe3AttController self, uint32 pointerAdjType)
	{
	AtUnused(self);
	if (pointerAdjType==cAtSdhPathPointerAdjTypeIncrease)
		return 3;
	return 4;
	}

static uint32 HwStepMaskHo(Tha6A210031PdhDe3AttController self,uint32 isError,  uint32 errorType)
    {
	AtUnused(self);
	if (isError == cThaAttForcePointerAdj)
		return cAf6_hocepadj_cfgerrstep_Mask;

	return m_Tha6A210031PdhDe3AttControllerMethods->HwStepMaskHo(self, isError, errorType);
    }

static uint32 HwStepMaskLo(Tha6A210031PdhDe3AttController self,uint32 isError,  uint32 errorType)
    {
    AtUnused(self);
    AtUnused(isError);
    AtUnused(errorType);
    return cBit31_0;
    }

static uint32 PositionMaskConfigurationMaskHo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    if (isError == cThaAttForcePointerAdj)
        return cAf6_hocepadj_cfgmsk_pos_Mask_02;

    return m_Tha6A210031PdhDe3AttControllerMethods->PositionMaskConfigurationMaskHo(self, isError, errorType);
    }

static uint32 PositionMaskConfigurationMaskLo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    if (isError == cThaAttForcePointerAdj)
        return cAf6_hocepadj_cfgmsk_pos_Mask_01;

    return m_Tha6A210031PdhDe3AttControllerMethods->PositionMaskConfigurationMaskLo(self, isError, errorType);
    }

static uint32 DataMaskConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
	AtUnused(self);
	if (isError == cThaAttForcePointerAdj)
		return cAf6_hocepadj_cfgfrc_ena_Mask;

	return m_Tha6A210031PdhDe3AttControllerMethods->DataMaskConfigurationMask(self, isError, errorType);

    }

static uint32 NumberOfErrorsMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
	AtUnused(self);
	if (isError == cThaAttForcePointerAdj)
		return cAf6_hocepadj_cfgerr_num_Mask;

	return m_Tha6A210031PdhDe3AttControllerMethods->NumberOfErrorsMask(self, isError, errorType);
    }

static uint32 RegPointerAdj(Tha6A210031PdhDe3AttController self)
	{
    if (IsHoStsPointerAdj(self))
        {
        if (IsPointerAdjV2(self))
            return cAf6Reg_stshoadjV2;
        return cAf6Reg_stshoadj; /* VC3 store 7VTG2, VC3 STORE DE3 */
        }
    else if (IsLoStsPointerAdj(self))
	    {
	    if (IsPointerAdjV2(self))
            return cAf6Reg_stsloadjV2;
	    return cAf6Reg_stsloadj; /* TU3->VC3/E3/DS3 VC11 VC12*/
	    }
    else
        {
        if (IsPointerAdjV2(self))
            return cAf6Reg_hocepadjV2;
        return cAf6Reg_hocepadj; /* VC3, VC4 */
        }
	}

static uint32 RegPointerAdj2nd(Tha6A210031PdhDe3AttController self)
    {
    if (IsPointerAdjV2(self))
       {
       if (IsHoStsPointerAdj(self))
           return cAf6Reg_hocepadjV2;
       else if (IsLoStsPointerAdj(self))
           return cInvalidUint32;
       }
    return cInvalidUint32;
    }

static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0546);
    }

static eBool IsHoVc(Tha6A210031PdhDe3AttController self)
    {
    AtChannel channel = AtAttControllerChannelGet((AtAttController)self);
    eBool hoVc = AtSdhChannelIsHoVc((AtSdhChannel)channel);
    if (!hoVc)
        hoVc = AtSdhChannelIsAu((AtSdhChannel)channel);
    if (AtSdhChannelIsTu((AtSdhChannel)channel))
        hoVc = cAtFalse;
    return hoVc;
    }

static uint32 RegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    eBool hoVc = IsHoVc(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
            if (mIsForcingAisUneqOptimize(self, alarmType))
                return hoVc?cReg_upen_LOPstscfg:cReg_upen_LOPvtcfg;
            return hoVc?cReg_upen_AISstscfg:cReg_upen_AISvtcfg;
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmErdiC:
            return hoVc?cReg_upen_RDIstscfg:cReg_upen_RDIvtcfg;
        case cAtSdhPathAlarmLop:
            return hoVc?cReg_upen_LOPstscfg:cReg_upen_LOPvtcfg;
        case cAtSdhPathAlarmUneq:
            if (mIsForcingAisUneqOptimize(self, alarmType))
                return hoVc?cReg_upen_LOPstscfg:cReg_upen_LOPvtcfg;
            return hoVc?cReg_upen_UEQstscfg:cReg_upen_UEQvtcfg;
        case cAtSdhPathAlarmLom:
            return cReg_upen_LOMstscfg;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtChannel channel = AtAttControllerChannelGet((AtAttController)self);
    eBool hoVc = AtSdhChannelIsHoVc((AtSdhChannel)channel);

    AtUnused(hoVc);
    switch (errorType)
        {
        case cAtSdhPathAlarmAis:
            if (mIsForcingAisUneqOptimize(self, errorType))
                return cReg_upen_LOPstssta;
            return cReg_upen_AISstssta;
        case cAtSdhPathAlarmRdi:
            return cReg_upen_RDIstssta;
        case cAtSdhPathAlarmLop:
            return cReg_upen_LOPstssta;
        case cAtSdhPathAlarmUneq:
            if (mIsForcingAisUneqOptimize(self, errorType))
                return cReg_upen_LOPstssta;
            return cReg_upen_UEQstssta;
        case cAtSdhPathAlarmLom:
            return cReg_upen_LOMstssta;
        default:
            return cBit31_0;
        }
    }

static uint8 AlarmTypeIndex(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
            if (mIsForcingAisUneqOptimize(self, alarmType))
                return 2;
            return 0;
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmErdiC:
            return 1;
        case cAtSdhPathAlarmLop:
            return 2;
        case cAtSdhPathAlarmUneq:
            if (mIsForcingAisUneqOptimize(self, alarmType))
                return 2;
            return 3;
        case cAtSdhPathAlarmLom:
            return 4;
        default:
            return 0;
        }
    }

static uint8 NumAlarmTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 5;
    }

static eBool  AlarmIsFromError(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmErdiC:
        case cAtSdhPathAlarmLop:
        case cAtSdhPathAlarmLom:
            return cAtTrue;
        case cAtSdhPathAlarmAis:
        case cAtSdhPathAlarmUneq:
            if (mIsForcingAisUneqOptimize(self, alarmType))
                return cAtTrue;
            return cAtFalse;

        default: return cAtFalse;
        }
    }

static uint32 RegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    eBool hoVc = IsHoVc(self);

    switch (errorType)
        {
        case cAtSdhPathCounterTypeBip:
            return hoVc?cReg_upen_BIPstscfg:cReg_upen_BIPvtcfg;
        case cAtSdhPathCounterTypeRei:
            return hoVc?cReg_upen_REIstscfg:cReg_upen_REIvtcfg;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathCounterTypeBip:
            return cReg_upen_BIPstssta;
        case cAtSdhPathCounterTypeRei:
            return cReg_upen_REIstssta;
        default:
            return cBit31_0;
        }
    }

static eBool IsAisUneqOptimizeSupported(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    if ((alarmType == cAtSdhPathAlarmLop) ||
        (alarmType == cAtSdhPathAlarmAis) ||
        (alarmType == cAtSdhPathAlarmUneq))
        {
        uint32 startVersionSupport = ThaVersionReaderReleasedDateBuild(18, 12, 12);
        uint32 hwVersion = ThaVersionReaderVersionNumberFromHwGet(ThaAttControllerVersionReader((ThaAttController) self));
        hwVersion = hwVersion >> 8; /* Ignoge version, just take date */
        if (hwVersion >= startVersionSupport)
            return cAtTrue;
        }
    return cAtFalse;
    }

static eAtRet InitAlarmDefault(Tha6A210031PdhDe3AttController self)
    {
    uint32 i;
    eAtRet ret = cAtOk;
    static const uint32 cAtSdhPathAlarmTypes[] = {cAtSdhPathAlarmAis, cAtSdhPathAlarmRdi, cAtSdhPathAlarmLom};
    for (i = 0; i < 3; i++)
        {
        if (AtAttControllerAlarmForceIsSupported((AtAttController)self, cAtSdhPathAlarmTypes[i]))
            {
            ret |= AtAttControllerUnForceAlarm((AtAttController)self, cAtSdhPathAlarmTypes[i]);
            ret |= AtAttControllerForceAlarmModeSet((AtAttController)self, cAtSdhPathAlarmTypes[i], cAtAttForceAlarmModeContinuous);
            ret |= AtAttControllerForceAlarmNumEventSet((AtAttController)self, cAtSdhPathAlarmTypes[i], 1);
            }
        }

    return ret;
    }

static eBool IsAddressBelongedToHoPointerForcing(AtAttController self, uint32 regAddr)
    {
    AtUnused(self);
    if ((regAddr >= 0xd0000) && (regAddr <= 0xd3fff))
        return cAtTrue;

    return cAtFalse;
    }

static ThaModuleOcn ModuleOcn(AtAttController self)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet(channel), cThaModuleOcn);
    }

static uint32 RealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModuleOcn ocnModule = ModuleOcn((AtAttController)self);
    uint8 slice, hwStsInSlice;

    if (ThaSdhChannel2HwMasterStsId(channel, cThaModulePoh, &slice, &hwStsInSlice) != cAtOk)
        return cInvalidUint32;

    if (!IsLoStsPointerAdj(self))
        {
        if (IsHoVc(self))
            {
            uint32 sliceIdCoef = IsAddressBelongedToHoPointerForcing((AtAttController)self, regAddr) ? 4096UL : 2048UL;
            return regAddr + hwStsInSlice + sliceIdCoef * slice + ThaModuleOcnBaseAddress(ocnModule);
            }

        return regAddr + 32UL * hwStsInSlice + slice * 16384UL + ThaModuleOcnBaseAddress(ocnModule);
        }
    else
        {
        if (IsHoVc(self))
            return regAddr + hwStsInSlice + slice * 2048UL  + ThaModuleOcnBaseAddress(ocnModule);

        return regAddr + 32UL * hwStsInSlice + slice * 16384UL + ThaModuleOcnBaseAddress(ocnModule);
        }

    return cInvalidUint32;
    }

static uint32 PointerRealAddressCepHoSts(Tha6A210031PdhDe3AttController self, ThaModuleOcn ocnModule, AtSdhChannel channel, uint32 regAddr, uint8 slice, uint8 hwStsInSlice)
    {
    AtUnused(channel);
    if (IsPointerAdjV2(self))
        return regAddr + hwStsInSlice  + 2048UL * slice + Tha60210011ModuleOcnBaseAddress(ocnModule);

    else
        return regAddr + hwStsInSlice  + 8192UL * slice + Tha60210011ModuleOcnBaseAddress(ocnModule);
    }

static uint32 PointerRealAddressHoSts(Tha6A210031PdhDe3AttController self, ThaModuleOcn ocnModule, AtSdhChannel channel, uint32 regAddr, uint8 slice, uint8 hwStsInSlice)
    {
    AtUnused(self);
    AtUnused(channel);
    return     regAddr + hwStsInSlice     + 4096UL * slice + Tha60210011ModuleOcnBaseAddress(ocnModule);
    }

static uint32 PointerRealAddressLoSts(Tha6A210031PdhDe3AttController self, ThaModuleOcn ocnModule, AtSdhChannel channel, uint32 regAddr, uint8 slice, uint8 hwStsInSlice)
    {
    AtUnused(self);
    AtUnused(channel);
    return     regAddr + 32UL * hwStsInSlice  + 4096UL * slice + Tha60210011ModuleOcnBaseAddress(ocnModule);
    }

static AtSdhChannel Au4SourceGet(Tha6A210031PdhDe3AttController self)
    {
    AtChannel channel = AtAttControllerChannelGet((AtAttController)self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)channel);
    AtSdhChannel terVc     = (AtSdhChannel)channel, ter_au3, ter_au4;
    AtUnused(channelType);
    AtUnused(ter_au3);
    AtUnused(ter_au4);
    if (channelType == cAtSdhChannelTypeAu4||channelType == cAtSdhChannelTypeAu3)
        {
        AtSdhChannel vc4_vc3 = AtSdhChannelSubChannelGet((AtSdhChannel)channel, 0);
        terVc = (AtSdhChannel)AtChannelSourceGet((AtChannel)vc4_vc3);
        }
    if (channelType == cAtSdhChannelTypeVc4||channelType == cAtSdhChannelTypeVc3)
        {
        terVc = (AtSdhChannel)AtChannelSourceGet((AtChannel)channel);
        }
    return terVc;
    }

static uint32 PointerRealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel   = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);

    if (IsHoStsPointerAdj(self)|| IsLoStsPointerAdj(self))
        {
        AtSdhChannel terVc = channel;
        if (!Tha60290021ModuleSdhIsTerminatedVc(channel))
            {
            terVc = (AtSdhChannel)AtChannelSourceGet((AtChannel)terVc);
            if (IsHoStsPointerAdj(self))
                terVc = Au4SourceGet(self);
            if (terVc==NULL)
                terVc     = channel;
            }

        if (ThaSdhChannel2HwMasterStsId(terVc, cThaModulePoh, &slice, &hwStsInSlice)==cAtOk)
            {
            /*AtPrintf("%s slice %d sts %d\r\n", AtObjectToString((AtObject)terVc), slice, hwStsInSlice);*/
            if (IsHoStsPointerAdj(self))
                return PointerRealAddressHoSts(self, ocnModule, terVc, regAddr, slice, hwStsInSlice);
            else if (IsLoStsPointerAdj(self))
                return PointerRealAddressLoSts(self, ocnModule, terVc, regAddr, slice, hwStsInSlice);
            }
        return 0;
        }
    else
        {
        if (ThaSdhChannel2HwMasterStsId(channel, cThaModulePoh, &slice, &hwStsInSlice)==cAtOk)
            return PointerRealAddressCepHoSts(self, ocnModule, channel, regAddr, slice, hwStsInSlice);
        return 0;
        }

    return cInvalidUint32;
    }

static uint32 PointerRealAddress2nd(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel   = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);

    if (IsHoStsPointerAdj(self))
        {
        AtSdhChannel terVc = channel;
        if (Tha60290021ModuleSdhIsTerminatedVc(channel))
            {
            terVc = (AtSdhChannel)AtChannelSourceGet((AtChannel)terVc);
            terVc = Au4SourceGet(self);
            if (terVc==NULL)
               terVc     = channel;
            }

        if (ThaSdhChannel2HwMasterStsId(terVc, cThaModulePoh, &slice, &hwStsInSlice)==cAtOk)
            {
            return PointerRealAddressCepHoSts(self, ocnModule, channel, regAddr, slice, hwStsInSlice);
            }
        return 0;
        }
    return cInvalidUint32;
    }

static AtDevice DeviceGet(AtChannel self)
    {
    return AtChannelDeviceGet(self);
    }

static Tha6A290021ModuleSdh SdhModule(AtChannel self)
    {
    return (Tha6A290021ModuleSdh) AtDeviceModuleGet(DeviceGet(self), cAtModuleSdh);
    }

static AtIpCore IpCore(AtChannel self)
    {
    return AtDeviceIpCoreGet(DeviceGet(self), 0);
    }

static uint16 LongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return Tha60290021ModuleSdhAttLongReadOnCore(SdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel));
    }

static uint16 LongWrite(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return Tha60290021ModuleSdhAttLongWriteOnCore(SdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel));
    }

static uint16 PointerLongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return Tha60290021ModuleSdhAttLongReadOnCore(SdhModule(channel), mMethodsGet(att)->PointerRealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel));
    }

static uint16 PointerLongWrite(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return Tha60290021ModuleSdhAttLongWriteOnCore(SdhModule(channel), mMethodsGet(att)->PointerRealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel));
    }

static uint16 PointerLongWrite2nd(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return Tha60290021ModuleSdhAttLongWriteOnCore(SdhModule(channel), mMethodsGet(att)->PointerRealAddress2nd(att, regAddr), dataBuffer, bufferLen, IpCore(channel));
    }

static uint32 Read(AtAttController self, uint32 regAddr)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    return mChannelHwRead(channel, mMethodsGet(att)->RealAddress(att, regAddr), cAtModuleSdh);
    }

static void Write(AtAttController self, uint32 regAddr,uint32 value)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    mChannelHwWrite(channel, mMethodsGet(att)->RealAddress(att, regAddr), value, cAtModuleSdh);
    }

static eBool AlarmForceIsSupported(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiC:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmLop:
        case cAtSdhPathAlarmUneq:
        case cAtSdhPathAlarmLom:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static uint32 ForcePointerAddress(AtAttController self)
    {
    AtUnused(self);
    return IsLoStsPointerAdj((Tha6A210031PdhDe3AttController)self) ? cAttReg_stsloadj_Base : cAttReg_hocepadj_Base;
    }

static void ForcePointerDurSw2Hw(uint32 swDur, uint32 *durLo, uint32 *durHi)
    {
    uint32 duration = swDur * 8000 - 1;
    *durLo = duration & cBit21_0;
    mFieldGet(duration, cBit26_22, 22, uint32, durHi);
    }

static uint32 ForcePointerDurHw2Sw(uint32 durLo, uint32 durHi)
    {
    return ((durLo + (durHi << 22)) + 1) / 8000;
    }

static eAtRet HwForcePointerAdjDurationSet(AtAttController self, uint32 duration, uint32 regAddr)
    {
    uint32 durHi, durLo;
    uint32 longRegVal[4];
    mMethodsGet(self)->PointerLongRead(self, regAddr, longRegVal, 4);
    ForcePointerDurSw2Hw(duration, &durLo, &durHi);

    mRegFieldSet(longRegVal[0], cAtt6A290021PointerForcingDurLo, durLo);
    mRegFieldSet(longRegVal[1], cAtt6A290021PointerForcingDurHi, durHi);
    mMethodsGet(self)->PointerLongWrite(self, regAddr, longRegVal, 4);

    return cAtOk;
    }

static eAtRet ForcePointerAdjDurationSet(AtAttController self, uint32 pointerType, uint32 duration)
    {
    AtUnused(pointerType);
    HwForcePointerAdjDurationSet(self, duration, ForcePointerAddress(self));
    if (IsLoStsPointerAdj((Tha6A210031PdhDe3AttController)self) == cAtFalse)
        HwForcePointerAdjDurationSet(self, duration, cAttReg_stshoadj_Base);

    return cAtOk;
    }

static uint32 ForcePointerAdjDurationGet(AtAttController self, uint32 pointerType)
    {
    uint32 durHi, durLo;
    uint32 regAddr = ForcePointerAddress(self);
    uint32 longRegVal[4];
    AtUnused(pointerType);
    mMethodsGet(self)->PointerLongRead(self, regAddr, longRegVal, 4);

    durLo = mRegField(longRegVal[0], cAtt6A290021PointerForcingDurLo);
    durHi = mRegField(longRegVal[1], cAtt6A290021PointerForcingDurHi);
    return ForcePointerDurHw2Sw(durLo, durHi);
    }

static uint8 ForcePointerModeSw2Hw(uint8 mode)
    {
    switch (mode)
        {
        case cAtAttForcePointerAdjModeG783a:
            return 0;
        case cAtAttForcePointerAdjModeG783b:
            return 1;
        case cAtAttForcePointerAdjModeG783c:
            return 2;
        case cAtAttForcePointerAdjModeG783d:
            return 3;
        case cAtAttForcePointerAdjModeG783e:
            return 4;
        case cAtAttForcePointerAdjModeG783f:
            return 5;
        case cAtAttForcePointerAdjModeG783gPart2:
            return 7;
        case cAtAttForcePointerAdjModeG783gPart3:
            return 8;
        case cAtAttForcePointerAdjModeG783gPart4:
            return 9;
        case cAtAttForcePointerAdjModeG783hPartb:
            return 11;
        case cAtAttForcePointerAdjModeG783hPartc:
            return 12;
        case cAtAttForcePointerAdjModeG783hPartd:
            return 13;
        case cAtAttForcePointerAdjModeG783i:
            return 14;
        case cAtAttForcePointerAdjModeG783jPartb:
            return 16;
        case cAtAttForcePointerAdjModeG783jPartc:
            return 17;
        case cAtAttForcePointerAdjModeG783jPartd:
            return 18;
        case cAtAttForcePointerAdjModeG7834frames:
            return 31;
        default:
            return 6; /*Invalid value*/
        }
    }

static eAtAttForcePointerAdjMode PointerAdjModeHw2Sw(uint8 mode)
    {
    switch (mode)
        {
        case 0:
            return cAtAttForcePointerAdjModeG783a;
        case 1:
            return cAtAttForcePointerAdjModeG783b;
        case 2:
            return cAtAttForcePointerAdjModeG783c;
        case 3:
            return cAtAttForcePointerAdjModeG783d;
        case 4:
            return cAtAttForcePointerAdjModeG783e;
        case 5:
            return cAtAttForcePointerAdjModeG783f;
        case 7:
            return cAtAttForcePointerAdjModeG783gPart2;
        case 8:
            return cAtAttForcePointerAdjModeG783gPart3;
        case 9:
            return cAtAttForcePointerAdjModeG783gPart4;
        case 11:
            return cAtAttForcePointerAdjModeG783hPartb;
        case 12:
            return cAtAttForcePointerAdjModeG783hPartc;
        case 13:
            return cAtAttForcePointerAdjModeG783hPartd;
        case 14:
            return cAtAttForcePointerAdjModeG783i;
        case 16:
            return cAtAttForcePointerAdjModeG783jPartb;
        case 17:
            return cAtAttForcePointerAdjModeG783jPartc;
        case 18:
            return cAtAttForcePointerAdjModeG783jPartd;
        case 31:
            return cAtAttForcePointerAdjModeG7834frames;

        default:
            return cAtAttForcePointerAdjModeG7834invalid; /*Invalid value*/
        }
    }

static eAtRet ForcePointerAdjModeSet(AtAttController self, uint32 pointerType, eAtAttForcePointerAdjMode mode)
    {
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    uint32 regVal, regAddr;
    uint32 typeOfpointer = (pointerType == 0)? increase: decrease;
    uint8 continuousMode = (mode == cAtAttForcePointerAdjModeContinuous)? cAtTrue : cAtFalse;
    uint8 oneshotMode = (mode == cAtAttForcePointerAdjModeOneshot)? cAtTrue : cAtFalse;
    uint8 hwMode = ForcePointerModeSw2Hw(mode);
    if(!IsLoStsPointerAdj((Tha6A210031PdhDe3AttController)self))
    {
        regAddr =cAttReg_hocepadj_Base;
        regVal = Read(self, regAddr);
        if((!continuousMode)&&(!oneshotMode))
            {
            mRegFieldSet(regVal, cAtt_hocepadj_cfg_frcmode_, hwMode);
            mRegFieldSet(regVal, cAtt_hocepadj_cfg_frccont_, cAtFalse);/*one shot mode*/
            }
        else
            {
            mRegFieldSet(regVal, cAtt_hocepadj_cfg_frccont_, continuousMode); /*TODO: Still not support cAtAttForcePointerAdjModeNeventInT and cAtAttForcePointerAdjModeConsecutive modes*/
            }
        mRegFieldSet(regVal, cAtt_hocepadj_cfg_frctype_, typeOfpointer);
        Write(self, regAddr, regVal);

        regAddr =cAttReg_stshoadj_Base;
        regVal = Read(self, regAddr);
        if(!continuousMode && !oneshotMode)
            {
            mRegFieldSet(regVal, cAtt_stshoadj_cfg_frcmode_, hwMode);
            mRegFieldSet(regVal, cAtt_stshoadj_cfg_frccont_, cAtFalse);/*one shot mode*/
            }
        else
            {
            mRegFieldSet(regVal, cAtt_stshoadj_cfg_frccont_, continuousMode); /*TODO: Still not support cAtAttForcePointerAdjModeNeventInT and cAtAttForcePointerAdjModeConsecutive modes*/
            }

        mRegFieldSet(regVal, cAtt_stshoadj_cfg_frctype_, continuousMode);
        Write(self, regAddr, regVal);

        regAddr =cAttReg_lopgramctl_Base;
        regVal = Read(self, regAddr);
        mRegFieldSet(regVal, cAtt_lopgramctl_TerStsPgStsSlvInd_, cAtFalse);/*Set master*/
        ThaSdhChannel2HwMasterStsId(channel, cThaModulePoh, &slice, &hwStsInSlice);
        mRegFieldSet(regVal, cAtt_lopgramctl_TerStsPgStsMstId_, hwStsInSlice);/*ID of the master STS-1 in the concatenation*/
        Write(self, regAddr, regVal);
        }
    else
        {
        regAddr =cAttReg_stsloadj_Base;
        regVal = Read(self, regAddr);
        if((!continuousMode)&&(!oneshotMode))
            {
            mRegFieldSet(regVal, cAtt_stsloadj_cfg_frcmode_, hwMode);
            mRegFieldSet(regVal, cAtt_stsloadj_cfg_frccont_, cAtFalse);/*one shot mode*/
            }
        else
            {
            mRegFieldSet(regVal, cAtt_stsloadj_cfg_frccont_, continuousMode); /*TODO: Still not support cAtAttForcePointerAdjModeNeventInT and cAtAttForcePointerAdjModeConsecutive modes*/
            }
        mRegFieldSet(regVal, cAtt_stsloadj_cfg_frctype_, typeOfpointer);
        Write(self, regAddr, regVal);

        return cAtOk;

        }

    return cAtOk;
    }

static eAtAttForcePointerAdjMode ForcePointerAdjModeGet(AtAttController self, uint32 pointerType)
    {
    eAtAttForcePointerAdjMode forcePointerAdjMode;
    if(!IsLoStsPointerAdj((Tha6A210031PdhDe3AttController)self))
        {
        uint32 regVal, regAddr;
        regAddr = cAttReg_hocepadj_Base;
        AtUnused(pointerType);
        regVal = Read(self, regAddr);
        forcePointerAdjMode = PointerAdjModeHw2Sw(mRegField(regVal, cAtt_hocepadj_cfg_frcmode_));
        }
    else
        {
        uint32 regVal, regAddr;
        regAddr = cAttReg_stsloadj_Base;
        AtUnused(pointerType);
        regVal = Read(self, regAddr);
        forcePointerAdjMode = PointerAdjModeHw2Sw(mRegField(regVal, cAtt_stsloadj_cfg_frcmode_));
        }

    return forcePointerAdjMode;
    }

static eAtRet HwForcePointerG783(AtAttController self, uint32 pointerType, eBool force)
    {
    if(!IsLoStsPointerAdj((Tha6A210031PdhDe3AttController)self))
        {
        uint32 regVal, regAddr;
        AtUnused(pointerType);
        regAddr =cAttReg_hocepadj_Base;
        regVal = Read(self, regAddr);
        mRegFieldSet(regVal, cAtt_hocepadj_cfg_frcdtvl_, mBoolToBin(force));
        Write(self, regAddr, regVal);

        regAddr =cAttReg_stshoadj_Base;
        regVal = Read(self, regAddr);
        mRegFieldSet(regVal, cAtt_stshoadj_cfg_frcdtvl_, mBoolToBin(force));
        Write(self, regAddr, regVal);
        }
    else
        {
        uint32 regVal, regAddr;
        AtUnused(pointerType);
        regAddr =cAttReg_stsloadj_Base;

        regVal = Read(self, regAddr);
        mRegFieldSet(regVal, cAtt_stsloadj_cfg_frcdtvl_, mBoolToBin(force));
        Write(self, regAddr, regVal);
        }

    return cAtOk;
    }

/*
 * TODO: it is better if we have AtSdhPathAttController
 *
 * So we can be easy provide methods of
 *  + AtSdhPathAttControllerPointerForce()
 *  + AtSdhPathAttControllerPointerInit()
 */

static eAtRet StsPointerForce(AtAttController self, eBool forced)
    {
    uint32  regVal;

    regVal = Read(self, cAttReg_spgramctl_Base);
    mRegFieldSet(regVal, cAtt_spgramctl_Stsfrcptren_, forced ? 1 : 0);
    Write(self, cAttReg_spgramctl_Base, regVal);

    return cAtOk;
    }

static eAtRet VtPointerForce(AtAttController self, eBool forced)
    {
    uint32  regVal;

    regVal = Read(self, cAttReg_vpgramctl_Base);
    mRegFieldSet(regVal, cAtt_vpgramctl_Vtfrcptren_, forced ? 1 : 0);
    Write(self, cAttReg_vpgramctl_Base, regVal);

    return cAtOk;
    }

static eAtRet StsPointerInit(AtAttController self)
    {
    uint32  regVal;

    regVal = Read(self, cAttReg_spgramctl_Base);
    mRegFieldSet(regVal, cAtt_spgramctl_Stsfrcptrinit_, 1);
    Write(self, cAttReg_spgramctl_Base, regVal);

    AtOsalUSleep(1000); /* 8 frames */

    mRegFieldSet(regVal, cAtt_spgramctl_Stsfrcptrinit_, 0);
    Write(self, cAttReg_spgramctl_Base, regVal);

    return cAtOk;
    }

static eAtRet VtPointerInit(AtAttController self)
    {
    uint32  regVal;

    regVal = Read(self, cAttReg_vpgramctl_Base);
    mRegFieldSet(regVal, cAtt_vpgramctl_Vtfrcptrinit_, 1);
    Write(self, cAttReg_vpgramctl_Base, regVal);

    AtOsalUSleep(4000); /* 8 multiframes */

    mRegFieldSet(regVal, cAtt_vpgramctl_Vtfrcptrinit_, 0);
    Write(self, cAttReg_vpgramctl_Base, regVal);

    return cAtOk;
    }

static eAtRet PointerInit(AtAttController self)
    {
    eAtRet ret = cAtOk;

    if (IsLoStsPointerAdj((Tha6A210031PdhDe3AttController)self))
        {
        ret |= VtPointerForce(self, cAtTrue);
        ret |= VtPointerInit(self);
        ret |= VtPointerForce(self, cAtFalse);
        }
    else
        {
        ret |= StsPointerForce(self, cAtTrue);
        ret |= StsPointerInit(self);
        ret |= StsPointerForce(self, cAtFalse);
        }

    return ret;
    }

static eAtRet Init(AtAttController self)
    {
    eAtRet ret = m_AtAttControllerMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return PointerInit(self);
    }

static eBool AlarmIsSpecialRdi(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtTrue;
    }

static eAtRet FrequenceOffsetPointerInit(AtAttController self, float ppm)
    {
    /* Set status Vt pointer and Sts pointer are normal when force ppm */
    eBool needToResetPointer = ((ppm < 0) || (ppm > 0)) ? cAtTrue : cAtFalse;

    if (needToResetPointer)
        return PointerInit(self);
    return cAtOk;
    }

static eAtRet FrequenceOffsetSet(AtAttController self, float ppm)
    {
    eAtRet ret = Tha6A290021SdhPathPointerInit(self, ppm);
    ret |= m_AtAttControllerMethods->FrequenceOffsetSet(self, ppm);
    return ret;
    }

static eAtRet StsPointerAdjEnable(AtAttController self, eBool enable)
    {
    uint32 regVal = Read(self, cAttReg_spgramctl_Base);
    mRegFieldSet(regVal, cAtt_spgramctl_Stsfrcptrinit_, mBoolToBin(enable));
    Write(self, cAttReg_spgramctl_Base, regVal);

    return cAtOk;
    }

static eAtRet VtPointerAdjEnable(AtAttController self, eBool enable)
    {
    uint32 regVal = Read(self, cAttReg_vpgramctl_Base);
    mRegFieldSet(regVal, cAtt_vpgramctl_Vtfrcptrinit_, mBoolToBin(enable));
    Write(self, cAttReg_vpgramctl_Base, regVal);

    return cAtOk;
    }

static eAtRet PointerAdjEnable(AtAttController self, eBool enable)
    {
    eAtRet ret = cAtOk;

    if (IsLoStsPointerAdj((Tha6A210031PdhDe3AttController)self))
        ret |= VtPointerAdjEnable(self, enable);
    else
        ret |= StsPointerAdjEnable(self, enable);

    return ret;
    }

static uint8  HwRdiType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
   /* ERDI disabled
    * 1        0        0  RDI
    * 0        0        0  No RDI
    *
    * ERDI enabled
    * 0        0        1  No RDI
    * 0        1        0  E-RDI payload defect  PLM
    * 1        0        1  E-RDI server defect  AIS, LOP
    * 1        1        0  E-RDI connectivity defect
    */

    AtSdhPath path = (AtSdhPath)AtAttControllerChannelGet((AtAttController)self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmRdi:    return 0x4;
        case cAtSdhPathAlarmErdiC:  return 0x6;
        case cAtSdhPathAlarmErdiS:  return 0x5;
        case cAtSdhPathAlarmErdiP:  return 0x2;
        default:
            return (AtSdhPathERdiIsEnabled(path)) ? 0x1 : 0x0;
        }
    }

static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtAttControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride, m_AtAttControllerMethods, sizeof(m_AtAttControllerOverride));
        mMethodOverride(m_AtAttControllerOverride, Init);
        mMethodOverride(m_AtAttControllerOverride, LongRead);
        mMethodOverride(m_AtAttControllerOverride, LongWrite);
        mMethodOverride(m_AtAttControllerOverride, PointerLongRead);
        mMethodOverride(m_AtAttControllerOverride, PointerLongWrite);
        mMethodOverride(m_AtAttControllerOverride, PointerLongWrite2nd);
        mMethodOverride(m_AtAttControllerOverride, Read);
        mMethodOverride(m_AtAttControllerOverride, Write);
        mMethodOverride(m_AtAttControllerOverride, AlarmForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, BipCounterGet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjModeSet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjModeGet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdj);
        mMethodOverride(m_AtAttControllerOverride, UnForcePointerAdj);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjDurationSet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjDurationGet);
        mMethodOverride(m_AtAttControllerOverride, FrequenceOffsetSet);
        mMethodOverride(m_AtAttControllerOverride, PointerAdjEnable);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static void OverrideThaAttController(ThaAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(self), sizeof(m_ThaAttControllerOverride));
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportDataMask8bit);
        mMethodOverride(m_ThaAttControllerOverride, ForcePointerBeginTimeGet);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void OverrideTha6A210031PdhDe3AttController(Tha6A210031PdhDe3AttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6A210031PdhDe3AttControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, m_Tha6A210031PdhDe3AttControllerMethods, sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PointerRealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PointerRealAddress2nd);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumAlarmTypes);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIsFromError);

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, InitAlarmDefault);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegPointerAdj);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegPointerAdj2nd);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwStepMaskHo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwStepMaskLo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PositionMaskConfigurationMaskHo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PositionMaskConfigurationMaskLo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumberOfErrorsMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, DataMaskConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, SwPointerAdjToHw);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PointerAdjTypeMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwForceModeConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportPointerAdj);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportPointerAdjV2);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportConvertStep);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, IsAisUneqOptimizeSupported);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIsSpecialRdi);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwRdiType);
        }

    mMethodsSet(self, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideAtAttController(self);
    OverrideThaAttController((ThaAttController) self);
    OverrideTha6A210031PdhDe3AttController((Tha6A210031PdhDe3AttController)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021SdhPathAttController);
    }

static AtAttController ObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031SdhPathAttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A290021SdhPathAttControllerNew(AtChannel sdhPath)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(controller, sdhPath);
    }

eAtRet Tha6A290021SdhPathPointerInit(AtAttController self, float ppm)
    {
    if (self)
        return FrequenceOffsetPointerInit(self, ppm);
    return cAtErrorNullPointer;
    }

eAtRet Tha6A290021SdhPathPointerPointerAdjEnable(AtAttController self, eBool enable)
    {
    if (self)
        return PointerAdjEnable(self, enable);
    return cAtErrorNullPointer;
    }


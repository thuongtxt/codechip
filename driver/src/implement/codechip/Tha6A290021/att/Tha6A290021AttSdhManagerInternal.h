/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6A290021AttSdhManagerInternal.h
 * 
 * Created Date: Macth 14, 2018
 *
 * Author      : chaudpt
 *
 * Description : ATT SDH Manager of 6A290021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021ATTSDHMANAGERINTERNAL_H_
#define _THA6A290021ATTSDHMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A210031/att/Tha6A210031AttSdhManagerInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha6A290021AttSdhManager* Tha6A290021AttSdhManager;
typedef struct tTha6A290021AttSdhManager
    {
    tTha6A210031AttSdhManager super;
    }tTha6A290021AttSdhManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttSdhManager Tha6A290021AttSdhManagerObjectInit(AtAttSdhManager self, AtModuleSdh sdh);

/* Debugger */

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021ATTSDHMANAGERINTERNAL_H_ */


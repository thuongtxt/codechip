/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A290021AttPdhManager.c
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : Default implementation of PDH module of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290021AttPdhManagerInternal.h"
#include "../pdh/Tha6A290021ModulePdhInternal.h"
#include "../pdh/Tha6A290021PdhDe1AttReg.h"
#include "../pdh/Tha6A290021PdhDe3AttReg.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A290021AttPdhManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttPdhManagerMethods m_AtAttPdhManagerOverride;
/* Save super implementations */


/*--------------------------- Forward declarations ---------------------------*/
uint16 Tha60290021ModulePdhAttLongReadOnCore(Tha6A290021ModulePdh self,
		                                     uint32 localAddress,
											 uint32 *dataBuffer,
											 uint16 bufferLen,
											 AtIpCore core,
											 uint32 holdRegOffset);
uint16 Tha60290021ModulePdhAttLongWriteOnCore(Tha6A290021ModulePdh self,
		                                      uint32 localAddress,
											  const uint32 *dataBuffer,
											  uint16 bufferLen,
											  AtIpCore core,
											  uint32 holdRegOffset);
/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtIpCore IpCore(AtAttPdhManager self)
    {
    AtModule pdh = (AtModule) AtAttPdhManagerModulePdh(self);
    return AtDeviceIpCoreGet(AtModuleDeviceGet(pdh), 0);
    }

static eAtRet De3AttInit(AtAttPdhManager self)
    {
    Tha6A290021ModulePdh pdh = (Tha6A290021ModulePdh) AtAttPdhManagerModulePdh(self);
    uint32       slice = 0;
    uint32       dataBuffer[2]={0,0};
    uint32       holdRegOffset=0;
    for (slice=0; slice <ThaModulePdhNumSlices((ThaModulePdh)pdh); slice++)
        {
        uint32   baseAddress = ThaModulePdhSliceBase((ThaModulePdh)pdh, slice);
        uint32   id = 0;
        for (id=0; id <= 0x17; id++)
            {
            holdRegOffset = 0x1000000 + 0x100000 * slice;
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cReg_upen_cpb_sta    +baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset);
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cReg_upen_rdi_sta    +baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset);
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cReg_upen_pbit_sta   +baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset);
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cReg_upen_faB_sta    +baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset);/* RDI */
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cReg_upen_bip8_sta   +baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset);/* */
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cReg_upen_rei_sta    +baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset); /* FA Byte */
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cReg_upen_gcB_sta    +baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset); /* BIP8 */
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cReg_upen_nrB_sta    +baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset); /* BIP8 */
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cReg_upen_gcB_sta    +baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset); /* BIP8 */
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cReg_upen_los_sta    +baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset); /* BIP8 */
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cReg_upen_ais_ds3_sta+baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset); /* BIP8 */
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cReg_upen_ais_e3_sta +baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset); /* AIS */
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cReg_upen_febe_sta   +baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset); /* AIS */
            }
        }
    return cAtOk;
    }

static eAtRet De1AttInit(AtAttPdhManager self)
    {
    Tha6A290021ModulePdh pdh = (Tha6A290021ModulePdh) AtAttPdhManagerModulePdh(self);
    uint32   slice = 0;
    uint32       dataBuffer[2]={0,0};
    uint32       holdRegOffset=0;
    for (slice=0; slice <ThaModulePdhNumSlices((ThaModulePdh)pdh); slice++)
        {
        uint32   baseAddress = ThaModulePdhSliceBase((ThaModulePdh)pdh, slice);
        uint32   id = 0;
        for (id=0; id <= 0x3FF; id++)
            {
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cDs1Reg_upen_fbitsta+baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset);
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cDs1Reg_upen_cpb_sta+baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset);
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cDs1Reg_upen_rai_sta+baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset);
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cDs1Reg_upen_los_sta+baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset);
            Tha60290021ModulePdhAttLongWriteOnCore(pdh, cDs1Reg_upen_rai_sta+baseAddress + id, dataBuffer, 2, IpCore(self), holdRegOffset);
            }
        }
    return cAtOk;
    }

static eBool HasRegister(AtAttPdhManager self, uint32 address)
	{
	AtUnused(self);
	if ((mInRange(address, 0x1080800, 0x1080FFF))
	    )
		return cAtTrue;
	return cAtFalse;
	}

static uint16 LongRead(AtAttPdhManager self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
	{
	Tha6A290021ModulePdh pdh = (Tha6A290021ModulePdh)(self->pdh);

	return Tha60290021ModulePdhAttLongReadOnCore(pdh, localAddress, dataBuffer, bufferLen, core, 0);
	}

static uint16 LongWrite(AtAttPdhManager self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
	{
	Tha6A290021ModulePdh pdh = (Tha6A290021ModulePdh)(self->pdh);

	return Tha60290021ModulePdhAttLongWriteOnCore(pdh, localAddress, dataBuffer, bufferLen, core, 0);
	}

static void OverrideAtAttPdhManager(AtAttPdhManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttPdhManagerOverride, mMethodsGet(self), sizeof(m_AtAttPdhManagerOverride));
        mMethodOverride(m_AtAttPdhManagerOverride, HasRegister);
        mMethodOverride(m_AtAttPdhManagerOverride, LongRead);
        mMethodOverride(m_AtAttPdhManagerOverride, LongWrite);
        mMethodOverride(m_AtAttPdhManagerOverride, De3AttInit);
        mMethodOverride(m_AtAttPdhManagerOverride, De1AttInit);
        }

    mMethodsSet(self, &m_AtAttPdhManagerOverride);
    }

static void Override(Tha6A290021AttPdhManager self)
    {
    OverrideAtAttPdhManager((AtAttPdhManager)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021AttPdhManager);
    }

AtAttPdhManager Tha6A290021AttPdhManagerObjectInit(AtAttPdhManager self, AtModulePdh pdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031AttPdhManagerObjectInit(self, pdh) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtAttPdhManager Tha6A290021AttPdhManagerNew(AtModulePdh pdh)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttPdhManager newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290021AttPdhManagerObjectInit(newObject, pdh);
    }



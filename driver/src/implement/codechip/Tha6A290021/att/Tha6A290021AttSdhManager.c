/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A290021AttSdhManager.c
 *
 * Created Date: Aug 23, 2017
 *
 * Author      :
 *
 * Description : Default implementation of Sdh module of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290021AttSdhManagerInternal.h"
#include "../sdh/Tha6A290021SdhAttReg.h"
#include "../sdh/Tha6A290021ModuleSdhInternal.h"
#include "../sdh/Tha6A290021SdhAttPointerAdjReg.h"
#include "../sdh/Tha6A290021SdhAttPointerAdjRegV2.h"
#include "../../../default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A290021AttSdhManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttSdhManagerMethods m_AtAttSdhManagerOverride;
static tThaAttSdhManagerMethods m_ThaAttSdhManagerOverride;

/* Save super implementations */
static const tAtAttSdhManagerMethods  *m_AtAttSdhManagerMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 StartVersionSupportPointerAdjV2(AtAttSdhManager self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0555);
    }

static ThaVersionReader VersionReader(AtAttSdhManager self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)(self->sdh));
    return ThaDeviceVersionReader(device);
    }

static eBool IsPointerAdjV2(AtAttSdhManager self)
    {
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtAttSdhManager) self));
    AtUnused(self);

    if (hwVersion >=StartVersionSupportPointerAdjV2(self))
        return cAtTrue;
    return cAtFalse;
    }

static AtIpCore IpCore(AtAttSdhManager self)
    {
    AtModule sdh = (AtModule) AtAttSdhManagerModuleSdh(self);
    return AtDeviceIpCoreGet(AtModuleDeviceGet(sdh), 0);
    }

static eAtRet LineAttInit(AtAttSdhManager self)
    {
    /* No line error and alarm status */
    AtUnused(self);
    return cAtOk;
    }

static eAtRet LoPathAttInit(AtAttSdhManager self)
    {
    Tha6A290021ModuleSdh sdh = (Tha6A290021ModuleSdh) AtAttSdhManagerModuleSdh(self);
    uint32 dataBuffer[2]={0,0};
    uint32 baseAddress = 0x100000;
    uint32 id, slice;
    AtIpCore ipCore = IpCore(self);

    /* Clear low order path pointer config */
    for (slice = 0; slice < 8; slice++)
        {
        uint32 offset = baseAddress + slice * 0x1000;
        for (id = 0; id <= 0x5FB; id++)
            {
            Tha60290021ModuleSdhAttLongReadOnCore(sdh, cAf6Reg_stsloadj + offset + id, dataBuffer, 2, ipCore);
            }
        }

    return cAtOk;
    }

static eBool NeedToForceErrorStatusInit(AtAttSdhManager self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet HoPathAttInit(AtAttSdhManager self)
    {
    Tha6A290021ModuleSdh sdh = (Tha6A290021ModuleSdh) AtAttSdhManagerModuleSdh(self);
    uint32 dataBuffer[2]={0,0};
    uint32 baseAddress = 0x100000;
    uint32 regAddr;
    uint32 id, slice;
    AtIpCore ipCore = IpCore(self);

    /* Clear high order path error and alarm status */
    if (NeedToForceErrorStatusInit(self))
        {
        for (slice = 0; slice < 8; slice++)
            {
            uint32 offset = baseAddress + slice * 0x1000;
            for (id = 0; id <= 0x2F; id++)
                {
                Tha60290021ModuleSdhAttLongReadOnCore(sdh, cReg_upen_LOPstssta + offset, dataBuffer, 2, ipCore);
                Tha60290021ModuleSdhAttLongReadOnCore(sdh, cReg_upen_AISstssta + offset, dataBuffer, 2, ipCore);
                Tha60290021ModuleSdhAttLongReadOnCore(sdh, cReg_upen_UEQstssta + offset, dataBuffer, 2, ipCore);
                Tha60290021ModuleSdhAttLongReadOnCore(sdh, cReg_upen_BIPstssta + offset, dataBuffer, 2, ipCore);
                Tha60290021ModuleSdhAttLongReadOnCore(sdh, cReg_upen_REIstssta + offset, dataBuffer, 2, ipCore);
                Tha60290021ModuleSdhAttLongReadOnCore(sdh, cReg_upen_RDIstssta + offset, dataBuffer, 2, ipCore);
                offset++;
                }
            }

        }

    if (IsPointerAdjV2(self))
        regAddr = cAf6Reg_stshoadjV2;
    else
        regAddr = cAf6Reg_stshoadj;

    /* Clear high path Pointer config */
    for (slice = 0; slice < 8; slice++)
        {
        uint32 offset = baseAddress + slice * 0x1000;
        for (id = 0; id <= 0x2F; id++)
            {
            Tha60290021ModuleSdhAttLongReadOnCore(sdh, cAf6Reg_hocepadj + offset, dataBuffer, 2, ipCore);
            Tha60290021ModuleSdhAttLongReadOnCore(sdh, regAddr + offset, dataBuffer, 2, ipCore);
            offset++;
            }
        }

    return cAtOk;
    }

static eAtRet LoPathForcePointerAdjEnable(AtAttSdhManager self, eBool en)
    {
    Tha6A290021ModuleSdh sdh = (Tha6A290021ModuleSdh) AtAttSdhManagerModuleSdh(self);
    uint8  sliceId     = 0;
    uint32 baseAddress = 0x100000;

    /* en:0 dis:1*/
    for (sliceId=0; sliceId<4; sliceId++)
        {
        uint32 offset =  sliceId*4096UL + baseAddress;
        mModuleHwWrite(sdh, cAf6Reg_enstsloadj + offset, en?0x0:0x1);
        }
    return cAtOk;
    }

static eAtRet HoPathForcePointerAdjEnable(AtAttSdhManager self, eBool en)
    {
    Tha6A290021ModuleSdh sdh = (Tha6A290021ModuleSdh) AtAttSdhManagerModuleSdh(self);
    uint8  sliceId     = 0;
    uint32 baseAddress = 0x100000;

    /* en:0 dis:1*/
    for (sliceId=0; sliceId<4; sliceId++)
        {
        uint32 offset =  sliceId*2048UL + baseAddress;
        mModuleHwWrite(sdh, cAf6Reg_enhocepadj + offset, en?0x0:0x1);
        }
    return cAtOk;
    }

static eBool    HasForcePointer(AtAttSdhManager self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet   Init(AtAttSdhManager self)
    {
    eAtRet ret = m_AtAttSdhManagerMethods->Init(self);
    ret |= ThaAttSdhManagerSohColRemoveSet((ThaAttSdhManager)self, 0);
    return ret;
    }

static eAtRet DebugHoPointerLooptimeEnable(ThaAttSdhManager self, eBool en)
    {
    ThaModuleSdh module =(ThaModuleSdh)( ((AtAttSdhManager)self)->sdh);
    uint32 address = 0;

    /*for (address = 0x0300100; address <0x0300130; address++)
        mModuleHwWrite(module, address , 0x11111111);*/
    for (address = 0x0300100; address <0x030012f; address++)
        {
        uint32 value = mModuleHwRead(module, address);

        mFieldIns(&value, cBit15_0, 0, en?0x1111:0);
        mModuleHwWrite(module, address , value);
        }

    return cAtOk;
    }

static eAtRet DebugLoPointerLooptimeEnable(ThaAttSdhManager self, eBool en)
    {
    ThaModuleSdh module =(ThaModuleSdh)( ((AtAttSdhManager)self)->sdh);
    uint32 address = 0, slice;
    for (slice=0; slice < 8; slice ++)
        {
        /* fill 0xc00800 0xc0081f 0
           fill 0xc40800 0xc4081f 0
           fill 0xc80800 0xc8081f 0
           fill 0xcc0800 0xcc081f 0
           fill 0xd00800 0xd0081f 0
           fill 0xd40800 0xd4081f 0
           fill 0xd80800 0xd8081f 0
           fill 0xdc0800 0xdc081f 0  */
        for (address = 0x800; address <0x820; address++)
        	{
            uint32 regAddr = 0xC00000 +  slice* 0x40000 + address;
            uint32 value = mModuleHwRead(module, regAddr);

            mFieldIns(&value, cBit7_0, 0, en?0x11:0);
            mModuleHwWrite(module, regAddr, value);
            }
        AtOsalUSleep(5000);
        /* CDR Lo-order VT Timing */
        for (address = 0xC00; address <0x1000; address++)
            {
            uint32 regAddr = 0xC00000 +  slice* 0x40000 + address;
            uint32 value = mModuleHwRead(module, regAddr);

            mFieldIns(&value, cBit7_0, 0, en?0x11:0);
            mModuleHwWrite(module, regAddr , value);
            }
        }

    return cAtOk;
    }

static eAtRet SourceMultiplexingResync(ThaAttSdhManager self)
    {
    ThaModuleSdh module = (ThaModuleSdh) (((AtAttSdhManager) self)->sdh);

    mModuleHwWrite(module, 0x1d0fff, 0xff);
    mModuleHwWrite(module, 0x1d1fff, 0xff);
    mModuleHwWrite(module, 0x1d2fff, 0xff);
    mModuleHwWrite(module, 0x1d3fff, 0xff);
    AtOsalUSleep(1000000);

    mModuleHwWrite(module, 0x1d0fff, 0x0);
    mModuleHwWrite(module, 0x1d1fff, 0x0);
    mModuleHwWrite(module, 0x1d2fff, 0x0);
    mModuleHwWrite(module, 0x1d3fff, 0x0);
    AtOsalUSleep(1000000);
    return cAtOk;
    }

static void OverrideAtAttSdhManager(AtAttSdhManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtAttSdhManagerMethods =  mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttSdhManagerOverride, m_AtAttSdhManagerMethods, sizeof(m_AtAttSdhManagerOverride));
        mMethodOverride(m_AtAttSdhManagerOverride, Init);
        mMethodOverride(m_AtAttSdhManagerOverride, HasForcePointer);
        mMethodOverride(m_AtAttSdhManagerOverride, LineAttInit);
        mMethodOverride(m_AtAttSdhManagerOverride, LoPathAttInit);
        mMethodOverride(m_AtAttSdhManagerOverride, LoPathForcePointerAdjEnable);
        mMethodOverride(m_AtAttSdhManagerOverride, HoPathAttInit);
        mMethodOverride(m_AtAttSdhManagerOverride, HoPathForcePointerAdjEnable);
        }

    mMethodsSet(self, &m_AtAttSdhManagerOverride);
    }

static void OverrideThaAttSdhManager(ThaAttSdhManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttSdhManagerOverride, mMethodsGet(self), sizeof(m_ThaAttSdhManagerOverride));
        mMethodOverride(m_ThaAttSdhManagerOverride, DebugHoPointerLooptimeEnable);
        mMethodOverride(m_ThaAttSdhManagerOverride, DebugLoPointerLooptimeEnable);
        mMethodOverride(m_ThaAttSdhManagerOverride, SourceMultiplexingResync);
        }

    mMethodsSet(self, &m_ThaAttSdhManagerOverride);
    }

static void Override(Tha6A290021AttSdhManager self)
    {
    OverrideAtAttSdhManager((AtAttSdhManager)self);
    OverrideThaAttSdhManager((ThaAttSdhManager)self);
    }

AtAttSdhManager Tha6A290021AttSdhManagerObjectInit(AtAttSdhManager self, AtModuleSdh sdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaAttSdhManager));

    /* Super constructor */
    if (Tha6A210031AttSdhManagerObjectInit(self, sdh) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtAttSdhManager Tha6A290021AttSdhManagerNew(AtModuleSdh sdh)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttSdhManager newObject = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaAttSdhManager));
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290021AttSdhManagerObjectInit(newObject, sdh);
    }



/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha6A210021AttPdhManagerInternal.h
 * 
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : ATT PDH Manager of 6A210021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021ATTPDHMANAGERINTERNAL_H_
#define _THA6A290021ATTPDHMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A210031/att/Tha6A210031AttPdhManagerInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha6A290021AttPdhManager* Tha6A290021AttPdhManager;
typedef struct tTha6A290021AttPdhManager
    {
    tTha6A210031AttPdhManager super;
    }tTha6A290021AttPdhManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttPdhManager Tha6A290021AttPdhManagerObjectInit(AtAttPdhManager self, AtModulePdh pdh);

/* Debugger */

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021ATTPDHMANAGERINTERNAL_H_ */


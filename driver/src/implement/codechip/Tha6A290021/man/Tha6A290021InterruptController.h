/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : Interrupt Controller
 *
 * File        : Tha6A290021InterruptControllerInternal.h
 *
 * Created Date: Jul 27, 2015
 *
 * Description : Interrupt Controller representation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021INTERRUPTCONTROLLER_H_
#define _THA6A290021INTERRUPTCONTROLLER_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/man/Tha60290021InterruptController.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021InterruptController
    {
    tTha60290021InterruptController super;
    } tTha6A290021InterruptController;
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
ThaIntrController Tha6A290021IntrControllerNew(AtIpCore core);
ThaIntrController Tha6A290021IntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore);

#ifdef __cplusplus
}
#endif

#endif /* _THA6A290021INTERRUPTCONTROLLER_H_ */

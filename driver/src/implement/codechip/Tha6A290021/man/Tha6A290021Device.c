/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha6A290021Device.c
 *
 * Created Date: Oct 8, 2015
 *
 * Description : Tha60210031 Device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290021DeviceInternal.h"
#include "../../Tha60290021/xc/Tha60290021ModuleXc.h"
#include "../ocn/Tha6A290021ModuleOcn.h"
#include "../pwe/Tha6A290021ModulePwe.h"
#include "../ram/Tha6A290021ModuleRam.h"
#include "../cdr/Tha6A290021ModuleCdr.h"
#include "../poh/Tha6A290021ModulePohInternal.h"
#include "Tha6A290021InterruptController.h"
#include "../physical/Tha6A290021Physical.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;
static tTha60210011DeviceMethods m_Tha60210011DeviceOverride;
static tTha60290021DeviceMethods m_Tha60290021DeviceOverride;

/* Super implementations */
static const tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ErrorGeneratorIsSupported(ThaDevice self)
    {
    ThaVersionReader versionReader                     = ThaDeviceVersionReader((AtDevice)self);
    uint32           hwVersion                         = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32           startVersionSupportErrorGenerator = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0550);
    return (hwVersion >= startVersionSupportErrorGenerator) ? cAtTrue : cAtFalse;
    }

static  AtSerdesManager SerdesManagerObjectCreate(AtDevice self)
    {
    return Tha6A290021SerdesManagerNew(self);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = (eThaPhyModule)moduleId;

    if (moduleId  == cAtModulePrbs) return (AtModule)Tha6A290021ModulePrbsNew(self);
    if (moduleId  == cAtModuleSdh)  return (AtModule)Tha6A290021ModuleSdhNew(self);
    if (moduleId  == cAtModuleSur)  return (AtModule)Tha6A290021ModuleSurNew(self);
    if (moduleId  == cAtModulePw)   return (AtModule)Tha6A290021ModulePwNew(self);
    if (moduleId  == cAtModuleEth)  return (AtModule)Tha6A290021ModuleEthNew(self);
    if (moduleId  == cAtModulePdh)  return (AtModule)Tha6A290021ModulePdhNew(self);
    if (moduleId  == cAtModuleRam)  return (AtModule)Tha6A290021ModuleRamNew(self);
    if (moduleId  == cAtModuleXc)   return (AtModule)Tha6A290021ModuleXcNew(self);

    /* PhyModule */
    if (phyModule  == cThaModuleMap)   return Tha6A290021ModuleMapNew(self);
    if (phyModule  == cThaModuleDemap) return Tha6A290021ModuleDemapNew(self);
    if (phyModule  == cThaModulePwe)   return Tha6A290021ModulePweNew(self);
    if (phyModule  == cThaModuleOcn)   return Tha6A290021ModuleOcnNew(self);
    if (phyModule  == cThaModuleCdr)   return Tha6A290021ModuleCdrNew(self);
    if (phyModule  == cThaModulePoh)   return Tha6A290021ModulePohNew(self);
    if (phyModule  == cThaModuleCla)   return NULL;
    if (phyModule  == cThaModulePda)   return NULL;

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eAtRet AllModulesInit(AtDevice self)
    {
    eAtRet ret = cAtOk;

    ret |= AtDeviceModuleInit(self, cAtModuleXc);
    ret |= AtDeviceModuleInit(self, cAtModuleSdh);
    ret |= AtDeviceModuleInit(self, cAtModulePdh);
    ret |= AtDeviceModuleInit(self, cAtModuleEncap);
    ret |= AtDeviceModuleInit(self, cAtModuleRam);
    ret |= AtDeviceModuleInit(self, cAtModulePw);
    ret |= AtDeviceModuleInit(self, cAtModuleClock);
    ret |= AtDeviceModuleInit(self, cAtModuleConcate);
    ret |= AtDeviceModuleInit(self, cAtModulePrbs);

    /* Initialize all physical modules */
    ret |= AtDeviceModuleInit(self, cThaModuleCdr);
    ret |= AtDeviceModuleInit(self, cThaModuleOcn);
    ret |= AtDeviceModuleInit(self, cThaModulePoh);

    return ret;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret = m_AtDeviceMethods->Init(self);
    ret |= Tha60290021ModuleXcHideModeSet((AtModuleXc)AtDeviceModuleGet(self, cAtModuleXc), cTha60290021XcHideModeDirect);
    return ret;
    }

static eAtRet AttHwFlushHoPointer(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryFlush(device, 0xC00800, 0xC0081f, 0x0);
    ThaDeviceMemoryFlush(device, 0xC40800, 0xC4081f, 0x0);
    ThaDeviceMemoryFlush(device, 0xC80800, 0xC8081f, 0x0);
    ThaDeviceMemoryFlush(device, 0xCC0800, 0xCC081f, 0x0);
    ThaDeviceMemoryFlush(device, 0xD00800, 0xD0081f, 0x0);
    ThaDeviceMemoryFlush(device, 0xD40800, 0xD4081f, 0x0);
    ThaDeviceMemoryFlush(device, 0xD80800, 0xD8081f, 0x0);
    ThaDeviceMemoryFlush(device, 0xDC0800, 0xDC081f, 0x0);
    return cAtOk;
    }

static eAtRet AttHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    AttHwFlushHoPointer(self);
    ThaDeviceMemoryFlush(device, 0xC00C00, 0xC00FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0xC40C00, 0xC40FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0xC80C00, 0xC80FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0xCC0C00, 0xCC0FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0xD00C00, 0xD00FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0xD40C00, 0xD40FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0xD80C00, 0xD80FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0xDC0C00, 0xDC0FFF, 0x0);
    return cAtOk;
    }

static eAtRet MapHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    ThaDeviceMemoryFlush(device, 0x800000, 0x817FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x840000, 0x857FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x880000, 0x897FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x8C0000, 0x8D7FFF, 0x0);

    ThaDeviceMemoryFlush(device, 0x900000, 0x917FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x940000, 0x957FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x980000, 0x997FFF, 0x0);
    ThaDeviceMemoryFlush(device, 0x9C0000, 0x9D7FFF, 0x0);

    return cAtOk;
    }

static eAtRet PrbsHwFlush(Tha60210011Device self)
	{
	ThaDevice device = (ThaDevice)self;
	ThaDeviceMemoryFlush(device, 0x68000, 0x680FF, 0x0);
	ThaDeviceMemoryFlush(device, 0x68100, 0x681FF, 0x0);
	ThaDeviceMemoryFlush(device, 0x95000, 0x952FB, 0x0);
	ThaDeviceMemoryFlush(device, 0x80100, 0x80117, 0x0);
	return cAtOk;
	}

static eAtRet PlaHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet PdaHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet ClaHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet PweHwFlush(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet BerRequestDdrDisable(Tha60210011Device self)
    {
	AtUnused(self);
	return cAtOk;
    }

static eBool DiagnosticModeIsEnabled(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtUnused(self);
    return Tha6A290021IntrControllerNew(core);
    }

static eBool IsRunningOnOtherPlatform(Tha60290021Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet DccHdlcPktGenInit(Tha60290021Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
												 cAtModuleSur,
                                                 cAtModuleXc,
                                                 cAtModuleSdh,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleBer,
                                                 cAtModulePrbs,
                                                 cAtModuleClock,
                                                 cAtModuleAps};

    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static eBool ShouldDeprovisionHwOnDeleting(ThaDevice self)
    {
	AtUnused(self);
	return cAtTrue;
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, Init);        
        mMethodOverride(m_AtDeviceOverride, AllModulesInit);
        mMethodOverride(m_AtDeviceOverride, SerdesManagerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, DiagnosticModeIsEnabled);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(tThaDeviceMethods));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        mMethodOverride(m_ThaDeviceOverride, ShouldDeprovisionHwOnDeleting);
        mMethodOverride(m_ThaDeviceOverride, ErrorGeneratorIsSupported);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideTha60150011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60150011DeviceOverride));

        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void OverrideTha60210011Device(AtDevice self)
    {
    Tha60210011Device device = (Tha60210011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210011DeviceOverride));

        mMethodOverride(m_Tha60210011DeviceOverride, PweHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, PlaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, ClaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, PdaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, PrbsHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, AttHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, MapHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, BerRequestDdrDisable);
        }

    mMethodsSet(device, &m_Tha60210011DeviceOverride);
    }

static void OverrideTha60290021Device(AtDevice self)
    {
    Tha60290021Device device = (Tha60290021Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021DeviceOverride, mMethodsGet(device), sizeof(m_Tha60290021DeviceOverride));

        mMethodOverride(m_Tha60290021DeviceOverride, IsRunningOnOtherPlatform);
        mMethodOverride(m_Tha60290021DeviceOverride, DccHdlcPktGenInit);
        }

    mMethodsSet(device, &m_Tha60290021DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60150011Device(self);
    OverrideTha60210011Device(self);
    OverrideTha60290021Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021Device);
    }

AtDevice Tha6A290021DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha6A290021DeviceNew(AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    return Tha6A290021DeviceObjectInit(newDevice, driver, productCode);
    }

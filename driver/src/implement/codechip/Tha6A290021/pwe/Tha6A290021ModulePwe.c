/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha6A290021ModulePwe.c
 *
 * Created Date: Sep 29, 2015
 *
 * Description : Module PWE of 6A290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290021ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tThaModulePweMethods m_ThaModulePweOverride;

/* Save super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PwEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(enable);
    return cAtOk;
    }

static eBool PwIsEnabled(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtFalse;
    }

static eAtRet HeaderRead(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 bufferSize)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(buffer);
    AtUnused(bufferSize);
    return cAtOk;
    }

static eAtRet HwResourceCheck(AtModule self)
    {
    /* This does not have */
    AtUnused(self);
    return cAtOk;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, HwResourceCheck);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static uint32 PwTxPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	AtUnused(self);
	AtUnused(pw);
	AtUnused(clear);
	return 0;
    }

static uint32 PwTxBytesGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	AtUnused(self);
	AtUnused(pw);
	AtUnused(clear);
	return 0;
    }

static uint32 PwTxLbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	AtUnused(self);
	AtUnused(pw);
	AtUnused(clear);
	return 0;
    }

static uint32 PwTxRbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	AtUnused(self);
	AtUnused(pw);
	AtUnused(clear);
	return 0;
    }

static uint32 PwTxMbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	AtUnused(self);
	AtUnused(pw);
	AtUnused(clear);
	return 0;
    }

static uint32 PwTxNbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	AtUnused(self);
	AtUnused(pw);
	AtUnused(clear);
	return 0;
    }

static uint32 PwTxPbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
	AtUnused(self);
	AtUnused(pw);
	AtUnused(clear);
	return 0;
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, mMethodsGet(pweModule), sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PwEnable);
        mMethodOverride(m_ThaModulePweOverride, PwIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, HeaderRead);
        mMethodOverride(m_ThaModulePweOverride, PwTxPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxBytesGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxLbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxRbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxMbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxNbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxPbitPacketsGet);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021ModulePwe);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A290021ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

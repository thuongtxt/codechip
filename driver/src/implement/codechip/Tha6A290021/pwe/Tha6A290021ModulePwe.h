/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60210051ModulePwe.h
 * 
 * Created Date: Nov 6, 2015
 *
 * Description : 60210051 PWE interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021MODULEPWE_H_
#define _THA6A290021MODULEPWE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/pwe/Tha60290021ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021ModulePwe
    {
    tTha60290021ModulePwe super;
    }tTha6A290021ModulePwe;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha6A290021ModulePweNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021MODULEPWE_H_ */


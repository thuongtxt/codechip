/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60290021ModuleOcn.h
 * 
 * Created Date: Jul 12, 2016
 *
 * Description : OCN module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021MODULEOCN_H_
#define _THA6A290021MODULEOCN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha6A290021ModuleOcnNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021MODULEOCN_H_ */


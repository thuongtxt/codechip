/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha6A290021ModuleOcn.c
 *
 * Created Date: Jul 12, 2016
 *
 * Description : OCN source code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290021ModuleOcn.h"
#include "../../Tha60290021/ocn/Tha60290021ModuleOcnInternal.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../sdh/Tha6A290021SdhAttPointerAdjRegV2.h"
#include "../../../default/man/ThaDevice.h" /* cThaModuleOcn */
/*--------------------------- Define -----------------------------------------*/
#define cAf6_rfmz1z2selctl_OCNRxZnAugId_Mask        cBit5_2
#define cAf6_rfmz1z2selctl_OCNRxZnAugId_Shift       2
#define cAf6_rfmz1z2selctl_OCNRxZnAu3Id_Mask        cBit1_0
#define cAf6_rfmz1z2selctl_OCNRxZnAu3Id_Shift       0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021ModuleOcn
    {
    tTha60290021ModuleOcn super;
    }tTha6A290021ModuleOcn;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleOcnMethods m_ThaModuleOcnOverride;
static tTha60210011ModuleOcnMethods m_Tha60210011ModuleOcnOverride;
static tTha60290021ModuleOcnMethods m_Tha60290021ModuleOcnOverride;

/* Supper implementation */
static const tThaModuleOcnMethods *m_ThaModuleOcnMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
#define cThaRegDS1E1J1RxFrmrCtrl                       0x00754000
#define cThaRxDE1MdMask                                cBit3_0
#define cThaRxDE1MdShift                               0

#define cThaRegDS1E1J1TxFrmrCtrl                       0x00794000
#define cThaTxDE1MdMask                                cBit3_0
#define cThaTxDE1MdShift                               0

static uint8 ThaModuleOcnVtTuTypeSw2HwPdhMode(eThaOcnVtTuType vtTuType)
    {
    if (vtTuType == cThaOcnVt15Tu11) return 0;
    if (vtTuType == cThaOcnVt2Tu12)  return 8;
    if (vtTuType == cThaOcnVt3)      return 0;
    if (vtTuType == cThaOcnVt6Tu2)   return 8;

    /* TU11 as default */
    return 0;
    }

static eAtRet RxVtgPdhReset(ThaModuleOcn self, AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaOcnVtTuType vtgTypeCfg)
    {
    uint32 regAddr, regVal, pdh_i;
    int32 offset, sliceOffset;
    uint8 hwSlice, hwStsId;
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModulePdh pdh = (ThaModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    ThaSdhChannelHwStsGet(tug2, cAtModulePdh, stsId, &hwSlice, &hwStsId);
    sliceOffset = ThaModulePdhSliceOffset((ThaModulePdh)pdh, (uint32)hwSlice);


    for (pdh_i=0; pdh_i<4; pdh_i++)
        {
        offset = sliceOffset +  (hwStsId * 32) + (vtgId * 4)+ (int32)pdh_i;
        regAddr = (uint32)(cThaRegDS1E1J1RxFrmrCtrl + offset);
        regVal = /*mModuleHwRead(pdh, regAddr)*/0;
        mFieldIns(&regVal,
                  cThaRxDE1MdMask,
                  cThaRxDE1MdShift,
                  ThaModuleOcnVtTuTypeSw2HwPdhMode(vtgTypeCfg));
        mModuleHwWrite(pdh, regAddr, regVal);
        }

    return cAtOk;
    }

static eAtRet TxVtgPdhReset(ThaModuleOcn self, AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaOcnVtTuType vtgTypeCfg)
    {
    uint32 regAddr, regVal, pdh_i;
    uint8 hwSlice, hwStsId;
    int32 offset, sliceOffset;
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModulePdh pdh = (ThaModulePdh)AtDeviceModuleGet(device, cAtModulePdh);

    ThaSdhChannelHwStsGet(tug2, cAtModulePdh, stsId, &hwSlice, &hwStsId);
    sliceOffset = ThaModulePdhSliceOffset((ThaModulePdh)pdh, (uint32)hwSlice);

    for (pdh_i=0; pdh_i<4; pdh_i++)
        {
        offset = sliceOffset + (hwStsId * 32) + (vtgId * 4) + (int32)pdh_i;
        regAddr = (uint32)(cThaRegDS1E1J1TxFrmrCtrl + offset);
        regVal = /*mModuleHwRead(pdh, regAddr)*/0;
        mFieldIns(&regVal,
                  cThaTxDE1MdMask,
                  cThaRxDE1MdShift,
                  ThaModuleOcnVtTuTypeSw2HwPdhMode(vtgTypeCfg));
        mModuleHwWrite(pdh, regAddr, regVal);
        }

    return cAtOk;
    }

static eAtRet TerPgSlaveIndicate(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, eBool isSlave)
    {
    uint32 regAddr, regVal;
    uint8 hwSlice, hwStsId;

    if (!Tha60290021ModuleSdhIsTerminatedVc(channel))
        {
        return cAtOk;
        }

    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &hwSlice, &hwStsId);
    regAddr = mMethodsGet(((Tha60210011ModuleOcn)self))->OcnTerStsPointerGeneratorPerChannelControlRegAddr(((Tha60210011ModuleOcn)self), channel) +
              mMethodsGet(self)->TxTerHwHoStsOffsetWithBaseAddress(self, channel, hwSlice, hwStsId);
    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cAf6_lopgramctl_TerStsPgStsSlvInd_Mask, cAf6_lopgramctl_TerStsPgStsSlvInd_Shift, isSlave ? 1 : 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool ChannelHasAttPointerProcessor(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtTrue;
    }

static uint32 OcnTerStsPointerGeneratorPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_lopgramctlV2;
    }

static uint32 TxTerHwHoStsOffsetWithBaseAddress(ThaModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(channel);
    return Tha60210011ModuleOcnBaseAddress(self) + (4096UL * hwSlice) + hwSts;
    }

static eAtRet TerPgStsConcatMasterSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId)
    {
    uint32 regAddr, regVal;
    uint8 hwSlice, hwSts;

    if (!Tha60290021ModuleSdhIsTerminatedVc(channel))
        return cAtOk;

    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &hwSlice, &hwSts);
    regAddr = mMethodsGet(((Tha60210011ModuleOcn)self))->OcnTerStsPointerGeneratorPerChannelControlRegAddr(((Tha60210011ModuleOcn)self), channel) +
              mMethodsGet(self)->TxTerHwHoStsOffsetWithBaseAddress(self, channel, hwSlice, hwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_lopgramctl_TerStsPgStsMstId_, hwSts);
    mRegFieldSet(regVal, cAf6_lopgramctl_TerStsPgStsSlvInd_, 0);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
    /*AtPrintf("%s %s: regAddr=%x, regVal=%x\r\n", "TerPgStsConcatMasterSet", AtObjectToString((AtObject)channel), regAddr, regVal);*/
    return cAtOk;
    }

static eAtRet TerPgStsConcatSlaveSet(ThaModuleOcn self, AtSdhChannel channel, uint8 masterStsId, uint8 slaveStsId)
    {
    uint32 regAddr, regVal;
    uint8 slice, hwSts;
    Tha60210011ModuleOcn ocn = (Tha60210011ModuleOcn)self;

    if (!Tha60290021ModuleSdhIsTerminatedVc(channel))
        return cAtOk;

    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, slaveStsId, &slice, &hwSts);

    /* Configure OCN Tx PG Slide 2 Per Channel Control register */
    regAddr = mMethodsGet(ocn)->OcnTerStsPointerGeneratorPerChannelControlRegAddr(ocn, channel) +
              mMethodsGet(self)->TxTerHwHoStsOffsetWithBaseAddress(self, channel, slice, hwSts);

    regVal = mChannelHwRead(channel, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, cAf6_lopgramctl_TerStsPgStsSlvInd_Mask, cAf6_lopgramctl_TerStsPgStsSlvInd_Shift, 1);

    ThaSdhChannelHwStsGet(channel, cThaModuleOcn, masterStsId, &slice, &hwSts);
    mFieldIns(&regVal, cAf6_lopgramctl_TerStsPgStsMstId_Mask,  cAf6_lopgramctl_TerStsPgStsMstId_Shift,  hwSts);

    /* Apply */
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
    /*AtPrintf("%s %s: regAddr=%x, regVal=%x\r\n", "TerPgStsConcatSlaveSet", AtObjectToString((AtObject)channel), regAddr, regVal);*/
    return cAtOk;
    }

static eAtRet TerPgStsConcatRelease(ThaModuleOcn self, AtSdhChannel channel, uint8 stsHwSlice, uint8 masterHwSts, uint8 hwSts)
    {
    Tha60210011ModuleOcn ocn = (Tha60210011ModuleOcn)self;
    uint8 fieldVal;
    eBool isSlaver;
    uint32 regAddr, regVal;

    if (!Tha60290021ModuleSdhIsTerminatedVc(channel))
        return cAtOk;
    /* This register is computed by sts, vtg, vt but don't care VTG and VT in this case */
    regAddr = mMethodsGet(ocn)->OcnTerStsPointerGeneratorPerChannelControlRegAddr(ocn, channel) +
              mMethodsGet(self)->TxTerHwHoStsOffsetWithBaseAddress(self, channel, stsHwSlice, hwSts);
    regVal  = mChannelHwRead(channel, regAddr, cThaModuleOcn);

    /* Check if it is slaver STS of this concatenation */
    isSlaver = (regVal & cAf6_lopgramctl_TerStsPgStsSlvInd_Mask) ? cAtTrue : cAtFalse;
    if (!isSlaver)
        return cAtFalse;

    /* Check if this is slaver of master STS */
    mFieldGet(regVal, cAf6_lopgramctl_TerStsPgStsMstId_Mask, cAf6_lopgramctl_TerStsPgStsMstId_Shift, uint8, &fieldVal);
    if (fieldVal != masterHwSts)
        return cAtFalse;

    /* Set slaver indication and its master */
    mFieldIns(&regVal, cAf6_lopgramctl_TerStsPgStsSlvInd_Mask, cAf6_lopgramctl_TerStsPgStsSlvInd_Shift, 0);
    mFieldIns(&regVal, cAf6_lopgramctl_TerStsPgStsMstId_Mask, cAf6_lopgramctl_TerStsPgStsMstId_Shift, hwSts);
    mChannelHwWrite(channel, regAddr, regVal, cThaModuleOcn);
    /*AtPrintf("%s %s: regAddr=%x, regVal=%x\r\n", "TerPgStsConcatRelease", AtObjectToString((AtObject)channel), regAddr, regVal);*/
    return cAtTrue;
    }

static eAtRet DefaultSet(ThaModuleOcn self)
    {
    eAtRet ret;

    ret = m_ThaModuleOcnMethods->DefaultSet(self);
    if (ret != cAtOk)
        return ret;

    return Tha60290021ModuleOcnPohProcessorSideSet(self, cTha6029LineSideFaceplate);
    }

static eAtRet SohOverEthInit(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool SohOverEthIsSupported(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 FaceplateLineRxZnRegValue2Position(Tha60290021ModuleOcn self, uint32 regVal)
    {
    uint8 aug1 = (uint8)mRegField(regVal, cAf6_rfmz1z2selctl_OCNRxZnAugId_);
    uint8 au3  = (uint8)mRegField(regVal, cAf6_rfmz1z2selctl_OCNRxZnAu3Id_);
    AtUnused(self);
    return (uint8)(aug1 * 3 + au3);
    }

static uint32 FaceplateLineRxZnPosition2RegValue(Tha60290021ModuleOcn self, uint8 sts1)
    {
    uint32 regVal = 0;
    AtUnused(self);
    mRegFieldSet(regVal, cAf6_rfmz1z2selctl_OCNRxZnAugId_, (sts1 / 3));
    mRegFieldSet(regVal, cAf6_rfmz1z2selctl_OCNRxZnAu3Id_, (sts1 % 3));
    return regVal;
    }

static void OverrideTha60210011ModuleOcn(Tha60210011ModuleOcn self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleOcnOverride, mMethodsGet(self), sizeof(m_Tha60210011ModuleOcnOverride));
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnTerStsPointerGeneratorPerChannelControlRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, ChannelHasAttPointerProcessor);
        }

    mMethodsSet(self, &m_Tha60210011ModuleOcnOverride);
    }

static void OverrideTha60290021ModuleOcn(AtModule self)
    {
    Tha60290021ModuleOcn module = (Tha60290021ModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModuleOcnOverride, mMethodsGet(module), sizeof(m_Tha60290021ModuleOcnOverride));
        mMethodOverride(m_Tha60290021ModuleOcnOverride, FaceplateLineRxZnRegValue2Position);
        mMethodOverride(m_Tha60290021ModuleOcnOverride, FaceplateLineRxZnPosition2RegValue);
        }

    mMethodsSet(module, &m_Tha60290021ModuleOcnOverride);
    }

static void OverrideThaModuleOcn(ThaModuleOcn self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleOcnMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, m_ThaModuleOcnMethods, sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, DefaultSet);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthInit);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthIsSupported);

        mMethodOverride(m_ThaModuleOcnOverride, TerPgStsConcatMasterSet);
        mMethodOverride(m_ThaModuleOcnOverride, TerPgStsConcatSlaveSet);
        mMethodOverride(m_ThaModuleOcnOverride, TerPgStsConcatRelease);
        mMethodOverride(m_ThaModuleOcnOverride, TxTerHwHoStsOffsetWithBaseAddress);
        mMethodOverride(m_ThaModuleOcnOverride, TerPgSlaveIndicate);
        mMethodOverride(m_ThaModuleOcnOverride, RxVtgPdhReset);
        mMethodOverride(m_ThaModuleOcnOverride, TxVtgPdhReset);
        }

    mMethodsSet(self, &m_ThaModuleOcnOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleOcn((ThaModuleOcn)self);
    OverrideTha60210011ModuleOcn((Tha60210011ModuleOcn)self);
    OverrideTha60290021ModuleOcn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021ModuleOcn);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A290021ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

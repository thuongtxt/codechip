/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha6A290021ModuleMap.c
 *
 * Created Date: Sep 11, 2015
 *
 * Description : Map module of 6A290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/map/Tha60210011ModuleMapInternal.h"
#include "../../Tha60210011/map/Tha60210011ModuleMapHoReg.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021ModuleMap
    {
    tTha60210011ModuleMap super;
    }tTha6A290021ModuleMap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;
static tThaModuleMapMethods         m_ThaModuleMapOverride;

/* Save super implementation */
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;
static const tThaModuleMapMethods         *m_ThaModuleMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsHwMasterSts(uint8 hwSts, uint8 startHwMasterSts, uint8 numHwMasterSts)
    {
    if ((hwSts >= startHwMasterSts) && (hwSts < (startHwMasterSts + numHwMasterSts)))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet HoBertHwDefault(ThaModuleMap self, AtSdhChannel channel)
    {
    uint8 sts_i = 0, sts1Id = 0, hwSlice = 0, hwSts = 0;
    uint8 numSts = AtSdhChannelNumSts(channel);
    uint8 startSts = AtSdhChannelSts1Get(channel);
    ThaModuleAbstractMap abstractMap = (ThaModuleAbstractMap)self;
    uint8 srcType = (AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeVc3) ? 0 : 1;
    ThaModuleDemap demapModule = (ThaModuleDemap)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleDemap);
    ThaModuleStmMap stmMapModule = (ThaModuleStmMap)self;

    uint32 demapRegAddress = ThaModuleDemapVcDmapChnCtrl(demapModule, (AtSdhVc)channel);
    uint32 mapRegAddress = mMethodsGet(stmMapModule)->MapLineCtrl(stmMapModule, channel);

    uint8 numMasterSts1 = (uint8)((numSts >= 3)  ? (numSts / 3) : 1);
    uint8 startHwMasterSts;
    uint32 regAddr, regVal;
    if (ThaSdhChannelHwStsGet(channel, cThaModuleDemap, startSts, &hwSlice, &startHwMasterSts) != cAtOk)
        mChannelLog(channel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        sts1Id = (uint8)(startSts + sts_i);

        if (ThaSdhChannelHwStsGet(channel, cThaModuleDemap, sts1Id, &hwSlice, &hwSts) != cAtOk)
            mChannelLog(channel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

        /* DEMAP-MAP */
        regAddr = demapRegAddress + ThaModuleAbstractMapHoSliceOffset(abstractMap, hwSlice) + hwSts;
        regVal  = mChannelHwRead(channel, regAddr, cThaModuleMap);

        mRegFieldSet(regVal, cAf6_demap_channel_ctrl_Demapsr_vc3n3c_, IsHwMasterSts(hwSts, startHwMasterSts, numMasterSts1) ? 1 : 0);
        mRegFieldSet(regVal, cAf6_demap_channel_ctrl_Demapsrctype_, srcType);
        mChannelHwWrite(channel, regAddr, regVal, cThaModuleMap);

        /* MAP */
        regAddr = mapRegAddress + ThaModuleAbstractMapHoSliceOffset(abstractMap, hwSlice) + hwSts;
        regVal  = mChannelHwRead(channel, regAddr, cThaModuleMap);

        mRegFieldSet(regVal, cAf6_map_line_ctrl_Mapsrc_vc3n3c_, IsHwMasterSts(hwSts, startHwMasterSts, numMasterSts1) ? 1 : 0);
        mRegFieldSet(regVal, cAf6_map_line_ctrl_Mapsrc_type_, srcType);
        mChannelHwWrite(channel, regAddr, regVal, cThaModuleMap);
        }
    return cAtOk;
    }

static eBool NeedEnableAfterBindToPw(ThaModuleAbstractMap self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtTrue;
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, NeedEnableAfterBindToPw);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void OverrideThaModuleMap(ThaModuleMap self)
    {
    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleMapMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, m_ThaModuleMapMethods, sizeof(m_ThaModuleMapOverride));

        /* Override register address */
        mMethodOverride(m_ThaModuleMapOverride, HoBertHwDefault);
        }

    mMethodsSet(self, &m_ThaModuleMapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleAbstractMap(self);
    OverrideThaModuleMap((ThaModuleMap) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021ModuleMap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A290021ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

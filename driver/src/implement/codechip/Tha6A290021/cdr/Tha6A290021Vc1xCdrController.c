/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaVc1xCdrController.c
 *
 * Created Date: Apr 20, 2013
 *
 * Description : CDR controller for AU-VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290021Vc1xCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/


/*--------------------------- Macros -----------------------------------------*/


/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrControllerMethods           m_ThaCdrControllerOverride;

/* Save super implementation */


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw AcrDcrTimingSource(ThaCdrController self)
    {
    AtUnused(self);
    return NULL;
    }

static AtPw CurrentAcrDcrTimingSource(ThaCdrController self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool FreeRunningIsSupported(ThaCdrController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, mMethodsGet(self), sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, FreeRunningIsSupported);
        mMethodOverride(m_ThaCdrControllerOverride, CurrentAcrDcrTimingSource);
        mMethodOverride(m_ThaCdrControllerOverride, AcrDcrTimingSource);
        }

    mMethodsSet(self, &m_ThaCdrControllerOverride);
    }



static void Override(ThaCdrController self)
    {
    OverrideThaCdrController((ThaCdrController)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021Vc1xCdrController);
    }

ThaCdrController Tha6A290021Vc1xCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVc1xCdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController Tha6A290021Vc1xCdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290021Vc1xCdrControllerObjectInit(newController, engineId, channel);
    }

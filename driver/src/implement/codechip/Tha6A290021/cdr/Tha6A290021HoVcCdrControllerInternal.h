/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha6A290021HoVcCdrControllerInternal.h
 * 
 * Created Date: May 23, 2017
 *
 * Description : HO VC CDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021HOVCCDRCONTROLLERINTERNAL_H_
#define _THA6A290021HOVCCDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/cdr/controllers/Tha60210011HoVcCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021HoVcCdrController
    {
    tTha60210011HoLineVcCdrController super;
    }tTha6A290021HoVcCdrController;

/*--------------------------- Forward declarations ---------------------------*/
ThaCdrController Tha6A290021HoVcCdrControllerNew(uint32 engineId, AtChannel channel);
/*--------------------------- Entries ----------------------------------------*/
ThaCdrController Tha6A290021HoVcCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021HOVCCDRCONTROLLERINTERNAL_H_ */


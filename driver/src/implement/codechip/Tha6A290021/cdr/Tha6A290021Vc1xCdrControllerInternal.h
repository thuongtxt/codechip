/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha6A290021Vc1xCdrControllerInternal.h
 * 
 * Created Date: May 23, 2017
 *
 * Description : HO VC CDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021VC1XCDRCONTROLLERINTERNAL_H_
#define _THA6A290021VC1XCDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cdr/controllers/ThaVc1xCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021Vc1xCdrController
    {
    tThaVc1xCdrController super;
    }tTha6A290021Vc1xCdrController;

/*--------------------------- Forward declarations ---------------------------*/
ThaCdrController Tha6A290021Vc1xCdrControllerNew(uint32 engineId, AtChannel channel);
/*--------------------------- Entries ----------------------------------------*/
ThaCdrController Tha6A290021Vc1xCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021VC1XCDRCONTROLLERINTERNAL_H_ */


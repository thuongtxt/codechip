/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha6A290021ModuleCdr.h
 * 
 * Created Date: May 23, 2017
 *
 * Description : CDR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha6A290021MODULECDR_H_
#define _Tha6A290021MODULECDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/cdr/Tha60290021ModuleCdrInternal.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021ModuleCdr
    {
    tTha60290021ModuleCdr super;
    }tTha6A290021ModuleCdr;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha6A290021ModuleCdrObjectInit(AtModule self, AtDevice device);
AtModule Tha6A290021ModuleCdrNew(AtDevice device);

ThaCdrController Tha6A290021Tu3VcCdrControllerNew(uint32 engineId, AtChannel channel);

#endif /* _Tha6A290021MODULECDR_H_ */


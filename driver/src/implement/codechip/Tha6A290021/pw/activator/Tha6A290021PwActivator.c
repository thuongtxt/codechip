/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha6A290021PwActivator.c
 *
 * Created Date: Sep 9, 2015
 *
 * Description : PW activator of 6A290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/common/AtChannelInternal.h"
#include "../../../../default/pw/adapters/ThaPwAdapter.h"
#include "../../../Tha60290021/pw/Tha60290021PwActivatorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021PwActivator
    {
    tTha60290021PwActivator super;
    }tTha6A290021PwActivator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwActivatorMethods        m_ThaPwActivatorOverride;
static tThaPwDynamicActivatorMethods m_ThaPwDynamicActivatorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PwEthPortSet(ThaPwActivator self, ThaPwAdapter adapter, AtEthPort ethPort)
    {
    AtUnused(self);
    AtUnused(adapter);
    AtUnused(ethPort);
    return cAtOk;
    }

static eAtModulePwRet PwCircuitUnbind(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret;
    AtPw pw = (AtPw)adapter;
    AtChannel circuit;

     ret = mMethodsGet(self)->HwPwDeallocate(self, adapter);
     ThaPwAdapterHwPwSet(adapter, NULL);

     circuit = AtPwBoundCircuitGet(pw);
     ret = AtChannelBindToPseudowire(circuit, NULL);
     if (ret == cAtOk)
         ret |= ThaPwAdapterBoundCircuitSet(adapter, NULL);

    return ret;
    }

static eAtRet Activate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    ThaHwPw hwPw;
    AtPw pw = (AtPw)adapter;

    hwPw = mMethodsGet(self)->HwPwAllocate(self, adapter);
    if (hwPw == NULL)
        return cAtErrorRsrcNoAvail;
    ThaPwAdapterHwPwSet(adapter, hwPw);

    return ThaPwActivatorPwCircuitConnect(self, adapter, AtPwBoundCircuitGet(pw));
    }

static eAtModulePwRet PwCircuitBind(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    if (!mMethodsGet(self)->PwCanBeActivatedWithCircuit(self, adapter, circuit))
        {
        AtChannelBoundPwSet(circuit, ThaPwAdapterPwGet(adapter));
        return cAtOk;
        }

    return mMethodsGet(self)->Activate(self, adapter);
    }

static eAtModulePwRet PwCircuitConnect(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    eAtRet ret = cAtOk;
    AtPw pw = (AtPw)adapter;
    AtUnused(self);

    /* Circuit will know how to bind to PW */
    ret = AtChannelBindToPseudowire(circuit, pw);
    if (ret != cAtOk)
        return ret;

    /* Set configuration on binding */
    ret |= AtChannelBoundPwSet(circuit, ThaPwAdapterPwGet(adapter));
    ret |= ThaPwAdapterBoundCircuitSet(adapter, circuit);

    return ret;
    }

static eAtRet ActivateConfiguration(ThaPwDynamicActivator self, ThaPwAdapter adapter)
    {
    AtUnused(self);
    AtUnused(adapter);
    return cAtOk;
    }

static eAtRet DeactivateConfiguration(ThaPwDynamicActivator self, ThaPwAdapter adapter)
    {
    AtUnused(self);
    AtUnused(adapter);
    return cAtOk;
    }

static eBool ShouldControlPwBandwidth(ThaPwActivator self)
    {
    /* Some product need disable this feature will control at concrete */
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaPwActivator(ThaPwActivator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwActivatorOverride, mMethodsGet(self), sizeof(m_ThaPwActivatorOverride));

        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitUnbind);
        mMethodOverride(m_ThaPwActivatorOverride, Activate);
        mMethodOverride(m_ThaPwActivatorOverride, PwEthPortSet);
        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitBind);
        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitConnect);
        mMethodOverride(m_ThaPwActivatorOverride, ShouldControlPwBandwidth);
        }

    mMethodsSet(self, &m_ThaPwActivatorOverride);
    }

static void OverrideThaPwDynamicActivator(ThaPwActivator self)
    {
    ThaPwDynamicActivator activator = (ThaPwDynamicActivator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDynamicActivatorOverride, mMethodsGet(activator), sizeof(m_ThaPwDynamicActivatorOverride));

        mMethodOverride(m_ThaPwDynamicActivatorOverride, ActivateConfiguration);
        mMethodOverride(m_ThaPwDynamicActivatorOverride, DeactivateConfiguration);
        }

    mMethodsSet(activator, &m_ThaPwDynamicActivatorOverride);
    }

static void Override(ThaPwActivator self)
    {
    OverrideThaPwActivator(self);
    OverrideThaPwDynamicActivator(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021PwActivator);
    }

static ThaPwActivator ObjectInit(ThaPwActivator self, AtModulePw pwModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021PwActivatorObjectInit(self, pwModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwActivator Tha6A290021PwDynamicActivatorNew(AtModulePw pwModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwActivator newActivator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newActivator == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newActivator, pwModule);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60210021PwDefectController.h
 * 
 * Created Date: Nov 3, 2015
 *
 * Description : PW defect controller interface for 60210021 product.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021PWDEFECTCONTROLLER_H_
#define _THA6A290021PWDEFECTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/defectcontrollers/ThaPwDefectControllerInternal.h"
#include "../../../Tha60210021/pw/defectcontroller/Tha60210021PwDefectController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwDefectController Tha6A290021PwDefectControllerNew(AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021PWDEFECTCONTROLLER_H_ */


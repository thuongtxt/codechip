/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha6A290021ModulePw.c
 *
 * Created Date: Sep 9, 2015
 *
 * Description : Pseudowire module of 6A290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/pw/Tha60210051ModulePwInternal.h"
#include "../../Tha60210011/pw/activator/Tha60210011PwActivator.h"
#include "../../Tha60290021/pw/Tha60290021ModulePwInternal.h"
#include "defectcontroller/Tha6A290021PwDefectController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021ModulePw
    {
    tTha60290021ModulePw super;
    }tTha6A290021ModulePw;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePwMethods m_ThaModulePwOverride;
static tAtModuleMethods    m_AtModuleOverride;

/* Save super implementation */
static const tAtModuleMethods *m_AtModuleMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedDoReConfig(ThaModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtPw PwKBytePwGet(ThaModulePw self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtRet Init(AtModule self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static ThaPwActivator PwActivatorCreate(ThaModulePw self)
    {
    return Tha6A290021PwDynamicActivatorNew((AtModulePw)self);
    }

static eBool CasIsSupported(ThaModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 DefaultCounterModule(ThaModulePw self)
    {
    return AtModuleTypeGet((AtModule)self);
    }

static ThaPwDefectController DefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    AtUnused(self);
    return Tha6A290021PwDefectControllerNew(pw);
    }

static ThaPwEthPortBinder PwEthPortBinderCreate(ThaModulePw self)
    {
    return ThaPwEthPortBinderNew(self);
    }

static eAtRet ResetPw(ThaModulePw self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtOk;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    ThaModulePwResourcesLimitationDisable((ThaModulePw)self, cAtTrue);

    return cAtOk;
    }

static eBool ShouldHandleCircuitAisForcingOnEnabling(ThaModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaModulePw(ThaModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(self), sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, PwActivatorCreate);
        mMethodOverride(m_ThaModulePwOverride, CasIsSupported);
        mMethodOverride(m_ThaModulePwOverride, DefaultCounterModule);
        mMethodOverride(m_ThaModulePwOverride, DefectControllerCreate);
        mMethodOverride(m_ThaModulePwOverride, PwEthPortBinderCreate);
        mMethodOverride(m_ThaModulePwOverride, ResetPw);
        mMethodOverride(m_ThaModulePwOverride, PwKBytePwGet);
        mMethodOverride(m_ThaModulePwOverride, NeedDoReConfig);
        mMethodOverride(m_ThaModulePwOverride, ShouldHandleCircuitAisForcingOnEnabling);
        }

    mMethodsSet(self, &m_ThaModulePwOverride);
    }

static void OverrideAtModule(AtModulePw self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Setup);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideThaModulePw((ThaModulePw)self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021ModulePw);
    }

static AtModulePw ObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha6A290021ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

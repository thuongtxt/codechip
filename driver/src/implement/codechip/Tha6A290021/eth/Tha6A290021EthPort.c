/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha6A290021EthPort.c
 *
 * Created Date: Sep 9, 2015
 *
 * Description : ETH port of 6A290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/eth/Tha60210011EthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021EthPort
    {
    tTha60210011EthPort super;
    }tTha6A290021EthPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods      m_AtChannelOverride;
static tAtEthPortMethods      m_AtEthPortOverride;
static tThaEthPortMethods      m_ThaEthPortOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static uint8 TxIpgGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryClear(AtChannel self)
	{
	AtUnused(self);
	return 0;
	}

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
	AtUnused(self);
	AtUnused(pAllCounters);
	return cAtOk;
    }
static eAtRet PcsReset(ThaEthPort self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideThaEthPort(ThaEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, mMethodsGet(self), sizeof(m_ThaEthPortOverride));

        mMethodOverride(m_ThaEthPortOverride, PcsReset);
        }

    mMethodsSet(self, &m_ThaEthPortOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, TxIpgGet);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort((ThaEthPort)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021EthPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011EthPortObjectInit((AtEthPort)self, portId, module) == NULL)
        return NULL;

    /* Initialize method */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha6A290021EthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

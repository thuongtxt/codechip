/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha6A290021ModuleRam.c
 *
 * Created Date: Aug 12, 2016
 *
 * Description : RAM module of product 6A290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/ram/Tha60290021ModuleRamInternal.h"
#include "Tha6A290021ModuleRam.h"
#include "Tha6A290021Ddr.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021ModuleRam
    {
    tTha60290021ModuleRam super;
    }tTha6A290021ModuleRam;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleRamMethods  m_AtModuleRamOverride;
static tAtModuleMethods m_AtModuleOverride;

/* Save super implementation */
static const tAtModuleRamMethods *m_AtModuleRamMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtRam DdrCreate(AtModuleRam self, AtIpCore core, uint8 ddrId)
    {
    switch (ddrId)
        {
        case 0: return Tha6A290021Ddr64BitNew(self, core, ddrId);
        case 1: return Tha6A290021Ddr64BitNew(self, core, ddrId);
        case 2: return Tha6A290021Ddr32BitNew(self, core, ddrId);
        case 3: return Tha6A290021Ddr32BitNew(self, core, ddrId);
        default:
            return NULL;
        }
    }

static void StatusClear(AtModule self)
    {
	AtUnused(self);
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRamMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, m_AtModuleRamMethods, sizeof(m_AtModuleRamOverride));

        mMethodOverride(m_AtModuleRamOverride, DdrCreate);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, StatusClear);
        }
    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModuleRam(self);
    OverrideAtModule((AtModule) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021ModuleRam);
    }

static AtModuleRam ObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModuleRamObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam Tha6A290021ModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

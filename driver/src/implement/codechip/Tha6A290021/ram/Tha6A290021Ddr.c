/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha6A290021Ddr.c
 *
 * Created Date: Sep 25, 2016
 *
 * Description : DDR
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/ram/Tha6029DdrInternal.h"
#include "Tha6A290021Ddr.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290021Ddr *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021Ddr
    {
    tTha6029Ddr super;
    }tTha6A290021Ddr;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtRamMethods  m_AtRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eAtRet HwTestStop(AtRam self)
    {
    AtUnused(self);

    return cAtOk;
    }

static void OverrideAtRam(AtRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtRamOverride, mMethodsGet(self), sizeof(m_AtRamOverride));

        mMethodOverride(m_AtRamOverride, HwTestStop);
        }

    mMethodsSet(self, &m_AtRamOverride);
    }

static void Override(AtRam self)
    {
    OverrideAtRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021Ddr);
    }

static AtRam ObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId, uint8 cellSize)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029DdrObjectInit(self, ramModule, core, ramId, cellSize) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

static AtRam Tha6A290021DdrNewWithCellSize(AtModuleRam ramModule, AtIpCore core, uint8 ramId, uint8 cellSize)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    return ObjectInit(newRam, ramModule, core, ramId, cellSize);
    }

AtRam Tha6A290021Ddr32BitNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    return Tha6A290021DdrNewWithCellSize(ramModule, core, ramId, 1);
    }

AtRam Tha6A290021Ddr64BitNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    return Tha6A290021DdrNewWithCellSize(ramModule, core, ramId, 2);
    }

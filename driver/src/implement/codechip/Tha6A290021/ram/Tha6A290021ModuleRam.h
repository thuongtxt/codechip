/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha6A290021ModuleRam.h
 * 
 * Created Date: Aug 12, 2016
 *
 * Description : RAM module of product 6A290021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef THA6A290021MODULERAM_H_
#define THA6A290021MODULERAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ram/ThaModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha6A290021ModuleRamNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* THA6A290021MODULERAM_H_ */


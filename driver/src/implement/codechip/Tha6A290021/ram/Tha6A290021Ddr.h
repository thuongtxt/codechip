/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha6A290021Ddr.h
 * 
 * Created Date: Sep 25, 2016
 *
 * Description : DDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021DDR_H_
#define _THA6A290021DDR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtRam Tha6A290021Ddr32BitNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha6A290021Ddr64BitNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021DDR_H_ */


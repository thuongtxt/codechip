/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60290022FaceplateSerdesController.c
 *
 * Created Date: Apr 25, 2017
 *
 * Description : Faceplate SERDES
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/physical/Tha6029FaceplateSerdesControllerInternal.h"
#include "Tha6A290021Physical.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021FaceplateSerdesController
    {
    tTha6029FaceplateSerdesController super;
    }tTha6A290021FaceplateSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;
static tTha6029FaceplateSerdesControllerMethods m_Tha6029FaceplateSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eAtSerdesMode DefaultMode(AtSerdesController self)
    {
    uint32 portId = AtSerdesControllerHwIdGet(self);

    if ((portId == 0) || (portId == 4) || (portId == 8) || (portId == 12))
        return cAtSerdesModeStm16;

    return cAtSerdesModeStm4;
    }

static ThaVersionReader VersionReader(AtSerdesController self)
    {
    AtDevice device = AtSerdesControllerDeviceGet(self);
    return ThaDeviceVersionReader(device);
    }

static eBool Stm1SamplingFromStm4(Tha6029FaceplateSerdesController self)
    {
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtSerdesController)self));
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x3, 0x0545);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }


static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, DefaultMode);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideTha6029FaceplateSerdesController(AtSerdesController self)
    {
    Tha6029FaceplateSerdesController controller = (Tha6029FaceplateSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029FaceplateSerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6029FaceplateSerdesControllerOverride));

        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, Stm1SamplingFromStm4);
        }

    mMethodsSet(controller, &m_Tha6029FaceplateSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideTha6029FaceplateSerdesController(self);
    OverrideAtSerdesController((AtSerdesController) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021FaceplateSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029FaceplateSerdesControllerObjectInit(self, manager, physicalPort, serdesId) == NULL)
       return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha6A290021FaceplateSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, manager, physicalPort, serdesId);
    }

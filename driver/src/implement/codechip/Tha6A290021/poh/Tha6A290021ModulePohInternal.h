/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha6A290011ModulePohInternal.h
 *
 * Created Date: May 07, 2018
 *
 * Description : 6A290011 POH Internal
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021MODULEPOHINTERNAL_H_
#define _THA6A290021MODULEPOHINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/poh/Tha60290021ModulePohInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021ModulePoh
    {
    tTha60290021ModulePoh super;
    }tTha6A290021ModulePoh;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha6A290021ModulePohObjectInit(AtModule self, AtDevice device);


#ifdef __cplusplus
}
#endif

#endif /* _THA6A290021MODULEPOHINTERNAL_H_ */

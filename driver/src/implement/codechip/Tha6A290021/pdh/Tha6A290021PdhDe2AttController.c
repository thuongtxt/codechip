/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A290021AttPdhDe2AttController.c
 *
 * Created Date: May 11, 2018
 *
 * Description : DE2 ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/att/ThaAttController.h"
#include "Tha6A290021PdhAttControllerInternal.h"
#include "Tha6A290021PdhReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttControllerMethods     m_AtAttControllerOverride;
static tThaAttControllerMethods    m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 RealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    AtPdhChannel de2   = (AtPdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModulePdh pdh = (ThaModulePdh)AtChannelModuleGet((AtChannel)de2);
    return regAddr + ThaModulePdhM12E12ControlOffset(pdh, (ThaPdhDe2)de2) + 0x700000;
    }

static uint32 NbitPerFrame(ThaAttController self)
    {
    AtPdhDe2 de2 = (AtPdhDe2)AtAttControllerChannelGet((AtAttController)self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)de2);
    switch (frameType)
        {
        case cAtPdhE2G_742Carrying4E1s:
            return 212*4;
        case cAtPdhDs2T1_107Carrying4Ds1s:
            return 294 * 4;
        case cAtPdhDs2G_747Carrying3E1s:
            return 840;
        default:
            return 840;
        }
    return 840;
    }

static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x0, 0x0, 0x0000);
    }

static AtDevice DeviceGet(AtChannel self)
    {
    return AtChannelDeviceGet(self);
    }

static Tha6A290021ModulePdh PdhModule(AtChannel self)
    {
    return (Tha6A290021ModulePdh) AtDeviceModuleGet(DeviceGet(self), cAtModulePdh);
    }

static AtIpCore IpCore(AtChannel self)
    {
    return AtDeviceIpCoreGet(DeviceGet(self), 0);
    }

static uint32 HoldRegisterOffset(AtPdhChannel channel)
    {
    uint8 slice, hwSts;
    ThaPdhChannelHwIdGet(channel, cAtModulePdh, &slice, &hwSts);
    return ThaModulePdhSliceBase((ThaModulePdh)PdhModule((AtChannel)channel), slice);
    }

static uint32 MinStep(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtPdhDe2CounterFbe)
        return 29;
    else
        return 1;
    }

static float MaxErrorNum_1s(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    float NFramePerSecond = ThaAttControllerNFramePerSecond((ThaAttController) self);
    if (errorType == cAtPdhDe2CounterFbe)
        return (NFramePerSecond *3)/1000;
    else
        return NFramePerSecond;
    }

static float MaxErrorNum(Tha6A210031PdhDe3AttController self, uint32 errorType, uint32 T/*ms*/)
    {
    float NFramePerSecond  = ThaAttControllerNFramePerSecond((ThaAttController) self); /* = 9.398 frame*/
    uint32 WINDOW_ADEN     = 3; /* 3ms*/
     /* = 33*/
    if (errorType == cAtPdhDe2CounterFbe)
        return (float) T / (float) WINDOW_ADEN; /* T ms */
    return (NFramePerSecond * (float)T)/1000;
    }

static uint32 RegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtPdhDe2CounterParity:
            return cAf6Reg_upen_par_cfg;
        case cAtPdhDe2CounterFbe:
            return cAf6Reg_upen_fbitcfg;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtPdhDe2CounterParity:
            return cAf6Reg_upen_par_sta;
        case cAtPdhDe2CounterFbe:
            return cAf6Reg_upen_fbit_sta;
        default:
            return cBit31_0;
        }
    }

static eBool ErrorForceIsSupported(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtPdhDe2CounterParity:
        case cAtPdhDe2CounterFbe:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static uint8 ErrorTypeIndex(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtPdhDe2CounterParity:
            return 0;
        case cAtPdhDe2CounterFbe:
            return 1;
        default:
            return 0;
        }
    }

static uint8 NumErrorTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 2;
    }

static eAtRet InitErrorDefault(Tha6A210031PdhDe3AttController self)
    {
    uint32 i=0;
    eAtRet ret = cAtOk;
    static const uint32 cAttPdhDe2ErrorTypeVal[] = {cAtPdhDe2CounterParity, cAtPdhDe2CounterFbe};

    for (i = 0; i < mCount(cAttPdhDe2ErrorTypeVal); i++)
        {
        if (AtAttControllerErrorForceIsSupported((AtAttController)self, cAttPdhDe2ErrorTypeVal[i]))
            {
            uint8 errorTypeIndex;
            ret |= AtAttControllerUnForceError((AtAttController)self, cAttPdhDe2ErrorTypeVal[i]);
            ret |= AtAttControllerForceErrorModeSet((AtAttController)self, cAttPdhDe2ErrorTypeVal[i],cAtAttForceErrorModeContinuous);
            errorTypeIndex = mMethodsGet(self)->ErrorTypeIndex(self, cAttPdhDe2ErrorTypeVal[i]);
            self->errorInfo[errorTypeIndex].errorStep = mMethodsGet(self)->MinStep(self, cAttPdhDe2ErrorTypeVal[i]);/*At the receiver, window to detect LOF is 3ms. Apply for the current design you must config step_cfg must >= 29 to avoid LOF set.*/
            ret |= AtAttControllerForceErrorNumErrorsSet((AtAttController)self, cAttPdhDe2ErrorTypeVal[i], 1);
            ret |= AtAttControllerForceErrorBitPositionSet((AtAttController)self, cAttPdhDe2ErrorTypeVal[i], 0xFFFF);
            }
        }
    return ret;
    }

static uint32 RegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtPdhDe2AlarmAis:
            return cAf6Reg_upen_ais_cfg;
        case cAtPdhDe2AlarmRai:
            return cAf6Reg_upen_rai_cfg;
        case cAtPdhDe2AlarmLof:
            return cAf6Reg_upen_fbitcfg;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtPdhDe1AlarmAis:
            return cAf6Reg_upen_ais_sta;
        case cAtPdhDe1AlarmRai:
            return cAf6Reg_upen_rai_sta;
        case cAtPdhDe1AlarmLof:
            return cAf6Reg_upen_fbitsta;
        default:
            return cBit31_0;
        }
    }

static eBool AlarmForceIsSupported(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtPdhDe2AlarmAis:
        case cAtPdhDe2AlarmRai:
        case cAtPdhDe2AlarmLof:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static uint8 AlarmTypeIndex(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtPdhDe2AlarmAis:
            return 0;
        case cAtPdhDe2AlarmRai:
            return 1;
        case cAtPdhDe2AlarmLof:
            return 2;
        default:
            return 0;
        }
    }

static uint8 NumAlarmTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 3;
    }

static const char* AlarmIndexToString(uint8 alarmIndex)
    {
    static const char* cAttPdhDe2AlarmTypeStr[] = {"AIS","RAI","LOF"};
    if (alarmIndex > mCount(cAttPdhDe2AlarmTypeStr))
        return cAttPdhDe2AlarmTypeStr[0];
    return cAttPdhDe2AlarmTypeStr[alarmIndex];
    }

static eAtRet InitAlarmDefault(Tha6A210031PdhDe3AttController self)
    {
    uint32 i=0;
    eAtRet ret = cAtOk;
    static const uint32 cAttPdhDe2AlarmTypeVal[] = {cAtPdhDe2AlarmAis, cAtPdhDe2AlarmRai, cAtPdhDe2AlarmLof};
    for (i = 0; i < mCount(cAttPdhDe2AlarmTypeVal); i++)
        {
        if (AtAttControllerAlarmForceIsSupported((AtAttController)self,cAttPdhDe2AlarmTypeVal[i]))
            {
            ret |= AtAttControllerUnForceAlarm((AtAttController)self, cAttPdhDe2AlarmTypeVal[i]);
            ret |= AtAttControllerForceAlarmModeSet((AtAttController)self, cAttPdhDe2AlarmTypeVal[i], cAtAttForceAlarmModeContinuous);
            ret |= AtAttControllerForceAlarmNumEventSet((AtAttController)self, cAttPdhDe2AlarmTypeVal[i], 1);
            }
        }
    return ret;
    }

static uint16 LongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    uint32 holdRegOffset = HoldRegisterOffset((AtPdhChannel)channel);
    return Tha60290021ModulePdhAttLongReadOnCore(PdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel), holdRegOffset);
    }

static uint16 LongWrite(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    uint32 holdRegOffset = HoldRegisterOffset((AtPdhChannel)channel);
    return Tha60290021ModulePdhAttLongWriteOnCore(PdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel), holdRegOffset);
    }

static eAtRet DefaultSet(ThaAttController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideTha6A210031PdhDe3AttController(Tha6A210031PdhDe3AttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(self), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, ErrorTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumErrorTypes);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, InitErrorDefault);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RealAddress);

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumAlarmTypes);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIndexToString);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, InitAlarmDefault);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, MinStep);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, MaxErrorNum_1s);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, MaxErrorNum);
        }

    mMethodsSet(self, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride, mMethodsGet(self), sizeof(m_AtAttControllerOverride));
        mMethodOverride(m_AtAttControllerOverride, LongRead);
        mMethodOverride(m_AtAttControllerOverride, LongWrite);
        mMethodOverride(m_AtAttControllerOverride, ErrorForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, AlarmForceIsSupported);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static void OverrideThaAttController(ThaAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(self), sizeof(m_ThaAttControllerOverride));

        mMethodOverride(m_ThaAttControllerOverride, DefaultSet);
        mMethodOverride(m_ThaAttControllerOverride, NbitPerFrame);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideAtAttController(self);
    OverrideThaAttController((ThaAttController)self);
    OverrideTha6A210031PdhDe3AttController((Tha6A210031PdhDe3AttController)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021PdhDe2AttController);
    }

AtAttController Tha6A290021PdhDe2AttControllerObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031PdhDe3AttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A290021PdhDe2AttControllerNew(AtChannel de3)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A290021PdhDe2AttControllerObjectInit(controller, de3);
    }

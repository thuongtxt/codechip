/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : Tha6A290021AttControllerInternal.h
 * 
 * Created Date: May 17, 2016
 *
 * Description : ATT controller header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021MODULEPDHINTERNAL_H_
#define _THA6A290021MODULEPDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/pdh/Tha60290021ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290021ModulePdh * Tha6A290021ModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe3 Tha6A290021PdhDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha6A290021PdhVcDe1New(AtSdhVc vc1x, AtModulePdh module);
AtPdhDe1 Tha6A290021PdhDe2De1New(uint32 channelId, AtModulePdh module);
AtPdhDe3 Tha6A290021PdhVcDe3New(uint32 channelId, AtModulePdh module, AtSdhChannel sdhChannel);

uint16 Tha60290021ModulePdhAttLongReadOnCore(Tha6A290021ModulePdh self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core, uint32 holdRegOffset);
uint16 Tha60290021ModulePdhAttLongWriteOnCore(Tha6A290021ModulePdh self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core, uint32 holdRegOffset);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021MODULEPDHINTERNAL_H_ */

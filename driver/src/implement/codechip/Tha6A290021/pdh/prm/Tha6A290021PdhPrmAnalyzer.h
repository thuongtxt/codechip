/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290022PdhPrmAnalyzer.h
 * 
 * Created Date: Nov 15, 2017
 *
 * Description : PRM analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290021PDHPRMANALYZER_H_
#define _THA6A290021PDHPRMANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer Tha6A290021PdhPrmAnalyzerNew(AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290021PDHPRMANALYZER_H_ */


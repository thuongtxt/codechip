/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_RD_PDH_PRM_H_
#define _AF6_REG_AF6CNC0021_RD_PDH_PRM_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL TX STUFF 0
Reg Addr   : 0x0_6000
Reg Formula: 
    Where  : 
Reg Desc   : 
config control Stuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_ctrl0                                                                         0x06000

/*--------------------------------------
BitField Name: out_txcfg_ctrl0
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [31:08]
--------------------------------------*/
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_Mask                                                      cBit31_8
#define cAf6_upen_cfg_ctrl0_out_txcfg_ctrl0_Shift                                                            8

/*--------------------------------------
BitField Name: fcs_err_ins
BitField Type: R/W
BitField Desc: (0): disable,(1): enable insert error
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_cfg_ctrl0_fcs_err_ins_Mask                                                             cBit7
#define cAf6_upen_cfg_ctrl0_fcs_err_ins_Shift                                                                7

/*--------------------------------------
BitField Name: reserve1
BitField Type: R/W
BitField Desc: reserve1
BitField Bits: [06:02]
--------------------------------------*/
#define cAf6_upen_cfg_ctrl0_reserve1_Mask                                                              cBit6_2
#define cAf6_upen_cfg_ctrl0_reserve1_Shift                                                                   2

/*--------------------------------------
BitField Name: fcsinscfg
BitField Type: R/W
BitField Desc: (1) enable insert FCS, (0) disable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cfg_ctrl0_fcsinscfg_Mask                                                               cBit1
#define cAf6_upen_cfg_ctrl0_fcsinscfg_Shift                                                                  1


/*------------------------------------------------------------------------------
Reg Name   : CONFIG FORCE INFO PRM
Reg Addr   : 0x0_6005
Reg Formula: 
    Where  : 
Reg Desc   : 
config control PRM INFO

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_prm_ctrl_Base                                                                     0x06005

/*--------------------------------------
BitField Name: enable_force
BitField Type: R/W
BitField Desc: select 8 bit INFO from PRM FORCE but except 2 bit NmNi, (1) is
enable, (0) is disable
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_prm_ctrl_enable_force_Mask                                                             cBit8
#define cAf6_upen_prm_ctrl_enable_force_Shift                                                                8

/*--------------------------------------
BitField Name: out_info_force
BitField Type: 
BitField Desc: 8 bits info force [07:05] : 3'd1 : to set bit G1 3'd2 :  to set
bit G2 3'd3 :  to set bit G3 3'd4 : to set bit G4 3'd5 : to set bit G5 3'd6 :
to set bit G6 [04] : to set bit SE [03] : to set bit FE [02] : to set bit LV
[01] : to set bit SL [00] : to set bit LB End: Begin:
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_prm_ctrl_out_info_force_Mask                                                         cBit7_0
#define cAf6_upen_prm_ctrl_out_info_force_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : TX CONFIG FORCE ERROR PARITY
Reg Addr   : 0x0_6C00
Reg Formula: 
    Where  : 
Reg Desc   : 
config control FORCE ERROR PARITY

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txcfg_force_Base                                                                  0x06C00
#define cAf6Reg_upen_txcfg_force_WidthVal                                                                   32

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng8
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng8_Mask                                               cBit25
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng8_Shift                                                  25

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng8
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng8_Mask                                               cBit24
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng8_Shift                                                  24

/*--------------------------------------
BitField Name: cfgforce_mdl_eng8
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng8_Mask                                                    cBit23
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng8_Shift                                                       23

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng7
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng7_Mask                                               cBit22
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng7_Shift                                                  22

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng7
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng7_Mask                                               cBit21
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng7_Shift                                                  21

/*--------------------------------------
BitField Name: cfgforce_mdl_eng7
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng7_Mask                                                    cBit20
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng7_Shift                                                       20

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng6
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng6_Mask                                               cBit19
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng6_Shift                                                  19

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng6
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng6_Mask                                               cBit18
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng6_Shift                                                  18

/*--------------------------------------
BitField Name: cfgforce_mdl_eng6
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng6_Mask                                                    cBit17
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng6_Shift                                                       17

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng5
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng5_Mask                                               cBit16
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng5_Shift                                                  16

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng5
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng5_Mask                                               cBit15
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng5_Shift                                                  15

/*--------------------------------------
BitField Name: cfgforce_mdl_eng5
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng5_Mask                                                    cBit14
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng5_Shift                                                       14

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng4
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng4_Mask                                               cBit13
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng4_Shift                                                  13

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng4
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng4_Mask                                               cBit12
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng4_Shift                                                  12

/*--------------------------------------
BitField Name: cfgforce_mdl_eng4
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng4_Mask                                                    cBit11
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng4_Shift                                                       11

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng3
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng3_Mask                                               cBit10
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng3_Shift                                                  10

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng3
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng3_Mask                                                cBit9
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng3_Shift                                                   9

/*--------------------------------------
BitField Name: cfgforce_mdl_eng3
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng3_Mask                                                     cBit8
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng3_Shift                                                        8

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng2
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng2_Mask                                                cBit7
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng2_Shift                                                   7

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng2
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng2_Mask                                                cBit6
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng2_Shift                                                   6

/*--------------------------------------
BitField Name: cfgforce_mdl_eng2
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng2_Mask                                                     cBit5
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng2_Shift                                                        5

/*--------------------------------------
BitField Name: cfgforce_mdlbuff2_eng1
BitField Type: R/W
BitField Desc: force error MDL buffer 2, (0) : disable, (1) : enable force
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng1_Mask                                                cBit4
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff2_eng1_Shift                                                   4

/*--------------------------------------
BitField Name: cfgforce_mdlbuff1_eng1
BitField Type: R/W
BitField Desc: force error MDL buffer 1, (0) : disable, (1) : enable force
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng1_Mask                                                cBit3
#define cAf6_upen_txcfg_force_cfgforce_mdlbuff1_eng1_Shift                                                   3

/*--------------------------------------
BitField Name: cfgforce_mdl_eng1
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng1_Mask                                                     cBit2
#define cAf6_upen_txcfg_force_cfgforce_mdl_eng1_Shift                                                        2

/*--------------------------------------
BitField Name: cfgforce_lbprm
BitField Type: R/W
BitField Desc: force error PRM LB config, (0) : disable, (1) : enable force
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_lbprm_Mask                                                        cBit1
#define cAf6_upen_txcfg_force_cfgforce_lbprm_Shift                                                           1

/*--------------------------------------
BitField Name: cfgforce_prm
BitField Type: R/W
BitField Desc: force error PRM config, (0) : disable, (1) : enable force
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_txcfg_force_cfgforce_prm_Mask                                                          cBit0
#define cAf6_upen_txcfg_force_cfgforce_prm_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : TX CONFIG DIS PARITY
Reg Addr   : 0x0_6C01
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DIS PARITY

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txcfg_dis_Base                                                                    0x06C01
#define cAf6Reg_upen_txcfg_dis_WidthVal                                                                     32

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng8
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng8_Mask                                                   cBit25
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng8_Shift                                                      25

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng8
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng8_Mask                                                   cBit24
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng8_Shift                                                      24

/*--------------------------------------
BitField Name: cfgdis_mdl_eng8
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng8_Mask                                                        cBit23
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng8_Shift                                                           23

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng7
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng7_Mask                                                   cBit22
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng7_Shift                                                      22

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng7
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng7_Mask                                                   cBit21
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng7_Shift                                                      21

/*--------------------------------------
BitField Name: cfgdis_mdl_eng7
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng7_Mask                                                        cBit20
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng7_Shift                                                           20

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng6
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng6_Mask                                                   cBit19
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng6_Shift                                                      19

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng6
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng6_Mask                                                   cBit18
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng6_Shift                                                      18

/*--------------------------------------
BitField Name: cfgdis_mdl_eng6
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng6_Mask                                                        cBit17
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng6_Shift                                                           17

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng5
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng5_Mask                                                   cBit16
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng5_Shift                                                      16

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng5
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng5_Mask                                                   cBit15
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng5_Shift                                                      15

/*--------------------------------------
BitField Name: cfgdis_mdl_eng5
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng5_Mask                                                        cBit14
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng5_Shift                                                           14

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng4
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng4_Mask                                                   cBit13
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng4_Shift                                                      13

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng4
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng4_Mask                                                   cBit12
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng4_Shift                                                      12

/*--------------------------------------
BitField Name: cfgdis_mdl_eng4
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng4_Mask                                                        cBit11
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng4_Shift                                                           11

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng3
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng3_Mask                                                   cBit10
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng3_Shift                                                      10

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng3
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng3_Mask                                                    cBit9
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng3_Shift                                                       9

/*--------------------------------------
BitField Name: cfgdis_mdl_eng3
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng3_Mask                                                         cBit8
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng3_Shift                                                            8

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng2
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng2_Mask                                                    cBit7
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng2_Shift                                                       7

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng2
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng2_Mask                                                    cBit6
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng2_Shift                                                       6

/*--------------------------------------
BitField Name: cfgdis_mdl_eng2
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng2_Mask                                                         cBit5
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng2_Shift                                                            5

/*--------------------------------------
BitField Name: cfgdis_mdlbuff2_eng1
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng1_Mask                                                    cBit4
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff2_eng1_Shift                                                       4

/*--------------------------------------
BitField Name: cfgdis_mdlbuff1_eng1
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng1_Mask                                                    cBit3
#define cAf6_upen_txcfg_dis_cfgdis_mdlbuff1_eng1_Shift                                                       3

/*--------------------------------------
BitField Name: cfgdis_mdl_eng1
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng1_Mask                                                         cBit2
#define cAf6_upen_txcfg_dis_cfgdis_mdl_eng1_Shift                                                            2

/*--------------------------------------
BitField Name: cfgdis_lbprm
BitField Type: R/W
BitField Desc: dis error PRM LB config, (0) : disable, (1) : enable dis
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_lbprm_Mask                                                            cBit1
#define cAf6_upen_txcfg_dis_cfgdis_lbprm_Shift                                                               1

/*--------------------------------------
BitField Name: cfgdis_prm
BitField Type: R/W
BitField Desc: dis error PRM config, (0) : disable, (1) : enable dis
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_txcfg_dis_cfgdis_prm_Mask                                                              cBit0
#define cAf6_upen_txcfg_dis_cfgdis_prm_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : TX STICKY PARITY
Reg Addr   : 0x0_6C02
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txsticky_par_Base                                                                 0x06C02
#define cAf6Reg_upen_txsticky_par_WidthVal                                                                  32

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng8
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng8_Mask                                                   cBit25
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng8_Shift                                                      25

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng8
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng8_Mask                                                   cBit24
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng8_Shift                                                      24

/*--------------------------------------
BitField Name: stk_mdl_eng8
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng8_Mask                                                        cBit23
#define cAf6_upen_txsticky_par_stk_mdl_eng8_Shift                                                           23

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng7
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng7_Mask                                                   cBit22
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng7_Shift                                                      22

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng7
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng7_Mask                                                   cBit21
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng7_Shift                                                      21

/*--------------------------------------
BitField Name: stk_mdl_eng7
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng7_Mask                                                        cBit20
#define cAf6_upen_txsticky_par_stk_mdl_eng7_Shift                                                           20

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng6
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng6_Mask                                                   cBit19
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng6_Shift                                                      19

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng6
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng6_Mask                                                   cBit18
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng6_Shift                                                      18

/*--------------------------------------
BitField Name: stk_mdl_eng6
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng6_Mask                                                        cBit17
#define cAf6_upen_txsticky_par_stk_mdl_eng6_Shift                                                           17

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng5
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng5_Mask                                                   cBit16
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng5_Shift                                                      16

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng5
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [15:15]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng5_Mask                                                   cBit15
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng5_Shift                                                      15

/*--------------------------------------
BitField Name: stk_mdl_eng5
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [14:14]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng5_Mask                                                        cBit14
#define cAf6_upen_txsticky_par_stk_mdl_eng5_Shift                                                           14

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng4
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng4_Mask                                                   cBit13
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng4_Shift                                                      13

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng4
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng4_Mask                                                   cBit12
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng4_Shift                                                      12

/*--------------------------------------
BitField Name: stk_mdl_eng4
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng4_Mask                                                        cBit11
#define cAf6_upen_txsticky_par_stk_mdl_eng4_Shift                                                           11

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng3
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng3_Mask                                                   cBit10
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng3_Shift                                                      10

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng3
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng3_Mask                                                    cBit9
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng3_Shift                                                       9

/*--------------------------------------
BitField Name: stk_mdl_eng3
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng3_Mask                                                         cBit8
#define cAf6_upen_txsticky_par_stk_mdl_eng3_Shift                                                            8

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng2
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng2_Mask                                                    cBit7
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng2_Shift                                                       7

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng2
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng2_Mask                                                    cBit6
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng2_Shift                                                       6

/*--------------------------------------
BitField Name: stk_mdl_eng2
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng2_Mask                                                         cBit5
#define cAf6_upen_txsticky_par_stk_mdl_eng2_Shift                                                            5

/*--------------------------------------
BitField Name: stk_mdlbuff2_eng1
BitField Type: R/W
BitField Desc: dis error MDL buffer 2, (0) : disable, (1) : enable dis
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng1_Mask                                                    cBit4
#define cAf6_upen_txsticky_par_stk_mdlbuff2_eng1_Shift                                                       4

/*--------------------------------------
BitField Name: stk_mdlbuff1_eng1
BitField Type: R/W
BitField Desc: dis error MDL buffer 1, (0) : disable, (1) : enable dis
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng1_Mask                                                    cBit3
#define cAf6_upen_txsticky_par_stk_mdlbuff1_eng1_Shift                                                       3

/*--------------------------------------
BitField Name: stk_mdl_eng1
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_mdl_eng1_Mask                                                         cBit2
#define cAf6_upen_txsticky_par_stk_mdl_eng1_Shift                                                            2

/*--------------------------------------
BitField Name: stk_lbprm
BitField Type: R/W
BitField Desc: dis error PRM LB config, (0) : disable, (1) : enable dis
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_lbprm_Mask                                                            cBit1
#define cAf6_upen_txsticky_par_stk_lbprm_Shift                                                               1

/*--------------------------------------
BitField Name: stk_prm
BitField Type: R/W
BitField Desc: dis error PRM config, (0) : disable, (1) : enable dis
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_txsticky_par_stk_prm_Mask                                                              cBit0
#define cAf6_upen_txsticky_par_stk_prm_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG PRM BIT C/R & STANDARD Tx
Reg Addr   : 0x1_0000 - 0x1_3FFF
Reg Formula: 0x1_0000+ $STSID*256 + $VTGID*32 + $VTID*8 + $SLICEID
    Where  : 
           + $STSID (0-47) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
Config PRM bit C/R & Standard

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_prm_txcfgcr_Base                                                                   0x8000
#define cAf6Reg_upen_prm_txcfgcr_WidthVal                                                                   32

/*--------------------------------------
BitField Name: cfg_prmen_tx
BitField Type: R/W
BitField Desc: config enable Tx, (0) is disable (1) is enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Mask                                                          cBit3
#define cAf6_upen_prm_txcfgcr_cfg_prmen_tx_Shift                                                             3

/*--------------------------------------
BitField Name: cfg_prmfcs_tx
BitField Type: R/W
BitField Desc: config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prmfcs_tx_Mask                                                         cBit2
#define cAf6_upen_prm_txcfgcr_cfg_prmfcs_tx_Shift                                                            2

/*--------------------------------------
BitField Name: cfg_prmstd_tx
BitField Type: R/W
BitField Desc: config standard Tx, (0) is ANSI, (1) is AT&T
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Mask                                                         cBit1
#define cAf6_upen_prm_txcfgcr_cfg_prmstd_tx_Shift                                                            1

/*--------------------------------------
BitField Name: cfg_prm_cr
BitField Type: R/W
BitField Desc: config bit command/respond PRM message
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Mask                                                            cBit0
#define cAf6_upen_prm_txcfgcr_cfg_prm_cr_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG PRM BIT L/B Tx
Reg Addr   : 0xA000 - 0x1_7FFF
Reg Formula: 0xA000+ $STSID*256 + $VTGID*32 + $VTID*8 + $SLICEID
    Where  : 
           + $STSID (0-47) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
CONFIG PRM BIT L/B Tx

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_prm_txcfglb_Base                                                                   0xA000
#define cAf6Reg_upen_prm_txcfglb_WidthVal                                                                   32

/*--------------------------------------
BitField Name: cfg_prm_enlb
BitField Type: R/W
BitField Desc: config enable CPU config LB bit, (0) is disable, (1) is enable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Mask                                                          cBit1
#define cAf6_upen_prm_txcfglb_cfg_prm_enlb_Shift                                                             1

/*--------------------------------------
BitField Name: cfg_prm_lb
BitField Type: R/W
BitField Desc: config bit Loopback PRM message
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Mask                                                            cBit0
#define cAf6_upen_prm_txcfglb_cfg_prm_lb_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX PRM
Reg Addr   : 0x1_0000 - 0x2_7FFF
Reg Formula: 0x2_0000+ $STSID*256 + $VTGID*32 + $VTID*8 + $SLICEID + $UPRO * 16384
    Where  : 
           + $UPRO (0-1) : upen read only
           + $STSID (0-47) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
counter byte message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txprm_cnt_byte_Base                                                               0x10000

/*--------------------------------------
BitField Name: cnt_byte_prm
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Mask                                                    cBit31_0
#define cAf6_upen_txprm_cnt_byte_cnt_byte_prm_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER PACKET MESSAGE TX PRM
Reg Addr   : 0x14000 - 0x2_FFFF
Reg Formula: 0x14000+ $STSID*256 + $VTGID*32 + $VTID*8 + $SLICEID + $UPRO * 16384
    Where  : 
           + $UPRO (0-1) : upen read only
           + $STSID (0-47) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
counter packet message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txprm_cnt_pkt_Base                                                                0x14000
#define cAf6Reg_upen_txprm_cnt_pkt_WidthVal                                                                 32

/*--------------------------------------
BitField Name: cnt_pkt_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Mask                                                      cBit14_0
#define cAf6_upen_txprm_cnt_pkt_cnt_pkt_prm_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message PRM STANDARD
Reg Addr   : 0x20000 - 0x4_3FFF
Reg Formula: 0x4_0000+ $STSID*256 + $VTGID*32 + $VTID*8 + $SLICEID
    Where  : 
           + $STSID (0-47) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
config standard message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_cfgstd_Base                                                                 0x20000
#define cAf6Reg_upen_rxprm_cfgstd_WidthVal                                                                  32

/*--------------------------------------
BitField Name: cfg_prmen_rx
BitField Type: R/W
BitField Desc: config enable Rx, (0) is disable (1) is enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_rxprm_cfgstd_cfg_prmen_rx_Mask                                                         cBit3
#define cAf6_upen_rxprm_cfgstd_cfg_prmen_rx_Shift                                                            3

/*--------------------------------------
BitField Name: cfg_prm_cr
BitField Type: R/W
BitField Desc: config C/R bit expected
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Mask                                                           cBit2
#define cAf6_upen_rxprm_cfgstd_cfg_prm_cr_Shift                                                              2

/*--------------------------------------
BitField Name: cfg_prmfcs_rx
BitField Type: R/W
BitField Desc: config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxprm_cfgstd_cfg_prmfcs_rx_Mask                                                        cBit1
#define cAf6_upen_rxprm_cfgstd_cfg_prmfcs_rx_Shift                                                           1

/*--------------------------------------
BitField Name: cfg_std_prm
BitField Type: R/W
BitField Desc: config standard   (0) is ANSI, (1) is AT&T
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Mask                                                          cBit0
#define cAf6_upen_rxprm_cfgstd_cfg_std_prm_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL DESTUFF 0
Reg Addr   : 0x23200
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_destuff_ctrl0_Base                                                                0x23200
#define cAf6Reg_upen_destuff_ctrl0_WidthVal                                                                 32

/*--------------------------------------
BitField Name: cfg_crmon
BitField Type: R/W
BitField Desc: (0) : disable, (1) : enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Mask                                                           cBit5
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Shift                                                              5

/*--------------------------------------
BitField Name: reserve1
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_reserve1_Mask                                                            cBit4
#define cAf6_upen_destuff_ctrl0_reserve1_Shift                                                               4

/*--------------------------------------
BitField Name: fcsmon
BitField Type: R/W
BitField Desc: (0) : disable monitor, (1) : enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_fcsmon_Mask                                                              cBit3
#define cAf6_upen_destuff_ctrl0_fcsmon_Shift                                                                 3

/*--------------------------------------
BitField Name: headermon_en
BitField Type: R/W
BitField Desc: (1) : enable monitor, (0) disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_headermon_en_Mask                                                        cBit0
#define cAf6_upen_destuff_ctrl0_headermon_en_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL RX PRM MONITOR
Reg Addr   : 0x23201
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_ctrl_Base                                                                   0x23201

/*--------------------------------------
BitField Name: prm_slid
BitField Type: R/W
BitField Desc: config slice ID to capture
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_upen_rxprm_ctrl_prm_slid_Mask                                                           cBit18_16
#define cAf6_upen_rxprm_ctrl_prm_slid_Shift                                                                 16

/*--------------------------------------
BitField Name: prm_cfgid
BitField Type: R/W
BitField Desc: PRM ID config monitor,
BitField Bits: [14:04]
--------------------------------------*/
#define cAf6_upen_rxprm_ctrl_prm_cfgid_Mask                                                           cBit14_4
#define cAf6_upen_rxprm_ctrl_prm_cfgid_Shift                                                                 4

/*--------------------------------------
BitField Name: prm_mon_en
BitField Type: R/W
BitField Desc: (1) : enable monitor, (0) disable when enable start monitor, 7
consecutive PRM message will been captured after 7 consecutive second this
monitor will stop when finish capture the last PRM message if need continue
capturing PRM message, need disable and enable again, and after 7s , will have
new 7 PRM message notice : while capture PRM message, this bit need to be value
"1"
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxprm_ctrl_prm_mon_en_Mask                                                             cBit0
#define cAf6_upen_rxprm_ctrl_prm_mon_en_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : STATUS RX PRM FINISH CAPTUR
Reg Addr   : 0x4_AA02
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_alr_capture_Base                                                                  0x4AA02
#define cAf6Reg_upen_alr_capture_WidthVal                                                                   32

/*--------------------------------------
BitField Name: alr_capture
BitField Type: R/W
BitField Desc: (1) : capture done, (0) tob ecapturing
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_alr_capture_alr_capture_Mask                                                           cBit0
#define cAf6_upen_alr_capture_alr_capture_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : BUFFER RX PRM MONITOR MESSAGE
Reg Addr   : 0x2_3680 -0x4_ACFF
Reg Formula: 0x2_3680+ $LOC
    Where  : 
           + $LOC (0-127) : LOC ID
Reg Desc   : 
config standard message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_mon_Base                                                                    0x23680
#define cAf6Reg_upen_rxprm_mon_WidthVal                                                                     32

/*--------------------------------------
BitField Name: val_mes
BitField Type: R/W
BitField Desc: value message of PRM
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_rxprm_mon_val_mes_Mask                                                               cBit7_0
#define cAf6_upen_rxprm_mon_val_mes_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX PRM
Reg Addr   : 0x30000 - 0x6_7FFF
Reg Formula: 0x6_0000+ $STSID*256 + $VTGID*32 + $VTID*8 + $SLICEID + $UPRO * 16384
    Where  : 
           + $UPRO (0-1) : upen read only
           + $STSID (0-47) : STS ID
           + $VTGID (0-3) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
counter good message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_gmess_Base                                                                  0x30000
#define cAf6Reg_upen_rxprm_gmess_WidthVal                                                                   32

/*--------------------------------------
BitField Name: cnt_gmess_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Mask                                                      cBit14_0
#define cAf6_upen_rxprm_gmess_cnt_gmess_prm_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX PRM
Reg Addr   : 0x34000 - 0x6_FFFF
Reg Formula: 0x34000+ $STSID*256 + $VTGID*32 + $VTID*8 + $SLICEID + $UPRO * 16384
    Where  : 
           + $UPRO (0-1) : upen read only
           + $STSID (0-47) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
counter drop message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_drmess_Base                                                                 0x34000
#define cAf6Reg_upen_rxprm_drmess_WidthVal                                                                  32

/*--------------------------------------
BitField Name: cnt_drmess_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Mask                                                    cBit14_0
#define cAf6_upen_rxprm_drmess_cnt_drmess_prm_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER MISS MESSAGE RX PRM
Reg Addr   : 0x38000 - 0x7_7FFF
Reg Formula: 0x38000+ $STSID*256 + $VTGID*32 + $VTID*8 + $SLICEID + $UPRO * 16384
    Where  : 
           + $UPRO (0-1) : upen read only
           + $STSID (0-47) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
counter miss message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_mmess_Base                                                                  0x38000
#define cAf6Reg_upen_rxprm_mmess_WidthVal                                                                   32

/*--------------------------------------
BitField Name: cnt_mmess_prm
BitField Type: R/W
BitField Desc: value counter packet
BitField Bits: [14:00]
--------------------------------------*/
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Mask                                                      cBit14_0
#define cAf6_upen_rxprm_mmess_cnt_mmess_prm_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX PRM
Reg Addr   : 0x3C000 - 0x7_FFFF
Reg Formula: 0x3C000+ $STSID*256 + $VTGID*32 + $VTID*8 + $SLICEID + $UPRO * 16384
    Where  : 
           + $UPRO (0-1) : upen read only
           + $STSID (0-47) : STS ID
           + $VTGID (0-6) : VTG ID
           + $SLICEID (0-7) : SLICE ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
counter byte message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_cnt_byte_Base                                                               0x3C000

/*--------------------------------------
BitField Name: cnt_byte_rxprm
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Mask                                                  cBit31_0
#define cAf6_upen_rxprm_cnt_byte_cnt_byte_rxprm_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : RX CONFIG FORCE ERROR PARITY
Reg Addr   : 0x23600
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxcfg_force_Base                                                                  0x23600
#define cAf6Reg_upen_rxcfg_force_WidthVal                                                                   32

/*--------------------------------------
BitField Name: cfgforce_rxmdl
BitField Type: R/W
BitField Desc: force error MDL config, (0) : disable, (1) : enable force
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxcfg_force_cfgforce_rxmdl_Mask                                                        cBit1
#define cAf6_upen_rxcfg_force_cfgforce_rxmdl_Shift                                                           1

/*--------------------------------------
BitField Name: cfgforce_rxprm
BitField Type: R/W
BitField Desc: force error PRM config, (0) : disable, (1) : enable force
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxcfg_force_cfgforce_rxprm_Mask                                                        cBit0
#define cAf6_upen_rxcfg_force_cfgforce_rxprm_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : RX CONFIG DIS PARITY
Reg Addr   : 0x23601
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxcfg_dis_Base                                                                    0x23601
#define cAf6Reg_upen_rxcfg_dis_WidthVal                                                                     32

/*--------------------------------------
BitField Name: cfgdis_rxmdl
BitField Type: R/W
BitField Desc: dis error MDL config, (0) : disable, (1) : enable dis
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxcfg_dis_cfgdis_rxmdl_Mask                                                            cBit1
#define cAf6_upen_rxcfg_dis_cfgdis_rxmdl_Shift                                                               1

/*--------------------------------------
BitField Name: cfgdis_rxprm
BitField Type: R/W
BitField Desc: dis error PRM config, (0) : disable, (1) : enable dis
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxcfg_dis_cfgdis_rxprm_Mask                                                            cBit0
#define cAf6_upen_rxcfg_dis_cfgdis_rxprm_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : RX STICKY PARITY
Reg Addr   : 0x23602
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxsticky_par_Base                                                                 0x23602
#define cAf6Reg_upen_rxsticky_par_WidthVal                                                                  32

/*--------------------------------------
BitField Name: stk_rxmdl
BitField Type: R/W
BitField Desc: sticky for mdl config error parity
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxsticky_par_stk_rxmdl_Mask                                                            cBit1
#define cAf6_upen_rxsticky_par_stk_rxmdl_Shift                                                               1

/*--------------------------------------
BitField Name: stk_rxprm
BitField Type: R/W
BitField Desc: sticky for prm config error parity
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxsticky_par_stk_rxprm_Mask                                                            cBit0
#define cAf6_upen_rxsticky_par_stk_rxprm_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : RX PRM INT STA
Reg Addr   : 0x4AA05
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_prmsta_Base                                                                   0x4AA05
#define cAf6Reg_upen_int_prmsta_WidthVal                                                                    32

/*--------------------------------------
BitField Name: prmlb_intsta2
BitField Type: R/W
BitField Desc: LB interrupt sta PRM 2
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_int_prmsta_prmlb_intsta2_Mask                                                          cBit1
#define cAf6_upen_int_prmsta_prmlb_intsta2_Shift                                                             1

/*--------------------------------------
BitField Name: prmlb_intsta1
BitField Type: R/W
BitField Desc: LB interrupt sta PRM 1
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_int_prmsta_prmlb_intsta1_Mask                                                          cBit0
#define cAf6_upen_int_prmsta_prmlb_intsta1_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB per Channel Interrupt1 Enable Control
Reg Addr   : 0x4_8000-0x4_83FF
Reg Formula: 0x4_8000+ $STSID1*32 + $VTGID*4 + $VTID
    Where  : 
           + $STSID1 (0-31) : STS1 ID
           + $VTGID (0-6) : VTG ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
This is the per Channel interrupt enable

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_cfg_lben_int1_Base                                                                 0x28000
#define cAf6Reg_prm_cfg_lben_int1_WidthVal                                                                  32

/*--------------------------------------
BitField Name: prm1_cfglben_int7
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int7_Mask                                                    cBit7
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int7_Shift                                                       7

/*--------------------------------------
BitField Name: prm1_cfglben_int6
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int6_Mask                                                    cBit6
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int6_Shift                                                       6

/*--------------------------------------
BitField Name: prm1_cfglben_int5
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int5_Mask                                                    cBit5
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int5_Shift                                                       5

/*--------------------------------------
BitField Name: prm1_cfglben_int4
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 5
BitField Bits: [4]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int4_Mask                                                    cBit4
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int4_Shift                                                       4

/*--------------------------------------
BitField Name: prm1_cfglben_int3
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int3_Mask                                                    cBit3
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int3_Shift                                                       3

/*--------------------------------------
BitField Name: prm1_cfglben_int2
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int2_Mask                                                    cBit2
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int2_Shift                                                       2

/*--------------------------------------
BitField Name: prm1_cfglben_int1
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int1_Mask                                                    cBit1
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int1_Shift                                                       1

/*--------------------------------------
BitField Name: prm1_cfglben_int0
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int0_Mask                                                    cBit0
#define cAf6_prm_cfg_lben_int1_prm1_cfglben_int0_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt1 per Channel Interrupt Status
Reg Addr   : 0x4_8400-0x4_87FF
Reg Formula: 0x4_8400+ $STSID1*32 + $VTGID*4 + $VTID
    Where  : 
           + $STSID1 (0-32) : STS1 ID
           + $VTGID (0-6) : VTG ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
This is the per Channel interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_int1_sta_Base                                                                   0x28400
#define cAf6Reg_prm_lb_int1_sta_WidthVal                                                                    32

/*--------------------------------------
BitField Name: prm_lbint1_sta7
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta7_Mask                                                        cBit7
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta7_Shift                                                           7

/*--------------------------------------
BitField Name: prm_lbint1_sta6
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta6_Mask                                                        cBit6
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta6_Shift                                                           6

/*--------------------------------------
BitField Name: prm_lbint1_sta5
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta5_Mask                                                        cBit5
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta5_Shift                                                           5

/*--------------------------------------
BitField Name: prm_lbint1_sta4
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 4
BitField Bits: [4]
--------------------------------------*/
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta4_Mask                                                        cBit4
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta4_Shift                                                           4

/*--------------------------------------
BitField Name: prm_lbint1_sta3
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta3_Mask                                                        cBit3
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta3_Shift                                                           3

/*--------------------------------------
BitField Name: prm_lbint1_sta2
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta2_Mask                                                        cBit2
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta2_Shift                                                           2

/*--------------------------------------
BitField Name: prm_lbint1_sta1
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta1_Mask                                                        cBit1
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta1_Shift                                                           1

/*--------------------------------------
BitField Name: prm_lbint1_sta0
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta0_Mask                                                        cBit0
#define cAf6_prm_lb_int1_sta_prm_lbint1_sta0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt1 per Channel Current Status
Reg Addr   : 0x2_8800-0x2_8BFF
Reg Formula: 0x2_8800+ $STSID1*32 + $VTGID*4 + $VTID
    Where  : 
           + $STSID1 (0-31) : STS1 ID
           + $VTGID (0-6) : VTG ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
This is the per Channel Current status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_int1_crrsta_Base                                                                0x28800
#define cAf6Reg_prm_lb_int1_crrsta_WidthVal                                                                 32

/*--------------------------------------
BitField Name: prm_lbint1_crrsta7
BitField Type: RW
BitField Desc: Current status of event slice 7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta7_Mask                                                  cBit7
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta7_Shift                                                     7

/*--------------------------------------
BitField Name: prm_lbint1_crrsta6
BitField Type: RW
BitField Desc: Current status of event slice 6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta6_Mask                                                  cBit6
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta6_Shift                                                     6

/*--------------------------------------
BitField Name: prm_lbint1_crrsta5
BitField Type: RW
BitField Desc: Current status of event slice 5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta5_Mask                                                  cBit5
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta5_Shift                                                     5

/*--------------------------------------
BitField Name: prm_lbint1_crrsta4
BitField Type: RW
BitField Desc: Current status of event slice 4
BitField Bits: [4]
--------------------------------------*/
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta4_Mask                                                  cBit4
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta4_Shift                                                     4

/*--------------------------------------
BitField Name: prm_lbint1_crrsta3
BitField Type: RW
BitField Desc: Current status of event slice 3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta3_Mask                                                  cBit3
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta3_Shift                                                     3

/*--------------------------------------
BitField Name: prm_lbint1_crrsta2
BitField Type: RW
BitField Desc: Current status of event slice 2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta2_Mask                                                  cBit2
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta2_Shift                                                     2

/*--------------------------------------
BitField Name: prm_lbint1_crrsta1
BitField Type: RW
BitField Desc: Current status of event slice 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta1_Mask                                                  cBit1
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta1_Shift                                                     1

/*--------------------------------------
BitField Name: prm_lbint1_crrsta0
BitField Type: RW
BitField Desc: Current status of event slice 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta0_Mask                                                  cBit0
#define cAf6_prm_lb_int1_crrsta_prm_lbint1_crrsta0_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt1 per Channel Interrupt OR Status
Reg Addr   : 0x4_8C00-0x4_8C20
Reg Formula: 0x4_8C00 +  $GID
    Where  : 
           + $GID (0-31) : group ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_int1sta_Base                                                                    0x28C00

/*--------------------------------------
BitField Name: prm_lbint1sta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding channel is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prm_lb_int1sta_prm_lbint1sta_Mask                                                        cBit31_0
#define cAf6_prm_lb_int1sta_prm_lbint1sta_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt1 OR Status
Reg Addr   : 0x2_8FFF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits. Each bit is used to store Interrupt OR status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_sta_int1_Base                                                                   0x28FFF

/*--------------------------------------
BitField Name: prm_lbsta_int1
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit is set and its interrupt is
enabled
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prm_lb_sta_int1_prm_lbsta_int1_Mask                                                      cBit31_0
#define cAf6_prm_lb_sta_int1_prm_lbsta_int1_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt1 Enable Control
Reg Addr   : 0x4_8FFE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 interrupt enable bits .

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_en_int_Base                                                                     0x28FFE

/*--------------------------------------
BitField Name: prm_lben_int
BitField Type: RW
BitField Desc: Set to 1 to enable to generate interrupt.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prm_lb_en_int_prm_lben_int_Mask                                                          cBit31_0
#define cAf6_prm_lb_en_int_prm_lben_int_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB per Channel Interrupt Enable Control
Reg Addr   : 0x28000-0x4_93FF
Reg Formula: 0x28000+ $STSID2*32 + $VTGID*4 + $VTID
    Where  : 
           + $STSID2 (0-15) : STS2 ID
           + $VTGID (0-6) : VTG ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
This is the per Channel interrupt enable

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_cfg_lben_int2_Base                                                                 0x28000
#define cAf6Reg_prm_cfg_lben_int2_WidthVal                                                                  32

/*--------------------------------------
BitField Name: prm2_cfglben_int7
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int7_Mask                                                    cBit7
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int7_Shift                                                       7

/*--------------------------------------
BitField Name: prm2_cfglben_int6
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int6_Mask                                                    cBit6
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int6_Shift                                                       6

/*--------------------------------------
BitField Name: prm2_cfglben_int5
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int5_Mask                                                    cBit5
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int5_Shift                                                       5

/*--------------------------------------
BitField Name: prm2_cfglben_int4
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 4
BitField Bits: [4]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int4_Mask                                                    cBit4
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int4_Shift                                                       4

/*--------------------------------------
BitField Name: prm2_cfglben_int3
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int3_Mask                                                    cBit3
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int3_Shift                                                       3

/*--------------------------------------
BitField Name: prm2_cfglben_int2
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int2_Mask                                                    cBit2
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int2_Shift                                                       2

/*--------------------------------------
BitField Name: prm2_cfglben_int1
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int1_Mask                                                    cBit1
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int1_Shift                                                       1

/*--------------------------------------
BitField Name: prm2_cfglben_int0
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt slice 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int0_Mask                                                    cBit0
#define cAf6_prm_cfg_lben_int2_prm2_cfglben_int0_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt per Channel Interrupt Status
Reg Addr   : 0x28400-0x4_97FF
Reg Formula: 0x28400+ $STSID2*32 + $VTGID*4 + $VTID
    Where  : 
           + $STSID2 (0-15) : STS2 ID
           + $VTGID (0-6) : VTG ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
This is the per Channel interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_int2_sta_Base                                                                   0x28400
#define cAf6Reg_prm_lb_int2_sta_WidthVal                                                                    32

/*--------------------------------------
BitField Name: prm_lbint2_sta7
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta7_Mask                                                        cBit7
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta7_Shift                                                           7

/*--------------------------------------
BitField Name: prm_lbint2_sta6
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta6_Mask                                                        cBit6
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta6_Shift                                                           6

/*--------------------------------------
BitField Name: prm_lbint2_sta5
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta5_Mask                                                        cBit5
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta5_Shift                                                           5

/*--------------------------------------
BitField Name: prm_lbint2_sta4
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 4
BitField Bits: [4]
--------------------------------------*/
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta4_Mask                                                        cBit4
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta4_Shift                                                           4

/*--------------------------------------
BitField Name: prm_lbint2_sta3
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta3_Mask                                                        cBit3
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta3_Shift                                                           3

/*--------------------------------------
BitField Name: prm_lbint2_sta2
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta2_Mask                                                        cBit2
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta2_Shift                                                           2

/*--------------------------------------
BitField Name: prm_lbint2_sta1
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta1_Mask                                                        cBit1
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta1_Shift                                                           1

/*--------------------------------------
BitField Name: prm_lbint2_sta0
BitField Type: RW
BitField Desc: Set 1 if there is a change event slice 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta0_Mask                                                        cBit0
#define cAf6_prm_lb_int2_sta_prm_lbint2_sta0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt per Channel Current Status
Reg Addr   : 0x28800-0x4_9BFF
Reg Formula: 0x28800+ $STSID2*32 + $VTGID*4 + $VTID
    Where  : 
           + $STSID2 (0-15) : STS2 ID
           + $VTGID (0-6) : VTG ID
           + $VTID (0-3) : VT ID
Reg Desc   : 
This is the per Channel Current status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_int2_crrsta_Base                                                                0x28800
#define cAf6Reg_prm_lb_int2_crrsta_WidthVal                                                                 32

/*--------------------------------------
BitField Name: prm_lbint2_crrsta7
BitField Type: RW
BitField Desc: Current status of event slice 7
BitField Bits: [7]
--------------------------------------*/
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta7_Mask                                                  cBit7
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta7_Shift                                                     7

/*--------------------------------------
BitField Name: prm_lbint2_crrsta6
BitField Type: RW
BitField Desc: Current status of event slice 6
BitField Bits: [6]
--------------------------------------*/
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta6_Mask                                                  cBit6
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta6_Shift                                                     6

/*--------------------------------------
BitField Name: prm_lbint2_crrsta5
BitField Type: RW
BitField Desc: Current status of event slice 5
BitField Bits: [5]
--------------------------------------*/
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta5_Mask                                                  cBit5
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta5_Shift                                                     5

/*--------------------------------------
BitField Name: prm_lbint2_crrsta4
BitField Type: RW
BitField Desc: Current status of event slice 4
BitField Bits: [4]
--------------------------------------*/
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta4_Mask                                                  cBit4
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta4_Shift                                                     4

/*--------------------------------------
BitField Name: prm_lbint2_crrsta3
BitField Type: RW
BitField Desc: Current status of event slice 3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta3_Mask                                                  cBit3
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta3_Shift                                                     3

/*--------------------------------------
BitField Name: prm_lbint2_crrsta2
BitField Type: RW
BitField Desc: Current status of event slice 2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta2_Mask                                                  cBit2
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta2_Shift                                                     2

/*--------------------------------------
BitField Name: prm_lbint2_crrsta1
BitField Type: RW
BitField Desc: Current status of event slice 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta1_Mask                                                  cBit1
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta1_Shift                                                     1

/*--------------------------------------
BitField Name: prm_lbint2_crrsta0
BitField Type: RW
BitField Desc: Current status of event slice 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta0_Mask                                                  cBit0
#define cAf6_prm_lb_int2_crrsta_prm_lbint2_crrsta0_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt per Channel Interrupt OR Status
Reg Addr   : 0x28C00-0x4_9C20
Reg Formula: 0x28C00 +  $GID
    Where  : 
           + $GID (0-31) : group ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_intsta2_Base                                                                    0x28C00

/*--------------------------------------
BitField Name: prm_lbintsta2
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding channel is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prm_lb_intsta2_prm_lbintsta2_Mask                                                        cBit31_0
#define cAf6_prm_lb_intsta2_prm_lbintsta2_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt OR Status
Reg Addr   : 0x4_9FFF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits. Each bit is used to store Interrupt OR status.

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_sta_int2_Base                                                                   0x49FFF

/*--------------------------------------
BitField Name: prm_lbsta_int2
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit is set and its interrupt is
enabled
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prm_lb_sta_int2_prm_lbsta_int2_Mask                                                      cBit31_0
#define cAf6_prm_lb_sta_int2_prm_lbsta_int2_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : PRM LB Interrupt Enable Control
Reg Addr   : 0x28FFE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 interrupt enable bits .

------------------------------------------------------------------------------*/
#define cAf6Reg_prm_lb_en_int2_Base                                                                    0x28FFE

/*--------------------------------------
BitField Name: prm_lben_int2
BitField Type: RW
BitField Desc: Set to 1 to enable to generate interrupt.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prm_lb_en_int2_prm_lben_int2_Mask                                                        cBit31_0
#define cAf6_prm_lb_en_int2_prm_lben_int2_Shift                                                              0

#endif /* _AF6_REG_AF6CNC0022_RD_PDH_PRM_H_ */

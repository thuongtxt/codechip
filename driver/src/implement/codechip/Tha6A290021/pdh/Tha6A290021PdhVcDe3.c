/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A290021PdhVcDe3.c
 *
 * Created Date: May 17, 2016
 *
 * Description : VC DE3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/att/ThaAttPdhManagerInternal.h"
#include "../../Tha60290021/pdh/Tha60290021PdhVcDe3Internal.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../Tha60210031/pdh/Tha60210031PdhDe3Internal.h"
#include "Tha6A290021ModulePdhInternal.h"
#include "Tha6A290021PdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290021PdhVcDe3 *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021PdhVcDe3
    {
    tTha60290021PdhVcDe3 super;
    AtSdhChannel sdhChannel;
    }tTha6A290021PdhVcDe3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tThaPdhDe3Methods    m_ThaPdhDe3Override;

/* Save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/


static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret = cAtOk;
    ret = m_AtChannelMethods->TimingSet(self, timingMode, timingSource);
    if (timingMode == cAtTimingModeFreeRunning||cAtTimingModeSdhSys)
        AtAttControllerFrequenceOffsetSet(AtChannelAttController(self), 0);
    return ret;
    }

static uint8 DefaultAisAllOnesThreshold(ThaPdhDe3 self)
    {
    /* In DUT, this is 0 for DS3 and 1 for E3.
     * How ever in ATT should be 1 for both DS3 and E3 */
    AtUnused(self);
    return 1;
    }

static eBool IsChannelTerminatedLine(AtSdhChannel self)
    {
    uint8 lineId = AtSdhChannelLineGet(self);
    AtModuleSdh sdhModule = (AtModuleSdh) AtChannelModuleGet((AtChannel) self);

    /* Current design only support PRBS VC for terminated Line */
    return Tha60290021ModuleSdhLineIsTerminated(sdhModule, lineId);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (IsChannelTerminatedLine((AtSdhChannel)self))
        {
        ret |= AtAttControllerInit(AtChannelAttController(self));
        ret |= AtPdhDe3StuffModeSet((AtPdhDe3)self, cAtPdhDe3StuffModeFineStuff);
        }
    return ret;
    }

static eAtRet De3FramingBindToPwSatop(ThaPdhDe3 self, AtPw pseudowire)
    {
    AtUnused(self);
    AtUnused(pseudowire);
    return cAtOk;
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmTypes, eAtRet (*fForceAlarm)(AtAttController, uint32), eAtRet (*fSetClearAlarm)(AtAttController, uint32))
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 alarmType = alarmTypes & (cBit0 << i);
        if (alarmType)
            {
            AtAttControllerForceAlarmModeSet(attController, alarmType, cAtAttForceAlarmModeContinuous);
            fSetClearAlarm(attController, alarmType);
            ret =  fForceAlarm(attController, alarmType);
            }
        }
    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerForceAlarm,AtAttControllerTxAlarmsSet);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerUnForceAlarm,AtAttControllerTxAlarmsClear);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcibleAlarmsGet(self);

    return cAtPdhDe3AlarmLos | cAtPdhDe3AlarmAis | cAtPdhDe3AlarmRai | cAtPdhDe3AlarmLof;
    }

static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorTypes, eBool force)
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 errorType = errorTypes & (cBit0 << i);
        if (errorType)
            {
            eAtAttPdhDe3ErrorType attErrorType = errorTypes;
            AtAttControllerForceErrorModeSet(attController, attErrorType, cAtAttForceErrorModeContinuous);
            if (force)
                ret =  AtAttControllerForceError(attController, attErrorType);
            else
                ret = AtAttControllerUnForceError(attController, attErrorType);
            }
        }
    return ret;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorForce(self, errorType);

    return HelperTxErrorForce(self, errorType, cAtTrue);
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorUnForce(self, errorType);

    return HelperTxErrorForce(self, errorType, cAtFalse);
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcableErrorsGet(self);

    return cAtPdhDe3CounterRei | cAtPdhDe3CounterFBit | cAtPdhDe3CounterCPBit | cAtPdhDe3CounterPBit;
    }

static void OverrideAtChannel(AtChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        }

    mMethodsSet(self, &m_AtChannelOverride);
    }

static void OverrideThaPdhDe3(ThaPdhDe3 self)
    {

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe3Override, mMethodsGet(self), sizeof(m_ThaPdhDe3Override));

        /* Setup methods */
        mMethodOverride(m_ThaPdhDe3Override, De3FramingBindToPwSatop);
        mMethodOverride(m_ThaPdhDe3Override, DefaultAisAllOnesThreshold);
        }

    mMethodsSet(self, &m_ThaPdhDe3Override);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideAtChannel((AtChannel)self);
    OverrideThaPdhDe3((ThaPdhDe3)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021PdhVcDe3);
    }

static AtPdhDe3 ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module, AtSdhChannel sdhChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60290021PdhVcDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->sdhChannel = sdhChannel;
    return self;
    }

AtPdhDe3 Tha6A290021PdhVcDe3New(uint32 channelId, AtModulePdh module, AtSdhChannel sdhChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe3 == NULL)
        return NULL;

    /* construct it */
    return ObjectInit(newDe3, channelId, module, sdhChannel);
    }

eBool Tha6A290021AttPdhForceLogicFromDut(AtChannel self)
    {
    AtModulePdh module = (AtModulePdh)AtChannelModuleGet(self);
    ThaAttPdhManager attManger = (ThaAttPdhManager)AtModulePdhAttManager(module);
    return ThaAttPdhManagerDebugForcingLogicGet(attManger);
    }

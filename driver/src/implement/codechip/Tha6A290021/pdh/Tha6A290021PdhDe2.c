/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A290021PdhDe2.c
 *
 * Created Date: May 03, 2018
 *
 * Description : 6A290021 PDH DE2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290021PdhDe2Internal.h"
#include "Tha6A290021PdhReg.h"
#include "../../../default/pdh/ThaModulePdh.h"
#include "../../../default/man/ThaDevice.h"
#include "../pdh/Tha6A290021ModulePdhInternal.h"
#include "Tha6A290021PdhAttControllerInternal.h"
/*--------------------------- Define -----------------------------------------*/
#define cThaRegRxM12E12FrmChnAlarmCurStatusMask(de2)      (cBit0 << (de2))
#define cThaRegRxM12E12FrmChnAlarmCurStatusShift(de2)     (de2)

#define cThaRegPdhRxFrameChannelIntrEnableMask(de3, de2)      cBit0 << (de2)
#define cThaRegPdhRxFrameChannelIntrEnableShift(de3, de2)     (de2)

typedef struct tTha6A290021PdhDe2 * Tha6A290021PdhDe2;

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A290021PdhDe2)self)


/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;


/* Save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmTypes, eAtRet (*fForceAlarm)(AtAttController, uint32), eAtRet (*fSetClearAlarm)(AtAttController, uint32))
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 alarmType = alarmTypes & (cBit0 << i);
        if (alarmType)
            {
            AtAttControllerForceAlarmModeSet(attController, alarmType, cAtAttForceAlarmModeContinuous);
            fSetClearAlarm(attController, alarmType);
            ret =  fForceAlarm(attController, alarmType);
            }
        }
    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerForceAlarm,AtAttControllerTxAlarmsSet);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerUnForceAlarm,AtAttControllerTxAlarmsClear);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcibleAlarmsGet(self);

    return cAtPdhDe2AlarmLos | cAtPdhDe2AlarmAis | cAtPdhDe2AlarmRai | cAtPdhDe2AlarmLof;
    }

static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorTypes, eBool force)
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 errorType = errorTypes & (cBit0 << i);
        if (errorType)
            {
            eAtAttPdhDe3ErrorType attErrorType = errorTypes;
            AtAttControllerForceErrorModeSet(attController, attErrorType, cAtAttForceErrorModeContinuous);
            if (force)
                ret =  AtAttControllerForceError(attController, attErrorType);
            else
                ret = AtAttControllerUnForceError(attController, attErrorType);
            }
        }
    return ret;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorForce(self, errorType);

    return HelperTxErrorForce(self, errorType, cAtTrue);
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorUnForce(self, errorType);

    return HelperTxErrorForce(self, errorType, cAtFalse);
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcableErrorsGet(self);

    return cAtPdhDe2CounterFbe | cAtPdhDe2CounterParity;
    }

static eAtRet HelperInterruptEnable(ThaPdhDe1 self, eBool enable)
    {
    ThaModulePdh pdh = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    uint32 channelId = AtChannelIdGet((AtChannel)self);
    AtPdhChannel de3 = AtPdhChannelParentChannelGet((AtPdhChannel)self);
    uint32 address = ThaModulePdhRxM12E12FrmChnIntrEnCtrlRegister(pdh) + ThaModulePdhM12E12ControlOffset(pdh, (ThaPdhDe2)self);
    uint32 regValue  = mModuleHwRead(pdh, address);
    mFieldIns(&regValue, cAf6_RxM12E12_per_chn_intr_cfg_DE2ParCnt_En_Mask,        cAf6_RxM12E12_per_chn_intr_cfg_DE2ParCnt_En_Shift,        enable ? 1 : 0);
    mFieldIns(&regValue, cAf6_RxM12E12_per_chn_intr_cfg_DE2FBECnt_En_Mask,        cAf6_RxM12E12_per_chn_intr_cfg_DE2FBECnt_En_Shift,        enable ? 1 : 0);
    mFieldIns(&regValue, cAf6_RxM12E12_per_chn_intr_cfg_DE2LosAllZeroIntrEn_Mask, cAf6_RxM12E12_per_chn_intr_cfg_DE2LosAllZeroIntrEn_Shift, enable ? 1 : 0);
    mFieldIns(&regValue, cAf6_RxM12E12_per_chn_intr_cfg_DE2AisAllOneIntrEn_Mask,  cAf6_RxM12E12_per_chn_intr_cfg_DE2AisAllOneIntrEn_Shift,  enable ? 1 : 0);
    mFieldIns(&regValue, cAf6_RxM12E12_per_chn_intr_cfg_DE2RDIIntrEn_Mask,        cAf6_RxM12E12_per_chn_intr_cfg_DE2RDIIntrEn_Shift,        enable ? 1 : 0);
    mFieldIns(&regValue, cAf6_RxM12E12_per_chn_intr_cfg_DE2LofIntrEn_Mask,        cAf6_RxM12E12_per_chn_intr_cfg_DE2LofIntrEn_Shift,        enable ? 1 : 0);
    mModuleHwWrite(pdh, address, regValue);

    AtUnused(de3);
    address = cAf6Reg_RxM12E12_chn_intr_cfg + ThaModulePdhM12E12ControlOffset(pdh, (ThaPdhDe2)self);
    regValue  = mModuleHwRead(pdh, address);
    mFieldIns(&regValue, cThaRegPdhRxFrameChannelIntrEnableMask(AtChannelIdGet((AtChannel)de3), channelId),        cThaRegPdhRxFrameChannelIntrEnableShift(AtChannelIdGet((AtChannel)de3), channelId),        enable ? 1 : 0);

    mModuleHwWrite(pdh, address, regValue);
    return cAtOk;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    return HelperInterruptEnable((ThaPdhDe1)self, cAtTrue);
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    return HelperInterruptEnable((ThaPdhDe1)self, cAtFalse);
    }


static eBool InterruptIsEnabled(AtChannel self)
    {
    ThaModulePdh pdh = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    AtPdhChannel de3 = AtPdhChannelParentChannelGet((AtPdhChannel)self);
    uint32 channelId = AtChannelIdGet(self);
    uint32 address = ThaModulePdhRxM12E12FrmChnIntrEnCtrlRegister(pdh) + ThaModulePdhM12E12ControlOffset(pdh, (ThaPdhDe2)self);
    uint32 regValue = mChannelHwRead(self, address, cAtModulePdh);

    AtUnused(de3);
    return (regValue & cThaRegPdhRxFrameChannelIntrEnableMask(AtChannelIdGet((AtChannel)de3), channelId) ) ? cAtTrue : cAtFalse;
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regAddr, regVal;
    uint32 swAlarms = 0;
    ThaModulePdh pdh = (ThaModulePdh)AtChannelModuleGet(self);

    /* Read alarm from hardware */
    regAddr = ThaModulePdhRxM12E12FrmChnAlarmCurStatusRegister(pdh) + ThaModulePdhM12E12ControlOffset(pdh, (ThaPdhDe2)self);
    regVal  = mModuleHwRead(pdh, regAddr);

    /* Common alarms */
    if (regVal & cAf6_RxM12E12_per_chn_curr_stat_DE2LosAllZeroCurrSta_Mask)
        swAlarms |= cAtPdhDe2AlarmLos;
    if (regVal & cAf6_RxM12E12_per_chn_curr_stat_DE2AisAllOneCurrSta_Mask)
        swAlarms |= cAtPdhDe2AlarmAis;
    if (regVal & cAf6_RxM12E12_per_chn_curr_stat_DE2RDICurrSta_Mask)
        swAlarms |= cAtPdhDe2AlarmRai;
    if (regVal & cAf6_RxM12E12_per_chn_curr_stat_DE2LofCurrSta_Mask)
        swAlarms |= cAtPdhDe2AlarmLof;

    return swAlarms;
    }

static uint32 InterruptRead2Clear(AtChannel self, eBool clear)
    {
    uint32       swStatus = 0;
    ThaModulePdh pdh      = (ThaModulePdh)AtChannelModuleGet(self);
    uint32       regAddr  = ThaModulePdhRxM12E12FrmChnIntrChngStatusRegister(pdh) + ThaModulePdhM12E12ControlOffset(pdh, (ThaPdhDe2)self);
    uint32       regVal   = mModuleHwRead(pdh, regAddr);

    /* Common alarms */
    if (regVal & cAf6_RxM12E12_per_chn_intr_stat_DE2LosAllZero_Intr_Mask)
        swStatus |= cAtPdhDe2AlarmLos;
    if (regVal & cAf6_RxM12E12_per_chn_intr_stat_DE2AisAllOne_Intr_Mask)
        swStatus |= cAtPdhDe2AlarmAis;
    if (regVal & cAf6_RxM12E12_per_chn_intr_stat_DE2RDI_Intr_Mask)
        swStatus |= cAtPdhDe2AlarmRai;
    if (regVal & cAf6_RxM12E12_per_chn_intr_stat_DE2Lof_Intr_Mask)
        swStatus |= cAtPdhDe2AlarmLof;

    /* Clear */
    if (clear)
        mModuleHwWrite(pdh, regAddr, regVal);

    return swStatus;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return InterruptRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return InterruptRead2Clear(self, cAtTrue);
    }

static uint32 CounterAddress(uint16 counterType)
    {
    switch (counterType)
        {
        case cAtPdhDe2CounterParity:
            return cAf6Reg_rxm12e12_par_cnt;
        case cAtPdhDe2CounterFbe:
            return cAf6Reg_rxm12e12_fbe_cnt;
        default:
            return cInvalidUint32;
        }
    }

static uint32 HwCounterGet(AtChannel self, uint16 counterType, eBool r2c)
    {
    ThaModulePdh pdh = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    uint32 address = 0x700000 + CounterAddress(counterType) + ThaModulePdhM12E12CounterOffset(pdh, (ThaPdhDe2)self, r2c);
    return mModuleHwRead(pdh, address);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return HwCounterGet(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return HwCounterGet(self, counterType, cAtTrue);
    }

static eAtRet InitThreshold(Tha6A290021PdhDe2 self)
    {
    ThaModulePdh pdh = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    uint32 offset    = ThaModulePdhM12E12ControlOffset(pdh, (ThaPdhDe2)self) +  0x700000;
    uint32 regAddr   = cAf6Reg_rxm12e12_ctrl + offset;
    uint32 regVal    = mModuleHwRead(pdh, regAddr);
    mFieldIns(&regVal, cAf6_rxm12e12_ctrl_RxDE2AisDetEnb_Mask, cAf6_rxm12e12_ctrl_RxDE2AisDetEnb_Shift,  1);
    mFieldIns(&regVal, cAf6_rxm12e12_ctrl_RxDE2LosDetEnb_Mask, cAf6_rxm12e12_ctrl_RxDE2LosDetEnb_Mask,   0);
    mFieldIns(&regVal, cAf6_rxm12e12_ctrl_RxDE2AisThres_Mask,  cAf6_rxm12e12_ctrl_RxDE2AisThres_Shift,   1);
    mFieldIns(&regVal, cAf6_rxm12e12_ctrl_RxDE2LosThres_Mask,  cAf6_rxm12e12_ctrl_RxDE2LosThres_Shift,   0);
    mFieldIns(&regVal, cAf6_rxm12e12_ctrl_FBEFbitCntEn_Mask,   cAf6_rxm12e12_ctrl_FBEFbitCntEn_Shift,    1);
    mFieldIns(&regVal, cAf6_rxm12e12_ctrl_FBEMbitCntEn_Mask,   cAf6_rxm12e12_ctrl_FBEMbitCntEn_Shift,    1);
    mModuleHwWrite(pdh, regAddr, regVal);

    regAddr = cAf6Reg_txm12e12_nsec_val_thrsh_cfg + offset;
    mModuleHwWrite(pdh, regAddr, 0x12EBB);

    regAddr = cAf6Reg_upen_fbit_sta + offset;
    mModuleHwWrite(pdh, regAddr, 0x3);
    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);

    if (ret != cAtOk)
        return ret;

    return InitThreshold(mThis(self));
    }

static void OverrideAtChannel(AtChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);

        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        }

    mMethodsSet(self, &m_AtChannelOverride);
    }

static void Override(AtPdhDe2 self)
    {
    OverrideAtChannel((AtChannel)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021PdhDe2);
    }

AtPdhDe2 Tha6A290021PdhDe2ObjectInit(AtPdhDe2 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhDe2ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe2 Tha6A290021PdhDe2New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe2 newDe2 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe2 == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290021PdhDe2ObjectInit(newDe2, channelId, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : Tha6A210031PdhDe1AttController.c
 *
 * Created Date: May 16, 2016
 *
 * Description : DE1 ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaPdhDe2De1.h"
#include "../../Tha6A210031/pdh/Tha6A210031PdhAttControllerInternal.h"
#include "Tha6A290021PdhAttControllerInternal.h"
#include "Tha6A290021PdhDe1AttReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttControllerMethods                m_AtAttControllerOverride;
static tThaAttControllerMethods                m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;
static tTha6A210031PdhDe1AttControllerMethods m_Tha6A210031PdhDe1AttControllerOverride;

/* Save super implementation */
static const tAtAttControllerMethods                *m_AtAttControllerMethods = NULL;
static const tTha6A210031PdhDe3AttControllerMethods *m_Tha6A210031PdhDe3AttControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 StartVersionSupportStep16bit(ThaAttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0552);
    }

static eAtRet De1CrcGapSet(AtAttController self, uint16 frameType)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    AtChannel channel = AtAttControllerChannelGet(self);
    uint32   regAddr = mMethodsGet(controller)->RealAddress(controller, 0x97800);
    AtUnused(frameType);
    mChannelHwWrite(channel, regAddr, 0, cAtModulePdh);
    return cAtOk;
    }

static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0546);
    }

static uint32 StartVersionSupportMfas(Tha6A210031PdhDe1AttController self)
	{
	AtUnused(self);
	return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0549);
	}

static uint32 StartVersionSupportTwoCRCForce(Tha6A210031PdhDe1AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0xFFFF);
    }

static AtDevice DeviceGet(AtChannel self)
    {
    return AtChannelDeviceGet(self);
    }

static Tha6A290021ModulePdh PdhModule(AtChannel self)
    {
    return (Tha6A290021ModulePdh) AtDeviceModuleGet(DeviceGet(self), cAtModulePdh);
    }

static AtIpCore IpCore(AtChannel self)
    {
    return AtDeviceIpCoreGet(DeviceGet(self), 0);
    }

static uint32 HoldRegisterOffset(AtPdhChannel channel)
    {
    uint8 slice, hwSts;
    uint8 hwDe2Id, hwDe1Id;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)channel);
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet(channel);

    if (vc != NULL)
        ThaPdhChannelHwIdGet(channel, cAtModulePdh, &slice, &hwSts);
    else
        ThaPdhDe2De1HwIdGet((ThaPdhDe1)channel, &slice, &hwSts, &hwDe2Id, &hwDe1Id, cAtModulePdh);

    return ThaModulePdhSliceBase(pdhModule, slice);
    }

static uint16 LongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    uint32 holdRegOffset = HoldRegisterOffset((AtPdhChannel)channel);
    return Tha60290021ModulePdhAttLongReadOnCore(PdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel), holdRegOffset);
    }

static uint16 LongWrite(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    uint32 holdRegOffset = HoldRegisterOffset((AtPdhChannel)channel);
    return Tha60290021ModulePdhAttLongWriteOnCore(PdhModule(channel), mMethodsGet(att)->RealAddress(att, regAddr), dataBuffer, bufferLen, IpCore(channel), holdRegOffset);
    }
    
static eAtRet ErrorTypeSet(AtAttController self, uint32 errorType)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    AtChannel channel = AtAttControllerChannelGet(self);
    uint32 regAddr = mMethodsGet(controller)->RealAddress(controller, cReg_dej1_tx_framer_ctrl);
    uint32 regVal = mChannelHwRead(channel,regAddr, cAtModulePdh);
    mRegFieldSet(regVal, c_dej1_tx_framer_ctrl_ReiForceEn_, (errorType == cAtPdhDe1ErrorRei) ? 1 : 0);
    mChannelHwWrite(channel, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet ForceError(AtAttController self, uint32 errorType)
    {
    eAtRet ret;
    ret  = m_AtAttControllerMethods->ForceError(self, errorType);
    ret |= ErrorTypeSet(self, errorType);
    return ret;
    }

static eBool ErrorForceIsSupported(AtAttController self, uint32 errorType)
    {
    if (errorType == cAtPdhDe1ErrorRei)
        return cAtTrue;
    return m_AtAttControllerMethods->ErrorForceIsSupported(self, errorType);
    }

static uint32 RegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    if (errorType == cAtPdhDe1ErrorRei)
        return cReg_upen_crc_cfg;

    return m_Tha6A210031PdhDe3AttControllerMethods->RegAddressFromErrorType(self, errorType);
    }

static uint8 ErrorTypeIndex(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    if ((errorType == cAtPdhDe1ErrorCrc) || (errorType == cAtPdhDe1ErrorRei))
        return 0;

    return m_Tha6A210031PdhDe3AttControllerMethods->ErrorTypeIndex(self, errorType);
    }

static eBool IsStep16bit(ThaAttController self, uint32 errorType)
    {
    if ((errorType == cAtPdhDe1ErrorCrc) || (errorType == cAtPdhDe1ErrorRei))
        {
        ThaVersionReader version = ThaAttControllerVersionReader((ThaAttController) self);
        uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(version);
        if (hwVersion >= mMethodsGet(self)->StartVersionSupportStep16bit(self))
            return cAtTrue;
        }

    return cAtFalse;
    }

static void OverrideTha6A210031PdhDe1AttController(Tha6A210031PdhDe1AttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe1AttControllerOverride, mMethodsGet(self), sizeof(m_Tha6A210031PdhDe1AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe1AttControllerOverride, StartVersionSupportMfas);
        mMethodOverride(m_Tha6A210031PdhDe1AttControllerOverride, StartVersionSupportTwoCRCForce);
        }

    mMethodsSet(self, &m_Tha6A210031PdhDe1AttControllerOverride);
    }

static void OverrideTha6A210031PdhDe3AttController(Tha6A210031PdhDe3AttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6A210031PdhDe3AttControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(self), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, ErrorTypeIndex);
        }

    mMethodsSet(self, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void OverrideThaAttController(ThaAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(self), sizeof(m_ThaAttControllerOverride));
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportStep16bit);
        mMethodOverride(m_ThaAttControllerOverride, IsStep16bit);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtAttControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride, mMethodsGet(self), sizeof(m_AtAttControllerOverride));
        mMethodOverride(m_AtAttControllerOverride, LongRead);
        mMethodOverride(m_AtAttControllerOverride, LongWrite);
        mMethodOverride(m_AtAttControllerOverride, De1CrcGapSet);
        mMethodOverride(m_AtAttControllerOverride, ForceError);
        mMethodOverride(m_AtAttControllerOverride, ErrorForceIsSupported);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideAtAttController(self);
    OverrideThaAttController((ThaAttController) self);
    OverrideTha6A210031PdhDe3AttController((Tha6A210031PdhDe3AttController) self);
    OverrideTha6A210031PdhDe1AttController((Tha6A210031PdhDe1AttController) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021PdhDe1AttController);
    }

AtAttController Tha6A290021PdhDe1AttControllerObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031PdhDe1AttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A290021PdhDe1AttControllerNew(AtChannel de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A290021PdhDe1AttControllerObjectInit(controller, de1);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A290021PdhDe1.c
 *
 * Created Date: May 17, 2016
 *
 * Description : DE2 DE1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/pdh/Tha60290021PdhDe2De1Internal.h"
#include "Tha6A290021ModulePdhInternal.h"
#include "Tha6A290021PdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290021PdhDe2De1 *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021PdhDe2De1
    {
    tTha60290021PdhDe2De1 super;
    AtAttController attController;
    } tTha6A290021PdhDe2De1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tAtPdhDe1Methods     m_AtPdhDe1Override;

/* Save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TxAlarmsGet(AtChannel self)
	{
    AtAttController attController = AtChannelAttController(self);
    return AtAttControllerTxAlarmsGet(attController);
	}

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret =  AtAttControllerInit(AtChannelAttController(self));
    return AtAttControllerDe1CrcGapSet(AtChannelAttController(self), cAtPdhE1MFCrc);
    }


static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmTypes, eBool force)
    {
    uint32 i;
    eAtRet ret = cAtOk;
    for (i = 0; i < 32; i++)
        {
        uint32 alarmType = alarmTypes & (cBit0 << i);
        if (alarmType)
            {
            AtAttController attController = AtChannelAttController(self);
            AtAttControllerForceAlarmModeSet(attController, alarmType, cAtAttForceAlarmModeContinuous);
            if (force)
            	{
            	AtAttControllerTxAlarmsSet(attController, alarmType);
                return AtAttControllerForceAlarm(attController, alarmType);
            	}
            AtAttControllerTxAlarmsClear(attController, alarmType);
            return AtAttControllerUnForceAlarm(attController, alarmType);
            }
        }
    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcedAlarmGet(self);

    return TxAlarmsGet(self);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcibleAlarmsGet(self);

    return cAtPdhDe3AlarmLos | cAtPdhDe3AlarmAis | cAtPdhDe3AlarmRai | cAtPdhDe3AlarmLof;
    }

static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorTypes, eAtRet (*fForceAlarm)(AtAttController, uint32))
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 errorType = errorTypes & (cBit0 << i);
        if (errorType)
            {
            AtAttControllerForceErrorModeSet(attController, errorType, cAtAttForceErrorModeContinuous);
            ret = fForceAlarm(attController, errorType);
            }
        }
    return ret;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerForceError);
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorUnForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerUnForceError);
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcedErrorGet(self);

    return 0;
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    if (Tha6A290021AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcableErrorsGet(self);

    return cAtPdhDe1CounterRei|cAtPdhDe1CounterFe;
    }

static eBool HasFarEndPerformance(AtPdhDe1 self, uint16 frameType)
    {
    AtUnused(self);
    AtUnused(frameType);
    return cAtTrue;
    }

static void OverrideAtPdhDe1(AtPdhDe1 self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhDe1Override, mMethodsGet(self), sizeof(tAtPdhDe1Methods));

        mMethodOverride(m_AtPdhDe1Override, HasFarEndPerformance);
        }

    mMethodsSet(self, &m_AtPdhDe1Override);
    }

static void OverrideAtChannel(AtPdhDe1 self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPdhDe1 self)
    {
    OverrideAtChannel(self);
    OverrideAtPdhDe1(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021PdhDe2De1);
    }

static AtPdhDe1 ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60290021PdhDe2De1ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    return self;
    }

AtPdhDe1 Tha6A290021PdhDe2De1New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newDe1 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe1 == NULL)
        return NULL;

    /* construct it */
    return ObjectInit(newDe1, channelId, module);
    }

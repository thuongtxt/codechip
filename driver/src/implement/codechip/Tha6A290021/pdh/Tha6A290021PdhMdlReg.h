/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_RD_PDH_MDL_H_
#define _AF6_REG_AF6CNC0021_RD_PDH_MDL_H_

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_upen_int_ndlsta                                                                        0x4AA06
#define cAf6_mdl_en_int1_mdlen_int1_Mask                                                               cBit7_0
#define cAf6Reg_mdl_en_int1                                                                            0x4A3FE
#define cAf6Reg_mdl_en_int2                                                                            0x4A7FE
#define cAf6Reg_mdl_sta_int1                                                                           0x4A3FF
#define cAf6Reg_mdl_int1sta_Base                                                                       0x4A300
#define cAf6Reg_mdl_int1_sta_Base                                                                      0x4A100
#define cAf6Reg_mdl_cfgen_int1_Base                                                                    0x4A000

/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL BUFFER1
Reg Addr   : 0x0000 - 0x012F
Reg Formula: 0x0000+ $SLICEID*0x800 + $DWORDID*16 + $DE3ID1
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID1(0-15)  : DE3 ID1
Reg Desc   : 
config message MDL BUFFER Channel ID 0-15

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_buffer1                                                                        0x0000

/*--------------------------------------
BitField Name: idle_byte13
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_idle_byte13_Mask                                                       cBit31_24
#define cAf6_upen_mdl_buffer1_idle_byte13_Shift                                                             24

/*--------------------------------------
BitField Name: ilde_byte12
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_ilde_byte12_Mask                                                       cBit23_16
#define cAf6_upen_mdl_buffer1_ilde_byte12_Shift                                                             16

/*--------------------------------------
BitField Name: idle_byte11
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_idle_byte11_Mask                                                        cBit15_8
#define cAf6_upen_mdl_buffer1_idle_byte11_Shift                                                              8

/*--------------------------------------
BitField Name: idle_byte10
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_idle_byte10_Mask                                                         cBit7_0
#define cAf6_upen_mdl_buffer1_idle_byte10_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL BUFFER1_2
Reg Addr   : 0x0130 - 0x01C7
Reg Formula: 0x0130+ $SLICEID*0x800 + $DWORDID*8 + $DE3ID2
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID2 (0-7) : DE3 ID2
Reg Desc   : 
config message MDL BUFFER Channel ID 16-23

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_buffer1_2                                                                      0x0130

/*--------------------------------------
BitField Name: idle_byte23
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_2_idle_byte23_Mask                                                     cBit31_24
#define cAf6_upen_mdl_buffer1_2_idle_byte23_Shift                                                           24

/*--------------------------------------
BitField Name: idle_byte22
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_2_idle_byte22_Mask                                                     cBit23_16
#define cAf6_upen_mdl_buffer1_2_idle_byte22_Shift                                                           16

/*--------------------------------------
BitField Name: idle_byte21
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_2_idle_byte21_Mask                                                      cBit15_8
#define cAf6_upen_mdl_buffer1_2_idle_byte21_Shift                                                            8

/*--------------------------------------
BitField Name: idle_byte20
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_buffer1_2_idle_byte20_Mask                                                       cBit7_0
#define cAf6_upen_mdl_buffer1_2_idle_byte20_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message MDL BUFFER2
Reg Addr   : 0x0200 - 0x032F
Reg Formula: 0x0200+$SLICEID*0x800 + $DE3ID1 + $DWORDID*16
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID1(0-15)  : DS3 E3 ID
Reg Desc   : 
config message MDL BUFFER2 Channel ID 0-15

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_tp1                                                                            0x0200

/*--------------------------------------
BitField Name: tp_byte13
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_tp_byte13_Mask                                                             cBit31_24
#define cAf6_upen_mdl_tp1_tp_byte13_Shift                                                                   24

/*--------------------------------------
BitField Name: tp_byte12
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_tp_byte12_Mask                                                             cBit23_16
#define cAf6_upen_mdl_tp1_tp_byte12_Shift                                                                   16

/*--------------------------------------
BitField Name: tp_byte11
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_tp_byte11_Mask                                                              cBit15_8
#define cAf6_upen_mdl_tp1_tp_byte11_Shift                                                                    8

/*--------------------------------------
BitField Name: tp_byte10
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_tp1_tp_byte10_Mask                                                               cBit7_0
#define cAf6_upen_mdl_tp1_tp_byte10_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message MDL BUFFER2_2
Reg Addr   : 0x0330 - 0x03C7
Reg Formula: 0x0330+$SLICEID*0x800 + $DWORDID*8+ $DE3ID2
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DWORDID (0-18) : DWORD ID
           + $DE3ID2 (0-7) : DE3 ID2
Reg Desc   : 
config message MDL BUFFER2 Channel ID 16-23

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_tp2                                                                            0x0330

/*--------------------------------------
BitField Name: tp_byte23
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_tp_byte23_Mask                                                             cBit31_24
#define cAf6_upen_mdl_tp2_tp_byte23_Shift                                                                   24

/*--------------------------------------
BitField Name: tp_byte22
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_tp_byte22_Mask                                                             cBit23_16
#define cAf6_upen_mdl_tp2_tp_byte22_Shift                                                                   16

/*--------------------------------------
BitField Name: tp_byte21
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_tp_byte21_Mask                                                              cBit15_8
#define cAf6_upen_mdl_tp2_tp_byte21_Shift                                                                    8

/*--------------------------------------
BitField Name: tp_byte20
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_mdl_tp2_tp_byte20_Mask                                                               cBit7_0
#define cAf6_upen_mdl_tp2_tp_byte20_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : SET TO HAVE PACKET MDL BUFFER 1
Reg Addr   : 0x0420 - 0x437
Reg Formula: 0x0420+$SLICEID*0x800 +$DE3ID
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sta_idle_alren                                                                     0x0420
#define cAf6Reg_upen_sta_idle_alren_WidthVal                                                                32

/*--------------------------------------
BitField Name: idle_cfgen
BitField Type: R/W
BitField Desc: (0) : engine clear for indication to have sent, (1) CPU set for
indication to have new message which must send for buffer 0
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_sta_idle_alren_idle_cfgen_Mask                                                         cBit0
#define cAf6_upen_sta_idle_alren_idle_cfgen_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : SET TO HAVE PACKET MDL BUFFER 2
Reg Addr   : 0x0440 - 0x457
Reg Formula: 0x0440+$SLICEID*0x800 +$DE3ID
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 2

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sta_tp_alren                                                                       0x0440
#define cAf6Reg_upen_sta_tp_alren_WidthVal                                                                  32

/*--------------------------------------
BitField Name: tp_cfgen
BitField Type: R/W
BitField Desc: (0) : engine clear for indication to have sent, (1) CPU set for
indication to have new message which must send for buffer 1
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_sta_tp_alren_tp_cfgen_Mask                                                             cBit0
#define cAf6_upen_sta_tp_alren_tp_cfgen_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG MDL Tx
Reg Addr   : 0x0460 - 0x477
Reg Formula: 0x0460+$SLICEID*0x800 +$DE3ID
    Where  : 
           + $SLICEID(0-7) : SLICE ID
           + $DE3ID (0-23) : DE3 ID
Reg Desc   : 
Config Tx MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_mdl                                                                            0x0460
#define cAf6Reg_upen_cfg_mdl_WidthVal                                                                       32

/*--------------------------------------
BitField Name: cfg_seq_tx
BitField Type: R/W
BitField Desc: config enable Tx continous, (0) is disable, (1) is enable
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_seq_tx_Mask                                                                cBit4
#define cAf6_upen_cfg_mdl_cfg_seq_tx_Shift                                                                   4

/*--------------------------------------
BitField Name: cfg_entx
BitField Type: R/W
BitField Desc: config enable Tx, (0) is disable, (1) is enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_entx_Mask                                                                  cBit3
#define cAf6_upen_cfg_mdl_cfg_entx_Shift                                                                     3

/*--------------------------------------
BitField Name: cfg_fcs_tx
BitField Type: R/W
BitField Desc: config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_fcs_tx_Mask                                                                cBit2
#define cAf6_upen_cfg_mdl_cfg_fcs_tx_Shift                                                                   2

/*--------------------------------------
BitField Name: cfg_mdlstd_tx
BitField Type: R/W
BitField Desc: config standard Tx, (0) is ANSI, (1) is AT&T
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_mdlstd_tx_Mask                                                             cBit1
#define cAf6_upen_cfg_mdl_cfg_mdlstd_tx_Shift                                                                1

/*--------------------------------------
BitField Name: cfg_mdl_cr
BitField Type: R/W
BitField Desc: config bit command/respond MDL message, bit 0 is channel 0 of DS3
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_mdl_cfg_mdl_cr_Mask                                                                cBit0
#define cAf6_upen_cfg_mdl_cfg_mdl_cr_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL IDLE
Reg Addr   : 0x0700 - 0x0717
Reg Formula: 0x0700+ $SLICEID*0x800 + $DE3ID + $UPRO * 128
    Where  : 
           + $DE3ID (0-23) : DE3 ID
           + $UPRO (0-1) : UP READ ONLY
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
counter byte read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_byteidle1                                                             0x0700

/*--------------------------------------
BitField Name: cntr2c_byte_idle_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_byteidle1_cntr2c_byte_idle_mdl_Mask                                    cBit31_0
#define cAf6_upen_txmdl_cntr2c_byteidle1_cntr2c_byte_idle_mdl_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL PATH
Reg Addr   : 0x0720 - 0x0737
Reg Formula: 0x0720+ $SLICEID*0x800 + $DE3ID + $UPRO * 128
    Where  : 
           + $DE3ID (0-23) : DE3 ID
           + $UPRO (0-1) : UP READ ONLY
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
counter byte read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_bytepath                                                              0x0720

/*--------------------------------------
BitField Name: cntr2c_byte_path_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Mask                                     cBit31_0
#define cAf6_upen_txmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE TX MDL TEST
Reg Addr   : 0x0740 - 0x0757
Reg Formula: 0x0740+ $SLICEID*0x800 + $DE3ID + $UPRO * 128
    Where  : 
           + $DE3ID (0-23) : DE3 ID
           + $UPRO (0-1) : UP READ ONLY
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
counter byte read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_bytetest                                                              0x0740

/*--------------------------------------
BitField Name: cntr2c_byte_test_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Mask                                     cBit31_0
#define cAf6_upen_txmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL IDLE
Reg Addr   : 0x0600 - 0x0697
Reg Formula: 0x0600+ $SLICEID*0x800 + $DE3ID + $UPRO * 128
    Where  : 
           + $DE3ID (0-23) : DE3 ID
           + $UPRO (0-1) : UP READ ONLY
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
counter valid read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_valididle                                                             0x0600
#define cAf6Reg_upen_txmdl_cntr2c_valididle_WidthVal                                                        32

/*--------------------------------------
BitField Name: cntr2c_valid_idle_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_valididle_cntr2c_valid_idle_mdl_Mask                                   cBit17_0
#define cAf6_upen_txmdl_cntr2c_valididle_cntr2c_valid_idle_mdl_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL PATH
Reg Addr   : 0x0620 - 0x06B7
Reg Formula: 0x0620+ $SLICEID*0x800 + $DE3ID + $UPRO * 128
    Where  : 
           + $DE3ID (0-23) : DE3 ID
           + $UPRO (0-1) : UP READ ONLY
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
counter valid read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_validpath                                                             0x0620

/*--------------------------------------
BitField Name: cntr2c_valid_path_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_validpath_cntr2c_valid_path_mdl_Mask                                   cBit17_0
#define cAf6_upen_txmdl_cntr2c_validpath_cntr2c_valid_path_mdl_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER VALID MESSAGE TX MDL TEST R2C
Reg Addr   : 0x0640 - 0x06D7
Reg Formula: 0x0640+ $SLICEID*0x800 + $DE3ID + $UPRO * 128
    Where  : 
           + $DE3ID (0-23) : DE3 ID
           + $UPRO (0-1) : UP READ ONLY
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
counter valid read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txmdl_cntr2c_validtest                                                             0x0640

/*--------------------------------------
BitField Name: cntr2c_valid_test_mdl
BitField Type: R/W
BitField Desc: value counter valid
BitField Bits: [17:00]
--------------------------------------*/
#define cAf6_upen_txmdl_cntr2c_validtest_cntr2c_valid_test_mdl_Mask                                   cBit17_0
#define cAf6_upen_txmdl_cntr2c_validtest_cntr2c_valid_test_mdl_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : Buff Message MDL with configuration type
Reg Addr   : 0x24000 - 0x25FFF
Reg Formula: 0x24000 + $STSID* 256 + $SLICEID*32 + $RXDWORDID
    Where  : 
           + $STSID (0-23) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $RXDWORDID (0-19) : RX Double WORD ID
Reg Desc   : 
buffer message MDL which is configured type

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_typebuff                                                                    0x24000

/*--------------------------------------
BitField Name: mdl_tbyte3
BitField Type: R/W
BitField Desc: MS BYTE
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte3_Mask                                                     cBit31_24
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte3_Shift                                                           24

/*--------------------------------------
BitField Name: mdl_tbyte2
BitField Type: R/W
BitField Desc: BYTE 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte2_Mask                                                     cBit23_16
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte2_Shift                                                           16

/*--------------------------------------
BitField Name: mdl_tbyte1
BitField Type: R/W
BitField Desc: BYTE 1
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte1_Mask                                                      cBit15_8
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte1_Shift                                                            8

/*--------------------------------------
BitField Name: mdl_tbyte0
BitField Type: R/W
BitField Desc: LS BYTE
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte0_Mask                                                       cBit7_0
#define cAf6_upen_rxmdl_typebuff_mdl_tbyte0_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Config Buff Message MDL TYPE
Reg Addr   : 0x23000 - 0x230BF
Reg Formula: 0x23000+ $STSID*8 + $SLICEID
    Where  : 
           + $STSID (0-23) : STS ID
           + $SLICEID (0-7) : SLICE ID
Reg Desc   : 
config type message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cfgtype                                                                     0x23000
#define cAf6Reg_upen_rxmdl_cfgtype_WidthVal                                                                 32

/*--------------------------------------
BitField Name: cfg_fcs_rx
BitField Type: R/W
BitField Desc: config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_fcs_rx_Mask                                                          cBit5
#define cAf6_upen_rxmdl_cfgtype_cfg_fcs_rx_Shift                                                             5

/*--------------------------------------
BitField Name: cfg_mdl_cr
BitField Type: R/W
BitField Desc: config C/R bit expected
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_cr_Mask                                                          cBit4
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_cr_Shift                                                             4

/*--------------------------------------
BitField Name: cfg_mdl_std
BitField Type: R/W
BitField Desc: (0) is ANSI, (1) is AT&T
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_Mask                                                         cBit3
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_std_Shift                                                            3

/*--------------------------------------
BitField Name: cfg_mdl_mask_test
BitField Type: R/W
BitField Desc: config enable mask to moitor test massage (1): enable, (0):
disable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_test_Mask                                                   cBit2
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_test_Shift                                                      2

/*--------------------------------------
BitField Name: cfg_mdl_mask_path
BitField Type: R/W
BitField Desc: config enable mask to moitor path massage (1): enable, (0):
disable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_path_Mask                                                   cBit1
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_path_Shift                                                      1

/*--------------------------------------
BitField Name: cfg_mdl_mask_idle
BitField Type: R/W
BitField Desc: config enable mask to moitor idle massage (1): enable, (0):
disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_idle_Mask                                                   cBit0
#define cAf6_upen_rxmdl_cfgtype_cfg_mdl_mask_idle_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL DESTUFF 0
Reg Addr   : 0x23200
Reg Formula: 
    Where  : 
Reg Desc   : 
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_destuff_ctrl0                                                                     0x23200
#define cAf6Reg_upen_destuff_ctrl0_WidthVal                                                                 32

/*--------------------------------------
BitField Name: cfg_crmon
BitField Type: R/W
BitField Desc: (0) : disable, (1) : enable
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Mask                                                           cBit5
#define cAf6_upen_destuff_ctrl0_cfg_crmon_Shift                                                              5

/*--------------------------------------
BitField Name: reserve1
BitField Type: R/W
BitField Desc: reserve
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_reserve1_Mask                                                            cBit4
#define cAf6_upen_destuff_ctrl0_reserve1_Shift                                                               4

/*--------------------------------------
BitField Name: fcsmon
BitField Type: R/W
BitField Desc: (0) : disable monitor, (1) : enable
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_fcsmon_Mask                                                              cBit3
#define cAf6_upen_destuff_ctrl0_fcsmon_Shift                                                                 3

/*--------------------------------------
BitField Name: headermon_en
BitField Type: R/W
BitField Desc: (1) : enable monitor, (0) disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_destuff_ctrl0_headermon_en_Mask                                                        cBit0
#define cAf6_upen_destuff_ctrl0_headermon_en_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF CONFIG1
Reg Addr   : 0x23210
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg1                                                                      0x23210

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 0-31
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg1_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg1_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 2
Reg Addr   : 0x23211
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg2                                                                      0x23211

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 32 -63
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg2_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg2_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 3
Reg Addr   : 0x23212
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg3                                                                      0x23212

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 64 -95
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg3_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg3_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 4
Reg Addr   : 0x23213
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg4                                                                      0x23213

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 96 -127
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg4_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg4_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 5
Reg Addr   : 0x23214
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg5                                                                      0x23214

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 128 -159
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg5_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg5_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : STICKY TO RECEIVE PACKET MDL BUFF CONFIG 6
Reg Addr   : 0x23215
Reg Formula: 
    Where  : 
Reg Desc   : 
engine set to alarm message event, and CPU clear to be received new messase

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mdl_stk_cfg6                                                                      0x23215

/*--------------------------------------
BitField Name: mdl_cfg_alr
BitField Type: R/W
BitField Desc: sticky for new message channel 160 -191
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_mdl_stk_cfg6_mdl_cfg_alr_Mask                                                       cBit31_0
#define cAf6_upen_mdl_stk_cfg6_mdl_cfg_alr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX MDL IDLE
Reg Addr   : 0x26000 - 0x264BF
Reg Formula: 0x26000+ $STSID*8 + $SLICEID + $UPRO * 1024
    Where  : 
           + $STSID (0-23) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter byte read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_byteidle                                                             0x26000

/*--------------------------------------
BitField Name: cntr2c_byte_idle_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_byteidle_cntr2c_byte_idle_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_byteidle_cntr2c_byte_idle_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX MDL PATH
Reg Addr   : 0x26100 - 0x265BF
Reg Formula: 0x26100+ $STSID*8 + $SLICEID + $UPRO * 1024
    Where  : 
           + $STSID (0-23) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter byte read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_bytepath                                                             0x26100

/*--------------------------------------
BitField Name: cntr2c_byte_path_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_bytepath_cntr2c_byte_path_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER BYTE MESSAGE RX MDL TEST
Reg Addr   : 0x26200 - 0x266BF
Reg Formula: 0x26200+ $STSID*8 + $SLICEID + $UPRO * 1024
    Where  : 
           + $STSID (0-23) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter byte read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_bytetest                                                             0x26200

/*--------------------------------------
BitField Name: cntr2c_byte_test_mdl
BitField Type: R/W
BitField Desc: value counter byte
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_bytetest_cntr2c_byte_test_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX MDL IDLE
Reg Addr   : 0x27000 - 0x278BF
Reg Formula: 0x27000+ $STSID*8 + $SLICEID + $UPRO * 2048
    Where  : 
           + $STSID (0-23) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter good read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_goodidle                                                             0x27000

/*--------------------------------------
BitField Name: cntr2c_good_idle_mdl
BitField Type: R/W
BitField Desc: value counter good
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_goodidle_cntr2c_good_idle_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_goodidle_cntr2c_good_idle_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX MDL PATH
Reg Addr   : 0x27100 - 0x279BF
Reg Formula: 0x27100+ $STSID*8 + $SLICEID + $UPRO * 2048
    Where  : 
           + $STSID (0-23) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter good read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_goodpath                                                             0x27100

/*--------------------------------------
BitField Name: cntr2c_good_path_mdl
BitField Type: R/W
BitField Desc: value counter good
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_goodpath_cntr2c_good_path_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_goodpath_cntr2c_good_path_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER GOOD MESSAGE RX MDL TEST
Reg Addr   : 0x27200 - 0x27ABF
Reg Formula: 0x27200+ $STSID*8 + $SLICEID + $UPRO * 2048
    Where  : 
           + $STSID (0-23) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter good read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_goodtest                                                             0x27200

/*--------------------------------------
BitField Name: cntr2c_good_test_mdl
BitField Type: R/W
BitField Desc: value counter good
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_goodtest_cntr2c_good_test_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_goodtest_cntr2c_good_test_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX MDL IDLE
Reg Addr   : 0x27300 - 0x27BBF
Reg Formula: 0x27300+ $STSID*8 + $SLICEID + $UPRO * 2048
    Where  : 
           + $STSID (0-23) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter drop read to clear IDLE message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_dropidle                                                             0x27300

/*--------------------------------------
BitField Name: cntr2c_drop_idle_mdl
BitField Type: R/W
BitField Desc: value counter drop
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_dropidle_cntr2c_drop_idle_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_dropidle_cntr2c_drop_idle_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX MDL PATH
Reg Addr   : 0x27400 - 0x27CBF
Reg Formula: 0x27400+ $STSID*8 + $SLICEID + $UPRO * 2048
    Where  : 
           + $STSID (0-23) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter drop read to clear PATH message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_droppath                                                             0x27400

/*--------------------------------------
BitField Name: cntr2c_drop_path_mdl
BitField Type: R/W
BitField Desc: value counter drop
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_droppath_cntr2c_drop_path_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_droppath_cntr2c_drop_path_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : COUNTER DROP MESSAGE RX MDL TEST R2C
Reg Addr   : 0x27500 - 0x27DBF
Reg Formula: 0x27500+ $STSID*8 + $SLICEID + $UPRO * 2048
    Where  : 
           + $STSID (0-23) : STS ID
           + $SLICEID (0-7) : SLICE ID
           + $UPRO (0-1) : UP READ ONLY
Reg Desc   : 
counter drop read to clear TEST message MDL

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxmdl_cntr2c_droptest                                                             0x27500

/*--------------------------------------
BitField Name: cntr2c_drop_test_mdl
BitField Type: R/W
BitField Desc: value counter drop
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxmdl_cntr2c_droptest_cntr2c_drop_test_mdl_Mask                                     cBit31_0
#define cAf6_upen_rxmdl_cntr2c_droptest_cntr2c_drop_test_mdl_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : MDL per Channel Interrupt Enable Control
Reg Addr   : 0x29000-0x290FF
Reg Formula: 0x29000 +  $STSID + $SLICEID * 32
    Where  : 
           + $STSID (0-23) : DE3 ID
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
This is the per Channel interrupt enable

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_cfgen_int                                                                          0x29000

/*--------------------------------------
BitField Name: mdl_cfgen_int
BitField Type: RW
BitField Desc: Set 1 to enable change event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_mdl_cfgen_int_mdl_cfgen_int_Mask                                                            cBit0
#define cAf6_mdl_cfgen_int_mdl_cfgen_int_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt per Channel Interrupt Status
Reg Addr   : 0x29100-0x291FF
Reg Formula: 0x29100 +  $STSID + $SLICEID * 32
    Where  : 
           + $STSID (0-23) : DE3 ID
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
This is the per Channel interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_int_sta                                                                            0x29100

/*--------------------------------------
BitField Name: mdlint_sta
BitField Type: RW
BitField Desc: Set 1 if there is a change event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_mdl_int_sta_mdlint_sta_Mask                                                                 cBit0
#define cAf6_mdl_int_sta_mdlint_sta_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt per Channel Current Status
Reg Addr   : 0x29200-0x292FF
Reg Formula: 0x29200 +  $STSID + $SLICEID * 32
    Where  : 
           + $STSID (0-23) : DE3 ID
           + $SLICEID(0-7) : SLICE ID
Reg Desc   : 
This is the per Channel Current status.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_int_crrsta                                                                         0x29200

/*--------------------------------------
BitField Name: mdlint_crrsta
BitField Type: RW
BitField Desc: Current status of event.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_mdl_int_crrsta_mdlint_crrsta_Mask                                                           cBit0
#define cAf6_mdl_int_crrsta_mdlint_crrsta_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt per Channel Interrupt OR Status
Reg Addr   : 0x29300-0x29307
Reg Formula: 0x29300 +  $GID
    Where  : 
           + $GID (0-7) : group ID
Reg Desc   : 
The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_intsta                                                                             0x29300

/*--------------------------------------
BitField Name: mdlintsta
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit of corresponding channel is
set and its interrupt is enabled.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mdl_intsta_mdlintsta_Mask                                                                cBit31_0
#define cAf6_mdl_intsta_mdlintsta_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt OR Status
Reg Addr   : 0x293FF
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 bits. Each bit is used to store Interrupt OR status.

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_sta_int                                                                            0x293FF
#define cAf6Reg_mdl_sta_int_WidthVal                                                                        32

/*--------------------------------------
BitField Name: mdlsta_int
BitField Type: RW
BitField Desc: Set to 1 if any interrupt status bit is set and its interrupt is
enabled
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_mdl_sta_int_mdlsta_int_Mask                                                               cBit7_0
#define cAf6_mdl_sta_int_mdlsta_int_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : MDL Interrupt Enable Control
Reg Addr   : 0x293FE
Reg Formula: 
    Where  : 
Reg Desc   : 
The register consists of 2 interrupt enable bits .

------------------------------------------------------------------------------*/
#define cAf6Reg_mdl_en_int                                                                             0x293FE
#define cAf6Reg_mdl_en_int_WidthVal                                                                         32

/*--------------------------------------
BitField Name: mdlen_int
BitField Type: RW
BitField Desc: Set to 1 to enable to generate interrupt.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_mdl_en_int_mdlen_int_Mask                                                                 cBit7_0
#define cAf6_mdl_en_int_mdlen_int_Shift                                                                      0

#endif /* _AF6_REG_AF6CNC0021_RD_PDH_MDL_H_ */

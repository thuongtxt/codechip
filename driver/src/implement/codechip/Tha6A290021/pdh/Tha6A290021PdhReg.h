/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date: May 11, 2018
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0011_RD_PDH_H_
#define _AF6_REG_AF6CCI0011_RD_PDH_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 HW Status
Reg Addr   : 0x00041100 - 0x000411FF
Reg Formula: 0x00041100 +  8*de3id + de2id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   : 
for HW debug only

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm12e12_hw_stat                                                                    0x00041100

/*--------------------------------------
BitField Name: RxM12E12Status
BitField Type: RW
BitField Desc: for HW debug only
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_rxm12e12_hw_stat_RxM12E12Status_Mask                                                      cBit1_0
#define cAf6_rxm12e12_hw_stat_RxM12E12Status_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Control
Reg Addr   : 0x00041000 - 0x000410FF
Reg Formula: 0x00041000 +  8*de3id + de2id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   : 
The RxM12E12 Control is use to configure for per channel Rx DS2/E2 operation.

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm12e12_ctrl                                                                       0x00041000
#define cAf6Reg_rxm12e12_ctrl_WidthVal                                                                      32

/*--------------------------------------
BitField Name: RxDE2AisDetEnb
BitField Type: RW
BitField Desc: This bit is set to 1 to enable AIS detection to RxDE2.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_rxm12e12_ctrl_RxDE2AisDetEnb_Mask                                                          cBit13
#define cAf6_rxm12e12_ctrl_RxDE2AisDetEnb_Shift                                                             13

/*--------------------------------------
BitField Name: RxDE2LosDetEnb
BitField Type: RW
BitField Desc: This bit is set to 1 to enable LOS detection to RxDE2.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_rxm12e12_ctrl_RxDE2LosDetEnb_Mask                                                          cBit12
#define cAf6_rxm12e12_ctrl_RxDE2LosDetEnb_Shift                                                             12

/*--------------------------------------
BitField Name: RxDE2AisThres
BitField Type: RW
BitField Desc: Threshold to AIS detect all one.
BitField Bits: [11:9]
--------------------------------------*/
#define cAf6_rxm12e12_ctrl_RxDE2AisThres_Mask                                                         cBit11_9
#define cAf6_rxm12e12_ctrl_RxDE2AisThres_Shift                                                               9

/*--------------------------------------
BitField Name: RxDE2LosThres
BitField Type: RW
BitField Desc: Threshold to LOS detect all zero.
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_rxm12e12_ctrl_RxDE2LosThres_Mask                                                          cBit8_6
#define cAf6_rxm12e12_ctrl_RxDE2LosThres_Shift                                                               6

/*--------------------------------------
BitField Name: FBEFbitCntEn
BitField Type: RW
BitField Desc: Enable FBE counts F-Bit
BitField Bits: [5]
--------------------------------------*/
#define cAf6_rxm12e12_ctrl_FBEFbitCntEn_Mask                                                             cBit5
#define cAf6_rxm12e12_ctrl_FBEFbitCntEn_Shift                                                                5

/*--------------------------------------
BitField Name: FBEMbitCntEn
BitField Type: RW
BitField Desc: Enable FBE counts M-Bit
BitField Bits: [4]
--------------------------------------*/
#define cAf6_rxm12e12_ctrl_FBEMbitCntEn_Mask                                                             cBit4
#define cAf6_rxm12e12_ctrl_FBEMbitCntEn_Shift                                                                4

/*--------------------------------------
BitField Name: RxDS2E2AISMask
BitField Type: RW
BitField Desc: This bit is set to 1 to disable AIS downstream masking to RxDE1
into LOF status. 1: UnMask. 0: Mask.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_rxm12e12_ctrl_RxDS2E2AISMask_Mask                                                           cBit3
#define cAf6_rxm12e12_ctrl_RxDS2E2AISMask_Shift                                                              3

/*--------------------------------------
BitField Name: RxDS2E2FrcLof
BitField Type: RW
BitField Desc: This bit is set to 1 to force the Rx DS2/E2 framer into LOF tus.
1: force LOF. 0: not force LOF.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_rxm12e12_ctrl_RxDS2E2FrcLof_Mask                                                            cBit2
#define cAf6_rxm12e12_ctrl_RxDS2E2FrcLof_Shift                                                               2

/*--------------------------------------
BitField Name: RxDS2E2Md
BitField Type: RW
BitField Desc: Rx DS2/E2 framing mode 00: DS2 T1.107 carrying 4 DS1s 01: DS2
G.747 carrying 3 E1s 10: E2 G.742 carrying 4 E1s 11: Reserved
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_rxm12e12_ctrl_RxDS2E2Md_Mask                                                              cBit1_0
#define cAf6_rxm12e12_ctrl_RxDS2E2Md_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer Parity Counter
Reg Addr   : 0x0041E00 - 0x00041EFF
Reg Formula: 0x0041E00 +  256*RdOnly + 8*de3id + de2id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   : 
This is the per channel Parity counter for DS2/E2 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm12e12_par_cnt                                                                     0x00041E00
#define cAf6Reg_rxm12e12_par_cnt_WidthVal                                                                   32

/*--------------------------------------
BitField Name: count
BitField Type: RW
BitField Desc: frame Parity counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm12e12_par_cnt_count_Mask                                                              cBit15_0
#define cAf6_rxm12e12_par_cnt_count_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer FBE Counter
Reg Addr   : 0x00041400 - 0x000414FF
Reg Formula: 0x00041400 +  256*RdOnly + 8*de3id + de2id
    Where  : 
           + $Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   : 
This is the per channel FBE counter for DS2/E2 receive framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm12e12_fbe_cnt                                                                    0x00041400
#define cAf6Reg_rxm12e12_fbe_cnt_WidthVal                                                                   32

/*--------------------------------------
BitField Name: count
BitField Type: RW
BitField Desc: frame FBE counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm12e12_fbe_cnt_count_Mask                                                              cBit15_0
#define cAf6_rxm12e12_fbe_cnt_count_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer FBE Threshold Control
Reg Addr   : 0x00041342
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the REI threshold configuration register for the PDH DS2/E2 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm12e12_fbe_thrsh_cfg                                                              0x00041342
#define cAf6Reg_rxm12e12_fbe_thrsh_cfg_WidthVal                                                             32

/*--------------------------------------
BitField Name: RxM12E12FbeThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the FBE counter exceed this
threshold
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm12e12_fbe_thrsh_cfg_RxM12E12FbeThres_Mask                                             cBit15_0
#define cAf6_rxm12e12_fbe_thrsh_cfg_RxM12E12FbeThres_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer Parity Threshold Control
Reg Addr   : 0x00041341
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the PAR threshold configuration register for the PDH DS2/E2 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm12e12_parity_thrsh_cfg                                                           0x00041341
#define cAf6Reg_rxm12e12_parity_thrsh_cfg_WidthVal                                                          32

/*--------------------------------------
BitField Name: RxM12E12ParThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the PAR counter exceed this
threshold
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_rxm12e12_parity_thrsh_cfg_RxM12E12ParThres_Mask                                          cBit15_0
#define cAf6_rxm12e12_parity_thrsh_cfg_RxM12E12ParThres_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer RAI Threshold Control
Reg Addr   : 0x00041343
Reg Formula:
    Where  :
Reg Desc   :
This is the RAI threshold configuration register for the PDH DS2/E2 Rx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_rxm12e12_rai_thrsh_cfg                                                              0x00041343
#define cAf6Reg_rxm12e12_rai_thrsh_cfg_WidthVal                                                             32

/*--------------------------------------
BitField Name: RxM12E12ParThres
BitField Type: RW
BitField Desc: Threshold to generate interrupt if the RAI counter exceed this
threshold
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_rxm12e12_rai_thrsh_cfg_RxM12E12ParThres_Mask                                              cBit2_0
#define cAf6_rxm12e12_rai_thrsh_cfg_RxM12E12ParThres_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer Channel Interrupt Enable Control
Reg Addr   : 0x00041BFE
Reg Formula:
    Where  :
Reg Desc   :
This is the Channel interrupt enable of DS2/E2 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_chn_intr_cfg                                                               0x00041BFE
#define cAf6Reg_RxM12E12_chn_intr_cfg_WidthVal                                                              32

/*--------------------------------------
BitField Name: RxM12E12IntrEn
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS2/E2 in DS2/E2 0: Disable
1: Enable
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_RxM12E12_chn_intr_cfg_RxM12E12IntrEn_Mask                                                cBit23_0
#define cAf6_RxM12E12_chn_intr_cfg_RxM12E12IntrEn_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer Channel Interrupt Status
Reg Addr   : 0x00041BFF
Reg Formula:
    Where  :
Reg Desc   :
This is the Channel interrupt tus of DS2/E2 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_chn_intr_stat                                                              0x00041BFF
#define cAf6Reg_RxM12E12_chn_intr_stat_WidthVal                                                             32

/*--------------------------------------
BitField Name: RxM12E12IntrSta
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS2/E2 in DS2/E2 0: No
Interrupt 1: Have Interrupt
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_RxM12E12_chn_intr_stat_RxM12E12IntrSta_Mask                                              cBit23_0
#define cAf6_RxM12E12_chn_intr_stat_RxM12E12IntrSta_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer per Channel Interrupt Enable Control
Reg Addr   : 0x00041800-0x0004139F
Reg Formula: 0x00041800 +  8*de3id + de2id
    Where  :
           + $de3id(0-26):
Reg Desc   :
This is the per Channel interrupt enable of DS2/E2 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_per_chn_intr_cfg                                                           0x00041800
#define cAf6Reg_RxM12E12_per_chn_intr_cfg_WidthVal                                                          32

/*--------------------------------------
BitField Name: DE2ParCnt_En
BitField Type: RW
BitField Desc: Set 1 to enable change DE2 Parity counter event to generate an
interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2ParCnt_En_Mask                                                 cBit5
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2ParCnt_En_Shift                                                    5

/*--------------------------------------
BitField Name: DE2FBECnt_En
BitField Type: RW
BitField Desc: Set 1 to enable change DE2 FBE counter event to generate an
interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2FBECnt_En_Mask                                                 cBit4
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2FBECnt_En_Shift                                                    4

/*--------------------------------------
BitField Name: DE2LosAllZeroIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOS All Zero event to generate an
interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2LosAllZeroIntrEn_Mask                                          cBit3
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2LosAllZeroIntrEn_Shift                                             3

/*--------------------------------------
BitField Name: DE2AisAllOneIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change AIS All One event to generate an
interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2AisAllOneIntrEn_Mask                                           cBit2
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2AisAllOneIntrEn_Shift                                              2

/*--------------------------------------
BitField Name: DE2RDIIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change RDI event to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2RDIIntrEn_Mask                                                 cBit1
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2RDIIntrEn_Shift                                                    1

/*--------------------------------------
BitField Name: DE2LofIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOF event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2LofIntrEn_Mask                                                 cBit0
#define cAf6_RxM12E12_per_chn_intr_cfg_DE2LofIntrEn_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer per Channel Interrupt Change Status
Reg Addr   : 0x00041900-0x000413BF
Reg Formula: 0x00041900  +8*de3id + de2id
    Where  :
           + $de3id(0-26):
Reg Desc   :
This is the per Channel interrupt status of DS2/E2 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_per_chn_intr_stat                                                          0x00041900
#define cAf6Reg_RxM12E12_per_chn_intr_stat_WidthVal                                                         32

/*--------------------------------------
BitField Name: DE2ParCnt_Intr
BitField Type: RW
BitField Desc: Set 1 if there is change DE2 Parity counter event to generate an
interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_stat_DE2ParCnt_Intr_Mask                                              cBit5
#define cAf6_RxM12E12_per_chn_intr_stat_DE2ParCnt_Intr_Shift                                                 5

/*--------------------------------------
BitField Name: DE2FBECnt_Intr
BitField Type: RW
BitField Desc: Set 1 if there is change DE2 FBE counter event to generate an
interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_stat_DE2FBECnt_Intr_Mask                                              cBit4
#define cAf6_RxM12E12_per_chn_intr_stat_DE2FBECnt_Intr_Shift                                                 4

/*--------------------------------------
BitField Name: DE2LosAllZero_Intr
BitField Type: RW
BitField Desc: Set 1 if there is a change LOS All Zero event to generate an
interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_stat_DE2LosAllZero_Intr_Mask                                          cBit3
#define cAf6_RxM12E12_per_chn_intr_stat_DE2LosAllZero_Intr_Shift                                             3

/*--------------------------------------
BitField Name: DE2AisAllOne_Intr
BitField Type: RW
BitField Desc: Set 1 if there is a change AIS All One event to generate an
interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_stat_DE2AisAllOne_Intr_Mask                                           cBit2
#define cAf6_RxM12E12_per_chn_intr_stat_DE2AisAllOne_Intr_Shift                                              2

/*--------------------------------------
BitField Name: DE2RDI_Intr
BitField Type: RW
BitField Desc: Set 1 if there is a  change RDI event to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_stat_DE2RDI_Intr_Mask                                                 cBit1
#define cAf6_RxM12E12_per_chn_intr_stat_DE2RDI_Intr_Shift                                                    1

/*--------------------------------------
BitField Name: DE2Lof_Intr
BitField Type: RW
BitField Desc: Set 1 if there is a  change LOF event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_intr_stat_DE2Lof_Intr_Mask                                                 cBit0
#define cAf6_RxM12E12_per_chn_intr_stat_DE2Lof_Intr_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RxM12E12 Framer per Channel Current Status
Reg Addr   : 0x00041A00-0x000413DF
Reg Formula: 0x00041A00  +8*de3id + de2id
    Where  :
           + $de3id(0-26):
Reg Desc   :
This is the per Channel Current status of DS2/E2 Rx Framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_RxM12E12_per_chn_curr_stat                                                          0x00041A00
#define cAf6Reg_RxM12E12_per_chn_curr_stat_WidthVal                                                         32

/*--------------------------------------
BitField Name: DE2ParCnt_CurrSta
BitField Type: RW
BitField Desc: Current status DE2 Parity counter event to generate an interrupt.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_curr_stat_DE2ParCnt_CurrSta_Mask                                           cBit5
#define cAf6_RxM12E12_per_chn_curr_stat_DE2ParCnt_CurrSta_Shift                                              5

/*--------------------------------------
BitField Name: DE2FBECnt_CurrSta
BitField Type: RW
BitField Desc: Current status DE2 FBE counter event to generate an interrupt.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_curr_stat_DE2FBECnt_CurrSta_Mask                                           cBit4
#define cAf6_RxM12E12_per_chn_curr_stat_DE2FBECnt_CurrSta_Shift                                              4

/*--------------------------------------
BitField Name: DE2LosAllZeroCurrSta
BitField Type: RW
BitField Desc: Current status of LOS All Zero event to generate an interrupt.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_curr_stat_DE2LosAllZeroCurrSta_Mask                                        cBit3
#define cAf6_RxM12E12_per_chn_curr_stat_DE2LosAllZeroCurrSta_Shift                                           3

/*--------------------------------------
BitField Name: DE2AisAllOneCurrSta
BitField Type: RW
BitField Desc: Current status of AIS All One event to generate an interrupt.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_curr_stat_DE2AisAllOneCurrSta_Mask                                         cBit2
#define cAf6_RxM12E12_per_chn_curr_stat_DE2AisAllOneCurrSta_Shift                                            2

/*--------------------------------------
BitField Name: DE2RDICurrSta
BitField Type: RW
BitField Desc: Current status of RDI event to generate an interrupt.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_curr_stat_DE2RDICurrSta_Mask                                               cBit1
#define cAf6_RxM12E12_per_chn_curr_stat_DE2RDICurrSta_Shift                                                  1

/*--------------------------------------
BitField Name: DE2LofCurrSta
BitField Type: RW
BitField Desc: Current status of LOF event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RxM12E12_per_chn_curr_stat_DE2LofCurrSta_Mask                                               cBit0
#define cAf6_RxM12E12_per_chn_curr_stat_DE2LofCurrSta_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : TxM12E12 Second Value Threshold Control
Reg Addr   : 0x00081ffe
Reg Formula:
    Where  :
Reg Desc   :
This is the Second Value threshold configuration register for the PDH DS2/E2 Tx framer

------------------------------------------------------------------------------*/
#define cAf6Reg_txm12e12_nsec_val_thrsh_cfg                                                         0x00081ffe

/*--------------------------------------
BitField Name: TxM12E12NsecValThres
BitField Type: RW
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_txm12e12_nsec_val_thrsh_cfg_TxM12E12NsecValThres_Mask                                    cBit31_0
#define cAf6_txm12e12_nsec_val_thrsh_cfg_TxM12E12NsecValThres_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : Force F Bit Error Configuration
Reg Addr   : 0x00081800-0x818FF
Reg Formula: 0x00081800 +  8*de3id + de2id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   :
This register is applicable to configure Force F Bit Error fore DS1/E1 Frame

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_fbitcfg                                                                        0x00081800
#define cAf6Reg_upen_fbitcfg_WidthVal                                                                       64

/*--------------------------------------
BitField Name: fbittim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define cAf6_upen_fbitcfg_fbittim_mod_Mask                                                           cBit27_26
#define cAf6_upen_fbitcfg_fbittim_mod_Shift                                                                 26

/*--------------------------------------
BitField Name: fbiterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define cAf6_upen_fbitcfg_fbiterr_clr_Mask                                                           cBit25_18
#define cAf6_upen_fbitcfg_fbiterr_clr_Shift                                                                 18

/*--------------------------------------
BitField Name: fbiterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define cAf6_upen_fbitcfg_fbiterr_set_Mask                                                           cBit17_10
#define cAf6_upen_fbitcfg_fbiterr_set_Shift                                                                 10

/*--------------------------------------
BitField Name: fbiterr_step
BitField Type: RW
BitField Desc: The Total Step to Force FBIT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define cAf6_upen_fbitcfg_fbiterr_step_Mask                                                            cBit9_2
#define cAf6_upen_fbitcfg_fbiterr_step_Shift                                                                 2

/*--------------------------------------
BitField Name: fbitmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for FBIT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define cAf6_upen_fbitcfg_fbitmsk_pos_Mask_01                                                        cBit31_18
#define cAf6_upen_fbitcfg_fbitmsk_pos_Shift_01                                                              18
#define cAf6_upen_fbitcfg_fbitmsk_pos_Mask_02                                                          cBit1_0
#define cAf6_upen_fbitcfg_fbitmsk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: fbitmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For FBIT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define cAf6_upen_fbitcfg_fbitmsk_ena_Mask                                                              cBit17
#define cAf6_upen_fbitcfg_fbitmsk_ena_Shift                                                                 17

/*--------------------------------------
BitField Name: fbiterr_num
BitField Type: RW
BitField Desc: The Total FBIT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define cAf6_upen_fbitcfg_fbiterr_num_Mask                                                            cBit16_1
#define cAf6_upen_fbitcfg_fbiterr_num_Shift                                                                  1

/*--------------------------------------
BitField Name: fbitfrc_mod
BitField Type: RW
BitField Desc: Force FBIT Mode 0: One shot ( N events in T the unit time) 1:
continous (fbiterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_fbitcfg_fbitfrc_mod_Mask                                                               cBit0
#define cAf6_upen_fbitcfg_fbitfrc_mod_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force F Bit Error Status
Reg Addr   : 0x00081900-0x819FF
Reg Formula: 0x00081900 +  8*de3id + de2id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   :
This register is applicable to see the Force F Bit Error Status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_fbitsta                                                                        0x00081900
#define cAf6Reg_upen_fbitsta_WidthVal                                                                       64

/*--------------------------------------
BitField Name: fbitsta_almset
BitField Type: RW
BitField Desc: FBIT alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define cAf6_upen_fbitsta_fbitsta_almset_Mask                                                           cBit27
#define cAf6_upen_fbitsta_fbitsta_almset_Shift                                                              27

/*--------------------------------------
BitField Name: fbitstaclralrm
BitField Type: RW
BitField Desc: FBIT clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define cAf6_upen_fbitsta_fbitstaclralrm_Mask                                                        cBit22_15
#define cAf6_upen_fbitsta_fbitstaclralrm_Shift                                                              15

/*--------------------------------------
BitField Name: fbitstasetalrm
BitField Type: RW
BitField Desc: FBIT Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define cAf6_upen_fbitsta_fbitstasetalrm_Mask                                                         cBit14_7
#define cAf6_upen_fbitsta_fbitstasetalrm_Shift                                                               7

/*--------------------------------------
BitField Name: fbitstatimecnt
BitField Type: RW
BitField Desc: FBIT Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define cAf6_upen_fbitsta_fbitstatimecnt_Mask                                                          cBit6_0
#define cAf6_upen_fbitsta_fbitstatimecnt_Shift                                                               0

/*--------------------------------------
BitField Name: fbitstafrc_ena
BitField Type: RW
BitField Desc: FBIT Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define cAf6_upen_fbitsta_fbitstafrc_ena_Mask                                                           cBit31
#define cAf6_upen_fbitsta_fbitstafrc_ena_Shift                                                              31

/*--------------------------------------
BitField Name: fbitstaerrstep
BitField Type: RW
BitField Desc: The Second FBIT Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define cAf6_upen_fbitsta_fbitstaerrstep_Mask                                                        cBit30_23
#define cAf6_upen_fbitsta_fbitstaerrstep_Shift                                                              23

/*--------------------------------------
BitField Name: fbitstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask FBIT Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define cAf6_upen_fbitsta_fbitstamsk_pos_Mask                                                        cBit22_19
#define cAf6_upen_fbitsta_fbitstamsk_pos_Shift                                                              19

/*--------------------------------------
BitField Name: fbitstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error FBIT Status
BitField Bits: [18:3]
--------------------------------------*/
#define cAf6_upen_fbitsta_fbitstaerr_cnt_Mask                                                         cBit18_3
#define cAf6_upen_fbitsta_fbitstaerr_cnt_Shift                                                               3

/*--------------------------------------
BitField Name: fbitstafrc_sta
BitField Type: RW
BitField Desc: FBIT State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define cAf6_upen_fbitsta_fbitstafrc_sta_Mask                                                          cBit2_1
#define cAf6_upen_fbitsta_fbitstafrc_sta_Shift                                                               1

/*--------------------------------------
BitField Name: fbitstaoneunit
BitField Type: RW
BitField Desc: Force FBIT in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_fbitsta_fbitstaoneunit_Mask                                                            cBit0
#define cAf6_upen_fbitsta_fbitstaoneunit_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force PAR Bit Error Configuration
Reg Addr   : 0x00081A00-0x81AFF
Reg Formula: 0x00081A00 +  8*de3id + de2id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   :
This register is applicable to configure Force PAR Bit Error

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_par_cfg                                                                        0x00081A00
#define cAf6Reg_upen_par_cfg_WidthVal                                                                       64

/*--------------------------------------
BitField Name: partim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define cAf6_upen_par_cfg_partim_mod_Mask                                                            cBit13_12
#define cAf6_upen_par_cfg_partim_mod_Shift                                                                  12

/*--------------------------------------
BitField Name: parerr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define cAf6_upen_par_cfg_parerr_clr_Mask                                                               cBit11
#define cAf6_upen_par_cfg_parerr_clr_Shift                                                                  11

/*--------------------------------------
BitField Name: parerr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define cAf6_upen_par_cfg_parerr_set_Mask                                                               cBit10
#define cAf6_upen_par_cfg_parerr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: parerr_step
BitField Type: RW
BitField Desc: The Total Step to Force PAR Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define cAf6_upen_par_cfg_parerr_step_Mask                                                             cBit9_2
#define cAf6_upen_par_cfg_parerr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: parmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for PAR Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define cAf6_upen_par_cfg_parmsk_pos_Mask_01                                                         cBit31_18
#define cAf6_upen_par_cfg_parmsk_pos_Shift_01                                                               18
#define cAf6_upen_par_cfg_parmsk_pos_Mask_02                                                           cBit1_0
#define cAf6_upen_par_cfg_parmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: parmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For  PAR Congiguration
BitField Bits: [17]
--------------------------------------*/
#define cAf6_upen_par_cfg_parmsk_ena_Mask                                                               cBit17
#define cAf6_upen_par_cfg_parmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: parerr_num
BitField Type: RW
BitField Desc: The Total PAR Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define cAf6_upen_par_cfg_parerr_num_Mask                                                             cBit16_1
#define cAf6_upen_par_cfg_parerr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: parfrc_mod
BitField Type: RW
BitField Desc: Force PAR Mode 0: One shot 1: continous (parerr_num = 16 bit
FFFF)
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_par_cfg_parfrc_mod_Mask                                                                cBit0
#define cAf6_upen_par_cfg_parfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force PAR Bit Error Status
Reg Addr   : : 0x00081B00 -0x81BFF
Reg Formula: : 0x00081B00 +  8*de3id + de2id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   :
This register is applicable to see the Force PAR Bit Error Status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_par_sta                                                                 0x00081B00
#define cAf6Reg_upen_par_sta_WidthVal                                                                64

/*--------------------------------------
BitField Name: cpbsta_almset
BitField Type: RW
BitField Desc: PAR bit alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define cAf6_upen_par_sta_cpbsta_almset_Mask                                                             cBit9
#define cAf6_upen_par_sta_cpbsta_almset_Shift                                                                9

/*--------------------------------------
BitField Name: cpbstaclralrm
BitField Type: RW
BitField Desc: PAR bit clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define cAf6_upen_par_sta_cpbstaclralrm_Mask                                                             cBit8
#define cAf6_upen_par_sta_cpbstaclralrm_Shift                                                                8

/*--------------------------------------
BitField Name: cpbstasetalrm
BitField Type: RW
BitField Desc: PAR bit Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define cAf6_upen_par_sta_cpbstasetalrm_Mask                                                             cBit7
#define cAf6_upen_par_sta_cpbstasetalrm_Shift                                                                7

/*--------------------------------------
BitField Name: cpbstatimecnt
BitField Type: RW
BitField Desc: PAR bit Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define cAf6_upen_par_sta_cpbstatimecnt_Mask                                                           cBit6_0
#define cAf6_upen_par_sta_cpbstatimecnt_Shift                                                                0

/*--------------------------------------
BitField Name: cpbstafrc_ena
BitField Type: RW
BitField Desc: PAR bit Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define cAf6_upen_par_sta_cpbstafrc_ena_Mask                                                            cBit31
#define cAf6_upen_par_sta_cpbstafrc_ena_Shift                                                               31

/*--------------------------------------
BitField Name: cpbstaerrstep
BitField Type: RW
BitField Desc: The Second PAR bit Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define cAf6_upen_par_sta_cpbstaerrstep_Mask                                                         cBit30_23
#define cAf6_upen_par_sta_cpbstaerrstep_Shift                                                               23

/*--------------------------------------
BitField Name: cpbstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask PAR bit Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define cAf6_upen_par_sta_cpbstamsk_pos_Mask                                                         cBit22_19
#define cAf6_upen_par_sta_cpbstamsk_pos_Shift                                                               19

/*--------------------------------------
BitField Name: cpbstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error PAR bit Status
BitField Bits: [18:3]
--------------------------------------*/
#define cAf6_upen_par_sta_cpbstaerr_cnt_Mask                                                          cBit18_3
#define cAf6_upen_par_sta_cpbstaerr_cnt_Shift                                                                3

/*--------------------------------------
BitField Name: cpbstafrc_sta
BitField Type: RW
BitField Desc: PAR bit State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define cAf6_upen_par_sta_cpbstafrc_sta_Mask                                                           cBit2_1
#define cAf6_upen_par_sta_cpbstafrc_sta_Shift                                                                1

/*--------------------------------------
BitField Name: cpbstaoneunit
BitField Type: RW
BitField Desc: Force PAR bit in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_par_sta_cpbstaoneunit_Mask                                                             cBit0
#define cAf6_upen_par_sta_cpbstaoneunit_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : F Bit Control
Reg Addr   : 0x81100-0x811FF
Reg Formula: 0x81100 +  8*de3id + de2id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   :
This register is applicable to see the  F bit Counter Status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_fbit_sta                                                                          0x81100
#define cAf6Reg_upen_fbit_sta_WidthVal                                                                      32

/*--------------------------------------
BitField Name: fas_enb
BitField Type: RW
BitField Desc: Force NFAS Enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_fbit_sta_fas_enb_Mask                                                                  cBit1
#define cAf6_upen_fbit_sta_fas_enb_Shift                                                                     1

/*--------------------------------------
BitField Name: mfas_enb
BitField Type: RW
BitField Desc: Force FAS Enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_fbit_sta_mfas_enb_Mask                                                                 cBit0
#define cAf6_upen_fbit_sta_mfas_enb_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Force RAI Alarm Configuration
Reg Addr   : 0x00081C00-0x81CFF
Reg Formula: 0x00081C00 +  8*de3id + de2id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   :
This register is applicable to configure Force RAI Error force DS1/E1 Frame

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rai_cfg                                                                        0x00081C00
#define cAf6Reg_upen_rai_cfg_WidthVal                                                                       64

/*--------------------------------------
BitField Name: raitim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define cAf6_upen_rai_cfg_raitim_mod_Mask                                                            cBit27_26
#define cAf6_upen_rai_cfg_raitim_mod_Shift                                                                  26

/*--------------------------------------
BitField Name: raierr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define cAf6_upen_rai_cfg_raierr_clr_Mask                                                            cBit25_18
#define cAf6_upen_rai_cfg_raierr_clr_Shift                                                                  18

/*--------------------------------------
BitField Name: raierr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define cAf6_upen_rai_cfg_raierr_set_Mask                                                            cBit17_10
#define cAf6_upen_rai_cfg_raierr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: raierr_step
BitField Type: RW
BitField Desc: The Total Step to Force RAI Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define cAf6_upen_rai_cfg_raierr_step_Mask                                                             cBit9_2
#define cAf6_upen_rai_cfg_raierr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: raimsk_pos
BitField Type: RW
BitField Desc: The Position Mask for RAI Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define cAf6_upen_rai_cfg_raimsk_pos_Mask_01                                                         cBit31_18
#define cAf6_upen_rai_cfg_raimsk_pos_Shift_01                                                               18
#define cAf6_upen_rai_cfg_raimsk_pos_Mask_02                                                           cBit1_0
#define cAf6_upen_rai_cfg_raimsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: raimsk_ena
BitField Type: RW
BitField Desc: The Data Mask For RAI Congiguration
BitField Bits: [17]
--------------------------------------*/
#define cAf6_upen_rai_cfg_raimsk_ena_Mask                                                               cBit17
#define cAf6_upen_rai_cfg_raimsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: raierr_num
BitField Type: RW
BitField Desc: The Total RAI Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define cAf6_upen_rai_cfg_raierr_num_Mask                                                             cBit16_1
#define cAf6_upen_rai_cfg_raierr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: raifrc_mod
BitField Type: RW
BitField Desc: Force RAI Mode 0: One shot ( N events in T the unit time) 1:
continous (raierr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_rai_cfg_raifrc_mod_Mask                                                                cBit0
#define cAf6_upen_rai_cfg_raifrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force RAI Alarm Status
Reg Addr   : 0x00081D00-0x81DFF
Reg Formula: 0x00081D00 +  8*de3id + de2id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   :
This register is applicable to see the Force RAI Error Status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rai_sta                                                                        0x00081D00
#define cAf6Reg_upen_rai_sta_WidthVal                                                                       64

/*--------------------------------------
BitField Name: raista_almset
BitField Type: RW
BitField Desc: RAI alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define cAf6_upen_rai_sta_raista_almset_Mask                                                            cBit27
#define cAf6_upen_rai_sta_raista_almset_Shift                                                               27

/*--------------------------------------
BitField Name: raistaclralrm
BitField Type: RW
BitField Desc: RAI clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define cAf6_upen_rai_sta_raistaclralrm_Mask                                                         cBit22_15
#define cAf6_upen_rai_sta_raistaclralrm_Shift                                                               15

/*--------------------------------------
BitField Name: raistasetalrm
BitField Type: RW
BitField Desc: RAI Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define cAf6_upen_rai_sta_raistasetalrm_Mask                                                          cBit14_7
#define cAf6_upen_rai_sta_raistasetalrm_Shift                                                                7

/*--------------------------------------
BitField Name: raistatimecnt
BitField Type: RW
BitField Desc: RAI Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define cAf6_upen_rai_sta_raistatimecnt_Mask                                                           cBit6_0
#define cAf6_upen_rai_sta_raistatimecnt_Shift                                                                0

/*--------------------------------------
BitField Name: raistafrc_ena
BitField Type: RW
BitField Desc: RAI Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define cAf6_upen_rai_sta_raistafrc_ena_Mask                                                            cBit31
#define cAf6_upen_rai_sta_raistafrc_ena_Shift                                                               31

/*--------------------------------------
BitField Name: raistaerrstep
BitField Type: RW
BitField Desc: The Second RAI Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define cAf6_upen_rai_sta_raistaerrstep_Mask                                                         cBit30_23
#define cAf6_upen_rai_sta_raistaerrstep_Shift                                                               23

/*--------------------------------------
BitField Name: raistamsk_pos
BitField Type: RW
BitField Desc: The Position Mask RAI Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define cAf6_upen_rai_sta_raistamsk_pos_Mask                                                         cBit22_19
#define cAf6_upen_rai_sta_raistamsk_pos_Shift                                                               19

/*--------------------------------------
BitField Name: raistaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error RAI Status
BitField Bits: [18:3]
--------------------------------------*/
#define cAf6_upen_rai_sta_raistaerr_cnt_Mask                                                          cBit18_3
#define cAf6_upen_rai_sta_raistaerr_cnt_Shift                                                                3

/*--------------------------------------
BitField Name: raistafrc_sta
BitField Type: RW
BitField Desc: RAI State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define cAf6_upen_rai_sta_raistafrc_sta_Mask                                                           cBit2_1
#define cAf6_upen_rai_sta_raistafrc_sta_Shift                                                                1

/*--------------------------------------
BitField Name: raistaoneunit
BitField Type: RW
BitField Desc: Force RAI in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_rai_sta_raistaoneunit_Mask                                                             cBit0
#define cAf6_upen_rai_sta_raistaoneunit_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS Error Configuration
Reg Addr   : 0x00081E00-0x81EFF
Reg Formula: 0x00081E00 +  8*de3id + de2id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   :
This register is applicable to configure Force AIS Error force DS1/E1 Frame

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ais_cfg                                                                        0x00081E00
#define cAf6Reg_upen_ais_cfg_WidthVal                                                                       64

/*--------------------------------------
BitField Name: aistim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define cAf6_upen_ais_cfg_aistim_mod_Mask                                                            cBit27_26
#define cAf6_upen_ais_cfg_aistim_mod_Shift                                                                  26

/*--------------------------------------
BitField Name: aiserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define cAf6_upen_ais_cfg_aiserr_clr_Mask                                                            cBit25_18
#define cAf6_upen_ais_cfg_aiserr_clr_Shift                                                                  18

/*--------------------------------------
BitField Name: aiserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define cAf6_upen_ais_cfg_aiserr_set_Mask                                                            cBit17_10
#define cAf6_upen_ais_cfg_aiserr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: aiserr_step
BitField Type: RW
BitField Desc: The Total Step to Force AIS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define cAf6_upen_ais_cfg_aiserr_step_Mask                                                             cBit9_2
#define cAf6_upen_ais_cfg_aiserr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: aismsk_pos
BitField Type: RW
BitField Desc: The Position Mask for AIS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define cAf6_upen_ais_cfg_aismsk_pos_Mask_01                                                         cBit31_18
#define cAf6_upen_ais_cfg_aismsk_pos_Shift_01                                                               18
#define cAf6_upen_ais_cfg_aismsk_pos_Mask_02                                                           cBit1_0
#define cAf6_upen_ais_cfg_aismsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: aismsk_ena
BitField Type: RW
BitField Desc: The Data Mask For AIS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define cAf6_upen_ais_cfg_aismsk_ena_Mask                                                               cBit17
#define cAf6_upen_ais_cfg_aismsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: aiserr_num
BitField Type: RW
BitField Desc: The Total AIS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define cAf6_upen_ais_cfg_aiserr_num_Mask                                                             cBit16_1
#define cAf6_upen_ais_cfg_aiserr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: aisfrc_mod
BitField Type: RW
BitField Desc: Force AIS Mode 0: One shot ( N events in T the unit time) 1:
continous (aiserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_ais_cfg_aisfrc_mod_Mask                                                                cBit0
#define cAf6_upen_ais_cfg_aisfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS Error Status
Reg Addr   : 0x00081F00 - 0x81FFF
Reg Formula: 0x00081F00 +  8*de3id + de2id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
Reg Desc   :
This register is applicable to see the Force AIS Error Status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ais_sta                                                                        0x00081F00
#define cAf6Reg_upen_ais_sta_WidthVal                                                                       64

/*--------------------------------------
BitField Name: AISsta_almset
BitField Type: RW
BitField Desc: AIS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define cAf6_upen_ais_sta_AISsta_almset_Mask                                                            cBit27
#define cAf6_upen_ais_sta_AISsta_almset_Shift                                                               27

/*--------------------------------------
BitField Name: AISstaclralrm
BitField Type: RW
BitField Desc: AIS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define cAf6_upen_ais_sta_AISstaclralrm_Mask                                                         cBit22_15
#define cAf6_upen_ais_sta_AISstaclralrm_Shift                                                               15

/*--------------------------------------
BitField Name: AISstasetalrm
BitField Type: RW
BitField Desc: AIS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define cAf6_upen_ais_sta_AISstasetalrm_Mask                                                          cBit14_7
#define cAf6_upen_ais_sta_AISstasetalrm_Shift                                                                7

/*--------------------------------------
BitField Name: AISstatimecnt
BitField Type: RW
BitField Desc: AIS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define cAf6_upen_ais_sta_AISstatimecnt_Mask                                                           cBit6_0
#define cAf6_upen_ais_sta_AISstatimecnt_Shift                                                                0

/*--------------------------------------
BitField Name: AISstafrc_ena
BitField Type: RW
BitField Desc: AIS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define cAf6_upen_ais_sta_AISstafrc_ena_Mask                                                            cBit31
#define cAf6_upen_ais_sta_AISstafrc_ena_Shift                                                               31

/*--------------------------------------
BitField Name: AISstaerrstep
BitField Type: RW
BitField Desc: The Second AIS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define cAf6_upen_ais_sta_AISstaerrstep_Mask                                                         cBit30_23
#define cAf6_upen_ais_sta_AISstaerrstep_Shift                                                               23

/*--------------------------------------
BitField Name: AISstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask AIS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define cAf6_upen_ais_sta_AISstamsk_pos_Mask                                                         cBit22_19
#define cAf6_upen_ais_sta_AISstamsk_pos_Shift                                                               19

/*--------------------------------------
BitField Name: AISstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error AIS Status
BitField Bits: [18:3]
--------------------------------------*/
#define cAf6_upen_ais_sta_AISstaerr_cnt_Mask                                                          cBit18_3
#define cAf6_upen_ais_sta_AISstaerr_cnt_Shift                                                                3

/*--------------------------------------
BitField Name: AISstafrc_sta
BitField Type: RW
BitField Desc: AIS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define cAf6_upen_ais_sta_AISstafrc_sta_Mask                                                           cBit2_1
#define cAf6_upen_ais_sta_AISstafrc_sta_Shift                                                                1

/*--------------------------------------
BitField Name: AISstaoneunit
BitField Type: RW
BitField Desc: Force AIS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_ais_sta_AISstaoneunit_Mask                                                             cBit0
#define cAf6_upen_ais_sta_AISstaoneunit_Shift                                                                0

#endif /* _AF6_REG_AF6CCI0011_RD_PDH_H_ */

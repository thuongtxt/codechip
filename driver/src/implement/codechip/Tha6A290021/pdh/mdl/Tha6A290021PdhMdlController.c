/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290022PdhMdlController.c
 *
 * Created Date: Oct 28, 2017
 *
 * Description : MDL Controller class
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pdh/ThaModulePdh.h"
#include "../../../../default/pdh/ThaPdhDe3.h"
#include "../../../Tha60210031/pdh/Tha60210031PdhMdlControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021PdhMdlController
    {
    tTha60210031PdhMdlController super;
    }tTha6A290021PdhMdlController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class*/
static char m_methodsInit = 0;

/* Override */
static tThaPdhMdlControllerMethods m_ThaPdhMdlControllerOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 InterruptOffset(ThaPdhMdlController self)
    {
    uint8 sliceId, hwIdInSlice;
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get((AtPdhMdlController)self);
    uint32 offset;

    if (ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice) != cAtOk)
        return cBit31_0;

    offset = (sliceId * 32UL);
    if (hwIdInSlice < 32)
        return offset + hwIdInSlice;

    return (uint32)(offset + (uint8)(hwIdInSlice - 32) + 0x400U);
    }

static void OverrideThaPdhMdlController(ThaPdhMdlController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhMdlControllerOverride, mMethodsGet(self), sizeof(m_ThaPdhMdlControllerOverride));
        mMethodOverride(m_ThaPdhMdlControllerOverride, InterruptOffset);
        }

    mMethodsSet(self, &m_ThaPdhMdlControllerOverride);
    }

static void Override(ThaPdhMdlController self)
    {
    OverrideThaPdhMdlController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021PdhMdlController);
    }

static AtPdhMdlController Tha6A290021PdhMdlControllerObjectInit(AtPdhMdlController self, AtPdhDe3 de3, uint32 mdlType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031PdhMdlControllerObjectInit(self, de3, mdlType) == NULL)
        return NULL;

    /* Setup methods */
    Override((ThaPdhMdlController)self);
    m_methodsInit = 1;

    return self;
    }

AtPdhMdlController Tha6A290021PdhMdlControllerNew(AtPdhDe3 self, uint32 mdlType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhMdlController newMdlController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newMdlController == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290021PdhMdlControllerObjectInit(newMdlController, self, mdlType);
    }

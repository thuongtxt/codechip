/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290022PdhMdlInterruptManager.c
 *
 * Created Date: Oct 18, 2017
 *
 * Description : MDL interrupt manager implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/pdh/AtPdhMdlControllerInternal.h"
#include "../../../../default/pdh/ThaModulePdh.h"
#include "../../../Tha60210011/pdh/mdl/Tha60210011PdhMdlInterruptManagerInternal.h"
#include "../Tha6A290021PdhMdlReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods        m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods       m_ThaInterruptManagerOverride;
static tThaPdhMdlInterruptManagerMethods m_ThaPdhMdlInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePdh PdhModule(AtInterruptManager self)
    {
    return (ThaModulePdh)AtInterruptManagerModuleGet(self);
    }

static uint32 NumStsInSlice(ThaInterruptManager self)
    {
    AtUnused(self);
    return 48;
    }

static uint32 InterruptStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_mdl_int_sta;
    }

static uint32 InterruptEnableRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_mdl_cfgen_int;
    }

static uint32 GroupOffset(uint8 groupId)
    {
    return groupId;
    }

static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    ThaModulePdh pdhModule = PdhModule(self);
    ThaPdhMdlInterruptManager manager = (ThaPdhMdlInterruptManager)self;
    uint32 baseAddress = ThaInterruptManagerBaseAddress((ThaInterruptManager)self);
    uint32 numSlices = ThaInterruptManagerNumSlices((ThaInterruptManager)self);
    uint32 numStsInSlice = ThaInterruptManagerNumStsInSlices((ThaInterruptManager)self);
    uint32 groupIntr = AtHalRead(hal, (baseAddress + mMethodsGet(manager)->GlbInterruptStatusRegister(manager)));
    uint8 groupId;
    AtUnused(glbIntr);

    for (groupId = 0; groupId < 8; groupId++)
        {
        if (groupIntr & cIteratorMask(groupId))
            {
            uint32 groupBaseAddress = baseAddress + GroupOffset(groupId);
            uint32 intrStatus = AtHalRead(hal, (groupBaseAddress + cAf6Reg_mdl_intsta)); /* each group 32 bits */
            uint32 intrMask   = AtHalRead(hal, (groupBaseAddress + cAf6Reg_mdl_intsta));
            uint8 sliceId;
            uint8 de3;
            intrStatus &= intrMask;

            for (sliceId = 0, de3 = (uint8)(groupId * 32); sliceId < numSlices; sliceId++)
                {
                if (intrStatus & cIteratorMask(sliceId))
                    {
                    uint32 intrStatus24 = AtHalRead(hal, (uint32)(groupBaseAddress + cAf6Reg_mdl_int1sta_Base + sliceId));
                    uint32 idx;

                    for (idx = 0; idx < 32 && de3 < numStsInSlice; de3++, idx++)
                        {
                        if (intrStatus24 & cIteratorMask(idx))
                            {
                            AtPdhDe3 de3Channel = ThaModulePdhDe3ChannelFromHwIdGet(pdhModule, cAtModulePdh, sliceId, de3);
                            AtPdhMdlController controller = ThaPdhDe3MdlControllerGet((ThaPdhDe3)de3Channel);
                            AtPdhMdlControllerInterruptProcess(controller, sliceId, de3, hal);
                            }
                        }
                    }
                }
            }
        }
    }

static uint32 GlbInterruptStatusRegister(ThaPdhMdlInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_mdl_sta_int; /* Global 8 bits for 8 groups */
    }

static void InterruptHwEnable(ThaInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    if (hal)
        {
        uint32 baseAddress = ThaInterruptManagerBaseAddress(self);
        uint32 regVal = (enable) ? cAf6_mdl_en_int_mdlen_int_Mask : 0x0;
        AtHalWrite(hal, baseAddress + cAf6Reg_mdl_en_int, regVal); /* Global 8 bits for 8 groups */
        }
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, NumStsInSlice);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptEnableRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptHwEnable);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void OverrideThaPdhMdlInterruptManager(AtInterruptManager self)
    {
    ThaPdhMdlInterruptManager manager = (ThaPdhMdlInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhMdlInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaPdhMdlInterruptManagerOverride));

        mMethodOverride(m_ThaPdhMdlInterruptManagerOverride, GlbInterruptStatusRegister);
        }

    mMethodsSet(manager, &m_ThaPdhMdlInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    OverrideThaPdhMdlInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210011PdhMdlInterruptManager);
    }

static AtInterruptManager Tha6A290021PdhMdlInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PdhMdlInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha6A290021PdhMdlInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290021PdhMdlInterruptManagerObjectInit(newManager, module);
    }

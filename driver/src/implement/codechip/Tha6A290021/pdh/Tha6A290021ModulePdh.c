/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A290021ModulePdh.c
 *
 * Created Date: Dec 3, 2015
 *
 * Description : PDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha6A210031/pdh/Tha6A210031PdhDe3AttReg.h"
#include "Tha6A290021ModulePdhInternal.h"
#include "Tha6A290021PdhAttControllerInternal.h"
#include "../../../default/att/ThaAttPdhManagerInternal.h"
#include "Tha6A290021PdhMdlReg.h"
#include "Tha6A290021PdhPrmReg.h"
#include "Tha6A290021PdhReg.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290021ModulePdh*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290021ModulePdh
    {
    tTha60290021ModulePdh super;
    AtLongRegisterAccess attLongRegisterAccess;
    }tTha6A290021ModulePdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tAtModulePdhMethods     m_AtModulePdhOverride;
static tThaModulePdhMethods    m_ThaModulePdhOverride;
static tThaStmModulePdhMethods m_ThaStmModulePdhOverride;
static tTha60210031ModulePdhMethods m_Tha60210031ModulePdhOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods = NULL;
static const tThaModulePdhMethods *m_ThaModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
AtPdhMdlController Tha6A290021PdhMdlControllerNew(AtPdhDe3 self, uint32 mdlType);
AtInterruptManager Tha6A290021PdhPrmInterruptManagerNew(AtModule module);
AtInterruptManager Tha6A290021PdhMdlInterruptManagerNew(AtModule module);
AtPdhPrmController Tha6A290021PdhPrmControllerNew(AtPdhDe1 de1);
AtPktAnalyzer Tha6A290021PdhPrmAnalyzerNew(AtChannel channel);

/*--------------------------- Implementation ---------------------------------*/

static uint32 RxM12E12FrmChnIntrEnCtrlBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_RxM12E12_per_chn_intr_cfg;
    }

static uint32 RxM12E12FrmChnIntrChngStatusBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_RxM12E12_per_chn_intr_stat;
    }

static uint32 RxM12E12FrmChnAlarmCurStatusBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_RxM12E12_per_chn_curr_stat;
    }

static AtObjectAny De2AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290021PdhDe2AttControllerNew(channel);
    }

static AtObjectAny De3AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290021PdhDe3AttControllerNew(channel);
    }

static eAtRet De2DefaultSet(ThaModulePdh self)
    {
    uint8  numSlices    = ThaModulePdhNumSlices(self);
    uint8 slice;
    for (slice =0; slice <numSlices; slice ++)
        {
        uint32 baseAddress = ThaModulePdhSliceBase(self, slice);
        mModuleHwWrite(self, cAf6Reg_rxm12e12_fbe_thrsh_cfg     + baseAddress, 0xFF);
        mModuleHwWrite(self, cAf6Reg_rxm12e12_parity_thrsh_cfg  + baseAddress, 0xFF);
        mModuleHwWrite(self, cAf6Reg_rxm12e12_rai_thrsh_cfg     + baseAddress, 0x7);
        }

    return cAtOk;
    }

static uint32 MdlTxPktOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    uint32 offset = r2c ? 0 : 128;

    AtUnused(self);
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return cAf6Reg_upen_txmdl_cntr2c_validpath + offset;
        case cAtPdhMdlControllerTypeIdleSignal:  return cAf6Reg_upen_txmdl_cntr2c_valididle + offset;
        case cAtPdhMdlControllerTypeTestSignal:  return cAf6Reg_upen_txmdl_cntr2c_validtest + offset;
        default:                                 return 0;
        }
    }

static uint32 MdlTxByteOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    uint32 offset = r2c ? 0 : 128;

    AtUnused(self);

    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return cAf6Reg_upen_txmdl_cntr2c_bytepath  + offset;
        case cAtPdhMdlControllerTypeIdleSignal:  return cAf6Reg_upen_txmdl_cntr2c_byteidle1 + offset;
        case cAtPdhMdlControllerTypeTestSignal:  return cAf6Reg_upen_txmdl_cntr2c_bytetest  + offset;
        default:                                 return 0;
        }
    }


static uint32 MdlRxCounterOffset(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    uint8 sliceId, hwIdInSlice;
    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);
    return (hwIdInSlice * 8UL) + sliceId;
    }

static uint32 MdlRxPktOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    uint32 offset = r2c ? 0 : 2048;

    AtUnused(self);

    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return cAf6Reg_upen_rxmdl_cntr2c_goodpath + offset;
        case cAtPdhMdlControllerTypeIdleSignal:  return cAf6Reg_upen_rxmdl_cntr2c_goodidle + offset;
        case cAtPdhMdlControllerTypeTestSignal:  return cAf6Reg_upen_rxmdl_cntr2c_goodtest + offset;
        default:                                 return 0;
        }
    }

static uint32 MdlRxDropOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    uint32 offset = r2c ? 0 : 2048;

    AtUnused(self);

    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return cAf6Reg_upen_rxmdl_cntr2c_droppath + offset;
        case cAtPdhMdlControllerTypeIdleSignal:  return cAf6Reg_upen_rxmdl_cntr2c_dropidle + offset;
        case cAtPdhMdlControllerTypeTestSignal:  return cAf6Reg_upen_rxmdl_cntr2c_droptest + offset;
        default:                                 return 0;
        }
    }

static uint32 MdlRxByteOffset(Tha60210031ModulePdh self, uint32 type, eBool r2c)
    {
    uint32 offset = r2c ? 0 : 1024;

    AtUnused(self);

    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return cAf6Reg_upen_rxmdl_cntr2c_bytepath + offset;
        case cAtPdhMdlControllerTypeIdleSignal:  return cAf6Reg_upen_rxmdl_cntr2c_byteidle + offset;
        case cAtPdhMdlControllerTypeTestSignal:  return cAf6Reg_upen_rxmdl_cntr2c_bytetest + offset;
        default:                                 return 0;
        }
    }

static uint32 UpenMdlBuffer1Base(Tha60210031ModulePdh self, uint8 hwStsId)
    {
    AtUnused(self);
    if (hwStsId < 16)
        return cAf6Reg_upen_mdl_buffer1;
    return cAf6Reg_upen_mdl_buffer1_2;
    }

static uint32 UpenMdlBuffer1Offset(Tha60210031ModulePdh self, uint8 sliceId, uint8 hwIdInSlice)
    {
    AtUnused(self);
    return (hwIdInSlice % 16) + ((uint32)sliceId * 0x800);
    }

static uint32 MdlTxInsCtrlBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_sta_idle_alren;
    }

static uint32 UpenCfgMdlBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cfg_mdl;
    }

static uint32 UpenDestuffCtrl0Base(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_destuff_ctrl0;
    }

static uint32 MdlControllerSliceOffset(Tha60210031ModulePdh self, uint8 sliceId)
    {
    AtUnused(self);
    return sliceId * 32UL;
    }

static uint32 UpenRxmdlCfgtypeOffset(Tha60210031ModulePdh self, AtPdhDe3 de3)
    {
    uint8 sliceId, hwIdInSlice;
    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice);
    return (hwIdInSlice * 8UL) + sliceId;
    }

static uint32 UpenRxmdlCfgtypeBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxmdl_cfgtype;
    }

static uint32 UpenCfgCtrl0Base(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cfg_ctrl0;
    }

static uint32 UpenPrmTxcfgcrBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_prm_txcfgcr_Base;
    }

static uint32 UpenPrmTxcfglbBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_prm_txcfglb_Base;
    }

static uint32 UpenRxprmCfgstdBase(Tha60210031ModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_cfgstd_Base;
    }

static AtObjectAny De1AttControllerCreate(ThaModulePdh self, AtChannel channel)
	{
	AtUnused(self);
	return Tha6A290021PdhDe1AttControllerNew(channel);
	}

static uint32 StartVersionSupportMdl(ThaModulePdh self)
	{
	AtUnused(self);
	return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0547);
	}

static uint32 M12E12CounterOffset(ThaModulePdh self, ThaPdhDe2 de2, eBool r2c)
    {
    uint8 slice, hwIdInSlice;
    uint32 offset = 0;
    AtPdhChannel de3 = AtPdhChannelParentChannelGet((AtPdhChannel)de2);

    ThaPdhChannelHwIdGet(de3, cAtModulePdh, &slice, &hwIdInSlice);
    offset = 256 * (uint32)(r2c?0:1) + ThaModulePdhM12E12ControlOffset(self, (ThaPdhDe2)de2);
    return offset;
    }

static ThaVersionReader VersionReader(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaDeviceVersionReader(device);
    }

static eBool NeedMdl(ThaModulePdh self)
	{
	uint32 startVersionHasThis = StartVersionSupportMdl(self);
	uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtModule)self));
	return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
	}

static eBool MdlIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    if (NeedMdl(self))
    	return cAtTrue;
    return cAtFalse;
    }

static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
	AtUnused(self);
	return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0547);
    }

static eAtRet StsVtDemapVc4FractionalVc3Set(ThaStmModulePdh self, AtSdhChannel sdhVc, eBool isVc4FractionalVc3)
    {
    AtUnused(self);
    AtUnused(sdhVc);
    AtUnused(isVc4FractionalVc3);
    return cAtOk;
    }

static AtPdhDe1 De2De1Create(AtModulePdh self, AtPdhDe2 de2, uint32 de1Id)
    {
    AtUnused(de2);
    return Tha6A290021PdhDe2De1New(de1Id, self);
    }

static AtPdhDe1 VcDe1Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    return Tha6A290021PdhVcDe1New((AtSdhVc)sdhVc, self);
    }

static AtPdhDe3 VcDe3Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    uint8 slice, hwSts;
    ThaSdhChannel2HwMasterStsId(sdhVc, cAtModulePdh, &slice, &hwSts);
    return Tha6A290021PdhVcDe3New(hwSts, self, sdhVc);
    }

static uint32 *AttPdhHoldRegistersGet(Tha6A290021ModulePdh self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x8};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = 1;

    return holdRegisters;
    }

static AtLongRegisterAccess AttPdhLongRegisterAccessCreate(Tha6A290021ModulePdh self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AttPdhHoldRegistersGet(self, &numHoldRegisters);

    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static AtLongRegisterAccess AttPdhLongRegisterAccess(Tha6A290021ModulePdh self, uint32 localAddress)
    {
    AtUnused(localAddress);

    return self->attLongRegisterAccess;
    }

static uint16 AttPdhLongWriteOnCore(Tha6A290021ModulePdh self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core, uint32 holdRegOffset)
    {
    AtLongRegisterAccess registerAccess = AttPdhLongRegisterAccess(self, localAddress);
    if (registerAccess == NULL) return 0;
    AtLongRegisterHoldRegisterOffsetSet(registerAccess, holdRegOffset);
    return AtLongRegisterAccessWrite(registerAccess, AtIpCoreHalGet(core), localAddress, dataBuffer, bufferLen);
    }

static uint16 AttPdhLongReadOnCore(Tha6A290021ModulePdh self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core, uint32 holdRegOffset)
    {
    AtLongRegisterAccess registerAccess = AttPdhLongRegisterAccess(self, localAddress);
    if (registerAccess == NULL) return 0;
    AtLongRegisterHoldRegisterOffsetSet(registerAccess, holdRegOffset);
    return AtLongRegisterAccessRead(registerAccess, AtIpCoreHalGet(core), localAddress, dataBuffer, bufferLen);
    }

static void SetupData(AtModulePdh self)
    {
    Tha6A290021ModulePdh module = (Tha6A290021ModulePdh)self;
    if (module->attLongRegisterAccess == NULL)
        module->attLongRegisterAccess = AttPdhLongRegisterAccessCreate(module);
    }

static AtPdhDe2 De2Create(AtModulePdh self, uint32 de2Id)
    {
    return Tha6A290021PdhDe2New(de2Id, self);
    }

static AtAttPdhManager AttPdhManagerCreate(AtModulePdh self)
    {
    return Tha6A290021AttPdhManagerNew(self);
    }

static AtPdhMdlController MdlControllerCreate(ThaModulePdh self, AtPdhDe3 de3, uint32 mdlType)
    {
    AtUnused(self);
    return Tha6A290021PdhMdlControllerNew(de3, mdlType);
    }

static AtInterruptManager PrmInterruptManagerObjectCreate(ThaModulePdh self)
    {
    return Tha6A290021PdhPrmInterruptManagerNew((AtModule)self);
    }

static AtInterruptManager MdlInterruptManagerObjectCreate(ThaModulePdh self)
    {
    return Tha6A290021PdhMdlInterruptManagerNew((AtModule)self);
    }

static AtPdhPrmController PrmControllerObjectCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return Tha6A290021PdhPrmControllerNew(de1);
    }

static AtPktAnalyzer PrmAnalyzerObjectCreate(ThaModulePdh self)
    {
    AtUnused(self);
    return Tha6A290021PdhPrmAnalyzerNew(NULL);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->attLongRegisterAccess));
    m_AtObjectMethods->Delete(self);
    }

static uint32 StartVersionSupportDe1Ssm(ThaModulePdh self)
    {
    /* Not now */
    AtUnused(self);
    return 0;
    }

static void OverrideAtObject(AtObject self)
    {
    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(self), sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, De2De1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe3Create);
        mMethodOverride(m_AtModulePdhOverride, De2Create);
        mMethodOverride(m_AtModulePdhOverride, AttPdhManagerCreate);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(pdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, MdlIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De3AttControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, De2AttControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, De1AttControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportPrm);
        mMethodOverride(m_ThaModulePdhOverride, PrmControllerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, PrmAnalyzerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, MdlControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, PrmInterruptManagerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, MdlInterruptManagerObjectCreate);
        mMethodOverride(m_ThaModulePdhOverride, De2DefaultSet);
        mMethodOverride(m_ThaModulePdhOverride, M12E12CounterOffset);
        mMethodOverride(m_ThaModulePdhOverride, RxM12E12FrmChnAlarmCurStatusBase);
        mMethodOverride(m_ThaModulePdhOverride, RxM12E12FrmChnIntrChngStatusBase);
        mMethodOverride(m_ThaModulePdhOverride, RxM12E12FrmChnIntrEnCtrlBase);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1Ssm);
        }

    mMethodsSet(pdhModule, &m_ThaModulePdhOverride);
    }

static void OverrideThaStmModulePdh(AtModulePdh self)
    {
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_ThaStmModulePdhOverride));

        mMethodOverride(m_ThaStmModulePdhOverride, StsVtDemapVc4FractionalVc3Set);
        }

    mMethodsSet(pdhModule, &m_ThaStmModulePdhOverride);
    }

static void OverrideTha60210031ModulePdh(Tha60210031ModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031ModulePdhOverride, mMethodsGet(self), sizeof(m_Tha60210031ModulePdhOverride));

        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenCfgCtrl0Base);
		mMethodOverride(m_Tha60210031ModulePdhOverride, UpenPrmTxcfgcrBase);
		mMethodOverride(m_Tha60210031ModulePdhOverride, UpenPrmTxcfglbBase);
		mMethodOverride(m_Tha60210031ModulePdhOverride, UpenRxprmCfgstdBase);

        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenRxmdlCfgtypeBase);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenRxmdlCfgtypeOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenMdlBuffer1Base);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenMdlBuffer1Offset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenCfgMdlBase);
        mMethodOverride(m_Tha60210031ModulePdhOverride, UpenDestuffCtrl0Base);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlTxInsCtrlBase);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlControllerSliceOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlTxByteOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlTxPktOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxCounterOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxByteOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxDropOffset);
        mMethodOverride(m_Tha60210031ModulePdhOverride, MdlRxPktOffset);
        }

    mMethodsSet(self, &m_Tha60210031ModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtObject((AtObject)self);
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    OverrideThaStmModulePdh(self);
    OverrideTha60210031ModulePdh((Tha60210031ModulePdh) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290021ModulePdh);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    /* Setup internal data */
    SetupData(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha6A290021ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

uint16 Tha60290021ModulePdhAttLongReadOnCore(Tha6A290021ModulePdh self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core, uint32 holdRegOffset)
    {
    uint16 numDwords;

    if (self == NULL)
        return 0;

    numDwords = AttPdhLongReadOnCore(self, localAddress, dataBuffer, bufferLen, core, holdRegOffset);
    if (numDwords == 0) return 0;

    if (AtDriverDebugIsEnabled())
        AtDeviceLongReadNotify(AtModuleDeviceGet((AtModule) self), localAddress, dataBuffer, numDwords, 0);

    return numDwords;
    }

uint16 Tha60290021ModulePdhAttLongWriteOnCore(Tha6A290021ModulePdh self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core, uint32 holdRegOffset)
    {
    uint16 numDwords;

    if (self == NULL)
        return 0;

    numDwords = AttPdhLongWriteOnCore(self, localAddress, dataBuffer, bufferLen, core, holdRegOffset);
    if (numDwords == 0) return 0;

    if (AtDriverDebugIsEnabled())
        AtDeviceLongWriteNotify(AtModuleDeviceGet((AtModule) self), localAddress, dataBuffer, numDwords, 0);

    return numDwords;
    }


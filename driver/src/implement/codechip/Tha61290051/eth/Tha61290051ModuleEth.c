/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290051ModuleEth.c
 *
 * Created Date: Aug 20, 2018
 *
 * Description : 60290051 module ETh implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha61290051ModuleEthInternal.h"
#include "Tha61290051ModuleEth.h"
#include "Tha61290051ETHPASSReg.h"
#include "../../Tha60290051/eth/Tha60290051ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021ModuleEthMethods         m_Tha60290021ModuleEthOverride;
static tAtModuleEthMethods                 m_AtModuleEthOverride;

/* Save super implementation */
static const tTha60290021ModuleEthMethods  *m_Tha60290021ModuleEthMethods = NULL;
static const tAtModuleEthMethods  *m_AtModuleEthMethods = NULL;

/*--------------------------- Implementation ---------------------------------*/
static AtEthPort PwPortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    return Tha61290051Int40GEthPortNew(portId, (AtModuleEth)self);
    }

static uint8 NumSixteenPortGroup(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return 1;
    }

static uint8 NumberOfFaceplateGroupsGet(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return 2;
    }

static AtEthPort FaceplatePortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    return Tha60290051FaceplateSerdesEthPortNew(portId, (AtModuleEth)self);
    }

static uint32 Af6Reg_upen_enbflwctrl_Base(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return cAf6Reg_upen_enbflwctrl_Base;
    }

static eBool PortCanBeUsed(AtModuleEth self, uint8 portId)
    {
    AtEthPort port = AtModuleEthPortGet(self, portId);
    if (Tha60290021ModuleEthIsFaceplatePort(port))
        return cAtTrue;
    if (Tha60290021ModuleEthIs40GInternalPort(port))
        return cAtTrue;
    if (Tha60290021ModuleEthIsBackplaneMac(port))
        return cAtTrue;
    return cAtFalse;
    }

static void OverrideTha60290021ModuleEth(AtModuleEth self)
    {
    Tha60290021ModuleEth module = (Tha60290021ModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021ModuleEthMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModuleEthOverride, m_Tha60290021ModuleEthMethods, sizeof(m_Tha60290021ModuleEthOverride));

        mMethodOverride(m_Tha60290021ModuleEthOverride, PwPortCreate);
        mMethodOverride(m_Tha60290021ModuleEthOverride, NumSixteenPortGroup);
        mMethodOverride(m_Tha60290021ModuleEthOverride, NumberOfFaceplateGroupsGet);
        mMethodOverride(m_Tha60290021ModuleEthOverride, FaceplatePortCreate);
        mMethodOverride(m_Tha60290021ModuleEthOverride, Af6Reg_upen_enbflwctrl_Base);
        }

    mMethodsSet(module, &m_Tha60290021ModuleEthOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, PortCanBeUsed);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideTha60290021ModuleEth(self);
    OverrideAtModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61290051ModuleEth);
    }

static AtModuleEth ObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha61290051ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

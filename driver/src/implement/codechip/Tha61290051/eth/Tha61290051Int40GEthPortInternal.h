/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha61290051Int40GEthPortInternal.h
 * 
 * Created Date: Aug 21, 2018
 *
 * Description : Tha61290051 Internal 40G MAC port data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA61290051INT40GETHPORTINTERNAL_H_
#define _THA61290051INT40GETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/eth/Tha60290022PwEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha61290051Int40GEthPort
    {
    tTha60290022PwEthPort super;
    }tTha61290051Int40GEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA61290051INT40GETHPORTINTERNAL_H_ */


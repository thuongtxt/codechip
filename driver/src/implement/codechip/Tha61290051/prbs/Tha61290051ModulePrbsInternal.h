/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha61290051ModulePrbsInternal.h
 * 
 * Created Date: Aug 21, 2018
 *
 * Description : 61290051 module prbs internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA61290051MODULEPRBSINTERNAL_H_
#define _THA61290051MODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/prbs/Tha60290022ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha61290051ModulePrbs
    {
    tTha60290022ModulePrbsV2 super;
    }tTha61290051ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA61290051MODULEPRBSINTERNAL_H_ */


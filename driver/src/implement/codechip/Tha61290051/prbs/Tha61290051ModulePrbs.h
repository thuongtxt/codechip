/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha61290051ModulePrbs.h
 * 
 * Created Date: Sep 20, 2018
 *
 * Description : 61290051 module prbs interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA61290051MODULEPRBS_H_
#define _THA61290051MODULEPRBS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "AtModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha61290051ModulePrbsNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA61290051MODULEPRBS_H_ */


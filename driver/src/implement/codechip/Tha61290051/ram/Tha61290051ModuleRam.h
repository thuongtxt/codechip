/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha61290051ModuleRam.h
 * 
 * Created Date: Sep 20, 2018
 *
 * Description : 61290051 module RAM interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA61290051MODULERAM_H_
#define _THA61290051MODULERAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "AtModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha61290051ModuleRamNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA61290051MODULERAM_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLOCK
 * 
 * File        : Tha61290051ClockExtractorInternal.h
 * 
 * Created Date: Sep 7, 2018
 *
 * Description : Internal data for clock extractor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA61290051CLOCKEXTRACTORINTERNAL_H_
#define _THA61290051CLOCKEXTRACTORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/clock/Tha60290021ClockExtractorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha61290051ClockExtractor
    {
    tTha60290021ClockExtractor super;
    }tTha61290051ClockExtractor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA61290051CLOCKEXTRACTORINTERNAL_H_ */


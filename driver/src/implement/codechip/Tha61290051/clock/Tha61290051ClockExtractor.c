/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLOCK
 *
 * File        : Tha61290051ClockExtractor.c
 *
 * Created Date: Sep 7, 2018
 *
 * Description : Implementation for clock extractor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha61290051ClockExtractorInternal.h"
#include "Tha61290051ModuleClock.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021ClockExtractorMethods m_Tha60290021ClockExtractorOverride;

/* Supper implementations */
static const tTha60290021ClockExtractorMethods *m_Tha60290021ClockExtractorMethods = NULL;
static tThaClockExtractorMethods m_ThaClockExtractorOverride;

/* Supper implementations */
static const tThaClockExtractorMethods *m_ThaClockExtractorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleClockRet HwSdhLineClockExtract(ThaClockExtractor self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    return cAtErrorModeNotSupport;
    }

static eBool HwClockExtractShouldUseDiag(Tha60290021ClockExtractor self, AtSerdesController serdes)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtClockExtractorModuleGet((AtClockExtractor)self));
    AtUnused(serdes);
    return AtDeviceDiagnosticModeIsEnabled(device);
    }

static void OverrideThaClockExtractor(AtClockExtractor self)
    {
    ThaClockExtractor extractor = (ThaClockExtractor) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClockExtractorMethods = mMethodsGet(extractor);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockExtractorOverride, m_ThaClockExtractorMethods, sizeof(m_ThaClockExtractorOverride));

        mMethodOverride(m_ThaClockExtractorOverride, HwSdhLineClockExtract);
        }

    mMethodsSet(extractor, &m_ThaClockExtractorOverride);
    }

static void OverrideTha60290021ClockExtractor(AtClockExtractor self)
    {
    Tha60290021ClockExtractor extractor = (Tha60290021ClockExtractor) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021ClockExtractorMethods = mMethodsGet(extractor);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ClockExtractorOverride, m_Tha60290021ClockExtractorMethods, sizeof(m_Tha60290021ClockExtractorOverride));

        mMethodOverride(m_Tha60290021ClockExtractorOverride, HwClockExtractShouldUseDiag);
        }

    mMethodsSet(extractor, &m_Tha60290021ClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideThaClockExtractor(self);
    OverrideTha60290021ClockExtractor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61290051ClockExtractor);
    }

static AtClockExtractor ObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021ClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtClockExtractor Tha61290051ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newExtractor, clockModule, extractorId);
    }

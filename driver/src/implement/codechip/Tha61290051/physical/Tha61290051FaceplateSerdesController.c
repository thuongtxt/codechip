/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha61290051FaceplateSerdesController.c
 *
 * Created Date: Aug 20, 2018
 *
 * Description : 61290051 faceplate serdes controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha61290051FaceplateSerdesControllerInternal.h"
#include "Tha61290051Physical.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods               m_AtSerdesControllerOverride;
static tTha6029FaceplateSerdesControllerMethods m_Tha6029FaceplateSerdesControllerOverride;

static const tTha6029FaceplateSerdesControllerMethods *m_Tha6029FaceplateSerdesControllerMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtSerdesMode DefaultMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeEth1G;
    }

static eAtModulePrbsRet PrbsHwSerdesModeSet(Tha6029FaceplateSerdesController self, uint32 mode)
    {
    eAtRet ret = m_Tha6029FaceplateSerdesControllerMethods->PrbsHwSerdesModeSet(self, mode);
    AtEthPort ethPort = NULL;

    if (ret != cAtOk)
        return ret;

    ethPort = (AtEthPort)AtSerdesControllerPhysicalPortGet((AtSerdesController)self);
    if (ethPort)
        {
        AtDevice device = AtChannelDeviceGet((AtChannel)ethPort);
        if (!AtDeviceDiagnosticModeIsEnabled(device))
            {
            AtPrbsEngine prbs = AtChannelPrbsEngineGet((AtChannel)ethPort);
            return AtPrbsEngineInit(prbs);
            }
        }
    return cAtOk;
    }

static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    uint32 portId = AtSerdesControllerHwIdGet(self);

    if ((mode == cAtSerdesModeEth10G))
        {
        if ((portId == 0) || (portId == 8))
            return cAtTrue;

        return cAtFalse;
        }

    if ((mode == cAtSerdesModeEth1G || mode == cAtSerdesModeEth100M))
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, DefaultMode);
        mMethodOverride(m_AtSerdesControllerOverride, ModeIsSupported);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideTha6029FaceplateSerdesController(AtSerdesController self)
    {
    Tha6029FaceplateSerdesController controller = (Tha6029FaceplateSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6029FaceplateSerdesControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029FaceplateSerdesControllerOverride, m_Tha6029FaceplateSerdesControllerMethods, sizeof(m_Tha6029FaceplateSerdesControllerOverride));

        mMethodOverride(m_Tha6029FaceplateSerdesControllerOverride, PrbsHwSerdesModeSet);
        }

    mMethodsSet(controller, &m_Tha6029FaceplateSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideTha6029FaceplateSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61290051FaceplateSerdesController);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290022FaceplateSerdesControllerObjectInit(self, manager, physicalPort, serdesId) == NULL)
       return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha61290051FaceplateSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, manager, physicalPort, serdesId);
    }

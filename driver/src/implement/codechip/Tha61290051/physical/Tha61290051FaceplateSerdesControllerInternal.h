/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha61290051FaceplateSerdesControllerInternal.h
 * 
 * Created Date: Aug 20, 2018
 *
 * Description : 6129051 faceplate serdes controller internal data.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA61290051FACEPLATESERDESCONTROLLERINTERNAL_H_
#define _THA61290051FACEPLATESERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/physical/Tha60290022FaceplateSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha61290051FaceplateSerdesController
    {
    tTha60290022FaceplateSerdesController super;
    }tTha61290051FaceplateSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA61290051FACEPLATESERDESCONTROLLERINTERNAL_H_ */


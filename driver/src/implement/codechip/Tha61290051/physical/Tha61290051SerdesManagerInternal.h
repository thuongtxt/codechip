/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha61290051SerdesManagerInternal.h
 * 
 * Created Date: Aug 20, 2018
 *
 * Description : Tha61290051 Physical serdes manager implementation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA61290051SERDESMANAGERINTERNAL_H_
#define _THA61290051SERDESMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290022/physical/Tha60290022SerdesManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha61290051SerdesManager
    {
    tTha60290022SerdesManager super;
    }tTha61290051SerdesManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA61290051SERDESMANAGERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : physical
 * 
 * File        : Tha61290051MateSerdesControllerInternal.h
 * 
 * Created Date: Sep 20, 2018
 *
 * Description : 61290051 mate serdes internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA61290051MATESERDESCONTROLLERINTERNAL_H_
#define _THA61290051MATESERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/physical/Tha6029MateSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha61290051MateSerdesController
    {
    tTha6029MateSerdesController super;

    }tTha61290051MateSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA61290051MATESERDESCONTROLLERINTERNAL_H_ */


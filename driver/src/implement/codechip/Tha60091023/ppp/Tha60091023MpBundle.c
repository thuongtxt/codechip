/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60091023MpBundle.c
 *
 * Created Date: May 5, 2014
 *
 * Description : Bundle
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ppp/ThaStmMpBundleInternal.h"
#include "../man/Tha60091023Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023MpBundle
    {
    tThaMpBundle super;
    }tTha60091023MpBundle;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaMpBundleMethods m_ThaMpBundleOverride;

/* Save super implement */
static const tThaMpBundleMethods *m_ThaMpBundleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 MaxNumQueues(ThaMpBundle self)
    {
	AtUnused(self);
    return 8;
    }

static uint32 NewCounterAddress(ThaMpBundle self, uint32 regAddress)
    {
    uint32 noChangedField = cBit31_12 & regAddress;
    uint32 changedField   = regAddress & cBit11_0;
    uint32 newAddress     = noChangedField | (changedField << 1);
    return AtChannelIdGet((AtChannel)self) + newAddress + ThaMpBundlePartOffset(self);
    }

static uint32 CounterAddress(ThaMpBundle self, uint32 regAddress)
    {
    if (Tha60091023Device24MpBundlesSupported(AtChannelDeviceGet((AtChannel)self)))
        return NewCounterAddress(self, regAddress);

    return m_ThaMpBundleMethods->CounterAddress(self, regAddress);
    }

static void OverrideThaMpBundle(AtMpBundle self)
    {
    ThaMpBundle mpBundle = (ThaMpBundle)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaMpBundleMethods = mMethodsGet(mpBundle);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaMpBundleOverride, m_ThaMpBundleMethods, sizeof(m_ThaMpBundleOverride));

        mMethodOverride(m_ThaMpBundleOverride, MaxNumQueues);
        mMethodOverride(m_ThaMpBundleOverride, CounterAddress);
        }

    mMethodsSet(mpBundle, &m_ThaMpBundleOverride);
    }

static void Override(AtMpBundle self)
    {
    OverrideThaMpBundle(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023MpBundle);
    }

static AtMpBundle ObjectInit(AtMpBundle self, uint32 channelId, AtModulePpp module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaMpBundleObjectInit(self, channelId, (AtModule)module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtMpBundle Tha60091023MpBundleNew(uint32 channelId, AtModulePpp module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtMpBundle newBundle = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBundle == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newBundle, channelId, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPEG
 * 
 * File        : Tha60091023MpegReg.h
 * 
 * Created Date: Dec 30, 2014
 *
 * Description : MPEG registers of product 60091023
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60091023MPEGREG_H_
#define _THA60091023MPEGREG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ppp/ThaMpegReg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#undef  cThaMPEGPppLinkCtrl1BundleIDMask
#undef  cThaMPEGPppLinkCtrl1BundleIDShift
#define cThaMPEGPppLinkCtrl1BundleIDMask   cBit5_1
#define cThaMPEGPppLinkCtrl1BundleIDShift  1

#ifdef __cplusplus
}
#endif
#endif /* _THA60091023MPEGREG_H_ */

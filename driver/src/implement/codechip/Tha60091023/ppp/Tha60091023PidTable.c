/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60091023PidTable.c
 *
 * Created Date: Dec 5, 2013
 *
 * Description : PPP Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60031032/ppp/Tha60031032PidTableInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023PidTable
    {
    tTha60031032PidTable super;
    }tTha60091023PidTable;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tThaPidTableMethods  m_ThaPidTableOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PartIsUnused(ThaPidTable self, uint8 partId)
    {
    const uint8 cPppPartId = 1;
	AtUnused(self);
    return (partId != cPppPartId) ? cAtTrue : cAtFalse;
    }

static void OverrideThaPidTable(AtPidTable self)
    {
    ThaPidTable pidTable = (ThaPidTable)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPidTableOverride, mMethodsGet(pidTable), sizeof(m_ThaPidTableOverride));

        mMethodOverride(m_ThaPidTableOverride, PartIsUnused);
        }

    mMethodsSet(pidTable, &m_ThaPidTableOverride);
    }

static void Override(AtPidTable self)
    {
    OverrideThaPidTable(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023PidTable);
    }

static AtPidTable ObjectInit(AtPidTable self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032PidTableObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPidTable Tha60091023PidTableNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPidTable table = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (table == NULL)
        return NULL;

    return ObjectInit(table, module);
    }

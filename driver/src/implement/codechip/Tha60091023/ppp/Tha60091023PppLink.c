/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60091023PppLink.c
 *
 * Created Date: Dec 30, 2014
 *
 * Description : Ppp Link of product 60091023
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60091023MpigReg.h"
#include "Tha60091023MpegReg.h"

#include "../../../default/ppp/ThaPppLinkInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/util/ThaUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023PppLink
    {
    tThaPppLink super;
    }tTha60091023PppLink;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHdlcLinkMethods  m_AtHdlcLinkOverride;

/*--------------------------- Forward declarations ---------------------------*/
eAtRet Tha60091023HdlcLinkPidBypass(AtHdlcLink self, eBool pidByPass);

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PidByPass(AtHdlcLink self, eBool pidByPass)
    {
    return Tha60091023HdlcLinkPidBypass(self, pidByPass);
    }

static void OverrideAtHdlcLink(AtPppLink self)
    {
    AtHdlcLink hdlcLink = (AtHdlcLink)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcLinkOverride, mMethodsGet(hdlcLink), sizeof(m_AtHdlcLinkOverride));

        mMethodOverride(m_AtHdlcLinkOverride, PidByPass);
        }

    mMethodsSet(hdlcLink, &m_AtHdlcLinkOverride);
    }

static void Override(AtPppLink self)
    {
    OverrideAtHdlcLink(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023PppLink);
    }

static AtPppLink ObjectInit(AtPppLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPppLinkObjectInit(self, hdlcChannel) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPppLink Tha60091023PppLinkNew(AtHdlcChannel hdlcChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLink, hdlcChannel);
    }

eAtRet Tha60091023HdlcLinkPidBypass(AtHdlcLink self, eBool pidByPass)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr;
    AtDevice device = AtChannelDeviceGet((AtChannel)AtHdlcLinkHdlcChannelGet(self));
    ThaModuleEncap encapModule = (ThaModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);

    regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    mChannelHwLongRead(self, regAddr, longRegVal, cThaNumDwordsInLongReg, cThaModuleMpig);
    mFieldIns(&(longRegVal[cThaPidModeDwordIndex]),
              mLinkMask(encapModule, PidMode),
              mLinkShift(encapModule, PidMode),
              mBoolToBin(pidByPass));
    mChannelHwLongWrite(self, regAddr, longRegVal, cThaNumDwordsInLongReg, cThaModuleMpig);

    return cAtOk;
    }

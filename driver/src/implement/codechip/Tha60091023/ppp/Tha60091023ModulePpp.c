/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60091023ModulePpp.c
 *
 * Created Date: Dec 5, 2013
 *
 * Description : PPP Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../ThaPdhMlppp/ppp/ThaPdhMlpppModulePppInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023ModulePpp
    {
    tThaPdhMlpppModulePpp super;
    }tTha60091023ModulePpp;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePppMethods  m_AtModulePppOverride;
static tThaModulePppMethods m_ThaModulePppOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPidTable PidTableObjectCreate(AtModulePpp self)
    {
    return Tha60091023PidTableNew((AtModule)self);
    }

static AtMpBundle MpBundleObjectCreate(AtModulePpp self, uint32 bundleId)
    {
    return Tha60091023MpBundleNew(bundleId, self);
    }

static uint8 MaxNumQueuesPerLink(ThaModulePpp self)
    {
	AtUnused(self);
    return 8;
    }

static uint32 MaxNumOfMpBundleGet(AtModulePpp self)
    {
	AtUnused(self);
    return 24;
    }

static void OverrideAtModulePpp(AtModulePpp self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePppOverride, mMethodsGet(self), sizeof(m_AtModulePppOverride));

        mMethodOverride(m_AtModulePppOverride, PidTableObjectCreate);
        mMethodOverride(m_AtModulePppOverride, MpBundleObjectCreate);
        mMethodOverride(m_AtModulePppOverride, MaxNumOfMpBundleGet);
        }

    mMethodsSet(self, &m_AtModulePppOverride);
    }

static void OverrideThaModulePpp(AtModulePpp self)
    {
    ThaModulePpp module = (ThaModulePpp)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePppOverride, mMethodsGet(module), sizeof(m_ThaModulePppOverride));
        mMethodOverride(m_ThaModulePppOverride, MaxNumQueuesPerLink);
        }

    mMethodsSet(module, &m_ThaModulePppOverride);
    }

static void Override(AtModulePpp self)
    {
    OverrideAtModulePpp(self);
    OverrideThaModulePpp(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023ModulePpp);
    }

static AtModulePpp ObjectInit(AtModulePpp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhMlpppModulePppObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePpp Tha60091023ModulePppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePpp newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

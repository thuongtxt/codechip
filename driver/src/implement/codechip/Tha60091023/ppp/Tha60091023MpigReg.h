/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPIG
 * 
 * File        : Tha60091023MpigReg.h
 * 
 * Created Date: Dec 30, 2014
 *
 * Description : MPIG registers of product 60091023
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60091023MPIGREG_H_
#define _THA60091023MPIGREG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ppp/ThaMpigReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Address 851000 */
/*--------------------------------------
BitField Name: PassMLPHdrField[32]
BitField Type: R/W
BitField Desc: By pass MLPPP header field
--------------------------------------*/
#undef  cThaMlppHdrModeMask
#undef  cThaMlppHdrModeShift
#undef  cThaMlppHdrModeDwordIndex
#define cThaMlppHdrModeMask                            cBit1
#define cThaMlppHdrModeShift                           1
#define cThaMlppHdrModeDwordIndex                      1

/*--------------------------------------
BitField Name: PassPPPPidField[29]
BitField Type: R/W
BitField Desc: By pass PPP protocol  field
--------------------------------------*/
#undef  cThaPidModeMask
#undef  cThaPidModeShift
#define cThaPidModeMask                                cBit0
#define cThaPidModeShift                               0
#define cThaPidModeDwordIndex                          1

/*--------------------------------------
BitField Name: PassAdrCtlField[28]
BitField Type: R/W
BitField Desc: By pass Address Control field
--------------------------------------*/
#undef  cThaAddrCtrlModeMask
#undef  cThaAddrCtrlModeShift
#define cThaAddrCtrlModeMask                           cBit31
#define cThaAddrCtrlModeShift                          31

/*--------------------------------------
BitField Name: AdrComp[0]
BitField Type: R/W
BitField Desc: Address field compress mode
--------------------------------------*/
#undef  cThaAdrCompMask
#undef  cThaAdrCompShift
#define cThaAdrCompMask                                cBit30
#define cThaAdrCompShift                               30

/*--------------------------------------
BitField Name: BlkAloc[3:0]
BitField Type: R/W
BitField Desc: The number of block 256 segment 32-bytes is allocated for per
               link. Each block correspond with bandwidth of 2 DS0.
--------------------------------------*/
#undef  cThaBlkAlocMask
#undef  cThaBlkAlocShift
#define cThaBlkAlocMask                                cBit29_26
#define cThaBlkAlocShift                               26

/*--------------------------------------
BitField Name: FcsErrDropEn[22]
BitField Type: R/W
BitField Desc: FCS Error drop packet enable
--------------------------------------*/
#undef  cThaFcsErrDropEnMask
#undef  cThaFcsErrDropEnShift
#define cThaFcsErrDropEnMask                           cBit25
#define cThaFcsErrDropEnShift                          25

/*--------------------------------------
BitField Name: AdrCtlErrDropEn[21]
BitField Type: R/W
BitField Desc: Address Control Error drop packet enable
--------------------------------------*/
#undef  cThaAdrCtlErrDropEnMask
#undef  cThaAdrCtlErrDropEnShift
#define cThaAdrCtlErrDropEnMask                        cBit24
#define cThaAdrCtlErrDropEnShift                       24

/*--------------------------------------
BitField Name: NCPRule[1:0]
BitField Type: R/W
BitField Desc: The rule of NCP packet
               - 0: Forward to PSN
               - 1: Forward to CPU Queue #1
               - 2: Forward to CPU Queue #2
               - 3: Discard
--------------------------------------*/
#undef  cThaNCPRuleMask
#undef  cThaNCPRuleShift
#define cThaNCPRuleMask                                cBit23_20
#define cThaNCPRuleShift                               20

/*--------------------------------------
BitField Name: LCPRule[1:0]
BitField Type: R/W
BitField Desc: The rule of LCP packet
               - 0: Forward to PSN
               - 1: Forward to CPU Queue #1
               - 2: Forward to CPU Queue #2
               - 3: Discard
--------------------------------------*/
#undef  cThaLCPRuleMask
#undef  cThaLCPRuleShift
#define cThaLCPRuleMask                                cBit19_16
#define cThaLCPRuleShift                               16

/*--------------------------------------
BitField Name: RSQRule[1:0]
BitField Type: R/W
BitField Desc: The rule of packet which need re-sequence, such as MLPPP packet
               - 0: Forward to PSN
               - 1: Forward to CPU Queue #1
               - 2: Forward to CPU Queue #2
               - 3: Discard
--------------------------------------*/
#undef  cThaMlpRuleRuleMask
#undef  cThaMlpRuleRuleShift
#define cThaMlpRuleRuleMask                            cBit15_12
#define cThaMlpRuleRuleShift                           12

/*--------------------------------------
BitField Name: PppRule[10:7]
BitField Type: R/W
BitField Desc: The rule of packet which don't need re-sequence, such as
               HDLC/PPP PW packet or BCP/IP  non MLPPP packet
               - 0: Forward to PSN
               - 1: Forward to CPU Queue #1
               - 2: Forward to CPU Queue #2
               - 3: Discard
--------------------------------------*/
#undef  cThaPppRuleMask
#undef  cThaPppRuleShift
#define cThaPppRuleMask                                cBit11_8
#define cThaPppRuleShift                               8

/*--------------------------------------
BitField Name: PKtMode[6:4]
BitField Type: R/W
BitField Desc: Packet Encapsulation mode
    0: HDLC PW
    1: HDLC Ter (CISCO-HDLC), Base on PPP-PID to classify to packet type
    2: PPP PW
    3: PPP Ter (Termination). Base on PPP-PID to classify to packet type
    4: FR PW
    5: FR Ter
    6-7: Unused
--------------------------------------*/
#undef  cThaPKtModeMask
#undef  cThaPKtModeShift
#define cThaPKtModeMask                                cBit7_5
#define cThaPKtModeShift                               5

/*--------------------------------------
BitField Name: BndID[3:0]
BitField Type: R/W
BitField Desc: Bundle ID. Look up from Link ID to Bundle ID
--------------------------------------*/
#undef  cThaBndIDMask
#undef  cThaBndIDShift
#define cThaBndIDMask                                  cBit4_0
#define cThaBndIDShift                                 0

#endif /* _THA60091023MPIGREG_H_ */


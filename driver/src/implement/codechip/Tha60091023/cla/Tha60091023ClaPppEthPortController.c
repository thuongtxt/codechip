/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60091023ClaPppEthPortController.c
 *
 * Created Date: Dec 4, 2013
 *
 * Description : PPP ETH Port controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/controllers/ThaClaEthPortControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023ClaPppEthPortController
    {
    tThaClaPppEthPortController super;
    }tTha60091023ClaPppEthPortController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaEthPortControllerMethods m_ThaClaEthPortControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023ClaPppEthPortController);
    }

static uint32 EthPortPartOffset(ThaClaEthPortController self, AtEthPort port)
    {
	AtUnused(port);
	AtUnused(self);
    return cBit24;
    }

static void OverrideThaClaEthPortController(ThaClaEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthPortControllerOverride));

        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPortPartOffset);
        }

    mMethodsSet(self, &m_ThaClaEthPortControllerOverride);
    }

static void Override(ThaClaEthPortController self)
    {
    OverrideThaClaEthPortController(self);
    }

static ThaClaEthPortController ObjectInit(ThaClaEthPortController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaPppEthPortControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthPortController Tha60091023ClaPppEthPortControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cla);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60091023ClaEthPortController.c
 *
 * Created Date: Nov 29, 2013
 *
 * Description : ETH Port controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/controllers/ThaClaPppPwEthPortControllerInternal.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023ClaEthPortController
    {
    tThaClaPppPwEthPortController super;
    }tTha60091023ClaEthPortController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaEthPortControllerMethods        m_ThaClaEthPortControllerOverride;
static tThaClaPppPwEthPortControllerMethods   m_ThaClaPppPwEthPortControllerOverride;

/* Save super implementation */
static const tThaClaEthPortControllerMethods *m_ThaClaEthPortControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023ClaEthPortController);
    }

static ThaClaEthPortController PppEthPortControllerCreate(ThaClaPppPwEthPortController self)
    {
    return Tha60091023ClaPppEthPortControllerNew(ThaClaControllerModuleGet((ThaClaController)self));
    }

static ThaClaEthPortController PwEthPortControllerCreate(ThaClaPppPwEthPortController self)
    {
    return Tha60091023ClaPwEthPortControllerNew(ThaClaControllerModuleGet((ThaClaController)self));
    }

static eBool CounterCanReadByPppController(eThaClaEthPortCounterType counterType)
    {
	AtUnused(counterType);
    return cAtFalse;
    }

static uint32 CounterPartOffset(ThaClaEthPortController self, AtEthPort port, eThaClaEthPortCounterType counterType)
    {
    /*if (CounterCanReadByPppController(counterType))
        return cBit24;*/
    return m_ThaClaEthPortControllerMethods->CounterPartOffset(self, port, counterType);
    }

static ThaClaEthPortController ControllerOfCounter(ThaClaPppPwEthPortController self, AtEthPort port, eThaClaEthPortCounterType counterType)
    {
	AtUnused(port);
    if (CounterCanReadByPppController(counterType))
        return ThaClaPppPwEthPortControllerPppEthPortController(self);
    else
        return ThaClaPppPwEthPortControllerPwEthPortController(self);
    }

static uint16 SVlanTpid(ThaClaEthPortController self, AtEthPort port)
    {
	AtUnused(port);
	AtUnused(self);
    return 0x8100;
    }

static eBool CanChangeMaxPacketSize(ThaClaEthPortController self)
    {
    uint32 startSupportedVersion = ThaVersionReaderVersionBuild(0x1, 0x3, 0x7);
    AtDevice device = AtModuleDeviceGet((AtModule)ThaClaControllerModuleGet((ThaClaController)self));
    return (AtDeviceVersionNumber(device) >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static eAtRet MaxPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 maxPacketSize)
    {
    ThaClaEthPortController pppEthPortController;

    if (!CanChangeMaxPacketSize(self))
        return m_ThaClaEthPortControllerMethods->MaxPacketSizeSet(self, port, maxPacketSize);

    pppEthPortController = ThaClaPppPwEthPortControllerPppEthPortController((ThaClaPppPwEthPortController)self);
    return mMethodsGet(pppEthPortController)->MaxPacketSizeSet(pppEthPortController, port, maxPacketSize);
    }

static eAtRet MaxPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
    ThaClaEthPortController pppEthPortController;

    if (!CanChangeMaxPacketSize(self))
        return m_ThaClaEthPortControllerMethods->MaxPacketSizeGet(self, port);

    pppEthPortController = ThaClaPppPwEthPortControllerPppEthPortController((ThaClaPppPwEthPortController)self);
    return mMethodsGet(pppEthPortController)->MaxPacketSizeGet(pppEthPortController, port);
    }

static eBool NeedMaxPacketSizeDefaultSetup(ThaClaEthPortController self)
    {
    return CanChangeMaxPacketSize(self);
    }

static void OverrideThaClaEthPortController(ThaClaEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaEthPortControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthPortControllerOverride, m_ThaClaEthPortControllerMethods, sizeof(m_ThaClaEthPortControllerOverride));
        mMethodOverride(m_ThaClaEthPortControllerOverride, CounterPartOffset);
        mMethodOverride(m_ThaClaEthPortControllerOverride, SVlanTpid);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MaxPacketSizeSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, NeedMaxPacketSizeDefaultSetup);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MaxPacketSizeGet);
        }

    mMethodsSet(self, &m_ThaClaEthPortControllerOverride);
    }

static void OverrideThaClaPppPwEthPortController(ThaClaEthPortController self)
    {
    ThaClaPppPwEthPortController controller = (ThaClaPppPwEthPortController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPppPwEthPortControllerOverride, mMethodsGet(controller), sizeof(m_ThaClaPppPwEthPortControllerOverride));

        mMethodOverride(m_ThaClaPppPwEthPortControllerOverride, PppEthPortControllerCreate);
        mMethodOverride(m_ThaClaPppPwEthPortControllerOverride, PwEthPortControllerCreate);
        mMethodOverride(m_ThaClaPppPwEthPortControllerOverride, ControllerOfCounter);
        }

    mMethodsSet(controller, &m_ThaClaPppPwEthPortControllerOverride);
    }

static void Override(ThaClaEthPortController self)
    {
    OverrideThaClaEthPortController(self);
    OverrideThaClaPppPwEthPortController(self);
    }

static ThaClaEthPortController ObjectInit(ThaClaEthPortController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaPppPwEthPortControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthPortController Tha60091023ClaEthPortControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cla);
    }


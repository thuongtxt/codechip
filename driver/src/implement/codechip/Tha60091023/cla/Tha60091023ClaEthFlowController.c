/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60091023ClaEthFlowController.c
 *
 * Created Date: Dec 2, 2013
 *
 * Description : CLA ETH Flow controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cla/controllers/ThaClaEthFlowControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023ClaEthFlowController
    {
    tThaClaEthFlowController super;
    }tTha60091023ClaEthFlowController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaControllerMethods        m_ThaClaControllerOverride;
static tThaClaEthFlowControllerMethods m_ThaClaEthFlowControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/
extern uint8 Tha6003PppProductQueueHwPriority(ThaClaEthFlowController self, uint8 queueId);

/*--------------------------- Implementation ---------------------------------*/
static eBool PartIsUnused(ThaClaController self, uint8 partId)
    {
    const uint8 cPppPartId = 1;
	AtUnused(self);
    return (partId != cPppPartId) ? cAtTrue : cAtFalse;
    }

static uint8 FcsRemoved(ThaClaEthFlowController self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 PartOffset(ThaClaEthFlowController self, ThaEthFlow flow)
    {
	AtUnused(flow);
	AtUnused(self);
    return cBit24;
    }

static uint16 VlanLookupOffset(ThaClaEthFlowController self, uint16 lkType)
    {
	AtUnused(self);
    return (lkType == 0) ? 0x800 : 0x0;
    }

static uint8 QueueHwPriority(ThaClaEthFlowController self, uint8 queueId)
    {
    return Tha6003PppProductQueueHwPriority(self, queueId);
    }

static eBool NeedInitializeQueuePriority(ThaClaEthFlowController self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaClaController(ThaClaEthFlowController self)
    {
    ThaClaController controller = (ThaClaController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, mMethodsGet(controller), sizeof(m_ThaClaControllerOverride));

        mMethodOverride(m_ThaClaControllerOverride, PartIsUnused);
        }

    mMethodsSet(controller, &m_ThaClaControllerOverride);
    }

static void OverrideThaClaEthFlowController(ThaClaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthFlowControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthFlowControllerOverride));

        mMethodOverride(m_ThaClaEthFlowControllerOverride, FcsRemoved);
        mMethodOverride(m_ThaClaEthFlowControllerOverride, PartOffset);
        mMethodOverride(m_ThaClaEthFlowControllerOverride, VlanLookupOffset);
        mMethodOverride(m_ThaClaEthFlowControllerOverride, QueueHwPriority);
        mMethodOverride(m_ThaClaEthFlowControllerOverride, NeedInitializeQueuePriority);
        }

    mMethodsSet(self, &m_ThaClaEthFlowControllerOverride);
    }

static void Override(ThaClaEthFlowController self)
    {
    OverrideThaClaController(self);
    OverrideThaClaEthFlowController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023ClaEthFlowController);
    }

static ThaClaEthFlowController ObjectInit(ThaClaEthFlowController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaEthFlowControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthFlowController Tha60091023ClaEthFlowControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthFlowController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cla);
    }

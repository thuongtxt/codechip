/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60091023ModuleCdr.c
 *
 * Created Date: May 6, 2014
 *
 * Description : CDR Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaPdhPwProduct/cdr/ThaPdhPwProductModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023ModuleCdr
    {
    tThaPdhPwProductModuleCdr super;
    }tTha60091023ModuleCdr;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleCdrMethods m_ThaModuleCdrOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultExtFrequency(ThaModuleCdr self)
    {
	AtUnused(self);
    return 0x25f8;
    }

static eBool NeedToSetExtDefaultFrequency(ThaModuleCdr self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint8 HwSystemTimingMode(ThaModuleCdr self)
    {
	AtUnused(self);
    return 4;
    }

static eBool HasLiuTimingMode(ThaModuleCdr self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtTimingMode OtherTimingSourceAsSystemTiming(ThaModuleCdr self)
    {
	AtUnused(self);
    return cAtTimingModeExt1Ref;
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, mMethodsGet(cdrModule), sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, HwSystemTimingMode);
        mMethodOverride(m_ThaModuleCdrOverride, HasLiuTimingMode);
        mMethodOverride(m_ThaModuleCdrOverride, OtherTimingSourceAsSystemTiming);
        mMethodOverride(m_ThaModuleCdrOverride, DefaultExtFrequency);
        mMethodOverride(m_ThaModuleCdrOverride, NeedToSetExtDefaultFrequency);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023ModuleCdr);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60091023ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

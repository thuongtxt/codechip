/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : Tha60091023ClockMonitor.c
 *
 * Created Date: May 6, 2014
 *
 * Description : Clock Monitor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../default/clock/ThaModuleClock.h"

#include "../../default/ThaStmPwProduct/clock/ThaStmPwProductClockMonitor.h"
#include "../../default/ThaStmPwProduct/clock/ThaStmPwProductModuleClock.h"
#include "Tha60091023ClockMonitor.h"

/*--------------------------- Define -----------------------------------------*/
#define cClockMonitoring_1 0xF0006D
#define cClockMonitoring_2 0xF0006E

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023ClockMonitor
    {
    tThaStmPwProductClockMonitor super;
    }tTha60091023ClockMonitor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClockMonitorMethods m_ThaClockMonitorOverride;

/* Save super implementation */
static const tThaClockMonitorMethods *m_ThaClockMonitorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ReferenceClock1FrequencyInKhzGet(ThaClockMonitor self)
    {
    uint32 regVal  = mModuleHwRead(self->clockModule, cClockMonitoring_1);
    uint32 frequency;

    mFieldGet(regVal, cBit15_0, 0, uint32, &frequency);
    return ThaClockMonitorKhzCalculation(frequency);
    }

static uint32 ReferenceClock2FrequencyInKhzGet(ThaClockMonitor self)
    {
    uint32 frequency;
    uint32 regVal  = mModuleHwRead(self->clockModule, cClockMonitoring_2);;

    mFieldGet(regVal, cBit31_16, 16, uint32, &frequency);
    return ThaClockMonitorKhzCalculation(frequency);
    }

static uint32 _ReferenceClock1FrequencyInKhzGet(ThaClockMonitor self, void *param)
    {
	AtUnused(param);
    return ReferenceClock1FrequencyInKhzGet(self);
    }

static uint32 _ReferenceClock2FrequencyInKhzGet(ThaClockMonitor self, void *param)
    {
	AtUnused(param);
    return ReferenceClock2FrequencyInKhzGet(self);
    }

static uint32 *_ReferenceClockExpectedFrequenciesInKhz(ThaClockMonitor self, void *param, uint8 *numFrequencies)
    {
	AtUnused(param);
    return ThaClockMonitorSystemClockExpectedFrequenciesInKhz(self, numFrequencies);
    }

static void ReferenceClock1StatusShow(ThaClockMonitor self)
    {
    uint8 numExpectedFrequencies;
    uint32 *expectedFrequencies = mMethodsGet(self)->SystemClockExpectedFrequenciesInKhz(self, &numExpectedFrequencies);

    AtPrintc(cSevNormal, "* Reference#1: ");
    ThaClockMonitorClockStatusShow(expectedFrequencies, numExpectedFrequencies, ReferenceClock1FrequencyInKhzGet(self));
    AtPrintc(cSevNormal, "\r\n");
    }

static void ReferenceClock2StatusShow(ThaClockMonitor self)
    {
    uint8 numExpectedFrequencies;
    uint32 *expectedFrequencies = mMethodsGet(self)->SystemClockExpectedFrequenciesInKhz(self, &numExpectedFrequencies);

    AtPrintc(cSevNormal, "* Reference#2: ");
    ThaClockMonitorClockStatusShow(expectedFrequencies, numExpectedFrequencies, ReferenceClock2FrequencyInKhzGet(self));
    AtPrintc(cSevNormal, "\r\n");
    }

static void Debug(ThaClockMonitor self)
    {
    m_ThaClockMonitorMethods->Debug(self);
    ReferenceClock1StatusShow(self);
    ReferenceClock2StatusShow(self);
    }

static void OverrideThaClockMonitor(ThaClockMonitor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClockMonitorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockMonitorOverride, m_ThaClockMonitorMethods, sizeof(m_ThaClockMonitorOverride));

        mMethodOverride(m_ThaClockMonitorOverride, Debug);
        }

    mMethodsSet(self, &m_ThaClockMonitorOverride);
    }

static void Override(ThaClockMonitor self)
    {
    OverrideThaClockMonitor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023ClockMonitor);
    }

static ThaClockMonitor ObjectInit(ThaClockMonitor self, AtModuleClock clockModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductClockMonitorObjectInit(self, clockModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClockMonitor Tha60091023ClockMonitorNew(AtModuleClock clockModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClockMonitor newMonitor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newMonitor, clockModule);
    }

uint32 Tha60091023ClockMonitorReferenceClock1Check(ThaClockMonitor self)
    {
    if (ThaClockMonitorClockCheck(self, _ReferenceClock1FrequencyInKhzGet, _ReferenceClockExpectedFrequenciesInKhz, NULL) == cAtOk)
        return 0;

    return ThaStmPwProductClockStatusConvert(cThaClockStatusReferenceClockFail(0));
    }

uint32 Tha60091023ClockMonitorReferenceClock2Check(ThaClockMonitor self)
    {
    if (ThaClockMonitorClockCheck(self, _ReferenceClock2FrequencyInKhzGet, _ReferenceClockExpectedFrequenciesInKhz, NULL) == cAtOk)
        return 0;

    return ThaStmPwProductClockStatusConvert(cThaClockStatusReferenceClockFail(1));
    }

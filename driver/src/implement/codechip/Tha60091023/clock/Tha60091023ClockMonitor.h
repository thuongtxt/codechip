/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60091023ClockMonitor.h
 * 
 * Created Date: Mar 2, 2015
 *
 * Description : Clock monitor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60091023CLOCKMONITOR_H_
#define _THA60091023CLOCKMONITOR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60091023ClockMonitorReferenceClock1Check(ThaClockMonitor self);
uint32 Tha60091023ClockMonitorReferenceClock2Check(ThaClockMonitor self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60091023CLOCKMONITOR_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60091023ModuleEth.c
 *
 * Created Date: Aug 29, 2013
 *
 * Description : ETH Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthPort.h"
#include "../../default/ThaPdhPwProduct/eth/ThaPdhPwProductModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023ModuleEth
    {
    tThaPdhPwProductModuleEth super;
    }tTha60091023ModuleEth;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleEthMethods  m_ThaModuleEthOverride;
static tAtModuleEthMethods   m_AtModuleEthOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023ModuleEth);
    }

static uint32 MaxNumMpFlows(ThaModuleEth self)
    {
	AtUnused(self);
    return 256;
    }

static uint16 MaxFlowsGet(AtModuleEth self)
    {
    return ThaModuleEthMaxFlowsGet((ThaModuleEth)self);
    }

static AtEthFlow FlowCreate(AtModuleEth self, uint16 flowId)
    {
    return ThaModuleEthPppFlowCreate(self, flowId);
    }

static ThaEthFlowHeaderProvider FlowHeaderProviderCreate(ThaModuleEth self)
    {
    return Tha60091023EthFlowHeaderProviderNew((AtModuleEth)self);
    }

static eBool UsePortSourceMacForAllFlows(ThaModuleEth self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    return Tha60091023EthPortNew(portId, self);
    }

static ThaEthFlowControllerFactory FlowControllerFactory(ThaModuleEth self)
    {
    AtUnused(self);
    return ThaEthFlowControllerFactoryDefault();
    }

static ThaOamFlowManager OamFlowManagerObjectCreate(ThaModuleEth self)
    {
    return ThaOamFlowManagerDefaultNew((AtModuleEth)self);
    }

static void OverrideThaModuleEth(ThaModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(self), sizeof(m_ThaModuleEthOverride));
        mMethodOverride(m_ThaModuleEthOverride, MaxNumMpFlows);
        mMethodOverride(m_ThaModuleEthOverride, FlowHeaderProviderCreate);
        mMethodOverride(m_ThaModuleEthOverride, UsePortSourceMacForAllFlows);
        mMethodOverride(m_ThaModuleEthOverride, FlowControllerFactory);
        mMethodOverride(m_ThaModuleEthOverride, OamFlowManagerObjectCreate);
        }

    mMethodsSet(self, &m_ThaModuleEthOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));
        mMethodOverride(m_AtModuleEthOverride, MaxFlowsGet);
        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        mMethodOverride(m_AtModuleEthOverride, FlowCreate);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void Override(ThaModuleEth self)
    {
    OverrideThaModuleEth(self);
    OverrideAtModuleEth((AtModuleEth)self);
    }

static AtModuleEth ObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductModuleEthObjectInit(self, device) == NULL)
        return NULL;

    Override((ThaModuleEth)self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60091023ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60091023EthFlowHeaderProvider.c
 *
 * Created Date: Oct 8, 2014
 *
 * Description : Header provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../Tha60031032/eth/Tha60031032EthFlowHeaderProvider.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023EthFlowHeaderProvider
    {
    tTha60031032EthFlowHeaderProvider super;
    }tTha60091023EthFlowHeaderProvider;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60031032EthFlowHeaderProviderMethods m_Tha60031032EthFlowHeaderProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionWithNcpMlppp(Tha60031032EthFlowHeaderProvider self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(0x1, 0x3, 0x01);
    }

static void OverrideTha60031032EthFlowHeaderProvider(ThaEthFlowHeaderProvider self)
    {
    Tha60031032EthFlowHeaderProvider provider = (Tha60031032EthFlowHeaderProvider)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60031032EthFlowHeaderProviderOverride, mMethodsGet(provider), sizeof(m_Tha60031032EthFlowHeaderProviderOverride));

        mMethodOverride(m_Tha60031032EthFlowHeaderProviderOverride, StartVersionWithNcpMlppp);
        }

    mMethodsSet(provider, &m_Tha60031032EthFlowHeaderProviderOverride);
    }

static void Override(ThaEthFlowHeaderProvider self)
    {
    OverrideTha60031032EthFlowHeaderProvider(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023EthFlowHeaderProvider);
    }

static ThaEthFlowHeaderProvider ObjectInit(ThaEthFlowHeaderProvider self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60031032EthFlowHeaderProviderObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowHeaderProvider Tha60091023EthFlowHeaderProviderNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowHeaderProvider newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProvider, ethModule);
    }

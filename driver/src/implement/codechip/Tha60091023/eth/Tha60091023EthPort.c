/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60091023EthPort.c
 *
 * Created Date: Dec 5, 2013
 *
 * Description : ETH Port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthPortInternal.h"
#include "../../../default/cla/controllers/ThaClaEthPortControllerInternal.h"
#include "../../../default/cos/controllers/ThaCosControllerInternal.h"
#include "../../../default/cla/controllers/ThaClaPppPwEthPortControllerInternal.h"
#include "../../default/ThaPdhPwProduct/eth/ThaPdhPwProductModuleEth.h"
#include "../../default/ThaPdhPwProduct/eth/ThaPdhPwProductEthPort.h"
#include "../cos/Tha60091023CosController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023EthPort
    {
    tThaEthPort super;
    }tTha60091023EthPort;

typedef enum eThaCounterType
    {
    cThaCounterTypeGood,
    cThaCounterTypeError,
    cThaCounterTypeNeutral,
    cThaCounterTypeNA
    }eThaCounterType;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tThaEthPortMethods m_ThaEthPortOverride;
static tAtEthPortMethods  m_AtEthPortOverride;

/* Save super implementation */
static const tAtChannelMethods  *m_AtChannelMethods  = NULL;
static const tThaEthPortMethods *m_ThaEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtSevLevel ColorOfCounterType(eThaCounterType counterType, uint32 counterValue)
    {
    if (counterType == cThaCounterTypeGood)
        return (counterValue > 0) ? cSevInfo : cSevCritical;

    if (counterType == cThaCounterTypeError)
        return (counterValue > 0) ? cSevCritical : cSevNormal;

    if (counterType == cThaCounterTypeNeutral)
        return (counterValue > 0) ? cSevInfo : cSevNormal;

    return cSevNormal;
    }

static void CounterPrint(const char *counterTypeString, uint32 pwCounterValue, uint32 pppCounterValue, eThaCounterType counterType)
    {
    eAtSevLevel pwCounterColor;
    eAtSevLevel pppCounterColor;

    /* Not applicable counter */
    if (counterType == cThaCounterTypeNA)
        {
        AtPrintc(cSevNormal, "N/A\r\n");
        return;
        }

    pwCounterColor  = ColorOfCounterType(counterType, pwCounterValue);
    pppCounterColor = ColorOfCounterType(counterType, pppCounterValue);
    AtPrintc(cSevNormal, "%-30s:   ", counterTypeString);
    AtPrintc(pwCounterColor, "%-12u   ", pwCounterValue);
    AtPrintc(pppCounterColor, "%-12u\r\n", pppCounterValue);
    }

static void CounterGet(AtChannel self, ThaClaEthPortController claController, ThaCosEthPortController cosController, tAtEthPortCounters *counters)
    {
    AtEthPort port  = (AtEthPort)self;

    counters->txPackets                = ThaCosEthPortControllerOutcomingPktRead2Clear(cosController, port, cAtTrue);
    counters->txBytes                  = ThaCosEthPortControllerOutcomingbyteRead2Clear(cosController, port, cAtTrue);
    counters->txOamPackets             = ThaCosEthPortControllerOamTxPktRead2Clear(cosController, port, cAtTrue);
    counters->txPeriodOamPackets       = ThaCosEthPortControllerOamFrequencyTxPktRead2Clear(cosController, port, cAtTrue);
    counters->txPwPackets              = ThaCosEthPortControllerPWTxPktRead2Clear(cosController, port, cAtTrue);
    counters->txPacketsLen0_64         = ThaCosEthPortControllerPktLensmallerthan64bytesRead2Clear(cosController, port, cAtTrue);
    counters->txPacketsLen65_127       = ThaCosEthPortControllerPktLenfrom65to127bytesRead2Clear(cosController, port, cAtTrue);
    counters->txPacketsLen128_255      = ThaCosEthPortControllerPktLenfrom128to255bytesRead2Clear(cosController, port, cAtTrue);
    counters->txPacketsLen256_511      = ThaCosEthPortControllerPktLenfrom256to511bytesRead2Clear(cosController, port, cAtTrue);
    counters->txPacketsLen512_1024     = ThaCosEthPortControllerPktLenfrom512to1024bytesRead2Clear(cosController, port, cAtTrue);
    counters->txPacketsLen1025_1528    = ThaCosEthPortControllerPktLenfrom1025to1528bytesRead2Clear(cosController, port, cAtTrue);
    counters->txPacketsJumbo           = ThaCosEthPortControllerPktJumboLenRead2Clear(cosController, port, cAtTrue);
    counters->txTopPackets             = ThaCosEthPortControllerPktTimePktToPRead2Clear(cosController, port, cAtTrue);
    counters->rxPackets                = ThaClaEthPortControllerIncomPktRead2Clear(claController, port, cAtTrue);
    counters->rxBytes                  = ThaClaEthPortControllerIncombyteRead2Clear(claController, port, cAtTrue);
    counters->rxErrEthHdrPackets       = ThaClaEthPortControllerRxHdrErrPacketsRead2Clear(claController, port, cAtTrue);
    counters->rxErrBusPackets          = ThaClaEthPortControllerEthPktBusErrRead2Clear(claController, port, cAtTrue);
    counters->rxErrFcsPackets          = ThaClaEthPortControllerEthPktFcsErrRead2Clear(claController, port, cAtTrue);
    counters->rxOversizePackets        = ThaClaEthPortControllerEthPktoversizeRead2Clear(claController, port, cAtTrue);
    counters->rxUndersizePackets       = ThaClaEthPortControllerEthPktundersizeRead2Clear(claController, port, cAtTrue);
    counters->rxPacketsLen0_64         = ThaClaEthPortControllerEthPktLensmallerthan64bytesRead2Clear(claController, port, cAtTrue);
    counters->rxPacketsLen65_127       = ThaClaEthPortControllerEthPktLenfrom65to127bytesRead2Clear(claController, port, cAtTrue);
    counters->rxPacketsLen128_255      = ThaClaEthPortControllerEthPktLenfrom128to255bytesRead2Clear(claController, port, cAtTrue);
    counters->rxPacketsLen256_511      = ThaClaEthPortControllerEthPktLenfrom256to511bytesRead2Clear(claController, port, cAtTrue);
    counters->rxPacketsLen512_1024     = ThaClaEthPortControllerEthPktLenfrom512to1024bytesRead2Clear(claController, port, cAtTrue);
    counters->rxPacketsLen1025_1528    = ThaClaEthPortControllerEthPktLenfrom1025to1528bytesRead2Clear(claController, port, cAtTrue);
    counters->rxPacketsJumbo           = ThaClaEthPortControllerEthPktJumboLenRead2Clear(claController, port, cAtTrue);
    counters->rxPwUnsupportedPackets   = ThaClaEthPortControllerPWunSuppedRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxErrPwLabelPackets      = ThaClaEthPortControllerHCBElookupnotmatchPktRead2Clear(claController, port, cAtTrue);
    counters->rxDiscardedPackets       = ThaClaEthPortControllerEthPktDscdRead2Clear(claController, port, cAtTrue);
    counters->rxPausePackets           = ThaClaEthPortControllerPauFrmRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxErrPausePackets        = ThaClaEthPortControllerPauFrmErrPktRead2Clear(claController, port, cAtTrue);
    counters->rxBrdCastPackets         = ThaClaEthPortControllerBcastRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxMultCastPackets        = ThaClaEthPortControllerMcastRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxArpPackets             = ThaClaEthPortControllerARPRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxOamPackets             = ThaClaEthPortControllerEthOamRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxEfmOamPackets          = ThaClaEthPortControllerEthOamRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxErrEfmOamPackets       = ThaClaEthPortControllerEthOamRxErrPktRead2Clear(claController, port, cAtTrue);
    counters->rxEthOamType1Packets     = ThaClaEthPortControllerEthOamtype0RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxEthOamType2Packets     = ThaClaEthPortControllerEthOamtype1RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxIpv4Packets            = ThaClaEthPortControllerIPv4RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxErrIpv4Packets         = ThaClaEthPortControllerIPv4PktErrRead2Clear(claController, port, cAtTrue);
    counters->rxIcmpIpv4Packets        = ThaClaEthPortControllerICMPv4RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxIpv6Packets            = ThaClaEthPortControllerIPv6RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxErrIpv6Packets         = ThaClaEthPortControllerIPv6PktErrRead2Clear(claController, port, cAtTrue);
    counters->rxIcmpIpv6Packets        = ThaClaEthPortControllerICMPv6RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxMefPackets             = ThaClaEthPortControllerMEFRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxErrMefPackets          = ThaClaEthPortControllerMEFErrRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxMplsPackets            = ThaClaEthPortControllerMPLSRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxErrMplsPackets         = ThaClaEthPortControllerMPLSErrRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxMplsErrOuterLblPackets = ThaClaEthPortControllerMPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(claController, port, cAtTrue);
    counters->rxMplsDataPackets        = ThaClaEthPortControllerMPLSDataRxPktwithPHPMdRead2Clear(claController, port, cAtTrue);
    counters->rxMplsDataPackets       += ThaClaEthPortControllerMPLSDataRxPktwithoutPHPMdRead2Clear(claController, port, cAtTrue);
    counters->rxLdpIpv4Packets         = ThaClaEthPortControllerLDPIPv4withPHPMdRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxLdpIpv4Packets        += ThaClaEthPortControllerLDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxLdpIpv6Packets         = ThaClaEthPortControllerLDPIPv6withPHPMdRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxLdpIpv6Packets        += ThaClaEthPortControllerLDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(claController, port, cAtTrue);
    counters->rxMplsIpv4Packets        = ThaClaEthPortControllerMPLSoverIPv4RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxMplsIpv6Packets        = ThaClaEthPortControllerMPLSoverIPv6RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxErrL2tpv3Packets       = ThaClaEthPortControllerL2TPPktErrRead2Clear(claController, port, cAtTrue);
    counters->rxL2tpv3Ipv4Packets      = ThaClaEthPortControllerL2TPIPv4RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxL2tpv3Ipv6Packets      = ThaClaEthPortControllerL2TPIPv6RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxL2tpv3Packets          = ThaClaEthPortControllerL2TPIPv4RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxL2tpv3Packets         += ThaClaEthPortControllerL2TPIPv6RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxErrUdpPackets          = ThaClaEthPortControllerUDPPktErrRead2Clear(claController, port, cAtTrue);
    counters->rxUdpIpv4Packets         = ThaClaEthPortControllerUDPIPv4RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxUdpIpv6Packets         = ThaClaEthPortControllerUDPIPv6RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxUdpPackets             = ThaClaEthPortControllerUDPIPv4RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxUdpPackets            += ThaClaEthPortControllerUDPIPv6RxPktRead2Clear(claController, port, cAtTrue);
    counters->rxErrPsnPackets          = ThaClaEthPortControllerRxErrPsnPacketsRead2Clear(claController, port, cAtTrue);
    counters->rxPacketsSendToCpu       = ThaClaEthPortControllerEthPktForwardtoCpuRead2Clear(claController, port, cAtTrue);
    counters->rxPacketsSendToPw        = ThaClaEthPortControllerEthPktForwardtoPDaRead2Clear(claController, port, cAtTrue);
    counters->rxTopPackets             = ThaClaEthPortControllerEthPktTimePktToPRead2Clear(claController, port, cAtTrue);
    counters->rxPacketDaMis            = 0;
    }

static eAtRet Debug(AtChannel self)
    {
    uint8 i = 0;
    ThaCosEthPortController cosController;
    ThaClaEthPortController claController;
    tAtEthPortCounters pwCounters;
    tAtEthPortCounters pppCounters;
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleCos cosModule = (ThaModuleCos)AtDeviceModuleGet(device, cThaModuleCos);
    ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    ThaCosEthPortController      cosPortController = ThaModuleCosEthPortControllerGet(cosModule);
    ThaClaPppPwEthPortController claPortController = (ThaClaPppPwEthPortController)ThaModuleClaEthPortControllerGet(claModule);
    const char *counterStrings[] = {"txPackets", "txBytes", "txOamPackets", "txPeriodOamPackets",
                                    "txPwPackets", "txPacketsLen0_64", "txPacketsLen65_127", "txPacketsLen128_255",
                                    "txPacketsLen256_511", "txPacketsLen512_1024", "txPacketsLen1025_1528",
                                    "txPacketsJumbo", "txTopPackets", "rxPackets", "rxBytes", "rxErrEthHdrPackets",
                                    "rxErrBusPackets", "rxErrFcsPackets", "rxOversizePackets", "rxUndersizePackets",
                                    "rxPacketsLen0_64", "rxPacketsLen65_127", "rxPacketsLen128_255","rxPacketsLen256_511",
                                    "rxPacketsLen512_1024", "rxPacketsLen1025_1528", "rxPacketsJumbo", "rxPwUnsupportedPackets",
                                    "rxErrPwLabelPackets", "rxDiscardedPackets", "rxPausePackets", "rxErrPausePackets",
                                    "rxBrdCastPackets", "rxMultCastPackets", "rxArpPackets", "rxOamPackets",
                                    "rxEfmOamPackets", "rxErrEfmOamPackets", "rxEthOamType1Packets", "rxEthOamType2Packets",
                                    "rxIpv4Packets", "rxErrIpv4Packets", "rxIcmpIpv4Packets", "rxIpv6Packets",
                                    "rxErrIpv6Packets", "rxIcmpIpv6Packets", "rxMefPackets", "rxErrMefPackets",
                                    "rxMplsPackets", "rxErrMplsPackets", "rxMplsErrOuterLblPackets",
                                    "rxMplsDataPackets", "rxLdpIpv4Packets", "rxLdpIpv6Packets", "rxMplsIpv4Packets", "rxMplsIpv6Packets", "rxErrL2tpv3Packets",
                                    "rxL2tpv3Ipv4Packets", "rxL2tpv3Ipv6Packets", "rxL2tpv3Packets", "rxErrUdpPackets",
                                    "rxUdpIpv4Packets", "rxUdpIpv6Packets", "rxUdpPackets", "rxErrPsnPackets",
                                    "rxPacketsSendToCpu", "rxPacketsSendToPw", "rxTopPackets", "rxPacketDaMis"};

    if ((cosPortController == NULL) || (claPortController == NULL))
        return cAtErrorNullPointer;

    cosController  = Tha60091023CosEthPortControllerPppEthPortController(cosPortController);
    claController  = ThaClaPppPwEthPortControllerPppEthPortController(claPortController);
    CounterGet(self, claController, cosController, &pppCounters);
    cosController   = Tha60091023CosEthPortControllerPwEthPortController(cosPortController);
    claController   = ThaClaPppPwEthPortControllerPwEthPortController(claPortController);
    CounterGet(self, claController, cosController, &pwCounters);

    /* Super */
    m_AtChannelMethods->Debug(self);


    /* Print PPP OAM counters  */
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "* Ethernet port counters:\r\n");
    AtPrintc(cSevNormal, "Port Id                       :   1              2\r\n");

    CounterPrint(counterStrings[i++], pwCounters.txPackets                , pppCounters.txPackets               , cThaCounterTypeGood);
    CounterPrint(counterStrings[i++], pwCounters.txBytes                  , pppCounters.txBytes                 , cThaCounterTypeGood);
    CounterPrint(counterStrings[i++], pwCounters.txOamPackets             , pppCounters.txOamPackets            , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.txPeriodOamPackets       , pppCounters.txPeriodOamPackets      , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.txPwPackets              , pppCounters.txPwPackets             , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.txPacketsLen0_64         , pppCounters.txPacketsLen0_64        , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.txPacketsLen65_127       , pppCounters.txPacketsLen65_127      , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.txPacketsLen128_255      , pppCounters.txPacketsLen128_255     , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.txPacketsLen256_511      , pppCounters.txPacketsLen256_511     , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.txPacketsLen512_1024     , pppCounters.txPacketsLen512_1024    , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.txPacketsLen1025_1528    , pppCounters.txPacketsLen1025_1528   , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.txPacketsJumbo           , pppCounters.txPacketsJumbo          , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.txTopPackets             , pppCounters.txTopPackets            , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxPackets                , pppCounters.rxPackets               , cThaCounterTypeGood);
    CounterPrint(counterStrings[i++], pwCounters.rxBytes                  , pppCounters.rxBytes                 , cThaCounterTypeGood);
    CounterPrint(counterStrings[i++], pwCounters.rxErrEthHdrPackets       , pppCounters.rxErrEthHdrPackets      , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxErrBusPackets          , pppCounters.rxErrBusPackets         , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxErrFcsPackets          , pppCounters.rxErrFcsPackets         , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxOversizePackets        , pppCounters.rxOversizePackets       , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxUndersizePackets       , pppCounters.rxUndersizePackets      , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxPacketsLen0_64         , pppCounters.rxPacketsLen0_64        , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxPacketsLen65_127       , pppCounters.rxPacketsLen65_127      , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxPacketsLen128_255      , pppCounters.rxPacketsLen128_255     , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxPacketsLen256_511      , pppCounters.rxPacketsLen256_511     , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxPacketsLen512_1024     , pppCounters.rxPacketsLen512_1024    , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxPacketsLen1025_1528    , pppCounters.rxPacketsLen1025_1528   , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxPacketsJumbo           , pppCounters.rxPacketsJumbo          , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxPwUnsupportedPackets   , pppCounters.rxPwUnsupportedPackets  , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxErrPwLabelPackets      , pppCounters.rxErrPwLabelPackets     , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxDiscardedPackets       , pppCounters.rxDiscardedPackets      , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxPausePackets           , pppCounters.rxPausePackets          , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxErrPausePackets        , pppCounters.rxErrPausePackets       , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxBrdCastPackets         , pppCounters.rxBrdCastPackets        , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxMultCastPackets        , pppCounters.rxMultCastPackets       , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxArpPackets             , pppCounters.rxArpPackets            , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxOamPackets             , pppCounters.rxOamPackets            , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxEfmOamPackets          , pppCounters.rxEfmOamPackets         , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxErrEfmOamPackets       , pppCounters.rxErrEfmOamPackets      , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxEthOamType1Packets     , pppCounters.rxEthOamType1Packets    , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxEthOamType2Packets     , pppCounters.rxEthOamType2Packets    , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxIpv4Packets            , pppCounters.rxIpv4Packets           , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxErrIpv4Packets         , pppCounters.rxErrIpv4Packets        , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxIcmpIpv4Packets        , pppCounters.rxIcmpIpv4Packets       , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxIpv6Packets            , pppCounters.rxIpv6Packets           , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxErrIpv6Packets         , pppCounters.rxErrIpv6Packets        , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxIcmpIpv6Packets        , pppCounters.rxIcmpIpv6Packets       , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxMefPackets             , pppCounters.rxMefPackets            , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxErrMefPackets          , pppCounters.rxErrMefPackets         , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxMplsPackets            , pppCounters.rxMplsPackets           , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxErrMplsPackets         , pppCounters.rxErrMplsPackets        , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxMplsErrOuterLblPackets , pppCounters.rxMplsErrOuterLblPackets, cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxMplsDataPackets        , pppCounters.rxMplsDataPackets       , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxLdpIpv4Packets         , pppCounters.rxLdpIpv4Packets        , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxLdpIpv6Packets         , pppCounters.rxLdpIpv6Packets        , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxMplsIpv4Packets        , pppCounters.rxMplsIpv4Packets       , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxMplsIpv6Packets        , pppCounters.rxMplsIpv6Packets       , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxErrL2tpv3Packets       , pppCounters.rxErrL2tpv3Packets      , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxL2tpv3Ipv4Packets      , pppCounters.rxL2tpv3Ipv4Packets     , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxL2tpv3Ipv6Packets      , pppCounters.rxL2tpv3Ipv6Packets     , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxL2tpv3Packets          , pppCounters.rxL2tpv3Packets         , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxErrUdpPackets          , pppCounters.rxErrUdpPackets         , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxUdpIpv4Packets         , pppCounters.rxUdpIpv4Packets        , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxUdpIpv6Packets         , pppCounters.rxUdpIpv6Packets        , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxUdpPackets             , pppCounters.rxUdpPackets            , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxErrPsnPackets          , pppCounters.rxErrPsnPackets         , cThaCounterTypeError);
    CounterPrint(counterStrings[i++], pwCounters.rxPacketsSendToCpu       , pppCounters.rxPacketsSendToCpu      , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxPacketsSendToPw        , pppCounters.rxPacketsSendToPw       , cThaCounterTypeGood);
    CounterPrint(counterStrings[i++], pwCounters.rxTopPackets             , pppCounters.rxTopPackets            , cThaCounterTypeNeutral);
    CounterPrint(counterStrings[i++], pwCounters.rxPacketDaMis            , pppCounters.rxPacketDaMis           , cThaCounterTypeError);

    return cAtOk;
    }

static eAtRet HwSwitch(ThaEthPort self, ThaEthPort toPort)
    {
    return ThaPdhPwProductEthPortSwitch(self, toPort);
    }

static eAtRet HwSwitchRelease(ThaEthPort self)
    {
    return ThaPdhPwProductEthPortSwitchRelease(self);
    }

static eAtModuleEthRet Bridge(AtEthPort self, AtEthPort toPort)
    {
	AtUnused(toPort);
	AtUnused(self);
    return cAtOk;
    }

static AtEthPort BridgedPortGet(AtEthPort self)
    {
    return ThaPdhPwProductEthPortBridgedPortGet(self);
    }

static eAtModuleEthRet BridgeRelease(AtEthPort self)
    {
	AtUnused(self);
    return cAtError;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    if (ThaPdhPwProductEthPortPortIsActive(self))
        return m_AtChannelMethods->AllCountersGet(self, pAllCounters);
    else
        return ThaPdhPwProductEthPortZeroCounters(self, pAllCounters);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    if (ThaPdhPwProductEthPortPortIsActive(self))
        return m_AtChannelMethods->AllCountersClear(self, pAllCounters);
    else
        return ThaPdhPwProductEthPortZeroCounters(self, pAllCounters);
    }

static uint32 DefectGet(AtChannel self)
    {
    return ThaPdhPwProductEthPortDefectGet((AtEthPort)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023EthPort);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort ethPort = (ThaEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPortMethods = mMethodsGet(ethPort);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, m_ThaEthPortMethods, sizeof(m_ThaEthPortOverride));
        mMethodOverride(m_ThaEthPortOverride, HwSwitch);
        mMethodOverride(m_ThaEthPortOverride, HwSwitchRelease);
        }

    mMethodsSet(ethPort, &m_ThaEthPortOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(tAtEthPortMethods));

        mMethodOverride(m_AtEthPortOverride, Bridge);
        mMethodOverride(m_AtEthPortOverride, BridgedPortGet);
        mMethodOverride(m_AtEthPortOverride, BridgeRelease);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideThaEthPort(self);
    OverrideAtEthPort(self);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60091023EthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

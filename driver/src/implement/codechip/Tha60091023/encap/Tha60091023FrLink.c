/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60091023FrLink.c
 *
 * Created Date: Dec 31, 2014
 *
 * Description : Frame Relay Link of product 60091023
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/encap/ThaFrLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023FrLink
    {
    tThaFrLink super;
    }tTha60091023FrLink;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHdlcLinkMethods m_AtHdlcLinkOverride;

/*--------------------------- Forward declarations ---------------------------*/
extern eAtRet Tha60091023HdlcLinkPidBypass(AtHdlcLink self, eBool pidByPass);

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PidByPass(AtHdlcLink self, eBool pidByPass)
    {
    return Tha60091023HdlcLinkPidBypass(self, pidByPass);
    }

static void OverrideAtHdlcLink(AtHdlcLink self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcLinkOverride, mMethodsGet(self), sizeof(m_AtHdlcLinkOverride));

        mMethodOverride(m_AtHdlcLinkOverride, PidByPass);
        }

    mMethodsSet(self, &m_AtHdlcLinkOverride);
    }

static void Override(AtHdlcLink self)
    {
    OverrideAtHdlcLink(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023FrLink);
    }

static AtHdlcLink ObjectInit(AtHdlcLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaFrLinkObjectInit(self, hdlcChannel) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcLink Tha60091023FrLinkNew(AtHdlcChannel hdlcChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLink, hdlcChannel);
    }

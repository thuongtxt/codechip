/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60091023ModuleEncap.c
 *
 * Created Date: Feb 27, 2014
 *
 * Description : Encapsulation module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHdlcChannel.h"
#include "../../../default/encap/ThaModuleEncapInternal.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/ppp/ThaPppLinkInternal.h"
#include "../../../default/encap/ThaCiscoHdlcLink.h"
#include "../../../default/encap/ThaFrLink.h"
#include "../ppp/Tha60091023MpigReg.h"
#include "../ppp/Tha60091023MpegReg.h"
#include "../man/Tha60091023Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
mDefineMaskShift(ThaModuleEncap, MlppHdrMode)
mDefineMaskShift(ThaModuleEncap, PidMode)
mDefineMaskShift(ThaModuleEncap, AddrCtrlMode)
mDefineMaskShift(ThaModuleEncap, AdrComp)
mDefineMaskShift(ThaModuleEncap, BlkAloc)
mDefineMaskShift(ThaModuleEncap, FcsErrDropEn)
mDefineMaskShift(ThaModuleEncap, AdrCtlErrDropEn)
mDefineMaskShift(ThaModuleEncap, NCPRule)
mDefineMaskShift(ThaModuleEncap, LCPRule)
mDefineMaskShift(ThaModuleEncap, MlpRuleRule)
mDefineMaskShift(ThaModuleEncap, PppRule)
mDefineMaskShift(ThaModuleEncap, PKtMode)
mDefineMaskShift(ThaModuleEncap, BndID)
mDefineMaskShift(ThaModuleEncap, MPEGPppLinkCtrl1BundleID)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023ModuleEncap
    {
    tThaModuleEncap super;
    }tTha60091023ModuleEncap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEncapMethods  m_AtModuleEncapOverride;
static tThaModuleEncapMethods m_ThaModuleEncapOverride;

/* Save super implementation */
static const tAtModuleEncapMethods *m_AtModuleEncapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HdlcByteCountersAreSupported(ThaModuleEncap self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool AcfcSupported(ThaModuleEncap self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static AtHdlcChannel HdlcPppChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    return Tha60031032HdlcChannelNew(channelId, cAtHdlcFrmPpp, self);
    }

static AtHdlcChannel CiscoHdlcChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    return Tha60031032HdlcChannelNew(channelId, cAtHdlcFrmCiscoHdlc, self);
    }

static AtHdlcChannel FrChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    return Tha60031032HdlcChannelNew(channelId, cAtHdlcFrmFr, self);
    }

static uint32 StartDeviceVersionThatSupportsFrameRelay(ThaModuleEncap self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(1, 2, 0);
    }

static uint32 StartDeviceVersionThatSupportsIdleByteCounter(ThaModuleEncap self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(1, 6, 0);
    }

static AtDevice Device(AtModuleEncap self)
    {
    return AtModuleDeviceGet((AtModule)self);
    }

static AtPppLink PppLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
    if (Tha60091023Device24MpBundlesSupported(Device(self)))
        return Tha60091023PppLinkNew(hdlcChannel);
    return ThaPppLinkNew(hdlcChannel);
    }

static AtHdlcLink CiscoHdlcLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
    if (Tha60091023Device24MpBundlesSupported(Device(self)))
        return (AtHdlcLink)Tha60091023CiscoHdlcLinkNew(hdlcChannel);
    return (AtHdlcLink)ThaCiscoHdlcLinkNew(hdlcChannel);
    }

static AtHdlcLink FrLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
    if (Tha60091023Device24MpBundlesSupported(Device(self)))
        return Tha60091023FrLinkNew(hdlcChannel);
    return (AtHdlcLink)ThaFrLinkNew(hdlcChannel);
    }

static void OverrideAtModuleEncap(AtModuleEncap self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEncapMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEncapOverride, mMethodsGet(self), sizeof(m_AtModuleEncapOverride));

        mMethodOverride(m_AtModuleEncapOverride, HdlcPppChannelObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, CiscoHdlcChannelObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, FrChannelObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, PppLinkObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, FrLinkObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, CiscoHdlcLinkObjectCreate);
        }

    mMethodsSet(self, &m_AtModuleEncapOverride);
    }

static void OverrideThaModuleEncap(AtModuleEncap self)
    {
    ThaModuleEncap encap = (ThaModuleEncap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEncapOverride, mMethodsGet(encap), sizeof(m_ThaModuleEncapOverride));

        mMethodOverride(m_ThaModuleEncapOverride, HdlcByteCountersAreSupported);
        mMethodOverride(m_ThaModuleEncapOverride, AcfcSupported);
        mMethodOverride(m_ThaModuleEncapOverride, StartDeviceVersionThatSupportsFrameRelay);
        mMethodOverride(m_ThaModuleEncapOverride, StartDeviceVersionThatSupportsIdleByteCounter);

        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MlppHdrMode)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, PidMode)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, AddrCtrlMode)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, AdrComp)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, BlkAloc)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, FcsErrDropEn)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, AdrCtlErrDropEn)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, NCPRule)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, LCPRule)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MlpRuleRule)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, PppRule)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, PKtMode)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, BndID)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MPEGPppLinkCtrl1BundleID)
        }

    mMethodsSet(encap, &m_ThaModuleEncapOverride);
    }

static void Override(AtModuleEncap self)
    {
    OverrideAtModuleEncap(self);
    OverrideThaModuleEncap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023ModuleEncap);
    }

static AtModuleEncap ObjectInit(AtModuleEncap self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleEncapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEncap Tha60091023ModuleEncapNew(AtDevice device)
    {
    /* Allocate memory for this module */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEncap newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Constructor */
    return ObjectInit(newModule, device);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60091023ModuleMap.c
 *
 * Created Date: Jul 17, 2014
 *
 * Description : MAP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/map/ThaModuleMapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegMapUnusedTimeslotsDefaultVal 0x1A0C00

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023ModuleMap
    {
    tThaModuleMap super;
    }tTha60091023ModuleMap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;

/* To save super implementation */
static const tAtModuleMethods      *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static eAtRet DefaultSet(ThaModuleMap self)
    {
	static const uint8 cUnusedTimeslotsDefaultVal = 0xFF;
	mModuleHwWrite(self, cThaRegMapUnusedTimeslotsDefaultVal, cUnusedTimeslotsDefaultVal);
    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet((ThaModuleMap)self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void OverrideAtModule(AtModule self)
	{
	if (!m_methodsInit)
		{
		AtOsal osal = AtSharedDriverOsalGet();
		m_AtModuleMethods = mMethodsGet(self);
		mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
		mMethodOverride(m_AtModuleOverride, Init);
		mMethodOverride(m_AtModuleOverride, AsyncInit);
		}

	mMethodsSet(self, &m_AtModuleOverride);
	}

static void Override(AtModule self)
	{
	OverrideAtModule(self);
	}

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023ModuleMap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super construction */
    if (ThaModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60091023ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : Tha60091023CosPwEthPortController.c
 *
 * Created Date: Dec 4, 2013
 *
 * Description : COS PW ETH Port controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cos/controllers/ThaCosEthPortControllerInternal.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023CosPwEthPortController
    {
    tThaCosEthPortControllerV2 super;
    }tTha60091023CosPwEthPortController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCosEthPortControllerMethods m_ThaCosEthPortControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023CosPwEthPortController);
    }

static uint32 PartOffset(ThaCosEthPortController self, AtEthPort port)
    {
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static void OverrideThaCosEthPortController(ThaCosEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCosEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaCosEthPortControllerOverride));

        mMethodOverride(m_ThaCosEthPortControllerOverride, PartOffset);
        }

    mMethodsSet(self, &m_ThaCosEthPortControllerOverride);
    }

static void Override(ThaCosEthPortController self)
    {
    OverrideThaCosEthPortController(self);
    }

static ThaCosEthPortController ObjectInit(ThaCosEthPortController self, ThaModuleCos cos)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCosEthPortControllerV2ObjectInit(self, cos) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCosEthPortController Tha60091023CosPwEthPortControllerNew(ThaModuleCos cos)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCosEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cos);
    }

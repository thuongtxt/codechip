/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : Tha60091023ModuleCos.c
 *
 * Created Date: Sep 7, 2013
 *
 * Description : COS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cos/ThaModuleCosInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023ModuleCos
    {
    tThaModuleCosV2 super;
    }tTha60091023ModuleCos;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleCosMethods m_ThaModuleCosOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaCosEthPortController EthPortControllerCreate(ThaModuleCos self)
    {
    return Tha60091023CosEthPortControllerNew(self);
    }

static eBool IsPwPart(ThaModuleCos self, uint8 partId)
    {
	AtUnused(self);
    return (partId == 0) ? cAtTrue : cAtFalse;
    }

static void OverrideThaModuleCos(AtModule self)
    {
    ThaModuleCos cos = (ThaModuleCos)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCosOverride, mMethodsGet(cos), sizeof(m_ThaModuleCosOverride));

        mMethodOverride(m_ThaModuleCosOverride, EthPortControllerCreate);
        mMethodOverride(m_ThaModuleCosOverride, IsPwPart);
        }

    mMethodsSet(cos, &m_ThaModuleCosOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCos(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023ModuleCos);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleCosV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60091023ModuleCosNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

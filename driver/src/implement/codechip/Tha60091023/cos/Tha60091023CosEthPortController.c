/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : Tha60091023CosEthPortController.c
 *
 * Created Date: Dec 2, 2013
 *
 * Description : ETH Port controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cos/controllers/ThaCosEthPortControllerInternal.h"
#include "Tha60091023CosController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60091023CosEthPortController *) ((void *)self))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023CosEthPortController
    {
    tThaCosEthPortController super;

    /* Private data */
    ThaCosEthPortController pppEthPortController;
    ThaCosEthPortController pwEthPortController;
    }tTha60091023CosEthPortController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                m_AtObjectOverride;
static tThaCosEthPortControllerMethods m_ThaCosEthPortControllerOverride;

/* Save super implementations */
static const tAtObjectMethods         *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023CosEthPortController);
    }

static ThaCosEthPortController PppEthPortController(ThaCosEthPortController self)
    {
    if (mThis(self)->pppEthPortController == NULL)
        mThis(self)->pppEthPortController = Tha60091023CosPppEthPortControllerNew(ThaCosControllerModuleGet((ThaCosController)self));
    return mThis(self)->pppEthPortController;
    }

static ThaCosEthPortController PwEthPortController(ThaCosEthPortController self)
    {
    if (mThis(self)->pwEthPortController == NULL)
        mThis(self)->pwEthPortController = Tha60091023CosPwEthPortControllerNew(ThaCosControllerModuleGet((ThaCosController)self));
    return mThis(self)->pwEthPortController;
    }

static uint32 EthPortOutcomingPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerOutcomingPktRead2Clear(PppEthPortController(self), port, r2c) +
           ThaCosEthPortControllerOutcomingPktRead2Clear(PwEthPortController(self), port, r2c);
    }

static uint32 EthPortOutcomingbyteRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerOutcomingbyteRead2Clear(PppEthPortController(self), port, r2c) +
           ThaCosEthPortControllerOutcomingbyteRead2Clear(PwEthPortController(self), port, r2c);
    }

static uint32 EthPortOamTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerOamTxPktRead2Clear(PppEthPortController(self), port, r2c) +
           ThaCosEthPortControllerOamTxPktRead2Clear(PwEthPortController(self), port, r2c);
    }

static uint32 EthPortOamFrequencyTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerOamFrequencyTxPktRead2Clear(PppEthPortController(self), port, r2c) +
           ThaCosEthPortControllerOamFrequencyTxPktRead2Clear(PwEthPortController(self), port, r2c);
    }

static uint32 EthPortPWTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPWTxPktRead2Clear(PppEthPortController(self), port, r2c) +
           ThaCosEthPortControllerPWTxPktRead2Clear(PwEthPortController(self), port, r2c);
    }

static uint32 EthPortPktLensmallerthan64bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLensmallerthan64bytesRead2Clear(PppEthPortController(self), port, r2c) +
           ThaCosEthPortControllerPktLensmallerthan64bytesRead2Clear(PwEthPortController(self), port, r2c);
    }

static uint32 EthPortPktLenfrom65to127bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom65to127bytesRead2Clear(PppEthPortController(self), port, r2c) +
           ThaCosEthPortControllerPktLenfrom65to127bytesRead2Clear(PwEthPortController(self), port, r2c);
    }

static uint32 EthPortPktLenfrom128to255bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom128to255bytesRead2Clear(PppEthPortController(self), port, r2c) +
           ThaCosEthPortControllerPktLenfrom128to255bytesRead2Clear(PwEthPortController(self), port, r2c);
    }

static uint32 EthPortPktLenfrom256to511bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom256to511bytesRead2Clear(PppEthPortController(self), port, r2c) +
           ThaCosEthPortControllerPktLenfrom256to511bytesRead2Clear(PwEthPortController(self), port, r2c);
    }

static uint32 EthPortPktLenfrom512to1024bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom512to1024bytesRead2Clear(PppEthPortController(self), port, r2c) +
           ThaCosEthPortControllerPktLenfrom512to1024bytesRead2Clear(PwEthPortController(self), port, r2c);
    }

static uint32 EthPortPktLenfrom1025to1528bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom1025to1528bytesRead2Clear(PppEthPortController(self), port, r2c) +
           ThaCosEthPortControllerPktLenfrom1025to1528bytesRead2Clear(PwEthPortController(self), port, r2c);
    }

static uint32 EthPortPktJumboLenRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktJumboLenRead2Clear(PppEthPortController(self), port, r2c) +
           ThaCosEthPortControllerPktJumboLenRead2Clear(PwEthPortController(self), port, r2c);
    }

static uint32 EthPortPktTimePktToPRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktTimePktToPRead2Clear(PppEthPortController(self), port, r2c) +
           ThaCosEthPortControllerPktTimePktToPRead2Clear(PwEthPortController(self), port, r2c);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->pppEthPortController);
    mThis(self)->pppEthPortController = NULL;
    AtObjectDelete((AtObject)mThis(self)->pwEthPortController);
    mThis(self)->pwEthPortController = NULL;

    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(ThaCosEthPortController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaCosEthPortController(ThaCosEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCosEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaCosEthPortControllerOverride));

        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOutcomingPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOutcomingbyteRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOamTxPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOamFrequencyTxPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPWTxPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLensmallerthan64bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom65to127bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom128to255bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom256to511bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom512to1024bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom1025to1528bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktJumboLenRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktTimePktToPRead2Clear);
        }

    mMethodsSet(self, &m_ThaCosEthPortControllerOverride);
    }

static void Override(ThaCosEthPortController self)
    {
    OverrideAtObject(self);
    OverrideThaCosEthPortController(self);
    }

static ThaCosEthPortController ObjectInit(ThaCosEthPortController self, ThaModuleCos cos)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCosEthPortControllerObjectInit(self, cos) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCosEthPortController Tha60091023CosEthPortControllerNew(ThaModuleCos cos)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCosEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cos);
    }

ThaCosEthPortController Tha60091023CosEthPortControllerPppEthPortController(ThaCosEthPortController self)
    {
    return PppEthPortController(self);
    }

ThaCosEthPortController Tha60091023CosEthPortControllerPwEthPortController(ThaCosEthPortController self)
    {
    return PwEthPortController(self);
    }

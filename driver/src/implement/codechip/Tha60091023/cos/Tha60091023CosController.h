/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : COS
 * 
 * File        : Tha60091023CosController.h
 * 
 * Created Date: Mar 2, 2015
 *
 * Description : Common declaration for COS controllers (both PW and PPP parts)
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60091023COSCONTROLLER_H_
#define _THA60091023COSCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCosEthPortController Tha60091023CosEthPortControllerPppEthPortController(ThaCosEthPortController self);
ThaCosEthPortController Tha60091023CosEthPortControllerPwEthPortController(ThaCosEthPortController self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60091023COSCONTROLLER_H_ */


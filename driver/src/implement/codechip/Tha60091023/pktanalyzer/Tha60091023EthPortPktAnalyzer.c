/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : Tha60091023EthPortPktAnalyzer.c
 *
 * Created Date: Apr 29, 2014
 *
 * Description : ETH Port packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/pktanalyzer/ThaStmPwProductEthPortPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023EthPortPktAnalyzer
    {
    tThaStmPwProductEthPortPktAnalyzer super;
    }tTha60091023EthPortPktAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SkipLastInCompletePacket(ThaPktAnalyzer self)
    {
	AtUnused(self);
    /* This product has to use packet analyzer of CES product but this analyzer
     * has a limitation that cannot capture large packet. But to analyze
     * header of large packet, the last in complete packet is still put to
     * packet list for displaying */
    return cAtFalse;
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer pktAnalyzer = (ThaPktAnalyzer)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, mMethodsGet(pktAnalyzer), sizeof(tThaPktAnalyzerMethods));
        mMethodOverride(m_ThaPktAnalyzerOverride, SkipLastInCompletePacket);
        }

    mMethodsSet(pktAnalyzer, &m_ThaPktAnalyzerOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023EthPortPktAnalyzer);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideThaPktAnalyzer(self);
    }

static AtPktAnalyzer ObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductEthPortPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60091023EthPortPktAnalyzerNew(AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPktAnalyzer, port);
    }

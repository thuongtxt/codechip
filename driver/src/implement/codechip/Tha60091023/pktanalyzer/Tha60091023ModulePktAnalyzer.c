/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : Tha60091023ModulePktAnalyzer.c
 *
 * Created Date: Apr 29, 2014
 *
 * Description : Packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/pktanalyzer/ThaStmPwProductModulePktAnalyzerInternal.h"
#include "../../../default/pktanalyzer/ThaPktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091023ModulePktAnalyzer
    {
    tThaStmPwProductModulePktAnalyzer super;
    }tTha60091023ModulePktAnalyzer;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePktAnalyzerMethods m_AtModulePktAnalyzerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPktAnalyzer EthPortPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthPort port)
    {
	AtUnused(self);
    return Tha60091023EthPortPktAnalyzerNew(port);
    }

static AtPktAnalyzer EthFlowPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthFlow flow)
    {
	AtUnused(self);
    return ThaEthFlowPktAnalyzerNew(flow);
    }

static AtPktAnalyzer PppLinkPktAnalyzerCreate(AtModulePktAnalyzer self, AtPppLink ppplink)
    {
	AtUnused(self);
    return ThaPppLinkPktAnalyzerNew(ppplink);
    }

static void OverrideAtModulePktAnalyzer(AtModulePktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtModulePktAnalyzerOverride));

        mMethodOverride(m_AtModulePktAnalyzerOverride, EthPortPktAnalyzerCreate);
        mMethodOverride(m_AtModulePktAnalyzerOverride, EthFlowPktAnalyzerCreate);
        mMethodOverride(m_AtModulePktAnalyzerOverride, PppLinkPktAnalyzerCreate);
        }

    mMethodsSet(self, &m_AtModulePktAnalyzerOverride);
    }

static void Override(AtModulePktAnalyzer self)
    {
    OverrideAtModulePktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023ModulePktAnalyzer);
    }

static AtModulePktAnalyzer ObjectInit(AtModulePktAnalyzer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModulePktAnalyzerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePktAnalyzer Tha60091023ModulePktAnalyzerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePktAnalyzer newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

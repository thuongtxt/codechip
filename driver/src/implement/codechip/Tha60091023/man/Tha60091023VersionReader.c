/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60091023VersionReader.c
 *
 * Created Date: May 13, 2014
 *
 * Description : Version reader for product 60091023
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/versionreader/ThaVersionReaderInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegChipVersion 0x1000002
#define cRegReleaseDate 0x1000003

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaVersionReaderMethods m_ThaVersionReaderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ReleaseDateRegister(ThaVersionReader self)
    {
	AtUnused(self);
    return cRegReleaseDate;
    }

static uint32 VersionRegister(ThaVersionReader self)
    {
	AtUnused(self);
    return cRegChipVersion;
    }

static void OverrideThaVersionReader(ThaVersionReader self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVersionReaderOverride, mMethodsGet(self), sizeof(m_ThaVersionReaderOverride));
        mMethodOverride(m_ThaVersionReaderOverride, ReleaseDateRegister);
        mMethodOverride(m_ThaVersionReaderOverride, VersionRegister);
        }

    mMethodsSet(self, &m_ThaVersionReaderOverride);
    }

static void Override(ThaVersionReader self)
    {
    OverrideThaVersionReader(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVersionReader);
    }

static ThaVersionReader ObjectInit(ThaVersionReader self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (device == NULL)
        return NULL;

    /* Super constructor */
    if (ThaVersionReaderObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaVersionReader Tha60091023VersionReaderNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaVersionReader newReader = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newReader == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newReader, device);
    }

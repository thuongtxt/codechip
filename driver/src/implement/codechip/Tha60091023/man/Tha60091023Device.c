/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60031023Device.c
 *
 * Created Date: Aug 28, 2013
 *
 * Description : Product 60031023
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60091023DeviceInternal.h"
#include "../../../ThaPdhMlppp/ppp/ThaPdhMlpppModulePppInternal.h"
#include "../../../default/encap/ThaModuleEncap.h"
#include "../../../default/binder/ThaEncapBinder.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
/* Implementation of this class */
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static const tAtDeviceMethods  *m_AtDeviceMethods  = NULL;
static const tThaDeviceMethods *m_ThaDeviceMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 _moduleId = (uint32)moduleId;
    switch (_moduleId)
        {
        case cThaModuleDemap:      return cAtTrue;
        case cThaModuleMap:        return cAtTrue;
        case cThaModuleCdr:        return cAtTrue;
        case cThaModuleCla:        return cAtTrue;
        case cThaModuleMpig:       return cAtTrue;
        case cThaModuleMpeg:       return cAtTrue;
        case cThaModulePmcMpeg:    return cAtTrue;
        case cThaModulePmcMpig:    return cAtTrue;
        case cThaModuleCos:        return cAtTrue;

        case cAtModulePdh:         return cAtTrue;
        case cAtModulePw:          return cAtTrue;
        case cAtModuleRam:         return cAtTrue;
        case cAtModuleEth:         return cAtTrue;
        case cAtModulePpp:         return cAtTrue;
        case cAtModuleClock:       return cAtTrue;
        case cAtModuleEncap:       return cAtTrue;
        case cAtModulePktAnalyzer: return cAtTrue;

        default:
            return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
        }
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    /* Modules create by ThaPdhMlpppDevice */
    if (moduleId == cAtModulePpp)         return (AtModule)Tha60091023ModulePppNew(self);
    if (moduleId == cAtModuleEth)         return (AtModule)Tha60091023ModuleEthNew(self);
    if (moduleId == cAtModulePw)          return (AtModule)Tha60091023ModulePwNew(self);
    if (moduleId == cAtModuleClock)       return (AtModule)Tha60091023ModuleClockNew(self);
    if (moduleId == cAtModulePktAnalyzer) return (AtModule)Tha60091023ModulePktAnalyzerNew(self);
    if (moduleId == cAtModulePdh)         return (AtModule)Tha60091023ModulePdhNew(self);

    /* These modules are rejected by a PW device, this device is merged MLPPP and PW,
     * so need to override here */
    if (moduleId == cAtModuleEncap)       return (AtModule)Tha60091023ModuleEncapNew(self);
    if (phyModule == cThaModuleMpig)      return ThaModuleMpigNew(self);
    if (phyModule == cThaModuleMpeg)      return ThaModuleMpegNew(self);
    if (phyModule == cThaModulePmcMpeg)   return ThaModulePmcMpegNew(self);
    if (phyModule == cThaModulePmcMpig)   return ThaModulePmcMpigNew(self);

    if (phyModule == cThaModuleCos)       return Tha60091023ModuleCosNew(self);
    if (phyModule == cThaModuleCla)       return Tha60091023ModuleClaNew(self);
    if (phyModule == cThaModuleCdr)       return Tha60091023ModuleCdrNew(self);
    if (phyModule == cThaModuleMap)       return Tha60091023ModuleMapNew(self);
    if (phyModule == cThaModulePda)       return Tha60091023ModulePdaNew(self);

    /* Others module create by super Tha60031021 */
    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleRam,
                                                 cAtModuleEth,
                                                 cAtModulePktAnalyzer,
                                                 cAtModulePpp,
                                                 cAtModuleClock,
                                                 cAtModuleEncap};
    AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static uint32 ModulePartOffset(ThaDevice self, eAtModule moduleId, uint8 partId)
    {
    uint32 moduleId_;
    uint8 modulePart = 0;

    /* All modules relate to PPP belong to part 2 */
    moduleId_ = (uint32)moduleId; /* Just to make the compiler happy */
    if ((moduleId_ == cThaModuleMpig)    ||
        (moduleId_ == cThaModuleMpeg)    ||
        (moduleId_ == cThaModulePmcMpeg) ||
        (moduleId_ == cThaModulePmcMpig) ||
        (moduleId_ == cAtModuleEncap)    ||
        (moduleId_ == cAtModulePpp))
        modulePart = 1;

    /* Module CLA has 2 part: one for PW and one for MLPPP */
    if (moduleId_ == cThaModuleCla)
        return ThaDevicePartOffset(self, partId);

    return ThaDevicePartOffset(self, modulePart);
    }

static uint8 MaxNumParts(ThaDevice self)
    {
	AtUnused(self);
    return 2;
    }

static uint8 NumPartsOfModule(ThaDevice self, uint32 moduleId)
    {
    uint32 moduleId_ = (uint32)moduleId; /* Just to make compiler be happy */

    if ((moduleId_ == cAtModuleClock)    ||
        (moduleId_ == cThaModuleMpig)    ||
        (moduleId_ == cThaModuleMpeg)    ||
        (moduleId_ == cThaModulePmcMpeg) ||
        (moduleId_ == cThaModulePmcMpig) ||
        (moduleId_ == cAtModuleEncap)    ||
        (moduleId_ == cAtModulePpp)      ||
        (moduleId_ == cAtModulePw))
        return 1;

    return m_ThaDeviceMethods->NumPartsOfModule(self, moduleId_);
    }

static ThaVersionReader VersionReaderCreate(ThaDevice self)
    {
    return Tha60091023VersionReaderNew((AtDevice)self);
    }

static AtEncapBinder EncapBinder(AtDevice self)
    {
    return ThaEncapDefaultBinder(self);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(m_AtDeviceOverride));
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, EncapBinder);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice thaDev = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDeviceMethods = mMethodsGet(thaDev);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, m_ThaDeviceMethods, sizeof(tThaDeviceMethods));
        mMethodOverride(m_ThaDeviceOverride, ModulePartOffset);
        mMethodOverride(m_ThaDeviceOverride, MaxNumParts);
        mMethodOverride(m_ThaDeviceOverride, NumPartsOfModule);
        mMethodOverride(m_ThaDeviceOverride, VersionReaderCreate);
        }

    mMethodsSet(thaDev, &m_ThaDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091023Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60091023DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

eBool Tha60091023Device24MpBundlesSupported(AtDevice device)
    {
    uint32 startVersion = ThaVersionReaderVersionBuild(0x1, 0x4, 0x0);
    return (AtDeviceVersionNumber(device) >= startVersion);
    }

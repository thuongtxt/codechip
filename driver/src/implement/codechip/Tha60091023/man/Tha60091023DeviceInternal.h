/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60091023DeviceInternal.h
 * 
 * Created Date: Aug 28, 2013
 *
 * Description : Product 60091023
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha60091023DEVICEINTERNAL_H_
#define _Tha60091023DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaPdhPwProduct/man/ThaPdhPwProductDeviceInternal.h"
#include "Tha60091023Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60091023Device
    {
    tThaPdhPwProductDevice super;
    }tTha60091023Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _Tha60091023DEVICEINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60091023Device.h
 * 
 * Created Date: Mar 2, 2015
 *
 * Description : Product 60091023
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60091023DEVICE_H_
#define _THA60091023DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60091023Device24MpBundlesSupported(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60091023DEVICE_H_ */


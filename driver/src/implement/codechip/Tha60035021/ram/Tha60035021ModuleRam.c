/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60035021ModuleRam.c
 *
 * Created Date: May 8, 2013
 *
 * Description : RAM module of product 60035021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ram/ThaModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha60035021ModuleRam *)((void *)self))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60035021ModuleRam
    {
    tThaModuleRam super;

    /* Private */
    uint8 numDdrs;
    }tTha60035021ModuleRam;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtModuleRamMethods m_AtModuleRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60035021ModuleRam);
    }

static uint8 NumDdrGet(AtModuleRam self)
    {
    return mThis(self)->numDdrs;
    }

static uint8 NumZbtGet(AtModuleRam self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 DdrAddressBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 23;
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, mMethodsGet(self), sizeof(m_AtModuleRamOverride));

        mMethodOverride(m_AtModuleRamOverride, NumDdrGet);
        mMethodOverride(m_AtModuleRamOverride, NumZbtGet);
        mMethodOverride(m_AtModuleRamOverride, DdrAddressBusSizeGet);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModuleRam(self);
    }

static AtModuleRam ObjectInit(AtModuleRam self, AtDevice device, uint8 numDdrs)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleRamObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->numDdrs = numDdrs;

    return self;
    }

AtModuleRam Tha60035021ModuleRamNew(AtDevice device, uint8 numDdrs)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device, numDdrs);
    }

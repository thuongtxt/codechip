/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60035021EthPortSerdesController.c
 *
 * Created Date: Sep 9, 2013
 *
 * Description : Ethernet port serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/physical/ThaSerdesControllerInternal.h"
#include "../../../default/eth/ThaModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#define cLoopInReg   0xF00042
#define cLoopInMask  cBit0
#define cLoopInShift 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60035021EthPortSerdesController
    {
    tThaSerdesController super;
    }tTha60035021EthPortSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSerdesControllerMethods m_ThaSerdesControllerOverride;
static tAtSerdesControllerMethods  m_AtSerdesControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModule ModuleId(AtSerdesController self)
    {
    AtModule module = AtChannelModuleGet(AtSerdesControllerPhysicalPortGet(self));
    return AtModuleTypeGet(module);
    }

static eAtRet LoopInEnable(ThaSerdesController self, eBool enable)
    {
    AtSerdesController controller = (AtSerdesController)self;
    uint32 offset = mMethodsGet(self)->PartOffset(self);
    uint32 regVal, regAddr;
    eAtModule moduleId = ModuleId(controller);

    regAddr = cLoopInReg + offset;
    regVal = AtSerdesControllerRead(controller, regAddr, moduleId);
    mRegFieldSet(regVal, cLoopIn, mBoolToBin(enable));
    AtSerdesControllerWrite(controller, regAddr, regVal, moduleId);

    return cAtOk;
    }

static eBool LoopInIsEnabled(ThaSerdesController self)
    {
    AtSerdesController controller = (AtSerdesController)self;
    uint32 offset = mMethodsGet(self)->PartOffset(self);
    uint32 regAddr = cLoopInReg + offset;
    eAtModule moduleId = ModuleId(controller);
    uint32 regVal = AtSerdesControllerRead(controller, regAddr, moduleId);

    if (regVal & cLoopInMask)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
	AtUnused(value);
	AtUnused(param);
	AtUnused(self);
    /* Not support */
    return cAtErrorNotImplemented;
    }

static uint32 PhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
	AtUnused(param);
	AtUnused(self);
    /* Not support */
    return 0;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60035021EthPortSerdesController);
    }

static uint32 PartOffset(ThaSerdesController self)
    {
    AtSerdesController controller = (AtSerdesController)self;
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet(AtSerdesControllerPhysicalPortGet(controller));
    AtEthPort port = (AtEthPort)AtSerdesControllerPhysicalPortGet(controller);

    return ThaModuleEthPartOffset(ethModule, ThaModuleEthPartOfPort(ethModule, port));
    }

static eAtRet LoopOutEnable(ThaSerdesController self, eBool enable)
    {
	AtUnused(self);
    /* Not support */
    if (enable == cAtTrue)
        return cAtErrorNotImplemented;

    return cAtOk;
    }

static eBool LoopOutIsEnabled(ThaSerdesController self)
    {
	AtUnused(self);
    /* Not support */
    return cAtFalse;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return ((mode == cAtSerdesModeGe) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeGe;
    }

static void OverrideThaSerdesController(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaSerdesControllerOverride));

        mMethodOverride(m_ThaSerdesControllerOverride, LoopOutEnable);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopOutIsEnabled);
        mMethodOverride(m_ThaSerdesControllerOverride, PartOffset);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInEnable);
        mMethodOverride(m_ThaSerdesControllerOverride, LoopInIsEnabled);
        }

    mMethodsSet(controller, &m_ThaSerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamSet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamGet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideThaSerdesController(self);
    OverrideAtSerdesController(self);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSerdesControllerObjectInit(self, (AtChannel)ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60035021EthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newController, ethPort, serdesId);
    }

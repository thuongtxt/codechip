/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60035021Device.c
 *
 * Created Date: Aug 21, 2013
 *
 * Description : Product 60035021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pw/ThaModulePw.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/ber/ThaModuleBer.h"
#include "../../default/ThaStmPwProduct/man/ThaStmPwProductDevice.h"
#include "Tha60035021Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60035021Device
    {
    tThaStmPwProductDevice super;

    /* Private data */
    }tTha60035021Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60035021Device);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    static const uint8 cNumDdrs = 2;
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModuleEth)  return (AtModule)Tha60035021ModuleEthNew(self);
    if (moduleId  == cAtModuleSdh)  return (AtModule)Tha60035021ModuleSdhNew(self);
    if (moduleId  == cAtModulePw)   return (AtModule)Tha60035021ModulePwNew(self);
    if (moduleId  == cAtModuleRam)  return (AtModule)Tha60035021ModuleRamNew(self, cNumDdrs);
    if (moduleId  == cAtModuleBer)  return (AtModule)ThaStmModuleBerNew(self, cThaModuleBerDefaultMaxNumEngines);
    if (moduleId  == cAtModulePdh)  return (AtModule)Tha60035021ModulePdhNew(self);

    if (phyModule == cThaModuleCdr) return Tha60035021ModuleCdrNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static uint8 NumPartsOfModule(ThaDevice self, uint32 moduleId)
    {
    AtUnused(self);
    AtUnused(moduleId);
    return 1;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    if (moduleId == cAtModuleBer)
        return cAtTrue;
    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleSdh,
                                                 cAtModuleBer,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleClock};
    AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static uint8 MaxNumParts(ThaDevice self)
    {
	AtUnused(self);
    return 1;
    }

static eBool CanHaveNewOcn(ThaDevice self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionWithNewOcn(ThaDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader((AtDevice)self), 0x14, 0x4, 0x15, 0x0);
    }

static eBool NeedCheckSSKey(AtDevice self)
    {
    return Tha600350xxDeviceNeedCheckSSKey(self);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, NumPartsOfModule);
        mMethodOverride(m_ThaDeviceOverride, MaxNumParts);
        mMethodOverride(m_ThaDeviceOverride, CanHaveNewOcn);
        mMethodOverride(m_ThaDeviceOverride, StartVersionWithNewOcn);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, NeedCheckSSKey);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60035021DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }

eBool Tha600350xxDeviceNeedCheckSSKey(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (AtDeviceIsSimulated(self) || ThaDeviceIsEp(device))
        return cAtFalse;

    /* If SSKey is not working and it needs to be temporary bypassed,
     * should return cAtFalse */
    return cAtTrue;
    }

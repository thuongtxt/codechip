/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60035021Device.h
 * 
 * Created Date: Mar 1, 2015
 *
 * Description : Product 60035021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60035021DEVICE_H_
#define _THA60035021DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha600350xxDeviceNeedCheckSSKey(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60035021DEVICE_H_ */


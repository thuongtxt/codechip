/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60035021ModulePw.c
 *
 * Created Date: Aug 21, 2013
 *
 * Description : PW module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pw/ThaModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60035021ModulePw
    {
    tThaModulePw super;
    }tTha60035021ModulePw;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePwMethods  m_AtModulePwOverride;
static tThaModulePwMethods m_ThaModulePwOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPwCep CepObjectCreate(AtModulePw self, uint32 pwId, eAtPwCepMode mode)
    {
	AtUnused(mode);
	AtUnused(pwId);
	AtUnused(self);
    return NULL;
    }

static uint32 MaxPwsGet(AtModulePw self)
    {
	AtUnused(self);
    return 512;
    }

static eBool PwIdleCodeIsSupported(ThaModulePw self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(pwModule), sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, PwIdleCodeIsSupported);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, mMethodsGet(self), sizeof(tAtModulePwMethods));
        mMethodOverride(m_AtModulePwOverride, CepObjectCreate);
        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtModulePw(self);
    OverrideThaModulePw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60035021ModulePw);
    }

static AtModulePw ObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60035021ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

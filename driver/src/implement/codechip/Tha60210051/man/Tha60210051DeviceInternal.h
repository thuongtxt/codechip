/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210051DeviceInternal.h
 * 
 * Created Date: Feb 22, 2016
 *
 * Description : 60210051 Device internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051DEVICEINTERNAL_H_
#define _THA60210051DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/man/Tha60210011DeviceInternal.h"
#include "Tha60210051Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051DeviceMethods
    {
    void (*HwFlushPdhSlice)(Tha60210051Device self, uint8 slice);
    void (*HwFlushCdrSlice)(Tha60210051Device self, uint8 slice);
    void (*HwFlushPmc)(Tha60210051Device self);

    eBool (*UseGlobalHold)(Tha60210051Device self);
    uint32 (*StartVersionSupportDistinctErdi)(AtDevice self);
    uint32 (*StartVersionSupportNewPsn)(AtDevice self);
    uint32 (*StartVersionSupportPowerControl)(AtDevice self);
    uint32 (*StartVersionApplyNewReset)(Tha60210051Device self);
    eBool (*ErrorGeneratorIsSupported)(Tha60210051Device self);
    }tTha60210051DeviceMethods;

typedef struct tTha60210051Device
    {
    tTha60210011Device super;
    const tTha60210051DeviceMethods *methods;

    /*async init hwflush state*/
    uint32 asyncInitHwFlushState;

    }tTha60210051Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60210051DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051DEVICEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2021 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210051Device.c
 *
 * Created Date: Mar 8, 2021
 *
 * Description : Product 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/prbs/ThaModulePrbs.h"
#include "../../../default/ber/ThaModuleBer.h"
#include "../../Tha60210011/common/Tha602100xxCommon.h"
#include "../clock/Tha60210051ModuleClock.h"
#include "Tha60210051DeviceInternal.h"
#include "Tha60210051DeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210051Device)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210051DeviceMethods m_methods;

/* Override */
static tAtObjectMethods          m_AtObjectOverride;
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;
static tTha60210011DeviceMethods m_Tha60210011DeviceOverride;

/* Super implementation */
static const tAtObjectMethods    *m_AtObjectMethods  = NULL;
static const tAtDeviceMethods    *m_AtDeviceMethods  = NULL;
static const tThaDeviceMethods   *m_ThaDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    /* Public modules */
    if (moduleId  == cAtModuleSdh)    return (AtModule)Tha60210051ModuleSdhNew(self);
    if (moduleId  == cAtModulePw)     return (AtModule)Tha60210051ModulePwNew(self);
    if (moduleId  == cAtModuleEth)    return (AtModule)Tha60210051ModuleEthNew(self);
    if (moduleId  == cAtModulePrbs)   return (AtModule)Tha60210051ModulePrbsNew(self);
    if (moduleId  == cAtModulePdh)    return (AtModule)Tha60210051ModulePdhNew(self);
    if (moduleId  == cAtModuleBer)    return (AtModule)Tha60210051ModuleBerNew(self);
    if (moduleId  == cAtModuleClock)  return (AtModule)Tha60210051ModuleClockNew(self);

    /* Physical modules */
    if (phyModule == cThaModulePoh)   return Tha60210051ModulePohNew(self);
    if (phyModule == cThaModulePda)   return Tha60210051ModulePdaNew(self);
    if (phyModule == cThaModuleCdr)   return Tha60210051ModuleCdrNew(self);
    if (phyModule == cThaModuleCla)   return Tha60210051ModuleClaNew(self);
    if (phyModule == cThaModulePwe)   return Tha60210051ModulePweNew(self);
    if (phyModule == cThaModuleMap)   return Tha60210051ModuleMapNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static ThaVersionReader VersionReaderCreate(ThaDevice self)
    {
    return Tha60210051VersionReaderNew((AtDevice)self);
    }

static eBool UseGlobalHold(Tha60210051Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportDistinctErdi(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x11, 0x16, 0x00);
    }

static uint32 StartVersionSupportNewPsn(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x10, 0x29, 0x30);
    }

static uint32 StartVersionSupportPowerControl(AtDevice device)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x21, 0x39);
    }

static eBool NeedCoreResetWhenDiagCheck(Tha60150011Device self)
    {
    /* FIXME: this should be enabled. Need debug before enabling this */
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldReActivate(Tha60210011Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 NumUsedOc48Slice(Tha60210011Device self)
    {
    return AtModuleSdhMaxLinesGet((AtModuleSdh)AtDeviceModuleGet((AtDevice)self, cAtModuleSdh));
    }

static void HwFlushLoPathLoopbackAndHoMap(ThaDevice self)
    {
    /* FIXME: this is bad, but necessary for debugging */
    /* LO path loopback */
    ThaDeviceMemoryFlush(self, 0x100005, 0x100005, 0x0);

    /* HO MAP */
    if (Tha60210011DeviceHasHoBus((Tha60210011Device)self))
        {
        ThaDeviceMemoryFlush(self, 0x60000, 0x607ff, 0x0);
        ThaDeviceMemoryFlush(self, 0x64000, 0x647ff, 0x0);
        }

    if (Tha60210011DeviceHasHoBus((Tha60210011Device)self))
        {
        ThaDeviceMemoryFlush(self, 0x300100, 0x30011f, 0x0);
        ThaDeviceMemoryFlush(self, 0x320800, 0x3208ff, 0x0);
        }
    }

static eBool UseModuleDefault(void)
    {
    return Tha602100xxHwAccessOptimized();
    }

static void HwFlushPdhSlice(Tha60210051Device self, uint8 slice)
    {
    ThaDevice device = (ThaDevice)self;
    uint32 offset = 0x100000UL * slice;

    ThaDeviceMemoryFlush(device, 0x1090002 + offset, 0x1090003 + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0x1030020 + offset, 0x103003f + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0x1024000 + offset, 0x10243ff + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0x1076000 + offset, 0x10763ff + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0x1040000 + offset, 0x104001f + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0x1080000 + offset, 0x108001f + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0x1041000 + offset, 0x10410ff + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0x1081000 + offset, 0x10810ff + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0x1054000 + offset, 0x10543ff + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0x1094000 + offset, 0x10943ff + offset, 0x0);
    }

static void HwFlushCdrSlice(Tha60210051Device self, uint8 slice)
    {
    ThaDevice device = (ThaDevice)self;
    uint32 offset = 0x40000UL * slice;

    ThaDeviceMemoryFlush(device, 0xc00800 + offset, 0xc0081f + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0xc00c00 + offset, 0xc00fff + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0xc20800 + offset, 0xc20bff + offset, 0x0);
    ThaDeviceMemoryFlush(device, 0xc21000 + offset, 0xc213ff + offset, 0x0);
    }

static void HwFlushPdhSlice0(ThaDevice self)
    {
    mMethodsGet(mThis(self))->HwFlushPdhSlice(mThis(self), 0);

    if (!UseModuleDefault())
        {
        ThaDeviceMemoryFlush(self, 0x800000, 0x800FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x801000, 0x801FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x802000, 0x802FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x803000, 0x803FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x810000, 0x8103FF, 0x0);
        ThaDeviceMemoryFlush(self, 0x814000, 0x814FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x815000, 0x815FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x816000, 0x816FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x817000, 0x817FFF, 0x0);
        }

    mMethodsGet(mThis(self))->HwFlushCdrSlice(mThis(self), 0);
    }

static void HwFlushPdhSlice1(ThaDevice self)
    {
    mMethodsGet(mThis(self))->HwFlushPdhSlice(mThis(self), 1);

    if (!UseModuleDefault())
        {
        ThaDeviceMemoryFlush(self, 0x840000, 0x840FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x841000, 0x841FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x842000, 0x842FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x843000, 0x843FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x850000, 0x8503FF, 0x0);
        ThaDeviceMemoryFlush(self, 0x854000, 0x854FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x855000, 0x855FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x856000, 0x856FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x857000, 0x857FFF, 0x0);
        }

    mMethodsGet(mThis(self))->HwFlushCdrSlice(mThis(self), 1);
    }

static void HwFlushPdhSlice2(ThaDevice self)
    {
    mMethodsGet(mThis(self))->HwFlushPdhSlice(mThis(self), 2);

    if (!UseModuleDefault())
        {
        ThaDeviceMemoryFlush(self, 0x880000, 0x880FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x881000, 0x881FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x882000, 0x882FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x883000, 0x883FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x890000, 0x8903FF, 0x0);
        ThaDeviceMemoryFlush(self, 0x894000, 0x894FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x895000, 0x895FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x896000, 0x896FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x897000, 0x897FFF, 0x0);
        }

    mMethodsGet(mThis(self))->HwFlushCdrSlice(mThis(self), 2);
    }

static void HwFlushPdhSlice3(ThaDevice self)
    {
    mMethodsGet(mThis(self))->HwFlushPdhSlice(mThis(self), 3);

    if (!UseModuleDefault())
        {
        ThaDeviceMemoryFlush(self, 0x8c0000, 0x8c0FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x8c1000, 0x8c1FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x8c2000, 0x8c2FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x8c3000, 0x8c3FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x8d0000, 0x8d03FF, 0x0);
        ThaDeviceMemoryFlush(self, 0x8d4000, 0x8d4FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x8d5000, 0x8d5FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x8d6000, 0x8d6FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x8d7000, 0x8d7FFF, 0x0);
        }

    mMethodsGet(mThis(self))->HwFlushCdrSlice(mThis(self), 3);
    }

static void HwFlushPdhSlice4(ThaDevice self)
    {
    mMethodsGet(mThis(self))->HwFlushPdhSlice(mThis(self), 4);

    if (!UseModuleDefault())
        {
        ThaDeviceMemoryFlush(self, 0x900000, 0x900FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x901000, 0x901FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x902000, 0x902FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x903000, 0x903FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x910000, 0x9103FF, 0x0);
        ThaDeviceMemoryFlush(self, 0x914000, 0x914FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x915000, 0x915FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x916000, 0x916FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x917000, 0x917FFF, 0x0);
        }

    mMethodsGet(mThis(self))->HwFlushCdrSlice(mThis(self), 4);
    }

static void HwFlushPdhSlice5(ThaDevice self)
    {
    mMethodsGet(mThis(self))->HwFlushPdhSlice(mThis(self), 5);

    if (!UseModuleDefault())
        {
        ThaDeviceMemoryFlush(self, 0x940000, 0x940FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x941000, 0x941FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x942000, 0x942FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x943000, 0x943FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x950000, 0x9503FF, 0x0);
        ThaDeviceMemoryFlush(self, 0x954000, 0x954FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x955000, 0x955FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x956000, 0x956FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x957000, 0x957FFF, 0x0);
        }

    mMethodsGet(mThis(self))->HwFlushCdrSlice(mThis(self), 5);
    }

static void HwFlushPdhSlice6(ThaDevice self)
    {
    mMethodsGet(mThis(self))->HwFlushPdhSlice(mThis(self), 6);

    if (!UseModuleDefault())
        {
        ThaDeviceMemoryFlush(self, 0x980000, 0x980FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x981000, 0x981FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x982000, 0x982FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x983000, 0x983FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x990000, 0x9903FF, 0x0);
        ThaDeviceMemoryFlush(self, 0x994000, 0x994FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x995000, 0x995FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x996000, 0x996FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x997000, 0x997FFF, 0x0);
        }

    mMethodsGet(mThis(self))->HwFlushCdrSlice(mThis(self), 6);
    }

static void HwFlushPdhSlice7(ThaDevice self)
    {
    mMethodsGet(mThis(self))->HwFlushPdhSlice(mThis(self), 7);

    if (!UseModuleDefault())
        {
        ThaDeviceMemoryFlush(self, 0x9c0000, 0x9c0FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x9c1000, 0x9c1FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x9c2000, 0x9c2FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x9c3000, 0x9c3FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x9d0000, 0x9d03FF, 0x0);
        ThaDeviceMemoryFlush(self, 0x9d4000, 0x9d4FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x9d5000, 0x9d5FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x9d6000, 0x9d6FFF, 0x0);
        ThaDeviceMemoryFlush(self, 0x9d7000, 0x9d7FFF, 0x0);
        }

    mMethodsGet(mThis(self))->HwFlushCdrSlice(mThis(self), 7);
    }

static void HwFlushPla0(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x40D000, 0x40D3FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x40D400, 0x40D7FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x41D000, 0x41D3FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x41D400, 0x41D7FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x42D000, 0x42D3FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x42D400, 0x42D7FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x43D000, 0x43D3FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x43D400, 0x43D7FF, 0x0);

    ThaDeviceMemoryFlush(self, 0x402000, 0x4023FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x406000, 0x4063FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x412000, 0x4123FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x416000, 0x4163FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x422000, 0x4223FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x426000, 0x4263FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x432000, 0x4323FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x436000, 0x4363FF, 0x0);

    ThaDeviceMemoryFlush(self, 0x480100, 0x48012F, 0x0);
    ThaDeviceMemoryFlush(self, 0x480300, 0x48032F, 0x0);
    ThaDeviceMemoryFlush(self, 0x480900, 0x48092F, 0x0);
    ThaDeviceMemoryFlush(self, 0x480B00, 0x480B2F, 0x0);
    }

static void HwFlushPla10(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x402800, 0x402BFF, 0x0);
    ThaDeviceMemoryFlush(self, 0x406800, 0x406BFF, 0x0);
    ThaDeviceMemoryFlush(self, 0x412800, 0x412BFF, 0x0);
    ThaDeviceMemoryFlush(self, 0x416800, 0x416BFF, 0x0);
    ThaDeviceMemoryFlush(self, 0x422800, 0x422BFF, 0x0);
    ThaDeviceMemoryFlush(self, 0x426800, 0x426BFF, 0x0);
    ThaDeviceMemoryFlush(self, 0x432800, 0x432BFF, 0x0);
    ThaDeviceMemoryFlush(self, 0x436800, 0x436BFF, 0x0);

    ThaDeviceMemoryFlush(self, 0x480680, 0x4806AF, 0x0);
    ThaDeviceMemoryFlush(self, 0x4806C0, 0x4806EF, 0x0);
    ThaDeviceMemoryFlush(self, 0x480E80, 0x480EAF, 0x0);
    ThaDeviceMemoryFlush(self, 0x480EC0, 0x480EEF, 0x0);
    }


static void HwFlushPla11(ThaDevice self)
    {
    ThaDeviceMemoryFlush(self, 0x480140, 0x48016F, 0x0);
    ThaDeviceMemoryFlush(self, 0x480340, 0x48036F, 0x0);
    ThaDeviceMemoryFlush(self, 0x480940, 0x48096F, 0x0);
    ThaDeviceMemoryFlush(self, 0x480B40, 0x480B6F, 0x0);

    ThaDeviceMemoryLongFlush(self, 0x490000, 0x490FFF);
    }

static void HwFlushPla12(ThaDevice self)
    {
    ThaDeviceMemoryLongFlush(self, 0x491000, 0x4917FF);
    ThaDeviceMemoryLongFlush(self, 0x4A0000, 0x4A0FFF);
    }

static void HwFlushPla13(ThaDevice self)
    {
    ThaDeviceMemoryLongFlush(self, 0x4A1000, 0x4A17FF);

    ThaDeviceMemoryFlush(self, 0x00494000, 0x004941FF, 0);
    ThaDeviceMemoryFlush(self, 0x00498000, 0x00498FFF, 0);
    }
    
static void HwFlushPda0(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x00510000, 0x00510FFF, 0);
    ThaDeviceMemoryFlush(self, 0x00511000, 0x00511FFF, 0);
    ThaDeviceMemoryFlush(self, 0x00514000, 0x00514FFF, 0);
    ThaDeviceMemoryFlush(self, 0x00515000, 0x00515FFF, 0);
    }

static void HwFlushPda1(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x00520000, 0x00520FFF, 0);
    ThaDeviceMemoryFlush(self, 0x00521000, 0x00521FFF, 0);
    ThaDeviceMemoryFlush(self, 0x00530000, 0x00530FFF, 0);
    ThaDeviceMemoryFlush(self, 0x00531000, 0x00531FFF, 0);
    }

static void HwFlushPda2(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x00532000, 0x00532FFF, 0);
    ThaDeviceMemoryFlush(self, 0x00533000, 0x00533FFF, 0);
    ThaDeviceMemoryFlush(self, 0x00534000, 0x00534FFF, 0);
    ThaDeviceMemoryFlush(self, 0x00535000, 0x00535FFF, 0);
    }

static void HwFlushPda3(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x00536000, 0x00536FFF, 0);
    ThaDeviceMemoryFlush(self, 0x00537000, 0x00537FFF, 0);
    ThaDeviceMemoryFlush(self, 0x00550000, 0x005501FF, 0);
    ThaDeviceMemoryFlush(self, 0x00560000, 0x00560FFF, 0);
    ThaDeviceMemoryFlush(self, 0x00561000, 0x00561FFF, 0);
    }

static void HwFlushCla00(ThaDevice self)
    {
    if (UseModuleDefault())
        return;

    ThaDeviceMemoryLongFlush(self, 0x680000, 0x680FFF);
    ThaDeviceMemoryLongFlush(self, 0x681000, 0x681FFF);
    }

static void HwFlushCla01(ThaDevice self)
    {
    if (UseModuleDefault())
        return;

    ThaDeviceMemoryLongFlush(self, 0x682000, 0x682FFF);
    ThaDeviceMemoryLongFlush(self, 0x683000, 0x683FFF);
    }

static void HwFlushCla10(ThaDevice self)
    {
    if (UseModuleDefault())
        return;

    ThaDeviceMemoryLongFlush(self, 0x690000, 0x690FFF);
    ThaDeviceMemoryLongFlush(self, 0x691000, 0x691FFF);
    }

static void HwFlushCla11(ThaDevice self)
    {
    if (UseModuleDefault())
        return;

    ThaDeviceMemoryLongFlush(self, 0x692000, 0x692FFF);
    ThaDeviceMemoryLongFlush(self, 0x693000, 0x693FFF);
    }

static void HwFlushCla20(ThaDevice self)
    {
    if (UseModuleDefault())
        return;

    ThaDeviceMemoryLongFlush(self, 0x6A0000, 0x6A0FFF);
    ThaDeviceMemoryLongFlush(self, 0x6A1000, 0x6A1FFF);
    }
    
static void HwFlushCla21(ThaDevice self)
    {
    if (UseModuleDefault())
        return;

    ThaDeviceMemoryLongFlush(self, 0x6A2000, 0x6A2FFF);
    ThaDeviceMemoryLongFlush(self, 0x6A3000, 0x6A3FFF);
    }
    
static void HwFlushCla30(ThaDevice self)
    {
    if (UseModuleDefault())
        return;

    ThaDeviceMemoryLongFlush(self, 0x6B0000, 0x6B0FFF);
    ThaDeviceMemoryLongFlush(self, 0x6B1000, 0x6B1FFF);
    }

static void HwFlushCla31(ThaDevice self)
    {
    if (UseModuleDefault())
        return;

    ThaDeviceMemoryLongFlush(self, 0x6B2000, 0x6B2FFF);
    ThaDeviceMemoryLongFlush(self, 0x6B3000, 0x6B3FFF);
    }

static void HwFlushCla4(ThaDevice self)
    {
    if (UseModuleDefault())
        return;

    ThaDeviceMemoryLongFlush(self, 0x670000, 0x670FFF);
    ThaDeviceMemoryLongFlush(self, 0x671000, 0x671FFF);
    }
    
static void HwFlushCla5(ThaDevice self)
    {
    if (UseModuleDefault())
        return;

    ThaDeviceMemoryLongFlush(self, 0x640000, 0x641FFF);
    }
    
static void HwFlushCla6(ThaDevice self)
    {
    if (UseModuleDefault())
        return;

    ThaDeviceMemoryLongFlush(self, 0x642000, 0x643FFF);
    }
    
static void HwFlushCla7(ThaDevice self)
    {
    if (UseModuleDefault())
        return;

    ThaDeviceMemoryLongFlush(self, 0x644000, 0x645FFF);
    }
    
static void HwFlushCla8(ThaDevice self)
    {
    if (UseModuleDefault())
        return;

    ThaDeviceMemoryLongFlush(self, 0x646000, 0x647FFF);
    }

static void HwFlushBerPoh0(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryLongFlush(self, 0x262000, 0x263FFF);
    ThaDeviceMemoryLongFlush(self, 0x260400, 0x2607FF);
    }

static void HwFlushBerPoh1(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x240400, 0x2407FF, 0x0);
    ThaDeviceMemoryFlush(self, 0x244000, 0x247FFF, 0x0);
    ThaDeviceMemoryFlush(self, 0x228000, 0x229FFF, 0x0);
    ThaDeviceMemoryFlush(self, 0x22A000, 0x22BFFF, 0x0);
    }
    
/* QDR Flush */
static void HwFlushBerPoh20(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x280000, 0x284FFF, 0x0);
    }

static void HwFlushBerPoh21(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x285000, 0x28AFFF, 0x0);
    }

static void HwFlushBerPoh22(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x28B000, 0x28FFFF, 0x0);
    }

static void HwFlushBerPoh30(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x290000, 0x294FFF, 0x0);
    }

static void HwFlushBerPoh31(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x295000, 0x29AFFF, 0x0);
    }

static void HwFlushBerPoh32(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x29B000, 0x29FFFF, 0x0);
    }

static void HwFlushBerPoh40(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x2A0000, 0x2A4FFF, 0x0);
    }

static void HwFlushBerPoh41(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x2A5000, 0x2AAFFF, 0x0);
    }

static void HwFlushBerPoh42(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x2AB000, 0x2AFFFF, 0x0);
    }

static void HwFlushBerPoh5(ThaDevice self)
    {
    if (Tha602100xxHwAccessOptimized())
        return;

    ThaDeviceMemoryFlush(self, 0x2B0000, 0x2B2FFF, 0x0);
    }

static void HwFlushPmc(Tha60210051Device self)
    {
    ThaDevice device = (ThaDevice)self;

    /* PMC */
    ThaDeviceMemoryFlush(device, 0x1E60000, 0x1E607FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x1E62000, 0x1E627FF, 0x0);
    ThaDeviceMemoryFlush(device, 0x1E64000, 0x1E644FF, 0x0);
    }

static void HwAsyncFlushPmc(ThaDevice self)
    {
    mMethodsGet(mThis(self))->HwFlushPmc(mThis(self));
    }

static eAtRet ClaHwFlush(Tha60210011Device device)
	{
	ThaDevice self = (ThaDevice)device;
	HwFlushCla00(self);
	HwFlushCla01(self);
	HwFlushCla10(self);
	HwFlushCla11(self);
	HwFlushCla20(self);
	HwFlushCla21(self);
	HwFlushCla30(self);
	HwFlushCla31(self);
	HwFlushCla4(self);
	HwFlushCla5(self);
	HwFlushCla6(self);
	HwFlushCla7(self);
	HwFlushCla8(self);
	return cAtOk;
	}

static eAtRet PdaHwFlush(Tha60210011Device device)
	{
	ThaDevice self = (ThaDevice)device;
	HwFlushPda0(self);
	HwFlushPda1(self);
	HwFlushPda2(self);
	HwFlushPda3(self);
	return cAtOk;
	}

static eAtRet PlaHwFlush(Tha60210011Device self)
	{
	ThaDevice device = (ThaDevice)self;
	HwFlushPla0(device);
	HwFlushPla10(device);
	HwFlushPla11(device);
	HwFlushPla12(device);
	HwFlushPla13(device);
	return cAtOk;
	}

static eAtRet PdhHwFlush(Tha60210011Device self)
    {
    ThaDevice device = (ThaDevice)self;
    HwFlushPdhSlice0(device);
    HwFlushPdhSlice1(device);
    HwFlushPdhSlice2(device);
    HwFlushPdhSlice3(device);
    HwFlushPdhSlice4(device);
    HwFlushPdhSlice5(device);
    HwFlushPdhSlice6(device);
    HwFlushPdhSlice7(device);
    return cAtOk;
    }

static eAtRet OcnHwFlush(Tha60210011Device self)
	{
	ThaDevice device = (ThaDevice)self;

	ThaDeviceMemoryFlush(device, 0x00160800, 0x00160DFF, 0);
	ThaDeviceMemoryFlush(device, 0x00164800, 0x00164DFF, 0);
	ThaDeviceMemoryFlush(device, 0x00168800, 0x00168DFF, 0);
	ThaDeviceMemoryFlush(device, 0x0016C800, 0x0016CDFF, 0);

	ThaDeviceMemoryFlush(device, 0x00123000, 0x0012302f, 0);
	ThaDeviceMemoryFlush(device, 0x00123200, 0x0012322f, 0);
	ThaDeviceMemoryFlush(device, 0x00123400, 0x0012342f, 0);
	ThaDeviceMemoryFlush(device, 0x00123600, 0x0012362f, 0);
	
	return cAtOk;
	}

static void HwFlushOcn(ThaDevice self)
	{
	Tha60210011Device device = (Tha60210011Device)self;
	mMethodsGet(device)->OcnHwFlush(device);
	}

static void HwFlush(Tha60150011Device self)
    {
    Tha60210011Device device = (Tha60210011Device)self;

    HwFlushLoPathLoopbackAndHoMap((ThaDevice)self);

    mMethodsGet(device)->PdhHwFlush(device);
    mMethodsGet(device)->PlaHwFlush(device);
    mMethodsGet(device)->ClaHwFlush(device);
    mMethodsGet(device)->PdaHwFlush(device);
    mMethodsGet(device)->OcnHwFlush(device);
    mMethodsGet(device)->PrbsHwFlush(device);
    mMethodsGet(device)->MapHwFlush(device);
    mMethodsGet(device)->AttHwFlush(device);
    HwFlushBerPoh0((ThaDevice)self);
    HwFlushBerPoh1((ThaDevice)self);
    HwFlushBerPoh20((ThaDevice)self);
    HwFlushBerPoh21((ThaDevice)self);
    HwFlushBerPoh22((ThaDevice)self);
    HwFlushBerPoh30((ThaDevice)self);
    HwFlushBerPoh31((ThaDevice)self);
    HwFlushBerPoh32((ThaDevice)self);
    HwFlushBerPoh40((ThaDevice)self);
    HwFlushBerPoh41((ThaDevice)self);
    HwFlushBerPoh42((ThaDevice)self);
    HwFlushBerPoh5((ThaDevice)self);
    mMethodsGet(mThis(self))->HwFlushPmc(mThis(self));
    }

static eBool ShouldResetBeforeInitializing(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ShouldRetryReset(ThaDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 ModuleActiveMask(ThaDevice self, uint32 moduleId)
    {
    if (moduleId == cThaModulePwe)
        return cBit20;
    return m_ThaDeviceMethods->ModuleActiveMask(self, moduleId);
    }

static uint32 ModuleActiveShift(ThaDevice self, uint32 moduleId)
    {
    if (moduleId == cThaModulePwe)
        return 20;
    return m_ThaDeviceMethods->ModuleActiveShift(self, moduleId);
    }

static eBool RamErrorGeneratorIsSupported(AtDevice self)
    {
    /*160406_4OC48_FUNC_6.4_10.01_L65*/
    uint32 startBuiltSupportErrorGenerator = ThaVersionReaderHardwareBuiltNumberBuild(0x1, 0x0, 0x01);/*textile: 10.01*/
    uint32 startVersionSupportErrorGenerator = ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x16, 0x04, 0x06, 0x64);

    if (AtDeviceVersionNumber(self) > startVersionSupportErrorGenerator)
        return cAtTrue;

    if (AtDeviceVersionNumber(self) == startVersionSupportErrorGenerator)
        return (AtDeviceBuiltNumber(self) >= startBuiltSupportErrorGenerator) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static eBool ClockStateValueV2IsSupported(ThaDevice self)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader((AtDevice)self);
    uint32 hwVersion = ThaVersionReaderHardwareVersion(versionReader);
    uint32 startVersion1 = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x2, 0x0000);
    uint32 startVersion2 = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x8, 0x0, 0x0000);
    uint32 startVersion3 = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x8, 0x1, 0x0000);

    if (hwVersion < startVersion1)
        return cAtFalse;

    if ((hwVersion >= startVersion2) && (hwVersion < startVersion3))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet HwAsyncFlush(Tha60210011Device self)
    {
    return Tha60210051DeviceHwAsyncFlush((ThaDevice)self);
    }

static uint32 StartBuiltNumberSupportPowerControl(AtDevice device)
    {
    AtUnused(device);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x1, 0x0, 0x8);
    }

static eBool SerdesPowerControlIsSupported(ThaDevice self)
    {
    AtDevice device = (AtDevice)self;
    if (AtDeviceVersionNumber(device) > Tha60210051DeviceStartVersionSupportPowerControl(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) == Tha60210051DeviceStartVersionSupportPowerControl(device))
       return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportPowerControl(device)) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(asyncInitHwFlushState);
    }

static uint32 StartVersionApplyNewReset(Tha60210051Device self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x4, 0x1000);
    }

static eBool ShouldApplyNewReset(Tha60210011Device self)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader((AtDevice)self);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersionApplyNewReset = mMethodsGet(mThis(self))->StartVersionApplyNewReset(mThis(self));

    return (hwVersion >= startVersionApplyNewReset) ? cAtTrue : cAtFalse;
    }

static eBool ShouldCheckHwResource(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ErrorGeneratorIsSupported(Tha60210051Device self)
    {
    AtDevice device = (AtDevice)self;
    uint32 startBuiltSupportErrorGenerator = ThaVersionReaderHardwareBuiltNumberBuild(0x1, 0x0, 0x01);/*textile: 10.01*/
    uint32 startVersionSupportErrorGenerator = ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x16, 0x04, 0x06, 0x64);

    if (AtDeviceVersionNumber(device) > startVersionSupportErrorGenerator)
        return cAtTrue;

    if (AtDeviceVersionNumber(device) == startVersionSupportErrorGenerator)
        return (AtDeviceBuiltNumber(device) >= startBuiltSupportErrorGenerator) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static void MethodsInit(Tha60210051Device self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, UseGlobalHold);
        mMethodOverride(m_methods, StartVersionSupportDistinctErdi);
        mMethodOverride(m_methods, StartVersionSupportNewPsn);
        mMethodOverride(m_methods, StartVersionSupportPowerControl);
        mMethodOverride(m_methods, StartVersionApplyNewReset);
        mMethodOverride(m_methods, HwFlushPdhSlice);
        mMethodOverride(m_methods, HwFlushCdrSlice);
        mMethodOverride(m_methods, HwFlushPmc);
        mMethodOverride(m_methods, ErrorGeneratorIsSupported);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtDevice self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideTha6015011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, NeedCoreResetWhenDiagCheck);
        mMethodOverride(m_Tha60150011DeviceOverride, HwFlush);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, ShouldResetBeforeInitializing);
        mMethodOverride(m_AtDeviceOverride, ShouldCheckHwResource);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, m_ThaDeviceMethods, sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, VersionReaderCreate);
        mMethodOverride(m_ThaDeviceOverride, ShouldRetryReset);
        mMethodOverride(m_ThaDeviceOverride, ModuleActiveMask);
        mMethodOverride(m_ThaDeviceOverride, ModuleActiveShift);
        mMethodOverride(m_ThaDeviceOverride, SerdesPowerControlIsSupported);
        mMethodOverride(m_ThaDeviceOverride, ClockStateValueV2IsSupported);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideTha60210011Device(AtDevice self)
    {
    Tha60210011Device device = (Tha60210011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210011DeviceOverride));

        mMethodOverride(m_Tha60210011DeviceOverride, ShouldReActivate);
        mMethodOverride(m_Tha60210011DeviceOverride, NumUsedOc48Slice);
        mMethodOverride(m_Tha60210011DeviceOverride, PdhHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, ClaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, PdaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, PlaHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, OcnHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, RamErrorGeneratorIsSupported);
        mMethodOverride(m_Tha60210011DeviceOverride, HwAsyncFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, ShouldApplyNewReset);
        }

    mMethodsSet(device, &m_Tha60210011DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtObject(self);
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha6015011Device(self);
    OverrideTha60210011Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051Device);
    }

AtDevice Tha60210051DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((Tha60210051Device)self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60210051DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051DeviceObjectInit(newDevice, driver, productCode);
    }

eBool Tha60210051DeviceUseGlobalHold(Tha60210051Device self)
    {
    if (self)
        return mMethodsGet(self)->UseGlobalHold(self);
    return cAtFalse;
    }

uint32 Tha60210051DeviceStartVersionSupportNewPsn(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->StartVersionSupportNewPsn(self);
    return cBit31_0;
    }

uint32 Tha60210051DeviceStartVersionSupportDistinctErdi(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->StartVersionSupportDistinctErdi(self);
    return cBit31_0;
    }

uint32 Tha60210051DeviceStartVersionSupportPowerControl(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->StartVersionSupportPowerControl(self);
    return cBit31_0;
    }

eBool Tha60210051DeviceEthErrorGeneratorIsSupported(AtDevice self)
    {
    if (self)
    	return mMethodsGet(mThis(self))->ErrorGeneratorIsSupported(mThis(self));
    return cAtFalse;
    }

tHwAsynFlushFunc *Tha60210051DeviceHwAsyncFlushFunctions(uint32 *numFunctions)
    {
    static tHwAsynFlushFunc funcList[] = {HwFlushLoPathLoopbackAndHoMap, HwFlushPdhSlice0,
                                          HwFlushPdhSlice1, HwFlushPdhSlice2,
                                          HwFlushPdhSlice3, HwFlushPdhSlice4,
                                          HwFlushPdhSlice5, HwFlushPdhSlice6,
                                          HwFlushPdhSlice7, HwFlushPla0,
                                          HwFlushPla10, HwFlushPla11,
                                          HwFlushPla12, HwFlushPla13,
                                          HwFlushPda0, HwFlushPda1,
                                          HwFlushPda2, HwFlushPda3,
                                          HwFlushOcn, HwFlushCla00,
                                          HwFlushCla01, HwFlushCla10,
                                          HwFlushCla11, HwFlushCla20,
                                          HwFlushCla21, HwFlushCla30,
                                          HwFlushCla31, HwFlushCla4,
                                          HwFlushCla5, HwFlushCla6,
                                          HwFlushCla7, HwFlushCla8,
                                          HwFlushBerPoh0, HwFlushBerPoh1,
                                          HwFlushBerPoh20, HwFlushBerPoh21,
                                          HwFlushBerPoh22, HwFlushBerPoh30,
                                          HwFlushBerPoh31, HwFlushBerPoh32,
                                          HwFlushBerPoh40, HwFlushBerPoh41,
                                          HwFlushBerPoh42, HwFlushBerPoh5,
                                          HwAsyncFlushPmc};
    	*numFunctions = mCount(funcList);
    	
    	return funcList;
    }

uint32 Tha60210051DeviceStartVersionApplyNewReset(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->StartVersionApplyNewReset(mThis(self));
    return cInvalidUint32;
    }


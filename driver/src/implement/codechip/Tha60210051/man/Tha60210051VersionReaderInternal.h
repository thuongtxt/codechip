/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210051VersionReaderInternal.h
 * 
 * Created Date: Jan 21, 2017
 *
 * Description : Version reader
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051VERSIONREADERINTERNAL_H_
#define _THA60210051VERSIONREADERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/man/Tha60210011VersionReader.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051VersionReader
    {
    tTha60210011VersionReader super;
    }tTha60210051VersionReader;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaVersionReader Tha60210051VersionReaderObjectInit(ThaVersionReader self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051VERSIONREADERINTERNAL_H_ */


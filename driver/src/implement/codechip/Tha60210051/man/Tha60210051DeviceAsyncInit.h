/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60210051DeviceAsyncInit.h
 * 
 * Created Date: Aug 16, 2016
 *
 * Description : Async init
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051DEVICEASYNCINIT_H_
#define _THA60210051DEVICEASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef void (*tHwAsynFlushFunc)(ThaDevice self);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60210051DeviceHwAsyncFlush(ThaDevice self);
tHwAsynFlushFunc *Tha60210051DeviceHwAsyncFlushFunctions(uint32 *numFunctions);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051DEVICEASYNCINIT_H_ */


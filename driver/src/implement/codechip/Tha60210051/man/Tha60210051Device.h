/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DEVICE
 * 
 * File        : Tha60210051Device.h
 * 
 * Created Date: Sep 30, 2015
 *
 * Description : Device management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051DEVICE_H_
#define _THA60210051DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051Device * Tha60210051Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60210051DeviceUseGlobalHold(Tha60210051Device self);

uint32 Tha60210051DeviceStartVersionSupportNewPsn(AtDevice self);
uint32 Tha60210051DeviceStartVersionSupportJitterCentering(AtDevice self);
uint32 Tha60210051DeviceStartVersionSupportDistinctErdi(AtDevice self);
uint32 Tha60210051DeviceStartVersionSupportPowerControl(AtDevice self);
eBool Tha60210051DeviceEthErrorGeneratorIsSupported(AtDevice self);
uint32 Tha60210051DeviceStartVersionApplyNewReset(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051DEVICE_H_ */


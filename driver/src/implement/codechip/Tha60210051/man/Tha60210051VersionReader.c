/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210051VersionReader.c
 *
 * Created Date: Sep 30, 2015
 *
 * Description : Version reader of 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60210051VersionReaderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaVersionReaderMethods m_ThaVersionReaderOverride;
static tTha60210011VersionReaderMethods m_Tha60210011VersionReaderOverride;

/* Save super implementation */
static const tThaVersionReaderMethods *m_ThaVersionReaderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasInternalBuiltNumber(ThaVersionReader self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtHal Hal(ThaVersionReader self)
    {
    AtDevice device = ThaVersionReaderDeviceGet(self);
    return AtDeviceIpCoreHalGet(device, 0);
    }

static uint32 BuiltNumberRead(ThaVersionReader self)
    {
    static const uint32 cBuiltNumberRegister = 0x3;
    AtHal hal = Hal(self);

    if (hal == NULL)
        return 0;

    return AtHalRead(hal, cBuiltNumberRegister);
    }

static uint32 VersionNumberRead(ThaVersionReader self)
    {
    static const uint32 cVersionRegister = 0x2;
    AtHal hal = Hal(self);
    return AtHalRead(hal, cVersionRegister);
    }

static uint32 VersionNumberFromHwGet(ThaVersionReader self)
    {
    return VersionNumberRead(self);
    }

static uint32 BuiltNumberFromHwGet(ThaVersionReader self)
    {
    return BuiltNumberRead(self);
    }

static uint32 StartVersionIgnoreDate(ThaVersionReader self)
    {
    AtUnused(self);
    return 0x15120235;
    }

static uint32 StartBuiltNumberIgnoreDate(ThaVersionReader self)
    {
    AtUnused(self);
    return 0x1017;
    }

static void OverrideTha60210011VersionReader(ThaVersionReader self)
    {
    Tha60210011VersionReader reader = (Tha60210011VersionReader)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011VersionReaderOverride, mMethodsGet(reader), sizeof(m_Tha60210011VersionReaderOverride));

        mMethodOverride(m_Tha60210011VersionReaderOverride, StartVersionIgnoreDate);
        mMethodOverride(m_Tha60210011VersionReaderOverride, StartBuiltNumberIgnoreDate);
        }

    mMethodsSet(reader, &m_Tha60210011VersionReaderOverride);
    }

static void OverrideThaVersionReader(ThaVersionReader self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaVersionReaderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVersionReaderOverride, m_ThaVersionReaderMethods, sizeof(m_ThaVersionReaderOverride));

        mMethodOverride(m_ThaVersionReaderOverride, BuiltNumberRead);
        mMethodOverride(m_ThaVersionReaderOverride, HasInternalBuiltNumber);
        mMethodOverride(m_ThaVersionReaderOverride, VersionNumberRead);
        mMethodOverride(m_ThaVersionReaderOverride, VersionNumberFromHwGet);
        mMethodOverride(m_ThaVersionReaderOverride, BuiltNumberFromHwGet);
        }

    mMethodsSet(self, &m_ThaVersionReaderOverride);
    }

static void Override(ThaVersionReader self)
    {
    OverrideThaVersionReader(self);
    OverrideTha60210011VersionReader(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051VersionReader);
    }

ThaVersionReader Tha60210051VersionReaderObjectInit(ThaVersionReader self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (device == NULL)
        return NULL;

    /* Super constructor */
    if (Tha60210011VersionReaderObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaVersionReader Tha60210051VersionReaderNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaVersionReader newReader = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newReader == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051VersionReaderObjectInit(newReader, device);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60210051DeviceAsyncInit.c
 *
 * Created Date: Aug 16, 2016
 *
 * Description : Async init
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210051DeviceInternal.h"
#include "Tha60210051DeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210051Device)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AsyncHwFlushStateSet(ThaDevice self, uint32 state)
    {
    mThis(self)->asyncInitHwFlushState = state;
    }

static uint32 AsyncInitStateGet(ThaDevice self)
    {
    return mThis(self)->asyncInitHwFlushState;
    }

eAtRet Tha60210051DeviceHwAsyncFlush(ThaDevice self)
    {
    eAtRet ret;
    uint32 state = AsyncInitStateGet(self);
    uint32 numFunctions;
    tHwAsynFlushFunc *functions = Tha60210051DeviceHwAsyncFlushFunctions(&numFunctions);
    tAtOsalCurTime profileTime;
    char stateString[64];

    AtSprintf(stateString, "Tha60210051DeviceHwAsyncFlush, state: %d", state);
    AtOsalCurTimeGet(&profileTime);
    functions[state](self);
    AtDeviceAccessTimeCountSinceLastProfileCheck((AtDevice)self, cAtTrue, AtSourceLocation, stateString);
    AtDeviceAccessTimeInMsSinceLastProfileCheck((AtDevice)self, cAtTrue, &profileTime, AtSourceLocation, stateString);

    state += 1;
    if (state == numFunctions)
        {
        state = 0;
        ret = cAtOk;
        }
    else
        ret = cAtErrorAgain;

    AsyncHwFlushStateSet(self, state);
    return ret;
    }

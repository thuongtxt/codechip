/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60210051ModuleBer.c
 *
 * Created Date: Nov 10, 2015
 *
 * Description : Module BER of 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/Tha60210051Device.h"
#include "../poh/Tha60210051ModulePoh.h"
#include "Tha60210051ModuleBerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods       m_AtModuleOverride;

/* Save super implementations */
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x20000A, 0x20000B};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters;

    if (Tha60210051DeviceUseGlobalHold((Tha60210051Device)AtModuleDeviceGet(self)))
        return m_AtModuleMethods->LongRegisterAccessCreate(self);

    holdRegisters = AtModuleHoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static void OverrideAtModule(AtModuleBer self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleBer self)
    {
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ModuleBer);
    }

AtModuleBer Tha60210051ModuleBerObjectInit(AtModuleBer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleBerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleBer Tha60210051ModuleBerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleBer newBerModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBerModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051ModuleBerObjectInit(newBerModule, device);
    }

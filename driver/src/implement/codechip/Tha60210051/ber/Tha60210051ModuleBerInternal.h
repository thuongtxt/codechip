/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60210051ModuleBerInternal.h
 * 
 * Created Date: Jul 13, 2016
 *
 * Description : Module BER representation header.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULEBERINTERNAL_H_
#define _THA60210051MODULEBERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/ber/Tha60210011ModuleBer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051ModuleBer
    {
    tTha60210011ModuleBer super;
    }tTha60210051ModuleBer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleBer Tha60210051ModuleBerObjectInit(AtModuleBer self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULEBERINTERNAL_H_ */


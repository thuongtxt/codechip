/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60210051ModuleMap.c
 *
 * Created Date: Jul 27, 2016
 *
 * Description : Module MAP of product 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/map/Tha60210011ModuleMapInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210051ModuleMap
    {
    tTha60210011ModuleMap super;
    }tTha60210051ModuleMap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods         m_AtModuleOverride;

/* Save super implementation */
static const tAtModuleMethods   *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "MAPHO Thalassa Map Line Control Slice 0",
         "MAPHO Thalassa Map Line Control Slice 1",
         "MAPHO Thalassa Map Line Control Slice 2",
         "MAPHO Thalassa Map Line Control Slice 3",

         "MAPHO Thalassa Demap Line Control Slice 0",
         "MAPHO Thalassa Demap Line Control Slice 1",
         "MAPHO Thalassa Demap Line Control Slice 2",
         "MAPHO Thalassa Demap Line Control Slice 3",

         "MAPLO Thalassa Map Channel Control slice 0",
         "MAPLO Thalassa Map Line Control slice 0",
         "MAPLO Thalassa Demap Channel Control slice 0",

         "MAPLO Thalassa Map Channel Control slice 1",
         "MAPLO Thalassa Map Line Control slice 1",
         "MAPLO Thalassa Demap Channel Control slice 1",

         "MAPLO Thalassa Map Channel Control slice 2",
         "MAPLO Thalassa Map Line Control slice 2",
         "MAPLO Thalassa Demap Channel Control slice 2",

         "MAPLO Thalassa Map Channel Control slice 3",
         "MAPLO Thalassa Map Line Control slice 3",
         "MAPLO Thalassa Demap Channel Control slice 3",

         "MAPLO Thalassa Map Channel Control slice 4",
         "MAPLO Thalassa Map Line Control slice 4",
         "MAPLO Thalassa Demap Channel Control slice 4",

         "MAPLO Thalassa Map Channel Control slice 5",
         "MAPLO Thalassa Map Line Control slice 5",
         "MAPLO Thalassa Demap Channel Control slice 5",

         "MAPLO Thalassa Map Channel Control slice 6",
         "MAPLO Thalassa Map Line Control slice 6",
         "MAPLO Thalassa Demap Channel Control slice 6",

         "MAPLO Thalassa Map Channel Control slice 7",
         "MAPLO Thalassa Map Line Control slice 7",
         "MAPLO Thalassa Demap Channel Control slice 7",
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(self), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ModuleMap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210051ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

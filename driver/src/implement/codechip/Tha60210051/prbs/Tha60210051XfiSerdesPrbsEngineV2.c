/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210051XfiSerdesPrbsEngineV2.c
 *
 * Created Date: Oct 22, 2015
 *
 * Description : Tha60210051 XFI PRBS Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210051XfiSerdesPrbsEngineV2Reg.h"
#include "../../Tha60210011/prbs/Tha6021XfiSerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mDebugCountersShow(infoMessage, regOffset, isGoodPkt)                                               \
    {                                                                                                       \
    regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset((AtPrbsEngine)self, regOffset);    \
    regValue   = AtPrbsEngineRead((AtPrbsEngine)self, regAddress, cAtModulePrbs);                           \
    AtPrintc(cSevNormal, "%s ", infoMessage);                                                               \
    if (isGoodPkt)                                                                                          \
        AtPrintc(regValue ? cSevInfo : cSevNormal, "%u\r\n", regValue);                                     \
    else                                                                                                    \
        AtPrintc(regValue ? cSevCritical : cSevNormal, "%u\r\n", regValue);                                 \
    }

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210051XfiSerdesPrbsEngineV2
    {
    tTha6021XfiSerdesPrbsEngine super;
    }tTha60210051XfiSerdesPrbsEngineV2;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6021XfiSerdesPrbsEngineMethods m_Tha6021EthPortSerdesPrbsEngineOverride;
static tAtPrbsEngineMethods               m_AtPrbsEngineOverride;

/* Save super implementation */
static const tTha6021XfiSerdesPrbsEngineMethods *m_Tha6021EthPortSerdesPrbsEngineMethods = NULL;
static const tAtPrbsEngineMethods               *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePrbsRet DefaultSet(Tha6021XfiSerdesPrbsEngine self)
    {
    uint32 regAddress, regValue;
    AtPrbsEngine engine = (AtPrbsEngine)self;

    regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsPatternGenCfg);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cXfiPrbsPatternGenCfgPktGenNum, 1);
    mRegFieldSet(regValue, cXfiPrbsPatternGenCfgLenMode, 0);
    mRegFieldSet(regValue, cXfiPrbsPatternGenCfgDaMode, 0);
    AtPrbsEngineWrite(engine, regAddress, regValue, cAtModulePrbs);

    regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsPktLenCfg);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cXfiPrbsPktLenCfgMax, 1500);
    mRegFieldSet(regValue, cXfiPrbsPktLenCfgMin, 64);
    AtPrbsEngineWrite(engine, regAddress, regValue, cAtModulePrbs);

    regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsRxPktCfg);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cXfiPrbsRxPktCfgFcsDrop, 1);
    AtPrbsEngineWrite(engine, regAddress, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eBool EngineIsWorking(AtPrbsEngine self)
    {
    return (AtPrbsEngineIsEnabled(self)) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    AtPrbsEngineModeSet(self, cAtPrbsModePrbs31);
    return ret;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    uint32 regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsPatternGenCfg);
    uint32 regValue = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cXfiPrbsPatternGenCfgErrForce, (force) ? 1 : 0);
    AtPrbsEngineWrite(engine, regAddress, regValue, cAtModulePrbs);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    uint32 regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsPatternGenCfg);
    uint32 regValue = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    return (mRegField(regValue, cXfiPrbsPatternGenCfgErrForce)) ? cAtTrue : cAtFalse;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 regAddress, regValue;
    AtPrbsEngine engine = (AtPrbsEngine)self;

    /* Need to clear some counters for checking
     * loss sync status if their values are full */
    Tha6021XfiSerdesPrbsEngineClearFulledCounters(self);

    if (Tha6021XfiSerdesPrbsEngineNothingReceived(self))
        return cAtPrbsEngineAlarmTypeLossSync;

    regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsRxPktErrStatus);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    if (regValue & cXfiPrbsRxPktErrStatusMask)
        return cAtPrbsEngineAlarmTypeError;

    return cAtPrbsEngineAlarmTypeNone;
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    uint32 regAddress, regValue;
    AtPrbsEngine engine = (AtPrbsEngine)self;
    uint32 alarms;

    regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsRxPktErrStk);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    alarms     = (regValue & cXfiPrbsRxPktErrStkLossSyncMask) ? cAtPrbsEngineAlarmTypeError : cAtPrbsEngineAlarmTypeNone;

    AtPrbsEngineWrite(engine, regAddress, regValue, cAtModulePrbs);

    return alarms;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddress, regValue;
    AtPrbsEngine engine = (AtPrbsEngine)self;

    regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsPatternGenCfg);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cXfiPrbsPatternGenCfgGenEn, (enable) ? 1 : 0);
    AtPrbsEngineWrite(engine, regAddress, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regAddress, regValue;
    AtPrbsEngine engine = (AtPrbsEngine)self;

    regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsPatternGenCfg);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);

    return (regValue & cXfiPrbsPatternGenCfgGenEnMask) ? cAtTrue : cAtFalse;
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    if (prbsMode == cAtPrbsModePrbsFixedPattern4Bytes)
        return cAtTrue;

    if (prbsMode == cAtPrbsModePrbs31)
        return cAtTrue;

    return cAtFalse;
    }

static uint8 PrbsModeSw2Hw(eAtPrbsMode prbsMode)
    {
    if (prbsMode == cAtPrbsModePrbsFixedPattern4Bytes)      return 0x1;
    if (prbsMode == cAtPrbsModePrbs31)                      return 0x2;

    return 0x0;
    }

static eAtPrbsMode PrbsModeHw2Sw(uint8 prbsMode)
    {
    if (prbsMode == 0x1)      return cAtPrbsModePrbsFixedPattern4Bytes;
    if (prbsMode == 0x2)      return cAtPrbsModePrbs31;

    return cAtPrbsModeInvalid;
    }


static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eAtRet ret = AtPrbsEngineTxModeSet(self, prbsMode);
    ret |= AtPrbsEngineRxModeSet(self, prbsMode);
    return ret;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineTxModeGet(self);
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddress, regValue;
    AtPrbsEngine engine = (AtPrbsEngine)self;

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsPktDataCfg);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cXfiPrbsPktDataCfgMode, PrbsModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(engine, regAddress, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    uint32 regAddress, regValue;
    AtPrbsEngine engine = (AtPrbsEngine)self;

    regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsPktDataCfg);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);

    return PrbsModeHw2Sw((uint8)mRegField(regValue, cXfiPrbsPktDataCfgMode));
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddress, regValue;
    AtPrbsEngine engine = (AtPrbsEngine)self;

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsRxPktCfg);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    mRegFieldSet(regValue, cXfiPrbsRxPktCfgMode, PrbsModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(engine, regAddress, regValue, cAtModulePrbs);

    return cAtOk;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    uint32 regAddress, regValue;
    AtPrbsEngine engine = (AtPrbsEngine)self;

    regAddress = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsRxPktCfg);
    regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);

    return PrbsModeHw2Sw((uint8)mRegField(regValue, cXfiPrbsRxPktCfgMode));
    }

static void DebugCountersShow(AtPrbsEngine self)
    {
    uint32 regAddress, regValue;

    if (!EngineIsWorking(self))
        return;

    AtPrintc(cSevInfo, "Debug counters (r2c mode):\r\n");

    mDebugCountersShow("Rx FCS error Pkt    :", cXfiPrbsRxFcsErrPkt(cAtTrue),  cAtFalse);
    mDebugCountersShow("Rx Length error Pkt :", cXfiPrbsRxLenErrPkt(cAtTrue),  cAtFalse);
    mDebugCountersShow("Rx Data error Pkt   :", cXfiPrbsRxDataErrPkt(cAtTrue), cAtFalse);
    mDebugCountersShow("Rx Len0_64 Pkt      :", cXfiPrbsRx0_64LenPkt,          cAtTrue);
    mDebugCountersShow("Rx Len65_128 Pkt    :", cXfiPrbsRx65_128LenPkt,        cAtTrue);
    mDebugCountersShow("Rx Len129_256 Pkt   :", cXfiPrbsRx129_256LenPkt,       cAtTrue);
    mDebugCountersShow("Rx Len257_512 Pkt   :", cXfiPrbsRx257_512LenPkt,       cAtTrue);
    mDebugCountersShow("Rx Len513_1024 Pkt  :", cXfiPrbsRx513_1024LenPkt,      cAtTrue);
    mDebugCountersShow("Rx Len1025_2048 Pkt :", cXfiPrbsRx1025_2048LenPkt,     cAtTrue);
    }

static void Debug(AtPrbsEngine self)
    {
    DebugCountersShow(self);
    }

static uint32 CounterOffset(uint16 counterType, eBool r2c)
    {
    if (counterType == cAtPrbsEngineCounterTxFrame)
        return cXfiPrbsGenPktCounter(r2c);
    if (counterType == cAtPrbsEngineCounterRxFrame)
        return cXfiPrbsRxPktCounters(r2c);
    if (counterType == cAtPrbsEngineCounterRxSync)
        return cXfiPrbsRxGoodPkt(r2c);

    return cBit31_0;
    }

static uint32 CounterAddress(AtPrbsEngine self, uint16 counterType, eBool r2c)
    {
    uint32 offset = CounterOffset(counterType, r2c);
    if (offset == cBit31_0)
        return offset;
    return Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(self, offset);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    if ((counterType == cAtPrbsEngineCounterRxLostFrame) ||
        (counterType == cAtPrbsEngineCounterRxSync))
        return cAtTrue;

    return m_AtPrbsEngineMethods->CounterIsSupported(self, counterType);
    }

static uint32 RxLostFrameCounters(Tha6021XfiSerdesPrbsEngine self, eBool r2c)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    uint32 regAddr = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsRxFcsErrPkt(r2c));
    uint32 totalLostCounters = AtPrbsEngineRead(engine, regAddr, cAtModulePrbs);

    regAddr = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsRxDataErrPkt(r2c));
    totalLostCounters += AtPrbsEngineRead(engine, regAddr, cAtModulePrbs);

    regAddr = Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(engine, cXfiPrbsRxLenErrPkt(r2c));
    return totalLostCounters + AtPrbsEngineRead(engine, regAddr, cAtModulePrbs);
    }

static uint32 HwCounterReadToClear(Tha6021XfiSerdesPrbsEngine self, uint16 counterType, eBool r2c)
    {
    uint32 regAddress;
    AtPrbsEngine engine = (AtPrbsEngine)self;

    if (counterType == cAtPrbsEngineCounterRxLostFrame)
        return RxLostFrameCounters(self, r2c);

    regAddress = CounterAddress(engine, counterType, r2c);
    return AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    }

static void OverrideTha6021EthPortSerdesPrbsEngine(AtPrbsEngine self)
    {
    Tha6021XfiSerdesPrbsEngine engine = (Tha6021XfiSerdesPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6021EthPortSerdesPrbsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortSerdesPrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6021EthPortSerdesPrbsEngineOverride));

        mMethodOverride(m_Tha6021EthPortSerdesPrbsEngineOverride, DefaultSet);
        mMethodOverride(m_Tha6021EthPortSerdesPrbsEngineOverride, HwCounterReadToClear);
        }

    mMethodsSet(engine, &m_Tha6021EthPortSerdesPrbsEngineOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, Debug);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6021EthPortSerdesPrbsEngine(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051XfiSerdesPrbsEngineV2);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021XfiSerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210051XfiSerdesPrbsEngineV2New(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, serdesController);
    }

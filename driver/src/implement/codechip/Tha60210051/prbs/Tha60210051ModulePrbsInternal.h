/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210051ModulePrbsInternal.h
 *
 * Created Date: Mar 23, 2016
 *
 * Description : PRBS module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULEPRBSINTERNAL_H_
#define _THA60210051MODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/prbs/Tha60210011ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051ModulePrbs
    {
    tTha60210011ModulePrbs super;
    }tTha60210051ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha60210051ModulePrbsObjectInit(AtModulePrbs self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULEPRBSINTERNAL_H_ */

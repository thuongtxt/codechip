/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210011XfiSerdesPrbsEngine.c
 *
 * Created Date: Oct 20, 2015
 *
 * Description : XFI SERDES PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/prbs/Tha6021XfiSerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cFrameCountersMask  cBit15_0
#define cFrameCountersShift 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210051XfiSerdesPrbsEngine
    {
    tTha6021XfiSerdesPrbsEngine super;
    }tTha60210051XfiSerdesPrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6021XfiSerdesPrbsEngineMethods m_Tha6021EthPortSerdesPrbsEngineOverride;

/* Save super implementation */
static const tTha6021XfiSerdesPrbsEngineMethods *m_Tha6021EthPortSerdesPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterAddress(AtPrbsEngine self, uint16 counterType, eBool r2c)
    {
    AtUnused(r2c);

    if (counterType == cAtPrbsEngineCounterTxFrame)
        return Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(self, 0x65);
    if (counterType == cAtPrbsEngineCounterRxFrame)
        return Tha6021EthPortSerdesPrbsEngineRegisterAddressWithOffset(self, 0x66);

    /* Should never happen */
    return cBit31_0;
    }

static uint32 HwCounterReadToClear(Tha6021XfiSerdesPrbsEngine self, uint16 counterType, eBool r2c)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    uint32 regAddress = CounterAddress(engine, counterType, r2c);
    uint32 regValue   = AtPrbsEngineRead(engine, regAddress, cAtModulePrbs);
    return regValue & cFrameCountersMask;
    }

static void OverrideTha6021EthPortSerdesPrbsEngine(AtPrbsEngine self)
    {
    Tha6021XfiSerdesPrbsEngine engine = (Tha6021XfiSerdesPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6021EthPortSerdesPrbsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6021EthPortSerdesPrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha6021EthPortSerdesPrbsEngineOverride));

        mMethodOverride(m_Tha6021EthPortSerdesPrbsEngineOverride, HwCounterReadToClear);
        }

    mMethodsSet(engine, &m_Tha6021EthPortSerdesPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha6021EthPortSerdesPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051XfiSerdesPrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021XfiSerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210051XfiSerdesPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, serdesController);
    }

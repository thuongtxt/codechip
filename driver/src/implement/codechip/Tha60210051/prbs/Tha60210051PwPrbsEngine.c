/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210051PwPrbsEngine.c
 *
 * Created Date: Oct 20, 2015
 *
 * Description : Tha60210051 PRBS Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../codechip/Tha60210021/prbs/Tha60210021PwPrbsEngineInternal.h"
#include "../../../default/prbs/ThaPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)     ((tTha60210051PwPrbsEngine *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210051PwPrbsEngine
    {
    tTha60210021PwPrbsEngine super;
    }tTha60210051PwPrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Supper implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051PwPrbsEngine);
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    AtUnused(self);
    return (force) ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static AtPrbsEngine Tha60210051PwPrbsEngineObjectInit(AtPrbsEngine self, AtPw pw, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210021PwPrbsEngineObjectInit(self, pw, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60210051PwPrbsEngineNew(AtPw pw, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051PwPrbsEngineObjectInit(newEngine, pw, engineId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210051ModulePrbs.c
 *
 * Created Date: Oct 20, 2015
 *
 * Description : Tha60210051 PRBS Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhChannel.h"
#include "AtPdhNxDs0.h"
#include "../../../default/prbs/ThaPrbsEngine.h"
#include "../../../default/prbs/ThaPrbsRegProvider.h"
#include "../../../codechip/Tha60210011/pw/Tha60210011ModulePw.h"
#include "../../../codechip/Tha60210011/sdh/Tha60210011ModuleSdh.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/pdh/ThaModulePdh.h"
#include "../../../default/man/ThaDevice.h"
#include "Tha60210051ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePrbsMethods m_ThaModulePrbsOverride;
static tAtModuleMethods      m_AtModuleOverride;

/* Save super implementation */
static const tThaModulePrbsMethods *m_ThaModulePrbsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaVersionReader VersionReader(ThaModulePrbs self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartVersionSupportPwPrbsEngine(ThaModulePrbs self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x10, 0x19, 0x00);
    }

static eBool BoundCircuitChannelIsHo(AtPw pw)
    {
    if (AtPwTypeGet(pw) != cAtPwTypeCEP)
        return cAtFalse;

    return Tha60210011ModuleSdhChannelIsHoPath((AtSdhChannel)AtPwBoundCircuitGet(pw));
    }

static eBool PwPrbsIsSupported(ThaModulePrbs self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceAllFeaturesAvailableInSimulation(device) || (AtDeviceVersionNumber(device) >= StartVersionSupportPwPrbsEngine(self)))
        return cAtTrue;

    return cAtFalse;
    }

static AtPrbsEngine PwPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPw pw)
    {
    /* To enable PRBS, PW need to be activated, not enough information to access HW while PW is logical */
    if ((AtPwEthPortGet(pw) == NULL) || (AtPwBoundCircuitGet(pw) == NULL))
        return NULL;

    if (BoundCircuitChannelIsHo(pw))
        return NULL;

    if (!ThaModulePrbsPwPrbsIsSupported(self))
        return NULL;

    return Tha60210051PwPrbsEngineNew(pw, engineId);
    }

static uint32 PdaBaseAddress(void)
    {
    return 0x500000;
    }

static uint32 PrbsPwPdaDefaultOffset(ThaModulePrbs self, AtPrbsEngine engine)
    {
    uint8 slice = 0;
    AtPw pw = (AtPw)AtPrbsEngineChannelGet(engine);

    AtUnused(self);

    if (Tha60210011PwCircuitSliceAndHwIdInSliceGet(pw, &slice, NULL) != cAtOk)
        {
        mChannelLog(pw, cAtLogLevelCritical, "Cannot get circuit slice and HW ID in slice");
        return 0x0;
        }

    /* Not support PW PRBS for HoLine */
    if (!Tha60210011PwCircuitBelongsToLoLine(pw))
        return 0x0;

    return ((slice / 2UL) * 0x2000UL) + ((slice % 2UL) * 0x1000UL) + AtChannelHwIdGet((AtChannel)pw) + PdaBaseAddress();
    }

static AtModulePw ModulePw(AtModule self)
    {
    return (AtModulePw)AtDeviceModuleGet(AtModuleDeviceGet(self), cAtModulePw);
    }

static const char *CapacityDescription(AtModule self)
    {
    static char capacity[128];
    AtSnprintf(capacity, 128, "PRBS engines: 16 engines (vc4, vc3, de3, vc1x, de1, nxds0), %u engines (pw)", AtModulePwMaxPwsGet(ModulePw(self)));
    return capacity;
    }

static uint32 MaxNumHoPrbsEngine(ThaModulePrbs self)
    {
    AtUnused(self);
    return 8;
    }

static ThaPrbsRegProvider RegProvider(ThaModulePrbs self)
    {
    AtUnused(self);
    return Tha60210051PrbsRegProvider();
    }

static void OverrideThaModulePrbs(ThaModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePrbsOverride, m_ThaModulePrbsMethods, sizeof(m_ThaModulePrbsOverride));

        mMethodOverride(m_ThaModulePrbsOverride, PwPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, PrbsPwPdaDefaultOffset);
        mMethodOverride(m_ThaModulePrbsOverride, MaxNumHoPrbsEngine);
        mMethodOverride(m_ThaModulePrbsOverride, RegProvider);
        mMethodOverride(m_ThaModulePrbsOverride, PwPrbsIsSupported);
        }

    mMethodsSet(self, &m_ThaModulePrbsOverride);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideThaModulePrbs((ThaModulePrbs)self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ModulePrbs);
    }

AtModulePrbs Tha60210051ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha60210051ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051ModulePrbsObjectInit(newModule, device);
    }

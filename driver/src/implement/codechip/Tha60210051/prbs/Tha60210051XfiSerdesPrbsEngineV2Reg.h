/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60210051XfiSerdesPrbsEngineV2Reg.h
 * 
 * Created Date: Oct 22, 2015
 *
 * Description : SERDES PRBS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051XFISERDESPRBSENGINEV2REG_H_
#define _THA60210051XFISERDESPRBSENGINEV2REG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif


/*  XFI PRBS Pattern Generate Configure */
#define cXfiPrbsPatternGenCfg  0x0

#define cXfiPrbsPatternGenCfgPktFlushMask     cBit17
#define cXfiPrbsPatternGenCfgPktFlushShift    17

#define cXfiPrbsPatternGenCfgGenEnMask          cBit16
#define cXfiPrbsPatternGenCfgGenEnShift         16

#define cXfiPrbsPatternGenCfgErrForceMask     cBit3
#define cXfiPrbsPatternGenCfgErrForceShift    3

#define cXfiPrbsPatternGenCfgErrForceMask     cBit3
#define cXfiPrbsPatternGenCfgErrForceShift    3

#define cXfiPrbsPatternGenCfgPktGenNumMask     cBit2
#define cXfiPrbsPatternGenCfgPktGenNumShift    2

#define cXfiPrbsPatternGenCfgLenModeMask     cBit1
#define cXfiPrbsPatternGenCfgLenModeShift    1

#define cXfiPrbsPatternGenCfgDaModeMask     cBit0
#define cXfiPrbsPatternGenCfgDaModeShift    0

/* XFI PRBS Pattern Packet lengthg Configure */
#define cXfiPrbsPktLenCfg   0x2

#define cXfiPrbsPktLenCfgMaxMask    cBit31_16
#define cXfiPrbsPktLenCfgMaxShift   16

#define cXfiPrbsPktLenCfgMinMask    cBit15_0
#define cXfiPrbsPktLenCfgMinShift   0

/* XFI PRBS Pattern Packet Data Configure */
#define cXfiPrbsPktDataCfg  0x3

#define cXfiPrbsPktDataCfgModeMask   cBit9_8
#define cXfiPrbsPktDataCfgModeShift  8

/* XFI PRBS Pattern Packet analyzer Data Configure */
#define cXfiPrbsRxPktCfg    0xA01

#define cXfiPrbsRxPktCfgFcsDropMask     cBit28
#define cXfiPrbsRxPktCfgFcsDropShift     28

#define cXfiPrbsRxPktCfgModeMask        cBit11_8
#define cXfiPrbsRxPktCfgModeShift       8

/* XFI PRBS Pattern Packet analyzer PRBS Sticky Error */
#define cXfiPrbsRxPktErrStk     0xB00

#define cXfiPrbsRxPktErrStkLossSyncMask     cBit0

/* XFI PRBS Pattern Packet analyzer PRBS status */
#define cXfiPrbsRxPktErrStatus     0xB01

#define cXfiPrbsRxPktErrStatusMask     cBit0

/* XFI PRBS Pattern Packet generate Counter R2C/RO */
#define cXfiPrbsGenPktCounter(r2c)      ((r2c) ? 0x20 : 0x10)

/* XFI PRBS Packet analyzer Counter pkttotal */
#define cXfiPrbsRxPktCounters(r2c)      ((r2c) ? 0x900 : 0x800)

/* XFI PRBS Packet analyzer Counter pktgood*/
#define cXfiPrbsRxGoodPkt(r2c)          ((r2c) ? 0x901 : 0x801)

/* XFI PRBS Packet analyzer Counter pktfcserr */
#define cXfiPrbsRxFcsErrPkt(r2c)   ((r2c) ? 0x902 : 0x802)

/* XFI PRBS Packet analyzer Counter pkt length err */
#define cXfiPrbsRxLenErrPkt(r2c)    ((r2c) ? 0x903 : 0x803)

/* XFI PRBS Packet analyzer Counter pkt PRBS Data err */
#define cXfiPrbsRxDataErrPkt(r2c)   ((r2c) ? 0x907 : 0x807)

#define cXfiPrbsRx0_64LenPkt            0x90A
#define cXfiPrbsRx65_128LenPkt          0x90B
#define cXfiPrbsRx129_256LenPkt         0x90C
#define cXfiPrbsRx257_512LenPkt         0x90D
#define cXfiPrbsRx513_1024LenPkt        0x90E
#define cXfiPrbsRx1025_2048LenPkt       0x90F

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051XFISERDESPRBSENGINEV2REG_H_ */


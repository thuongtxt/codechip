/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60210051PrbsRegProvider.c
 *
 * Created Date: Jan 6, 2016
 *
 * Description : Register provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210051PrbsRegProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cPwPdaPwPrbsGenCtrl 0x00080400
#define cPwPdaPwPrbsMonCtrl 0x00080800

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsRegProviderMethods m_ThaPrbsRegProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 RegPdaPwPrbsGenCtrl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cPwPdaPwPrbsGenCtrl;
    }

static uint32 RegPdaPwPrbsMonCtrl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cPwPdaPwPrbsMonCtrl;
    }

static void OverrideThaPrbsRegProvider(ThaPrbsRegProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsRegProviderOverride, mMethodsGet(self), sizeof(m_ThaPrbsRegProviderOverride));

        mMethodOverride(m_ThaPrbsRegProviderOverride, RegPdaPwPrbsGenCtrl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegPdaPwPrbsMonCtrl);
        }

    mMethodsSet(self, &m_ThaPrbsRegProviderOverride);
    }

static void Override(ThaPrbsRegProvider self)
    {
    OverrideThaPrbsRegProvider(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051PrbsRegProvider);
    }

ThaPrbsRegProvider Tha60210051PrbsRegProviderObjectInit(ThaPrbsRegProvider self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PrbsRegProviderObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPrbsRegProvider Tha60210051PrbsRegProvider(void)
    {
    static tTha60210051PrbsRegProvider shareProvider;
    static ThaPrbsRegProvider pShareProvider = NULL;
    if (pShareProvider == NULL)
        pShareProvider = Tha60210051PrbsRegProviderObjectInit((ThaPrbsRegProvider)&shareProvider);
    return pShareProvider;
    }

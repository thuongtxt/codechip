/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Tha60210051Pdh Module
 *
 * File        : Tha60210051ModulePdh.c
 *
 * Created Date: Oct 28, 2015
 *
 * Description : PDH module functions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "Tha60210051ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePdhMethods m_ThaModulePdhOverride;

/* Save super implementation */
static const tThaModulePdhMethods *m_ThaModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaVersionReader VersionReader(ThaModulePdh self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static uint32 StartVersionSupportDe3Disabling(ThaModulePdh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x10, 0x28, 0x00);
    }

static uint32 StartVersionSupportAisDownStream(ThaModulePdh self)
    {
    return ThaVersionReaderPwVersionBuild(VersionReader(self), 0x15, 0x10, 0x28, 0x00);
    }

static uint8 NumSlices(ThaModulePdh self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 StartVersionSupportDe3FramedSatop(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x0, 0x1069);
    }

static eBool De3FramedSatopIsSupported(ThaModulePdh self)
    {
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    uint32 startSupportedVersion = mMethodsGet(self)->StartVersionSupportDe3FramedSatop(self);
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static eBool De3AisSelectablePatternIsSupported(ThaModulePdh self)
    {
    uint32 startSupportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x0, 0x1070);
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static uint32 StartVersionSupportDe1LomfConsequentialAction(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x5, 0x1074);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, m_ThaModulePdhMethods, sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3Disabling);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportAisDownStream);
        mMethodOverride(m_ThaModulePdhOverride, NumSlices);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3FramedSatop);
        mMethodOverride(m_ThaModulePdhOverride, De3FramedSatopIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De3AisSelectablePatternIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe1LomfConsequentialAction);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideThaModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ModulePdh);
    }

AtModulePdh Tha60210051ModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60210051ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051ModulePdhObjectInit(newModule, device);
    }

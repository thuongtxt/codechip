/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH module name
 *
 * File        : Tha60210051ModulePdhInternal.h
 *
 * Created Date: Jul 13, 2016
 *
 * Description : PDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULEPDHINTERNAL_H_
#define _THA60210051MODULEPDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/pdh/Tha60210011ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051ModulePdh
    {
    tTha60210011ModulePdh super;
    } tTha60210051ModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh Tha60210051ModulePdhObjectInit(AtModulePdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULEPDHINTERNAL_H_ */


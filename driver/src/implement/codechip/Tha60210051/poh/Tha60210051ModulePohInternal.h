/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60210051ModulePohInternal.h
 * 
 * Created Date: Dec 29, 2015
 *
 * Description : POH Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULEPOHINTERNAL_H_
#define _THA60210051MODULEPOHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/poh/Tha60210011ModulePohInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051ModulePoh
    {
    tTha60210011ModulePoh super;

    AtLongRegisterAccess clk155LongRegAccess;
    AtLongRegisterAccess clk311LongRegAccess;
    }tTha60210051ModulePoh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210051ModulePohObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULEPOHINTERNAL_H_ */


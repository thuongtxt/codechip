/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210051ModulePoh.c
 *
 * Created Date: Sep 29, 2015
 *
 * Description : Module POH of 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/util/AtLongRegisterAccess.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60210011/poh/Tha60210011ModulePohInternal.h"
#include "../../Tha60210011/poh/Tha60210011PohProcessor.h"
#include "../man/Tha60210051Device.h"
#include "Tha60210051PohProcessor.h"
#include "Tha60210051ModulePoh.h"
#include "Tha60210051ModulePohInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210051ModulePoh* )self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tAtObjectMethods             m_AtObjectOverride;
static tThaModulePohMethods         m_ThaModulePohOverride;
static tTha60210011ModulePohMethods m_Tha60210011ModulePohOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Reg_ter_ctrlhi(ThaModulePoh self, uint8 sts, uint8 ocid)
    {
    AtUnused(self);
    return sts + ocid * 48UL;
    }

static uint32 Reg_ter_ctrllo(ThaModulePoh self, uint8 sts, uint8 ocid, uint8 vt)
    {
    AtUnused(self);
    return (sts) * 28UL + (ocid) * 1344UL + (vt);
    }

static uint32 *Clk155HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x20000A, 0x20000B};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static uint32 *Clk311HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x21000A, 0x21000B};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    return Clk155HoldRegistersGet(self, numberOfHoldRegisters);
    }

static AtLongRegisterAccess Clk155LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = Clk155HoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static AtLongRegisterAccess Clk311LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = Clk311HoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static AtLongRegisterAccess Clk155LongRegisterAccess(AtModule self)
    {
    if (mThis(self)->clk155LongRegAccess)
        return mThis(self)->clk155LongRegAccess;

    mThis(self)->clk155LongRegAccess = Clk155LongRegisterAccessCreate(self);
    return mThis(self)->clk155LongRegAccess;
    }

static AtLongRegisterAccess Clk311LongRegisterAccess(AtModule self)
    {
    if (mThis(self)->clk311LongRegAccess)
        return mThis(self)->clk311LongRegAccess;

    mThis(self)->clk311LongRegAccess = Clk311LongRegisterAccessCreate(self);
    return mThis(self)->clk311LongRegAccess;
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    if (Tha60210051DeviceUseGlobalHold((Tha60210051Device)AtModuleDeviceGet(self)))
        return m_AtModuleMethods->LongRegisterAccess(self, localAddress);

    if (Tha60210011ModulePohAddressBelongTo155Part((Tha60210011ModulePoh)self, localAddress))
        return Clk155LongRegisterAccess(self);

    return Clk311LongRegisterAccess(self);
    }

static uint32 NumberStsSlices(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 NumberVtSlices(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 8;
    }

static eBool DistinctErdiInterruptIsSupported(ThaModulePoh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210051DeviceStartVersionSupportDistinctErdi(device))
        return cAtTrue;
        
    return cAtFalse;
    }

static ThaPohProcessor AuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    if (DistinctErdiInterruptIsSupported(self))
        return Tha60210051AuVcPohProcessorNew(vc);
    else
        return Tha60210011AuVcPohProcessorNew(vc);
    }

static ThaPohProcessor Tu3VcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    if (DistinctErdiInterruptIsSupported(self))
        return Tha60210051Tu3VcPohProcessorNew(vc);
    else
        return Tha60210011Tu3VcPohProcessorNew(vc);
    }

static ThaPohProcessor Vc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    if (DistinctErdiInterruptIsSupported(self))
        return Tha60210051Vc1xPohProcessorNew(vc);
    else
        return Tha60210011Vc1xPohProcessorNew(vc);
    }

static uint32 StartVersionSupportSeparateRdiErdiS(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x7, 0x6, 0x1040);
    }

static void DeleteLongRegAccess(AtLongRegisterAccess *access)
    {
    AtObjectDelete((AtObject)*access);
    *access = NULL;
    }

static void Delete(AtObject self)
    {
    DeleteLongRegAccess(&(mThis(self)->clk155LongRegAccess));
    DeleteLongRegAccess(&(mThis(self)->clk311LongRegAccess));

    /* Call super to fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210051ModulePoh *object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(clk155LongRegAccess);
    mEncodeObject(clk311LongRegAccess);
    }

static void OverrideTha60210011ModulePoh(AtModule self)
    {
    Tha60210011ModulePoh pohModule = (Tha60210011ModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePohOverride, mMethodsGet(pohModule), sizeof(m_Tha60210011ModulePohOverride));

        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_ter_ctrlhi);
        mMethodOverride(m_Tha60210011ModulePohOverride, Reg_ter_ctrllo);
        mMethodOverride(m_Tha60210011ModulePohOverride, NumberStsSlices);
        mMethodOverride(m_Tha60210011ModulePohOverride, NumberVtSlices);
        mMethodOverride(m_Tha60210011ModulePohOverride, StartVersionSupportSeparateRdiErdiS);
        }

    mMethodsSet(pohModule, &m_Tha60210011ModulePohOverride);
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, AuVcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Tu3VcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Vc1xPohProcessorCreate);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideTha60210011ModulePoh(self);
    OverrideThaModulePoh(self);
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ModulePoh);
    }

AtModule Tha60210051ModulePohObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModulePohObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210051ModulePohNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051ModulePohObjectInit(newModule, device);
    }

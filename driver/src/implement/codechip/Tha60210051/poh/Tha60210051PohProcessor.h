/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60210051PohProcessor.h
 * 
 * Created Date: Nov 16, 2015
 *
 * Description : POH processor interface for 60210051 product.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051POHPROCESSOR_H_
#define _THA60210051POHPROCESSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/poh/Tha60210011PohProcessorInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPohProcessor Tha60210051AuVcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha60210051Tu3VcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha60210051Vc1xPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha6A000010Vc1xPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha6A000010Tu3VcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha6A000010AuVcPohProcessorNew(AtSdhVc vc);

/* Shared one-bit RDI/ERDI-S notification on change of ERDI monitoring */
eAtRet Tha6021PohProcessorErdiEnableWithRdiErdiSNotify(ThaPohProcessor self, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051POHPROCESSOR_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60210051PohProcessorInternal.h
 * 
 * Created Date: Dec 29, 2015
 *
 * Description : POH processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051POHPROCESSORINTERNAL_H_
#define _THA60210051POHPROCESSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/poh/Tha60210011PohProcessorInternal.h"
#include "Tha60210051PohProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051AuVcPohProcessor
    {
    tTha60210011AuVcPohProcessor super;
    }tTha60210051AuVcPohProcessor;

typedef struct tTha60210051Tu3VcPohProcessor
    {
    tTha60210011Tu3VcPohProcessor super;
    }tTha60210051Tu3VcPohProcessor;

typedef struct tTha60210051Vc1xPohProcessor
    {
    tTha60210011Vc1xPohProcessor super;
    }tTha60210051Vc1xPohProcessor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPohProcessor Tha60210051AuVcPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc);
ThaPohProcessor Tha60210051Tu3VcPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc);
ThaPohProcessor Tha60210051Vc1xPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051POHPROCESSORINTERNAL_H_ */


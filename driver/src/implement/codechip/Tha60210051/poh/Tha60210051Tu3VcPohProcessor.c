/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60210051Tu3VcPohProcessor.c
 *
 * Created Date: Nov 16, 2015
 *
 * Description : TU3 VC-3 POH implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "Tha60210051PohProcessor.h"
#include "Tha60210051PohReg.h"
#include "Tha60210051PohProcessorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods                    m_AtChannelOverride;
static tAtSdhPathMethods                    m_AtSdhPathOverride;
static tThaPohProcessorMethods              m_ThaPohProcessorOverride;
static tTha60210011AuVcPohProcessorMethods  m_Tha60210011AuVcPohProcessorOverride;

/* Save super implementation */
static const tAtChannelMethods         *m_AtChannelMethods = NULL;
static const tAtSdhPathMethods         *m_AtSdhPathMethods = NULL;
static const tThaPohProcessorMethods   *m_ThaPohProcessorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePoh ModulePoh(AtChannel self)
    {
    return (ThaModulePoh)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePoh);
    }

static uint32 RdiMask(Tha60210011AuVcPohProcessor self)
    {
    if (Tha60210011ModulePohSeparateRdiErdiSIsSupported((Tha60210011ModulePoh)ModulePoh((AtChannel)self)))
        return cAf6_alm_mskhi_rdimsk_Mask;

    return AtSdhPathERdiIsEnabled((AtSdhPath)self) ? 0 : cAf6_alm_mskhi_erdimsk_Mask;
    }

static uint32 ErdiSMask(Tha60210011AuVcPohProcessor self)
        {
    if (Tha60210011ModulePohSeparateRdiErdiSIsSupported((Tha60210011ModulePoh)ModulePoh((AtChannel)self)))
        return cAf6_alm_mskhi_erdimsk_Mask;

    return AtSdhPathERdiIsEnabled((AtSdhPath)self) ? cAf6_alm_mskhi_erdimsk_Mask : 0;
        }

static uint32 DefectHw2Sw(Tha60210011AuVcPohProcessor self, uint32 regVal)
    {
    uint32 defects = 0;

    if (regVal & mMethodsGet(self)->RdiMask(self))
        defects |= cAtSdhPathAlarmRdi;
    if (regVal & mMethodsGet(self)->ErdiSMask(self))
        defects |= cAtSdhPathAlarmErdiS;
    if (regVal & cAf6_alm_stahi_erdicsta_Mask)
        defects |= cAtSdhPathAlarmErdiC;
    if (regVal & cAf6_alm_stahi_erdipsta_Mask)
        defects |= cAtSdhPathAlarmErdiP;
    if (regVal & cAf6_alm_stahi_bertcasta_Mask)
        defects |= cAtSdhPathAlarmBerTca;
    if (regVal & cAf6_alm_stahi_timsta_Mask)
        defects |= cAtSdhPathAlarmTim;
    if (regVal & cAf6_alm_stahi_uneqsta_Mask)
        defects |= cAtSdhPathAlarmUneq;
    if (regVal & cAf6_alm_stahi_plmsta_Mask)
        defects |= cAtSdhPathAlarmPlm;
    if (regVal & cAf6_alm_stahi_aissta_Mask)
        defects |= cAtSdhPathAlarmAis;
    if (regVal & cAf6_alm_stahi_lopsta_Mask)
        defects |= cAtSdhPathAlarmLop;
    if (mMethodsGet(self)->HasPayloadUneqDefect(self))
        defects |= cAtSdhPathAlarmPayloadUneq;
    if (regVal & cAf6_alm_stahi_bersfsta_Mask)
        defects |= cAtSdhPathAlarmBerSf;
    if (regVal & cAf6_alm_stahi_bersdsta_Mask)
        defects |= cAtSdhPathAlarmBerSd;

    return defects;
    }

static uint32 PohDefectGet(Tha60210011AuVcPohProcessor self)
    {
    uint32 regAddr = mMethodsGet(self)->PohStatusReportRegAddr(self) +
                     mMethodsGet(self)->PohInterruptRegOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    return DefectHw2Sw(self, regVal);
    }

static uint32 InterruptRead2Clear(Tha60210011AuVcPohProcessor self, eBool readOnClear)
    {
    uint32 regAddr, regVal;
    uint32 stickies = 0;

    regAddr = mMethodsGet(self)->PohInterruptStatusRegAddr(self) +
              mMethodsGet(self)->PohInterruptRegOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    if (regVal & cAf6_alm_chghi_bertcastachg_Mask)
        stickies |= cAtSdhPathAlarmBerTca;
    if (regVal & mMethodsGet(self)->RdiMask(self))
        stickies |= cAtSdhPathAlarmRdi;
    if (regVal & mMethodsGet(self)->ErdiSMask(self))
        stickies |= cAtSdhPathAlarmErdiS;
    if (regVal & cAf6_alm_chghi_erdicstachg_Mask)
        stickies |= cAtSdhPathAlarmErdiC;
    if (regVal & cAf6_alm_chghi_erdipstachg_Mask)
        stickies |= cAtSdhPathAlarmErdiP;
    if (regVal & cAf6_alm_chghi_timstachg_Mask)
        stickies |= cAtSdhPathAlarmTim;
    if (regVal & cAf6_alm_chghi_uneqstachg_Mask)
        stickies |= cAtSdhPathAlarmUneq;
    if (regVal & cAf6_alm_chghi_plmstachg_Mask)
        stickies |= cAtSdhPathAlarmPlm;
    if (regVal & cAf6_alm_chghi_aisstachg_Mask)
        stickies |= cAtSdhPathAlarmAis;
    if (regVal & cAf6_alm_chghi_lopstachg_Mask)
        stickies |= cAtSdhPathAlarmLop;
    if (regVal & cAf6_alm_chghi_bersfstachg_Mask)
        stickies |= cAtSdhPathAlarmBerSf;
    if (regVal & cAf6_alm_chghi_bersdstachg_Mask)
        stickies |= cAtSdhPathAlarmBerSd;
    if (mMethodsGet(self)->HasPayloadUneqSticky(self, readOnClear))
        stickies |= cAtSdhPathAlarmPayloadUneq;

    if (Tha60210011ModulePohPslTtiInterruptIsSupported(ModulePoh((AtChannel)self)))
        {
        if (regVal & cAf6_alm_chghi_jnstachg_Mask)
            stickies |= cAtSdhPathAlarmTtiChange;
        if (regVal & cAf6_alm_chghi_pslstachg_Mask)
            stickies |= cAtSdhPathAlarmPslChange;
        }

    if (readOnClear)
        mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return stickies;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
	uint32 mask = cAtSdhPathAlarmBerTca |
	              cAtSdhPathAlarmRdi    |
	              cAtSdhPathAlarmErdiS  |
	              cAtSdhPathAlarmErdiC  |
	              cAtSdhPathAlarmErdiP  |
	              cAtSdhPathAlarmTim    |
	              cAtSdhPathAlarmUneq   |
	              cAtSdhPathAlarmPlm    |
	              cAtSdhPathAlarmLop    |
	              cAtSdhPathAlarmAis    |
	              cAtSdhPathAlarmBerSf  |
	              cAtSdhPathAlarmBerSd;

    if (Tha60210011ModulePohPslTtiInterruptIsSupported(ModulePoh(self)))
    	mask |= (cAtSdhPathAlarmTtiChange | cAtSdhPathAlarmPslChange);

    return mask;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(processor)->PohInterruptMaskRegAddr(processor) +
              mMethodsGet(processor)->PohInterruptRegOffset(processor);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    if (defectMask & cAtSdhPathAlarmBerTca)
        mRegFieldSet(regVal, cAf6_alm_mskhi_bertcamsk_, (enableMask & cAtSdhPathAlarmBerTca) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmRdi)
        {
        uint32 rdiMask = mMethodsGet(processor)->RdiMask(processor);
        regVal = (enableMask & cAtSdhPathAlarmRdi) ? (regVal | rdiMask) : (regVal & ~rdiMask);
        }
    if (defectMask & cAtSdhPathAlarmErdiS)
        {
        uint32 erdisMask = mMethodsGet(processor)->ErdiSMask(processor);
        regVal = (enableMask & cAtSdhPathAlarmErdiS) ? (regVal | erdisMask) : (regVal & ~erdisMask);
        }
    if (defectMask & cAtSdhPathAlarmErdiC)
        mRegFieldSet(regVal, cAf6_alm_mskhi_erdicmsk_, (enableMask & cAtSdhPathAlarmErdiC) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmErdiP)
        mRegFieldSet(regVal, cAf6_alm_mskhi_erdipmsk_, (enableMask & cAtSdhPathAlarmErdiP) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmTim)
        mRegFieldSet(regVal, cAf6_alm_mskhi_timmsk_, (enableMask & cAtSdhPathAlarmTim) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmUneq)
        mRegFieldSet(regVal, cAf6_alm_mskhi_uneqmsk_, (enableMask & cAtSdhPathAlarmUneq) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmPlm)
        mRegFieldSet(regVal, cAf6_alm_mskhi_plmmsk_, (enableMask & cAtSdhPathAlarmPlm) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmLop)
        mRegFieldSet(regVal, cAf6_alm_mskhi_lopmsk_, (enableMask & cAtSdhPathAlarmLop) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmAis)
        mRegFieldSet(regVal, cAf6_alm_mskhi_aismsk_, (enableMask & cAtSdhPathAlarmAis) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmBerSf)
        mRegFieldSet(regVal, cAf6_alm_mskhi_bersfmsk_, (enableMask & cAtSdhPathAlarmBerSf) ? 1 : 0);
    if (defectMask & cAtSdhPathAlarmBerSd)
        mRegFieldSet(regVal, cAf6_alm_mskhi_bersdmsk_, (enableMask & cAtSdhPathAlarmBerSd) ? 1 : 0);

    if (Tha60210011ModulePohPslTtiInterruptIsSupported(ModulePoh(self)))
        {
        if (defectMask & cAtSdhPathAlarmTtiChange)
            mRegFieldSet(regVal, cAf6_alm_mskhi_jnstachgmsk_, (enableMask & cAtSdhPathAlarmTtiChange) ? 1 : 0);
        if (defectMask & cAtSdhPathAlarmPslChange)
            mRegFieldSet(regVal, cAf6_alm_mskhi_pslstachgmsk_, (enableMask & cAtSdhPathAlarmPslChange) ? 1 : 0);
        }

    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr, regVal;
    uint32 mask = 0;

    regAddr = mMethodsGet(processor)->PohInterruptMaskRegAddr(processor) +
              mMethodsGet(processor)->PohInterruptRegOffset(processor);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    if (regVal & cAf6_alm_mskhi_bertcamsk_Mask)
        mask |= cAtSdhPathAlarmBerTca;
    if (regVal & mMethodsGet(processor)->RdiMask(processor))
        mask |= cAtSdhPathAlarmRdi;
    if (regVal & mMethodsGet(processor)->ErdiSMask(processor))
        mask |= cAtSdhPathAlarmErdiS;
    if (regVal & cAf6_alm_mskhi_erdicmsk_Mask)
        mask |= cAtSdhPathAlarmErdiC;
    if (regVal & cAf6_alm_mskhi_erdipmsk_Mask)
        mask |= cAtSdhPathAlarmErdiP;
    if (regVal & cAf6_alm_mskhi_timmsk_Mask)
        mask |= cAtSdhPathAlarmTim;
    if (regVal & cAf6_alm_mskhi_uneqmsk_Mask)
        mask |= cAtSdhPathAlarmUneq;
    if (regVal & cAf6_alm_mskhi_plmmsk_Mask)
        mask |= cAtSdhPathAlarmPlm;
    if (regVal & cAf6_alm_mskhi_lopmsk_Mask)
        mask |= cAtSdhPathAlarmLop;
    if (regVal & cAf6_alm_mskhi_aismsk_Mask)
        mask |= cAtSdhPathAlarmAis;
    if (regVal & cAf6_alm_mskhi_bersfmsk_Mask)
        mask |= cAtSdhPathAlarmBerSf;
    if (regVal & cAf6_alm_mskhi_bersdmsk_Mask)
        mask |= cAtSdhPathAlarmBerSd;

    if (Tha60210011ModulePohPslTtiInterruptIsSupported(ModulePoh((AtChannel)self)))
        {
        if (regVal & cAf6_alm_mskhi_jnstachgmsk_Mask)
            mask |= cAtSdhPathAlarmTtiChange;
        if (regVal & cAf6_alm_mskhi_pslstachgmsk_Mask)
            mask |= cAtSdhPathAlarmPslChange;
        }

    return mask;
    }

static void InterruptProcess(AtSdhPath self, uint8 slice, uint8 sts, uint8 vt, AtHal hal)
    {
    AtSdhChannel vc3 = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    AtChannel tu3 = (AtChannel)AtSdhChannelParentChannelGet(vc3);
    Tha60210011AuVcPohProcessor pohProcessor = (Tha60210011AuVcPohProcessor)self;
    uint32 offset = mMethodsGet(pohProcessor)->PohInterruptRegOffset(pohProcessor);
    uint32 intrAddress = mMethodsGet(pohProcessor)->PohInterruptStatusRegAddr(pohProcessor) + offset;
    uint32 intrMask = AtHalRead(hal, mMethodsGet(pohProcessor)->PohInterruptMaskRegAddr(pohProcessor) + offset);
    uint32 intrStatus = AtHalRead(hal, intrAddress);
    uint32 currentStatus = 0;
    uint32 events = 0;
    uint32 defects = 0;
    uint32 rdiMask;
    AtUnused(slice);
    AtUnused(sts);
    AtUnused(vt);

    intrStatus |= Tha60210011AuVcPohProcessorSoftStickyGet(pohProcessor, currentStatus);
    intrStatus &= intrMask;

    /* Clear interrupt */
    AtHalWrite(hal, intrAddress, intrStatus);
    currentStatus = AtHalRead(hal, mMethodsGet(pohProcessor)->PohStatusReportRegAddr(pohProcessor) + offset);

    /* TU-AIS */
    if (intrStatus & cAf6_alm_msklo_aismsk_Mask)
        {
        events |= cAtSdhPathAlarmAis;
        if (currentStatus & cAf6_alm_stalo_aissta_Mask)
            defects |= cAtSdhPathAlarmAis;
        }

    /* TU-LOP */
    if (intrStatus & cAf6_alm_msklo_lopmsk_Mask)
        {
        events |= cAtSdhPathAlarmLop;
        if (currentStatus & cAf6_alm_stalo_lopsta_Mask)
            defects |= cAtSdhPathAlarmLop;
        }

    /* LP-RDI */
    rdiMask = mMethodsGet(pohProcessor)->RdiMask(pohProcessor);
    if (intrStatus & rdiMask)
        {
        events |= cAtSdhPathAlarmRdi;
        if (currentStatus & rdiMask)
            defects |= cAtSdhPathAlarmRdi;
        }

    /* LP-ERDI-S */
    rdiMask = mMethodsGet(pohProcessor)->ErdiSMask(pohProcessor);
    if (intrStatus & rdiMask)
        {
        events |= cAtSdhPathAlarmErdiS;
        if (currentStatus & rdiMask)
            defects |= cAtSdhPathAlarmErdiS;
        }

    /* LP-ERDI-C */
    if (intrStatus & cAf6_alm_msklo_erdicmsk_Mask)
        {
        events |= cAtSdhPathAlarmErdiC;
        if (currentStatus & cAf6_alm_stalo_erdicsta_Mask)
            defects |= cAtSdhPathAlarmErdiC;
        }

    /* LP-ERDI-P */
    if (intrStatus & cAf6_alm_msklo_erdipmsk_Mask)
        {
        events |= cAtSdhPathAlarmErdiP;
        if (currentStatus & cAf6_alm_stalo_erdipsta_Mask)
            defects |= cAtSdhPathAlarmErdiP;
        }

    /* LP-TIM */
    if (intrStatus & cAf6_alm_msklo_timmsk_Mask)
        {
        events |= cAtSdhPathAlarmTim;
        if (currentStatus & cAf6_alm_stalo_timsta_Mask)
            defects |= cAtSdhPathAlarmTim;
        }

    /* LP-UNEQ */
    if (intrStatus & cAf6_alm_msklo_uneqmsk_Mask)
        {
        events |= cAtSdhPathAlarmUneq;
        if (currentStatus & cAf6_alm_stalo_uneqsta_Mask)
            defects |= cAtSdhPathAlarmUneq;
        }

    /* LP-PLM */
    if (intrStatus & cAf6_alm_msklo_plmmsk_Mask)
        {
        events |= cAtSdhPathAlarmPlm;
        if (currentStatus & cAf6_alm_stalo_plmsta_Mask)
            defects |= cAtSdhPathAlarmPlm;
        }

    /* LP-RFI (available for VC11) */
    if (intrStatus & cAf6_alm_msklo_rfimsk_Mask)
        {
        events |= cAtSdhPathAlarmRfi;
        if (currentStatus & cAf6_alm_stalo_rfista_Mask)
            defects |= cAtSdhPathAlarmRfi;
        }

    /* LP-BER-TCA */
    if (intrStatus & cAf6_alm_msklo_bertcamsk_Mask)
        {
        events |= cAtSdhPathAlarmBerTca;
        if (currentStatus & cAf6_alm_stalo_bertcasta_Mask)
            defects |= cAtSdhPathAlarmBerTca;
        }

    /* LP-BER-SD */
    if (intrStatus & cAf6_alm_msklo_bersdmsk_Mask)
        {
        events |= cAtSdhPathAlarmBerSd;
        if (currentStatus & cAf6_alm_stalo_bersdsta_Mask)
            defects |= cAtSdhPathAlarmBerSd;
        }

    /* LP-BER-SF */
    if (intrStatus & cAf6_alm_msklo_bersfmsk_Mask)
        {
        events |= cAtSdhPathAlarmBerSf;
        if (currentStatus & cAf6_alm_stalo_bersfsta_Mask)
            defects |= cAtSdhPathAlarmBerSf;
        }

    if (Tha60210011ModulePohPslTtiInterruptIsSupported(ModulePoh((AtChannel)self)))
        {
        /* LP new TTI event. */
        if (intrStatus & cAf6_alm_msklo_jnstachgmsk_Mask)
            events |= cAtSdhPathAlarmTtiChange;

        /* LP new PSL event. */
        if (intrStatus & cAf6_alm_msklo_pslstachgmsk_Mask)
            events |= cAtSdhPathAlarmPslChange;
        }

    AtChannelAllAlarmListenersCall(tu3, events, defects);

    AtSdhChannelAlarmForwardingClearanceNotifyWithSubChannelOnly(vc3, DefectHw2Sw((Tha60210011AuVcPohProcessor)self, currentStatus), cAtTrue);
    }

static uint8 RxPslGet(AtSdhPath self)
    {
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr, regVal;

    if (!Tha60210011ModulePohPslTtiInterruptIsSupported(ModulePoh((AtChannel)self)))
        return m_AtSdhPathMethods->RxPslGet(self);

    regAddr = mMethodsGet(processor)->PohCpeStatusRegAddr(processor) +
              mMethodsGet(processor)->PohCpeStatusRegOffset(processor);
    regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    return (uint8)mRegField(regVal, cAf6_pohcpestssta_C2acpt_);
    }

static eAtRet ERdiMonitorEnable(ThaPohProcessor self, eBool enable)
    {
    eAtRet ret = m_ThaPohProcessorMethods->ERdiMonitorEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    return Tha6021PohProcessorErdiEnableWithRdiErdiSNotify(self, enable);
    }

static eAtRet TxERdiEnable(ThaPohProcessor self, eBool enable)
    {
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(processor)->TerminateInsertControlRegAddr(processor) +
              mMethodsGet(processor)->TerminateInsertControlRegOffset(processor);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    mRegFieldSet(regVal, cAf6_ter_ctrllo_rdien_, (enable) ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool TxERdiIsEnabled(ThaPohProcessor self)
    {
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(processor)->TerminateInsertControlRegAddr(processor) +
              mMethodsGet(processor)->TerminateInsertControlRegOffset(processor);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    return (regVal & cAf6_ter_ctrllo_rdien_Mask) ? cAtFalse : cAtTrue;
    }

static void OverrideAtChannel(ThaPohProcessor self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);

        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhPath(ThaPohProcessor self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhPathMethods = mMethodsGet(path);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, InterruptProcess);
        mMethodOverride(m_AtSdhPathOverride, RxPslGet);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideThaPohProcessor(ThaPohProcessor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPohProcessorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPohProcessorOverride, mMethodsGet(self), sizeof(m_ThaPohProcessorOverride));
        mMethodOverride(m_ThaPohProcessorOverride, TxERdiEnable);
        mMethodOverride(m_ThaPohProcessorOverride, TxERdiIsEnabled);
        mMethodOverride(m_ThaPohProcessorOverride, ERdiMonitorEnable);
        }

    mMethodsSet(self, &m_ThaPohProcessorOverride);
    }

static void OverrideTha60210011AuVcPohProcessor(ThaPohProcessor self)
    {
    Tha60210011AuVcPohProcessor path = (Tha60210011AuVcPohProcessor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011AuVcPohProcessorOverride, mMethodsGet(path), sizeof(m_Tha60210011AuVcPohProcessorOverride));
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, InterruptRead2Clear);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, PohDefectGet);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, RdiMask);
        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, ErdiSMask);
        }

    mMethodsSet(path, &m_Tha60210011AuVcPohProcessorOverride);
    }

static void Override(ThaPohProcessor self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhPath(self);
    OverrideThaPohProcessor(self);
    OverrideTha60210011AuVcPohProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051Tu3VcPohProcessor);
    }

ThaPohProcessor Tha60210051Tu3VcPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tu3VcPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60210051Tu3VcPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60210051Tu3VcPohProcessorObjectInit(newProcessor, vc);
    }


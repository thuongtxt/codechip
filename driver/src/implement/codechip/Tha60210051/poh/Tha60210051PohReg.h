/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0051_RD_POH_BER_H_
#define _AF6_REG_AF6CCI0051_RD_POH_BER_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Register
Reg Addr   : 0x00_000A
Reg Formula: 0x00_000A + holdnum
    Where  : 
           + $holdnum(0-1): Hold register number
Reg Desc   : 
This register is used for access long register of Ber, Message.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdreg_Base                                                                          0x00000A
#define cAf6Reg_holdreg(holdnum)                                                          (0x00000A+(holdnum))
#define cAf6Reg_holdreg_WidthVal                                                                            32
#define cAf6Reg_holdreg_WriteMask                                                                          0x0

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdreg_holdvalue_Bit_Start                                                                     0
#define cAf6_holdreg_holdvalue_Bit_End                                                                      31
#define cAf6_holdreg_holdvalue_Mask                                                                   cBit31_0
#define cAf6_holdreg_holdvalue_Shift                                                                         0
#define cAf6_holdreg_holdvalue_MaxVal                                                               0xffffffff
#define cAf6_holdreg_holdvalue_MinVal                                                                      0x0
#define cAf6_holdreg_holdvalue_RstVal                                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Register
Reg Addr   : 0x01_000A
Reg Formula: 0x01_000A + holdnum
    Where  : 
           + $holdnum(0-1): Hold register number
Reg Desc   : 
This register is used for access long register of Grabber.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdregclk2_Base                                                                      0x01000A
#define cAf6Reg_holdregclk2(holdnum)                                                      (0x01000A+(holdnum))
#define cAf6Reg_holdregclk2_WidthVal                                                                        32
#define cAf6Reg_holdregclk2_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdregclk2_holdvalue_Bit_Start                                                                 0
#define cAf6_holdregclk2_holdvalue_Bit_End                                                                  31
#define cAf6_holdregclk2_holdvalue_Mask                                                               cBit31_0
#define cAf6_holdregclk2_holdvalue_Shift                                                                     0
#define cAf6_holdregclk2_holdvalue_MaxVal                                                           0xffffffff
#define cAf6_holdregclk2_holdvalue_MinVal                                                                  0x0
#define cAf6_holdregclk2_holdvalue_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Indirect Control Register
Reg Addr   : 0x00_000C
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used for access long register of Ber, Message.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdindctlreg_Base                                                                    0x00000C
#define cAf6Reg_holdindctlreg                                                                         0x00000C
#define cAf6Reg_holdindctlreg_WidthVal                                                                      32
#define cAf6Reg_holdindctlreg_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdindctlreg_holdvalue_Bit_Start                                                               0
#define cAf6_holdindctlreg_holdvalue_Bit_End                                                                31
#define cAf6_holdindctlreg_holdvalue_Mask                                                             cBit31_0
#define cAf6_holdindctlreg_holdvalue_Shift                                                                   0
#define cAf6_holdindctlreg_holdvalue_MaxVal                                                         0xffffffff
#define cAf6_holdindctlreg_holdvalue_MinVal                                                                0x0
#define cAf6_holdindctlreg_holdvalue_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Indirect Data Register
Reg Addr   : 0x00_000D
Reg Formula: 0x00_000D + holdnum
    Where  : 
           + $holdnum(0-2): Hold register number
Reg Desc   : 
This register is used for access long register of Ber, Message.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdinddatreg_Base                                                                    0x00000D
#define cAf6Reg_holdinddatreg(holdnum)                                                    (0x00000D+(holdnum))
#define cAf6Reg_holdinddatreg_WidthVal                                                                      32
#define cAf6Reg_holdinddatreg_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdinddatreg_holdvalue_Bit_Start                                                               0
#define cAf6_holdinddatreg_holdvalue_Bit_End                                                                31
#define cAf6_holdinddatreg_holdvalue_Mask                                                             cBit31_0
#define cAf6_holdinddatreg_holdvalue_Shift                                                                   0
#define cAf6_holdinddatreg_holdvalue_MaxVal                                                         0xffffffff
#define cAf6_holdinddatreg_holdvalue_MinVal                                                                0x0
#define cAf6_holdinddatreg_holdvalue_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Indirect Control Register
Reg Addr   : 0x01_000C
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used for access long register of Grabber.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdindctlreg2_Base                                                                   0x01000C
#define cAf6Reg_holdindctlreg2                                                                        0x01000C
#define cAf6Reg_holdindctlreg2_WidthVal                                                                     32
#define cAf6Reg_holdindctlreg2_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdindctlreg2_holdvalue_Bit_Start                                                              0
#define cAf6_holdindctlreg2_holdvalue_Bit_End                                                               31
#define cAf6_holdindctlreg2_holdvalue_Mask                                                            cBit31_0
#define cAf6_holdindctlreg2_holdvalue_Shift                                                                  0
#define cAf6_holdindctlreg2_holdvalue_MaxVal                                                        0xffffffff
#define cAf6_holdindctlreg2_holdvalue_MinVal                                                               0x0
#define cAf6_holdindctlreg2_holdvalue_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Hold Indirect Data Register
Reg Addr   : 0x01_000D
Reg Formula: 0x01_000D + holdnum
    Where  : 
           + $holdnum(0-2): Hold register number
Reg Desc   : 
This register is used for access long register of Grabber.

------------------------------------------------------------------------------*/
#define cAf6Reg_holdinddatreg2_Base                                                                   0x01000D
#define cAf6Reg_holdinddatreg2(holdnum)                                                   (0x01000D+(holdnum))
#define cAf6Reg_holdinddatreg2_WidthVal                                                                     32
#define cAf6Reg_holdinddatreg2_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: holdvalue
BitField Type: RW
BitField Desc: Hold value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_holdinddatreg2_holdvalue_Bit_Start                                                              0
#define cAf6_holdinddatreg2_holdvalue_Bit_End                                                               31
#define cAf6_holdinddatreg2_holdvalue_Mask                                                            cBit31_0
#define cAf6_holdinddatreg2_holdvalue_Shift                                                                  0
#define cAf6_holdinddatreg2_holdvalue_MaxVal                                                        0xffffffff
#define cAf6_holdinddatreg2_holdvalue_MinVal                                                               0x0
#define cAf6_holdinddatreg2_holdvalue_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Threshold Global Control
Reg Addr   : 0x00_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to set Threshold for stable detection.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_trshglbctr_Base                                                                  0x000003
#define cAf6Reg_pcfg_trshglbctr                                                                       0x000003
#define cAf6Reg_pcfg_trshglbctr_WidthVal                                                                    32
#define cAf6Reg_pcfg_trshglbctr_WriteMask                                                                  0x0

/*--------------------------------------
BitField Name: V5RFIStbTrsh
BitField Type: RW
BitField Desc: V5 RDI Stable Thershold
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_V5RFIStbTrsh_Bit_Start                                                         24
#define cAf6_pcfg_trshglbctr_V5RFIStbTrsh_Bit_End                                                           27
#define cAf6_pcfg_trshglbctr_V5RFIStbTrsh_Mask                                                       cBit27_24
#define cAf6_pcfg_trshglbctr_V5RFIStbTrsh_Shift                                                             24
#define cAf6_pcfg_trshglbctr_V5RFIStbTrsh_MaxVal                                                           0xf
#define cAf6_pcfg_trshglbctr_V5RFIStbTrsh_MinVal                                                           0x0
#define cAf6_pcfg_trshglbctr_V5RFIStbTrsh_RstVal                                                           0x1

/*--------------------------------------
BitField Name: V5RDIStbTrsh
BitField Type: RW
BitField Desc: V5 RDI Stable Thershold
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_V5RDIStbTrsh_Bit_Start                                                         20
#define cAf6_pcfg_trshglbctr_V5RDIStbTrsh_Bit_End                                                           23
#define cAf6_pcfg_trshglbctr_V5RDIStbTrsh_Mask                                                       cBit23_20
#define cAf6_pcfg_trshglbctr_V5RDIStbTrsh_Shift                                                             20
#define cAf6_pcfg_trshglbctr_V5RDIStbTrsh_MaxVal                                                           0xf
#define cAf6_pcfg_trshglbctr_V5RDIStbTrsh_MinVal                                                           0x0
#define cAf6_pcfg_trshglbctr_V5RDIStbTrsh_RstVal                                                           0x1

/*--------------------------------------
BitField Name: V5SlbStbTrsh
BitField Type: RW
BitField Desc: V5 Signal Lable Stable Thershold
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_V5SlbStbTrsh_Bit_Start                                                         16
#define cAf6_pcfg_trshglbctr_V5SlbStbTrsh_Bit_End                                                           19
#define cAf6_pcfg_trshglbctr_V5SlbStbTrsh_Mask                                                       cBit19_16
#define cAf6_pcfg_trshglbctr_V5SlbStbTrsh_Shift                                                             16
#define cAf6_pcfg_trshglbctr_V5SlbStbTrsh_MaxVal                                                           0xf
#define cAf6_pcfg_trshglbctr_V5SlbStbTrsh_MinVal                                                           0x0
#define cAf6_pcfg_trshglbctr_V5SlbStbTrsh_RstVal                                                           0x1

/*--------------------------------------
BitField Name: G1RDIStbTrsh
BitField Type: RW
BitField Desc: G1 RDI Path Stable Thershold
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_G1RDIStbTrsh_Bit_Start                                                         12
#define cAf6_pcfg_trshglbctr_G1RDIStbTrsh_Bit_End                                                           15
#define cAf6_pcfg_trshglbctr_G1RDIStbTrsh_Mask                                                       cBit15_12
#define cAf6_pcfg_trshglbctr_G1RDIStbTrsh_Shift                                                             12
#define cAf6_pcfg_trshglbctr_G1RDIStbTrsh_MaxVal                                                           0xf
#define cAf6_pcfg_trshglbctr_G1RDIStbTrsh_MinVal                                                           0x0
#define cAf6_pcfg_trshglbctr_G1RDIStbTrsh_RstVal                                                           0x1

/*--------------------------------------
BitField Name: C2PlmStbTrsh
BitField Type: RW
BitField Desc: C2 Path Signal Lable Stable Thershold
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_C2PlmStbTrsh_Bit_Start                                                          8
#define cAf6_pcfg_trshglbctr_C2PlmStbTrsh_Bit_End                                                           11
#define cAf6_pcfg_trshglbctr_C2PlmStbTrsh_Mask                                                        cBit11_8
#define cAf6_pcfg_trshglbctr_C2PlmStbTrsh_Shift                                                              8
#define cAf6_pcfg_trshglbctr_C2PlmStbTrsh_MaxVal                                                           0xf
#define cAf6_pcfg_trshglbctr_C2PlmStbTrsh_MinVal                                                           0x0
#define cAf6_pcfg_trshglbctr_C2PlmStbTrsh_RstVal                                                           0x1

/*--------------------------------------
BitField Name: JnStbTrsh
BitField Type: RW
BitField Desc: J1/J2 Message Stable Threshold
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_JnStbTrsh_Bit_Start                                                             4
#define cAf6_pcfg_trshglbctr_JnStbTrsh_Bit_End                                                               7
#define cAf6_pcfg_trshglbctr_JnStbTrsh_Mask                                                            cBit7_4
#define cAf6_pcfg_trshglbctr_JnStbTrsh_Shift                                                                 4
#define cAf6_pcfg_trshglbctr_JnStbTrsh_MaxVal                                                              0xf
#define cAf6_pcfg_trshglbctr_JnStbTrsh_MinVal                                                              0x0
#define cAf6_pcfg_trshglbctr_JnStbTrsh_RstVal                                                              0x1

/*--------------------------------------
BitField Name: debound
BitField Type: RW
BitField Desc: Debound Threshold
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_pcfg_trshglbctr_debound_Bit_Start                                                               0
#define cAf6_pcfg_trshglbctr_debound_Bit_End                                                                 3
#define cAf6_pcfg_trshglbctr_debound_Mask                                                              cBit3_0
#define cAf6_pcfg_trshglbctr_debound_Shift                                                                   0
#define cAf6_pcfg_trshglbctr_debound_MaxVal                                                                0xf
#define cAf6_pcfg_trshglbctr_debound_MinVal                                                                0x0
#define cAf6_pcfg_trshglbctr_debound_RstVal                                                                0x1


/*------------------------------------------------------------------------------
Reg Name   : POH Hi-order Path Over Head Grabber
Reg Addr   : 0x02_4000
Reg Formula: 0x02_4000 + $stsid * 8 + $sliceid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
Reg Desc   : 
This register is used to grabber Hi-Order Path Overhead

------------------------------------------------------------------------------*/
#define cAf6Reg_pohstspohgrb_Base                                                                     0x024000
#define cAf6Reg_pohstspohgrb(sliceid, stsid)                                    (0x024000+(stsid)*8+(sliceid))
#define cAf6Reg_pohstspohgrb_WidthVal                                                                       64
#define cAf6Reg_pohstspohgrb_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: K3
BitField Type: RO
BitField Desc: K3 byte
BitField Bits: [63:56]
--------------------------------------*/
#define cAf6_pohstspohgrb_K3_Bit_Start                                                                      56
#define cAf6_pohstspohgrb_K3_Bit_End                                                                        63
#define cAf6_pohstspohgrb_K3_Mask                                                                    cBit31_24
#define cAf6_pohstspohgrb_K3_Shift                                                                          24
#define cAf6_pohstspohgrb_K3_MaxVal                                                                        0x0
#define cAf6_pohstspohgrb_K3_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_K3_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: F3
BitField Type: RO
BitField Desc: F3 byte
BitField Bits: [55:48]
--------------------------------------*/
#define cAf6_pohstspohgrb_F3_Bit_Start                                                                      48
#define cAf6_pohstspohgrb_F3_Bit_End                                                                        55
#define cAf6_pohstspohgrb_F3_Mask                                                                    cBit23_16
#define cAf6_pohstspohgrb_F3_Shift                                                                          16
#define cAf6_pohstspohgrb_F3_MaxVal                                                                        0x0
#define cAf6_pohstspohgrb_F3_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_F3_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: H4
BitField Type: RO
BitField Desc: H4 byte
BitField Bits: [47:40]
--------------------------------------*/
#define cAf6_pohstspohgrb_H4_Bit_Start                                                                      40
#define cAf6_pohstspohgrb_H4_Bit_End                                                                        47
#define cAf6_pohstspohgrb_H4_Mask                                                                     cBit15_8
#define cAf6_pohstspohgrb_H4_Shift                                                                           8
#define cAf6_pohstspohgrb_H4_MaxVal                                                                        0x0
#define cAf6_pohstspohgrb_H4_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_H4_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: F2
BitField Type: RO
BitField Desc: F2 byte
BitField Bits: [39:32]
--------------------------------------*/
#define cAf6_pohstspohgrb_F2_Bit_Start                                                                      32
#define cAf6_pohstspohgrb_F2_Bit_End                                                                        39
#define cAf6_pohstspohgrb_F2_Mask                                                                      cBit7_0
#define cAf6_pohstspohgrb_F2_Shift                                                                           0
#define cAf6_pohstspohgrb_F2_MaxVal                                                                        0x0
#define cAf6_pohstspohgrb_F2_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_F2_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: G1
BitField Type: RO
BitField Desc: G1 byte
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_pohstspohgrb_G1_Bit_Start                                                                      24
#define cAf6_pohstspohgrb_G1_Bit_End                                                                        31
#define cAf6_pohstspohgrb_G1_Mask                                                                    cBit31_24
#define cAf6_pohstspohgrb_G1_Shift                                                                          24
#define cAf6_pohstspohgrb_G1_MaxVal                                                                       0xff
#define cAf6_pohstspohgrb_G1_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_G1_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: C2
BitField Type: RO
BitField Desc: C2 byte
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pohstspohgrb_C2_Bit_Start                                                                      16
#define cAf6_pohstspohgrb_C2_Bit_End                                                                        23
#define cAf6_pohstspohgrb_C2_Mask                                                                    cBit23_16
#define cAf6_pohstspohgrb_C2_Shift                                                                          16
#define cAf6_pohstspohgrb_C2_MaxVal                                                                       0xff
#define cAf6_pohstspohgrb_C2_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_C2_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: N1
BitField Type: RO
BitField Desc: N1 byte
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_pohstspohgrb_N1_Bit_Start                                                                       8
#define cAf6_pohstspohgrb_N1_Bit_End                                                                        15
#define cAf6_pohstspohgrb_N1_Mask                                                                     cBit15_8
#define cAf6_pohstspohgrb_N1_Shift                                                                           8
#define cAf6_pohstspohgrb_N1_MaxVal                                                                       0xff
#define cAf6_pohstspohgrb_N1_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_N1_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: J1
BitField Type: RO
BitField Desc: J1 byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohstspohgrb_J1_Bit_Start                                                                       0
#define cAf6_pohstspohgrb_J1_Bit_End                                                                         7
#define cAf6_pohstspohgrb_J1_Mask                                                                      cBit7_0
#define cAf6_pohstspohgrb_J1_Shift                                                                           0
#define cAf6_pohstspohgrb_J1_MaxVal                                                                       0xff
#define cAf6_pohstspohgrb_J1_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_J1_RstVal                                                                        0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Lo-order VT Over Head Grabber
Reg Addr   : 0x02_6000
Reg Formula: 0x02_6000 + $sliceid*1344 + $stsid*28 + $vtid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $vtid(0-27): VT Identification
Reg Desc   : 
This register is used to grabber Lo-Order Path Overhead. Incase the TU3 mode, the $vtid = 0, using for Tu3 POH grabber.
Incase VT mode, the $vtid = 0-27, using for VT POH grabber.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohvtpohgrb_Base                                                                      0x026000
#define cAf6Reg_pohvtpohgrb(sliceid, stsid, vtid)                     (0x026000+(sliceid)*1344+(stsid)*28+(vtid))
#define cAf6Reg_pohvtpohgrb_WidthVal                                                                        32
#define cAf6Reg_pohvtpohgrb_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: Byte3
BitField Type: RO
BitField Desc: G1 byte or K3 byte or K4 byte
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte3_Bit_Start                                                                    24
#define cAf6_pohvtpohgrb_Byte3_Bit_End                                                                      31
#define cAf6_pohvtpohgrb_Byte3_Mask                                                                  cBit31_24
#define cAf6_pohvtpohgrb_Byte3_Shift                                                                        24
#define cAf6_pohvtpohgrb_Byte3_MaxVal                                                                     0xff
#define cAf6_pohvtpohgrb_Byte3_MinVal                                                                      0x0
#define cAf6_pohvtpohgrb_Byte3_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: Byte2
BitField Type: RO
BitField Desc: C2 byte or F3 byte or N2 byte
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte2_Bit_Start                                                                    16
#define cAf6_pohvtpohgrb_Byte2_Bit_End                                                                      23
#define cAf6_pohvtpohgrb_Byte2_Mask                                                                  cBit23_16
#define cAf6_pohvtpohgrb_Byte2_Shift                                                                        16
#define cAf6_pohvtpohgrb_Byte2_MaxVal                                                                     0xff
#define cAf6_pohvtpohgrb_Byte2_MinVal                                                                      0x0
#define cAf6_pohvtpohgrb_Byte2_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: Byte1
BitField Type: RO
BitField Desc: N1 byte or H4 byte or J2 byte
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte1_Bit_Start                                                                     8
#define cAf6_pohvtpohgrb_Byte1_Bit_End                                                                      15
#define cAf6_pohvtpohgrb_Byte1_Mask                                                                   cBit15_8
#define cAf6_pohvtpohgrb_Byte1_Shift                                                                         8
#define cAf6_pohvtpohgrb_Byte1_MaxVal                                                                     0xff
#define cAf6_pohvtpohgrb_Byte1_MinVal                                                                      0x0
#define cAf6_pohvtpohgrb_Byte1_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: Byte0
BitField Type: RO
BitField Desc: J1 byte or F2 byte or V5 byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohvtpohgrb_Byte0_Bit_Start                                                                     0
#define cAf6_pohvtpohgrb_Byte0_Bit_End                                                                       7
#define cAf6_pohvtpohgrb_Byte0_Mask                                                                    cBit7_0
#define cAf6_pohvtpohgrb_Byte0_Shift                                                                         0
#define cAf6_pohvtpohgrb_Byte0_MaxVal                                                                     0xff
#define cAf6_pohvtpohgrb_Byte0_MinVal                                                                      0x0
#define cAf6_pohvtpohgrb_Byte0_RstVal                                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE STS/TU3 Control Register
Reg Addr   : 0x02_A000
Reg Formula: 0x02_A000 + $sliceid*96 + $stsid*2 + $tu3en
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $tu3en(0-1): Tu3enable, 0: STS, 1:Tu3
Reg Desc   : 
This register is used to configure the POH Hi-order Path Monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpestsctr_Base                                                                     0x02A000
#define cAf6Reg_pohcpestsctr(sliceid, stsid, tu3en)                   (0x02A000+(sliceid)*96+(stsid)*2+(tu3en))
#define cAf6Reg_pohcpestsctr_WidthVal                                                                       32
#define cAf6Reg_pohcpestsctr_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: PlmEnb
BitField Type: RW
BitField Desc: PLM enable
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pohcpestsctr_PlmEnb_Bit_Start                                                                  19
#define cAf6_pohcpestsctr_PlmEnb_Bit_End                                                                    19
#define cAf6_pohcpestsctr_PlmEnb_Mask                                                                   cBit19
#define cAf6_pohcpestsctr_PlmEnb_Shift                                                                      19
#define cAf6_pohcpestsctr_PlmEnb_MaxVal                                                                    0x1
#define cAf6_pohcpestsctr_PlmEnb_MinVal                                                                    0x0
#define cAf6_pohcpestsctr_PlmEnb_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: VcaisDstren
BitField Type: RW
BitField Desc: VcaisDstren
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pohcpestsctr_VcaisDstren_Bit_Start                                                             18
#define cAf6_pohcpestsctr_VcaisDstren_Bit_End                                                               18
#define cAf6_pohcpestsctr_VcaisDstren_Mask                                                              cBit18
#define cAf6_pohcpestsctr_VcaisDstren_Shift                                                                 18
#define cAf6_pohcpestsctr_VcaisDstren_MaxVal                                                               0x1
#define cAf6_pohcpestsctr_VcaisDstren_MinVal                                                               0x0
#define cAf6_pohcpestsctr_VcaisDstren_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PlmDstren
BitField Type: RW
BitField Desc: PlmDstren
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pohcpestsctr_PlmDstren_Bit_Start                                                               17
#define cAf6_pohcpestsctr_PlmDstren_Bit_End                                                                 17
#define cAf6_pohcpestsctr_PlmDstren_Mask                                                                cBit17
#define cAf6_pohcpestsctr_PlmDstren_Shift                                                                   17
#define cAf6_pohcpestsctr_PlmDstren_MaxVal                                                                 0x1
#define cAf6_pohcpestsctr_PlmDstren_MinVal                                                                 0x0
#define cAf6_pohcpestsctr_PlmDstren_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: UneqDstren
BitField Type: RW
BitField Desc: UneqDstren
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pohcpestsctr_UneqDstren_Bit_Start                                                              16
#define cAf6_pohcpestsctr_UneqDstren_Bit_End                                                                16
#define cAf6_pohcpestsctr_UneqDstren_Mask                                                               cBit16
#define cAf6_pohcpestsctr_UneqDstren_Shift                                                                  16
#define cAf6_pohcpestsctr_UneqDstren_MaxVal                                                                0x1
#define cAf6_pohcpestsctr_UneqDstren_MinVal                                                                0x0
#define cAf6_pohcpestsctr_UneqDstren_RstVal                                                                0x0

/*--------------------------------------
BitField Name: TimDstren
BitField Type: RW
BitField Desc: TimDstren
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pohcpestsctr_TimDstren_Bit_Start                                                               15
#define cAf6_pohcpestsctr_TimDstren_Bit_End                                                                 15
#define cAf6_pohcpestsctr_TimDstren_Mask                                                                cBit15
#define cAf6_pohcpestsctr_TimDstren_Shift                                                                   15
#define cAf6_pohcpestsctr_TimDstren_MaxVal                                                                 0x1
#define cAf6_pohcpestsctr_TimDstren_MinVal                                                                 0x0
#define cAf6_pohcpestsctr_TimDstren_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: Sdhmode
BitField Type: RW
BitField Desc: SDH mode
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpestsctr_Sdhmode_Bit_Start                                                                 14
#define cAf6_pohcpestsctr_Sdhmode_Bit_End                                                                   14
#define cAf6_pohcpestsctr_Sdhmode_Mask                                                                  cBit14
#define cAf6_pohcpestsctr_Sdhmode_Shift                                                                     14
#define cAf6_pohcpestsctr_Sdhmode_MaxVal                                                                   0x1
#define cAf6_pohcpestsctr_Sdhmode_MinVal                                                                   0x0
#define cAf6_pohcpestsctr_Sdhmode_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: Blkmden
BitField Type: RW
BitField Desc: Block mode BIP
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pohcpestsctr_Blkmden_Bit_Start                                                                 13
#define cAf6_pohcpestsctr_Blkmden_Bit_End                                                                   13
#define cAf6_pohcpestsctr_Blkmden_Mask                                                                  cBit13
#define cAf6_pohcpestsctr_Blkmden_Shift                                                                     13
#define cAf6_pohcpestsctr_Blkmden_MaxVal                                                                   0x1
#define cAf6_pohcpestsctr_Blkmden_MinVal                                                                   0x0
#define cAf6_pohcpestsctr_Blkmden_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ERDIenb
BitField Type: RW
BitField Desc: Enable E-RDI
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pohcpestsctr_ERDIenb_Bit_Start                                                                 12
#define cAf6_pohcpestsctr_ERDIenb_Bit_End                                                                   12
#define cAf6_pohcpestsctr_ERDIenb_Mask                                                                  cBit12
#define cAf6_pohcpestsctr_ERDIenb_Shift                                                                     12
#define cAf6_pohcpestsctr_ERDIenb_MaxVal                                                                   0x1
#define cAf6_pohcpestsctr_ERDIenb_MinVal                                                                   0x0
#define cAf6_pohcpestsctr_ERDIenb_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: PslExp
BitField Type: RW
BitField Desc: C2 Expected Path Signal Lable Value
BitField Bits: [11:4]
--------------------------------------*/
#define cAf6_pohcpestsctr_PslExp_Bit_Start                                                                   4
#define cAf6_pohcpestsctr_PslExp_Bit_End                                                                    11
#define cAf6_pohcpestsctr_PslExp_Mask                                                                 cBit11_4
#define cAf6_pohcpestsctr_PslExp_Shift                                                                       4
#define cAf6_pohcpestsctr_PslExp_MaxVal                                                                   0xff
#define cAf6_pohcpestsctr_PslExp_MinVal                                                                    0x0
#define cAf6_pohcpestsctr_PslExp_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: TimEnb
BitField Type: RW
BitField Desc: Enable Monitor TIM
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pohcpestsctr_TimEnb_Bit_Start                                                                   3
#define cAf6_pohcpestsctr_TimEnb_Bit_End                                                                     3
#define cAf6_pohcpestsctr_TimEnb_Mask                                                                    cBit3
#define cAf6_pohcpestsctr_TimEnb_Shift                                                                       3
#define cAf6_pohcpestsctr_TimEnb_MaxVal                                                                    0x1
#define cAf6_pohcpestsctr_TimEnb_MinVal                                                                    0x0
#define cAf6_pohcpestsctr_TimEnb_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: Reiblkmden
BitField Type: RW
BitField Desc: Block mode REI
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pohcpestsctr_Reiblkmden_Bit_Start                                                               2
#define cAf6_pohcpestsctr_Reiblkmden_Bit_End                                                                 2
#define cAf6_pohcpestsctr_Reiblkmden_Mask                                                                cBit2
#define cAf6_pohcpestsctr_Reiblkmden_Shift                                                                   2
#define cAf6_pohcpestsctr_Reiblkmden_MaxVal                                                                0x1
#define cAf6_pohcpestsctr_Reiblkmden_MinVal                                                                0x0
#define cAf6_pohcpestsctr_Reiblkmden_RstVal                                                                0x0

/*--------------------------------------
BitField Name: J1mode
BitField Type: RW
BitField Desc: 0: 1Byte 1:16Byte 2:64byte 3:Floating
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pohcpestsctr_J1mode_Bit_Start                                                                   0
#define cAf6_pohcpestsctr_J1mode_Bit_End                                                                     1
#define cAf6_pohcpestsctr_J1mode_Mask                                                                  cBit1_0
#define cAf6_pohcpestsctr_J1mode_Shift                                                                       0
#define cAf6_pohcpestsctr_J1mode_MaxVal                                                                    0x3
#define cAf6_pohcpestsctr_J1mode_MinVal                                                                    0x0
#define cAf6_pohcpestsctr_J1mode_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE VT Control Register
Reg Addr   : 0x02_8000
Reg Formula: 0x02_8000 + $sliceid*1344 + $stsid*28 +$vtid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $vtid(0-27):Vt Identification
Reg Desc   : 
This register is used to configure the POH Lo-order Path Monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpevtctr_Base                                                                      0x028000
#define cAf6Reg_pohcpevtctr(sliceid, stsid, vtid)                     (0x028000+(sliceid)*1344+(stsid)*28+(vtid))
#define cAf6Reg_pohcpevtctr_WidthVal                                                                        32
#define cAf6Reg_pohcpevtctr_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: PlmEnb
BitField Type: RW
BitField Desc: VcaisDstren
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpevtctr_PlmEnb_Bit_Start                                                                   14
#define cAf6_pohcpevtctr_PlmEnb_Bit_End                                                                     14
#define cAf6_pohcpevtctr_PlmEnb_Mask                                                                    cBit14
#define cAf6_pohcpevtctr_PlmEnb_Shift                                                                       14
#define cAf6_pohcpevtctr_PlmEnb_MaxVal                                                                     0x1
#define cAf6_pohcpevtctr_PlmEnb_MinVal                                                                     0x0
#define cAf6_pohcpevtctr_PlmEnb_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: VcaisDstren
BitField Type: RW
BitField Desc: VcaisDstren
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pohcpevtctr_VcaisDstren_Bit_Start                                                              13
#define cAf6_pohcpevtctr_VcaisDstren_Bit_End                                                                13
#define cAf6_pohcpevtctr_VcaisDstren_Mask                                                               cBit13
#define cAf6_pohcpevtctr_VcaisDstren_Shift                                                                  13
#define cAf6_pohcpevtctr_VcaisDstren_MaxVal                                                                0x1
#define cAf6_pohcpevtctr_VcaisDstren_MinVal                                                                0x0
#define cAf6_pohcpevtctr_VcaisDstren_RstVal                                                                0x0

/*--------------------------------------
BitField Name: PlmDstren
BitField Type: RW
BitField Desc: PlmDstren
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pohcpevtctr_PlmDstren_Bit_Start                                                                12
#define cAf6_pohcpevtctr_PlmDstren_Bit_End                                                                  12
#define cAf6_pohcpevtctr_PlmDstren_Mask                                                                 cBit12
#define cAf6_pohcpevtctr_PlmDstren_Shift                                                                    12
#define cAf6_pohcpevtctr_PlmDstren_MaxVal                                                                  0x1
#define cAf6_pohcpevtctr_PlmDstren_MinVal                                                                  0x0
#define cAf6_pohcpevtctr_PlmDstren_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: UneqDstren
BitField Type: RW
BitField Desc: UneqDstren
BitField Bits: [11]
--------------------------------------*/
#define cAf6_pohcpevtctr_UneqDstren_Bit_Start                                                               11
#define cAf6_pohcpevtctr_UneqDstren_Bit_End                                                                 11
#define cAf6_pohcpevtctr_UneqDstren_Mask                                                                cBit11
#define cAf6_pohcpevtctr_UneqDstren_Shift                                                                   11
#define cAf6_pohcpevtctr_UneqDstren_MaxVal                                                                 0x1
#define cAf6_pohcpevtctr_UneqDstren_MinVal                                                                 0x0
#define cAf6_pohcpevtctr_UneqDstren_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: TimDstren
BitField Type: RW
BitField Desc: TimDstren
BitField Bits: [10]
--------------------------------------*/
#define cAf6_pohcpevtctr_TimDstren_Bit_Start                                                                10
#define cAf6_pohcpevtctr_TimDstren_Bit_End                                                                  10
#define cAf6_pohcpevtctr_TimDstren_Mask                                                                 cBit10
#define cAf6_pohcpevtctr_TimDstren_Shift                                                                    10
#define cAf6_pohcpevtctr_TimDstren_MaxVal                                                                  0x1
#define cAf6_pohcpevtctr_TimDstren_MinVal                                                                  0x0
#define cAf6_pohcpevtctr_TimDstren_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: VSdhmode
BitField Type: RW
BitField Desc: SDH mode
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pohcpevtctr_VSdhmode_Bit_Start                                                                  9
#define cAf6_pohcpevtctr_VSdhmode_Bit_End                                                                    9
#define cAf6_pohcpevtctr_VSdhmode_Mask                                                                   cBit9
#define cAf6_pohcpevtctr_VSdhmode_Shift                                                                      9
#define cAf6_pohcpevtctr_VSdhmode_MaxVal                                                                   0x1
#define cAf6_pohcpevtctr_VSdhmode_MinVal                                                                   0x0
#define cAf6_pohcpevtctr_VSdhmode_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: VBlkmden
BitField Type: RW
BitField Desc: Block mode BIP
BitField Bits: [8]
--------------------------------------*/
#define cAf6_pohcpevtctr_VBlkmden_Bit_Start                                                                  8
#define cAf6_pohcpevtctr_VBlkmden_Bit_End                                                                    8
#define cAf6_pohcpevtctr_VBlkmden_Mask                                                                   cBit8
#define cAf6_pohcpevtctr_VBlkmden_Shift                                                                      8
#define cAf6_pohcpevtctr_VBlkmden_MaxVal                                                                   0x1
#define cAf6_pohcpevtctr_VBlkmden_MinVal                                                                   0x0
#define cAf6_pohcpevtctr_VBlkmden_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ERDIenb
BitField Type: RW
BitField Desc: Enable E-RDI
BitField Bits: [7]
--------------------------------------*/
#define cAf6_pohcpevtctr_ERDIenb_Bit_Start                                                                   7
#define cAf6_pohcpevtctr_ERDIenb_Bit_End                                                                     7
#define cAf6_pohcpevtctr_ERDIenb_Mask                                                                    cBit7
#define cAf6_pohcpevtctr_ERDIenb_Shift                                                                       7
#define cAf6_pohcpevtctr_ERDIenb_MaxVal                                                                    0x1
#define cAf6_pohcpevtctr_ERDIenb_MinVal                                                                    0x0
#define cAf6_pohcpevtctr_ERDIenb_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: VslExp
BitField Type: RW
BitField Desc: V5 Expected Path Signal Lable Value
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_pohcpevtctr_VslExp_Bit_Start                                                                    4
#define cAf6_pohcpevtctr_VslExp_Bit_End                                                                      6
#define cAf6_pohcpevtctr_VslExp_Mask                                                                   cBit6_4
#define cAf6_pohcpevtctr_VslExp_Shift                                                                        4
#define cAf6_pohcpevtctr_VslExp_MaxVal                                                                     0x7
#define cAf6_pohcpevtctr_VslExp_MinVal                                                                     0x0
#define cAf6_pohcpevtctr_VslExp_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: TimEnb
BitField Type: RW
BitField Desc: Enable Monitor TIM
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pohcpevtctr_TimEnb_Bit_Start                                                                    3
#define cAf6_pohcpevtctr_TimEnb_Bit_End                                                                      3
#define cAf6_pohcpevtctr_TimEnb_Mask                                                                     cBit3
#define cAf6_pohcpevtctr_TimEnb_Shift                                                                        3
#define cAf6_pohcpevtctr_TimEnb_MaxVal                                                                     0x1
#define cAf6_pohcpevtctr_TimEnb_MinVal                                                                     0x0
#define cAf6_pohcpevtctr_TimEnb_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: Reiblkmden
BitField Type: RW
BitField Desc: Block mode REI
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pohcpevtctr_Reiblkmden_Bit_Start                                                                2
#define cAf6_pohcpevtctr_Reiblkmden_Bit_End                                                                  2
#define cAf6_pohcpevtctr_Reiblkmden_Mask                                                                 cBit2
#define cAf6_pohcpevtctr_Reiblkmden_Shift                                                                    2
#define cAf6_pohcpevtctr_Reiblkmden_MaxVal                                                                 0x1
#define cAf6_pohcpevtctr_Reiblkmden_MinVal                                                                 0x0
#define cAf6_pohcpevtctr_Reiblkmden_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: J2mode
BitField Type: RW
BitField Desc: 0: 1Byte 1:16Byte 2:64byte 3:Floating
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pohcpevtctr_J2mode_Bit_Start                                                                    0
#define cAf6_pohcpevtctr_J2mode_Bit_End                                                                      1
#define cAf6_pohcpevtctr_J2mode_Mask                                                                   cBit1_0
#define cAf6_pohcpevtctr_J2mode_Shift                                                                        0
#define cAf6_pohcpevtctr_J2mode_MaxVal                                                                     0x3
#define cAf6_pohcpevtctr_J2mode_MinVal                                                                     0x0
#define cAf6_pohcpevtctr_J2mode_RstVal                                                                     0x0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE STS Status Register
Reg Addr   : 0x02_D500
Reg Formula: 0x02_D500 + $sliceid*48 + $stsid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
Reg Desc   : 
This register is used to get POH Hi-order status of monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpestssta_Base                                                                     0x02D500
#define cAf6Reg_pohcpestssta(sliceid, stsid)                                   (0x02D500+(sliceid)*48+(stsid))
#define cAf6Reg_pohcpestssta_WidthVal                                                                       32
#define cAf6Reg_pohcpestssta_WriteMask                                                                     0x0


/*--------------------------------------
BitField Name: JnStbCnt
BitField Type: RO
BitField Desc: Jn Stable Counter
BitField Bits: [30:27]
--------------------------------------*/
#define cAf6_pohcpestssta_JnStbCnt_Mask                                                              cBit30_27
#define cAf6_pohcpestssta_JnStbCnt_Shift                                                                    27

/*--------------------------------------
BitField Name: JnMisExpMsg
BitField Type: RO
BitField Desc: Jn mismatch state between the accepted Jn message and expected Jn
message
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pohcpestssta_JnMisExpMsg_Mask                                                              cBit26
#define cAf6_pohcpestssta_JnMisExpMsg_Shift                                                                 26

/*--------------------------------------
BitField Name: JnMisCurMsg
BitField Type: RO
BitField Desc: Jn mismatch state between the accepted Jn message and new Jn
message
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pohcpestssta_JnMisCurMsg_Mask                                                              cBit25
#define cAf6_pohcpestssta_JnMisCurMsg_Shift                                                                 25

/*--------------------------------------
BitField Name: JnBadFrame
BitField Type: RO
BitField Desc: Jn framing is not matched to expected Jn mode
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pohcpestssta_JnBadFrame_Mask                                                               cBit24
#define cAf6_pohcpestssta_JnBadFrame_Shift                                                                  24

/*--------------------------------------
BitField Name: JnState
BitField Type: RO
BitField Desc: Jn State Machine
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_pohcpestssta_JnState_Mask                                                               cBit23_22
#define cAf6_pohcpestssta_JnState_Shift                                                                     22

/*--------------------------------------
BitField Name: JnFrmCnt
BitField Type: RO
BitField Desc: Jn frame counter during Jn in-frame state
BitField Bits: [21:16]
--------------------------------------*/
#define cAf6_pohcpestssta_JnFrmCnt_Mask                                                              cBit21_16
#define cAf6_pohcpestssta_JnFrmCnt_Shift                                                                    16

/*--------------------------------------
BitField Name: JnTimPCurSta
BitField Type: RO
BitField Desc: Jn TIM current status.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pohcpestssta_JnTimPCurSta_Mask                                                             cBit15
#define cAf6_pohcpestssta_JnTimPCurSta_Shift                                                                15

/*--------------------------------------
BitField Name: JnStbState
BitField Type: RO
BitField Desc: Jn Stable State
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpestssta_JnStbState_Mask                                                               cBit14
#define cAf6_pohcpestssta_JnStbState_Shift                                                                  14

/*--------------------------------------
BitField Name: C2stbsta
BitField Type: RO
BitField Desc: C2 stable status
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pohcpestssta_C2stbsta_Bit_Start                                                                12
#define cAf6_pohcpestssta_C2stbsta_Bit_End                                                                  12
#define cAf6_pohcpestssta_C2stbsta_Mask                                                                 cBit12
#define cAf6_pohcpestssta_C2stbsta_Shift                                                                    12
#define cAf6_pohcpestssta_C2stbsta_MaxVal                                                                  0x1
#define cAf6_pohcpestssta_C2stbsta_MinVal                                                                  0x0
#define cAf6_pohcpestssta_C2stbsta_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: C2stbcnt
BitField Type: RO
BitField Desc: C2 stable counter
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_pohcpestssta_C2stbcnt_Bit_Start                                                                 8
#define cAf6_pohcpestssta_C2stbcnt_Bit_End                                                                  11
#define cAf6_pohcpestssta_C2stbcnt_Mask                                                               cBit11_8
#define cAf6_pohcpestssta_C2stbcnt_Shift                                                                     8
#define cAf6_pohcpestssta_C2stbcnt_MaxVal                                                                  0xf
#define cAf6_pohcpestssta_C2stbcnt_MinVal                                                                  0x0
#define cAf6_pohcpestssta_C2stbcnt_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: C2acpt
BitField Type: RO
BitField Desc: C2 accept byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohcpestssta_C2acpt_Bit_Start                                                                   0
#define cAf6_pohcpestssta_C2acpt_Bit_End                                                                     7
#define cAf6_pohcpestssta_C2acpt_Mask                                                                  cBit7_0
#define cAf6_pohcpestssta_C2acpt_Shift                                                                       0
#define cAf6_pohcpestssta_C2acpt_MaxVal                                                                   0xff
#define cAf6_pohcpestssta_C2acpt_MinVal                                                                    0x0
#define cAf6_pohcpestssta_C2acpt_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE VT/TU3 Status Register
Reg Addr   : 0x02_C000
Reg Formula: 0x02_C000 + $sliceid*1344 + $stsid*28 +$vtid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $vtid(0-27):Vt Identification
Reg Desc   : 
This register is used to get POH Lo-order status of monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpevtsta_Base                                                                      0x02C000
#define cAf6Reg_pohcpevtsta(sliceid, stsid, vtid)                     (0x02C000+(sliceid)*1344+(stsid)*28+(vtid))
#define cAf6Reg_pohcpevtsta_WidthVal                                                                        32
#define cAf6Reg_pohcpevtsta_WriteMask                                                                      0x0


/*--------------------------------------
BitField Name: JnStbCnt
BitField Type: RO
BitField Desc: Jn Stable Counter
BitField Bits: [30:27]
--------------------------------------*/
#define cAf6_pohcpevtsta_JnStbCnt_Mask                                                               cBit30_27
#define cAf6_pohcpevtsta_JnStbCnt_Shift                                                                     27

/*--------------------------------------
BitField Name: JnMisExpMsg
BitField Type: RO
BitField Desc: Jn mismatch state between the accepted Jn message and expected Jn
message
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pohcpevtsta_JnMisExpMsg_Mask                                                               cBit26
#define cAf6_pohcpevtsta_JnMisExpMsg_Shift                                                                  26

/*--------------------------------------
BitField Name: JnMisCurMsg
BitField Type: RO
BitField Desc: Jn mismatch state between the accepted Jn message and new Jn
message
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pohcpevtsta_JnMisCurMsg_Mask                                                               cBit25
#define cAf6_pohcpevtsta_JnMisCurMsg_Shift                                                                  25

/*--------------------------------------
BitField Name: JnBadFrame
BitField Type: RO
BitField Desc: Jn framing is not matched to expected Jn mode
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pohcpevtsta_JnBadFrame_Mask                                                                cBit24
#define cAf6_pohcpevtsta_JnBadFrame_Shift                                                                   24

/*--------------------------------------
BitField Name: JnState
BitField Type: RO
BitField Desc: Jn State Machine
BitField Bits: [23:22]
--------------------------------------*/
#define cAf6_pohcpevtsta_JnState_Mask                                                                cBit23_22
#define cAf6_pohcpevtsta_JnState_Shift                                                                      22

/*--------------------------------------
BitField Name: JnFrmCnt
BitField Type: RO
BitField Desc: Jn frame counter during Jn in-frame state
BitField Bits: [21:16]
--------------------------------------*/
#define cAf6_pohcpevtsta_JnFrmCnt_Mask                                                               cBit21_16
#define cAf6_pohcpevtsta_JnFrmCnt_Shift                                                                     16

/*--------------------------------------
BitField Name: JnTimPCurSta
BitField Type: RO
BitField Desc: Jn TIM current status.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pohcpevtsta_JnTimPCurSta_Mask                                                              cBit15
#define cAf6_pohcpevtsta_JnTimPCurSta_Shift                                                                 15

/*--------------------------------------
BitField Name: JnStbState
BitField Type: RO
BitField Desc: Jn Stable State
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpevtsta_JnStbState_Mask                                                                cBit14
#define cAf6_pohcpevtsta_JnStbState_Shift                                                                   14

/*--------------------------------------
BitField Name: Vslstbsta
BitField Type: RO
BitField Desc: VSL stable status
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pohcpevtsta_Vslstbsta_Bit_Start                                                                13
#define cAf6_pohcpevtsta_Vslstbsta_Bit_End                                                                  13
#define cAf6_pohcpevtsta_Vslstbsta_Mask                                                                 cBit13
#define cAf6_pohcpevtsta_Vslstbsta_Shift                                                                    13
#define cAf6_pohcpevtsta_Vslstbsta_MaxVal                                                                  0x1
#define cAf6_pohcpevtsta_Vslstbsta_MinVal                                                                  0x0
#define cAf6_pohcpevtsta_Vslstbsta_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: Vslstbcnt
BitField Type: RO
BitField Desc: VSL stable counter
BitField Bits: [12:9]
--------------------------------------*/
#define cAf6_pohcpevtsta_Vslstbcnt_Bit_Start                                                                 9
#define cAf6_pohcpevtsta_Vslstbcnt_Bit_End                                                                  12
#define cAf6_pohcpevtsta_Vslstbcnt_Mask                                                               cBit12_9
#define cAf6_pohcpevtsta_Vslstbcnt_Shift                                                                     9
#define cAf6_pohcpevtsta_Vslstbcnt_MaxVal                                                                  0xf
#define cAf6_pohcpevtsta_Vslstbcnt_MinVal                                                                  0x0
#define cAf6_pohcpevtsta_Vslstbcnt_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: Vslacpt
BitField Type: RO
BitField Desc: VSL accept byte
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_pohcpevtsta_Vslacpt_Bit_Start                                                                   6
#define cAf6_pohcpevtsta_Vslacpt_Bit_End                                                                     8
#define cAf6_pohcpevtsta_Vslacpt_Mask                                                                  cBit8_6
#define cAf6_pohcpevtsta_Vslacpt_Shift                                                                       6
#define cAf6_pohcpevtsta_Vslacpt_MaxVal                                                                    0x7
#define cAf6_pohcpevtsta_Vslacpt_MinVal                                                                    0x0
#define cAf6_pohcpevtsta_Vslacpt_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: RFIstatus
BitField Type: RO
BitField Desc: RFI status
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_pohcpevtsta_RFIstatus_Bit_Start                                                                 0
#define cAf6_pohcpevtsta_RFIstatus_Bit_End                                                                   5
#define cAf6_pohcpevtsta_RFIstatus_Mask                                                                cBit5_0
#define cAf6_pohcpevtsta_RFIstatus_Shift                                                                     0
#define cAf6_pohcpevtsta_RFIstatus_MaxVal                                                                 0x3f
#define cAf6_pohcpevtsta_RFIstatus_MinVal                                                                  0x0
#define cAf6_pohcpevtsta_RFIstatus_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 STS Expected Message buffer
Reg Addr   : 0x0B_0000
Reg Formula: 0x0B_0000 + $sliceid*384 + $stsid*8 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J1 Expected Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstsexp_Base                                                                     0x0B0000
#define cAf6Reg_pohmsgstsexp(sliceid, stsid, msgid)                   (0x0B0000+(sliceid)*384+(stsid)*8+(msgid))
#define cAf6Reg_pohmsgstsexp_WidthVal                                                                       64
#define cAf6Reg_pohmsgstsexp_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: J1ExpMsg
BitField Type: RW
BitField Desc: J1 Expected Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstsexp_J1ExpMsg_Bit_Start                                                                 0
#define cAf6_pohmsgstsexp_J1ExpMsg_Bit_End                                                                  63
#define cAf6_pohmsgstsexp_J1ExpMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstsexp_J1ExpMsg_Shift_01                                                                  0
#define cAf6_pohmsgstsexp_J1ExpMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstsexp_J1ExpMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 STS Current Message buffer
Reg Addr   : 0x0B_1000
Reg Formula: 0x0B_1000 + $sliceid*384 + $stsid*8 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J1 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstscur_Base                                                                     0x0B1000
#define cAf6Reg_pohmsgstscur(sliceid, stsid, msgid)                   (0x0B1000+(sliceid)*384+(stsid)*8+(msgid))
#define cAf6Reg_pohmsgstscur_WidthVal                                                                       64
#define cAf6Reg_pohmsgstscur_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: J1CurMsg
BitField Type: RW
BitField Desc: J1 Current Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstscur_J1CurMsg_Bit_Start                                                                 0
#define cAf6_pohmsgstscur_J1CurMsg_Bit_End                                                                  63
#define cAf6_pohmsgstscur_J1CurMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstscur_J1CurMsg_Shift_01                                                                  0
#define cAf6_pohmsgstscur_J1CurMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstscur_J1CurMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J2 Expected Message buffer
Reg Addr   : 0x08_0000
Reg Formula: 0x08_0000 + $sliceid*10752 + $stsid*224 + $vtid*8 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $vtid(0-27): VT Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J2 Expected Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgvtexp_Base                                                                      0x080000
#define cAf6Reg_pohmsgvtexp(sliceid, stsid, vtid, msgid)              (0x080000+(sliceid)*10752+(stsid)*224+(vtid)*8+(msgid))
#define cAf6Reg_pohmsgvtexp_WidthVal                                                                        64
#define cAf6Reg_pohmsgvtexp_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: J2ExpMsg
BitField Type: RW
BitField Desc: J2 Expected Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgvtexp_J2ExpMsg_Bit_Start                                                                  0
#define cAf6_pohmsgvtexp_J2ExpMsg_Bit_End                                                                   63
#define cAf6_pohmsgvtexp_J2ExpMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgvtexp_J2ExpMsg_Shift_01                                                                   0
#define cAf6_pohmsgvtexp_J2ExpMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgvtexp_J2ExpMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J2 Current Message buffer
Reg Addr   : 0x09_0000
Reg Formula: 0x09_0000 + $sliceid*10752 + $stsid*224 + $vtid*8 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $vtid(0-27): VT Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J2 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgvtcur_Base                                                                      0x090000
#define cAf6Reg_pohmsgvtcur(sliceid, stsid, vtid, msgid)              (0x090000+(sliceid)*10752+(stsid)*224+(vtid)*8+(msgid))
#define cAf6Reg_pohmsgvtcur_WidthVal                                                                        64
#define cAf6Reg_pohmsgvtcur_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: J2CurMsg
BitField Type: RW
BitField Desc: J2 Current Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgvtcur_J2CurMsg_Bit_Start                                                                  0
#define cAf6_pohmsgvtcur_J2CurMsg_Bit_End                                                                   63
#define cAf6_pohmsgvtcur_J2CurMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgvtcur_J2CurMsg_Shift_01                                                                   0
#define cAf6_pohmsgvtcur_J2CurMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgvtcur_J2CurMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 Insert Message buffer
Reg Addr   : 0x0B_2000
Reg Formula: 0x0B_2000 + $sliceid*512 + $stsid*8 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J1 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstsins_Base                                                                     0x0B2000
#define cAf6Reg_pohmsgstsins(sliceid, stsid, msgid)                   (0x0B2000+(sliceid)*512+(stsid)*8+(msgid))
#define cAf6Reg_pohmsgstsins_WidthVal                                                                       64
#define cAf6Reg_pohmsgstsins_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: J1InsMsg
BitField Type: RW
BitField Desc: J1 Insert Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstsins_J1InsMsg_Bit_Start                                                                 0
#define cAf6_pohmsgstsins_J1InsMsg_Bit_End                                                                  63
#define cAf6_pohmsgstsins_J1InsMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstsins_J1InsMsg_Shift_01                                                                  0
#define cAf6_pohmsgstsins_J1InsMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstsins_J1InsMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J2 Insert Message buffer
Reg Addr   : 0x0A_0000
Reg Formula: 0x0A_0000 + $sliceid*10752 + $stsid*224 + $vtid*8 + $msgid
    Where  : 
           + $sliceid(0-3): Slice Identification
           + $stsid(0-47): STS Identification
           + $vtid(0-27): VT Identification
           + $msgid(0-7): Message ID
Reg Desc   : 
The J2 Insert Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgvtins_Base                                                                      0x0A0000
#define cAf6Reg_pohmsgvtins(sliceid, stsid, vtid, msgid)              (0x0A0000+(sliceid)*10752+(stsid)*224+(vtid)*8+(msgid))
#define cAf6Reg_pohmsgvtins_WidthVal                                                                        64
#define cAf6Reg_pohmsgvtins_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: J2InsMsg
BitField Type: RW
BitField Desc: J2 Insert Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgvtins_J2InsMsg_Bit_Start                                                                  0
#define cAf6_pohmsgvtins_J2InsMsg_Bit_End                                                                   63
#define cAf6_pohmsgvtins_J2InsMsg_Mask_01                                                             cBit31_0
#define cAf6_pohmsgvtins_J2InsMsg_Shift_01                                                                   0
#define cAf6_pohmsgvtins_J2InsMsg_Mask_02                                                             cBit31_0
#define cAf6_pohmsgvtins_J2InsMsg_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Control STS
Reg Addr   : 0x04_0400
Reg Formula: 0x04_0400 + $STS + $OCID*48
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3)  : Line ID
Reg Desc   : 
This register is used to control STS POH insert .

------------------------------------------------------------------------------*/
#define cAf6Reg_ter_ctrlhi_Base                                                                       0x040400
#define cAf6Reg_ter_ctrlhi(STS, OCID)                                               (0x040400+(STS)+(OCID)*48)
#define cAf6Reg_ter_ctrlhi_WidthVal                                                                         32
#define cAf6Reg_ter_ctrlhi_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: rdien
BitField Type: RW
BitField Desc: 1: send old version RDI code G1 bit6,7 : send ERDI version code
G1 bit6,7
BitField Bits: [8]
--------------------------------------*/
#define cAf6_ter_ctrlhi_rdien_Mask                                                                       cBit8
#define cAf6_ter_ctrlhi_rdien_Shift                                                                          8

/*--------------------------------------
BitField Name: g1spare
BitField Type: RW
BitField Desc: G1 spare value
BitField Bits: [6]
--------------------------------------*/
#define cAf6_ter_ctrlhi_g1spare_Bit_Start                                                                    6
#define cAf6_ter_ctrlhi_g1spare_Bit_End                                                                      6
#define cAf6_ter_ctrlhi_g1spare_Mask                                                                     cBit6
#define cAf6_ter_ctrlhi_g1spare_Shift                                                                        6
#define cAf6_ter_ctrlhi_g1spare_MaxVal                                                                     0x1
#define cAf6_ter_ctrlhi_g1spare_MinVal                                                                     0x0
#define cAf6_ter_ctrlhi_g1spare_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: plm
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if PLM detected
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ter_ctrlhi_plm_Bit_Start                                                                        5
#define cAf6_ter_ctrlhi_plm_Bit_End                                                                          5
#define cAf6_ter_ctrlhi_plm_Mask                                                                         cBit5
#define cAf6_ter_ctrlhi_plm_Shift                                                                            5
#define cAf6_ter_ctrlhi_plm_MaxVal                                                                         0x1
#define cAf6_ter_ctrlhi_plm_MinVal                                                                         0x0
#define cAf6_ter_ctrlhi_plm_RstVal                                                                         0x0

/*--------------------------------------
BitField Name: uneq
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if UNEQ detected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ter_ctrlhi_uneq_Bit_Start                                                                       4
#define cAf6_ter_ctrlhi_uneq_Bit_End                                                                         4
#define cAf6_ter_ctrlhi_uneq_Mask                                                                        cBit4
#define cAf6_ter_ctrlhi_uneq_Shift                                                                           4
#define cAf6_ter_ctrlhi_uneq_MaxVal                                                                        0x1
#define cAf6_ter_ctrlhi_uneq_MinVal                                                                        0x0
#define cAf6_ter_ctrlhi_uneq_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if TIM detected
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ter_ctrlhi_timmsk_Bit_Start                                                                     3
#define cAf6_ter_ctrlhi_timmsk_Bit_End                                                                       3
#define cAf6_ter_ctrlhi_timmsk_Mask                                                                      cBit3
#define cAf6_ter_ctrlhi_timmsk_Shift                                                                         3
#define cAf6_ter_ctrlhi_timmsk_MaxVal                                                                      0x1
#define cAf6_ter_ctrlhi_timmsk_MinVal                                                                      0x0
#define cAf6_ter_ctrlhi_timmsk_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: aislopmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if AIS,LOP detected
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ter_ctrlhi_aislopmsk_Bit_Start                                                                  2
#define cAf6_ter_ctrlhi_aislopmsk_Bit_End                                                                    2
#define cAf6_ter_ctrlhi_aislopmsk_Mask                                                                   cBit2
#define cAf6_ter_ctrlhi_aislopmsk_Shift                                                                      2
#define cAf6_ter_ctrlhi_aislopmsk_MaxVal                                                                   0x1
#define cAf6_ter_ctrlhi_aislopmsk_MinVal                                                                   0x0
#define cAf6_ter_ctrlhi_aislopmsk_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: jnfrmd
BitField Type: RW
BitField Desc: 0:1 byte, 1: 16/64byte
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ter_ctrlhi_jnfrmd_Bit_Start                                                                     0
#define cAf6_ter_ctrlhi_jnfrmd_Bit_End                                                                       1
#define cAf6_ter_ctrlhi_jnfrmd_Mask                                                                    cBit1_0
#define cAf6_ter_ctrlhi_jnfrmd_Shift                                                                         0
#define cAf6_ter_ctrlhi_jnfrmd_MaxVal                                                                      0x3
#define cAf6_ter_ctrlhi_jnfrmd_MinVal                                                                      0x0
#define cAf6_ter_ctrlhi_jnfrmd_RstVal                                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Control VT/TU3
Reg Addr   : 0x04_4000
Reg Formula: 0x04_4000 + $STS*28 + $OCID*1344 + $VT
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3)  : Line ID
           + $VT(0-27)
Reg Desc   : 
This register is used to control STS POH insert. TU3 is at VT ID = 0. Fields must be the same as ter_ctrlhi

------------------------------------------------------------------------------*/
#define cAf6Reg_ter_ctrllo_Base                                                                       0x044000
#define cAf6Reg_ter_ctrllo(STS, OCID, VT)                                 (0x044000+(STS)*28+(OCID)*1344+(VT))
#define cAf6Reg_ter_ctrllo_WidthVal                                                                         32
#define cAf6Reg_ter_ctrllo_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: rdien
BitField Type: RW
BitField Desc: 1: send old version RDI code Z7 bit5,6,7 : send ERDI version code
Z7 bit5,6,7
BitField Bits: [16]
--------------------------------------*/
#define cAf6_ter_ctrllo_rdien_Mask                                                                      cBit16
#define cAf6_ter_ctrllo_rdien_Shift                                                                         16

/*--------------------------------------
BitField Name: k4b0b1
BitField Type: RW
BitField Desc: K4b0b1 value
BitField Bits: [14:13]
--------------------------------------*/
#define cAf6_ter_ctrllo_k4b0b1_Bit_Start                                                                    13
#define cAf6_ter_ctrllo_k4b0b1_Bit_End                                                                      14
#define cAf6_ter_ctrllo_k4b0b1_Mask                                                                  cBit14_13
#define cAf6_ter_ctrllo_k4b0b1_Shift                                                                        13
#define cAf6_ter_ctrllo_k4b0b1_MaxVal                                                                      0x3
#define cAf6_ter_ctrllo_k4b0b1_MinVal                                                                      0x0
#define cAf6_ter_ctrllo_k4b0b1_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: k4aps
BitField Type: RW
BitField Desc: K4aps value
BitField Bits: [12:11]
--------------------------------------*/
#define cAf6_ter_ctrllo_k4aps_Bit_Start                                                                     11
#define cAf6_ter_ctrllo_k4aps_Bit_End                                                                       12
#define cAf6_ter_ctrllo_k4aps_Mask                                                                   cBit12_11
#define cAf6_ter_ctrllo_k4aps_Shift                                                                         11
#define cAf6_ter_ctrllo_k4aps_MaxVal                                                                       0x3
#define cAf6_ter_ctrllo_k4aps_MinVal                                                                       0x0
#define cAf6_ter_ctrllo_k4aps_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: k4spare
BitField Type: RW
BitField Desc: K4spare value
BitField Bits: [10]
--------------------------------------*/
#define cAf6_ter_ctrllo_k4spare_Bit_Start                                                                   10
#define cAf6_ter_ctrllo_k4spare_Bit_End                                                                     10
#define cAf6_ter_ctrllo_k4spare_Mask                                                                    cBit10
#define cAf6_ter_ctrllo_k4spare_Shift                                                                       10
#define cAf6_ter_ctrllo_k4spare_MaxVal                                                                     0x1
#define cAf6_ter_ctrllo_k4spare_MinVal                                                                     0x0
#define cAf6_ter_ctrllo_k4spare_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: rfival
BitField Type: RW
BitField Desc: RFI value
BitField Bits: [9]
--------------------------------------*/
#define cAf6_ter_ctrllo_rfival_Bit_Start                                                                     9
#define cAf6_ter_ctrllo_rfival_Bit_End                                                                       9
#define cAf6_ter_ctrllo_rfival_Mask                                                                      cBit9
#define cAf6_ter_ctrllo_rfival_Shift                                                                         9
#define cAf6_ter_ctrllo_rfival_MaxVal                                                                      0x1
#define cAf6_ter_ctrllo_rfival_MinVal                                                                      0x0
#define cAf6_ter_ctrllo_rfival_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: vslval
BitField Type: RW
BitField Desc: VT signal label value
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_ter_ctrllo_vslval_Bit_Start                                                                     6
#define cAf6_ter_ctrllo_vslval_Bit_End                                                                       8
#define cAf6_ter_ctrllo_vslval_Mask                                                                    cBit8_6
#define cAf6_ter_ctrllo_vslval_Shift                                                                         6
#define cAf6_ter_ctrllo_vslval_MaxVal                                                                      0x7
#define cAf6_ter_ctrllo_vslval_MinVal                                                                      0x0
#define cAf6_ter_ctrllo_vslval_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: plm
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if PLM detected
BitField Bits: [5]
--------------------------------------*/
#define cAf6_ter_ctrllo_plm_Bit_Start                                                                        5
#define cAf6_ter_ctrllo_plm_Bit_End                                                                          5
#define cAf6_ter_ctrllo_plm_Mask                                                                         cBit5
#define cAf6_ter_ctrllo_plm_Shift                                                                            5
#define cAf6_ter_ctrllo_plm_MaxVal                                                                         0x1
#define cAf6_ter_ctrllo_plm_MinVal                                                                         0x0
#define cAf6_ter_ctrllo_plm_RstVal                                                                         0x0

/*--------------------------------------
BitField Name: uneq
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if UNEQ detected
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ter_ctrllo_uneq_Bit_Start                                                                       4
#define cAf6_ter_ctrllo_uneq_Bit_End                                                                         4
#define cAf6_ter_ctrllo_uneq_Mask                                                                        cBit4
#define cAf6_ter_ctrllo_uneq_Shift                                                                           4
#define cAf6_ter_ctrllo_uneq_MaxVal                                                                        0x1
#define cAf6_ter_ctrllo_uneq_MinVal                                                                        0x0
#define cAf6_ter_ctrllo_uneq_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if TIM detected
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ter_ctrllo_timmsk_Bit_Start                                                                     3
#define cAf6_ter_ctrllo_timmsk_Bit_End                                                                       3
#define cAf6_ter_ctrllo_timmsk_Mask                                                                      cBit3
#define cAf6_ter_ctrllo_timmsk_Shift                                                                         3
#define cAf6_ter_ctrllo_timmsk_MaxVal                                                                      0x1
#define cAf6_ter_ctrllo_timmsk_MinVal                                                                      0x0
#define cAf6_ter_ctrllo_timmsk_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: aislopmsk
BitField Type: RW
BitField Desc: 0 : Enable, 1: Disable send ERDI if AIS,LOP detected
BitField Bits: [2]
--------------------------------------*/
#define cAf6_ter_ctrllo_aislopmsk_Bit_Start                                                                  2
#define cAf6_ter_ctrllo_aislopmsk_Bit_End                                                                    2
#define cAf6_ter_ctrllo_aislopmsk_Mask                                                                   cBit2
#define cAf6_ter_ctrllo_aislopmsk_Shift                                                                      2
#define cAf6_ter_ctrllo_aislopmsk_MaxVal                                                                   0x1
#define cAf6_ter_ctrllo_aislopmsk_MinVal                                                                   0x0
#define cAf6_ter_ctrllo_aislopmsk_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: jnfrmd
BitField Type: RW
BitField Desc: 0:1 byte, 1: 16/64byte
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_ter_ctrllo_jnfrmd_Bit_Start                                                                     0
#define cAf6_ter_ctrllo_jnfrmd_Bit_End                                                                       1
#define cAf6_ter_ctrllo_jnfrmd_Mask                                                                    cBit1_0
#define cAf6_ter_ctrllo_jnfrmd_Shift                                                                         0
#define cAf6_ter_ctrllo_jnfrmd_MaxVal                                                                      0x3
#define cAf6_ter_ctrllo_jnfrmd_MinVal                                                                      0x0
#define cAf6_ter_ctrllo_jnfrmd_RstVal                                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Buffer STS
Reg Addr   : 0x01_0800
Reg Formula: 0x01_0800 + $OCID*256 + $STS*4 + $BGRP
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3)  : Line ID
           + $BGRP(0-3)
Reg Desc   : 
This register is used for storing POH BYTEs inserted to Sonet/SDH. %%
BGRP = 0 : G1,J1  %%
BGRP = 1 : N1,C2  %%
BGRP = 2 : H4,F2  %%
BGRP = 3 : K3,F3

------------------------------------------------------------------------------*/
#define cAf6Reg_rtlpohccterbufhi_Base                                                                 0x010800
#define cAf6Reg_rtlpohccterbufhi(STS, OCID, BGRP)                         (0x010800+(OCID)*256+(STS)*4+(BGRP))
#define cAf6Reg_rtlpohccterbufhi_WidthVal                                                                   32
#define cAf6Reg_rtlpohccterbufhi_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: byte1msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0)write to buffer
BitField Bits: [17]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte1msk_Bit_Start                                                            17
#define cAf6_rtlpohccterbufhi_byte1msk_Bit_End                                                              17
#define cAf6_rtlpohccterbufhi_byte1msk_Mask                                                             cBit17
#define cAf6_rtlpohccterbufhi_byte1msk_Shift                                                                17
#define cAf6_rtlpohccterbufhi_byte1msk_MaxVal                                                              0x1
#define cAf6_rtlpohccterbufhi_byte1msk_MinVal                                                              0x0
#define cAf6_rtlpohccterbufhi_byte1msk_RstVal                                                              0x0

/*--------------------------------------
BitField Name: byte1
BitField Type: RW
BitField Desc: Byte1 (G1/N1/H4/K3)
BitField Bits: [16:9]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte1_Bit_Start                                                                9
#define cAf6_rtlpohccterbufhi_byte1_Bit_End                                                                 16
#define cAf6_rtlpohccterbufhi_byte1_Mask                                                              cBit16_9
#define cAf6_rtlpohccterbufhi_byte1_Shift                                                                    9
#define cAf6_rtlpohccterbufhi_byte1_MaxVal                                                                0xff
#define cAf6_rtlpohccterbufhi_byte1_MinVal                                                                 0x0
#define cAf6_rtlpohccterbufhi_byte1_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: byte0msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0) write to buffer
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte0msk_Bit_Start                                                             8
#define cAf6_rtlpohccterbufhi_byte0msk_Bit_End                                                               8
#define cAf6_rtlpohccterbufhi_byte0msk_Mask                                                              cBit8
#define cAf6_rtlpohccterbufhi_byte0msk_Shift                                                                 8
#define cAf6_rtlpohccterbufhi_byte0msk_MaxVal                                                              0x1
#define cAf6_rtlpohccterbufhi_byte0msk_MinVal                                                              0x0
#define cAf6_rtlpohccterbufhi_byte0msk_RstVal                                                              0x0

/*--------------------------------------
BitField Name: byte0
BitField Type: RW
BitField Desc: Byte0 (J1/C2/F2/F3)
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_rtlpohccterbufhi_byte0_Bit_Start                                                                0
#define cAf6_rtlpohccterbufhi_byte0_Bit_End                                                                  7
#define cAf6_rtlpohccterbufhi_byte0_Mask                                                               cBit7_0
#define cAf6_rtlpohccterbufhi_byte0_Shift                                                                    0
#define cAf6_rtlpohccterbufhi_byte0_MaxVal                                                                0xff
#define cAf6_rtlpohccterbufhi_byte0_MinVal                                                                 0x0
#define cAf6_rtlpohccterbufhi_byte0_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Termintate Insert Buffer TU3/VT
Reg Addr   : 0x01_8000
Reg Formula: 0x01_8000 + $OCID*4096 + $STS*64 + $VT*2 + $BGRP
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3)  : Line ID
           + $VT(0-27)
           + $BGRP(0-1)
Reg Desc   : 
This register is used for storing POH BYTEs inserted to Sonet/SDH. TU3 is at VT ID = 0,1 %%
For VT %%
BGRP = 0 : J2,V5 %%
BGRP = 1 : K4,N2 %%
For TU3 %%
VT = 0, BGRP = 0 : G1,J1 %%
VT = 0, BGRP = 1 : N1,C2 %%
VT = 1, BGRP = 0 : H4,F2 %%
VT = 1, BGRP = 1 : K3,F3

------------------------------------------------------------------------------*/
#define cAf6Reg_rtlpohccterbuflo_Base                                                                 0x018000
#define cAf6Reg_rtlpohccterbuflo(STS, OCID, VT, BGRP)                 (0x018000+(OCID)*4096+(STS)*64+(VT)*2+(BGRP))
#define cAf6Reg_rtlpohccterbuflo_WidthVal                                                                   32
#define cAf6Reg_rtlpohccterbuflo_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: byte1msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0)write to buffer
BitField Bits: [17]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte1msk_Bit_Start                                                            17
#define cAf6_rtlpohccterbuflo_byte1msk_Bit_End                                                              17
#define cAf6_rtlpohccterbuflo_byte1msk_Mask                                                             cBit17
#define cAf6_rtlpohccterbuflo_byte1msk_Shift                                                                17
#define cAf6_rtlpohccterbuflo_byte1msk_MaxVal                                                              0x1
#define cAf6_rtlpohccterbuflo_byte1msk_MinVal                                                              0x0
#define cAf6_rtlpohccterbuflo_byte1msk_RstVal                                                              0x0

/*--------------------------------------
BitField Name: byte1
BitField Type: RW
BitField Desc: Byte1 (J2/K4)
BitField Bits: [16:9]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte1_Bit_Start                                                                9
#define cAf6_rtlpohccterbuflo_byte1_Bit_End                                                                 16
#define cAf6_rtlpohccterbuflo_byte1_Mask                                                              cBit16_9
#define cAf6_rtlpohccterbuflo_byte1_Shift                                                                    9
#define cAf6_rtlpohccterbuflo_byte1_MaxVal                                                                0xff
#define cAf6_rtlpohccterbuflo_byte1_MinVal                                                                 0x0
#define cAf6_rtlpohccterbuflo_byte1_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: byte0msk
BitField Type: WO
BitField Desc: Enable/Disable (1/0) write to buffer
BitField Bits: [8]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte0msk_Bit_Start                                                             8
#define cAf6_rtlpohccterbuflo_byte0msk_Bit_End                                                               8
#define cAf6_rtlpohccterbuflo_byte0msk_Mask                                                              cBit8
#define cAf6_rtlpohccterbuflo_byte0msk_Shift                                                                 8
#define cAf6_rtlpohccterbuflo_byte0msk_MaxVal                                                              0x1
#define cAf6_rtlpohccterbuflo_byte0msk_MinVal                                                              0x0
#define cAf6_rtlpohccterbuflo_byte0msk_RstVal                                                              0x0

/*--------------------------------------
BitField Name: byte0
BitField Type: RW
BitField Desc: Byte0 (V5/N2)
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_rtlpohccterbuflo_byte0_Bit_Start                                                                0
#define cAf6_rtlpohccterbuflo_byte0_Bit_End                                                                  7
#define cAf6_rtlpohccterbuflo_byte0_Mask                                                               cBit7_0
#define cAf6_rtlpohccterbuflo_byte0_Shift                                                                    0
#define cAf6_rtlpohccterbuflo_byte0_MaxVal                                                                0xff
#define cAf6_rtlpohccterbuflo_byte0_MinVal                                                                 0x0
#define cAf6_rtlpohccterbuflo_byte0_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Global Control
Reg Addr   : 0x06_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to enable STS,VT,DSN globally.

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbenb_Base                                                                      0x060000
#define cAf6Reg_pcfg_glbenb                                                                           0x060000
#define cAf6Reg_pcfg_glbenb_WidthVal                                                                        32
#define cAf6Reg_pcfg_glbenb_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: timerenb
BitField Type: RW
BitField Desc: Enable timer
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pcfg_glbenb_timerenb_Bit_Start                                                                  3
#define cAf6_pcfg_glbenb_timerenb_Bit_End                                                                    3
#define cAf6_pcfg_glbenb_timerenb_Mask                                                                   cBit3
#define cAf6_pcfg_glbenb_timerenb_Shift                                                                      3
#define cAf6_pcfg_glbenb_timerenb_MaxVal                                                                   0x1
#define cAf6_pcfg_glbenb_timerenb_MinVal                                                                   0x0
#define cAf6_pcfg_glbenb_timerenb_RstVal                                                                   0x1

/*--------------------------------------
BitField Name: stsenb
BitField Type: RW
BitField Desc: Enable STS/TU3 channel
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pcfg_glbenb_stsenb_Bit_Start                                                                    2
#define cAf6_pcfg_glbenb_stsenb_Bit_End                                                                      2
#define cAf6_pcfg_glbenb_stsenb_Mask                                                                     cBit2
#define cAf6_pcfg_glbenb_stsenb_Shift                                                                        2
#define cAf6_pcfg_glbenb_stsenb_MaxVal                                                                     0x1
#define cAf6_pcfg_glbenb_stsenb_MinVal                                                                     0x0
#define cAf6_pcfg_glbenb_stsenb_RstVal                                                                     0x1

/*--------------------------------------
BitField Name: vtenb
BitField Type: RW
BitField Desc: Enable STS/TU3 channel
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pcfg_glbenb_vtenb_Bit_Start                                                                     1
#define cAf6_pcfg_glbenb_vtenb_Bit_End                                                                       1
#define cAf6_pcfg_glbenb_vtenb_Mask                                                                      cBit1
#define cAf6_pcfg_glbenb_vtenb_Shift                                                                         1
#define cAf6_pcfg_glbenb_vtenb_MaxVal                                                                      0x1
#define cAf6_pcfg_glbenb_vtenb_MinVal                                                                      0x0
#define cAf6_pcfg_glbenb_vtenb_RstVal                                                                      0x1

/*--------------------------------------
BitField Name: dsnsenb
BitField Type: RW
BitField Desc: Enable STS/TU3 channel
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pcfg_glbenb_dsnsenb_Bit_Start                                                                   0
#define cAf6_pcfg_glbenb_dsnsenb_Bit_End                                                                     0
#define cAf6_pcfg_glbenb_dsnsenb_Mask                                                                    cBit0
#define cAf6_pcfg_glbenb_dsnsenb_Shift                                                                       0
#define cAf6_pcfg_glbenb_dsnsenb_MaxVal                                                                    0x1
#define cAf6_pcfg_glbenb_dsnsenb_MinVal                                                                    0x0
#define cAf6_pcfg_glbenb_dsnsenb_RstVal                                                                    0x1


/*------------------------------------------------------------------------------
Reg Name   : POH BER Error Sticky
Reg Addr   : 0x06_0001
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to check error in BER engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_stkalarm_Base                                                                         0x060001
#define cAf6Reg_stkalarm                                                                              0x060001
#define cAf6Reg_stkalarm_WidthVal                                                                           32
#define cAf6Reg_stkalarm_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: stserr
BitField Type: W1C
BitField Desc: STS error
BitField Bits: [1]
--------------------------------------*/
#define cAf6_stkalarm_stserr_Bit_Start                                                                       1
#define cAf6_stkalarm_stserr_Bit_End                                                                         1
#define cAf6_stkalarm_stserr_Mask                                                                        cBit1
#define cAf6_stkalarm_stserr_Shift                                                                           1
#define cAf6_stkalarm_stserr_MaxVal                                                                        0x1
#define cAf6_stkalarm_stserr_MinVal                                                                        0x0
#define cAf6_stkalarm_stserr_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: vterr
BitField Type: W1C
BitField Desc: VT error
BitField Bits: [0]
--------------------------------------*/
#define cAf6_stkalarm_vterr_Bit_Start                                                                        0
#define cAf6_stkalarm_vterr_Bit_End                                                                          0
#define cAf6_stkalarm_vterr_Mask                                                                         cBit0
#define cAf6_stkalarm_vterr_Shift                                                                            0
#define cAf6_stkalarm_vterr_MaxVal                                                                         0x1
#define cAf6_stkalarm_vterr_MinVal                                                                         0x0
#define cAf6_stkalarm_vterr_RstVal                                                                         0x0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Threshold 1
Reg Addr   : 0x06_2047
Reg Formula: 0x06_2047 + $Rate[2:0]*8  + $Rate[6:3]*128
    Where  : 
           + $Rate(0-127): STS Rate for rate from STS1,STS3,STS6...STS48,(0-16)...VT1.5,VT2,DS1,E1(65,67,69,71)....
Reg Desc   : 
This register is used to configure threshold of BER level 3.

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwptrsh1_Base                                                                     0x062047
#define cAf6Reg_imemrwptrsh1(Rate)                                    (0x062047+(Rate)[2:0]*8+(Rate)[6:3]*128)
#define cAf6Reg_imemrwptrsh1_WidthVal                                                                       32
#define cAf6Reg_imemrwptrsh1_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: setthres
BitField Type: RW
BitField Desc: SetThreshold
BitField Bits: [18:9]
--------------------------------------*/
#define cAf6_imemrwptrsh1_setthres_Bit_Start                                                                 9
#define cAf6_imemrwptrsh1_setthres_Bit_End                                                                  18
#define cAf6_imemrwptrsh1_setthres_Mask                                                               cBit18_9
#define cAf6_imemrwptrsh1_setthres_Shift                                                                     9
#define cAf6_imemrwptrsh1_setthres_MaxVal                                                                0x3ff
#define cAf6_imemrwptrsh1_setthres_MinVal                                                                  0x0
#define cAf6_imemrwptrsh1_setthres_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: winthres
BitField Type: RW
BitField Desc: WindowThreshold
BitField Bits: [8:0]
--------------------------------------*/
#define cAf6_imemrwptrsh1_winthres_Bit_Start                                                                 0
#define cAf6_imemrwptrsh1_winthres_Bit_End                                                                   8
#define cAf6_imemrwptrsh1_winthres_Mask                                                                cBit8_0
#define cAf6_imemrwptrsh1_winthres_Shift                                                                     0
#define cAf6_imemrwptrsh1_winthres_MaxVal                                                                0x1ff
#define cAf6_imemrwptrsh1_winthres_MinVal                                                                  0x0
#define cAf6_imemrwptrsh1_winthres_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Threshold 2
Reg Addr   : 0x06_0400
Reg Formula: 0x06_0400 + $Rate*8 + $Thresloc
    Where  : 
           + $Rate(0-63): STS Rate for rate from STS1,STS3,STS6...STS48,....VT1.5,VT2,DS1,E1....
           + $Thresloc(0-7): Set/Clear/Window threshold for BER level from 4 to 8
Reg Desc   : 
This register is used to configure threshold of BER level 4 to level 8.

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwptrsh2_Base                                                                     0x060400
#define cAf6Reg_imemrwptrsh2(Rate, Thresloc)                                    (0x060400+(Rate)*8+(Thresloc))
#define cAf6Reg_imemrwptrsh2_WidthVal                                                                       64
#define cAf6Reg_imemrwptrsh2_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: scwthres1
BitField Type: RW
BitField Desc: Set/Clear/Window Threshold
BitField Bits: [33:17]
--------------------------------------*/
#define cAf6_imemrwptrsh2_scwthres1_Bit_Start                                                               17
#define cAf6_imemrwptrsh2_scwthres1_Bit_End                                                                 33
#define cAf6_imemrwptrsh2_scwthres1_Mask_01                                                          cBit31_17
#define cAf6_imemrwptrsh2_scwthres1_Shift_01                                                                17
#define cAf6_imemrwptrsh2_scwthres1_Mask_02                                                            cBit1_0
#define cAf6_imemrwptrsh2_scwthres1_Shift_02                                                                 0

/*--------------------------------------
BitField Name: scwthres2
BitField Type: RW
BitField Desc: Set/Clear/Window Threshold
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_imemrwptrsh2_scwthres2_Bit_Start                                                                0
#define cAf6_imemrwptrsh2_scwthres2_Bit_End                                                                 16
#define cAf6_imemrwptrsh2_scwthres2_Mask                                                              cBit16_0
#define cAf6_imemrwptrsh2_scwthres2_Shift                                                                    0
#define cAf6_imemrwptrsh2_scwthres2_MaxVal                                                             0x1ffff
#define cAf6_imemrwptrsh2_scwthres2_MinVal                                                                 0x0
#define cAf6_imemrwptrsh2_scwthres2_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Control VT/DSN
Reg Addr   : 0x06_2000
Reg Formula: 0x06_2000 + $STS*128 + $OCID*8+ $VTG
    Where  : 
           + $STS(0-47): STS
           + $OCID(0-11)  : Line ID, 0-5: VT, 6-11: DE1
           + $VTG(0-6)   : VT group
Reg Desc   : 
This register is used to enable and set threshold SD SF .

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwpctrl1_Base                                                                     0x062000
#define cAf6Reg_imemrwpctrl1(STS, OCID, VTG)                               (0x062000+(STS)*128+(OCID)*8+(VTG))
#define cAf6Reg_imemrwpctrl1_WidthVal                                                                       64
#define cAf6Reg_imemrwpctrl1_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: tcatrsh4
BitField Type: RW
BitField Desc: TCA threshold raise channel 4
BitField Bits: [45:43]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh4_Bit_Start                                                                43
#define cAf6_imemrwpctrl1_tcatrsh4_Bit_End                                                                  45
#define cAf6_imemrwpctrl1_tcatrsh4_Mask                                                              cBit13_11
#define cAf6_imemrwpctrl1_tcatrsh4_Shift                                                                    11
#define cAf6_imemrwpctrl1_tcatrsh4_MaxVal                                                                  0x0
#define cAf6_imemrwpctrl1_tcatrsh4_MinVal                                                                  0x0
#define cAf6_imemrwpctrl1_tcatrsh4_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: tcatrsh3
BitField Type: RW
BitField Desc: TCA threshold raise channel 3
BitField Bits: [42:40]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh3_Bit_Start                                                                40
#define cAf6_imemrwpctrl1_tcatrsh3_Bit_End                                                                  42
#define cAf6_imemrwpctrl1_tcatrsh3_Mask                                                               cBit10_8
#define cAf6_imemrwpctrl1_tcatrsh3_Shift                                                                     8
#define cAf6_imemrwpctrl1_tcatrsh3_MaxVal                                                                  0x0
#define cAf6_imemrwpctrl1_tcatrsh3_MinVal                                                                  0x0
#define cAf6_imemrwpctrl1_tcatrsh3_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: tcatrsh2
BitField Type: RW
BitField Desc: TCA threshold raise channel 2
BitField Bits: [39:37]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh2_Bit_Start                                                                37
#define cAf6_imemrwpctrl1_tcatrsh2_Bit_End                                                                  39
#define cAf6_imemrwpctrl1_tcatrsh2_Mask                                                                cBit7_5
#define cAf6_imemrwpctrl1_tcatrsh2_Shift                                                                     5
#define cAf6_imemrwpctrl1_tcatrsh2_MaxVal                                                                  0x0
#define cAf6_imemrwpctrl1_tcatrsh2_MinVal                                                                  0x0
#define cAf6_imemrwpctrl1_tcatrsh2_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: tcatrsh1
BitField Type: RW
BitField Desc: TCA threshold raise channel 1
BitField Bits: [36:34]
--------------------------------------*/
#define cAf6_imemrwpctrl1_tcatrsh1_Bit_Start                                                                34
#define cAf6_imemrwpctrl1_tcatrsh1_Bit_End                                                                  36
#define cAf6_imemrwpctrl1_tcatrsh1_Mask                                                                cBit4_2
#define cAf6_imemrwpctrl1_tcatrsh1_Shift                                                                     2
#define cAf6_imemrwpctrl1_tcatrsh1_MaxVal                                                                  0x0
#define cAf6_imemrwpctrl1_tcatrsh1_MinVal                                                                  0x0
#define cAf6_imemrwpctrl1_tcatrsh1_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: etype4
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 3
BitField Bits: [31]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype4_Bit_Start                                                                  31
#define cAf6_imemrwpctrl1_etype4_Bit_End                                                                    31
#define cAf6_imemrwpctrl1_etype4_Mask                                                                   cBit31
#define cAf6_imemrwpctrl1_etype4_Shift                                                                      31
#define cAf6_imemrwpctrl1_etype4_MaxVal                                                                    0x1
#define cAf6_imemrwpctrl1_etype4_MinVal                                                                    0x0
#define cAf6_imemrwpctrl1_etype4_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sftrsh4
BitField Type: RW
BitField Desc: SF threshold raise channel 3
BitField Bits: [30:28]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh4_Bit_Start                                                                 28
#define cAf6_imemrwpctrl1_sftrsh4_Bit_End                                                                   30
#define cAf6_imemrwpctrl1_sftrsh4_Mask                                                               cBit30_28
#define cAf6_imemrwpctrl1_sftrsh4_Shift                                                                     28
#define cAf6_imemrwpctrl1_sftrsh4_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sftrsh4_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sftrsh4_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: sdtrsh4
BitField Type: RW
BitField Desc: SD threshold raise channel 3
BitField Bits: [27:25]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh4_Bit_Start                                                                 25
#define cAf6_imemrwpctrl1_sdtrsh4_Bit_End                                                                   27
#define cAf6_imemrwpctrl1_sdtrsh4_Mask                                                               cBit27_25
#define cAf6_imemrwpctrl1_sdtrsh4_Shift                                                                     25
#define cAf6_imemrwpctrl1_sdtrsh4_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sdtrsh4_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sdtrsh4_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ena4
BitField Type: RW
BitField Desc: Enable channel 3
BitField Bits: [24]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena4_Bit_Start                                                                    24
#define cAf6_imemrwpctrl1_ena4_Bit_End                                                                      24
#define cAf6_imemrwpctrl1_ena4_Mask                                                                     cBit24
#define cAf6_imemrwpctrl1_ena4_Shift                                                                        24
#define cAf6_imemrwpctrl1_ena4_MaxVal                                                                      0x1
#define cAf6_imemrwpctrl1_ena4_MinVal                                                                      0x0
#define cAf6_imemrwpctrl1_ena4_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: etype3
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 2
BitField Bits: [23]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype3_Bit_Start                                                                  23
#define cAf6_imemrwpctrl1_etype3_Bit_End                                                                    23
#define cAf6_imemrwpctrl1_etype3_Mask                                                                   cBit23
#define cAf6_imemrwpctrl1_etype3_Shift                                                                      23
#define cAf6_imemrwpctrl1_etype3_MaxVal                                                                    0x1
#define cAf6_imemrwpctrl1_etype3_MinVal                                                                    0x0
#define cAf6_imemrwpctrl1_etype3_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sftrsh3
BitField Type: RW
BitField Desc: SF threshold raise channel 2
BitField Bits: [22:20]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh3_Bit_Start                                                                 20
#define cAf6_imemrwpctrl1_sftrsh3_Bit_End                                                                   22
#define cAf6_imemrwpctrl1_sftrsh3_Mask                                                               cBit22_20
#define cAf6_imemrwpctrl1_sftrsh3_Shift                                                                     20
#define cAf6_imemrwpctrl1_sftrsh3_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sftrsh3_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sftrsh3_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: sdtrsh3
BitField Type: RW
BitField Desc: SD threshold raise channel 2
BitField Bits: [19:17]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh3_Bit_Start                                                                 17
#define cAf6_imemrwpctrl1_sdtrsh3_Bit_End                                                                   19
#define cAf6_imemrwpctrl1_sdtrsh3_Mask                                                               cBit19_17
#define cAf6_imemrwpctrl1_sdtrsh3_Shift                                                                     17
#define cAf6_imemrwpctrl1_sdtrsh3_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sdtrsh3_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sdtrsh3_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ena3
BitField Type: RW
BitField Desc: Enable channel 2
BitField Bits: [16]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena3_Bit_Start                                                                    16
#define cAf6_imemrwpctrl1_ena3_Bit_End                                                                      16
#define cAf6_imemrwpctrl1_ena3_Mask                                                                     cBit16
#define cAf6_imemrwpctrl1_ena3_Shift                                                                        16
#define cAf6_imemrwpctrl1_ena3_MaxVal                                                                      0x1
#define cAf6_imemrwpctrl1_ena3_MinVal                                                                      0x0
#define cAf6_imemrwpctrl1_ena3_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: etype2
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 1
BitField Bits: [15]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype2_Bit_Start                                                                  15
#define cAf6_imemrwpctrl1_etype2_Bit_End                                                                    15
#define cAf6_imemrwpctrl1_etype2_Mask                                                                   cBit15
#define cAf6_imemrwpctrl1_etype2_Shift                                                                      15
#define cAf6_imemrwpctrl1_etype2_MaxVal                                                                    0x1
#define cAf6_imemrwpctrl1_etype2_MinVal                                                                    0x0
#define cAf6_imemrwpctrl1_etype2_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sftrsh2
BitField Type: RW
BitField Desc: SF threshold raise channel 1
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh2_Bit_Start                                                                 12
#define cAf6_imemrwpctrl1_sftrsh2_Bit_End                                                                   14
#define cAf6_imemrwpctrl1_sftrsh2_Mask                                                               cBit14_12
#define cAf6_imemrwpctrl1_sftrsh2_Shift                                                                     12
#define cAf6_imemrwpctrl1_sftrsh2_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sftrsh2_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sftrsh2_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: sdtrsh2
BitField Type: RW
BitField Desc: SD threshold raise channel 1
BitField Bits: [11:9]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh2_Bit_Start                                                                  9
#define cAf6_imemrwpctrl1_sdtrsh2_Bit_End                                                                   11
#define cAf6_imemrwpctrl1_sdtrsh2_Mask                                                                cBit11_9
#define cAf6_imemrwpctrl1_sdtrsh2_Shift                                                                      9
#define cAf6_imemrwpctrl1_sdtrsh2_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sdtrsh2_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sdtrsh2_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ena2
BitField Type: RW
BitField Desc: Enable channel 1
BitField Bits: [8]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena2_Bit_Start                                                                     8
#define cAf6_imemrwpctrl1_ena2_Bit_End                                                                       8
#define cAf6_imemrwpctrl1_ena2_Mask                                                                      cBit8
#define cAf6_imemrwpctrl1_ena2_Shift                                                                         8
#define cAf6_imemrwpctrl1_ena2_MaxVal                                                                      0x1
#define cAf6_imemrwpctrl1_ena2_MinVal                                                                      0x0
#define cAf6_imemrwpctrl1_ena2_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: etype1
BitField Type: RW
BitField Desc: 0: DS1/VT1.5 1: E1/VT2 channel 0
BitField Bits: [7]
--------------------------------------*/
#define cAf6_imemrwpctrl1_etype1_Bit_Start                                                                   7
#define cAf6_imemrwpctrl1_etype1_Bit_End                                                                     7
#define cAf6_imemrwpctrl1_etype1_Mask                                                                    cBit7
#define cAf6_imemrwpctrl1_etype1_Shift                                                                       7
#define cAf6_imemrwpctrl1_etype1_MaxVal                                                                    0x1
#define cAf6_imemrwpctrl1_etype1_MinVal                                                                    0x0
#define cAf6_imemrwpctrl1_etype1_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: sftrsh1
BitField Type: RW
BitField Desc: SF threshold raise channel 0
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sftrsh1_Bit_Start                                                                  4
#define cAf6_imemrwpctrl1_sftrsh1_Bit_End                                                                    6
#define cAf6_imemrwpctrl1_sftrsh1_Mask                                                                 cBit6_4
#define cAf6_imemrwpctrl1_sftrsh1_Shift                                                                      4
#define cAf6_imemrwpctrl1_sftrsh1_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sftrsh1_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sftrsh1_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: sdtrsh1
BitField Type: RW
BitField Desc: SD threshold raise channel 0
BitField Bits: [3:1]
--------------------------------------*/
#define cAf6_imemrwpctrl1_sdtrsh1_Bit_Start                                                                  1
#define cAf6_imemrwpctrl1_sdtrsh1_Bit_End                                                                    3
#define cAf6_imemrwpctrl1_sdtrsh1_Mask                                                                 cBit3_1
#define cAf6_imemrwpctrl1_sdtrsh1_Shift                                                                      1
#define cAf6_imemrwpctrl1_sdtrsh1_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl1_sdtrsh1_MinVal                                                                   0x0
#define cAf6_imemrwpctrl1_sdtrsh1_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ena1
BitField Type: RW
BitField Desc: Enable channel 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_imemrwpctrl1_ena1_Bit_Start                                                                     0
#define cAf6_imemrwpctrl1_ena1_Bit_End                                                                       0
#define cAf6_imemrwpctrl1_ena1_Mask                                                                      cBit0
#define cAf6_imemrwpctrl1_ena1_Shift                                                                         0
#define cAf6_imemrwpctrl1_ena1_MaxVal                                                                      0x1
#define cAf6_imemrwpctrl1_ena1_MinVal                                                                      0x0
#define cAf6_imemrwpctrl1_ena1_RstVal                                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Control STS/TU3
Reg Addr   : 0x06_2007
Reg Formula: 0x06_2007 + $STS*128 + $OCID*8
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-7)  : Line ID, Line 4-7 channel 2 for DE3
Reg Desc   : 
This register is used to enable and set threshold SD SF .

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwpctrl2_Base                                                                     0x062007
#define cAf6Reg_imemrwpctrl2(STS, OCID)                                          (0x062007+(STS)*128+(OCID)*8)
#define cAf6Reg_imemrwpctrl2_WidthVal                                                                       64
#define cAf6Reg_imemrwpctrl2_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: tcatrsh2
BitField Type: RW
BitField Desc: TCA threshold raise channel 2
BitField Bits: [39:37]
--------------------------------------*/
#define cAf6_imemrwpctrl2_tcatrsh2_Bit_Start                                                                37
#define cAf6_imemrwpctrl2_tcatrsh2_Bit_End                                                                  39
#define cAf6_imemrwpctrl2_tcatrsh2_Mask                                                                cBit7_5
#define cAf6_imemrwpctrl2_tcatrsh2_Shift                                                                     5
#define cAf6_imemrwpctrl2_tcatrsh2_MaxVal                                                                  0x0
#define cAf6_imemrwpctrl2_tcatrsh2_MinVal                                                                  0x0
#define cAf6_imemrwpctrl2_tcatrsh2_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: tcatrsh1
BitField Type: RW
BitField Desc: TCA threshold raise channel 1
BitField Bits: [36:34]
--------------------------------------*/
#define cAf6_imemrwpctrl2_tcatrsh1_Bit_Start                                                                34
#define cAf6_imemrwpctrl2_tcatrsh1_Bit_End                                                                  36
#define cAf6_imemrwpctrl2_tcatrsh1_Mask                                                                cBit4_2
#define cAf6_imemrwpctrl2_tcatrsh1_Shift                                                                     2
#define cAf6_imemrwpctrl2_tcatrsh1_MaxVal                                                                  0x0
#define cAf6_imemrwpctrl2_tcatrsh1_MinVal                                                                  0x0
#define cAf6_imemrwpctrl2_tcatrsh1_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: rate2
BitField Type: RW
BitField Desc: STS Rate 0-63 type
BitField Bits: [27:21]
--------------------------------------*/
#define cAf6_imemrwpctrl2_rate2_Bit_Start                                                                   21
#define cAf6_imemrwpctrl2_rate2_Bit_End                                                                     27
#define cAf6_imemrwpctrl2_rate2_Mask                                                                 cBit27_21
#define cAf6_imemrwpctrl2_rate2_Shift                                                                       21
#define cAf6_imemrwpctrl2_rate2_MaxVal                                                                    0x7f
#define cAf6_imemrwpctrl2_rate2_MinVal                                                                     0x0
#define cAf6_imemrwpctrl2_rate2_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: sftrsh2
BitField Type: RW
BitField Desc: SF threshold raise channel 1
BitField Bits: [20:18]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sftrsh2_Bit_Start                                                                 18
#define cAf6_imemrwpctrl2_sftrsh2_Bit_End                                                                   20
#define cAf6_imemrwpctrl2_sftrsh2_Mask                                                               cBit20_18
#define cAf6_imemrwpctrl2_sftrsh2_Shift                                                                     18
#define cAf6_imemrwpctrl2_sftrsh2_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl2_sftrsh2_MinVal                                                                   0x0
#define cAf6_imemrwpctrl2_sftrsh2_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: sdtrsh2
BitField Type: RW
BitField Desc: SD threshold raise channel 1
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sdtrsh2_Bit_Start                                                                 15
#define cAf6_imemrwpctrl2_sdtrsh2_Bit_End                                                                   17
#define cAf6_imemrwpctrl2_sdtrsh2_Mask                                                               cBit17_15
#define cAf6_imemrwpctrl2_sdtrsh2_Shift                                                                     15
#define cAf6_imemrwpctrl2_sdtrsh2_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl2_sdtrsh2_MinVal                                                                   0x0
#define cAf6_imemrwpctrl2_sdtrsh2_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ena2
BitField Type: RW
BitField Desc: Enable channel 1
BitField Bits: [14]
--------------------------------------*/
#define cAf6_imemrwpctrl2_ena2_Bit_Start                                                                    14
#define cAf6_imemrwpctrl2_ena2_Bit_End                                                                      14
#define cAf6_imemrwpctrl2_ena2_Mask                                                                     cBit14
#define cAf6_imemrwpctrl2_ena2_Shift                                                                        14
#define cAf6_imemrwpctrl2_ena2_MaxVal                                                                      0x1
#define cAf6_imemrwpctrl2_ena2_MinVal                                                                      0x0
#define cAf6_imemrwpctrl2_ena2_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: rate1
BitField Type: RW
BitField Desc: STS Rate 0-63 type
BitField Bits: [13:7]
--------------------------------------*/
#define cAf6_imemrwpctrl2_rate1_Bit_Start                                                                    7
#define cAf6_imemrwpctrl2_rate1_Bit_End                                                                     13
#define cAf6_imemrwpctrl2_rate1_Mask                                                                  cBit13_7
#define cAf6_imemrwpctrl2_rate1_Shift                                                                        7
#define cAf6_imemrwpctrl2_rate1_MaxVal                                                                    0x7f
#define cAf6_imemrwpctrl2_rate1_MinVal                                                                     0x0
#define cAf6_imemrwpctrl2_rate1_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: sftrsh1
BitField Type: RW
BitField Desc: SF threshold raise channel 0
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sftrsh1_Bit_Start                                                                  4
#define cAf6_imemrwpctrl2_sftrsh1_Bit_End                                                                    6
#define cAf6_imemrwpctrl2_sftrsh1_Mask                                                                 cBit6_4
#define cAf6_imemrwpctrl2_sftrsh1_Shift                                                                      4
#define cAf6_imemrwpctrl2_sftrsh1_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl2_sftrsh1_MinVal                                                                   0x0
#define cAf6_imemrwpctrl2_sftrsh1_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: sdtrsh1
BitField Type: RW
BitField Desc: SD threshold raise channel 0
BitField Bits: [3:1]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sdtrsh1_Bit_Start                                                                  1
#define cAf6_imemrwpctrl2_sdtrsh1_Bit_End                                                                    3
#define cAf6_imemrwpctrl2_sdtrsh1_Mask                                                                 cBit3_1
#define cAf6_imemrwpctrl2_sdtrsh1_Shift                                                                      1
#define cAf6_imemrwpctrl2_sdtrsh1_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl2_sdtrsh1_MinVal                                                                   0x0
#define cAf6_imemrwpctrl2_sdtrsh1_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ena1
BitField Type: RW
BitField Desc: Enable channel 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_imemrwpctrl2_ena1_Bit_Start                                                                     0
#define cAf6_imemrwpctrl2_ena1_Bit_End                                                                       0
#define cAf6_imemrwpctrl2_ena1_Mask                                                                      cBit0
#define cAf6_imemrwpctrl2_ena1_Shift                                                                         0
#define cAf6_imemrwpctrl2_ena1_MaxVal                                                                      0x1
#define cAf6_imemrwpctrl2_ena1_MinVal                                                                      0x0
#define cAf6_imemrwpctrl2_ena1_RstVal                                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Report VT/DSN
Reg Addr   : 0x06_8000
Reg Formula: 0x06_8000 + $STS*336 + $OCID*28 + $VT
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-11)     : Line ID, VT:0-5,DSN:6-11
           + $VT(0-27)   : VT/DS1 ID
Reg Desc   : 
This register is used to get current BER rate .

------------------------------------------------------------------------------*/
#define cAf6Reg_ramberratevtds_Base                                                                   0x068000
#define cAf6Reg_ramberratevtds(STS, OCID, VT)                              (0x068000+(STS)*336+(OCID)*28+(VT))
#define cAf6Reg_ramberratevtds_WidthVal                                                                     32
#define cAf6Reg_ramberratevtds_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: hwsta
BitField Type: RW
BitField Desc: Hardware status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ramberratevtds_hwsta_Bit_Start                                                                  3
#define cAf6_ramberratevtds_hwsta_Bit_End                                                                    3
#define cAf6_ramberratevtds_hwsta_Mask                                                                   cBit3
#define cAf6_ramberratevtds_hwsta_Shift                                                                      3
#define cAf6_ramberratevtds_hwsta_MaxVal                                                                   0x1
#define cAf6_ramberratevtds_hwsta_MinVal                                                                   0x0
#define cAf6_ramberratevtds_hwsta_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: rate
BitField Type: RW
BitField Desc: BER rate
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ramberratevtds_rate_Bit_Start                                                                   0
#define cAf6_ramberratevtds_rate_Bit_End                                                                     2
#define cAf6_ramberratevtds_rate_Mask                                                                  cBit2_0
#define cAf6_ramberratevtds_rate_Shift                                                                       0
#define cAf6_ramberratevtds_rate_MaxVal                                                                    0x7
#define cAf6_ramberratevtds_rate_MinVal                                                                    0x0
#define cAf6_ramberratevtds_rate_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Report STS/TU3
Reg Addr   : 0x06_C000
Reg Formula: 0x06_C000 + $STS*16 + $OCID*2 + $TU3TYPE
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-7)     : Line ID
           + $TU3TYPE(0-1)  : Type TU3:1, STS:0
Reg Desc   : 
This register is used to get current BER rate . BER DE3 used with OCID 4-7, TU3TYPE = 1.

------------------------------------------------------------------------------*/
#define cAf6Reg_ramberrateststu3_Base                                                                 0x06C000
#define cAf6Reg_ramberrateststu3(STS, OCID, TU3TYPE)                    (0x06C000+(STS)*16+(OCID)*2+(TU3TYPE))
#define cAf6Reg_ramberrateststu3_WidthVal                                                                   32
#define cAf6Reg_ramberrateststu3_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: hwsta
BitField Type: RW
BitField Desc: Hardware status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ramberrateststu3_hwsta_Bit_Start                                                                3
#define cAf6_ramberrateststu3_hwsta_Bit_End                                                                  3
#define cAf6_ramberrateststu3_hwsta_Mask                                                                 cBit3
#define cAf6_ramberrateststu3_hwsta_Shift                                                                    3
#define cAf6_ramberrateststu3_hwsta_MaxVal                                                                 0x1
#define cAf6_ramberrateststu3_hwsta_MinVal                                                                 0x0
#define cAf6_ramberrateststu3_hwsta_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: rate
BitField Type: RW
BitField Desc: BER rate
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ramberrateststu3_rate_Bit_Start                                                                 0
#define cAf6_ramberrateststu3_rate_Bit_End                                                                   2
#define cAf6_ramberrateststu3_rate_Mask                                                                cBit2_0
#define cAf6_ramberrateststu3_rate_Shift                                                                     0
#define cAf6_ramberrateststu3_rate_MaxVal                                                                  0x7
#define cAf6_ramberrateststu3_rate_MinVal                                                                  0x0
#define cAf6_ramberrateststu3_rate_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Counter Report STS
Reg Addr   : 0x0C_A000
Reg Formula: 0x0C_A000 + $STS + $OCID*48
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3)     : Line ID
Reg Desc   : 
This register is used to get POH Counter, Rx SDH pointer increase, decrease counter.

------------------------------------------------------------------------------*/
#define cAf6Reg_ipm_cnthi_Base                                                                        0x0CA000
#define cAf6Reg_ipm_cnthi(STS, OCID)                                                (0x0CA000+(STS)+(OCID)*48)
#define cAf6Reg_ipm_cnthi_WidthVal                                                                          32
#define cAf6Reg_ipm_cnthi_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: reicnt
BitField Type: RC
BitField Desc: REI counter
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_ipm_cnthi_reicnt_Bit_Start                                                                     16
#define cAf6_ipm_cnthi_reicnt_Bit_End                                                                       31
#define cAf6_ipm_cnthi_reicnt_Mask                                                                   cBit31_16
#define cAf6_ipm_cnthi_reicnt_Shift                                                                         16
#define cAf6_ipm_cnthi_reicnt_MaxVal                                                                    0xffff
#define cAf6_ipm_cnthi_reicnt_MinVal                                                                       0x0
#define cAf6_ipm_cnthi_reicnt_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: bipcnt
BitField Type: RC
BitField Desc: BIP counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ipm_cnthi_bipcnt_Bit_Start                                                                      0
#define cAf6_ipm_cnthi_bipcnt_Bit_End                                                                       15
#define cAf6_ipm_cnthi_bipcnt_Mask                                                                    cBit15_0
#define cAf6_ipm_cnthi_bipcnt_Shift                                                                          0
#define cAf6_ipm_cnthi_bipcnt_MaxVal                                                                    0xffff
#define cAf6_ipm_cnthi_bipcnt_MinVal                                                                       0x0
#define cAf6_ipm_cnthi_bipcnt_RstVal                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Counter Report TU3/VT
Reg Addr   : 0x0C_8000
Reg Formula: 0x0C_8000 + $STS*28 + $OCID*1344 + $VT
    Where  : 
           + $STS(0-47)  : STS
           + $OCID(0-3)   : Line ID
           + $VT(0-27)   : VT ID
Reg Desc   : 
This register is used to get POH Counter, Rx SDH pointer increase, decrease counter.

------------------------------------------------------------------------------*/
#define cAf6Reg_ipm_cntlo_Base                                                                        0x0C8000
#define cAf6Reg_ipm_cntlo(STS, OCID, VT)                                  (0x0C8000+(STS)*28+(OCID)*1344+(VT))
#define cAf6Reg_ipm_cntlo_WidthVal                                                                          32
#define cAf6Reg_ipm_cntlo_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: reicnt
BitField Type: RC
BitField Desc: REI counter
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_ipm_cntlo_reicnt_Bit_Start                                                                     16
#define cAf6_ipm_cntlo_reicnt_Bit_End                                                                       31
#define cAf6_ipm_cntlo_reicnt_Mask                                                                   cBit31_16
#define cAf6_ipm_cntlo_reicnt_Shift                                                                         16
#define cAf6_ipm_cntlo_reicnt_MaxVal                                                                    0xffff
#define cAf6_ipm_cntlo_reicnt_MinVal                                                                       0x0
#define cAf6_ipm_cntlo_reicnt_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: bipcnt
BitField Type: RC
BitField Desc: BIP counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ipm_cntlo_bipcnt_Bit_Start                                                                      0
#define cAf6_ipm_cntlo_bipcnt_Bit_End                                                                       15
#define cAf6_ipm_cntlo_bipcnt_Mask                                                                    cBit15_0
#define cAf6_ipm_cntlo_bipcnt_Shift                                                                          0
#define cAf6_ipm_cntlo_bipcnt_MaxVal                                                                    0xffff
#define cAf6_ipm_cntlo_bipcnt_MinVal                                                                       0x0
#define cAf6_ipm_cntlo_bipcnt_RstVal                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Mask Report STS
Reg Addr   : 0x0D_0000
Reg Formula: 0x0D_0000 + $STS + $OCID*128
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_mskhi_Base                                                                        0x0D0000
#define cAf6Reg_alm_mskhi(STS, OCID)                                               (0x0D0000+(STS)+(OCID)*128)
#define cAf6Reg_alm_mskhi_WidthVal                                                                          32
#define cAf6Reg_alm_mskhi_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: rdimsk
BitField Type: RW
BitField Desc: rdi mask
BitField Bits: [14]
--------------------------------------*/
#define cAf6_alm_mskhi_rdimsk_Mask                                                                      cBit14
#define cAf6_alm_mskhi_rdimsk_Shift                                                                         14

/*--------------------------------------
BitField Name: jnstachgmsk
BitField Type: RW
BitField Desc: jnstachg mask
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_mskhi_jnstachgmsk_Bit_Start                                                                13
#define cAf6_alm_mskhi_jnstachgmsk_Bit_End                                                                  13
#define cAf6_alm_mskhi_jnstachgmsk_Mask                                                                 cBit13
#define cAf6_alm_mskhi_jnstachgmsk_Shift                                                                    13
#define cAf6_alm_mskhi_jnstachgmsk_MaxVal                                                                  0x1
#define cAf6_alm_mskhi_jnstachgmsk_MinVal                                                                  0x0
#define cAf6_alm_mskhi_jnstachgmsk_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: pslstachgmsk
BitField Type: RW
BitField Desc: pslstachg mask
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_mskhi_pslstachgmsk_Bit_Start                                                               12
#define cAf6_alm_mskhi_pslstachgmsk_Bit_End                                                                 12
#define cAf6_alm_mskhi_pslstachgmsk_Mask                                                                cBit12
#define cAf6_alm_mskhi_pslstachgmsk_Shift                                                                   12
#define cAf6_alm_mskhi_pslstachgmsk_MaxVal                                                                 0x1
#define cAf6_alm_mskhi_pslstachgmsk_MinVal                                                                 0x0
#define cAf6_alm_mskhi_pslstachgmsk_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: bersdmsk
BitField Type: RW
BitField Desc: bersd mask
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_mskhi_bersdmsk_Bit_Start                                                                   11
#define cAf6_alm_mskhi_bersdmsk_Bit_End                                                                     11
#define cAf6_alm_mskhi_bersdmsk_Mask                                                                    cBit11
#define cAf6_alm_mskhi_bersdmsk_Shift                                                                       11
#define cAf6_alm_mskhi_bersdmsk_MaxVal                                                                     0x1
#define cAf6_alm_mskhi_bersdmsk_MinVal                                                                     0x0
#define cAf6_alm_mskhi_bersdmsk_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: bersfmsk
BitField Type: RW
BitField Desc: bersf  mask
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_mskhi_bersfmsk_Bit_Start                                                                   10
#define cAf6_alm_mskhi_bersfmsk_Bit_End                                                                     10
#define cAf6_alm_mskhi_bersfmsk_Mask                                                                    cBit10
#define cAf6_alm_mskhi_bersfmsk_Shift                                                                       10
#define cAf6_alm_mskhi_bersfmsk_MaxVal                                                                     0x1
#define cAf6_alm_mskhi_bersfmsk_MinVal                                                                     0x0
#define cAf6_alm_mskhi_bersfmsk_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: erdimsk
BitField Type: RW
BitField Desc: erdis/rdi mask
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_mskhi_erdimsk_Bit_Start                                                                     9
#define cAf6_alm_mskhi_erdimsk_Bit_End                                                                       9
#define cAf6_alm_mskhi_erdimsk_Mask                                                                      cBit9
#define cAf6_alm_mskhi_erdimsk_Shift                                                                         9
#define cAf6_alm_mskhi_erdimsk_MaxVal                                                                      0x1
#define cAf6_alm_mskhi_erdimsk_MinVal                                                                      0x0
#define cAf6_alm_mskhi_erdimsk_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: bertcamsk
BitField Type: RW
BitField Desc: bertca mask
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_mskhi_bertcamsk_Bit_Start                                                                   8
#define cAf6_alm_mskhi_bertcamsk_Bit_End                                                                     8
#define cAf6_alm_mskhi_bertcamsk_Mask                                                                    cBit8
#define cAf6_alm_mskhi_bertcamsk_Shift                                                                       8
#define cAf6_alm_mskhi_bertcamsk_MaxVal                                                                    0x1
#define cAf6_alm_mskhi_bertcamsk_MinVal                                                                    0x0
#define cAf6_alm_mskhi_bertcamsk_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: erdicmsk
BitField Type: RW
BitField Desc: erdicmsk  mask
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_mskhi_erdicmsk_Bit_Start                                                                    7
#define cAf6_alm_mskhi_erdicmsk_Bit_End                                                                      7
#define cAf6_alm_mskhi_erdicmsk_Mask                                                                     cBit7
#define cAf6_alm_mskhi_erdicmsk_Shift                                                                        7
#define cAf6_alm_mskhi_erdicmsk_MaxVal                                                                     0x1
#define cAf6_alm_mskhi_erdicmsk_MinVal                                                                     0x0
#define cAf6_alm_mskhi_erdicmsk_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: erdipmsk
BitField Type: RW
BitField Desc: erdipmsk  mask
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_mskhi_erdipmsk_Bit_Start                                                                    6
#define cAf6_alm_mskhi_erdipmsk_Bit_End                                                                      6
#define cAf6_alm_mskhi_erdipmsk_Mask                                                                     cBit6
#define cAf6_alm_mskhi_erdipmsk_Shift                                                                        6
#define cAf6_alm_mskhi_erdipmsk_MaxVal                                                                     0x1
#define cAf6_alm_mskhi_erdipmsk_MinVal                                                                     0x0
#define cAf6_alm_mskhi_erdipmsk_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: rfimsk
BitField Type: RW
BitField Desc: rfi/lom mask
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_mskhi_rfimsk_Bit_Start                                                                      5
#define cAf6_alm_mskhi_rfimsk_Bit_End                                                                        5
#define cAf6_alm_mskhi_rfimsk_Mask                                                                       cBit5
#define cAf6_alm_mskhi_rfimsk_Shift                                                                          5
#define cAf6_alm_mskhi_rfimsk_MaxVal                                                                       0x1
#define cAf6_alm_mskhi_rfimsk_MinVal                                                                       0x0
#define cAf6_alm_mskhi_rfimsk_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: tim mask
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_mskhi_timmsk_Bit_Start                                                                      4
#define cAf6_alm_mskhi_timmsk_Bit_End                                                                        4
#define cAf6_alm_mskhi_timmsk_Mask                                                                       cBit4
#define cAf6_alm_mskhi_timmsk_Shift                                                                          4
#define cAf6_alm_mskhi_timmsk_MaxVal                                                                       0x1
#define cAf6_alm_mskhi_timmsk_MinVal                                                                       0x0
#define cAf6_alm_mskhi_timmsk_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: uneqmsk
BitField Type: RW
BitField Desc: uneq mask
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_mskhi_uneqmsk_Bit_Start                                                                     3
#define cAf6_alm_mskhi_uneqmsk_Bit_End                                                                       3
#define cAf6_alm_mskhi_uneqmsk_Mask                                                                      cBit3
#define cAf6_alm_mskhi_uneqmsk_Shift                                                                         3
#define cAf6_alm_mskhi_uneqmsk_MaxVal                                                                      0x1
#define cAf6_alm_mskhi_uneqmsk_MinVal                                                                      0x0
#define cAf6_alm_mskhi_uneqmsk_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: plmmsk
BitField Type: RW
BitField Desc: plm mask
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_mskhi_plmmsk_Bit_Start                                                                      2
#define cAf6_alm_mskhi_plmmsk_Bit_End                                                                        2
#define cAf6_alm_mskhi_plmmsk_Mask                                                                       cBit2
#define cAf6_alm_mskhi_plmmsk_Shift                                                                          2
#define cAf6_alm_mskhi_plmmsk_MaxVal                                                                       0x1
#define cAf6_alm_mskhi_plmmsk_MinVal                                                                       0x0
#define cAf6_alm_mskhi_plmmsk_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: aismsk
BitField Type: RW
BitField Desc: ais mask
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_mskhi_aismsk_Bit_Start                                                                      1
#define cAf6_alm_mskhi_aismsk_Bit_End                                                                        1
#define cAf6_alm_mskhi_aismsk_Mask                                                                       cBit1
#define cAf6_alm_mskhi_aismsk_Shift                                                                          1
#define cAf6_alm_mskhi_aismsk_MaxVal                                                                       0x1
#define cAf6_alm_mskhi_aismsk_MinVal                                                                       0x0
#define cAf6_alm_mskhi_aismsk_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: lopmsk
BitField Type: RW
BitField Desc: lop mask
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_mskhi_lopmsk_Bit_Start                                                                      0
#define cAf6_alm_mskhi_lopmsk_Bit_End                                                                        0
#define cAf6_alm_mskhi_lopmsk_Mask                                                                       cBit0
#define cAf6_alm_mskhi_lopmsk_Shift                                                                          0
#define cAf6_alm_mskhi_lopmsk_MaxVal                                                                       0x1
#define cAf6_alm_mskhi_lopmsk_MinVal                                                                       0x0
#define cAf6_alm_mskhi_lopmsk_RstVal                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Report STS
Reg Addr   : 0x0D_0040
Reg Formula: 0x0D_0040 + $STS + $OCID*128
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_stahi_Base                                                                        0x0D0040
#define cAf6Reg_alm_stahi(STS, OCID)                                               (0x0D0040+(STS)+(OCID)*128)
#define cAf6Reg_alm_stahi_WidthVal                                                                          32
#define cAf6Reg_alm_stahi_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: bersdsta
BitField Type: W1C
BitField Desc: bersd  status
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_stahi_bersdsta_Bit_Start                                                                   11
#define cAf6_alm_stahi_bersdsta_Bit_End                                                                     11
#define cAf6_alm_stahi_bersdsta_Mask                                                                    cBit11
#define cAf6_alm_stahi_bersdsta_Shift                                                                       11
#define cAf6_alm_stahi_bersdsta_MaxVal                                                                     0x1
#define cAf6_alm_stahi_bersdsta_MinVal                                                                     0x0
#define cAf6_alm_stahi_bersdsta_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: bersfsta
BitField Type: W1C
BitField Desc: bersf  status
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_stahi_bersfsta_Bit_Start                                                                   10
#define cAf6_alm_stahi_bersfsta_Bit_End                                                                     10
#define cAf6_alm_stahi_bersfsta_Mask                                                                    cBit10
#define cAf6_alm_stahi_bersfsta_Shift                                                                       10
#define cAf6_alm_stahi_bersfsta_MaxVal                                                                     0x1
#define cAf6_alm_stahi_bersfsta_MinVal                                                                     0x0
#define cAf6_alm_stahi_bersfsta_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: erdista
BitField Type: RO
BitField Desc: erdis/rdi  status
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_stahi_erdista_Bit_Start                                                                     9
#define cAf6_alm_stahi_erdista_Bit_End                                                                       9
#define cAf6_alm_stahi_erdista_Mask                                                                      cBit9
#define cAf6_alm_stahi_erdista_Shift                                                                         9
#define cAf6_alm_stahi_erdista_MaxVal                                                                      0x1
#define cAf6_alm_stahi_erdista_MinVal                                                                      0x0
#define cAf6_alm_stahi_erdista_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: bertcasta
BitField Type: RO
BitField Desc: bertca status
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_stahi_bertcasta_Bit_Start                                                                   8
#define cAf6_alm_stahi_bertcasta_Bit_End                                                                     8
#define cAf6_alm_stahi_bertcasta_Mask                                                                    cBit8
#define cAf6_alm_stahi_bertcasta_Shift                                                                       8
#define cAf6_alm_stahi_bertcasta_MaxVal                                                                    0x1
#define cAf6_alm_stahi_bertcasta_MinVal                                                                    0x0
#define cAf6_alm_stahi_bertcasta_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: erdicsta
BitField Type: RO
BitField Desc: erdic status
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_stahi_erdicsta_Bit_Start                                                                    7
#define cAf6_alm_stahi_erdicsta_Bit_End                                                                      7
#define cAf6_alm_stahi_erdicsta_Mask                                                                     cBit7
#define cAf6_alm_stahi_erdicsta_Shift                                                                        7
#define cAf6_alm_stahi_erdicsta_MaxVal                                                                     0x1
#define cAf6_alm_stahi_erdicsta_MinVal                                                                     0x0
#define cAf6_alm_stahi_erdicsta_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: erdipsta
BitField Type: RO
BitField Desc: erdip stable status
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_stahi_erdipsta_Bit_Start                                                                    6
#define cAf6_alm_stahi_erdipsta_Bit_End                                                                      6
#define cAf6_alm_stahi_erdipsta_Mask                                                                     cBit6
#define cAf6_alm_stahi_erdipsta_Shift                                                                        6
#define cAf6_alm_stahi_erdipsta_MaxVal                                                                     0x1
#define cAf6_alm_stahi_erdipsta_MinVal                                                                     0x0
#define cAf6_alm_stahi_erdipsta_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: rfista
BitField Type: RO
BitField Desc: rfi/lom status
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_stahi_rfista_Bit_Start                                                                      5
#define cAf6_alm_stahi_rfista_Bit_End                                                                        5
#define cAf6_alm_stahi_rfista_Mask                                                                       cBit5
#define cAf6_alm_stahi_rfista_Shift                                                                          5
#define cAf6_alm_stahi_rfista_MaxVal                                                                       0x1
#define cAf6_alm_stahi_rfista_MinVal                                                                       0x0
#define cAf6_alm_stahi_rfista_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: timsta
BitField Type: RO
BitField Desc: tim status
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_stahi_timsta_Bit_Start                                                                      4
#define cAf6_alm_stahi_timsta_Bit_End                                                                        4
#define cAf6_alm_stahi_timsta_Mask                                                                       cBit4
#define cAf6_alm_stahi_timsta_Shift                                                                          4
#define cAf6_alm_stahi_timsta_MaxVal                                                                       0x1
#define cAf6_alm_stahi_timsta_MinVal                                                                       0x0
#define cAf6_alm_stahi_timsta_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: uneqsta
BitField Type: RO
BitField Desc: uneq status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_stahi_uneqsta_Bit_Start                                                                     3
#define cAf6_alm_stahi_uneqsta_Bit_End                                                                       3
#define cAf6_alm_stahi_uneqsta_Mask                                                                      cBit3
#define cAf6_alm_stahi_uneqsta_Shift                                                                         3
#define cAf6_alm_stahi_uneqsta_MaxVal                                                                      0x1
#define cAf6_alm_stahi_uneqsta_MinVal                                                                      0x0
#define cAf6_alm_stahi_uneqsta_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: plmsta
BitField Type: RO
BitField Desc: plm status
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_stahi_plmsta_Bit_Start                                                                      2
#define cAf6_alm_stahi_plmsta_Bit_End                                                                        2
#define cAf6_alm_stahi_plmsta_Mask                                                                       cBit2
#define cAf6_alm_stahi_plmsta_Shift                                                                          2
#define cAf6_alm_stahi_plmsta_MaxVal                                                                       0x1
#define cAf6_alm_stahi_plmsta_MinVal                                                                       0x0
#define cAf6_alm_stahi_plmsta_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: aissta
BitField Type: RO
BitField Desc: ais status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_stahi_aissta_Bit_Start                                                                      1
#define cAf6_alm_stahi_aissta_Bit_End                                                                        1
#define cAf6_alm_stahi_aissta_Mask                                                                       cBit1
#define cAf6_alm_stahi_aissta_Shift                                                                          1
#define cAf6_alm_stahi_aissta_MaxVal                                                                       0x1
#define cAf6_alm_stahi_aissta_MinVal                                                                       0x0
#define cAf6_alm_stahi_aissta_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: lopsta
BitField Type: RO
BitField Desc: lop status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_stahi_lopsta_Bit_Start                                                                      0
#define cAf6_alm_stahi_lopsta_Bit_End                                                                        0
#define cAf6_alm_stahi_lopsta_Mask                                                                       cBit0
#define cAf6_alm_stahi_lopsta_Shift                                                                          0
#define cAf6_alm_stahi_lopsta_MaxVal                                                                       0x1
#define cAf6_alm_stahi_lopsta_MinVal                                                                       0x0
#define cAf6_alm_stahi_lopsta_RstVal                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Status Report STS
Reg Addr   : 0x0D_0020
Reg Formula: 0x0D_0020 + $STS + $OCID*128
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_chghi_Base                                                                        0x0D0020
#define cAf6Reg_alm_chghi(STS, OCID)                                               (0x0D0020+(STS)+(OCID)*128)
#define cAf6Reg_alm_chghi_WidthVal                                                                          32
#define cAf6Reg_alm_chghi_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: jnstachg
BitField Type: RW
BitField Desc: jn message change
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_chghi_jnstachg_Bit_Start                                                                   13
#define cAf6_alm_chghi_jnstachg_Bit_End                                                                     13
#define cAf6_alm_chghi_jnstachg_Mask                                                                    cBit13
#define cAf6_alm_chghi_jnstachg_Shift                                                                       13
#define cAf6_alm_chghi_jnstachg_MaxVal                                                                     0x1
#define cAf6_alm_chghi_jnstachg_MinVal                                                                     0x0
#define cAf6_alm_chghi_jnstachg_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: pslstachg
BitField Type: RW
BitField Desc: psl byte change
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_chghi_pslstachg_Bit_Start                                                                  12
#define cAf6_alm_chghi_pslstachg_Bit_End                                                                    12
#define cAf6_alm_chghi_pslstachg_Mask                                                                   cBit12
#define cAf6_alm_chghi_pslstachg_Shift                                                                      12
#define cAf6_alm_chghi_pslstachg_MaxVal                                                                    0x1
#define cAf6_alm_chghi_pslstachg_MinVal                                                                    0x0
#define cAf6_alm_chghi_pslstachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: bersdstachg
BitField Type: W1C
BitField Desc: bersd stable status change
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_chghi_bersdstachg_Bit_Start                                                                11
#define cAf6_alm_chghi_bersdstachg_Bit_End                                                                  11
#define cAf6_alm_chghi_bersdstachg_Mask                                                                 cBit11
#define cAf6_alm_chghi_bersdstachg_Shift                                                                    11
#define cAf6_alm_chghi_bersdstachg_MaxVal                                                                  0x1
#define cAf6_alm_chghi_bersdstachg_MinVal                                                                  0x0
#define cAf6_alm_chghi_bersdstachg_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: bersfstachg
BitField Type: W1C
BitField Desc: bersf stable status change
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_chghi_bersfstachg_Bit_Start                                                                10
#define cAf6_alm_chghi_bersfstachg_Bit_End                                                                  10
#define cAf6_alm_chghi_bersfstachg_Mask                                                                 cBit10
#define cAf6_alm_chghi_bersfstachg_Shift                                                                    10
#define cAf6_alm_chghi_bersfstachg_MaxVal                                                                  0x1
#define cAf6_alm_chghi_bersfstachg_MinVal                                                                  0x0
#define cAf6_alm_chghi_bersfstachg_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: erdistachg
BitField Type: W1C
BitField Desc: erdis/rdi status change
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_chghi_erdistachg_Bit_Start                                                                  9
#define cAf6_alm_chghi_erdistachg_Bit_End                                                                    9
#define cAf6_alm_chghi_erdistachg_Mask                                                                   cBit9
#define cAf6_alm_chghi_erdistachg_Shift                                                                      9
#define cAf6_alm_chghi_erdistachg_MaxVal                                                                   0x1
#define cAf6_alm_chghi_erdistachg_MinVal                                                                   0x0
#define cAf6_alm_chghi_erdistachg_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: bertcastachg
BitField Type: W1C
BitField Desc: bertca status change
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_chghi_bertcastachg_Bit_Start                                                                8
#define cAf6_alm_chghi_bertcastachg_Bit_End                                                                  8
#define cAf6_alm_chghi_bertcastachg_Mask                                                                 cBit8
#define cAf6_alm_chghi_bertcastachg_Shift                                                                    8
#define cAf6_alm_chghi_bertcastachg_MaxVal                                                                 0x1
#define cAf6_alm_chghi_bertcastachg_MinVal                                                                 0x0
#define cAf6_alm_chghi_bertcastachg_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: erdicstachg
BitField Type: W1C
BitField Desc: erdic status change
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_chghi_erdicstachg_Bit_Start                                                                 7
#define cAf6_alm_chghi_erdicstachg_Bit_End                                                                   7
#define cAf6_alm_chghi_erdicstachg_Mask                                                                  cBit7
#define cAf6_alm_chghi_erdicstachg_Shift                                                                     7
#define cAf6_alm_chghi_erdicstachg_MaxVal                                                                  0x1
#define cAf6_alm_chghi_erdicstachg_MinVal                                                                  0x0
#define cAf6_alm_chghi_erdicstachg_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: erdipstachg
BitField Type: W1C
BitField Desc: erdip status change
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_chghi_erdipstachg_Bit_Start                                                                 6
#define cAf6_alm_chghi_erdipstachg_Bit_End                                                                   6
#define cAf6_alm_chghi_erdipstachg_Mask                                                                  cBit6
#define cAf6_alm_chghi_erdipstachg_Shift                                                                     6
#define cAf6_alm_chghi_erdipstachg_MaxVal                                                                  0x1
#define cAf6_alm_chghi_erdipstachg_MinVal                                                                  0x0
#define cAf6_alm_chghi_erdipstachg_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: rfistachg
BitField Type: W1C
BitField Desc: rfi/lom status change
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_chghi_rfistachg_Bit_Start                                                                   5
#define cAf6_alm_chghi_rfistachg_Bit_End                                                                     5
#define cAf6_alm_chghi_rfistachg_Mask                                                                    cBit5
#define cAf6_alm_chghi_rfistachg_Shift                                                                       5
#define cAf6_alm_chghi_rfistachg_MaxVal                                                                    0x1
#define cAf6_alm_chghi_rfistachg_MinVal                                                                    0x0
#define cAf6_alm_chghi_rfistachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: timstachg
BitField Type: W1C
BitField Desc: tim status change
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_chghi_timstachg_Bit_Start                                                                   4
#define cAf6_alm_chghi_timstachg_Bit_End                                                                     4
#define cAf6_alm_chghi_timstachg_Mask                                                                    cBit4
#define cAf6_alm_chghi_timstachg_Shift                                                                       4
#define cAf6_alm_chghi_timstachg_MaxVal                                                                    0x1
#define cAf6_alm_chghi_timstachg_MinVal                                                                    0x0
#define cAf6_alm_chghi_timstachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: uneqstachg
BitField Type: W1C
BitField Desc: uneq status change
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_chghi_uneqstachg_Bit_Start                                                                  3
#define cAf6_alm_chghi_uneqstachg_Bit_End                                                                    3
#define cAf6_alm_chghi_uneqstachg_Mask                                                                   cBit3
#define cAf6_alm_chghi_uneqstachg_Shift                                                                      3
#define cAf6_alm_chghi_uneqstachg_MaxVal                                                                   0x1
#define cAf6_alm_chghi_uneqstachg_MinVal                                                                   0x0
#define cAf6_alm_chghi_uneqstachg_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: plmstachg
BitField Type: W1C
BitField Desc: plm status change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_chghi_plmstachg_Bit_Start                                                                   2
#define cAf6_alm_chghi_plmstachg_Bit_End                                                                     2
#define cAf6_alm_chghi_plmstachg_Mask                                                                    cBit2
#define cAf6_alm_chghi_plmstachg_Shift                                                                       2
#define cAf6_alm_chghi_plmstachg_MaxVal                                                                    0x1
#define cAf6_alm_chghi_plmstachg_MinVal                                                                    0x0
#define cAf6_alm_chghi_plmstachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: aisstachg
BitField Type: W1C
BitField Desc: ais status change
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_chghi_aisstachg_Bit_Start                                                                   1
#define cAf6_alm_chghi_aisstachg_Bit_End                                                                     1
#define cAf6_alm_chghi_aisstachg_Mask                                                                    cBit1
#define cAf6_alm_chghi_aisstachg_Shift                                                                       1
#define cAf6_alm_chghi_aisstachg_MaxVal                                                                    0x1
#define cAf6_alm_chghi_aisstachg_MinVal                                                                    0x0
#define cAf6_alm_chghi_aisstachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: lopstachg
BitField Type: W1C
BitField Desc: lop status change
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_chghi_lopstachg_Bit_Start                                                                   0
#define cAf6_alm_chghi_lopstachg_Bit_End                                                                     0
#define cAf6_alm_chghi_lopstachg_Mask                                                                    cBit0
#define cAf6_alm_chghi_lopstachg_Shift                                                                       0
#define cAf6_alm_chghi_lopstachg_MaxVal                                                                    0x1
#define cAf6_alm_chghi_lopstachg_MinVal                                                                    0x0
#define cAf6_alm_chghi_lopstachg_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report STS
Reg Addr   : 0x0D_007F
Reg Formula: 0x0D_007F + $OCID*128
    Where  : 
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm global change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchghi_Base                                                                     0x0D007F
#define cAf6Reg_alm_glbchghi(OCID)                                                       (0x0D007F+(OCID)*128)
#define cAf6Reg_alm_glbchghi_WidthVal                                                                       32
#define cAf6Reg_alm_glbchghi_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: glbstachg
BitField Type: RO
BitField Desc: global status change bit
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbchghi_glbstachg_Bit_Start                                                                0
#define cAf6_alm_glbchghi_glbstachg_Bit_End                                                                 23
#define cAf6_alm_glbchghi_glbstachg_Mask                                                              cBit23_0
#define cAf6_alm_glbchghi_glbstachg_Shift                                                                    0
#define cAf6_alm_glbchghi_glbstachg_MaxVal                                                            0xffffff
#define cAf6_alm_glbchghi_glbstachg_MinVal                                                                 0x0
#define cAf6_alm_glbchghi_glbstachg_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report STS
Reg Addr   : 0x0D_007E
Reg Formula: 0x0D_007E + $OCID*128
    Where  : 
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm global mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmskhi_Base                                                                     0x0D007E
#define cAf6Reg_alm_glbmskhi(OCID)                                                       (0x0D007E + (OCID)*128)
#define cAf6Reg_alm_glbmskhi_WidthVal                                                                       32
#define cAf6Reg_alm_glbmskhi_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: glbmsk
BitField Type: RW
BitField Desc: global mask
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbmskhi_glbmsk_Bit_Start                                                                   0
#define cAf6_alm_glbmskhi_glbmsk_Bit_End                                                                    23
#define cAf6_alm_glbmskhi_glbmsk_Mask                                                                 cBit23_0
#define cAf6_alm_glbmskhi_glbmsk_Shift                                                                       0
#define cAf6_alm_glbmskhi_glbmsk_MaxVal                                                               0xffffff
#define cAf6_alm_glbmskhi_glbmsk_MinVal                                                                    0x0
#define cAf6_alm_glbmskhi_glbmsk_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Mask Report VT/TU3
Reg Addr   : 0x0E_0000
Reg Formula: 0x0E_0000 + $STS*32 + $OCID*4096 + $VTID
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
           + $VTID(0-27)  : VT ID
Reg Desc   : 
This register is used to get POH alarm mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_msklo_Base                                                                        0x0E0000
#define cAf6Reg_alm_msklo(STS, OCID, VTID)                              (0x0E0000+(STS)*32+(OCID)*4096+(VTID))
#define cAf6Reg_alm_msklo_WidthVal                                                                          32
#define cAf6Reg_alm_msklo_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: rdimsk
BitField Type: RW
BitField Desc: rdi mask
BitField Bits: [14]
--------------------------------------*/
#define cAf6_alm_msklo_rdimsk_Mask                                                                      cBit14
#define cAf6_alm_msklo_rdimsk_Shift                                                                         14

/*--------------------------------------
BitField Name: jnstachgmsk
BitField Type: RW
BitField Desc: jnstachg mask
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_msklo_jnstachgmsk_Bit_Start                                                                13
#define cAf6_alm_msklo_jnstachgmsk_Bit_End                                                                  13
#define cAf6_alm_msklo_jnstachgmsk_Mask                                                                 cBit13
#define cAf6_alm_msklo_jnstachgmsk_Shift                                                                    13
#define cAf6_alm_msklo_jnstachgmsk_MaxVal                                                                  0x1
#define cAf6_alm_msklo_jnstachgmsk_MinVal                                                                  0x0
#define cAf6_alm_msklo_jnstachgmsk_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: pslstachgmsk
BitField Type: RW
BitField Desc: pslstachg mask
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_msklo_pslstachgmsk_Bit_Start                                                               12
#define cAf6_alm_msklo_pslstachgmsk_Bit_End                                                                 12
#define cAf6_alm_msklo_pslstachgmsk_Mask                                                                cBit12
#define cAf6_alm_msklo_pslstachgmsk_Shift                                                                   12
#define cAf6_alm_msklo_pslstachgmsk_MaxVal                                                                 0x1
#define cAf6_alm_msklo_pslstachgmsk_MinVal                                                                 0x0
#define cAf6_alm_msklo_pslstachgmsk_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: bersdmsk
BitField Type: RW
BitField Desc: bersd mask
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_msklo_bersdmsk_Bit_Start                                                                   11
#define cAf6_alm_msklo_bersdmsk_Bit_End                                                                     11
#define cAf6_alm_msklo_bersdmsk_Mask                                                                    cBit11
#define cAf6_alm_msklo_bersdmsk_Shift                                                                       11
#define cAf6_alm_msklo_bersdmsk_MaxVal                                                                     0x1
#define cAf6_alm_msklo_bersdmsk_MinVal                                                                     0x0
#define cAf6_alm_msklo_bersdmsk_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: bersfmsk
BitField Type: RW
BitField Desc: bersf  mask
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_msklo_bersfmsk_Bit_Start                                                                   10
#define cAf6_alm_msklo_bersfmsk_Bit_End                                                                     10
#define cAf6_alm_msklo_bersfmsk_Mask                                                                    cBit10
#define cAf6_alm_msklo_bersfmsk_Shift                                                                       10
#define cAf6_alm_msklo_bersfmsk_MaxVal                                                                     0x1
#define cAf6_alm_msklo_bersfmsk_MinVal                                                                     0x0
#define cAf6_alm_msklo_bersfmsk_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: erdimsk
BitField Type: RW
BitField Desc: erdis/rdi mask
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_msklo_erdimsk_Bit_Start                                                                     9
#define cAf6_alm_msklo_erdimsk_Bit_End                                                                       9
#define cAf6_alm_msklo_erdimsk_Mask                                                                      cBit9
#define cAf6_alm_msklo_erdimsk_Shift                                                                         9
#define cAf6_alm_msklo_erdimsk_MaxVal                                                                      0x1
#define cAf6_alm_msklo_erdimsk_MinVal                                                                      0x0
#define cAf6_alm_msklo_erdimsk_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: bertcamsk
BitField Type: RW
BitField Desc: bertca mask
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_msklo_bertcamsk_Bit_Start                                                                   8
#define cAf6_alm_msklo_bertcamsk_Bit_End                                                                     8
#define cAf6_alm_msklo_bertcamsk_Mask                                                                    cBit8
#define cAf6_alm_msklo_bertcamsk_Shift                                                                       8
#define cAf6_alm_msklo_bertcamsk_MaxVal                                                                    0x1
#define cAf6_alm_msklo_bertcamsk_MinVal                                                                    0x0
#define cAf6_alm_msklo_bertcamsk_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: erdicmsk
BitField Type: RW
BitField Desc: erdic  mask
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_msklo_erdicmsk_Bit_Start                                                                    7
#define cAf6_alm_msklo_erdicmsk_Bit_End                                                                      7
#define cAf6_alm_msklo_erdicmsk_Mask                                                                     cBit7
#define cAf6_alm_msklo_erdicmsk_Shift                                                                        7
#define cAf6_alm_msklo_erdicmsk_MaxVal                                                                     0x1
#define cAf6_alm_msklo_erdicmsk_MinVal                                                                     0x0
#define cAf6_alm_msklo_erdicmsk_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: erdipmsk
BitField Type: RW
BitField Desc: erdip  mask
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_msklo_erdipmsk_Bit_Start                                                                    6
#define cAf6_alm_msklo_erdipmsk_Bit_End                                                                      6
#define cAf6_alm_msklo_erdipmsk_Mask                                                                     cBit6
#define cAf6_alm_msklo_erdipmsk_Shift                                                                        6
#define cAf6_alm_msklo_erdipmsk_MaxVal                                                                     0x1
#define cAf6_alm_msklo_erdipmsk_MinVal                                                                     0x0
#define cAf6_alm_msklo_erdipmsk_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: rfimsk
BitField Type: RW
BitField Desc: rfi mask
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_msklo_rfimsk_Bit_Start                                                                      5
#define cAf6_alm_msklo_rfimsk_Bit_End                                                                        5
#define cAf6_alm_msklo_rfimsk_Mask                                                                       cBit5
#define cAf6_alm_msklo_rfimsk_Shift                                                                          5
#define cAf6_alm_msklo_rfimsk_MaxVal                                                                       0x1
#define cAf6_alm_msklo_rfimsk_MinVal                                                                       0x0
#define cAf6_alm_msklo_rfimsk_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: tim mask
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_msklo_timmsk_Bit_Start                                                                      4
#define cAf6_alm_msklo_timmsk_Bit_End                                                                        4
#define cAf6_alm_msklo_timmsk_Mask                                                                       cBit4
#define cAf6_alm_msklo_timmsk_Shift                                                                          4
#define cAf6_alm_msklo_timmsk_MaxVal                                                                       0x1
#define cAf6_alm_msklo_timmsk_MinVal                                                                       0x0
#define cAf6_alm_msklo_timmsk_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: uneqmsk
BitField Type: RW
BitField Desc: uneq mask
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_msklo_uneqmsk_Bit_Start                                                                     3
#define cAf6_alm_msklo_uneqmsk_Bit_End                                                                       3
#define cAf6_alm_msklo_uneqmsk_Mask                                                                      cBit3
#define cAf6_alm_msklo_uneqmsk_Shift                                                                         3
#define cAf6_alm_msklo_uneqmsk_MaxVal                                                                      0x1
#define cAf6_alm_msklo_uneqmsk_MinVal                                                                      0x0
#define cAf6_alm_msklo_uneqmsk_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: plmmsk
BitField Type: RW
BitField Desc: plm mask
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_msklo_plmmsk_Bit_Start                                                                      2
#define cAf6_alm_msklo_plmmsk_Bit_End                                                                        2
#define cAf6_alm_msklo_plmmsk_Mask                                                                       cBit2
#define cAf6_alm_msklo_plmmsk_Shift                                                                          2
#define cAf6_alm_msklo_plmmsk_MaxVal                                                                       0x1
#define cAf6_alm_msklo_plmmsk_MinVal                                                                       0x0
#define cAf6_alm_msklo_plmmsk_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: aismsk
BitField Type: RW
BitField Desc: ais mask
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_msklo_aismsk_Bit_Start                                                                      1
#define cAf6_alm_msklo_aismsk_Bit_End                                                                        1
#define cAf6_alm_msklo_aismsk_Mask                                                                       cBit1
#define cAf6_alm_msklo_aismsk_Shift                                                                          1
#define cAf6_alm_msklo_aismsk_MaxVal                                                                       0x1
#define cAf6_alm_msklo_aismsk_MinVal                                                                       0x0
#define cAf6_alm_msklo_aismsk_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: lopmsk
BitField Type: RW
BitField Desc: lop mask
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_msklo_lopmsk_Bit_Start                                                                      0
#define cAf6_alm_msklo_lopmsk_Bit_End                                                                        0
#define cAf6_alm_msklo_lopmsk_Mask                                                                       cBit0
#define cAf6_alm_msklo_lopmsk_Shift                                                                          0
#define cAf6_alm_msklo_lopmsk_MaxVal                                                                       0x1
#define cAf6_alm_msklo_lopmsk_MinVal                                                                       0x0
#define cAf6_alm_msklo_lopmsk_RstVal                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Report VT/TU3
Reg Addr   : 0x0E_0800
Reg Formula: 0x0E_0800 + $STS*32 + $OCID*4096 + $VTID
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
           + $VTID(0-27)  : VT ID
Reg Desc   : 
This register is used to get POH alarm status.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_stalo_Base                                                                        0x0E0800
#define cAf6Reg_alm_stalo(STS, OCID, VTID)                              (0x0E0800+(STS)*32+(OCID)*4096+(VTID))
#define cAf6Reg_alm_stalo_WidthVal                                                                          32
#define cAf6Reg_alm_stalo_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: bersdsta
BitField Type: RW
BitField Desc: bersd status
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_stalo_bersdsta_Bit_Start                                                                   11
#define cAf6_alm_stalo_bersdsta_Bit_End                                                                     11
#define cAf6_alm_stalo_bersdsta_Mask                                                                    cBit11
#define cAf6_alm_stalo_bersdsta_Shift                                                                       11
#define cAf6_alm_stalo_bersdsta_MaxVal                                                                     0x1
#define cAf6_alm_stalo_bersdsta_MinVal                                                                     0x0
#define cAf6_alm_stalo_bersdsta_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: bersfsta
BitField Type: RW
BitField Desc: bersf  status
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_stalo_bersfsta_Bit_Start                                                                   10
#define cAf6_alm_stalo_bersfsta_Bit_End                                                                     10
#define cAf6_alm_stalo_bersfsta_Mask                                                                    cBit10
#define cAf6_alm_stalo_bersfsta_Shift                                                                       10
#define cAf6_alm_stalo_bersfsta_MaxVal                                                                     0x1
#define cAf6_alm_stalo_bersfsta_MinVal                                                                     0x0
#define cAf6_alm_stalo_bersfsta_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: erdista
BitField Type: RO
BitField Desc: erdis/rdi status
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_stalo_erdista_Bit_Start                                                                     9
#define cAf6_alm_stalo_erdista_Bit_End                                                                       9
#define cAf6_alm_stalo_erdista_Mask                                                                      cBit9
#define cAf6_alm_stalo_erdista_Shift                                                                         9
#define cAf6_alm_stalo_erdista_MaxVal                                                                      0x1
#define cAf6_alm_stalo_erdista_MinVal                                                                      0x0
#define cAf6_alm_stalo_erdista_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: bertcasta
BitField Type: RO
BitField Desc: bertca status
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_stalo_bertcasta_Bit_Start                                                                   8
#define cAf6_alm_stalo_bertcasta_Bit_End                                                                     8
#define cAf6_alm_stalo_bertcasta_Mask                                                                    cBit8
#define cAf6_alm_stalo_bertcasta_Shift                                                                       8
#define cAf6_alm_stalo_bertcasta_MaxVal                                                                    0x1
#define cAf6_alm_stalo_bertcasta_MinVal                                                                    0x0
#define cAf6_alm_stalo_bertcasta_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: erdicsta
BitField Type: RO
BitField Desc: erdic status
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_stalo_erdicsta_Bit_Start                                                                    7
#define cAf6_alm_stalo_erdicsta_Bit_End                                                                      7
#define cAf6_alm_stalo_erdicsta_Mask                                                                     cBit7
#define cAf6_alm_stalo_erdicsta_Shift                                                                        7
#define cAf6_alm_stalo_erdicsta_MaxVal                                                                     0x1
#define cAf6_alm_stalo_erdicsta_MinVal                                                                     0x0
#define cAf6_alm_stalo_erdicsta_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: erdipsta
BitField Type: RO
BitField Desc: erdip status
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_stalo_erdipsta_Bit_Start                                                                    6
#define cAf6_alm_stalo_erdipsta_Bit_End                                                                      6
#define cAf6_alm_stalo_erdipsta_Mask                                                                     cBit6
#define cAf6_alm_stalo_erdipsta_Shift                                                                        6
#define cAf6_alm_stalo_erdipsta_MaxVal                                                                     0x1
#define cAf6_alm_stalo_erdipsta_MinVal                                                                     0x0
#define cAf6_alm_stalo_erdipsta_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: rfista
BitField Type: RO
BitField Desc: rfi status
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_stalo_rfista_Bit_Start                                                                      5
#define cAf6_alm_stalo_rfista_Bit_End                                                                        5
#define cAf6_alm_stalo_rfista_Mask                                                                       cBit5
#define cAf6_alm_stalo_rfista_Shift                                                                          5
#define cAf6_alm_stalo_rfista_MaxVal                                                                       0x1
#define cAf6_alm_stalo_rfista_MinVal                                                                       0x0
#define cAf6_alm_stalo_rfista_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: timsta
BitField Type: RO
BitField Desc: tim status
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_stalo_timsta_Bit_Start                                                                      4
#define cAf6_alm_stalo_timsta_Bit_End                                                                        4
#define cAf6_alm_stalo_timsta_Mask                                                                       cBit4
#define cAf6_alm_stalo_timsta_Shift                                                                          4
#define cAf6_alm_stalo_timsta_MaxVal                                                                       0x1
#define cAf6_alm_stalo_timsta_MinVal                                                                       0x0
#define cAf6_alm_stalo_timsta_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: uneqsta
BitField Type: RO
BitField Desc: uneq status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_stalo_uneqsta_Bit_Start                                                                     3
#define cAf6_alm_stalo_uneqsta_Bit_End                                                                       3
#define cAf6_alm_stalo_uneqsta_Mask                                                                      cBit3
#define cAf6_alm_stalo_uneqsta_Shift                                                                         3
#define cAf6_alm_stalo_uneqsta_MaxVal                                                                      0x1
#define cAf6_alm_stalo_uneqsta_MinVal                                                                      0x0
#define cAf6_alm_stalo_uneqsta_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: plmsta
BitField Type: RO
BitField Desc: plm status
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_stalo_plmsta_Bit_Start                                                                      2
#define cAf6_alm_stalo_plmsta_Bit_End                                                                        2
#define cAf6_alm_stalo_plmsta_Mask                                                                       cBit2
#define cAf6_alm_stalo_plmsta_Shift                                                                          2
#define cAf6_alm_stalo_plmsta_MaxVal                                                                       0x1
#define cAf6_alm_stalo_plmsta_MinVal                                                                       0x0
#define cAf6_alm_stalo_plmsta_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: aissta
BitField Type: RO
BitField Desc: ais status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_stalo_aissta_Bit_Start                                                                      1
#define cAf6_alm_stalo_aissta_Bit_End                                                                        1
#define cAf6_alm_stalo_aissta_Mask                                                                       cBit1
#define cAf6_alm_stalo_aissta_Shift                                                                          1
#define cAf6_alm_stalo_aissta_MaxVal                                                                       0x1
#define cAf6_alm_stalo_aissta_MinVal                                                                       0x0
#define cAf6_alm_stalo_aissta_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: lopsta
BitField Type: RO
BitField Desc: lop status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_stalo_lopsta_Bit_Start                                                                      0
#define cAf6_alm_stalo_lopsta_Bit_End                                                                        0
#define cAf6_alm_stalo_lopsta_Mask                                                                       cBit0
#define cAf6_alm_stalo_lopsta_Shift                                                                          0
#define cAf6_alm_stalo_lopsta_MaxVal                                                                       0x1
#define cAf6_alm_stalo_lopsta_MinVal                                                                       0x0
#define cAf6_alm_stalo_lopsta_RstVal                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Status Report STS
Reg Addr   : 0x0E_0400
Reg Formula: 0x0E_0400 + $STS*32 + $OCID*4096 + $VTID
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
           + $VTID(0-27)  : VT ID
Reg Desc   : 
This register is used to get POH alarm change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_chglo_Base                                                                        0x0E0400
#define cAf6Reg_alm_chglo(STS, OCID, VTID)                              (0x0E0400+(STS)*32+(OCID)*4096+(VTID))
#define cAf6Reg_alm_chglo_WidthVal                                                                          32
#define cAf6Reg_alm_chglo_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: jnstachg
BitField Type: RW
BitField Desc: jn message change
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_chglo_jnstachg_Bit_Start                                                                   13
#define cAf6_alm_chglo_jnstachg_Bit_End                                                                     13
#define cAf6_alm_chglo_jnstachg_Mask                                                                    cBit13
#define cAf6_alm_chglo_jnstachg_Shift                                                                       13
#define cAf6_alm_chglo_jnstachg_MaxVal                                                                     0x1
#define cAf6_alm_chglo_jnstachg_MinVal                                                                     0x0
#define cAf6_alm_chglo_jnstachg_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: pslstachg
BitField Type: RW
BitField Desc: psl byte change
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_chglo_pslstachg_Bit_Start                                                                  12
#define cAf6_alm_chglo_pslstachg_Bit_End                                                                    12
#define cAf6_alm_chglo_pslstachg_Mask                                                                   cBit12
#define cAf6_alm_chglo_pslstachg_Shift                                                                      12
#define cAf6_alm_chglo_pslstachg_MaxVal                                                                    0x1
#define cAf6_alm_chglo_pslstachg_MinVal                                                                    0x0
#define cAf6_alm_chglo_pslstachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: bersdstachg
BitField Type: W1C
BitField Desc: bersd stable status change
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_chglo_bersdstachg_Bit_Start                                                                11
#define cAf6_alm_chglo_bersdstachg_Bit_End                                                                  11
#define cAf6_alm_chglo_bersdstachg_Mask                                                                 cBit11
#define cAf6_alm_chglo_bersdstachg_Shift                                                                    11
#define cAf6_alm_chglo_bersdstachg_MaxVal                                                                  0x1
#define cAf6_alm_chglo_bersdstachg_MinVal                                                                  0x0
#define cAf6_alm_chglo_bersdstachg_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: bersfstachg
BitField Type: W1C
BitField Desc: bersf stable status change
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_chglo_bersfstachg_Bit_Start                                                                10
#define cAf6_alm_chglo_bersfstachg_Bit_End                                                                  10
#define cAf6_alm_chglo_bersfstachg_Mask                                                                 cBit10
#define cAf6_alm_chglo_bersfstachg_Shift                                                                    10
#define cAf6_alm_chglo_bersfstachg_MaxVal                                                                  0x1
#define cAf6_alm_chglo_bersfstachg_MinVal                                                                  0x0
#define cAf6_alm_chglo_bersfstachg_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: erdistachg
BitField Type: W1C
BitField Desc: erdis/rdi status change
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_chglo_erdistachg_Bit_Start                                                                  9
#define cAf6_alm_chglo_erdistachg_Bit_End                                                                    9
#define cAf6_alm_chglo_erdistachg_Mask                                                                   cBit9
#define cAf6_alm_chglo_erdistachg_Shift                                                                      9
#define cAf6_alm_chglo_erdistachg_MaxVal                                                                   0x1
#define cAf6_alm_chglo_erdistachg_MinVal                                                                   0x0
#define cAf6_alm_chglo_erdistachg_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: bertcastachg
BitField Type: W1C
BitField Desc: bertca status change
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_chglo_bertcastachg_Bit_Start                                                                8
#define cAf6_alm_chglo_bertcastachg_Bit_End                                                                  8
#define cAf6_alm_chglo_bertcastachg_Mask                                                                 cBit8
#define cAf6_alm_chglo_bertcastachg_Shift                                                                    8
#define cAf6_alm_chglo_bertcastachg_MaxVal                                                                 0x1
#define cAf6_alm_chglo_bertcastachg_MinVal                                                                 0x0
#define cAf6_alm_chglo_bertcastachg_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: erdicstachg
BitField Type: W1C
BitField Desc: erdic status change
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_chglo_erdicstachg_Bit_Start                                                                 7
#define cAf6_alm_chglo_erdicstachg_Bit_End                                                                   7
#define cAf6_alm_chglo_erdicstachg_Mask                                                                  cBit7
#define cAf6_alm_chglo_erdicstachg_Shift                                                                     7
#define cAf6_alm_chglo_erdicstachg_MaxVal                                                                  0x1
#define cAf6_alm_chglo_erdicstachg_MinVal                                                                  0x0
#define cAf6_alm_chglo_erdicstachg_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: erdipstachg
BitField Type: W1C
BitField Desc: erdip status change
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_chglo_erdipstachg_Bit_Start                                                                 6
#define cAf6_alm_chglo_erdipstachg_Bit_End                                                                   6
#define cAf6_alm_chglo_erdipstachg_Mask                                                                  cBit6
#define cAf6_alm_chglo_erdipstachg_Shift                                                                     6
#define cAf6_alm_chglo_erdipstachg_MaxVal                                                                  0x1
#define cAf6_alm_chglo_erdipstachg_MinVal                                                                  0x0
#define cAf6_alm_chglo_erdipstachg_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: rfistachg
BitField Type: W1C
BitField Desc: rfi status change
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_chglo_rfistachg_Bit_Start                                                                   5
#define cAf6_alm_chglo_rfistachg_Bit_End                                                                     5
#define cAf6_alm_chglo_rfistachg_Mask                                                                    cBit5
#define cAf6_alm_chglo_rfistachg_Shift                                                                       5
#define cAf6_alm_chglo_rfistachg_MaxVal                                                                    0x1
#define cAf6_alm_chglo_rfistachg_MinVal                                                                    0x0
#define cAf6_alm_chglo_rfistachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: timstachg
BitField Type: W1C
BitField Desc: tim status change
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_chglo_timstachg_Bit_Start                                                                   4
#define cAf6_alm_chglo_timstachg_Bit_End                                                                     4
#define cAf6_alm_chglo_timstachg_Mask                                                                    cBit4
#define cAf6_alm_chglo_timstachg_Shift                                                                       4
#define cAf6_alm_chglo_timstachg_MaxVal                                                                    0x1
#define cAf6_alm_chglo_timstachg_MinVal                                                                    0x0
#define cAf6_alm_chglo_timstachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: uneqstachg
BitField Type: W1C
BitField Desc: uneq status change
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_chglo_uneqstachg_Bit_Start                                                                  3
#define cAf6_alm_chglo_uneqstachg_Bit_End                                                                    3
#define cAf6_alm_chglo_uneqstachg_Mask                                                                   cBit3
#define cAf6_alm_chglo_uneqstachg_Shift                                                                      3
#define cAf6_alm_chglo_uneqstachg_MaxVal                                                                   0x1
#define cAf6_alm_chglo_uneqstachg_MinVal                                                                   0x0
#define cAf6_alm_chglo_uneqstachg_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: plmstachg
BitField Type: W1C
BitField Desc: plm status change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_chglo_plmstachg_Bit_Start                                                                   2
#define cAf6_alm_chglo_plmstachg_Bit_End                                                                     2
#define cAf6_alm_chglo_plmstachg_Mask                                                                    cBit2
#define cAf6_alm_chglo_plmstachg_Shift                                                                       2
#define cAf6_alm_chglo_plmstachg_MaxVal                                                                    0x1
#define cAf6_alm_chglo_plmstachg_MinVal                                                                    0x0
#define cAf6_alm_chglo_plmstachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: aisstachg
BitField Type: W1C
BitField Desc: ais status change
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_chglo_aisstachg_Bit_Start                                                                   1
#define cAf6_alm_chglo_aisstachg_Bit_End                                                                     1
#define cAf6_alm_chglo_aisstachg_Mask                                                                    cBit1
#define cAf6_alm_chglo_aisstachg_Shift                                                                       1
#define cAf6_alm_chglo_aisstachg_MaxVal                                                                    0x1
#define cAf6_alm_chglo_aisstachg_MinVal                                                                    0x0
#define cAf6_alm_chglo_aisstachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: lopstachg
BitField Type: W1C
BitField Desc: lop status change
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_chglo_lopstachg_Bit_Start                                                                   0
#define cAf6_alm_chglo_lopstachg_Bit_End                                                                     0
#define cAf6_alm_chglo_lopstachg_Mask                                                                    cBit0
#define cAf6_alm_chglo_lopstachg_Shift                                                                       0
#define cAf6_alm_chglo_lopstachg_MaxVal                                                                    0x1
#define cAf6_alm_chglo_lopstachg_MinVal                                                                    0x0
#define cAf6_alm_chglo_lopstachg_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Or Status Report VT/TU3
Reg Addr   : 0x0E_0C00
Reg Formula: 0x0E_0C00 + $STS + $OCID*4096
    Where  : 
           + $STS(0-23)  : STS
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm or status change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_orstalo_Base                                                                      0x0E0C00
#define cAf6Reg_alm_orstalo(STS, OCID)                                            (0x0E0C00+(STS)+(OCID)*4096)
#define cAf6Reg_alm_orstalo_WidthVal                                                                        32
#define cAf6Reg_alm_orstalo_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: orstachg
BitField Type: RO
BitField Desc: or status change bit
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_alm_orstalo_orstachg_Bit_Start                                                                  0
#define cAf6_alm_orstalo_orstachg_Bit_End                                                                   27
#define cAf6_alm_orstalo_orstachg_Mask                                                                cBit27_0
#define cAf6_alm_orstalo_orstachg_Shift                                                                      0
#define cAf6_alm_orstalo_orstachg_MaxVal                                                             0xfffffff
#define cAf6_alm_orstalo_orstachg_MinVal                                                                   0x0
#define cAf6_alm_orstalo_orstachg_RstVal                                                                   0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report VT/TU3
Reg Addr   : 0x0E_0FFF
Reg Formula: 0x0E_0FFF + $OCID*4096
    Where  : 
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm global change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchglo_Base                                                                     0x0E0FFF
#define cAf6Reg_alm_glbchglo(OCID)                                                      (0x0E0FFF+(OCID)*4096)
#define cAf6Reg_alm_glbchglo_WidthVal                                                                       32
#define cAf6Reg_alm_glbchglo_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: glbstachg
BitField Type: RO
BitField Desc: global status change bit
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbchglo_glbstachg_Bit_Start                                                                0
#define cAf6_alm_glbchglo_glbstachg_Bit_End                                                                 23
#define cAf6_alm_glbchglo_glbstachg_Mask                                                              cBit23_0
#define cAf6_alm_glbchglo_glbstachg_Shift                                                                    0
#define cAf6_alm_glbchglo_glbstachg_MaxVal                                                            0xffffff
#define cAf6_alm_glbchglo_glbstachg_MinVal                                                                 0x0
#define cAf6_alm_glbchglo_glbstachg_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report VT/TU3
Reg Addr   : 0x0E_0FFE
Reg Formula: 0x0E_0FFE + $OCID*4096
    Where  : 
           + $OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   : 
This register is used to get POH alarm global mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmsklo_Base                                                                     0x0E0FFE
#define cAf6Reg_alm_glbmsklo(OCID)                                                      (0x0E0FFEUL + (OCID) * 4096)
#define cAf6Reg_alm_glbmsklo_WidthVal                                                                       32
#define cAf6Reg_alm_glbmsklo_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: glbmsk
BitField Type: RW
BitField Desc: global status change bit
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbmsklo_glbmsk_Bit_Start                                                                   0
#define cAf6_alm_glbmsklo_glbmsk_Bit_End                                                                    23
#define cAf6_alm_glbmsklo_glbmsk_Mask                                                                 cBit23_0
#define cAf6_alm_glbmsklo_glbmsk_Shift                                                                       0
#define cAf6_alm_glbmsklo_glbmsk_MaxVal                                                               0xffffff
#define cAf6_alm_glbmsklo_glbmsk_MinVal                                                                    0x0
#define cAf6_alm_glbmsklo_glbmsk_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report
Reg Addr   : 0x00_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH alarm global mask report for high,low order.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmsk_Base                                                                       0x000004
#define cAf6Reg_alm_glbmsk                                                                            0x000004
#define cAf6Reg_alm_glbmsk_WidthVal                                                                         32
#define cAf6Reg_alm_glbmsk_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: glbmsklo
BitField Type: RW
BitField Desc: global mask bit for low order slice -
ocid11...ocid3,ocid1,ocid10...ocid2,ocid0
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_alm_glbmsk_glbmsklo_Bit_Start                                                                  16
#define cAf6_alm_glbmsk_glbmsklo_Bit_End                                                                    27
#define cAf6_alm_glbmsk_glbmsklo_Mask                                                                cBit27_16
#define cAf6_alm_glbmsk_glbmsklo_Shift                                                                      16
#define cAf6_alm_glbmsk_glbmsklo_MaxVal                                                                  0xfff
#define cAf6_alm_glbmsk_glbmsklo_MinVal                                                                    0x0
#define cAf6_alm_glbmsk_glbmsklo_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: glbmskhi
BitField Type: RW
BitField Desc: global mask change bit for high order slice -
ocid15...ocid3,ocid1,ocid14...ocid2,ocid0
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_alm_glbmsk_glbmskhi_Bit_Start                                                                   0
#define cAf6_alm_glbmsk_glbmskhi_Bit_End                                                                    15
#define cAf6_alm_glbmsk_glbmskhi_Mask                                                                 cBit15_0
#define cAf6_alm_glbmsk_glbmskhi_Shift                                                                       0
#define cAf6_alm_glbmsk_glbmskhi_MaxVal                                                                 0xffff
#define cAf6_alm_glbmsk_glbmskhi_MinVal                                                                    0x0
#define cAf6_alm_glbmsk_glbmskhi_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report
Reg Addr   : 0x00_0005
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH alarm global change status report for high,low order.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchg_Base                                                                       0x000005
#define cAf6Reg_alm_glbchg                                                                            0x000005
#define cAf6Reg_alm_glbchg_WidthVal                                                                         32
#define cAf6Reg_alm_glbchg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: glbstachglo
BitField Type: RO
BitField Desc: global status change bit for low order slice -
ocid11...ocid3,ocid1,ocid10...ocid2,ocid0
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_alm_glbchg_glbstachglo_Bit_Start                                                               16
#define cAf6_alm_glbchg_glbstachglo_Bit_End                                                                 27
#define cAf6_alm_glbchg_glbstachglo_Mask                                                             cBit27_16
#define cAf6_alm_glbchg_glbstachglo_Shift                                                                   16
#define cAf6_alm_glbchg_glbstachglo_MaxVal                                                               0xfff
#define cAf6_alm_glbchg_glbstachglo_MinVal                                                                 0x0
#define cAf6_alm_glbchg_glbstachglo_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: glbstachghi
BitField Type: RO
BitField Desc: global status change bit for high order slice -
ocid15...ocid3,ocid1,ocid14...ocid2,ocid0
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_alm_glbchg_glbstachghi_Bit_Start                                                                0
#define cAf6_alm_glbchg_glbstachghi_Bit_End                                                                 15
#define cAf6_alm_glbchg_glbstachghi_Mask                                                              cBit15_0
#define cAf6_alm_glbchg_glbstachghi_Shift                                                                    0
#define cAf6_alm_glbchg_glbstachghi_MaxVal                                                              0xffff
#define cAf6_alm_glbchg_glbstachghi_MinVal                                                                 0x0
#define cAf6_alm_glbchg_glbstachghi_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt  Global Status Out Report
Reg Addr   : 0x00_0006
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get POH alarm global change status report for high,low order after ANDED with mask.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchgo_Base                                                                      0x000006
#define cAf6Reg_alm_glbchgo                                                                           0x000006
#define cAf6Reg_alm_glbchgo_WidthVal                                                                        32
#define cAf6Reg_alm_glbchgo_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: glbstachglo
BitField Type: RO
BitField Desc: global status change bit for low order slice -
ocid11...ocid3,ocid1,ocid10...ocid2,ocid0
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_alm_glbchgo_glbstachglo_Bit_Start                                                              16
#define cAf6_alm_glbchgo_glbstachglo_Bit_End                                                                27
#define cAf6_alm_glbchgo_glbstachglo_Mask                                                            cBit27_16
#define cAf6_alm_glbchgo_glbstachglo_Shift                                                                  16
#define cAf6_alm_glbchgo_glbstachglo_MaxVal                                                              0xfff
#define cAf6_alm_glbchgo_glbstachglo_MinVal                                                                0x0
#define cAf6_alm_glbchgo_glbstachglo_RstVal                                                                0x0

/*--------------------------------------
BitField Name: glbstachghi
BitField Type: RO
BitField Desc: global status change bit for high order slice -
ocid15...ocid3,ocid1,ocid14...ocid2,ocid0
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_alm_glbchgo_glbstachghi_Bit_Start                                                               0
#define cAf6_alm_glbchgo_glbstachghi_Bit_End                                                                15
#define cAf6_alm_glbchgo_glbstachghi_Mask                                                             cBit15_0
#define cAf6_alm_glbchgo_glbstachghi_Shift                                                                   0
#define cAf6_alm_glbchgo_glbstachghi_MaxVal                                                             0xffff
#define cAf6_alm_glbchgo_glbstachghi_MinVal                                                                0x0
#define cAf6_alm_glbchgo_glbstachghi_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : AME version
Reg Addr   : 0x0F_0000
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to get Version of Alarm Monitoring Engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_amever_Base                                                                      0x0F0000
#define cAf6Reg_upen_amever                                                                           0x0F0000
#define cAf6Reg_upen_amever_WidthVal                                                                        32
#define cAf6Reg_upen_amever_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: AMEEngId
BitField Type: RO
BitField Desc: Engine ID
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_amever_AMEEngId_Bit_Start                                                                 16
#define cAf6_upen_amever_AMEEngId_Bit_End                                                                   31
#define cAf6_upen_amever_AMEEngId_Mask                                                               cBit31_16
#define cAf6_upen_amever_AMEEngId_Shift                                                                     16
#define cAf6_upen_amever_AMEEngId_MaxVal                                                                0xffff
#define cAf6_upen_amever_AMEEngId_MinVal                                                                   0x0
#define cAf6_upen_amever_AMEEngId_RstVal                                                                0xAA41

/*--------------------------------------
BitField Name: AMEGlbSTS
BitField Type: RO
BitField Desc: STS bits
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_upen_amever_AMEGlbSTS_Bit_Start                                                                12
#define cAf6_upen_amever_AMEGlbSTS_Bit_End                                                                  15
#define cAf6_upen_amever_AMEGlbSTS_Mask                                                              cBit15_12
#define cAf6_upen_amever_AMEGlbSTS_Shift                                                                    12
#define cAf6_upen_amever_AMEGlbSTS_MaxVal                                                                  0xf
#define cAf6_upen_amever_AMEGlbSTS_MinVal                                                                  0x0
#define cAf6_upen_amever_AMEGlbSTS_RstVal                                                                  0x9

/*--------------------------------------
BitField Name: AMEGlbVT
BitField Type: RO
BitField Desc: VT bits
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_upen_amever_AMEGlbVT_Bit_Start                                                                  8
#define cAf6_upen_amever_AMEGlbVT_Bit_End                                                                   11
#define cAf6_upen_amever_AMEGlbVT_Mask                                                                cBit11_8
#define cAf6_upen_amever_AMEGlbVT_Shift                                                                      8
#define cAf6_upen_amever_AMEGlbVT_MaxVal                                                                   0xf
#define cAf6_upen_amever_AMEGlbVT_MinVal                                                                   0x0
#define cAf6_upen_amever_AMEGlbVT_RstVal                                                                   0x5

/*--------------------------------------
BitField Name: AMEGlbVer
BitField Type: RO
BitField Desc: Version
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_amever_AMEGlbVer_Bit_Start                                                                 0
#define cAf6_upen_amever_AMEGlbVer_Bit_End                                                                   7
#define cAf6_upen_amever_AMEGlbVer_Mask                                                                cBit7_0
#define cAf6_upen_amever_AMEGlbVer_Shift                                                                     0
#define cAf6_upen_amever_AMEGlbVer_MaxVal                                                                 0xff
#define cAf6_upen_amever_AMEGlbVer_MinVal                                                                  0x0
#define cAf6_upen_amever_AMEGlbVer_RstVal                                                                 0x10


/*------------------------------------------------------------------------------
Reg Name   : AME Control
Reg Addr   : 0x0F_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control operation of AM engine

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_amectl_Base                                                                      0x0F0002
#define cAf6Reg_upen_amectl                                                                           0x0F0002
#define cAf6Reg_upen_amectl_WidthVal                                                                        32
#define cAf6Reg_upen_amectl_WriteMask                                                                      0x0

/*--------------------------------------
BitField Name: AMECtlScale
BitField Type: RW
BitField Desc: Scale timer, devide by 2^AMECtlScale
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_upen_amectl_AMECtlScale_Bit_Start                                                               4
#define cAf6_upen_amectl_AMECtlScale_Bit_End                                                                 5
#define cAf6_upen_amectl_AMECtlScale_Mask                                                              cBit5_4
#define cAf6_upen_amectl_AMECtlScale_Shift                                                                   4
#define cAf6_upen_amectl_AMECtlScale_MaxVal                                                                0x3
#define cAf6_upen_amectl_AMECtlScale_MinVal                                                                0x0
#define cAf6_upen_amectl_AMECtlScale_RstVal                                                                0x0

/*--------------------------------------
BitField Name: AMECtlForceInt
BitField Type: RW
BitField Desc: Force Interrupt Pin
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_amectl_AMECtlForceInt_Bit_Start                                                            2
#define cAf6_upen_amectl_AMECtlForceInt_Bit_End                                                              2
#define cAf6_upen_amectl_AMECtlForceInt_Mask                                                             cBit2
#define cAf6_upen_amectl_AMECtlForceInt_Shift                                                                2
#define cAf6_upen_amectl_AMECtlForceInt_MaxVal                                                             0x1
#define cAf6_upen_amectl_AMECtlForceInt_MinVal                                                             0x0
#define cAf6_upen_amectl_AMECtlForceInt_RstVal                                                             0x0

/*--------------------------------------
BitField Name: AMECtlClrHOff
BitField Type: RW
BitField Desc: Clear with hold_off_timer
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_amectl_AMECtlClrHOff_Bit_Start                                                             1
#define cAf6_upen_amectl_AMECtlClrHOff_Bit_End                                                               1
#define cAf6_upen_amectl_AMECtlClrHOff_Mask                                                              cBit1
#define cAf6_upen_amectl_AMECtlClrHOff_Shift                                                                 1
#define cAf6_upen_amectl_AMECtlClrHOff_MaxVal                                                              0x1
#define cAf6_upen_amectl_AMECtlClrHOff_MinVal                                                              0x0
#define cAf6_upen_amectl_AMECtlClrHOff_RstVal                                                              0x1

/*--------------------------------------
BitField Name: AMECtlActive
BitField Type: RW
BitField Desc: Active
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_amectl_AMECtlActive_Bit_Start                                                              0
#define cAf6_upen_amectl_AMECtlActive_Bit_End                                                                0
#define cAf6_upen_amectl_AMECtlActive_Mask                                                               cBit0
#define cAf6_upen_amectl_AMECtlActive_Shift                                                                  0
#define cAf6_upen_amectl_AMECtlActive_MaxVal                                                               0x1
#define cAf6_upen_amectl_AMECtlActive_MinVal                                                               0x0
#define cAf6_upen_amectl_AMECtlActive_RstVal                                                               0x1


/*------------------------------------------------------------------------------
Reg Name   : AME SF/SD Alarm Mask
Reg Addr   : 0x0F_0004
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to Select alarm {ais,lop,plm,ber_sd,ber_sf} into SF/SD table

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_amemask_Base                                                                     0x0F0004
#define cAf6Reg_upen_amemask                                                                          0x0F0004
#define cAf6Reg_upen_amemask_WidthVal                                                                       32
#define cAf6Reg_upen_amemask_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: AMESDAlmMask
BitField Type: RW
BitField Desc: SD Alarm Mask, set 1 to choose bit_0: choose BER_SF alarm bit_1:
choose BER_SD alarm bit_2: choose PLM alarm bit_3: choose LOP alarm bit_4:
choose AIS alarm
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_amemask_AMESDAlmMask_Bit_Start                                                             8
#define cAf6_upen_amemask_AMESDAlmMask_Bit_End                                                              15
#define cAf6_upen_amemask_AMESDAlmMask_Mask                                                           cBit15_8
#define cAf6_upen_amemask_AMESDAlmMask_Shift                                                                 8
#define cAf6_upen_amemask_AMESDAlmMask_MaxVal                                                             0xff
#define cAf6_upen_amemask_AMESDAlmMask_MinVal                                                              0x0
#define cAf6_upen_amemask_AMESDAlmMask_RstVal                                                             0x1B

/*--------------------------------------
BitField Name: AMESFAlmMask
BitField Type: RW
BitField Desc: SF Alarm Mask
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_amemask_AMESFAlmMask_Bit_Start                                                             0
#define cAf6_upen_amemask_AMESFAlmMask_Bit_End                                                               7
#define cAf6_upen_amemask_AMESFAlmMask_Mask                                                            cBit7_0
#define cAf6_upen_amemask_AMESFAlmMask_Shift                                                                 0
#define cAf6_upen_amemask_AMESFAlmMask_MaxVal                                                             0xff
#define cAf6_upen_amemask_AMESFAlmMask_MinVal                                                              0x0
#define cAf6_upen_amemask_AMESFAlmMask_RstVal                                                             0x1B


/*------------------------------------------------------------------------------
Reg Name   : AME MASR Interrupt Mask
Reg Addr   : 0x0F_0010
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to enable/disable interrupt pin

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ameintdi_Base                                                                    0x0F0010
#define cAf6Reg_upen_ameintdi                                                                         0x0F0010
#define cAf6Reg_upen_ameintdi_WidthVal                                                                      32
#define cAf6Reg_upen_ameintdi_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: AMESDDis
BitField Type: RW
BitField Desc: SD Interrupt disable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_ameintdi_AMESDDis_Bit_Start                                                                2
#define cAf6_upen_ameintdi_AMESDDis_Bit_End                                                                  2
#define cAf6_upen_ameintdi_AMESDDis_Mask                                                                 cBit2
#define cAf6_upen_ameintdi_AMESDDis_Shift                                                                    2
#define cAf6_upen_ameintdi_AMESDDis_MaxVal                                                                 0x1
#define cAf6_upen_ameintdi_AMESDDis_MinVal                                                                 0x0
#define cAf6_upen_ameintdi_AMESDDis_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: AMESFDis
BitField Type: RW
BitField Desc: SF Interrupt disable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_ameintdi_AMESFDis_Bit_Start                                                                1
#define cAf6_upen_ameintdi_AMESFDis_Bit_End                                                                  1
#define cAf6_upen_ameintdi_AMESFDis_Mask                                                                 cBit1
#define cAf6_upen_ameintdi_AMESFDis_Shift                                                                    1
#define cAf6_upen_ameintdi_AMESFDis_MaxVal                                                                 0x1
#define cAf6_upen_ameintdi_AMESFDis_MinVal                                                                 0x0
#define cAf6_upen_ameintdi_AMESFDis_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: AMEIntDis
BitField Type: RW
BitField Desc: AME Interrupt disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_ameintdi_AMEIntDis_Bit_Start                                                               0
#define cAf6_upen_ameintdi_AMEIntDis_Bit_End                                                                 0
#define cAf6_upen_ameintdi_AMEIntDis_Mask                                                                cBit0
#define cAf6_upen_ameintdi_AMEIntDis_Shift                                                                   0
#define cAf6_upen_ameintdi_AMEIntDis_MaxVal                                                                0x1
#define cAf6_upen_ameintdi_AMEIntDis_MinVal                                                                0x0
#define cAf6_upen_ameintdi_AMEIntDis_RstVal                                                                0x1


/*------------------------------------------------------------------------------
Reg Name   : AME SF MASR register
Reg Addr   : 0x0F_0011
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to report SF row STS/VT table

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sfrowsts_Base                                                                    0x0F0011
#define cAf6Reg_upen_sfrowsts                                                                         0x0F0011
#define cAf6Reg_upen_sfrowsts_WidthVal                                                                      32
#define cAf6Reg_upen_sfrowsts_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: AMESFrowVT
BitField Type: RC
BitField Desc: SF VT ROW status
BitField Bits: [23:12]
--------------------------------------*/
#define cAf6_upen_sfrowsts_AMESFrowVT_Bit_Start                                                             12
#define cAf6_upen_sfrowsts_AMESFrowVT_Bit_End                                                               23
#define cAf6_upen_sfrowsts_AMESFrowVT_Mask                                                           cBit23_12
#define cAf6_upen_sfrowsts_AMESFrowVT_Shift                                                                 12
#define cAf6_upen_sfrowsts_AMESFrowVT_MaxVal                                                             0xfff
#define cAf6_upen_sfrowsts_AMESFrowVT_MinVal                                                               0x0
#define cAf6_upen_sfrowsts_AMESFrowVT_RstVal                                                               0x0

/*--------------------------------------
BitField Name: AMESFrowSTS
BitField Type: RC
BitField Desc: SF STS ROW Status
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_sfrowsts_AMESFrowSTS_Bit_Start                                                             0
#define cAf6_upen_sfrowsts_AMESFrowSTS_Bit_End                                                              11
#define cAf6_upen_sfrowsts_AMESFrowSTS_Mask                                                           cBit11_0
#define cAf6_upen_sfrowsts_AMESFrowSTS_Shift                                                                 0
#define cAf6_upen_sfrowsts_AMESFrowSTS_MaxVal                                                            0xfff
#define cAf6_upen_sfrowsts_AMESFrowSTS_MinVal                                                              0x0
#define cAf6_upen_sfrowsts_AMESFrowSTS_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : AME SD MASR register
Reg Addr   : 0x0F_0012
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to report SD row STS/VT table

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sdrowsts_Base                                                                    0x0F0012
#define cAf6Reg_upen_sdrowsts                                                                         0x0F0012
#define cAf6Reg_upen_sdrowsts_WidthVal                                                                      32
#define cAf6Reg_upen_sdrowsts_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: AMESDrowVT
BitField Type: RC
BitField Desc: SD VT ROW status bit_0: STS#0-STS#31 bit_1: STS#32-STS#63 ..
BitField Bits: [23:12]
--------------------------------------*/
#define cAf6_upen_sdrowsts_AMESDrowVT_Bit_Start                                                             12
#define cAf6_upen_sdrowsts_AMESDrowVT_Bit_End                                                               23
#define cAf6_upen_sdrowsts_AMESDrowVT_Mask                                                           cBit23_12
#define cAf6_upen_sdrowsts_AMESDrowVT_Shift                                                                 12
#define cAf6_upen_sdrowsts_AMESDrowVT_MaxVal                                                             0xfff
#define cAf6_upen_sdrowsts_AMESDrowVT_MinVal                                                               0x0
#define cAf6_upen_sdrowsts_AMESDrowVT_RstVal                                                               0x0

/*--------------------------------------
BitField Name: AMESDrowSTS
BitField Type: RC
BitField Desc: SD STS ROW Status
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_sdrowsts_AMESDrowSTS_Bit_Start                                                             0
#define cAf6_upen_sdrowsts_AMESDrowSTS_Bit_End                                                              11
#define cAf6_upen_sdrowsts_AMESDrowSTS_Mask                                                           cBit11_0
#define cAf6_upen_sdrowsts_AMESDrowSTS_Shift                                                                 0
#define cAf6_upen_sdrowsts_AMESDrowSTS_MaxVal                                                            0xfff
#define cAf6_upen_sdrowsts_AMESDrowSTS_MinVal                                                              0x0
#define cAf6_upen_sdrowsts_AMESDrowSTS_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : AME STS Status table
Reg Addr   : 0x0F_2000
Reg Formula: 0x0F_2000 + $sd_table*0x8000 + $sts_group
    Where  : 
           + $sd_table(0-1): 1 -> SD table, 0 -> SF table
           + $sts_group(0-11) sts group
Reg Desc   : 
This register is used to report STS status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxstasts_Base                                                                    0x0F2000
#define cAf6Reg_upen_xxstasts(sdtable, stsgroup)                        (0x0F2000+(sdtable)*0x8000+(stsgroup))
#define cAf6Reg_upen_xxstasts_WidthVal                                                                      32
#define cAf6Reg_upen_xxstasts_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: AMEStaSTS
BitField Type: RO
BitField Desc: STS Table Status bit_0: STS#0 bit_1: STS#1 ...
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_xxstasts_AMEStaSTS_Bit_Start                                                               0
#define cAf6_upen_xxstasts_AMEStaSTS_Bit_End                                                                31
#define cAf6_upen_xxstasts_AMEStaSTS_Mask                                                             cBit31_0
#define cAf6_upen_xxstasts_AMEStaSTS_Shift                                                                   0
#define cAf6_upen_xxstasts_AMEStaSTS_MaxVal                                                         0xffffffff
#define cAf6_upen_xxstasts_AMEStaSTS_MinVal                                                                0x0
#define cAf6_upen_xxstasts_AMEStaSTS_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : AME STS Enable table
Reg Addr   : 0x0F_3000
Reg Formula: 0x0F_3000 + $sd_table*0x8000 + $sts_group
    Where  : 
           + $sd_table(0-1): 1 -> SD table, 0 -> SF table
           + $sts_group(0-11) sts group
Reg Desc   : 
This register is used to enable STS table

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxcfgsts_Base                                                                    0x0F3000
#define cAf6Reg_upen_xxcfgsts(sdtable, stsgroup)                        (0x0F3000+(sdtable)*0x8000+(stsgroup))
#define cAf6Reg_upen_xxcfgsts_WidthVal                                                                      32
#define cAf6Reg_upen_xxcfgsts_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: AMEEnaSTS
BitField Type: RW
BitField Desc: STS Table Enabl bit_0: STS#0 bit_1: STS#1 ...
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_xxcfgsts_AMEEnaSTS_Bit_Start                                                               0
#define cAf6_upen_xxcfgsts_AMEEnaSTS_Bit_End                                                                31
#define cAf6_upen_xxcfgsts_AMEEnaSTS_Mask                                                             cBit31_0
#define cAf6_upen_xxcfgsts_AMEEnaSTS_Shift                                                                   0
#define cAf6_upen_xxcfgsts_AMEEnaSTS_MaxVal                                                         0xffffffff
#define cAf6_upen_xxcfgsts_AMEEnaSTS_MinVal                                                                0x0
#define cAf6_upen_xxcfgsts_AMEEnaSTS_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : AME ROW VT Status table
Reg Addr   : 0x0F_2100
Reg Formula: 0x0F_2100 + $sd_table*0x8000 + $sts_group
    Where  : 
           + $sd_table(0-1): 1 -> SD table, 0 -> SF table
           + $sts_group(0-11) sts_group
Reg Desc   : 
This register is used to report Row VT status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxrowvt_Base                                                                     0x0F2100
#define cAf6Reg_upen_xxrowvt(sdtable, stsgroup)                         (0x0F2100+(sdtable)*0x8000+(stsgroup))
#define cAf6Reg_upen_xxrowvt_WidthVal                                                                       32
#define cAf6Reg_upen_xxrowvt_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: AMEStaRowVT
BitField Type: RC
BitField Desc: Row VT Table Status bit_0: STS#0 bit_1: STS#1 ...
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_xxrowvt_AMEStaRowVT_Bit_Start                                                              0
#define cAf6_upen_xxrowvt_AMEStaRowVT_Bit_End                                                               31
#define cAf6_upen_xxrowvt_AMEStaRowVT_Mask                                                            cBit31_0
#define cAf6_upen_xxrowvt_AMEStaRowVT_Shift                                                                  0
#define cAf6_upen_xxrowvt_AMEStaRowVT_MaxVal                                                        0xffffffff
#define cAf6_upen_xxrowvt_AMEStaRowVT_MinVal                                                               0x0
#define cAf6_upen_xxrowvt_AMEStaRowVT_RstVal                                                               0x0


/*------------------------------------------------------------------------------
Reg Name   : AME VT Status table
Reg Addr   : 0x0F_2400
Reg Formula: 0x0F_2400 + $sd_table*0x8000 + $sts_id
    Where  : 
           + $sd_table(0-1): 1 -> SD table, 0 -> SF table
           + $sts_id(0-383) sts id
Reg Desc   : 
This register is used to report VT status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxstavt_Base                                                                     0x0F2400
#define cAf6Reg_upen_xxstavt(sdtable, stsid)                               (0x0F2400+(sdtable)*0x8000+(stsid))
#define cAf6Reg_upen_xxstavt_WidthVal                                                                       32
#define cAf6Reg_upen_xxstavt_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: AMEStaVT
BitField Type: RO
BitField Desc: VT Table Status bit_0: VT#0 bit_1: VT#1 ...
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_upen_xxstavt_AMEStaVT_Bit_Start                                                                 0
#define cAf6_upen_xxstavt_AMEStaVT_Bit_End                                                                  27
#define cAf6_upen_xxstavt_AMEStaVT_Mask                                                               cBit27_0
#define cAf6_upen_xxstavt_AMEStaVT_Shift                                                                     0
#define cAf6_upen_xxstavt_AMEStaVT_MaxVal                                                            0xfffffff
#define cAf6_upen_xxstavt_AMEStaVT_MinVal                                                                  0x0
#define cAf6_upen_xxstavt_AMEStaVT_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : AME VT Enable table
Reg Addr   : 0x0F_3400
Reg Formula: 0x0F_3400 + $sd_table*0x8000 + $sts_id
    Where  : 
           + $sd_table(0-1): 1 -> SD table, 0 -> SF table
           + $sts_id(0-383) sts_id
Reg Desc   : 
This register is used to enable VT table

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxcfgvt_Base                                                                     0x0F3400
#define cAf6Reg_upen_xxcfgvt(sdtable, stsid)                               (0x0F3400+(sdtable)*0x8000+(stsid))
#define cAf6Reg_upen_xxcfgvt_WidthVal                                                                       32
#define cAf6Reg_upen_xxcfgvt_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: AMEEnaVT
BitField Type: RW
BitField Desc: VT Table Enable bit_0: VT#0 bit_1: VT#1 ...
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_upen_xxcfgvt_AMEEnaVT_Bit_Start                                                                 0
#define cAf6_upen_xxcfgvt_AMEEnaVT_Bit_End                                                                  27
#define cAf6_upen_xxcfgvt_AMEEnaVT_Mask                                                               cBit27_0
#define cAf6_upen_xxcfgvt_AMEEnaVT_Shift                                                                     0
#define cAf6_upen_xxcfgvt_AMEEnaVT_MaxVal                                                            0xfffffff
#define cAf6_upen_xxcfgvt_AMEEnaVT_MinVal                                                                  0x0
#define cAf6_upen_xxcfgvt_AMEEnaVT_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : AME STS Hold Off timer
Reg Addr   : 0x0F_3800
Reg Formula: 0x0F_3800 + $sts_id
    Where  : 
           + $sts_id(0-383) sts_id
Reg Desc   : 
This register is used to config hold off timer for STS alarm, hold_off_timer = AMETmsMax * AMETmsUnit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxcfgtms_Base                                                                    0x0F3800
#define cAf6Reg_upen_xxcfgtms(stsid)                                                        (0x0F3800+(stsid))
#define cAf6Reg_upen_xxcfgtms_WidthVal                                                                      32
#define cAf6Reg_upen_xxcfgtms_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: AMETmsMax
BitField Type: RW
BitField Desc: Hold off timer
BitField Bits: [09:03]
--------------------------------------*/
#define cAf6_upen_xxcfgtms_AMETmsMax_Bit_Start                                                               3
#define cAf6_upen_xxcfgtms_AMETmsMax_Bit_End                                                                 9
#define cAf6_upen_xxcfgtms_AMETmsMax_Mask                                                              cBit9_3
#define cAf6_upen_xxcfgtms_AMETmsMax_Shift                                                                   3
#define cAf6_upen_xxcfgtms_AMETmsMax_MaxVal                                                               0x7f
#define cAf6_upen_xxcfgtms_AMETmsMax_MinVal                                                                0x0
#define cAf6_upen_xxcfgtms_AMETmsMax_RstVal                                                                0xA

/*--------------------------------------
BitField Name: AMETmsUnit
BitField Type: RW
BitField Desc: Timer Unit
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_xxcfgtms_AMETmsUnit_Bit_Start                                                              0
#define cAf6_upen_xxcfgtms_AMETmsUnit_Bit_End                                                                2
#define cAf6_upen_xxcfgtms_AMETmsUnit_Mask                                                             cBit2_0
#define cAf6_upen_xxcfgtms_AMETmsUnit_Shift                                                                  0
#define cAf6_upen_xxcfgtms_AMETmsUnit_MaxVal                                                               0x7
#define cAf6_upen_xxcfgtms_AMETmsUnit_MinVal                                                               0x0
#define cAf6_upen_xxcfgtms_AMETmsUnit_RstVal                                                               0x1


/*------------------------------------------------------------------------------
Reg Name   : AME VT Hold Off timer
Reg Addr   : 0x0F_4000
Reg Formula: 0x0F_4000 + $sts_id*0x20 + $vt_id
    Where  : 
           + $sts_id(0-383) vt_id
           + $vt_id(0-27)
Reg Desc   : 
This register is used to config hold off timer for VT alarm, hold_off_timer = AMETmvMax * AMETmvUnit

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxcfgtmv_Base                                                                    0x0F4000
#define cAf6Reg_upen_xxcfgtmv(stsid, vtid)                                      (0x0F4000+(stsid)*0x20+(vtid))
#define cAf6Reg_upen_xxcfgtmv_WidthVal                                                                      32
#define cAf6Reg_upen_xxcfgtmv_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: AMETmvMax
BitField Type: RW
BitField Desc: Hold off timer
BitField Bits: [09:03]
--------------------------------------*/
#define cAf6_upen_xxcfgtmv_AMETmvMax_Bit_Start                                                               3
#define cAf6_upen_xxcfgtmv_AMETmvMax_Bit_End                                                                 9
#define cAf6_upen_xxcfgtmv_AMETmvMax_Mask                                                              cBit9_3
#define cAf6_upen_xxcfgtmv_AMETmvMax_Shift                                                                   3
#define cAf6_upen_xxcfgtmv_AMETmvMax_MaxVal                                                               0x7f
#define cAf6_upen_xxcfgtmv_AMETmvMax_MinVal                                                                0x0
#define cAf6_upen_xxcfgtmv_AMETmvMax_RstVal                                                                0x1

/*--------------------------------------
BitField Name: AMETmvUnit
BitField Type: RW
BitField Desc: Timer Unit
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upen_xxcfgtmv_AMETmvUnit_Bit_Start                                                              0
#define cAf6_upen_xxcfgtmv_AMETmvUnit_Bit_End                                                                2
#define cAf6_upen_xxcfgtmv_AMETmvUnit_Mask                                                             cBit2_0
#define cAf6_upen_xxcfgtmv_AMETmvUnit_Shift                                                                  0
#define cAf6_upen_xxcfgtmv_AMETmvUnit_MaxVal                                                               0x7
#define cAf6_upen_xxcfgtmv_AMETmvUnit_MinVal                                                               0x0
#define cAf6_upen_xxcfgtmv_AMETmvUnit_RstVal                                                               0x3


/*------------------------------------------------------------------------------
Reg Name   : AME LO ID Lookup
Reg Addr   : 0x0F_1000
Reg Formula: 0x0F_1000 + $slice_id*0x40 + $sts_id
    Where  : 
           + $slice_id (0-7): slice id
           + $sts_id(0-47) sts_id
Reg Desc   : 
This register is used to loopkup LO {slice_id,sts_id} -> TFI-5 {slice_id,sts_id}

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxcfglkv_Base                                                                    0x0F1000
#define cAf6Reg_upen_xxcfglkv(sliceid, stsid)                                (0x0F1000+(sliceid)*0x40+(stsid))
#define cAf6Reg_upen_xxcfglkv_WidthVal                                                                      32
#define cAf6Reg_upen_xxcfglkv_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: AMELkvLine
BitField Type: RW
BitField Desc: TFI-5 Line ID
BitField Bits: [08:06]
--------------------------------------*/
#define cAf6_upen_xxcfglkv_AMELkvLine_Bit_Start                                                              6
#define cAf6_upen_xxcfglkv_AMELkvLine_Bit_End                                                                8
#define cAf6_upen_xxcfglkv_AMELkvLine_Mask                                                             cBit8_6
#define cAf6_upen_xxcfglkv_AMELkvLine_Shift                                                                  6
#define cAf6_upen_xxcfglkv_AMELkvLine_MaxVal                                                               0x7
#define cAf6_upen_xxcfglkv_AMELkvLine_MinVal                                                               0x0
#define cAf6_upen_xxcfglkv_AMELkvLine_RstVal                                                               0x0

/*--------------------------------------
BitField Name: AMELkvSts
BitField Type: RW
BitField Desc: TFI-5 STS ID
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_upen_xxcfglkv_AMELkvSts_Bit_Start                                                               0
#define cAf6_upen_xxcfglkv_AMELkvSts_Bit_End                                                                 5
#define cAf6_upen_xxcfglkv_AMELkvSts_Mask                                                              cBit5_0
#define cAf6_upen_xxcfglkv_AMELkvSts_Shift                                                                   0
#define cAf6_upen_xxcfglkv_AMELkvSts_MaxVal                                                               0x3f
#define cAf6_upen_xxcfglkv_AMELkvSts_MinVal                                                                0x0
#define cAf6_upen_xxcfglkv_AMELkvSts_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : AME HO ID Lookup
Reg Addr   : 0x0F_1800
Reg Formula: 0x0F_1800 + $slice_id*0x40 + $sts_id
    Where  : 
           + $slice_id (0-7): slice id  (also line id)
           + $sts_id(0-47) sts_id
Reg Desc   : 
This register is used to loopkup HO {slice_id,sts_id} -> TFI-5 {slice_id,sts_id}

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_xxcfglks_Base                                                                    0x0F1800
#define cAf6Reg_upen_xxcfglks(sliceid, stsid)                                (0x0F1800+(sliceid)*0x40+(stsid))
#define cAf6Reg_upen_xxcfglks_WidthVal                                                                      32
#define cAf6Reg_upen_xxcfglks_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: AMELksLine
BitField Type: RW
BitField Desc: TFI-5 Line ID
BitField Bits: [08:06]
--------------------------------------*/
#define cAf6_upen_xxcfglks_AMELksLine_Bit_Start                                                              6
#define cAf6_upen_xxcfglks_AMELksLine_Bit_End                                                                8
#define cAf6_upen_xxcfglks_AMELksLine_Mask                                                             cBit8_6
#define cAf6_upen_xxcfglks_AMELksLine_Shift                                                                  6
#define cAf6_upen_xxcfglks_AMELksLine_MaxVal                                                               0x7
#define cAf6_upen_xxcfglks_AMELksLine_MinVal                                                               0x0
#define cAf6_upen_xxcfglks_AMELksLine_RstVal                                                               0x0

/*--------------------------------------
BitField Name: AMELksSts
BitField Type: RW
BitField Desc: TFI-5 STS ID
BitField Bits: [05:00]
--------------------------------------*/
#define cAf6_upen_xxcfglks_AMELksSts_Bit_Start                                                               0
#define cAf6_upen_xxcfglks_AMELksSts_Bit_End                                                                 5
#define cAf6_upen_xxcfglks_AMELksSts_Mask                                                              cBit5_0
#define cAf6_upen_xxcfglks_AMELksSts_Shift                                                                   0
#define cAf6_upen_xxcfglks_AMELksSts_MaxVal                                                               0x3f
#define cAf6_upen_xxcfglks_AMELksSts_MinVal                                                                0x0
#define cAf6_upen_xxcfglks_AMELksSts_RstVal                                                                0x0

#endif /* _AF6_REG_AF6CCI0051_RD_POH_BER_H_ */

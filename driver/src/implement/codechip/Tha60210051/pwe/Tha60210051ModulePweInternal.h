/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60210051ModulePweInternal.h
 * 
 * Created Date: Feb 29, 2016
 *
 * Description : 60210051 module PWE internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULEPWEINTERNAL_H_
#define _THA60210051MODULEPWEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/pwe/Tha60210011ModulePweInternal.h"
#include "Tha60210051ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cPweNumLongRegisterAccess 4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051ModulePwe * Tha60210051ModulePwe;
typedef struct tTha60210051ModulePweMethods
    {
    uint32* (*PlaHoldRegistersGet)(Tha60210051ModulePwe self, uint16 *numberOfHoldRegisters);
    eBool   (*NewPsnIsSupported)(Tha60210051ModulePwe self);
    eBool   (*ShouldReactivateWhenCacheInUnsafe)(Tha60210051ModulePwe self);
    void (*PlaCacheShow)(Tha60210051ModulePwe self);
    }tTha60210051ModulePweMethods;

typedef struct tTha60210051ModulePwe
    {
    tTha60210011ModulePwe super;
    const tTha60210051ModulePweMethods * methods;

    AtLongRegisterAccess pweLongRegisterAccess[cPweNumLongRegisterAccess];
    AtLongRegisterAccess plaLongRegisterAccess;
    }tTha60210051ModulePwe;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210051ModulePweObjectInit(AtModule self, AtDevice device);
#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULEPWEINTERNAL_H_ */


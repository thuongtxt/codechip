/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60210051ModulePwe.c
 *
 * Created Date: Sep 29, 2015
 *
 * Description : Module PWE of 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60210011/pwe/Tha60210011ModulePweInternal.h"
#include "../man/Tha60210051Device.h"
#include "Tha60210051ModulePwe.h"
#include "Tha60210051ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cPweNumHoldRegister       4

#define cAf6RegPlaNumCaches 0x488032
#define cAf6RegPlaNumBlocks 0x488031

#define cPlaMaxNum256ByteCacheTotal 1024
#define cPlaMaxNum128ByteCacheTotal 2048
#define cPlaMaxNum64ByteCacheTotal  6144
#define cPlaMaxHiBlockTotal         16384
#define cPlaMaxLoBlockTotal         32768

#define cPlaMaxNum256ByteCachToReactivate  (cPlaMaxNum256ByteCacheTotal / 2)
#define cPlaMaxNum128ByteCachToReactivate  (cPlaMaxNum128ByteCacheTotal / 2)
#define cPlaMaxNum64ByteCatchToReactivate  (cPlaMaxNum64ByteCacheTotal / 2)
#define cPlaMaxHiBlockToReactivate         (cPlaMaxHiBlockTotal / 2)
#define cPlaMaxLoBlockToReactivate         (cPlaMaxLoBlockTotal / 2)

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210051ModulePwe)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210051ModulePweMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tAtObjectMethods             m_AtObjectOverride;
static tThaModulePweMethods         m_ThaModulePweOverride;
static tTha60210011ModulePweMethods m_Tha60210011ModulePweOverride;

/* Save super implementations */
static const tAtObjectMethods             *m_AtObjectMethods             = NULL;
static const tAtModuleMethods             *m_AtModuleMethods             = NULL;
static const tThaModulePweMethods         *m_ThaModulePweMethods         = NULL;
static const tTha60210011ModulePweMethods *m_Tha60210011ModulePweMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *PlaHoldRegistersGet(Tha60210051ModulePwe self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x48C000, 0x48C001, 0x48C002};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    return mMethodsGet(mThis(self))->PlaHoldRegistersGet(mThis(self), numberOfHoldRegisters);
    }

static AtLongRegisterAccess PlaLongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = mMethodsGet(mThis(self))->PlaHoldRegistersGet(mThis(self), &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static uint32 *PweHoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters, uint8 pweIndex)
    {
    static uint32 holdRegisters[] = {0x7C00A, 0x7C00B, 0x7C00C,
                                     0x8C00A, 0x8C00B, 0x8C00C,
                                     0x9C00A, 0x9C00B, 0x9C00C,
                                     0xAC00A, 0xAC00B, 0xAC00C};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = cPweNumHoldRegister;

    return holdRegisters + pweIndex * cPweNumHoldRegister;
    }

static AtLongRegisterAccess PweLongRegisterAccessCreate(AtModule self, uint8 pweIndex)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = PweHoldRegistersGet(self, &numHoldRegisters, pweIndex);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static AtLongRegisterAccess PweLongRegisterAccess(AtModule self, uint8 pweIndex)
    {
    if (mThis(self)->pweLongRegisterAccess[pweIndex])
        return mThis(self)->pweLongRegisterAccess[pweIndex];

    mThis(self)->pweLongRegisterAccess[pweIndex] = PweLongRegisterAccessCreate(self, pweIndex);
    return mThis(self)->pweLongRegisterAccess[pweIndex];
    }

static AtLongRegisterAccess PlaLongRegisterAccess(AtModule self)
    {
    if (mThis(self)->plaLongRegisterAccess)
        return mThis(self)->plaLongRegisterAccess;

    mThis(self)->plaLongRegisterAccess = PlaLongRegisterAccessCreate(self);
    return mThis(self)->plaLongRegisterAccess;
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    uint8 pweIndex;

    if (Tha60210051DeviceUseGlobalHold((Tha60210051Device)AtModuleDeviceGet(self)))
        return m_AtModuleMethods->LongRegisterAccess(self, localAddress);

    if (Tha60210051ModulePweAddressBelongToPlaPart(localAddress, &pweIndex))
        return PlaLongRegisterAccess(self);

    return PweLongRegisterAccess(self, pweIndex);
    }

static void DeleteLongRegAccess(AtLongRegisterAccess *access)
    {
    AtObjectDelete((AtObject)*access);
    *access = NULL;
    }

static void Delete(AtObject self)
    {
    uint8 i;

    DeleteLongRegAccess(&(mThis(self)->plaLongRegisterAccess));

    for (i = 0; i < cPweNumLongRegisterAccess; i++)
        DeleteLongRegAccess(&(mThis(self)->pweLongRegisterAccess[i]));

    /* Call super to fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static uint32 CounterOffset(AtPw pw)
    {
    uint32 pwId = AtChannelIdGet((AtChannel)pw);
    return ((pwId >> 11) * 0x8000) + (pwId & cBit10_0);
    }

static eBool NewPsnIsSupported(Tha60210051ModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60210051DeviceStartVersionSupportNewPsn(device))
        return cAtTrue;

    return cAtFalse;
    }

/*
 * Format of PSN buffer configured to HW: <UDPCHKSUM[31:0]> <IPv4CHKSUM[31:0]> < unused [31:7]> <rtppt[6:0]> <rtpssrc[31:0]> <PSN>
 *                                        |________8 bytes____________________|________8 bytes______________________________|
 */
static uint8 NumberOfAddtionalByte(Tha60210011ModulePwe self, AtPw adapter)
    {
    if (!mMethodsGet(mThis(self))->NewPsnIsSupported(mThis(self)))
        return m_Tha60210011ModulePweMethods->NumberOfAddtionalByte(self, adapter);

    return 16;
    }

static AtPwUdpPsn UdpPsnGet(AtPwPsn psn)
    {
    if (psn == NULL)
        return NULL;

    if (AtPwPsnTypeGet(psn) == cAtPwPsnTypeUdp)
        return (AtPwUdpPsn)psn;

    return UdpPsnGet(AtPwPsnLowerPsnGet(psn));
    }

static AtPwIpV4Psn IpV4PsnGet(AtPwPsn psn)
    {
    if (psn == NULL)
        return NULL;

    if (AtPwPsnTypeGet(psn) == cAtPwPsnTypeIPv4)
        return (AtPwIpV4Psn)psn;

    return IpV4PsnGet(AtPwPsnLowerPsnGet(psn));
    }

static uint32 UdpChecksum(AtPwPsn psn)
    {
    return AtPwUdpPsnCheckSumCalculate(UdpPsnGet(psn));
    }

static uint32 IpV4Checksum(AtPwPsn psn)
    {
    return AtPwIpV4PsnCheckSumCalculate(IpV4PsnGet(psn));
    }

/*
 * Format of PSN buffer configured to HW: <UDPCHKSUM[31:0]> <IPv4CHKSUM[31:0]> < unused [31:7]> <rtppt[6:0]> <rtpssrc[31:0]> <PSN>
 *                                        |________8 bytes____________________|________8 bytes______________________________|
 */
static uint8 PutDataToFirstSegment(Tha60210011ModulePwe self, AtPw pw, AtPwPsn psn, uint8 *dataBuffer, uint32* firstSegment, eThaPwAdapterHeaderWriteMode writeMode)
    {
    if (!mMethodsGet(mThis(self))->NewPsnIsSupported(mThis(self)))
        return m_Tha60210011ModulePweMethods->PutDataToFirstSegment(self, pw, psn, dataBuffer, firstSegment, writeMode);

    if (writeMode == cThaPwAdapterHeaderWriteModeDefault)
        {
        firstSegment[3] = UdpChecksum(psn);
        firstSegment[2] = IpV4Checksum(psn);
        firstSegment[1] = (AtPwRtpTxPayloadTypeGet(pw) & cBit6_0);
        firstSegment[0] = AtPwRtpTxSsrcGet(pw);
        }

    /* No data has been written, first segment is fully used for special purpose */
    return 0;
    }

static eBool IsSimulated(ThaModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return AtDeviceIsSimulated(device);
    }

static eBool ShouldFixedSleep(void)
    {
    return cAtFalse;
    }

static eAtRet PwRemovingStart(ThaModulePwe self, AtPw pw)
    {
    eAtRet ret = m_ThaModulePweMethods->PwRemovingStart(self, pw);

    if (!IsSimulated(self))
        if (ShouldFixedSleep())
            AtOsalUSleep(2000);

    return ret;
    }

static void CacheUsageGet(ThaModulePwe self, tThaPweCache *cache)
    {
    uint32 longReg[cThaNumDwordsInLongReg];

    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), 0);
    mModuleHwLongRead(self, cAf6RegPlaNumCaches, longReg, cThaNumDwordsInLongReg, core);
    cache->free256ByteCache = longReg[1]  & cBit15_0;
    cache->free128ByteCache = (longReg[0] & cBit31_16) >> 16;
    cache->free64ByteCache  = longReg[0]  & cBit15_0;

    mModuleHwLongRead(self, cAf6RegPlaNumBlocks, longReg, cThaNumDwordsInLongReg, core);
    cache->freeNumHiBlock = (longReg[0] & cBit31_16) >> 16;
    cache->freeNumLoBlock = longReg[0] & cBit15_0;

    cache->used256ByteCache = cPlaMaxNum256ByteCacheTotal - cache->free256ByteCache;
    cache->used128ByteCache = cPlaMaxNum128ByteCacheTotal - cache->free128ByteCache;
    cache->used64ByteCache  = cPlaMaxNum64ByteCacheTotal - cache->free64ByteCache;
    cache->usedNumHiBlock = cPlaMaxHiBlockTotal - cache->freeNumHiBlock;
    cache->usedNumLoBlock = cPlaMaxLoBlockTotal - cache->freeNumLoBlock;

    /* Not supported cache types must be set to invalid value to let the outside know */
    cache->free512ByteCache = cInvalidUint32;
    cache->used512ByteCache = cInvalidUint32;
    }

static eBool CanAccessCacheUsage(ThaModulePwe self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool IsRamCacheUnderThreshold(AtModule self)
    {
    tThaPweCache cache;

    ThaModulePweCacheUsageGet((ThaModulePwe)self, &cache);

    if (cache.free256ByteCache <= cPlaMaxNum256ByteCachToReactivate)
        return cAtTrue;
    if (cache.free128ByteCache <= cPlaMaxNum128ByteCachToReactivate)
        return cAtTrue;
    if (cache.free64ByteCache <= cPlaMaxNum64ByteCatchToReactivate)
        return cAtTrue;
    if (cache.freeNumHiBlock <= cPlaMaxHiBlockToReactivate)
        return cAtTrue;
    if (cache.freeNumLoBlock <= cPlaMaxLoBlockToReactivate)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet HwResourceCheck(AtModule self)
    {
    tThaPweCache cache;
    AtDevice device = AtModuleDeviceGet(self);

    if (AtDeviceIsSimulated(device) || AtDeviceInAccessible(device))
        return cAtOk;

    ThaModulePweCacheUsageGet((ThaModulePwe)self, &cache);
    AtModuleResourceDiminishedLog(self, "256bytes caches", cache.free256ByteCache, cPlaMaxNum256ByteCacheTotal);
    AtModuleResourceDiminishedLog(self, "128bytes caches", cache.free128ByteCache, cPlaMaxNum128ByteCacheTotal);
    AtModuleResourceDiminishedLog(self, "64bytes caches", cache.free64ByteCache, cPlaMaxNum64ByteCacheTotal);
    AtModuleResourceDiminishedLog(self, "Hi blocks", cache.freeNumHiBlock, cPlaMaxHiBlockTotal);
    AtModuleResourceDiminishedLog(self, "Lo blocks", cache.freeNumLoBlock, cPlaMaxLoBlockTotal);

    return cAtOk;
    }

static eBool ShouldReactivateWhenCacheInUnsafe(Tha60210051ModulePwe self)
    {
    if (!AtModuleIsActive((AtModule)self))
        return cAtFalse;

    /* To quickly resolve when re-activate logic cause any critical affect. */
    return cAtTrue;
    }

static void RamCacheLog(AtModule self)
    {
    tThaPweCache cache;

    if (IsSimulated((ThaModulePwe)self))
        return;

    ThaModulePweCacheUsageGet((ThaModulePwe)self, &cache);
    AtModuleLog(self, cAtLogLevelWarning, AtSourceLocation,
                "Reactivate because of RAM cache is not safe: cache256Byte = %d, cache128Byte = %d, cache64Byte = %d, hiBlock = %d, loBlock = %d\r\n",
                cache.free256ByteCache, cache.free128ByteCache, cache.free64ByteCache, cache.freeNumHiBlock, cache.freeNumLoBlock);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);

    if (mMethodsGet(mThis(self))->ShouldReactivateWhenCacheInUnsafe(mThis(self)) &&
        ThaModulePweCanAccessCacheUsage((ThaModulePwe)self) &&
        IsRamCacheUnderThreshold(self))
        {
        RamCacheLog(self);
        AtModuleReactivate(self);
        }

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void ShowCache(const char *title, uint32 cacheNumber)
    {
    if (cacheNumber == cInvalidUint32)
        AtPrintc(cSevDebug, "%s: N/S\n", title);
    else
        AtPrintc(cSevDebug, "%s: %u\n", title, cacheNumber);
    }

static eAtRet Debug(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Debug(self);

    mMethodsGet(mThis(self))->PlaCacheShow(mThis(self));

    return ret;
    }

static void PlaCacheShow(Tha60210051ModulePwe self)
    {
    tThaPweCache cache;
    ThaModulePweCacheUsageGet((ThaModulePwe)self, &cache);

    ShowCache("Number of 512byte cache", cache.free512ByteCache);
    ShowCache("Number of 256byte cache", cache.free256ByteCache);
    ShowCache("Number of 128byte cache", cache.free128ByteCache);
    ShowCache("Number of 64byte cache", cache.free64ByteCache);
    ShowCache("Number of Hi block cache", cache.freeNumHiBlock);
    ShowCache("Number of Lo block cache", cache.freeNumLoBlock);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60210051ModulePwe *object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjects(pweLongRegisterAccess, cPweNumLongRegisterAccess);
    mEncodeObject(plaLongRegisterAccess);
    }

static void DebuggerEntriesFill(AtModule self, AtDebugger debugger)
    {
    tThaPweCache cache;
    ThaModulePweCacheUsageGet((ThaModulePwe)self, &cache);

    m_AtModuleMethods->DebuggerEntriesFill(self, debugger);

    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoResourceNew("256byte cache", cPlaMaxNum256ByteCacheTotal, cache.free256ByteCache));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoResourceNew("128byte cache", cPlaMaxNum128ByteCacheTotal, cache.free128ByteCache));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoResourceNew("64byte cache", cPlaMaxNum64ByteCacheTotal, cache.free64ByteCache));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoResourceNew("Hi block", cPlaMaxHiBlockTotal, cache.freeNumHiBlock));
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoResourceNew("Lo block", cPlaMaxLoBlockTotal, cache.freeNumLoBlock));
    }

static void OverrideTha60210011ModulePwe(AtModule self)
    {
    Tha60210011ModulePwe modulePwe = (Tha60210011ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePweMethods = mMethodsGet(modulePwe);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePweOverride, m_Tha60210011ModulePweMethods, sizeof(m_Tha60210011ModulePweOverride));
        mMethodOverride(m_Tha60210011ModulePweOverride, PutDataToFirstSegment);
        mMethodOverride(m_Tha60210011ModulePweOverride, NumberOfAddtionalByte);
        }

    mMethodsSet(modulePwe, &m_Tha60210011ModulePweOverride);
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, m_ThaModulePweMethods, sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, CounterOffset);
        mMethodOverride(m_ThaModulePweOverride, PwRemovingStart);
        mMethodOverride(m_ThaModulePweOverride, CacheUsageGet);
        mMethodOverride(m_ThaModulePweOverride, CanAccessCacheUsage);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, HwResourceCheck);
        mMethodOverride(m_AtModuleOverride, DebuggerEntriesFill);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideTha60210011ModulePwe(self);
    OverrideThaModulePwe(self);
    }

static void MethodsInit(Tha60210051ModulePwe self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, PlaHoldRegistersGet);
        mMethodOverride(m_methods, NewPsnIsSupported);
        mMethodOverride(m_methods, ShouldReactivateWhenCacheInUnsafe);
        mMethodOverride(m_methods, PlaCacheShow);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ModulePwe);
    }

AtModule Tha60210051ModulePweObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210051ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051ModulePweObjectInit(newModule, device);
    }

eBool Tha60210051ModulePweAddressBelongToPlaPart(uint32 localAddress, uint8 *pweIndex)
    {
    uint32 baseAddress = localAddress & cBit23_20;

    if (baseAddress == 0x400000)
        return cAtTrue;

    baseAddress = localAddress & cBit19_16;
    *pweIndex = (uint8)((baseAddress - 0x70000) >> 16);
    if (*pweIndex >= cPweNumLongRegisterAccess)  /* Out of range, force to use PLA */
        return cAtTrue;

    return cAtFalse;
    }   
    

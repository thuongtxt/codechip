/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      :  
 * 
 * File        : Tha60210051ModulePdaInternal.h
 * 
 * Created Date: Mar 23, 2016
 *
 * Description : 60210051 module PDA internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/
 
#ifndef _THA60210051MODULEPDAINTERNAL_H_
#define _THA60210051MODULEPDAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/pda/Tha60210011ModulePdaInternal.h"
#include "Tha60210051ModulePda.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cHwBlockCheckReg         0x500007
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051ModulePda * Tha60210051ModulePda;

typedef struct tTha60210051ModulePdaMethods
    {
    eBool (*HwValueUnit1PacketIsSupported)(Tha60210051ModulePda self);
    eBool (*HwPwIdCacheIsSupported)(Tha60210051ModulePda self);
    }tTha60210051ModulePdaMethods;

typedef struct tTha60210051ModulePda
    {
    tTha60210011ModulePda super;
    const tTha60210051ModulePdaMethods *methods;
    }tTha60210051ModulePda;

/*--------------------------- Forward declarations ---------------------------*/
AtModule Tha60210051ModulePdaObjectInit(AtModule self, AtDevice device);

/*--------------------------- Entries ----------------------------------------*/
 
#ifdef __cplusplus
}
#endif

#endif /* _THA60210051MODULEPDAINTERNAL_H_ */

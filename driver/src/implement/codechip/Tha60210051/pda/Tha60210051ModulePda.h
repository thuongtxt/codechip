/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60210051ModulePda.h
 * 
 * Created Date: Jan 15, 2016
 *
 * Description : PDA module header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULEPDA_H_
#define _THA60210051MODULEPDA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/pda/ThaStmPwProductModulePda.h"
#include "../../Tha60210011/pda/Tha60210011ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint16 Tha6021ModulePdaCESoPMinPayloadSize(ThaModulePda self, AtPw pw);
uint32 Tha6021ModulePdaMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
uint32 Tha6021ModulePdaCEPMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
uint16 Tha6021ModulePdaNumberOfPacketByUs(ThaModulePda self, AtPw pw, uint32 numUs);
uint16 Tha6021ModulePdaCESoPMaxNumFrames(ThaModulePda self, AtPw pw);
eBool Tha6021ModulePdaJitterBufferAndJitterDelayAreValid(ThaModulePda self, AtPw pw, uint32 jitterBufferUs, uint32 jitterDelayUs);
uint32 Tha6021ModulePdaMinNumPacketsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize);
uint32 Tha6021ModulePdaMaxJitterDelayForJitterBuffer(ThaModulePda self, AtPw pw, uint32 jitterBufferSize_us, uint32 extraJitter_us);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULEPDA_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60210051ModulePda.c
 *
 * Created Date: Sep 29, 2015
 *
 * Description : Module PDA of 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pw/ThaPwUtil.h"
#include "../../Tha60210011/pda/Tha60210011ModulePdaInternal.h"
#include "../../Tha60210011/pw/Tha60210011ModulePw.h"
#include "../man/Tha60210051Device.h"
#include "Tha60210051ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cJitBufNumPkMask         cBit19_4
#define cJitBufNumPkShift        4

#define cHwPwLocalIdCache        0x5C0000
#define cHwPwLocalIdMask         cBit9_0
#define cHwPwLocalIdShift        0
#define cHwPwLocalSliceOc24Mask  cBit10
#define cHwPwLocalSliceOc24Shift 10
#define cHwPwLocalSliceOc48Mask  cBit12_11
#define cHwPwLocalSliceOc48Shift 11
#define cHwPwLocalIdIsHoMask     cBit13
#define cHwPwLocalIdIsHoShift    13

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210051ModulePda)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210051ModulePdaMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModulePdaMethods         m_ThaModulePdaOverride;
static tThaModulePdaV2Methods       m_ThaModulePdaV2Override;
static tTha60210011ModulePdaMethods m_Tha60210011ModulePdaOverride;

/* Save super implementations */
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tThaModulePdaMethods *m_ThaModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x500000, 0x500001, 0x500002};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters;

    if (Tha60210051DeviceUseGlobalHold((Tha60210051Device)AtModuleDeviceGet(self)))
        return m_AtModuleMethods->LongRegisterAccessCreate(self);

    holdRegisters = AtModuleHoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static uint32 MaxNumJitterBufferBlocks(ThaModulePda self)
    {
    AtUnused(self);
    return 524288;
    }

static uint32 JitterBufferBlockSizeInByte(ThaModulePda self)
    {
    AtUnused(self);
    return 1024;
    }

static uint32 HotConfigureEngineStatus(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return 0x500011;
    }

static uint32 HotConfigureEngineControl(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return 0x500010;
    }

static uint32 HotConfigureEngineRequestInfoPwIdMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cBit13_1;
    }

static uint8 HotConfigureEngineRequestInfoPwIdShift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 CounterOffset(ThaModulePda self, AtPw pw)
    {
    uint32 pwId = AtChannelIdGet((AtChannel)pw);
    AtUnused(self);
    return ((pwId >> 11) * 0x10000) + (pwId & cBit10_0);
    }

static uint32 StartVersionSupportJitterCentering(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x10, 0x30, 0x30);
    }

static eBool JitterBufferCenterIsSupported(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= StartVersionSupportJitterCentering(device))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 HotConfigureEnginePwId(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);
    return AtChannelIdGet((AtChannel)pw);
    }

static uint16 CEPMtu(ThaModulePda self, AtPw pw)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);
    AtUnused(self);

    if ((channelType == cAtSdhChannelTypeVc11) ||
        (channelType == cAtSdhChannelTypeVc12) ||
        (channelType == cAtSdhChannelTypeVc3))
        return 2044;

    /* When pw has not been bound circuit, return maximum value that PDA accept
     * error will be returned when PW is activated with specific circuit which does not accept a value of payload size */
    return 4092;
    }

static uint16 MaxPayloadSize(uint16 mtu, AtPw pw)
    {
    uint32 totalHeaderLength = ThaPwAdapterTotalHeaderLengthGet(pw);
    if (mtu < totalHeaderLength)
        return 0;

    return (uint16)(mtu - totalHeaderLength);
    }

static uint16 CEPMaxPayloadSize(ThaModulePda self, AtPw pw)
    {
    return MaxPayloadSize(CEPMtu(self, pw), pw);
    }

static uint16 SAToPMaxPayloadSize(ThaModulePda self, AtPw pw)
    {
    const uint16 cSAToPMtu = 2048;
    AtUnused(self);

    return MaxPayloadSize(cSAToPMtu, pw);
    }

static uint16 SAToPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)AtPwBoundCircuitGet(pw);
    eAtPdhChannelType channelType = (pdhChannel) ? AtPdhChannelTypeGet(pdhChannel) : cAtPdhChannelTypeUnknown;
    AtUnused(self);

    if (channelType == cAtPdhChannelTypeE1)
        return 128;

    if ((channelType == cAtPdhChannelTypeDs3) ||
        (channelType == cAtPdhChannelTypeE3))
        return 256;

    /* When pw has not been bound circuit, return minimum value that PDA accept
     * error will be returned when PW is activated with specific circuit which does not accept a value of payload size */
    return 96;
    }

static uint16 CEPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);
    AtUnused(self);

    if ((channelType == cAtSdhChannelTypeVc11) ||
        (channelType == cAtSdhChannelTypeUnknown))
        return 104;

    if (channelType == cAtSdhChannelTypeVc12)
        return 140;

    if (channelType == cAtSdhChannelTypeVc3)
        return 256;

    return 783;
    }

static uint16 CESoPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    return Tha6021ModulePdaCESoPMinPayloadSize(self, pw);
    }

static uint32 MinNumPacketsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    AtUnused(payloadSize);
    AtUnused(pw);
    AtUnused(self);
    return 5; /* as E3 SAToP */
    }

static uint32 MinNumPacketsForJitterDelay(ThaModulePda self, AtPw pw)
    {
    AtUnused(pw);
    AtUnused(self);
    return 3;
    }

static uint32 CEPMinNumPacketsForJitterDelay(ThaModulePda self, AtPw pw)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    uint8 channelType = AtSdhChannelTypeGet(vc);
    AtUnused(self);

    switch (channelType)
        {
        case cAtSdhChannelTypeVc4_64c:  return 48;
        case cAtSdhChannelTypeVc4_16c:  return 24;
        case cAtSdhChannelTypeVc4_4c:   return 6;
        case cAtSdhChannelTypeVc4:      return 3;
        case cAtSdhChannelTypeVc3:      return 3;
        case cAtSdhChannelTypeVc12:     return 3;
        case cAtSdhChannelTypeVc11:     return 3;
        default:                        return 3;
        }
    }

static uint32 MinNumMicrosecondsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    AtUnused(payloadSize);
    AtUnused(pw);
    AtUnused(self);
    /* Lowest jitter buffer size is applicable for VC4-64c */
    return 64;
    }

static uint32 MaxNumMicrosecondsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    uint8 numSts;
    eAtSdhChannelType channelType;
    AtSdhChannel circuit = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    AtUnused(payloadSize);
    AtUnused(self);

    /* If do not have circuit information,
     * just simply return maximum value */
    if (circuit == NULL)
        return 256000;

    if (AtPwTypeGet(pw) != cAtPwTypeCEP)
        return 256000;

    channelType = AtSdhChannelTypeGet(circuit);
    numSts = AtSdhChannelNumSts(circuit);
    if (channelType == cAtSdhChannelTypeVc4_4c)
        return 128000;

    if (channelType == cAtSdhChannelTypeVc4_nc)
        return (numSts < 48) ? 128000 : 64000;

    if (channelType == cAtSdhChannelTypeVc4_16c)
        return 64000;

    if (channelType == cAtSdhChannelTypeVc4_64c)
        return 5000;

    return 256000;
    }

static uint32 StartVersionSupportHwValueUnit1Packet(AtDevice device)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x12, 0x28, 0x41);
    }

static eBool HwValueUnit1PacketIsSupported(Tha60210051ModulePda self)
    {
    AtDevice device = (AtDevice)AtModuleDeviceGet((AtModule)self);
    if (AtDeviceVersionNumber(device) >= StartVersionSupportHwValueUnit1Packet(device))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 NumCurrentPacketsInJitterBuffer(ThaModulePda self, AtPw pwAdapter)
    {
    ThaModulePdaV2 modulePda = (ThaModulePdaV2)self;
    Tha60210051ModulePda module = (Tha60210051ModulePda)self;
    uint32 regAddr, regVal;

    if (!mMethodsGet(module)->HwValueUnit1PacketIsSupported(module))
        return m_ThaModulePdaMethods->NumCurrentPacketsInJitterBuffer(self, pwAdapter);

    regAddr = mMethodsGet(modulePda)->PDATdmJitBufStat(modulePda) + mPwOffset(self, pwAdapter);
    regVal  = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);

    return mRegField(regVal, cJitBufNumPk);
    }

static uint32 NumCurrentAdditionalBytesInJitterBuffer(ThaModulePda self, AtPw pwAdapter)
    {
    Tha60210051ModulePda module = (Tha60210051ModulePda)self;
    if (!mMethodsGet(module)->HwValueUnit1PacketIsSupported(module))
        return m_ThaModulePdaMethods->NumCurrentAdditionalBytesInJitterBuffer(self, pwAdapter);

    return 0;
    }

static uint32 StartVersionSupportHwPwIdCache(AtDevice device)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x16, 0x03, 0x17, 0x55);
    }

static uint32 StartBuiltNumberSupportHwPwIdCache(AtDevice device)
    {
    AtUnused(device);
    return ThaVersionReaderHardwareBuiltNumberBuild(0x1, 0x0, 0x16);
    }

static eBool HwPwIdCacheIsSupported(Tha60210051ModulePda self)
    {
    AtDevice device = (AtDevice)AtModuleDeviceGet((AtModule)self);

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) > StartVersionSupportHwPwIdCache(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) == StartVersionSupportHwPwIdCache(device))
        return (AtDeviceBuiltNumber(device) >= StartBuiltNumberSupportHwPwIdCache(device)) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static uint32 LoSliceHwPwIdGet(Tha60210011ModulePda self, ThaPwAdapter adapter, uint8 loSlice)
    {
    uint32 regVal;
    uint8 oc48Slice, oc24Slice;

    if (!mMethodsGet(mThis(self))->HwPwIdCacheIsSupported(mThis(self)))
        return cThaMapInvalidPwChannelId;

    oc48Slice = loSlice / 2;
    oc24Slice = loSlice % 2;
    regVal = mChannelHwRead(adapter, cHwPwLocalIdCache + mPwOffset(self, adapter), cThaModulePda);
    if ((oc48Slice != mRegField(regVal, cHwPwLocalSliceOc48)) ||
        (oc24Slice != mRegField(regVal, cHwPwLocalSliceOc24)) ||
        (mRegField(regVal, cHwPwLocalIdIsHo) != 0))
        {
        AtChannelLog((AtChannel)adapter, cAtLogLevelCritical, AtSourceLocation, "Could not find HW local ID\r\n");
        return cThaMapInvalidPwChannelId;
        }

    return mRegField(regVal, cHwPwLocalId);
    }

static void LoSliceHwPwIdSet(Tha60210011ModulePda self, ThaPwAdapter adapter, uint8 loSlice, uint32 hwId)
    {
    uint32 regVal = 0;
    uint8 oc48Slice, oc24Slice;

    if (!mMethodsGet(mThis(self))->HwPwIdCacheIsSupported(mThis(self)))
        return;

    oc48Slice = loSlice / 2;
    oc24Slice = loSlice % 2;

    mRegFieldSet(regVal, cHwPwLocalSliceOc48, oc48Slice);
    mRegFieldSet(regVal, cHwPwLocalSliceOc24, oc24Slice);
    mRegFieldSet(regVal, cHwPwLocalId, hwId);

    mChannelHwWrite(adapter, cHwPwLocalIdCache + mPwOffset(self, adapter), regVal, cThaModulePda);
    }

static void OneRegShow(AtPw pw, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay((AtChannel)pw, address, cThaModulePda);
    }

static void PwRegsShow(ThaModulePda self, AtPw pw)
    {
    m_ThaModulePdaMethods->PwRegsShow(self, pw);

    if (Tha60210011PwCircuitBelongsToLoLine(pw) && mMethodsGet(mThis(self))->HwPwIdCacheIsSupported(mThis(self)))
        OneRegShow(pw, "Lo slice local ID lookup control", cHwPwLocalIdCache + mPwOffset(self, pw));
    }

static uint32 NumFreeBlocksHwGet(ThaModulePda self)
    {
    return mModuleHwRead(self, cHwBlockCheckReg) & cBit19_0;
    }

static uint32 MinJitterDelayFromMinNumPackets(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize, uint32 minNumPackets)
    {
    uint32 payloadSizeInBytes = ThaPwUtilPayloadSizeInBytes(pw, circuit, payloadSize);
    uint32 rateInBytePerMs = ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, circuit);
    uint32 minDelayUs = ThaPwUtilTimeInUsForNumPackets(minNumPackets, rateInBytePerMs, payloadSizeInBytes);
    AtUnused(self);

    return minDelayUs;
    }

static uint32 MinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    return Tha6021ModulePdaMinJitterDelay(self, pw, circuit, payloadSize);
    }

static uint32 CEPMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    return Tha6021ModulePdaCEPMinJitterDelay(self, pw, circuit, payloadSize);
    }

static uint32 MinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    return MinJitterDelay(self, pw, circuit, payloadSize) * 2U;
    }

static uint32 CEPMinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    return CEPMinJitterDelay(self, pw, circuit, payloadSize) * 2U;
    }

static uint16 NumberOfPacketByUs(ThaModulePda self, AtPw pw, uint32 numUs)
    {
    return Tha6021ModulePdaNumberOfPacketByUs(self, pw, numUs);
    }

static uint32 ExtraJitterBuffer(AtPw pw)
    {
    /* This products does not requires extra jitter buffer for CEP/SAToP */
    if (AtPwTypeGet(pw) != cAtPwTypeCESoP)
        return 0;

    return ThaPwAdapterExtraJitterBufferGet((ThaPwAdapter)pw);
    }

static eBool PwJitterBufferSizeIsValid(ThaModulePda self, AtPw pw, uint32 jitterBufferUs)
    {
    uint32 jitterDelayUs = AtPwJitterBufferDelayGet(pw);
    AtUnused(self);

    /* New formula makes constrain on the value of jitter buffer size, which is
     * at least twice jitter delay. */
    if ((jitterDelayUs * 2U + ExtraJitterBuffer(pw)) > jitterBufferUs)
        return cAtFalse;

    return cAtTrue;
    }

static eBool PwJitterDelayIsValid(ThaModulePda self, AtPw pw, uint32 jitterDelayUs)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(jitterDelayUs);
    /* New formula does not make constrain on the value of jitter delay. */
    return cAtTrue;
    }

static eBool PwJitterBufferAndJitterDelayAreValid(ThaModulePda self, AtPw pw, uint32 jitterBufferUs, uint32 jitterDelayUs)
    {
    return Tha6021ModulePdaJitterBufferAndJitterDelayAreValid(self, pw, jitterBufferUs, jitterDelayUs);
    }

static eBool ShouldSetDefaultJitterBufferSizeOnJitterDelaySet(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtTrue;
    }

static uint32 CESoPMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSizeInFrames)
    {
    return m_ThaModulePdaMethods->CESoPMinJitterDelay(self, pw, circuit, payloadSizeInFrames);
    }

static uint32 CESoPMinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSizeInFrames)
    {
    return CESoPMinJitterDelay(self, pw, circuit, payloadSizeInFrames) * 2U + ExtraJitterBuffer(pw);
    }

static uint32 DefaultJitterBufferSize(ThaModulePda self, AtPw pw, uint32 jitterDelayUs)
    {
    AtUnused(self);
    return jitterDelayUs * 2U + ExtraJitterBuffer(pw);
    }

static uint16 CESoPMaxNumFrames(ThaModulePda self, AtPw pw)
    {
    return Tha6021ModulePdaCESoPMaxNumFrames(self, pw);
    }

static uint16 CESoPMinNumFrames(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 4U;
    }

static uint32 MaxJitterDelayForJitterBuffer(ThaModulePda self, AtPw pw, uint32 jitterBufferSize_us)
    {
    return Tha6021ModulePdaMaxJitterDelayForJitterBuffer(self, pw, jitterBufferSize_us, ExtraJitterBuffer(pw));
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModulePdaV2(AtModule self)
    {
    ThaModulePdaV2 pdaModuleV2 = (ThaModulePdaV2)self;
    
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaV2Override, mMethodsGet(pdaModuleV2), sizeof(m_ThaModulePdaV2Override));

        mBitFieldOverride(ThaModulePdaV2, m_ThaModulePdaV2Override, HotConfigureEngineRequestInfoPwId)
        mMethodOverride(m_ThaModulePdaV2Override, HotConfigureEngineStatus);
        mMethodOverride(m_ThaModulePdaV2Override, HotConfigureEngineControl);
        mMethodOverride(m_ThaModulePdaV2Override, HotConfigureEnginePwId);
        }

    mMethodsSet(pdaModuleV2, &m_ThaModulePdaV2Override);
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, m_ThaModulePdaMethods, sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, CounterOffset);
        mMethodOverride(m_ThaModulePdaOverride, MaxNumJitterBufferBlocks);
        mMethodOverride(m_ThaModulePdaOverride, JitterBufferBlockSizeInByte);
        mMethodOverride(m_ThaModulePdaOverride, JitterBufferCenterIsSupported);
        mMethodOverride(m_ThaModulePdaOverride, CEPMaxPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, CEPMinPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, SAToPMaxPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, SAToPMinPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, CESoPMinPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, MinNumPacketsForJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, MinNumPacketsForJitterDelay);
        mMethodOverride(m_ThaModulePdaOverride, CEPMinNumPacketsForJitterDelay);
        mMethodOverride(m_ThaModulePdaOverride, MinNumMicrosecondsForJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, MaxNumMicrosecondsForJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, NumCurrentPacketsInJitterBuffer);
        mMethodOverride(m_ThaModulePdaOverride, NumCurrentAdditionalBytesInJitterBuffer);
        mMethodOverride(m_ThaModulePdaOverride, PwRegsShow);
        mMethodOverride(m_ThaModulePdaOverride, NumFreeBlocksHwGet);
        mMethodOverride(m_ThaModulePdaOverride, MinJitterDelay);
        mMethodOverride(m_ThaModulePdaOverride, CEPMinJitterDelay);
        mMethodOverride(m_ThaModulePdaOverride, MinJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, CEPMinJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, NumberOfPacketByUs);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferSizeIsValid);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterDelayIsValid);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferAndJitterDelayAreValid);
        mMethodOverride(m_ThaModulePdaOverride, ShouldSetDefaultJitterBufferSizeOnJitterDelaySet);
        mMethodOverride(m_ThaModulePdaOverride, CESoPMinJitterDelay);
        mMethodOverride(m_ThaModulePdaOverride, CESoPMinJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, DefaultJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, CESoPMaxNumFrames);
        mMethodOverride(m_ThaModulePdaOverride, CESoPMinNumFrames);
        mMethodOverride(m_ThaModulePdaOverride, MaxJitterDelayForJitterBuffer);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void OverrideTha60210011ModulePda(AtModule self)
    {
    Tha60210011ModulePda pdaModule = (Tha60210011ModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePdaOverride, mMethodsGet(pdaModule), sizeof(m_Tha60210011ModulePdaOverride));

        mMethodOverride(m_Tha60210011ModulePdaOverride, LoSliceHwPwIdGet);
        mMethodOverride(m_Tha60210011ModulePdaOverride, LoSliceHwPwIdSet);
        }

    mMethodsSet(pdaModule, &m_Tha60210011ModulePdaOverride);
    }

static void MethodsInit(AtModule self)
    {
    Tha60210051ModulePda module = (Tha60210051ModulePda)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HwValueUnit1PacketIsSupported);
        mMethodOverride(m_methods, HwPwIdCacheIsSupported);
        }

    mMethodsSet(module, &m_methods);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePda(self);
    OverrideThaModulePdaV2(self);
    OverrideTha60210011ModulePda(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ModulePda);
    }

AtModule Tha60210051ModulePdaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210051ModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051ModulePdaObjectInit(newModule, device);
    }

uint16 Tha6021ModulePdaCESoPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 8; /* bytes */
    }

uint32 Tha6021ModulePdaMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    uint32 minNumPackets = ThaModulePdaMinNumPacketsForJitterDelay(self, pw);
    return MinJitterDelayFromMinNumPackets(self, pw, circuit, payloadSize, minNumPackets);
    }

uint32 Tha6021ModulePdaCEPMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    uint32 minNumPackets = ThaModulePdaCEPMinNumPacketsForJitterDelay(self, pw);
    return MinJitterDelayFromMinNumPackets(self, pw, circuit, payloadSize, minNumPackets);
    }

uint16 Tha6021ModulePdaNumberOfPacketByUs(ThaModulePda self, AtPw pw, uint32 numUs)
    {
    uint32 rateInBytePerMs = ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, AtPwBoundCircuitGet(pw));
    uint32 maxNumPackets = mMethodsGet(self)->MaxNumPacketsForJitterBufferSize(self, pw, ThaPwAdapterPayloadSizeInBytes((ThaPwAdapter)pw));
    uint32 numPackets = ThaPwUtilNumPacketsForTimeInUsRoundUp(numUs, rateInBytePerMs, ThaPwAdapterPayloadSizeInBytes((ThaPwAdapter)pw));
    return (uint16)mMin(maxNumPackets, numPackets);
    }

uint16 Tha6021ModulePdaCESoPMaxNumFrames(ThaModulePda self, AtPw pw)
    {
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtPwBoundCircuitGet(pw);
    AtPdhDe1 de1 = AtPdhNxDS0De1Get(nxDs0);
    AtUnused(self);

    if (!ThaPwCESoPAdapterWithCas((ThaPwCESoPAdapter)pw))
        return 63U;

    if (AtPdhDe1IsE1(de1))
        return 16U;

    return 24U;
    }

eBool Tha6021ModulePdaJitterBufferAndJitterDelayAreValid(ThaModulePda self, AtPw pw, uint32 jitterBufferUs, uint32 jitterDelayUs)
    {
    if (jitterBufferUs < ThaModulePdaDefaultJitterBufferSize(self, pw, jitterDelayUs))
        return cAtFalse;

    return cAtTrue;
    }

uint32 Tha6021ModulePdaMinNumPacketsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    uint32 minJitterBufferUs = AtPwMinJitterBufferSize(pw, AtPwBoundCircuitGet(pw), (uint16)payloadSize);
    return ThaModulePdaPwNumPacketsByUsAndPayload(self, pw, minJitterBufferUs, ThaPwAdapterPayloadSizeInBytes((ThaPwAdapter)pw));
    }

uint32 Tha6021ModulePdaMaxJitterDelayForJitterBuffer(ThaModulePda self, AtPw pw, uint32 jitterBufferSize_us, uint32 extraJitter_us)
    {
    AtUnused(self);
    AtUnused(pw);
    return (jitterBufferSize_us - extraJitter_us) / 2U;
    }

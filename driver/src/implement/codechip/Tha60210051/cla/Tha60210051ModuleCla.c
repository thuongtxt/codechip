/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210051ModuleCla.c
 *
 * Created Date: Sep 29, 2015
 *
 * Description : Module CLA of 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/Tha60210051Device.h"
#include "Tha60210051ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60210051ModuleCla*)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods       m_AtModuleOverride;
static tThaModuleClaMethods   m_ThaModuleClaOverride;
static tThaModuleClaPwMethods m_ThaModuleClaPwOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x060000A, 0x060000B, 0x060000C};
    AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters;

    if (Tha60210051DeviceUseGlobalHold((Tha60210051Device)AtModuleDeviceGet(self)))
        return m_AtModuleMethods->LongRegisterAccessCreate(self);

    holdRegisters = AtModuleHoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static ThaClaEthPortController EthPortControllerCreate(ThaModuleCla self)
    {
    return Tha60210051ClaPwEthPortControllerNew(self);
    }

static uint32 PwCounterOffset(ThaModuleClaPw self, AtPw adapter)
    {
    uint32 pwId;
    AtUnused(self);

    /* See RD */
    pwId = AtChannelIdGet((AtChannel)adapter);
    return ((pwId >> 11) * 0x10000) + (pwId & 0x7FF);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, EthPortControllerCreate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void OverrideThaModuleClaPw(AtModule self)
    {
    ThaModuleClaPw claModule = (ThaModuleClaPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaPwOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaPwOverride));

        mMethodOverride(m_ThaModuleClaPwOverride, PwCounterOffset);
        }

    mMethodsSet(claModule, &m_ThaModuleClaPwOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleCla(self);
    OverrideThaModuleClaPw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ModuleCla);
    }

AtModule Tha60210051ModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60210051ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051ModuleClaObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210051ClaControllerInternal.h
 * 
 * Created Date: Mar 21, 2016
 *
 * Description : 60210051 CLA controller internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051CLACONTROLLERINTERNAL_H_
#define _THA60210051CLACONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210011/cla/controllers/Tha60210011ClaPwEthPortControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/*typedef struct tTha60210051ClaPwEthPortController
    {
    tTha60210011ClaPwEthPortController super;
    }tTha60210051ClaPwEthPortController;*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaEthPortController Tha60210051ClaPwEthPortControllerObjectInit(ThaClaEthPortController self, ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051CLACONTROLLERINTERNAL_H_ */


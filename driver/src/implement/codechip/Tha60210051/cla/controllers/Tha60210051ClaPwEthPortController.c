/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60210051ClaPwEthPortController.c
 *
 * Created Date: Oct 23, 2015
 *
 * Description : PW cla controller of 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "Tha60210051ClaPwEthPortControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count FCS error packet
Reg Addr   : 0x000D1838(RC)
Reg Formula: 0x000D1838 + eth_port
    Where  :
           + $eth_port(0-3): Receive Ethernet port ID
Reg Desc   :
This register is statistic counter for the over size packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_rx_port_fcs_error_pkt_cnt_rc_Base                                                   0x000D1038

/*------------------------------------------------------------------------------
Reg Name   : Receive Ethernet port Count PSN error packet
Reg Addr   : 0x00000005(RO)
Reg Formula: 0x00000005 + eth_port*2 + (r2c)
    Where  :
Reg Desc   :
This register is statistic count PSN error packet at receive side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_psn_err1_ro_Base                                                            0x00000005
#define cAf6Reg_Eth_cnt_psn_err1_rc_Base                                                            0x00000006
#define cAf6Reg_Eth_cnt_psn_err2_ro_Base                                                            0x00000007
#define cAf6Reg_Eth_cnt_psn_err2_rc_Base                                                            0x00000008

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaEthPortControllerMethods m_ThaClaEthPortControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleCla ClaModule(ThaClaEthPortController self)
    {
    return (ThaModuleCla)ThaClaControllerModuleGet((ThaClaController)self);
    }

static uint32 CounterOffset(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    return (AtChannelIdGet((AtChannel)port) + ((r2c) ? 0x800 : 0x0) + Tha60210011ModuleClaBaseAddress(ClaModule(self)));
    }

static uint32 EthPktFcsErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cAf6Reg_rx_port_fcs_error_pkt_cnt_rc_Base + CounterOffset(self, port, r2c), cThaModuleCla);
    }

static uint32 RxErrPsnPacketsRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    uint32 r2cOffset = (r2c) ? 0x1 : 0x0;
    uint32 portId = AtChannelIdGet((AtChannel)port);
    uint32 offset = (portId * 2UL) + Tha60210011ModuleClaBaseAddress(ClaModule(self)) + r2cOffset;
    return mChannelHwRead(port, cAf6Reg_Eth_cnt_psn_err1_ro_Base + offset, cThaModuleCla);
    }

static void OverrideThaClaEthPortController(ThaClaEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthPortControllerOverride));

        /* Counters */
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktFcsErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, RxErrPsnPacketsRead2Clear);
        }

    mMethodsSet(self, &m_ThaClaEthPortControllerOverride);
    }

static void Override(ThaClaEthPortController self)
    {
    OverrideThaClaEthPortController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ClaPwEthPortController);
    }

ThaClaEthPortController Tha60210051ClaPwEthPortControllerObjectInit(ThaClaEthPortController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ClaPwEthPortControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthPortController Tha60210051ClaPwEthPortControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051ClaPwEthPortControllerObjectInit(newController, cla);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60210051ModuleCla.h
 * 
 * Created Date: Feb 29, 2016
 *
 * Description : Module CLA header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULECLAINTERNAL_H_
#define _THA60210051MODULECLAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/cla/Tha60210011ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051ModuleCla
    {
    tTha60210011ModuleCla super;
    }tTha60210051ModuleCla;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60210051ModuleClaObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULECLAINTERNAL_H_ */


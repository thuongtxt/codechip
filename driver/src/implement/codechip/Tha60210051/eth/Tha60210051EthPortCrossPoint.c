/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210051EthPortCrossPoint.c
 *
 * Created Date: May 25, 2016
 *
 * Description : 60210051 Ethernet port version 2 with Cross-point supported
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "Tha60210051ModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cSerdesRxSelectReg 0xF00040
#define cSerdesTxSelectReg 0xF00041

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210051EthPortCrossPoint)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtEthPortMethods    m_AtEthPortOverride;

/* Save super implementation */
static const tAtEthPortMethods *m_AtEthPortMethods = NULL;
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MacIdToSerdesSelectMask(uint32 hwMacId)
    {
    if (hwMacId == 0)
        return cBit2_0;

    if (hwMacId == 1)
        return cBit6_4;

    if (hwMacId == 2)
        return cBit18_16;

    if (hwMacId == 3)
        return cBit22_20;

    return 0;
    }

static uint32 MacIdToSerdesSelectShift(uint32 hwMacId)
    {
    if (hwMacId == 0)
        return 0;

    if (hwMacId == 1)
        return 4;

    if (hwMacId == 2)
        return 16;

    if (hwMacId == 3)
        return 20;

    return 0;
    }

static eBool IsCemMacs(uint32 macId)
    {
    if (macId < cStartExtraMacId)
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsCemSerdes(uint32 serdesId)
    {
    if (serdesId < cStartExtraSerdesId)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 HwSerdesIdToRegValue(uint32 macId, uint32 serdesId)
    {
    if (IsCemMacs(macId))
        return serdesId;
    return (serdesId - cStartExtraSerdesId);
    }

static eBool IsBridgableSerdes(AtEthPort self, AtSerdesController serdes)
    {
    uint32 macId = AtChannelHwIdGet((AtChannel)self);
    AtModuleEth module = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    AtSerdesController bridgableSerdes;
    uint32 groupId, bridgableSerdesId;

    if (IsCemMacs(macId))
        {
        groupId = Tha60210051ModuleEthXfiGroupGet(module);
        bridgableSerdesId = Tha60210051ModuleEthCemMacIdToBridgedSerdesIdForMainboardSlot0(macId, groupId);
        bridgableSerdes = AtModuleEthSerdesController(module, bridgableSerdesId);
        return (serdes == bridgableSerdes) ? cAtTrue: cAtFalse;
        }

    groupId = Tha60210051ModuleEthExtraXfiGroupGet(module);
    bridgableSerdesId = Tha60210051ModuleEthExtraMacIdToBridgedSerdesIdForMainboardSlot0(macId, groupId);
    bridgableSerdes = AtModuleEthSerdesController(module, bridgableSerdesId);
    return (serdes == bridgableSerdes) ? cAtTrue : cAtFalse;
    }

static eBool IsSerdesOfMac(AtEthPort self, AtSerdesController serdes)
    {
    uint32 macId = AtChannelHwIdGet((AtChannel)self);
    AtModuleEth module = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    AtSerdesController macSerdes;
    uint32 groupId, macSerdesId;

    if (IsCemMacs(macId))
        {
        groupId = Tha60210051ModuleEthXfiGroupGet(module);
        macSerdesId = Tha60210051ModuleEthCemMacIdToSerdesIdForMainboardSlot0(macId, groupId);
        macSerdes = AtModuleEthSerdesController(module, macSerdesId);
        return (serdes == macSerdes) ? cAtTrue: cAtFalse;
        }

    groupId = Tha60210051ModuleEthExtraXfiGroupGet(module);
    macSerdesId = Tha60210051ModuleEthExtraMacIdToSerdesIdForMainboardSlot0(macId, groupId);
    macSerdes = AtModuleEthSerdesController(module, macSerdesId);
    return (serdes == macSerdes) ? cAtTrue : cAtFalse;
    }

static eBool IsSelectableSerdes(AtEthPort self, AtSerdesController serdes)
    {

    if (IsBridgableSerdes(self, serdes))
        return cAtTrue;

    if (IsSerdesOfMac(self, serdes))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    AtModuleEth module = (AtModuleEth)AtChannelModuleGet((AtChannel)self);

    if (Tha60210051ModuleEthXfiGroupIsSetup(module) && !IsSelectableSerdes(self, serdes))
        return cAtErrorInvlParm;

    return Tha60210051EthPortCrossPointRxSerdesSelect(self, serdes);
    }

static uint32 HwRegValueToHwSerdesId(uint32 macId, uint32 regValue)
    {
    if (IsCemMacs(macId))
        return regValue;
    return (regValue + cStartExtraSerdesId);
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    ThaModuleEth module = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    uint32 regVal = mChannelHwRead(self, cSerdesRxSelectReg, cAtModuleEth);
    uint32 portHwId = AtChannelHwIdGet((AtChannel)self);
    uint32 serdesHwId, serdesSwId;
    uint32 mask, shift;

    mask  = MacIdToSerdesSelectMask(portHwId);
    shift = MacIdToSerdesSelectShift(portHwId);
    mFieldGet(regVal, mask, shift, uint32, &serdesHwId);

    serdesHwId = HwRegValueToHwSerdesId(portHwId, serdesHwId);
    serdesSwId = mMethodsGet(module)->SerdesIdHwToSw(module, serdesHwId);

    return AtModuleEthSerdesController((AtModuleEth)module, serdesSwId);
    }

static uint32 SerdesTxEnableMask(uint32 macId, uint32 serdesId)
    {
    if (IsCemMacs(macId))
        return (cBit8 << serdesId);

    return (cBit24 << (serdesId - cStartExtraSerdesId));
    }

static uint32 SerdesTxEnableShift(uint32 macId, uint32 serdesId)
    {
    if (IsCemMacs(macId))
        return (8 + serdesId);

    return (24 + (serdesId - cStartExtraSerdesId));
    }

static uint32 SerdesTxSelectMask(uint32 macId, uint32 serdesId)
    {
    if (IsCemMacs(macId))
        return (cBit0 << serdesId);

    return (cBit16 << (serdesId - cStartExtraSerdesId));
    }

static uint32 SerdesTxSelectShift(uint32 macId, uint32 serdesId)
    {
    if (IsCemMacs(macId))
        return serdesId;

    return (16 + (serdesId - cStartExtraSerdesId));
    }

static uint32 MacIdToRegValue(uint32 macId)
    {
    if (IsCemMacs(macId))
        return macId;
    return (macId - cStartExtraMacId);
    }

static uint32 RegValueToMacId(uint32 serdesId, uint32 regValue)
    {
    if (IsCemSerdes(serdesId))
        return regValue;
    return (regValue + cStartExtraMacId);
    }

static eAtModuleEthRet TxSerdesHwMacSet(AtEthPort self, AtSerdesController serdes)
    {
    uint32 regVal = mChannelHwRead(self, cSerdesTxSelectReg, cAtModuleEth);
    uint32 portHwId = AtChannelHwIdGet((AtChannel)self);
    uint32 serdesHwId = mMethodsGet(serdes)->HwIdGet(serdes);

    mFieldIns(&regVal, SerdesTxSelectMask(portHwId, serdesHwId), SerdesTxSelectShift(portHwId, serdesHwId), MacIdToRegValue(portHwId));
    mChannelHwWrite(self, cSerdesTxSelectReg, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool TxSerdesHwIsEnabled(AtEthPort self, AtSerdesController serdes)
    {
    uint32 regVal = mChannelHwRead(self, cSerdesTxSelectReg, cAtModuleEth);
    uint32 portHwId = AtChannelHwIdGet((AtChannel)self);
    uint32 serdesHwId = mMethodsGet(serdes)->HwIdGet(serdes);
    uint8 serdesEnabled;

    mFieldGet(regVal, SerdesTxEnableMask(portHwId, serdesHwId), SerdesTxEnableShift(portHwId, serdesHwId), uint8, &serdesEnabled);

    return serdesEnabled ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet TxSerdesBridgeHwRelease(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    return AtSerdesControllerEnable(serdes, cAtFalse);
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    /* Already bridge */
    AtSerdesController bridgedSerdes = AtEthPortTxBridgedSerdes(self);
    if (serdes == bridgedSerdes)
        return cAtOk;

    /* It is being bridged to another interface */
    if (bridgedSerdes && serdes && (serdes != bridgedSerdes))
        return cAtErrorChannelBusy;

    if (serdes)
        {
        if (!IsBridgableSerdes(self, serdes))
            return cAtErrorInvlParm;

        return Tha60210051EthPortCrossPointTxSerdesHwBridge(self, serdes);
        }

    return TxSerdesBridgeHwRelease(self, bridgedSerdes);
    }

static eAtModuleEthRet TxSerdesSet(AtEthPort self, AtSerdesController serdes)
    {
    eAtRet ret = cAtOk;

    /* Already set */
    AtSerdesController txSerdes = AtEthPortTxSerdesGet(self);
    if (serdes == txSerdes)
        return cAtOk;

    /* It is being bridged to this interface */
    if (serdes == AtEthPortTxBridgedSerdes(self))
        return cAtErrorChannelBusy;

    if (!IsSerdesOfMac(self, serdes))
        return cAtErrorInvlParm;

    if (txSerdes)
        {
        ret = TxSerdesBridgeHwRelease(self, txSerdes);
        if (ret != cAtOk)
            return ret;
        }

    return Tha60210015EthPortCrossPointerTxSerdesSet(self, serdes);
    }

static AtSerdesController TxSerdesGet(AtEthPort self)
    {
    return mThis(self)->txSerdes;
    }

static uint32 TxSerdesHwId(AtSerdesController serdes)
    {
    if (serdes)
        return mMethodsGet(serdes)->HwIdGet(serdes);

    return cInvalidUint32;
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    ThaModuleEth module = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    uint32 regVal = mChannelHwRead(self, cSerdesTxSelectReg, cAtModuleEth);
    uint32 portHwId = AtChannelHwIdGet((AtChannel)self);
    uint32 portHwRegVal, portHwIdFromRegVal, serdesHwId, serdesSwId;
    ThaEthSerdesManager serdesManager = ThaModuleEthSerdesManager(module);
    uint32 minSerdes, maxSerdes, numSerdes = ThaEthSerdesManagerNumberOfCreatedSerdesControllerGet(serdesManager);
    uint8 serdesEnabled;

    /* No tx, no bridge */
    AtSerdesController txSerdes = AtEthPortTxSerdesGet(self);
    if (txSerdes == NULL)
        return NULL;

    if (IsCemMacs(portHwId))
        {
        maxSerdes = cStartExtraSerdesId;
        minSerdes = 0;
        }
    else
        {
        maxSerdes = numSerdes;
        minSerdes = cStartExtraSerdesId;
        }

    for (serdesHwId = minSerdes; serdesHwId < maxSerdes; serdesHwId++)
        {
        mFieldGet(regVal, SerdesTxEnableMask(portHwId, serdesHwId), SerdesTxEnableShift(portHwId, serdesHwId), uint8, &serdesEnabled);
        if (serdesEnabled == 0)
            continue;

        mFieldGet(regVal, SerdesTxSelectMask(portHwId, serdesHwId), SerdesTxSelectShift(portHwId, serdesHwId), uint32, &portHwRegVal);
        portHwIdFromRegVal = RegValueToMacId(serdesHwId, portHwRegVal);
        if ((portHwIdFromRegVal == portHwId) && (serdesHwId != TxSerdesHwId(txSerdes)))
            {
            serdesSwId = mMethodsGet(module)->SerdesIdHwToSw(module, serdesHwId);
            return AtModuleEthSerdesController((AtModuleEth)module, serdesSwId);
            }
        }

    return NULL;
    }

static eBool TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtSerdesController bridgedSerdes = AtEthPortTxBridgedSerdes(self);
    if (bridgedSerdes && serdes && (serdes != bridgedSerdes))
        return cAtFalse;

    if (serdes == AtEthPortTxSerdesGet(self))
        return cAtFalse;

    return cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210051EthPortCrossPoint object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(txSerdes);
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, RxSerdesSelect);
        mMethodOverride(m_AtEthPortOverride, RxSelectedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesBridge);
        mMethodOverride(m_AtEthPortOverride, TxBridgedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesCanBridge);
        mMethodOverride(m_AtEthPortOverride, TxSerdesSet);
        mMethodOverride(m_AtEthPortOverride, TxSerdesGet);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051EthPortCrossPoint);
    }

AtEthPort Tha60210051EthPortCrossPointObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051EthPortObjectInit((AtEthPort)self, portId, module) == NULL)
        return NULL;

    /* Initialize method */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60210051EthPortCrossPointNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051EthPortCrossPointObjectInit(newPort, portId, module);
    }

eAtModuleEthRet Tha60210051EthPortCrossPointTxSerdesHwBridge(AtEthPort self, AtSerdesController serdes)
    {
    /* If serdes is enabled, it is being bridged for another port */
    if (TxSerdesHwIsEnabled(self, serdes))
        return cAtErrorChannelBusy;

    TxSerdesHwMacSet(self, serdes);

    return AtSerdesControllerEnable(serdes, cAtTrue);
    }

eAtModuleEthRet Tha60210015EthPortCrossPointerTxSerdesSet(AtEthPort self, AtSerdesController serdes)
    {
    eAtRet ret = cAtOk;

    ret = Tha60210051EthPortCrossPointTxSerdesHwBridge(self, serdes);
    if (ret != cAtOk)
        return ret;

    mThis(self)->txSerdes = serdes;
    return cAtOk;
    }

eAtModuleEthRet Tha60210051EthPortCrossPointRxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    uint32 regVal, portHwId, serdesHwId, mask, shift;

    regVal = mChannelHwRead(self, cSerdesRxSelectReg, cAtModuleEth);
    portHwId = AtChannelHwIdGet((AtChannel)self);
    serdesHwId = mMethodsGet(serdes)->HwIdGet(serdes);
    mask = MacIdToSerdesSelectMask(portHwId);
    shift = MacIdToSerdesSelectShift(portHwId);
    mFieldIns(&regVal, mask, shift, HwSerdesIdToRegValue(portHwId, serdesHwId));
    mChannelHwWrite(self, cSerdesRxSelectReg, regVal, cAtModuleEth);

    return cAtOk;
    }

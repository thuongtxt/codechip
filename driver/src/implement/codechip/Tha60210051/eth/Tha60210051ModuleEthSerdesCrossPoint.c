/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60210051ModuleEthSerdesCrossPoint.c
 *
 * Created Date: Sep 29, 2016
 *
 * Description : Implement specific for serdes cross point with extra mac and serdes
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "Tha60210051ModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eBool IsCemXfiGroup(uint32 groupId)
    {
    if (groupId < cStartExtraGroupId)
        return cAtTrue;

    return cAtFalse;
    }

uint32 Tha60210051ModuleEthCemMacIdToSerdesIdForMainboardSlot0(uint32 macId, uint32 xfiGroupId)
    {
    if (xfiGroupId == 0)
        {
        if (macId == 0)
            return 0;
        if (macId == 1)
            return 1;
        }

    if (xfiGroupId == 1)
        {
        if (macId == 0)
            return 2;
        if (macId == 1)
            return 3;
        }

    return 0;
    }

uint32 Tha60210051ModuleEthCemMacIdToBridgedSerdesIdForMainboardSlot0(uint32 macId, uint32 xfiGroupId)
    {
    if (xfiGroupId == 0)
        {
        if (macId == 0)
            return 4;
        if (macId == 1)
            return 5;
        }

    if (xfiGroupId == 1)
        {
        if (macId == 0)
            return 6;
        if (macId == 1)
            return 7;
        }

    return 4;
    }

uint32 Tha60210051ModuleEthExtraMacIdToSerdesIdForMainboardSlot0(uint32 macId, uint32 xfiGroupId)
    {
    if (xfiGroupId == 2)
        {
        if (macId == 2)
            return 8;
        if (macId == 3)
            return 9;
        }

    if (xfiGroupId == 3)
        {
        if (macId == 2)
            return 10;
        if (macId == 3)
            return 11;
        }

    return 8;
    }

uint32 Tha60210051ModuleEthExtraMacIdToBridgedSerdesIdForMainboardSlot0(uint32 macId, uint32 xfiGroupId)
    {
    if (xfiGroupId == 2)
        {
        if (macId == 2)
            return 12;
        if (macId == 3)
            return 13;
        }

    if (xfiGroupId == 3)
        {
        if (macId == 2)
            return 14;
        if (macId == 3)
            return 15;
        }

    return 12;
    }

static eAtRet CemXfiGroupSerdesDisable(AtModuleEth self, uint32 xfiGroupIdx)
    {
    uint32 numXfis, xfi, disabledXfi, xfi_i;
    uint32 disabledGroupId = Tha60210051ModuleEthDisabledXfiGroup(self, xfiGroupIdx);
    ThaEthSerdesManager manager = ThaModuleEthSerdesManager((ThaModuleEth)self);
    const uint32 *xfis = ThaEthSerdesManagerXfisForGroup(manager, xfiGroupIdx, &numXfis);
    const uint32 *disabledXfis = ThaEthSerdesManagerXfisForGroup(manager, disabledGroupId, &numXfis);
    AtSerdesController serdes, disabledSerdes;
    eAtRet ret;

    /* enalbe serdes */
    for (xfi_i = 0; xfi_i < numXfis; xfi_i++)
        {
        xfi = xfis[xfi_i];
        disabledXfi = disabledXfis[xfi_i];

        serdes = AtModuleEthSerdesController(self, xfi);
        ret = AtSerdesControllerEnable(serdes, cAtFalse);
        if (ret != cAtOk)
            return ret;

        disabledSerdes = AtModuleEthSerdesController(self, disabledXfi);
        ret = AtSerdesControllerEnable(disabledSerdes, cAtFalse);
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

static eAtRet CemXfiGroupSerdesPowerUp(AtModuleEth self, uint32 xfiGroupIdx)
    {
    uint32 numXfis, xfi, xfi_i;
    ThaEthSerdesManager manager = ThaModuleEthSerdesManager((ThaModuleEth)self);
    const uint32 *xfis = ThaEthSerdesManagerXfisForGroup(manager, xfiGroupIdx, &numXfis);
    AtSerdesController serdes;
    eAtRet ret;

    /* enalbe serdes */
    for (xfi_i = 0; xfi_i < numXfis; xfi_i++)
        {
        xfi = xfis[xfi_i];

        serdes = AtModuleEthSerdesController(self, xfi);
        ret = AtSerdesControllerPowerDown(serdes, cAtFalse);
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

static eAtRet ExtraXfiGroupSerdesPowerUp(AtModuleEth self, uint32 xfiGroupIdx)
    {
    uint32 numXfis, xfi, xfi_i;
    ThaEthSerdesManager manager = ThaModuleEthSerdesManager((ThaModuleEth)self);
    const uint32 *xfis = ThaEthSerdesManagerXfisForGroup(manager, xfiGroupIdx, &numXfis);
    AtSerdesController serdes;
    ThaEthSerdesManager serdesManager = ThaModuleEthSerdesManager((ThaModuleEth)self);
    uint32 numCreatedSerdes = ThaEthSerdesManagerNumberOfCreatedSerdesControllerGet(serdesManager);
    eAtRet ret;

    /* enable serdes */
    for (xfi_i = 0; xfi_i < numXfis; xfi_i++)
        {
        xfi = xfis[xfi_i];

        /* serdesId is greater than number of created serdes for the first time initializing */
        if (xfi >= numCreatedSerdes)
            continue;

        serdes = AtModuleEthSerdesController(self, xfi);
        ret = AtSerdesControllerPowerDown(serdes, cAtFalse);
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

static eAtRet ExtraXfiGroupSerdesDisable(AtModuleEth self, uint32 xfiGroupIdx)
    {
    uint32 numXfis, xfi, disabledXfi, xfi_i;
    uint32 disabledGroupId = Tha60210051ModuleEthDisabledXfiGroup(self, xfiGroupIdx);
    ThaEthSerdesManager manager = ThaModuleEthSerdesManager((ThaModuleEth)self);
    const uint32 *xfis = ThaEthSerdesManagerXfisForGroup(manager, xfiGroupIdx, &numXfis);
    const uint32 *disabledXfis = ThaEthSerdesManagerXfisForGroup(manager, disabledGroupId, &numXfis);
    AtSerdesController serdes, disabledSerdes;
    ThaEthSerdesManager serdesManager = ThaModuleEthSerdesManager((ThaModuleEth)self);
    uint32 numCreatedSerdes = ThaEthSerdesManagerNumberOfCreatedSerdesControllerGet(serdesManager);
    eAtRet ret;

    /* enable serdes */
    for (xfi_i = 0; xfi_i < numXfis; xfi_i++)
        {
        xfi = xfis[xfi_i];
        disabledXfi = disabledXfis[xfi_i];

        /* serdesId is greater than number of created serdes for the first time initializing */
        if ((xfi >= numCreatedSerdes) || (disabledXfi >= numCreatedSerdes))
            continue;

        serdes = AtModuleEthSerdesController(self, xfi);
        ret = AtSerdesControllerEnable(serdes, cAtFalse);
        if (ret != cAtOk)
            return ret;

        disabledSerdes = AtModuleEthSerdesController(self, disabledXfi);
        ret = AtSerdesControllerEnable(disabledSerdes, cAtFalse);
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

static eAtRet SerdesToMacAssign(AtEthPort port, AtSerdesController serdes, AtSerdesController bridgedSerdes)
    {
    eAtRet ret;

    ret = Tha60210051EthPortCrossPointRxSerdesSelect(port, serdes);
    if (ret != cAtOk)
        return ret;

    ret = Tha60210015EthPortCrossPointerTxSerdesSet(port, serdes);/* this also enable serdes */
    if (ret != cAtOk)
        return ret;

    ret = Tha60210051EthPortCrossPointTxSerdesHwBridge(port, bridgedSerdes);/* this also enable serdes */
    return ret;
    }

static eAtRet CemXfiGroupDefaultSerdesAssignedToMac(AtModuleEth self, uint32 xfiGroupIdx)
    {
    uint32 xfi, mac_i, bridgedXfi;
    AtSerdesController serdes, bridgedSerdes;
    AtEthPort port;
    eAtRet ret;

    /* select serdes and set tx serdes*/
    for (mac_i = 0; mac_i < cStartExtraMacId; mac_i++)
        {
        xfi = Tha60210051ModuleEthCemMacIdToSerdesIdForMainboardSlot0(mac_i, xfiGroupIdx);
        serdes = AtModuleEthSerdesController(self, xfi);
        port   = AtModuleEthPortGet(self, (uint8)mac_i);

        bridgedXfi = Tha60210051ModuleEthCemMacIdToBridgedSerdesIdForMainboardSlot0(mac_i, xfiGroupIdx);
        bridgedSerdes = AtModuleEthSerdesController(self, bridgedXfi);
        ret = SerdesToMacAssign(port, serdes, bridgedSerdes);
        if (ret != cAtOk)
            return ret;
        }
    return cAtOk;
    }

static eAtRet ExtraXfiGroupDefaultSerdesAssignedToMac(AtModuleEth self, uint32 xfiGroupIdx)
    {
    uint32 xfi, mac_i, bridgedXfi;
    AtSerdesController serdes, bridgedSerdes;
    AtEthPort port;
    uint32 numMacs = AtModuleEthNumberOfCreatedPortsGet(self);
    ThaEthSerdesManager serdesManager = ThaModuleEthSerdesManager((ThaModuleEth)self);
    uint32 numCreatedSerdes = ThaEthSerdesManagerNumberOfCreatedSerdesControllerGet(serdesManager);
    eAtRet ret;

    /* select serdes and set tx serdes*/
    for (mac_i = cStartExtraMacId; mac_i < numMacs; mac_i++)
        {
        xfi = Tha60210051ModuleEthExtraMacIdToSerdesIdForMainboardSlot0(mac_i, xfiGroupIdx);
        bridgedXfi = Tha60210051ModuleEthExtraMacIdToBridgedSerdesIdForMainboardSlot0(mac_i, xfiGroupIdx);
        /* serdesId is greater than number of created serdes for the first time initializing */
        if ((xfi >= numCreatedSerdes) || (bridgedXfi >= numCreatedSerdes))
            continue;

        serdes = AtModuleEthSerdesController(self, xfi);
        port   = AtModuleEthPortGet(self, (uint8)mac_i);
        bridgedSerdes = AtModuleEthSerdesController(self, bridgedXfi);
        ret = SerdesToMacAssign(port, serdes, bridgedSerdes);
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

eAtRet Tha60210051ModuleEthCrossPointCemXfiGroupSet(AtModuleEth self, uint32 xfiGroupIdx)
    {
    eAtRet ret;

    if (!IsCemXfiGroup(xfiGroupIdx))
        return cAtErrorOutOfRangParm;

    ret = CemXfiGroupSerdesDisable(self, xfiGroupIdx);
    if (ret != cAtOk)
        return ret;

    ret = CemXfiGroupDefaultSerdesAssignedToMac(self, xfiGroupIdx);
    if (ret != cAtOk)
        return ret;

    ret = CemXfiGroupSerdesPowerUp(self, xfiGroupIdx);
    return ret;
    }

eAtRet Tha60210051ModuleEthCrossPointExtraXfiGroupSet(AtModuleEth self, uint32 xfiGroupIdx)
    {
    eAtRet ret;

    if (IsCemXfiGroup(xfiGroupIdx))
        return cAtErrorOutOfRangParm;

    ret = ExtraXfiGroupSerdesDisable(self, xfiGroupIdx);
    if (ret != cAtOk)
        return ret;

    ret = ExtraXfiGroupDefaultSerdesAssignedToMac(self, xfiGroupIdx);
    if (ret != cAtOk)
        return ret;

    ret = ExtraXfiGroupSerdesPowerUp(self, xfiGroupIdx);
    return ret;
    }


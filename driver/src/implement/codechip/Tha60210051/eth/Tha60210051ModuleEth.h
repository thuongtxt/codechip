/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : Tha60210051ModuleEth.h
 * 
 * Created Date: May 25, 2016
 *
 * Description : 60210051 module ethernet interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULEETH_H_
#define _THA60210051MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051EthPort   * Tha60210051EthPort;
typedef struct tTha60210051ModuleEth * Tha60210051ModuleEth;
typedef struct tTha60210051EthPortCrossPoint * Tha60210051EthPortCrossPoint;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60210051ModuleEthCrossPointIsSupported(AtModuleEth self);
eAtRet Tha60210051ModuleEthCrossPointCemXfiGroupSet(AtModuleEth self, uint32 xfiGroupIdx);
eAtRet Tha60210051ModuleEthCrossPointExtraXfiGroupSet(AtModuleEth self, uint32 xfiGroupIdx);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULEETH_H_ */


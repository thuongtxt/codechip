/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60210051ModuleEthInternal.h
 * 
 * Created Date: Oct 8, 2015
 *
 * Description : ETH module of 60210051
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULEETHINTERNAL_H_
#define _THA60210051MODULEETHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/eth/Tha60210011ModuleEthInternal.h"
#include "../../Tha60210011/eth/Tha60210011EthPortInternal.h"
#include "Tha60210051ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cMaxMux  2
#define cMaxMacs 2
#define cMaxExtraMacs 2

#define cStartExtraSerdesId 8
#define cStartExtraMacId    2
#define cStartExtraGroupId  2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60210051ModuleEthMethods
    {
    uint32 (*StartHwVersionSupportCrossPoint)(Tha60210051ModuleEth self);
    eAtRet (*CrossPointSerdesSetup)(Tha60210051ModuleEth self);
    void (*XfiGroupSelect)(Tha60210051ModuleEth self, uint32 xfiGroupIdx);
    uint32 (*XfiGroupGet)(Tha60210051ModuleEth self);
    uint32 (*StartHardwareVersionSupportRxErrPsnPacketsCounter)(Tha60210051ModuleEth self);
    }tTha60210051ModuleEthMethods;

typedef struct tTha60210051ModuleEth
    {
    tTha60210011ModuleEth super;
    const tTha60210051ModuleEthMethods* methods;

    /* Private data */
    uint32 xfiGroup;
    uint32 extraXfiGroup;
    eBool  isGroupSetup;
    }tTha60210051ModuleEth;

typedef struct tTha60210051EthPort
    {
    tTha60210011EthPort super;
    AtErrorGenerator errorGenerator;
    }tTha60210051EthPort;

typedef struct tTha60210051EthPortCrossPoint
    {
    tTha60210051EthPort super;
    AtSerdesController txSerdes;
    }tTha60210051EthPortCrossPoint;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha60210051ModuleEthObjectInit(AtModuleEth self, AtDevice device);
AtEthPort Tha60210051EthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);
AtEthPort Tha60210051EthPortCrossPointObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

eBool Tha60210051ModuleEthPowerControlIsSupported(AtModuleEth self);
void Tha60210051EthPortSerdesWillPowerDownNotify(ThaEthPort self);
uint32 Tha60210051ModuleEthStartHardwareVersionSupportRxErrPsnPacketsCounter(AtModuleEth self);

uint32 Tha60210051ModuleEthDisabledXfiGroup(AtModuleEth self, uint32 xfiGroup);
eBool Tha60210051ModuleEthXfiGroupIsSetup(AtModuleEth self);
eAtModuleEthRet Tha60210051EthPortCrossPointTxSerdesHwBridge(AtEthPort self, AtSerdesController serdes);
eAtModuleEthRet Tha60210015EthPortCrossPointerTxSerdesSet(AtEthPort self, AtSerdesController serdes);
eAtModuleEthRet Tha60210051EthPortCrossPointRxSerdesSelect(AtEthPort self, AtSerdesController serdes);
uint32 Tha60210051ModuleEthCemMacIdToSerdesIdForMainboardSlot0(uint32 macId, uint32 xfiGroupId);
uint32 Tha60210051ModuleEthCemMacIdToBridgedSerdesIdForMainboardSlot0(uint32 macId, uint32 xfiGroupId);
uint32 Tha60210051ModuleEthExtraMacIdToSerdesIdForMainboardSlot0(uint32 macId, uint32 xfiGroupId);
uint32 Tha60210051ModuleEthExtraMacIdToBridgedSerdesIdForMainboardSlot0(uint32 macId, uint32 xfiGroupId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULEETHINTERNAL_H_ */


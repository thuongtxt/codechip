/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2021 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210051ModuleEth.c
 *
 * Created Date: Mar 8, 2021
 *
 * Description : Ethernet module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/prbs/Tha6021EthPortSerdesPrbsEngine.h"
#include "../man/Tha60210051Device.h"
#include "Tha60210051ModuleEthInternal.h"
#include "Tha60210051ModuleEthReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210051ModuleEth)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60210051ModuleEthMethods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtModuleMethods     m_AtModuleOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;
static tAtModuleEthMethods  m_AtModuleEthOverride;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods    = NULL;
static const tAtModuleMethods    *m_AtModuleMethods    = NULL;
static const tAtModuleEthMethods *m_AtModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void XfiGroupSelect(Tha60210051ModuleEth self, uint32 xfiGroupIdx)
    {
    uint32 mask[cMaxMux][cMaxMacs]  = {{cThaReg_XFI_Mac0_Mux1_Sel_Mask, cThaReg_XFI_Mac1_Mux1_Sel_Mask},
                                       {cThaReg_XFI_Mac0_Mux2_Sel_Mask, cThaReg_XFI_Mac1_Mux2_Sel_Mask}};
    uint32 shift[cMaxMux][cMaxMacs] = {{cThaReg_XFI_Mac0_Mux1_Sel_Shift, cThaReg_XFI_Mac1_Mux1_Sel_Shift},
                                       {cThaReg_XFI_Mac0_Mux2_Sel_Shift, cThaReg_XFI_Mac1_Mux2_Sel_Shift}};
    uint32 mac, regVal = 0;

    for (mac = 0; mac < cMaxMacs; mac++)
        {
        mFieldIns(&regVal, mask[0][mac], shift[0][mac], xfiGroupIdx);
        mFieldIns(&regVal, mask[1][mac], shift[1][mac], 0); /* Set default from active RSP */
        }

    mModuleHwWrite(self, cThaReg_top_o_control0, regVal);
    }

static eBool CanEnableTxSerdes(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
    if (Tha60210051ModuleEthCrossPointIsSupported(self))
        return (cMaxMacs + cMaxExtraMacs);

    return cMaxMacs;
    }

static eBool ShouldUsePrbsEngineV2(void)
    {
    /* HW recommend to use this engine to work with XFI PRBS */
    return cAtTrue;
    }

static ThaEthSerdesManager SerdesManagerCreate(ThaModuleEth self)
    {
    return Tha60210051EthPortSerdesManagerNew((AtModuleEth)self);
    }

static AtPrbsEngine PortSerdesPrbsEngineCreate(ThaModuleEth self, AtEthPort ethPort, uint32 serdesId)
    {
    AtSerdesController controller = AtModuleEthSerdesController((AtModuleEth)self, serdesId);
    AtUnused(ethPort);

    if (ShouldUsePrbsEngineV2())
        return Tha60210051XfiSerdesPrbsEngineV2New(controller);

    return Tha60210051XfiSerdesPrbsEngineNew(controller);
    }

static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    if (Tha60210051ModuleEthCrossPointIsSupported(self))
        {
        if (portId < cStartExtraMacId)
            return Tha60210051EthPortCrossPointNew(portId, self);
        return Tha60210051EthExtraPortNew(portId, self);
        }

    return Tha60210051EthPortNew(portId, self);
    }

static uint32 SerdesDrpBaseAddress(AtModuleEth self)
    {
    AtUnused(self);
    return 0xF45004; /* 0xF60000 = 0xF45004 + 0x1AFFC */
    }

static uint32 DrpPortOffset(AtDrp self, uint32 selectedPortId)
    {
    AtUnused(self);
    AtUnused(selectedPortId);
    return 0x1AFFC;
    }

static tAtDrpAddressCalculator *NonCrossPointSerdesDrpAddressCalculator(void)
    {
    static tAtDrpAddressCalculator calculator;
    static tAtDrpAddressCalculator *pCalculator = NULL;

    if (pCalculator)
        return pCalculator;

    AtOsalMemInit(&calculator, 0, sizeof(tAtDrpAddressCalculator));
    calculator.PortOffset = DrpPortOffset;
    pCalculator = &calculator;

    return pCalculator;
    }

static AtDrp NonCrossPointSerdesDrpCreate(AtModuleEth self, uint32 serdesId)
    {
    AtDrp drp = m_AtModuleEthMethods->SerdesDrpCreate(self, serdesId);
    AtDrpAddressCalculatorSet(drp, NonCrossPointSerdesDrpAddressCalculator());
    return drp;
    }

static uint32 CrossPointDrpPortSelectRegisterAddress(AtDrp self)
    {
    AtUnused(self);
    return 0xF00044;
    }

static uint32 CrossPointDrpPortSelectRegisterMask(AtDrp self)
    {
    AtUnused(self);
    return cBit7_0;
    }

static uint32 CrossPointDrpPortSelectRegisterShift(AtDrp self)
    {
    AtUnused(self);
    return 0;
    }

static tAtDrpAddressCalculator *CrossPointSerdesDrpAddressCalculator(void)
    {
    static tAtDrpAddressCalculator calculator;
    static tAtDrpAddressCalculator *pCalculator = NULL;

    if (pCalculator)
        return pCalculator;

    AtOsalMemInit(&calculator, 0, sizeof(tAtDrpAddressCalculator));
    calculator.PortOffset = DrpPortOffset;
    calculator.PortSelectRegisterAddress = CrossPointDrpPortSelectRegisterAddress;
    calculator.PortSelectRegisterMask    = CrossPointDrpPortSelectRegisterMask;
    calculator.PortSelectRegisterShift   = CrossPointDrpPortSelectRegisterShift;
    pCalculator = &calculator;

    return pCalculator;
    }

static AtDrp CrossPointSerdesDrpCreate(AtModuleEth self, uint32 serdesId)
    {
    AtDrp drp = m_AtModuleEthMethods->SerdesDrpCreate(self, serdesId);
    AtDrpAddressCalculatorSet(drp, CrossPointSerdesDrpAddressCalculator());
    return drp;
    }

static AtDrp SerdesDrpCreate(AtModuleEth self, uint32 serdesId)
    {
    if (Tha60210051ModuleEthCrossPointIsSupported(self))
        return CrossPointSerdesDrpCreate(self, serdesId);

    return NonCrossPointSerdesDrpCreate(self, serdesId);
    }

static eBool SerdesHasDrp(AtModuleEth self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAtTrue;
    }

static eAtRet Init(AtModule self)
    {
    AtModuleEth ethModule = (AtModuleEth)self;
    eAtRet ret;

    /* Super init */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (!ThaModuleEthHasSerdesGroup((ThaModuleEth)self))
        return cAtOk;

    /* Set default to select XFI group#0 as active */
    ret = Tha60210051ModuleEthXfiGroupSet(ethModule, 0);
    if (ret != cAtOk)
        return ret;

    if (Tha60210051ModuleEthCrossPointIsSupported(ethModule)
        && (AtModuleEthNumberOfCreatedPortsGet(ethModule) >= cStartExtraMacId))
        {
        ret = Tha60210051ModuleEthExtraXfiGroupSet(ethModule, cStartExtraGroupId);
        if (ret != cAtOk)
            return ret;
        }

    mThis(self)->isGroupSetup = cAtTrue;

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static eBool PowerControlIsSupported(AtModuleEth self)
    {
    return ThaDeviceSerdesPowerControlIsSupported((ThaDevice)AtModuleDeviceGet((AtModule)self));
    }

static eAtRet XfiGroupEnableAndPowerUp(AtModuleEth self, uint32 groupId)
    {
    uint32 numSerdes, serdes_i;
    ThaEthSerdesManager manager = ThaModuleEthSerdesManager((ThaModuleEth)self);
    const uint32 *serdeses = ThaEthSerdesManagerXfisForGroup(manager, groupId, &numSerdes);
    eAtRet ret = cAtOk;

    if (serdeses == NULL)
        return cAtErrorInvlParm;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        uint32 serdesId = serdeses[serdes_i];
        AtSerdesController serdes = AtModuleEthSerdesController(self, serdesId);
        ret |= AtSerdesControllerFpgaEnable(serdes, cAtTrue);

        if (AtSerdesControllerPowerCanControl(serdes))
            ret |= AtSerdesControllerPowerDown(serdes, cAtFalse);
        }

    return ret;
    }

static eAtRet XfiGroupDisable(AtModuleEth self, uint32 groupId)
    {
    uint32 numSerdes, serdes_i;
    ThaEthSerdesManager manager = ThaModuleEthSerdesManager((ThaModuleEth)self);
    const uint32 *Serdeses = ThaEthSerdesManagerXfisForGroup(manager, groupId, &numSerdes);
    eAtRet ret = cAtOk;

    if (Serdeses == NULL)
        return cAtErrorInvlParm;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        uint32 serdesId = Serdeses[serdes_i];
        AtSerdesController serdes = AtModuleEthSerdesController(self, serdesId);
        ret |= AtSerdesControllerFpgaEnable(serdes, cAtFalse);
        }

    return ret;
    }

static eAtRet XfiGroupPowerdown(AtModuleEth self, uint32 groupId)
    {
    uint32 numSerdes, serdes_i;
    ThaEthSerdesManager manager = ThaModuleEthSerdesManager((ThaModuleEth)self);
    const uint32 *Serdeses = ThaEthSerdesManagerXfisForGroup(manager, groupId, &numSerdes);
    eAtRet ret = cAtOk;

    if (Serdeses == NULL)
        return cAtErrorInvlParm;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        uint32 serdesId = Serdeses[serdes_i];
        AtSerdesController serdes = AtModuleEthSerdesController(self, serdesId);

        if (AtSerdesControllerPowerCanControl(serdes))
            ret |= AtSerdesControllerPowerDown(serdes, cAtTrue);
        }

    return ret;
    }

static eBool XfiGroupIsValid(AtModuleEth self, uint32 groupId)
    {
    ThaEthSerdesManager manager = ThaModuleEthSerdesManager((ThaModuleEth)self);

    if (Tha60210051ModuleEthCrossPointIsSupported(self))
        return (groupId < cStartExtraGroupId) ? cAtTrue : cAtFalse;

    return (groupId < ThaEthSerdesManagerNumXfiGroups(manager)) ? cAtTrue : cAtFalse;
    }

static eAtRet XfiGroupSet(AtModuleEth self, uint32 groupId)
    {
    eAtRet ret = cAtOk;
    uint32 disabledGroupId;

    mMethodsGet(mThis(self))->XfiGroupSelect(mThis(self), groupId);

    /* All of ports in selected group must be enabled while all ports of other
     * groups must be disabled */
    ret |= XfiGroupEnableAndPowerUp(self, groupId);

    disabledGroupId = Tha60210051ModuleEthDisabledXfiGroup(self, groupId);
    if (XfiGroupIsValid(self, disabledGroupId))
        ret |= XfiGroupDisable(self, disabledGroupId);

    return ret;
    }

static eBool ExtraXfiGroupIsValid(AtModuleEth self, uint32 groupId)
    {
    ThaEthSerdesManager manager = ThaModuleEthSerdesManager((ThaModuleEth)self);

    if (!Tha60210051ModuleEthCrossPointIsSupported(self))
        return cAtFalse;

    return (groupId >= cStartExtraGroupId) && (groupId < ThaEthSerdesManagerNumXfiGroups(manager)) ? cAtTrue : cAtFalse;
    }

static uint32 ExtraSerdesIdToPortId(ThaModuleEth self, uint32 serdesId)
    {
    if (Tha60210051ModuleEthCrossPointIsSupported((AtModuleEth)self))
        {
        switch (serdesId)
            {
            case 8: return 2;
            case 10: return 2;
            case 12: return 2;
            case 14: return 2;
            case 9: return 3;
            case 11: return 3;
            case 13: return 3;
            case 15: return 3;
            default:
                return serdesId;
            }
        }
    return serdesId;
    }

static uint32 SerdesIdToEthPortId(ThaModuleEth self, uint32 serdesId)
    {
    AtUnused(self);
    switch (serdesId)
        {
        case 0: return 0;
        case 2: return 0;
        case 4: return 0;
        case 6: return 0;
        case 1: return 1;
        case 3: return 1;
        case 5: return 1;
        case 7: return 1;
        default:
            return ExtraSerdesIdToPortId(self, serdesId);
        }
    }

static uint32 ExtraEthPortIdIdToSerdesId(ThaModuleEth self, uint32 ethPortId)
    {
    if (Tha60210051ModuleEthCrossPointIsSupported((AtModuleEth)self))
        {
        uint32 xfiGroup = Tha60210051ModuleEthExtraXfiGroupGet((AtModuleEth)self);
        if (xfiGroup == 2)
            return ethPortId + 6;

        if (xfiGroup == 3)
            return ethPortId + 8;

        return ethPortId;
        }
    return ethPortId;
    }

static uint32 EthPortIdToSerdesId(ThaModuleEth self, uint32 ethPortId)
    {
    uint32 xfiGroup = Tha60210051ModuleEthXfiGroupGet((AtModuleEth)self);
    if (xfiGroup == 0)
        return ethPortId;

    if (xfiGroup == 1)
        return ethPortId + 2;

    return ExtraEthPortIdIdToSerdesId(self, ethPortId);
    }

static eBool HasSerdesGroup(ThaModuleEth self)
    {
    ThaEthSerdesManager manager = ThaModuleEthSerdesManager(self);
    return ThaEthSerdesManagerHasXfiGroups(manager);
    }

static uint32 StartHwVersionSupportCrossPoint(Tha60210051ModuleEth self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x8, 0x0, 0x0);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210051ModuleEth object = (Tha60210051ModuleEth)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(xfiGroup);
    mEncodeUInt(extraXfiGroup);
    mEncodeUInt(isGroupSetup);
    }

static uint32 XfiGroupGet(Tha60210051ModuleEth self)
    {
    return self->xfiGroup;
    }

static uint32 StartHardwareVersionSupportRxErrPsnPacketsCounter(Tha60210051ModuleEth self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x6, 0x1005);
    }

static void MethodsInit(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StartHwVersionSupportCrossPoint);
        mMethodOverride(m_methods, XfiGroupSelect);
        mMethodOverride(m_methods, XfiGroupGet);
        mMethodOverride(m_methods, StartHardwareVersionSupportRxErrPsnPacketsCounter);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051ModuleEth);
    }

static void OverrideAtObject(AtModuleEth self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);

        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static eAtRet Setup(AtModule self)
    {
    mThis(self)->isGroupSetup = cAtFalse;

    /* Super setup */
    return m_AtModuleMethods->Setup(self);
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Setup);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, PortSerdesPrbsEngineCreate);
        mMethodOverride(m_ThaModuleEthOverride, SerdesManagerCreate);
        mMethodOverride(m_ThaModuleEthOverride, SerdesIdToEthPortId);
        mMethodOverride(m_ThaModuleEthOverride, EthPortIdToSerdesId);
        mMethodOverride(m_ThaModuleEthOverride, CanEnableTxSerdes);
        mMethodOverride(m_ThaModuleEthOverride, HasSerdesGroup);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        mMethodOverride(m_AtModuleEthOverride, SerdesDrpBaseAddress);
        mMethodOverride(m_AtModuleEthOverride, SerdesDrpCreate);
        mMethodOverride(m_AtModuleEthOverride, SerdesHasDrp);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    }

AtModuleEth Tha60210051ModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;
    mThis(self)->isGroupSetup = cAtFalse;
    mThis(self)->xfiGroup     = cBit31_0;
    mThis(self)->extraXfiGroup = cBit31_0;

    return self;
    }

AtModuleEth Tha60210051ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051ModuleEthObjectInit(newModule, device);
    }

eAtRet Tha60210051ModuleEthXfiGroupSet(AtModuleEth self, uint32 groupId)
    {
    eAtRet ret = cAtOk;
    uint32 disableGroup;

    if (self == NULL)
        return cAtErrorNullPointer;

    if (!mMethodsGet((ThaModuleEth)self)->HasSerdesGroup((ThaModuleEth)self))
        return cAtErrorModeNotSupport;

    if (!XfiGroupIsValid(self, groupId))
        return cAtErrorOutOfRangParm;

    /* had been set then do nothing */
    if (Tha60210051ModuleEthXfiGroupIsSetup(self) && (groupId == mThis(self)->xfiGroup))
        return cAtOk;

    if (Tha60210051ModuleEthCrossPointIsSupported(self))
        ret = Tha60210051ModuleEthCrossPointCemXfiGroupSet(self, groupId);
    else
        ret = XfiGroupSet(self, groupId);

    if (ret == cAtOk)
        mThis(self)->xfiGroup = groupId;
    /* power down serdes must be placed after groupId is updated
     * because powerdown will disable pw incase current serdes belong
     * to current mac. so to avoid pw is disabled when switching group,
     * the following code must be called:
     * */
    disableGroup = Tha60210051ModuleEthDisabledXfiGroup(self, groupId);
    return XfiGroupPowerdown(self, disableGroup);
    }

uint32 Tha60210051ModuleEthXfiGroupGet(AtModuleEth self)
    {
    if (self == NULL)
        return cBit31_0;

    if (!mMethodsGet((ThaModuleEth)self)->HasSerdesGroup((ThaModuleEth)self))
        return cBit31_0;

    return mThis(self)->xfiGroup;
    }

eAtRet Tha60210051ModuleEthExtraXfiGroupSet(AtModuleEth self, uint32 groupId)
    {
    eAtRet ret = cAtOk;
    uint32 disabledGroupId;

    if (self == NULL)
        return cAtErrorNullPointer;

    if (!Tha60210051ModuleEthCrossPointIsSupported(self))
        return cAtErrorModeNotSupport;

    if (!ThaModuleEthHasSerdesGroup((ThaModuleEth)self))
        return cAtErrorModeNotSupport;

    if (!ExtraXfiGroupIsValid(self, groupId))
        return cAtErrorOutOfRangParm;

    /* had been set then do nothing */
    if (Tha60210051ModuleEthXfiGroupIsSetup(self)
        && (groupId == mThis(self)->extraXfiGroup))
        return cAtOk;

    ret = Tha60210051ModuleEthCrossPointExtraXfiGroupSet(self, groupId);
    if (ret == cAtOk)
        mThis(self)->extraXfiGroup = groupId;

    /* power down serdes must be placed after groupId is updated
     * because powerdown will disable pw incase current serdes belong
     * to current mac. so to avoid pw is disabled when switching group,
     * the following code must be called:
     * */
    disabledGroupId = Tha60210051ModuleEthDisabledXfiGroup(self, groupId);
    return XfiGroupPowerdown(self, disabledGroupId);
    }

uint32 Tha60210051ModuleEthExtraXfiGroupGet(AtModuleEth self)
    {
    if (self == NULL)
        return cBit31_0;

    if (!ThaModuleEthHasSerdesGroup((ThaModuleEth)self))
        return cBit31_0;

    if (!Tha60210051ModuleEthCrossPointIsSupported(self))
        return cBit31_0;

    return mThis(self)->extraXfiGroup;
    }

eBool Tha60210051ModuleEthPowerControlIsSupported(AtModuleEth self)
    {
    if (self)
        return PowerControlIsSupported(self);
    return cAtFalse;
    }

eBool Tha60210051ModuleEthCrossPointIsSupported(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersionSupportCrossPoint = mMethodsGet(mThis(self))->StartHwVersionSupportCrossPoint(mThis(self));

    if (hwVersion >= startVersionSupportCrossPoint)
        return cAtTrue;

    return cAtFalse;
    }

uint32 Tha60210051ModuleEthStartHardwareVersionSupportRxErrPsnPacketsCounter(AtModuleEth self)
    {
    if (self)
        return mMethodsGet(mThis(self))->StartHardwareVersionSupportRxErrPsnPacketsCounter(mThis(self));
    return cInvalidUint32;
    }

eBool Tha60210051ModuleEthXfiGroupIsSetup(AtModuleEth self)
    {
    return mThis(self)->isGroupSetup;
    }

uint32 Tha60210051ModuleEthDisabledXfiGroup(AtModuleEth self, uint32 xfiGroup)
    {
    if (xfiGroup == 0)
        return 1;

    if (xfiGroup == 1)
        return 0;

    if (Tha60210051ModuleEthCrossPointIsSupported(self))
        {
        if (xfiGroup == 2)
            return 3;

        if (xfiGroup == 3)
            return 2;
        }

    return cBit31_0;
    }

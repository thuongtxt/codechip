/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Eth
 *
 * File        : Tha60210051EthExtraPort.c
 *
 * Created Date: Oct 3, 2016
 *
 * Description : Specific extra MAC implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60210051ModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210051EthExtraPort
    {
    tTha60210051EthPortCrossPoint super;
    } tTha60210051EthExtraPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tAtEthPortMethods m_AtEthPortOverride;

/* Save super implementation */
static const tAtEthPortMethods *m_AtEthPortMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return cAtFalse;
    }

static eAtRet Init(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "extra_eport";
    }

static eAtModuleEthRet SourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet SourceMacAddressGet(AtEthPort port, uint8 *address)
    {
    AtUnused(address);
    AtUnused(port);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet MacCheckingEnable(AtEthPort port, eBool enable)
    {
    AtUnused(port);
    return (enable) ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool MacCheckingIsEnabled(AtEthPort port)
    {
    AtUnused(port);
    return cAtFalse;
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed10G) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed10G) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortSpeed10G;
    }

static eAtModuleEthRet DuplexModeSet(AtEthPort self, eAtEthPortDuplexMode duplexMode)
    {
    AtUnused(duplexMode);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet TxIpgSet(AtEthPort self, uint8 txIpg)
    {
    AtUnused(txIpg);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint8 TxIpgGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEthRet RxIpgSet(AtEthPort self, uint8 rxIpg)
    {
    AtUnused(rxIpg);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint8 RxIpgGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEthRet AutoNegEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    return (enable) ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool AutoNegIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool AutoNegIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    AtUnused(interface);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtEthPortAutoNegState AutoNegStateGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet AllConfigGet(AtChannel self, void *pAllConfig)
    {
    AtUnused(self);
    AtUnused(pAllConfig);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet MaxPacketSizeSet(AtEthPort self, uint32 maxPacketSize)
    {
    AtUnused(maxPacketSize);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint32 MaxPacketSizeGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEthRet MinPacketSizeSet(AtEthPort self, uint32 minPacketSize)
    {
    AtUnused(minPacketSize);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint32 MinPacketSizeGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ProvisionedBandwidthInKbpsGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 RunningBandwidthInKbpsGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEthRet HiGigEnable(AtEthPort self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool HiGigIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet DicEnable(AtEthPort self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool DicIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet IpV4AddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV4AddressGet(AtEthPort self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV6AddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV6AddressGet(AtEthPort self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    return (loopbackMode == cAtLoopbackModeRelease) ? cAtTrue : cAtFalse;
    }

static eAtRet LedStateSet(AtEthPort self, eAtLedState ledState)
    {
    AtUnused(ledState);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtLedState LedStateGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtLedStateOff;
    }

static uint32 QueueCurrentBandwidthInBpsGet(AtEthPort self, uint8 queueId)
    {
    AtUnused(self);
    AtUnused(queueId);
    return 0;
    }

static AtList QueueAllPwsGet(AtEthPort self, uint8 queueId)
    {
    AtUnused(self);
    AtUnused(queueId);
    return NULL;
    }

static uint32 MaxQueuesGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static AtErrorGenerator TxErrorGeneratorGet(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static AtErrorGenerator RxErrorGeneratorGet(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, AllConfigGet);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, SourceMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, MacCheckingEnable);
        mMethodOverride(m_AtEthPortOverride, MacCheckingIsEnabled);
        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, DuplexModeSet);
        mMethodOverride(m_AtEthPortOverride, TxIpgSet);
        mMethodOverride(m_AtEthPortOverride, TxIpgGet);
        mMethodOverride(m_AtEthPortOverride, RxIpgSet);
        mMethodOverride(m_AtEthPortOverride, RxIpgGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegEnable);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsEnabled);
        mMethodOverride(m_AtEthPortOverride, AutoNegStateGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsSupported);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, MaxPacketSizeSet);
        mMethodOverride(m_AtEthPortOverride, MaxPacketSizeGet);
        mMethodOverride(m_AtEthPortOverride, MinPacketSizeSet);
        mMethodOverride(m_AtEthPortOverride, MinPacketSizeGet);
        mMethodOverride(m_AtEthPortOverride, ProvisionedBandwidthInKbpsGet);
        mMethodOverride(m_AtEthPortOverride, RunningBandwidthInKbpsGet);
        mMethodOverride(m_AtEthPortOverride, HiGigEnable);
        mMethodOverride(m_AtEthPortOverride, HiGigIsEnabled);
        mMethodOverride(m_AtEthPortOverride, DicEnable);
        mMethodOverride(m_AtEthPortOverride, DicIsEnabled);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressGet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressGet);
        mMethodOverride(m_AtEthPortOverride, LedStateSet);
        mMethodOverride(m_AtEthPortOverride, LedStateGet);
        mMethodOverride(m_AtEthPortOverride, QueueCurrentBandwidthInBpsGet);
        mMethodOverride(m_AtEthPortOverride, QueueAllPwsGet);
        mMethodOverride(m_AtEthPortOverride, MaxQueuesGet);
        mMethodOverride(m_AtEthPortOverride, TxErrorGeneratorGet);
        mMethodOverride(m_AtEthPortOverride, RxErrorGeneratorGet);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051EthExtraPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051EthPortCrossPointObjectInit((AtEthPort)self, portId, module) == NULL)
        return NULL;

    /* Initialize method */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60210051EthExtraPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

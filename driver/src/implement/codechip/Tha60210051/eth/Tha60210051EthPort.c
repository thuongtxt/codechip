/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : Tha60210051EthPort.c
 *
 * Created Date: Oct 23, 2015
 *
 * Description : Ethernet port of 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/eth/ThaEthPortInternal.h"
#include "../../Tha60210031/eth/Tha60210031EthPortErrorGenerator.h"
#include "Tha60210051ModuleEthReg.h"
#include "Tha60210051ModuleEthInternal.h"
#include "Tha60210051EthPortErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
/* ETH Port Register */
#define cEthPortSerdesSelectReg cThaReg_top_o_control0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60210051EthPort)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods      m_AtChannelOverride;
static tThaEthPortMethods     m_ThaEthPortOverride;
static tAtEthPortMethods      m_AtEthPortOverride;
static tAtObjectMethods       m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtEthPortMethods *m_AtEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MacIdToSerdesSelectMask(uint32 hwMacId)
    {
    if (hwMacId == 0)
        return cBit5_4;

    if (hwMacId == 1)
        return cBit9_8;

    return 0;
    }

static uint32 MacIdToSerdesSelectShift(uint32 hwMacId)
    {
    if (hwMacId == 0)
        return 4;

    if (hwMacId == 1)
        return 8;

    return 0;
    }

static uint32 HwSerdesToSelectVal(uint32 hwSerdesId)
    {
    switch (hwSerdesId)
        {
        case 0: return 0;
        case 1: return 0;
        case 4: return 1;
        case 5: return 1;
        case 2: return 2;
        case 3: return 2;
        case 6: return 3;
        case 7: return 3;
        default:
            return 0;
        }
    }

static uint32 HwValToHwSerdes(uint32 hwMacId, uint32 val)
    {
    if (hwMacId == 0)
        {
        switch (val)
            {
            case 0: return 0;
            case 1: return 4;
            case 2: return 2;
            case 3: return 6;
            default:
                return 0;
            }
        }
    else
        {
        switch (val)
            {
            case 0: return 1;
            case 1: return 5;
            case 2: return 3;
            case 3: return 7;
            default:
                return 1;
            }
        }
    }

static eBool SerdesIsInMac(uint32 hwMacId, AtSerdesController serdes)
    {
    uint32 serdesId = mMethodsGet(serdes)->HwIdGet(serdes);

    /* mac#0: hwserdes 0,4,2,6
     * mac#1: hwserdes 1,5,3,7*/

    if ((hwMacId > cMaxMacs) || (serdesId > 7))
        return cAtFalse;

    if (hwMacId == 0 && ((serdesId % 2) == 0))
        return cAtTrue;

    if (hwMacId == 1 && ((serdesId % 2) != 0))
        return cAtTrue;

    return cAtFalse;
    }

static eBool SerdesIsInGroup(uint32 groupId, AtSerdesController serdes)
    {
    /* 2 xfi sw group: 0(0,1,4,5), 1(2,3,6,7) */
    uint32 serdesId = AtSerdesControllerIdGet(serdes);

    if (groupId == 0 && (serdesId == 0 || serdesId == 1 || serdesId == 4 || serdesId == 5))
        return cAtTrue;

    if (groupId == 1 && (serdesId == 2 || serdesId == 3 || serdesId == 6 || serdesId == 7))
        return cAtTrue;

    return cAtFalse;
    }

static eBool SerdesOperationIsValid(AtEthPort self, AtSerdesController serdes)
    {
    AtModuleEth module = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint32 xfiGroup;
    uint32 portHwId;

    if (!ThaModuleEthHasSerdesGroup((ThaModuleEth)module))
        return cAtTrue;

    xfiGroup = Tha60210051ModuleEthXfiGroupGet(module);
    portHwId = AtChannelHwIdGet((AtChannel)self);

    if (!SerdesIsInGroup(xfiGroup, serdes) || !SerdesIsInMac(portHwId, serdes))
        return cAtFalse;

    return cAtTrue;
    }

static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    AtModuleEth module = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint32 regVal = mChannelHwRead(self, cEthPortSerdesSelectReg, cAtModuleEth);
    uint32 portHwId = AtChannelHwIdGet((AtChannel)self);
    uint32 serdesHwId = mMethodsGet(serdes)->HwIdGet(serdes);
    uint32 mask, shift, val;

    if (Tha60210051ModuleEthXfiGroupIsSetup(module) && !SerdesOperationIsValid(self, serdes))
        return cAtErrorInvlParm;

    mask = MacIdToSerdesSelectMask(portHwId);
    shift = MacIdToSerdesSelectShift(portHwId);
    val = HwSerdesToSelectVal(serdesHwId);
    mFieldIns(&regVal, mask, shift, val);
    mChannelHwWrite(self, cEthPortSerdesSelectReg, regVal, cAtModuleEth);

    return cAtOk;
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    ThaModuleEth module = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    uint32 regVal = mChannelHwRead(self, cEthPortSerdesSelectReg, cAtModuleEth);
    uint32 portHwId = AtChannelHwIdGet((AtChannel)self);
    uint32 serdesHwId, serdesSwId;
    uint32 mask, shift, val;

    mask  = MacIdToSerdesSelectMask(portHwId);
    shift = MacIdToSerdesSelectShift(portHwId);
    mFieldGet(regVal, mask, shift, uint32, &val);
    serdesHwId = HwValToHwSerdes(portHwId, val);
    serdesSwId = mMethodsGet(module)->SerdesIdHwToSw(module, serdesHwId);

    return AtModuleEthSerdesController((AtModuleEth)module, serdesSwId);
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    if (!SerdesOperationIsValid(self, serdes))
        return cAtErrorModeNotSupport;

    /* HW bridge as default */
    return cAtOk;
    }

static eBool TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    return SerdesOperationIsValid(self, serdes);
    }

static AtModuleEth ModuleEth(AtEthPort self)
    {
    return (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    AtModuleEth moduleEth = ModuleEth(self);
    uint32 brigdedSerdesId;
    AtSerdesController controller = AtEthPortSerdesController(self);
    uint32 serdesId = AtSerdesControllerIdGet((controller));

    brigdedSerdesId = serdesId + 4;

    return AtModuleEthSerdesController(moduleEth, brigdedSerdesId);
    }

static eBool DebugCounterIsSupported(ThaEthPort10Gb self, uint8 counterType)
    {
    eAtEthPortCounterType typeCounter = counterType;

    AtUnused(self);
    if ((typeCounter == cAtEthPortCounterTxPackets)             ||
        (typeCounter == cAtEthPortCounterTxBytes)               ||
        (typeCounter == cAtEthPortCounterTxPacketsLen0_64)      ||
        (typeCounter == cAtEthPortCounterTxPacketsLen65_127)    ||
        (typeCounter == cAtEthPortCounterTxPacketsLen128_255)   ||
        (typeCounter == cAtEthPortCounterTxPacketsLen256_511)   ||
        (typeCounter == cAtEthPortCounterTxPacketsLen512_1024)  ||
        (typeCounter == cAtEthPortCounterTxPacketsLen1025_1528) ||
        (typeCounter == cAtEthPortCounterTxPacketsLen1529_2047) ||
        (typeCounter == cAtEthPortCounterTxPacketsJumbo)        ||
        (typeCounter == cAtEthPortCounterTxBrdCastPackets)      ||
        (typeCounter == cAtEthPortCounterTxMultCastPackets)     ||
        (typeCounter == cAtEthPortCounterTxUniCastPackets)      ||
        (typeCounter == cAtEthPortCounterRxPackets)             ||
        (typeCounter == cAtEthPortCounterRxBytes)               ||
        (typeCounter == cAtEthPortCounterRxPacketsLen0_64)      ||
        (typeCounter == cAtEthPortCounterRxPacketsLen65_127)    ||
        (typeCounter == cAtEthPortCounterRxPacketsLen128_255)   ||
        (typeCounter == cAtEthPortCounterRxPacketsLen256_511)   ||
        (typeCounter == cAtEthPortCounterRxPacketsLen512_1024)  ||
        (typeCounter == cAtEthPortCounterRxPacketsLen1025_1528) ||
        (typeCounter == cAtEthPortCounterRxPacketsLen1529_2047) ||
        (typeCounter == cAtEthPortCounterRxPacketsJumbo)        ||
        (typeCounter == cAtEthPortCounterRxOversizePackets)     ||
        (typeCounter == cAtEthPortCounterRxUndersizePackets)    ||
        (typeCounter == cAtEthPortCounterRxUniCastPackets)      ||
        (typeCounter == cAtEthPortCounterRxBrdCastPackets)      ||
        (typeCounter == cAtEthPortCounterRxMultCastPackets)     ||
        (typeCounter == cAtEthPortCounterRxErrFcsPackets))
        return cAtTrue;

    return cAtFalse;
    }

static ThaVersionReader VersionReader(AtModuleEth self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static eBool RxErrPsnPacketsCounterIsSupported(AtChannel self)
    {
    AtModuleEth moduleEth = ModuleEth((AtEthPort) self);
    ThaVersionReader versionReader = VersionReader(moduleEth);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (hwVersion >= Tha60210051ModuleEthStartHardwareVersionSupportRxErrPsnPacketsCounter(moduleEth))
        return cAtTrue;

    return cAtFalse;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (counterType == cAtEthPortCounterRxErrPsnPackets)
        return RxErrPsnPacketsCounterIsSupported(self);

    return DebugCounterIsSupported((ThaEthPort10Gb)self, (uint8)counterType);
    }

static uint32 MaxBandwidthInKbps(ThaEthPort self)
    {
    AtUnused(self);
    return 10000000; /* 10G */
    }

static uint32 RemainingBpsOfMaxBandwidth(ThaEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    ThaEthSerdesManager serdesManager = ThaModuleEthSerdesManager(ethModule);
    uint32 serdesId, portId;

    portId = AtChannelHwIdGet((AtChannel)self);
    serdesId = mMethodsGet(ethModule)->EthPortIdToSerdesId(ethModule, portId);

    if (serdesManager)
        return ThaEthSerdesManagerSerdesController(serdesManager, serdesId);

    return m_AtEthPortMethods->SerdesController(self);
    }

static void ErrorGeneneratorDelete(AtChannel self)
    {
    AtObjectDelete((AtObject)mThis(self)->errorGenerator);
    mThis(self)->errorGenerator = NULL;
    }

static void ErrorGeneratorDefaultInit(AtChannel self)
    {
    Tha60210051EthPortErrorGeneratorHwDefaultSet(self);
    ErrorGeneneratorDelete(self);
    }

static eAtRet Init(AtChannel self)
    {
    static uint8 defaultMac[6] = {0, 0, 0, 0, 0, 0};
    AtEthPort port = (AtEthPort)self;
    eAtRet ret = m_AtChannelMethods->Init(self);

    if (!AtEthPortHasMacFunctionality(port))
        return cAtOk;

    ret |= AtEthPortTxIpgSet(port, 8);
    ret |= AtEthPortRxIpgSet(port, 4);
    if (ret != cAtOk)
        return ret;

    AtEthPortSourceMacAddressSet((AtEthPort)self, defaultMac);

    if (Tha60210051EthPortErrorGeneratorIsSupported(self))
        ErrorGeneratorDefaultInit(self);

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    /* Delete internal objects */
    ErrorGeneneratorDelete((AtChannel)self);

    /* Fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static AtErrorGenerator ErrorGeneratorGet(AtEthPort self, eAtSide side)
    {
    if (!Tha60210051EthPortErrorGeneratorIsSupported((AtChannel)self))
        return NULL;

    if (mThis(self)->errorGenerator == NULL)
        mThis(self)->errorGenerator = Tha60210051EthPortErrorGeneratorNew((AtChannel)self);

    if (AtErrorGeneratorIsStarted(mThis(self)->errorGenerator))
        {
        if (Tha60210031EthPortErrorGeneratorSideGet(mThis(self)->errorGenerator) != side)
            return NULL;
        }

    Tha60210031EthPortErrorGeneratorSideSet(mThis(self)->errorGenerator, side);
    return mThis(self)->errorGenerator;
    }

static AtErrorGenerator TxErrorGeneratorGet(AtEthPort self)
    {
    return ErrorGeneratorGet(self, cAtSideTx);
    }

static AtErrorGenerator RxErrorGeneratorGet(AtEthPort self)
    {
    return ErrorGeneratorGet(self, cAtSideRx);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60210051EthPort object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(errorGenerator);
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort ethPort = (ThaEthPort)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, mMethodsGet(ethPort), sizeof(m_ThaEthPortOverride));
        mMethodOverride(m_ThaEthPortOverride, MaxBandwidthInKbps);
        mMethodOverride(m_ThaEthPortOverride, RemainingBpsOfMaxBandwidth);
        }

    mMethodsSet(ethPort, &m_ThaEthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, RxSerdesSelect);
        mMethodOverride(m_AtEthPortOverride, RxSelectedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesBridge);
        mMethodOverride(m_AtEthPortOverride, TxBridgedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesCanBridge);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, TxErrorGeneratorGet);
        mMethodOverride(m_AtEthPortOverride, RxErrorGeneratorGet);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051EthPort);
    }

AtEthPort Tha60210051EthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011EthPortObjectInit((AtEthPort)self, portId, module) == NULL)
        return NULL;

    /* Initialize method */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60210051EthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60210051EthPortObjectInit(newPort, portId, module);
    }

void Tha60210051EthPortSerdesWillPowerDownNotify(ThaEthPort self)
    {
    tListenerWraper* wrapper;
    AtIterator iterator;

    if (self->listeners == NULL)
        return;

    iterator = AtListIteratorCreate(self->listeners);
    while ((wrapper = (tListenerWraper*)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->listener.SerdesPowerControlWillPowerDown)
            wrapper->listener.SerdesPowerControlWillPowerDown((AtEthPort)self, wrapper->userData);
        }

    AtObjectDelete((AtObject)iterator);
    }


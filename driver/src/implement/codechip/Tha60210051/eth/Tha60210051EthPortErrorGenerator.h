/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : 60210051 ETH
 * 
 * File        : Tha60210051EthPortErrorGenerator.h
 * 
 * Created Date: Apr 6, 2016
 *
 * Description : Interface of the 60210051 ETH port error generator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051ETHPORTERRORGENERATOR_H_
#define _THA60210051ETHPORTERRORGENERATOR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60210051EthPortErrorGeneratorIsSupported(AtChannel self);
void Tha60210051EthPortErrorGeneratorHwDefaultSet(AtChannel self);
AtErrorGenerator Tha60210051EthPortErrorGeneratorNew(AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60210051ETHPORTERRORGENERATOR_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60210051ModuleEthReg.h
 * 
 * Created Date: Oct 24, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60210051MODULEETHREG_H_
#define _THA60210051MODULEETHREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cThaReg_top_o_control0              0xF00040
#define cThaReg_XFI_Mac0_Mux1_Sel_Mask      cBit5
#define cThaReg_XFI_Mac0_Mux1_Sel_Shift     5
#define cThaReg_XFI_Mac0_Mux2_Sel_Mask      cBit4
#define cThaReg_XFI_Mac0_Mux2_Sel_Shift     4

#define cThaReg_XFI_Mac1_Mux1_Sel_Mask      cBit9
#define cThaReg_XFI_Mac1_Mux1_Sel_Shift     9
#define cThaReg_XFI_Mac1_Mux2_Sel_Mask      cBit8
#define cThaReg_XFI_Mac1_Mux2_Sel_Shift     8

#define cThaReg_top_o_control1              0xF00041

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60210051MODULEETHREG_H_ */


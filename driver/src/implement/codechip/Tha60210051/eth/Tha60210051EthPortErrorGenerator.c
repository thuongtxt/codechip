/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : 60210051 ETH
 *
 * File        : Tha60210051EthPortErrorGenerator.c
 *
 * Created Date: Apr 6, 2016
 *
 * Description : Error generator for the 60210051 ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../../../default/eth/ThaEthPort.h"
#include "../../Tha60210031/eth/Tha60210031EthPortErrorGeneratorInternal.h"
#include "../man/Tha60210051Device.h"
#include "Tha60210051EthPortErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_ETH_FCS_errins_en_cfg 0x0000007

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60210051EthPortErrorGenerator
    {
    tTha60210031EthPortErrorGenerator super;
    }tTha60210051EthPortErrorGenerator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210031EthPortErrorGeneratorMethods m_Tha60210031EthPortErrorGeneratorOverride;

/* Save super implementation */
static const tTha60210031EthPortErrorGeneratorMethods *m_Tha60210031EthPortErrorGeneratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ETH_FCS_errins_en_cfg_Address(Tha60210031EthPortErrorGenerator self)
    {
    AtEthPort port = (AtEthPort)AtErrorGeneratorSourceGet((AtErrorGenerator)self);
    return cAf6Reg_ETH_FCS_errins_en_cfg + ThaEthPortMacBaseAddress((ThaEthPort)port);
    }

static eBool NeedDisablePwReoderingOnPcsErrorForcing(Tha60210031EthPortErrorGenerator self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void Tha60210031ErrorGeneratorOverride(AtErrorGenerator self)
    {
    Tha60210031EthPortErrorGenerator generator = (Tha60210031EthPortErrorGenerator)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210031EthPortErrorGeneratorMethods = mMethodsGet(generator);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031EthPortErrorGeneratorOverride, m_Tha60210031EthPortErrorGeneratorMethods, sizeof(m_Tha60210031EthPortErrorGeneratorOverride));

        /* Setup methods */
        mMethodOverride(m_Tha60210031EthPortErrorGeneratorOverride, ETH_FCS_errins_en_cfg_Address);
        mMethodOverride(m_Tha60210031EthPortErrorGeneratorOverride, NeedDisablePwReoderingOnPcsErrorForcing);
        }

    mMethodsSet(generator, &m_Tha60210031EthPortErrorGeneratorOverride);
    }

static void Override(AtErrorGenerator self)
    {
    Tha60210031ErrorGeneratorOverride(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210051EthPortErrorGenerator);
    }

static AtErrorGenerator ObjectInit(AtErrorGenerator self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031EthPortErrorGeneratorObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtErrorGenerator Tha60210051EthPortErrorGeneratorNew(AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtErrorGenerator errorGenerator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (errorGenerator == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(errorGenerator, channel);
    }

void Tha60210051EthPortErrorGeneratorHwDefaultSet(AtChannel self)
    {
    uint32 address;
    AtUnused(self);
    address = cAf6Reg_ETH_FCS_errins_en_cfg + ThaEthPortMacBaseAddress((ThaEthPort)self);
    mChannelHwWrite(self, address, 0, cAtModuleEth);
    }

eBool Tha60210051EthPortErrorGeneratorIsSupported(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return Tha60210051DeviceEthErrorGeneratorIsSupported(device);
    }
